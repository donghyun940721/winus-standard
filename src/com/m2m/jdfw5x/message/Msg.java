package com.m2m.jdfw5x.message;

@SuppressWarnings("PMD")
public class Msg {

	private String message;
	private boolean alert;
	private String returlUrl;
	private String urlTarget;

	public String getMessage() {
		return message;
	}

	public boolean isAlert() {
		return alert;
	}

	public void setAlert(boolean alert) {
		this.alert = alert;
	}

	public String getReturlUrl() {
		return returlUrl;
	}

	public void setReturlUrl(String returlUrl) {
		this.returlUrl = returlUrl;
	}

	public String getUrlTarget() {
		return urlTarget;
	}

	public void setUrlTarget(String urlTarget) {
		this.urlTarget = urlTarget;
	}

	public Msg(String message) {
		this.message = message;
	}

	public Msg() {
		message = "";
	}
}