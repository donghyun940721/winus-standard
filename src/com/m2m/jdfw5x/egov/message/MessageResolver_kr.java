package com.m2m.jdfw5x.egov.message;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.MessageSourceAccessor;

public class MessageResolver_kr {

	protected static Log log = LogFactory.getLog(MessageResolver_kr.class);

	private static MessageSourceAccessor msgAssr;
	final static Locale LOCALE = Locale.KOREAN;

	public MessageResolver_kr() {
	}

	public synchronized void setMessageSourceAccessor(MessageSourceAccessor msgAssr) {
		MessageResolver_kr.msgAssr = msgAssr;
	}

	public static String getMessage(MessageSourceResolvable msgReslv) {
		return msgAssr.getMessage(msgReslv, LOCALE);
	}

	public static String getMessage(String code) {
		return msgAssr.getMessage(code, LOCALE);
	}
	
	public static String getText(String code) {
		return msgAssr.getMessage(code, LOCALE);
	}

	public static String getMessage(String code, Object args[]) {
		if (args == null)
			return msgAssr.getMessage(code);
		else
			return msgAssr.getMessage(code, args);
	}

	public static String getMessage(String code, Object args[], Locale locale) {
		return msgAssr.getMessage(code, args, locale);
	}

	public static String getMessage(String code, Object args[], String msgStr) {
		return msgAssr.getMessage(code, args, msgStr);
	}

	public static String getMessage(String code, Object args[], String msgStr,
			Locale locale) {
		return msgAssr.getMessage(code, args, msgStr, locale);
	}

}
