package com.m2m.jdfw5x.egov.database;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;

import egovframework.rte.psl.orm.ibatis.support.SqlMapClientDaoSupport;

public class SqlMapAbstractDAO extends SqlMapClientDaoSupport {
	protected Log loger = LogFactory.getLog(super.getClass());

	SqlMapClient sqlMapClient = null;

	@Resource(name = "sqlMapClient")
	public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
		super.setSqlMapClient(sqlMapClient);
	}

	protected List executeQueryForList(String statementName, Object obj) {
		return getSqlMapClientTemplate().queryForList(statementName, obj);
	}

	protected List executeQueryForList(String statementName, Object obj, int skipResults, int maxResults) {
		return getSqlMapClientTemplate().queryForList(statementName, obj, skipResults, maxResults);
	}

	protected Object executeQueryForObject(String statementName, Object obj) {
		return getSqlMapClientTemplate().queryForObject(statementName, obj);
	}

	protected Object executeInsert(String statementName, Object obj) {
		return getSqlMapClientTemplate().insert(statementName, obj);
	}

	protected int executeUpdate(String statementName, Object obj) {
		return getSqlMapClientTemplate().update(statementName, obj);
	}

	protected int executeDelete(String statementName, Object obj) {
		return getSqlMapClientTemplate().delete(statementName, obj);
	}

	protected Object executeView(String statementName, Object obj) {
		if (obj instanceof Map) {
			super.setSqlMapClient(this.sqlMapClient);
		}

		return getSqlMapClientTemplate().queryForObject(statementName, obj);
	}

	protected GenericResultSet executeQueryWq(String statementName, Object obj) {
		GenericResultSet wqrs = new GenericResultSet();

		List listWqrs = getSqlMapClientTemplate().queryForList(statementName, obj);

		wqrs.setCpage(1);
		wqrs.setTpage(1);
		wqrs.setTotCnt(listWqrs.size());
		wqrs.setList(listWqrs);

		return wqrs;
	}

	protected GenericResultSet executeQueryMaxWq(String statementName, Object obj, int maxResult) {
		GenericResultSet wqrs = new GenericResultSet();

		List listWqrs = getSqlMapClientTemplate().queryForList(statementName, obj, 0, maxResult);

		wqrs.setCpage(1);
		wqrs.setTpage(1);
		wqrs.setTotCnt(listWqrs.size());
		wqrs.setList(listWqrs);

		return wqrs;
	}

	protected GenericResultSet executeQueryPageWq(String statementName, Map map) {
		GenericResultSet wqrs = new GenericResultSet();

		if (!(map.containsKey("pageIndex"))) {
			map.put("pageIndex", "1");
		}
		if (!(map.containsKey("pageSize"))) {
			map.put("pageSize", "15");
		}

		Object tmpPageIndex = map.get("pageIndex");

		if ((tmpPageIndex == null) || (tmpPageIndex.equals(""))) {
			tmpPageIndex = "1";
		}
		int pageIndex = Integer.parseInt(String.valueOf(tmpPageIndex));

		int pageSize = Integer.parseInt((String) map.get("pageSize"));

		int sPageNum = (pageIndex - 1) * pageSize + 1;

		int pageTotal = ((Integer) getSqlMapClientTemplate().queryForObject(statementName + "Count", map)).intValue();

		int pageBlank = (int) Math.ceil(pageTotal / (double) pageSize);
		if (pageBlank < 1)
			pageBlank = 1;
		map.put("S_PAGE_LEN", map.get("pageSize"));
		map.put("S_PAGE_NUM", Integer.valueOf(sPageNum));

		wqrs.setCpage(pageIndex);
		wqrs.setTpage(pageBlank);
		wqrs.setTotCnt(pageTotal);
		wqrs.setList(getSqlMapClientTemplate().queryForList(statementName, map));
		return wqrs;
	}

	protected List list(String statementName, Object obj) {
		return getSqlMapClientTemplate().queryForList(statementName, obj);
	}

	protected List listWithPaging(String statementName, Object obj, int pageIndex, int pageSize) {
		int skipResults = pageIndex * pageSize;
		int maxResults = pageIndex * pageSize + pageSize;
		return getSqlMapClientTemplate().queryForList(statementName, obj, skipResults, maxResults);
	}
   protected int executeInsertBatch(String statementName, List list, int size) {
       int result = 0;
       SqlMapClient sqlMapClient = getSqlMapClient();
       try{
           sqlMapClient.startTransaction();
           sqlMapClient.startBatch();
           
           int total = list.size();
           for(int i = 0; i < total ; i++){
               sqlMapClient.insert(statementName,list.get(i));
               if(i%size == 0){
                   sqlMapClient.executeBatch();
                   sqlMapClient.startBatch();
               }
           }
           sqlMapClient.executeBatch();
           sqlMapClient.commitTransaction();
           
           result = 1;
           
       } catch (Exception e){
    	   e.printStackTrace();
       } finally {
           try {
               sqlMapClient.endTransaction();
           } catch(SQLException e){
               e.printStackTrace();
           }
       }
       
        return result; 
    }
   
   protected int executeInsertBatch(String[] statementNames, List[] lists, int size) {
	    int result = 0;
	    SqlMapClient sqlMapClient = getSqlMapClient();
	    
	    try {
	        sqlMapClient.startTransaction();
	        sqlMapClient.startBatch();

	        // statementNames와 lists는 같은 길이여야 함
	        int totalBatches = statementNames.length;

	        for (int batchIndex = 0; batchIndex < totalBatches; batchIndex++) {
	            String statementName = statementNames[batchIndex];
	            List<Object> list = lists[batchIndex];

	            int total = list.size();
	            for (int i = 0; i < total; i++) {
	                sqlMapClient.insert(statementName, list.get(i));
	                if (i % size == 0) {
	                    sqlMapClient.executeBatch();
	                    sqlMapClient.startBatch();
	                }
	            }
	        }

	        // 최종 배치 실행
	        sqlMapClient.executeBatch();
	        sqlMapClient.commitTransaction();

	        result = 1;

	    } catch (Exception e) {
	        e.printStackTrace();
	        throw new RuntimeException("Error occurred while executing batch insert.", e); // 예외 전달
	    } finally {
	        try {
	            sqlMapClient.endTransaction();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }

	    return result;
	}
}