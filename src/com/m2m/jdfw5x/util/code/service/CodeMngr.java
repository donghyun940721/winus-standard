
package com.m2m.jdfw5x.util.code.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component("CodeMngr")
public final class CodeMngr {
    protected static final Log LOGGER = LogFactory.getLog("CodeMngr");

    protected static CodeMngr instn;

    protected static final boolean LODAFLAG = true;

    CodeMnVO codeMnVo = new CodeMnVO();

    public CodeMngr() {
    }

    public synchronized static CodeMngr getInstance() throws Exception {
        if (instn == null) {
            throw new Exception("CodeMngr instance is null.");
        }
        return instn;
    }

    public synchronized static void init() throws Exception {
        if (instn == null) {
            LOGGER.info("[SUCCESS] CodeMngr init Start!.");

            instn = new CodeMngr();
            try {
                instn.setCodeProperties();
            } catch (Exception localException) {
            }
        }
    }

    public static void reload() throws Exception {
        LOGGER.info("[SUCCESS] CodeMngr : try reload!.");

        instn = null;
        init();
    }

    private void setCodeProperties() {
        Connection con = null;
        StringBuffer sql = new StringBuffer();
        PreparedStatement statement = null;
        ResultSet rs = null;
        int i = 0;
        try {
            con = CodeDB.getDataSource().getConnection();

            sql.append(" SELECT                                  \r\n")
                    .append("     /*+ INDEX(T01 PK_TMSYS030) */       \r\n")
                    .append("     'COMM' AS CODE_TYPE                 \r\n")
                    .append("     , CODE_TYPE_CD AS CODE_GRP          \r\n")
                    .append("     , '' AS CODE_GRP_NM                 \r\n")
                    .append("     , BA_CODE_CD AS CODE                \r\n")
                    .append("     , BA_CODE_NM  AS CODE_NM            \r\n")
                    .append("     , VIEW_SEQ AS SORT_SEQ              \r\n")
                    .append("     , 'Y' AS USE_YN                     \r\n")
                    .append(" FROM TMSYS030                           \r\n")
                    .append(" WHERE DEL_YN = 'N'                      \r\n");
            /*
             * .append("UNION ALL                   \r\n")
             * .append("SELECT 'NAT' AS CODE_TYPE   \r\n")
             * .append(", FTA_AGRE_CD AS CODE_GRP   \r\n")
             * .append(", FTA_AGRE AS CODE_GRP_NM   \r\n")
             * .append(", NAT_CD AS CODE            \r\n")
             * .append(", KOR_NAT_NM AS CODE_NM     \r\n")
             * .append(", 1 AS SORT_SEQ             \r\n")
             * .append(", 'Y' AS USE_YN             \r\n")
             * .append("FROM NATION_CODE            \r\n")
             * .append("UNION ALL           				\r\n")
             * .append("SELECT 'DRB' AS CODE_TYPE         	\r\n")
             * .append("     , COMM_CLS AS CODE_GRP     	\r\n")
             * .append("     , DESC1 AS CODE_GRP_NM      	\r\n")
             * .append("     , COMM_CD AS CODE             \r\n")
             * .append("     , DESC2 AS CODE_NM            \r\n")
             * .append("     , SEQ AS SORT_SEQ             \r\n")
             * .append("     , USE_NOT AS USE_YN           \r\n")
             * .append("FROM TCD_COMM_C                   	\r\n");
             */

            statement = con.prepareStatement(sql.toString());
            rs = statement.executeQuery();

            while (rs.next()) {
                CodeVO code = new CodeVO();
                code.setCode(rs.getString("CODE"));
                code.setCodeNm(rs.getString("CODE_NM"));
                code.setUseYn(rs.getString("USE_YN"));
                code.setSortSeq(Integer.valueOf(rs.getInt("SORT_SEQ")));
                CodeTypeVO codeType;
                if (this.codeMnVo.isExist(rs.getString("CODE_TYPE")).booleanValue()) {
                    codeType = this.codeMnVo.get(rs.getString("CODE_TYPE"));
                } else {
                    codeType = new CodeTypeVO();
                    codeType.setCodeType(rs.getString("CODE_TYPE"));
                }
                CodeGrpVO codeGroup;
                if (codeType.isExist(rs.getString("CODE_GRP")).booleanValue()) {
                    codeGroup = codeType.getCodeGrp(rs.getString("CODE_GRP"));
                } else {
                    codeGroup = new CodeGrpVO();
                    codeGroup.setCodeGrp(rs.getString("CODE_GRP"));
                    codeGroup.setCodeGrpNm(rs.getString("CODE_GRP_NM"));
                }
                codeGroup.add(code);
                codeType.add(codeGroup);
                this.codeMnVo.add(codeType);
                i++;
            }
            if ( LOGGER.isDebugEnabled() ) {
            	LOGGER.debug("CodeMngr init Success::" + i + "Codes Loaded");
            }
            
        } catch (Exception e) {
            if (LOGGER.isWarnEnabled()) {
            	LOGGER.warn("[ERROR] CodeMngr : Can't read from code master table!", e);
            }
        } finally {
            try {
            	if (rs != null)
                rs.close();
            } catch (Exception localException3) {
            }
            try {
            	if (statement != null)
                statement.close();
            } catch (Exception localException4) {
            }
            if (con != null)
                try {
                    con.close();
                } catch (SQLException localSQLException1) {
                }
        }
    }

    public String getCodeName(String groupId, String code) {
        return getCodeNm("COMM", groupId, code);
    }

    public String getCodeNm(String groupType, String groupId, String code) {
        CodeTypeVO codeType = this.codeMnVo.get(groupType);
        CodeGrpVO codeGroup = codeType.getCodeGrp(groupId);
        CodeVO c = codeGroup.getCode(code);
        return c.getCodeNm();
    }

    public String getGroupName(String groupId) {
        return getGroupName("COMM", groupId);
    }

    public String getGroupName(String groupType, String groupId) {
        CodeTypeVO codeType = this.codeMnVo.get(groupType);
        CodeGrpVO codeGroup = codeType.getCodeGrp(groupId);
        /**
         * if (codeGroup != null) { return codeGroup.getCodeGrpNm(); } return
         * "";
         */
        /*** 수정 ***/
        if (codeGroup != null) {
            return codeGroup.getCodeGrpNm();
        } else {
            return "";
        }

    }

    public List<CodeVO> getValues(String groupId) {
        return getValues("COMM", groupId);
    }

    public List<CodeVO> getValues(String groupType, String groupId) {
        CodeTypeVO codeType = this.codeMnVo.get(groupType);
        CodeGrpVO codeGroup = codeType.getCodeGrp(groupId);
        CodeVO code = null;
        // List values = new ArrayList();
        /*** 수정(위에꺼 써도 상관은 없을듯??) ***/
        List<CodeVO> values = new ArrayList<CodeVO>();

        for (String key : codeGroup.getCode().keySet()) {
            code = codeGroup.getCode(key);

            if (code.isUseYn().booleanValue()) {
                values.add(code);
            }

        }

        // Comparator comp = new CodeMngr.1(this);
        /*** 수정 ***/
        Comparator<CodeVO> comp = new Comparator<CodeVO>() {
            public int compare(CodeVO o1, CodeVO o2) {
                if (o1.getSortSeq() > o2.getSortSeq()) {
                    return 1;
                } else if (o1.getSortSeq() < o2.getSortSeq()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };

        Collections.sort(values, comp);

        return values;
    }

    public List<CodeVO> getValues(String groupType, String groupId, ArrayList op) {
        CodeTypeVO codeType = this.codeMnVo.get(groupType);
        CodeGrpVO codeGroup = codeType.getCodeGrp(groupId);
        CodeVO code = null;
        // ArrayList codes = new ArrayList();
        /*** 수정(위에꺼 써도 상관은 없을듯??) ***/
        ArrayList<CodeVO> codes = new ArrayList<CodeVO>();

        for (String key : codeGroup.getCode().keySet()) {
            code = codeGroup.getCode(key);
            if (code.isUseYn().booleanValue()) {
                codes.add(code);
            }
        }

        /*** 수정(위에꺼 써도 상관은 없을듯??) ***/
        List<CodeVO> result = new ArrayList<CodeVO>();
        for (Iterator<String> iter = op.iterator(); iter.hasNext();) {
            // key = iter.next();
            // code = codeGroup.getCode(key);
            result.add(code);
        }
        return result;

    }
}
