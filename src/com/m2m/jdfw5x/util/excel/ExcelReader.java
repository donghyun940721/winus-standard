package com.m2m.jdfw5x.util.excel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.formula.ConditionalFormattingEvaluator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.binary.XSSFBSheetHandler.SheetContentsHandler;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.CommentsTable;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;


/**
 * Excel Reader Public Class Use the Method written below
 * @method {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} => Excel Handler Reading 기본 메소드
 * @method {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startSheet, int startRow, int endRow, int startCell)} => Excel Handler Reading 오버로딩용 메소드
 * @method {@link ExcelReader#excelReadHandlerByTemplate(List excelTemplate, File file)} => Excel Handler Reading Template Data 버전
 * @method {@link ExcelReader#excelLimitRowReadByHandlerWithPassword(File file, String[] cellName, int startRow, String password)} => Excel Handler Reading 파일 패스워드 추가 
 * @method {@link ExcelReader#excelReadHandlerByTemplateWithPassword(List excelTemplate, File file, String password)} => Excel Handler Reading 파일 패스워드 추가  Template Data 버전
*/
public class ExcelReader {
	
	protected static final Log logger = LogFactory.getLog(ExcelReader.class);

	/*
     * 
     * HANDLER READING
     * 
    */
	
    /**
     * Excel Handler Reading 기본 메소드
     * @param file File => 업로드 파일
     * @param cellName String[] => 매칭될 컬럼명
     * @param startRow int => 시작열 (시작열 이전 데이터 무시)
     * @throws Exception
     * @return List => Map<컬럼, 데이터> List로 리턴.
     * @date   23.08.16
     * @description 기존 excelLimitRowRead 대체 메소드
     * <br>엑셀 확장자 .xls 일 경우, 기존로직(
     * {@link ExcelReader#rReadLimitRow(Workbook workbook,String[] cellName, int maxRow, int startSheet, int startRow,int endRow, int startCell, int mergeGubun)}
     * )사용
    */
    public static List<Map> excelLimitRowReadByHandler(File file, String[] cellName, int startRow) throws Exception {
        List<Map> list = null;

        BufferedInputStream bis = null;
        try {
            if (!file.exists()) {
                throw new Exception("FIle Not EXIXT...");
            }
            
            String filename = file.getPath();
            
            //FSUtil.fileDecrypt(filename, filename);
            
            bis = new BufferedInputStream(new FileInputStream(file));
            ZipSecureFile.setMinInflateRatio(-1.0d);
            
            SheetHandler sheetHandler = new SheetHandler(cellName,startRow); 
            
            String name = file.getName();
            
            if(name.endsWith(".xls")){
                //hssf 기존방식
                Workbook workbook = WorkbookFactory.create(bis);
                list = rReadLimitRow(workbook, cellName, 100000, 0, startRow, 1000, 0, 0);
            }
            else{
                //SAX 방식 xml package
                OPCPackage pkg = OPCPackage.open(bis);
                //poi excel reader
                XSSFReader xssfReader = new XSSFReader(pkg);
                                
                //get data in excel
                ReadOnlySharedStringsTable data = new ReadOnlySharedStringsTable(pkg);
                
                //get style in poi excel reader
                StylesTable styles = xssfReader.getStylesTable();
                //InputStream st = new FileInputStream(file);
                //get first sheet
                InputStream sheetStream = xssfReader.getSheetsData().next();
                /*
                //get all sheets example
                Iterator<InputStream> sheets = xssfReader.getSheetsData();
                while (sheets.hasNext()) {
                    System.out.println("Processing new sheet:\n");
                    try (InputStream sheet = sheets.next()) {
                        InputSource sheetSource = new InputSource(sheet);
                        parser.parse(sheetSource);
                    }
                    System.out.println("");
                }
                */
                
                InputSource sheetSource = new InputSource(sheetStream);
                
                DataFormatter dataFormatter = new customDataFormmater();
                Format format = new DecimalFormat("#########.###");

                dataFormatter.setDefaultNumberFormat(format);
                
                ContentHandler handler = new XSSFSheetXMLHandler(styles,data,sheetHandler,dataFormatter,false);
                
                XMLReader sheetParser = SAXHelper.newXMLReader();
                sheetParser.setContentHandler(handler);
                sheetParser.parse(sheetSource);
                
                sheetStream.close();
                
                list = sheetHandler.getRows();
            }
            
            System.out.println("listSize : " + list.size());
                        
            bis.close();
            // file.delete();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("fail to read :", e);
            }
            throw e;
        } finally {
            if (bis != null) {
                bis.close();
                // file.delete();
            }
        }
        return list;
    }

    /**
     * 기존 excelLimitRowRead 대체 메소드 엑셀 확장자 .xls 일 경우, 기존로직(rReadLimitRow) 사용
     * @param file File => 업로드 파일
     * @param cellName String[] => 매칭될 컬럼명
     * @param startSheet int => 사용 x 반드시 첫번째 sheet만 읽음
     * @param startRow int => 시작열 (시작열 이전 데이터 무시)
     * @param endRow int => 확장자 .xls -> 일경우 100000 으로 사용
     * @param startCell int => 사용 x
     * @throws Exception
     * @return List
     * @date   23.08.16
     * @description {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} 오버로딩 처리
     */
    public static List<Map> excelLimitRowReadByHandler(File file, String[] cellName,
            int startSheet, int startRow, int endRow, int startCell)
            throws Exception {
        return excelLimitRowReadByHandler(file, cellName, startRow);
    }

    /**
     *  Excel Handler Reading 템플릿 데이터 리딩 버전
     * @param excelTemplate List => 템플릿 List Data
     * @param file File => 업로드 파일
     * @throws Exception
     * @return List
     * @date   23.08.16
     * @description 
     * 기존 {@link ExcelReader#rReadLimitRow(List excelTemplate,File file)} 대체 메소드
     * <br>template List 컬럼 데이터 처리 후 {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} 호출
     * <br> 확장자 .xls 일 경우, 기존로직(
     * {@link ExcelReader#rReadLimitRow(Workbook workbook,String[] cellName, int maxRow, int startSheet, int startRow,int endRow, int startCell, int mergeGubun)}
     * )사용
     */
    public static List<Map<String, Object>> excelReadHandlerByTemplate(List<Map<String, Object>> excelTemplate, File file) throws Exception {
        List list = null;
        try {
            Map<Integer,String> cellFormat = new HashMap<Integer,String>();
            Map<String,String> defaultValues = new HashMap<String,String>(); // 기본값을 저장할 맵 추가
            int primaryCol = -1;
            for (Map<String, Object> item : excelTemplate) {
                if(String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("null") || String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("")){
                    break;
                }
                int viewSeq = Integer.parseInt(String.valueOf(item.get("FORMAT_VIEW_SEQ"))) - 1;  // cell 검증시 index가 0부터 시작이므로 -1 해서 사용.
                String bindCol = String.valueOf(item.get("FORMAT_BIND_COL"));
                cellFormat.put(viewSeq, bindCol); //viewseq : colNm 형태로 담음.
                
                // FORMAT_DEFAULT_VALUE가 있으면 저장
                if(item.containsKey("FORMAT_DEFAULT_VALUE") && item.get("FORMAT_DEFAULT_VALUE") != null 
                   && !String.valueOf(item.get("FORMAT_DEFAULT_VALUE")).equals("null") 
                   && !String.valueOf(item.get("FORMAT_DEFAULT_VALUE")).equals("")) {
                    defaultValues.put(bindCol, String.valueOf(item.get("FORMAT_DEFAULT_VALUE")));
                }
                
                if(bindCol.equals("ITEM_CD")){
                    primaryCol = viewSeq; // 필수 컬럼 -> 해당 컬럼에 값이 없을경우 루프 종료.
                }
            }
            Set<Integer> keySet = cellFormat.keySet();
            Integer[] keys = new Integer[keySet.size()];
            keySet.toArray(keys);
            
            Arrays.sort(keys);
            String[] collName = new String[keys[keys.length-1]+1];

            for (int i = 0 ; i < keys.length ; i++){
                collName[keys[i]] = cellFormat.get(keys[i]);
            }
            int startRow = Integer.parseInt(String.valueOf(excelTemplate.get(0).get("FORMAT_START_ROW"))) - 1;
            
            list = excelLimitRowReadByHandler(file, collName, startRow);
            
            // 템플릿 기본값 적용 추가
            if(!defaultValues.isEmpty() && list != null) {
                for(Object rowObj : list) {
                    if(rowObj instanceof Map) {
                        Map row = (Map)rowObj;
                        for(Map.Entry<String, String> entry : defaultValues.entrySet()) {
                            String colName = entry.getKey();
                            String defaultValue = entry.getValue();
                            
                            // 값이 비어있거나 null인 경우 기본값 적용
                            if(!row.containsKey(colName) || row.get(colName) == null || 
                               String.valueOf(row.get(colName)).equals("") || 
                               String.valueOf(row.get(colName)).equals("null")) {
                                row.put(colName, defaultValue);
                            }
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("fail to read :", e);
            }
            throw e;
        }
        return list;
    }
//    public static List<Map<String, Object>> excelReadHandlerByTemplate(List<Map<String, Object>> excelTemplate, File file) throws Exception {
//        List list = null;
//        try {
//            Map<Integer,String> cellFormat = new HashMap<Integer,String>();
//            int primaryCol = -1;
//            for (Map<String, Object> item : excelTemplate) {
//                if(String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("null") || String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("")){
//                    break;
//                }
//                int viewSeq = Integer.parseInt(String.valueOf(item.get("FORMAT_VIEW_SEQ"))) - 1;  // cell 검증시 index가 0부터 시작이므로 -1 해서 사용.
//                cellFormat.put(viewSeq, String.valueOf(item.get("FORMAT_BIND_COL"))); //viewseq : colNm 형태로 담음.
//                
//                if(String.valueOf(item.get("FORMAT_BIND_COL")).equals("ITEM_CD")){
//                    primaryCol = viewSeq; // 필수 컬럼 -> 해당 컬럼에 값이 없을경우 루프 종료.
//                }
//            }
//            Set<Integer> keySet = cellFormat.keySet();
//            Integer[] keys = new Integer[keySet.size()];
//            keySet.toArray(keys);
//            
//            Arrays.sort(keys);
//            String[] collName = new String[keys[keys.length-1]+1];
//
//            for (int i = 0 ; i < keys.length ; i++){
//                collName[keys[i]] = cellFormat.get(keys[i]);
//            }
//            int startRow = Integer.parseInt(String.valueOf(excelTemplate.get(0).get("FORMAT_START_ROW"))) - 1;
//            
//            list = excelLimitRowReadByHandler(file, collName, startRow);
//            
//        } catch (Exception e) {
//            if (logger.isErrorEnabled()) {
//                logger.error("fail to read :", e);
//            }
//            throw e;
//        } finally {
//
//        }
//        return list;
//    }
    
    /**
     *  Excel Handler Reading 패스워드 추가 버전
     * @param file File => 업로드 파일
     * @param cellName String[] => 매칭될 컬럼명
     * @param startRow int => 시작열 (시작열 이전 데이터 무시)
     * @param password String => 파일 비밀번호
     * @throws Exception
     * @return List
     * @date   23.08.16
     * @description 
     * 파일 password로 연 후 기본 메소드 ({@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)})와 동일로직
     */
    public static List<Map> excelLimitRowReadByHandlerWithPassword(File file, String[] cellName, int startRow, String password) throws Exception {
        List<Map> list = null;

        BufferedInputStream bis = null;
        try {
            if (!file.exists()) {
                throw new Exception("FIle Not EXIXT...");
            }
            if(password == null || password.equals("")){
                throw new Exception("Password Is Empty");
            }
            
            String filename = file.getPath();
            
            //FSUtil.fileDecrypt(filename, filename);
            
            bis = new BufferedInputStream(new FileInputStream(file));
            
            SheetHandler sheetHandler = new SheetHandler(cellName,startRow); 
            
            String name = file.getName();
            
            if(name.endsWith(".xls")){
                //hssf 기존방식
                Workbook workbook = WorkbookFactory.create(bis, password);
                list = rReadLimitRow(workbook, cellName, 100000, 0, startRow, 1000, 0, 0);
            }
            else{
                POIFSFileSystem poifs = new POIFSFileSystem(bis);
                EncryptionInfo passInfo = new EncryptionInfo(poifs);
                Decryptor decryptor = Decryptor.getInstance(passInfo);
                decryptor.verifyPassword(password);
                
                //SAX 방식 xml package
                OPCPackage pkg = OPCPackage.open(decryptor.getDataStream(poifs));
                //poi excel reader
                XSSFReader xssfReader = new XSSFReader(pkg);
                                
                //get data in excel
                ReadOnlySharedStringsTable data = new ReadOnlySharedStringsTable(pkg);
                
                //get style in poi excel reader
                StylesTable styles = xssfReader.getStylesTable();
                //InputStream st = new FileInputStream(file);
                //get first sheet
                InputStream sheetStream = xssfReader.getSheetsData().next();
                /*
                //get all sheets example
                Iterator<InputStream> sheets = xssfReader.getSheetsData();
                while (sheets.hasNext()) {
                    System.out.println("Processing new sheet:\n");
                    try (InputStream sheet = sheets.next()) {
                        InputSource sheetSource = new InputSource(sheet);
                        parser.parse(sheetSource);
                    }
                    System.out.println("");
                }
                */
                
                InputSource sheetSource = new InputSource(sheetStream);
                
                DataFormatter dataFormatter = new customDataFormmater();
                Format format = new DecimalFormat("#########.###");

                dataFormatter.setDefaultNumberFormat(format);
                
                ContentHandler handler = new XSSFSheetXMLHandler(styles,data,sheetHandler,dataFormatter,false);
                
                XMLReader sheetParser = SAXHelper.newXMLReader();
                sheetParser.setContentHandler(handler);
                sheetParser.parse(sheetSource);
                
                sheetStream.close();
                
                list = sheetHandler.getRows();
            }
            
            System.out.println("listSize : " + list.size());
                        
            bis.close();
            // file.delete();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("fail to read :", e);
            }
        } finally {
            if (bis != null) {
                bis.close();
                // file.delete();
            }
        }
        return list;
    }
    
    /**
     *  Excel Handler Reading 템플릿 데이터 리딩 + 패스워드 추가 버전
     * @param excelTemplate List => 템플릿 List Data
     * @param file File => 업로드 파일
     * @param password String => 파일 비밀번호
     * @throws Exception
     * @return List
     * @date   23.08.16
     * @description 
     * 기존 {@link ExcelReader#excelReadRowByTemplatePassWord(List excelTemplate,File file, String passWord)} 대체 메소드
     * <br>template List 컬럼 데이터 처리 후
     * <br> {@link ExcelReader#excelLimitRowReadByHandlerWithPassword(File file, String[] cellName, int startRow, String password)}호출
     */
    public static List<Map<String, Object>> excelReadHandlerByTemplateWithPassword(List<Map<String, Object>> excelTemplate, File file, String password) throws Exception {
        List list = null;
        try {
            Map<Integer,String> cellFormat = new HashMap<Integer,String>();
            int primaryCol = -1;
            for (Map<String, Object> item : excelTemplate) {
                if(String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("null") || String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("")){
                    break;
                }
                int viewSeq = Integer.parseInt(String.valueOf(item.get("FORMAT_VIEW_SEQ"))) - 1;  // cell 검증시 index가 0부터 시작이므로 -1 해서 사용.
                cellFormat.put(viewSeq, String.valueOf(item.get("FORMAT_BIND_COL"))); //viewseq : colNm 형태로 담음.
                
                if(String.valueOf(item.get("FORMAT_BIND_COL")).equals("ITEM_CD")){
                    primaryCol = viewSeq; // 필수 컬럼 -> 해당 컬럼에 값이 없을경우 루프 종료.
                }
            }
            Set<Integer> keySet = cellFormat.keySet();
            Integer[] keys = new Integer[keySet.size()];
            keySet.toArray(keys);
            
            Arrays.sort(keys);
            String[] collName = new String[keys[keys.length-1]+1];
            List<String> cellArray = new ArrayList<String>();
            for (int i = 0 ; i < keys.length ; i++){
                collName[keys[i]] = cellFormat.get(keys[i]);
            }
            int startRow = Integer.parseInt(String.valueOf(excelTemplate.get(0).get("FORMAT_START_ROW"))) - 1;
            
            list = excelLimitRowReadByHandlerWithPassword(file, collName, startRow, password);
            
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("fail to read :", e);
            }
            throw e;
        } finally {

        }
        return list;
    }
    
    /*
     * 
     * .xls 용 method
     * 
    */

    /**
     * 확장자 .xls read용 private method
     * @param workbook Workbook => POI WorkBook 파일 객체
     * @param cellName String[] => 매칭될 컬럼명
     * @param maxRow int => 마지막 ROW Index
     * @param startSheet int => 시작 Sheet Index
     * @param startRow int => 시작 Row Index
     * @param endRow int => Sheet내 마지막 Row Index
     * @param startCell int => 시작 Column Index
     * @param mergeGubun int => 병합 구분 여부 (0,1,2)
     * @throws Exception 
     * @return List
     * @description
     * 
     * 
    */
    private static List<Map> rReadLimitRow(Workbook workbook,
            String[] cellName, int maxRow, int startSheet, int startRow,
            int endRow, int startCell, int mergeGubun) throws Exception {
        List list = new ArrayList();
        Map rowMap = null;
        int sheets = workbook.getNumberOfSheets();
        int sheetIdx = startSheet;
        
        Sheet sheet = workbook.getSheetAt(sheetIdx);

        int firstRow = sheet.getFirstRowNum() + startRow;
        int lastRow = endRow + startRow;

        int lcnt = 0;

        if (lastRow > maxRow) {
            lcnt = lastRow / maxRow;
        }

        for (int z = 0; z <= lcnt; z++) {
            
            if (z > 0) {
                firstRow = z * maxRow;
            }

            for (int rowIdx = firstRow; rowIdx < lastRow; rowIdx++) {// 첫로우부터 마지막 로우까지 for-loop
                int cIdx = 0;
                rowMap = new LinkedHashMap();

                Row row = sheet.getRow(rowIdx);
                if (row == null) {
                    continue;
                }
                
                int firstCell = startCell;

                int lastCell = cellName.length + startCell;
                
                boolean isClearedRow = true; 
                
                for (int cellIdx = firstCell; cellIdx < lastCell; cellIdx++) { // 첫컬럼부터 마지막 컬럼까지 for-loop
                    Object data = null;

                    Cell cell = row.getCell(cellIdx);
                    if (cell == null) {
                        cell = row.createCell(cellIdx);
                    }
                    int type = cell.getCellType();
                    if (type == 3) {
                        if (mergeGubun == 1) {
                            cell = getRowCellMerge(row, cell, cellIdx);
                        } else if (mergeGubun == 2) {
                            cell = getCellMerge(cell, cellIdx);
                        }
                        type = cell.getCellType();
                    }
                    
                    data = getData3(cell);
                    if(isClearedCell(type,data)==false){
                        isClearedRow = false;
                    }
                    if(cellName[cIdx] != null){
                        rowMap.put(cellName[cIdx], data);
                    }
                    cIdx++;
                }
                if(!isClearedRow){
                    list.add(rowMap);
                }
            }
        }
        return list;
    }

    /**
     * Called by rReadLimitRow
     * @param cell Cell => HssfCell poi CellData
     * @throws Exception
     * @return Object => Data Result
     * @description
     * {@link ExcelReader#rReadLimitRow(Workbook workbook,String[] cellName, int maxRow, int startSheet, int startRow,int endRow, int startCell, int mergeGubun)}
     * 에 의해 호출됨
     * <br>CellType에 따라 formatting 된 결과값 리턴
     * 
    */
    private static Object getData3(Cell cell) throws Exception {
        Object data = null;
        //NumberFormat nf = new DecimalFormat("#########.###");
        Workbook workbook = cell.getSheet().getWorkbook(); 
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        DataFormatter formatter = new DataFormatter();
        if(cell == null){   //특이케이스 : 마지막행 이후 공백이 null 들어오는 템플릿 리턴처리.
            data = "";
            return data;
        }
        switch (cell.getCellTypeEnum()) {
            case FORMULA:   //FORMULA
                switch (cell.getCachedFormulaResultTypeEnum()) {
                    case BOOLEAN:
                        data = cell.getBooleanCellValue();
                        break;
                    case NUMERIC:
                        data = cell.getNumericCellValue();
                        break;
                    case STRING:
                        data = cell.getStringCellValue();
                        break;
                }
                break;
            case NUMERIC:   //NUMERIC
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    data = DateUtil.getDateFormat(cell.getDateCellValue(), "yyyyMMdd");
                } else {
                    data = formatter.formatCellValue(cell);
                }
    
                break;
            case STRING:    //STRING
                data = String.valueOf(cell.getStringCellValue().trim());
                break;
            case BLANK: //BLANK
                data = "";
                break;
            case BOOLEAN:   //BOOLEAN
                data = Boolean.valueOf(cell.getBooleanCellValue());
                break;
            case ERROR: //ERROR
                data = Byte.valueOf(cell.getErrorCellValue());
                break;
            default:
                data = "";
                break;          
        }

        return data;
    }

    /**
     * Called by rReadLimitRow
     * @param type
     * @param data
     * @throws Exception
     * @return boolean
     * @description
     * 
    */
    private static boolean isClearedCell(int type, Object data) throws Exception {
        boolean flag = false;
        switch (type) {
            case 2: //FORMULA
            case 0: //NUMERIC
            case 1: //STRING
            case 3: //BLANK
                if(data==null||data.toString().equals("")){
                    flag = true;
                }
                else{
                    flag = false;
                }
                break;
            case 4: //BOOLEAN
            case 5: //ERROR
                if(data==null){
                    flag = true;
                }
                else{
                    flag = false;
                }
                break;
            default:
                flag = true;
                break;          
        }
        return flag;
    }


    /*
     * 
     * NOT ORGANIZE YET
     * 
    */

    /***
     * 
     * @param row
     * @param cell
     * @param cellIdx
     * @return
     * @throws Exception
    */
    private static Cell getRowCellMerge(Row row, Cell cell, int cellIdx)
            throws Exception {
        Sheet sheet = row.getSheet();
        int rowIdx = row.getRowNum();

        if (rowIdx != sheet.getFirstRowNum()) {
            for (int i = rowIdx; i >= sheet.getFirstRowNum(); i--) {
                if (sheet.getRow(i).getCell(cellIdx).getCellType() != 3) {
                    return sheet.getRow(i).getCell(cellIdx);
                }
            }
        }
        return cell;
    }

    /***
     * 
     * @param cell
     * @param cellIdx
     * @return
     * @throws Exception
    */
    private static Cell getCellMerge(Cell cell, int cellIdx) throws Exception {
        Row row = cell.getRow();
        if (cellIdx != row.getFirstCellNum()) {
            for (int i = cellIdx; i >= row.getFirstCellNum(); i--) {
                if (row.getCell(i).getCellType() != 3) {
                    return row.getCell(i);
                }
            }
        }
        return cell;
    }


	/*
	 * 
	 * DEPRECATED
	 * 
	 */
	
	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelReadHandlerByTemplate(List excelTemplate, File file) } instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     * @description 임가공 주문 엑셀 업로드 
     */
    @Deprecated
    private static List<Map<String, Object>> readLimitRowOm(Workbook workbook, List<Map<String, Object>> excelTemplate) throws Exception {
        Map<Integer, String> templateDict = new HashMap<Integer, String>();
        int primaryCol = 0;
        int[] maxSeq = new int[excelTemplate.size()];//사용 컬럼중 마지막 seq 컬럼을 찾기위한 Integer Array 생성.
        int maxSeq_idx = 0;
        for (Map<String, Object> item : excelTemplate) {
            if(String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("null") || String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("")){
                break;
            }
            int viewSeq = Integer.parseInt(String.valueOf(item.get("FORMAT_VIEW_SEQ"))) - 1;  // cell 검증시 index가 0부터 시작이므로 -1 해서 사용.
            templateDict.put(viewSeq, String.valueOf(item.get("FORMAT_BIND_COL"))); //viewseq : colNm 형태로 담음.
            
            if(String.valueOf(item.get("FORMAT_BIND_COL")).equals("ITEM_CD")){
                primaryCol = viewSeq; // 필수 컬럼 -> 해당 컬럼에 값이 없을경우 루프 종료.
            }
            maxSeq[maxSeq_idx++] = Integer.valueOf(String.valueOf(item.get("FORMAT_VIEW_SEQ")));
        }
        
        int startRow = Integer.parseInt(String.valueOf(excelTemplate.get(0).get("FORMAT_START_ROW"))) - 1;
        Arrays.sort(maxSeq);
        int lastCol = maxSeq[maxSeq.length - 1]; //사용컬럼중 가장 마지막 Seq
        List<Map<String, Object>> parseResult = new ArrayList();
        Map rowMap = null;
        int sheets = workbook.getNumberOfSheets();  // 전체 Sheet 갯수.

        //STEP 1. Sheet 갯수만큼 루핑.
        int sheetIdx = 0; //FIX. 첫번째 Sheet 만 읽도록.
            
        Sheet sheet = workbook.getSheetAt(sheetIdx);

        for (Row row : sheet) {
            if(row.getRowNum() < startRow){//시작행전은 스킵.
                continue;
            }
            Map<String, Object> rMap = new HashMap<String, Object>();   //각 Row를 담을 Map ,, 매 Loop마다 초기화.
            
            Cell pkCell = row.getCell(primaryCol);
            /*
            if(String.valueOf(getData2(pkCell)).equals(null) || (String.valueOf(getData2(pkCell)).trim()).equals("")  || (String.valueOf(getData2(pkCell)).trim()).equals("합계")){
            // if(pkCell == null ||String.valueOf(getData2(pkCell)).equals(null) || (String.valueOf(getData2(pkCell)).trim()).equals("")  || (String.valueOf(getData2(pkCell)).trim()).equals("합계")){
                break;
            }
            */
            
            for (int cellIdx = 0; cellIdx <= lastCol; cellIdx++) {
                Cell cell = row.getCell(cellIdx, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                if(templateDict.containsKey(cellIdx)){
                    if(templateDict.get(cellIdx).equals("ORD_QTY")){// 수량컬럼에 숫자 빼고 제거.
                        String strCellVal = String.valueOf(getData2(cell));
                        cell.setCellValue(strCellVal.replaceAll("[^\\d]", ""));
                    }
                    rMap.put(templateDict.get(cellIdx), getData2(cell));
                    
                }
            }
            parseResult.add(rMap);
            
        }

        return parseResult;
    }   
	
	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelReadHandlerByTemplate(List excelTemplate, File file) } instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     * @description 임가공 주문 엑셀 업로드 
     */
    @Deprecated
    public static List<Map<String, Object>> excelReadRowOM(List<Map<String, Object>> excelTemplate, File file) throws Exception {
        List<Map<String, Object>> list = null;

        BufferedInputStream bis = null;
        int maxRow = 100000;
        try {
            if (!file.exists()) {
                throw new Exception("File is not exists...");
            }
            
            String filename = file.getPath();
            
            bis = new BufferedInputStream(new FileInputStream(file));
            Workbook workbook = WorkbookFactory.create(bis);

            list = readLimitRowOm(workbook, excelTemplate);
            bis.close();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("fail to read :", e);
            }
        } finally {
            if (bis != null) {
                bis.close();
                // file.delete();
            }
        }
        return list;
    }
	
	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelReadHandlerByTemplateWithPassword(List excelTemplate, File file, String password) } instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
    public static List<Map<String, Object>> excelReadRowByTemplatePassWord(List<Map<String, Object>> excelTemplate, File file, String passWord) throws Exception {
        List<Map<String, Object>> list = null;

        BufferedInputStream bis = null;
        int maxRow = 100000;
        try {
            if (!file.exists()) {
                throw new Exception("File is not exists...");
            }
            
            String filename = file.getPath();
            
            //FSUtil.fileDecrypt(filename, filename);
            
            bis = new BufferedInputStream(new FileInputStream(file));
            Workbook workbook = WorkbookFactory.create(bis,passWord);

            list = rReadLimitRow(workbook, excelTemplate);
            bis.close();
            // file.delete();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("fail to read :", e);
            }
        } finally {
            if (bis != null) {
                bis.close();
                // file.delete();
            }
        }
        return list;
    }

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
	@Deprecated
	public static List<Map> excelRead(File file, String[] cellName)
			throws Exception {
		return excelRead(file, cellName, 0, 0, 0, 0);
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet) throws Exception {
		return excelRead(file, cellName, startSheet, 0, 0, 0);
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet, int startRow) throws Exception {
		return excelRead(file, cellName, startSheet, startRow, 0, 0);
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet, int startRow, int startCell) throws Exception {
		return excelRead(file, cellName, startSheet, startRow, startCell, 0);
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     * @return List
     */
    @Deprecated
	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet, int startRow, int startCell, int mergeGubun)
			throws Exception {
		List list = null;

		BufferedInputStream bis = null;
//		int maxRow = 10000;
		int maxRow = 100000;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			bis = new BufferedInputStream(new FileInputStream(file));

			Workbook workbook = WorkbookFactory.create(bis);

			list = rRead(workbook, cellName, maxRow, startSheet, startRow,
					startCell, mergeGubun);

			bis.close();
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				bis.close();
				file.delete();
			}
		}
		return list;
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	private static List<Map> rRead(Workbook workbook, String[] cellName,
			int maxRow, int startSheet, int startRow, int startCell,
			int mergeGubun) throws Exception {
		List list = new ArrayList();
		Map rowMap = null;

		int sheets = workbook.getNumberOfSheets();

		for (int sheetIdx = startSheet; sheetIdx < sheets; sheetIdx++) {
			Sheet sheet = workbook.getSheetAt(sheetIdx);

			int firstRow = sheet.getFirstRowNum() + startRow;
			int lastRow = sheet.getLastRowNum();
			int lcnt = 0;
			int modRow = maxRow;

			if (lastRow > maxRow) {
				lcnt = lastRow / maxRow;
			}

			for (int z = 0; z <= lcnt; z++) {
				if ((z == lcnt) && (firstRow != lastRow) && (z != 0)) {
					modRow = lastRow % (lcnt * z);
				}
				if (z > 0) {
					firstRow = z * maxRow;
					modRow = firstRow + maxRow;
				}

				for (int rowIdx = firstRow; rowIdx < modRow; rowIdx++) {
					int cIdx = 0;
					rowMap = new LinkedHashMap();

					Row row = sheet.getRow(rowIdx);

					if (row == null) {
						continue;
					}

					short firstCell = (short) (sheet.getRow(firstRow)
							.getFirstCellNum() + startCell);
					short lastCell = row.getLastCellNum();

					for (short cellIdx = firstCell; cellIdx < lastCell; cellIdx = (short) (cellIdx + 1)) {
						Object data = null;

						Cell cell = row.getCell(cellIdx);

						if (cell == null) {
							cell = row.createCell(cellIdx);
						}

						int type = cell.getCellType();

						if (type == 3) {
							if (mergeGubun == 1){
								cell = getRowCellMerge(row, cell, cellIdx);
							} else if (mergeGubun == 2) {
								cell = getCellMerge(cell, cellIdx);
							}
							type = cell.getCellType();
						}

						data = getData(type, cell);
						rowMap.put(cellName[cIdx], data);
						cIdx++;
					}

					list.add(rowMap);
				}
			}
		}
		return list;
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	public static List<Map> excelLimitRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell, int endCell)
			throws Exception {
		return excelLimitRead(file, cellName, startSheet, startRow, endRow,
				startCell, endCell, 0);
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	public static List<Map> excelLimitRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell,
			int endCell, int mergeGubun) throws Exception {
		List list = null;

		BufferedInputStream bis = null;
		int maxRow = 10000;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			bis = new BufferedInputStream(new FileInputStream(file));

			Workbook workbook = WorkbookFactory.create(bis);

			list = rReadLimit(workbook, cellName, maxRow, startSheet, startRow,
					endRow, startCell, endCell, mergeGubun);
			// bis.close();
			// file.delete();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				bis.close();
			}
			if (file != null) {
				file.delete();
			}
		}
		return list;
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	private static List<Map> rReadLimit(Workbook workbook, String[] cellName,
			int maxRow, int startSheet, int startRow, int endRow,
			int startCell, int endCell, int mergeGubun) throws Exception {
		List list = new ArrayList();
		Map rowMap = null;
		int sheets = workbook.getNumberOfSheets();

		for (int sheetIdx = startSheet; sheetIdx < sheets; sheetIdx++) {
			Sheet sheet = workbook.getSheetAt(sheetIdx);

			int firstRow = sheet.getFirstRowNum() + startRow;
			int lastRow = endRow + startRow;
			int lcnt = 0;
			// int modRow = maxRow;

			if (lastRow > maxRow) {
				lcnt = lastRow / maxRow;
			}

			for (int z = 0; z <= lcnt; z++) {
				if ((z == lcnt) && (firstRow != lastRow) && (z != 0)) {
					// modRow = lastRow % (lcnt * z);
				}
				if (z > 0) {
					firstRow = z * maxRow;
					// modRow = firstRow + maxRow;
				}

				for (int rowIdx = firstRow; rowIdx < lastRow; rowIdx++) {
					int cIdx = 0;
					rowMap = new LinkedHashMap();

					Row row = sheet.getRow(rowIdx);

					if (row == null) {
						continue;
					}

					int firstCell = sheet.getRow(firstRow).getFirstCellNum()
							+ startCell;

					int lastCell = endCell + startCell;

					for (int cellIdx = firstCell; cellIdx < lastCell; cellIdx++) {
						Object data = null;

						Cell cell = row.getCell(cellIdx);

						if (cell == null) {
							cell = row.createCell(cellIdx);
						}

						int type = cell.getCellType();

						if (type == 3) {
							if (mergeGubun == 1) {
								cell = getRowCellMerge(row, cell, cellIdx);
							} else if (mergeGubun == 2) {
								cell = getCellMerge(cell, cellIdx);
							}
							type = cell.getCellType();
						}

						data = getData(type, cell);
						rowMap.put(cellName[cIdx], data);
						cIdx++;
					}

					list.add(rowMap);
				}
			}
		}

		return list;
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelReadHandlerByTemplate(List excelTemplate, File file)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
	*/
	@Deprecated
	private static List<Map<String, Object>> rReadLimitRow(Workbook workbook, List<Map<String, Object>> excelTemplate) throws Exception {
		Map<Integer, String> templateDict = new HashMap<Integer, String>();
		int primaryCol = 0;
		int[] maxSeq = new int[excelTemplate.size()];//사용 컬럼중 마지막 seq 컬럼을 찾기위한 Integer Array 생성.
		int maxSeq_idx = 0;
		for (Map<String, Object> item : excelTemplate) {
			if(String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("null") || String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("")){
				break;
			}
			int viewSeq = Integer.parseInt(String.valueOf(item.get("FORMAT_VIEW_SEQ"))) - 1;  // cell 검증시 index가 0부터 시작이므로 -1 해서 사용.
			templateDict.put(viewSeq, String.valueOf(item.get("FORMAT_BIND_COL")));	//viewseq : colNm 형태로 담음.
			
			if(String.valueOf(item.get("FORMAT_BIND_COL")).equals("ITEM_CD")){
				primaryCol = viewSeq; // 필수 컬럼 -> 해당 컬럼에 값이 없을경우 루프 종료.
			}
			maxSeq[maxSeq_idx++] = Integer.valueOf(String.valueOf(item.get("FORMAT_VIEW_SEQ")));
		}
		
		int startRow = Integer.parseInt(String.valueOf(excelTemplate.get(0).get("FORMAT_START_ROW"))) - 1;
		Arrays.sort(maxSeq);
		int lastCol = maxSeq[maxSeq.length - 1]; //사용컬럼중 가장 마지막 Seq
		List<Map<String, Object>> parseResult = new ArrayList();
		Map rowMap = null;
		int sheets = workbook.getNumberOfSheets();	// 전체 Sheet 갯수.

		//STEP 1. Sheet 갯수만큼 루핑.
		int sheetIdx = 0; //FIX. 첫번째 Sheet 만 읽도록.			
		Sheet sheet = workbook.getSheetAt(sheetIdx);

		for (int rowIdx = 0; rowIdx < 100000; rowIdx++) {
			if(rowIdx < startRow){//시작행전은 스킵.
				continue;
			}
			
			Row row = sheet.getRow(rowIdx);
			Map<String, Object> rMap = new HashMap<String, Object>();	//각 Row를 담을 Map ,, 매 Loop마다 초기화.
			
			if(row==null){//마지막행에 도달했을 경우 또는 중간에 빈 Row가 있을경우 탐색 중단. row == null
				break;
			}
			boolean isPkCellNull = false;
			Cell pkCell = row.getCell(primaryCol, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
			if(String.valueOf(getData2(pkCell)).equals(null) || (String.valueOf(getData2(pkCell)).trim()).equals("")){
				isPkCellNull = true;
			}
			if((String.valueOf(getData2(pkCell)).trim()).equals("합계")){
				break;
			}
			
			for (int cellIdx = 0; cellIdx <= lastCol; cellIdx++) {
				Cell cell = row.getCell(cellIdx, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
				if(templateDict.containsKey(cellIdx)){
					if(templateDict.get(cellIdx).equals("ORD_QTY")){// 수량컬럼에 숫자 빼고 제거.
						String strCellVal = String.valueOf(getData2(cell));
						cell.setCellType(CellType.STRING);
						cell.setCellValue(strCellVal.replaceAll("[^\\d]", ""));
					}
					if (isPkCellNull){
						if(getData2(cell)!=null && (String)getData2(cell) != ""){
							ServiceUtil.isValidReturnCode("ExcelReader","-1",(rowIdx+"행 : 필수값이 존재하지 않습니다."));
						}
						continue;
					}
					rMap.put(templateDict.get(cellIdx), getData2(cell));
					
				}
			}
			if (isPkCellNull){
				continue;
			}
			parseResult.add(rMap);
		}
		return parseResult;
	}

	/**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
     */
    @Deprecated
	public static List<Map> excelLimitRowRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell)
			throws Exception {
		return excelLimitRowRead(file, cellName, startSheet, startRow, endRow,
				startCell, 0);
	}

	/**
	 * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelLimitRowReadByHandler(File file, String[] cellName, int startRow)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
	 */
    @Deprecated
	public static List<Map> excelLimitRowRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell,int mergeGubun) throws Exception {
		List<Map> list = null;

		BufferedInputStream bis = null;
		int maxRow = 100000;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			
			String filename = file.getPath();
			
			//FSUtil.fileDecrypt(filename, filename);
			
			bis = new BufferedInputStream(new FileInputStream(file));
			Workbook workbook = WorkbookFactory.create(bis);
			list = rReadLimitRow(workbook, cellName, maxRow, startSheet,
					startRow, endRow, startCell, mergeGubun);
			bis.close();
			// file.delete();
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("fail to read :", e);
			}
		} finally {
			if (bis != null) {
				bis.close();
				// file.delete();
			}
		}
		return list;
	}

    /**
     * @deprecated
     * This method is no longer be used.
     * <p> Use {@link ExcelReader#excelReadHandlerByTemplate(List excelTemplate, File file)} instead.
     * @throws Exception
     * @return List
     * @date 23.08.16
    */
    @Deprecated
	public static List<Map<String, Object>> excelReadRowByTemplate(List<Map<String, Object>> excelTemplate, File file) throws Exception {
		List<Map<String, Object>> list = null;

		BufferedInputStream bis = null;
		int maxRow = 100000;
		try {
			if (!file.exists()) {
				throw new Exception("File is not exists...");
			}
			
			String filename = file.getPath();
			
			//FSUtil.fileDecrypt(filename, filename);
			
			bis = new BufferedInputStream(new FileInputStream(file));
			Workbook workbook = WorkbookFactory.create(bis);

			list = rReadLimitRow(workbook, excelTemplate);
			bis.close();
			// file.delete();
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("fail to read :", e);
			}
			throw e;
		} finally {
			if (bis != null) {
				bis.close();
				// file.delete();
			}
		}
		return list;
	}
	
	private static Object getData(int type, Cell cell) throws Exception {
        Object data = null;
        NumberFormat nf = new DecimalFormat("#########.###");
        switch (type) {
            case 2: //FORMULA
                data = String.valueOf(cell.getStringCellValue().trim());
                break;
            case 0: //NUMERIC
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    data = DateUtil.getDateFormat(cell.getDateCellValue(), "yyyy-MM-dd");
                } else {
                    data = nf.format(cell.getNumericCellValue());
                }
    
                break;
            case 1: //STRING
                data = String.valueOf(cell.getStringCellValue().trim());
                break;
            case 3: //BLANK
                data = "";
                break;
            case 4: //BOOLEAN
                data = Boolean.valueOf(cell.getBooleanCellValue());
                break;
            case 5: //ERROR
                data = Byte.valueOf(cell.getErrorCellValue());
                break;
            default:
                data = "";
                break;          
        }

        return data;
    }
    
    private static Object getData2(Cell cell) throws Exception {
        Object data = null;
        NumberFormat nf = new DecimalFormat("#########.###");
        Workbook workbook = cell.getSheet().getWorkbook(); 
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        DataFormatter formatter = new DataFormatter();
        if(cell == null){   //특이케이스 : 마지막행 이후 공백이 null 들어오는 템플릿 리턴처리.
            data = "";
            return data;
        }
        switch (cell.getCellTypeEnum()) {
            case FORMULA:   //FORMULA
                data = formatter.formatCellValue(cell, evaluator);
                break;
            case NUMERIC:   //NUMERIC
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    data = DateUtil.getDateFormat(cell.getDateCellValue(), "yyyyMMdd");
                } else {
                    data = nf.format(cell.getNumericCellValue());
                }
    
                break;
            case STRING:    //STRING
                data = String.valueOf(cell.getStringCellValue().trim());
                break;
            case BLANK: //BLANK
                data = "";
                break;
            case BOOLEAN:   //BOOLEAN
                data = Boolean.valueOf(cell.getBooleanCellValue());
                break;
            case ERROR: //ERROR
                data = Byte.valueOf(cell.getErrorCellValue());
                break;
//          case 9: //강제처리
//              String str = String.valueOf(cell.getStringCellValue().trim());
//              str.replaceAll("[^\\d]", "");
//              data = str;
//              break;
            default:
                data = "";
                break;          
        }

        return data;
    }
}

class SheetHandler implements SheetContentsHandler{
	
	//header를 제외한 데이터부분
	private List rows = new ArrayList();
	//cell 호출시마다 쌓아놓을 1 row List
	private Map row = new LinkedHashMap();
	//Header 정보를 입력
	private String[] header;
	//빈 값을 체크하기 위해 사용할 셀번호
	private int currentCol = -1;
	//현재 읽고 있는 Cell의 Col
	private int currRowNum = 0;
	private int startRow = 0;
	private int headerCount = 0;
	
	
	SheetHandler(String[] cellName, int startRow){
		this.header = cellName;
		this.startRow = startRow;
		int headerCount = 0;
		for (int i = 0 ; i < cellName.length ; i++){
		    if(cellName[i] != null){
		        headerCount++;
		    }
		}
		this.headerCount = headerCount;
		
	}
	
	@Override
	public void cell(String columnName, String value, XSSFComment arg2) {
	    CellReference cellRef = new CellReference(columnName);
		int iCol = cellRef.getCol();
        int emptyCol = iCol - currentCol;
        //공백문자 한개 무시
        if(value.equals(" ")){
            return;
        }
        //읽은 Cell의 번호를 이용하여 빈Cell 자리에 빈값을 강제로 저장시켜줌
        for(int i = 1 ; i < emptyCol ; i++) {
            if(!(currentCol+i < header.length)){
                break;
            }
            if(header[currentCol+i] != null){
                row.put(header[currentCol+i],"");
            }
        }
        if(!(iCol < header.length)){
            currentCol = header.length-1;
            return;
        }
        currentCol = iCol;
        
        if(header[currentCol] != null){
            if(header[currentCol].equals("ORD_QTY")){
            	// 수량컬럼에서 숫자와 소수점(.)만 남기고 제거
                value = value.replaceAll("[^0-9.]", "");
//                value = value.replaceAll("[^\\d]", "");
            }
            row.put(header[currentCol],value);
        }
	}

	@Override
	public void endRow(int rowNum) {
		if(rowNum < startRow) {
			return;
		} 
		else {
			if(row.size() == 0){
			    row.clear();
				return;
			}
            //헤더의 길이가 현재 로우보다 더 길다면 Cell의 뒷자리가 빈값임으로 해당값만큼 공백
            if(row.size() < headerCount) {
                for (int i = currentCol+1; i < header.length; i++) {
                    if(header[i] != null){
                        row.put(header[i],"");
                    }
                }
            }
            rows.add(new LinkedHashMap(row));
        }
       row.clear();
	}

	@Override
	public void headerFooter(String arg0, boolean arg1, String arg2) {
		
	}

	@Override
	public void startRow(int rowNum) {
		this.currentCol = -1;
		this.currRowNum = rowNum;
		row.clear();
	}

	@Override
	public void hyperlinkCell(String arg0, String arg1, String arg2,
			String arg3, XSSFComment arg4) {
		
		
	}
	
	public List getRows(){
		return rows;
	}

}

class customDataFormmater extends DataFormatter{
    private Format format;
    customDataFormmater(){
        super(true);
        super.addFormat("yyyymmdd", new SimpleDateFormat("yyyyMMdd"));
        this.format = new DecimalFormat("#.##########");
    }
    @Override
    public String formatRawCellContents(double value, int formatIndex, String formatString, boolean use1904Windowing) {
        switch(formatIndex){
            case 49 :
                formatString = "@";
                break;
            case 14 :
                formatIndex = 14;
                formatString = "yyyymmdd";
                break;
        }
        //System.out.println(super.formatRawCellContents(value,formatIndex,formatString,use1904Windowing));
        return super.formatRawCellContents(value,formatIndex,formatString,use1904Windowing);
    }
    @Override
    public String formatRawCellContents(double value, int formatIndex, String formatString) {
        String result = "";
        
        switch(formatIndex){
            case 49 :
                formatString = "@";
                break;
            case 14 :
                //result = super.formatRawCellContents(value,formatIndex,formatString);
                formatIndex = 14;
                formatString = "yyyymmdd";
                break;
            case 0 :
                break;
            default :
                switch(formatString){
                    case "yyyy-mm-dd" :
                    case "yyyy\\-mm\\-dd" :
                    case "yyyy/mm/dd;@":
                        formatString = "yyyymmdd";
                        break;
                    default : 
                        return this.format.format(value);
                }
        }
        //System.out.println(super.formatRawCellContents(value,formatIndex,formatString));
        return super.formatRawCellContents(value,formatIndex,formatString);
    }
}
