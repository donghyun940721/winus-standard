package com.m2m.jdfw5x.util;

@SuppressWarnings("PMD")
public final class CharSetUtil {

	public static final String ISO_CHARSET = "8859_1";

	private CharSetUtil() {
	}

	public static String transfer(String str, String fromCharsetName, String toCharsetName) {
		if (str == null) {
			return "";

		} else {
			try {
				return new String(str.getBytes(fromCharsetName), toCharsetName);
			} catch (Exception e) {
				return "";
			}
		}
	}
}
