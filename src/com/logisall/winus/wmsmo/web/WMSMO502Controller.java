package com.logisall.winus.wmsmo.web;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsmo.service.WMSMO502Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSMO502Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMO502Service")
    private WMSMO502Service service;

    @RequestMapping("/WINUS/WMSMO502.action")
    public ModelAndView wmsmo502(Map<String, Object> model){
        return new ModelAndView("winus/wmsmo/WMSMO502");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }

	/*-
	 * Method ID    : list
	 * Method 설명  : 상품별물류기기 조회
	 * 작성자       : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMO502/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명  : 상품별물류기기 조회
	 * 작성자       : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMO502/save.action")
    public ModelAndView save(Map<String, Object> model){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();
        try{
            m = service.save(model);
        }catch(Exception e){
            e.printStackTrace();
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("MSG_ORA", e.getMessage());
        }
        mav.addAllObjects(m);
        return mav;
    }
    
}
