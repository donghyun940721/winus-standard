package com.logisall.winus.wmsmo.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsmo.service.WMSMO080Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSMO080Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMO080Service")
	private WMSMO080Service service;

 
    /**
     * Method ID   : wmsmo080
     * Method 설명    : 매핑조회
     * 작성자               : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSMO080.action")
    public ModelAndView wmsmo080(Map<String, Object> model){
        return new ModelAndView("winus/wmsmo/WMSMO080");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
//
    /**
     * Method ID    : list
     * Method 설명      : 매핑  조회
     * 작성자                 : 기드온
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSMO080/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : detailList
     * Method 설명      : 매핑 상세조회
     * 작성자                 : 기드온
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSMO080/detailList.action")
    public ModelAndView detailList(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.detailList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
}

