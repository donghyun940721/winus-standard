package com.logisall.winus.wmsmo.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsmo.service.WMSMO040Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSMO040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMO040Service")
	private WMSMO040Service service;

    @RequestMapping("/WINUS/WMSMO040.action")
    public ModelAndView wmsmo020(Map<String, Object> model){
        return new ModelAndView("winus/wmsmo/WMSMO040");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }

    @RequestMapping("/WMSMO040/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSMO040/detailList.action")
    public ModelAndView detailList(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.detailList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSMO040/save.action")
    public ModelAndView save(Map<String, Object> model){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();
        try{
            m = service.save(model);
        }catch(Exception e){
            e.printStackTrace();
            m.put("MSG", MessageResolver.getMessage("save.error"));       
        }
        mav.addAllObjects(m);
        return mav;
    }    
    
    
    @RequestMapping("/WMSMO040/checkWeight.action")
    public ModelAndView checkWeight(Map<String, Object> model){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();
        try{
            m = service.checkWeight(model);
            
        }catch(Exception e){
            e.printStackTrace();
            m.put("MSG", MessageResolver.getMessage("save.error"));       
        }
        mav.addAllObjects(m);
        return mav;
    }      
}

    
  
