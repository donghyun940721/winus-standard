package com.logisall.winus.wmsmo.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO040Dao")
public class WMSMO040Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.inRfidList", model);
    }
    
    public GenericResultSet detailList(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.listRfidInOrderItem", model);
    }
    
    public Object save(Map<String, Object> model){
        executeUpdate("wmsop040.PK_WMSOP040.SP_MAPPING_COMPLETE", model);
        return model;
    }
    
    public Object checkWeight(Map<String, Object> model){
        executeUpdate("wmsop040.PK_WMSOP040.SP_CHECK_WEIGHT", model);
        return model;
    }    
}
