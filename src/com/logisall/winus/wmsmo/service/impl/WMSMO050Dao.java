package com.logisall.winus.wmsmo.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO050Dao")
public class WMSMO050Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 매핑 조회
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsrf010.list", model);
    }
    
    /**
     * Method ID    : lisdetailList
     * Method 설명      : 매핑 상세조회
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public GenericResultSet detailList(Map<String, Object> model) {
        return executeQueryPageWq("wmsrf010.detailList", model);
    }  
    
    /**
     * Method ID    : save
     * Method 설명      : 매핑해체
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsrf010.pk_wmsop040.sp_demapping_complete", model);
        return model;
    }
}
