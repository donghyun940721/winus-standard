package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsmo.service.WMSMO100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO100Service")
public class WMSMO100ServiceImpl implements WMSMO100Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMO100Dao")
    private WMSMO100Dao dao;

    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    @Override
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.detailList(model));
        return map;
    }
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());

            for(int i = 0 ; i < totCnt ; i++){
            	String[] ordId       = new String[1];
                String[] ordSeq      = new String[1];
                String[] ordQty      = new String[1];
                String[] workDt      = new String[1];
                String[] ordWeight   = new String[1];
                String[] refSubLotId = new String[1];
                String[] rtiEpcCd    = new String[1];
                String[] lcId        = new String[1];
                
            	ordId[0]        = (String)model.get("ORD_ID"+i);               
            	ordSeq[0]       = (String)model.get("ORD_SEQ"+i);
            	ordQty[0]       = (String)model.get("ORD_QTY"+i);               
            	workDt[0]       = (String)model.get("WORK_DT"+i);
            	ordWeight[0]    = (String)model.get("ORD_WEIGHT"+i);               
            	refSubLotId[0]  = (String)model.get("REF_SUB_LOT_ID"+i);
            	rtiEpcCd[0]     = (String)model.get("RTI_EPC_CD"+i);               
            	//lcId[i]         = (String)model.get("LC_ID"+i);
            	
            	 Map<String, Object> modelIns = new HashMap<String, Object>();
                 modelIns.put("ORD_ID"          , ordId);
                 modelIns.put("ORD_SEQ"         , ordSeq);
                 modelIns.put("ORD_QTY"         , ordQty);
                 modelIns.put("WORK_DT"         , workDt);
                 modelIns.put("ORD_WEIGHT"      , ordWeight);
                 modelIns.put("REF_SUB_LOT_ID"  , refSubLotId);
                 modelIns.put("RTI_EPC_CD"      , rtiEpcCd);
                 //modelIns.put("LC_ID"           , lcId);

                 modelIns.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
                 modelIns.put("WORD_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
                 modelIns.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));

                 modelIns = (Map<String, Object>)dao.save(modelIns);
                 ServiceUtil.isValidReturnCode("WMSMO100", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
           

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
     
    
}
