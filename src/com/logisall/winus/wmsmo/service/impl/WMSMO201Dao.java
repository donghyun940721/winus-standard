package com.logisall.winus.wmsmo.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO201Dao")
public class WMSMO201Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectPool
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmsmo201.selectPool", model);
    }
    
    /**
     * Method ID  : selectCust
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectCust(Map<String, Object> model){
        return executeQueryForList("wmsmo201.selectCust", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 물류용기관리 등록
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model){ //wmsmo201.insert
        executeUpdate("wmsmo201.PK_WMSOP030.SP_RTI_OUT_COMPLETE", model);
        return model;
    }
        
    /**
     * Method ID  : list
     * Method 설명  : 물류용기관리 조회
     * 작성자             : 기드온
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsmo201.list", model);
    }

}
