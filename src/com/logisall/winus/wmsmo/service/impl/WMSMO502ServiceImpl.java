package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsmo.service.WMSMO502Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO502Service")
public class WMSMO502ServiceImpl implements WMSMO502Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSMO502Dao")
    private WMSMO502Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명 : 상품별물류기기 조회
     * 작성자           : kwt
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명 : 상품별물류기기 조회
     * 작성자           : kwt
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelPut = new HashMap<String, Object>();
        
        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());

            for(int i = 0 ; i < totCnt ; i++){
            	String[] lcId       = new String[1];
            	String[] ritemId    = new String[1];
                String[] custLotNo  = new String[1];
                String[] workQty    = new String[1];
                String[] epcCd      = new String[1];
                String[] sapBarcode = new String[1];
                
            	Map<String, Object> modelIns = new HashMap<String, Object>();
            	
            	lcId[0]       = (String)model.get(ConstantIF.SS_SVC_NO);
            	ritemId[0]    = (String)model.get("I_RITEM_ID"+i);               
            	custLotNo[0]  = (String)model.get("I_CUST_LOT_NO"+i);
            	workQty[0]    = (String)model.get("I_WORK_QTY"+i);               
            	epcCd[0]      = (String)model.get("I_EPC_CD"+i);
            	sapBarcode[0] = "";
            	
            	modelIns.put("I_LC_ID"       	, lcId);
            	modelIns.put("I_RITEM_ID"       , ritemId);
                modelIns.put("I_CUST_LOT_NO"    , custLotNo);
                modelIns.put("I_WORK_QTY"       , workQty);
                
                modelIns.put("I_WORD_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("I_USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
                
                if(model.get("I_EPC_CD"+i).toString().length() > 0){
                	//시리얼번호가 있으면
                	modelIns.put("I_EPC_CD"         , epcCd);
                	modelPut = (Map<String, Object>)dao.saveIn(modelIns);
                }else{
                	//시리얼번호가 없으면
                	modelIns.put("I_SAP_BARCODE"         , sapBarcode);
                	modelPut = (Map<String, Object>)dao.save(modelIns);
                }
            }
            
            ServiceUtil.isValidReturnCode("WMSMO502", String.valueOf(modelPut.get("O_MSG_CODE")), (String)modelPut.get("O_MSG_NAME"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
