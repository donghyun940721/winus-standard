package com.logisall.winus.wmsmo.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO502Dao")
public class WMSMO502Dao extends SqlMapAbstractDAO{   
    /**
     * Method ID    : list
     * Method 설명      : 상품별물류기기 목록 조회
     * 작성자                 : kwt
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsmo502.list", model);
    }    
    
    /**
     * Method ID    : saveIn
     * Method 설명      : 상품별물류기기 목록 조회
     * 작성자                 : kwt
     * @param   model
     * @return
     */
    public Object saveIn(Map<String, Object> model){
        executeUpdate("wmsmo502.PK_WMSST070.SP_KIT_COMPLETE_MOBILE_INPLT", model);
        return model;
    }
    
    /**
     * Method ID    : save
     * Method 설명      : 상품별물류기기 목록 조회
     * 작성자                 : kwt
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsmo502.PK_WMSST070.SP_KIT_COMPLETE_MOBILE", model);
        return model;
    }
}
