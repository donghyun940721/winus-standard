package com.logisall.winus.wmsmo.service;

import java.util.Map;


public interface WMSMO202Service {
    public Map<String, Object> selectDataBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    
}
