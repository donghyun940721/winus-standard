package com.logisall.winus.tmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmscm.service.TMSCM010Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSCM010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSCM010Service")
	private TMSCM010Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 거래처 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSCM010.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		// return new ModelAndView("winus/tmscm/TMSCM010Q1",
		// service.selectBox(model)); // 맨 앞에 / 없음에 주의, .vm 없음에 주의
		return new ModelAndView("winus/tmscm/TMSCM010Q1");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 거래처 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSCM010/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : test
	 * Method 설명 : 거래처 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSCM010/selectBox.action")
	public ModelAndView test(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectBox2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get select info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : tmscm010Q3
	 * Method 설명      : 거래처팝업3
	 * 작성자                 : chSong
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/TMSCM010Q3.action")
	public ModelAndView tmscm010Q3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/tmscm/TMSCM010Q3");
	}

}
