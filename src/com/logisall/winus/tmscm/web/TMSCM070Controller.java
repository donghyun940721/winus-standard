package com.logisall.winus.tmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmscm.service.TMSCM070Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSCM070Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSCM070Service")
	private TMSCM070Service service;

	/*-
	 * Method ID   : selectAuthBtn
	 * Method 설명    : 팝업을위한버튼권한조회
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSCM070/selectAuthBtn.action")
	public ModelAndView selectAuthBtn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectAuthBtn(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get auth btn info :", e);
			}
		}
		return mav;
	}
}
