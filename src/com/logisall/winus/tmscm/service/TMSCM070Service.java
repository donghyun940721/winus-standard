package com.logisall.winus.tmscm.service;

import java.util.Map;

public interface TMSCM070Service {
	public Map<String, Object> selectAuthBtn(Map<String, Object> model) throws Exception;
}
