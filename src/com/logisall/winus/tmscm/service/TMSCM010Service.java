package com.logisall.winus.tmscm.service;

import java.util.Map;

public interface TMSCM010Service {

	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception;
	
}
