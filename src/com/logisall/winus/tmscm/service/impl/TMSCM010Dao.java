package com.logisall.winus.tmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSCM010Dao")
public class TMSCM010Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : list
	 * Method 설명 : 거래처 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms030.list", model);
	}

    /**
     * Method ID : selectBox
     * Method 설명 : 거래처구분 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectBox(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectBox
     * Method 설명 : 거래처구분 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectBox2(Map<String, Object> model){
        return executeQueryForList("tmsys030.selectBox", model);
    }
}
