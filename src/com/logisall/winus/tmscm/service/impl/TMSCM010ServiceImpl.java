package com.logisall.winus.tmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.tmscm.service.TMSCM010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("TMSCM010Service")
public class TMSCM010ServiceImpl implements TMSCM010Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSCM010Dao")
    private TMSCM010Dao dao;

    /**
     * Method ID : list
     * Method 설명 : 거래처 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : selectBox
     * Method 설명 : 거래처구분 셀렉트박스
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            model.put("inKey", "CG");
            map.put("ETC", dao.selectBox(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectBox
     * Method 설명 : 거래처구분 셀렉트박스
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        try {
            model.put("inKey", "CG");
            map.put("ETC", dao.selectBox2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}
