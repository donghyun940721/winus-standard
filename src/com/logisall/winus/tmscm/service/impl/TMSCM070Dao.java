package com.logisall.winus.tmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSCM070Dao")
public class TMSCM070Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
    /**
     * Method ID   : selectAuthBtn
     * Method 설명    : 버튼권한조회
     * 작성자               : chSong
     * @param model
     * @return
     */
    public Object selectAuthBtn(Map<String, Object> model){
        return executeQueryForList("user_info.detailRoll", model);
    }
}
