package com.logisall.winus.wmstr.service.impl;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmstr.service.WMSTR010Service;

@Service("WMSTR010Service")
public class WMSTR010ServiceImpl implements WMSTR010Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTR010Dao")
    private WMSTR010Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE2
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        ObjectMapper objectMapper = new ObjectMapper();
        Object header = dao.listE2Header(model);
        Map<String, String> param = objectMapper.convertValue(header, Map.class);
        model.put("LC_NM", param.get("LC_NM"));
        map.put("LIST", dao.listE2(model));
        map.put("HEADER", param);
        return map;
    }

    

    /**
     * 
     * 대체 Method ID	: listE3
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        
        ObjectMapper objectMapper = new ObjectMapper();
        Object header = dao.listE2Header(model);
        Map<String, String> param = objectMapper.convertValue(header, Map.class);
        model.put("LC_NM", param.get("LC_NM"));
        map.put("LIST", dao.listE3(model));
        map.put("HEADER", param);
        return map;
    }
    
    

    /**
     * 
     * 대체 Method ID	: listE4
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE4(model));
        return map;
    }
	

    /**
     * 
     * 대체 Method ID	: listSubE4
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSubE4(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listSubE4(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE0
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE0(Map<String, Object> model) throws Exception {
    	String gbn = dao.chkMapGbn(model).toString();
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(gbn.equals("N")) {
        	map.put("LIST", dao.listE02(model));
        }else if(dao.listE0(model).getList().isEmpty()) {
        	map.put("LIST", dao.listSubE0(model));
        }else {
        	map.put("LIST", dao.listE0(model));
        }
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: getItemList
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> getItemList(Map<String, Object> model) throws Exception {
    	model.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    	String gbn = dao.chkMapGbn(model).toString();
        Map<String, Object> map = new HashMap<String, Object>();
        if(gbn.equals("N")) {
        	map.put("LIST", dao.getItemList2(model));
        }else {
        	map.put("LIST", dao.getItemList(model));
        }
        
        return map;
    }
    
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	
}
