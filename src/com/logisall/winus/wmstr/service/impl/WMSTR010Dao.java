package com.logisall.winus.wmstr.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTR010Dao")
public class WMSTR010Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        //return executeQueryPageWq("wmstr010.list", model);
    	return executeQueryWq("wmstr010.list", model);
    }
    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryWq("wmstr010.listE2", model);
    }
    
    /**
     * Method ID  : listE2Header
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public Object listE2Header(Map<String, Object> model) {
        return executeView("wmstr010.listE2Header", model);
    }
    
    /**
     * Method ID  : listE3
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryWq("wmstr010.listE3", model);
    }

    /**
     * Method ID  : listE4
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listE4(Map<String, Object> model) {
        return executeQueryWq("wmstr010.listE4", model);
    }
    
    /**
     * Method ID  : listSubE4
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listSubE4(Map<String, Object> model) {
        return executeQueryWq("wmstr010.listSubE4", model);
    }
    
    /**
     * Method ID  : listE0
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listE0(Map<String, Object> model) {
        return executeQueryWq("wmstr010.listE0", model);
    }
    
    /**
     * Method ID  : listE02
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listE02(Map<String, Object> model) {
        return executeQueryWq("wmstr010.listE02", model);
    }
    
    
    /**
     * Method ID  : chkMapGbn
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public Object chkMapGbn(Map<String, Object> model) {
        return executeView("wmstr010.chkMapGbn", model);
    }
    
    /**
     * Method ID  : listSubE0
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listSubE0(Map<String, Object> model) {
        return executeQueryWq("wmstr010.listSubE0", model);
    }
    
    /**
     * Method ID  : getItemList
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet getItemList(Map<String, Object> model) {
        return executeQueryWq("wmstr010.getItemList", model);
    }
    
    /**
     * Method ID  : getItemList2
     * Method 설명   : s
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet getItemList2(Map<String, Object> model) {
        return executeQueryWq("wmstr010.getItemList2", model);
    }
    
    
}


