package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST401Service {
    public Map<String, Object> wmsst401list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
