package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST290Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> sublist(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> dlt(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}

