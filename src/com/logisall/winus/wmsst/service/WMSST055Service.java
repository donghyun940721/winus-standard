package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST055Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list057(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> wmsst058list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> wmsst059list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel_locSum(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
