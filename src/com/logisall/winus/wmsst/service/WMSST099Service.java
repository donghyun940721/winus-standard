package com.logisall.winus.wmsst.service;

import java.util.List;
import java.util.Map;


public interface WMSST099Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listTotal(Map<String, Object> model) throws Exception;

}
