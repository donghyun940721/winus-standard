package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST160Dao")
public class WMSST160Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst160.list", model);
    }
    

    
    /**
     * Method ID : selectCYCL01
     * Method 설명 : 순환재고조사방식 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectCYCL01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectCYCL01
     * Method 설명 : 순환재고조사ID 셀렉트 박스
     * 작성자 : dongyeob
     * @param model
     * @return
     */
    public Object selectSBCYCLSTOCKID(Map<String, Object> model){
        return executeQueryForList("wmsst160.selectSBCYCLSTOCKID", model);
    }
    
    /**
     * Method ID    : update
     * Method 설명      : 순환재고조사 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsst160.update", model);
    }  
    
    public Object orderInsert(Map<String, Object> model){
        executeUpdate("wmsst160.PK_WMSST050.SP_CYCLE_STOCK_WORK", model);
        return model;
    }
    
    
    public Object runSpRotationalStockTakingJob(Map<String, Object> model){
        executeUpdate("pk_wmsjb000.sp_rotational_stock_taking", model);
        return model;
    }
    
    public Object inExcelUploadTemplate(Map<String, Object> model){
        return executeUpdate("wmsst160.PK_WMSST120.SP_CYCL_STOCK_REG",model);
    }
    
    /**
     * Method ID : saveCyclStep
     * Method 설명 : 재고조사 일괄처리 데이터 추출
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object saveCyclStep(Map<String, Object> model) {
        return executeQueryPageWq("wmsst160.saveCyclStep", model);
    }   
    

    /**
     * Method ID : orderInsertCallback
     * Method 설명 : 재고조사 일괄처리 CALLBACK
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object orderInsertCallback(Map<String, Object> model){
        executeUpdate("wmsst160.PK_WMSST050.SP_CYCLE_STOCK_WORK_CALLBACK", model);
        return model;
    }

    /**
     * Method ID    : update
     * Method 설명      : 순환재고조사 삭제
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
	public Object delete1(Map<String, Object> model) {
		return executeUpdate("wmsst160.delete1", model);
		
	}


	/**
     * Method ID    : update
     * Method 설명      : 순환재고조사 삭제
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
	public Object delete2(Map<String, Object> model) {
		return executeUpdate("wmsst160.delete2", model);
	}



	public Object cyclStockDelete(Map<String, Object> model) {
		executeUpdate("wmsst160.sp_cycl_stock_delete", model);
		return model;
	}
}
