package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST400Dao")
public class WMSST400Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : wmsst400list
     * Method 설명 : 
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet wmsst400list(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst400.list", model);
    }   
    
    /**
     * Method ID : wmsst400list2
     * Method 설명 : 랙보충 ( DPS ) 
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet wmsst400list2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst400.list2", model);
    }   
    
    /**
     * Method ID : listByOrder
     * Method 설명 : 랙보충 ( 주문별 ) 
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet listByOrder(Map<String, Object> model) {
        return executeQueryWq("wmsst400.listByOrder", model);
    }   
    
    /**
     * Method ID : historyList
     * Method 설명 : 랙보충 히스토리 리스트
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet historyList(Map<String, Object> model) {
        return executeQueryWq("wmsst400.historyList", model);
    }
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID   		 : moveStockItem
     * Method 설명      	 : 랙보충
     * 작성자                 : KSJ
     * @param   model
     * @return
     */
    public Object moveStockItem(Map<String, Object> model){
        executeUpdate("wmsst040.pk_wmsst040.pc_sav_movestock_item_mobile", model);
        return model;
    }
    
    /**
     * Method ID	: selectHistoryInsert
     * Method 설명	: 랙보충전 oz 파라미터 저장
     * 작성자			: schan
     * @param model
     * @return
     */
    public Object selectHistoryInsert(Map<String, Object> model){
        executeUpdate("wmsst040.selectHistoryInsert", model);
        return model;
    }
}