package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST603Dao")
public class WMSST603Dao extends SqlMapAbstractDAO{
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst603.list", model);
    }
    
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst603.save", model);
        return model;
    }
}
