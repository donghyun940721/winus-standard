package com.logisall.winus.wmsst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST019Dao")
public class WMSST019Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list 
     * Method 설명  : EBIZ 주문관리 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst019.list", model);
    }   
    

}


