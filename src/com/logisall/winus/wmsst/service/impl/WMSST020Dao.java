package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST020Dao")
public class WMSST020Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : listBlNo
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listBlNo_con(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst020.listBlNo_con", model);
    }

    public GenericResultSet listBlNo(Map<String, Object> model) {
    	String str = (String) model.get("vrSrchBox");
    	
    	if(str != null && !str.equals("")){
    		return executeQueryPageWq("wmsst020.listBlNoBiz", model); // 비즈컨설팅 box 조회
    	}else{
    		return executeQueryPageWq("wmsst020.listBlNo", model);
    	}
        
    }
    
    /**
     * Method ID : listBlNo_E2
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listBlNo_E2(Map<String, Object> model) {
    	String str = (String) model.get("vrSrchBox");
    	
    	if(str != null && !str.equals("")){
    		return executeQueryPageWq("wmsst020.listBlNoBiz", model); // 비즈컨설팅 box 조회
    	}else{
    		return executeQueryPageWq("wmsst020.listBlNo_E2", model);
    	}
        
    }
    
    public GenericResultSet listBlNo_E2_con(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst020.listBlNo_E2_con", model);
    }
    /**
     * Method ID : listBlNoVX
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet listBlNoVX(Map<String, Object> model) {
            return executeQueryPageWq("wmsst020.listBlNoVX", model);
    }
    
    
    /**
	  * Method ID : listBarcode
	  * Method 설명 : 재고 입출고내역 조회 (바코드)
	  * 작성자 : 기드온
	  * @param model
	  * @return
	  */
	 public GenericResultSet listBarcode(Map<String, Object> model) {
	     return executeQueryPageWq("wmsst020.listBarcode", model);
	 }   

    /**
     * Method ID  : save
     * Method 설명  : 재고 입출고내역 작업일자, 작업id 수정 프로시저
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object save(Map<String, Object> model){
        return  executeUpdate("wmsst020.pk_wmsop001.sp_set_workdate", model);
    }
    
    /**
     * Method ID   		: listSummaryCount
     * Method 설명      : 재고입출고내역 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listSummaryCount(Map<String, Object> model) {
		return executeView("wmsst020.listSummaryCount", model);
	}
	
	/**
     * Method ID   		: listSummaryCount_E2
     * Method 설명      : 재고입출고내역(비고표시) COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listSummaryCount_E2(Map<String, Object> model) {
		return executeView("wmsst020.listSummaryCount_E2", model);
	}
	
	/**
	 * Method ID   	: listSummaryCountVX
	 * Method 설명      : 재고입출고내역 COUNT SUMMARY
	 * 작성자           : sing09
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	public Object listSummaryCountVX(Map<String, Object> model) {
	    return executeView("wmsst020.listSummaryCountVX", model);
	}
}
