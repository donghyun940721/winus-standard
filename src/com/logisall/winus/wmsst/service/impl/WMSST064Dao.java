package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST064Dao")
public class WMSST064Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 :  YOUM 거래처별 일보 조회
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmsst064.list", model);
    }   
    
    /**
     * Method ID : list2
     * Method 설명 :  RIGID 거래처별 일보 조회
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
        return executeQueryWq("wmsst064.list2", model);
    }
    /**
     * Method ID : list3
     * Method 설명 : YOUM 거래처별 통합 일보 조회
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public GenericResultSet list3(Map<String, Object> model) {
        return executeQueryWq("wmsst064.list3", model);
    }
    
    /**
     * Method ID : list4
     * Method 설명 :  RIGID 거래처별 통합 일보 조회
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public GenericResultSet list4(Map<String, Object> model) {
        return executeQueryWq("wmsst064.list4", model);
    }   
    

    

    /**
     * Method ID : ordComplete
     * Method 설명 : 검수내역 입고확정
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public Object ordComplete(Map<String, Object> model) {
    	executeUpdate("wmsst064.pk_wmsop021.sp_oms_op_mobile_in_complete", model);
    	return model;
    }
    
    

}

