package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST076Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSST076Service")
public class WMSST076ServiceImpl implements WMSST076Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSST076Dao")
    private WMSST076Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 로케이션별재고 목록 조회
     * 작성자                      : dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE01(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE01(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 로케이션별재고 목록 조회
     * 작성자                      : dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE02(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE02(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 창고별재고 목록 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE01(model));
        return map;
    }  
    
    /**
     * 
     * 대체 Method ID	: save
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
        	 Map<String, Object> modelIns = new HashMap<String, Object>();        	 
        	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
        
        	if(tmpCnt > 0)
        	{
        		String[] custId 	= new String[tmpCnt];
        		String[] rItemId 	= new String[tmpCnt];
        		String[] uomId 		= new String[tmpCnt];
        		String[] subLotId 	= new String[tmpCnt];
        		
        		String[] locId 		= new String[tmpCnt];
        		String[] custLotNo 	= new String[tmpCnt];
        		String[] workQty 	= new String[tmpCnt];
        		String[] oldLotType = new String[tmpCnt];
        		String[] oldOwnerCd = new String[tmpCnt];
        		
        		String[] newLotType 		= new String[tmpCnt];
        		String[] newOwnerCd 		= new String[tmpCnt];
        		String[] changeMemo 		= new String[tmpCnt];
        		String[] itemBestDateEnd 	= new String[tmpCnt];
        		String[] makeDt				= new String[tmpCnt];        		
        	        	
        		// Array Data
	            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	            	custId[i] 		= (String)model.get("CUST_ID" + i);
	            	rItemId[i] 		= (String)model.get("RITEM_ID" + i);
	            	uomId[i] 		= (String)model.get("UOM_ID" + i);
	            	subLotId[i]		= (String)model.get("SUB_LOT_ID" + i);	            	

	            	locId[i]		= (String)model.get("LOC_ID" + i);
	            	custLotNo[i]	= (String)model.get("CUST_LOT_NO" + i);
	            	workQty[i]		= (String)model.get("WORK_QTY" + i);
	            	oldLotType[i] 	= (String)model.get("OLD_LOT_TYPE" + i);
	            	oldOwnerCd[i] 	= (String)model.get("OLD_OWNER_CD" + i);		
	            	
	            	newLotType[i] 		= (String)model.get("SUB_LOT_TYPE" + i);
	            	newOwnerCd[i] 		= (String)model.get("NEW_OWNER_CD" + i);
	            	changeMemo[i] 		= (String)model.get("CHANGE_MEMO"	+ i);
	            	itemBestDateEnd[i] 	= (String)model.get("ITEM_BEST_DATE_END"	+ i);
	            	makeDt[i] 			= (String)model.get("MAKE_DT"	+ i);
	            }
	            
	            modelIns.put("selectIds" 		, model.get("selectIds")); 
	            modelIns.put("I_LC_ID"			, model.get("LC_ID"));
	            modelIns.put("I_CUST_ID"		, custId);
	            modelIns.put("I_RITEM_ID"		, rItemId);
	            modelIns.put("I_UOM_ID"			, uomId);
	            modelIns.put("I_SUB_LOT_ID"		, subLotId);				
				
	            modelIns.put("I_LOC_ID"			, locId);
	            modelIns.put("I_CUST_LOT_NO"	, custLotNo);
	            modelIns.put("I_WORK_QTY"		, workQty);
	            modelIns.put("I_OLD_LOT_TYPE"	, oldLotType);
	            modelIns.put("I_OLD_OWNER_CD"	, oldOwnerCd);								
				
	            modelIns.put("I_NEW_LOT_TYPE"		, newLotType);
	            modelIns.put("I_NEW_OWNER_CD"		, newOwnerCd);
	            modelIns.put("I_CHANGE_MEMO"		, changeMemo);
	            modelIns.put("I_ITEM_BEST_DATE_END"	, itemBestDateEnd);          
	            modelIns.put("I_MAKE_DT"			, makeDt);          

				modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
	        	modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
	        	
	        	// modelIns = (Map<String, Object>)dao.update(modelIns);
	        	
	        	Object temp = dao.update(modelIns);
	        	ServiceUtil.isValidReturnCode("WMSST076", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
        	}            
        	
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
          } catch (BizException be) {
          	  if (log.isErrorEnabled()) {
		  	  	log.error("Fail to get result :", be);
		  	  } 
          	  m.put("errCnt", 1);
          	  m.put("MSG", be.getMessage() );         
                
          } catch(Exception e){
              throw e;
          }
          return m;
	    }
    /**
     * 
     * 대체 Method ID : updateTemplate
     * 대체 Method 설명 : 
     * 작성자          : 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateTemplate(Map<String, Object> model) throws Exception {
        Gson gson         = new Gson();
        String jsonString = gson.toJson(model);
        String sendData   = new StringBuffer().append(jsonString).toString();
        
        JsonParser Parser   = new JsonParser();
        JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
        JsonArray listBody  = (JsonArray) jsonObj.get("LIST");
        
        int listBodyCnt   = listBody.size();
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
            if(listBodyCnt > 0){
                //"LOC_CD", "RITEM_CD", "ORD_ID", "ORD_SEQ", "WORK_QTY", "ITEM_BEST_DATE_END", "MAKE_DT", "CUST_LOT_NO", "CHANGE_MEMO"
                String[] CUST_ID = new String[listBodyCnt];
                String[] LOC_CD = new String[listBodyCnt];
                String[] RITEM_CD = new String[listBodyCnt];
                String[] ITEM_NM = new String[listBodyCnt];
                String[] ORD_ID = new String[listBodyCnt];
                String[] ORD_SEQ = new String[listBodyCnt];
                String[] WORK_QTY = new String[listBodyCnt];
                String[] ITEM_BEST_DATE_END = new String[listBodyCnt];
                String[] MAKE_DT = new String[listBodyCnt];
                String[] CUST_LOT_NO = new String[listBodyCnt];
                String[] CHANGE_MEMO = new String[listBodyCnt];
                
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                    JsonObject object = (JsonObject) listBody.get(i);
                    
                    CUST_ID[i] = (String)model.get("vrSrchCustId");
                    LOC_CD[i] = (String)object.get("LOC_CD").toString().replaceAll("\"", "");
                    RITEM_CD[i] = (String)object.get("RITEM_CD").toString().replaceAll("\"", "");
                    ITEM_NM[i] = (String)object.get("ITEM_NM").toString().replaceAll("\"", "");
                    ORD_ID[i] = (String)object.get("ORD_ID").toString().replaceAll("\"", "");
                    ORD_SEQ[i] = (String)object.get("ORD_SEQ").toString().replaceAll("\"", "");
                    WORK_QTY[i] = (String)object.get("WORK_QTY").toString().replaceAll("\"", "");
                    ITEM_BEST_DATE_END[i] = (String)object.get("ITEM_BEST_DATE_END").toString().replaceAll("\"", "");
                    MAKE_DT[i] = (String)object.get("MAKE_DT").toString().replaceAll("\"", "");
                    CUST_LOT_NO[i] = (String)object.get("CUST_LOT_NO").toString().replaceAll("\"", "");
                    CHANGE_MEMO[i] = (String)object.get("CHANGE_MEMO").toString().replaceAll("\"", "");

                }
                
                modelIns.put("I_LC_ID"          , model.get("SS_SVC_NO"));
                modelIns.put("I_CUST_ID"        , CUST_ID);
                modelIns.put("I_LOC_CD"        , LOC_CD);
                modelIns.put("I_RITEM_CD"        , RITEM_CD);
                modelIns.put("I_ITEM_NM"        , ITEM_NM);
                modelIns.put("I_ORD_ID"        , ORD_ID);
                
                modelIns.put("I_ORD_SEQ"        , ORD_SEQ);
                modelIns.put("I_WORK_QTY"        , WORK_QTY);
                modelIns.put("I_ITEM_BEST_DATE_END"        , ITEM_BEST_DATE_END);
                modelIns.put("I_MAKE_DT"        , MAKE_DT);
                modelIns.put("I_CUST_LOT_NO"        , CUST_LOT_NO);
                
                modelIns.put("I_CHANGE_MEMO"        , CHANGE_MEMO);
                
                modelIns.put("I_WORK_IP"        , model.get("SS_CLIENT_IP"));  
                modelIns.put("I_USER_NO"        , model.get("SS_USER_NO"));
                
                modelIns = (Map<String, Object>)dao.updateTemplate(modelIns);
                ServiceUtil.isValidReturnCode("WMSST076", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
        
        }
        catch(Exception e){
            //throw e;
            throw new Exception( e.getMessage());
        }
        return m;
    }
}


