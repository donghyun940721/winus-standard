package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST060Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST060Service")
public class WMSST060ServiceImpl extends AbstractServiceImpl implements WMSST060Service {
    
    @Resource(name = "WMSST060Dao")
    private WMSST060Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고실사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : detail
     * Method 설명 : 재고실사 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.detail(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : listExcel
     * Method 설명 : 재고실사 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 재고실사 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
     
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] subLotId = new String[iuCnt]; 
            String[] ritemId = new String[iuCnt]; 
            String[] locId = new String[iuCnt]; 
            String[] uomId = new String[iuCnt]; 
            String[] stockQty = new String[iuCnt]; 
            
            String[] custLotNo = new String[iuCnt]; 
            
            for(int i = 0 ; i < iuCnt; i ++){
                subLotId[i]      = (String)model.get("subLotId"+i);
                ritemId[i]       = (String)model.get("ritemId"+i);
                locId[i]         = (String)model.get("locId"+i);
                uomId[i]         = (String)model.get("uomId"+i);
                stockQty[i]      = (String)model.get("stockQty"+i);
                custLotNo[i]     = (String)model.get("custLotNo"+i);
            }

            if(iuCnt > 0){
               Map<String, Object> modelSP = new HashMap<String, Object>();
         
               modelSP.put("I_SUB_LOT_ID", subLotId);
               modelSP.put("I_RITEM_ID", ritemId);
               modelSP.put("I_LOC_ID", locId);
               modelSP.put("I_UOM_ID", uomId);
               modelSP.put("I_STOCK_QTY", stockQty);
               
               modelSP.put("I_CUST_LOT_NO", custLotNo);

               modelSP.put("I_ORD_SUBTYPE", "40");
               modelSP.put("I_LC_ID", model.get("SS_SVC_NO"));
               modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
               modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
                   
               modelSP = (Map<String, Object>)dao.save(modelSP);
               ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
            }
            m.put("MSG", MessageResolver.getMessage("move.success"));
            m.put("errCnt", 0);
                   
        } catch(BizException be) {
           m.put("errCnt", 1);
           m.put("MSG", MessageResolver.getMessage("move.error"));               
                        
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
