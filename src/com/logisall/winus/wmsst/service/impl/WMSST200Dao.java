package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST200Dao")
public class WMSST200Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 현재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst200.list", model);
    }
    
    /**
     * Method ID : mappingSelectYn
     * Method 설명 : 매핑yn
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object mappingSelectYn(Map<String, Object> model){
        return executeQueryForList("wmsst200.mappingSelectYn", model);
    }
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : sublist
     * Method 설명 : 현재고 서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model){
        return executeQueryPageWq("wmsst200.sublist", model);
    }
    
    
    /**
     * Method ID : delete
     * Method 설명 : 현재고 비고 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsst200.update", model);
    }
    
    /**
     * Method ID  : save
     * Method 설명  : 현재고 PLT 수량 수정
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object save(Map<String, Object> model){
        return  executeUpdate("wmsst200.pk_wmsst200.sp_save_plt_qty", model);
    }
    
    /**
     * Method ID : poplist
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet poplist(Map<String, Object> model) {
        return executeQueryPageWq("wmsst200.searchSet", model);
    }   

    
    /**
     * Method ID : popdetail
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet popdetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst200.subSearch", model);
    }   
}
