package com.logisall.winus.wmsst.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsst.service.WMSST400Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST400Service")
public class WMSST400ServiceImpl extends AbstractServiceImpl implements WMSST400Service {
    
    @Resource(name = "WMSST400Dao")
    private WMSST400Dao dao;
    
    @Resource(name = "WMSST010Dao")
    private WMSST010Dao st010dao;

    /**
     * Method ID : wmsst400list
     * Method 설명 : 재고실사(확정) 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> wmsst400list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.wmsst400list(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID : wmsst400list2
     * Method 설명 : 랙보충 ( DPS )
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> wmsst400list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            System.out.println(model);
            map.put("LIST", dao.wmsst400list2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : listByOrder
     * Method 설명 : 랙보충 (주문별)
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listByOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            int selectIds = Integer.parseInt(model.get("selectIds").toString());
            
            List<Map<String, String>> vrOrdList = new ArrayList();            
            for(int i = 0 ; i < selectIds ; i++){
                Map<String, String> ordInfo = new HashMap<String, String>(); 
                ordInfo.put("vrOrdId", (String)model.get("vrOrdId"+i));
                ordInfo.put("vrOrdSeq", (String)model.get("vrOrdSeq"+i));
                vrOrdList.add(ordInfo);
            }
            model.put("vrOrdList", vrOrdList);  
            
            map.put("LIST", dao.listByOrder(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    /**
     * Method ID : historyList
     * Method 설명 : 랙보충 히스토리 리스트
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> historyList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            System.out.println(model);
            map.put("LIST", dao.historyList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : listExcel
     * Method 설명 : 입별재고 엑셀다운
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.wmsst400list(model));
        
        return map;
    }    
   
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            map.put("ZONEGUBUN", st010dao.selecZone(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     *  Method ID 		 : moveStockItem 
     *  Method 설명  	 : 랙보충 프로시저
     *  작성자            	 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> moveStockItem(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());

            String[] epcCd 		= new String[tmpCnt];
            String[] lcId 			= new String[tmpCnt];
            String[] transCustId 	= new String[tmpCnt];
            String[] ordId 			= new String[tmpCnt];
            String[] ordSeq 		= new String[tmpCnt];
            String[] workQty 		= new String[tmpCnt];
            String[] workWgt 		= new String[tmpCnt];
            String[] refSubLotId 	= new String[tmpCnt];
            String[] sapBarcode	= new String[tmpCnt];
            String[] fromLocCd 	= new String[tmpCnt];
            String[] toLocCd 		= new String[tmpCnt];
            String[] custLotNo 	= new String[tmpCnt];

            // I_ 로 시작하는 변수는 안담김
            for (int i = 0; i < tmpCnt; i++) {
            	epcCd[i] 			= (String)model.get("I_EPC_CD" + i);
            	transCustId[i] 		= (String)model.get("I_TRANS_CUST_ID" + i);
            	ordId[i] 			= (String)model.get("I_ORD_ID" + i);
            	ordSeq[i] 			= (String)model.get("I_ORD_SEQ" + i);
                workWgt[i] 			= (String)model.get("I_WORK_WGT" + i);
                sapBarcode[i]		= "14"; // 고정값
                custLotNo[i] 		= (String)model.get("I_CUST_LOT_NO" + i);
                
                refSubLotId[i] 		= (String)model.get("SUB_LOT_ID" + i);
                lcId[i] 			= (String)model.get("LC_ID" + i);
                fromLocCd[i] 		= (String)model.get("LOC_CD" + i);
                toLocCd[i] 			= (String)model.get("FIX_LOC_CD" + i);
                workQty[i] 			= (String)model.get("STOCK_QTY" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("I_EPC_CD", epcCd);
            modelIns.put("I_LC_ID", lcId);
            modelIns.put("I_TRANS_CUST_ID", transCustId);
            modelIns.put("I_ORD_ID", ordId);
            modelIns.put("I_ORD_SEQ", ordSeq);
            modelIns.put("I_WORK_QTY", workQty);
            modelIns.put("I_WORK_WGT", workWgt);
            modelIns.put("I_REF_SUB_LOT_ID", refSubLotId);
            modelIns.put("I_SAP_BARCODE", sapBarcode);
            modelIns.put("I_FROM_LOC_CD", fromLocCd);
            modelIns.put("I_TO_LOC_CD", toLocCd);
            modelIns.put("I_CUST_LOT_NO", custLotNo);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.moveStockItem(modelIns);

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            m.put("MSG_ORA", "");
        }catch(Exception e){
//        	if (log.isErrorEnabled()) {
//				log.error("Fail to get result :", e);
//			} 
//            m.put("errCnt", 1);
//            m.put("MSG", MessageResolver.getMessage("save.error") );
            throw e;
        }
        return m;
    }
    
    /**
     *  Method ID	: selectHistoryInsert 
     *  Method 설명 	: 랙보충전 oz 파라미터 저장
     *  작성자			: schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> selectHistoryInsert(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            String uuid = UUID.randomUUID().toString();
            
            for (int i = 0; i < tmpCnt; i++) {
            	Map<String, Object> obj = new HashMap<String, Object>();
            	obj.put("SUB_LOT_ID"	, (String)model.get("SUB_LOT_ID" + i));
            	obj.put("LOC_CD"		, (String)model.get("LOC_CD" + i));
            	obj.put("FIX_LOC_CD"	, (String)model.get("FIX_LOC_CD" + i));
            	obj.put("STOCK_QTY"		, (String)model.get("STOCK_QTY" + i));
            	obj.put("ITEM_CD"		, (String)model.get("ITEM_CD" + i));
            	obj.put("ITEM_NM"		, (String)model.get("ITEM_NM" + i));
            	obj.put("MAKE_QTY"		, (String)model.get("MAKE_QTY" + i));
            	obj.put("ITEM_BAR_CD"	, (String)model.get("ITEM_BAR_CD" + i));
                obj.put("ITEM_DATE_END"	, (String)model.get("ITEM_DATE_END" + i));
                obj.put("FIX_LOC_NM"	, (String)model.get("FIX_LOC_NM" + i));
                obj.put("CUST_LOT_NO"    , (String)model.get("CUST_LOT_NO" + i));
            	obj.put("BOX_BAR_CD"	, (String)model.get("BOX_BAR_CD" + i));
            	obj.put("LC_ID"			, (String)model.get("LC_ID" + i));
            	obj.put("CUST_ID"       , (String)model.get("CUST_ID" + i));
            	obj.put("UUID_SEQ"		, (i+1));
            	obj.put("uuid"			, uuid);
            	obj.put("SYS_USER_CD"	, (String)model.get(ConstantIF.SS_USER_NO));
            	obj.put("WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
            	
            	obj = (Map<String, Object>)dao.selectHistoryInsert(obj);
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            m.put("UUID", uuid);
            m.put("MSG_ORA", "");
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
            throw e;
        }
        return m;
    }
}
