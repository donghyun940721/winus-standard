package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST350Dao")
public class WMSST350Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list_T1
     * Method 설명 : 임가공(GS 출고) 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet list_T1(Map<String, Object> model) {
        return executeQueryPageWq("wmsst350.list_T1", model);
    }   
    
    /**
     * Method ID : listExcel_T1
     * Method 설명 : 임가공(GS 출고 엑셀 리포팅)
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listExcel_T1(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst350.listExcel_T1", model);
    }   
    
    /**
     * Method ID : list_T2
     * Method 설명 : 임가공(GS 전체) 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet list_T2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst350.list_T2", model);
    }   
    
    /**
     * Method ID : listSub_T1
     * Method 설명 : 임가공(GS) 구성품 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listSub_T1(Map<String, Object> model) {
        return executeQueryPageWq("wmsst350.listSub_T1", model);
    }   
    
    /**
     * Method ID : listSub_T2
     * Method 설명 : 임가공(GS 출고) 구성품 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listSub_T2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst350.listSub_T2", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    
    
    /**
     * Method ID : listZone_T1
     * Method 설명 : 임가공조회(출고)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listZone_T1(Map<String, Object> model) {
        return executeQueryPageWq("wmsst350.listZone_T1", model);
    }  
    
    /**
     * Method ID : listZone_T2
     * Method 설명 : 임가공조회(전체)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listZone_T2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst350.listZone_T2", model);
    }   
    
    /**
     * Method ID : listSubZone_T1
     * Method 설명 : 임가공구성품 조회(출고)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listSubZone_T1(Map<String, Object> model) {
        return executeQueryPageWq("wmsst350.listSubZone_T1", model);
    }   
    
    /**
     * Method ID : listSubZone_T2
     * Method 설명 : 임가공 구성품 조회(전체)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listSubZone_T2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst350.listSubZone_T2", model);
    }   
    
}

