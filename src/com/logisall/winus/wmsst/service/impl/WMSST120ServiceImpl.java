package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST120Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST120Service")
public class WMSST120ServiceImpl extends AbstractServiceImpl implements WMSST120Service {
    
    @Resource(name = "WMSST120Dao")
    private WMSST120Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.detail(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 대체 Method ID    : selectBox
     * 대체 Method 설명      : 물류용기관리 화면 데이타셋
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("inKey", "CYCL01");
        map.put("CYCL01", dao.selectCYCL01(model));
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 임가공 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        int errCnt = 0;

            try{
                int iuCnt = Integer.parseInt(model.get("selectIds").toString());
                
                if(iuCnt > 0){
                    for(int i=0; i<iuCnt; i++){
                        main.put("ST_GUBUN", model.get("ST_GUBUN"+i));
                        main.put("CYCL_STOCK_ID", model.get("CYCL_STOCK_ID"+i));
                        main.put("WORK_SEQ", model.get("WORK_SEQ"+i));
                        main.put("REAL_QTY", model.get("REAL_QTY"+i));
                        main.put("DIFF_REASON", model.get("DIFF_REASON"+i));
                        main.put("SS_USER_NO", model.get("SS_USER_NO"));
                    
                        if(main.get("ST_GUBUN").equals("UPDATE")){
                            dao.update(main);
                        }else{
                            errCnt++;
                        }
                    }
                }else{
                    errCnt++;
                }
                
                if(errCnt == 0){
                    m.put("MSG", MessageResolver.getMessage("save.success"));
                    m.put("errCnt", errCnt);
                }else{
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("errCnt", errCnt);
                }
            }catch (Exception e) {
                throw e;
            }
        return m;
    }
    
    
    /**
     * Method ID : updateSp
     * Method 설명 : 재실행
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateSp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        // int errCnt = 0;
        
        try{
        	String executeDate = (String)model.get("DATE_EXE");
        	if (StringUtils.isNotEmpty(executeDate)) {
        		executeDate = executeDate.replaceAll("-", "");        		
        	}
        	modelIns.put("WORK_DT", executeDate);
        	modelIns.put("WORK_NO", "SYSTEM");
        	modelIns.put("LC_ID", model.get("SS_SVC_NO"));
            modelIns = (Map<String, Object>)dao.runSpRotationalStockTakingJob(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
	        map.put("MSG", MessageResolver.getMessage("update.success"));
	        map.put("errCnt", 0);

        } catch(BizException be) {
            map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("update.error") );
            
            if ( log.isWarnEnabled() ) {
            	log.warn("[CronQuartzJob] END... (Error Occured) :" + be.getMessage() );
            }   	        
	        
        }catch (Exception e) {
            map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("update.error") );
            if (log.isErrorEnabled()) {
            	log.error("[CronQuartzJob] Error Occured :", e);
            }
        }
        return map;
    }
        
    
    /**
     * Method ID : listExcel
     * Method 설명 : 순환재고조사 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : listExcelSub
     * Method 설명 : 순환재고조사 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.detail(model));
        
        return map;
    }    
    
    /**
     * Method ID : uploadExcel
     * Method 설명 : 재고조사결과엑셀입력 프로시저 
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> uploadExcel(Map<String, Object> model, List<Map> list) throws Exception {
    	int listBodyCnt   = list.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
            	String[] cyclStockId 		= new String[listBodyCnt];
            	String[] workSeq 		= new String[listBodyCnt];
            	String[] locCd 		= new String[listBodyCnt];
            	String[] ritemCd 		= new String[listBodyCnt];
            	String[] ritmeId 		= new String[listBodyCnt];
            	
            	String[] bestdate 		= new String[listBodyCnt];
            	String[] expQty 		= new String[listBodyCnt];
            	String[] realQty 		= new String[listBodyCnt];
            	String[] diffReason 		= new String[listBodyCnt];
            	
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	Map object =  list.get(i);
                	
                	cyclStockId[i]			= object.containsKey("CYCL_STOCK_ID") ? (String)object.get("CYCL_STOCK_ID").toString().replaceAll("\"", "") : "";	
                	workSeq[i]			  	= object.containsKey("WORK_SEQ"     ) ? (String)object.get("WORK_SEQ"     ).toString().replaceAll("\"", "") : "";				
                	locCd[i]			    = object.containsKey("LOC_CD"       ) ? (String)object.get("LOC_CD"       ).toString().replaceAll("\"", "") : "";			
                	ritemCd[i]		  		= object.containsKey("RITEM_CD"     ) ? (String)object.get("RITEM_CD"     ).toString().replaceAll("\"", "") : "";				
                	ritmeId[i]			  	= object.containsKey("RITEM_NM"     ) ? (String)object.get("RITEM_NM"     ).toString().replaceAll("\"", "") : "";			

                	bestdate[i]			  = object.containsKey("BEST_DATE"    ) ? (String)object.get("BEST_DATE"    ).toString().replaceAll("\"", "") : "";				
                	expQty[i]		  	  = object.containsKey("EXP_QTY"      ) ? (String)object.get("EXP_QTY"      ).toString().replaceAll("\"", "") : "";				
                	realQty[i]			  = object.containsKey("REAL_QTY"     ) ? (String)object.get("REAL_QTY"     ).toString().replaceAll("\"", "") : "";			
                	diffReason[i]		  = object.containsKey("DIFF_REASON"  ) ? (String)object.get("DIFF_REASON"  ).toString().replaceAll("\"", "") : "";			
                	
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                
                modelIns.put( "I_CYCL_STOCK_ID" ,cyclStockId);                                
                modelIns.put( "I_WORK_SEQ"    	,workSeq);
                modelIns.put( "I_LOC_CD"        ,locCd);
                modelIns.put( "I_RITEM_CD"     	,ritemCd);
                modelIns.put( "I_RITEM_NM"      ,ritmeId);
                
                modelIns.put( "I_BEST_DATE"    	,bestdate);
                modelIns.put( "I_EXP_QTY"       ,expQty);
                modelIns.put( "I_REAL_QTY"      ,realQty);
                modelIns.put( "I_DIFF_REASON"   ,diffReason);

                modelIns.put("I_LC_ID"    			, model.get("SS_SVC_NO")); 
                modelIns.put("I_CUST_ID"    		, model.get("vrCustId"));
  
                modelIns.put("I_WORK_IP"  			, model.get("SS_CLIENT_IP"));  
                modelIns.put("I_USER_NO"  			, model.get("SS_USER_NO"));
                
                //dao                
                modelIns = (Map<String, Object>)dao.uploadExcel(modelIns);
                ServiceUtil.isValidReturnCode("WMSST120", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
//                m.put("MSG_ORA", modelIns.get("O_MSG_CODE"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
