package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsst.service.WMSST401Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST401Service")
public class WMSST401ServiceImpl extends AbstractServiceImpl implements WMSST401Service {
    
    @Resource(name = "WMSST401Dao")
    private WMSST401Dao dao;

    /**
     * Method ID 			: wmsst401list
     * Method 설명 		: 랙보충내역 
     * 작성자 				: KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> wmsst401list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		  if (model.get("page") == null) {
                  model.put("pageIndex", "1");
              } else {
                  model.put("pageIndex", model.get("page"));
              }
              if (model.get("rows") == null) {
                  model.put("pageSize", "20");
              } else {
                  model.put("pageSize", model.get("rows"));
              }
              
    		map.put("LIST", dao.wmsst401list(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID 			: listExcel
     * Method 설명 		: 랙보충내역 엑셀다운
     * 작성자 				: KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.wmsst401list(model));
        
        return map;
    }    
   
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            model.put("inKey", "ORD01");
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
}
