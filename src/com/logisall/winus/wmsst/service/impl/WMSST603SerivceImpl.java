package com.logisall.winus.wmsst.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsst.service.WMSST603Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST603Service")
public class WMSST603SerivceImpl extends AbstractServiceImpl implements WMSST603Service{

	@Resource(name = "WMSST603Dao")
    private WMSST603Dao dao;
	
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }

    	map.put("LIST", dao.list(model));
        return map;
    }
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	String ordId = "";
            	String ordSeq = "";
            	String userMemo = "";
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId   = (String)model.get("ORD_ID"+i);               
                	ordSeq  = (String)model.get("ORD_SEQ"+i); 
                	userMemo  = (String)model.get("USER_MEMO"+i); 
                	
                    //session 및 등록정보
                    modelIns.put("LC_ID", LC_ID);
                    modelIns.put("WORK_IP", WORK_IP);
                    modelIns.put("USER_NO", USER_NO);
                    
                    //프로시져에 보낼것들 다담는다
                    modelIns.put("ordId", ordId);
                    modelIns.put("ordSeq", ordSeq);
                    modelIns.put("userMemo", userMemo);
                	dao.save(modelIns);
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
   public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       int errCnt = 0;
       int insertCnt = (list != null)?list.size():0;
           try{     
	   			for (int i = 0; i < insertCnt; i++) {
	   				Map<String, Object> paramMap = (Map) list.get(i);
	   				Map<String, Object> modelIns = new HashMap<String, Object>();
	
					if ( (paramMap.get("I_ORD_ID")  != null && StringUtils.isNotEmpty(paramMap.get("I_ORD_ID").toString()))
					   &&(paramMap.get("I_ORD_SEQ") != null && StringUtils.isNotEmpty(paramMap.get("I_ORD_SEQ").toString())) ) {
						
			            modelIns.put("ordId" , (String)paramMap.get("I_ORD_ID"));
			            modelIns.put("ordSeq", (String)paramMap.get("I_ORD_SEQ"));
			            modelIns.put("userMemo" , (String)paramMap.get("I_USER_MEMO"));
			            
			            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			            
			            dao.save(modelIns);
					}
				}
               
               m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
               m.put("MSG_ORA", "");
               m.put("errCnt", errCnt);
               
           } catch(Exception e){
               throw e;
           }
       return m;
   }
}
