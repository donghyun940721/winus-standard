package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST099Dao")
public class WMSST099Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 :  재고내역조회(SDI) 기본 조회 쿼리
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmsst099.list", model);
    }   
    
    /**
     * Method ID : listTotal
     * Method 설명 :  재고내역조회(SDI) 기본 조회 쿼리 listTotal
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public GenericResultSet listTotal(Map<String, Object> model) {
        return executeQueryWq("wmsst099.listTotal", model);
    }   
    

//    /**
//     * Method ID : ordComplete
//     * Method 설명 : 검수내역 입고확정
//     * 작성자 : SUMMER H
//     * @param model
//     * @return
//     */
//    public Object ordComplete(Map<String, Object> model) {
//    	executeUpdate("wmsst064.pk_wmsop021.sp_oms_op_mobile_in_complete", model);
//    	return model;
//    }
//    
    

}

