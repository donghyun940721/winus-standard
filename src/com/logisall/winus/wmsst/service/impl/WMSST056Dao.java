package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST056Dao")
public class WMSST056Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 일별재고 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst056.list", model);
    }
    
    /**
     * Method ID : listT2
     * Method 설명 : 일별재고 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet listT2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst056.listT2", model);
    }
}

