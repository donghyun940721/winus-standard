package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST110Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST110Service")
public class WMSST110ServiceImpl extends AbstractServiceImpl implements WMSST110Service {
    
    @Resource(name = "WMSST110Dao")
    private WMSST110Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.detail(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID : listExcel
     * Method 설명 : HEADER 엑셀다운
     * 작성자 : SUMEMR
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        if(!"".equals(model.get("hdnCustId"))){
            model.put("hdnCustId", model.get("hdnCustId"));
        }
        if(!"".equals(model.get("txtSrchLocId"))){
            model.put("txtSrchLocId", model.get("txtSrchLocId"));
        }
        if(!"".equals(model.get("txtSrchItemId"))){
            model.put("txtSrchItemId", model.get("txtSrchItemId"));
        }
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : DETIAL 엑셀다운
     * 작성자 : SUMEMR
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        if(!"".equals(model.get("hdnCustId"))){
            model.put("hdnCustId", model.get("hdnCustId"));
        }
        if(!"".equals(model.get("UOM_ID"))){
            model.put("UOM_ID", model.get("UOM_ID"));
        }
        if(!"".equals(model.get("RITEM_ID"))){
            model.put("RITEM_ID", model.get("RITEM_ID"));
        }
        if(!"".equals(model.get("LOC_ID"))){
            model.put("LOC_ID", model.get("LOC_ID"));
        }
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.detail(model));
        
        return map;
    }
    
}
