package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST055Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST055Service")
public class WMSST055ServiceImpl extends AbstractServiceImpl implements WMSST055Service {
    
    @Resource(name = "WMSST055Dao")
    private WMSST055Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 입별재고 조회
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    
    
    /**
     * Method ID : list
     * Method 설명 : 입별재고 조회
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list057(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.list057(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    
    
    
    
    
    /**
     * Method ID : list
     * Method 설명 : 입별재고상세(new) -> (기간별)
     * 작성자 : kijun11
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.listE2(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    
    
    
    
    /**
     * Method ID : wmsst058list
     * Method 설명 : 재고실사(확정) 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> wmsst058list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.wmsst058list(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID : listExcel
     * Method 설명 : 입별재고 엑셀다운
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listExcel(model));
        
        return map;
    }    
   
    
    /**
     * Method ID : listExcel_locSum
     * Method 설명 : 엑셀(일자별Loc합계)
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel_locSum(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listExcel_locSum(model));
        
        return map;
    }    
        
    
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    public Map<String, Object> wmsst059list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.wmsst059list(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    } 
    
}
