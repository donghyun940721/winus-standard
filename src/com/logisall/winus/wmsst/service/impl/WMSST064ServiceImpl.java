package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;









import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsst.service.WMSST064Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Service("WMSST064Service")
public class WMSST064ServiceImpl extends AbstractServiceImpl implements WMSST064Service {
    
    @Resource(name = "WMSST064Dao")
    private WMSST064Dao dao;


    /**
     * Method ID : list
     * Method 설명 : youm 거래처별 일보 조회
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list2
     * Method 설명 : rigid 거래처별 일보 조회
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list2(model));
        } catch (Exception e) {
            log.error(e.toString());

            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list3
     * Method 설명 : youm 거래처별 통합 일보 조회
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list3(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list4
     * Method 설명 : rigid 거래처별 통합 일보 조회
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list4(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list4(model));
        } catch (Exception e) {
            log.error(e.toString());

            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 
     * 대체 Method ID   : ordComplete
     * 대체 Method 설명    : 검수내역 입고확정
     * 작성자               	  : SUMMER HYUN
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> ordComplete(Map<String, Object> model) throws Exception {
    	 
    	 Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		int tempCnt = Integer.parseInt(model.get("selectIds").toString());
    		System.out.println("impl tempCnt - "+tempCnt);
    		if(tempCnt > 0){
    			
    			//필요한 값 DB로 보내서 조회 
    			
    			String[] ordId = new String[tempCnt];
    			String[] ordSeq = new String[tempCnt];
    			String[] custId = new String[tempCnt];
    			String[] ritemId = new String[tempCnt];
    			String[] lcId = new String[tempCnt];

    			String[] itemBestDateEnd = new String[tempCnt];
    			String[] makeDate = new String[tempCnt];
    			String[] workQty = new String[tempCnt];

    			String[] boxBarCd = new String[tempCnt];
    			String[] toLocCd = new String[tempCnt];
    			String[] boxInQty = new String[tempCnt];
    			String[] itemBarCd = new String[tempCnt];
    			String[] workMemo = new String[tempCnt];

    			String[] custLotNo = new String[tempCnt];
    			
    			for(int i = 0 ; i < tempCnt ; i ++){
    				ordId[i]		= (String)model.get("ORG_ORD_ID"+i);
    				ordSeq[i]		= (String)model.get("ORG_ORD_SEQ"+i);
    				custId[i]		= (String)model.get("CUST_ID");    				
    				ritemId[i]		= (String)model.get("RITEM_ID"+i);
    				lcId[i]			=(String)model.get(ConstantIF.SS_SVC_NO);

    				itemBestDateEnd[i]		= null;
    				makeDate[i]		= null;
    				workQty[i]		= (String)model.get("TOTAL_WORK_QTY"+i);

    				boxBarCd[i]		= null;
    				toLocCd[i]		= null;
    				boxInQty[i]		= null;
    				itemBarCd[i]	= null;
    				workMemo[i]		= null;

    				custLotNo[i]	= null;
    			}
    			m.put("LC_ID", lcId);
    			m.put("ORD_ID", ordId);
    			m.put("ORD_SEQ", ordSeq);
    			m.put("CUST_ID", custId);
    			m.put("RITEM_ID", ritemId);

    			m.put("ITEM_BEST_DATE_END", itemBestDateEnd);
    			m.put("MAKE_DATE", makeDate);
    			m.put("WORK_QTY", workQty);
    			
    			m.put("BOX_BAR_CD", boxBarCd);
    			m.put("TO_LOC_CD", toLocCd);
    			m.put("BOX_IN_QTY", boxInQty);
    			m.put("ITEM_BAR_CD", itemBarCd);
    			m.put("WORK_MEMO", workMemo);

    			m.put("CUST_LOT_NO", custLotNo);
    			
    			//수정자, WORK_IP
				m.put("WORK_IP",(String)model.get(ConstantIF.SS_CLIENT_IP));
				m.put("USER_NO",(String)model.get(ConstantIF.SS_USER_NO));
				
				System.out.println("impl -  "+m.toString());
				
				dao.ordComplete(m);
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   m.put("errCnt", 1);
    	   m.put("MSG_ORA", e.getMessage());
    	}
       
        return m;
    }
    
    
}

