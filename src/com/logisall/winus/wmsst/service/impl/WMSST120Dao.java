package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST120Dao")
public class WMSST120Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst120.list", model);
    }
    
    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet detail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst120.detail", model);
    }
    
    /**
     * Method ID : selectCYCL01
     * Method 설명 : 순환재고조사방식 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectCYCL01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID    : update
     * Method 설명      : 순환재고조사 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsst121.update", model);
    }  
    
    /**
     * Method ID    : updateSp
     * Method 설명      : 순환재고조사 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object updateSp(Map<String, Object> model) {
        return executeUpdate("wmsst121.updateSp", model);
    }   
    
    
    public Object runSpRotationalStockTakingJob(Map<String, Object> model){
        executeUpdate("pk_wmsjb000.sp_rotational_stock_taking_man", model);
        return model;
    }      
    
    /**
	 * Method ID : uploadExcel - 재고조사결과엑셀입력 프로시저 
	 * 저장 작성자 : summer
	 * 
	 * @param model
	 * @return
	 */
	public Object uploadExcel(Map<String, Object> model) {
		executeUpdate("wmsst120.pk_wmsst120.sp_excel_stock_inspection_rst_upload", model);
		return model;
	}
}
