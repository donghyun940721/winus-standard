package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST055Dao")
public class WMSST055Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 일별재고 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst055.list", model);
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 일별재고 조회(엑셀다운로드)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet listExcel(Map<String, Object> model) {
        return executeQueryWq("wmsst055.listExcel", model);
    }
    
    
    /**
     * Method ID : listExcel_locSum
     * Method 설명 : 엑셀(일자별Loc합계)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listExcel_locSum(Map<String, Object> model) {
        return executeQueryWq("wmsst055.listExcel_locSum", model);
    }
    
    
    
    /**
     * Method ID : list057
     * Method 설명 : 일별재고상세(new) 조회
     * 작성자 : kijun11
     * @param model
     * @return
     */
    public GenericResultSet list057(Map<String, Object> model) {
        return executeQueryPageWq("wmsst057.list", model);
    }
    
    
    /**
     * Method ID : listE2
     * Method 설명 : 일별재고상세(기간별) 조회
     * 작성자 : kijun11
     * @param model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst055.listE2", model);
    }   
    
    
    /**
     * Method ID : wmsst058list
     * Method 설명 : 재고실사(확정)
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet wmsst058list(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst058.list", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    public GenericResultSet wmsst059list(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst059.list", model);
    }  
}

