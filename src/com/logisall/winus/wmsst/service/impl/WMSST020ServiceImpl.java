package com.logisall.winus.wmsst.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST020Service")
public class WMSST020ServiceImpl extends AbstractServiceImpl implements WMSST020Service {
    
    @Resource(name = "WMSST020Dao")
    private WMSST020Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            String vrViewSet = "N";
            List<String> checkbox = new ArrayList<>();
            
            if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
            if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
            if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
            if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){
            	checkbox.add("30");
            	if(model.get("SS_SVC_NO").equals("0000004760")){
            		// 콘크리트웍스일때만!
            		checkbox.add("164");
            		checkbox.add("168");
            	}
            }
            if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
            if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
            if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
            if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
            if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
            if(model.containsKey("chViewSetItem") && model.get("chViewSetItem").equals("on")){checkbox.add("90"); checkbox.add("100");}
            if(model.containsKey("chViewOutRtn") && model.get("chViewOutRtn").equals("on")){checkbox.add("111");}
            if(model.containsKey("chViewOrtn") && model.get("chViewOrtn").equals("on")){checkbox.add("158");}
            if(model.containsKey("chViewSrtn") && model.get("chViewSrtn").equals("on")){checkbox.add("159");}
            if(model.containsKey("chViewB2bPcs") && model.get("chViewB2bPcs").equals("on")){checkbox.add("160");}
            if(model.containsKey("chViewB2bBox") && model.get("chViewB2bBox").equals("on")){checkbox.add("161");}
            if(model.containsKey("chViewB2c") && model.get("chViewB2c").equals("on")){checkbox.add("162");}
            if(model.containsKey("chViewB2cCom") && model.get("chViewB2cCom").equals("on")){checkbox.add("163");}
            if(model.containsKey("chView142") && model.get("chView142").equals("on")){checkbox.add("142");}
            if(model.containsKey("chView143") && model.get("chView143").equals("on")){checkbox.add("143");}
            if(model.containsKey("chView164") && model.get("chView164").equals("on")){checkbox.add("164");}
            if(model.containsKey("chView165") && model.get("chView165").equals("on")){checkbox.add("165");}
            
            model.put("vrViewSet", vrViewSet);
            model.put("chekbox", checkbox);
            if(!model.get("vrSrchBarcode").equals("")){
                //바코드내역조회
                map.put("LIST", dao.listBarcode(model));
            }else if(model.get("vrSrchBlNo").equals("VX")){
                
                if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
                    model.put("vrViewSet", "N");
                }
                //VX 입출고내역 조회
                map.put("LIST", dao.listBlNoVX(model));
            }else if(!model.get("vrSrchBlNo").equals("")){
            	
            	if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
					model.put("vrViewSet", "N");
				}
                //B/L 입출고내역 조회
                map.put("LIST", dao.listBlNo(model));
            }else{
            	if(model.get("SS_SVC_NO").equals("0000004760")){
            		// 콘크리트웍스일때만!
            		map.put("LIST", dao.listBlNo_con(model));
            	}else{
                    //B/L 입출고내역 조회
                    map.put("LIST", dao.listBlNo(model));
            	}
            }
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list_E2
     * Method 설명 : 재고 입출고내역(비고표시) 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list_E2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            String vrViewSet = "N";
            List<String> checkbox = new ArrayList<>();
            
            if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
            if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
            if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
            if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){
            	checkbox.add("30");
            	if(model.get("SS_SVC_NO").equals("0000004760")){
            		// 콘크리트웍스일때만!
            		checkbox.add("164");
            		checkbox.add("168");
            	}
            }
            if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
            if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
            if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
            if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
            if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
            if(model.containsKey("chViewSetItem") && model.get("chViewSetItem").equals("on")){checkbox.add("90"); checkbox.add("100");}
            if(model.containsKey("chViewOutRtn") && model.get("chViewOutRtn").equals("on")){checkbox.add("111");}
            if(model.containsKey("chViewOrtn") && model.get("chViewOrtn").equals("on")){checkbox.add("158");}
            if(model.containsKey("chViewSrtn") && model.get("chViewSrtn").equals("on")){checkbox.add("159");}
            if(model.containsKey("chViewB2bPcs") && model.get("chViewB2bPcs").equals("on")){checkbox.add("160");}
            if(model.containsKey("chViewB2bBox") && model.get("chViewB2bBox").equals("on")){checkbox.add("161");}
            if(model.containsKey("chViewB2c") && model.get("chViewB2c").equals("on")){checkbox.add("162");}
            if(model.containsKey("chViewB2cCom") && model.get("chViewB2cCom").equals("on")){checkbox.add("163");}
            if(model.containsKey("chView142") && model.get("chView142").equals("on")){checkbox.add("142");}
            if(model.containsKey("chView143") && model.get("chView143").equals("on")){checkbox.add("143");}
            if(model.containsKey("chView164") && model.get("chView164").equals("on")){checkbox.add("164");}
            if(model.containsKey("chView165") && model.get("chView165").equals("on")){checkbox.add("165");}
            
            model.put("vrViewSet", vrViewSet);
            model.put("chekbox", checkbox);
            if(!model.get("vrSrchBarcode").equals("")){
                //바코드내역조회
                map.put("LIST", dao.listBarcode(model));
            }else if(model.get("vrSrchBlNo").equals("VX")){
                
                if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
                    model.put("vrViewSet", "N");
                }
                //VX 입출고내역 조회
                map.put("LIST", dao.listBlNoVX(model));
            }else if(!model.get("vrSrchBlNo").equals("")){
            	
            	if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
					model.put("vrViewSet", "N");
				}
                //B/L 입출고내역 조회
                map.put("LIST", dao.listBlNo_E2(model));
            }else{
            	if(model.get("SS_SVC_NO").equals("0000004760")){
            		// 콘크리트웍스일때만!
            		map.put("LIST", dao.listBlNo_E2_con(model));
            	}else{
                    //B/L 입출고내역 조회
                    map.put("LIST", dao.listBlNo_E2(model));
            	}
            }
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 재고 입출고내역 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        

        String vrViewSet = "N";
        List<String> checkbox = new ArrayList<>();
        
        if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
        if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
        if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
        if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){
        	checkbox.add("30");
        	if(model.get("SS_SVC_NO").equals("0000004760")){
        		// 콘크리트웍스일때만!
        		checkbox.add("164");
        		checkbox.add("168");
        	}
        }
        if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
        if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
        if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
        if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
        if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
        if(model.containsKey("chViewSetItem") && model.get("chViewSetItem").equals("on")){checkbox.add("90"); checkbox.add("100");}
        if(model.containsKey("chViewOutRtn") && model.get("chViewOutRtn").equals("on")){checkbox.add("111");}
        if(model.containsKey("chViewOrtn") && model.get("chViewOrtn").equals("on")){checkbox.add("158");}
        if(model.containsKey("chViewSrtn") && model.get("chViewSrtn").equals("on")){checkbox.add("159");}
        if(model.containsKey("chViewB2bPcs") && model.get("chViewB2bPcs").equals("on")){checkbox.add("160");}
        if(model.containsKey("chViewB2bBox") && model.get("chViewB2bBox").equals("on")){checkbox.add("161");}
        if(model.containsKey("chViewB2c") && model.get("chViewB2c").equals("on")){checkbox.add("162");}
        if(model.containsKey("chViewB2cCom") && model.get("chViewB2cCom").equals("on")){checkbox.add("163");}
        if(model.containsKey("chView142") && model.get("chView142").equals("on")){checkbox.add("142");}
        if(model.containsKey("chView143") && model.get("chView143").equals("on")){checkbox.add("143");}
        if(model.containsKey("chView164") && model.get("chView164").equals("on")){checkbox.add("164");}
        if(model.containsKey("chView165") && model.get("chView165").equals("on")){checkbox.add("165");}
        
        model.put("vrViewSet", vrViewSet);
        model.put("chekbox", checkbox);
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
       // map.put("LIST", dao.listBlNo(model));
        
        if(!model.get("vrSrchBarcode").equals("")){
            //바코드내역조회
            map.put("LIST", dao.listBarcode(model));
        }else if(model.get("vrSrchBlNo").equals("VX")){
            
            if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
                model.put("vrViewSet", "N");
            }
            //VX 입출고내역 조회
            map.put("LIST", dao.listBlNoVX(model));
        }else if(!model.get("vrSrchBlNo").equals("")){
        	
        	if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
				model.put("vrViewSet", "N");
			}
            //B/L 입출고내역 조회
            map.put("LIST", dao.listBlNo(model));
        }else{
        	if(model.get("SS_SVC_NO").equals("0000004760")){
        		// 콘크리트웍스일때만!
        		map.put("LIST", dao.listBlNo_con(model));
        	}else{
                //B/L 입출고내역 조회
                map.put("LIST", dao.listBlNo(model));
        	}
        }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 재고 입출고내역(비고표시) 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel_E2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        

        String vrViewSet = "N";
        List<String> checkbox = new ArrayList<>();
        
        if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
        if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
        if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
        if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){
        	checkbox.add("30");
        	if(model.get("SS_SVC_NO").equals("0000004760")){
        		// 콘크리트웍스일때만!
        		checkbox.add("164");
        		checkbox.add("168");
        	}
        }
        if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
        if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
        if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
        if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
        if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
        if(model.containsKey("chViewSetItem") && model.get("chViewSetItem").equals("on")){checkbox.add("90"); checkbox.add("100");}
        if(model.containsKey("chViewOutRtn") && model.get("chViewOutRtn").equals("on")){checkbox.add("111");}
        if(model.containsKey("chViewOrtn") && model.get("chViewOrtn").equals("on")){checkbox.add("158");}
        if(model.containsKey("chViewSrtn") && model.get("chViewSrtn").equals("on")){checkbox.add("159");}
        if(model.containsKey("chViewB2bPcs") && model.get("chViewB2bPcs").equals("on")){checkbox.add("160");}
        if(model.containsKey("chViewB2bBox") && model.get("chViewB2bBox").equals("on")){checkbox.add("161");}
        if(model.containsKey("chViewB2c") && model.get("chViewB2c").equals("on")){checkbox.add("162");}
        if(model.containsKey("chViewB2cCom") && model.get("chViewB2cCom").equals("on")){checkbox.add("163");}
        if(model.containsKey("chView142") && model.get("chView142").equals("on")){checkbox.add("142");}
        if(model.containsKey("chView143") && model.get("chView143").equals("on")){checkbox.add("143");}
        if(model.containsKey("chView164") && model.get("chView164").equals("on")){checkbox.add("164");}
        if(model.containsKey("chView165") && model.get("chView165").equals("on")){checkbox.add("165");}
        
        model.put("vrViewSet", vrViewSet);
        model.put("chekbox", checkbox);
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
       // map.put("LIST", dao.listBlNo(model));
        
        if(!model.get("vrSrchBarcode").equals("")){
            //바코드내역조회
            map.put("LIST", dao.listBarcode(model));
        }else if(model.get("vrSrchBlNo").equals("VX")){
            
            if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
                model.put("vrViewSet", "N");
            }
            //VX 입출고내역 조회
            map.put("LIST", dao.listBlNoVX(model));
        }else if(!model.get("vrSrchBlNo").equals("")){
        	
        	if(model.get("vrViewSet").equals("Y")){ //xml > vrViewSet Y : 미사용 
				model.put("vrViewSet", "N");
			}
            //B/L 입출고내역 조회
            map.put("LIST", dao.listBlNo_E2(model));
        }else{
        	if(model.get("SS_SVC_NO").equals("0000004760")){
        		// 콘크리트웍스일때만!
        		map.put("LIST", dao.listBlNo_E2_con(model));
        	}else{
                //B/L 입출고내역 조회
                map.put("LIST", dao.listBlNo_E2(model));
        	}
        }
        
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 재고입출고내역 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] iWorkDt = new String[iuCnt]; 
            String[] iWorkId = new String[iuCnt]; 
            
            for(int i = 0 ; i < iuCnt; i ++){
                iWorkDt[i]      = (String)model.get("WORK_DT"+i);
                iWorkId[i]      = (String)model.get("WORK_ID"+i);
            }
          
            if(iuCnt > 0){
               Map<String, Object> modelSP = new HashMap<String, Object>();
         
               modelSP.put("I_WORK_ID", iWorkId);
               modelSP.put("I_WORK_DT", iWorkDt);
               modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
               modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
          
               dao.save(modelSP);
               m.put("MSG", MessageResolver.getMessage("update.success"));
               m.put("errCnt", errCnt);
             }
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   		: listSummaryCount
     * Method 설명      : 재고입출고내역 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }

        String vrViewSet = "N";
        List<String> checkbox = new ArrayList<>();
        
        if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
        if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
        if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
        if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){
        	checkbox.add("30");
        	if(model.get("SS_SVC_NO").equals("0000004760")){
        		// 콘크리트웍스일때만!
        		checkbox.add("164");
        		checkbox.add("168");
        	}
        }
        if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
        if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
        if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
        if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
        if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
        if(model.containsKey("chViewSetItem") && model.get("chViewSetItem").equals("on")){checkbox.add("90"); checkbox.add("100");}
        if(model.containsKey("chViewOutRtn") && model.get("chViewOutRtn").equals("on")){checkbox.add("111");}
        if(model.containsKey("chViewOrtn") && model.get("chViewOrtn").equals("on")){checkbox.add("158");}
        if(model.containsKey("chViewSrtn") && model.get("chViewSrtn").equals("on")){checkbox.add("159");}
        if(model.containsKey("chViewB2bPcs") && model.get("chViewB2bPcs").equals("on")){checkbox.add("160");}
        if(model.containsKey("chViewB2bBox") && model.get("chViewB2bBox").equals("on")){checkbox.add("161");}
        if(model.containsKey("chViewB2c") && model.get("chViewB2c").equals("on")){checkbox.add("162");}
        if(model.containsKey("chViewB2cCom") && model.get("chViewB2cCom").equals("on")){checkbox.add("163");}
        if(model.containsKey("chView142") && model.get("chView142").equals("on")){checkbox.add("142");}
        if(model.containsKey("chView143") && model.get("chView143").equals("on")){checkbox.add("143");}
        if(model.containsKey("chView164") && model.get("chView164").equals("on")){checkbox.add("164");}
        if(model.containsKey("chView165") && model.get("chView165").equals("on")){checkbox.add("165");}
        
        model.put("vrViewSet", vrViewSet);
        model.put("chekbox", checkbox);
        if(model.get("vrSrchBlNo").equals("VX")){
            map.put("LIST", dao.listSummaryCountVX(model));
        }else{
            map.put("LIST", dao.listSummaryCount(model));
        }
        return map;
    }
    
    
    /**
     * Method ID   		: listSummaryCount_E2
     * Method 설명      : 재고입출고내역(비고표시) COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSummaryCount_E2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }

        String vrViewSet = "N";
        List<String> checkbox = new ArrayList<>();
        
        if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
        if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
        if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
        if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){
        	checkbox.add("30");
        	if(model.get("SS_SVC_NO").equals("0000004760")){
        		// 콘크리트웍스일때만!
        		checkbox.add("164");
        		checkbox.add("168");
        	}
        }
        if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
        if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
        if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
        if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
        if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
        if(model.containsKey("chViewSetItem") && model.get("chViewSetItem").equals("on")){checkbox.add("90"); checkbox.add("100");}
        if(model.containsKey("chViewOutRtn") && model.get("chViewOutRtn").equals("on")){checkbox.add("111");}
        if(model.containsKey("chViewOrtn") && model.get("chViewOrtn").equals("on")){checkbox.add("158");}
        if(model.containsKey("chViewSrtn") && model.get("chViewSrtn").equals("on")){checkbox.add("159");}
        if(model.containsKey("chViewB2bPcs") && model.get("chViewB2bPcs").equals("on")){checkbox.add("160");}
        if(model.containsKey("chViewB2bBox") && model.get("chViewB2bBox").equals("on")){checkbox.add("161");}
        if(model.containsKey("chViewB2c") && model.get("chViewB2c").equals("on")){checkbox.add("162");}
        if(model.containsKey("chViewB2cCom") && model.get("chViewB2cCom").equals("on")){checkbox.add("163");}
        if(model.containsKey("chView142") && model.get("chView142").equals("on")){checkbox.add("142");}
        if(model.containsKey("chView143") && model.get("chView143").equals("on")){checkbox.add("143");}
        if(model.containsKey("chView164") && model.get("chView164").equals("on")){checkbox.add("164");}
        if(model.containsKey("chView165") && model.get("chView165").equals("on")){checkbox.add("165");}
        
        model.put("vrViewSet", vrViewSet);
        model.put("chekbox", checkbox);
        if(model.get("vrSrchBlNo").equals("VX")){
            map.put("LIST", dao.listSummaryCountVX(model));
        }else{
            map.put("LIST", dao.listSummaryCount_E2(model));
        }
        return map;
    }
}
