package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST010Dao")
public class WMSST010Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 현재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.list", model);
    }
    
    /**
     * Method ID : listV2
     * Method 설명 : 현재고 조회V2
     * 작성자 : seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet listV2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.listV2", model);
    }
    
    /**
     * Method ID : listV2_E3
     * Method 설명 : listV2_E3
     * 작성자 : KIJUN11
     * @param model
     * @return
     */
    public GenericResultSet listV2_E3(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.listV2_E3", model);
    }
    
    /**
     * Method ID : listV3
     * Method 설명 : 현재고 조회V3
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listV3(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst010.listV3", model);
    }
    
    
    /**
     * Method ID : listBlNo
     * Method 설명 : BlNo 현재고 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listBlNo(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst010.listBlNo", model);
    }
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selecZone
     * Method 설명 : 현재고조회(APPAREL) ZONE 조회
     * 작성자 : 김채린
     * @param model
     * @return
     */
    public Object selecZone(Map<String, Object> model){
        return executeQueryForList("wmsms094.selecZone", model);
    }
    
    /**
     * Method ID : sublist
     * Method 설명 : 현재고 서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model){
        return executeQueryPageWq("wmsst010.sublist", model);
    }
    
    /**
     * Method ID : sublist
     * Method 설명 : 현재고 서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublistExcel3(Map<String, Object> model){
        return executeQueryWq("wmsst010.sublistExcel", model);
    }
    
    /**
     * Method ID : sublistExcel014E1_2
     * Method 설명 : 현재고(New)서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
	public Object sublistExcel014E1_2(Map<String, Object> model) {
		return executeQueryWq("wmsst014.sublistExcel014E1_2", model);
	}
    
    /**
     * Method ID : listV2_detail
     * Method 설명 : 현재고 조회V2 detail
     * 작성자 : seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet listV2_detail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.listV2_detail", model);
    }
    
    /**
     * Method ID : listV2_detail
     * Method 설명 : 일별재고(수불장)현재고 조회V2 detail
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet subListV2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.subListV2", model);
    }
    
    /**
     * Method ID : listV3_detail
     * Method 설명 : 현재고 조회V3 detail
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listV3_detail(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst010.listV3_detail", model);
    }
    
    
    /**
     * Method ID : listBlNo_detail
     * Method 설명 : BlNo 현재고 조회 detail
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listBlNo_detail(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst010.listBlNo_detail", model);
    }
    
    
    
    /**
     * Method ID : delete
     * Method 설명 : 현재고 비고 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsst010.update", model);
    }
    
    /**
     * Method ID  : save
     * Method 설명  : 현재고 PLT 수량 수정
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object save(Map<String, Object> model){
        return  executeUpdate("wmsst010.pk_wmsst010.sp_save_plt_qty", model);
    }
    
    /**
     * Method ID : poplist
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet poplist(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.searchSet", model);
    }   
    
    /**
     * Method ID : popdetail
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet popdetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.subSearch", model);
    }
    
    /**
     * Method ID	: subulManualComp
     * Method 설명 	: 현재고 수불재생성
     * 작성자			: 기드온
     * @param model
     * @return
     */
    public Object subulManualComp(Map<String, Object> model){
        return  executeUpdate("wmsst010.pk_wmsst010.sp_subul_manual_complete", model);
    }
    
    /**
     * Method ID	: initSubul
     * Method 설명 	: 현재고  당일수불재고초기화
     * 작성자			: 기드온
     * @param model
     * @return
     */
    public Object initSubul(Map<String, Object> model){
        return  executeUpdate("wmsst010.initSubul", model);
    }
    
    /**
     * Method ID : listApparel
     * Method 설명 : 일별재고 조회(APPAREL)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listApparel(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.listApparel", model);
    } 
    
    /**
     * Method ID : listE5
     * Method 설명 : 현재고 조회(APPAREL_ERP)
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet listE5(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.listE5", model);
    }
    
    /**
     * Method ID : sublistE5
     * Method 설명 : 현재고 상세 조회(APPAREL_ERP)
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet sublistE5(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.sublistE5", model);
    }
    
    /**
     * Method ID : itemList
     * Method 설명 : 상품별 현재고 조회 팝업
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet itemList(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.itemList", model);
    }
    
    /**
     * Method ID  : stockInvalidItemEndDate
     * Method 설명   : 유효기간 만료 및 N일이내 상품 조회 
     * 작성자              : yhku
     * @param   model
     * @return
     */
    public Object stockInvalidItemEndDate(Map<String, Object> model){
        return executeQueryForList("wmsst010.stockInvalidItemEndDate", model);
    }

	public Object sublistExcel_HanSung(Map<String, Object> model) {
		return executeQueryWq("wmsst014.sublist_HanSung", model);
	}
    
}
