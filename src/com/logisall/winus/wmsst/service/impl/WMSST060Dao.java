package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST060Dao")
public class WMSST060Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 재고실사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst060.search", model);
    }
    
    /**
     * Method ID : detail
     * Method 설명 : 재고실사 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet detail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst060.detailSearch", model);
    }
    /**
     * Method ID  : save
     * Method 설명  : 재고실사 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst010.pk_wmsst010.sp_reg_stock", model);
        return model;
    }
}
