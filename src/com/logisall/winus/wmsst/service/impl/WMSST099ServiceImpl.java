package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsst.service.WMSST099Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Service("WMSST099Service")
public class WMSST099ServiceImpl extends AbstractServiceImpl implements WMSST099Service {
    
    @Resource(name = "WMSST099Dao")
    private WMSST099Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고내역조회(SDI) 기본 조회 쿼리
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID : listTotal
     * Method 설명 : 재고내역조회(SDI) 기본 조회 쿼리 listTotal
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listTotal(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.listTotal(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
}

