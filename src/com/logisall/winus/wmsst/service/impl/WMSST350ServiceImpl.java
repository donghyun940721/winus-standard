package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.logisall.winus.wmsop.service.impl.WMSOP520Dao;
import com.logisall.winus.wmsst.service.WMSST350Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST350Service")
public class WMSST350ServiceImpl extends AbstractServiceImpl implements WMSST350Service {
    
    @Resource(name = "WMSST350Dao")
    private WMSST350Dao dao;


    @Resource(name = "WMSOP520Dao")
    private WMSOP520Dao OP520dao;
    
    @Resource(name = "WMSST010Dao")
    private WMSST010Dao ST010dao;
    
    /**
     * Method ID   : selectItemGrp
     * Method 설명    : 출고관리 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ZONEGUBUN"	, ST010dao.selecZone(model));
        map.put("ITEMGRP"	, OP520dao.selectItemGrp(model));
        if(model.get("SS_SVC_NO").equals("0000001840")){
	        map.put("ITEMGRP98"	, OP520dao.selectItemGrp98(model));
	        map.put("ITEMGRP99"	, OP520dao.selectItemGrp99(model));
        }
        return map;
    }
    
    /**
     * Method ID : list_T1
     * Method 설명 : 임가공(GS) 조회
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list_T1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.list_T1(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : list_T2
     * Method 설명 : 임가공(GS 전체) 조회
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list_T2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "20");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}        
    		map.put("LIST", dao.list_T2(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID : listSub_T1
     * Method 설명 : 임가공(GS) 구성품 조회 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub_T1(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.listSub_T1(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
    	return map;
    }   
    
    /**
     * Method ID : listSub_T2
     * Method 설명 : 임가공(GS) 구성품 조회 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub_T2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "20");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}        
    		map.put("LIST", dao.listSub_T2(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID : listExcel_T1
     * Method 설명 : 원부재료 엑셀 일괄 리포팅
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel_T1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listExcel_T1(model));
        
        return map;
    }    
    
    /**
     * Method ID : listExcel
     * Method 설명 : 택배발급이력 엑셀다운
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel_T2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	model.put("pageIndex", "1");
    	model.put("pageSize", "60000");
    	
    	map.put("LIST", dao.list_T2(model));
    	
    	return map;
    }    
   
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            model.put("inKey", "ORD01");
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    
    /**
     * Method ID : listZone_T1
     * Method 설명 : 임가공 조회(출고)
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listZone_T1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.listZone_T1(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : listZone_T2
     * Method 설명 : 임가공 조회(전체)
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listZone_T2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "20");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}        
    		map.put("LIST", dao.listZone_T2(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID : listSubZone_T1
     * Method 설명 : 임가공 구성품 조회(출고)
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSubZone_T1(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.listSubZone_T1(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
    	return map;
    }   
    
    /**
     * Method ID : listSubZone_T2
     * Method 설명 : 임가공 구성품 조회(전체) 
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSubZone_T2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "20");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}        
    		map.put("LIST", dao.listSubZone_T2(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    } 
}
