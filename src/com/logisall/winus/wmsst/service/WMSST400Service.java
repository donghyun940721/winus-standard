package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST400Service {
    public Map<String, Object> wmsst400list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> wmsst400list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listByOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> historyList(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> moveStockItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectHistoryInsert(Map<String, Object> model) throws Exception;
}
