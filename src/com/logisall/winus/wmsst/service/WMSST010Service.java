package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listV2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listV2_E3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listV2_detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> subListV2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listV3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listV3_detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listBlNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> listBlNo_detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> update_save(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectPopExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel3(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> subulManualComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> initSubul(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listApparel(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelApparel(Map<String, Object> model) throws Exception;
    public Map<String, Object> itemList(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel2E5(Map<String, Object> model) throws Exception;
	public Map<String, Object> listExcel014E1_2(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> stockInvalidItemEndDate(Map<String, Object> model) throws Exception;
	public Map<String, Object> listExcel_HanSung(Map<String, Object> model) throws Exception;
    
}
