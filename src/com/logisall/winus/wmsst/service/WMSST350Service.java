package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST350Service {
    public Map<String, Object> list_T1(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list_T2(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listSub_T1(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listSub_T2(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel_T1(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel_T2(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> listZone_T1(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listZone_T2(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listSubZone_T1(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listSubZone_T2(Map<String, Object> model) throws Exception;    
}
