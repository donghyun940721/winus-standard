package com.logisall.winus.wmsst.service;

import java.util.List;
import java.util.Map;

public interface WMSST603Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
}
