package com.logisall.winus.wmsst.service;

import java.util.List;
import java.util.Map;


public interface WMSST064Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> list3(Map<String, Object> model) throws Exception;
    public Map<String, Object> list4(Map<String, Object> model) throws Exception;
    public Map<String, Object> ordComplete(Map<String, Object> model) throws Exception;

}
