package com.logisall.winus.wmsst.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST120Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSST120Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST120Service")
	private WMSST120Service service;
	
	static final String[] COLUMN_NAME_WMSST120_PK = { //재고조사결과엑셀입력
		"CYCL_STOCK_ID"
		,"WORK_SEQ"
		,"LOC_CD"
		,"RITEM_CD"
		,"RITEM_NM"
		
		,"BEST_DATE"
		,"EXP_QTY"
		,"REAL_QTY"
		,"DIFF_REASON"
	};

	/*-
	 * Method ID : mn
	 * Method 설명 : 순환재고조사 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST120.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST120", service.selectBox(model));
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 순환재고조사 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST120/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : detail
	 * Method 설명 : 순환재고조사 상세 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST120/detail.action")
	public ModelAndView detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 순환재고조사  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST120/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 순환재고조사  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST120/updateSp.action")
	public ModelAndView updateSp(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.updateSp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("update.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 순환재고조사 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST120/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일자"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("순환재고조사방식"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상태"), "4", "4", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"   , "S"},
                                    {"LOC_CD"   , "S"},
                                    {"WORK_DT"  , "S"},
                                    {"CYCL_STOCK_TYPE_EXCEL"  , "S"},
                                    {"WORK_STAT"  , "S"}
                                   
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("재고조사");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 순환재고조사 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST120/excelSub.action")
	public void listExcelSub(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelSub(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownSub(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel(sub) :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDownSub
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDownSub(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getMessage("재고조사번호"), "0", "0", "0", "0", "200"},
				            		{MessageResolver.getMessage("재고조사SEQ"), "1", "1", "0", "0", "200"},
            						{MessageResolver.getMessage("로케이션"), "2", "2", "0", "0", "200"},
            						{MessageResolver.getMessage("상품코드"), "3", "3", "0", "0", "200"},
            						{MessageResolver.getMessage("상품"), "4", "4", "0", "0", "200"},
            						{MessageResolver.getMessage("유효기간"), "5", "5", "0", "0", "200"},
            					//	{MessageResolver.getMessage("EPC CODE"), "3", "3", "0", "0", "200"},
            						{MessageResolver.getMessage("현재고수량"), "6", "6", "0", "0", "100"},
            					//	{MessageResolver.getMessage("확인여부"), "6", "6", "0", "0", "200"},
            						{MessageResolver.getMessage("실사수량"), "7", "7", "0", "0", "200"},
            						{MessageResolver.getMessage("차이이유"), "8", "8", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"CYCL_STOCK_ID"   , "S"},
				            		{"WORK_SEQ"   , "S"},                
				            		{"LOC_CD"   , "S"},
                                    {"RITEM_CD"   , "S"},
                                    {"RITEM_NM"   , "S"},
                                  //  {"ITEM_EPC_CD"  , "N"},
                                    {"BEST_DATE"  , "S"},
                                    {"EXP_QTY"  , "N"},
                              //      {"WORK_STAT"  , "S"},
                                    {"REAL_QTY"  , "N"},
                                    {"DIFF_REASON"  , "S"}                                   
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("재고조사상세");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}  
    
    /**
	 * Method ID : wmsst120pop 
	 * Method 설명 : 재고조사결과엑셀입력 
	 * 작성자 : Summer
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST120pop.action")
	public ModelAndView WMSST120pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST120pop");
	}
	
	 /**
	 * Method ID : uploadExcel 
	 * Method 설명 : 재고조사결과엑셀입력 프로시저 
	 * 파일 읽기 작성자 : summer
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST120/uploadExcel.action")
	public ModelAndView uploadExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			String uploadType = (String) model.get("uploadType");
			List<Map> list = new ArrayList<Map>();
			list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSST120_PK, 0, startRow, 10000, 0);
			
			/*
			 * 엑셀 파일 검증
			 */
			for(int i =0 ; i<list.size() ; i++){
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i+2;
				
				String fieldCyclId = (String) hashMap.get(COLUMN_NAME_WMSST120_PK[0]);
				String fieldCyclSeq = (String) hashMap.get(COLUMN_NAME_WMSST120_PK[1]);
				
				if(fieldCyclId == null || fieldCyclId.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSST120_PK[0]+"재고조사번호는 필수 값입니다. 라인 : "+rowNum);
				}
				
				if(fieldCyclSeq == null || fieldCyclSeq.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSST120_PK[0]+"재고조사SEQ는 필수 값입니다. 라인 : "+rowNum);
				}
				
			}
			
			m = service.uploadExcel(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
}
