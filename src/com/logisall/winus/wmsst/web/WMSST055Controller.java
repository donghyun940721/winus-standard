package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST055Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST055Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST055Service")
	private WMSST055Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 일별재고 화면
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST055.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST055", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 일별재고 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST055/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	
	
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 일별재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST055/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownWMSST055(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	
	/*-
	 * Method ID : excel_locSum
	 * Method 설명 : 엑셀(일자별Loc합계)
	 * 작성자 : yhku
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST055/excel_locSum.action")
	public void listExcel_locSum(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel_locSum(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownWMSST053LOCSUM(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : mn1
	 * Method 설명 : 일별재고상세(new) 화면
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST057.action")
	public ModelAndView mn1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST057", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list1
	 * Method 설명 : 일별재고상세(new) 조회
	 * 작성자 : kijun11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST057/list.action")
	public ModelAndView list1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list057(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*-
	 * Method ID : list1
	 * Method 설명 : 일별재고상세(new) -> (기간별)
	 * 작성자 : Kijun11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST057/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*-
	 * Method ID : listExcel1
	 * Method 설명 : 일별재고 엑셀다운
	 * 작성자 : 이성중
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST057/excel.action")
	public void listExcel1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : dh1
	 * Method 설명 : 재고실사(확정)
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST058.action")
	public ModelAndView dh1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST058", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : dh_list1
	 * Method 설명 : 재고실사(확정)조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST058/list.action")
	public ModelAndView dh_list1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.wmsst058list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : dh_listExcel1
	 * Method 설명 : 재고실사(확정) 엑셀다운
	 * 작성자 : 이성중
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST058/excel.action")
	public void dh_listExcel1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : WMSST058Q1
	 * Method 설명 : 재고실사(확정) 엑셀다운
	 * 작성자 : 이성중
	 * @param model
	 */
	@RequestMapping("/WMSST058Q1.action")
	public ModelAndView WMSST058Q1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST058Q1");
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : 민환기
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주LOT번호"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("이동중수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품상태"), "7", "7", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("로케이션"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션 유형"), "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UOM"), "10", "10", "0", "0", "200"},                                   
                                   {MessageResolver.getMessage("재고중량"), "11", "11", "0", "0", "200"} 
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"SUBUL_DT"         , "S"},
                                    {"CUST_NM"          , "S"},
                                    {"CUST_LOT_NO"      , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    
                                    {"STOCK_QTY"        , "N"},
                                    {"NOW_MOVING_QTY"   , "N"},
                                    {"ITEM_STAT"        , "S"},
                                    
                                    {"LOC_CD"        	, "S"},
                                    {"LOC_TYPE"         , "S"},                                    

                                    {"UOM_NM"           , "S"},                                    
                                    {"STOCK_WEIGHT"     , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("일별재고_상세");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	} 
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : 민환기
     *
     * @param response
     * @param grs
     */
    protected void doExcelDownWMSST055(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주LOT번호"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("상품바코드"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("이동중수량"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품상태"), "8", "8", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("로케이션"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션 유형"), "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("입수"), "11", "11", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UOM"), "12", "12", "0", "0", "200"},                                   
                                   {MessageResolver.getMessage("재고중량"), "13", "13", "0", "0", "200"},     
                                   {MessageResolver.getMessage("유통기한"), "14", "14", "0", "0", "200"}     
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"SUBUL_DT"         , "S"},
                                    {"CUST_NM"          , "S"},
                                    {"CUST_LOT_NO"      , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    
                                    {"ITEM_BAR_CD"         , "S"},
                                    {"STOCK_QTY"        , "N"},
                                    {"NOW_MOVING_QTY"   , "N"},
                                    {"ITEM_STAT"        , "S"},
                                    
                                    {"LOC_CD"        	, "S"},
                                    {"LOC_TYPE"         , "S"},                                    
                                    {"UNIT_NM"          , "S"}, 
                                    
                                    {"UOM_NM"           , "S"},                                    
                                    {"STOCK_WEIGHT"     , "S"},
                                    {"ITEM_BEST_DATE_END"     , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("일별재고_상세");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}    
    
    
    
    
    /*-
     * Method ID : doExcelDownWMSST053LOCSUM
     * Method 설명 : 
     * 작성자 : yhku
     *
     * @param response
     * @param grs
     */
    protected void doExcelDownWMSST053LOCSUM(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션앞코드"), "1", "1", "0", "0", "400"},
                                   {MessageResolver.getMessage("로케이션수"), "2", "2", "0", "0", "200"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"SUBUL_DT"         , "S"},
            						{"LOC_CD_FIR"       , "S"},
                                    {"LOC_CNT"          , "N"},
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("일별재고_상세(일자별Loc합계)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}    
    
    /*-
	 * Method ID : 일별재고(수불장)
	 * Method 설명 : 일별재고(수불장) 화면
	 * 작성자 : KCR
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST059.action")
	public ModelAndView dh_subul_list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST059", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSST059/list.action")
	public ModelAndView dh_subul_list1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.wmsst059list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : 현재고조회(대화물류)
	 * Method 설명 : 현재고조회(대화물류) 화면
	 * 작성자 : kijun11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST066.action")
	public ModelAndView dh_subul_list2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST066", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	
	
}