package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST401Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST401Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST401Service")
	private WMSST401Service service;

	/*-
	 * Method ID 		: mn1
	 * Method 설명 	: 랙보충내역
	 * 작성자 			: KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST401.action")
	public ModelAndView mn1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST401", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID		: list
	 * Method 설명 	: 랙보충 내역 조회
	 * 작성자			: KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST401/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.wmsst401list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : wmsrp030
	 * Method 설명      : 피킹리스트발행 (피킹)
	 * 작성자                 : chSong 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSST401Q1.action")
	public ModelAndView wmsst401Q1(Map<String, Object> model) {
		return new ModelAndView("winus/wmsst/WMSST401Q1");
	}
	
	/*-
	 * Method ID 		: listExcel
	 * Method 설명 	: 랙보충 엑셀다운
	 * 작성자 			: KSJ
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST401/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : 민환기
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("상태"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일시"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("주문차수"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("FROM"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage(""), "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("TO"), "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("상품코드"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "8", "8", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("이동수량"), "10", "10", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UOM"), "11", "11", "0", "0", "200"},                                   
                                   {MessageResolver.getMessage("창고"), "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("사유(구분)"), "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("사유(상세)"), "14", "14", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"WORK_STAT"        , "S"},
                                    {"ITEM_WORK_TM"  , "S"},
                                    {"ORD_DEGREE"  		, "S"},
                                    {"FROM_LOC_CD"    , "S"},
                                    {"X"        				, "S"},
                                    {"TO_LOC_CD"         , "S"},
                                    
                                    {"ITEM_CODE"        , "S"},
                                    {"ITEM_NM"   		   , "S"},
                                    {"CUST_NM"           , "S"},
                                    
                                    {"STOCK_QTY"        	, "NR"},
                                    {"MOVE_QTY"         , "NR"},                                    

                                    {"UOM_NM"           , "S"},                                    
                                    {"WH_NM"     			, "S"},
                                    {"REASON_CD"     	, "S"},
                                    {"WORK_DESC"     	, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("랙보충내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

}