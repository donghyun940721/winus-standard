package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST110Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST110Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST110Service")
	private WMSST110Service service;


	/**
     * Method ID : mn
     * Method 설명 : 유효기간경고관리 화면
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSST110.action")
    public ModelAndView mn(Map<String, Object> model) {
        return new ModelAndView("winus/wmsst/WMSST110");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
    
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSST110/list.action")
    public ModelAndView list(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
        } 
        return mav;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSST110/detail.action")
    public ModelAndView detail(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.detail(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get detail result :", e);
			}
        } 
        return mav;
    }
    
	/*-
	 * Method ID : excel
	 * Method 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST110/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : DETAIL EXCEL
	 * Method 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST110/excel2.action")
	public void listExcel2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주명")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "3", "3", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("총수량")				, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("유통기한 만료 수량")		, "5", "5", "0", "0", "400"},
                                   {MessageResolver.getMessage("유효일수 30일 미만")		, "6", "6", "0", "0", "400"},
                                   {MessageResolver.getMessage("유효일수 60일 미만")		, "7", "7", "0", "0", "400"},
                                   {MessageResolver.getMessage("나머지")				, "8", "8", "0", "0", "200"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"           	, "S"},
                                    {"LOC_CD"           	, "S"},
                                    {"RITEM_CD"       		, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"TOT_SUM"          , "N"},
                                    {"OVER_1"          	, "N"},
                                    {"OVER_30"  		, "N"},
                                    {"OVER_60"          , "N"},
                                    {"OVER_ETC"         , "N"},
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("유통기한관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주명")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "3", "3", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("수량")		, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("유효기간만료일")		, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("유효일수")		, "7", "7", "0", "0", "200"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"           	, "S"},
                                    {"LOC_CD"           	, "S"},
                                    {"RITEM_CD"       		, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"STOCK_QTY"          	, "N"},
                                    {"UOM_NM"  				, "S"},
                                    {"ITEM_BEST_DATE_END"   , "S"},
                                    {"LAST_DAY"         	, "N"},
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("유통기한상세내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
