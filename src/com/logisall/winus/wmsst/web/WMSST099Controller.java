package com.logisall.winus.wmsst.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST099Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST099Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST099Service")
	private WMSST099Service service;


	/*-
	 * Method ID : WMSST099
	 * Method 설명 : 재고내역조회(SDI) 접근
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	 @RequestMapping("/WINUS/WMSST099.action")
	    public ModelAndView WMSST099(Map<String, Object> model){
	        return new ModelAndView("winus/wmsst/WMSST099");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
	    }
	
	/*-
	 * Method ID : list
	 * Method 설명 : 재고내역조회(SDI) 기본 조회 쿼리
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST099/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listTotal
	 * Method 설명 : 재고내역조회(SDI) 기본 조회 쿼리 total
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST099/listTotal.action")
	public ModelAndView listTotal(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listTotal(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	
	

}