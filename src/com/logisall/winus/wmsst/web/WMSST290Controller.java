package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST290Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST290Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST290Service")
	private WMSST290Service service;

	/*-
	 * Method ID : 
	 * Method 설명 : 재고관리 > 임가공통합작업화면
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST290.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST290", service.selectBox(model));
	}
	
	/*-
	 * Method ID : 
	 * Method 설명 : 재고관리 > 임가공통합작업조회
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST290/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : 
	 * Method 설명 : 재고관리 > 임가공통합작업조회
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST290/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.sublist(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSST290/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
			m.put("errCnt", "0");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	@RequestMapping("/WMSST290/dlt.action")
	public ModelAndView dlt(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.dlt(model);
			m.put("errCnt", "0");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
//	/*-
//	 * Method ID : 
//	 * Method 설명 : 
//	 * 작성자 : 
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSST010/pop.action")
//	public ModelAndView popmn(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsst/WMSST290");
//	}

}
