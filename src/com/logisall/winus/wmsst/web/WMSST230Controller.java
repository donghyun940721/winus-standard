package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST230Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST230Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST230Service")
	private WMSST230Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 현재고 조회 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST230.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST230", service.selectBox(model));
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 현재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST230/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}


	/*-
	 * Method ID : sublist
	 * Method 설명 : 현재고 서브 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST230/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get sub list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 현재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST230/hExcel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.hListExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
			                       {MessageResolver.getMessage("화주명"), "0", "0", "0", "1", "200"},
			                       {MessageResolver.getMessage("상품코드"), "1", "1", "0", "1", "200"},
			                       {MessageResolver.getMessage("상품명"), "2", "2", "0", "1", "200"},
			                       {MessageResolver.getMessage("UOM"), "3", "3", "0", "1", "200"},
                       
			            		   {model.get("LC_NM01").toString(), "4", "8", "0", "0", "200"},
			                       {model.get("LC_NM02").toString(), "9", "13", "0", "0", "200"},
			                       {model.get("LC_NM03").toString(), "14", "18", "0", "0", "200"},
			                       {model.get("LC_NM04").toString(), "19", "23", "0", "0", "200"},
			                       {model.get("LC_NM05").toString(), "24", "28", "0", "0", "200"},
			                       {model.get("LC_NM06").toString(), "29", "33", "0", "0", "200"},
			                       {model.get("LC_NM07").toString(), "34", "38", "0", "0", "200"},
			                       {model.get("LC_NM08").toString(), "39", "43", "0", "0", "200"},
			                       {model.get("LC_NM09").toString(), "44", "48", "0", "0", "200"},
			                       {model.get("LC_NM10").toString(), "49", "53", "0", "0", "200"},
                                   
			                       {MessageResolver.getMessage("총 재고")	   , "54", "54", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 정상재고")  , "55", "55", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 불량재고")  , "56", "56", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 입고예정")  , "57", "57", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 미배송")   , "58", "58", "0", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "4", "4", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "5", "5", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "6", "6", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "7", "7", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "8", "8", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "9", "9", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "10", "10", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "11", "11", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "12", "12", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "13", "13", "1", "1", "120"},
                                  
                                   {MessageResolver.getMessage("재고")	 , "14", "14", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "15", "15", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "16", "16", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "17", "17", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "18", "18", "1", "1", "120"},
                              
                                   {MessageResolver.getMessage("재고")	 , "19", "19", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "20", "20", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "21", "21", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "22", "22", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "23", "23", "1", "1", "120"},
                                  
                                   {MessageResolver.getMessage("재고")	 , "24", "24", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "25", "25", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "26", "26", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "27", "27", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "28", "28", "1", "1", "120"},
                                 
                                   {MessageResolver.getMessage("재고")	 , "29", "29", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "30", "30", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "31", "31", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "32", "32", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "33", "33", "1", "1", "120"},
                                
                                   {MessageResolver.getMessage("재고")	 , "34", "34", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "35", "35", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "36", "36", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "37", "37", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "38", "38", "1", "1", "120"},
                                 
                                   {MessageResolver.getMessage("재고")	 , "39", "39", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "40", "40", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "41", "41", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "42", "42", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "43", "43", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "44", "44", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "45", "45", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "46", "46", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "47", "47", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "48", "48", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "49", "49", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "50", "50", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "51", "51", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "52", "52", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "53", "53", "1", "1", "120"}
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"   , "S"},
                                    {"RITEM_CD"  , "S"},
                                    {"RITEM_NM"  , "S"},
                                    {"UOM_CD"  	 , "S"},
                                    
                                    {"STOCK_QTY_01"			, "N"},
                                    {"NORMAL_QTY_01"		, "N"},
                                    {"BAD_QTY_01"			, "N"},
                                    {"DLV_OUT_REQ_QTY_01"	, "N"},
                                    {"READY_IN_QTY_01"		, "N"},
                                    
                                    {"STOCK_QTY_02"			, "N"},
                                    {"NORMAL_QTY_02"		, "N"},
                                    {"BAD_QTY_02"			, "N"},
                                    {"DLV_OUT_REQ_QTY_02"	, "N"},
                                    {"READY_IN_QTY_02"		, "N"},
                                    
                                    {"STOCK_QTY_03"			, "N"},
                                    {"NORMAL_QTY_03"		, "N"},
                                    {"BAD_QTY_03"			, "N"},
                                    {"DLV_OUT_REQ_QTY_03"	, "N"},
                                    {"READY_IN_QTY_03"		, "N"},
                                    
                                    {"STOCK_QTY_04"			, "N"},
                                    {"NORMAL_QTY_04"		, "N"},
                                    {"BAD_QTY_04"			, "N"},
                                    {"DLV_OUT_REQ_QTY_04"	, "N"},
                                    {"READY_IN_QTY_04"		, "N"},
                                    
                                    {"STOCK_QTY_05"			, "N"},
                                    {"NORMAL_QTY_05"		, "N"},
                                    {"BAD_QTY_05"			, "N"},
                                    {"DLV_OUT_REQ_QTY_05"	, "N"},
                                    {"READY_IN_QTY_05"		, "N"},
                                    
                                    {"STOCK_QTY_06"			, "N"},
                                    {"NORMAL_QTY_06"		, "N"},
                                    {"BAD_QTY_06"			, "N"},
                                    {"DLV_OUT_REQ_QTY_06"	, "N"},
                                    {"READY_IN_QTY_06"		, "N"},
                                    
                                    {"STOCK_QTY_07"			, "N"},
                                    {"NORMAL_QTY_07"		, "N"},
                                    {"BAD_QTY_07"			, "N"},
                                    {"DLV_OUT_REQ_QTY_07"	, "N"},
                                    {"READY_IN_QTY_07"		, "N"},
                                    
                                    {"STOCK_QTY_08"			, "N"},
                                    {"NORMAL_QTY_08"		, "N"},
                                    {"BAD_QTY_08"			, "N"},
                                    {"DLV_OUT_REQ_QTY_08"	, "N"},
                                    {"READY_IN_QTY_08"		, "N"},
                                    
                                    {"STOCK_QTY_09"			, "N"},
                                    {"NORMAL_QTY_09"		, "N"},
                                    {"BAD_QTY_09"			, "N"},
                                    {"DLV_OUT_REQ_QTY_09"	, "N"},
                                    {"READY_IN_QTY_09"		, "N"},
                                    
                                    {"STOCK_QTY_10"			, "N"},
                                    {"NORMAL_QTY_10"		, "N"},
                                    {"BAD_QTY_10"			, "N"},
                                    {"DLV_OUT_REQ_QTY_10"	, "N"},
                                    {"READY_IN_QTY_10"		, "N"},
                                    
                                    {"ROW_SUM_1"			, "N"},
                                    {"ROW_SUM_2"			, "N"},
                                    {"ROW_SUM_3"			, "N"},
                                    {"ROW_SUM_4"			, "N"},
                                    {"ROW_SUM_5"			, "N"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText(MessageResolver.getMessage("현재고조회(센터별)"));
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : listExcel2
	 * Method 설명 : 현재고 상세 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST230/excel2.action")
	public void listExcel2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.dListExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	/*-
	 * Method ID : doExcelDown3
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품"), "2", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고수량"), "4", "4", "0", "0", "200"},
                                   {"UOM", "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("B/L번호"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급"), "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자"), "11", "11", "0", "0", "200"},
                                   {"STOCK_ID", "12", "12", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"          , "S"},
                                    {"LOC_NM"           , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    
                                    {"STOCK_QTY"  		, "N"},
                                    {"UOM_NM"           , "S"},
                                    {"REAL_PLT_QTY"		, "N"},
                                    {"BL_NO"         	, "S"},
                                    {"WH_NM"         	, "S"},
                                    
                                    {"CUST_LOT_NO"      , "S"},
                                    {"ITEM_CLASS"       , "S"},
                                    {"MAKE_DT"          , "S"},
                                    {"STOCK_ID"         , "S"},
                                   
                                   }; 
            
            //파일명
            String fileName ="WMSST";
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST230/sampleExcelDown.action")
	public void sampleExcelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getSampleExcelDown(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doSampleExcelDown2(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	protected void doSampleExcelDown2(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				String nName = MessageResolver.getText((String) model.get("Name_" + i)).replace("&lt;br&gt;", "").replace("&lt;/font&gt;&lt;br&gt;", "").replace("&lt;/font&gt;", "").replace("&lt;font color=&quot;#B1B1B1&quot;&gt;", "");
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{nName, i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..	
				if(i <= 4){
					
					valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
				}else{
					valueName[i] = new String[]{(String) model.get("Col_" + i), "N"};
				}
			}
			// 파일명
			String fileName = MessageResolver.getText("현재고조회(센터별처리현황)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
