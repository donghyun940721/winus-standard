package com.logisall.winus.wmsst.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST160Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSST160Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST160Service")
	private WMSST160Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 순환재고조사 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST160.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST160", service.selectBox(model));
	}
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 순환재고조사결과 엑셀템플릿입력
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST160/WMSST160pop1.action")
	public ModelAndView WMSST160pop1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST160pop1", service.selectBox(model));
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 순환재고조사ID 조회
	 * 작성자 : dongyeob
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST160/selectSBCYCLSTOCKID.action")
	public ModelAndView selectSBCYCLSTOCKID(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView",service.selectSBCYCLSTOCKID(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 순환재고조사 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST160/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 순환재고조사  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST160/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : save
	 * Method 설명 : 순환재고조사  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST160/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSST160/orderInsert.action")
    public ModelAndView orderInsert(Map<String, Object> model){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();
        try{
            m = service.orderInsert(model);
        }catch(Exception e){
            e.printStackTrace();
            m.put("MSG", MessageResolver.getMessage("save.error"));       
        }
        mav.addAllObjects(m);
        return mav;
    }
	
    /*-
     * Method ID : orderInsertBp
     * Method 설명 : 재고조사 일괄처리
     * 작성자 : sing09
     * @param request
     * @param response
     * @param model
     */
	@RequestMapping("/WMSST160/orderInsertBp.action")
	public ModelAndView orderInsertBp(Map<String, Object> model){
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m  = new HashMap<String, Object>();
	    try{
	        m = service.orderInsertBp(model);
	    }catch(Exception e){
	        e.printStackTrace();
	        m.put("MSG", MessageResolver.getMessage("save.error"));       
	    }
	    mav.addAllObjects(m);
	    return mav;
	}
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 순환재고조사 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST160/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            
            String[][] columnInfo = {
                    {"CUST_NM",MessageResolver.getMessage("화주"), "S"}
                    , {"CUST_LOT_NO",MessageResolver.getMessage("LOT번호"), "S"}
                    , {"LOC_CD",MessageResolver.getMessage("로케이션"), "S"}
                    , {"RITEM_CD",MessageResolver.getMessage("상품코드"), "S"}
                    , {"RITEM_NM",MessageResolver.getMessage("상품명"), "S"}
                    
                    , {"UOM_NM",MessageResolver.getMessage("대표UOM"), "S"}
                    , {"EXP_QTY",MessageResolver.getMessage("현재고수량"), "N"}
                    , {"REAL_QTY",MessageResolver.getMessage("실사수량"), "N"}
                    , {"DIFF_QTY",MessageResolver.getMessage("차이수량"), "N"}
                    , {"DIFF_REASON",MessageResolver.getMessage("차이이유"), "S"}
                    
                    , {"ERR_CD",MessageResolver.getMessage("오류코드"), "S"}
                    , {"BEST_DATE",MessageResolver.getMessage("유효기간"), "S"}
                    , {"WORK_STAT",MessageResolver.getMessage("확인여부"), "S"}
                    , {"WORK_DT",MessageResolver.getMessage("작업일자"), "S"}
                    , {"CYCL_STOCK_ID",MessageResolver.getMessage("재고조사번호"), "S"}
                    
                    , {"WORK_SEQ",MessageResolver.getMessage("순번"), "S"}
                    , {"UPD_NO",MessageResolver.getMessage("작업자"), "S"}
                    , {"UPD_DT",MessageResolver.getMessage("작업일자"), "S"}
                    , {"WORK_RESULT",MessageResolver.getMessage("증감"), "S"}
            };
            
            String[][] headerEx = new String[columnInfo.length][6];
            String[][] valueName = new String[columnInfo.length][2];
            
            for(int i = 0 ; i < columnInfo.length ; i++){
                headerEx[i][0] = columnInfo[i][1];
                headerEx[i][1] = Integer.toString(i);
                headerEx[i][2] = Integer.toString(i);
                headerEx[i][3] = "0";
                headerEx[i][4] = "0";
                headerEx[i][5] = "200";
                
                valueName[i][0] = columnInfo[i][0];
                valueName[i][1] = columnInfo[i][2];
            }

            
			// 파일명
			String fileName = MessageResolver.getText("재고조사결과");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download Excel file...", e);
			}
		}
	}
    
    /*-
	 * Method ID   : inExcelUploadTemplate
	 * Method 설명 : 엑셀업로드
	 * 작성자      : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST160/inExcelUploadTemplate.action")
	public ModelAndView inExcelUploadTemplate(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			model.put("WORK_IP", request.getRemoteAddr());

			//int startRow = Integer.parseInt((String) model.get("startRow"));
			int startRow = 1;
			
			List<Map> list = new ArrayList<Map>();
			// 화주	로케이션	상품코드	상품명	현재고수량	실사수량	차이이유	오류코드	유효기간	확인여부	작업일자	재고조사번호	순번
			String[] colNameArray = {
				"CUST_NM"
				, "LOC_CD"
				, "RITEM_CD"
				, "RITEM_NM"
				, "EXP_QTY"
				, "REAL_QTY"
				, "DIFF_REASON"
				, "ERR_CD"
				, "BEST_DATE"
				, "WORK_STAT"
				, "WORK_DT"
				, "CYCL_STOCK_ID"
				, "WORK_SEQ"
			};
			
			//list = ExcelReader.excelLimitRowRead(destination, colNameArray, 0, startRow, 1000000, 0);
			list = ExcelReader.excelLimitRowReadByHandler(destination, colNameArray, 0, startRow, 1000000, 0);
			
			m = service.inExcelUploadTemplate(model, list);

			destination.deleteOnExit();
			mav = new ModelAndView("jsonView", m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
}
