package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST300Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST300Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST300Service")
	private WMSST300Service service;

	/*-
	 * Method ID : 
	 * Method 설명 : 재고관리 > 임가공통합작업화면
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST300.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST300", service.selectBox(model));
	}

}
