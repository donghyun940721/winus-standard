package com.logisall.winus.wmsst.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS090Service;
import com.logisall.winus.wmsst.service.WMSST064Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSST064Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST064Service")
	private WMSST064Service service;
	
	@Resource(name = "WMSMS090Service")
	private WMSMS090Service service2;

	/*-
	 * Method ID : WMSST064
	 * Method 설명 : 입고검수조회 화면 접근
	 * 작성자 : kijun11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST064.action")
	public ModelAndView WMSST064(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST064", service2.selectData(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : youm 거래처별 일보 조회
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST064/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list2
	 * Method 설명 : rigid 거래처 별 일보 조회
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST064/list2.action")
	public ModelAndView list2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list3
	 * Method 설명 : youm 거래처별 통합 일보 조회
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST064/list3.action")
	public ModelAndView list3(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list4
	 * Method 설명 : rigid 거래처 별 통합 일보 조회
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST064/list4.action")
	public ModelAndView list4(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list4(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID   : ordComplete
	 * Method 설명 : 검수내역 입고확정
	 * 작성자      : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST064/ordComplete.action")
	public ModelAndView ordComplete(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = new HashMap<String,Object>();
		try {
			m = service.ordComplete(model);
			mav = new ModelAndView("jsonView", m);

			} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	

}