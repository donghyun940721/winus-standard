package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST010Service")
	private WMSST010Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 현재고 조회 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST010.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST010", service.selectBox(model));
	}
	
	/*-
	 * Method ID : mnV2
	 * Method 설명 : 현재고 조회 화면 V2.0 (Spread JS 적용)
	 * 작성자 : Seongjun Kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST012.action")
	public ModelAndView mnV2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST012", service.selectBox(model));
	}
	
	/*-
	 * Method ID : mnApparel
	 * Method 설명 : 현재고 조회 화면(APPAREL)
	 * 작성자 : Seongjun Kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST013.action")
	public ModelAndView mnApparel(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST013", service.selectBox(model));
	}
	
	/*-
	 * Method ID : mnApparel
	 * Method 설명 : 현재고 조회 화면(APPAREL)
	 * 작성자 : Seongjun Kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST014.action")
	public ModelAndView mnApparelNew(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST014", service.selectBox(model));
	}
	
	/*-
	 * Method ID : blno
	 * Method 설명 : blno 현재고 조회 화면
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST015.action")
	public ModelAndView WMSST015(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST015", service.selectBox(model));
	}
	
	


	/*-
	 * Method ID : popmn
	 * Method 설명 : 세트상품 조회 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/pop.action")
	public ModelAndView popmn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST010E2");
	}

	/*-
	 * Method ID : poplist
	 * Method 설명 : 세트상품 조회 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/poplist.action")
	public ModelAndView poplist(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : popdetail
	 * Method 설명 : 세트상품 상세조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/popdetail.action")
	public ModelAndView popdetail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detailPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : list
	 * Method 설명 : 현재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : itemList
	 * Method 설명 : 상품별 현재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/itemList.action")
	public ModelAndView itemList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.itemList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : sublist
	 * Method 설명 : 현재고 서브 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get sub list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listV2
	 * Method 설명 : 현재고 조회V2
	 * 작성자 : seongjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listV2.action")
	public ModelAndView listV2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listV2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : listV2_E3
	 * Method 설명 : listV2_E3
	 * 작성자 : KIJUN11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listV2_E3.action")
	public ModelAndView listV2_E3(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listV2_E3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listV2_datail
	 * Method 설명 : 현재고 detail 조회 V2
	 * 작성자 : seongjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listV2_detail.action")
	public ModelAndView listV2_detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listV2_detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : listV2_datail
	 * Method 설명 : 일별재고(수불장) 현재고 detail 조회 V2
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/subListV2.action")
	public ModelAndView subListV2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.subListV2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listV3
	 * Method 설명 : 현재고 조회V3
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listV3.action")
	public ModelAndView listV3(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listV3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : listV3_datail
	 * Method 설명 : 현재고 detail 조회 V3
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listV3_detail.action")
	public ModelAndView listV3_detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listV3_detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	

	/*-
	 * Method ID : listBlNo
	 * Method 설명 : BLNo 현재고 조회
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listBlNo.action")
	public ModelAndView listBlNo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listBlNo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : listBlNo_datail
	 * Method 설명 : BlNo 현재고 detail 조회
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listBlNo_detail.action")
	public ModelAndView listBlNo_detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listBlNo_detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	
	
	

	/*-
	 * Method ID : save
	 * Method 설명 : 현재고  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST010/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	

	/*-
	 * Method ID : save
	 * Method 설명 : 현재고조회(대화물류) 비고  저장
	 * 작성자 : kijun11
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST010/update_save.action")
	public ModelAndView update_save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.update_save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID : listExcel
	 * Method �꽕紐� : �쁽�옱怨� �뿊���떎�슫
	 * �옉�꽦�옄 : 湲곕뱶�삩
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST010/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				String svcNo = String.valueOf(model.get("SS_SVC_NO"));
				// 대화물류
				if(svcNo.equals("0000003541") || svcNo.equals("0000003721") || svcNo.equals("0000003722") || svcNo.equals("0000003720")){
					this.doExcelDown_DH(response, grs);
				}else if(svcNo.equals("0000003620")){
					// 비즈컨설팅
					this.doExcelDown_Biz(response, grs);
				} else{
					this.doExcelDown(response, grs);
				}
				
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품바코드")	, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("전일재고")		, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고")		, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("일반출고")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("크로스도킹")	, "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("이벤트출고")	, "10", "10", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("반품출고")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("정상재고")		, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("BOX수량")		, "13", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")		, "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고")		, "15", "15", "0", "0", "200"},

                                   {MessageResolver.getMessage("출고가용재고")	, "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("AS재고")		, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("격리수량")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("금액")		, "19", "19", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("중량")		, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "21", "21", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("적정재고")		, "22", "22", "0", "0", "200"},
                                   {MessageResolver.getMessage("비고")		, "23", "23", "0", "0", "200"},
                                   {MessageResolver.getMessage("브렌드")		, "24", "24", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주상품코드")	, "25", "25", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "26", "26", "0", "0", "200"},
                                   {MessageResolver.getMessage("CBM")		, "27", "27", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"ITEM_BAR_CD"          , "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    {"IN_QTY"            	, "N"},
                                    {"OUT_QTY"           	, "N"},
                                    {"CROSS_QTY"         	, "N"},
                                    {"EVENT_QTY"         	, "N"},
                                    
                                    {"RETURN_QTY"        	, "N"},
                                    {"STOCK_QTY"        	, "N"},
                                    {"BOX_QTY"           	, "N"},
                                    {"PLT_QTY"           	, "N"},
                                    {"BAD_QTY"           	, "N"},

                                    {"OUT_ABLE_QTY"         , "N"},
                                    {"AS_QTY"            	, "N"},
                                    {"ISO_QTY"            	, "N"},
                                    {"TOTAL_PRICE"        	, "N"},
                                    {"STOCK_WEIGHT"         , "N"},
                                    {"UOM_NM"            	, "S"},
                                    
                                    {"PROP_QTY"             , "N"},
                                    {"REMARK"               , "S"},
                                    {"MAKER_NM"             , "S"},
                                    {"CUST_ITEM_CD"         , "S"},
                                    {"ITEM_SIZE"            , "S"},
                                    {"CBM_QTY"            	, "N"}
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	protected void doExcelDown_DH(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
								   {MessageResolver.getMessage("박스바코드")	, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입수")		, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("P/L")		, "7", "7", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("전일재고")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고")		, "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("일반출고")		, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("크로스도킹")	, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("이벤트출고")	, "12", "12", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("반품출고")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("정상재고")		, "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("BOX수량")		, "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")		, "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고")		, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("전체재고")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("AS재고")		, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("격리수량")		, "21", "21", "0", "0", "200"},
                                   {MessageResolver.getMessage("중량")		, "22", "22", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "23", "23", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("적정재고")		, "24", "24", "0", "0", "200"},
                                   {MessageResolver.getMessage("비고")		, "25", "25", "0", "0", "200"},
                                   {MessageResolver.getMessage("브렌드")		, "26", "26", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주상품코드")	, "27", "27", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "28", "28", "0", "0", "200"},
                                   {MessageResolver.getMessage("CBM")		, "29", "29", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
									{"BOX_BAR_CD"          	, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"UNIT_NM"  	, "N"},
                                    {"PLT_UOM"  	, "N"},
                                    
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    {"IN_QTY"            	, "N"},
                                    {"OUT_QTY"           	, "N"},
                                    {"CROSS_QTY"         	, "N"},
                                    {"EVENT_QTY"         	, "N"},
                                    
                                    {"RETURN_QTY"        	, "N"},
                                    {"STOCK_QTY"        	, "N"},
                                    {"BOX_QTY"           	, "N"},
                                    {"PLT_QTY"           	, "N"},
                                    {"BAD_QTY"           	, "N"},
                                    {"STOCK_TOTAL_QTY"      , "N"},	
                                    {"OUT_ABLE_QTY"         , "N"},
                                    {"AS_QTY"            	, "N"},
                                    {"ISO_QTY"            	, "N"},
                                    {"STOCK_WEIGHT"         , "N"},
                                    {"UOM_NM"            	, "S"},
                                    
                                    {"PROP_QTY"             , "N"},
                                    {"REMARK"               , "S"},
                                    {"MAKER_NM"             , "S"},
                                    {"CUST_ITEM_CD"         , "S"},
                                    {"ITEM_SIZE"            , "S"},
                                    {"CBM_QTY"            	, "N"}
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	protected void doApparelExcelDown_ERP(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("ERP창고코드")	, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("ERP창고코드명"), "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "6", "6", "0", "0", "200"},

                                   {MessageResolver.getMessage("브렌드")		, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주상품코드")	, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("전일재고")		, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("일반출고")		, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("반품출고")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("정상재고")		, "14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("불량재고")		, "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("전체재고")		, "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("입수")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("EA수량")		, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("BOX수량")		, "21", "21", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")		, "22", "22", "0", "0", "200"},
                                   {MessageResolver.getMessage("BAG수량")		, "23", "23", "0", "0", "200"},
                                   {MessageResolver.getMessage("CBM")		, "24", "24", "0", "0", "200"},
                                   {MessageResolver.getMessage("중량")		, "25", "25", "0", "0", "200"},
                                   {MessageResolver.getMessage("AS재고")		, "26", "26", "0", "0", "200"},
                                   {MessageResolver.getMessage("격리수량")		, "27", "27", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("적정재고")		, "28", "28", "0", "0", "200"},
                                   {MessageResolver.getMessage("비고")		, "29", "29", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"ERP_WH_CD"       		, "S"},
                                    {"ERP_WH_CD_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"MAKER_NM"          	, "S"},
                                    {"CUST_ITEM_CD"         , "S"},
                                    {"ITEM_SIZE"          	, "S"},
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    {"IN_QTY"            	, "N"},
                                    {"OUT_QTY"           	, "N"},
                                    {"RETURN_QTY"         	, "N"},
                                    {"STOCK_QTY"         	, "N"},
                                    
                                    {"BAD_QTY"           	, "N"},
                                    {"STOCK_TOTAL_QTY"      , "N"},
                                    {"OUT_ABLE_QTY"         , "N"},
                                    {"UNIT_NM"            	, "S"},
                                    {"UOM_NM"            	, "S"},
                                    {"EA_QTY"            	, "N"},
                                    {"BOX_QTY"           	, "N"},
                                    {"PLT_QTY"           	, "N"},
                                    {"BAG_QTY"            	, "N"},
                                    {"CBM_QTY"            	, "N"},
                                    {"STOCK_WEIGHT"         , "N"},
                                    {"AS_QTY"            	, "N"},
                                    {"ISO_QTY"            	, "N"},
                                    
                                    {"PROP_QTY"             , "N"},
                                    {"REMARK"               , "S"}
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회_ERP");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
//			ConstantIF.FILE_ATTACH_PATH => E드라이브가 없는데 E드라이브로 떨구려고해서 에러가남.
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	protected void doExcelDown_Biz(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("박스바코드")	, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("스타일")		, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("컬러")		    , "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("전일재고")		, "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입고")		    , "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고")		    , "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("정상재고")		, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "14", "14", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"BOX_BAR_CD"          	, "S"},
                                    {"CUST_LEGACY_ITEM_CD"  , "S"},
                                    {"COLOR"          		, "S"},
                                    {"ITEM_SIZE"          	, "S"},
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    
                                    {"IN_QTY"            	, "N"},
                                    {"OUT_QTY"           	, "N"},
                                    {"STOCK_QTY"        	, "N"},
                                    {"BAD_QTY"           	, "N"},
                                    {"OUT_ABLE_QTY"         , "N"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : popExcel
	 * Method 설명 : 세트팝업 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST010/popexcel.action")
	public void popExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.selectPopExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pop excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown2
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주명"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("코드"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM"), "4", "4", "0", "0", "200"},
                                   {"/", "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("코드"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "7", "7", "0", "0", "200"},
                                   {"QTY", "8", "8", "0", "0", "200"},
                                   {"UOM", "9", "9", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"           , "S"},
                                    {"RITEM_CD"          , "S"},
                                    {"RITEM_NM"          , "S"},
                                    {"STOCK_QTY"         , "N"},
                                    {"UOM_NM"            , "S"},
                                    
                                    {"M"                 , "S"},
                                    {"PART_RITEM_CD"     , "S"},
                                    {"PART_RITEM_NM"     , "S"},
                                    {"PART_QTY"          , "N"},
                                    {"PART_UOM_NM"       , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID : listExcel2
	 * Method 설명 : 현재고 상세 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST010/excel2.action")
	public void listExcel2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownSubList(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : listExcel3
	 * Method 설명 : 현재고 상세 전체엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST010/excel3.action")
	public void listExcel3(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel3(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown3(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : listExcel2
	 * Method 설명 : 현재고 상세(New) 전체엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST014/excel014E1_2.action")
	public void listExcel014E1_2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String svcNo = String.valueOf(model.get("SS_SVC_NO"));
			
			if(svcNo.equals("0000004201")){
				map = service.listExcel_HanSung(model);
			}else{
				map = service.listExcel014E1_2(model);
			}

			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				if(svcNo.equals("0000004201")){
					// 비즈컨설팅
					this.doExcelDown_HanSung(response, grs);
				}else{
					this.doExcelDown014E1_2(response, grs);
				}
				
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	
	/*-
	 * Method ID : doExcelDownSubList
	 * Method 설명 : 양식 꼬임으로 따로 뺌
	 * 작성자 : KSJ
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDownSubList(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자")	, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고일자")	, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")	, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")	, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")	, "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량")		, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고예정수량")	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT계산수량")	, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")		, "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("B/L번호")	, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고")	, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호")	, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급")	, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자"), "14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("유효기간만료일"), "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("무게"), "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("UNIT번호"), "17", "17", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"			, "S"},
                                    {"IN_DT"			, "S"},
                                    {"LOC_NM"			, "S"},
                                    {"RITEM_CD"			, "S"},
                                    {"RITEM_NM"			, "S"},
                                    
                                    {"STOCK_QTY"		, "N"},
                                    {"OUT_EXP_QTY"     , "N"},
                                    {"UOM_NM"			, "S"},
                                    {"CAL_PLT_QTY"		, "N"},
                                    {"REAL_PLT_QTY"		, "N"},
                                    
                                    {"BL_NO"			, "S"},
                                    {"WH_NM"			, "S"},
                                    {"CUST_LOT_NO"		, "S"},
                                    {"ITEM_CLASS"		, "S"},
                                    {"MAKE_DT"			, "S"},
                                    
                                    {"ITEM_BEST_DATE_END"		, "S"},
                                    {"STOCK_WEIGHT"		, "N"},
                                    {"UNIT_NO"		, "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("현재고조회상세");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }

	/*-
	 * Method ID : doExcelDown3
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown3(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자")	, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고일자")	, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")	, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")	, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")	, "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량")		, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("불용재고")  	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT계산수량")	, "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("PLT수량")	, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("B/L번호")	, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고")	, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호")	, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급")	, "14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("제조일자"), 	"15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("유효기간만료일"), "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("무게"), 		"17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("UNIT번호"), 	"18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("입수"), 		"19", "19", "0", "0", "200"},
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"			, "S"},
                                    {"IN_DT"			, "S"},
                                    {"LOC_NM"			, "S"},
                                    {"RITEM_CD"			, "S"},
                                    {"RITEM_NM"			, "S"},
                                    
                                    {"STOCK_QTY"		, "N"},
                                    {"NOT_ABLE_QTY"     , "N"},
                                    {"OUT_ABLE_QTY"     , "N"},
                                    {"UOM_NM"			, "S"},
                                    {"CAL_PLT_QTY"		, "N"},
                                    
                                    {"REAL_PLT_QTY"		, "N"},
                                    {"BL_NO"			, "S"},
                                    {"WH_NM"			, "S"},
                                    {"CUST_LOT_NO"		, "S"},
                                    {"ITEM_CLASS"		, "S"},
                                   
                                    {"MAKE_DT"			, "S"},
                                    {"ITEM_BEST_DATE_END"		, "S"},
                                    {"STOCK_WEIGHT"		, "N"},
                                    {"UNIT_NO"		, "S"},
                                    {"UNIT_NM"      , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("현재고조회상세");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
	/*-
	 * Method ID : doExcelDown014E1_2
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown014E1_2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자")		, "0",  "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고일자")		, "1",  "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")		, "2",  "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션타입")	, "3",  "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("품온구분")		, "4",  "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "5",  "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "6",  "6", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량")		, "7",  "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("불용재고")  	, "8",  "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "9",  "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고예정수량")	, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")		, "12", "12", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("B/L번호")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고")		, "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호")		, "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급")		, "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자")		, "17", "17", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("유효기간만료일")	, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("무게")		, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("UNIT번호")	, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("입수")		, "21", "21", "0", "0", "200"}
                                   
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"			, "S"},
                                    {"IN_DT"			, "S"},
                                    {"LOC_NM"			, "S"},
                                    {"LOC_TYPE"			, "S"},
                                    {"TEMP_TYPE_NAME_E1D"       , "S"},
                                    {"RITEM_CD"			, "S"},
                                    {"RITEM_NM"			, "S"},
                                    
                                    {"STOCK_QTY"		, "N"},
                                    {"NOT_ABLE_QTY"     , "N"},
                                    {"OUT_ABLE_QTY"     , "N"},
                                    {"OUT_EXP_QTY"     	, "N"},
                                    {"UOM_NM"			, "S"},
                                    {"REAL_PLT_QTY"		, "N"},
                                    
                                    {"BL_NO"			, "S"},
                                    {"WH_NM"			, "S"},
                                    {"CUST_LOT_NO"		, "S"},
                                    {"ITEM_CLASS"		, "S"},
                                    {"MAKE_DT"			, "S"},
                                    
                                    {"ITEM_BEST_DATE_END"		, "S"},
                                    {"STOCK_WEIGHT"				, "N"},
                                    {"UNIT_NO"					, "S"},
                                    {"UNIT_NM"					, "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("현재고조회상세(New)");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
	/*-
	 * Method ID : doExcelDown014E1_2
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown_HanSung(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자")		, "0",  "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고일자")		, "1",  "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")		, "2",  "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션타입")	, "3",  "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("품온구분")		, "4",  "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "5",  "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "6",  "6", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량")			, "7",  "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("불용재고")  		, "8",  "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")		, "9",  "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고예정수량")		, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("EA낱개수량(BOX)")	, "11", "11", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("출하최소유효기간")	, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")			, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")		, "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("B/L번호")		, "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고")		, "16", "16", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("LOT번호")		, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자")		, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("유효기간만료일")	, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("무게")		, "21", "21", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UNIT번호")	, "22", "22", "0", "0", "200"},
                                   {MessageResolver.getMessage("입수")		, "23", "23", "0", "0", "200"}
                                   
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"			, "S"},
                                    {"IN_DT"			, "S"},
                                    {"LOC_NM"			, "S"},
                                    {"LOC_TYPE"			, "S"},
                                    {"TEMP_TYPE_NAME_E1D"       , "S"},
                                    {"RITEM_CD"			, "S"},
                                    {"RITEM_NM"			, "S"},
                                    
                                    {"STOCK_QTY"		, "N"},
                                    {"NOT_ABLE_QTY"     , "N"},
                                    {"OUT_ABLE_QTY"     , "N"},
                                    {"OUT_EXP_QTY"     	, "N"},
                                    {"BOX_QTY"     		, "N"},
                                    {"OUT_BEST_DATE_NUM"     	, "N"},
                                    
                                    
                                    {"UOM_NM"			, "S"},
                                    {"REAL_PLT_QTY"		, "N"},
                                    
                                    {"BL_NO"			, "S"},
                                    {"WH_NM"			, "S"},
                                    {"CUST_LOT_NO"		, "S"},
                                    {"ITEM_CLASS"		, "S"},
                                    {"MAKE_DT"			, "S"},
                                    
                                    {"ITEM_BEST_DATE_END"		, "S"},
                                    {"STOCK_WEIGHT"				, "N"},
                                    {"UNIT_NO"					, "S"},
                                    {"UNIT_NM"					, "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("현재고조회상세(New)");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
	/*-
	 * Method ID	: initSubul
	 * Method 설명	: 현재고  당일수불재고초기화
	 * 작성자			: 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST010/initSubul.action")
	public ModelAndView initSubul(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.initSubul(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: subulManualComp
	 * Method 설명	: 현재고  수불재생성
	 * 작성자			: 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST010/subulManualComp.action")
	public ModelAndView subulManualComp(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.subulManualComp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 현재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST010/excelByApparel.action")
	public void listExcelByApparel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				if(model.get(ConstantIF.SS_SVC_NO).equals("0000003780")){//이노서브 엑셀 양식 고정
					this.doExcelDownByApparelInosub(response, grs);
				}else{
					this.doExcelDownByApparel(response, grs);
				}
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDownByApparel(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품바코드")	, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("브랜드")		, "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("화주상품코드")	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("색상")		, "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("전일재고")		, "10", "10", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입고")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고")		, "12", "12", "0", "0", "200"},

                                   {MessageResolver.getMessage("정상재고")     , "13", "13", "0", "0", "200"},

                                   {MessageResolver.getMessage("전체재고")     , "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고예정수량")  , "16", "16", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입고대기")		, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("온라인반품대기")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("오프라인반품대기")	, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("결품")		, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("주문취소")		, "21", "21", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("불량재고")		, "22", "22", "0", "0", "200"},
                                   {MessageResolver.getMessage("격리재고")		, "23", "23", "0", "0", "200"},
                                   {MessageResolver.getMessage("B급재고")		, "24", "24", "0", "0", "200"},
                                   {MessageResolver.getMessage("CS재고")		, "25", "25", "0", "0", "200"},
                                   {MessageResolver.getMessage("AS재고")		, "26", "26", "0", "0", "200"},
                                   {MessageResolver.getMessage("임가공랙재고")	, "27", "27", "0", "0", "200"},
                                   {MessageResolver.getMessage("피킹수량")		, "28", "28", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("비고")		, "29", "29", "0", "0", "200"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"ITEM_BAR_CD"          , "S"},
                                    {"MAKER_NM"          	, "S"},
                                    
                                    {"CUST_ITEM_CD"  		, "S"},
                                    {"RITEM_NM"            	, "S"},
                                    {"ITEM_SIZE"           	, "S"},
                                    {"COLOR"         		, "S"},
                                    {"BEFORE_STOCK_QTY_053" , "N"},
                                    
                                    {"IN_QTY"        		, "N"},
                                    {"OUT_QTY"        		, "N"},
                                    {"STOCK_QTY"            , "N"},

                                    {"STOCK_TOTAL_QTY"      , "N"},
                                    {"OUT_ABLE_QTY"         , "N"},
                                    {"OUT_EXP_QTY"          , "N"},
                                    
                                    {"IN_READY_QTY"      	, "N"},
                                    {"ON_RTN_READY_QTY"     , "N"},
                                    {"OFF_RTN_READY_QTY"    , "N"},
                                    {"MISSING_QTY"          , "N"},
                                    {"ORD_CANCEL_QTY"       , "N"},
                                    
                                    {"BAD_QTY"           	, "N"},
                                    {"ISO_QTY"           	, "N"},
                                    {"B_QTY" 	 		    , "N"},
                                    {"CS_QTY"  	  			, "N"},
                                    {"AS_QTY"  	  			, "N"},
                                    {"CHANGE_RDY_QTY"     	, "N"},
                                    {"PICKING_QTY"     		, "N"},
                                    
                                    {"REMARK"            	, "S"},
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회(APPAREL)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	protected void doExcelDownByApparelInosub(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품바코드")	, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("브랜드")		, "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("화주상품코드")	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("색상")		, "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("전일재고")		, "10", "10", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입고")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고")		, "12", "12", "0", "0", "200"},

                                   {MessageResolver.getMessage("정상재고")     , "13", "13", "0", "0", "200"},

                                   {MessageResolver.getMessage("전체재고")     , "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고예정수량")  , "16", "16", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입고대기")		, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("격리재고")		, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("B급재고")		, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("CS재고")		, "21", "21", "0", "0", "200"},
                                   {MessageResolver.getMessage("AS재고")		, "22", "22", "0", "0", "200"},
                                   {MessageResolver.getMessage("임가공랙재고")	, "23", "23", "0", "0", "200"},
                                   {MessageResolver.getMessage("피킹수량")		, "24", "24", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("비고")		, "25", "25", "0", "0", "200"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"ITEM_BAR_CD"          , "S"},
                                    {"MAKER_NM"          	, "S"},
                                    
                                    {"CUST_ITEM_CD"  		, "S"},
                                    {"RITEM_NM"            	, "S"},
                                    {"ITEM_SIZE"           	, "S"},
                                    {"COLOR"         		, "S"},
                                    {"BEFORE_STOCK_QTY_053" , "N"},
                                    
                                    {"IN_QTY"        		, "N"},
                                    {"OUT_QTY"        		, "N"},
                                    {"STOCK_QTY"            , "N"},

                                    {"STOCK_TOTAL_QTY"      , "N"},
                                    {"OUT_ABLE_QTY"         , "N"},
                                    {"OUT_EXP_QTY"          , "N"},
                                    
                                    {"IN_READY_QTY"      	, "N"},
                                    {"BAD_QTY"           	, "N"},
                                    {"ISO_QTY"           	, "N"},
                                    {"B_QTY" 	 		    , "N"},
                                    {"CS_QTY"  	  			, "N"},
                                    {"AS_QTY"  	  			, "N"},
                                    {"CHANGE_RDY_QTY"     	, "N"},
                                    {"PICKING_QTY"     		, "N"},
                                    
                                    {"REMARK"            	, "S"},
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회(APPAREL)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : listApparel
	 * Method 설명 : 현재고조회APPAREL
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listApparel.action")
	public ModelAndView listApparel(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listApparel(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listExcel(APPAREL)
	 * Method 설명 : 일별재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST051/excelApparel.action")
	public void excelApparel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelApparel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownApparel(response, grs);
			}	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownApparel(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
        	String[][] headerEx = {
					                    {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
					                    {MessageResolver.getMessage("화주"), "1", "1", "0", "0", "200"},
					                    {MessageResolver.getMessage("상품코드"), "2", "2", "0", "0", "200"},
					                    {MessageResolver.getMessage("상품명"), "3", "3", "0", "0", "200"},
					                    {MessageResolver.getMessage("입고량"), "4", "4", "0", "0", "200"},
					                    
					                    {MessageResolver.getMessage("일반출고"), "5", "5", "0", "0", "200"},
					                    {MessageResolver.getMessage("이벤트출고"), "6", "6", "0", "0", "200"},
					                    {MessageResolver.getMessage("재고량"), "7", "7", "0", "0", "200"},
					                    {"UOM", "8", "8", "0", "0", "200"},
					                    {MessageResolver.getMessage("현재고"), "9", "9", "0", "0", "200"},
					                    
					                    {MessageResolver.getMessage("현불량"), "10", "10", "0", "0", "200"},
					                    {MessageResolver.getMessage("AS재고"), "11", "11", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					            		{"SUBUL_DT"          , "S"},
					                    {"CUST_NM"           , "S"},
					                    {"RITEM_CD"          , "S"},
					                    {"RITEM_NM"          , "S"},
					                    {"IN_QTY"            , "N"},
					                    
					                    {"OUT_QTY"           , "N"},
					                    {"EVENT_QTY"         , "N"},
					                    {"STOCK_QTY"         , "N"},
					                    {"UOM_NM"            , "S"},
					                    {"GOOD_QTY"          , "N"},
					
					                    {"BAD_QTY"           , "N"},
					                    {"AS_QTY"            , "N"}
					                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("일별재고조회(APPAREL)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	
	/*-
	 * Method ID : listE5
	 * Method 설명 : 현재고 조회(APPAREL_ERP)
	 * 작성자 : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/listE5.action")
	public ModelAndView listE5(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE5(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : sublistE5
	 * Method 설명 : 현재고(APPAREL_ERP) 서브 조회
	 * 작성자 : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST010/sublistE5.action")
	public ModelAndView sublistE5(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSubE5(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get sub list :", e);
			}
		}
		return mav;
	}
	
	/*-
	  * Method ID : listExcelE5
	 * Method 설명 : 현재고(APPAREL_ERP) 엑셀다운
	 * 작성자 : KSJ
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST010/excelE5.action")
	public void listExcelE5(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelE5(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doApparelExcelDown_ERP(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : listExcel2E5
	 * Method 설명 : 현재고(APPAREL_ERP) 상세 엑셀다운
	 * 작성자 : KSJ
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST010/excel2E5.action")
	public void listExcel2E5(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel2E5(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2E5(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
     * Method ID : doExcelDown2E5
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown2E5(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자")  , "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고일자")  , "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")  , "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")  , "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")   , "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량")  , "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고예정수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")   , "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량") , "8", "8", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("B/L번호") , "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고")    , "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호") , "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급")  , "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자"), "13", "13", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("유효기간만료일"), "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("무게"), "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("UNIT번호"), "16", "16", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"          , "S"},
                                    {"IN_DT"            , "S"},
                                    {"LOC_NM"           , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    
                                    {"STOCK_QTY"        , "N"},
                                    {"OUT_EXP_QTY"     , "N"},
                                    {"UOM_NM"           , "S"},
                                    {"REAL_PLT_QTY"     , "N"},
                                    
                                    {"BL_NO"            , "S"},
                                    {"WH_NM"            , "S"},
                                    {"CUST_LOT_NO"      , "S"},
                                    {"ITEM_CLASS"       , "S"},
                                    {"MAKE_DT"          , "S"},
                                    
                                    {"ITEM_BEST_DATE_END"       , "S"},
                                    {"STOCK_WEIGHT"     , "N"},
                                    {"UNIT_NO"      , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("현재고조회상세");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
                log.error("fail download Excel file...", e);
            }
        }
    }
    
    /*-
	 * Method ID    : stockInvalidItemEndDate
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSST010/stockInvalidItemEndDate.action")
	public ModelAndView stockInvalidItemEndDate(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.stockInvalidItemEndDate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
