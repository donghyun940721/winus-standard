package com.logisall.winus.wmsst.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST010Service;
import com.logisall.winus.wmsst.service.WMSST019Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST019Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST019Service")
	private WMSST019Service service;
	
	@Resource(name = "WMSST010Service")
	private WMSST010Service service010;

	/**
	 * Method ID 	: 
	 * Method 설명 	: 현재고단가조회 화면 진입
	 * 작성자 			: SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST019.action")
	public ModelAndView mn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST019", service010.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID 	: list
	 * Method 설명 	: 현재고단가조회
	 * 작성자 			: SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST019/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			//System.out.println(model);
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

    
    
}