package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST350Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST350Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST350Service")
	private WMSST350Service service;
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 임가공(GS) 화면
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST350.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST350", service.selectItemGrp(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list_T1
	 * Method 설명 : 임가공(GS) 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST350/list_T1.action")
	public ModelAndView list_T1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : list_T2
	 * Method 설명 : 임가공(GS 전체) 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST350/list_T2.action")
	public ModelAndView list_T2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : listSub_T1
	 * Method 설명      : 임가공(GS)서브 조회
	 * 작성자                 : 이성중
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSST350/listSub_T1.action")
	public ModelAndView listSub_T1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub_T1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : listSub_T2
	 * Method 설명      : 임가공(GS 전체)서브 조회
	 * 작성자                 : 이성중
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSST350/listSub_T2.action")
	public ModelAndView listSub_T2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub_T2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*-
	 * Method ID : listZone_T1
	 * Method 설명 : 임가공 조회(출고)
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST350/listZone_T1.action")
	public ModelAndView listZone_T1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listZone_T1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : listZone_T2
	 * Method 설명 : 임가공 조회(전체)
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST350/listZone_T2.action")
	public ModelAndView listZone_T2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listZone_T2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : listSubZone_T1
	 * Method 설명      : 임가공 서브 조회(출고)
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSST350/listSubZone_T1.action")
	public ModelAndView listSubZone_T1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSubZone_T1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : listSubZone_T2
	 * Method 설명      : 임가공 서브 조회(전체)
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSST350/listSubZone_T2.action")
	public ModelAndView listSubZone_T2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSubZone_T2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	
	/*-
	 * Method ID : listExcel1
	 * Method 설명 : 원부재료 엑셀 일괄 리포팅
	 * 작성자 : 이성중
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST350/excel.action")
	public void listExcel1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel_T1(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : 이성중
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("세트상품코드"), "0", "0", "0", "0", "250"},
                                   {MessageResolver.getMessage("세트상품명칭"), "1", "1", "0", "0", "400"},
                                   {MessageResolver.getMessage("구성품코드"), "2", "2", "0", "0", "220"},
                                   {MessageResolver.getMessage("구성품명칭"), "3", "3", "0", "0", "290"},
                                   {MessageResolver.getMessage("총 필요수량"), "4", "4", "0", "0", "220"},
                                   {MessageResolver.getMessage("주문수량"), "5", "5", "0", "0", "220"},
                                   {MessageResolver.getMessage("세트품 1개 생성시 필요수량"), "6", "6", "0", "0", "400"}                       
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"ITEM_CD"          , "S"},
                                    {"ITEM_NM"          , "S"},
                                    {"PART_ITEM_CD"     , "S"},
                                    {"PART_ITEM_NM"     , "S"},
                                    {"MAKE_PART_QTY"    , "N"},
                                    {"REQ_SET_QTY"      , "N"},
                                    {"PART_QTY"   		, "N"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("출고_원부재료전체");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}    
}