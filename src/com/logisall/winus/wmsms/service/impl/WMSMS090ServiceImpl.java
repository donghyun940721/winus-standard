package com.logisall.winus.wmsms.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
//import com.m2m.jdfw5x.egov.exception.BizException;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS090Service;
import com.logisall.winus.wmsop.service.impl.WMSOP520Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS090Service")
public class WMSMS090ServiceImpl implements WMSMS090Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS090Dao")
	private WMSMS090Dao dao;
	
	@Resource(name = "WMSOP520Dao")
	private WMSOP520Dao dao_gs;

	private final static String[] CHECK_VALIDATE_WMSMS090 = { "CUST_ID", "REP_UOM_ID" };

	/**
	 * 대체 Method ID : selectData 대체 Method 설명 : 상품 목록 필요 데이타셋 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ITEMGRP", dao.selectItemGrp(model));
		if(model.get("SS_SVC_NO").equals("0000001840")){
			map.put("ITEMGRP98", dao_gs.selectItemGrp98(model));
			map.put("ITEMGRP99", dao_gs.selectItemGrp99(model));
		}
//		map.put("ITEMGRPLV1", dao.selectItemGrpLv1(model));
//		map.put("ITEMGRPLV2", dao.selectItemGrpLv2(model));
//		map.put("ITEMGRPLV3", dao.selectItemGrpLv3(model));
		map.put("UOM", dao.selectUom(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listItem 대체 Method 설명 : 상품 목록 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listItem(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.listItem(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : saveItem 대체 Method 설명 : 상품관리저장(저장,수정,삭제) 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveItem(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
			int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
			if (iuCnt > 0) {
				// 저장, 수정
				String[] itemIdTemp = new String[iuCnt]; // 상품ID
				String[] unitPrice = new String[iuCnt];
				String[] custBarcode = new String[iuCnt]; // 화주 바코드
				String[] ritemId = new String[iuCnt]; // 센터화주상품ID
				String[] custId = new String[iuCnt]; // 화주ID

				String[] stockUomId = new String[iuCnt]; // 재고단위
				String[] buyUnit = new String[iuCnt]; // 구매단위
				String[] itemType = new String[iuCnt]; // 상품구분
				String[] itemWgt = new String[iuCnt]; // 상품중량
				String[] itemGrpId = new String[iuCnt]; // 상품군

				String[] itemGrpType = new String[iuCnt]; // 상품군 type
				String[] packingBoxType = new String[iuCnt]; // 포장box종류
				String[] propStockDay = new String[iuCnt]; // 적정재고일수
				String[] timePeriodDay = new String[iuCnt]; // 유효기간일수
				String[] shipAbc = new String[iuCnt]; // 출하abc

				String[] setItemYn = new String[iuCnt]; // 임가공상품여부
				String[] weightClass = new String[iuCnt]; // 적재무게등급
				String[] ordCustId = new String[iuCnt]; // 발주거래처ID
				String[] itemBarCd = new String[iuCnt]; // 상품바코드
				String[] itemSize = new String[iuCnt]; // 상품크기

				String[] kanCd = new String[iuCnt]; // KAN코드
				String[] vatYn = new String[iuCnt]; // 부가세여부
				String[] itemClass = new String[iuCnt]; // 상품등급
				String[] opQty = new String[iuCnt]; // OP수량
				String[] rsQty = new String[iuCnt]; // RACK보충수량

				String[] eaYn = new String[iuCnt]; // 낱개관리여부
				String[] itemEpcCd = new String[iuCnt]; // RFID태그
				String[] inWhId = new String[iuCnt]; // 입고창고코드
				String[] inZone = new String[iuCnt]; // 입고존
				String[] outZone = new String[iuCnt]; // 입고존
				String[] pltYn = new String[iuCnt]; // plt관리여부

				String[] repUomIdTemp = new String[iuCnt]; // uomid
				String[] opUomId = new String[iuCnt]; // op_uomid
				String[] propQty = new String[iuCnt]; // 적정재고
				String[] expiryDate = new String[iuCnt]; // 유통기한
				String[] tagPrefix = new String[iuCnt]; // tag_prefix

				String[] custEpcCd = new String[iuCnt]; // 회사 epc코드
				String[] itemCode = new String[iuCnt]; // 상품코드
				String[] custItemCd = new String[iuCnt]; // 화주상품코드
				String[] itemEngNm = new String[iuCnt]; // 상품명(영문)
				String[] itemKorNm = new String[iuCnt]; // 상품명(한글)

				String[] itemShortNm = new String[iuCnt]; // 상품명(약어)
				String[] itemNm = new String[iuCnt]; // 상품명

				// 추가된부분
				String[] bestDateType = new String[iuCnt]; // 유효기간통제기준
				String[] bestDateNum = new String[iuCnt]; // 유효기간
				String[] bestDateUnit = new String[iuCnt]; // 유효기간단위
				String[] useDateNum = new String[iuCnt]; // 소비기한
				String[] useDateUnit = new String[iuCnt]; // 소비기한단위

				String[] outBestDateNum = new String[iuCnt]; // 출하최소유효기간
				String[] outBestDateUnit = new String[iuCnt]; // 출하최소유효기간단위
				String[] inBestDateNum = new String[iuCnt]; // 입고최소유효기간
				String[] inBestDateUnit = new String[iuCnt]; // 입고최소유효기간단위
				
				//String[] insertOrdTime = new String[iuCnt]; // 자동주문시간

				String[] color = new String[iuCnt]; // 색상
				String[] itfCd = new String[iuCnt]; // ITF_CD
				String[] sizeH = new String[iuCnt]; // 세로
				String[] sizeL = new String[iuCnt]; // 높이
				String[] sizeW = new String[iuCnt]; // 가로

				String[] boxEpcCd = new String[iuCnt]; // 박스EPC코드
				String[] typeSt = new String[iuCnt]; // 상품관리TYPE
				String[] boxBarCd = new String[iuCnt]; // 박스바코드

				String[] custLegacyItemCd = new String[iuCnt]; // 박스바코드
				String[] makerNm = new String[iuCnt]; // 상품명(한글)
				String[] lotUseYn = new String[iuCnt];
				String[] autoOutOrdYn = new String[iuCnt];
				String[] autoCalPltYn = new String[iuCnt];
				String[] outLocRec = new String[iuCnt];
				String[] inLocRec = new String[iuCnt];
				
				String[] itemDevisionCd = new String[iuCnt]; // 상품명(한글)
				String[] lotPrefix = new String[iuCnt];
				String[] reworkItemYn = new String[iuCnt];
				
				String[] inOutBound = new String[iuCnt];
				
				String[] inCustZone = new String[iuCnt];
				String[] outCustZone = new String[iuCnt];
				String[] itemDesc = new String[iuCnt];
				String[] workingTime = new String[iuCnt];
				String[] currencyName = new String[iuCnt];

				
				String[] itemCapaCity = new String[iuCnt];
				String[] setItemType = new String[iuCnt];
				String[] useYn = new String[iuCnt];
				String[] itemTmp = new String[iuCnt];
				String[] fixLocId = new String[iuCnt];
				String[] unitQty = new String[iuCnt];
				String[] unitNm = new String[iuCnt];

				String[] salesPrice = new String[iuCnt];
				String[] devItemTmp = new String[iuCnt];
				
				String[] parcelInQty = new String[iuCnt];
				String[] parcelCbm = new String[iuCnt];
				String[] itemGrp2ndId = new String[iuCnt]; // 상품군2
				String[] itemGrp3rdId = new String[iuCnt]; // 상품군3
				
				//2023-04-26 추가 SUMMER H
				String[] countryOrigin = new String[iuCnt]; //원산지
				String[] parcelComTy  = new String[iuCnt]; 	//택배분리타입
				String[] serialChkYn  = new String[iuCnt];	//시리얼검수여부
				String[] parcelCompCd = new String[iuCnt];  //택배사코드
				String[] machineType  = new String[iuCnt];  //설비유형
				String[] fixLocZoneId = new String[iuCnt];  //고정존
				String[] minParcelCbm = new String[iuCnt];  //최소포장CBM
				String[] subMaterials = new String[iuCnt];  //부자재
                String[] innerBoxBarCd = new String[iuCnt]; //내부박스바코드
                String[] PLTBarCd = new String[iuCnt]; 		//파레트바코드
                String[] itemEtc1 = new String[iuCnt]; 		//비고1
				
				String lcId = ""; // 센터ID
				String workIp = "";
				String userNo = "";

				for (int i = 0; i < iuCnt; i++) {
					itemIdTemp[i] = (String) model.get("I_ITEM_ID" + i);
					unitPrice[i] = (String) model.get("I_UNIT_PRICE" + i);
					custBarcode[i] = (String) model.get("I_CUST_BARCODE" + i);
					ritemId[i] = (String) model.get("I_RITEM_ID" + i);
					custId[i] = (String) model.get("I_CUST_ID" + i);

					System.out.println((String) model.get("I_ITEM_TYPE" + i));
					stockUomId[i] = (String) model.get("I_STOCK_UOM_ID" + i);
					buyUnit[i] = (String) model.get("I_BUY_UNIT" + i);
					itemType[i] = (String) model.get("I_ITEM_TYPE" + i);
					itemWgt[i] = (String) model.get("I_ITEM_WGT" + i);
					itemGrpId[i] = (String) model.get("I_ITEM_GRP_ID" + i);

					itemGrpType[i] = (String) model.get("I_ITEM_GRP_TYPE" + i);
					packingBoxType[i] = (String) model.get("I_PACKING_BOX_TYPE" + i);
					propStockDay[i] = (String) model.get("I_PROP_STOCK_DAY" + i);
					timePeriodDay[i] = (String) model.get("I_TIME_PERIOD_DAY" + i);
					shipAbc[i] = (String) model.get("I_SHIP_ABC" + i);

					setItemYn[i] = (String) model.get("I_SET_ITEM_YN" + i);
					weightClass[i] = (String) model.get("I_WEIGHT_CLASS" + i);
					ordCustId[i] = (String) model.get("I_ORD_CUST_ID" + i);
					itemBarCd[i] = (String) model.get("I_ITEM_BAR_CD" + i);
					itemSize[i] = (String) model.get("I_ITEM_SIZE" + i);
 
					kanCd[i] = (String) model.get("I_KAN_CD" + i);
					vatYn[i] = (String) model.get("I_VAT_YN" + i);
					itemClass[i] = (String) model.get("I_ITEM_CLASS" + i);
					opQty[i] = (String) model.get("I_OP_QTY" + i);
					rsQty[i] = (String) model.get("I_RS_QTY" + i);

					eaYn[i] = (String) model.get("I_EA_YN" + i);
					itemEpcCd[i] = (String) model.get("I_ITEM_EPC_CD" + i);
					inWhId[i] = (String) model.get("I_IN_WH_ID" + i);
					inZone[i] = (String) model.get("I_IN_ZONE" + i);
					outZone[i] = (String) model.get("I_OUT_ZONE" + i);
					pltYn[i] = (String) model.get("I_PLT_YN" + i);

					repUomIdTemp[i] = (String) model.get("I_REP_UOM_ID" + i);
					opUomId[i] = (String) model.get("I_OP_UOM_ID" + i);
					propQty[i] = (String) model.get("I_PROP_QTY" + i);
					expiryDate[i] = (String) model.get("I_EXPIRY_DATE" + i);
					tagPrefix[i] = (String) model.get("I_TAG_PREFIX" + i);

					custEpcCd[i] = (String) model.get("I_CUST_EPC_CD" + i);
					itemCode[i] = (String) model.get("I_ITEM_CODE" + i);
					custItemCd[i] = (String) model.get("I_CUST_ITEM_CD" + i);
					itemEngNm[i] = (String) HtmlUtils.htmlUnescape(model.get("I_ITEM_ENG_NM" + i).toString());
					itemKorNm[i] = (String) HtmlUtils.htmlUnescape(model.get("I_ITEM_KOR_NM" + i).toString()); //.toString().replaceAll("&amp;", "&");
					itemShortNm[i] = (String) model.get("I_ITEM_SHORT_NM" + i);
					itemNm[i] = (String) HtmlUtils.htmlUnescape(model.get("I_ITEM_NM" + i).toString());

					// 추가된부분
					bestDateType[i] = (String) model.get("I_BEST_DATE_TYPE" + i);
					bestDateNum[i] = (String) model.get("I_BEST_DATE_NUM" + i);
					bestDateUnit[i] = (String) model.get("I_BEST_DATE_UNIT" + i);
					useDateNum[i] = (String) model.get("I_USE_DATE_NUM" + i);
					useDateUnit[i] = (String) model.get("I_USE_DATE_UNIT" + i);

					outBestDateNum[i] = (String) model.get("I_OUT_BEST_DATE_NUM" + i);
					outBestDateUnit[i] = (String) model.get("I_OUT_BEST_DATE_UNIT" + i);
					inBestDateNum[i] = (String) model.get("I_IN_BEST_DATE_NUM" + i);
					inBestDateUnit[i] = (String) model.get("I_IN_BEST_DATE_UNIT" + i);

					//insertOrdTime[i] = (String) model.get("I_INSERT_ORD_TIME" + i);
					
					color[i] = (String) model.get("I_COLOR" + i);
					itfCd[i] = (String) model.get("I_ITF_CD" + i);
					sizeH[i] = (String) model.get("I_SIZE_H" + i);
					sizeL[i] = (String) model.get("I_SIZE_L" + i);
					sizeW[i] = (String) model.get("I_SIZE_W" + i);

					boxEpcCd[i] = (String) model.get("I_BOX_EPC_CD" + i);
					typeSt[i] = (String) model.get("I_TYPE_ST" + i);
					boxBarCd[i] = (String) model.get("I_BOX_BAR_CD" + i);

					custLegacyItemCd[i] = (String) model.get("I_CUST_LEGACY_ITEM_CD" + i);
					makerNm[i] = (String) model.get("I_MAKER_NM" + i);
					lotUseYn[i] = (String) model.get("I_LOT_USE_YN" + i);
					autoOutOrdYn[i] = (String) model.get("I_AUTO_OUT_ORD_YN" + i);
					autoCalPltYn[i] = (String) model.get("I_AUTO_CAL_PLT_YN" + i);
					outLocRec[i] = (String) model.get("I_OUT_LOC_REC" + i);
					inLocRec[i] = (String) model.get("I_IN_LOC_REC" + i);
					
					itemDevisionCd[i] = (String) model.get("I_ITEM_DEVISION_CD" + i);
					lotPrefix[i] = (String) model.get("I_LOT_PREFIX" + i);
					reworkItemYn[i] = (String) model.get("I_REWORK_ITEM_YN" + i);
					
					inOutBound[i] = (String) model.get("I_IN_OUT_BOUND" + i);
					
					inCustZone[i] = (String) model.get("I_IN_CUST_ZONE" + i);
					outCustZone[i] = (String) model.get("I_OUT_CUST_ZONE" + i);
					itemDesc[i] = (String) model.get("I_ITEM_DESC" + i);
					workingTime[i] = (String) model.get("I_WORKING_TIME" + i);
					currencyName[i] = (String) model.get("I_CURRENCY_NAME" + i);
					
					itemCapaCity[i] = (String) model.get("I_ITEM_CAPACITY" + i);
					setItemType[i] = (String) model.get("I_SET_ITEM_TYPE" + i);
					useYn[i] = (String) model.get("I_USE_YN" + i);
					itemTmp[i] = (String) model.get("I_ITEM_TMP" + i);
					fixLocId[i] = (String) model.get("I_FIX_LOC_ID" + i);
					unitQty[i] = (String) model.get("I_UNIT_QTY" + i);
					unitNm[i] = (String) model.get("I_UNIT_NM" + i);
					salesPrice[i] = (String) model.get("I_SALES_PRICE"+ i);
					devItemTmp[i] = (String) model.get("I_DEV_ITEM_TMP"+ i);
					
					parcelInQty[i] = (String) model.get("I_PARCEL_IN_QTY" + i);
					parcelCbm[i] = (String) model.get("I_PARCEL_CBM" + i);
					itemGrp2ndId[i] = (String) model.get("I_ITEM_GRP_2ND_ID" + i);
					itemGrp3rdId[i] = (String) model.get("I_ITEM_GRP_3RD_ID" + i);
					
					//2023-04-26 추가 SUMMER H
					countryOrigin[i]= (String) model.get("I_COUNTRY_ORIGIN" + i);
					parcelComTy[i] 	= (String) model.get("I_PARCEL_COM_TY" + i);
					serialChkYn[i] 	= (String) model.get("I_SERIAL_CHK_YN" + i);
					parcelCompCd[i] = (String) model.get("I_PARCEL_COMP_CD" + i);
					machineType[i] 	= (String) model.get("I_MACHINE_TYPE" + i);
					fixLocZoneId[i] = (String) model.get("I_FIX_LOC_ZONE_ID" + i);
					minParcelCbm[i] = (String) model.get("I_MIN_PARCEL_CBM" + i);
					subMaterials[i] = (String) model.get("I_SUB_MATERIALS" + i);
					innerBoxBarCd[i]= (String) model.get("I_INNER_BOX_BAR_CD" + i);
					PLTBarCd[i] 	= (String) model.get("I_PLT_BAR_CD" + i);
					itemEtc1[i] 	= (String) model.get("I_ITEM_ETC1" + i);
					
				}

				lcId = (String) model.get(ConstantIF.SS_SVC_NO);
				workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
				userNo = (String) model.get(ConstantIF.SS_USER_NO);

				Map<String, Object> modelIu = new HashMap<String, Object>();
				modelIu.put("ITEM_ID", itemIdTemp); // varchar2 20 not null
				modelIu.put("UNIT_PRICE", unitPrice); // number (15 (소수점포함 38))
				modelIu.put("CUST_BARCODE", custBarcode); // varchar2 20
				modelIu.put("RITEM_ID", ritemId); // varchar2 20 not null
				modelIu.put("CUST_ID", custId); // varchar2 20 not null

				modelIu.put("STOCK_UOM_ID", stockUomId); // varchar2 20
				modelIu.put("BUY_UNIT", buyUnit); // varchar2 20
				modelIu.put("ITEM_TYPE", itemType); // varchar2 20
				modelIu.put("ITEM_WGT", itemWgt); // number(10, 3)
				modelIu.put("ITEM_GRP_ID", itemGrpId); // varchar2 20

				modelIu.put("ITEM_GRP_TYPE", itemGrpType); // varchar2 20
				modelIu.put("PACKING_BOX_TYPE", packingBoxType); // varchar2 20
				modelIu.put("PROP_STOCK_DAY", propStockDay); // number 3
				modelIu.put("TIME_PERIOD_DAY", timePeriodDay); // number 10
				modelIu.put("SHIP_ABC", shipAbc); // varchar2 20

				modelIu.put("SET_ITEM_YN", setItemYn); // char 1
				modelIu.put("WEIGHT_CLASS", weightClass); // varchar2 20
				modelIu.put("ORD_CUST_ID", ordCustId); // varchar2 20
				modelIu.put("ITEM_BAR_CD", itemBarCd); // varchar2 20
				modelIu.put("ITEM_SIZE", itemSize); // varchar2 20

				modelIu.put("KAN_CD", kanCd); // varchar2 20
				modelIu.put("VAT_YN", vatYn); // char 1
				modelIu.put("ITEM_CLASS", itemClass); // varchar2 20
				modelIu.put("OP_QTY", opQty); // number 10
				modelIu.put("RS_QTY", rsQty); // number 10

				modelIu.put("EA_YN", eaYn); // char 1
				modelIu.put("ITEM_EPC_CD", itemEpcCd); // varchar2 100
				modelIu.put("IN_WH_ID", inWhId); // varchar2 20
				modelIu.put("IN_ZONE", inZone); // varchar2 20
				modelIu.put("OUT_ZONE", outZone); // varchar2 20
				modelIu.put("PLT_YN", pltYn); // char 1

				modelIu.put("REP_UOM_ID", repUomIdTemp); // varchar2 20 not null
				modelIu.put("OP_UOM_ID", opUomId); // varchar2 20
				modelIu.put("PROP_QTY", propQty); // number (15 (소수점포함 38))
				modelIu.put("EXPIRY_DATE", expiryDate); // varchar2 8
				modelIu.put("TAG_PREFIX", tagPrefix); // varchar2 20

				modelIu.put("CUST_EPC_CD", custEpcCd); // varchar2 20
				modelIu.put("ITEM_CODE", itemCode); // varchar2 200
				modelIu.put("CUST_ITEM_CD", custItemCd);// varchar2 200
				modelIu.put("ITEM_ENG_NM", itemEngNm); // varchar2 200
				modelIu.put("ITEM_KOR_NM", itemKorNm); // varchar2 200

				modelIu.put("ITEM_SHORT_NM", itemShortNm); // varchar2 200
				modelIu.put("ITEM_NM", itemNm); // varchar2 100
				modelIu.put("LC_ID", lcId); // varchar2 20 not null

				modelIu.put("WORK_IP", workIp);
				modelIu.put("USER_NO", userNo);

				modelIu.put("BEST_DATE_TYPE", bestDateType); // 유효기간통제기준
				modelIu.put("BEST_DATE_NUM", bestDateNum); // 유효기간
				modelIu.put("BEST_DATE_UNIT", bestDateUnit); // 유효기간단위
				modelIu.put("USE_DATE_NUM", useDateNum); // 소비기한
				modelIu.put("USE_DATE_UNIT", useDateUnit); // 소비기한단위

				modelIu.put("OUT_BEST_DATE_NUM", outBestDateNum); // 출하최소유효기간
				modelIu.put("OUT_BEST_DATE_UNIT", outBestDateUnit); // 출하최소유효기간단위
				modelIu.put("IN_BEST_DATE_NUM", inBestDateNum); // 입고최소유효기간
				modelIu.put("IN_BEST_DATE_UNIT", inBestDateUnit); // 입고최소유효기간단위
				
				//modelIu.put("INSERT_ORD_TIME", insertOrdTime); // 입고최소유효기간

				// 추가된부분(프로시져에서는 아직안씀)
				modelIu.put("COLOR", color); // 색상
				modelIu.put("ITF_CD", itfCd); // ITF_CD
				modelIu.put("SIZE_H", sizeH); // 사이즈_높이
				modelIu.put("SIZE_L", sizeL); // 사이즈_세로
				modelIu.put("SIZE_W", sizeW); // 사이즈_가로

				modelIu.put("BOX_EPC_CD", boxEpcCd); // 박스EPC코드
				modelIu.put("TYPE_ST", typeSt); // 상품관리TYPE
				modelIu.put("BOX_BAR_CD", boxBarCd); // 상품관리TYPE

				modelIu.put("CUST_LEGACY_ITEM_CD", custLegacyItemCd); // 상품관리TYPE
				modelIu.put("MAKER_NM", makerNm); // varchar2 200
				modelIu.put("LOT_USE_YN", lotUseYn); // char 1
				modelIu.put("AUTO_OUT_ORD_YN", autoOutOrdYn); // char 1
				modelIu.put("AUTO_CAL_PLT_YN", autoCalPltYn); // char 1
				modelIu.put("OUT_LOC_REC", outLocRec); // char 1
				modelIu.put("IN_LOC_REC", inLocRec); // char 1
				

				modelIu.put("ITEM_DEVISION_CD", itemDevisionCd); // varchar2 200
				modelIu.put("LOT_PREFIX", lotPrefix); // varchar2 50
				modelIu.put("REWORK_ITEM_YN", reworkItemYn); // char 1
				
				modelIu.put("IN_OUT_BOUND", inOutBound); // char 3
				
				modelIu.put("IN_CUST_ZONE", inCustZone); // char 20
				modelIu.put("OUT_CUST_ZONE", outCustZone); // char 20
				modelIu.put("ITEM_DESC", itemDesc); // varchar2 1000
				modelIu.put("WORKING_TIME", workingTime); // number (15 (소수점포함 38))
				modelIu.put("CURRENCY_NAME", currencyName); // VARCHAR2(3)
				
				modelIu.put("ITEM_CAPACITY", itemCapaCity); // NUMBER
				modelIu.put("SET_ITEM_TYPE", setItemType); // CHAR(1)
				modelIu.put("USE_YN", useYn); // CHAR(1)
				modelIu.put("ITEM_TMP", itemTmp); // CHAR(1)
				modelIu.put("FIX_LOC_ID", fixLocId); // CHAR(1)
				modelIu.put("UNIT_QTY", unitQty); // CHAR(1)
				modelIu.put("UNIT_NM", unitNm); // CHAR(1)
				modelIu.put("SALES_PRICE", salesPrice); // CHAR(1)
				modelIu.put("DEV_ITEM_TMP", devItemTmp); // CHAR(1)
				
				modelIu.put("PARCEL_IN_QTY", parcelInQty); // NUMBER (10,6)
				modelIu.put("PARCEL_CBM", parcelCbm); // NUMBER (10,6)
				modelIu.put("ITEM_GRP_2ND_ID", itemGrp2ndId); // varchar2 20
				modelIu.put("ITEM_GRP_3RD_ID", itemGrp3rdId); // varchar2 20
				
				//2023-04-26 추가 SUMMER H
				modelIu.put("COUNTRY_ORIGIN", countryOrigin); 	//  
				modelIu.put("PARCEL_COM_TY", parcelComTy); 		// 
				modelIu.put("SERIAL_CHK_YN", serialChkYn); 		// 
				modelIu.put("PARCEL_COMP_CD", parcelCompCd); 	// 
				modelIu.put("MACHINE_TYPE", machineType); 		// 
				modelIu.put("FIX_LOC_ZONE_ID", fixLocZoneId); 	// 
				modelIu.put("MIN_PARCEL_CBM", minParcelCbm); 	// 
				modelIu.put("SUB_MATERIALS", subMaterials); 	// 
				modelIu.put("INNER_BOX_BAR_CD", innerBoxBarCd); // 
				modelIu.put("PLT_BAR_CD", PLTBarCd); 			// 
				modelIu.put("ITEM_ETC1", itemEtc1); 			// 
				
				ServiceUtil.checkInputValidation2(modelIu, CHECK_VALIDATE_WMSMS090);
				//System.out.println("service===>"+modelIu);
				// dao
				dao.saveItem(modelIu);
				ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIu.get("O_MSG_CODE")),
						(String) modelIu.get("O_MSG_NAME"));

			}
			// 등록 수정 끝

			// 삭제
			if (delCnt > 0) {
				for (int i = 0; i < Integer.parseInt(model.get("D_selectIds").toString()); i++) {
					
					Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "RITEM");
					modelSP.put("I_CODE", model.get("D_RITEM_ID" + i));				
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }
					
					Map<String, Object> modelDt = new HashMap<String, Object>();
					modelDt.put("USER_ID", model.get(ConstantIF.SS_USER_ID));
					modelDt.put("ITEM_ID", model.get("D_ITEM_ID" + i));
					modelDt.put("RITEM_ID", model.get("D_RITEM_ID" + i));
					modelDt.put("SS_USER_NO", model.get("SS_USER_NO"));
					
					if ("DELETE".equals(model.get("D_ST_GUBUN" + i))) {
						dao.deleteItem(modelDt);
						dao.deleteRitem(modelDt);
					} else {
						errCnt++;
						m.put("errCnt", errCnt);
						throw new BizException(MessageResolver.getMessage("save.error"));
					}
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	// /**
	// *
	// * 대체 Method ID : deleteItem
	// * 대체 Method 설명 : 상품 목록 삭제 (x)
	// * 작성자 : chsong
	// * @param model
	// * @return
	// * @throws Exception
	// */
	// @Override
	// public Map<String, Object> deleteItem(Map<String, Object> model) throws
	// Exception {
	// Map<String, Object> m = new HashMap<String, Object>();
	// int errCnt = 0;
	// try{
	// dao.deleteItem(model);
	// dao.deleteRitem(model);
	// } catch(Exception e){
	// throw e;
	// }
	// return m;
	// }

	/**
	 * 
	 * 대체 Method ID : listUom 대체 Method 설명 : UOM 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listUom(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.listUom(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : saveUom 대체 Method 설명 : UOM저장 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveUom(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("USER_NO", model.get(ConstantIF.SS_USER_NO));
				modelDt.put("ST_GUBUN", model.get("ST_GUBUN" + i));
				modelDt.put("APPLY_FR_DT", model.get("APPLY_FR_DT" + i));
				modelDt.put("APPLY_TO_DT", model.get("APPLY_TO_DT" + i));
				modelDt.put("CLOSE_DT", model.get("CLOSE_DT" + i));
				modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
				modelDt.put("UOM1_CD", model.get("UOM1_CD" + i));
				modelDt.put("UOM2_CD", model.get("UOM2_CD" + i));
				modelDt.put("QTY", model.get("QTY" + i));
				modelDt.put("UOM_SEQ", model.get("UOM_SEQ" + i));

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insertUom(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.updateUom(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					dao.deleteUom(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
				
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());
			
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * 대체 Method ID : listExcel 대체 Method 설명 : 상품목록 엑셀. 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.listItem(model));

		return map;
	}
	
	/**
	 * 대체 Method ID : 다중 바코드 조회 
	 * 작성자 : schan
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> pop3List(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("vrLcId", model.get(ConstantIF.SS_SVC_NO));
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "10");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.pop3List(model));

		return map;
	}
	
	/**
	 * 대체 Method ID : 다중 바코드 저장 
	 * 작성자 : schan
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @Override
    public Map<String, Object> pop3Save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	List itemBarCdList = dao.pop3BarList(model);
        	// 아이템 바코드 중복 체크
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
        		Map<String,Object> map = new HashMap<String,Object>();
        		Map<String,Object> orgmap = new HashMap<String,Object>();
        		String newItemBarCd = (String)model.get("ITEM_BAR_CD"      + i);
        		String orgItemBarCd = (String)model.get("ORG_ITEM_BAR_CD"  + i);
        		
        		if("INSERT".equals(model.get("ST_GUBUN"+i))){
        			map.put("ITEM_BAR_CD", newItemBarCd);
        			itemBarCdList.add(map);
        			continue;
        		}
        		orgmap.put("ITEM_BAR_CD", orgItemBarCd);
        		if (itemBarCdList.indexOf(orgmap) != -1){
        			map.put("ITEM_BAR_CD", newItemBarCd);
        			itemBarCdList.set(itemBarCdList.indexOf(orgmap),map);
        		}
        		else{
                    m.put("errCnt", errCnt);
                    m.put("MSG", "입력값중 잘못된 값이 있습니다.");
                    return m;
        		}
        	}

        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
        		Map<String,Object> map = new HashMap<String,Object>();
        		String newItemBarCd = (String)model.get("ITEM_BAR_CD"      + i);
        		map.put("ITEM_BAR_CD", newItemBarCd);
        		if (Collections.frequency(itemBarCdList, map) > 1){
        			m.put("errCnt", errCnt);
                    m.put("MSG", "입력값중 중복된 값이 있습니다.");
                    return m;
        		}
        	}
        	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("CUST_ID"    , model.get("SrchCustId"));
                modelDt.put("RITEM_ID"    , model.get("SrchRitemId"));
                modelDt.put("ITEM_BAR_CD"       , model.get("ITEM_BAR_CD"   + i));
                modelDt.put("ITEM_MAKER"          , model.get("ITEM_MAKER"       + i));
                
                modelDt.put("ITEM_NM"          , model.get("ITEM_NM"       + i));
                modelDt.put("UOM1_CD"          , model.get("UOM1_CD"       + i));
                modelDt.put("UOM2_CD"          , model.get("UOM2_CD"       + i));
                modelDt.put("CVT_QTY"          , model.get("CVT_QTY"       + i));
                modelDt.put("DEF_QTY"          , model.get("DEF_QTY"       + i));
                
                modelDt.put("ORG_ITEM_BAR_CD"          , model.get("ORG_ITEM_BAR_CD"       + i));
                
                modelDt.put("WORK_IP"      , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("USER_ID"      , model.get(ConstantIF.SS_USER_NO));
                modelDt.put("ST_GUBUN"     , model.get("ST_GUBUN"   + i));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	dao.pop3Insert(modelDt);
                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.pop3Update(modelDt);
                }else if(model.get("ORG_ITEM_BAR_CD" + i).equals(model.get("vrRepBarCd"))){
                	
                }else{
                    errCnt++;
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            e.printStackTrace();
            m.put("errCnt", errCnt);
            m.put("MSG", e.getMessage());
        }
        return m;
    }
    
    
    /**
	 * 대체 Method ID : 다중 바코드 동기화 
	 * 작성자 : dhkim
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @Override
    public Map<String, Object> pop3Sync(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	dao.pop3Sync(model);
        	
        	m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        }
        catch(Exception e){
            e.printStackTrace();
            m.put("errCnt", errCnt);
            m.put("MSG", e.getMessage());
        }
        
        
        return m;
    }
    
    
    /**
	 * 대체 Method ID : 다중 바코드 삭제 
	 * 작성자 : schan
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @Override
    public Map<String, Object> pop3Delete(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try{
             for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                 Map<String, Object> modelDt = new HashMap<String, Object>();
                 modelDt.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
                 modelDt.put("CUST_ID"    , model.get("SrchCustId"));
                 modelDt.put("RITEM_ID"    , model.get("SrchRitemId"));

                 modelDt.put("ORG_ITEM_BAR_CD"    , model.get("ORG_ITEM_BAR_CD" + i));
                 
                 modelDt.put("WORK_IP"      , model.get(ConstantIF.SS_CLIENT_IP));
                 modelDt.put("USER_ID"      , model.get(ConstantIF.SS_USER_NO));
                 dao.pop3Delete(modelDt);
             }     
             m.put("errCnt" , 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));
         } catch(Exception e){
             m.put("errCnt" , 1);
             m.put("MSG", MessageResolver.getMessage("delete.error"));
         }
         return m;
    }
    

	/**
	 * 
	 * 대체 Method ID : saveUom2 대체 Method 설명 : UOM저장2 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveUom2(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("USER_ID", model.get(ConstantIF.SS_USER_ID));
				modelDt.put("ST_GUBUN", model.get("ST_GUBUN" + i));
				modelDt.put("APPLY_FR_DT", model.get("APPLY_FR_DT" + i));
				modelDt.put("APPLY_TO_DT", model.get("APPLY_TO_DT" + i));
				modelDt.put("CLOSE_DT", model.get("CLOSE_DT" + i));
				modelDt.put("UOM1_CD", model.get("UOM1_CD" + i));
				modelDt.put("UOM2_CD", model.get("UOM2_CD" + i));
				modelDt.put("QTY", model.get("QTY" + i));
				modelDt.put("UOM_SEQ", model.get("UOM_SEQ" + i));

				modelDt.put("vrCustCd", model.get("vrCustCd"));
				modelDt.put("vrCustNm", model.get("vrCustNm"));
				modelDt.put("vrItemCd", model.get("vrItemCd"));

				modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insertUom2(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
			
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : saveCsvWh Method 설명 : CSV 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : 기존 등록 ITEM_CODE 중복 체크
	 * 
	 * @param model
	 * @return
	 */
	public Map<String, Object> overapCheck(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		
		if (srchKey.equals("ITEM_CODE")) {
			map.put("ITEM_CODE", dao.overapCheck(model));
		}
		return map;
	}
	
	/**
	 * Method ID : save Method 설명 : 통합 HelpDesk 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Map<String, Object> modelDt = new HashMap<String, Object>();
				// 확장자 잘라내기
				String str = (String) model.get("D_ATCH_FILE_NAME");
				String ext = "." + str.substring((str.lastIndexOf('.') + 1));
				// 파일 ID 만들기
				int fileSeq = 1;
				String fileId = "";
				fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
				// 저장준비
				modelDt.put("FILE_ID", fileId);
				modelDt.put("ATTACH_GB", "ITEM"); // 통합 HELPDESK 업로드
				modelDt.put("FILE_VALUE", "wmsms090"); // 기존코드 "tmsba230" 로
														// 하드코딩되어 있음 파일구분값?
				modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE")); // 서버저장경로
				modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME")); // 원본파일명
				modelDt.put("FILE_EXT", ext); // 파일 확장자
				modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));// 파일 사이즈
				// 저장
				String fileMngNo = (String) dao.fileUpload(modelDt);
				// 리턴값 파일 id랑 파일경로
				model.put("IMAGE_ID", fileId);
				model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
				
				dao.insertInfo(model);
			}

			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
	
	public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RST", dao.insertValidate(model));
		return map;
	}
	
	/**
     * 
     * 대체 Method ID   : saveUploadDataPk - 엑셀입력 저장 SUMMER
     * 대체 Method 설명    : 템플릿 상품 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveUploadDataPk(Map<String, Object> model, List<Map> list) throws Exception {

		int listBodyCnt   = list.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
            	String[] itemLocalNm 	= new String[listBodyCnt];
            	String[] itemEngNm 		= new String[listBodyCnt];
            	String[] itemBarCd 		= new String[listBodyCnt];
            	String[] itemCode 		= new String[listBodyCnt];
            	String[] uomCd 			= new String[listBodyCnt];
            	
            	String[] itemWgt 		= new String[listBodyCnt];
            	String[] setItemYn 		= new String[listBodyCnt];
            	String[] itemSize		= new String[listBodyCnt];
            	String[] pltYn 			= new String[listBodyCnt];
            	String[] itemNm 		= new String[listBodyCnt];
            	
            	String[] custItemCd 	= new String[listBodyCnt];
            	String[] outLocRec 		= new String[listBodyCnt];
            	String[] useYn 			= new String[listBodyCnt];
            	String[] fixLocCd 		= new String[listBodyCnt];
            	
            	String[] unitPrice 		= new String[listBodyCnt];
            	String[] propQty 		= new String[listBodyCnt];
            	String[] expiryDate 	= new String[listBodyCnt];
            	String[] color 			= new String[listBodyCnt];
            	
            	String[] sizeW 			= new String[listBodyCnt];
            	String[] sizeH 			= new String[listBodyCnt];
            	String[] sizeL 			= new String[listBodyCnt];
            	String[] boxBarCd 		= new String[listBodyCnt];
            	String[] custLegacyItemCd = new String[listBodyCnt];
            	
            	String[] makerNm 		= new String[listBodyCnt];
            	String[] itemCapacity 	= new String[listBodyCnt];
            	String[] cvtQty 		= new String[listBodyCnt];
            	String[] uomCdFrom 	= new String[listBodyCnt];
            	String[] uomCdTo 		= new String[listBodyCnt];
                
            	String[] unitQty 		= new String[listBodyCnt];
            	String[] custBarcode    = new String[listBodyCnt];
            	String[] itemDesc       = new String[listBodyCnt];
            	String[] itemDevisionCd = new String[listBodyCnt];
            	
            	String[] itemGrpCd = new String[listBodyCnt];
            	String[] item2ndGrpCd = new String[listBodyCnt];
            	String[] item3rdGrpCd = new String[listBodyCnt];
            	String[] itemEtc1 = new String[listBodyCnt];
            	
            	//2023-04-26 추가 SUMMER H
				String[] countryOrigin = new String[listBodyCnt]; //원산지
				String[] parcelComTy  = new String[listBodyCnt]; 	//택배분리타입
				String[] serialChkYn  = new String[listBodyCnt];	//시리얼검수여부
				String[] parcelCompCd = new String[listBodyCnt];  //택배사코드
				String[] machineType  = new String[listBodyCnt];  //설비유형
				String[] minParcelCbm = new String[listBodyCnt];  //최소포장CBM
				String[] subMaterials = new String[listBodyCnt];  //부자재
                String[] innerBoxBarCd = new String[listBodyCnt]; //내부박스바코드
                String[] PLTBarCd = new String[listBodyCnt]; 		//파레트바코드
            	//2023-09-05 추가
                String[] outBestDateNum = new String[listBodyCnt]; 	//출하최소유효기간
                String[] outBestDateUnit = new String[listBodyCnt]; //출하최소유효기간단위
                //2023-12-28 추가
                String[] itemTmp = new String[listBodyCnt]; 	//품온구분
                
                //2024-11-28 추가
                String[] pacelInQty = new String[listBodyCnt]; 	//최대합포장수량
                String[] pacelCbm = new String[listBodyCnt]; 	//최대합포장면적
                String[] typeSt = new String[listBodyCnt]; 	//상품관리TYPE
                String[] bestDateType = new String[listBodyCnt]; 	//유통기간통제기준
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	Map object = list.get(i);
                	itemLocalNm[i] 		= object.containsKey("ITEM_LOCAL_NM") ? (String)object.get("ITEM_LOCAL_NM").toString().replaceAll("\"", "") : "";
                	itemEngNm[i] 		= object.containsKey("ITEM_ENG_NM") ? (String)object.get("ITEM_ENG_NM").toString().replaceAll("\"", "") : "";
                	itemBarCd[i] 		= object.containsKey("ITEM_BAR_CD") ? (String)object.get("ITEM_BAR_CD").toString().replaceAll("\"", "") : "";
                	itemCode[i] 		= object.containsKey("ITEM_CODE") ? (String)object.get("ITEM_CODE").toString().replaceAll("\"", "") : "";
                	uomCd[i] 			= object.containsKey("UOM_CD") ? (String)object.get("UOM_CD").toString().replaceAll("\"", "") : "";
                	
                	itemWgt[i]			= object.containsKey("ITEM_WGT") ? (String)object.get("ITEM_WGT").toString().replaceAll("\"", "") : "";
                	setItemYn[i] 		= object.containsKey("SET_ITEM_YN") ? (String)object.get("SET_ITEM_YN").toString().replaceAll("\"", "") : "";
                	itemSize[i] 		= object.containsKey("ITEM_SIZE") ? (String)object.get("ITEM_SIZE").toString().replaceAll("\"", "") : "";
                	pltYn[i] 			= object.containsKey("PLT_YN") ? (String)object.get("PLT_YN").toString().replaceAll("\"", "") : "";
                	itemNm[i] 			= object.containsKey("ITEM_NM") ? (String)object.get("ITEM_NM").toString().replaceAll("\"", "") : "";
                	
                	custItemCd[i] 		= object.containsKey("CUST_ITEM_CD") ? (String)object.get("CUST_ITEM_CD").toString().replaceAll("\"", "") : "";
                	outLocRec[i] 		= object.containsKey("OUT_LOC_REC") ? (String)object.get("OUT_LOC_REC").toString().replaceAll("\"", "") : "";
                	useYn[i] 			= object.containsKey("USE_YN") ? (String)object.get("USE_YN").toString().replaceAll("\"", "") : "";
                	fixLocCd[i] 		= object.containsKey("FIX_LOC_CD") ? (String)object.get("FIX_LOC_CD").toString().replaceAll("\"", "") : "";
                	unitPrice[i] 		= object.containsKey("UNIT_PRICE") ? (String)object.get("UNIT_PRICE").toString().replaceAll("\"", "") : "";
                	propQty[i] 			= object.containsKey("PROP_QTY") ? (String)object.get("PROP_QTY").toString().replaceAll("\"", "") : "";
                	expiryDate[i] 		= object.containsKey("EXPIRY_DATE") ? (String)object.get("EXPIRY_DATE").toString().replaceAll("\"", "") : "";
                	color[i] 			= object.containsKey("COLOR") ? (String)object.get("COLOR").toString().replaceAll("\"", "") : "";
                	
                	sizeW[i] 			= object.containsKey("SIZE_W") ? (String)object.get("SIZE_W").toString().replaceAll("\"", "") : "";
                	sizeH[i] 			= object.containsKey("SIZE_H") ? (String)object.get("SIZE_H").toString().replaceAll("\"", "") : "";
                	sizeL[i] 			= object.containsKey("SIZE_L") ? (String)object.get("SIZE_L").toString().replaceAll("\"", "") : "";
                	boxBarCd[i] 		= object.containsKey("BOX_BAR_CD") ? (String)object.get("BOX_BAR_CD").toString().replaceAll("\"", "") : "";
                	custLegacyItemCd[i] = object.containsKey("CUST_LEGACY_ITEM_CD") ? (String)object.get("CUST_LEGACY_ITEM_CD").toString().replaceAll("\"", "") : "";
                	
                	makerNm[i] 			= object.containsKey("MAKER_NM") ? (String)object.get("MAKER_NM").toString().replaceAll("\"", "") : "";
                	itemCapacity[i]		= object.containsKey("ITEM_CAPACITY") ? (String)object.get("ITEM_CAPACITY").toString().replaceAll("\"", "") : "";
                	cvtQty[i]			= object.containsKey("CVT_QTY") ? (String)object.get("CVT_QTY").toString().replaceAll("\"", "") : "";
                	uomCdFrom[i]		= object.containsKey("UOM1_CD") ? (String)object.get("UOM1_CD").toString().replaceAll("\"", "") : "";
                	uomCdTo[i]			= object.containsKey("UOM2_CD") ? (String)object.get("UOM2_CD").toString().replaceAll("\"", "") : "";
                	
                	unitQty[i]			= object.containsKey("UNIT_QTY") ? (String)object.get("UNIT_QTY").toString().replaceAll("\"", "") : "";
                	custBarcode[i]      = object.containsKey("CUST_BARCODE") ? (String)object.get("CUST_BARCODE").toString().replaceAll("\"", "") : "";
                	itemDesc[i]         = object.containsKey("ITEM_DESC") ? (String)object.get("ITEM_DESC").toString().replaceAll("\"", "") : "";
                	itemDevisionCd[i]   = object.containsKey("ITEM_DEVISION_CD") ? (String)object.get("ITEM_DEVISION_CD").toString().replaceAll("\"", "") : "";
                	
                	itemGrpCd[i]        = object.containsKey("ITEM_GRP_CD") ? (String)object.get("ITEM_GRP_CD").toString().replaceAll("\"", "") : "";
                	item2ndGrpCd[i]     = object.containsKey("ITEM_2ND_GRP_CD") ? (String)object.get("ITEM_2ND_GRP_CD").toString().replaceAll("\"", "") : "";
                	item3rdGrpCd[i]     = object.containsKey("ITEM_3RD_GRP_CD") ? (String)object.get("ITEM_3RD_GRP_CD").toString().replaceAll("\"", "") : "";
                	itemEtc1[i]     = object.containsKey("ITEM_ETC1") ? (String)object.get("ITEM_ETC1").toString().replaceAll("\"", "") : "";
                	
                	//2023-04-26 추가 SUMMER H
                	countryOrigin[i] 	= object.containsKey("COUNTRY_ORIGIN") ? (String)object.get("COUNTRY_ORIGIN").toString().replaceAll("\"", "") : "";
                	parcelComTy[i] 		= object.containsKey("PARCEL_COM_TY") ? (String)object.get("PARCEL_COM_TY").toString().replaceAll("\"", "") : "";
                	serialChkYn[i] 		= object.containsKey("SERIAL_CHK_YN") ? (String)object.get("SERIAL_CHK_YN").toString().replaceAll("\"", "") : "";
                	parcelCompCd[i]		= object.containsKey("PARCEL_COMP_CD") ? (String)object.get("PARCEL_COMP_CD").toString().replaceAll("\"", "") : "";
                	machineType[i] 		= object.containsKey("MACHINE_TYPE") ? (String)object.get("MACHINE_TYPE").toString().replaceAll("\"", "") : "";
              		minParcelCbm[i] 	= object.containsKey("MIN_PARCEL_CBM") ? (String)object.get("MIN_PARCEL_CBM").toString().replaceAll("\"", "") : "";
                	subMaterials[i] 	= object.containsKey("SUB_MATERIALS") ? (String)object.get("SUB_MATERIALS").toString().replaceAll("\"", "") : "";
                	innerBoxBarCd[i]	= object.containsKey("INNER_BOX_BAR_CD") ? (String)object.get("INNER_BOX_BAR_CD").toString().replaceAll("\"", "") : "";
                	PLTBarCd[i] 		= object.containsKey("PLT_BAR_CD") ? (String)object.get("PLT_BAR_CD").toString().replaceAll("\"", "") : "";
                	//2023-09-05 추가
                	outBestDateNum[i] 	= object.containsKey("OUT_BEST_DATE_NUM") ? (String)object.get("OUT_BEST_DATE_NUM").toString().replaceAll("\"", "") : "";
                	outBestDateUnit[i] 	= object.containsKey("OUT_BEST_DATE_UNIT") ? (String)object.get("OUT_BEST_DATE_UNIT").toString().replaceAll("\"", "") : "";
                	//2023-12-28 추가
                	itemTmp[i] 	= object.containsKey("ITEM_TMP") ? (String)object.get("ITEM_TMP").toString().replaceAll("\"", "") : "";
                	
                	//2024-11-28 추가
                	pacelInQty[i] 	= object.containsKey("PARCEL_IN_QTY") ? (String)object.get("PARCEL_IN_QTY").toString().replaceAll("\"", "") : "";
                	pacelCbm[i] 	= object.containsKey("PARCEL_CBM") ? (String)object.get("PARCEL_CBM").toString().replaceAll("\"", "") : "";
                	typeSt[i] 	= object.containsKey("TYPE_ST") ? (String)object.get("TYPE_ST").toString().replaceAll("\"", "") : "";
                	bestDateType[i] 	= object.containsKey("BEST_DATE_TYPE") ? (String)object.get("BEST_DATE_TYPE").toString().replaceAll("\"", "") : "";
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("I_ITEM_LOCAL_NM"		, itemLocalNm);
                modelIns.put("I_ITEM_ENG_NM"		, itemEngNm);
                modelIns.put("I_ITEM_BAR_CD"		, itemBarCd);
                modelIns.put("I_ITEM_CODE"			, itemCode);
                modelIns.put("I_UOM_CD"				, uomCd);
                
                modelIns.put("I_ITEM_WGT"			, itemWgt);
                modelIns.put("I_SET_ITEM_YN"		, setItemYn);
                modelIns.put("I_ITEM_SIZE"			, itemSize);
                modelIns.put("I_PLT_YN"				, pltYn);
                modelIns.put("I_ITEM_NM"			, itemNm);
                
                modelIns.put("I_CUST_ITEM_CD"		, custItemCd);
                modelIns.put("I_OUT_LOC_REC"		, outLocRec);
                modelIns.put("I_USE_YN"				, useYn);
                modelIns.put("I_FIX_LOC_CD"			, fixLocCd);
                modelIns.put("I_UNIT_PRICE"			, unitPrice);
                modelIns.put("I_PROP_QTY"			, propQty);
                modelIns.put("I_EXPIRY_DATE"		, expiryDate);
                modelIns.put("I_COLOR"				, color);
                
                modelIns.put("I_SIZE_W"				, sizeW);
                modelIns.put("I_SIZE_H"				, sizeH);
                modelIns.put("I_SIZE_L"				, sizeL);
                modelIns.put("I_BOX_BAR_CD"			, boxBarCd);
                modelIns.put("I_CUST_LEGACY_ITEM_CD", custLegacyItemCd);
                
                modelIns.put("I_MAKER_NM"			, makerNm);
                modelIns.put("I_ITEM_CAPACITY"		, itemCapacity);
                modelIns.put("I_CVT_QTY"		    , cvtQty);
                modelIns.put("I_UOM1_CD"		    , uomCdFrom);
                modelIns.put("I_UOM2_CD"		    , uomCdTo);
                
                modelIns.put("I_UNIT_QTY"		    , unitQty);
                modelIns.put("I_CUST_BARCODE"       , custBarcode);
                modelIns.put("I_ITEM_DESC"          , itemDesc);
                modelIns.put("I_ITEM_DEVISION_CD"   , itemDevisionCd);
                
                modelIns.put("I_ITEM_GRP_CD"        , itemGrpCd);
                modelIns.put("I_ITEM_2ND_GRP_CD"   , item2ndGrpCd);
                modelIns.put("I_ITEM_3RD_GRP_CD"   , item3rdGrpCd);
                modelIns.put("I_ITEM_ETC1"   , itemEtc1);
                
                //2023-04-26 추가 SUMMER H
                modelIns.put("COUNTRY_ORIGIN"		, countryOrigin);   
                modelIns.put("PARCEL_COM_TY"		, parcelComTy); 	
                modelIns.put("SERIAL_CHK_YN"		, serialChkYn); 	 
                modelIns.put("PARCEL_COMP_CD"		, parcelCompCd); 	
                modelIns.put("MACHINE_TYPE"			, machineType); 		 
                modelIns.put("MIN_PARCEL_CBM"		, minParcelCbm); 	
                modelIns.put("SUB_MATERIALS"		, subMaterials); 
                modelIns.put("INNER_BOX_BAR_CD"		, innerBoxBarCd);
                modelIns.put("PLT_BAR_CD"			, PLTBarCd); 	
                //2023-09-05 추가
                modelIns.put("OUT_BEST_DATE_NUM"	, outBestDateNum); 
                modelIns.put("OUT_BEST_DATE_UNIT"	, outBestDateUnit); 
                
                //2023-12-28 추가
                modelIns.put("ITEM_TMP"	, itemTmp); 
                
               //2024-11-28 추가
                modelIns.put("PARCEL_IN_QTY"	, pacelInQty); 
                modelIns.put("PARCEL_CBM"	, pacelCbm);
                modelIns.put("TYPE_ST"	, typeSt);
                modelIns.put("BEST_DATE_TYPE"	, bestDateType);
                
                modelIns.put("I_DUP_YN"    			, model.get("chkDuplicationYn"));
                modelIns.put("I_BLANK_UPDATE_SKIP_YN"    , model.get("blankUpdateSkipYn"));
                modelIns.put("I_CUST_ID"    		, model.get("vrCustId"));
                modelIns.put("I_LC_ID"    			, model.get("SS_SVC_NO"));   
                modelIns.put("I_WORK_IP"  			, model.get("SS_CLIENT_IP"));  
                modelIns.put("I_USER_NO"  			, model.get("SS_USER_NO"));

                System.out.println(">>>" + model.get("chkDuplicationYn") + " // " + model.get("vrCustId"));
                //dao                
                modelIns = (Map<String, Object>)dao.saveUploadDataPk(modelIns);
                ServiceUtil.isValidReturnCodeNew("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveLcSync
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String[] spLcSyncArrData = model.get("MULIT_LC_ID_ARR").toString().split(",");
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            Map<String, Object> modelIns = new HashMap<String, Object>();
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    modelIns.put("I_MAKE_LC_ID"		, spLcSyncArrData);
                    modelIns.put("I_ITEM_CODE"		, (String)model.get("ITEM_CODE"+i));
                    modelIns.put("I_CUST_ID"		, (String)model.get("CUST_ID"+i));
                    modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                    //dao
                    modelIns = (Map<String, Object>)dao.saveLcSync(modelIns);
                }
            }
            ServiceUtil.isValidReturnCode("WMSMS090", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : checkUomChange
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> checkUomChange(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
		
			Map<String, Object> modelSP = new HashMap<String, Object>();
			modelSP.put("I_TYPE", "RITEM");
			modelSP.put("I_CODE", model.get("RITEM_ID"));				
			String checkExistData = dao.checkExistData(modelSP);
			
			//System.out.println(model.get("RITEM_ID") + " ::: " + checkExistData);
			if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
				throw new BizException("재고가 존재합니다.");
            }
		
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("재고가 존재하지 않습니다."));
	
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
	            log.info(be.getMessage());
	        }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());
	
		} catch (Exception e) {
			throw e;
		}
		return m;
    }

	/**
	 * 
	 * 대체 Method ID : deletedList
	 * 대체 Method 설명 : 삭제 상품 목록 조회 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> deletedList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.deletedList(model));
		return map;
	}
	
	/**
	 * 
	 * 대체 Method ID : restoreItem 
	 * 설명 : 삭제 품목 복구 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> restoreItem(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("ITEM_ID", model.get("ITEM_ID_" + i));

                modelDt.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO)   );
                modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                dao.restoreItem(modelDt);
                dao.restoreItemDetail(modelDt);
			}
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("save.success"));
				
		} catch (Exception e) {
			if (log.isInfoEnabled()) {
				log.info(e.getMessage());
			}
			m.put("errCnt", 1);
			m.put("MSG", e.getMessage());
			
			throw e;
		}
		return m;
	}
	
	
	/**
	 * 
	 * 대체 Method ID : selectDuplicateBarcd 
	 * 설명 : 중복 바코드 조회
	 * 작성자 : ykim
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectDuplicateBarcd(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap();
		int errCnt = 0;
		try {
			m.put("LIST", dao.selectDuplicateBarcd(model));
			
		} catch (Exception e) {
			if (log.isInfoEnabled()) {
				log.info(e.getMessage());
			}
			throw e;
		}
		return m;
	}
	
	/**
     * Method ID : saveUploadItemLoc
     * Method 설명 : 엑셀 업로드 (상품 대표 로케이션 입력)
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadItemLoc(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
            	/*
            	 * 1. 상품코드, 로케이션 코드 유효성 체크
            	 * 2. 상품정보 업데이트
            	 * 3. 
            	 */
            	boolean queryCheck = true;
            	
            	for(int i = 0; i < list.size(); ++i){
            		Map<String, Object> pMap = (Map) list.get(i);
            		pMap.put("vrLcId", model.get("SS_SVC_NO"));
            		pMap.put("vrCustId", model.get("vrCustId"));
            		
            		if((int) dao.cntItemCd(pMap) < 1 || (int) dao.cntLocCd(pMap) < 1){
        			  m.put("MSG", "["+ (i+2) +"행]상품코드 : " + pMap.get("ITEM_CD") + ", 로케이션코드 : " + pMap.get("LOC_CD") + "\n확인해주시길 바랍니다.");
        			  // 엑셀 양식 2행부터 시작이라 2 부터 시작하도록 하드코딩
                      m.put("MSG_ORA", "");
                      m.put("errCnt", -1);
                      queryCheck = false;
                      break;
            		}
            	}
            	
            	if(queryCheck){
            		for(int i = 0; i < list.size(); ++i){
                		Map<String, Object> pMap = (Map) list.get(i);
                		pMap.put("vrLcId", model.get("SS_SVC_NO"));
                		pMap.put("vrCustId", model.get("vrCustId"));
                		
               		 	dao.saveUploadItemLoc(pMap);
                	}
                	
                    m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                    m.put("MSG_ORA", "");
                    m.put("errCnt", errCnt);
            	}
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
	
    /**
	 * Method ID	 : getBoxBarCd
	 * Method 설명   : 박스바코드 조회 (null, '', 1)
	 * 작성자 		 : KSJ
	 * 작성일 	     : 2022.07.11
	 * @param model
	 * @return
	 */
    @Override
    public Map<String, Object> getBoxBarCd(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.getBoxBarCd(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID      : uploadBoxBarCd
     * 대체 Method 설명    : LOT별재고현황 -> Lot 번호 box -> pcs 분해 (비즈컨설팅) 
     * 작성자              : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> uploadBoxBarCd(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	
            	for(int i = 0; i < list.size(); ++i){
            		Map<String, Object> modelDt = new HashMap<String, Object>();
        		    modelDt.put("ITEM_CD"  	, (String)((Map<String, String>)list.get(i)).get("ITEM_CD"));
                    modelDt.put("BOX_BAR_CD"  	, (String)((Map<String, String>)list.get(i)).get("BOX_BAR_CD"));
                    modelDt.put("LC_ID"  		, model.get("SS_SVC_NO"));
                    modelDt.put("CUST_ID"  		, model.get("S_CUST_ID"));
                    
                    dao.uploadBoxBarCd(modelDt);
            	}

            	m.put("errCnt", errCnt);
                m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID     : wmsms098listE1
     * 대체 Method 설명 : 추가상품목록관리 조회
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> wmsms098listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.wmsms098listE1(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID     : wmsms098saveItem
     * 대체 Method 설명 : 추가상품목록관리 등록
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> wmsms098saveItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        int selectIds = 0;
        try{
            selectIds = Integer.parseInt(model.get("ROW_COUNT").toString());
            int res = 0;
            for(int i = 0 ; i < selectIds ; i++){
                modelIns.put("LC_ID",model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("CUST_ID",model.get("vrSrchCustId"));
                modelIns.put("PARENTS_RITEM_ID", model.get("PARENTS_RITEM_ID_"+i));
                modelIns.put("CHILD_RITEM_ID", model.get("CHILD_RITEM_ID_"+i));
                modelIns.put("CHILD_EXT_QTY", model.get("CHILD_EXT_QTY_"+i));
                modelIns.put("OLD_PARENTS_RITEM_ID", model.get("OLD_PARENTS_RITEM_ID_"+i));
                modelIns.put("OLD_CHILD_RITEM_ID", model.get("OLD_CHILD_RITEM_ID_"+i));
                modelIns.put("DEL_YN", model.get("DEL_YN_"+i));
                modelIns.put("USER_NO",model.get(ConstantIF.SS_USER_NO));
                
                String gubun = (String)model.get("OLD_PARENTS_RITEM_ID_"+i);
                
                res = (gubun == null|| gubun.equals("null") || gubun.equals("")) ? (int)dao.wmsms098saveItem(modelIns) : (int)dao.wmsms098updateItem(modelIns);

            }
            
            map.put("MSG", "save.sucess");
            map.put("errCnt", 0);
            
        }catch (Exception e){
           throw e;
        }
        return map;
    }

    /**
     * @title 프로시저 pk_wmsms090e.SP_SAVE_ITEM 을 서비스코드로 작성
     * @author kih7485
     * */
    @Override
    public Map<String, Object> saveItemMasterV2(Map<String, Object> model) throws Exception {
      Map<String, Object> map = new HashMap<String, Object>();
      try{
        List<Map<String, Object>> items = (List<Map<String, Object>>) model.get("items"); 
        for (Map<String, Object> item : items) {
          String itemLcId = (String) item.get("LC_ID");
          String custId = (String) item.get("CUST_ID");
          String itemCode = (String )item.get("ITEM_CODE");
          String itemPeriodStandard = (String) item.get("BEST_DATE_TYPE");
          String tempType = (String) item.get("TEMP_TYPE");  //품온타입
          String facTempType = (String) item.get("DEV_TEMP_TYPE");//설비품온타입
          
          if(StringUtils.isEmpty(itemLcId)) {
              throw new NullPointerException("센터ID가 없습니다.");
          }
          if(StringUtils.isEmpty(custId)) {
            throw new NullPointerException("화주사ID가 없습니다.");
          }
          if(StringUtils.isEmpty(itemCode)) {
            throw new NullPointerException("상품코드가 없습니다.");
          }
          //유통기간통제기준 유효기간일 때
          if("1".equals(itemPeriodStandard)) {
            item.remove("USE_DATE_NUM");
            item.remove("USE_DATE_UNIT");
          }
          
          //유통기간통제기준 소비기간일 때
          if("2".equals(itemPeriodStandard)) {
            item.remove("BEST_DATE_NUM");
            item.remove("BEST_DATE_UNIT");
          }
          
          //품온코드 디폴트값 설정
          tempType = StringUtils.isEmpty(tempType) ? "3" : tempType; 
          facTempType = StringUtils.isEmpty(tempType) ? "3" : tempType; 
          item.put("TEMP_TYPE",tempType); 
          item.put("DEV_TEMP_TYPE",facTempType); 
          List<Map<String, Object>> itemPkgngs = (List<Map<String, Object>>) item.get("itemPkgngs"); 
          for (Map<String, Object> itemPkgng : itemPkgngs) {
            itemPkgng.put("LC_ID", item.get("LC_ID"));
          }
        }
        
        dao.saveItemsV2(model, items);
        
        map.put("MSG", "save.sucess");
        map.put("errCnt", 0);
      }catch (Exception e){
        throw e;
      }
      return map;
    }
 
    
    private void checkItemValid(Map<String, Object> item) {
      
    }

    /**
     * Method ID : saveUploadMultiItem
     * Method 설명 : 엑셀 업로드 (다중상품 입력)
     * 작성자 : HDY
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadMultiItemBarcode(Map<String, Object> model, List<Map> list) throws Exception {
    	int listBodyCnt   = list.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
            	String[] itemCd 		= new String[listBodyCnt];
            	String[] itemNm 		= new String[listBodyCnt];
            	String[] itemBarCd 		= new String[listBodyCnt];
            	String[] itemMaker 		= new String[listBodyCnt];
            	String[] uom1Cd 		= new String[listBodyCnt];
            	
            	String[] uom2Cd 		= new String[listBodyCnt];
            	String[] cvtQty 		= new String[listBodyCnt];
            	String[] defQty 		= new String[listBodyCnt];

                for(int i = 0 ; i < listBodyCnt ; i ++){
                	Map object =  list.get(i);
                	
                	itemCd[i] 		= object.containsKey("ITEM_CD") ? (String)object.get("ITEM_CD").toString().replaceAll("\"", "") : "";
                	itemNm[i] 			= object.containsKey("ITEM_NM") ? (String)object.get("ITEM_NM").toString().replaceAll("\"", "") : "";
                	itemBarCd[i] 		= object.containsKey("ITEM_BAR_CD") ? (String)object.get("ITEM_BAR_CD").toString().replaceAll("\"", "") : "";
                	itemMaker[i] 		= object.containsKey("ITEM_MAKER") ? (String)object.get("ITEM_MAKER").toString().replaceAll("\"", "") : "";
                	uom1Cd[i] 			= object.containsKey("UOM1_CD") ? (String)object.get("UOM1_CD").toString().replaceAll("\"", "") : "";
                	
                	uom2Cd[i] 			= object.containsKey("UOM2_CD") ? (String)object.get("UOM2_CD").toString().replaceAll("\"", "") : "";
                	cvtQty[i] 			= object.containsKey("CVT_QTY") ? (String)object.get("CVT_QTY").toString().replaceAll("\"", "") : "";
                	defQty[i] 			= object.containsKey("DEF_QTY") ? (String)object.get("DEF_QTY").toString().replaceAll("\"", "") : "";

                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                
                modelIns.put("I_ITEM_CD"			, itemCd);
                modelIns.put("I_ITEM_NM"			, itemNm);
                modelIns.put("I_ITEM_BAR_CD"		, itemBarCd);
                modelIns.put("I_ITEM_MAKER"			, itemMaker);
                modelIns.put("I_UOM1_CD"			, uom1Cd);
                
                modelIns.put("I_UOM2_CD"			, uom2Cd);
                modelIns.put("I_CVT_QTY"			, cvtQty);
                modelIns.put("I_DEF_QTY"			, defQty);

                modelIns.put("I_CUST_ID"    		, model.get("vrCustId"));
                modelIns.put("I_LC_ID"    			, model.get("SS_SVC_NO"));   
                modelIns.put("I_WORK_IP"  			, model.get("SS_CLIENT_IP"));  
                modelIns.put("I_USER_NO"  			, model.get("SS_USER_NO"));

                System.out.println(">>>" + model.get("chkDuplicationYn") + " // " + model.get("vrCustId"));
                //dao                
                modelIns = (Map<String, Object>)dao.saveUploadMultiItemBarcode(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : uploadEtcExcel
     * Method 설명 : 엑셀 누락값 업로드 (업데이트)
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> uploadEtcExcel(Map<String, Object> model, List<Map> list) throws Exception {
    	int listBodyCnt   = list.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
            	String[] itemCd 		= new String[listBodyCnt];
            	
            	String[] itemShortNm 		= new String[listBodyCnt];
            	String[] inZone 		= new String[listBodyCnt];
            	String[] inOutBound 		= new String[listBodyCnt];
            	String[] outZone 		= new String[listBodyCnt];
            	String[] inCustZone 		= new String[listBodyCnt];
            	
            	String[] itfCd 		= new String[listBodyCnt];
            	String[] outCustZone 		= new String[listBodyCnt];
            	String[] tagFrefix 		= new String[listBodyCnt];
            	String[] boxEpcCd 		= new String[listBodyCnt];
            	String[] itemEpcCd 		= new String[listBodyCnt];
            	
            	String[] currencyName 		= new String[listBodyCnt];
            	String[] salesPrice 		= new String[listBodyCnt];
            	String[] packingBoxType 		= new String[listBodyCnt];
            	String[] setItemType 		= new String[listBodyCnt];
            	String[] lotUseYn 		= new String[listBodyCnt];
            	
            	String[] workingTime 		= new String[listBodyCnt];
            	String[] shipAbc 		= new String[listBodyCnt];
            	String[] propStockDay 		= new String[listBodyCnt];
            	String[] weight_class 		= new String[listBodyCnt];
            	String[] bestDateNum 		= new String[listBodyCnt];
            	
            	String[] useDateNum 		= new String[listBodyCnt];
            	String[] inBestDateNum 		= new String[listBodyCnt];
            	String[] useDateUnit 		= new String[listBodyCnt];
            	String[] autoOutOrdYn 		= new String[listBodyCnt];
            	String[] bestDateUnit 		= new String[listBodyCnt];
            	
            	String[] inBestDateUnit 		= new String[listBodyCnt];
            	String[] lotPrefix 		= new String[listBodyCnt];
            	String[] autoCalPltYn 		= new String[listBodyCnt];
            	String[] reworkItmeYn 		= new String[listBodyCnt];
            	String[] devTempType 		= new String[listBodyCnt];
            	
            	String[] inLocRec 		= new String[listBodyCnt];
            	String[] unitNm 		= new String[listBodyCnt];
            	String[] fixLocZoneId 		= new String[listBodyCnt];
            	String[] itemType 		= new String[listBodyCnt];
            	
            	

                for(int i = 0 ; i < listBodyCnt ; i ++){
                	Map object =  list.get(i);
                	
                	itemCd[i]		    	= object.containsKey("ITEM_CD"          ) ? (String)object.get("ITEM_CD"          ).toString().replaceAll("\"", "") : "";	
                	
                	itemShortNm[i]			= object.containsKey("ITEM_SHORT_NM"    ) ? (String)object.get("ITEM_SHORT_NM"    ).toString().replaceAll("\"", "") : "";				
                	inZone[i]			    = object.containsKey("IN_ZONE"          ) ? (String)object.get("IN_ZONE"          ).toString().replaceAll("\"", "") : "";			
                	inOutBound[i]		  	= object.containsKey("IN_OUT_BOUND"     ) ? (String)object.get("IN_OUT_BOUND"     ).toString().replaceAll("\"", "") : "";				
                	outZone[i]			  	= object.containsKey("OUT_ZONE"         ) ? (String)object.get("OUT_ZONE"         ).toString().replaceAll("\"", "") : "";			
                	inCustZone[i]		  	= object.containsKey("IN_CUST_ZONE"     ) ? (String)object.get("IN_CUST_ZONE"     ).toString().replaceAll("\"", "") : "";				
                	
                	itfCd[i]			    = object.containsKey("ITF_CD"           ) ? (String)object.get("ITF_CD"           ).toString().replaceAll("\"", "") : "";			
                	outCustZone[i]			= object.containsKey("OUT_CUST_ZONE"    ) ? (String)object.get("OUT_CUST_ZONE"    ).toString().replaceAll("\"", "") : "";				
                	tagFrefix[i]		  	= object.containsKey("TAG_PREFIX"       ) ? (String)object.get("TAG_PREFIX"       ).toString().replaceAll("\"", "") : "";				
                	boxEpcCd[i]			  	= object.containsKey("BOX_EPC_CD"       ) ? (String)object.get("BOX_EPC_CD"       ).toString().replaceAll("\"", "") : "";			
                	itemEpcCd[i]		  	= object.containsKey("ITEM_EPC_CD"      ) ? (String)object.get("ITEM_EPC_CD"      ).toString().replaceAll("\"", "") : "";				
                	
                	currencyName[i]			= object.containsKey("CURRENCY_NAME"    ) ? (String)object.get("CURRENCY_NAME"    ).toString().replaceAll("\"", "") : "";				
                	salesPrice[i]		  	= object.containsKey("SALES_PRICE"      ) ? (String)object.get("SALES_PRICE"      ).toString().replaceAll("\"", "") : "";				
                	packingBoxType[i]		= object.containsKey("PACKING_BOX_TYPE" ) ? (String)object.get("PACKING_BOX_TYPE" ).toString().replaceAll("\"", "") : "";					
                	setItemType[i]			= object.containsKey("SET_ITEM_TYPE"    ) ? (String)object.get("SET_ITEM_TYPE"    ).toString().replaceAll("\"", "") : "";				
                	lotUseYn[i]			  	= object.containsKey("LOT_USE_YN"       ) ? (String)object.get("LOT_USE_YN"       ).toString().replaceAll("\"", "") : "";			
                	
                	workingTime[i]			= object.containsKey("WORKING_TIME"     ) ? (String)object.get("WORKING_TIME"     ).toString().replaceAll("\"", "") : "";				
                	shipAbc[i]			  	= object.containsKey("SHIP_ABC"         ) ? (String)object.get("SHIP_ABC"         ).toString().replaceAll("\"", "") : "";			
                	propStockDay[i]			= object.containsKey("PROP_STOCK_DAY"   ) ? (String)object.get("PROP_STOCK_DAY"   ).toString().replaceAll("\"", "") : "";				
                	weight_class[i]			= object.containsKey("WEIGHT_CLASS"     ) ? (String)object.get("WEIGHT_CLASS"     ).toString().replaceAll("\"", "") : "";				
                	bestDateNum[i]			= object.containsKey("BEST_DATE_NUM"    ) ? (String)object.get("BEST_DATE_NUM"    ).toString().replaceAll("\"", "") : "";				
                	
                	useDateNum[i]		  	= object.containsKey("USE_DATE_NUM"     ) ? (String)object.get("USE_DATE_NUM"     ).toString().replaceAll("\"", "") : "";				
                	inBestDateNum[i]		= object.containsKey("IN_BEST_DATE_NUM" ) ? (String)object.get("IN_BEST_DATE_NUM" ).toString().replaceAll("\"", "") : "";					
                	useDateUnit[i]			= object.containsKey("USE_DATE_UNIT"    ) ? (String)object.get("USE_DATE_UNIT"    ).toString().replaceAll("\"", "") : "";				
                	autoOutOrdYn[i]			= object.containsKey("AUTO_OUT_ORD_YN"  ) ? (String)object.get("AUTO_OUT_ORD_YN"  ).toString().replaceAll("\"", "") : "";				
                	bestDateUnit[i]			= object.containsKey("BEST_DATE_UNIT"   ) ? (String)object.get("BEST_DATE_UNIT"   ).toString().replaceAll("\"", "") : "";				
                	
                	inBestDateUnit[i]		= object.containsKey("IN_BEST_DATE_UNIT") ? (String)object.get("IN_BEST_DATE_UNIT").toString().replaceAll("\"", "") : "";					
                	lotPrefix[i]		  	= object.containsKey("LOT_PREFIX"       ) ? (String)object.get("LOT_PREFIX"       ).toString().replaceAll("\"", "") : "";				
                	autoCalPltYn[i]			= object.containsKey("AUTO_CAL_PLT_YN"  ) ? (String)object.get("AUTO_CAL_PLT_YN"  ).toString().replaceAll("\"", "") : "";				
                	reworkItmeYn[i]			= object.containsKey("REWORK_ITEM_YN"   ) ? (String)object.get("REWORK_ITEM_YN"   ).toString().replaceAll("\"", "") : "";				
                	devTempType[i]			= object.containsKey("DEV_TEMP_TYPE"    ) ? (String)object.get("DEV_TEMP_TYPE"    ).toString().replaceAll("\"", "") : "";				
                	
                	inLocRec[i]			  	= object.containsKey("IN_LOC_REC"       ) ? (String)object.get("IN_LOC_REC"       ).toString().replaceAll("\"", "") : "";			
                	unitNm[i]			    = object.containsKey("UNIT_NM"          ) ? (String)object.get("UNIT_NM"          ).toString().replaceAll("\"", "") : "";			
                	fixLocZoneId[i]			= object.containsKey("FIX_LOC_ZONE_ID"  ) ? (String)object.get("FIX_LOC_ZONE_ID"  ).toString().replaceAll("\"", "") : "";				
                	itemType[i]		  	= object.containsKey("ITEM_TYPE"       ) ? (String)object.get("ITEM_TYPE"       ).toString().replaceAll("\"", "") : "";	
                	
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                
                modelIns.put( "I_ITEM_CD" ,itemCd);                                
                
                modelIns.put( "I_ITEM_SHORT_NM"    ,itemShortNm   );
                modelIns.put( "I_IN_ZONE"          ,inZone        );
                modelIns.put( "I_IN_OUT_BOUND"     ,inOutBound    );
                modelIns.put( "I_OUT_ZONE"         ,outZone       );
                modelIns.put( "I_IN_CUST_ZONE"     ,inCustZone    );
                
                modelIns.put( "I_ITF_CD"           ,itfCd         );
                modelIns.put( "I_OUT_CUST_ZONE"    ,outCustZone   );
                modelIns.put( "I_TAG_PREFIX"       ,tagFrefix     );
                modelIns.put( "I_BOX_EPC_CD"       ,boxEpcCd      );
                modelIns.put( "I_ITEM_EPC_CD"      ,itemEpcCd     );
                
                modelIns.put( "I_CURRENCY_NAME"    ,currencyName  );
                modelIns.put( "I_SALES_PRICE"      ,salesPrice    );
                modelIns.put( "I_PACKING_BOX_TYPE" ,packingBoxType);
                modelIns.put( "I_SET_ITEM_TYPE"    ,setItemType   );
                modelIns.put( "I_LOT_USE_YN"       ,lotUseYn      );
                
                modelIns.put( "I_WORKING_TIME"     ,workingTime   );
                modelIns.put( "I_SHIP_ABC"         ,shipAbc       );
                modelIns.put( "I_PROP_STOCK_DAY"   ,propStockDay  );
                modelIns.put( "I_WEIGHT_CLASS"     ,weight_class  );
                modelIns.put( "I_BEST_DATE_NUM"    ,bestDateNum   );
                
                modelIns.put( "I_USE_DATE_NUM"     ,useDateNum    );
                modelIns.put( "I_IN_BEST_DATE_NUM" ,inBestDateNum );
                modelIns.put( "I_USE_DATE_UNIT"    ,useDateUnit   );
                modelIns.put( "I_AUTO_OUT_ORD_YN"  ,autoOutOrdYn  );
                modelIns.put( "I_BEST_DATE_UNIT"   ,bestDateUnit  );
                
                modelIns.put( "I_IN_BEST_DATE_UNIT",inBestDateUnit);
                modelIns.put( "I_LOT_PREFIX"       ,lotPrefix     );
                modelIns.put( "I_AUTO_CAL_PLT_YN"  ,autoCalPltYn  );
                modelIns.put( "I_REWORK_ITEM_YN"   ,reworkItmeYn  );
                modelIns.put( "I_DEV_TEMP_TYPE"    ,devTempType   );
                
                modelIns.put( "I_IN_LOC_REC"       ,inLocRec      );
                modelIns.put( "I_UNIT_NM"          ,unitNm        );
                modelIns.put( "I_FIX_LOC_ZONE_CD"  ,fixLocZoneId  );
                modelIns.put( "I_ITEM_TYPE"       ,itemType     );

                modelIns.put("I_LC_ID"    			, model.get("SS_SVC_NO")); 
                modelIns.put("I_CUST_ID"    		, model.get("vrCustId"));
  
                modelIns.put("I_WORK_IP"  			, model.get("SS_CLIENT_IP"));  
                modelIns.put("I_USER_NO"  			, model.get("SS_USER_NO"));
                
                //dao                
                modelIns = (Map<String, Object>)dao.uploadEtcExcel(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
