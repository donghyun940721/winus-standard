package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsms.service.WMSMS092Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS092Service")
public class WMSMS092ServiceImpl implements WMSMS092Service{
    protected Log log = LogFactory.getLog(this.getClass());

    private final static String[] CHECK_VALIDATE_WMSMS092 = {"SET_RITEM_ID", "PART_RITEM_ID"};
    
    @Resource(name = "WMSMS092Dao")
    private WMSMS092Dao dao;
    
    
    /**
     * 대체 Method ID    : list
     * 대체 Method 설명      : 세트상품 화면 데이타셋
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("UOM", dao.selectUom(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    : list
     * 대체 Method 설명      : 세트상품 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    : listE5
     * 대체 Method 설명      : 세트상품구성내역조회 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listE5(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    : listSub
     * 대체 Method 설명      : 구성상품 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listSub(model));
        return map;
    }    
    

    /**
     * 
     * 대체 Method ID    : list
     * 대체 Method 설명      : 세트상품 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listE6(model));
        return map;
    }
    
    
    /**
     * 
     * 대체 Method ID    : listE7
     * 대체 Method 설명      : 매칭정보관리
     * 작성자                        : kcr 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE7(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listE7(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveSub 22.11.01
     * 대체 Method 설명    : 구성상품 저장 프로시저
     * 작성자                      : SUMMER HYUN
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
		try {
			
			int totCnt = Integer.parseInt(model.get("selectIds").toString());
			if(totCnt>0){
				String[] setRitemCode    	  	= new String[totCnt];
		        String[] partRitemCode    		= new String[totCnt];
		        String[] uomCode    	  		= new String[totCnt];
		        String[] qty    	  			= new String[totCnt];
		        
				for (int i = 0; i < totCnt; i++) {
					setRitemCode[i] 	= (String) model.get("SET_RITEM_CODE" + i);
					partRitemCode[i] 	= (String) model.get("PART_RITEM_CODE" + i);
					uomCode[i] 			= (String) model.get("UOM_CODE" + i);
					qty[i] 				= (String) model.get("QTY" + i);
				}
				
				m.put("SET_ITEM_CODE"	, setRitemCode);
				m.put("PART_ITEM_CODE"	, partRitemCode);
				m.put("UOM_CODE"		, uomCode);
				m.put("PART_ITEM_QTY"	, qty);
				
				m.put("WORK_IP" 		, model.get(ConstantIF.SS_CLIENT_IP));
				m.put("USER_NO"   		, model.get(ConstantIF.SS_USER_NO));
		        
		        m.put("CUST_CODE"    		, model.get("CUST_ID"));          
		        m.put("LC_ID"       	, model.get("LC_ID"));

		        // PACKAGE 실행
				dao.insert(m);
				ServiceUtil.isValidReturnCode("WMSMS092", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));

			} else {
				errCnt++;
				m.put("errCnt", errCnt);
				throw new BizException(MessageResolver.getMessage("save.error"));
			}

			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
 /* 
    * 대체 Method ID   : deleteItem
    * 대체 Method 설명    : 구성상품 삭제 분리
    * 작성자                      : Summer HYUN
    * @param model
    * @return
    * @throws Exception
    */
   @Override
   public Map<String, Object> deleteItem(Map<String, Object> model) throws Exception {
   	Map<String, Object> m = new HashMap<String, Object>();
       int errCnt = 0;
       try{
           for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
               Map<String, Object> modelDt = new HashMap<String, Object>();
               modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));

               modelDt.put("selectIds"     , model.get("selectIds"));
               modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN"+i));
               modelDt.put("UOM_ID"        , model.get("UOM_ID"+i));           //화면필수
               modelDt.put("QTY"           , model.get("QTY"+i));              //화면필수
               modelDt.put("SET_RITEM_ID"  , model.get("SET_RITEM_ID"+i));     //db필수       //유효성체크
               modelDt.put("PART_RITEM_ID" , model.get("PART_RITEM_ID"+i));    //화면,db필수   //유효성체크
               modelDt.put("SET_SEQ"       , model.get("SET_SEQ"+i));          //db필수(insert시 생성)
               
               ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS092);
            
              if("DELETE".equals(model.get("ST_GUBUN"+i))){
               	dao.delete(modelDt);
               }else{
                   errCnt++;
                   m.put("errCnt", errCnt);
                   throw new BizException(MessageResolver.getMessage("save.error"));
               }
           }
           m.put("errCnt", errCnt);
           m.put("MSG", MessageResolver.getMessage("save.success"));
           
       } catch (BizException be) {
           if (log.isInfoEnabled()) {
               log.info(be.getMessage());
           }
           m.put("MSG", be.getMessage());
           
       } catch(Exception e){
           throw e;
       }
       return m;
   }
    
    
    /**
     * 대체 Method ID   : listSubExcel
     * 대체 Method 설명 : 구성상품 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSubExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listSub(model));
        
        return map;
    }
    
    /**
     * 대체 Method ID   : listSubExcelE5
     * 대체 Method 설명 : 구성상품 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSubExcelE5(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE5(model));
        
        return map;
    }
    
    /**
     * 대체 Method ID   : listSubExcelE6
     * 대체 Method 설명 : 구성상품 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSubExcelE6(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE7(model));
        
        return map;
    }
    
    
    /**
	 * Method ID : saveUploadData - old
	 * Method 설명 : 엑셀파일 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
/*	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	} */ 
    
	/**
	 * Method ID : saveUploadData 22.11.01
	 * Method 설명 : 엑셀파일 저장 
	 * 작성자 : summer H
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			Map<String, Object> paramMap = null;
			if(insertCnt>0){
				String[] setRitemCode    	  	= new String[insertCnt];
		        String[] partRitemCode    		= new String[insertCnt];
		        String[] uomCode    	  		= new String[insertCnt];
		        String[] qty    	  			= new String[insertCnt];
		        String custCode = null;
		        
				for (int i = 0; i < insertCnt; i++) {
					paramMap = (Map)list.get(i);
					setRitemCode[i] 	= (String) paramMap.get("SET_ITEM_CODE");
					partRitemCode[i] 	= (String) paramMap.get("PART_ITEM_CODE");
					uomCode[i] 			= (String) paramMap.get("UOM_CODE");
					qty[i] 				= (String) paramMap.get("PART_ITEM_QTY");
					custCode 			= (String) paramMap.get("CUST_CODE");
				}
				
				m.put("SET_ITEM_CODE"	, setRitemCode);
				m.put("PART_ITEM_CODE"	, partRitemCode);
				m.put("UOM_CODE"		, uomCode);
				m.put("PART_ITEM_QTY"	, qty);
				m.put("CUST_CODE"	, custCode);
				
				m.put("WORK_IP" 		, model.get(ConstantIF.SS_CLIENT_IP));
				m.put("USER_NO"   		, model.get(ConstantIF.SS_USER_NO));
				m.put("LC_ID"	, (String)model.get(ConstantIF.SS_SVC_NO));
		        
				dao.insert(m);
				ServiceUtil.isValidReturnCode("WMSMS092", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
				 m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			} else {
				errCnt++;
				 m.put("MSG", MessageResolver.getMessage("엑셀저장실패", new String[] { String.valueOf(insertCnt) }));
			}
			 m.put("errCnt", errCnt);
	        
			 m.put("MSG_ORA", "");
		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}
	
	/*-
	 * Method ID   : getItemSubGrid
	 * Method 설명 : 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> getItemSubGrid(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("SUB_ITEM")) {
			map.put("SUB_ITEM", dao.getItemSubGrid(model));
		}
		return map;
	}
    

    /**
     * 
     * 대체 Method ID   : saveSub
     * 대체 Method 설명    : 구성상품 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE6(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("selectIds"     , model.get("selectIds"));
                modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN"+i));
                modelDt.put("UOM_ID"        , model.get("UOM_ID"+i));           //화면필수
                modelDt.put("QTY"           , model.get("QTY"+i));              //화면필수
                modelDt.put("SET_RITEM_ID"  , model.get("SET_RITEM_ID"+i));     //db필수       //유효성체크
                modelDt.put("PART_RITEM_ID" , model.get("PART_RITEM_ID"+i));    //화면,db필수   //유효성체크
                modelDt.put("SET_SEQ"       , model.get("SET_SEQ"+i));          //db필수(insert시 생성)
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS092);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insertE6(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	//System.out.println("여기?? -- "+model.get("ST_GUBUN"+i));
                    dao.deleteE6(modelDt);
                }
//                else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
//                    dao.update(modelDt);
//                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
//                    dao.delete(modelDt);
//                }
                else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveSub
     * 대체 Method 설명    : 구성상품 저장
     * 작성자                      : summer H - 데이원 업데이트 사용 2023.02.10
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE6_2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("selectIds"     , model.get("selectIds"));
                modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN"+i));
                modelDt.put("UOM_ID"        , model.get("UOM_ID"+i));           //화면필수
                modelDt.put("QTY"           , model.get("QTY"+i));              //화면필수
                modelDt.put("SET_RITEM_ID"  , model.get("SET_RITEM_ID"+i));     //db필수       //유효성체크
                modelDt.put("PART_RITEM_ID" , model.get("PART_RITEM_ID"+i));    //화면,db필수   //유효성체크
                modelDt.put("SET_SEQ"       , model.get("SET_SEQ"+i));          //db필수(insert시 생성)
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS092);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteE6(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.updateE6(modelDt);
                }
                else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
