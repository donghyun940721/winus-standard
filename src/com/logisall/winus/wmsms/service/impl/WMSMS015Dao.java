package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS015Dao")
public class WMSMS015Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID    : list 
	 * Method 설명 : 화주/사용자 관리 > 사용자 조회 
	 * 작성자          : KSJ
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("WMSMS015.list", model);
	}
	
	/**
	 * Method ID    : listDetail 
	 * Method 설명 : 화주/사용자 관리 > 화주 조회 
	 * 작성자          : KSJ
	 * @param model
	 * @return
	 */
	public GenericResultSet listDetail(Map<String, Object> model) {
		return executeQueryPageWq("WMSMS015.listDetail", model);
	}
	
	/**
	 * Method ID    : select 
	 * Method 설명 : 화주/사용자 관리 > TMSYS093 > 화주/사용자 > 데이터 유무 조회 
	 * 작성자          : KSJ
	 * @param model
	 * @return
	 */
	public int select(Map<String, Object> model) {
		int count = (int) executeQueryForObject("WMSMS015.select", model);
		return count;
	}
	
	/**
	 * Method ID    : insert 
	 * Method 설명 : 화주/사용자 관리 > TMSYS093 > 화주/사용자 등록
	 * 작성자          : KSJ
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("WMSMS015.insert", model);
	}
	
	/**
	 * Method ID    : update 
	 * Method 설명 : 화주/사용자 관리 > TMSYS093 > 화주/사용자 수정 
	 * 작성자          : KSJ
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("WMSMS015.update", model);
	}
	
	/**
	 * Method ID    : delete 
	 * Method 설명 : 화주/사용자 관리 > TMSYS093 > 화주/사용자 삭제 
	 * 작성자          : KSJ
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("WMSMS015.delete", model);
	}
}
