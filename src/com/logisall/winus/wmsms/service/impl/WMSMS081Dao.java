package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS081Dao")
public class WMSMS081Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : list Method 설명 : ZONE정보등록 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms081.listZone", model);
	}

	/**
	 * Method ID : sublist Method 설명 : ZONE정보등록 서브 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet sublist(Map<String, Object> model) {
		return executeQueryPageWq("wmsms082.list", model);
	}

	/**
	 * Method ID : insert Method 설명 : ZONE정보등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsms081.insert", model);
	}

	/**
	 * Method ID : update Method 설명 : ZONE정보 수정 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsms081.update", model);
	}

	/**
	 * Method ID : delete Method 설명 : ZONE정보 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmsms081.delete", model);
	}

	/**
	 * Method ID : insertSub Method 설명 : ZONE정보 서브 등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insertSub(Map<String, Object> model) {
		return executeInsert("wmsms082.insert", model);
	}

	/**
	 * Method ID : updateSub Method 설명 : ZONE정보 서브 수정 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object updateSub(Map<String, Object> model) {
		return executeUpdate("wmsms082.update", model);
	}

	/**
	 * Method ID : deleteSub Method 설명 : ZONE정보 서브 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteSub(Map<String, Object> model) {
		return executeDelete("wmsms082.delete", model);
	}

	/**
	 * Method ID : selectlocchek Method 설명 : 로케이션 (중복검사) 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Integer selectlocchek(Map<String, Object> model) {
		return (Integer) executeView("wmsms082.chekCount", model);
	}

	/**
	 * Method ID : saveUploadData Method 설명 : 업로드데이터등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object saveUploadData(Map<String, Object> model) throws Exception {
		executeUpdate("wmsms081.pk_wmsms080.sp_save_excel_loc_zone", model);
        return model;
    }
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }	
}
