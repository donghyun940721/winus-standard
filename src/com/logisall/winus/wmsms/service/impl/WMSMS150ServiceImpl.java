package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS150Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS150Service")
public class WMSMS150ServiceImpl implements WMSMS150Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS150Dao")
	private WMSMS150Dao dao;

	/**
	 * 대체 Method ID : selectBox 대체 Method 설명 : 물류용기관리 화면 데이타셋 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("inKey", "CYCL01");
		map.put("CYCL01", dao.selectCYCL01(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : list 대체 Method 설명 : 순환재고조사 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.list(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : save 대체 Method 설명 : 순환재고조사 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("CYCL_STOCK_CONFIG_ID", model.get("CYCL_STOCK_CONFIG_ID" + i));
				modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
				modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO)); // LC_ID

				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {
					modelDt.put("CUST_ID", model.get("CUST_ID" + i));
					modelDt.put("CYCL_STOCK_TYPE", model.get("CYCL_STOCK_TYPE" + i));
					modelDt.put("TYPE_ST", model.get("TYPE_ST" + i));
					modelDt.put("ITEM_TY", model.get("ITEM_TY" + i));

					if (model.get("CYCL_STOCK_TYPE" + i).equals("2")) {
						modelDt.put("WORK_DAY_NUM", model.get("WORK_DAY_NUM" + i));
					} else if (model.get("CYCL_STOCK_TYPE" + i).equals("3")) {
						modelDt.put("LOC_ID", model.get("LOC_ID" + i));
					} else if (model.get("CYCL_STOCK_TYPE" + i).equals("4")) {
						modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
					}
				}

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.update(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					dao.delete_WMSST121(modelDt);
					dao.delete_WMSST120(modelDt);
					dao.delete_WMSMS150(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * 대체 Method ID : listExcel 대체 Method 설명 : 엑셀다운 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.list(model));

		return map;
	}

	/**
	 * Method ID : saveUploadData Method 설명 : 엑셀파일 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}));
			m.put("errCnt", errCnt);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}
	
	/**
     * Method ID     : itemPop
     * Method 설명       : 상품목록 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> itemPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if("20".equals(model.get("SS_USER_GB"))){
                model.put("vrSrchCustId", model.get(ConstantIF.SS_CLIENT_CD));
            }
            map.put("LIST", dao.itemPop(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}
