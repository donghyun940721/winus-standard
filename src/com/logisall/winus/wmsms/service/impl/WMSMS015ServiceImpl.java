package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsms.service.WMSMS015Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS015Service")
public class WMSMS015ServiceImpl implements WMSMS015Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS015Dao")
    private WMSMS015Dao dao;
    
    /**
     * 
     * 대체 Method ID       : list
     * 대체 Method 설명    : 화주/사용자 관리 -> 사용자 조회
     * 작성자                    : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "10000");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID       : listDetail
     * 대체 Method 설명    : 화주/사용자 관리 -> 화주 조회
     * 작성자                    : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("selectCnt") != null && !model.get("selectCnt").equals("")){
        	int selectCnt = Integer.parseInt(model.get("selectCnt").toString());
  	        String[] vrSrchCustCdSelectBox = new String[selectCnt];
  	        
  	        if(selectCnt == 1){
  	        	vrSrchCustCdSelectBox[0] =  String.valueOf(model.get("vrSrchCustCdSelectBox"));
  	        }else{
  	        	vrSrchCustCdSelectBox =  (String[]) model.get("vrSrchCustCdSelectBox");
  	        }
  	        model.put("vrSrchCustCdSelectBox", vrSrchCustCdSelectBox);
        }
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "10000");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID       : save
     * 대체 Method 설명    : 화주 <-> 사용자 저장
     * 작성자                    : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	String [] vrSrchCustIds = ((String) model.get("vrSrchCustIds")).split(",");
        	String vrSrchUserNo = (String) model.get("vrSrchUserNo");
        	
        	for(int i = 0; i < vrSrchCustIds.length; ++i){
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("vrSrchUserNo", vrSrchUserNo);
                modelIns.put("vrSrchCustId", vrSrchCustIds[i]);
                modelIns.put("SS_SVC_NO", model.get("SS_SVC_NO"));
                modelIns.put("SS_USER_NO", model.get("SS_USER_NO"));
                modelIns.put("WORK_IP", model.get("WORK_IP"));
                
                // TMSYS093 테이블에 데이터 존재 유무 확인
                int chkCnt = dao.select(modelIns);
                
                if(chkCnt > 0){ // 데이터가 있으면 UPDATE
                	dao.update(modelIns);	
                }else{ // 데이터가 없으면 INSERT
                	dao.insert(modelIns);
                }
        	}
        	
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
        	m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * 대체 Method ID       : delete
     * 대체 Method 설명    : 화주 <-> 사용자 삭제
     * 작성자                    : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	String [] vrSrchCustIds = ((String) model.get("vrSrchCustIds")).split(",");
        	String vrSrchUserNo = (String) model.get("vrSrchUserNo");
        	
        	for(int i = 0; i < vrSrchCustIds.length; ++i){
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("vrSrchUserNo", vrSrchUserNo);
                modelIns.put("vrSrchCustId", vrSrchCustIds[i]);
                modelIns.put("SS_SVC_NO", model.get("SS_SVC_NO"));
                modelIns.put("SS_USER_NO", model.get("SS_USER_NO"));
                modelIns.put("WORK_IP", model.get("WORK_IP"));
                
                // TMSYS093 테이블에 데이터 존재 유무 확인
                int chkCnt = dao.select(modelIns);
                
                if(chkCnt > 0){ // 데이터가 있으면 DEL_YN = 'Y'
                	dao.delete(modelIns);	
                }
        	}
        	
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        
        return m;
    }
}
