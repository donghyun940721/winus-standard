package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsms.service.WMSMS091Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS091Service")
public class WMSMS091ServiceImpl implements WMSMS091Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS091Dao")
    private WMSMS091Dao dao;
    
    /**
     * 대체 Method ID   : selectData
     * 대체 Method 설명 : 상품 목록 필요 데이타셋
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
//        map.put("UOM", dao.selectUom(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : list
     * 대체 Method 설명 : 기준관리 > 상품정보추가등록
     * 작성자      : KCR
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : save
     * 대체 Method 설명 : 기준관리 > 상품정보추가등록 > 저장
     * 작성자      : KCR
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
//        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("ITEM_KOR_NM"    , model.get("ITEM_KOR_NM"+i));
                modelDt.put("BOX_BAR_CD"     , model.get("BOX_BAR_CD"+i));
                modelDt.put("ITEM_BAR_CD"    , model.get("ITEM_BAR_CD"+i));
                
                
                modelDt.put("RITEM_ID"    , model.get("RITEM_ID"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);                    
                }
                else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            
            m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
  
}
