package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS095Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS095Service")
public class WMSMS095ServiceImpl implements WMSMS095Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS095Dao")
    private WMSMS095Dao dao;

    /**
     * Method ID : listItem
     * Method 설명 : 화주별거래처상품관리 상품조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.itemList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listItemGrp
     * Method 설명 : 화주별거래처상품관리 상품군조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.itemGrpList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : itemSave
     * Method 설명 : 화주별거래처상품관리 상품 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveItem(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("selectIds", model.get("selectIds"));
                modelDt.put("TRUST_CUST_ID", model.get("TRUST_CUST_ID"+i));
                modelDt.put("CUST_ID", model.get("CUST_ID"+i));
                modelDt.put("RITEM_ID", model.get("RITEM_ID"+i));
                modelDt.put("RITEM_ID_C", model.get("RITEM_ID_C"+i));
                modelDt.put("BEST_DATE_NUM", model.get("BEST_DATE_NUM"+i));
                modelDt.put("BEST_DATE_UNIT", model.get("BEST_DATE_UNIT"+i));
                modelDt.put("VAILD_FIFO_YN", model.get("VAILD_FIFO_YN"+i));
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                modelDt.put("SS_CLIENT_IP"  , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("WGT_PRIORITY", model.get("WGT_PRIORITY"+i));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    if(model.get("WGT_PRIORITY"+i).equals("1") && !"undefined".equalsIgnoreCase((String)model.get("ITEM_WGT_ARROW_RANGE"+i))){
                        modelDt.put("ITEM_WGT_ARROW_RANGE", model.get("ITEM_WGT_ARROW_RANGE"+i));
                    }else if ( !"undefined".equalsIgnoreCase((String)model.get("ITEM_WGT_ARROW_RATE"+i)) ){
                        modelDt.put("ITEM_WGT_ARROW_RATE", model.get("ITEM_WGT_ARROW_RATE"+i));
                    }                	
                	
                    int chek = dao.itemInsertSearch(modelDt);
                    if(chek < 1){
                        // 중복값이 없으면 등록
                        dao.itemInsert(modelDt);
                        m.put("MSG", MessageResolver.getMessage("insert.success"));
                        m.put("errCnt", errCnt);
                    }else{
                        // 중복값이 있는것 중에 삭제된건지 정상적으로 있는건지 체크
                        int chek2 = dao.itemInsertSearchDelYN(modelDt);
                        if(chek2 == 1){
                            // 삭제된 값이 있으면 그 값을 살리기
                            dao.itemInSertUpdate(modelDt);
                            m.put("MSG", MessageResolver.getMessage("insert.success"));
                            m.put("errCnt", errCnt);
                        }else{
                            // 중복값중에 삭제된게 아니면 중복이라고 메시지 띠우기
                            errCnt++;
                            m.put("errCnt", errCnt);
                            throw new BizException(MessageResolver.getMessage("ritemid.chek"));
                        }
                    }
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                	
                    if(model.get("WGT_PRIORITY"+i).equals("1") && !"undefined".equalsIgnoreCase((String)model.get("ITEM_WGT_ARROW_RANGE"+i))){
                        modelDt.put("ITEM_WGT_ARROW_RANGE", model.get("ITEM_WGT_ARROW_RANGE"+i));
                    }else if ( !"undefined".equalsIgnoreCase((String)model.get("ITEM_WGT_ARROW_RATE"+i)) ){
                        modelDt.put("ITEM_WGT_ARROW_RATE", model.get("ITEM_WGT_ARROW_RATE"+i));
                    }                	
                        // 중복값이 있는것 중에 삭제된건지 정상적으로 있는건지 체크
                        int chek2 = dao.itemInsertSearchDelYN(modelDt);
                        if(chek2 == 1){
                            dao.itemDelete(modelDt);
                            dao.itemUpdateUpdate(modelDt);
                            m.put("MSG", MessageResolver.getMessage("update.success"));
                            m.put("errCnt", errCnt);
                        }else{
                            String chek3 = dao.itemUpdateSearch(modelDt);
                            int chek4 = dao.itemUpdateSearch2(modelDt);
                            if(modelDt.get("RITEM_ID_C").equals(chek3)){
                                dao.itemUpdate(modelDt);
                                m.put("MSG", MessageResolver.getMessage("update.success"));
                                m.put("errCnt", errCnt);
                            }else if(chek4 == 0){
                                dao.itemUpdate(modelDt);
                                m.put("MSG", MessageResolver.getMessage("update.success"));
                                m.put("errCnt", errCnt);
                            }else{
                                errCnt++;
                                m.put("errCnt", errCnt);
                                throw new BizException(MessageResolver.getMessage("ritemid.chek"));
                            }
                        }
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.itemDelete(modelDt);
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            } 
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveItemGrp
     * Method 설명 : 화주별거래처상품관리 상품군 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("selectIds", model.get("selectIds"));
                modelDt.put("TRUST_CUST_ID", model.get("TRUST_CUST_ID"+i));
                modelDt.put("CUST_ID", model.get("CUST_ID"+i));
                modelDt.put("ITEM_GRP_ID", model.get("ITEM_GRP_ID"+i));
                modelDt.put("ITEM_GRP_ID_C", model.get("ITEM_GRP_ID_C"+i));
                modelDt.put("WGT_ARROW_RATE", model.get("WGT_ARROW_RATE"+i));
                modelDt.put("BEST_DATE_NUM", model.get("BEST_DATE_NUM"+i));
                modelDt.put("BEST_DATE_UNIT", model.get("BEST_DATE_UNIT"+i));
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                modelDt.put("SS_CLIENT_IP"  , model.get(ConstantIF.SS_CLIENT_IP));
                
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    int chek = dao.itemGrpInsertSearch(modelDt);
                    if(chek < 1){
                        // 중복값이 없으면 등록
                        dao.itemGrpInsert(modelDt);
                        m.put("MSG", MessageResolver.getMessage("insert.success"));
                        m.put("errCnt", errCnt);
                    }else{
                        // 중복값이 있는것 중에 삭제된건지 정상적으로 있는건지 체크
                        int chek2 = dao.itemGrpInsertSearchDelYN(modelDt);
                        if(chek2 == 1){
                            // 삭제된 값이 있으면 그 값을 살리기
                            dao.itemGrpInSertUpdate(modelDt);
                            m.put("MSG", MessageResolver.getMessage("insert.success"));
                            m.put("errCnt", errCnt);
                        }else{
                            // 중복값중에 삭제된게 아니면 중복이라고 메시지 띠우기
                            errCnt++;
                            m.put("errCnt", errCnt);
                            throw new BizException(MessageResolver.getMessage("itemgrp.chek"));
                        }
                    }
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    int chek = dao.itemGrpInsertSearch(modelDt);
                    if(chek < 1){
                        // 중복값이 없으면 수정
                        dao.itemGrpUpdate(modelDt);
                        m.put("MSG", MessageResolver.getMessage("update.success"));
                        m.put("errCnt", errCnt);
                    }else{
                        // 중복값이 있는것 중에 삭제된건지 정상적으로 있는건지 체크
                        int chek2 = dao.itemGrpInsertSearchDelYN(modelDt);
                        if(chek2 == 1){
                            dao.itemGrpDelete(modelDt);
                            dao.itemGrpUpdateUpdate(modelDt);
                            m.put("MSG", MessageResolver.getMessage("update.success"));
                            m.put("errCnt", errCnt);
                        }else{
                            String chek3 = dao.itemGrpUpdateSearch(modelDt);
                            int chek4 = dao.itemGrpUpdateSearch2(modelDt);
                            
                            if(modelDt.get("ITEM_GRP_ID_C").equals(chek3)){
                                dao.itemGrpUpdate(modelDt);
                                m.put("MSG", MessageResolver.getMessage("update.success"));
                                m.put("errCnt", errCnt);
                            }else if(chek4 == 0){
                                dao.itemGrpUpdate(modelDt);
                                m.put("MSG", MessageResolver.getMessage("update.success"));
                                m.put("errCnt", errCnt);
                            }else{
                                errCnt++;
                                m.put("errCnt", errCnt);
                                throw new BizException(MessageResolver.getMessage("itemgrp.chek"));
                            }
                        } 
                    }
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.itemGrpDelete(modelDt);
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            } 
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
            model.put("inKey", "YMD"); // 연월일
            map.put("YMD", dao.selectYMD(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 대체 Method ID     : listExcel
     * 대체 Method 설명        : 화주별거래처 상품 엑셀.
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.itemList(model));
        
        return map;
    }
    /**
     * 대체 Method ID     : listExcelItemGrp
     * 대체 Method 설명        : 화주별거래처 상품군 엑셀.
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.itemGrpList(model));
        
        return map;
    }
    
    
    /**
     * Method ID : saveCsvItem
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCsvItem(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.insertCsvItem(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }    
}
