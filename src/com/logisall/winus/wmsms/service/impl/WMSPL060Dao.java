package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPL060Dao")
public class WMSPL060Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : insert Method 설명 : 물류용기관리 등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmspl060.insert", model);
	}

	/**
	 * Method ID : update Method 설명 : 물류용기관리 수정 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmspl060.update", model);
	}

	/**
	 * Method ID : delete Method 설명 : 물류용기관리 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmspl060.delete", model);
	}

	/**
	 * Method ID : list Method 설명 : 물류용기관리 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return GenericResultSet
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmspl060.list", model);
	}
	
	/**
     * Method ID : list Method 설명 : 물류용기관리 조회 작성자 : 기드온
     * 
     * @param model
     * @return GenericResultSet
     */
    public GenericResultSet listPool(Map<String, Object> model) {
        return executeQueryPageWq("wmspl060.listPool", model);
    }
    
    /**
     * Method ID : insert Method 설명 : 물류용기관리 등록 작성자 : 기드온
     * 
     * @param model
     * @return Object
     */
    public Object poolInsert(Map<String, Object> model) {
        return executeInsert("wmspl060.poolInsert", model);
    }

    /**
     * Method ID : update Method 설명 : 물류용기관리 수정 작성자 : 기드온
     * 
     * @param model
     * @return Object
     */
    public Object poolUpdate(Map<String, Object> model) {
        return executeUpdate("wmspl060.poolUpdate", model);
    }

    /**
     * Method ID : delete Method 설명 : 물류용기관리 삭제 작성자 : 기드온
     * 
     * @param model
     * @return Object
     */
    public Object poolDelete(Map<String, Object> model) {
        return executeUpdate("wmspl060.poolDelete", model);
    }
	

}
