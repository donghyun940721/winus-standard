package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmsms.service.WMSMS097Service;

@Service("WMSMS097Service")
public class WMSMS097ServiceImpl implements WMSMS097Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSMS097Dao")
    private WMSMS097Dao dao;
    
    /**
     * 대체 Method ID    : selectData
     * 대체 Method 설명      : 화주별물류기기 화면 데이타셋
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
}
