
package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.tmsys.service.impl.TMSYS020Dao;
import com.logisall.winus.wmsms.service.WMSMS130Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS130Service")
public class WMSMS130ServiceImpl implements WMSMS130Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS130Dao")
    private WMSMS130Dao dao;
    
    @Resource(name = "TMSYS020Dao")
    private TMSYS020Dao tmsys020dao;
    
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트박스 생성
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("AUTH", tmsys020dao.selectAuthBox(model));   

        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : list 
     * Method 설명 : 마감관리 조회 
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * Method ID : save
     * Method 설명 : 마감관리 저장
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            
            int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            if(iuCnt > 0){
                String[] closeId = new String[iuCnt];      // 마감정보ID
                String[] workType = new String[iuCnt];     // 작업종류
                String[] lcId = new String[iuCnt];         // LC_ID
                String[] custId = new String[iuCnt];       // 화주id
                String[] authCd = new String[iuCnt];       // 권한
                String[] closeDt = new String[iuCnt];      // 마감일자
                String[] startMm = new String[iuCnt];      // 시작월
                String[] startDd = new String[iuCnt];      // 시작일
                String[] endMm = new String[iuCnt];        // 종료월
                String[] endDd = new String[iuCnt];        // 종료일
                String[] closeGb = new String[iuCnt];      // 마감구분
                String[] autoYn = new String[iuCnt];       // 자동여부
                String[] allowDay = new String[iuCnt];     // 허용일수
                String[] closeMm = new String[iuCnt];      // 마감월
                String[] closeDd = new String[iuCnt];      // 마감일
                String[] delYn = new String[iuCnt];        // 삭제여부
                
                String userNo = "";               
                String workIp = "";
                
                for(int i = 0 ; i < iuCnt ; i ++){
                    closeId[i]     = (String)model.get("CLOSE_ID"+i);
                    workType[i]    = (String)model.get("WORK_TYPE"+i);
                    lcId[i]        = (String)model.get(ConstantIF.SS_SVC_NO);
                    custId[i]      = (String)model.get("CUST_ID"+i);
                    authCd[i]      = (String)model.get("AUTH_CD"+i);
                    closeDt[i]     = (String)model.get("CLOSE_DT"+i);
                    startMm[i]     = (String)model.get("START_MM"+i);
                    startDd[i]     = (String)model.get("START_DD"+i);
                    endMm[i]       = (String)model.get("END_MM"+i);
                    endDd[i]       = (String)model.get("END_DD"+i);
                    closeGb[i]     = (String)model.get("CLOSE_GB"+i);
                    autoYn[i]      = (String)model.get("AUTO_YN"+i);
                    allowDay[i]    = (String)model.get("ALLOW_DAY"+i);
                    closeMm[i]     = (String)model.get("CLOSE_MM"+i);
                    closeDd[i]     = (String)model.get("CLOSE_DD"+i);
                    delYn[i]       = "N";              
                }
                userNo = (String)model.get(ConstantIF.SS_USER_NO);
                workIp = (String)model.get(ConstantIF.SS_CLIENT_IP);
                    
                Map<String, Object> modelSP = new HashMap<String, Object>();
                modelSP.put("I_CLOSE_ID",       closeId);
                modelSP.put("I_WORK_TYPE",      workType);
                modelSP.put("I_LC_ID",          lcId);
                modelSP.put("I_CUST_ID",        custId);
                modelSP.put("I_AUTH_CD",        authCd);
                modelSP.put("I_CLOSE_DT",       closeDt);
                modelSP.put("I_START_MM",       startMm);
                modelSP.put("I_START_DD",       startDd);
                modelSP.put("I_END_MM",         endMm);
                modelSP.put("I_END_DD",         endDd);
                modelSP.put("I_CLOSE_GB",       closeGb);
                modelSP.put("I_AUTO_YN",        autoYn);
                modelSP.put("I_ALLOW_DAY",      allowDay);
                modelSP.put("I_CLOSE_MM",       closeMm);
                modelSP.put("I_CLOSE_DD",       closeDd);
                modelSP.put("I_DEL_YN",         delYn);
                modelSP.put("I_USER_NO",        userNo);
                modelSP.put("I_WORK_IP",        workIp);
              
                String[] val = dao.saveCloseInfo(modelSP);                    
                if(!val[1].equals("0")){
                        errCnt++;
                        m.put("MSG", val[0]);
                }
            }
            
            if(delCnt > 0){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                for(int i = 0 ; i < delCnt ; i ++){
                    modelDt.put("CLOSE_ID",     model.get("CLOSE_ID" + i));
                    modelDt.put("ST_GUBUN",     model.get("D_ST_GUBUN" + i));
                }
                modelDt.put("UPD_NO", model.get("SS_USER_NO"));
                modelDt.put("LC_ID", model.get("SS_SVC_NO"));
                if("DELETE".equals(modelDt.get("ST_GUBUN"))){
                    dao.delete(modelDt);
                }
            }
            
            if(errCnt == 0){
                m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("errCnt", errCnt);
            }else{                
                if(m.get("MSG").equals("")){
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                }
                m.put("errCnt", errCnt);
            }                
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 마감관리 엑셀다운
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }

}
