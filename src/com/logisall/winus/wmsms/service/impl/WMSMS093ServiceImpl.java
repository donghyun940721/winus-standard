package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;

import com.logisall.winus.wmsms.service.WMSMS093Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS093Service")
public class WMSMS093ServiceImpl implements WMSMS093Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSMS093Dao")
    private WMSMS093Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명 : 상품별물류기기 조회
     * 작성자           : kwt
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("ITEM_GRP_TYPE", "G");
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명 : 상품별물류기기 저장
     * 작성자           : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String gb;
        int runChek = 0;
        int errCnt = 0;
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                gb = model.get("PROCESS_GB"+i).toString();
                if(model.get("TAG_LC_ID"+i).toString().length() == 0 || model.get("TAG_CUST_ID"+i).toString().length() == 0 || model.get("TAG_RITEM_ID"+i).toString().length() == 0){
                	runChek = 1;
                }
                
                modelDt.put("INSERT_ORD_TIME", model.get("INSERT_ORD_TIME"+i));
                modelDt.put("INSERT_ORD_QTY",  model.get("INSERT_ORD_QTY"+i));
                modelDt.put("INSERT_ORD_CNT",  model.get("INSERT_ORD_CNT"+i));

                modelDt.put("START_TIME",      model.get("START_TIME"+i));
                modelDt.put("STOP_TIME",       model.get("STOP_TIME"+i));
                modelDt.put("END_TIME",        model.get("END_TIME"+i));
                modelDt.put("BEFORE_END_TIME", model.get("BEFORE_END_TIME"+i));

                modelDt.put("SRC_LC_ID",       model.get("SRC_LC_ID"+i));
                modelDt.put("SRC_CUST_ID",     model.get("SRC_CUST_ID"+i));
                modelDt.put("SRC_RITEM_ID",    model.get("SRC_RITEM_ID"+i));
                modelDt.put("TAG_LC_ID",       model.get("TAG_LC_ID"+i));
                modelDt.put("TAG_CUST_ID",     model.get("TAG_CUST_ID"+i));
                modelDt.put("TAG_RITEM_ID",    model.get("TAG_RITEM_ID"+i));
                
                modelDt.put("GROUPING_ZONE1",  model.get("GROUPING_ZONE1"+i));
                modelDt.put("GROUPING_ZONE2",  model.get("GROUPING_ZONE2"+i));

                modelDt.put("PROCESS_GB",      model.get("PROCESS_GB"+i));
                
                if(gb.equals("START")){ 		 // START
                	if(runChek == 1){
                		// insert
                		dao.start_insert(modelDt);
                	}else if(runChek == 0){
                		// update
                		dao.start_update(modelDt);
                	}
                }else if(gb.equals("STOP")){  // STOP
                    dao.stop_update(modelDt);
                }else if(gb.equals("END")){   // END
                    dao.end_update(modelDt);
                }else {
                  errCnt++;
                  m.put("errCnt", errCnt);
                  throw new BizException(MessageResolver.getMessage("save.error"));
                }

            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));

        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());

        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : autoOrderInsert
     * 대체 Method 설명    : 주문입력
     * 작성자                      : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> autoOrderInsert(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            String[] lcId    	  = new String[totCnt];
            String[] ritemId 	  = new String[totCnt];
            String[] custLotNo 	  = new String[totCnt];
            String[] insertOrdQty = new String[totCnt];
           
            Map<String, Integer> pdaSeqMap = new HashMap<String, Integer>();
            for(int i = 0 ; i < totCnt ; i++){    
            	lcId[i]    		= (String)model.get("LC_ID"+i);
            	ritemId[i] 		= (String)model.get("RITEM_ID"+i);
            	custLotNo[i] 	= (String)model.get("CUST_LOT_NO"+i);
            	insertOrdQty[i] = (String)model.get("INSERT_ORD_QTY"+i);
            	
                Map<String, Object> modelIns = new HashMap<String, Object>();

                modelIns.put("LC_ID"        	, lcId);
                modelIns.put("RITEM_ID" 		, ritemId);
                modelIns.put("CUST_LOT_NO" 		, custLotNo);
                modelIns.put("INSERT_ORD_QTY" 	, insertOrdQty);
                modelIns.put("SS_CLIENT_IP" 	, model.get(ConstantIF.SS_CLIENT_IP));
            	modelIns.put("SS_USER_NO"   	, model.get(ConstantIF.SS_USER_NO));    
                
            	//System.out.println(">>> "+i + " :: " + "LCID : " + (String)model.get("LC_ID"+i)+ " RITEMID : " + (String)model.get("RITEM_ID"+i));
            	
                modelIns = (Map<String, Object>)dao.autoOrderInsert(modelIns);
                ServiceUtil.isValidReturnCode("WMSMS093", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
