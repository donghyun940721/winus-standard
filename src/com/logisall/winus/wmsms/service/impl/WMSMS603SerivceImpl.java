package com.logisall.winus.wmsms.service.impl;

import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsms.service.WMSMS603Service;
import com.lowagie.text.pdf.codec.Base64.OutputStream;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSMS603Service")
public class WMSMS603SerivceImpl extends AbstractServiceImpl implements WMSMS603Service{

	@Resource(name = "WMSMS603Dao")
    private WMSMS603Dao dao;
	
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        GenericResultSet res = dao.list(model);
        List list = res.getList();
        map.put("LIST", res);
        return map;
    }
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 dao.updateDetail(model);
    	 dao.updateOut_loc_rec(model);
    	 dao.insertHistory(model);
         return m;
    }
    
    
    @Override
    public Map<String, Object> print_zebra(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 
         // 프린터의 IP 주소와 포트 번호
         String printerIp = "192.168.1.100"; // Zebra 프린터의 IP 주소
         int printerPort = 9100; // Zebra 프린터의 기본 포트

         // 출력할 ZPL 명령
         String zplCommand = 
             "^XA\n" + // 시작 명령
             "^FO50,50\n" + // 시작 위치 (x=50, y=50)
             "^A0N,50,50\n" + // 글꼴 설정 (A0 폰트, 크기 50x50)
             "^FDHello, Zebra!\n" + // 출력할 텍스트
             "^XZ"; // 종료 명령

         try (Socket socket = new Socket(printerIp, printerPort);
              OutputStream outputStream = (OutputStream) socket.getOutputStream()) {
             // ZPL 명령어 전송
             outputStream.write(zplCommand.getBytes());
             outputStream.flush();
             System.out.println("ZPL 명령 전송 완료");
         } catch (Exception e) {
             e.printStackTrace();
             System.out.println("프린터 연결 실패 또는 ZPL 전송 실패");
         }
    	 
         return m;
    }
}
