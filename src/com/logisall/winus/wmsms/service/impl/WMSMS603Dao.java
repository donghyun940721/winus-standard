package com.logisall.winus.wmsms.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS603Dao")
public class WMSMS603Dao extends SqlMapAbstractDAO{
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms603.list", model);
    }
    
    public Object updateDetail(Map<String, Object> model) {
        return executeUpdate("wmsms603.updateDetail", model);
    }
    
    public Object updateOut_loc_rec(Map<String, Object> model) {
        return executeUpdate("wmsms603.updateOut_loc_rec", model);
    }
    
    public Object insertHistory(Map<String, Object> model) {
    	return executeInsert("wmsms603.insertHistory", model);
    }
}
