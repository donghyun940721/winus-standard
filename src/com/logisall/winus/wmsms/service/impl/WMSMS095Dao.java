package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS095Dao")
public class WMSMS095Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 화주 정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms095.search", model);
    }   
    /**
     * Method ID : itemList
     * Method 설명 : 상품 목록 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet itemList(Map<String, Object> model) {
        return executeQueryPageWq("wmsms095.itemSearch", model);
    }   
    
    /**
     * Method ID : itemGrpList
     * Method 설명 : 상품군 목록 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet itemGrpList(Map<String, Object> model) {
        return executeQueryPageWq("wmsms095.itemGrpSearch", model);
    }   
    
    /**
     * Method ID    : itemInsert
     * Method 설명      : 상품 목록  등록
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object itemInsert(Map<String, Object> model) {
        return executeInsert("wmsms095.itemInsert", model);
    } 
    
    /**
     * Method ID    : itemGrpInsert
     * Method 설명      : 상품 목록군  등록
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object itemGrpInsert(Map<String, Object> model) {
        return executeInsert("wmsms095.itemGrpInsert", model);
    } 
    
    /**
     * Method ID    : itemDelete
     * Method 설명      : 상품 목록 삭제
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemDelete(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemDelete", model);
    }  
    
    /**
     * Method ID    : itemGrpDelete
     * Method 설명      : 상품군 목록 삭제
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemGrpDelete(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemGrpDelete", model);
    }  
    /**
     * Method ID    : itemInSertUpdate
     * Method 설명      : 상품 목록 등록 -> 수정 (삭제된거 살리기)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemInSertUpdate(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemInSertUpdate", model);
    }  
    
    /**
     * Method ID    : itemGrpInSertUpdate
     * Method 설명      : 상품군 목록 등록 -> 수정 (삭제된거 살리기)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemGrpInSertUpdate(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemGrpInSertUpdate", model);
    }  
    /**
     * Method ID    : itemUpdateUpdate
     * Method 설명      : 상품 목록 수정 -> 수정 (삭제된거 살리기)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemUpdateUpdate(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemUpdateUpdate", model);
    }  
    /**
     * Method ID    : itemGrpUpdateUpdate
     * Method 설명      : 상품군 목록 수정 -> 수정 (삭제된거 살리기)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemGrpUpdateUpdate(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemGrpUpdateUpdate", model);
    }  
    
    /**
     * Method ID    : itemUpdate
     * Method 설명      : 상품 목록 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemUpdate(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemUpdate", model);
    }  
    /**
     * Method ID    : itemGrpUpdate
     * Method 설명      : 상품군 목록 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object itemGrpUpdate(Map<String, Object> model) {
        return executeUpdate("wmsms095.itemGrpUpdate", model);
    }  
    /**
     * Method ID : selectYMD
     * Method 설명 : 연월일 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectYMD(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    /**
     * Method ID    : itemInsertSearch
     * Method 설명      : 상품 목록 등록 (중복검사)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Integer itemInsertSearch(Map<String, Object> model){
        return (Integer) executeView("wmsms095.itemInsertSearch", model);
    }
    /**
     * Method ID    : itemUpdateSearch
     * Method 설명      : 상품 목록 수정 (중복검사)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public String itemUpdateSearch(Map<String, Object> model){
        return (String) executeView("wmsms095.itemUpdateSearch", model);
    }
    /**
     * Method ID    : itemUpdateSearch2
     * Method 설명      : 상품 목록 등록 (중복검사)2
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Integer itemUpdateSearch2(Map<String, Object> model){
        return (Integer) executeView("wmsms095.itemUpdateSearch2", model);
    }
    
    /**
     * Method ID    : itemGrpUpdateSearch
     * Method 설명      : 상품군 목록 수정 (중복검사)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public String itemGrpUpdateSearch(Map<String, Object> model){
        return (String) executeView("wmsms095.itemGrpUpdateSearch", model);
    }
    /**
     * Method ID    : itemGrpUpdateSearch2
     * Method 설명      : 상품군 목록 등록 (중복검사)2
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Integer itemGrpUpdateSearch2(Map<String, Object> model){
        return (Integer) executeView("wmsms095.itemGrpUpdateSearch2", model);
    }
    
    /**
     * Method ID    : itemInsertSearchDelYN
     * Method 설명      : 상품 목록 등록 (중복검사-삭제검사)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Integer itemInsertSearchDelYN(Map<String, Object> model){
        return (Integer) executeView("wmsms095.itemInsertSearchDel_YN", model);
    }
    
    /**
     * Method ID    : itemGrpInsertSearch
     * Method 설명      : 상품군 목록 등록 (중복검사)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Integer itemGrpInsertSearch(Map<String, Object> model){
        return (Integer) executeView("wmsms095.itemGrpInsertSearch", model);
    }

    /**
     * Method ID    : itemGrpInsertSearchDelYN
     * Method 설명      : 상품군 목록 등록 (중복검사-삭제검사)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Integer itemGrpInsertSearchDelYN(Map<String, Object> model){
        return (Integer) executeView("wmsms095.itemGrpInsertSearchDel_YN", model);
    }  
    
    /**
     * Method ID : insertCsv
     * Method 설명 : 상품 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertCsvItem(Map<String, Object> model, List list) throws Exception {
    		
    		SqlMapClient sqlMapClient = getSqlMapClient();
    		try {
    			sqlMapClient.startTransaction();
    			Map<String, Object> paramMap = null;
	    		for (int i=0;i<list.size();i++) {
	    			paramMap = (Map)list.get(i);
	    			    			
	    			if ( 
	    					(paramMap.get("TRUST_CUST_ID") != null && StringUtils.isNotEmpty( paramMap.get("TRUST_CUST_ID").toString()) )
	    					&& (paramMap.get("CUST_ID") != null && StringUtils.isNotEmpty( paramMap.get("CUST_ID").toString()) ) 
	    					&& (paramMap.get("RITEM_ID") != null && StringUtils.isNotEmpty( paramMap.get("RITEM_ID").toString()) ) 
	    					
	    					) {
	    				paramMap.put("SS_USER_NO", 		model.get("SS_USER_NO"));
		    			paramMap.put("SS_SVC_NO", 		model.get("SS_SVC_NO"));
		    			paramMap.put("SS_CLIENT_IP", 	model.get("SS_CLIENT_IP"));
		    			
		    			sqlMapClient.insert("wmsms095.itemInsert", paramMap);

	    			}
	    		}
	    		sqlMapClient.endTransaction();
	    		
    		} catch(Exception e) {
    			e.printStackTrace();
    			throw e;
    			
    		} finally {
    			if (sqlMapClient != null) {
    				sqlMapClient.endTransaction();
    			}
    		}
    }     
}
