package com.logisall.winus.wmsms.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.engine.execution.BatchException;
import com.ibatis.sqlmap.engine.execution.BatchResult;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS090Dao")
public class WMSMS090Dao extends SqlMapAbstractDAO {
	private final Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회 작성자 :
	 * chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object selectItemGrp(Map<String, Object> model) {
		return executeQueryForList("wmsms094.selectItemGrp", model);
	}

	/**
	 * Method ID : selectUom Method 설명 : 신규생성시 LCID마다 다른 UOM 필요데이터 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object selectUom(Map<String, Object> model) {
		return executeQueryForList("wmsms100.selectUom", model);
	}

	/**
	 * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listItem(Map<String, Object> model) {
		return executeQueryPageWq("wmsms091.listItem", model);
	}

	/**
	 * Method ID : saveItem Method 설명 : 상품정보 저장 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object saveItem(Map<String, Object> model) {
		return executeUpdate("wmsms090.pk_wmsms090e.sp_save_item", model);
	}

	/**
	 * Method ID : deleteItem Method 설명 : 상품정보삭제시 wmsms090 삭제 (실질적은 delyn -> y 로 변경)
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteItem(Map<String, Object> model) {
		return executeUpdate("wmsms090.delete", model);
	}

	/**
	 * Method ID : deleteRitem Method 설명 : 상품정보삭제시 wmsms091 삭제 (실질적은 delyn -> y 로
	 * 변경) 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteRitem(Map<String, Object> model) {
		return executeUpdate("wmsms091.delete", model);
	}

	/**
	 * Method ID : listUom Method 설명 : UOM환산이력 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listUom(Map<String, Object> model) {
		return executeQueryPageWq("wmsms101.listUom", model);
	}

	/**
	 * Method ID : insertUom Method 설명 : UOM환산이력 등록 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insertUom(Map<String, Object> model) {
		return executeInsert("wmsms101.insert", model);
	}

	/**
	 * Method ID : updateUom Method 설명 : UOM환산이력 수정 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateUom(Map<String, Object> model) {
		return executeUpdate("wmsms101.update", model);
	}

	/**
	 * Method ID : deleteUom Method 설명 : UOM환산이력 삭제 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteUom(Map<String, Object> model) {
		return executeUpdate("wmsms101.delete", model);
	}

	/**
	 * Method ID : insertUom2 Method 설명 : UOM환산이력 등록 Ritem을 db쿼리에서 찾아서 쓰는방법 작성자 :
	 * chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insertUom2(Map<String, Object> model) {
		return executeInsert("wmsms101.insert2", model);
	}

	/**
	 * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if (paramMap.get("ITEM_KOR_NM") != null && StringUtils.isNotEmpty((String) paramMap.get("ITEM_KOR_NM"))
						&& paramMap.get("ITEM_CODE") != null
						&& StringUtils.isNotEmpty((String) paramMap.get("ITEM_CODE")) && paramMap.get("CUST_ID") != null
						&& StringUtils.isNotEmpty((String) paramMap.get("CUST_ID"))
						&& paramMap.get("REP_UOM_ID") != null
						&& StringUtils.isNotEmpty((String) paramMap.get("REP_UOM_ID"))
						&& paramMap.get("SET_ITEM_YN") != null
						&& StringUtils.isNotEmpty((String) paramMap.get("SET_ITEM_YN"))
						&& paramMap.get("UOM_QTY_STAN") != null
						&& StringUtils.isNotEmpty((String) paramMap.get("UOM_QTY_STAN"))
						&& paramMap.get("UOM_CD_STAN") != null
						&& StringUtils.isNotEmpty((String) paramMap.get("UOM_CD_STAN"))
						&& paramMap.get("UOM_CD_TARGET") != null
						&& StringUtils.isNotEmpty((String) paramMap.get("UOM_CD_TARGET"))) {
					/* ITEM_CODE 중복체크가(chkDuplicationYn) Y 이면 : 조회 체크 */
					if ("Y".equals(model.get("chkDuplicationYn"))) {
						String existRsSet = (String) sqlMapClient.queryForObject("wmsms090.selectExistItemCodeDup",
								paramMap);
						if (existRsSet == null && StringUtils.isEmpty(existRsSet)) {
							/* LC_ID, CUST_ID, ITEM_CODE 기준 조회값이 없으면 : 중복이 없으면 : 신규등록 */
							String itemId = (String) sqlMapClient.insert("wmsms090.insertUploadData", paramMap);
							paramMap.put("ITEM_ID", itemId);

							String ritemId = (String) sqlMapClient.insert("wmsms091.insertUploadData", paramMap);
							paramMap.put("RITEM_ID", ritemId);

							sqlMapClient.insert("wmsms090.insertUploadUomData", paramMap);
						} else {
							System.out.println(">> duplication itemcode : " + existRsSet);
						}
					} else {
						String itemId = (String) sqlMapClient.insert("wmsms090.insertUploadData", paramMap);
						paramMap.put("ITEM_ID", itemId);

						String ritemId = (String) sqlMapClient.insert("wmsms091.insertUploadData", paramMap);
						paramMap.put("RITEM_ID", ritemId);

						sqlMapClient.insert("wmsms090.insertUploadUomData", paramMap);
					}
				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}

	/*-
	 * Method ID : checkExistData
	 * Method 설명 : 기준정보 삭제 가능여부 확인
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	public String checkExistData(Map<String, Object> model) {
		return (String) executeView("wmsms030.selectExistData", model);
	}

	/**
	 * Method ID : overapCheck 작성자 : wdy
	 * 
	 * @param model
	 * @return
	 */
	public Object overapCheck(Map<String, Object> model) {
		return executeQueryForList("wmsms090.overapCheck", model);
	}

	/**
	 * Method ID : fileUpload Method 설명 : 파일을 업로드 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object fileUpload(Map<String, Object> model) {
		return executeInsert("tmsys900.fileInsert", model);
	}

	/**
	 * Method ID : insert Method 설명 : 통합 HelpDesk 등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insertInfo(Map<String, Object> model) {
		return executeInsert("wmsms090.itemImgInfoSave", model);
	}

	/**
	 * Method ID : insertValidate Method 설명 : 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insertValidate(Map<String, Object> model) {
		return executeQueryForList("wmsms090.insertValidate", model);
	}

	/**
	 * Method ID : saveUploadDataPk - 엑셀입력 템플릿 저장 Method 설명 : 템플릿 상품 저장 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object saveUploadDataPk(Map<String, Object> model) {
		executeUpdate("wmsms090.pk_wmsms090e.sp_save_excel_simp_item", model);
		return model;
	}

	/**
	 * Method ID : saveLcSync Method 설명 : 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object saveLcSync(Map<String, Object> model) {
		executeUpdate("wmsms090.pk_wmsms090.sp_save_multi_lc_item", model);
		return model;
	}

	public Object deletedList(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsms090.deletedList", model);
		genericResultSet.setList(list);
		return genericResultSet;
	}

	/**
	 * Method ID : restoreItem Method 설명 : 삭제된 상품 재등록 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 */
	public Object restoreItem(Map<String, Object> model) {
		executeUpdate("wmsms090.restoreItem", model);
		return model;
	}

	/**
	 * Method ID : restoreItem Method 설명 : 삭제된 상품 재등록 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 */
	public Object restoreItemDetail(Map<String, Object> model) {
		executeUpdate("wmsms090.restoreItemDetail", model);
		return model;
	}

	/**
	 * Method ID : selectDuplicateBarcd Method 설명 : 중복 바코드 조회 작성자 : ykim
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet selectDuplicateBarcd(Map<String, Object> model) {
		return executeQueryWq("wmsms090.selectDuplicateBarcd", model);
	}

	/**
	 * Method ID : pop3List Method 설명 : 다중 바코드 조회 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet pop3List(Map<String, Object> model) {
		return executeQueryPageWq("wmsms090.pop3List", model);
	}

	/**
	 * Method ID : pop3BarList Method 설명 : 다중 바코드 등록 조회 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public List pop3BarList(Map<String, Object> model) {
		return executeQueryForList("wmsms090.pop3BarList", model);
	}

	/**
	 * Method ID : pop3Insert Method 설명 : 다중 바코드 등록 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object pop3Insert(Map<String, Object> model) {
		return executeInsert("wmsms090.pop3Insert", model);
	}

	/**
	 * Method ID : pop3Update Method 설명 : 다중 바코드 수정 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object pop3Update(Map<String, Object> model) {
		return executeUpdate("wmsms090.pop3Update", model);
	}

	/**
	 * Method ID : pop3Delete Method 설명 : 다중 바코드 삭제 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object pop3Delete(Map<String, Object> model) {
		return executeUpdate("wmsms090.pop3Delete", model);
	}

	/**
	 * Method ID : pop3Sync Method 설명 : 다중 바코드 동기화 작성자 : dhkim
	 * 
	 * @param model
	 * @return
	 */
	public Object pop3Sync(Map<String, Object> model) {
		return executeInsert("wmsms090.pop3Sync", model);
	}

	/**
	 * Method ID : saveUploadItemLoc Method 설명 : 상품 대표로케이션 입력 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object saveUploadItemLoc(Map<String, Object> model) throws Exception {
		return executeUpdate("wmsms090.updateItemLoc", model);
	}

	/**
	 * Method ID : cntItemCd Method 설명 : 상품코드 존재 확인 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object cntItemCd(Map<String, Object> model) {
		return executeQueryForObject("wmsms090.cntItemCd", model);
	}

	/**
	 * Method ID : cntLocCd Method 설명 : 로케이션 존재 확인 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object cntLocCd(Map<String, Object> model) {
		return executeQueryForObject("wmsms090.cntLocCd", model);
	}

	/**
	 * Method ID : getBoxBarCd Method 설명 : 박스바코드 조회 (null, '', 1) 작성자 : KSJ 작성일 :
	 * 2022.07.11
	 * 
	 * @param model
	 * @return
	 */
	public Object getBoxBarCd(Map<String, Object> model) {
		return executeQueryForList("wmsms090.getBoxBarCd", model);
	}

	/**
	 * Method ID : uploadBoxBarCd Method 설명 : 박스바코드 변경 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object uploadBoxBarCd(Map<String, Object> model) {
		return executeUpdate("wmsms090.updateBoxBarCd", model);
	}

	/**
	 * Method ID : wmsms098listE1 Method 설명 : 추가상품목록관리 조회 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public List wmsms098listE1(Map<String, Object> model) {
		return executeQueryForList("wmsms090.wmsms098listE1", model);
	}

	/**
	 * Method ID : wmsms098saveItem Method 설명 : 추가상품목록관리 등록 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public int wmsms098saveItem(Map<String, Object> model) {
		return executeUpdate("wmsms090.wmsms098saveItem", model);
	}

	/**
	 * Method ID : wmsms098updateItem Method 설명 : 추가상품목록관리 업데이트 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public int wmsms098updateItem(Map<String, Object> model) {
		return executeUpdate("wmsms090.wmsms098updateItem", model);
	}

	public void saveItemsV2(Map<String, Object> model, List<Map<String, Object>> itemList) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		int batchSize = 300; // Batch size to avoid ORA-01000
		int insertCnt = 0;
		int updateCnt = 0;

		try {
			sqlMapClient.startTransaction();
			sqlMapClient.startBatch();

			for (int i = 0; i < itemList.size(); i++) {
				Map<String, Object> item = itemList.get(i);
				processItem(sqlMapClient, item);

				if (i > 0 && i % batchSize == 0) {
					sqlMapClient.executeBatch();
					sqlMapClient.startBatch(); // Reset batch to avoid ORA-01000
				}
			}

			sqlMapClient.executeBatch();
			sqlMapClient.commitTransaction();
		} catch (Exception e) {
			log.error(e);
			logFailedBatch(itemList, itemList.size() - (itemList.size() % batchSize), itemList.size());
			throw e;
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}

		log.info("Insert count: " + insertCnt + ", Update count: " + updateCnt);
	}

	private void processItem(SqlMapClient sqlMapClient, Map<String, Object> item) throws SQLException {
		Map<String, Object> existItem = (Map<String, Object>) sqlMapClient
				.queryForObject("wmsms090.selectExistItemData", item);
		boolean insertItemFlag = existItem == null;

		if (insertItemFlag) {
			handleInsertItem(sqlMapClient, item);
		} else {
			handleUpdateItem(sqlMapClient, item, existItem);
		}

		processItemUoms(sqlMapClient, item);
	}

	private void handleInsertItem(SqlMapClient sqlMapClient, Map<String, Object> item) throws SQLException {
		String itemId = (String) sqlMapClient.insert("wmsms090.insertUploadData", item);
		item.put("ITEM_ID", itemId);
		item.put("TIME_PERIOD_DAY", 0);

		String ritemId = (String) sqlMapClient.insert("wmsms091.insertUploadData", item);
		item.put("RITEM_ID", ritemId);
		item.put("DEL_YN", "N");
	}

	private void handleUpdateItem(SqlMapClient sqlMapClient, Map<String, Object> item, Map<String, Object> existItem)
			throws SQLException {
		item.put("RITEM_ID", existItem.get("RITEM_ID"));
		item.put("ITEM_ID", existItem.get("ITEM_ID"));
		item.put("USE_YN", "Y");
		item.put("DEL_YN", "N");
		sqlMapClient.update("wmsms090.updateUploadData", item);
		sqlMapClient.update("wmsms091.updateUploadData", item);
	}

	private void processItemUoms(SqlMapClient sqlMapClient, Map<String, Object> item) throws SQLException {
		List<Map<String, Object>> itemUomList = (List<Map<String, Object>>) item.get("itemPkgngs");

		for (Map<String, Object> itemUom : itemUomList) {
			itemUom.put("RITEM_ID", item.get("RITEM_ID"));
			Map<String, Object> existItemUom = (Map<String, Object>) sqlMapClient
					.queryForObject("wmsms090.selectExistUomData", itemUom);
			boolean insertItemUomFlag = existItemUom == null;

			if (insertItemUomFlag) {
				handleInsertItemUom(sqlMapClient, itemUom, item);
			} else {
				handleUpdateItemUom(sqlMapClient, itemUom, existItemUom);
			}
		}
	}

	private void handleInsertItemUom(SqlMapClient sqlMapClient, Map<String, Object> itemUom, Map<String, Object> item)
			throws SQLException {
		itemUom.put("UOM_CD", itemUom.get("UOM1_CD"));
		String uomCdStan = (String) sqlMapClient.queryForObject("wmsms090.selectUomId", itemUom);
		itemUom.put("UOM_CD", itemUom.get("UOM2_CD"));
		String uomCdTarget = (String) sqlMapClient.queryForObject("wmsms090.selectUomId", itemUom);

		itemUom.put("UOM_CD_STAN", uomCdStan);
		itemUom.put("UOM_CD_TARGET", uomCdTarget);
		itemUom.put("UOM_QTY_STAN", itemUom.get("QTY"));
		itemUom.put("DEL_YN", "N");
		item.put("REP_UOM_ID", uomCdStan);
		sqlMapClient.insert("wmsms090.insertUploadUomData", itemUom);
	}

	private void handleUpdateItemUom(SqlMapClient sqlMapClient, Map<String, Object> itemUom,
			Map<String, Object> existItemUom) throws SQLException {
		existItemUom.put("RITEM_ID", itemUom.get("RITEM_ID"));
		existItemUom.put("QTY", itemUom.get("QTY"));
		sqlMapClient.update("wmsms101.update", existItemUom);
	}

	/**
	 * Method ID : saveUploadMultiItem - 다중상품밬토드 엑셀입력 템플릿 저장 Method 설명 : 템플릿 다중상품바코드
	 * 저장 작성자 : hdy
	 * 
	 * @param model
	 * @return
	 */
	public Object saveUploadMultiItemBarcode(Map<String, Object> model) {
		executeUpdate("wmsms090.pk_wmsms090e.sp_save_excel_mult_itemBarcode", model);
		return model;
	}
	
	/**
	 * Method ID : uploadEtcExcel - 상품정보엑셀누락값 저장
	 * 저장 작성자 : summer
	 * 
	 * @param model
	 * @return
	 */
	public Object uploadEtcExcel(Map<String, Object> model) {
		executeUpdate("wmsms090.pk_wmsms090e.sp_excel_item_info_etc", model);
		return model;
	}
	
	private void logFailedBatch(List<Map<String, Object>> itemList, int startIndex, int endIndex) {
	    for (int i = startIndex; i < endIndex; i++) {
	        Map<String, Object> item = itemList.get(i);
	        log.error("Failed Batch Item: " + item);
	    }
	}

}
