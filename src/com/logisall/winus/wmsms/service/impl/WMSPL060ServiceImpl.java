package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSPL060Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPL060Service")
public class WMSPL060ServiceImpl implements WMSPL060Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPL060Dao")
	private WMSPL060Dao dao;

	/**
	 * 
	 * 대체 Method ID : list 대체 Method 설명 : 물류용기관리 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.list(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : save 대체 Method 설명 : 물류용기관리 저장 작성자 : 기드온 수정 : 상품화를 위한 저장 추가
	 * (상품화, 물류용기등록) chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				String stGubun = (String) model.get("I_ST_GUBUN" + i);

				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
				modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO)); // LC_ID
				modelDt.put("selectIds", model.get("selectIds"));
				modelDt.put("ST_GUBUN", model.get("ST_GUBUN" + i));

				modelDt.put("SUB_MATERIAL_ID", model.get("SUB_MATERIAL_ID" + i));
				modelDt.put("SUB_MATERIAL_CODE", model.get("SUB_MATERIAL_CODE" + i));
				modelDt.put("CUST_ID", model.get("CUST_ID" + i));

				modelDt.put("SUB_MATERIAL_NAME", model.get("SUB_MATERIAL_NAME" + i));
				modelDt.put("REMARK", model.get("REMARK" + i));
				modelDt.put("DEL_YN", model.get("DEL_YN" + i));
				modelDt.put("UNIT_PRICE", model.get("UNIT_PRICE" + i));
				modelDt.put("SUB_MATERIAL_BAR_CD", model.get("SUB_MATERIAL_BAR_CD" + i));

				modelDt.put("SIZE_H", model.get("SIZE_H" + i));
				modelDt.put("SIZE_L", model.get("SIZE_L" + i));
				modelDt.put("SIZE_W", model.get("SIZE_W" + i));
				modelDt.put("REP_UOM_ID", model.get("REP_UOM_ID" + i));
				modelDt.put("SUB_MATERIAL_CAPACITY", model.get("SUB_MATERIAL_CAPACITY" + i));
				

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.update(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {	
					dao.delete(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
			
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());
			
		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	/**
     * 
     * 대체 Method ID : list 대체 Method 설명 : 물류용기관리 조회 작성자 : 기드온
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listPool(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listPool(model));
        return map;
    }	
    /**
     * 
     * 대체 Method ID : save 대체 Method 설명 : 물류용기관리 저장 작성자 : 기드온 수정 : 상품화를 위한 저장 추가
     * (상품화, 물류용기등록) chSong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> savePool(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try {
            for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
                String stGubun = (String) model.get("I_ST_GUBUN" + i);

                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO)); // LC_ID
                modelDt.put("selectIds", model.get("selectIds"));
                modelDt.put("ST_GUBUN", model.get("ST_GUBUN" + i));

                modelDt.put("SUB_MATERIAL_ID", model.get("SUB_MATERIAL_ID" + i));
                modelDt.put("POOL_ID", model.get("POOL_ID" + i));
                modelDt.put("QTY", model.get("QTY" + i));
                modelDt.put("SEQ_IDX", model.get("SEQ_IDX" + i));
                

                if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
                    dao.poolInsert(modelDt);
                } else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
                    dao.poolUpdate(modelDt);
                } else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {    
                    dao.poolDelete(modelDt);
                } else {
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
}
