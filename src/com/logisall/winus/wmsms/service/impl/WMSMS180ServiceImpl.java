package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS180Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS180Service")
public class WMSMS180ServiceImpl implements WMSMS180Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS180Dao")
    private WMSMS180Dao dao;

    /**
     * Method ID : listSub
     * Method 설명 : ZONE정보등록 서브 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.sublist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : saveSub
     * Method 설명 : ZONE정보등록 서브 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	Map<String, Object> modelDtM = new HashMap<String, Object>();
        	modelDtM.put("SEL_CUST_ID"		,model.get("SEL_CUST_ID"));
        	modelDtM.put("LC_ID"			,model.get("SS_SVC_NO"));
        	modelDtM.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID"));
        	dao.saveInitDelete(modelDtM);
        	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("CUST_ID"		,model.get("CUST_ID" + i));
                modelDt.put("CODE_VALUE"	,model.get("CODE_VALUE" + i));
                modelDt.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID" + i));
                modelDt.put("LC_ID"			,model.get("SS_SVC_NO"));
                modelDt.put("SS_USER_NO"	,model.get("SS_USER_NO"));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	//dao.saveInitDelete(modelDt);
                	dao.saveInsert(modelDt);
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveSub
     * Method 설명 : ZONE정보등록 서브 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> deleteSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            if(1 == 1){
            	Map<String, Object> modelDtM = new HashMap<String, Object>();
            	modelDtM.put("SEL_CUST_ID"		,model.get("SEL_CUST_ID"));
            	modelDtM.put("LC_ID"			,model.get("SS_SVC_NO"));
            	modelDtM.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID"));
            	dao.saveInitDelete(modelDtM);
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
                m.put("errCnt", errCnt);
            }else{
                errCnt++;
                m.put("errCnt", errCnt);
                throw new Exception(MessageResolver.getMessage("save.error"));
            }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
