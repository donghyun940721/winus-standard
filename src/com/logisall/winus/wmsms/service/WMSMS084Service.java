package com.logisall.winus.wmsms.service;

import java.util.Map;

public interface WMSMS084Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
  

	
}
