package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS094Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listLvl(Map<String, Object> model) throws Exception;
    public Map<String, Object> listLvlDepth(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrpLvl(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLvl(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCsvItemGroup(Map<String, Object> model, List list) throws Exception;    
}
