package com.logisall.winus.wmsms.service;

import java.util.Map;

public interface WMSMS603Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public Map<String, Object> print_zebra(Map<String, Object> model) throws Exception;
}
