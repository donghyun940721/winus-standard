package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS093Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> autoOrderInsert(Map<String, Object> model) throws Exception;
}
