package com.logisall.winus.wmsms.service;

import java.util.Map;

public interface WMSMS097Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
}
