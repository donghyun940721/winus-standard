package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;


public interface WMSMS015Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
//    public Map<String, Object> update(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
}
