package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS090Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> listUom(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUom(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> pop3List(Map<String, Object> model) throws Exception;
    public Map<String, Object> pop3Save(Map<String, Object> model) throws Exception;
    public Map<String, Object> pop3Sync(Map<String, Object> model) throws Exception;
    public Map<String, Object> pop3Delete(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUom2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveUploadDataPk(Map<String, Object> model, List<Map> list) throws Exception;
    public Map<String, Object> overapCheck(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception;
	public Map<String, Object> checkUomChange(Map<String, Object> model) throws Exception;
	public Map<String, Object> deletedList(Map<String, Object> model) throws Exception;
	public Map<String, Object> restoreItem(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectDuplicateBarcd(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveUploadItemLoc(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> getBoxBarCd(Map<String, Object> model) throws Exception;
	public Map<String, Object> uploadBoxBarCd(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> wmsms098listE1(Map<String, Object> model) throws Exception;
	public Map<String, Object> wmsms098saveItem(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> saveItemMasterV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveUploadMultiItemBarcode(Map<String, Object> model, List<Map> list) throws Exception;
	public Map<String, Object> uploadEtcExcel(Map<String, Object> model, List<Map> list) throws Exception;
}