package com.logisall.winus.wmsms.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsms.service.WMSMS603Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSMS603Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSMS603Service")
	private WMSMS603Service service;

	@RequestMapping("/WINUS/WMSMS603.action")
	public ModelAndView wmsms603(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS603");
	}
	
    @RequestMapping("/WMSMS603/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    @RequestMapping("/WMSMS603/save.action")
    public ModelAndView save(Map<String, Object> model) throws Exception {
    	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
	
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
    
    @RequestMapping("/WMSMS603/print_zebra.action")
    public ModelAndView print_zebra(Map<String, Object> model) throws Exception {
    	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
	
		try {
			m = service.print_zebra(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
