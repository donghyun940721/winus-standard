package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsms.service.WMSMS015Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSMS015Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS015Service")
    private WMSMS015Service service;
    
    /*-
	 * Method ID           : WMSMS015
	 * Method 설명        : 화주/사용자 관리
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSMS015.action")
	public ModelAndView WMSMS015(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS015");
	}

	/*-
	 * Method ID           : list
	 * Method 설명        : 화주/사용자 관리 > 사용자 조회
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS015/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID           : listDetail
	 * Method 설명        : 화주/사용자 관리 > 화주 조회
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS015/listDetail.action")
	public ModelAndView listDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID           : save
	 * Method 설명        : 화주 <-> 사용자 저장
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS015/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID           : delete
	 * Method 설명        : 화주 <-> 사용자 삭제
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS015/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
