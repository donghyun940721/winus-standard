package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS090Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS090Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS090Service")
    private WMSMS090Service service;
    
	static final String[] COLUMN_NAME_WMSMS090 = {
			"ITEM_KOR_NM", "ITEM_ENG_NM", "ITEM_SHORT_NM", "ITEM_EPC_CD", "ITEM_BAR_CD", //5 line
			"DEL_YN", "WORK_IP", "LC_ID", "CUST_ID", "ITEM_CODE",//5 line
			"STOCK_UOM_ID", "BUY_UNIT", "ITEM_TYPE", "ITEM_WGT", "ITEM_GRP_ID",//5 line
			"PACKING_BOX_TYPE", "PROP_STOCK_DAY", "TIME_PERIOD_DAY", "SHIP_ABC", "SET_ITEM_YN",//5 line
			"SET_ITEM_TYPE", "ITEM_CAPACITY",//2 line
			"WEIGHT_CLASS", "ORD_CUST_ID", "ITEM_SIZE", "KAN_CD",//4 line
			"VAT_YN", "ITEM_CLASS", "OP_QTY", "RS_QTY", "EA_YN",//5 line
			"ITEM_EPC_CD", "IN_WH_ID", "IN_ZONE", "PLT_YN", "REP_UOM_ID",//5 line
			"MIN_UOM_ID", "ITEM_NM", "QTY",  "OP_UOM_ID", "CUST_BARCODE",//5 line
			"CUST_ITEM_CD", "UNIT_PRICE", "REMARK", "PROP_QTY", "EXPIRY_DATE",//5 line
			"RFID", "TAG_PREFIX", "CUST_EPC_CD", "USE_YN", "BEST_DATE_TYPE",//5 line
			"BEST_DATE_NUM", "BEST_DATE_UNIT", "OUT_BEST_DATE_NUM", "OUT_BEST_DATE_UNIT", "IN_BEST_DATE_NUM",//5 line
			"IN_BEST_DATE_UNIT", "USE_DATE_NUM", "USE_DATE_UNIT", "COLOR", "SIZE_W",//5 line
			"SIZE_H", "SIZE_L", "ITF_CD", "TYPE_ST", "BOX_EPC_CD",//5 line
			"BOX_BAR_CD", "CUST_LEGACY_ITEM_CD", "MAKER_NM", "LOT_USE_YN", "REG_NO",//5 line
			"UPD_NO","UOM_QTY_STAN", "UOM_CD_STAN", "UOM_CD_TARGET",//4 line
			"AUTO_OUT_ORD_YN","AUTO_CAL_PLT_YN","OUT_LOC_REC","ITEM_DEVISION_CD","WORKING_TIME"//5 line
	};
	
	static final String[] COLUMN_NAME_WMSMS090_PK = { //일반엑셀입력 02
		 "ITEM_LOCAL_NM"
		,"ITEM_ENG_NM"
		,"ITEM_BAR_CD"
		,"ITEM_CODE"
		,"UOM_CD"

		,"ITEM_WGT"
		,"SET_ITEM_YN"
		,"ITEM_SIZE"
		,"PLT_YN"
		,"ITEM_NM"

		,"CUST_ITEM_CD"
		,"OUT_LOC_REC" // 
		,"USE_YN"//
		,"FIX_LOC_CD"//
		,"UNIT_PRICE"
		,"PROP_QTY"
		,"EXPIRY_DATE"
		,"COLOR"

		,"SIZE_W"
		,"SIZE_H"
		,"SIZE_L"
		,"BOX_BAR_CD"
		,"CUST_LEGACY_ITEM_CD"

		,"MAKER_NM"
		,"ITEM_CAPACITY"

		,"UNIT_QTY"
		,"CVT_QTY"
		,"UOM1_CD"
		,"UOM2_CD"
		,"CUST_BARCODE"
		,"ITEM_DESC"
		,"ITEM_DEVISION_CD"
		
		,"ITEM_GRP_CD"
		,"ITEM_2ND_GRP_CD"
		,"ITEM_3RD_GRP_CD"
		,"ITEM_ETC1"
		
		,"COUNTRY_ORIGIN"
		,"PARCEL_COM_TY"
		,"SERIAL_CHK_YN"
		,"PARCEL_COMP_CD"
		,"MACHINE_TYPE"

		,"MIN_PARCEL_CBM"
		,"SUB_MATERIALS"
		,"INNER_BOX_BAR_CD"
		,"PLT_BAR_CD"
		,"OUT_BEST_DATE_NUM"
		,"OUT_BEST_DATE_UNIT"
		,"ITEM_TMP"
		
		,"PARCEL_IN_QTY"
		,"PARCEL_CBM"
		,"TYPE_ST"
		,"BEST_DATE_TYPE"
	};
	
	static final String[] COLUMN_NAME_WMSMS090_PK_SIMPLE = {//간편엑셀입력 03
		"ITEM_NM"
		,"ITEM_LOCAL_NM"
		,"ITEM_ENG_NM"
		,"ITEM_BAR_CD"
		,"ITEM_CODE"
		,"UOM_CD"

		,"SET_ITEM_YN"
		,"PLT_YN"

		,"CUST_ITEM_CD"
		,"UNIT_PRICE"
		,"BOX_BAR_CD"
		,"UNIT_QTY"
		,"CVT_QTY"
		,"UOM1_CD"
		,"UOM2_CD"
	};
	
	static final String[] COLUMN_NAME_WMSMS090_FULL = {
		"ITEM_LOCAL_NM"
		,"ITEM_ENG_NM"
		,"ITEM_BAR_CD"
		,"ITEM_CODE"
		,"UOM_CD"

		,"ITEM_WGT"
		,"SET_ITEM_YN"
		,"ITEM_SIZE"
		,"PLT_YN"
		,"ITEM_NM"

		,"CUST_ITEM_CD"
		,"UNIT_PRICE"
		,"PROP_QTY"
		,"EXPIRY_DATE"
		,"COLOR"

		,"SIZE_W"
		,"SIZE_H"
		,"SIZE_L"
		,"BOX_BAR_CD"
		,"CUST_LEGACY_ITEM_CD"

		,"MAKER_NM"
		,"ITEM_CAPACITY"
		,"UOM1_CD"
		,"UOM2_CD"
		,"CVT_QTY"
		,"UNIT_QTY"
	};
	
	static final String[] COLUMN_NAME_WMSMS090_ITEM_LOC = {
		"ITEM_CD"
		,"LOC_CD"
	};
	
	static final String[] COLUMN_NAME_WMSMS090_UPDATE_BOXBAR = {
		"ITEM_CD"
		,"ITEM_NM"
		,"BOX_BAR_CD"
	};
	
	static final String[] COLUMN_NAME_WMSMS099_MULTI_ITEM_BAR = {
		"ITEM_CD"
		,"ITEM_NM"
		,"ITEM_BAR_CD"
		,"ITEM_MAKER"
		,"UOM1_CD"
		,"UOM2_CD"
		,"CVT_QTY"
		,"DEF_QTY"
	};
	
	static final String[] COLUMN_NAME_WMSMS090_ETC = {
		"ITEM_CD"
		
		,"ITEM_SHORT_NM"
		,"IN_ZONE"
		,"IN_OUT_BOUND"
		,"OUT_ZONE"
		,"IN_CUST_ZONE"
		
		,"ITF_CD"
		,"OUT_CUST_ZONE"
		,"TAG_PREFIX"
		,"BOX_EPC_CD"
		,"ITEM_EPC_CD"
		
		,"CURRENCY_NAME"
		,"SALES_PRICE"
		,"PACKING_BOX_TYPE"
		,"SET_ITEM_TYPE"
		,"LOT_USE_YN"
		
		,"WORKING_TIME"
		,"SHIP_ABC"
		,"PROP_STOCK_DAY"
		,"WEIGHT_CLASS"
		,"BEST_DATE_NUM"
		
		,"USE_DATE_NUM"
		,"IN_BEST_DATE_NUM"
		,"USE_DATE_UNIT"
		,"AUTO_OUT_ORD_YN"
		,"BEST_DATE_UNIT"
		
		,"IN_BEST_DATE_UNIT"
		,"LOT_PREFIX"
		,"AUTO_CAL_PLT_YN"
		,"REWORK_ITEM_YN"
		,"DEV_TEMP_TYPE"
		
		,"IN_LOC_REC"
		,"UNIT_NM"
		,"FIX_LOC_ZONE_ID"
		,"ITEM_TYPE"
	};

	/**
	 * Method ID : wmsms090 Method 설명 : 상품정보관리 화면 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSMS090.action")
	public ModelAndView wmsms090(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090", service.selectData(model));
	}
	
	/**
	 * Method ID : wmsms090pop 
	 * Method 설명 : 상품정보관리 화면 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090pop.action")
	public ModelAndView wmsms090pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090pop");
	}
	
	/**
	 * Method ID : wmsms099pop 
	 * Method 설명 : 상품바코드 팝업 
	 * 작성자 : SING09
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS099pop.action")
	public ModelAndView wmsms099pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS099pop");
	}
	
	/**
	 * Method ID	: WMSMS090popBOX 
	 * Method 설명  : 박스바코드 업로드 팝업
	 * 작성자 		: KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090popBOX.action")
	public ModelAndView WMSMS090popBOX(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090popBOX");
	}

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/list.action")
	public ModelAndView listItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			//특수문자 처리
			model.put("vrSrchItemGrpId98",org.springframework.web.util.HtmlUtils.htmlUnescape((String) model.get("vrSrchItemGrpId98")));
			
			mav = new ModelAndView("jqGridJsonView", service.listItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : saveItem Method 설명 : 상품정보관리 저장수정 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/save.action")
	public ModelAndView saveItem(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveItem(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : listUom Method 설명 : UOM환산이력 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/listUom.action")
	public ModelAndView listUom(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listUom(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list uom :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : saveSub Method 설명 : UOM환산이력 저장 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/saveSub.action")
	public ModelAndView saveSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveUom(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : saveSub2 Method 설명 : UOM환산이력 저장2 Ritem_cd를 db에서 찾아서 처리 작성자 :
	 * chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/saveSub2.action")
	public ModelAndView saveSub2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveUom2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save sub2 :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : listExcel Method 설명 : 엑셀다운로드 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				if("0000001840".equals(model.get(ConstantIF.SS_SVC_NO))){
					System.out.println(model.get(ConstantIF.SS_SVC_NO));
					this.doExcelDown_GS(response, grs);
				}else{
					this.doExcelDown(response, grs);
				}
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel file :", e);
			}
		}
	}
	
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            
            String[] headerList = {
                    MessageResolver.getText("화주")          
                    , MessageResolver.getText("상품군(대)")  
                    , MessageResolver.getText("상품코드")    
                    , MessageResolver.getText("상품명")      
                    , MessageResolver.getText("상품명(영어)")

                    , MessageResolver.getText("대표UOM")     

                    , MessageResolver.getText("기본입고존")  
                    , MessageResolver.getText("기본출고존")  
                    , MessageResolver.getText("상품중량")    
                    , MessageResolver.getText("세로")        
                    , MessageResolver.getText("높이")        

                    , MessageResolver.getText("가로")        
                    , MessageResolver.getText("임가공상품여부")
                    , MessageResolver.getText("임가공상품타입")
                    , MessageResolver.getText("상품크기")    
                    , MessageResolver.getText("용적")        

                    , MessageResolver.getText("상품바코드")                                                          
                    , MessageResolver.getText("화주상품코드")
                    , MessageResolver.getText("박스바코드")  
                    , MessageResolver.getText("상품설명")    
                    , MessageResolver.getText("유효기간")    

                    , MessageResolver.getText("적정재고수량")                 
                    , MessageResolver.getText("적정재고일수")
                    , MessageResolver.getText("MAKER이름")   
                    , MessageResolver.getText("단가")        
                    , MessageResolver.getText("통화명")      

                    , MessageResolver.getText("UOM_YN")                 
                    , MessageResolver.getText("입고LOT사용여부")
                    , MessageResolver.getText("DEL_YN")      
                    , MessageResolver.getText("품온구분")    
                    , MessageResolver.getText("상품군(중)")  
                    , MessageResolver.getText("상품군(소)")  

                    , MessageResolver.getText("입수")        
                    , MessageResolver.getText("적재단위")    
                    , MessageResolver.getText("기본고정로케이션")

                    , MessageResolver.getText("환산수량(숫자만)")
                    , MessageResolver.getText("환산수량단위(ex. BOX)")
                    , MessageResolver.getText("변환단위(ex. PLT)")    
                    , MessageResolver.getText("고객상품바코드")
                    , MessageResolver.getText("출고추천타입")
            };
            String[][] headerEx = new String[headerList.length][6];
            for (int i = 0 ; i < headerList.length; i++){
                headerEx[i][0] = headerList[i];
                headerEx[i][1] = String.valueOf(i);
                headerEx[i][2] = String.valueOf(i);
                headerEx[i][3] = String.valueOf(0);
                headerEx[i][4] = String.valueOf(0);
                headerEx[i][5] = String.valueOf(200);
            }
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					            		  {"CUST_NM"			, "S"}
					            		, {"ITEM_GRP_NM"		, "S"}
					            		, {"ITEM_CODE"			, "S"}
					            		, {"ITEM_KOR_NM"		, "S"}
					            		, {"ITEM_ENG_NM"        , "S"}
					            		
					            		, {"REP_UOM_CD"			, "S"}
					            		
					            		, {"IN_ZONE_NM"			, "S"}
					            		, {"OUT_ZONE_NM"		, "S"}
					            		, {"ITEM_WGT"			, "S"}
					            		, {"SIZE_H"				, "S"}
					            		, {"SIZE_L"				, "S"}
					            		
					            		, {"SIZE_W"				, "S"}
					            		, {"SET_ITEM_YN"		, "S"}
					            		, {"SET_ITEM_TYPE"		, "S"}
					            		, {"ITEM_SIZE"			, "S"}
					            		, {"ITEM_CAPACITY"		, "S"}
					            		
					            		, {"ITEM_BAR_CD"		, "S"}					            							            		
					            		, {"CUST_ITEM_CD"		, "S"}
					            		, {"BOX_BAR_CD"			, "S"}
					            		, {"ITEM_DESC"			, "S"}
					            		, {"BEST_DATE_NUM"		, "S"}
					            		
					            		, {"PROP_QTY"			, "S"}					            		
					            		, {"PROP_STOCK_DAY"		, "S"}
					            		, {"MAKER_NM"			, "S"}
					            		, {"UNIT_PRICE"			, "S"}
					            		, {"CURRENCY_NAME"		, "S"}
					            		
					            		, {"UOM_YN"				, "S"}					            		
					            		, {"LOT_USE_YN"			, "S"}
					            		, {"DEL_YN"				, "S"}
					            		, {"TEMP_TYPE_NAME"		, "S"}
					            		, {"ITEM_GRP_NM2"		, "S"}
					            		, {"ITEM_GRP_NM3"		, "S"}
					            		, {"UNIT_QTY"		, "S"}
					            		, {"REP_UOM_CD"		, "S"}
					            		, {"FIX_LOC_CD"		, "S"}
					            		
					            		, {"QTY"		, "S"}
					            		, {"UOM1_CODE"		, "S"}
					            		, {"UOM2_CODE"		, "S"}
					            		, {"CUST_BARCODE"       , "S"}
					            		, {"OUT_LOC_REC_NM"       , "S"}
					            		
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("상품정보관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
    protected void doExcelDown_GS(HttpServletResponse response, GenericResultSet grs) {
    	try{
    		//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
    		//헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
    		String[][] headerEx = {
    				{MessageResolver.getText("화주")      		, "0", "0", "0", "0", "200"}
    				, {MessageResolver.getText("상품코드")      	, "1", "1", "0", "0", "200"}
    				, {MessageResolver.getText("상품명")      		, "2", "2", "0", "0", "200"}
    				, {MessageResolver.getText("상품군(대)")      	, "3", "3", "0", "0", "200"}
    				, {MessageResolver.getText("상품군(중)")		, "4", "4", "0", "0", "200"} 
    				
    				, {MessageResolver.getText("상품군(소)")		, "5", "5", "0", "0", "200"} 
    				, {MessageResolver.getText("대표UOM")      	, "6", "6", "0", "0", "200"}
    				, {MessageResolver.getText("임가공상품여부")    	, "7", "7", "0", "0", "200"}
    				, {MessageResolver.getText("상품바코드")      	, "8", "8", "0", "0", "200"}					            							            		
    				, {MessageResolver.getText("상품설명")      	, "9", "9", "0", "0", "200"}
    				
    				, {MessageResolver.getText("유효기간")      	, "10", "10", "0", "0", "200"}	//
    				, {MessageResolver.getText("MAKER이름")		, "11", "11", "0", "0", "200"}
    				, {MessageResolver.getText("단가")      		, "12", "12", "0", "0", "200"}
    				, {MessageResolver.getText("판매가")      		, "13", "13", "0", "0", "200"}
    				, {MessageResolver.getText("보관방법")			, "14", "14", "0", "0", "200"} 
    				
    				, {MessageResolver.getText("상품중량")      	, "15", "15", "0", "0", "200"}
    		};	
    		//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
    		String[][] valueName = {
    				{"CUST_NM"			, "S"}
    				, {"ITEM_CODE"			, "S"}
    				, {"ITEM_KOR_NM"		, "S"}
    				, {"ITEM_GRP_NM"		, "S"}
    				, {"ITEM_GRP_NM2"		, "S"}
    				
    				, {"ITEM_GRP_NM3"		, "S"}
    				, {"REP_UOM_CD"			, "S"}
    				, {"SET_ITEM_YN"		, "S"}
    				, {"ITEM_BAR_CD"		, "S"}					            							            		
    				, {"ITEM_DESC"			, "S"}
    				
    				, {"BEST_DATE_NUM"		, "S"}
    				, {"MAKER_NM"			, "S"}
    				, {"UNIT_PRICE"			, "S"}
    				, {"SALES_PRICE"		, "S"}
    				, {"ITEM_SHORT_NM"		, "S"}
    				
    				, {"ITEM_WGT"			, "S"}
    		}; 
    		
    		// 파일명
    		String fileName = MessageResolver.getText("상품정보관리");
    		// 시트명
    		String sheetName = "Sheet1";
    		// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    		String marCk = "N";
    		// ComUtil코드
    		String etc = "";
    		
    		ExcelWriter wr = new ExcelWriter();
    		wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
    		
    	} catch (Exception e) {
    		if (log.isErrorEnabled()) {
    			log.error("fail download Excel file...", e);
    		}
    	}
    }

	/**
	 * Method ID : wmsms0902 Method 설명 : 상품정보관리 화면 sample 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSMS0902.action")
	public ModelAndView wmsms0902(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS0902", service.selectData(model));
	}

	/**
	 * Method ID : WMSMS090E4 Method 설명 : 엑셀업로드 화면 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090E4.action")
	public ModelAndView wmsms090E4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090E4");
	}
	
	/**
	 * Method ID : WMSMS090E5
	 * @author kimzero
	 * @desc 엑셀업로드 화면 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090E5.action")
	public ModelAndView wmsms090E5(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090E5");
	}
	
	/**
	 * Method ID : WMSMS090E6
	 * @author KSJ
	 * @desc 상품 대표로케이션 지정
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090E6.action")
	public ModelAndView WMSMS090E6(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090E6");
	}

	/**
	 * Method ID : uploadInfo Method 설명 : Excel 파일 읽기 작성자 : kwt
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS090, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}     

	/**
	 * Method ID : 기존 등록 ITEM_CODE 중복 체크
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/overapCheck.action")
	public ModelAndView overapCheck(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.overapCheck(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : save
	 * Method 설명 : 통합 HelpDesk 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/saveItemImg.action")
	public ModelAndView saveItemImg(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveItemImg(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSMS090/insertValidate.action")
	public ModelAndView insertValidate(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.insertValidate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : uploadInfo - 엑셀입력저장 (일반/간편) SUMMER
	 *  Method 설명 : Excel 파일 읽기 
	 *  작성자 : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/uploadInfoPk.action")
	public ModelAndView uploadInfoPk(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			String uploadType = (String) model.get("uploadType");
			List<Map> list = new ArrayList<Map>();
			if(uploadType.equals("NORMAL")){ //일반엑셀입력
				//excelLimitRowRead(File file, String[] cellName, int startSheet, int startRow, int endRow, int startCell)
				//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_PK, 0, startRow, 10000, 0);
			    list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS090_PK, 0, startRow, 10000, 0);
			}else if (uploadType.equals("SIMPLE")){ //간편엑셀입력
				//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_PK_SIMPLE, 0, startRow, 10000, 0);
				list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS090_PK_SIMPLE, 0, startRow, 10000, 0);
			}
			else if (uploadType.equals("FULL")){ //사용안함
				//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_FULL, 0, startRow, 10000, 0);
				list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS090_FULL, 0, startRow, 10000, 0);
			}
//			COLUMN_NAME_WMSMS090_PK_SIMPLE
			Map<String, Object> mapBody = new HashMap<String, Object>();
//			mapBody.put("LIST", list);
			mapBody.put("chkDuplicationYn"	,model.get("chkDuplicationYn"));
			mapBody.put("blankUpdateSkipYn"  ,model.get("blankUpdateSkipYn"));
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			//run
			m = service.saveUploadDataPk(mapBody, list);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}  
	
	/*-
	 * Method ID : saveLcSync 
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090/saveLcSync.action")
	public ModelAndView saveLcSync(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveLcSync(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : checkUomChange
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090/checkUomChange.action")
	public ModelAndView checkUomChange(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.checkUomChange(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : wmsms090pop2 
	 * Method 설명 : 상품정보관리 화면 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090pop2.action")
	public ModelAndView wmsms090pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090pop2");
	}
	
	/**
	 * Method ID : wmsms090pop3 
	 * Method 설명 : 상품정보관리 화면 
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090pop3.action")
	public ModelAndView wmsms090pop3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090pop3");
	}
	
	/**
	 * Method ID : wmsms090pop3list
	 * Method 설명 : 상품정보관리 화면 
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090pop3/list.action")
	public ModelAndView wmsms090pop3list(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.pop3List(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSMS090pop3/save.action")
	public ModelAndView wmsms090pop3save(Map<String, Object> model) throws Exception  {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.pop3Save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSMS090pop3/sync.action")
	public ModelAndView wmsms090pop3sync(Map<String, Object> model) throws Exception  {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.pop3Sync(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSMS090pop3/delete.action")
	public ModelAndView wmsms090pop3delete(Map<String, Object> model) throws Exception  {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.pop3Delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/deletedList.action")
	public ModelAndView deletedList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.deletedList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/restoreItem.action")
	public ModelAndView restoreItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.restoreItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}
	
	
	/**
	 * Method ID : selectDuplicateBarcd
	 * Method 설명 : 중복바코드 조회(상품관리메뉴)
	 * 작성자 : ykim
	 * 작성일 : 21.11.19
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/selectDuplicateBarcd.action")
	public ModelAndView selectDuplicateBarcd(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.selectDuplicateBarcd(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : uploadItemLoc 
	 * Method 설명 : 상품 대표로케이션 업로드  
	 * 파일 읽기 작성자 : KSJ
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/uploadItemLoc.action")
	public ModelAndView uploadItemLoc(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			String uploadType = (String) model.get("uploadType");
			List<Map> list = new ArrayList<Map>();
			//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_ITEM_LOC, 0, startRow, 10000, 0);
			list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS090_ITEM_LOC, 0, startRow, 10000, 0);
			
			/*
			 * 엑셀 파일 검증
			 */
			for(int i =0 ; i<list.size() ; i++){
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i+2;
				
				String fieldOrdId = (String) hashMap.get(COLUMN_NAME_WMSMS090_ITEM_LOC[0]);
				if(fieldOrdId == null || fieldOrdId.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSMS090_ITEM_LOC[0]+"(상품코드)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldOrdSeq = (String) hashMap.get(COLUMN_NAME_WMSMS090_ITEM_LOC[1]);
				if(fieldOrdSeq == null || fieldOrdSeq.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSMS090_ITEM_LOC[1]+"(로케이션코드)는 필수 값입니다. 라인 : "+rowNum);
				}
			}
			
			m = service.saveUploadItemLoc(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}  
	
	/**
	 * Method ID	 : getBoxBarCd
	 * Method 설명   : 박스바코드 조회 (null, '', 1)
	 * 작성자 		 : KSJ
	 * 작성일 	     : 2022.07.11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/getBoxBarCd.action")
	public ModelAndView getBoxBarCd(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.getBoxBarCd(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID 		: uploadBoxBarCd
	 * Method 설명 		: 박스바코드 수정 엑셀 업로드 
	 * 작성자 			: KSJ
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/uploadBoxBarCd.action")
	public ModelAndView uploadBoxBarCd(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			model.put("WORK_IP", request.getRemoteAddr());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			String uploadType = (String) model.get("uploadType");
			
			List<Map> list = new ArrayList<Map>();			
			//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_UPDATE_BOXBAR, 0, startRow, 10000, 0);
			list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS090_UPDATE_BOXBAR, 0, startRow, 10000, 0);

			m = service.uploadBoxBarCd(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
   /**
     * Method ID : wmsms098 
     * Method 설명 : 추가상품목록관리 화면
     * 작성자 : schan
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WINUS/WMSMS098.action")
    public ModelAndView wmsms098(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsms/WMSMS098");
    }
    
    /**
     * Method ID : wmsms098listE1
     * Method 설명 : 추가상품목록관리 조회
     * 작성자 : schan
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS098/listE1.action")
    public ModelAndView wmsms098listE1(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.wmsms098listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID : wmsms098saveItem
     * Method 설명 : 추가상품목록 등록
     * 작성자 : schan
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS098/saveItem.action")
    public ModelAndView wmsms098saveItem(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        Map<String,Object>map = new HashMap<String,Object>();
        try {
            map = service.wmsms098saveItem(model);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("MSG", e.getStackTrace());
            map.put("errCnt",-1);
        }
        mav.addAllObjects(map);
        return mav;
    }
    
    /**
	 * Method ID : WMSMS090popMultiBarcode
	 * @author HDY
	 * @desc 상품 다중바코드 업로드  
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090popMultiBarcode.action")
	public ModelAndView WMSMS090popMultiBarcode(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090popMultiBarcode");
	}
	
    /**
	 * Method ID : uploadMultiBarcode 
	 * Method 설명 : 상품 다중바코드 업로드  
	 * 파일 읽기 작성자 : HDY
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/uploadMultiBarcode.action")
	public ModelAndView uploadMultiBarcode(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			String uploadType = (String) model.get("uploadType");
			List<Map> list = new ArrayList<Map>();
			//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_ITEM_LOC, 0, startRow, 10000, 0);
			list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS099_MULTI_ITEM_BAR, 0, startRow, 10000, 0);
			
			/*
			 * 엑셀 파일 검증
			 */
			for(int i =0 ; i<list.size() ; i++){
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i+2;
				
				String fieldOrdId = (String) hashMap.get(COLUMN_NAME_WMSMS099_MULTI_ITEM_BAR[0]);
				if(fieldOrdId == null || fieldOrdId.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSMS099_MULTI_ITEM_BAR[0]+"(상품코드)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldOrdSeq = (String) hashMap.get(COLUMN_NAME_WMSMS099_MULTI_ITEM_BAR[1]);
				if(fieldOrdSeq == null || fieldOrdSeq.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSMS099_MULTI_ITEM_BAR[1]+"(상품바코드)는 필수 값입니다. 라인 : "+rowNum);
				}
			}
			
			m = service.saveUploadMultiItemBarcode(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	 /**
		 * Method ID : WMSMS090popETC
		 * @author SUMMER
		 * @desc 상품 누락값 업로드 --로컬전용  
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSMS090popETC.action")
		public ModelAndView WMSMS090popETC(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
			return new ModelAndView("winus/wmsms/WMSMS090popETC");
		}
	
	 /**
		 * Method ID : uploadEtcExcel 
		 * Method 설명 : 상품 누락값 업로드 --로컬전용  
		 * 파일 읽기 작성자 : summer
		 * 
		 * @param request
		 * @param response
		 * @param model
		 * @return
		 */
		@RequestMapping("/WMSMS090/uploadEtcExcel.action")
		public ModelAndView uploadEtcExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			try {
				request.setCharacterEncoding("utf-8");

				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				MultipartFile file = multipartRequest.getFile("txtFile");
				String fileName = file.getOriginalFilename();
				String filePaths = ConstantIF.TEMP_PATH;

				if (!FileHelper.existDirectory(filePaths)) {
					FileHelper.createDirectorys(filePaths);
				}
				File destinationDir = new File(filePaths);
				File destination = File.createTempFile("excelTemp", fileName, destinationDir);
				FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

				int startRow = Integer.parseInt((String) model.get("startRow"));
				
				String uploadType = (String) model.get("uploadType");
				List<Map> list = new ArrayList<Map>();
				list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS090_ETC, 0, startRow, 10000, 0);
				
				/*
				 * 엑셀 파일 검증
				 */
				for(int i =0 ; i<list.size() ; i++){
					Map<String, Object> hashMap = new HashMap<String, Object>();
					hashMap = list.get(i);
					int rowNum = i+2;
					
					String fieldOrdId = (String) hashMap.get(COLUMN_NAME_WMSMS090_ETC[0]);
					if(fieldOrdId == null || fieldOrdId.equals("")){
						throw new Exception("Exception:"+COLUMN_NAME_WMSMS090_ETC[0]+"(상품코드)는 필수 값입니다. 라인 : "+rowNum);
					}
					
				}
				
				m = service.uploadEtcExcel(model, list);

				destination.deleteOnExit();
				mav.addAllObjects(m);

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to upload Excel info :", e);
				}
				m = new HashMap<String, Object>();
				m.put("MSG", MessageResolver.getMessage("save.error"));
				m.put("MSG_ORA", e.getMessage());
				m.put("errCnt", "1");
				mav.addAllObjects(m);
			}
			return mav;
		}
}
