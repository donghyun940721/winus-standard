package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS010Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSMS010 = {
			"CUST_CD", "CUST_NM", "TRADE_NM", "E_MAIL", "BIZ_NO"
			, "DUTY_MOBILE" , "DUTY_NM", "DUTY_PHONE", "BIZ_NO_TYPE", "HTTP"
			, "REP_NM", "TEL", "BIZ_COND" , "BIZ_TYPE", "SETTLE_TERM"
			, "ZIP", "ADDR", "FAX", "CREDIT_GRADE", "DLV_CNTL"
			, "CUST_EPC_CD", "MANAGE_GRADE", "MANAGE_EVENT", "MANAGE_BIZ_CONT", "DEAL_TYPE"
			, "USE_CAL_YN", "CAL_TYPE", "SAFE_STOCK_TY", "CLIENT_NEC_YN", "RCV_RCM_TY"
			, "SAP_USE_YN", "SET_RELEASE_YN", "PARCEL_TRACE_YN"
	};
	
	static final String[] COLUMN_NAME_WMSMS010_SIMPLE = {
		"CUST_CD", "CUST_NM", "TRADE_NM", "E_MAIL", "BIZ_NO"
		, "MOBILE_NO", "REP_NM", "TEL", "ZIP", "ADDR", "FAX"
};

	//
	@Resource(name = "WMSMS010Service")
	private WMSMS010Service service;

	/*-
	 * Method ID : mn 
	 * Method 설명 : 화주정보관리 화면 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSMS010.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS010");
	}

	/*-
	 * Method ID : list 
	 * Method 설명 : 화주정보관리 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS010/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save 
	 * Method 설명 : 화주정보관리 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS010/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSMS010/pdf_test.action")
	public ModelAndView generatePdfReport(Map<String, Object> model) {

		ModelAndView mv = new ModelAndView();

		mv.addObject("Board", "TEST");
		mv.addObject("format", "TEST");

		mv.setViewName("helloReport");

		return mv;

	}

	/*-
	 * Method ID : listExcel 
	 * Method 설명 : 로케이션정보 엑셀다운 
	 * 작성자 : 기드온
	 * 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSMS010/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Excel info :", e);
			}
		}
	}

	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
			String[][] headerEx = {
					  {MessageResolver.getMessage("코드"), "0", "0", "0", "0", "200"}
					, {MessageResolver.getMessage("화주명"), "1", "1", "0", "0", "200"}
					, {MessageResolver.getMessage("생성일"), "2", "2", "0", "0", "200"}
					, {MessageResolver.getMessage("생성자"), "3", "3", "0", "0", "200"}
			};
			
			// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
			// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {{"CUST_CD", "S"}, {"CUST_NM", "S"}, {"REG_DT", "S"}, {"REG_NM", "S"}};

			// 파일명
			String fileName = MessageResolver.getText("화주정보관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : WMSMS010E2
	 * Method 설명      : 엑셀업로드 화면
	 * 작성자                 : kwt
	 * @param   model
	 * @param request
	 * @param response	 
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS010E2.action")
	public ModelAndView wmsms010E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS010E2");
	}

	/*-
	 * Method ID    : WMSMS010E3
	 * Method 설명      : 엑셀업로드 화면
	 * 작성자                 : kimzero
	 * @param   model
	 * @param request
	 * @param response	 
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS010E3.action")
	public ModelAndView wmsms010E3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS010E3");
	}
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * 수정 2022.12.23 SUMMER HYUN - 일반엑셀 업로드 헤더 어레이 수정 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS010/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());
			model.put("WORK_IP", request.getRemoteAddr());
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			String uploadType = (String) model.get("uploadType");
			List<Map> list = new ArrayList<Map>();
			if(uploadType.equals("NORMAL")){
				//excelLimitRowRead(File file, String[] cellName, int startSheet, int startRow, int endRow, int startCell)
				//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS010, 0, startRow, 10000, 0);
			    list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS010, 0, startRow, 10000, 0);
			}else if (uploadType.equals("SIMPLE")){
				//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS010_SIMPLE, 0, startRow, 10000, 0);
			    list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSMS010_SIMPLE, 0, startRow, 10000, 0);
			}
			m = service.saveCsv(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID : mn2
	 * Method 설명 : 화주 조회 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS010Q1.action")
	public ModelAndView mn3(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS010Q1");
	}
	
	/*-
	 * Method ID : listQ1
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS010Q1/list.action")
	public ModelAndView listQ1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listQ1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listEmployee
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS010Q1/listEmployee.action")
	public ModelAndView listEmployeeQ1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listEmployeeQ1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : saveLcSync 
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS010/saveLcSync.action")
	public ModelAndView saveLcSync(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveLcSync(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
