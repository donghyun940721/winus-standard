package com.logisall.winus.wmsms.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsms.service.WMSMS097Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSMS097Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS097Service")
	private WMSMS097Service service;

	/*-
	 * Method ID    : wmsms097
	 * Method 설명      : 화주별물류기기 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSMS097.action")
	public ModelAndView wmsms097(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS097", service.selectData(model));
	}

}
