package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSPL060Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSPL060Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPL060Service")
	private WMSPL060Service service;

	/**
	 * Method ID : wmspl060 
	 * Method 설명 : 부자재 관리 화면 
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSPL060.action")
	public ModelAndView wmspl060(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSPL060");
	}

    /**
     * Method ID : list 
     * Method 설명 : 부자재 관리 조회 
     * 작성자 : schan
     * @param model
     * @return
     */
	@RequestMapping("/WMSPL060/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}

	/**
     * Method ID : save
     * Method 설명 : 부자재 저장 
     * 작성자 : schan
     * @param model
     * @return
     */
	@RequestMapping("/WMSPL060/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID : listPool
     * Method 설명 : 부자재 물류용기매핑 리스트 조회 
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WMSPL060/listPool.action")
    public ModelAndView listPool(Map<String, Object> model) {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listPool(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to list :", e);
            }
        }
        return mav;
    }
    
    /**
     * Method ID : savePool
     * Method 설명 : 부자재 물류용기매핑 저장 
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WMSPL060/savePool.action")
    public ModelAndView savePool(Map<String, Object> model) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.savePool(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    
	
	
	

}