package com.logisall.winus.batch.web;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Controller;

import com.logisall.winus.batch.service.CronFileJobService;



@Controller
public class CronFileJobController extends QuartzJobBean{

	CronFileJobService cronFileJobService;
	
	protected Log log = LogFactory.getLog(this.getClass());
	
    @Autowired
    public void setCronFileJobService(CronFileJobService cronFileJobService) {
        this.cronFileJobService = cronFileJobService;
    }

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		/*
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			cronFileJobService.runSpJob(model);
		}catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to Subul Job Service :", e);
			}
		}
	*/	
	}
	
}
