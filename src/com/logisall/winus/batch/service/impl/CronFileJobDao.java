package com.logisall.winus.batch.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("cronFileJobDao")
public class CronFileJobDao extends SqlMapAbstractDAO{
    
    public Object getFileControllDate(Map<String, Object> model){
        return executeQueryForList("batch.fileList", model);
    }

    public Object updateDeletedFileData(Map<String, Object> model){
        return executeUpdate("batch.updateDeletedFileData", model);
    }


}
