package com.logisall.winus.batch.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.batch.service.CronFileJobService;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;



@Service("cronFileJobService")
@SuppressWarnings("PMD")
public class CronFileJobServiceImpl extends AbstractServiceImpl implements CronFileJobService {
	
	Log logger = LogFactory.getLog(this.getClass());
	
	@Resource(name="cronFileJobDao")
	private CronFileJobDao dao;
	    
	public Map<String, Object> runSpJob(Map<String, Object> model) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();	
		
		try {
		    
		    List<Map<String,Object>> list = (List<Map<String,Object>>)dao.getFileControllDate(model);
		    
		    for (int i = 0 ; i < list.size() ; i++){
		        deleteFile(list.get(i));
		        dao.updateDeletedFileData(list.get(i));
		    }



    		map.put("errCnt", 0);
    		// map.put("MSG", MessageResolver.getMessage("save.success"));
            if ( logger.isInfoEnabled() ) {
            	logger.info("[cronFileJob] END... (Success)");
            }
    		
        }catch (Exception e) {
		    if ( logger.isInfoEnabled() ) {
            	logger.info("[cronFileJob] END... (Error Occured) :" + e.getMessage() );
            }  
		    throw e;
        }
		return map;
	}
	
	private void deleteFile(Map<String, Object> map)throws Exception{
	    
	    String lcId = (String)map.get("LC_ID");
	    String custId = (String)map.get("CUST_ID");
	    String date = (String)map.get("FILE_CONTROLL_DATE");
	    
	    String path = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\RealPacking\\"+lcId+"\\"+custId;
	    
	    try{
	        File folder = new File(path);
	        if(!folder.exists()){
	            return;
	        }
	        File[] folderList = folder.listFiles();
	        
	        for(int i = 0 ; i < folderList.length ; i++){
	            String datePath = folderList[i].getPath();
	            String delDate = datePath.replace(path+"\\","");
	            
	            if(Integer.parseInt(date) > Integer.parseInt(delDate)){
	                File delFolder = new File(datePath);
	                if(!delFolder.exists()){
	                    continue;
	                }
	                File[] delFolderList = delFolder.listFiles();
	                for (int j = 0 ; j < delFolderList.length ; j++){
	                    if(delFolderList[j].isFile()){
	                        delFolderList[j].delete();
	                    }
	                }
	                if(delFolder.listFiles().length == 0){
	                    delFolder.delete();
	                }
	            }
	        }
	        
	    }catch(Exception e){
	        e.printStackTrace();
	        throw e;
	    }
	
	}
}
