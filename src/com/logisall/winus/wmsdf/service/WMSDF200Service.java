package com.logisall.winus.wmsdf.service;

import java.util.Map;

public interface WMSDF200Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE0(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> getItemList(Map<String, Object> model) throws Exception;
}
