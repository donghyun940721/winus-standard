package com.logisall.winus.wmsdf.service;

import java.util.Map;

public interface WMSAG501Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubE4(Map<String, Object> model) throws Exception;
}
