package com.logisall.winus.wmsdf.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAG501Dao")
public class WMSAG501Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
        //return executeQueryPageWq("wmsag501.list", model);
    	return executeQueryWq("wmsag501.listE1", model);
    }
    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryWq("wmsag501.listE2", model);
    }
    
    /**
     * Method ID  : listE3
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryWq("wmsag501.listE3", model);
    }

    /**
     * Method ID  : listE4
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE4(Map<String, Object> model) {
        return executeQueryWq("wmsag501.listE4", model);
    }
    
    /**
     * Method ID  : listSubE4
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listSubE4(Map<String, Object> model) {
        return executeQueryWq("wmsag501.listSubE4", model);
    }
  
}


