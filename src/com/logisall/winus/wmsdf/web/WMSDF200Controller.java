package com.logisall.winus.wmsdf.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST010Service;
import com.logisall.winus.wmsdf.service.WMSDF200Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSDF200Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSDF200Service")
    private WMSDF200Service service;
    
    @Resource(name = "WMSST010Service")
	private WMSST010Service WMSST010service;
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
    
    /**
     * Method ID	: WMSDF200
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSDF200.action")
    public ModelAndView wmsts010(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsdf/WMSDF200", WMSST010service.selectBox(model) );
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSDF200/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listE2
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSDF200/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
   
	
	
	 /**
     * Method ID	: listE3
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSDF200/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
	
	
    
	 /**
    * Method ID	: listE4
    * Method 설명	: 
    * 작성자			: yhku
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSDF200/listE4.action")
   public ModelAndView listE4(Map<String, Object> model) throws Exception {
   	ModelAndView mav = null;
       try {
           mav = new ModelAndView("jqGridJsonView", service.listE4(model));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return mav;
   }
   
	 /**
    * Method ID	: listSubE4
    * Method 설명	: 
    * 작성자			: yhku
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSDF200/listSubE4.action")
   public ModelAndView listSubE4(Map<String, Object> model) throws Exception {
   	ModelAndView mav = null;
       try {
           mav = new ModelAndView("jqGridJsonView", service.listSubE4(model));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return mav;
   }
   

  /**
  * Method ID	: listE0
  * Method 설명	: 
  * 작성자			: yjw
  * @param   model
  * @return  
  * @throws Exception 
  */
 @RequestMapping("/WMSDF200/listE0.action")
 public ModelAndView listE0(Map<String, Object> model) throws Exception {
 	ModelAndView mav = null;
     try {
         mav = new ModelAndView("jqGridJsonView", service.listE0(model));
     } catch (Exception e) {
         e.printStackTrace();
     }
     return mav;
 }
 
 /**
  * Method ID	: listE0
  * Method 설명	: 
  * 작성자			: yjw
  * @param   model
  * @return  
  * @throws Exception 
  */
 @RequestMapping("/WMSDF200/getItemList.action")
 public ModelAndView getItemList(Map<String, Object> model) throws Exception {
 	ModelAndView mav = null;
     try {
         mav = new ModelAndView("jqGridJsonView", service.getItemList(model));
     } catch (Exception e) {
         e.printStackTrace();
     }
     return mav;
 }
   
   
    
}
