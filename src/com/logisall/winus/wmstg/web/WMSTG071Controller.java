package com.logisall.winus.wmstg.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG071Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG071Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG071Service")
	private WMSTG071Service service;

	/*-
	 * Method ID    		  : wmstg071
	 * Method 설명     	 	  : 센터별 LOT 상품 조회 화면
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG071.action")
	public ModelAndView wmstg071(Map<String, Object> model) throws Exception {
		ModelAndView mv = new ModelAndView("winus/wmstg/WMSTG071", service.templateData(model));
		return mv;
	}
	
	/*-
	 * Method ID    	     : list
	 * Method 설명        : 센터별 LOT 상품 조회 화면
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG071/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
				mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   		 : listExcel
	 * Method 설명       : 엑셀다운로드
	 * 작성자            : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG071/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : KSJ
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
        	
        	List<String> vrSrchLcIdArr = new ArrayList();
            String[] spVrSrchLcIdArr = model.get("vrSrchLcId").toString().split(",");
            String[] vrLcNmArr = model.get("vrSrchLcNm").toString().split(",");
           
            int colAllCnt = 9;
            int colCnt = 8;
            String [][] headerEx = new String [1 + (spVrSrchLcIdArr.length * colAllCnt)][6];
            headerEx[0] = new String[]{"LOT_NO", "0", "0", "0", "1", "300"};
            
            for(int i = 0; i < spVrSrchLcIdArr.length; ++i){
            	headerEx[(i * colAllCnt) + 1] = new String []{MessageResolver.getText(vrLcNmArr[i])      , String.valueOf((i * colCnt) + 1), String.valueOf((i * colCnt) + 8), "0", "0", "300"};
            	
            	headerEx[(i * colAllCnt) + 2] = new String []{MessageResolver.getText("상품코드")      , String.valueOf((i * colCnt) + 1), String.valueOf((i * colCnt) + 1), "1", "1", "300"};
            	headerEx[(i * colAllCnt) + 3] = new String []{MessageResolver.getText("상품명")      	, String.valueOf((i * colCnt) + 2), String.valueOf((i * colCnt) + 2), "1", "1", "500"};
            	headerEx[(i * colAllCnt) + 4] = new String []{MessageResolver.getText("창고코드")      , String.valueOf((i * colCnt) + 3), String.valueOf((i * colCnt) + 3), "1", "1", "300"};
            	headerEx[(i * colAllCnt) + 5] = new String []{MessageResolver.getText("창고명")      	, String.valueOf((i * colCnt) + 4), String.valueOf((i * colCnt) + 4), "1", "1", "300"};
            	headerEx[(i * colAllCnt) + 6] = new String []{MessageResolver.getText("로케이션명")    , String.valueOf((i * colCnt) + 5), String.valueOf((i * colCnt) + 5), "1", "1", "300"};
            	
            	headerEx[(i * colAllCnt) + 7] = new String []{MessageResolver.getText("UOM")      		, String.valueOf((i * colCnt) + 6), String.valueOf((i * colCnt) + 6), "1", "1", "100"};
            	headerEx[(i * colAllCnt) + 8] = new String []{MessageResolver.getText("재고수량")      , String.valueOf((i * colCnt) + 7), String.valueOf((i * colCnt) + 7), "1", "1", "150"};
            	headerEx[(i * colAllCnt) + 9] = new String []{MessageResolver.getText("출고예정수량")  , String.valueOf((i * colCnt) + 8), String.valueOf((i * colCnt) + 8), "1", "1", "150"};
            }
            
            String [][] valueName = new String [1 + (spVrSrchLcIdArr.length * 8)][2];
            valueName[0] = new String[]{"MAIN_CUST_LOT_NO", "S"};
            
            for(int i = 0; i < spVrSrchLcIdArr.length; ++i){
            	
            	valueName[(i * colCnt) + 1] = new String[]{"RITEM_CD_"+ String.valueOf(i), "S"};
            	valueName[(i * colCnt) + 2] = new String[]{"RITEM_NM_"+ String.valueOf(i), "S"};
            	valueName[(i * colCnt) + 3] = new String[]{"WH_CD_"+ String.valueOf(i)	 , "S"};
            	valueName[(i * colCnt) + 4] = new String[]{"WH_NM_"+ String.valueOf(i)	 , "S"};
            	valueName[(i * colCnt) + 5] = new String[]{"LOC_CD_"+ String.valueOf(i)  , "S"};

            	valueName[(i * colCnt) + 6] = new String[]{"UOM_CD_"+ String.valueOf(i)  , "S"};
            	valueName[(i * colCnt) + 7] = new String[]{"STOCK_QTY_"+ String.valueOf(i),  "S"};
            	valueName[(i * colCnt) + 8] = new String[]{"OUT_EXP_QTY_"+ String.valueOf(i),  "S"};
            }
        
            
            //파일명
            String fileName = MessageResolver.getText("센터별 LOT 재고 조회");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
}
