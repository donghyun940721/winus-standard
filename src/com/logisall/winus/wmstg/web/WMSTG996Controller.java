package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG996Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG996Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG996Service")
    private WMSTG996Service service;
    
    /**
     * Method ID    : wmsTG996
     * Method 설명      : 로케이션별재고조회 화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSTG996.action")
    public ModelAndView wmsTG996(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmstg/WMSTG996", service.selectBox(model));     
    }
}
