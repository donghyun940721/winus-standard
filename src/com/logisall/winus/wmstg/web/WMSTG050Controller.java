package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG050Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG050Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG050Service")
	private WMSTG050Service service;

	/*-
	 * Method ID    : wmstg050
	 * Method 설명      : 유효기간경고재고조회 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG050.action")
	public ModelAndView wmstg050(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG050");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 유효기간경고재고 목록 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG050/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : list2
	 * Method 설명      : 유효기간경고재고 목록 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG050/list2.action")
	public ModelAndView list2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG050/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}

	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("화주")      , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("로케이션")   , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("타입")  		, "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")   , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("수량")      , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("입수")      , "6", "6", "0", "0", "100"},
                                   
                                   {"UOM"                               , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("유효일수")   , "8", "8", "0", "0", "100"},
                                   {MessageResolver.getText("유효기간")   , "9", "9", "0", "0", "100"},
                                   {MessageResolver.getText("제조일자")   , "10", "10", "0", "0", "100"},
                                   {"LOT_NO"                            , "11", "11", "0", "0", "100"},
                                   {MessageResolver.getText("창고")      , "12", "12", "0", "0", "100"},
                                   {"SUB_LOT_ID"      					, "13", "13", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"      , "S"},
                                    {"LOC_CD"       , "S"},
                                    {"LOC_TYPE_NM"       , "S"},
                                    {"RITEM_CD"     , "S"},
                                    {"RITEM_NM"     , "S"},
                                    {"STOCK_QTY"    , "N"},
                                    {"UNIT_NM"    , "N"},
                                    
                                    {"UOM_NM"       , "S"},
                                    {"OVER_DAY"     , "S"},
                                    {"VALID_DT_EXCEL"     , "S"},
                                    {"MAKE_DT_EXCEL", "S"},
                                    {"CUST_LOT_NO"  , "S"},
                                    {"WH_NM"        , "S"},
                                    {"SUB_LOT_ID"        , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("유효기간경고재고조회");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
	/*-
	 * Method ID    : 유통기한 대시보드
	 * Method 설명      : 유통기한 대시보드
	 * 작성자                 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSDH100.action")
	public ModelAndView wmsdh100(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsdh/WMSDH100");
	}
	
	/*-
	 * Method ID    : 유통기한 초과 리스트
	 * Method 설명      : 유통기한 초과 리스트
	 * 작성자                 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDH100/listOut.action")
	public ModelAndView listOut(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listOut(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : 유통기한 경과 리스트
	 * Method 설명      : 유통기한 경과 리스트
	 * 작성자                 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDH100/listIn.action")
	public ModelAndView listIn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listIn(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : 유통기한 대시보드 차트
	 * Method 설명      : 유통기한 대시보드 차트
	 * 작성자                 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDH100/listsCount.action")
	public ModelAndView listsCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listsCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : 유통기한 초과 주문등록
	 * Method 설명      : 유통기한 초과 주문등록
	 * 작성자                 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDH100/saveEndDateOut.action")
	public ModelAndView saveEndDateOut(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.saveEndDateOut(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
