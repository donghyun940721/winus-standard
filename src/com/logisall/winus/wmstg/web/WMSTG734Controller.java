package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST051Service;
import com.logisall.winus.wmsst.service.WMSST065Service;
import com.logisall.winus.wmstg.service.WMSTG732Service;
import com.logisall.winus.wmstg.service.WMSTG733Service;
import com.logisall.winus.wmstg.service.WMSTG734Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG734Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG734Service")
	private WMSTG734Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 일별재고 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG734.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG734");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	

	/*-
	 * Method ID : list
	 * Method 설명 : 일별재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG734/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : listE2
	 * Method 설명 : DPS 보충리스트 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG734/list2.action")
	public ModelAndView list2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listT2
	 * Method 설명 : DPS 보충리스트 조회
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG734/listT2.action")
	public ModelAndView listT2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listT2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}