package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG735Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG735Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG735Service")
	private WMSTG735Service service;
	
	
	/*-
	 * Method ID : TG600E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 화면
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG735.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG735", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	

	@RequestMapping("/WMSTG735/listE01.action")
	public ModelAndView listE01(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			    if (model.get("page") == null) {
	                model.put("pageIndex", "1");
	            } else {
	                model.put("pageIndex", model.get("page"));
	            }
	            if (model.get("rows") == null) {
	                model.put("pageSize", "20");
	            } else {
	                model.put("pageSize", model.get("rows"));
	            }
	            
	            mav = new ModelAndView("jqGridJsonView", service.listE01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	@RequestMapping("/WMSTG735/listE04.action")
	public ModelAndView listE04(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			    if (model.get("page") == null) {
	                model.put("pageIndex", "1");
	            } else {
	                model.put("pageIndex", model.get("page"));
	            }
	            if (model.get("rows") == null) {
	                model.put("pageSize", "20");
	            } else {
	                model.put("pageSize", model.get("rows"));
	            }
	            
	            mav = new ModelAndView("jqGridJsonView", service.listE04(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : listExcelE04
	 * Method 설명 : 엑셀다운
	 * 작성자 : 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSTG735/listExcelE04.action")
	public void listExcelE04(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
	        
			map = service.listExcelE04(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
					this.doExcelDownE04(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
	 /*-
     * Method ID : doExcelDownE04
     * Method 설명 : 
     * 작성자 : 
     *
     * @param response
     * @param grs
     */
    protected void doExcelDownE04(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("날짜")   		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("주문수량")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("주문 등록")	, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("피킹리스트발행")	, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("검수확정")		, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고확정") 	, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("미출고")		, "6", "6", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("출고확정(출고일기준)")  , "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고확정 PCS(출고일기준)")  , "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고확정(브랜드스토어, 출고일기준)") , "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고확정(그외,출고일기준)")  , "10", "10", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("로젠송장(출고일기준)") , "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("로젠단포(출고일기준)") , "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("로젠합포(출고일기준)") , "13", "13", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"OUT_REQ_DT"           , "S"},
                                    {"DAT_SUM_CNT"          , "N"},
                                    {"REQ_ORD_CNT"       	, "N"},
                                    {"PICKING_CNT"      	, "N"},
                                    {"PICKING_COM_CNT"      , "N"},
                                    
                                    {"REQ_COM_CNT"      	, "N"},
                                    {"INCOM_CNT"       	 	, "N"},
                                    
                                    {"COM_CNT"        		, "N"},
                                    {"COM_EA_CNT"        	, "N"},
                                    {"COM_BRAND_CNT"        , "N"},
                                    {"COM_BRANDNO_CNT"      , "N"},
                                    {"ROGEN_CNT"            , "N"},
                                    {"PARCEL_INVC_O_CNT"    , "N"},
                                    {"PARCEL_INVC_M_CNT"    , "N"},
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("일별 전체 작업 진행 현황");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
}