package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG310Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG310Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG310Service")
	private WMSTG310Service service;

	/*-
	 * Method ID : WMSTG310
	 * Method 설명 : 날짜별 임가공 화면
	 * 작성자 : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG310.action")
	public ModelAndView WMSTG310(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG310");
	}

	/*-
	 * Method ID : listE1
	 * Method 설명 :  날짜별 임가공 실적 조회
	 * 작성자 : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG310/listE1.action")
	public ModelAndView listE1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listE2
	 * Method 설명 :  날짜별 임가공 조회
	 * 작성자 : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG310/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : excelDownE1
	 * Method 설명 : 날짜별 임가공내역 실적 엑셀다운
	 * 작성자 : KSJ
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSTG310/excelDownE1.action")
	public void excelDownE1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDownE1(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE1(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    

    /*-
     * Method ID : doExcelDownE1
     * Method 설명 : 날짜별 임가공 실적 내역 excel write
     * 작성자 : KSJ
     *
     * @param response
     * @param grs
     */
	protected void doExcelDownE1(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		 {MessageResolver.getMessage("화주코드")	,		"0", "0", "0", "0", "100"},
	                                 {MessageResolver.getMessage("화주명"), 			"1", "1", "0", "0", "100"},
	                                 {MessageResolver.getMessage("주문유형"),			"2", "2", "0", "0", "80"},
	                                 {MessageResolver.getMessage("상품코드"), 		"3", "3", "0", "0", "200"},
	                                 {MessageResolver.getMessage("상품명"),   			"4", "4", "0", "0", "200"},
	                                 {MessageResolver.getMessage("세트상품코드"),   "5", "5", "0", "0", "200"},
	                                 {MessageResolver.getMessage("세트상품명"),  	"6", "6", "0", "0", "200"},
	                                 {MessageResolver.getMessage("부속상품코드"),   "7", "7", "0", "0", "200"},
	                                 {MessageResolver.getMessage("부속상품명"), 	 	"8", "8", "0", "0", "200"},
	                                 {MessageResolver.getMessage("로케이션명"),  	"9", "9", "0", "0", "200"},
	                                 {MessageResolver.getMessage("ORI수량"), 			"10", "10", "0", "0", "80"},
	                                 {MessageResolver.getMessage("수량"),  				"11", "11", "0", "0", "80"},
	                                 {MessageResolver.getMessage("작업수량"),  		"12", "12", "0", "0", "80"},
	                                 {MessageResolver.getMessage("UOM"),  			"13", "13", "0", "0", "80"}
                                 };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					                    {"CUST_CD"   			, "S"},
					                    {"CUST_NM"   		, "S"},
					                    {"ORD_TYPE"  			, "S"},
					                    {"ITEM_CODE"  		, "S"},
					                    {"ITEM_NM" 			, "S"},
					                    {"SET_RITEM_CD"  	, "S"},
					                    {"SET_RITEM_NM"   	, "S"},
					                    {"PART_RITEM_CD"  	, "S"},
					                    {"PART_RITEM_NM" , "S"},
					                    {"LOC_CD"  			, "S"},
					                    {"ORI_QTY" 			, "NR"},
					                    {"QTY"  					, "NR"},
					                    {"WORK_QTY"  		, "NR"},
					                    {"UOM_NM"  			, "S"}
					                   }; 
			// 파일명
			String fileName = MessageResolver.getText("날짜별 임가공 실적 내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
    
	/*-
	 * Method ID : excelDownE2
	 * Method 설명 : 날짜별 임가공내역 엑셀다운
	 * 작성자 : KSJ
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSTG310/excelDownE2.action")
	public void excelDownE2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDownE2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    

    /*-
     * Method ID : doExcelDownE2
     * Method 설명 : 날짜별 임가공내역 excel write
     * 작성자 : KSJ
     *
     * @param response
     * @param grs
     */
	protected void doExcelDownE2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		 {MessageResolver.getMessage("작업일자"),	"0", "0", "0", "0", "200"},
	                                 {MessageResolver.getMessage("주문유형"), 	"1", "1", "0", "0", "200"},
	                                 {MessageResolver.getMessage("상품코드"),	"2", "2", "0", "0", "200"},
	                                 {MessageResolver.getMessage("상품명"), 	"3", "3", "0", "0", "200"},
	                                 {MessageResolver.getMessage("수량"),   	"4", "4", "0", "0", "200"},
	                                 {MessageResolver.getMessage("셋트여부"),  "5", "5", "0", "0", "200"}
                                 };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					                    {"WORK_DT"   	, "S"},
					                    {"ORD_TYPE"   	, "S"},
					                    {"RITEM_CD"  	, "S"},
					                    {"RITEM_NM"  	, "S"},
					                    {"WORK_QTY" 	, "NR"},
					                    {"SET_ITEM_YN"  	, "S"}
					                   }; 
			// 파일명
			String fileName = MessageResolver.getText("날짜별 임가공내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
