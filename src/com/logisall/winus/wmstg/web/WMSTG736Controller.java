package com.logisall.winus.wmstg.web;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG730Service;
import com.logisall.winus.wmstg.service.WMSTG736Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG736Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG736Service")
	private WMSTG736Service service;
	
	/*-
	 * Method ID		 : WMSTG736
	 * Method 설명    : 올리브영 배송지연 화면 
	 * 작성자 			 : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG736.action")
	public ModelAndView WMSTG736(Map<String, Object> model) throws Exception {
		// return new ModelAndView("winus/wmstg/WMSTG736", service.selectCountry(model));
		return new ModelAndView("winus/wmstg/WMSTG736");
	}
	
	/*-
	 * Method ID    		 : list
	 * Method 설명        : 리브영 배송지연 리드 타임 조회 리스트
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG736/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    		 : WMSTG736Q1
	 * Method 설명        : 국가 조회 popup
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
    @RequestMapping("/WMSTG736Q1.action")
    public ModelAndView WMSTG736Q1(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmstg/WMSTG736Q1");
    }
    
    /*-
	 * Method ID    		 : listQ1
	 * Method 설명        : 국가 조회 
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG736/listQ1.action")
	public ModelAndView listQ1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listQ1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    		 : listSummary
	 * Method 설명        : 올리브영 배송지연 리드타임 요약 (국가별, 등급별)
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG736/listSummary.action")
	public ModelAndView listSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}
