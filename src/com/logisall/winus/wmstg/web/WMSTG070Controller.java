package com.logisall.winus.wmstg.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmstg.service.WMSTG070Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSTG070Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG070Service")
	private WMSTG070Service service;
	
	static final String[] COLUMN_NAME_WMSTG070_updateLotNo = {
		"LOT_NO",
		"ITEM_CD",
		"OWNER_CD"
	};	
	
	/*-
	 * Method ID    : wmstg070
	 * Method 설명      : LOT별재고조회 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG070.action")
	public ModelAndView wmstg070(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG070", service.selectItemGrp(model));
	}
	
	
	/*-
	 * Method ID    : wmstg090
	 * Method 설명      : LOT별재고조회 화면(new)
	 * 작성자                 : ykim
	 * @param   model
	 * @desc    jqgrid => spreadjs 로 변경개발.
	 * @date    21.09.08
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG090.action")
	public ModelAndView wmstg090(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG090", service.selectItemGrp(model));
	}
	

	/*-
	 * Method ID    : list
	 * Method 설명      : LOT별재고 목록 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG070/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			//KCC홈씨씨 조회로직 분리
			if(String.valueOf(model.get("SS_SVC_NO")).equals("0000002940")){
				mav = new ModelAndView("jqGridJsonView", service.list_kcc(model));
			}else{
				mav = new ModelAndView("jqGridJsonView", service.list(model));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG070/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {"LOT_NO"                           		 , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("LOCK")      	 , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("화주")     	 , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("창고")     	 , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("로케이션")  	 , "4", "4", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("상품코드")      , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     	 , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("수량")  	     , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("무게")  	     , "8", "8", "0", "0", "100"},
                                   {"UOM"                                	 , "9", "9", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("입고처코드")  , "10", "10", "0", "0", "100"},
                                   {"UNIT_NO"                            	 , "11", "11", "0", "0", "100"},
                                   {MessageResolver.getText("컨테이너번호")  , "12", "12", "0", "0", "100"},
                                   {MessageResolver.getText("박스바코드")	 	 , "13", "13", "0", "0", "100"},
                                   {MessageResolver.getText("생성일자")	 	 , "14", "14", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_LOT_NO"	, "S"},
                                    {"LOCK_YN"    	, "S"},
                                    {"CUST_NM"    	, "S"},
                                    {"WH_NM"      	, "S"},
                                    {"LOC_CD"     	, "S"},
                                    
                                    {"RITEM_CD"   	, "S"},
                                    {"RITEM_NM"   	, "S"},
                                    {"STOCK_QTY"  	, "N"},
                                    {"STOCK_WEIGHT" , "N"},
                                    {"UOM_NM"     	, "S"},
                                    {"OWNER_CD"		, "S"},
                                    {"UNIT_NO"    	, "S"},
                                    {"CNTR_NO"    	, "S"},
                                    {"BOX_BAR_CD"  	, "S"},
                                    {"REG_DT"     	, "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("LOT별재고");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
    
    /*-
	 * Method ID    : shippingTamplate
	 * Method 설명      : LOT별재고현황 저장
	 * 작성자                 : SUMMER HYUN
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG070/shippingTemplate.action")
	public ModelAndView shippingTemplate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.shippingTemplate(model);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
    
    /*-
	 * Method ID    : save
	 * Method 설명      : LOT별재고현황 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG070/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
   /*-
	 * Method ID    	: delLotNo
	 * Method 설명      : LOT별재고현황 -> Lot 번호 삭제
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG070/delLotNo.action")
	public ModelAndView delLotNo(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delLotNo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID  	: WMSTG070Q2_BIZ
	 * Method 설명  : LOT 수정(비즈컨설팅) BOX -> PCS
	 * 작성자       : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG070Q2_BIZ.action")
	public ModelAndView WMSTG070Q2_BIZ(Map<String, Object> model) {
		return new ModelAndView("winus/wmstg/WMSTG070Q2_BIZ");
	}
	
	/*-
	 * Method ID 		: uploadLotNo
	 * Method 설명 		: LOT 수정 엑셀 업로드 
	 * 작성자 			: KSJ
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG070/uploadLotNo.action")
	public ModelAndView uploadLotNo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			model.put("WORK_IP", request.getRemoteAddr());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			String uploadType = (String) model.get("uploadType");
			
			List<Map> list = new ArrayList<Map>();			
			//list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSTG070_updateLotNo, 0, startRow, 10000, 0);
			list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSTG070_updateLotNo, 0, startRow, 10000, 0);

			m = service.decomposeLotNo(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listSummaryCount
	 * Method 설명      : LOT별 재고현황 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG070/listSummaryCount.action")
	public ModelAndView listSummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listSummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}
