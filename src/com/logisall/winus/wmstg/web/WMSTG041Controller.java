package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG041Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG041Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG041Service")
	private WMSTG041Service service;

	/*-
	 * Method ID    : wmstg041
	 * Method 설명      : 로케이션별재고조회 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG041.action")
	public ModelAndView wmstg041(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG041", service.selectBox(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 로케이션별별재고 목록 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG041/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG041/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}
    
	/*-
	 * Method ID    	: listSummaryCount
	 * Method 설명      : 로케이션별 재고현황 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG041/listSummaryCount.action")
	public ModelAndView listSummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listSummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getText("ZONE")       , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("로케이션")      , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("로케이션타입")      , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("창고")         , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("화주")         , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")      , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("물류코드")  	   , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")       , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("상품바코드")      , "8", "8", "0", "0", "100"},
                                   {MessageResolver.getText("수량")         , "9", "9", "0", "0", "100"},
                                   {MessageResolver.getText("출고예정수량")         , "10", "10", "0", "0", "100"},
                                   {MessageResolver.getText("가용수량")         , "11", "11", "0", "0", "100"},
                                   {MessageResolver.getText("유통기한")      , "12", "12", "0", "0", "100"},
                                   {MessageResolver.getText("입수")        , "13", "13", "0", "0", "100"},
                                   {"UOM"                                  , "14", "14", "0", "0", "100"},
                                   {MessageResolver.getText("적치율")+"(%)" , "15", "15", "0", "0", "100"},
                                   {MessageResolver.getText("가용")+"PLT"   , "16", "16", "0", "0", "100"},
                                   {MessageResolver.getText("LOT번호")   , "17", "17", "0", "0", "100"},
                                   {MessageResolver.getText("화주상품코드")      , "18", "18", "0", "0", "150"}
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"ZONE_NM"        , "S"},
                                    {"LOC_CD"         , "S"},
                                    {"LOC_TYPE_NM"    , "S"},
                                    {"WH_NM"          , "S"},
                                    {"CUST_NM"        , "S"},
                                    {"RITEM_CD"       , "S"},
                                    {"BOX_BAR_CD"     , "S"},
                                    {"RITEM_NM"       , "S"},
                                    {"ITEM_BAR_CD"     , "S"},
                                    {"STOCK_QTY"      , "N"},
                                    {"OUT_EXP_QTY"    , "N"},
                                    {"OUT_ABLE_QTY"    , "N"},
                                    {"ITEM_BEST_DATE_END"  , "S"},
                                    {"UNIT_NM"      , "S"},
                                    {"UOM_NM"         , "S"},
                                    {"LOC_PER"        , "S"},
                                    {"REMAIN_PLT_QTY" , "N"},
                                    {"CUST_LOT_NO"    , "S"},
                                    {"CUST_ITEM_CD"     , "S"}
                                   }; 
            //파일명
            String fileName = MessageResolver.getText("존별별재고현황");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
    
    /*-
	 * Method ID    : listExcelV2
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG041/excelV2.action")
	public void listExcelV2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownV2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDownV2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getText("ZONE")       , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("로케이션")      , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("ERP코드")      , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("창고")         , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("화주")         , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")      , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")       , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("사이즈")       , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("브랜드")       , "8", "8", "0", "0", "100"},
                                   {MessageResolver.getText("색상")       , "9", "9", "0", "0", "100"},
                                   {MessageResolver.getText("화주상품코드")       , "10", "10", "0", "0", "100"},
                                   {MessageResolver.getText("수량")         , "11", "11", "0", "0", "100"},
                                   {MessageResolver.getText("입고예정수량")         , "12", "12", "0", "0", "100"},
                                   {MessageResolver.getText("출고예정수량")         , "13", "13", "0", "0", "100"},
                                   {"UOM"                                  			, "14", "14", "0", "0", "100"},
                                   {MessageResolver.getText("적치율")+"(%)" , "15", "15", "0", "0", "100"},
                                   {MessageResolver.getText("가용")+"PLT"   , "16", "16", "0", "0", "100"},
                                   {MessageResolver.getText("LOT번호")   , "17", "17", "0", "0", "100"},
                                   {MessageResolver.getText("유통기한")   , "18", "18", "0", "0", "100"}
                                   
  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"ZONE_NM"        , "S"},
                                    {"LOC_CD"         , "S"},
                                    {"ERP_WH_CD"         , "S"},
                                    {"WH_NM"          , "S"},
                                    {"CUST_NM"        , "S"},
                                    {"RITEM_CD"       , "S"},
                                    {"RITEM_NM"       , "S"},
                                    {"ITEM_SIZE"         , "S"},
                                    {"MAKER_NM"         , "S"},
                                    {"COLOR"         , "S"},
                                    {"CUST_ITEM_CD"         , "S"},
                                    {"STOCK_QTY"      , "N"},
                                    {"IN_EXP_QTY"      , "N"},
                                    {"OUT_EXP_QTY"      , "N"},
                                    {"UOM_NM"         , "S"},
                                    {"LOC_PER"        , "S"},
                                    {"REMAIN_PLT_QTY" , "N"},
                                    {"CUST_LOT_NO"    , "S"},
                                    {"ITEM_BEST_DATE_END"    , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("존별별재고현황(APPAREL)");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
    
	/*-
	 * Method ID    	: listExcelBiz
	 * Method 설명      : 엑셀다운로드(비즈컨설팅)
	 * 작성자           : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG041/listExcelBiz.action")
	public void listExcelBiz(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelBiz(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownBiz(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}
    
    
    /*-
     * Method ID : doExcelDownBiz
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDownBiz(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
        	String[][] headerEx = {
        			{MessageResolver.getText("ZONE")       , "0", "0", "0", "0", "100"},
                    {MessageResolver.getText("로케이션")       , "1", "1", "0", "0", "100"},
                    {MessageResolver.getText("창고")           , "2", "2", "0", "0", "100"},
                    {MessageResolver.getText("화주")           , "3", "3", "0", "0", "100"},
                    {MessageResolver.getText("상품코드")       , "4", "4", "0", "0", "100"},
                    {MessageResolver.getText("박스바코드")     , "5", "5", "0", "0", "100"},
                    {MessageResolver.getText("상품명")         , "6", "6", "0", "0", "100"},
                    {MessageResolver.getText("스타일")         , "7", "7", "0", "0", "100"},
                    {MessageResolver.getText("컬러")           , "8", "8", "0", "0", "100"},
                    {MessageResolver.getText("사이즈")         , "9", "9", "0", "0", "100"},
                    {MessageResolver.getText("수량")           , "10", "10", "0", "0", "100"},
                    {MessageResolver.getText("입고예정수량")   , "11", "11", "0", "0", "100"},
                    {MessageResolver.getText("출고예정수량")   , "12", "12", "0", "0", "100"},
                    {MessageResolver.getText("가용수량")   	   , "13", "13", "0", "0", "100"},
                    {MessageResolver.getText("유통기한")       , "14", "14", "0", "0", "100"},
                    {"UOM"                                     , "15", "15", "0", "0", "100"},
                    {MessageResolver.getText("적치율")+"(%)"   , "16", "16", "0", "0", "100"},
                    {MessageResolver.getText("가용")+"PLT"     , "17", "17", "0", "0", "100"},
                    {MessageResolver.getText("LOT번호")        , "18", "18", "0", "0", "100"}
                    
                   };
			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {
								 {"ZONE_NM"        , "S"},
			                     {"LOC_CD"         , "S"},
			                     {"WH_NM"          , "S"},
			                     {"CUST_NM"        , "S"},
			                     {"RITEM_CD"       , "S"},
			                     {"BOX_BAR_CD"     , "S"},
			                     {"RITEM_NM"       , "S"},
			                     {"CUST_LEGACY_ITEM_CD", "S"},
			                     {"COLOR"          , "S"},
			                     {"ITEM_SIZE"      , "S"},
			                     {"STOCK_QTY"      , "N"},
                                 {"IN_EXP_QTY"      , "N"},
                                 {"OUT_EXP_QTY"      , "N"},
                                 {"OUT_ABLE_QTY"      , "N"},
			                     {"ITEM_BEST_DATE_END"  , "S"},
			                     {"UOM_NM"         , "S"},
			                     {"LOC_PER"        , "S"},
			                     {"REMAIN_PLT_QTY" , "N"},
			                     {"CUST_LOT_NO"    , "S"}
			                    }; 
            
            //파일명
            String fileName = MessageResolver.getText("존별별재고현황");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
}
