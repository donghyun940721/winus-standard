package com.logisall.winus.wmstg.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST051Service;
import com.logisall.winus.wmsst.service.WMSST065Service;
import com.logisall.winus.wmstg.service.WMSTG732Service;
import com.logisall.winus.wmstg.service.WMSTG723Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSTG723Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG723Service")
	private WMSTG723Service service;

	/*-
	 * Method ID : WMSTG723
	 * Method 설명 : 입고검수조회 화면 접근
	 * 작성자 : kijun11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG723.action")
	public ModelAndView WMSTG723(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			System.out.println("testtesttest");
			mav = new ModelAndView("winus/wmstg/WMSTG723");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 입고검수내역조회
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list2
	 * Method 설명 : 입고검수내역상세조회
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/list2.action")
	public ModelAndView list2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*
	 * Method ID : deleteAll
	 * Method 설명 : 입고검수내역 주문별 삭제
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/deleteAll.action")
	public ModelAndView deleteAll(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			m = service.deleteAll(model);
		} catch (Exception e) {
		//	System.out.println("controller catch");
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID : delete
	 * Method 설명 : 입고검수내역삭제
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/delete.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			m = service.delete(model);
		} catch (Exception e) {
		//	System.out.println("controller catch");
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : update
	 * Method 설명 : 입고검수내역수정
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/update.action")
	public ModelAndView update(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			m = service.update(model);
		} catch (Exception e) {
		//	System.out.println("controller catch");
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : ordComplete
	 * Method 설명 : 검수내역 입고확정
	 * 작성자      : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/ordComplete.action")
	public ModelAndView ordComplete(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = new HashMap<String,Object>();
		try {
			m = service.ordComplete(model);
			mav = new ModelAndView("jsonView", m);

			} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : WMTG723Q1
	 * Method 설명      : 신규원주문 POP 화면
	 * 작성자                 : SUMMER
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG723/WMSTG723Q1.action")
	public ModelAndView wmstg723q1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG723Q1");
	}
	
	/*-
	 * Method ID : selectNewOmOrd
	 * Method 설명 : SAP 신규 입고 내역 조회
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/selectNewOmOrd.action")
	public ModelAndView selectNewOmOrd(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.selectNewOmOrd(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	//updateNewOrgOrdNo
	/*-
	 * Method ID : updateNewOrgOrdNo
	 * Method 설명 : 신규원주문번호로 검수내역 변경
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/updateNewOrgOrdNo.action")
	public ModelAndView updateNewOrgOrdNo(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			m = service.updateNewOrgOrdNo(model);
		} catch (Exception e) {
		//	System.out.println("controller catch");
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : updateNewOrgOrdNoV2
	 * Method 설명 : 신규원주문번호로 검수내역 변경
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG723/updateNewOrgOrdNoV2.action")
	public ModelAndView updateNewOrgOrdNoV2(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			System.out.println("controller send");
			m = service.updateNewOrgOrdNoV2(model);
			System.out.println("controller out");

		} catch (Exception e) {
		//	System.out.println("controller catch");
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", 1);
		}
		mav.addAllObjects(m);
		return mav;
	}
}