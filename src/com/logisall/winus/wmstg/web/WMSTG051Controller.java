package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG051Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG051Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG051Service")
    private WMSTG051Service service;

    /*-
     * Method ID    : wmstg050
     * Method 설명      : 유효기간경고재고조회 화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSTG051.action")
    public ModelAndView wmstg051(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmstg/WMSTG051");
    }

    /*-
     * Method ID    : list
     * Method 설명      : 유효기간경고재고 목록 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTG051/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;

        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("fail to get list...", e);
            }
        }
        return mav;
    }

 

    /*-
     * Method ID    : listExcel
     * Method 설명      : 엑셀다운로드
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @RequestMapping("/WMSTG051/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcel(model);
            GenericResultSet grs = (GenericResultSet) map.get("LIST");
            if (grs.getTotCnt() > 0) {
                this.doExcelDown(response, grs);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("fail to download excel...", e);
            }
        }
    }

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try {
            // 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            // 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                    { MessageResolver.getText("로케이션"), "0", "0", "0", "0", "100" },
                    { MessageResolver.getText("상품코드"), "1", "1", "0", "0", "100" },
                    { MessageResolver.getText("상품명"), "2", "2", "0", "0", "180" },
                    { MessageResolver.getText("수량"), "3", "3", "0", "0", "100" },
                    { MessageResolver.getText("입고일자"), "4", "4", "0", "0", "100" },
                    { MessageResolver.getText("관리일자"), "5", "5", "0", "0", "100" },
                    { MessageResolver.getText("관리일자(경과일수)"), "6", "6", "0", "0", "100" },                
                    { MessageResolver.getText("유효기간"), "7", "7", "0", "0", "100" },
                    { MessageResolver.getText("철수기준일"), "8", "8", "0", "0", "100" },
                    { MessageResolver.getText("철수기준"), "9", "9", "0", "0", "100" },
                    { MessageResolver.getText("잔여일수(경과일수)"), "10", "10", "0", "0", "150" },
                    { MessageResolver.getText("유효일수"), "11", "11", "0", "0", "100" },
                    { MessageResolver.getText("구분"), "12", "12", "0", "0", "100" }
            };
            // {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
            // 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                    { "LOC_CD", "S" },
                    { "RITEM_CD", "S" },
                    { "RITEM_NM", "S" },
                    { "STOCK_QTY", "N" },
                    { "IN_DT", "S" },
                    { "IN_ADD_DT", "S" },
                    { "IN_ADD_CAL", "N" },
                    { "VALID_DT", "S" },
                    { "OUT_DAY", "S" },
                    { "OUT_BEST_DATE_NUM", "N" },
                    { "NEXT_DAY", "N" },
                    { "OVER_DAY", "N" },
                    { "TOTAL_DAY", "S" }
            };

            // 파일명
            String fileName = MessageResolver.getText("유효기간경고재고조회");
            // 시트명
            String sheetName = "Sheet1";
            // 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
            String marCk = "N";
            // ComUtil코드
            String etc = "";

            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc,
                    response);
            // wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName,
            // sheetName, marCk, etc, response);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("fail download Excel file...", e);
            }
        }
    }

    /*-
     * Method ID    : 유통기한 대시보드
     * Method 설명      : 유통기한 대시보드
     * 작성자                 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSDH101.action")
    public ModelAndView wmsdh101(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsdh/WMSDH101");
    }

}
