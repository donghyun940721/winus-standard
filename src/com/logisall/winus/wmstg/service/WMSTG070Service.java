package com.logisall.winus.wmstg.service;

import java.util.List;
import java.util.Map;


public interface WMSTG070Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> shippingTemplate(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> delLotNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> decomposeLotNo(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> listSummaryCount(Map<String, Object> model) throws Exception;
}
