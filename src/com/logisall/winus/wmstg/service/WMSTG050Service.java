package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG050Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listIn(Map<String, Object> model) throws Exception;
    public Map<String, Object> listOut(Map<String, Object> model) throws Exception;
    public Map<String, Object> listsCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveEndDateOut(Map<String, Object> model) throws Exception;
}
