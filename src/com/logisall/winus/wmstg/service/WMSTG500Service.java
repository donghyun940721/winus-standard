package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG500Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSum(Map<String, Object> model) throws Exception; 
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;

}
