package com.logisall.winus.wmstg.service;

import java.util.List;
import java.util.Map;


public interface WMSTG723Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteAll(Map<String, Object> model) throws Exception;
    public Map<String, Object> update(Map<String, Object> model) throws Exception;
    public Map<String, Object> ordComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectNewOmOrd(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateNewOrgOrdNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateNewOrgOrdNoV2(Map<String, Object> model) throws Exception;
}
