package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG310Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> excelDownE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelDownE2(Map<String, Object> model) throws Exception;
}
