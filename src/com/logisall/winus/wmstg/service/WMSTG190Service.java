package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG190Service {
	public Map<String, Object> selectLocList(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> zoneList(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listGubun(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetailLocSet(Map<String, Object> model) throws Exception;
}
