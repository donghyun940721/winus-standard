package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmstg.service.WMSTG041Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG041Service")
public class WMSTG041ServiceImpl implements WMSTG041Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG041Dao")
    private WMSTG041Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 로케이션별재고 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID      : listBizExcel
     * 대체 Method 설명    : 로케이션별재고 목록 조회 (비즈컨설팅 엑셀조회)
     * 작성자              : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcelBiz(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listBizExcel(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 창고별재고 목록 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }  
    
    /**
     * Method ID   		: listSummaryCount
     * Method 설명      : 로케이션별 재고현황 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("LIST", dao.listSummaryCount(model));

        return map;
    }
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ZONEGUBUN", dao.selecZone(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}
