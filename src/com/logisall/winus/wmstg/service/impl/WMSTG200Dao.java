package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG200Dao")
public class WMSTG200Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : selectPoolGrp
     * Method 설명  : 화면내 필요한 용기군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectPoolGrp(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 개체재고및이력추적 개체재고리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmstg200.list", model);
    }   

    /**
     * Method ID  : listSub 
     * Method 설명  : 개체재고및이력추적 이력추적이동
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listSub(Map<String, Object> model){
        return executeQueryPageWq("wmstg200.listSub", model);
    }
    
    /**
     * Method ID  : DetailPop 
     * Method 설명  : 개체재고및이력추적 이력추적이동
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet DetailPop(Map<String, Object> model){
        return executeQueryPageWq("wmstg200.DetailPop", model);
    }
}
