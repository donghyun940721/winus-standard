package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG500Dao")
public class WMSTG500Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID  : selectLocList
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object toYearList(Map<String, Object> model){
        return executeQueryForList("wmstg500.toYearList", model);
    }
    
    /**
     * Method ID  : selectItemGrp
     * Method �ㅻ�  : ��硫대�� ������ ����援� 媛��몄�ㅺ린
     * ���깆��             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
	/**
     * Method ID : listBlNo
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listBlNo(Map<String, Object> model) {
        return executeQueryPageWq("wmstg500.listBlNo", model);
    }
    
    /**
  * Method ID : listBarcode
  * Method 설명 : 재고 입출고내역 조회 (바코드)
  * 작성자 : 기드온
  * @param model
  * @return
  */
 public GenericResultSet listBarcode(Map<String, Object> model) {
     return executeQueryPageWq("wmstg500.listBarcode", model);
 }   

    /**
     * Method ID  : save
     * Method 설명  : 재고 입출고내역 작업일자, 작업id 수정 프로시저
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object save(Map<String, Object> model){
        return  executeUpdate("wmstg500.pk_wmsop001.sp_set_workdate", model);
    }
    
    /**
     * Method ID    : listSum
     * Method 설명      : 재고 입출고내역 합계
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object listSum(Map<String, Object> model) {
        return executeView("wmstg500.listSum", model);
    }
    /**
     * Method ID    : listBarcodeSum
     * Method 설명      : 재고 입출고내역 합계(바코드)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object listBarcodeSum(Map<String, Object> model) {
        return executeView("wmstg500.listBarcodeSum", model);
    }
}
