package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG731Dao")
public class WMSTG731Dao extends SqlMapAbstractDAO{

	/**
	 * Method ID 		: listQ1
	 * Method 설명   : 국가 조회
	 * 작성자			: KSJ
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ1(Map<String, Object> model) {
		return executeQueryPageWq("wmstg731.listQ1", model);
	}
    
    /**
     * Method ID : list
     * Method 설명 : 올리브영 배송지연 리드 타임 조회 리스트
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) throws Exception {
		return executeQueryPageWq("wmstg731.list", model);
	}
    
    /**
     * Method ID : listSummary
     * Method 설명 : 올리브영 배송지연 리드타임 요약 (국가별, 등급별)
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet listSummary(Map<String, Object> model) throws Exception {
		return executeQueryPageWq("wmstg731.listSummary", model);
	}
}
