package com.logisall.winus.wmstg.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG700Dao")
public class WMSTG700Dao extends SqlMapAbstractDAO{

		/**
     * Method ID : list
     * Method 설명 : 배차정보 리스트
     * 작성자 : 공통
     * @param model
     * @return
     */
	public Object list(Map<String, Object> model) {
		List list = executeQueryForList("wmsms050.dspList", model);
		GenericResultSet genericResultSet = new GenericResultSet();
		genericResultSet.setList(list);
		return genericResultSet;
	}    
    
	
}
