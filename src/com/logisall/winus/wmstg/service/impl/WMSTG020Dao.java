package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG020Dao")
public class WMSTG020Dao extends SqlMapAbstractDAO{

	/**
     * Method ID  : selectItemGrp
     * Method �ㅻ�  : ��硫대�� ������ ����援� 媛��몄�ㅺ린
     * ���깆��             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 창고별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.whItemList", model);
    }
    
    /**
     * Method ID    : listSub
     * Method 설명      : 창고별 재고 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.whItemSubList", model);
    }
}
