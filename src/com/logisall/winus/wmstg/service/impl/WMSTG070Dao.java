package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG070Dao")
public class WMSTG070Dao extends SqlMapAbstractDAO{

	/**
     * Method ID  : selectItemGrp
     * Method �ㅻ�  : ��硫대�� ������ ����援� 媛��몄�ㅺ린
     * ���깆��             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : LOT별재고조회 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	
    	String str = (String) model.get("vrSrchBox");
    	
    	if(str != null && !str.equals("")){
    		return executeQueryPageWq("wmstg070.lotItemListBiz", model); // 비즈컨설팅 box 조회
    	}else{
    		return executeQueryPageWq("wmstg070.lotItemList", model);
    	}
        
    }

    /**
     * Method ID    : list
     * Method 설명      : LOT별재고조회 목록 조회(kcc홈씨씨)
     * 작성자                 : ykim
     * @param   model
     * @return
     */
    public GenericResultSet list_kcc(Map<String, Object> model) {
        return executeQueryPageWq("wmstg070.lotItemList_kcc", model);
    }
    
    /**
	 * Method ID : update 
	 * Method 설명 : LOT별재고현황 저장
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmstg070.update", model);
	}
	 /**
	 * Method ID : update 
	 * Method 설명 : LOT별재고현황 저장
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update_ownerCd(Map<String, Object> model) {
		return executeUpdate("wmstg070.update_ownerCd", model);
	}
	
    /**
	 * Method ID 	: delLotNo 
	 * Method 설명  : LOT별재고현황 -> Lot 번호 삭제
	 * 작성자 		: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object delLotNo(Map<String, Object> model) {
		return executeUpdate("wmstg070.delLotNo", model);
	}
	
	 /**
	 * Method ID 	: getLotNo 
	 * Method 설명  : Lot No 조회
	 * 작성자 		: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public int getLotNo(Map<String, Object> mode) {
		return (int)executeView("wmstg070.getLotNo", mode);
	}
	
	 /**
	 * Method ID   : decomposeCustLotNo 
	 * Method 설명 : 비즈컨설팅 LOT 해제 BOX -> PCS
	 * 작성자      : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object decomposeCustLotNo(Map<String, Object> model) {
		return executeUpdate("wmstg070.decomposeCustLotNo", model);
	}
	
	 /**
     * Method ID   		: listSummaryCount
     * Method 설명      : LOT별 재고현황 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listSummaryCount(Map<String, Object> model) {
		return executeView("wmstg070.listSummaryCount", model);
	}
}
