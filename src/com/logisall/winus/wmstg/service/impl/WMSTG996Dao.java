package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG996Dao")
public class WMSTG996Dao extends SqlMapAbstractDAO{

	/**
     * Method ID  : selectLocList
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectLocList(Map<String, Object> model){
        return executeQueryForList("wmstg180.selectLocList", model);
    }
	
	/**
     * Method ID  : selectPool
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmsmo907.selectPool", model);
    }
}


