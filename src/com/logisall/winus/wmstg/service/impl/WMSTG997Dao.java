package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG997Dao")
public class WMSTG997Dao extends SqlMapAbstractDAO{

	/**
     * Method ID  : selectLocList
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectLocList(Map<String, Object> model){
        return executeQueryForList("wmstg997.selectLocList", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmstg997.locStockList", model);
    }

    /**
     * Method ID    : listGubunst
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listGubun(Map<String, Object> model) {
        return executeQueryPageWq("wmstg997.listGubunSet", model);
    }
    
    /**
     * Method ID    : listDetail
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmstg997.listDetail", model);
    }
    
    /**
     * Method ID    : listDetailLocSet
     * Method 설명      : 로케이션별재고 목록 조회 (LOC 지정 상태 일 경우)
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetailLocSet(Map<String, Object> model) {
        return executeQueryPageWq("wmstg997.listDetailLocSet", model);
    }
    
    /**
     * Method ID : searchDetail
     * Method 설명 : 재고이동 pop 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet searchDetailType1(Map<String, Object> model) {
        return executeQueryPageWq("wmstg997.searchDetailType1", model);
    }
    
    /**
     * Method ID : searchDetail
     * Method 설명 : 재고이동 pop 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet searchDetailType2(Map<String, Object> model) {
        return executeQueryPageWq("wmstg997.searchDetailType2", model);
    }

    /**
     * Method ID : searchDetail
     * Method 설명 : 재고이동 pop 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet searchDetailType3(Map<String, Object> model) {
        return executeQueryPageWq("wmstg997.searchDetailType3", model);
    }
    
    /**
	 * Method ID : reMapping 
	 * Method 설명 : 재매핑 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object reMapping_WMSRF010(Map<String, Object> model) {
		return executeUpdate("wmstg997.reMapping_WMSRF010", model);
	}
	
	/**
	 * Method ID : reMapping 
	 * Method 설명 : 재매핑 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object reMapping_WMSRF011(Map<String, Object> model) {
		return executeUpdate("wmstg997.reMapping_WMSRF011", model);
	}
	
	/**
     * Method ID  : selectPool
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmsmo907.selectPool", model);
    }
    
    /**
     * Method ID  : locSetSearch
     * Method 설명  : locSetSearch
     * 작성자             : 
     * @param model
     * @return
     */
    public Object locSetSearch(Map<String, Object> model){
        return executeQueryForList("wmstg997.locSetSearch", model);
    }
    
    /**
     * Method ID  : locSetSearchRun
     * Method 설명  : locSetSearchRun
     * 작성자             : 
     * @param model
     * @return
     */
    public Object locSetSearchRun(Map<String, Object> model){
        return executeQueryForList("wmstg997.locSetSearchRun", model);
    }
    
    /**
     * Method ID  : errCntView
     * Method 설명  : errCntView
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object errCntView(Map<String, Object> model){
        return executeQueryForList("wmstg997.errCntView", model);
    }
    
    /**
	 * Method ID : updateLocStat 
	 * Method 설명 : LOC상태변경 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateLocStat(Map<String, Object> model) {
		return executeUpdate("wmsop030.updateLocStat", model);
	}
	
	/**
     * Method ID  : selectLocStat
     * Method 설명  : selectLocStat
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectLocStat(Map<String, Object> model){
        return executeQueryForList("wmstg997.selectLocStat", model);
    }
    
    /**
     * Method ID  : initChkLoc
     * Method 설명  : initChkLoc
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object initChkLoc(Map<String, Object> model){
        return executeQueryForList("wmstg997.initChkLoc", model);
    }
}


