package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG220Dao")
public class WMSTG220Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    public GenericResultSet detailList(Map<String, Object> model) {
        return executeQueryPageWq("wmstg220.detailList", model);
    }
    
    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model) {
	return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectLoc01(Map<String, Object> model) {
	return executeQueryForList("wmstg220.selectLoc01", model);
    }
    
    /**
     * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listItem(Map<String, Object> model) {
	return executeQueryPageWq("wmstg220.listItem", model);
    }
}

