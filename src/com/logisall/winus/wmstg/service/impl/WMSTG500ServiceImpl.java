package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmstg.service.WMSTG500Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG500Service")
public class WMSTG500ServiceImpl extends AbstractServiceImpl implements WMSTG500Service {
    
    @Resource(name = "WMSTG500Dao")
    private WMSTG500Dao dao;

    /**
     * 
     * 대체 Method ID   : selectLocList
     * 대체 Method 설명    : 제품군별재고조회 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("YEAR", dao.toYearList(model));
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if(model.get("vrViewGrn").equals("N")){
                model.put("vrViewGrn", "");
            }else{
                model.put("vrViewGrn", "21");
            }
            if(model.get("vrViewIn").equals("N")){
                model.put("vrViewIn", "");
            }else{
                model.put("vrViewIn", "20");
            }
            if(model.get("vrViewPicking").equals("N")){
                model.put("vrViewPicking", "");
            }else{
                model.put("vrViewPicking", "31");
            }
            if(model.get("vrViewOut").equals("N")){
                model.put("vrViewOut", "");
            }else{
                model.put("vrViewOut", "30");
            }
            if(model.get("vrViewEtc").equals("N")){
                model.put("vrViewEtc", "");
                model.put("vrViewEtc2", "");
            }else{
                model.put("vrViewEtc", "50");
                model.put("vrViewEtc2", "60");
            }
            String vrViewGrn = (String)model.get("vrViewGrn");
            String vrViewIn = (String)model.get("vrViewIn");
            String vrViewPicking = (String)model.get("vrViewPicking");
            String vrViewOut = (String)model.get("vrViewOut");
            String vrViewEtc = (String)model.get("vrViewEtc");           
            String vrViewEtc2 = (String)model.get("vrViewEtc2");           
            
            String chekbox[] = {vrViewGrn, vrViewIn, vrViewPicking, vrViewOut, vrViewEtc, vrViewEtc2};
            model.put("chekbox", chekbox);
    
            if(!model.get("vrSrchBarcode").equals("")){
//                바코드내역조회
                map.put("LIST", dao.listBarcode(model));
            }else if(!model.get("vrSrchBlNo").equals("")){
//                B/L 입출고내역 조회
                map.put("LIST", dao.listBlNo(model));
            }else{
//                B/L 입출고내역 조회
                map.put("LIST", dao.listBlNo(model));
            }
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listSum
     * 대체 Method 설명    : 재고 입출고내역 합계
     * 작성자                      : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSum(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("vrViewGrn").equals("N")){
            model.put("vrViewGrn", "");
        }else{
            model.put("vrViewGrn", "21");
        }
        if(model.get("vrViewIn").equals("N")){
            model.put("vrViewIn", "");
        }else{
            model.put("vrViewIn", "20");
        }
        if(model.get("vrViewPicking").equals("N")){
            model.put("vrViewPicking", "");
        }else{
            model.put("vrViewPicking", "31");
        }
        if(model.get("vrViewOut").equals("N")){
            model.put("vrViewOut", "");
        }else{
            model.put("vrViewOut", "30");
        }
        if(model.get("vrViewEtc").equals("N")){
            model.put("vrViewEtc", "");
            model.put("vrViewEtc2", "");
        }else{
            model.put("vrViewEtc", "50");
            model.put("vrViewEtc2", "60");
        }
        String vrViewGrn = (String)model.get("vrViewGrn");
        String vrViewIn = (String)model.get("vrViewIn");
        String vrViewPicking = (String)model.get("vrViewPicking");
        String vrViewOut = (String)model.get("vrViewOut");
        String vrViewEtc = (String)model.get("vrViewEtc");           
        String vrViewEtc2 = (String)model.get("vrViewEtc2");           
        
        String chekbox[] = {vrViewGrn, vrViewIn, vrViewPicking, vrViewOut, vrViewEtc, vrViewEtc2};
        model.put("chekbox", chekbox);
        if(!model.get("vrSrchBarcode").equals("")){
//          바코드내역 합계
            map.put("SUM", dao.listBarcodeSum(model));
      }else if(!model.get("vrSrchBlNo").equals("")){
//          B/L 입출고내역 합계
          map.put("SUM", dao.listSum(model));
      }else{
//          B/L 입출고내역 합계
          map.put("SUM", dao.listSum(model));

      }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 재고 입출고내역 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        
        if(model.get("vrViewGrn").equals("N")){
            model.put("vrViewGrn", "");
        }else{
            model.put("vrViewGrn", "21");
        }
        if(model.get("vrViewIn").equals("N")){
            model.put("vrViewIn", "");
        }else{
            model.put("vrViewIn", "20");
        }
        if(model.get("vrViewPicking").equals("N")){
            model.put("vrViewPicking", "");
        }else{
            model.put("vrViewPicking", "31");
        }
        if(model.get("vrViewOut").equals("N")){
            model.put("vrViewOut", "");
        }else{
            model.put("vrViewOut", "30");
        }
        if(model.get("vrViewEtc").equals("N")){
            model.put("vrViewEtc", "");
            model.put("vrViewEtc2", "");
        }else{
            model.put("vrViewEtc", "50");
            model.put("vrViewEtc2", "60");
        }
        String vrViewGrn = (String)model.get("vrViewGrn");
        String vrViewIn = (String)model.get("vrViewIn");
        String vrViewPicking = (String)model.get("vrViewPicking");
        String vrViewOut = (String)model.get("vrViewOut");
        String vrViewEtc = (String)model.get("vrViewEtc");           
        String vrViewEtc2 = (String)model.get("vrViewEtc2");           
        
        String chekbox[] = {vrViewGrn, vrViewIn, vrViewPicking, vrViewOut, vrViewEtc, vrViewEtc2};
        model.put("chekbox", chekbox);
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listBlNo(model));
        
        return map;
    }
    
    
    /**
     * Method ID : save
     * Method 설명 : 재고입출고내역 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] iWorkDt = new String[iuCnt]; 
            String[] iWorkId = new String[iuCnt]; 
            
            for(int i = 0 ; i < iuCnt; i ++){
                iWorkDt[i]      = (String)model.get("WORK_DT"+i);
                iWorkId[i]      = (String)model.get("WORK_ID"+i);
            }
          
            if(iuCnt > 0){
               Map<String, Object> modelSP = new HashMap<String, Object>();
         
               modelSP.put("I_WORK_ID", iWorkId);
               modelSP.put("I_WORK_DT", iWorkDt);
               modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
               modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
          
               dao.save(modelSP);
               m.put("MSG", MessageResolver.getMessage("update.success"));
               m.put("errCnt", errCnt);
             }
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
