package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG190Dao")
public class WMSTG190Dao extends SqlMapAbstractDAO{

	/**
     * Method ID  : selectLocList
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectLocList(Map<String, Object> model){
        return executeQueryForList("wmstg190.selectLocList", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmstg190.locStockList", model);
    }

    /**
     * Method ID : zoneList
     * Method 설명 : ZONE정보등록 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet zoneList(Map<String, Object> model) {
        return executeQueryPageWq("wmstg190.zoneList", model);
    }
    
    /**
     * Method ID : sublist
     * Method 설명 : ZONE정보등록 서브 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model) {
        return executeQueryPageWq("wmstg190.list", model);
    }
    
    /**
     * Method ID    : listGubun
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listGubun(Map<String, Object> model) {
        return executeQueryPageWq("wmstg190.listGubunSet", model);
    }
    
    /**
     * Method ID    : listDetail
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmstg190.listDetail", model);
    }
    
    /**
     * Method ID    : listDetailLocSet
     * Method 설명      : 로케이션별재고 목록 조회 (LOC 지정 상태 일 경우)
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetailLocSet(Map<String, Object> model) {
        return executeQueryPageWq("wmstg190.listDetailLocSet", model);
    }
}


