package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;









import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsst.service.WMSST065Service;
import com.logisall.winus.wmstg.service.WMSTG732Service;
import com.logisall.winus.wmstg.service.WMSTG723Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Service("WMSTG723Service")
public class WMSTG723ServiceImpl extends AbstractServiceImpl implements WMSTG723Service {
    
    @Resource(name = "WMSTG723Dao")
    private WMSTG723Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 입고검수내역조회
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list2
     * Method 설명 : 입고검수내역상세조회
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : deleteAll
     * Method 설명 : 입고검수내역주문별삭제
     * 작성자 : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteAll(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	
    	try {
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
				 Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("ORG_ORD_ID", 				model.get("ORG_ORD_ID" + i));
	             modelDt.put("ORG_ORD_SEQ", 				model.get("ORG_ORD_SEQ" + i));
	             modelDt.put("SOURCE_ORD_SEQ", 				model.get("SOURCE_ORD_SEQ" + i));
	             modelDt.put("SOURCE_ORD_ID", 				model.get("SOURCE_ORD_ID" + i));
	             modelDt.put("LC_ID", 					(String)model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("CUST_ID", 				model.get("CUST_ID"));
	             modelDt.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
	             dao.deleteAll(modelDt);
    		}
    		
    		map.put("MSG", MessageResolver.getMessage("delete.success"));
    		map.put("errCnt", 0);
    		
    	} catch (Exception e) {
    		map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("errCnt", "1");
    	}
    	
    	return map;
    }
    
    /**
     * Method ID : delete
     * Method 설명 : 입고검수내역삭제
     * 작성자 : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	
    	try {
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
				 Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("RCV_CHK_ID", 				model.get("RCV_CHK_ID" + i));
	             modelDt.put("LC_ID", 					(String)model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("CUST_ID", 				model.get("CUST_ID"));
	             modelDt.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
	             dao.delete(modelDt);
    		}
    		
    		map.put("MSG", MessageResolver.getMessage("delete.success"));
    		map.put("errCnt", 0);
    		
    	} catch (Exception e) {
    		map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("errCnt", "1");
    	}
    	
    	return map;
    }
    
    /**
     * Method ID : update
     * Method 설명 : 입고검수내역수정
     * 작성자 : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> update(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	
    	try {
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
				 Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("RCV_CHK_ID", 				model.get("RCV_CHK_ID" + i));
	             modelDt.put("CONF_QTY", 				model.get("CONF_QTY" + i));
	             modelDt.put("LC_ID", 					(String)model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("CUST_ID", 				model.get("CUST_ID"));
	             modelDt.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
	             dao.update(modelDt);
    		}
    		
    		map.put("MSG", MessageResolver.getMessage("save.success"));
    		map.put("errCnt", 0);
    		
    	} catch (Exception e) {
    		map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("errCnt", "1");
    	}
    	
    	return map;
    }

    /**
     * 
     * 대체 Method ID   : ordComplete
     * 대체 Method 설명    : 검수내역 입고확정
     * 작성자               	  : SUMMER HYUN
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> ordComplete(Map<String, Object> model) throws Exception {
    	 
    	 Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		int tempCnt = Integer.parseInt(model.get("selectIds").toString());
    		System.out.println("impl tempCnt - "+tempCnt);
    		if(tempCnt > 0){
    			
    			//필요한 값 DB로 보내서 조회 
    			
    			String[] ordId = new String[tempCnt];
    			String[] ordSeq = new String[tempCnt];
    			String[] custId = new String[tempCnt];
    			String[] ritemId = new String[tempCnt];
    			String[] lcId = new String[tempCnt];

    			String[] itemBestDateEnd = new String[tempCnt];
    			String[] makeDate = new String[tempCnt];
    			String[] workQty = new String[tempCnt];

    			String[] boxBarCd = new String[tempCnt];
    			String[] toLocCd = new String[tempCnt];
    			String[] boxInQty = new String[tempCnt];
    			String[] itemBarCd = new String[tempCnt];
    			String[] workMemo = new String[tempCnt];

    			String[] custLotNo = new String[tempCnt];
    			
    			for(int i = 0 ; i < tempCnt ; i ++){
    				ordId[i]		= (String)model.get("ORG_ORD_ID"+i);
    				ordSeq[i]		= (String)model.get("ORG_ORD_SEQ"+i);
    				custId[i]		= (String)model.get("CUST_ID");    				
    				ritemId[i]		= (String)model.get("RITEM_ID"+i);
    				lcId[i]			=(String)model.get(ConstantIF.SS_SVC_NO);

    				itemBestDateEnd[i]		= null;
    				makeDate[i]		= null;
    				workQty[i]		= (String)model.get("TOTAL_WORK_QTY"+i);

    				boxBarCd[i]		= null;
    				toLocCd[i]		= null;
    				boxInQty[i]		= null;
    				itemBarCd[i]	= null;
    				workMemo[i]		= null;

    				custLotNo[i]	= null;
    			}
    			m.put("LC_ID", lcId);
    			m.put("ORD_ID", ordId);
    			m.put("ORD_SEQ", ordSeq);
    			m.put("CUST_ID", custId);
    			m.put("RITEM_ID", ritemId);

    			m.put("ITEM_BEST_DATE_END", itemBestDateEnd);
    			m.put("MAKE_DATE", makeDate);
    			m.put("WORK_QTY", workQty);
    			
    			m.put("BOX_BAR_CD", boxBarCd);
    			m.put("TO_LOC_CD", toLocCd);
    			m.put("BOX_IN_QTY", boxInQty);
    			m.put("ITEM_BAR_CD", itemBarCd);
    			m.put("WORK_MEMO", workMemo);

    			m.put("CUST_LOT_NO", custLotNo);
    			
    			//수정자, WORK_IP
				m.put("WORK_IP",(String)model.get(ConstantIF.SS_CLIENT_IP));
				m.put("USER_NO",(String)model.get(ConstantIF.SS_USER_NO));
				
				System.out.println("impl -  "+m.toString());
				
				dao.ordComplete(m);
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   m.put("errCnt", 1);
    	   m.put("MSG_ORA", e.getMessage());
    	}
       
        return m;
    }
    
    /**
     * Method ID : selectNewOmOrd
     * Method 설명 : SAP신규입고조회
     * 작성자 : SUMMER
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectNewOmOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.selectNewOmOrd(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : updateNewOrgOrdNo
     * Method 설명 : 신규원주문번호로 검수내역 변경
     * 작성자 : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateNewOrgOrdNo(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	
    	try {
    	//	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
				 Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("orgOrdNo", 				model.get("orgOrdNo"));
	             modelDt.put("orgOrdSeq", 				model.get("orgOrdSeq"));
	             modelDt.put("newOrgOrdNo", 				model.get("newOrgOrdNo"));
	             modelDt.put("newOrgOrdSeq", 				model.get("newOrgOrdSeq"));
	             modelDt.put("lcId", 					(String)model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("custId", 				model.get("custId"));
	             modelDt.put("userNo", 				(String)model.get(ConstantIF.SS_USER_NO));
	             dao.updateNewOrgOrdNo(modelDt);
    		//}
    		
    		map.put("MSG", MessageResolver.getMessage("save.success"));
    		map.put("errCnt", 0);
    		
    	} catch (Exception e) {
    		map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("errCnt", "1");
    	}
    	
    	return map;
    }
    
    
    
    /*
    * Method ID : updateNewOrgOrdNoV2
    * Method 설명 : 신규원주문번호로 검수내역 변경
    * 작성자 : SUMMER H
    * @param model
    * @return
    * @throws Exception
    */
   @Override
   public Map<String, Object> updateNewOrgOrdNoV2(Map<String, Object> model) throws Exception {
   	Map<String, Object> map = new HashMap<String, Object>();
   	
   	
   	try {
   		System.out.println("service imple");
   		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
				 Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("orgOrdNo", 				model.get("orgOrdNo"+i));
	             modelDt.put("orgOrdSeq", 				model.get("orgOrdSeq"+i));
	             modelDt.put("newOrgOrdNo", 			model.get("newOrgOrdNo"+i));
	             modelDt.put("newOrgOrdSeq", 			model.get("newOrgOrdSeq"+i));
	             modelDt.put("newOrgOrdQty", 			model.get("newOrgOrdQty"+i));
	             modelDt.put("lcId", 					(String)model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("custId", 					model.get("vrSrchCustId"));
	             modelDt.put("userNo", 					(String)model.get(ConstantIF.SS_USER_NO));
	             
	             System.out.println(modelDt.toString());
	             
	             dao.updateNewOrgOrdNo(modelDt);
   		}
   		
   		map.put("MSG", MessageResolver.getMessage("save.success"));
   		map.put("errCnt", 0);
   		
   	} catch (Exception e) {
   		map.put("MSG", MessageResolver.getMessage("save.error"));
   		map.put("errCnt", "1");
   	}
   	
   	return map;
   }
}

