package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG130Dao")
public class WMSTG130Dao extends SqlMapAbstractDAO{

    /**
     * Method ID    : list
     * Method 설명      : 거래처별출고내역 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms050.transItemList", model);
    }
    
    /**
     * Method ID  : selectComboCode_TRANS
     * Method 설명  : selectComboCode 운송사조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectComboCodeTrans(Map<String, Object> model){
        return executeQueryForList("wmsms050.selectComboCode_TRANS", model);
    }
}
