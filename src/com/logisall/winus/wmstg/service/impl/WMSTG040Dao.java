package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG040Dao")
public class WMSTG040Dao extends SqlMapAbstractDAO{

    /**
     * Method ID    : list
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	
//    	System.out.println("vrVirtualLocation : "+model.get("vrVirtualLocation"));
//    	System.out.println("vrEmptyLocation : "+model.get("vrEmptyLocation"));
//    	System.out.println("vrTotalLocation : "+model.get("vrTotalLocation"));
    
    	if(model.get("vrTotalLocation") != null ){ // 전체 로케이션 조회
    		return executeQueryPageWq("wmsms080.emptyStockLocationList", model);
		}else{
			if(model.get("vrEmptyLocation") == null ){ // 빈 로케이션 조회
				return executeQueryPageWq("wmsms080.loItemList", model);
	    	} else {
	    		return executeQueryPageWq("wmsms080.emptyLocationList", model);
	    	}
		}
    }

    /**
     * Method ID    : listBizExcel
     * Method 설명  : 로케이션별재고 목록 조회 (비즈컨설팅)
     * 작성자       : KSJ
     * @param   model
     * @return
     */
    public GenericResultSet listBizExcel(Map<String, Object> model) {
    	
    	if(model.get("vrEmptyLocation") == null ){ // 빈 로케이션 조회
			return executeQueryPageWq("wmsms080.loItemListBiz", model);
    	} else {
    		return executeQueryPageWq("wmsms080.emptyLocationList", model);
    	}
    	
    }
}
