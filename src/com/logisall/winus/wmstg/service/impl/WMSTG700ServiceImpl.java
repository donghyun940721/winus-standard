package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmstg.service.WMSTG700Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG700Service")
public class WMSTG700ServiceImpl implements WMSTG700Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSTG700Dao")
    private WMSTG700Dao dao;

    /**
     * Method ID : list
     * Method 설명 : 배차정보 
     * 작성자 : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
           
                       
            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	
}