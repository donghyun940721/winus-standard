package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmstg.service.WMSTG200Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG200Service")
public class WMSTG200ServiceImpl extends AbstractServiceImpl implements WMSTG200Service {
    
    @Resource(name = "WMSTG200Dao")
    private WMSTG200Dao dao;

    /**
     * Method ID   : selectPoolGrp
     * Method 설명    : 물류용기출고관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("POOLGRP", dao.selectPoolGrp(model));
        return map;
    }

    /**
     * Method ID    : list
     * Method 설명      : 개체재고및이력추적 개체재고리스트
     * 작성자                 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }           
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    } 

    /**
     * 
     * 대체 Method ID   : listSub
     * 대체 Method 설명    : 개체재고및이력추적 이력추적이동
     * 작성자                      : chSong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listSub(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 대체 Method ID   : DetailPop
     * 대체 Method 설명    : 상품 목록 필요 데이타셋
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> DetailPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.DetailPop(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 물류용기 현재고 엑셀리스트
     * 작성자                      : chSong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
}