package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG051Dao")
public class WMSTG051Dao extends SqlMapAbstractDAO{

    /**
     * Method ID    : list
     * Method 설명      : 유효기간경고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	return executeQueryPageWq("wmstg051.dayItemList", model);
    }
    
    
    
}
