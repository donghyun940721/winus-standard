package com.logisall.winus.wmstg.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.JsonUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmscm.service.impl.WMSCM300Dao;
import com.logisall.winus.wmstg.service.WMSTG071Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG071Service")
public class WMSTG071ServiceImpl implements WMSTG071Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG071Dao")
    private WMSTG071Dao dao;
    
    @Resource(name = "WMSCM300Dao")
    private WMSCM300Dao dao030;

    
    private final static String[] CHECK_VALIDATE_WMSTG071 = {"STOCK_ID", "LOCK_YN"};
    
    /**
     * 
     * 대체 Method ID   	  : templateData
     * 대체 Method 설명       : 센터별 LOT 상품 조회 -> select 옵션값, 컬럼 정보
     * 작성자                 : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> templateData(Map<String, Object> model) throws Exception {
     	Map<String, Object> map = new HashMap<String, Object>();
    	
     	if(!model.containsKey("LC_ID")){
    		String lcId = (String)model.get("SS_SVC_NO");
    		model.put("LC_ID", lcId);
    	}
        try {
        	long defaultTemplateCount = dao030.defaultTemplateCount(model);
    		if(defaultTemplateCount < 1){
            	
    			dao030.createDefaultTemplate(model);
        		dao030.createDefaultColumns(model);
            	
            }
    		
    		model.put("vrTemplateType", "WMSTG071");
    		
    		List<Map<String, Object>> list = dao.colInfoList(model);
    		
    		for(int i = 0; i < list.size(); ++i){
    			String colName = (String) list.get(i).get("COLUMN_NAME");
    			list.get(i).put("COLUMN_NAME", MessageResolver.getMessage(colName));
    		}
    		
    		map.put("colInfoList", JsonUtil.getJsonStringFromList(list));
    		map.put("List", dao.colInfoList(model));
        		        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }
        
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   	  : list
     * 대체 Method 설명      : 센터별 LOT 상품 조회 
     * 작성자                      : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        
        char alpha = 65 ;
        List<Map<String, Object>> arrSubQry = new ArrayList();
        List<Map<String, Object>> arrQry = new ArrayList();
        
        // LOT_NO array 
        List<String> vrSrchLotNoArr = new ArrayList();
        String[] spVrSrchLotNoArr = model.get("vrSrchLotNo").toString().split(",");
        
        for (String keyword : spVrSrchLotNoArr ){
        	vrSrchLotNoArr.add(keyword.trim());
        }

        // subQuery & select & lc_id Array
        List<String> vrSrchLcIdArr = new ArrayList();
        String[] spVrSrchLcIdArr = model.get("vrSrchLcId").toString().split(",");
        
        List<String> arrSelects = new ArrayList();
        List<String> arrJoin = new ArrayList();
        
        int colSeq = 0;
        
        for (String keyword : spVrSrchLcIdArr ){
        	keyword = keyword.trim();
        	vrSrchLcIdArr.add(keyword);
        	
        	Map<String, Object> mapSubQry = new HashMap<String, Object>();
        	mapSubQry.put("lcId", keyword);
        	mapSubQry.put("custLotNo", spVrSrchLotNoArr);
        	mapSubQry.put("alias", String.valueOf(alpha));
        	mapSubQry.put("colSeq", colSeq);
        	
        	arrSelects.add("," + String.valueOf(alpha) + ".*");
        	arrJoin.add("AND MAIN.MAIN_CUST_LOT_NO = " + String.valueOf(alpha) + ".CUST_LOT_NO_"+ colSeq +" (+)");
        	
        	arrSubQry.add(mapSubQry);
        	alpha += 1;
        	colSeq += 1;
        }
        
        model.put("vrSrchLotNoArr", vrSrchLotNoArr);
        model.put("vrSrchLcIdArr", vrSrchLcIdArr);
        model.put("arrSubQry"	 , arrSubQry);
        model.put("arrSelects"	 , arrSelects);
        model.put("arrJoin"		 , arrJoin);
        
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   	  : listExcel
     * 대체 Method 설명      : 센터별 LOT 상품 조회 엑셀
     * 작성자                      : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        char alpha = 65 ;
        List<Map<String, Object>> arrSelectQry = new ArrayList();
        List<Map<String, Object>> arrSubQry = new ArrayList();
        List<Map<String, Object>> arrQry = new ArrayList();
        
        // LOT_NO array 
        List<String> vrSrchLotNoArr = new ArrayList();
        String[] spVrSrchLotNoArr = model.get("vrSrchLotNo").toString().split(",");
        
        for (String keyword : spVrSrchLotNoArr ){
        	vrSrchLotNoArr.add(keyword.trim());
        }

        // subQuery & select & lc_id Array
        List<String> vrSrchLcIdArr = new ArrayList();
        String[] spVrSrchLcIdArr = model.get("vrSrchLcId").toString().split(",");
        
        List<String> arrJoin = new ArrayList();
        
        int colSeq = 0;
        
        for (String keyword : spVrSrchLcIdArr ){
        	keyword = keyword.trim();
        	vrSrchLcIdArr.add(keyword);
        	
        	Map<String, Object> mapSelectQry = new HashMap<String, Object>();
        	//mapSelectQry.put("alias", "," + String.valueOf(alpha));
        	mapSelectQry.put("colSeq", colSeq);
        	
        	Map<String, Object> mapSubQry = new HashMap<String, Object>();
        	mapSubQry.put("lcId", keyword);
        	mapSubQry.put("custLotNo", spVrSrchLotNoArr);
        	mapSubQry.put("alias", String.valueOf(alpha));
        	mapSubQry.put("colSeq", colSeq);
        	
        	arrJoin.add("AND MAIN.MAIN_CUST_LOT_NO = " + String.valueOf(alpha) + ".CUST_LOT_NO_"+ colSeq +" (+)");
        	
        	arrSelectQry.add(mapSelectQry);
        	arrSubQry.add(mapSubQry);
        	
        	alpha += 1;
        	colSeq += 1;
        }
        
        model.put("vrSrchLotNoArr", vrSrchLotNoArr);
        model.put("vrSrchLcIdArr", vrSrchLcIdArr);
        model.put("arrSubQry"	 , arrSubQry);
        model.put("arrSelectQry" , arrSelectQry);
        model.put("arrJoin"		 , arrJoin);
        
        map.put("LIST", dao.excelList(model));
        return map;
    }  
}
