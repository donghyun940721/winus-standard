package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmstg.service.WMSTG620Service;
//import com.logisall.winus.wmsms.service.impl.WMSMS090Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG620Service")
public class WMSTG620ServiceImpl implements WMSTG620Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSTG620Dao")
    private WMSTG620Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSTG070 = {"STOCK_ID", "LOCK_YN"};

	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	

    /**
     * Method ID : list
     * Method 설명 : 통계관리 > 재고조정이력조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
                       
            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	
	/**
     * Method ID : listExcel
     * Method 설명 : 재고조정이력조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }

	@Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("UNIT_AMT"  , model.get("UNIT_AMT"+i));
                modelDt.put("ORD_ID"    , model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"    , model.get("ORD_SEQ"+i));
                modelDt.put("RITEM_ID"   , model.get("RITEM_ID"+i));
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            
            //한번이라도 "D"면 삭제
            if (strGubun == "D")
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
            else
            	m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	
}