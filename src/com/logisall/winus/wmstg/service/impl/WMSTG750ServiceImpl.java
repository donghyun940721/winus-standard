package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmstg.service.WMSTG750Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG750Service")
public class WMSTG750ServiceImpl implements WMSTG750Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSTG750Dao")
    private WMSTG750Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSTG070 = {"STOCK_ID", "LOCK_YN"};

	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	
	@Override
	public Map<String, Object> listE01(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.listE01(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	@Override
	public Map<String, Object> listE02(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listE02(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	
	@Override
	public Map<String, Object> listE03(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listE03(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	@Override
	public Map<String, Object> listE04(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (model.get("page") == null) {
				model.put("pageIndex", "1");
			} else {
				model.put("pageIndex", model.get("page"));
			}
			if (model.get("rows") == null) {
				model.put("pageSize", "20");
			} else {
				model.put("pageSize", model.get("rows"));
			}
			map.put("LIST", dao.listE04(model));
			
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}
	
	
	@Override
	public Map<String, Object> list01Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE01(model));
        return map;
	}
    
	@Override
	public Map<String, Object> list02Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE02(model));
        return map;
	}
	
	@Override
	public Map<String, Object> list03Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE03(model));
        return map;
	}
	
	@Override
	public Map<String, Object> list04Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		model.put("pageIndex", "1");
		model.put("pageSize", "60000");
		map.put("LIST", dao.listE04(model));
		return map;
	}
	
	
	
}