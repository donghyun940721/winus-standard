package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmstg.service.WMSTG310Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG310Service")
public class WMSTG310ServiceImpl extends AbstractServiceImpl implements WMSTG310Service {
    
    @Resource(name = "WMSTG310Dao")
    private WMSTG310Dao dao;
    

    /**
     * Method ID : listE1
     * Method 설명 : 날짜별 임가공 실적 조회
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.listE1(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listE2
     * Method 설명 : 날짜별 임가공 조회
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.listE2(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : excelDownE1
     * Method 설명 : 날짜별 임가공 실적 내역 excel
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> excelDownE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE1(model));
        
        return map;
    }
    
    /**
     * Method ID : excelDownE2
     * Method 설명 : 날짜별 임가공내역 excel
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> excelDownE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE2(model));
        
        return map;
    }
}
