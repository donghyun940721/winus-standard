package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG723Dao")
public class WMSTG723Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 출고박스조회
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmstg723.list", model);
    }   
    
    /**
     * Method ID : list2
     * Method 설명 : 출고박스조회
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
        return executeQueryWq("wmstg723.list2", model);
    } 
    
    /**
     * Method ID  : deleteAll 
     * Method 설명  : 입고검수내역 주문별 삭제
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int deleteAll(Map<String, Object> model) {
       return executeUpdate("wmstg723.updateInspectionDelYnAll", model);
       
    } 
    
    /**
     * Method ID  : delete 
     * Method 설명  : 입고검수내역삭제
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int delete(Map<String, Object> model) {
       return executeUpdate("wmstg723.updateInspectionDelYn", model);
       
    } 
    /**
     * Method ID  : update 
     * Method 설명  : 입고검수내역수정
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int update(Map<String, Object> model) {
       return executeUpdate("wmstg723.updateInspectionQty", model);
       
    } 
    
    /**
     * Method ID : ordComplete
     * Method 설명 : 검수내역 입고확정
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public Object ordComplete(Map<String, Object> model) {
    	executeUpdate("wmstg723.pk_wmsop021.sp_oms_op_mobile_in_complete", model);
    	return model;
    }
    
    /**
     * Method ID : selectNewOmOrd
     * Method 설명 : SAP신규입고조회
     * 작성자 : SUMMER H
     * @param model
     * @return
     */
    public GenericResultSet selectNewOmOrd(Map<String, Object> model) {
        return executeQueryWq("wmstg723.selectNewOmOrd", model);
    }  
    
    /**
     * Method ID  : updateNewOrgOrdNo 
     * Method 설명  : 신규원주문번호로 검수내역 변경
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int updateNewOrgOrdNo(Map<String, Object> model) {
       return executeUpdate("wmstg723.updateNewOrgOrdNo", model);
       
    } 

    /**
     * Method ID  : updateNewOrgOrdNoV2 
     * Method 설명  : 신규원주문번호로 검수내역 변경
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int updateNewOrgOrdNoV2(Map<String, Object> model) {
       return executeUpdate("wmstg723.updateNewOrgOrdNoV2", model);
       
    } 
}

