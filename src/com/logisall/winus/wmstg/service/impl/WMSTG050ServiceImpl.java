package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmstg.service.WMSTG050Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG050Service")
public class WMSTG050ServiceImpl implements WMSTG050Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG050Dao")
    private WMSTG050Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 유효기간경고 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : list2
     * 대체 Method 설명    : 유효기간경고 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list2(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 유효기간경고재고 목록 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : listIn
     * 대체 Method 설명    : 유효기간 대시보드 경과리스트
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
	@Override
	public Map<String, Object> listIn(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listIn(model));
        return map;
	}
    /**
     * 
     * 대체 Method ID   : listOut
     * 대체 Method 설명    : 유효기간 대시보드 초과리스트
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
	@Override
	public Map<String, Object> listOut(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listOut(model));
        return map;
	}
    /**
     * 
     * 대체 Method ID   : listsCount
     * 대체 Method 설명    : 유효기간 대시보드 차트리스트
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
	@Override
	public Map<String, Object> listsCount(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listsCount(model));
        return map;
	}  
	
	/**
	 * 
	 * 대체 Method ID   : saveEndDateOut
	 * 대체 Method 설명    : 유효기간 초과 주문 등록
	 * 작성자                      : sing09
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> saveEndDateOut(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		
		try{
            int listBodyCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(listBodyCnt > 0){
	            String[] lcId		  = new String[listBodyCnt];         
	            String[] custId       = new String[listBodyCnt];     
	            String[] ritemId      = new String[listBodyCnt];     
	            String[] uomId		  = new String[listBodyCnt];     
	            String[] subLotId     = new String[listBodyCnt];    
	            String[] workQty	  = new String[listBodyCnt];     
	            String[] ordDesc	  = new String[listBodyCnt];     
	           
	            for (int i = 0; i < listBodyCnt; i++) {
	                lcId[i]         = (String)model.get(ConstantIF.SS_SVC_NO);    
	                custId[i]       = (String)model.get("vrCustId");
	                ritemId[i]      = (String)model.get("RITEM_ID"+i); 
	                uomId[i]      	= (String)model.get("UOM_ID"+i); 
	                subLotId[i]     = (String)model.get("SUB_LOT_ID"+i); 
	                workQty[i]      = (String)model.get("STOCK_QTY"+i); 
	                ordDesc[i]      = (String)model.get("ORD_DESC"+i); 
	            }
	          
	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();
	
	            modelIns.put("I_LC_ID"        	, lcId);
	            modelIns.put("I_CUST_ID"    	, custId);                
	            modelIns.put("I_RITEM_ID"     	, ritemId);  
	            modelIns.put("I_UOM_ID"			, uomId);
	            modelIns.put("I_SUB_LOT_ID"     , subLotId);  
	            modelIns.put("I_WORK_QTY"		, workQty);
	            modelIns.put("I_ORD_DESC"		, ordDesc);
	            
	            modelIns.put("WORK_IP"  		, (String)model.get(ConstantIF.SS_CLIENT_IP));  
	            modelIns.put("USER_NO"  		, (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.saveEndDateOut(modelIns);
            ServiceUtil.isValidReturnCode("WMSTG050", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", 0);
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
	}  
}
