package com.logisall.winus.wmstg.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.logisall.winus.wmstg.service.WMSTG070Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG070Service")
public class WMSTG070ServiceImpl implements WMSTG070Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG070Dao")
    private WMSTG070Dao dao;
    
    @Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
    
    
    private final static String[] CHECK_VALIDATE_WMSTG070 = {"STOCK_ID", "LOCK_YN"};
    private final static String[] CHECK_VALIDATE_WMSTG070_DEL_LOTNO = {"CUST_ID", "SUB_LOT_ID","CUST_LOT_NO"};
    
    /**
     * 
     * Method ID   : selectItemGrp
     * Method �ㅻ�    : ��怨�愿�由� ��硫댁���� ������ �곗�댄��
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : LOT별재고조회 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : LOT별재고조회 목록 조회(kcc홈씨씨)
     * 작성자                      : ykim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_kcc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        map.put("LIST", dao.list_kcc(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : LOT재고 목록 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }  
    
    /**
     * 
     * 대체 Method ID   : shippingTemplate
     * 대체 Method 설명    : LOT별재고현황 출고처리
     * 작성자                      : Summer Hyun
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> shippingTemplate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int listSize = Integer.parseInt(model.get("selectIds").toString());
        
        
        try{
        	//배열 선언 
        	if(listSize > 0){
    			String[] no             = new String[listSize];     
    			String[] stockId             = new String[listSize];     
    			
    			String[] outReqDt       = new String[listSize];         
    			String[] inReqDt        = new String[listSize];     
    			String[] custOrdNo      = new String[listSize];     
    			String[] custOrdSeq     = new String[listSize];    
    			String[] trustCustCd    = new String[listSize];     
    			
    			String[] transCustCd    = new String[listSize];                     
    			String[] transCustTel   = new String[listSize];         
    			String[] transReqDt     = new String[listSize];     
    			String[] custCd         = new String[listSize];     //
    			String[] ordQty         = new String[listSize];     
    			
    			String[] uomCd          = new String[listSize];                
    			String[] sdeptCd        = new String[listSize];         
    			String[] salePerCd      = new String[listSize];     
    			String[] carCd          = new String[listSize];     
    			String[] drvNm          = new String[listSize];     
    			
    			String[] dlvSeq         = new String[listSize];                
    			String[] drvTel         = new String[listSize];         
    			String[] custLotNo      = new String[listSize];     //
    			String[] blNo           = new String[listSize];     
    			String[] recDt          = new String[listSize];     
    			
    			String[] outWhCd        = new String[listSize];                
    			String[] inWhCd         = new String[listSize];         
    			String[] makeDt         = new String[listSize];     
    			String[] timePeriodDay  = new String[listSize];     
    			String[] workYn         = new String[listSize];     
    			
    			String[] rjType         = new String[listSize];                
    			String[] locYn          = new String[listSize];         
    			String[] confYn         = new String[listSize];     
    			String[] eaCapa         = new String[listSize];     
    			String[] inOrdWeight    = new String[listSize];     
    			
    			String[] itemCd         = new String[listSize];                
    			String[] itemNm         = new String[listSize];         
    			String[] transCustNm    = new String[listSize];     
    			String[] transCustAddr  = new String[listSize];     
    			String[] transEmpNm     = new String[listSize];     
    			
    			String[] remark         = new String[listSize];                
    			String[] transZipNo     = new String[listSize];         
    			String[] etc2           = new String[listSize];     
    			String[] unitAmt        = new String[listSize];     
    			String[] transBizNo     = new String[listSize];     
    			
    			String[] inCustAddr     = new String[listSize];                
    			String[] inCustCd       = new String[listSize];         
    			String[] inCustNm       = new String[listSize];     
    			String[] inCustTel      = new String[listSize];     
    			String[] inCustEmpNm    = new String[listSize];     
    			
    			String[] expiryDate     = new String[listSize];
    			String[] salesCustNm    = new String[listSize];
    			String[] zip     		= new String[listSize];
    			String[] addr     		= new String[listSize];
    			String[] phone1    	 	= new String[listSize];
    			
    			String[] etc1    		= new String[listSize];
    			String[] unitNo    		= new String[listSize];               
    			String[] salesCompanyNm	 = new String[listSize];
    			String[] timeDate       = new String[listSize];   //상품유효기간     
    			String[] timeDateEnd    = new String[listSize];   //상품유효기간만료일
    			
    			String[] timeUseEnd     = new String[listSize];   //소비가한만료일
    			String[] locCd     		= new String[listSize];   //로케이션코드
    			
    			String[] epType     	= new String[listSize];   //납품유형
				String[] lotType     	= new String[listSize];   //LOT속성 	2022-03-04
				String[] ownerCd     	= new String[listSize];   //소유자		2022-03-04
				
				
				String[] custLegacyItemCd = new String[listSize]; //화주 상품코드	2022-05-24
				String[] itemBarcode    = new String[listSize];   //상품 바코드		2022-05-24
				String[] itemSize     	= new String[listSize];   //상품 사이즈		2022-05-24
				String[] color     		= new String[listSize];   //상품 색상		2022-05-24
        	
	        	//전달 받은 값 배열에 저장 
	            for(int i = 0 ; i < listSize; i ++){
	                Map<String, Object> modelDt = new HashMap<String, Object>();
	                
	                stockId[i]		 = (String) model.get("STOCK_ID"+i);
	                custCd[i]  		 = (String) model.get("CUST_CD"+i); 		//CUST_ID
	                outWhCd[i] 		 = (String) model.get("WH_CD"+i);  			//WH_  
	                itemCd[i]  		 = (String) model.get("ITEM_CD"+i);  		//ITEM_CD
	                itemBarcode[i]   = (String) model.get("ITEM_BAR_CD"+i); 	//ITEM_BAR_CD 
	                ordQty[i]  	 	 = (String) model.get("STOCK_QTY"+i);  		//STOCK_QTY ordQty
	              //  locCd[i]  	 	 = (String) model.get("LOC_CD"+i);  		//LOC_CD
	                custLotNo[i]  	 	 = (String) model.get("CUST_LOT_NO"+i);  	//CUST_LOT_NO
	                stockId[i]  	 	 = (String) model.get("STOCK_ID"+i);  		//CUST_LOT_NO
	                
	                
	            }
	            
	            //프로시저에 보낼 것
	            Map<String, Object> modelIns = new HashMap<String, Object>();
	            
	            modelIns.put("stockId", "stockId");
	            modelIns.put("vrOrdType", "02");
	            
	            modelIns.put("no"  , no);
			
	            modelIns.put("reqDt"     	, outReqDt);
				modelIns.put("whCd"      	, outWhCd);
				
				
				modelIns.put("custOrdNo"    	, custOrdNo);
				modelIns.put("custOrdSeq"   	, custOrdSeq);
				modelIns.put("trustCustCd"  	, trustCustCd); //5
				
				modelIns.put("transCustCd"  	, transCustCd);
				modelIns.put("transCustTel" 	, transCustTel);
				modelIns.put("transReqDt"   	, transReqDt);
				modelIns.put("custCd"       	, custCd);
				modelIns.put("ordQty"       	, ordQty);      //10
				
				modelIns.put("uomCd"        	, uomCd);
				modelIns.put("sdeptCd"      	, sdeptCd);
				modelIns.put("salePerCd"    	, salePerCd);
				modelIns.put("carCd"        	, carCd);
				modelIns.put("drvNm"        	, drvNm);       //15
				
				modelIns.put("dlvSeq"       	, dlvSeq);
				modelIns.put("drvTel"       	, drvTel);
				modelIns.put("custLotNo"    	, custLotNo);
				modelIns.put("blNo"         	, blNo);
				modelIns.put("recDt"        	, recDt);       //20
				
				modelIns.put("makeDt"       	, makeDt);
				modelIns.put("timePeriodDay"	, timePeriodDay);
				modelIns.put("workYn"       	, workYn);                
				modelIns.put("rjType"       	, rjType);
				modelIns.put("locYn"        	, locYn);       //25
				
				modelIns.put("confYn"       	, confYn);     
				modelIns.put("eaCapa"       	, eaCapa);
				modelIns.put("inOrdWeight"  	, inOrdWeight); //28
				modelIns.put("itemCd"           , itemCd);
				modelIns.put("itemNm"           , itemNm);
				
				modelIns.put("transCustNm"      , transCustNm);
				modelIns.put("transCustAddr"    , transCustAddr);
				modelIns.put("transEmpNm"       , transEmpNm);
				modelIns.put("remark"           , remark);
				modelIns.put("transZipNo"       , transZipNo);
				
				modelIns.put("etc2"             , etc2);
				modelIns.put("unitAmt"          , unitAmt);
				modelIns.put("transBizNo"       , transBizNo);
				modelIns.put("inCustAddr"       , inCustAddr);
				modelIns.put("inCustCd"         , inCustCd);
				
				modelIns.put("inCustNm"         , inCustNm);                 
				modelIns.put("inCustTel"        , inCustTel);
				modelIns.put("inCustEmpNm"      , inCustEmpNm);      
				modelIns.put("expiryDate"       , expiryDate);
				modelIns.put("salesCustNm"      , salesCustNm);
				
				modelIns.put("zip"       		, zip);
				modelIns.put("addr"       		, addr);
				modelIns.put("phone1"       	, phone1);
				modelIns.put("etc1"     	 	, etc1);
				modelIns.put("unitNo"     	 	, unitNo);
				
				modelIns.put("salesCompanyNm"    	, salesCompanyNm);
				
				modelIns.put("time_date"        , timeDate);
				modelIns.put("time_date_end"    , timeDateEnd);                
				modelIns.put("time_use_end"     , timeUseEnd);  
				modelIns.put("locCd"     		, locCd);  
				modelIns.put("epType"     		, epType);  
				
				modelIns.put("lotType"     		, lotType);  
				modelIns.put("ownerCd"     		, ownerCd);
				
				modelIns.put("custLegacyItemCd" , custLegacyItemCd);  
				modelIns.put("itemBarcode"     	, itemBarcode);  
				modelIns.put("itemSize"     	, itemSize);  
				modelIns.put("color"     		, color);  
				
				modelIns.put("vrOrdSubType"    	, "30");   
				modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
				modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
				modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
				
	            
	            //프로시저 호출 dao
	            modelIns = (Map<String, Object>)dao030.saveExcelOrder_AS(modelIns);
				ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
				m.put("O_CUR", modelIns.get("O_CUR"));  
            
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : LOT별재고현황 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 	, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  	, model.get("ST_GUBUN"+i));

                modelDt.put("STOCK_ID"      , model.get("STOCK_ID"+i));
                modelDt.put("CUST_LOT_NO"  	, model.get("CUST_LOT_NO"+i));
                modelDt.put("LOCK_YN"     	, model.get("LOCK_YN"+i));
                modelDt.put("OWNER_CD"      , model.get("OWNER_CD"+i));
                
                modelDt.put("LC_ID"    		, model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("WORK_IP"		, model.get(ConstantIF.SS_CLIENT_IP));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSTG070);
                
                // 0000003620 == 비즈컨설팅 덕평물류센터 0000001262 == pr_whare
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    //dao.insert(modelDt);
                	 dao.update(modelDt);
                	 if(model.get(ConstantIF.SS_SVC_NO).equals("0000003620") || model.get(ConstantIF.SS_SVC_NO).equals("0000001262")){
                		 dao.update_ownerCd(modelDt);
                	 }
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    if(model.get(ConstantIF.SS_SVC_NO).equals("0000003620")|| model.get(ConstantIF.SS_SVC_NO).equals("0000001262")){
               		 dao.update_ownerCd(modelDt);
                    }
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID      : delLotNo
     * 대체 Method 설명    : LOT별재고현황 -> Lot 번호 삭제
     * 작성자              : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> delLotNo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 	, model.get("selectIds"));

                modelDt.put("CUST_ID"  			, model.get("CUST_ID"+i));
                modelDt.put("SUB_LOT_ID"      	, model.get("SUB_LOT_ID"+i));
                modelDt.put("CUST_LOT_NO"     	, model.get("CUST_LOT_NO"+i));
                
                modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("WORK_IP"		, model.get(ConstantIF.SS_CLIENT_IP));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSTG070_DEL_LOTNO);
                
                dao.delLotNo(modelDt);
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * 대체 Method ID      : decomposeLotNo
     * 대체 Method 설명    : LOT별재고현황 -> Lot 번호 box -> pcs 분해 (비즈컨설팅) 
     * 작성자              : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> decomposeLotNo(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	
        	StringBuilder stb = new StringBuilder();
        	
        	for(int i = 0; i < list.size(); ++i){
        		 Map<String, Object> modelDt = new HashMap<String, Object>();
                 modelDt.put("RITEM_CD"  	, (String)((Map<String, String>)list.get(i)).get("ITEM_CD"));
                 modelDt.put("CUST_LOT_NO"  , (String)((Map<String, String>)list.get(i)).get("LOT_NO"));
                 modelDt.put("OWNER_CD"  	, (String)((Map<String, String>)list.get(i)).get("OWNER_CD"));
                 modelDt.put("LC_ID"  		, model.get("SS_SVC_NO"));
                 modelDt.put("CUST_ID"  	, "0000295975");
                 
                 int cnt = (int) dao.getLotNo(modelDt);
                 
                 if(cnt == 0){
                	 stb.append("[").append(i+1).append("행]").append( "박스번호 : ").append(modelDt.get("CUST_LOT_NO")).append(" 상품코드 : ").append(modelDt.get("RITEM_CD")).append("\n");
                	 errCnt = -1;
                 }
        	}
        	
        	if(errCnt < 0){
        		stb.append("해당 박스번호, 상품이 조회되지 않습니다.");
        		
                m.put("errCnt", errCnt);
                m.put("MSG", stb.toString());
        	}else {
            	for(int i = 0; i < list.size(); ++i){
            		 Map<String, Object> modelDt = new HashMap<String, Object>();
            		    modelDt.put("RITEM_CD"  	, (String)((Map<String, String>)list.get(i)).get("ITEM_CD"));
                        modelDt.put("CUST_LOT_NO"  	, (String)((Map<String, String>)list.get(i)).get("LOT_NO"));
                        modelDt.put("OWNER_CD"  	, (String)((Map<String, String>)list.get(i)).get("OWNER_CD"));
                        modelDt.put("LC_ID"  		, model.get("SS_SVC_NO"));
                        modelDt.put("CUST_ID"  		, "0000295975");
                     
                    dao.decomposeCustLotNo(modelDt);
                    
                    m.put("errCnt", errCnt);
                    m.put("MSG", MessageResolver.getMessage("save.success"));
            	}
        	}
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   		: listSummaryCount
     * Method 설명      : LOT별 재고현황 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listSummaryCount(model));
        return map;
    }
    
}
