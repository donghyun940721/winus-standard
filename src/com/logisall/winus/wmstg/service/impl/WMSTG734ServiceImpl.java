package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;





import com.logisall.winus.wmsst.service.WMSST065Service;
import com.logisall.winus.wmstg.service.WMSTG732Service;
import com.logisall.winus.wmstg.service.WMSTG733Service;
import com.logisall.winus.wmstg.service.WMSTG734Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG734Service")
public class WMSTG734ServiceImpl extends AbstractServiceImpl implements WMSTG734Service {
    
    @Resource(name = "WMSTG734Dao")
    private WMSTG734Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 입별재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
             
            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : list2
     * Method 설명 : 일별재고-> 주문 단가 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {

    		map.put("LIST", dao.list2(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }
    /**
     * Method ID : listT2
     * Method 설명 : DPS 보충리스트 조회
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listT2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {

    		map.put("LIST", dao.listT2(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }
}

