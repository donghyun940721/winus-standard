package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmstg.service.WMSTG080Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG080Service")
public class WMSTG080ServiceImpl extends AbstractServiceImpl implements WMSTG080Service{
	 
    @Resource(name = "WMSTG080Dao")
    private WMSTG080Dao dao;

    /**
     * Method ID : prevList
     * Method 설명 : 유통기한별 재고 조회 지정기간(올푸드)
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> prevList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.prevList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : curList
     * Method 설명 : 유통기한별 재고 조회 지정기간 + 현재고 + 기발주(올푸드)
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> curList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.curList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : prevListExcel
     * Method 설명 : 유통기한별 재고 조회 지정기간 (올푸드) Excel down
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     * @throws Exception
     */
	public Map<String, Object> prevListExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.prevList(model));

		return map;
	}
	
	/**
     * Method ID : curListExcel
     * Method 설명 : 유통기한별 재고 조회 지정기간 + 현재고 + 기발주(올푸드) Excel down
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     * @throws Exception
     */
	public Map<String, Object> curListExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.curList(model));

		return map;
	}
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

}
