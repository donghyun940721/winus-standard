package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG310Dao")
public class WMSTG310Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : listE1
     * Method 설명 : 날짜별 임가공 실적 조회
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
        return executeQueryPageWq("wmstg310.listE1", model);
    }
    
    /**
     * Method ID : listE2
     * Method 설명 : 날짜별 임가공 조회
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmstg310.listE2", model);
    }
}
