package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmstg.service.WMSTG736Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG736Service")
public class WMSTG736ServiceImpl implements WMSTG736Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSTG736Dao")
    private WMSTG736Dao dao;

    /**
     * Method ID		 : listQ1
     * Method 설명 	 : 국가 조회
     * 작성자 			 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }         
            map.put("LIST", dao.listQ1(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }

        return map;
    }
	
    /**
     * Method ID : list
     * Method 설명 : 올리브영 배송지연 리드 타임 조회 리스트
     * 작성자 : KSJ
     * @param model
     * @return
     */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
    
    /**
     * Method ID : listSummary
     * Method 설명 : 올리브영 배송지연 리드타임 요약 (국가별, 등급별)
     * 작성자 : KSJ
     * @param model
     * @return
     */
	@Override
	public Map<String, Object> listSummary(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listSummary(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
}