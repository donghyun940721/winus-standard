package com.logisall.winus.wmstg.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG071Dao")
public class WMSTG071Dao extends SqlMapAbstractDAO{
    
    /**
     * Method ID    		 : selectItemGrp
     * Method 설명      	 : 센터별 LOT 상품 조회 -> select 리스트
     * 작성자                 : KSJ
     * @param   model
     * @return
     */
    public List<Map<String, Object>> colInfoList(Map<String, Object> model){
		return (List<Map<String, Object>>) executeQueryForList("wmstg071.colInfoList", model);
    }
    
    
    /**
     * Method ID   		 : list
     * Method 설명       : 센터별 LOT 상품 조회
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmstg071.list", model);
    }
    
    /**
     * Method ID   		 : excelList
     * Method 설명       : 센터별 LOT 상품 조회 excel
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public GenericResultSet excelList(Map<String, Object> model) {
        return executeQueryPageWq("wmstg071.excelList", model);
    }
    
}
