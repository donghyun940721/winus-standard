package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG050Dao")
public class WMSTG050Dao extends SqlMapAbstractDAO{

    /**
     * Method ID    : list
     * Method 설명      : 유효기간경고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst010.dayItemList", model);
    }
    /**
     * Method ID    : list2
     * Method 설명      : 유효기간경고 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst010.dayItemList2", model);
    }
    /**
     * Method ID    : listIn
     * Method 설명      : 유효기간 대시보드 경과리스트
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public GenericResultSet listIn(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.dayItemListIn", model);
    }
    
    /**
     * Method ID    : listOut
     * Method 설명      : 유효기간 대시보드 초과리스트
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public GenericResultSet listOut(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.dayItemListOut", model);
    }
    
    /**
     * Method ID    : listsCount
     * Method 설명      : 유효기간 대시보드 차트리스트
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public GenericResultSet listsCount(Map<String, Object> model) {
    	return executeQueryWq("wmsst010.dayItemListsCount", model);
    }
    
    /**
     * Method ID    : saveEndDateOut
     * Method 설명      : 유효기간 초과 상품 등록
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object saveEndDateOut(Map<String, Object> model){
        executeUpdate("wmsop031.pk_wmsop031.sp_ord_ins_sublot_loc", model);
        return model;
    }
    
}
