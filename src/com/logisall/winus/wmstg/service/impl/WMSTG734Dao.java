package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG734Dao")
public class WMSTG734Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 출고박스조회
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmstg734.list", model);
    }   
    /**
     * Method ID : listE2
     * Method 설명 : 출고 시리얼 조회
     * 작성자 : 
     * @param model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
    	return executeQueryWq("wmstg734.list2", model);
    }   
    
    /**
     * Method ID : listT2
     * Method 설명 : DPS 보충리스트 조회
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
    public GenericResultSet listT2(Map<String, Object> model) {
    	return executeQueryWq("wmstg734.listT2", model);
    }   

}

