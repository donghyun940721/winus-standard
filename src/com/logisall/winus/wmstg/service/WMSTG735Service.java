package com.logisall.winus.wmstg.service;

import java.util.Map;
import java.util.List;

public interface WMSTG735Service {
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE01(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE04(Map<String, Object> model) throws Exception;

    public Map<String, Object> listExcelE04(Map<String, Object> model) throws Exception;
    
    
}
