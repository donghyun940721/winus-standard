package com.logisall.winus.wmstg.service;

import java.util.Map;

public interface WMSTG080Service {
	public Map<String, Object> prevList(Map<String, Object> model) throws Exception;
	public Map<String, Object> curList(Map<String, Object> model) throws Exception;
	public Map<String, Object> prevListExcel(Map<String, Object> model) throws Exception;
	public Map<String, Object> curListExcel(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
