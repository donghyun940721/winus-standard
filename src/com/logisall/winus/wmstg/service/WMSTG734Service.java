package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG734Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT2(Map<String, Object> model) throws Exception;

}
