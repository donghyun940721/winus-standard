package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG041Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelBiz(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;;
}
