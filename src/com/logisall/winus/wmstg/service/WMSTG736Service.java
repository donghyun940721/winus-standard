package com.logisall.winus.wmstg.service;

import java.util.Map;
import java.util.List;

public interface WMSTG736Service {
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSummary(Map<String, Object> model) throws Exception;
}
