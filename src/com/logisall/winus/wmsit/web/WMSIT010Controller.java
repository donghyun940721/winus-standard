package com.logisall.winus.wmsit.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsit.service.WMSIT010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT010Service")
	private WMSIT010Service service;
	
	/*-
	 * Method ID : mn
	 * Method 설명 : B2B검수 화면
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT010.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsit/WMSIT010", service.selectItemGrp(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : ordRitemList
	 * Method 설명 : B2B검수 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT010/ordRitemList.action")
	public ModelAndView ordRitemList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordRitemList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID 	: getBoxNum
	 * Method 설명  : B2B검수 box no 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT010/getBoxNum.action")
	public ModelAndView getBoxNum(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getBoxNum(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : chkConfirmB2B
	 * Method 설명 : B2B검수
	 * 작성자 : sing09
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIT010/chkConfirmB2B.action")
	public ModelAndView chkConfirmB2B(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.chkConfirmB2B(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : ordChkConfirm
	 * Method 설명      : B2B 검수 리스트 조회 (우측 그리드)
	 * 작성자                 : 이성중
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSIT010/ordChkConfirm.action")
	public ModelAndView ordChkConfirm(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordChkConfirm(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID : doExcelDown
     * Method 설명 : box_no별 엑셀 조회 및 출력
     * 작성자 : ysi
     *
     * @param response
     * @param grs
     */
  
	@RequestMapping("/WMSIT010/selectBoxNumList.action")
	public void selectBoxNumList(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.selectBoxNumList(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				if(model.get("SS_SVC_NO").equals("0000003620")){ // 비즈컨설팅 덕평일 경우
					this.doExcelDownBiz(response, grs);
				}else{
					this.doExcelDown(response, grs);
				}
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}
	
	/*-
     * Method ID : doExcelDown
     * Method 설명 : box_no별 엑셀 조회 및 출력
     * 작성자 : YSI
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("작업일자")      , "0", "0", "0", "0", "150"},
                                   {MessageResolver.getText("박스번호")      , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("검수순번")         , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")         , "3", "3", "0", "0", "150"},
                                   {MessageResolver.getText("상품명")      , "4", "4", "0", "0", "150"},
                                   {MessageResolver.getText("주문수량")  	   , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("검수량")       , "6", "6", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"         , "S"},
                                    {"CHK_BOX_NO"    , "S"},
                                    {"CHK_SEQ"          , "S"},
                                    {"ITEM_CD"        , "S"},
                                    {"ITEM_NM"       , "S"},
                                    {"ORD_QTY"     , "N"},
                                    {"CHK_QTY"       , "N"}
                                   }; 
            //파일명
            String fileName = MessageResolver.getText("박스넘버별조회");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : box_no별 엑셀 조회 및 출력
     * 작성자 : YSI
     *
     * @param response
     * @param grs
     */
    protected void doExcelDownBiz(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("작업일자")      , "0", "0", "0", "0", "150"},
                                   {MessageResolver.getText("박스번호")      , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("검수순번")      , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")      , "3", "3", "0", "0", "150"},
                                   {MessageResolver.getText("스타일")      	 , "4", "4", "0", "0", "150"},
                                   {MessageResolver.getText("컬러")      	 , "5", "5", "0", "0", "150"},
                                   {MessageResolver.getText("사이즈")      	 , "6", "6", "0", "0", "150"},
                                   {MessageResolver.getText("주문수량")  	 , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("검수량")        , "8", "8", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"          , "S"},
                                    {"CHK_BOX_NO"    	, "S"},
                                    {"CHK_SEQ"          , "S"},
                                    {"ITEM_CD"        	, "S"},
                                    {"CUST_LEGACY_ITEM_CD"       , "S"},
                                    {"COLOR"       		, "S"},
                                    {"ITEM_SIZE"        , "S"},
                                    {"ORD_QTY"     		, "N"},
                                    {"CHK_QTY"       	, "N"}
                                   }; 
            //파일명
            String fileName = MessageResolver.getText("박스넘버별조회");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
}