package com.logisall.winus.wmsit.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.logisall.winus.wmsit.service.WMSIT051Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT051Controller {
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSIT051Service")
    private WMSIT051Service service;
     
    /**
     * Method ID : WMSIT051
     * Method 설명 : 입고검수(주문생성)
     * 작성자 : 
     * @param model
     * @return
    */
    @RequestMapping("/WINUS/WMSIT051.action")
    public ModelAndView WMSIT051(Map<String, Object> model){
        return new ModelAndView("winus/wmsit/WMSIT051");
    }
    	
    /**
     * Method ID : getItemList
     * Method 설명 : 입고검수(주문생성)
     * 작성자 : 
     * @param model
     * @return
    */
    @RequestMapping("/WMSIT051/getItemInfo.action")
    public ModelAndView getItemInfo(Map<String, Object> model){
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.getItemInfo(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    
    /**
     * Method ID : checkInComplete
     * Method 설명 : 입고검수확정(주문생성)
     * 작성자 : 
     * @param model
     * @return
    */
    @RequestMapping(value="/WMSIT051/checkInComplete.action" , method = RequestMethod.POST)
    public ModelAndView checkInComplete(Map<String,Object> model,@RequestBody HashMap<String, Object> model2){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            model.putAll(model2);
            m = service.checkInComplete(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("errCnt", -1);
            m.put("MSG",e.getMessage());
        }
        mav.addAllObjects(m);
        return mav;
    }

}