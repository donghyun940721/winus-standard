package com.logisall.winus.wmsit.web;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;



import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT100Service;
import com.logisall.winus.wmsit.service.WMSIT110Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSIT110Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT110Service")
	private WMSIT110Service service;
	
    @Resource(name = "WMSIT100Service")
    private WMSIT100Service service100;
	
	/*-
	 * Method ID : wmsit110
	 * Method 설명 : 영상피킹검수
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT110.action")
	public ModelAndView wmsit110(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
		    mav = new ModelAndView("winus/wmsit/WMSIT110");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID : wmsit120
     * Method 설명 : 송장패킹검수
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSIT120.action")
    public ModelAndView wmsit120(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("winus/wmsit/WMSIT120");
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get info :", e);
            }
        }
        return mav;
    }
    
    /*-
     * Method ID : wmsit121
     * Method 설명 : 패킹검수(이디야)
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSIT121.action")
    public ModelAndView wmsit121(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("winus/wmsit/WMSIT121");
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get info :", e);
            }
        }
        return mav;
    }
	
	/**
     * Method ID : orderItemList
     * Method 설명 : 검수 품목 주문조회
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WMSIT110/orderItemList.action")
    public ModelAndView orderItemList(Map<String, Object> model) {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.orderItemList(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    
    /**
     * Method ID : packingItemList
     * Method 설명 : 패킹 품목 조회
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WMSIT110/packingItemList.action")
    public ModelAndView packingItemList(Map<String, Object> model) {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.packingItemList(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    
    /**
     * Method ID : packingItemListEdiya
     * Method 설명 : 패킹 품목 조회(이디야)
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WMSIT110/packingItemListEdiya.action")
    public ModelAndView packingItemListEdiya(Map<String, Object> model) {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.packingItemListEdiya(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    
    /*-
     * Method ID    : workPickingComp
     * Method 설명      : B2B 검수 리스트 조회 (우측 그리드)
     * 작성자                 : 이성중
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIT110/workPickingComp.action")
    public ModelAndView workPickingComp(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");

            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");
            
            String date = (new SimpleDateFormat("yyyyMMdd")).format(System.currentTimeMillis());
            
            String fileName = file.getOriginalFilename();

            String filePaths = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\RealPacking\\"+(String)model.get(ConstantIF.SS_SVC_NO)+"\\"+(String)model.get("CUST_ID")+"\\"+date;
            //String hostUrl = request.getServerName();
            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);
            }
            
            //videoID 파일이름에 추가
            Map<String,Object> modelID = service.getVideoID(model);
            String id = (String)modelID.get("VIDEO_ID");
            fileName = "RealPacking"+id+"_"+fileName;
            
            File destinationFile = new File(filePaths, fileName);
            
            //destinationFile.getTotalSpace();
            
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));
            
            model.put("FILE_NAME",fileName);
            model.put("VIDEO_ID",id);
            model.put("URL",filePaths);
            service.saveVideo(model);
            
            m = service100.pickingComplete(model);
            
            if(!m.get("header").toString().equals("Y")){
                m.put("MSG", "오류가 발생하였습니다.(1) : " + m.get("message"));
                mav.addAllObjects(m);
                return mav;
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }   
    /*-
     * Method ID    : packingComp
     * Method 설명      : 송장패킹검수 완료 
     * 작성자                 : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIT110/packingComplete.action")
    public ModelAndView packingComplete(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");

            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");
            
            String date = (new SimpleDateFormat("yyyyMMdd")).format(System.currentTimeMillis());
            
            String fileName = file.getOriginalFilename();

            String filePaths = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\RealPacking\\"+(String)model.get(ConstantIF.SS_SVC_NO)+"\\"+(String)model.get("CUST_ID")+"\\"+date;
            //String hostUrl = request.getServerName();
            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);
            }
            
            //videoID 파일이름에 추가
            Map<String,Object> modelID = service.getVideoID(model);
            String id = (String)modelID.get("VIDEO_ID");
            fileName = "RealPacking"+id+"_"+fileName;
            
            File destinationFile = new File(filePaths, fileName);
            
            //destinationFile.getTotalSpace();
            
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));
            
            model.put("FILE_NAME",fileName);
            model.put("VIDEO_ID",id);
            model.put("URL",filePaths);
            service.saveVideo(model);
            
            m = service.packingComplete(model);
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
            m.put("CODE", "-1");
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID    : packingCompleteEdiya
     * Method 설명      : 패킹검수 완료(이디야) 
     * 작성자                 : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIT110/packingCompleteEdiya.action")
    public ModelAndView packingCompleteEdiya(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");

            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");
            
            String date = (new SimpleDateFormat("yyyyMMdd")).format(System.currentTimeMillis());
            
            String fileName = file.getOriginalFilename();

            String filePaths = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\RealPacking\\"+(String)model.get(ConstantIF.SS_SVC_NO)+"\\"+(String)model.get("CUST_ID")+"\\"+date;
            //String hostUrl = request.getServerName();
            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);
            }
            
            //videoID 파일이름에 추가
            Map<String,Object> modelID = service.getVideoID(model);
            String id = (String)modelID.get("VIDEO_ID");
            fileName = "RealPacking"+id+"_"+fileName;
            
            File destinationFile = new File(filePaths, fileName);
            
            //destinationFile.getTotalSpace();
            
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));
            
            model.put("FILE_NAME",fileName);
            model.put("VIDEO_ID",id);
            model.put("URL",filePaths); 
            
            int idx = Integer.parseInt((String)model.get("selectIds"));
            
            if(idx > 0){
                
                model.put("selectIds", "1");
                model.put("ORD_ID0", (String)model.get("ORD_ID"));
                model.put("ORD_SEQ0", "");
                model.put("INVC_NO", (String)model.get("FA_ISSUE_LABEL"));
                
                service.saveVideo(model);
                
                m = service.packingCompleteEdiya(model);
            }
            
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID    : getVideoPop
     * Method 설명      : 비디오 조회 팝업
     * 작성자                 : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIT110/getVideoPop.action")
    public ModelAndView getVideoPop(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsit/WMSIT110pop");
    }
    
    /*-
     * Method ID    : getVideoData
     * Method 설명      : 비디오 조회
     * 작성자                 : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIT110/getVideoData.action")
    public void getVideoData(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        
        try {
            
            Map<String,String[]> param = request.getParameterMap();
            
            if(param == null){
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            
            model.put("LC_ID",(String)request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
            model.put("ordId",param.get("ordId")[0]);
            if( param.get("ordSeq") != null ){
            	model.put("ordSeq",param.get("ordSeq")[0]);
            }
            if( param.get("boxNo") != null ){
            	model.put("boxNo",param.get("boxNo")[0]);
            }
            if( param.get("invcNo") != null ){
            	model.put("invcNo",param.get("invcNo")[0]);
            }
            if( param.get("videoId") != null ){
                model.put("videoId",param.get("videoId")[0]);
            }
            
            m = service.getVideoData(model);
            
            if(m == null){
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            
            String Url = (String)m.get("URL");
            String fileName = (String)m.get("FILE_NAME");
            
            File file = new File(Url, fileName);
            
            if (!file.isFile()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            
            try(FileInputStream fin = new FileInputStream(file);
                    BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream())){
                
                int fSize = (int)file.length();
                
                response.setContentType("video/mp4");
                response.setContentLength(fSize);
                response.setHeader("Accept-Ranges", "bytes");
                response.setHeader("Content-Disposition", "attachment; filename="+fileName+";");
                
                byte[] bytes = new byte[4096];
                
                while(fin.read(bytes) >= 0){
                    bos.write(bytes);
                }
                                
            }catch(Exception e){
            	System.out.println("Client connection reset: " +e.getMessage());
            }
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
        }
    }

    @RequestMapping("/WMSIT110/getPackingHistoryEdiya.action")
    public ResponseEntity<Map<String, Object>> getPackingHistoryEdiya(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	resultMap = service.getPackingHistory(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to list :", e);
            }
        }
        return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
    }
    
    @RequestMapping("/WMSIT110/getEdiyaPackingHistoryResult")
    public ModelAndView getEdiyaPackingHistoryResult (Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.getEdiyaPackingHistoryResult(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIT110/getOrdIdByOrderNoEdiya.action")
    public ResponseEntity<Map<String, Object>> getOrdIdByOrderNoEdiya (Map<String, Object> model) throws Exception {
    	 Map<String, Object> resultMap = new HashMap<String, Object>();
         try {
         	resultMap = service.getOrdIdByOrderNoEdiya(model);
         } catch (Exception e) {
             if (log.isErrorEnabled()) {
                 log.error("Fail to list :", e);
             }
         }
         return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
    }
}