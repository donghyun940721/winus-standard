package com.logisall.winus.wmsit.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT020Service;
import com.logisall.winus.wmsit.service.WMSIT050Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT050Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT050Service")
	private WMSIT050Service service;
	
	/*-
	 * Method ID : WMSIT050
	 * Method 설명 : 반품입고검수 접근
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	 @RequestMapping("/WINUS/WMSIT050.action")
	    public ModelAndView WMSIT050(Map<String, Object> model){
	        return new ModelAndView("winus/wmsit/WMSIT050");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
	 }
	 
	 /*-
	 * Method ID : WMSIT050.ordSelectbyInvcNo
	 * Method 설명 : 반품송장번호를 통한 주문내역 확인
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */ 
	@RequestMapping("/WMSIT050/ordSelectbyInvcNo.action")
	public ModelAndView ordRitemList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordSelectbyInvcNo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : WMSIT050.getOrdId
	 * Method 설명 : 반품송장을 통한 주문내역 가져오기 
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */ 
	@RequestMapping("/WMSIT050/getOrdId.action")
	public ModelAndView getOrdId(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getOrdId(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	 /*-
		 * Method ID : WMSIT050.getBoxNum
		 * Method 설명 : 반품입고 검수 박스 넘버 가져오기 
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT050/getBoxNum.action")
		public ModelAndView getBoxNum(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jsonView", service.getBoxNum(model));
				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}
		

		/*-
		 * Method ID : inOrdChkConfirm
		 * Method 설명 : 입고검수완료
		 * 작성자 : 
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSIT050/inOrdChkConfirm.action")
		public ModelAndView chkConfirmB2B(Map<String, Object> model) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			try {
				System.out.println("model ====> "+model);
				m = service.inOrdChkConfirm(model);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save :", e);
				}
				m = new HashMap<String, Object>();
				System.out.println(m);
				m.put("MSG", m.get("MSG"));
			}
			mav.addAllObjects(m);
			return mav;
		}		
		
		 /*-
		 * Method ID : WMSIT050.getBoxInfo
		 * Method 설명 : 반품송장번호와 박스번호를 통한 검수내역 확인
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT050/getBoxInfo.action")
		public ModelAndView getBoxInfo(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jqGridJsonView", service.getBoxInfo(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}
		
		 /*-
		 * Method ID : WMSIT050.updateCkDel
		 * Method 설명 : 입고검수내역 삭제 CK030.DEL_YN = 'Y'
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT050/updateCkDel.action")
		public ModelAndView updateCkDel(Map<String, Object> model) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			try {
				m = service.updateCkDel(model);
				m.put("MSG", "박스 검수 내역 삭제 성공");
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to Delete Check Box", e);
				}
				m = new HashMap<String, Object>();
				m.put("MSG", "박스 검수 내역 삭제 실패 - "+e.getMessage());
				m.put("errCnt", "1");
			}
			mav.addAllObjects(m);
			return mav;
		}
		
		/*-
		 * Method ID : inOrdChkConfirm
		 * Method 설명 : 반품입고 적치 완료
		 * 작성자 : 
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSIT050/completeChkOrder.action")
		public ModelAndView completeChkOrder(Map<String, Object> model) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			try {
				System.out.println("model ====> "+model);
				m = service.completeChkOrder(model);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save :", e);
				}
				m = new HashMap<String, Object>();
				System.out.println(m);
				m.put("MSG", m.get("MSG"));
			}
			mav.addAllObjects(m);
			return mav;
		}	
		
		 /*-
		 * Method ID : WMSIT050.chkItemCd
		 * Method 설명 : 아이템 정보 확인
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT050/chkItemCd.action")
		public ModelAndView chkItemCd(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jsonView", service.chkItemCd(model));
				System.out.println("TEST COND ==> "+mav);
				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					System.out.println(e.getMessage());
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}

}