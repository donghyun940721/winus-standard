package com.logisall.winus.wmsit.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT100Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT100Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT100Service")
	private WMSIT100Service service;
	
    @Resource(name = "WMSOP642Service")
    private WMSOP642Service WMSOP642_service;
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 송장피킹검수 - 대화물류
	 * 작성자 : kimzero
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT100.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
		    mav = new ModelAndView("winus/wmsit/WMSIT100");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : invcItemList
	 * Method 설명 : 운송장 품목 조회
	 * 작성자 : kimzero
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT100/invcItemList.action")
	public ModelAndView invcItemList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.invcItemList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID 	: getBoxNum
	 * Method 설명  : B2B검수 box no 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT100/getBoxNum.action")
	public ModelAndView getBoxNum(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getBoxNum(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : chkConfirmB2B
	 * Method 설명 : B2B검수
	 * 작성자 : sing09
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIT100/chkConfirmB2B.action")
	public ModelAndView chkConfirmB2B(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.chkConfirmB2B(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : ordChkConfirm
	 * Method 설명      : B2B 검수 리스트 조회 (우측 그리드)
	 * 작성자                 : 이성중
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSIT100/ordChkConfirm.action")
	public ModelAndView ordChkConfirm(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordChkConfirm(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	   /*-
     * Method ID : poolBoxList
     * Method 설명 : 대화물류 검수 조회 (박스정보)
     * 작성자 : schan
     * @param model
     * @return
     */
    @RequestMapping("/WMSIT100/poolBoxList.action")
    public ModelAndView poolBoxList(Map<String, Object> model) {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.poolBoxList(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    
  /*
  * Method ID : workPickingComp
  * Method 설명 : 대화물류 검수완료
  * 작성자 : schan
  * @param model
  * @return
  */
 @RequestMapping("/WMSIT100/workPickingComp.action")
 public ModelAndView workPickingComp(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
     ModelAndView mav = new ModelAndView("jsonView");
     Map<String, Object> m = new HashMap<String, Object>();
     String errLog = "";
     try {
         model.put("hostUrl", request.getServerName());
         /* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
         if(model.get("PARCEL_COM_TY").equals("04")){
             errLog = "운송사 접수 구분";
             if(!(model.containsKey("RECEIVE_YN") && (model.get("RECEIVE_YN").equals("N") || model.get("RECEIVE_YN").equals("Y")))){
                 m.put("header", "E");
                 m.put("message", "접수구분 데이터가 없습니다.");
                 mav.addAllObjects(m);
                 return mav;
             }
             
             errLog = "검수완료";
             m = service.pickingComplete(model);
             
             if(!m.get("header").toString().equals("Y")){
                 m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(1) : " + m.get("message"));
                 mav.addAllObjects(m);
                 return mav;
             }
             
             if(model.get("RECEIVE_YN").equals("N")){
                 //출력만되고 접수 안되어있으면
                 //배송접수만 진행
                 errLog = "배송접수";
                 m = service.receiveInvcNo(model);
                 if(!m.get("header").toString().equals("Y")){
                     m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(2) : " + m.get("message"));
                     mav.addAllObjects(m);
                     return mav;
                 }
                 m.put("POI_NO_YN", "N");
                 m.put("MSG", m.get("message"));
                 
             } else if(Integer.parseInt((String)model.get("BOX_NO")) > Integer.parseInt((String)model.get("GUBUN"))){
                 //접수는 되어있는데, 검수만 안된 상태면 제외
                 //출력,접수 전체 되어있으면
                 //배송추가접수 진행 및 프린트 실행
                 errLog = "송장추가발행";
                 m = service.addInvcNo(model);
                 if(!m.get("header").toString().equals("Y")){
                     m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(3) : " + m.get("message"));
                     mav.addAllObjects(m);
                     return mav;
                 }
                 
                 errLog = "택배 송장 조회";
                 
                 
                 m = service.workInvcNoList(model);
                 //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                 //beforeTime = System.currentTimeMillis();
                 GenericResultSet grs = (GenericResultSet)m.get("LIST");
                 List list = grs.getList();
                 if(list == null || list.isEmpty() == true || list.size() != 1){
                     m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(4) : LIST=" + list);
                     mav.addAllObjects(m);
                     return mav;
                 }
                 
                 String invcNo = (String)((Map<String,Object>)list.get(0)).get("INVC_NO");
                 
                 errLog = "택배 송장 출력";
                 model.put("invcNo", invcNo);
                 
                 
                 Map<String,Object> modelIns = new HashMap<String,Object>();
                 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
                 for(int i = 0 ; i < tmpCnt ; i++){
                     modelIns.put("ORD_ID"+i, (String)model.get("ORD_ID"+i));
                     modelIns.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
                     modelIns.put("INVC_NO"+i, invcNo);
                 }
                 modelIns.put("PARCEL_COM_TY", (String)model.get("PARCEL_COM_TY"));
                 modelIns.put("ORDER_FLAG", "01");
                 modelIns.put("selectIds", tmpCnt);
                 
                 
                 modelIns.put(ConstantIF.SS_SVC_NO, (String)model.get(ConstantIF.SS_SVC_NO));
                 modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_CLIENT_IP));
                 modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_USER_NO));
                 m = WMSOP642_service.dlvPrintPoiNoUpdate(modelIns);
                 //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                 if(!m.get("POI_NO_YN").equals("Y") || m.get("POI_NO") == null){
                     m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(5) : " + m.get("MSG"));
                     mav.addAllObjects(m);
                     return mav;
                 }
                 
             }
         }else{
             m.put("header", "E");
             m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
         }
     } catch (Exception e) {
         if (log.isErrorEnabled()) {
             log.error("Fail to get picking info :", e);
         }
         m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
     }
     mav.addAllObjects(m);
     return mav;
 }
    
    
    
    
    
}