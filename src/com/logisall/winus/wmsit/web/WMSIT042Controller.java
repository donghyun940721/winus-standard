package com.logisall.winus.wmsit.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsit.service.WMSIT042Service;
import com.logisall.winus.wmsit.service.WMSIT110Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT042Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT042Service")
	private WMSIT042Service service;

	@Resource(name = "WMSIT110Service")
	private WMSIT110Service service110;
	
	/*-
	 * Method ID : WMSIT042
	 * Method 설명 : 입고검수(플랜닥스) 접근
	 * 작성자 : KSG
	 * @param model
	 * @return
	 */
	 @RequestMapping("/WINUS/WMSIT042.action")
	 public ModelAndView WMSIT042(Map<String, Object> model){
		 return new ModelAndView("winus/wmsit/WMSIT042");
	 }
	 
    @RequestMapping("/WMSIT042/ord_list.action")
    public ModelAndView ord_list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.ord_list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIT042/chk_list.action")
    public ModelAndView chk_list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.chk_list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
	@RequestMapping("/WMSIT042/get_item_info.action")
	public ModelAndView get_item_info(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.get_item_info(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSIT042/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
    
	@RequestMapping("/WMSIT042/save_detail.action")
	public ModelAndView save_detail(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.save_detail(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}