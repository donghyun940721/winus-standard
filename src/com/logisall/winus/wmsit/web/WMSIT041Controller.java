package com.logisall.winus.wmsit.web;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT041Service;
import com.logisall.winus.wmsit.service.WMSIT110Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSIT041Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT041Service")
	private WMSIT041Service service;
	

	@Resource(name = "WMSIT110Service")
	private WMSIT110Service wmsit010service;
	
	/*-
	 * Method ID : WMSIT041
	 * Method 설명 : 무의뢰 반품입고검수 접근
	 * 작성자 : yhku 
	 * @param model
	 * @return
	 */
	 @RequestMapping("/WINUS/WMSIT041.action")
	    public ModelAndView WMSIT041(Map<String, Object> model){
	        return new ModelAndView("winus/wmsit/WMSIT041");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
	 }
	 
	 

	 /*-
	 * Method ID : wmsit041.noOrdSelectbyInvcNo
	 * Method 설명 : 반품송장번호를 통한 주문내역 확인(무의뢰)
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */ 
	@RequestMapping("/WMSIT041/noOrdSelectbyInvcNo.action")
	public ModelAndView noOrdSelectbyInvcNo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.noOrdSelectbyInvcNo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	 /*-
		 * Method ID : getItemInfo
		 * Method 설명 : 상품정보 조회
		 * 작성자 : yhku
		 * 
		 * @param model
		 * @return
		 */
		@RequestMapping("/WMSIT041/getItemInfo.action")
		public ModelAndView getItemInfo(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jqGridJsonView", service.getItemInfo(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("fail to get list...", e);
				}
			}
			return mav;
		}
	
	
		
		/*-
		 * Method ID : completeChkNoReqReturnOrder
		 * Method 설명 : 무의뢰 반품입고 적치 완료
		 * 작성자 : yhku
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSIT041/completeChkNoReqReturnOrder.action")
		public ModelAndView completeChkNoReqReturnOrder(Map<String, Object> model) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			try {
				System.out.println("model ====> "+model);
				m = service.completeChkNoReqReturnOrder(model);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save :", e);
				}
				m = new HashMap<String, Object>();
				m.put("errCnt", -1);
	    		m.put("MSG", e.getMessage() );
	    		m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			mav.addAllObjects(m);
			return mav;
		}	
		
		
		
		/*-
		 * Method ID : completeChkNoReqOrdRec
		 * Method 설명 : 무의뢰 반품입고 적치 완료
		 * 작성자 : yhku
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSIT041/completeChkNoReqOrdRec.action")
		public ModelAndView completeChkNoReqOrdRec(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		//public ModelAndView completeChkNoReqOrdRec(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = new HashMap<String, Object>();
			String errLog = "";
			try {
				System.out.println("model ====> "+model);
				Map<String, Object> modelIns = new HashMap<String,Object>();
				String videoYn = (String)model.get("I_VIDEO_YN"); 
				int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
				
				
				//반품송장번호 조회 
				modelIns = new HashMap<String,Object>();
				modelIns.put("SS_SVC_NO", (String)model.get(ConstantIF.SS_SVC_NO));
				modelIns.put("vrSrchCustId", (String)model.get("I_CUST_ID"));
				modelIns.put("vrSrchInvcNo", (String)model.get("I_DELIVERY_NO"));
				m = service.noOrdSelectbyInvcNo(modelIns);
				GenericResultSet grs0 = (GenericResultSet)m.get("LIST");
				List list0 = grs0.getList();
				if(list0.size() < 1){
					

					/*
					 * 반품검수 SP
					 */
					errLog = "[반품검수] ";
					m = service.completeChkNoReqReturnOrder(model);
					if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
						m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(1) : " + m.get("MSG"));
	             		mav.addAllObjects(m);
	            		return mav;
					}				
					
					
					/*
					 * 주문정보
					 */
					errLog = "[주문정보 조회] ";
					modelIns = new HashMap<String,Object>();
					modelIns.put("SS_SVC_NO", (String)model.get(ConstantIF.SS_SVC_NO));
					modelIns.put("vrSrchCustId", (String)model.get("I_CUST_ID"));
					modelIns.put("vrSrchInvcNo", (String)model.get("I_DELIVERY_NO"));
					m = service.noOrdSelectbyInvcNo(modelIns);
					grs0 = (GenericResultSet)m.get("LIST");
					list0 = grs0.getList();
					if(list0.size() < 1){
						m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(2) : LIST=" + list0);
	             		mav.addAllObjects(m);
	            		return mav;
					}else{
						m.put("errCnt", 0);
			    		m.put("MSG", MessageResolver.getMessage("save.success"));
					}
				
				}
				
				
				/*
				 * 비디오등록
				 */
				
				if(videoYn.equals("Y")){
					errLog = "[녹화저장] ";
					modelIns = new HashMap<String,Object>();
					request.setCharacterEncoding("utf-8");
	
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					MultipartFile file = multipartRequest.getFile("txtFile");
					
					String date = (new SimpleDateFormat("yyyyMMdd")).format(System.currentTimeMillis());
					String fileName = file.getOriginalFilename();
					String filePaths = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\RealPacking\\"+(String)model.get(ConstantIF.SS_SVC_NO)+"\\"+(String)model.get("I_CUST_ID")+"\\"+date;
					//String hostUrl = request.getServerName();
					if (!FileHelper.existDirectory(filePaths)) {
					    FileHelper.createDirectorys(filePaths);
					}
	
		            //videoID 파일이름에 추가
		            Map<String,Object> modelID = wmsit010service.getVideoID(model);
		            String id = (String)modelID.get("VIDEO_ID");
		            fileName = "RealPacking"+id+"_"+fileName;
		            
		            File destinationFile = new File(filePaths, fileName);
		            //destinationFile.getTotalSpace();
		            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));
		            
		            int idx = list0.size();
		            for(int i = 0 ; i < idx ; i++){
		            	modelIns.put("ORD_ID"+i , String.valueOf(((Map<String, Object>) list0.get(i)).get("ORD_ID")));
		            	modelIns.put("ORD_SEQ"+i , String.valueOf(((Map<String, Object>) list0.get(i)).get("ORD_SEQ")));
		            }
		            
		            modelIns.put("FILE_NAME",fileName);
		            modelIns.put("VIDEO_ID",id);
		            modelIns.put("URL",filePaths);
		            modelIns.put("selectIds", String.valueOf(idx));
		            modelIns.put("SS_SVC_NO", (String)model.get(ConstantIF.SS_SVC_NO));
		            modelIns.put("CUST_ID", (String)model.get("I_CUST_ID"));
		            modelIns.put("BOX_NO", (String)model.get("I_CHK_BOX_NO"));
		            modelIns.put("INVC_NO", (String)model.get("I_DELIVERY_NO"));
		            
					System.out.println("modelIns >>====> "+modelIns);
		            m = wmsit010service.saveVideo(modelIns);
		            
		            if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
						m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(3) : " + m.get("MSG"));
	             		mav.addAllObjects(m);
	            		return mav;
					}	
				}
	            
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save :", e);
				}
				m = new HashMap<String, Object>();
				m.put("errCnt", -1);
	    		m.put("MSG", e.getMessage() );
	    		m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			mav.addAllObjects(m);
			return mav;
		}	
		
	
	

}