package com.logisall.winus.wmsit.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT020Service;
import com.logisall.winus.wmsit.service.WMSIT040Service;
import com.logisall.winus.wmsit.service.WMSIT110Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSIT040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT040Service")
	private WMSIT040Service service;
	
	@Resource(name = "WMSIT110Service")
	private WMSIT110Service service110;
	
	/*-
	 * Method ID : WMSIT040
	 * Method 설명 : 반품입고검수 접근
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */
	 @RequestMapping("/WINUS/WMSIT040.action")
	    public ModelAndView WMSIT040(Map<String, Object> model){
	        return new ModelAndView("winus/wmsit/WMSIT040");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
	 }
	 
	 /*-
	 * Method ID : wmsit040.ordSelectbyInvcNo
	 * Method 설명 : 반품송장번호를 통한 주문내역 확인
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */ 
	@RequestMapping("/WMSIT040/ordSelectbyInvcNo.action")
	public ModelAndView ordRitemList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordSelectbyInvcNo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : wmsit040.getOrdId
	 * Method 설명 : 반품송장을 통한 주문내역 가져오기 
	 * 작성자 : SUMMER
	 * @param model
	 * @return
	 */ 
	@RequestMapping("/WMSIT040/getOrdId.action")
	public ModelAndView getOrdId(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getOrdId(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	 /*-
		 * Method ID : wmsit040.getBoxNum
		 * Method 설명 : 반품입고 검수 박스 넘버 가져오기 
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT040/getBoxNum.action")
		public ModelAndView getBoxNum(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jsonView", service.getBoxNum(model));
				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}
		
		/*-
		 * Method ID : inOrdChkConfirm
		 * Method 설명 : 입고검수완료
		 * 작성자 : 
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSIT040/inOrdChkConfirm.action")
		public ModelAndView chkConfirmB2B(Map<String, Object> model) throws Exception {

			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			
			try {
				m = service.inOrdChkConfirm(model);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save :", e);
				}
				m = new HashMap<String, Object>();
				System.out.println(m);
				m.put("MSG", m.get("MSG"));
			}
			mav.addAllObjects(m);
			return mav;
		}		

		
		/*-
		 * Method ID : inOrdChkConfirm
		 * Method 설명 : 입고검수완료 - 검수화면녹화 추가 2024.08.20 반영
		 * 작성자 : 
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSIT040/inOrdChkConfirm2.action")
		public ModelAndView chkConfirmB2B2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
//		public ModelAndView chkConfirmB2B2(MultipartHttpServletRequest request, Map<String, Object> model) throws Exception {
//		public ModelAndView chkConfirmB2B2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model){	

//			System.out.println("inOrdChkConfirm2 컨트롤러");
			
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			
			String errLog = "";
			
			try {
				
				String videoYn = (String)model.get("I_VIDEO_YN"); 
				
				System.out.println("model ====> "+model);
				
				errLog = "[검수저장]";
				m = service.inOrdChkConfirm(model);
				if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
					m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(1) : " + m.get("MSG"));
             		mav.addAllObjects(m);
            		return mav;
				}else{
					m.put("errCnt", 0);
					m.put("MSG", MessageResolver.getMessage("save.success"));
				}
				
				/*
				 * 비디오등록
				 */
				Map<String, Object> modelIns = null;
				
				if(videoYn.equals("Y")){
					errLog = "[녹화저장] ";
					modelIns = new HashMap<String,Object>();
					request.setCharacterEncoding("utf-8");
	
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					MultipartFile file = multipartRequest.getFile("txtFile");
					
					
					String date = (new SimpleDateFormat("yyyyMMdd")).format(System.currentTimeMillis());
					String fileName = file.getOriginalFilename();
					String filePaths = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\RealPacking\\"+(String)model.get(ConstantIF.SS_SVC_NO)+"\\"+(String)model.get("I_CUST_ID")+"\\"+date;
					//String hostUrl = request.getServerName();
					if (!FileHelper.existDirectory(filePaths)) {
					    FileHelper.createDirectorys(filePaths);
					}
	
		            //videoID 파일이름에 추가
		            Map<String,Object> modelID = service110.getVideoID(model);
		            String id = (String)modelID.get("VIDEO_ID");
		            fileName = "RealPacking"+id+"_"+fileName;
		            
		            File destinationFile = new File(filePaths, fileName);
		            //destinationFile.getTotalSpace();
		            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));		        
		            
		            modelIns.put("FILE_NAME",fileName);
		            modelIns.put("VIDEO_ID",id);
		            modelIns.put("URL",filePaths);
		            modelIns.put("SIZE",(String)model.get("SIZE"));
          
		            modelIns.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
		            modelIns.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
		            modelIns.put("WORK_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
		            modelIns.put("CUST_ID"     , (String)model.get("I_CUST_ID"));
		            modelIns.put("ORD_ID"     , (String)model.get("ORD_ID"));
		            modelIns.put("ORD_SEQ"     , (String)model.get("ORD_SEQ"));
		            modelIns.put("BOX_NO"     , (String)model.get("I_CHK_BOX_NO_STRING"));
		            modelIns.put("INVC_NO"     , (String)model.get("INVC_NO"));
		            
//					System.out.println("modelIns >>====> "+modelIns);
		            m = service.saveVideo(modelIns);
		            
		            if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
						m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(2) : " + m.get("MSG"));
	             		mav.addAllObjects(m);
	            		return mav;
					}	
				}
				
				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save :", e);
				}
				m = new HashMap<String, Object>();
				System.out.println(m);
				m.put("errCnt", -1);
				m.put("MSG", m.get("MSG"));
			}
			mav.addAllObjects(m);
			return mav;
		}		
		
		 /*-
		 * Method ID : wmsit040.getBoxInfo
		 * Method 설명 : 반품송장번호와 박스번호를 통한 검수내역 확인
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT040/getBoxInfo.action")
		public ModelAndView getBoxInfo(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jqGridJsonView", service.getBoxInfo(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}
		
		 /*-
		 * Method ID : wmsit040.updateCkDel
		 * Method 설명 : 입고검수내역 삭제 CK030.DEL_YN = 'Y'
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT040/updateCkDel.action")
		public ModelAndView updateCkDel(Map<String, Object> model) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			try {
				m = service.updateCkDel(model);
				m.put("MSG", "박스 검수 내역 삭제 성공");
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to Delete Check Box", e);
				}
				m = new HashMap<String, Object>();
				m.put("MSG", "박스 검수 내역 삭제 실패 - "+e.getMessage());
				m.put("errCnt", "1");
			}
			mav.addAllObjects(m);
			return mav;
		}
		
		/*-
		 * Method ID : inOrdChkConfirm
		 * Method 설명 : 반품입고 적치 완료
		 * 작성자 : 
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSIT040/completeChkOrder.action")
		public ModelAndView completeChkOrder(Map<String, Object> model) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = null;
			try {
				System.out.println("model ====> "+model);
				m = service.completeChkOrder(model);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save :", e);
				}
				m = new HashMap<String, Object>();
				System.out.println(m);
				m.put("MSG", m.get("MSG"));
			}
			mav.addAllObjects(m);
			return mav;
		}	
		
		 /*-
		 * Method ID : wmsit040.chkItemCd
		 * Method 설명 : 아이템 정보 확인
		 * 작성자 : SUMMER
		 * @param model
		 * @return
		 */ 
		@RequestMapping("/WMSIT040/chkItemCd.action")
		public ModelAndView chkItemCd(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jsonView", service.chkItemCd(model));
				System.out.println("TEST COND ==> "+mav);
				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					System.out.println(e.getMessage());
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}

		/**
	     * Method ID    : saveVideo
	     * Method 설명      : 웹캠영상 녹화 저장
	     * 작성자                 : schan
	     * @param   model
	     * @return  
	     */
	    @RequestMapping("/WMSIT040/saveVideo.action")
	    public ModelAndView saveVideo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception { // 
	    	//    public ModelAndView saveVideo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception { // 
	        ModelAndView mav = new ModelAndView("jsonView");
	        Map<String, Object> m = new HashMap<String, Object>();
	        try {
	        	
	        	System.out.println("녹화 저장 컨트롤러");
	        	
	            request.setCharacterEncoding("utf-8");

	            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	            MultipartFile file = multipartRequest.getFile("txtFile");
	            
	            String date = (new SimpleDateFormat("yyyyMMdd")).format(System.currentTimeMillis());
	            
	            String fileName = file.getOriginalFilename();
//	            String filePaths = ConstantIF.FILE_ATTACH_PATH +"\\ATCH_FILE\\RealPacking\\"+date;
	            String filePaths = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\RealPacking\\"+(String)model.get(ConstantIF.SS_SVC_NO)+"\\"+(String)model.get("CUST_ID")+"\\"+date;
	           
	            //String hostUrl = request.getServerName();
	            if (!FileHelper.existDirectory(filePaths)) {
	                FileHelper.createDirectorys(filePaths);
	            }
	            
	            //videoID 파일이름에 추가
	            Map<String,Object> modelID = service110.getVideoID(model);
	            String id = (String)modelID.get("VIDEO_ID");
	            fileName = "RealPacking"+id+"_"+fileName;
	            
	            File destinationFile = new File(filePaths, fileName);
	            
	            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));
	            
	            model.put("FILE_NAME",fileName);
	            model.put("VIDEO_ID",id);
	            model.put("URL",filePaths);
	            model.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
	            model.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
	            model.put("WORK_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
	            
	            service.saveVideo(model);
	            
	            m.put("ID", id);
	            m.put("MSG", "SUCCESS");
	            
	            
	        } catch (Exception e) {
	            if (log.isErrorEnabled()) {
	                log.error("Fail to save :", e);
	            }
	            m.put("MSG", MessageResolver.getMessage("save.error"));
	        }
	        mav.addAllObjects(m);
	        return mav;
	    }
	    
	    /**
	     * Method ID    : getVideoData
	     * Method 설명      : video 데이터 select
	     * 작성자                 : schan
	     * @param   model
	     * @return  
	     */
	    @RequestMapping("/WMSIT040/getVideoData.action")
	    public void getVideoData(HttpServletRequest request, HttpServletResponse response) {
	        Map<String, Object> m = new HashMap<String, Object>();
	        Map<String, Object> model = new HashMap<String, Object>();
	        
	        BufferedOutputStream bos = null;
	        FileInputStream fin = null;
	        
	        try {
	            
	            Map<String,String[]> param = request.getParameterMap();
	            
	            if(param == null){
	                response.sendError(HttpServletResponse.SC_NOT_FOUND);
	                return;
	            }
	            
	            model.put("LC_ID",(String)request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
	            model.put("CUST_ID",param.get("CUST_ID")[0]);
	            model.put("BOX_NO",param.get("BOX_NO")[0]);
	            model.put("INVC_NO",param.get("INVC_NO")[0]);
	            model.put("ORD_ID",param.get("ORD_ID")[0]);
	            
	            m = service.getVideoData(model);
	            
	            if(m == null){
	            	 return;
	            }
	            
	            String Url = (String)m.get("URL");
	            String fileName = (String)m.get("FILE_NAME");
	            
	            File file = new File(Url, fileName);
	            
	            if (!file.isFile()) {
	                response.sendError(HttpServletResponse.SC_NOT_FOUND);
	                return;
	            }
	            
	            fin = new FileInputStream(file);
	            int fSize = (int)file.length();

	            response.setContentType("application/octet-stream");
	            response.setContentLength(fSize);
	            response.setHeader("Content-Disposition", "attachment; filename="+fileName+";");
	            
	            byte[] bytes = new byte[1024];
	            bos = new BufferedOutputStream(response.getOutputStream());
	            
	            while(fin.read(bytes) >= 0){
	                bos.write(bytes);
	            }
	            
	            bos.flush();
	            
	        } catch (Exception e) {
	            if (log.isErrorEnabled()) {
	                log.error("Fail to save :", e);
	            }
	        }  finally {
	            if (bos != null) {
	                try {
	                    bos.close();
	                } catch (Exception ie) {
	                }
	            }
	            if (fin != null) {
	                try {
	                    fin.close();
	                } catch (Exception ise) {
	                }
	            }   
	        }
	        
	        
	    }
		
}