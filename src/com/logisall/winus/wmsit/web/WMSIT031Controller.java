package com.logisall.winus.wmsit.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT031Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT031Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT031Service")
	private WMSIT031Service service;
	
	@Resource(name = "WMSOP642Service")
	private WMSOP642Service WMSOP642_service;
	

	/*-
	 * Method ID : mn
	 * Method 설명 : 공통  검수화면
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT031.action")
	public ModelAndView wmsit031(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsit/WMSIT031", service.selectItemGrp(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	

	@RequestMapping("/WMSIT031E1pop1.action")
	public ModelAndView WMSOP030popvx8(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsit/WMSIT031E1pop1");
	}

	
	/*-
	 * Method ID : listSingle
	 * Method 설명 : 공통 검수 조회 (단포)
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT031/listSingle.action")
	public ModelAndView listSingle(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSingle(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listMulti
	 * Method 설명 : 공통 검수 조회 (합포)
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT031/listMulti.action")
	public ModelAndView listMulti(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String,Object> m = new HashMap<String, Object>();
		try {
			m = service.listMulti(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : poolBoxList
	 * Method 설명 : 공통 검수 조회 (박스정보)
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT031/poolBoxList.action")
	public ModelAndView poolBoxList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.poolBoxList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID     : workPickingComp
	 * Method 설명      	 : 공통  송장발행-> 검수완료 -> 시리얼발행 -> 송장출력
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIT031/workPickingComp.action")
	public ModelAndView WMSIT031_workPickingComp(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String errLog = "";
		try {
				Map<String, Object> modelIns = new HashMap<String,Object>();
				Map<String, Object> WMSOP642Model = new HashMap<String, Object>();
				int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
				String invcNo = "";
				String parcelYn = model.get("PARCEL_YN").toString();
				String parcelPrevYn = model.get("PARCELPREV_YN").toString();

				/*
				 * 주문상태 조회한다.
				 */
				errLog = "주문확인";
				modelIns =  new HashMap<String, Object>();
	      		modelIns.put("LC_ID", (String)model.get("LC_ID"));
	      		modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
	      		modelIns.put("ORD_ID", (String)model.get("ORD_ID0"));
	      		m  = service.WMSIT031_chkOrd(modelIns);
	      		
	      		if(Integer.parseInt(m.get("errCnt").toString()) != 0){
	      			m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : " + m.get("MSG"));
             		mav.addAllObjects(m);
            		return mav;
	      		}
				
				/*
				 * 송장접수
				 */
				boolean taskParcelEaiOn = false;
				boolean taskInvcMakeOn = false;
				if(parcelYn.equals("Y")){
					/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
					if(model.get("PARCEL_COM_TY").equals("04") || model.get("PARCEL_COM_TY").equals("06")){
						
						
						/*
						 * 선접수가 있는 경우
						 */
						// 해당 BoxNo로 접수된 송장이 있으면, 검수한 내역으로 DF110 update만 하고 출력
						// 없으면, (기존과  같이) 접수 진행하여 출력한다. 
						if(parcelPrevYn.equals("Y")){
							
							errLog = "[(선접수) 송장 발행 존재 확인]";
							modelIns = new HashMap<String,Object>();
							modelIns.put("LC_ID", (String)model.get("LC_ID"));
							modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
							modelIns.put("GUBUN", (String)model.get("BOX_NO")); //박스번호
							modelIns.put("ORD_ID", (String)model.get("ORD_ID0"));
							
							m = service.WMSIT031_workInvcNo(modelIns);
							GenericResultSet grs0 = (GenericResultSet)m.get("LIST");
							List list0 = grs0.getList();
							
							//System.out.println("list0.size()  : " + list0.size() );
							if(list0.size() > 1){
								m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(1) : LIST=" + list0);
			             		mav.addAllObjects(m);
			            		return mav;
			            		
							}else if(list0 == null || list0.isEmpty() == true){
								
								taskParcelEaiOn = true; //정상적으로 송장접수
								
							}else{
								invcNo = (String)((Map<String,Object>)list0.get(0)).get("INVC_NO");
								String dlvCompCd = (String)((Map<String,Object>)list0.get(0)).get("DLV_COMP_CD");
								
								
								//선송장 접수 정보  업데이트 - merge 작업한다. 
								errLog = "[(선접수) 송장 발행 정보 업데이트]";
								String[] ordId = new String[tmpCnt];
								String[] ordSeq = new String[tmpCnt];
								String[] chkQty = new String[tmpCnt];
								for(int i = 0 ; i < tmpCnt ; i++){
									ordId[i] = (String)model.get("ORD_ID"+i);
									ordSeq[i] = (String)model.get("ORD_SEQ"+i);
									chkQty[i] = (String)model.get("CHK_QTY"+i);
								}
								modelIns = new HashMap<String,Object>();
								modelIns.put("ORD_ID", ordId);
								modelIns.put("ORD_SEQ", ordSeq);
								modelIns.put("CHK_QTY", chkQty);
								modelIns.put("INVC_NO", invcNo );
								modelIns.put("DLV_COMP_CD", dlvCompCd );
								modelIns.put("CHK_BOX_NO", (String)model.get("BOX_NO"));
								modelIns.put("LC_ID", (String)model.get("LC_ID"));
								modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
				                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
								m = service.WMSIT031_workInvcNoUpdateMerge(modelIns);
				                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
				                //beforeTime = System.currentTimeMillis();
								if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
									m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(2) : " + m.get("MSG"));
				             		mav.addAllObjects(m);
				            		return mav;
								}
								
							}
							
						/*
						 * 선접수 없는 경우,
						 */
						}else{
							taskParcelEaiOn = true;
						}
						
						
						
						
						if(taskParcelEaiOn){
							//주문번호 박스구분을 기준으로, 송장 발행여부 선행 확인후 수행한다. 
							errLog = "[송장 발행 존재 확인]";
							modelIns = new HashMap<String,Object>();
							modelIns.put("LC_ID", (String)model.get("LC_ID"));
							modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
							modelIns.put("GUBUN", (String)model.get("BOX_NO"));
							modelIns.put("ORD_ID", (String)model.get("ORD_ID0"));
							
							m = service.WMSIT031_workInvcNo(modelIns);
							GenericResultSet grs0 = (GenericResultSet)m.get("LIST");
							List list0 = grs0.getList();
							if(list0.size() > 0){
								m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(3) : LIST=" + list0);
			             		mav.addAllObjects(m);
			            		return mav;
							}
							
							

							
							// time set
							//long beforeTime = System.currentTimeMillis();
							errLog = "택배 송장 발행";
							//택배 송장 발행
							for(int i = 0 ; i < tmpCnt; i++){
								WMSOP642Model.put("LC_ID"+i, (String)model.get("LC_ID"));
								WMSOP642Model.put("CUST_ID"+i, (String)model.get("CUST_ID"));
								WMSOP642Model.put("TRACKING_NO"+i, (String)model.get("BOX_NO"));
								WMSOP642Model.put("DENSE_FLAG"+i, (String)model.get("DENSE_FLAG"+i));
								WMSOP642Model.put("ORD_QTY"+i, (String)model.get("CHK_QTY"+i));
								WMSOP642Model.put("ORD_ID"+i, (String)model.get("ORD_ID"+i));
								WMSOP642Model.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
								WMSOP642Model.put("INVC_NO"+i, (String)model.get("INVC_NO"+i));
							}
							WMSOP642Model.put("selectIds", (String)model.get("selectIds"));
							WMSOP642Model.put("PARCEL_SEQ_YN", (String)model.get("PARCEL_SEQ_YN"));
							WMSOP642Model.put("PARCEL_COM_TY", (String)model.get("PARCEL_COM_TY"));
							WMSOP642Model.put("PARCEL_COM_TY_SEQ", (String)model.get("PARCEL_COM_TY_SEQ"));
							WMSOP642Model.put("PARCEL_ORD_TY", (String)model.get("PARCEL_ORD_TY"));
							WMSOP642Model.put("PARCEL_PAY_TY", (String)model.get("PARCEL_PAY_TY"));
							WMSOP642Model.put("PARCEL_BOX_TY", (String)model.get("PARCEL_BOX_TY"));
							WMSOP642Model.put("PARCEL_ETC_TY", (String)model.get("PARCEL_ETC_TY"));
							WMSOP642Model.put("PARCEL_PAY_PRICE", (String)model.get("PARCEL_PAY_PRICE"));
							WMSOP642Model.put("ADD_FLAG", (String)model.get("ADD_FLAG")); //'N'
							WMSOP642Model.put("DIV_FLAG", (String)model.get("DIV_FLAG")); //'DIV'
							WMSOP642Model.put("WORK_TYPE", (String)model.get("WORK_TYPE"));
							
							WMSOP642Model.put("SS_CLIENT_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
							WMSOP642Model.put("SS_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			                
							String hostUrl = request.getServerName();
							WMSOP642Model.put("hostUrl", hostUrl);
							
							m = WMSOP642_service.DlvInvcNoOrderCommTotal(WMSOP642Model);
			                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
			                //beforeTime = System.currentTimeMillis();
							if(!m.get("header").toString().equals("Y")){
								m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(4) : " + m.get("message"));
			             		mav.addAllObjects(m);
			            		return mav;
							}else{
								taskInvcMakeOn = true;
							}
						
						
							/*
							 * 운송장출력 - 운송장번호 조회
							 */	
							errLog = "택배 송장 조회";
							modelIns = new HashMap<String,Object>();
							modelIns.put("LC_ID", (String)model.get("LC_ID"));
							modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
							modelIns.put("GUBUN", (String)model.get("BOX_NO"));
							modelIns.put("ORD_ID", (String)model.get("ORD_ID0"));
							
							m = service.WMSIT031_workInvcNo(modelIns);
			                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
			                //beforeTime = System.currentTimeMillis();
							GenericResultSet grs = (GenericResultSet)m.get("LIST");
							List list = grs.getList();
							if(list == null || list.isEmpty() == true || list.size() != 1){
								m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(5) : LIST=" + list);
			             		mav.addAllObjects(m);
			            		return mav;
							}
							invcNo = (String)((Map<String,Object>)list.get(0)).get("INVC_NO");
							
							
						}
						
					}else{
						m.put("MSG", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
						mav.addAllObjects(m);
						return mav;
					}
						
				}
				
				/*
				 * 운송장출력 - 운송장번호 조회
				 */
				if(parcelYn.equals("Y")){
					
				}
				
				
				/*
				 * 검수완료 
				 */
				errLog = "검수완료";
				//검수완료
				modelIns = new HashMap<String,Object>();
				String[] ordId = new String[tmpCnt];
				String[] ordSeq = new String[tmpCnt];
				String[] ritemId = new String[tmpCnt];
				String[] uomId = new String[tmpCnt];
				String[] ordQty = new String[tmpCnt];
				String[] chkQty = new String[tmpCnt];
				for(int i = 0 ; i < tmpCnt ; i++){
					ordId[i] = (String)model.get("ORD_ID"+i);
					ordSeq[i] = (String)model.get("ORD_SEQ"+i);
					ritemId[i] = (String)model.get("RITEM_ID"+i);
					uomId[i] = (String)model.get("UOM_ID"+i);
					ordQty[i] = (String)model.get("ORD_QTY"+i);
					chkQty[i] = (String)model.get("CHK_QTY"+i);
				}
				modelIns.put("ORD_ID", ordId);
				modelIns.put("ORD_SEQ", ordSeq);
				modelIns.put("RITEM_ID", ritemId);
				modelIns.put("UOM_ID", uomId);
				modelIns.put("ORD_QTY", ordQty);
				modelIns.put("CHK_QTY", chkQty);
				modelIns.put("LC_ID", (String)model.get("LC_ID"));
				modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
				modelIns.put("ORD_TYPE", (String)model.get("ORD_TYPE"));
				modelIns.put("CHK_BOX_NO", (String)model.get("BOX_NO"));
				modelIns.put("CHK_BOX_ID", (String)model.get("BOX_ID"));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				
				m = service.WMSIT031_workPickingComp(modelIns);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                //beforeTime = System.currentTimeMillis();
				
				if(Integer.parseInt(m.get("errCnt").toString()) != 0){
					
					//정상적으로 Eai로 송장접수한 경우, 검수 오류시 송장정보 삭제한다. 
					if(taskInvcMakeOn){//if(parcelYn.equals("Y")){
	             		WMSOP642Model = new HashMap<String, Object>();
	             		WMSOP642Model.put("LC_ID", (String)model.get("LC_ID"));
	             		WMSOP642Model.put("INVC_NO", invcNo);
	             		WMSOP642Model.put("DLV_COMP_CD", (String)model.get("PARCEL_COM_TY"));
	             		//선접수(T,S) 제외하고 진행한다.
	             		WMSOP642Model.put("WORK_TYPE", (String)model.get("WORK_TYPE"));
	             		WMSOP642_service.wmsdf110DelYnUpdate(WMSOP642Model);
					}
             		m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(6) : " + m.get("MSG"));
             		mav.addAllObjects(m);
            		return mav;
             	
				}
				
				
				
				/*
				 * 시리얼등록
				 */
             	errLog = "시리얼번호 발행";
             	modelIns = new HashMap<String,Object>();
				ArrayList<String> serialOrdId = new ArrayList<String>();
				ArrayList<String> serialOrdSeq = new ArrayList<String>();
				ArrayList<String> serialRitemId = new ArrayList<String>();
				ArrayList<String> serialUomId = new ArrayList<String>();
				ArrayList<String> serialOrdQty = new ArrayList<String>();
				ArrayList<String> serialChkQty = new ArrayList<String>();
				ArrayList<String> serialNo = new ArrayList<String>();
				int serialCount = 0;
				for(int i = 0 ; i < tmpCnt ; i++){
					if(model.get("SERIAL_CHK_YN"+i).equals("Y")){
						String tempOrdId = (String)model.get("ORD_ID"+i);
						String tempOrdSeq = (String)model.get("ORD_SEQ"+i);
						String tempRitemId = (String)model.get("RITEM_ID"+i);
						String tempUomId = (String)model.get("UOM_ID"+i);
						String tempOrdQty = (String)model.get("ORD_QTY"+i);
						String tempChkQty = (String)model.get("CHK_QTY"+i);
						String[] tmepSerialNo = model.get("SERIAL_NO"+i).toString().split(",");
						for(int j = 0 ; j < tmepSerialNo.length ; j++){
							serialOrdId.add(tempOrdId);
							serialOrdSeq.add(tempOrdSeq);
							serialRitemId.add(tempRitemId);
							serialUomId.add(tempUomId);
							serialOrdQty.add(tempOrdQty);
							serialChkQty.add(tempChkQty);
							serialNo.add(tmepSerialNo[j]);
							serialCount++;
						}
					}
				}
				if(serialCount > 0){
					modelIns.put("ORD_ID", serialOrdId.toArray(new String[serialOrdId.size()]));
					modelIns.put("ORD_SEQ", serialOrdSeq.toArray(new String[serialOrdSeq.size()]));
					modelIns.put("RITEM_ID", serialRitemId.toArray(new String[serialRitemId.size()]));
					modelIns.put("UOM_ID", serialUomId.toArray(new String[serialUomId.size()]));
					modelIns.put("ORD_QTY", serialOrdQty.toArray(new String[serialOrdQty.size()]));
					modelIns.put("CHK_QTY", serialChkQty.toArray(new String[serialChkQty.size()]));
					modelIns.put("SERIAL_NO", serialNo.toArray(new String[serialNo.size()]));
					modelIns.put("LC_ID", (String)model.get("LC_ID"));
					modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
					modelIns.put("ORD_TYPE", (String)model.get("ORD_TYPE"));
					modelIns.put("CHK_BOX_NO", (String)model.get("BOX_NO"));
					modelIns.put("CHK_BOX_ID", (String)model.get("BOX_ID"));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	             	m = service.WMSIT031_workSerialComp(modelIns);
	                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
	                //beforeTime = System.currentTimeMillis();
	             	if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
	             		m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(7) : " + m.get("MSG"));
	             		mav.addAllObjects(m);
	            		return mav;
	             	}
				}
				
				
				
				/*
				 * 송장출력
				 */
				if(parcelYn.equals("Y")){
					errLog = "택배 송장 출력";
					modelIns = new HashMap<String,Object>();
					for(int i = 0 ; i < tmpCnt ; i++){
						modelIns.put("ORD_ID"+i, (String)model.get("ORD_ID"+i));
						modelIns.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
						modelIns.put("INVC_NO"+i, invcNo);
					}
					modelIns.put("PARCEL_COM_TY", (String)model.get("PARCEL_COM_TY"));
					modelIns.put("PARCEL_COM_TY_SEQ", (String)model.get("PARCEL_COM_TY_SEQ"));
					modelIns.put("ORDER_FLAG", "01");
					modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
					modelIns.put("selectIds", tmpCnt);
					
					
					modelIns.put(ConstantIF.SS_SVC_NO, (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_USER_NO));
					m = WMSOP642_service.dlvPrintPoiNoUpdate(modelIns);
	                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
					if(!m.get("POI_NO_YN").equals("Y") || m.get("POI_NO") == null){
						m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(8) : " + m.get("MSG"));
	             		mav.addAllObjects(m);
	            		return mav;
					}
				}else{
					m.put("POI_NO_YN", "Y"); //성공 
				}
				
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID     : pickigCompParcelCancel
	 * Method 설명      	 : 송장  검수삭제
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIT031/pickigCompParcelCancel.action")
	public ModelAndView pickigCompParcelCancel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String errLog = "송장 검수 삭제";
		try {	
			m = service.pickigCompParcelCancel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID     : pickigCompBoxCancel
	 * Method 설명      	 : 박스  검수삭제
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIT031/pickigCompBoxCancel.action")
	public ModelAndView pickigCompBoxCancel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String errLog = "박스 검수 삭제";
		try {	
			m = service.pickigCompBoxCancel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	 * Method ID     : pickigCompMissingOrd
	 * Method 설명      	 : 결품확정
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIT031/pickigCompMissingOrd.action")
	public ModelAndView pickigCompMissingOrd(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String errLog = "결품확정";
		try {	
			m = service.pickigCompMissingOrd(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			//m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
			m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : 관리자에게 문의해주세요.");
		}
		mav.addAllObjects(m);
		return mav;
	}
}