package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT042Service { 
	public Map<String, Object> ord_list(Map<String, Object> model) throws Exception;
	public Map<String, Object> chk_list(Map<String, Object> model) throws Exception;
	public Map<String, Object> get_item_info(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public Map<String, Object> save_detail(Map<String, Object> model) throws Exception;
}
