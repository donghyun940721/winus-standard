package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT041Service { 
    public Map<String, Object> noOrdSelectbyInvcNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> getItemInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> completeChkNoReqReturnOrder(Map<String, Object> model) throws Exception;
    
    
}
