package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT031Service {
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> workPickingComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSingle(Map<String, Object> model) throws Exception;
    public Map<String, Object> listMulti(Map<String, Object> model) throws Exception;
    public Map<String, Object> poolBoxList(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT031_workPickingComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT031_workSerialComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT031_workInvcNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT031_workInvcNoUpdateMerge(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT031_workUpdateDelInvcNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickigCompBoxCancel(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickigCompMissingOrd(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickigCompParcelCancel(Map<String, Object> model) throws Exception;
	public Map<String, Object> WMSIT031_chkOrd(Map<String, Object> modelIns) throws Exception;
}
