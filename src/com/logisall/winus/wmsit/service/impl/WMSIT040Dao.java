package com.logisall.winus.wmsit.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT040Dao")
public class WMSIT040Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : ordSelectbyInvcNo
     * Method 설명 : 반품송장번호를 통한 주문내역 확인
     * 작성자 : 
     * @param model
     * @return
     */
	 public GenericResultSet ordSelectbyInvcNo(Map<String, Object> model) {
	    return executeQueryPageWq("wmsit040.ordSelectbyInvcNo", model);
	 }
	 
	/**
	 * Method ID : getBoxInfo
	 * Method 설명 : 반품송장번호와 박스번호를 통한 검수내역 확인
	 * 작성자 : 
	 * @param model
	 * @return
	*/
	public Object getOrdId(Map<String, Object> model) {
		 //String getOrdId = (String)executeView("wmsit040.getOrdId", model);
	     return executeQueryForList("wmsit040.getOrdId", model);
	}	 
	 
	/**
	     * Method ID : getBoxNum
	     * Method 설명 : 
	     * 작성자 : 
	     * @param model
	     * @return
	     */
	public int getBoxNum(Map<String, Object> model) {
		return (int) executeView("wmsit040.getBoxNum", model);
	}

    /**
     * Method ID : inOrdChkConfirm
     * Method 설명 :  입고검수내역 저장 
     * 작성자 : 
     * @param model
     * @return
     */
	public Object inOrdChkConfirm(Map<String, Object> model){
	   executeUpdate("wmsit040.pk_wmsck030.sp_out_in_ord_chk_confirm", model);
	   return model;
	}	
	
    /**
     * Method ID : getBoxInfo
     * Method 설명 : 반품송장번호와 박스번호를 통한 검수내역 확인
     * 작성자 : 
     * @param model
     * @return
     */
	 public GenericResultSet getBoxInfo(Map<String, Object> model) {
	    return executeQueryPageWq("wmsit040.getBoxInfo", model);
	 }
	 
	 

	/**
	 * Method ID : updateCkDel
	 * Method 설명 :  입고검수내역 삭제 CK030.DEL_YN = 'Y'
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	public Object updateCkDel(Map<String, Object> model){
	    return executeUpdate("wmsit040.updateCkDel", model);
	}   
	
    /**
     * Method ID : completeChkOrder
     * Method 설명 :  반품입고 적치 완료
     * 작성자 : 
     * @param model
     * @return
     */
	public Object completeChkOrder(Map<String, Object> model){
	   executeUpdate("wmsit040.pk_wmsck030.sp_in_multi_loc_check_complete_array", model);
	   return model;
	}	
	
	 
    /**
     * Method ID : chkItemCd
     * Method 설명 : 아이템정보 검색 
     * 작성자 : 
     * @param model
     * @return
     */
	public Object chkItemCd(Map<String, Object> model) {
	     return executeQueryForList("wmsit040.chkItemCd", model);
	}
	
    /**
    * Method ID    : saveVideo
    * Method 설명      : 비디오 데이터 저장
    * 작성자                 : schan
    * @param   model
    * @return  Object
    */
   public Object saveVideo(Map<String, Object> model){
       executeInsert("wmsit040.insertVideo", model);
       return model;
   }
   
   /**
    * Method ID : getVideoData
    * Method 설명 : 비디오 데이터 select
    * 작성자 : schan
    * @param model
    * @return
    */
   public Map<String,Object> getVideoData(Map<String, Object> model) {
       return (Map<String, Object>) executeQueryForObject("wmsif040.getVideoData", model);
   }
    
}

