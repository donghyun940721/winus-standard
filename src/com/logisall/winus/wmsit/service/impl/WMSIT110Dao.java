package com.logisall.winus.wmsit.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT110Dao")
public class WMSIT110Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	// WINUSUSR_IF
	@Autowired
    private SqlMapClient sqlMapClientWinusIf;
    
    /**
     * Method ID : orderItemList
     * Method 설명 : 피킹검수 리스트 조회(좌측 그리드)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet orderItemList(Map<String, Object> model) {
        return executeQueryWq("wmsit110.orderItemList", model);
    }   
    
    /**
     * Method ID : packingItemList
     * Method 설명 : 패킹 리스트 조회(좌측 그리드)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet packingItemList(Map<String, Object> model) {
        return executeQueryWq("wmsit110.packingItemList", model);
    }  
    
    /**
     * Method ID : packingItemListEdiya
     * Method 설명 : 패킹 리스트 조회(좌측 그리드 , 이디야)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet packingItemListEdiya(Map<String, Object> model) {
        GenericResultSet wqrs = new GenericResultSet();
        try {
            List resultList = sqlMapClientWinusIf.queryForList("wmsit110.packingItemListEdiya", model);
            wqrs.setCpage(1);
            wqrs.setTpage(1);
            wqrs.setTotCnt(resultList.size());
            wqrs.setList(resultList);
        }catch(SQLException e) {
            throw new RuntimeSQLException(e);
        }
        return wqrs;
    }
    
    /**
     * Method ID : getVideoID
     * Method 설명 : 비디오 ID
     * 작성자 : schan
     * @param model
     * @return
     */
    public Object getVideoID(Map<String, Object> model) {
        return executeQueryForObject("wmsit110.getVideoID", model);
    }
    
    /**
     * Method ID    : saveVideo
     * Method 설명      : 비디오 데이터 저장
     * 작성자                 : schan
     * @param   model
     * @return  Object
     */
    public Object saveVideo(Map<String, Object> model){
        executeInsert("wmsit110.insertVideo", model);
        return model;
    }    
    
    /**
     * Method ID    : updatePackingBox
     * Method 설명      : 패킹 박스 업데이트
     * 작성자                 : schan
     * @param   model
     * @return  Object
     */
    public Object updatePackingBox(Map<String, Object> model){
        executeUpdate("wmsit110.updatePackingBox", model);
        return model;
    }
    
    /**
     * Method ID    : insertPackingResult
     * Method 설명      : 검수정보 입력 (이디야)
     * 작성자                 : schan
     * @param   model
     * @return  Object
     */
    public void insertPackingResult(Map<String, Object> model){
        try {
            sqlMapClientWinusIf.update("wmsit110.insertPackingResultEdiya", model);
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    
    /**
     * Method ID : getVideoData
     * Method 설명 : 비디오 데이터 select
     * 작성자 : schan
     * @param model
     * @return
     */
    public Map<String,Object> getVideoData(Map<String, Object> model) {
        return (Map<String, Object>) executeQueryForObject("wmsit110.getVideoData", model);
    }
    
    /**
     * Method ID    : getPackingHistory
     * Method 설명      : 패킹이력(이디야)
     * 작성자                 : wl2258
     * @param   model
     * @return  Object
     */
    public List<Map<String, Object>> getPackingHistory(Map<String, Object> model) {
    	int size = Integer.valueOf(model.get("size").toString());
    	int page = Integer.valueOf(model.get("page").toString());
    	
		model.put("offset", (page - 1) * size);
		return executeQueryForList("wmsit110.getPackingHistory", model);
    }

    public Map<String, Object> getPackingHistoryCount(Map<String, Object> model) {
    	return (Map<String, Object>) executeQueryForObject("wmsit110.getPackingHistoryCount",  model);
    }
    
    public GenericResultSet getEdiyaPackingHistoryResult(Map<String, Object> model) {
        GenericResultSet genericResultSet = new GenericResultSet();
        try {
            List list = sqlMapClientWinusIf.queryForList("wmsit110.getEdiyaPackingHistoryResult", model);
            genericResultSet.setList(list);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genericResultSet;
    }
    
    public Map<String, Object> getOrdIdByOrderNoEdiya(Map<String, Object> model) {
    	return (Map<String, Object>) executeQueryForObject("wmsit110.getOrdIdByOrderNoEdiya", model);
    }
}

