package com.logisall.winus.wmsit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT042Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT042Service")
public class WMSIT042ServiceImpl extends AbstractServiceImpl implements WMSIT042Service {
    
    @Resource(name = "WMSIT042Dao")
    private WMSIT042Dao dao;
    
    @Override
    public Map<String, Object> ord_list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        GenericResultSet res = dao.ord_list(model);
        List list = res.getList();
        map.put("LIST", res);
        return map;
    }
    
    @Override
    public Map<String, Object> chk_list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        GenericResultSet res = dao.chk_list(model);
        List list = res.getList();
        map.put("LIST", res);
        return map;
    }
    
    @Override
    public Map<String, Object> get_item_info(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 String flag = (String)model.get("flag");

        	 m.put("row", dao.check_row(model));
        	 m.put("map", dao.get_item_info(model));
             
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){
    	         String[] arr_lc_id  	= new String[tmpCnt];                
    	         String[] arr_cuse_id 	= new String[tmpCnt];         
    	         String[] arr_ord_id  	= new String[tmpCnt];
    	         String[] arr_ord_seq 	= new String[tmpCnt];
    	         String[] arr_to_loc_cd = new String[tmpCnt];
    	         
                 for(int i = 0 ; i < tmpCnt ; i ++){
                	 arr_lc_id[i]     = (String)model.get(ConstantIF.SS_SVC_NO);               
                	 arr_cuse_id[i]   = (String)model.get("CUST_ID");    
                	 arr_ord_id[i]    = (String)model.get("ORD_ID"+i);
                	 arr_ord_seq[i]	  = (String)model.get("ORD_SEQ"+i);
                	 arr_to_loc_cd[i] = (String)model.get("TO_LOC_CD"+i);
                 }
                 
        	     Map<String, Object> modelIns = new HashMap<String, Object>();
        	     
    	         modelIns.put("iLcId", 		arr_lc_id);
    	         modelIns.put("iCustId",	arr_cuse_id);
    	         modelIns.put("iOrdId", 	arr_ord_id);
    	         modelIns.put("iOrdSeq",	arr_ord_seq);
    	         modelIns.put("iToLocCd",	arr_to_loc_cd);
    	         
    	         modelIns.put("WORK_IP", 	(String)model.get(ConstantIF.SS_CLIENT_IP));
    	         modelIns.put("USER_NO", 	(String)model.get(ConstantIF.SS_USER_NO));

    	         modelIns = (Map<String, Object>)dao.orderIns(modelIns);

             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }
    
    @Override
    public Map<String, Object> save_detail(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
	    try {
	    	Map<String, Object> modelDelete = new HashMap<String, Object>();
	    	modelDelete.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	    	modelDelete.put("ord_id_detail", (String)model.get("ord_id_detail"));
	    	modelDelete.put("ord_seq_detail", (String)model.get("ord_seq_detail"));
	    	modelDelete.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	    	dao.delete(modelDelete);
	    	
	    	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	        if(tmpCnt > 0){
	            for(int i = 0 ; i < tmpCnt ; i ++){
	            	Map<String, Object> modelIns = new HashMap<String, Object>();
	            	modelIns.put("ORD_ID", (String)model.get("ORD_ID"+i));
	            	modelIns.put("ORD_SEQ", (String)model.get("ORD_SEQ"+i));
	            	modelIns.put("ORD_TYPE", (String)model.get("ORD_TYPE"+i));
	            	modelIns.put("CONF_SEQ", (String)model.get("CONF_SEQ"+i));
	            	modelIns.put("SERIAL_NO", (String)model.get("SERIAL_NO"+i));
	            	modelIns.put("MAKE_DT", (String)model.get("MAKE_DT"+i));
	            	modelIns.put("USE_DT", (String)model.get("USE_DT"+i));
	            	modelIns.put("LOT_NO", (String)model.get("LOT_NO"+i));
	            	modelIns.put("RITEM_ID", (String)model.get("RITEM_ID"+i));
	            	modelIns.put("UOM_ID", (String)model.get("UOM_ID"+i));
	            	modelIns.put("ORD_QTY", (String)model.get("ORD_QTY"+i));
	            	modelIns.put("CONF_QTY", (String)model.get("CONF_QTY"+i));
	                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                modelIns.put("CUST_ID", (String)model.get("CUST_ID"+i));
	                modelIns.put("SS_CLIENT_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            	 
	                dao.insert(modelIns);
	            }
	        }
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
	    } catch(Exception ex) {
	        m.put("errCnt", 1);
	        m.put("MSG", ex.getMessage() );
	    }
	    return m;
    }
}
