package com.logisall.winus.wmsit.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.stereotype.Service;











import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsit.service.WMSIT110Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT110Service")
public class WMSIT110ServiceImpl extends AbstractServiceImpl implements WMSIT110Service {
    
    @Resource(name = "WMSIT110Dao")
    private WMSIT110Dao dao;

    
    /**
     * Method ID : orderItemList
     * Method 설명 : 검수 품목 주문조회
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> orderItemList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } 
            if (model.get("rows") == null) {
                model.put("pageSize", "10000");
            }        
            map.put("LIST", dao.orderItemList(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : packingItemList
     * Method 설명 : 패킹 품목 조회
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> packingItemList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } 
            if (model.get("rows") == null) {
                model.put("pageSize", "10000");
            }        
            map.put("LIST", dao.packingItemList(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : packingItemListEdiya
     * Method 설명 : 패킹 품목 조회 (이디야)
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> packingItemListEdiya(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } 
            if (model.get("rows") == null) {
                model.put("pageSize", "10000");
            }        
            map.put("LIST", dao.packingItemListEdiya(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    
    /**
     * Method ID : getVideoID
     * Method 설명 : 비디오데이터 저장전 ID 추출
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getVideoID(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("VIDEO_ID",(String)dao.getVideoID(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 대체 Method ID   : saveVideo
     * 대체 Method 설명    : 비디오 blob type db insert
     * 작성자                    : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveVideo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("CUST_ID"     , (String)model.get("CUST_ID"));
            
            modelIns.put("VIDEO_ID"     , model.get("VIDEO_ID"));
            modelIns.put("URL"     , model.get("URL"));
            modelIns.put("FILE_NAME"     , model.get("FILE_NAME"));
            modelIns.put("BOX_NO", model.get("BOX_NO"));
            modelIns.put("INVC_NO", model.get("INVC_NO"));
            
            modelIns.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("WORK_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            
            
            int idx = Integer.parseInt((String)model.get("selectIds"));
            
            for(int i = 0 ; i < idx ; i++){
                Map<String, Object> modelI = new HashMap<String,Object>();
                modelI.putAll(modelIns);
                modelI.put("ORD_ID"     , (String)model.get("ORD_ID"+i));
                modelI.put("ORD_SEQ"     , (String)model.get("ORD_SEQ"+i));
                dao.saveVideo(modelI);
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }    
    
    @Override
    public Map<String,Object> packingComplete (Map<String, Object> model) throws Exception{
        Map<String,Object> m = new HashMap<String,Object>();
        try{
            
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            Map<String, Object> modelIns = new HashMap<String, Object>();

            if(model.get("chkBoxUpdate").equals("true")){
                for(int i = 0 ; i < tmpCnt ; i++){
                    modelIns.clear();
                    modelIns.put("ORD_ID", model.get("ORD_ID"+i));
                    modelIns.put("ORD_SEQ", model.get("ORD_SEQ"+i));
                    modelIns.put("BOX_ID", model.get("BOX_ID"));
                    modelIns.put("BOX_NO", model.get("BOX_NO"));
                    modelIns.put("CUST_ID", model.get("CUST_ID"));
                    modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    
                    dao.updatePackingBox(modelIns);
                }
            }
            
            m.put("errCnt",0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            m.put("errCnt",-1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        return m;
    }
    
    @Override
    public Map<String,Object> packingCompleteEdiya (Map<String, Object> model) throws Exception{
        Map<String,Object> m = new HashMap<String,Object>();
        try{
            
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(tmpCnt > 0){
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("vrSrchLabelNo", (String)model.get("FA_ISSUE_LABEL"));
                dao.insertPackingResult(modelIns);
            }
            
            m.put("errCnt",0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            m.put("errCnt",-1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        return m;
    }
    
    /**
     * Method ID : getVideoData
     * Method 설명 : 비디오 데이터 select
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getVideoData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = dao.getVideoData(model);
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

	@Override
	public Map<String, Object> getPackingHistory(Map<String, Object> model) throws Exception {
		Map<String,Object> m = new HashMap<String,Object>();
        try{
			List<Map<String, Object>> pakcingHistoryList = dao.getPackingHistory(model);
            m.put("data", pakcingHistoryList);
            
            int size = Integer.parseInt(model.get("size").toString());
            int count = Integer.parseInt(dao.getPackingHistoryCount(model).get("COUNT").toString());
            int lastPage = (int) Math.ceil((double) count / size);
            
            m.put("last_page", lastPage);
            m.put("errCnt", 0);
            
        }catch(Exception e){
        	log.error(e.getMessage());
            m.put("errCnt", -1);
            m.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return m;
	}
	
	@Override
	public Map<String,Object> getEdiyaPackingHistoryResult(Map<String, Object> model)throws Exception{
	    Map<String, Object> map = new HashMap<String, Object>();
	    try {
	        map.put("LIST", dao.getEdiyaPackingHistoryResult(model));
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
        return map;
	}

	@Override
	public Map<String, Object> getOrdIdByOrderNoEdiya(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = dao.getOrdIdByOrderNoEdiya(model);
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
}
