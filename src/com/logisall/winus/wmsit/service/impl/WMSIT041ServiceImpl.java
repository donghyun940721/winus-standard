package com.logisall.winus.wmsit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsit.service.WMSIT041Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT041Service")
public class WMSIT041ServiceImpl extends AbstractServiceImpl implements WMSIT041Service {
    
    @Resource(name = "WMSIT041Dao")
    private WMSIT041Dao dao;
    

    
    /**
     * Method ID : noOrdSelectbyInvcNo
     * Method 설명 : 반품송장번호를 통한 주문내역 확인( 무의뢰)
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> noOrdSelectbyInvcNo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {     
            map.put("LIST", dao.noOrdSelectbyInvcNo(model));  
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    
    /**
     * Method ID : getItemInfo
     * Method 설명 : 상품정보 조회
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
	public Map<String, Object> getItemInfo(Map<String, Object> model){
		Map<String, Object> map = new HashMap<String, Object>();
        try {     
            map.put("LIST", dao.getItemInfo(model));  
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
		return map;
	}
       
       
	
	
	/**
     * Method ID : completeChkNoReqReturnOrder
     * Method 설명 : 무의뢰 반품입고 적치 완료
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> completeChkNoReqReturnOrder(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	String rstString = "";
    	try{
    		
			int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
    		System.out.println("model =======> "+model);
			if(tmpCnt > 0){
				String[] ritemId  = new String[tmpCnt];                
                String[] uomId = new String[tmpCnt];          
                String[] chkQty = new String[tmpCnt];          
                String[] rtnMsg = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ritemId[i]    = (String)model.get("I_RITEM_ID"+i);               
                	uomId[i]   = (String)model.get("I_UOM_ID"+i);         
                	chkQty[i]   = (String)model.get("I_CHK_QTY"+i);         
                	rtnMsg[i]   = (String)model.get("I_RTN_MSG"+i);         
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("I_LC_ID"	, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_CUST_ID", (String)model.get("I_CUST_ID"));
                modelIns.put("I_ORD_BIZ_TYPE", (String)model.get("I_ORD_BIZ_TYPE"));
                modelIns.put("I_IN_REQ_DT", (String)model.get("I_IN_REQ_DT"));
                modelIns.put("I_DELIVERY_NO", (String)model.get("I_DELIVERY_NO"));
                modelIns.put("I_CHK_BOX_NO", (String)model.get("I_CHK_BOX_NO"));
                modelIns.put("I_LOC_ID", (String)model.get("I_LOC_ID"));
                modelIns.put("I_IN_CUST_ID", (String)model.get("I_IN_CUST_ID"));
                modelIns.put("I_WORK_MEMO", (String)model.get("I_WORK_MEMO"));
                modelIns.put("I_CUST_NM", (String)model.get("I_CUST_NM"));
                modelIns.put("I_CUST_TEL", (String)model.get("I_CUST_TEL"));
                modelIns.put("I_CUST_ADDR", (String)model.get("I_CUST_ADDR"));;
                modelIns.put("I_RITEM_ID", ritemId);
                modelIns.put("I_UOM_ID", uomId);
                modelIns.put("I_CHK_QTY", chkQty);
                modelIns.put("I_RTN_MSG", rtnMsg);
                modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			modelIns = (Map<String, Object>)dao.completeChkNoReqReturnOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSIT041", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }    	
	    				
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(BizException be) {
    		System.out.println(" be.getMessage()  =====> " + be.getMessage());
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		
    		System.out.println(" e.getMessage()  =====> " + e.getMessage());
    		m.put("errCnt", -1);
    		m.put("MSG", e.getMessage() );
    		throw e;
    	}
    	return m;
    } 
       
       
       
    
}
