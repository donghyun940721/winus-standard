package com.logisall.winus.wmsit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsit.service.WMSIT051Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT051Service")
public class WMSIT051ServiceImpl extends AbstractServiceImpl implements WMSIT051Service {
    
    @Resource(name = "WMSIT051Dao")
    private WMSIT051Dao dao;
    
    /**
     * Method ID : getItemInfo
     * Method 설명 :  입고검수(주문생성) 상품 조회
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getItemInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.getItemInfo(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : checkInComplete
     * Method 설명 :  입고검수(주문생성) 입고확정
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor=Exception.class)
    public Map<String, Object> checkInComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            /*
             * 1. 입고주문 등록
             * 2. 해당 주문 간편입고 진행
             * 3. 완료
             * 실패시 롤백 진행
             * 
             * */
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            List<Map<String,Object>> list = (List<Map<String,Object>>) model.get("workList");
            
            int listCount = list.size();
            
            if (listCount < 1){
                throw new Exception(MessageResolver.getMessage("save.error"));
            }
            
            String[] dsSub_rowStatus    = new String[listCount];
            String[] ordSeq             = new String[listCount];
            String[] ritemId            = new String[listCount];
            String[] custLotNo          = new String[listCount];
            String[] realInQty          = new String[listCount];
            String[] realOutQty         = new String[listCount];
            String[] makeDt             = new String[listCount];
            String[] timePeriodDay      = new String[listCount];
            String[] locYn              = new String[listCount];
            String[] pdaCd              = new String[listCount];
            String[] workYn             = new String[listCount];
            String[] rjType             = new String[listCount];
            String[] realPltQty         = new String[listCount];
            String[] realBoxQty         = new String[listCount];
            String[] confYn             = new String[listCount];
            String[] unitAmt            = new String[listCount];
            String[] amt                = new String[listCount];
            String[] eaCapa             = new String[listCount];
            String[] boxBarcode         = new String[listCount];
            String[] inOrdUomId         = new String[listCount];
            String[] inWorkUomId        = new String[listCount];
            String[] outOrdUomId        = new String[listCount];
            String[] outWorkUomId       = new String[listCount];
            String[] inOrdQty           = new String[listCount];
            String[] inWorkOrdQty       = new String[listCount];
            String[] outOrdQty          = new String[listCount];
            String[] outWorkOrdQty      = new String[listCount];
            String[] refSubLotId        = new String[listCount];
            String[] dspId              = new String[listCount];
            String[] carId              = new String[listCount];
            String[] cntrId             = new String[listCount];
            String[] cntrNo             = new String[listCount];
            String[] cntrType           = new String[listCount];
            String[] badQty             = new String[listCount];
            String[] uomNm              = new String[listCount];
            String[] unitPrice          = new String[listCount];
            String[] whNm               = new String[listCount];
            String[] itemKorNm          = new String[listCount];
            String[] itemEngNm          = new String[listCount];
            String[] repUomId           = new String[listCount];
            String[] uomCd              = new String[listCount];
            String[] uomId              = new String[listCount];
            String[] repUomCd           = new String[listCount];
            String[] repuomNm           = new String[listCount];
            String[] itemGrpId          = new String[listCount];
            String[] expiryDate         = new String[listCount];
            String[] inOrdWeight        = new String[listCount];
            String[] rtiNm              = new String[listCount];
            String[] itemBestDate       = new String[listCount];
            String[] itemBestDateEnd    = new String[listCount];
            String[] unitNo             = new String[listCount];
            String[] ordDesc            = new String[listCount];
            String[] validDt            = new String[listCount];
            String[] etc2               = new String[listCount];
            
            for(int i = 0 ; i < listCount ; i++){
                Map<String,Object> data = list.get(i);
                dsSub_rowStatus[i] = "INSERT";
                ritemId[i] = (String)data.get("RITEM_ID");
                inWorkOrdQty[i] = String.valueOf(data.get("ORD_QTY"));
                itemBestDateEnd[i] = (String)data.get("ITEM_BEST_DATE_END");
                makeDt[i] = (String)data.get("MAKE_DT");
                custLotNo[i] = (String)data.get("CUST_LOT_NO");
                inOrdUomId[i] = (String)data.get("UOM_ID");
                inWorkUomId[i] = (String)data.get("UOM_ID");
                
            }
                        
            modelIns.put("dsMain_rowStatus"     , "");
            modelIns.put("vrOrdId"              , "");
            modelIns.put("inReqDt"              , DateUtil.getLocalDate());
            modelIns.put("inDt"                 , "");
            modelIns.put("custPoid"             , "");
            modelIns.put("custPoseq"            , "");
            modelIns.put("orgOrdId"             , "");
            modelIns.put("orgOrdSeq"            , "");
            modelIns.put("vrWhId"               , "");
            modelIns.put("outWhId"              , "");
            modelIns.put("transCustId"          , "");
            modelIns.put("vrCustId"             , model.get("CUST_ID"));
            modelIns.put("pdaFinishYn"          , "");
            modelIns.put("blNo"                 , "");
            modelIns.put("workStat"             , "");
            modelIns.put("ordType"              , "01");
            modelIns.put("ordSubtype"           , "20");
            modelIns.put("outReqDt"             , "");
            modelIns.put("outDt"                , "");
            modelIns.put("pdaStat"              , "");
            modelIns.put("workSeq"              , "");
            modelIns.put("capaTot"              , "");
            modelIns.put("kinOutYn"             , "");
            modelIns.put("carConfYn"            , "");
            modelIns.put("gvLcId"               , model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("tplOrdId"             , "");
            modelIns.put("approveYn"            , "");
            modelIns.put("payYn"                , "");
            modelIns.put("inCustId"             , "");
            modelIns.put("inCustAddr"           , "");
            modelIns.put("inCustEmpNm"          , "");
            modelIns.put("inCustTel"            , "");
            
            modelIns.put("dsSub_rowStatus"      , dsSub_rowStatus);
            modelIns.put("ordSeq"               , ordSeq         );
            modelIns.put("ritemId"              , ritemId        );
            modelIns.put("custLotNo"            , custLotNo      );
            modelIns.put("realInQty"            , realInQty      );
            modelIns.put("realOutQty"           , realOutQty     );
            modelIns.put("makeDt"               , makeDt         );
            modelIns.put("timePeriodDay"        , timePeriodDay  );
            modelIns.put("locYn"                , locYn          );
            modelIns.put("pdaCd"                , pdaCd          );
            modelIns.put("workYn"               , workYn         );
            modelIns.put("rjType"               , rjType         );
            modelIns.put("realPltQty"           , realPltQty     );
            modelIns.put("realBoxQty"           , realBoxQty     );
            modelIns.put("confYn"               , confYn         );
            modelIns.put("unitAmt"              , unitAmt        );
            modelIns.put("amt"                  , amt            );
            modelIns.put("eaCapa"               , eaCapa         );
            modelIns.put("boxBarcode"           , boxBarcode     );
            modelIns.put("inOrdUomId"           , inOrdUomId     );
            modelIns.put("inWorkUomId"          , inWorkUomId    );
            modelIns.put("outOrdUomId"          , outOrdUomId    );
            modelIns.put("outWorkUomId"         , outWorkUomId   );
            modelIns.put("inOrdQty"             , inOrdQty       );
            modelIns.put("inWorkOrdQty"         , inWorkOrdQty   );
            modelIns.put("outOrdQty"            , outOrdQty      );
            modelIns.put("outWorkOrdQty"        , outWorkOrdQty  );
            modelIns.put("refSubLotId"          , refSubLotId    );
            modelIns.put("dspId"                , dspId          );
            modelIns.put("carId"                , carId          );
            modelIns.put("cntrId"               , cntrId         );
            modelIns.put("cntrNo"               , cntrNo         );
            modelIns.put("cntrType"             , cntrType       );
            modelIns.put("badQty"               , badQty         );
            modelIns.put("uomNm"                , uomNm          );
            modelIns.put("unitPrice"            , unitPrice      );
            modelIns.put("whNm"                 , whNm           );
            modelIns.put("itemKorNm"            , itemKorNm      );
            modelIns.put("itemEngNm"            , itemEngNm      );
            modelIns.put("repUomId"             , repUomId       );
            modelIns.put("uomCd"                , uomCd          );
            modelIns.put("uomId"                , uomId          );
            modelIns.put("repUomCd"             , repUomCd       );
            modelIns.put("repuomNm"             , repuomNm       );
            modelIns.put("itemGrpId"            , itemGrpId      );
            modelIns.put("expiryDate"           , expiryDate     );
            modelIns.put("inOrdWeight"          , inOrdWeight    );
            modelIns.put("rtiNm"                , rtiNm          );
            modelIns.put("itemBestDate"         , itemBestDate   );
            modelIns.put("itemBestDateEnd"      , itemBestDateEnd);
            modelIns.put("unitNo"               , unitNo         );
            modelIns.put("ordDesc"              , ordDesc        );
            modelIns.put("validDt"              , validDt        );
            modelIns.put("etc2"                 , etc2           );
            
            modelIns.put("USER_NO"            , model.get(ConstantIF.SS_USER_NO));
            modelIns.put("WORK_ID"            , model.get(ConstantIF.SS_CLIENT_IP));
            
            Map<String,Object> res = (Map<String,Object>) dao.insertOrder(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(res.get("O_MSG_CODE")), (String)res.get("O_MSG_NAME"));
            
            String[] vrOrdId            = new String[listCount];
            String[] vrOrdSeq           = new String[listCount];
            
            for(int i = 0 ; i < listCount ; i++){
                vrOrdId[i] = (String)res.get("O_MSG_NAME");
                vrOrdSeq[i] = String.valueOf(i+1);
            }
            
            modelIns = new HashMap<String,Object>();
            
            modelIns.put("ordId", vrOrdId);
            modelIns.put("ordSeq", vrOrdSeq);
            
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String,Object>) dao.saveSimpleIn(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            map.put("errCnt",modelIns.get("O_MSG_CODE"));
            map.put("MSG",MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
            e.printStackTrace();
            map.put("errCnt", -1);
            map.put("MSG", e.getMessage());
            throw e;
        }
        return map;
    }
    
}
