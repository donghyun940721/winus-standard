package com.logisall.winus.wmsit.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT031Dao")
public class WMSIT031Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
  
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    
    /**
     * Method ID    : saveOutB2BChkConfirm
     * Method 설명      : 출고 B2B 검수(PK_WMSST010)
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object saveOutB2BChkConfirm(Map<String, Object> model){
        executeUpdate("wmsit020.pk_wmsit020.sp_out_b2b_chk_confirm", model);
        return model;
    }
    
     
    /**
     * Method ID : listSingle
     * Method 설명 : 공통  검수조회(단포)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listSingle(Map<String, Object> model) {
        return executeQueryWq("wmsit031.listSingle", model);
    }   
    /**
     * Method ID : listMulti
     * Method 설명 : 공통  검수조회(합포)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet listMulti(Map<String, Object> model) {
        return executeQueryWq("wmsit031.listMulti", model);
    }
    /**
     * Method ID : chkOrd
     * Method 설명 : 공통  주문 리스트 플래그 조회(합포)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public Object chkOrd(Map<String, Object> model){
    	executeUpdate("wmsit031.pk_wmsdf010.sp_get_check_confirm_ord", model);
    	return model;
    }
    
    /**
     * Method ID : poolBoxList
     * Method 설명 : 공통  검수조회(박스마스터)
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet poolBoxList(Map<String, Object> model) {
        return executeQueryWq("wmsit031.poolBoxList", model);
    }
    
    /**
     * Method ID    : WMSIT031_workPickingComp
     * Method 설명      : 공통  출고검수 
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object WMSIT031_workPickingComp(Map<String, Object> model){
        executeUpdate("wmsit031.pk_wmsdf010.sp_parcel_chk_confirm", model);
        return model;
    }
    /**
     * Method ID    : WMSIT031_workSerialComp
     * Method 설명      : 공통  시리얼 등록
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object WMSIT031_workSerialComp(Map<String, Object> model){
        executeUpdate("wmsit031.pk_wmsdf010.sp_parcel_chk_serial", model);
        return model;
    }
    /**
     * Method ID    : WMSIT031_workSerialComp
     * Method 설명      : 공통  송장 조회
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public GenericResultSet WMSIT031_workInvcNo(Map<String, Object> model){
    	return executeQueryWq("wmsit031.listInvcNo", model);
    }
    
    
    
    /**
     * Method ID    : WMSIT031_workInvcNoUpdateMerge
     * Method 설명      : 공통  출고검수 
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object WMSIT031_workInvcNoUpdateMerge(Map<String, Object> model){
        executeUpdate("wmsit031.pk_wmsdf010.sp_parcel_prev_info_update", model);
        return model;
    }
    
    
    /**
	 * Method ID	: WMSIT031_workUpdateDelInvcNo 
	 * Method 설명	: 송장정보 del update 
	 * 작성자			: yhku
	 * 
	 * @param model
	 * @return
	 */
	public Object WMSIT031_workUpdateDelInvcNo(Map<String, Object> model) {
		return executeUpdate("wmsit031.workUpdateDelInvcNo", model);
	}
	
    
    
    
    
    /**
     * Method ID    : pickigCompParcelCancel
     * Method 설명      : 송장  검수취소
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object pickigCompParcelCancel(Map<String, Object> model){
    	executeUpdate("wmsit031.pk_wmsdf010.sp_check_confirm_cancel", model);
    	return model;
    }
    
    
    /**
     * Method ID    : pickigCompBoxCancel
     * Method 설명      : 박스  검수취소
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object pickigCompBoxCancel(Map<String, Object> model){
    	executeUpdate("wmsit031.pk_wmsdf010.sp_check_confirm_box_cancel", model);
    	return model;
    }
    
    
    /**
     * Method ID    : pickigCompMissingOrd
     * Method 설명      : 결품확정
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object pickigCompMissingOrd(Map<String, Object> model){
    	executeUpdate("wmsit031.pk_wmsdf010.sp_parcel_check_confirm_missing_ord", model);
    	return model;
    }
    
}

