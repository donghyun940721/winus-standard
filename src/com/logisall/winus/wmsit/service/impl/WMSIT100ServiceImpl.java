package com.logisall.winus.wmsit.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsit.service.WMSIT100Service;
import com.logisall.winus.wmsop.service.impl.WMSOP520Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT100Service")
public class WMSIT100ServiceImpl extends AbstractServiceImpl implements WMSIT100Service {
    
    @Resource(name = "WMSIT100Dao")
    private WMSIT100Dao dao;


    @Resource(name = "WMSOP520Dao")
    private WMSOP520Dao dao1;
    
    /**
     * Method ID   : selectItemGrp
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao1.selectItemGrp(model));
        map.put("ITEMGRP98", dao1.selectItemGrp98(model));
        map.put("ITEMGRP99", dao1.selectItemGrp99(model));
        return map;
    }
    
    /**
     * Method ID : invcItemList
     * Method 설명 : 송장피킹 검수 조회
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> invcItemList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } 
            if (model.get("rows") == null) {
                model.put("pageSize", "10000");
            }        
            map.put("LIST", dao.invcItemList(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : chkConfirmB2B
     * Method 설명 : B2B검수
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> chkConfirmB2B(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	try{
    		int boxCnt = Integer.parseInt(model.get("boxCnt").toString());
    		int totalCnt = Integer.parseInt(model.get("totalCnt").toString());
    		if(boxCnt > 0){
    			//box count 수
    			for(int i = 1 ; i <= boxCnt ; i ++){
    				int tempCnt = 0;
    				
    				for(int j = 0; j < totalCnt; j ++){
    					if(i == Integer.parseInt((String) model.get("BOX_NO_"+j))){
    						//box당 String Array 배열 수 체크
    						tempCnt++;
    					}
    				}
    				if(tempCnt > 0){
	    				String[] COMP_ORD_ID    = new String[tempCnt];
	    				String[] COMP_ORD_SEQ  	= new String[tempCnt];
	    				String[] COMP_RITEM_ID  = new String[tempCnt];
	    				String[] COMP_UOM_ID 	= new String[tempCnt];
	    				String[] COMP_ORD_QTY 	= new String[tempCnt];
	    				String[] COMP_CHK_QTY 	= new String[tempCnt];
	    				String BOX_NO = "";
	    				
	    				// 배열 사이즈 체크 우측 그리드 row는 모두 돌리면서 배열 compCnt는 배열수에 맞게 처리..
	    				int compCnt = 0;
	    				for(int j = 0; j < totalCnt; j ++){
	    					//for문  box와  total count 박스 일치 시 담아줌
	    					if(i == Integer.parseInt((String) model.get("BOX_NO_"+j))){
		    					COMP_ORD_ID[compCnt]    	= (String)model.get("COMP_ORD_ID_"	+j);
		    					COMP_ORD_SEQ[compCnt] 	= (String)model.get("COMP_ORD_SEQ_"	+j);
		    					COMP_RITEM_ID[compCnt]    = (String)model.get("COMP_RITEM_ID_"+j);
		    					COMP_UOM_ID[compCnt] 		= (String)model.get("COMP_UOM_ID_"	+j);
		    					COMP_ORD_QTY[compCnt] 	= (String)model.get("COMP_ORD_QTY_"	+j);
		    					COMP_CHK_QTY[compCnt] 	= (String)model.get("COMP_CHK_QTY_"	+j);
		    					BOX_NO 				= (String)model.get("BOX_NO_"		+j);
		    					compCnt++;
	    					}
	    				}
	    				//프로시져 담을것 
	    				Map<String, Object> modelIns = new HashMap<String, Object>();
	    				//ARRAY 형
	    				modelIns.put("COMP_ORD_ID"	, COMP_ORD_ID);
	    				modelIns.put("COMP_ORD_SEQ"	, COMP_ORD_SEQ);
	    				modelIns.put("COMP_RITEM_ID", COMP_RITEM_ID);
	    				modelIns.put("COMP_UOM_ID"	, COMP_UOM_ID);
	    				modelIns.put("COMP_ORD_QTY"	, COMP_ORD_QTY);
	    				modelIns.put("COMP_CHK_QTY"	, COMP_CHK_QTY);
	    				
	    				//VARCHAR BOX
	    				modelIns.put("COMP_CHK_BOX_NO"	, BOX_NO);
	    				
	    				//VARCHAR 형 고정
	    				modelIns.put("COMP_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	    				modelIns.put("COMP_CUST_ID", (String)model.get("I_CUST_ID"));
	    				modelIns.put("COMP_ORD_TYPE", (String)model.get("I_ORD_TYPE"));
	    				modelIns.put("COMP_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	    				modelIns.put("COMP_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	    				
	    				
	    				//프로시저에 던지고 box count for문 계속.. 
		    			modelIns = (Map<String, Object>)dao.chkConfirmB2B(modelIns);
		    			ServiceUtil.isValidReturnCode("WMSIT100", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	    			}
    			}
    		}
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(BizException be) {
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID : ordChkConfirm
     * Method 설명 : B2B 검수 리스트 조회 (우측 그리드)
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> ordChkConfirm(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.ordChkConfirm(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
    	return map;
    }   
    
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            model.put("inKey", "ORD01");
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : getBoxNum
     * Method 설명 : box no 조회
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getBoxNum(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("BOXNUM", dao.getBoxNum(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : poolBoxList
     * Method 설명 : 대화물류 검수조회(박스마스터)
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> poolBoxList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {   
              map.put("LIST", dao.poolBoxList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : pickingComplete
     * Method 설명 : 대화물류 검수완료
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> pickingComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            Map<String, Object> modelIns = new HashMap<String, Object>();

            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];
            String[] workQty = new String[tmpCnt];
            for(int i = 0 ; i < tmpCnt ; i++){
                ordId[i] = (String)model.get("ORD_ID"+i);
                ordSeq[i] = (String)model.get("ORD_SEQ"+i);
                workQty[i] = (String)model.get("WORK_QTY"+i);
            }
            modelIns.put("ORD_ID", ordId);
            modelIns.put("ORD_SEQ", ordSeq);
            modelIns.put("WORK_QTY", workQty);
            modelIns.put("BOX_ID", model.get("BOX_ID"));
            modelIns.put("CUST_ID", model.get("CUST_ID"));
            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            
            modelIns = (Map<String, Object>)dao.pickingComplete(modelIns);
            ServiceUtil.isValidReturnCode("WMSIT100", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            map.put("header","Y");
            
        } catch (Exception e) {
            log.error(e.toString());
            e.printStackTrace();
            map.put("header","N");
            map.put("message", e.getMessage());
        }
        return map;
    }
    
    /**
     *  Method ID        :  workInvcNoList
     *  Method 설명    : 대화물류 송장번호 조회
     *  작성자              : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> workInvcNoList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String,Object>();
            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
            modelIns.put("GUBUN", (String)model.get("BOX_NO"));
            modelIns.put("ORD_ID", (String)model.get("ORD_ID0"));
            map.put("LIST", dao.workInvcNoList(modelIns));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 
     * 대체 Method ID : receiveInvcNo
     * 대체 Method 설명 : 대화물류 출고검수 택배접수
     * 작성자          : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> receiveInvcNo(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         String hostUrl     = (String)model.get("hostUrl");
         String eaiUrl  = "";
         String sUrl    = "";
         if(hostUrl != null && hostUrl.equals("localhost")){
            //eaiUrl  = "https://172.31.10.253:5201/"; //실운영(local에서 접근)
        	eaiUrl  = "https://10.30.1.14:5201/"; 
         }else{
            //eaiUrl  = "https://52.79.206.98:5201/"; //실운영
        	eaiUrl  = "https://10.30.1.14:5201/";
         }
         
         if(model.get("PARCEL_COM_TY").equals("04")){//대한통운
            sUrl = eaiUrl +  "PARCEL/CJLOGIS/COMM/ORDER/POST";
         }
        
         try{
             //운임비금액
             String parcelPayPrice = "";
             if(model.get("PARCEL_PAY_PRICE") != null && !model.get("PARCEL_PAY_PRICE").equals("")){
                 parcelPayPrice  = model.get("PARCEL_PAY_PRICE").toString();
             }
             //박스구분
             String parcelBoxTy = "";
             if(model.get("BOX_GB") != null && !model.get("BOX_GB").equals("")){
                 parcelBoxTy  = model.get("BOX_GB").toString();
             }
             //기타구분
             String parcelEtcTy = "";
             if(model.get("PARCEL_ETC_TY") != null && !model.get("PARCEL_ETC_TY").equals("")){
                 parcelEtcTy  = model.get("PARCEL_ETC_TY").toString();
             }
             String invcNo = model.get("INVC_NO").toString();
             JSONObject jsonObject = new JSONObject();
             JSONArray ord_seq_array = new JSONArray();
             
             for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
                 //JSON NEW
                 JSONObject data = new JSONObject();
                 data.put("ORD_ID", (String)model.get("ORD_ID"+j));
                 data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
                 data.put("ORD_QTY", (String)model.get("WORK_QTY"+j));
                 ord_seq_array.add(data);
             }
         
             jsonObject.put("ORD_INFO", ord_seq_array);
             String AddDataJson = ord_seq_array.toString();
         
             //Json Data
             String jsonInputString = "{\"docRequest\":   {\"PARCEL_SEQ_YN\":\""     +model.get("PARCEL_SEQ_YN").toString()+"\""
                                     +                   ",\"PARCEL_COM_TY\":\""     +model.get("PARCEL_COM_TY").toString()+"\""             
                                     +                   ",\"PARCEL_COM_TY_SEQ\":\"" +model.get("PARCEL_COM_TY_SEQ").toString()+"\""
                                     +                   ",\"PARCEL_ORD_TY\":\""     +model.get("PARCEL_ORD_TY").toString()+"\""
                                     +                   ",\"PARCEL_PAY_TY\":\""     +model.get("PARCEL_PAY_TY").toString()+"\""
                                     +                   ",\"PARCEL_PAY_PRICE\":\""  +parcelPayPrice+"\""
                                     +                   ",\"PARCEL_BOX_TY\":\""     +parcelBoxTy+"\""
                                     +                   ",\"PARCEL_ETC_TY\":\""     +parcelEtcTy+"\""
                                     //+                 ",\"ADD_NROW\":\""          +""+"\""
                                     
                                     +                   ",\"LC_ID\":\""             +(String)model.get(ConstantIF.SS_SVC_NO)+"\""
                                     +                   ",\"CUST_ID\":\""           +(String)model.get("CUST_ID")+"\""
                                     
                                     +                   ",\"ORD_INFO\":"            + AddDataJson
                                     +                   ",\"USER_NO\":\""           +(String)model.get(ConstantIF.SS_USER_NO)+"\""
                             //      +                   ",\"DATA_SENDER_NM\":\""    +(String)model.get("DATA_SENDER_NM"+i)+"\"}"
                                     + "}"
                                     + ",\"I_INVC_NO\":\""+ invcNo + "\""
                                     + ",\"I_MODIFY_CODE\":\"" + "BOX" +"\"}";
             System.out.println("jsonInputString(RTN) : "+ jsonInputString);
             
             m =  connectionDlvEAI(sUrl, jsonInputString, m);
            
         } catch(Exception e){
             throw e;
         }
         return m;
    }
    /**
     * 
     * 대체 Method ID : addInvcNo
     * 대체 Method 설명 : 대화물류 출고검수 송장 추가 접수
     * 작성자          : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> addInvcNo(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         String hostUrl     = (String)model.get("hostUrl");
         String eaiUrl  = "";
         String sUrl    = "";
         if(hostUrl != null && hostUrl.equals("localhost")){
            //eaiUrl  = "https://172.31.10.253:5201/"; //실운영(local에서 접근)
        	eaiUrl  = "https://10.30.1.14:5201/"; 
         }else{
            //eaiUrl  = "https://52.79.206.98:5201/"; //실운영
        	eaiUrl  = "https://10.30.1.14:5201/"; 
         }
         
         if(model.get("PARCEL_COM_TY").equals("04")){//대한통운
            sUrl = eaiUrl +  "PARCEL/CJLOGIS/COMM/ORDER/V2";
         }
        
         try{
             //운임비금액
             String parcelPayPrice = "";
             if(model.get("PARCEL_PAY_PRICE") != null && !model.get("PARCEL_PAY_PRICE").equals("")){
                 parcelPayPrice  = model.get("PARCEL_PAY_PRICE").toString();
             }
             //박스구분
             String parcelBoxTy = "";
             if(model.get("BOX_GB") != null && !model.get("BOX_GB").equals("")){
                 parcelBoxTy  = model.get("BOX_GB").toString();
             }
             //기타구분
             String parcelEtcTy = "";
             if(model.get("PARCEL_ETC_TY") != null && !model.get("PARCEL_ETC_TY").equals("")){
                 parcelEtcTy  = model.get("PARCEL_ETC_TY").toString();
             }
             String invcNo = model.get("INVC_NO").toString();
             JSONObject jsonObject = new JSONObject();
             JSONArray ord_seq_array = new JSONArray();
             
             for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
                 //JSON NEW
                 JSONObject data = new JSONObject();
                 data.put("ORD_ID", (String)model.get("ORD_ID"+j));
                 data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
                 data.put("ORD_QTY", (String)model.get("WORK_QTY"+j));
                 ord_seq_array.add(data);
             }
         
             jsonObject.put("ORD_INFO", ord_seq_array);
             String AddDataJson = ord_seq_array.toString();
         
             //Json Data
             String jsonInputString = "{\"docRequest\":   {\"PARCEL_SEQ_YN\":\""     +model.get("PARCEL_SEQ_YN").toString()+"\""
                                     +                   ",\"PARCEL_COM_TY\":\""     +model.get("PARCEL_COM_TY").toString()+"\""             
                                     +                   ",\"PARCEL_COM_TY_SEQ\":\"" +model.get("PARCEL_COM_TY_SEQ").toString()+"\""
                                     +                   ",\"PARCEL_ORD_TY\":\""     +model.get("PARCEL_ORD_TY").toString()+"\""
                                     +                   ",\"PARCEL_PAY_TY\":\""     +model.get("PARCEL_PAY_TY").toString()+"\""
                                     +                   ",\"PARCEL_PAY_PRICE\":\""  +parcelPayPrice+"\""
                                     +                   ",\"PARCEL_BOX_TY\":\""     +parcelBoxTy+"\""
                                     +                   ",\"PARCEL_ETC_TY\":\""     +parcelEtcTy+"\""
                                     //+                 ",\"ADD_NROW\":\""          +""+"\""
                                     
                                     +                   ",\"LC_ID\":\""             +(String)model.get(ConstantIF.SS_SVC_NO)+"\""
                                     +                   ",\"CUST_ID\":\""           +(String)model.get("CUST_ID")+"\""
                                     +                   ",\"Add_FLAG\":\""           + "N" +"\""
                                     +                   ",\"DIV_FLAG\":\""           + "DIV" +"\""
                                     
                                     +                   ",\"BOX_NO\":\""           +(String)model.get("BOX_NO")+"\""
                                     
                                     +                   ",\"ORD_INFO\":"            + AddDataJson
                                     +                   ",\"USER_NO\":\""           +(String)model.get(ConstantIF.SS_USER_NO)+"\""
                             //      +                   ",\"DATA_SENDER_NM\":\""    +(String)model.get("DATA_SENDER_NM"+i)+"\"}"
                                     + "}"
                                     + ", \"PUB_HOST_NAME\":\""+ "WINUS"+"\"" +"}";
             System.out.println("jsonInputString(RTN) : "+ jsonInputString);
             
             m =  connectionDlvEAI(sUrl, jsonInputString, m);
            
         } catch(Exception e){
             throw e;
         }
         return m;
    }
    
    public Map<String, Object> connectionDlvEAI (String sUrl, String jsonInputString, Map<String, Object> m) throws Exception {
    	//TLS 버전 설정
    	System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
    	
    	//ssl disable
        disableSslVerification();
        //System.out.println("sUrl : " + sUrl);
        
        URL url = null; 
        url = new URL(sUrl);
        
        HttpsURLConnection con = null;
        con = (HttpsURLConnection) url.openConnection();
        
        //웹페이지 로그인 권한 적용
        String userpass = "eaiuser01" + ":" + "eaiuser01";
        String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        con.setRequestProperty ("Authorization", basicAuth);
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setConnectTimeout(0);
        con.setReadTimeout(0);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        
        //JSON 보내는 Output stream
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        
        //Response data 받는 부분
        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            //System.out.println(response.toString());
            
            JSONObject jsonData = new JSONObject(response.toString());
            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
            Iterator iterator = docResponse.keys();
            while(iterator.hasNext()){
                String key = (String)iterator.next();
                //System.out.println("iterator .. :  "+key);
                if(key.equals("header"))  m.put("header" , docResponse.get("header").toString());// Y, E
                if(key.equals("message")) m.put("message", docResponse.get("message").toString());// msg
                //------ 추가 ------ 
                if(key.equals("poino")) m.put("POI_NO", docResponse.get("poino").toString());//poino
            }
            
        }
            
        con.disconnect();
        
        return m;
    }
    
    /**
     * 
     * 대체 Method ID : disableSslVerification
     * 대체 Method 설명 :
     * 작성자          : chsong
     * @param model
     * @return
     * @throws Exception
     */
    private static void disableSslVerification() {
        try{
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    
                }
            }};

            //Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            //Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            //Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
    
    
}
