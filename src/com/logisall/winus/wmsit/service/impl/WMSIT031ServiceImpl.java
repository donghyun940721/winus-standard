package com.logisall.winus.wmsit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsit.service.WMSIT031Service;
import com.logisall.winus.wmsop.service.impl.WMSOP520Dao;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT031Service")
public class WMSIT031ServiceImpl extends AbstractServiceImpl implements WMSIT031Service {
    
    @Resource(name = "WMSIT031Dao")
    private WMSIT031Dao dao;


    @Resource(name = "WMSOP520Dao")
    private WMSOP520Dao dao1;
    
    /**
     * Method ID   : selectItemGrp
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao1.selectItemGrp(model));
//        map.put("ITEMGRP98", dao1.selectItemGrp98(model));
//        map.put("ITEMGRP99", dao1.selectItemGrp99(model));
        return map;
    }
    
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            model.put("inKey", "ORD01");
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    
    /**
     *  Method ID 		 :  workPickingComp
     *  Method 설명  	 : 검수완료
     *  작성자            	 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> workPickingComp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	int boxCnt = Integer.parseInt(model.get("boxCnt").toString());
        	int totalCnt = Integer.parseInt(model.get("totalCnt").toString());
        	if(boxCnt > 0){
        		//box count 수
    			for(int i = 1 ; i <= boxCnt ; i ++){
    				int tmpCnt = 0;
    				
    				for(int j = 0; j < totalCnt; j ++){
    					if(i == Integer.parseInt((String) model.get("COMP_CHK_BOX_NO"+j))){
    						//box당 String Array 배열 수 체크
    						tmpCnt++;
    					}
    				}
    				if(tmpCnt > 0){
    					String[] ordId              = new String[tmpCnt];
	     	            String[] ordSeq 			= new String[tmpCnt];
	     	            String[] ritemId		 	= new String[tmpCnt];
	     	            String[] uomId 				= new String[tmpCnt];
	     	            String[] ordQty 			= new String[tmpCnt];
	     	            String[] chkQty 			= new String[tmpCnt];
	     	            String BOX_NO = "";
	     	            
	     	            // 배열 사이즈 체크 우측 그리드 row는 모두 돌리면서 배열 compCnt는 배열수에 맞게 처리..
	    				int compCnt = 0;
	    				for(int j = 0; j < totalCnt; j ++){
	    					//for문  box와  total count 박스 일치 시 담아줌
	    					if(i == Integer.parseInt((String) model.get("COMP_CHK_BOX_NO" + j))){
	    	        			ordId[compCnt] 			= (String)model.get("COMP_ORD_ID" + j);
	    	        			ordSeq[compCnt] 		= (String)model.get("COMP_ORD_SEQ" + j);
	    	        			ritemId[compCnt] 		= (String)model.get("COMP_RITEM_ID" + j);
	    	        			uomId[compCnt] 			= (String)model.get("COMP_UOM_ID" + j);
	    	        			ordQty[compCnt] 		= (String)model.get("COMP_ORD_QTY" + j);
	    	        			chkQty[compCnt]			= (String)model.get("COMP_CHK_QTY" + j);
	    	        			BOX_NO 					= (String)model.get("COMP_CHK_BOX_NO" + j);
		    					compCnt++;
		    					
//		    					System.out.println(j+" COMP_CHK_BOX_NO : "+(String) model.get("COMP_CHK_BOX_NO" + j));
//		    					System.out.println(j+" COMP_ORD_ID : "+(String) model.get("COMP_ORD_ID" + j));
//		    					System.out.println(j+" COMP_ORD_SEQ : "+(String) model.get("COMP_ORD_SEQ" + j));
//		    					System.out.println(j+" COMP_RITEM_ID : "+(String) model.get("COMP_RITEM_ID" + j));
//		    					System.out.println(j+" COMP_UOM_ID : "+(String) model.get("COMP_UOM_ID" + j));
//		    					System.out.println(j+" COMP_ORD_QTY : "+(String) model.get("COMP_ORD_QTY" + j));
//		    					System.out.println(j+" COMP_CHK_QTY : "+(String) model.get("COMP_CHK_QTY" + j));
	    					}
	    				}
	     				// 프로시져에 보낼것들 다담는다
	     	            Map<String, Object> modelIns = new HashMap<String, Object>();
	     	            modelIns.put("ORD_ID"  			, ordId);
	     	            modelIns.put("ORD_SEQ"			, ordSeq);
	     	            modelIns.put("RITEM_ID"			, ritemId);
	     	            modelIns.put("UOM_ID"			, uomId);
	     	            modelIns.put("ORD_QTY"   		, ordQty);
	     	            modelIns.put("CHK_QTY"   		, chkQty);
	     	            
	     	            modelIns.put("LC_ID",  	     (String)model.get("COMP_LC_ID"));
	     	            modelIns.put("CUST_ID",  	 (String)model.get("COMP_CUST_ID"));
	     	            modelIns.put("ORD_TYPE",  	 (String)model.get("COMP_ORD_TYPE"));
	     	            modelIns.put("CHK_BOX_NO",   BOX_NO);
	     	            modelIns.put("USER_NO",  	 (String)model.get("COMP_USER_NO"));
	     	            modelIns.put("WORK_IP",  	 (String)model.get("COMP_WORK_IP"));
	     	            
	     	            //System.out.println(modelIns);
	     	            
	     	            modelIns = (Map<String, Object>)dao.saveOutB2BChkConfirm(modelIns);
	     	            ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	//     	            if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
	//     	            }
	    			}
     			}
            }
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    
    /**
     * Method ID : listSingle
     * Method 설명 : 공통  검수조회(단포)
     * 작성자 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSingle(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {   
              map.put("LIST", dao.listSingle(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listMulti
     * Method 설명 : 공통  검수조회(합포)
     * 작성자 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listMulti(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try { 
        	  GenericResultSet grs = dao.listMulti(model);
        	  List list = grs.getList();
        	  if((list == null || list.isEmpty()) && !(model.containsKey("reSearchFlag") && model.get("reSearchFlag").equals("Y"))){
        		  Map<String, Object> modelIns =  new HashMap<String, Object>();
        		  modelIns.put("LC_ID", model.get("SS_SVC_NO"));
        		  modelIns.put("CUST_ID", model.get("vrSrchCustId"));
        		  modelIns.put("ORD_ID", model.get("vrSrchOrdId"));
        		  map = (Map<String, Object>)dao.chkOrd(modelIns);
        		  map.put("LIST", grs);
   	              ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(map.get("O_MSG_CODE")), (String)map.get("O_MSG_NAME"));
				}
        	  map.put("O_MSG_CODE", "0");
        	  
        	  map.put("msg", grs.getMessage());
        	  map.put("page", grs.getCpage());
        	  map.put("records", grs.getList().size());
        	  map.put("rows",grs.getList());
        	  map.put("total", grs.getTotCnt());
              
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", e.getMessage());
        }
        return map;
    }
    
    
    /**
     * Method ID : WMSIT031_chkOrd
     * Method 설명 : 주문상태 검색 
     * 작성자 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> WMSIT031_chkOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try { 
				map = (Map<String, Object>)dao.chkOrd(model);
				ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(map.get("O_MSG_CODE")), (String)map.get("O_MSG_NAME"));
				map.put("MSG", MessageResolver.getMessage("save.success"));
				map.put("MSG_ORA", "");
				map.put("errCnt", 0);
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
        	map.put("MSG", e.getMessage());
        }
        return map;
    }
    
    
    
    
    /**
     * Method ID : poolBoxList
     * Method 설명 : 공통  검수조회(박스마스터)
     * 작성자 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> poolBoxList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {   
              map.put("LIST", dao.poolBoxList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     *  Method ID 		 :  WMSIT031_workPickingComp
     *  Method 설명  	 : 공통  검수완료 
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> WMSIT031_workPickingComp(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		m = (Map<String, Object>)dao.WMSIT031_workPickingComp(model);
            ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", e.getMessage());
        }
        return m;
    }
    /**
     *  Method ID 	 :  WMSIT031_workSerialComp
     *  Method 설명  	 : 공통  시리얼 입력 
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> WMSIT031_workSerialComp(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		m = (Map<String, Object>)dao.WMSIT031_workSerialComp(model);
            ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", e.getMessage());
        }
        return m;
    }
    /**
     *  Method ID 		 :  WMSIT031_workInvcNo
     *  Method 설명  	 : 공통  송장번호 조회
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> WMSIT031_workInvcNo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {   
              map.put("LIST", dao.WMSIT031_workInvcNo(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    

    /**
     *  Method ID 		 :  WMSIT031_workInvcNoUpdateMerge
     *  Method 설명  	 : 선접수 송장정보 업데이트
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> WMSIT031_workInvcNoUpdateMerge(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {   
        	m = (Map<String, Object>)dao.WMSIT031_workInvcNoUpdateMerge(model);
        	ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
        	m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", "0");
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", e.getMessage());
        }
        return m;
    }
    
    
    /**
     *  Method ID 		 :  WMSIT031_workUpdateDelInvcNo
     *  Method 설명  	 : 송장정보 del update 
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> WMSIT031_workUpdateDelInvcNo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {   
        	dao.WMSIT031_workUpdateDelInvcNo(model);
        	map.put("errCnt", "0");
        } catch (Exception e) {
        	map.put("errCnt", "1");
        }
        return map;
    }
    
    
   
    /**
     *  Method ID 		 :  WMSIT031_workInvcNo
     *  Method 설명  	 : 공통  송장번호 조회
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> pickigCompParcelCancel(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	modelIns.put("LC_ID", model.get("LC_ID"));
        	modelIns.put("CUST_ID", model.get("CUST_ID"));
        	modelIns.put("PARCEL_COM_TY", model.get("PARCEL_COM_TY"));
        	modelIns.put("INVC_NO", model.get("INVC_NO"));
        	modelIns.put("INVC_DELETE_YN", model.get("INVC_DELETE_YN"));
        	
        	modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
        	modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

        	m = (Map<String, Object>) dao.pickigCompParcelCancel(modelIns);
        	ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", e.getMessage());
        }
        return m;
    }
    
    
    
    /**
     *  Method ID 		 :  pickigCompBoxCancel
     *  Method 설명  	 : 공통  송장번호 조회
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> pickigCompBoxCancel(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
        	
        	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            String[] ordId  		= new String[tmpCnt];                
            String[] boxNo 		= new String[tmpCnt];
            String[] invcNo 		= new String[tmpCnt];
            
            for(int i = 0 ; i < tmpCnt ; i ++){
            	ordId[i]    	= (String)model.get("ORD_ID"+i);               
                boxNo[i]   		= (String)model.get("BOX_NO"+i);
                invcNo[i]   	= (String)model.get("INVC_NO"+i);
            }
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("LC_ID"	, model.get("LC_ID"));
			modelIns.put("CUST_ID"	, model.get("CUST_ID"));
			modelIns.put("ORD_ID"	, ordId);
			modelIns.put("BOX_NO"	, boxNo);
			modelIns.put("INVC_NO"	, invcNo);
			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			
			modelIns = (Map<String, Object>)dao.pickigCompBoxCancel(modelIns);
	        ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", e.getMessage());
        }
        return m;
    }
    
    
    /**
     *  Method ID 		 :  pickigCompMissingOrd
     *  Method 설명  	 : 결품확정
     *  작성자            	 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> pickigCompMissingOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        try {
        	
        	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
        	if(tmpCnt > 0){
	            String[] ordId  		= new String[tmpCnt];                
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
	            	ordId[i]    	= (String)model.get("ORD_ID"+i);               
	            }
	            //프로시져에 보낼것들 다담는다
				modelIns.put("LC_ID"	, model.get("LC_ID"));
				modelIns.put("CUST_ID"	, model.get("CUST_ID"));
				modelIns.put("ORD_BIZ_TYPE"	, model.get("ORD_BIZ_TYPE"));
				modelIns.put("ORD_ID"	, ordId);
				modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			
				modelIns = (Map<String, Object>)dao.pickigCompMissingOrd(modelIns);
		        ServiceUtil.isValidReturnCode("WMSIT031", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
        	}
        		
        	m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
        }catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage());
            m.put("ERROR_MSG", (String)modelIns.get("O_MSG_NAME"));
            
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            throw e;
        }
        return m;
    }
}
