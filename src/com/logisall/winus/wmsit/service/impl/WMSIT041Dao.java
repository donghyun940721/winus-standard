package com.logisall.winus.wmsit.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT041Dao")
public class WMSIT041Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : noOrdSelectbyInvcNo
     * Method 설명 : 반품송장번호를 통한 주문내역 확인
     * 작성자 : yhku
     * @param model
     * @return
     */
	 public GenericResultSet noOrdSelectbyInvcNo(Map<String, Object> model) {
	    return executeQueryWq("wmsit041.noOrdSelectbyInvcNo", model);
	 }
	 
	 
	 /**
     * Method ID : getItemInfo
     * Method 설명 : 상품정보 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
	 public GenericResultSet getItemInfo(Map<String, Object> model) {
	    return executeQueryWq("wmsit041.getItemInfo", model);
	 }
	 

    /**
     * Method ID : completeChkNoReqReturnOrder
     * Method 설명 : 무의뢰 반품입고 적치 완료(주문생성~)
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Object completeChkNoReqReturnOrder(Map<String, Object> model){
	   executeUpdate("wmsit041.pk_wmsck030.sp_return_no_req_order_complete_array", model);
	   return model;
	}	
    
	
	
	
	
}

