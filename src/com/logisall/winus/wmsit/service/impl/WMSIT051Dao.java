package com.logisall.winus.wmsit.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT051Dao")
public class WMSIT051Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
    /**
     * Method ID : orderItemList
     * Method 설명 : 입고 검수 상품 검색
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet getItemInfo(Map<String, Object> model) {
        return executeQueryWq("wmsit051.getItemInfo", model);
    }

    /**
     * Method ID : insertOrder
     * Method 설명 : 입고 주문 생성
     * 작성자 : schan
     * @param model
     * @return
     */
    public Object insertOrder(Map<String, Object> model) {
        executeUpdate("wmsit051.pk_wmsop020.sp_insert_order_2048array", model);
        return model;
    }
    
    /**
     * Method ID : saveSimpleIn
     * Method 설명 : 입고 주문 간편입고
     * 작성자 : schan
     * @param model
     * @return
     */
    public Object saveSimpleIn(Map<String, Object> model) {
        executeUpdate("wmsop020.pk_wmsop020.sp_simple_in", model);
        return model;
    }

}

