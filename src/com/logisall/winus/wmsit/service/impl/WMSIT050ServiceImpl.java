package com.logisall.winus.wmsit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsit.service.WMSIT050Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT050Service")
public class WMSIT050ServiceImpl extends AbstractServiceImpl implements WMSIT050Service {
    
    @Resource(name = "WMSIT040Dao")
    private WMSIT040Dao dao;
    
    /**
     * Method ID : getOrdId
     * Method 설명 :  반품송장을 통한 주문내역 가져오기 
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getOrdId(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	//ORD_ID, ORD_ID_COUNT 보내기
        	Object result = dao.getOrdId(model);
            map.put("RESULT", result);
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    } 
    
    /**
     * Method ID : ordSelectbyInvcNo
     * Method 설명 : 반품송장번호를 통한 주문내역 확인
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> ordSelectbyInvcNo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {     
            System.out.println("vrSrchCustCd : ______________" + model.get("vrSrchCustCd"));
            map.put("LIST", dao.ordSelectbyInvcNo(model));  
            System.out.println(model);
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : getBoxNum
     * Method 설명 :  반품입고 검수 박스 넘버 가져오기 
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getBoxNum(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("BOXNUM", dao.getBoxNum(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    } 

    /**
     * Method ID : inOrdChkConfirm
     * Method 설명 : 입고검수내역 저장 
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> inOrdChkConfirm(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	String rstString = "";
    	try{
    		
    		int cnt = Integer.parseInt(model.get("cnt").toString());
    		System.out.println("model =======> "+model);
    		
    		if(cnt > 0){
    				
	    				String[] I_ORD_SEQ    = new String[cnt];
	    				String[] I_RITEM_ID  	= new String[cnt];
	    				String[] I_UOM_ID  = new String[cnt];
	    				String[] I_ORD_QTY 	= new String[cnt];
	    				String[] I_WORK_QTY 	= new String[cnt];
	    				
	    				// 배열 사이즈 체크 우측 그리드 row는 모두 돌리면서 배열 compCnt는 배열수에 맞게 처리..
	    				for(int j = 0; j < cnt; j ++){
	    						I_ORD_SEQ[j]    	= (String)model.get("I_ORD_SEQ"	+j);
		    					I_RITEM_ID[j]    = (String)model.get("I_RITEM_ID"+j);
		    					I_UOM_ID[j] 		= (String)model.get("I_UOM_ID"	+j);
		    					I_ORD_QTY[j] 	= (String)model.get("I_ORD_QTY"	+j);
		    					I_WORK_QTY[j] 	= (String)model.get("I_WORK_QTY"	+j);
	    				}
	    				//프로시져 담을것 
	    				Map<String, Object> modelIns = new HashMap<String, Object>();
	    				//ARRAY 형
	    				modelIns.put("I_ORD_SEQ"	, I_ORD_SEQ);
	    				modelIns.put("I_RITEM_ID"	, I_RITEM_ID);
	    				modelIns.put("I_UOM_ID", I_UOM_ID);
	    				modelIns.put("I_ORD_QTY"	, I_ORD_QTY);
	    				modelIns.put("I_WORK_QTY"	, I_WORK_QTY);
	    				
	    				//VARCHAR BOX
	    				modelIns.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	    				modelIns.put("I_CUST_ID"	, (String)model.get("I_CUST_ID"));
	    				modelIns.put("I_ORD_ID", (String)model.get("I_ORD_ID"));
	    				modelIns.put("I_LOC_CD", (String)model.get("I_LOC_CD"));
	    				modelIns.put("I_WORK_MEMO", (String)model.get("I_WORK_MEMO"));
	    				modelIns.put("I_CHK_BOX_NO", (String)model.get("I_CHK_BOX_NO_STRING"));
	    				
	    				modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	    				modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	    				
	    				
	    				//프로시저에 던지고 box count for문 계속.. 
		    			modelIns = (Map<String, Object>)dao.inOrdChkConfirm(modelIns);
		    			ServiceUtil.isValidReturnCode("WMSIT040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
		    			rstString = String.valueOf(modelIns.get("O_MSG_NAME"));
		    			
    			}
    		
    		m.put("errCnt", 0);
    		m.put("MSG", rstString);
    		
    	} catch(BizException be) {
    		System.out.println(" be.getMessage()  =====> " + be.getMessage());
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		
    		System.out.println(" e.getMessage()  =====> " + e.getMessage());
    		m.put("errCnt", -1);
    		m.put("MSG", e.getMessage() );
    		throw e;
    	}
    	return m;
    }    
    
    /**
     * Method ID : getBoxInfo
     * Method 설명 : 반품송장번호와 박스번호를 통한 검수내역 확인
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getBoxInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {     
            map.put("LIST", dao.getBoxInfo(model));  
            System.out.println(model);
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }  
    
    /**
     * Method ID : updateCkDel
     * Method 설명 : 입고검수내역 삭제 CK030.DEL_YN = 'Y'
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateCkDel(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
            try{
                dao.updateCkDel(model);
                m.put("MSG", "수정이 완료되었습니다");
            } catch(Exception e){
                throw e;
            }
        return m;
    }
    
    /**
     * Method ID : completeChkOrder
     * Method 설명 : 반품입고 적치 완료
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> completeChkOrder(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	String rstString = "";
    	try{
    		
    		int cnt = Integer.parseInt(model.get("cnt").toString());
    		System.out.println("model =======> "+model);
    		
    		if(cnt > 0){
    				
	    				String[] I_ORD_SEQ    = new String[cnt];
	    				
	    				// 배열 사이즈 체크 우측 그리드 row는 모두 돌리면서 배열 compCnt는 배열수에 맞게 처리..
	    				for(int j = 0; j < cnt; j ++){
	    						I_ORD_SEQ[j]    	= (String)model.get("I_ORD_SEQ"	+j);
	    				}
	    				//프로시져 담을것 
	    				Map<String, Object> modelIns = new HashMap<String, Object>();
	    				//ARRAY 형
	    				modelIns.put("I_ORD_SEQ"	, I_ORD_SEQ);
	    				
	    				//VARCHAR BOX
	    				modelIns.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	    				modelIns.put("I_CUST_ID"	, (String)model.get("I_CUST_ID"));
	    				modelIns.put("I_ORD_ID", (String)model.get("I_ORD_ID"));
	    		    	
	    				modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	    				modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	    				
	    				
	    				//프로시저에 던지고 box count for문 계속.. 
		    			modelIns = (Map<String, Object>)dao.completeChkOrder(modelIns);
		    			System.out.println("modelIns --> "+modelIns);
		    			
		    			ServiceUtil.isValidReturnCode("WMSIT040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
		    			
    			}
	    				
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(BizException be) {
    		System.out.println(" be.getMessage()  =====> " + be.getMessage());
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		
    		System.out.println(" e.getMessage()  =====> " + e.getMessage());
    		m.put("errCnt", -1);
    		m.put("MSG", e.getMessage() );
    		throw e;
    	}
    	return m;
    } 
    
    /**
     * Method ID : chkItemCd
     * Method 설명 :  반품입고 검수 박스 넘버 가져오기 
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> chkItemCd(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	//ORD_ID, ORD_ID_COUNT 보내기
        	Object result = dao.chkItemCd(model);
        	
        	System.out.println("TEST ==> "+result);
            map.put("RESULT", result);
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    } 
}
