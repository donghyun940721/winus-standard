package com.logisall.winus.wmsit.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT042Dao")
public class WMSIT042Dao extends SqlMapAbstractDAO {
    public GenericResultSet ord_list(Map<String, Object> model) {
        return executeQueryPageWq("wmsit042.ord_list", model);
    }
    
    public GenericResultSet chk_list(Map<String, Object> model) {
        return executeQueryPageWq("wmsit042.chk_list", model);
    }
    
	public List get_item_info(Map<String, Object> model) {
		List rstList = list("wmsit042.get_item_info", model);
		return rstList;
	}
	
	public List check_row(Map<String, Object> model) {
		List rstList = list("wmsit042.check_row", model);
		return rstList;
	}
	
    public Object orderIns(Map<String, Object> model){
    	executeUpdate("pk_wmsit001.sp_in_order_complete", model);
    	return model;
    }
	
    public Object delete(Map<String, Object> model) {
        return executeDelete("wmsit042.delete", model);
    }  
	
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsit042.insert", model);
    }
}

