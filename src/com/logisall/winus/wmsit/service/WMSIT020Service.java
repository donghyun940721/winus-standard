package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT020Service {
    public Map<String, Object> list_T1(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listSub_T1(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> workPickingComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> ordChkConfirm(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSingle(Map<String, Object> model) throws Exception;
    public Map<String, Object> listMulti(Map<String, Object> model) throws Exception;
    public Map<String, Object> poolBoxList(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT030_workPickingComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT030_workSerialComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT030_workInvcNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickigCompCancel(Map<String, Object> model) throws Exception;
}
