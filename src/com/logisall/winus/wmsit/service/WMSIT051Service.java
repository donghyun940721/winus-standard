package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT051Service { 
    public Map<String, Object> getItemInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> checkInComplete(Map<String, Object> model) throws Exception;
}
