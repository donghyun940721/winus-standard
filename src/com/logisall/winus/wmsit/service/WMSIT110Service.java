package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT110Service {
    public Map<String, Object> orderItemList(Map<String, Object> model) throws Exception;
    public Map<String,Object> getVideoID (Map<String, Object> model) throws Exception;
    public Map<String,Object> saveVideo (Map<String, Object> model) throws Exception;
    public Map<String,Object> packingItemList (Map<String, Object> model) throws Exception;
    public Map<String,Object> packingItemListEdiya (Map<String, Object> model) throws Exception;
    
    public Map<String,Object> packingComplete (Map<String, Object> model) throws Exception;
    public Map<String,Object> packingCompleteEdiya (Map<String, Object> model) throws Exception;
    public Map<String, Object> getVideoData(Map<String, Object> model) throws Exception;
    public Map<String, Object> getPackingHistory(Map<String, Object> model) throws Exception;
    public Map<String, Object> getEdiyaPackingHistoryResult(Map<String, Object> model) throws Exception;
    public Map<String, Object> getOrdIdByOrderNoEdiya(Map<String, Object> model) throws Exception;
}
