package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT050Service { 
    public Map<String, Object> ordSelectbyInvcNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> getBoxNum(Map<String, Object> model) throws Exception;
    public Map<String, Object> chkItemCd(Map<String, Object> model) throws Exception;
    public Map<String, Object> getOrdId(Map<String, Object> model) throws Exception;
    public Map<String, Object> inOrdChkConfirm(Map<String, Object> model) throws Exception;
    public Map<String, Object> getBoxInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateCkDel(Map<String, Object> model) throws Exception;
    public Map<String, Object> completeChkOrder(Map<String, Object> model) throws Exception;

}
