package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT032Service {
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> workPickingComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSingle(Map<String, Object> model) throws Exception;
    public Map<String, Object> listMulti(Map<String, Object> model) throws Exception;
    public Map<String, Object> poolBoxList(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT032_workPickingComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT032_workSerialComp(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT032_workInvcNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT032_workInvcNoUpdateMerge(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSIT032_workUpdateDelInvcNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickigCompBoxCancel(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickigCompMissingOrd(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickigCompParcelCancel(Map<String, Object> model) throws Exception;
}
