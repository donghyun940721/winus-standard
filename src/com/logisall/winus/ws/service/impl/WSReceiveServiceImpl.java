package com.logisall.winus.ws.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.ws.service.WSReceiveService;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

@Service("WSReceiveService")
public class WSReceiveServiceImpl implements WSReceiveService {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WSReceiveDao")
    private WSReceiveDao dao;
    
    
    
    public Map<String, Object> saveJSONStringToFile(Map<String, Object> model) throws Exception{
    	    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
		FileOutputStream fileOutputStream = null;
		OutputStreamWriter outputStreamWriter = null;
		BufferedWriter bufferedWriter = null;
		
		String path 		= (String)model.get(ConstantWSIF.KEY_JSON_FILE_PATH);
		String fileName 	= (String)model.get(ConstantWSIF.KEY_JSON_FILE_NAME);
		String jsonString 	= (String)model.get(ConstantWSIF.KEY_JSON_STRING);
		
		try {
			if (!CommonUtil.isNull(path)) {
				if (!FileUtil.existDirectory(path)) {
					FileHelper.createDirectorys(path);
				}
			}
			File jsonFile = new File(path, fileName);
			fileOutputStream = new FileOutputStream(jsonFile);
			outputStreamWriter = new OutputStreamWriter(fileOutputStream, ConstantWSIF.ENCODING_TYPE_JSON_FILE);			
			bufferedWriter = new BufferedWriter(outputStreamWriter);
			
			bufferedWriter.write(jsonString);
			bufferedWriter.flush();
			
			bufferedWriter.close();			
			fileOutputStream.close();
			fileOutputStream.close();
			
			model.put("D_SVC_NO", 			ConstantWSIF.WS_ADMIN_ID);
			model.put("D_IN_OUT_CLS", 		"I");
			model.put("D_DOC_TYPE", 		"JSON");
			model.put("D_DOC_ID", 			"TYPE_1");
			model.put("D_XML_FILE_PATH",	(String)model.get(ConstantWSIF.KEY_JSON_FILE_PATH_DIRECTORY));
			model.put("D_DOC_NAME", 		fileName);			
            model.put("D_TR_STAT",			ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_INSERT);
            model.put("D_ERR_CD",			"");
            model.put("D_MESSAGE",			"");
            model.put("INSERT_NO", 			"SYSTEM");
            
            String trId = (String)dao.insert(model);
            if (log.isInfoEnabled()) {
            	log.info("trId ######################################### :" + trId);
            }
            
            map.put(ConstantWSIF.KEY_TR_ID, trId);
			
		} catch (Exception e) {
			if (log.isInfoEnabled()) {
				log.info("Exception ######################################### :" + e.getMessage());
			}
            e.printStackTrace();
            map.put(ConstantWSIF.KEY_TR_ID, null);
            map.put("MSG", MessageResolver.getMessage("save.error", new String[] {}));
			
		} finally {
			// 자원 해제
			if (bufferedWriter != null) {
				try {
					bufferedWriter.close();
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

			if (outputStreamWriter != null) {
				try {
					outputStreamWriter.close();
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

		}        
        return map;
        
    } 
    /**
     * 
     * Method ID   : insertRetry
     * Method 설명 : 송수신문서 재처리
     * 작성자      :  기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception {
    	
        Map<String, Object> map = new HashMap<String, Object>();
                
        try {
            model.put("D_TR_STAT","R");
            model.put("D_ERR_CD","");
            model.put("D_MESSAGE","");
            model.put("INSERT_NO", "SYSTEM");
            dao.insert(model);
            
            /*
	            Map<String, Object> modelDt = new HashMap<String, Object>();
	            modelDt.put("SVC_NO", model.get(ConstantIF.SS_SVC_NO));
	            modelDt.put("TR_ID", model.get("D_TR_ID"));
	            modelDt.put("TR_STAT", "RS");
	            
	            dao.updateTrStat(modelDt);
            */
            
            map.put("MSG", MessageResolver.getMessage("complete"));
        } catch (Exception e) {
            e.printStackTrace();
            map.put("MSG", MessageResolver.getMessage("save.error", new String[] {}));
        }        
        // log.info(map);
        
        return map;
        
    }
    
    public Map<String, Object> updateState(Map<String, Object> model) throws Exception {
    	
        Map<String, Object> map = new HashMap<String, Object>();                
        try {	    
			model.put("SVC_NO", 			ConstantWSIF.WS_ADMIN_ID);			
        	dao.updateTrStat(model);        	
            map.put("MSG", MessageResolver.getMessage("complete"));
            
        } catch (Exception e) {
            e.printStackTrace();
            map.put("MSG", MessageResolver.getMessage("save.error", new String[] {}));
        }
        return map;
        
    }    
    
    
    public Map<String, Object> listTestList(Map<String, Object> model) throws Exception {
    	
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
        	map.put("LIST", dao.listTestList(model));
        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    	
    }
}
