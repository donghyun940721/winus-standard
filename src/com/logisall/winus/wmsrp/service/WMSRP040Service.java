package com.logisall.winus.wmsrp.service;

import java.util.Map;



public interface WMSRP040Service {
    public Map<String, Object> updateOut(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateOutOz(Map<String, Object> model) throws Exception;
}
