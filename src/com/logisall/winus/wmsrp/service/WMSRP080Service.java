package com.logisall.winus.wmsrp.service;

import java.util.Map;



public interface WMSRP080Service {
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception;
}
