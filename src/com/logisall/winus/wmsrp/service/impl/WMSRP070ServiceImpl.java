package com.logisall.winus.wmsrp.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsrp.service.WMSRP070Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSRP070Service")
public class WMSRP070ServiceImpl extends AbstractServiceImpl implements WMSRP070Service {
    
    @Resource(name = "WMSRP070Dao")
    private WMSRP070Dao dao;

    /**
     * 
     * Method ID   : newBarcodePa
     * Method 설명    : 바코드생성
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateBarcodePa(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] ordId  = new String[tmpCnt];                
            String[] ordSeq = new String[tmpCnt];         
            String[] inOrdQty  = new String[tmpCnt];   

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i]    = (String)model.get("ORD_ID"+i);               
                ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                inOrdQty[i] = (String)model.get("IN_ORD_QTY"+i);  
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);
            modelIns.put("inOrdQty", inOrdQty);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updateBarcodePa(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP070", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
                        
            m.put("BarCode", modelIns.get("O_CUR"));
            m.put("MSG", MessageResolver.getMessage("list.success"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            throw e;
        }
        return m;
    }
}
