package com.logisall.winus.wmsrp.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.PDF.model.DocPickingListHeaderVO;
import com.logisall.winus.frm.common.PDF.model.DocRptGRNWorkOrderListDetailVO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSRP020Dao")
public class WMSRP020Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

     /**
     * Method ID    : grnSearch
     * Method 설명      : 작업지시서(입하)발행 프로시져
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object updateGrn(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_print_grn", model);
        return model;
    }
    
    /**
     * Method ID    : selectGrnHeader
     * Method 설명      : 작업지시서(입하)발행 헤더
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public DocPickingListHeaderVO selectGrnHeader(Map<String, Object> model) throws Exception{
        return (DocPickingListHeaderVO)getSqlMapClientTemplate().queryForObject("wmsop010.selectGrnHeader", model);
    }
    
    /**
     * Method ID    : selectGrnDetail
     * Method 설명      : 작업지시서(입하)발행 상세
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public DocRptGRNWorkOrderListDetailVO[] selectGrnDetail(Map<String, Object> model) throws Exception{
        return (DocRptGRNWorkOrderListDetailVO[])getSqlMapClientTemplate().queryForList("wmsop010.selectGrnDetail", model).toArray(new DocRptGRNWorkOrderListDetailVO[0]);
    }
    
    
    
    
    
    
    public Map<String, Object> selectGrnHeaderMap(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsop010.selectGrnHeaderMap", model);
    }    
    
    public List<Map<String, Object>> selectGrnDetailList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("wmsop010.selectGrnDetailList", model);
    }
    
    /**
     * Method ID    : selectGrnListCSV
     * Method 설명      : 작업지시서(입하)발행 CSV
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet selectGrnListCSV(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.selectGrnListCSV", model);
    }
    
    public DocPickingListHeaderVO selectCycleStockHeader(Map<String, Object> model) throws Exception{
        return (DocPickingListHeaderVO)getSqlMapClientTemplate().queryForObject("wmsop010.selectGrnHeader", model);
    }
    
    public DocPickingListHeaderVO selectCycleStockDetail(Map<String, Object> model) throws Exception{
        return (DocPickingListHeaderVO)getSqlMapClientTemplate().queryForObject("wmsop010.selectGrnHeader", model);
    }
    
}
