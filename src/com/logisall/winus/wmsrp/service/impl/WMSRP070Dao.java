package com.logisall.winus.wmsrp.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSRP070Dao")
public class WMSRP070Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID    : newBarcodePa
     * Method 설명      : 바코드생성
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object updateBarcodePa(Map<String, Object> model){
        executeUpdate("wmspd020.pk_wmspd020.sp_new_barcode_pa", model);
        return model;
    }
}
