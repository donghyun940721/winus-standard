package com.logisall.winus.wmsom.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsom.service.WMSOM091Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM091Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM091Service")
    private WMSOM091Service service;
    
    /**
     * Method ID    : wmsom091
     * Method 설명      : 거래처발주
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM091.action")
    public ModelAndView wmsom091(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM091", service.selectItemGrp(model));
    }
	
    /**
     * Method ID    : list
     * Method 설명      : 메인 거래처 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM091/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
     * Method ID : listItemTabs1
     * Method 설명 : 매출거래처관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSOM091/listItemTabs1.action")
    public ModelAndView listItemTabs1(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listItemTabs1(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }
    
    /*-
	 * Method ID    : saveSimpleOutOrderTemp
	 * Method 설명      : 거래처발주 Temp 입력
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOM091/saveSimpleOutOrderTemp.action")
	public ModelAndView saveSimpleOutOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSimpleOutOrderTemp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : ReqOrdSeq
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOM091/ReqOrdSeq.action")
	public ModelAndView ReqOrdSeq(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.ReqOrdSeq(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID : listOrdDetail
     * Method 설명 : 매출거래처관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSOM091/listOrdDetail.action")
    public ModelAndView listOrdDetail(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listOrdDetail(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }
}
