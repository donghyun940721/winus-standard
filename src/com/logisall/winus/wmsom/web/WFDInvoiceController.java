package com.logisall.winus.wmsom.web;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.wmsom.service.WFDInvoiceService;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

/**
 * 거래명세서(롯데웰푸드)
 */
@Controller
public class WFDInvoiceController {

	private final Log log = LogFactory.getLog(this.getClass());

	@Autowired
	private WFDInvoiceService wfdInvoiceService;

	@RequestMapping("/WINUS/WFDInvoice.action")
	public ModelAndView WMSIF902(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/wellfood/WFDInvoice");
	}
	
	@RequestMapping("/INVOICE/selectOrderList.action")
	public ResponseEntity<Map<String, Object>> selectOrderList(Map<String, Object> model) throws Exception {
		Map<String, Object> resultMap = wfdInvoiceService.selectOrderList(model);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/INVOICE/selectOrderDetailList.action")
	public ResponseEntity<Map<String, Object>> selectOrderDetailList(Map<String, Object> model) throws Exception {
		Map<String, Object> resultMap = wfdInvoiceService.selectOrderDetailList(model);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC001/updatePrintCount.action")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> updatePrintCount(Map<String, Object> model, @RequestBody Map<String, Object> reqMap) throws Exception {
		Map<String, Object> resultMap = wfdInvoiceService.updatePrintCount(model, reqMap);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
}
