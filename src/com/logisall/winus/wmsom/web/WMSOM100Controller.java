package com.logisall.winus.wmsom.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsom.service.WMSOM100Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOM100Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM100Service")
    private WMSOM100Service WMSOM100Service;
    
    @Resource(name = "WMSOP030Service")
	private WMSOP030Service WMSOP030Service;
    
    /**
     * Method ID    		: WMSOM100
     * Method 설명      	: 임가공주문관리
     * 작성자                : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM100.action")
    public ModelAndView WMSOM100(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM100", WMSOM100Service.getUomList(model));
    }
    
    /**
     * Method ID    		: WMSOM100_V2
     * Method 설명      	: 임가공주문관리V2
     * 작성자                : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM100_V2.action")
    public ModelAndView WMSOM100_V2(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM100_V2/WMSOM100_V2", WMSOM100Service.getUomList(model));
    }
    
    /**
     * Method ID    		: WMSOM100Q1
     * Method 설명      	: 임가공주문관리(공통)
     * 작성자                : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM100Q1.action")
    public ModelAndView WMSOM100Q1(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM100Q1");
    }
    
    /**
     * Method ID    		: WMSOM100Q2
     * Method 설명      	: 임가공주문관리(거래처별)
     * 작성자                : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM100Q2.action")
    public ModelAndView WMSOM100Q1LT(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM100Q2", WMSOM100Service.getTransCustList(model));
    }
    
    /**
     * Method ID   		 : listE1
     * Method 설명      	 : 임가공주문관리 조회
     * 작성자                 : kSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM100/listE1.action")
    public ModelAndView listE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", WMSOM100Service.listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID   		 : listE2_H
     * Method 설명      	 : 임가공주문관리_tab2_header
     * 작성자                 : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM100/listE2_H.action")
    public ModelAndView listE2_H(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", WMSOM100Service.listE2_H(model));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method 설명      	 : 임가공주문관리 V2_tab2_header
     */
    @RequestMapping("/WMSOM100/listE2_H_V2.action")
    public ModelAndView listE2_H_V2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            /**
             * WMSOM000과 WMSMS091 테이블 비교 후 등록안된 품목리스트 리턴
             * */
            mav = new ModelAndView("jqGridJsonView", WMSOM100Service.listE2_H_V2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    /**
     * Method ID   		 : listE3_H
     * Method 설명      	 : 임가공주문관리_tab3_header
     * 작성자                 : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM100/listE3_H.action")
    public ModelAndView listE3_H(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", WMSOM100Service.listE3_H(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID   		 : listE3_D
     * Method 설명      	 : 임가공주문관리_tab3_detail
     * 작성자                 : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM100/listE3_D.action")
    public ModelAndView listE2_D(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", WMSOM100Service.listE3_D(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID   		 : saveE2
     * Method 설명      	 : 임가공주문관리_tab2_ 1, 세트상품 저장 ->  2. 부속상품 저장
     * 작성자                 : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSOM100/saveE2.action")
	public ModelAndView saveE2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = WMSOM100Service.saveE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	  /**
     * Method ID   		 : saveE2
     * Method 설명       : 임가공주문관리_tab2_ 1, 세트상품 삭제
     * 작성자            : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSOM100/deleteE2.action")
	public ModelAndView deleteE2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = WMSOM100Service.deleteE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
    /**
     * Method ID   		 : saveE3
     * Method 설명      	 : 임가공주문관리_tab3_ 부속상품 수정
     * 작성자                 : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSOM100/saveE3.action")
	public ModelAndView saveE3(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = WMSOM100Service.saveE3(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID   		 : deleteE3_H
     * Method 설명       : 임가공주문관리_tab3_ 세트상품 삭제
     * 작성자            : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSOM100/deleteE3_H.action")
	public ModelAndView deleteE3_H(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = WMSOM100Service.deleteE3_H(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID   		 : saveE3
     * Method 설명      	 : 임가공주문관리_tab3_ 부속상품 삭제
     * 작성자                 : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSOM100/deleteE3.action")
	public ModelAndView deleteE3(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = WMSOM100Service.deleteE3(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID   		 : deleteYnOm
     * Method 설명      	 : 임가공주문관리 OM000 삭제
     * 작성자                 : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSOM100/deleteYnOm.action")
	public ModelAndView deleteYnOm(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = WMSOM100Service.deleteYnOm(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	 * 임가공 주문 동기화 V2
	 * */
	@RequestMapping("/WMSOM100/saveSyncOrderJavaB2C_V2.action")
	@ResponseBody
	public ModelAndView saveSyncOrderJavaB2C_V2(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
		    HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = WMSOM100Service.saveSyncOrderJavaB2C_V2(model, reqMap);
		
		}catch (BizException be) {
		    	m.put("MSG",  be.getMessage());
			m.put("errCnt", "1");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}finally {
		    mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  	 : inExcelFileUploadJavaB2C
	 * Method 설명 	 : Excel 파일 읽기 (B2C)
	 * 작성자             : KSJ
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOM100/saveSyncOrderJavaB2C.action")
	public ModelAndView saveSyncOrderJavaB2C(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			
			m = WMSOM100Service.saveSyncOrderJavaB2C(model);
			
			//완료후 결과 리턴.
			mav = new ModelAndView("jsonView", m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
    /*-
	 * Method ID  	 : inExcelFileUploadJavaB2C
	 * Method 설명 	 : Excel 파일 읽기 (B2C)
	 * 작성자             : KSJ
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOM100Q1/inExcelUploadTemplateB2C.action")
	public ModelAndView inExcelFileUploadJavaB2C(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			//STEP 1. EXCEL 인코딩 셋팅.
			request.setCharacterEncoding(ConstantIF.PROPERTY_FILE_ENCODING);		//'utf-8' String Const.

			//STEP 1-1. 파일을 이름으로 받아서 임시저장 하기 위한 Directory 셋팅.
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			//STEP 1-1. Directory 체크 -> 없으면 생성.
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			//STEP 1-2. 셋팅한 경로로 Path 셋팅후 파일 저장(FileCopyUtils 사용)
			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = WMSOM100Service.getTemplate(model);
			
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			//list = ExcelReader.excelReadRowOM(excelTemplate, destination);
			list = ExcelReader.excelReadHandlerByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		    , model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		    , model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	    , model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	    , model.get("SS_USER_NO"));
			mapBody.put("vrCustCd"		    , model.get("vrCustCd"));
			mapBody.put("vrCustId"		    , model.get("vrCustId"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			mapBody.put("vrCustSeq"			,model.get("vrCustSeq"));
			mapBody.put("vrCustSeqNm"		,model.get("vrCustSeqNm"));
			
			mapBody.put("vrSrchOrdDegree"	,model.get("vrSrchOrdDegree"));
			mapBody.put("vrSrchTransCustCd"	,model.get("vrSrchTransCustCd"));
			mapBody.put("vrSrchTransCustNm"	,model.get("vrSrchTransCustNm"));
			model.put("templateId", "0000000003");
						
			
			/* DB 컬럼 추출 */
			List<Map<String, Object>> mapHeader = new ArrayList();
			mapHeader = WMSOM100Service.getResult(model);
			
			//STEP 4-2. Run Query.
			// m = service.saveExcelOrderJavaCust(mapBody, mapHeader);
			m = WMSOM100Service.saveExcelOrderJavaB2C(mapBody, mapHeader);
			
			//FINISH. 모든 스텝 완료후 임시저장했던 파일 삭제.
			if (destination.exists()) {
				destination.delete();
			}
			//완료후 결과 리턴.
			mav = new ModelAndView("jsonView", m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  	 : inExcelFileUploadJavaB2CTrans
	 * Method 설명 	 : Excel 파일 읽기 (B2C) - 거래처별
	 * 작성자             : KSJ
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOM100Q2/inExcelUploadTemplateB2CTrans.action")
	public ModelAndView inExcelFileUploadJavaB2CTrans(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			//STEP 1. EXCEL 인코딩 셋팅.
			request.setCharacterEncoding(ConstantIF.PROPERTY_FILE_ENCODING);		//'utf-8' String Const.

			//STEP 1-1. 파일을 이름으로 받아서 임시저장 하기 위한 Directory 셋팅.
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			//STEP 1-1. Directory 체크 -> 없으면 생성.
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			//STEP 1-2. 셋팅한 경로로 Path 셋팅후 파일 저장(FileCopyUtils 사용)
			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = WMSOM100Service.getTemplate(model);
			
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			//list = ExcelReader.excelReadRowOM(excelTemplate, destination);
			list = ExcelReader.excelReadHandlerByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		    , model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		    , model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	    , model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	    , model.get("SS_USER_NO"));
			mapBody.put("vrCustCd"		    , model.get("vrCustCd"));
			mapBody.put("vrCustId"		    , model.get("vrCustId"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			mapBody.put("vrCustSeq"			,model.get("vrCustSeq"));
			mapBody.put("vrCustSeqNm"		,model.get("vrCustSeqNm"));

			mapBody.put("vrSrchOrdDegree"	,model.get("vrSrchOrdDegree"));
			mapBody.put("vrSrchTransCustCd"	,model.get("vrSrchTransCustCd"));
			mapBody.put("vrSrchTransCustNm"	,model.get("vrSrchTransCustNm"));
			model.put("templateId", "0000000003");
						
			
			/* DB 컬럼 추출 */
			List<Map<String, Object>> mapHeader = new ArrayList();
			mapHeader = WMSOM100Service.getResult(model);
			
			//STEP 4-2. Run Query.
			// m = service.saveExcelOrderJavaCust(mapBody, mapHeader);
			m = WMSOM100Service.saveExcelOrderJavaB2CTrans(mapBody, mapHeader);
			
			//FINISH. 모든 스텝 완료후 임시저장했던 파일 삭제.
			if (destination.exists()) {
				destination.delete();
			}
			//완료후 결과 리턴.
			mav = new ModelAndView("jsonView", m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
}
