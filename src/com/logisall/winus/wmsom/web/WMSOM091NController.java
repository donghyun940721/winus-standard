package com.logisall.winus.wmsom.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsom.service.WMSOM091NService;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM091NController {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM091NService")
    private WMSOM091NService service;
    
    /**
     * Method ID    : wmsom091n
     * Method 설명      : 거래처발주
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM091N.action")
    public ModelAndView wmsom091n(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM091N", service.selectItemGrp(model));
    }
	
    /**
     * Method ID    : list
     * Method 설명      : 메인 거래처 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM091N/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
     * Method ID : listItemTabs1
     * Method 설명 : 매출거래처관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSOM091N/listItemTabs1.action")
    public ModelAndView listItemTabs1(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listItemTabs1(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }
    
    /*-
	 * Method ID    : saveSimpleOutOrderTemp
	 * Method 설명      : 거래처발주 Temp 입력
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOM091N/saveSimpleOutOrderTemp.action")
	public ModelAndView saveSimpleOutOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSimpleOutOrderTemp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
     * Method ID : listOrdDetail
     * Method 설명 : 매출거래처관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSOM091N/listOrdDetail.action")
    public ModelAndView listOrdDetail(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listOrdDetail(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }

	/*-
	 * Method ID    : wmscm091N1
	 * Method 설명      : 상품조회 POP 화면(N)
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091N1.action")
	public ModelAndView wmscm091N1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM091N1", service.selectData(model));
	}    
    
}
