package com.logisall.winus.wmsom.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.wmsom.service.WMSOM020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM020Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM020Service")
    private WMSOM020Service service;
    
    /**
     * Method ID	: wmsom120
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM120.action")
    public ModelAndView wmsom120(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM120");
    }
    @RequestMapping("/WINUS/WMSOM121.action")
    public ModelAndView wmsom121(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM121");
    }
    
    /**
     * Method ID	: wmsom010
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM020.action")
    public ModelAndView wmsom010(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM020");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/listDetail.action")
    public ModelAndView listDetail(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
   
    
    /*-
	 * Method ID    : /WMSOM020pop.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM020pop.action")
	public ModelAndView wmsom010pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM020pop");
	}
	
	/*-
	 * Method ID    : /WMSOM020pop2.action
	 * Method 설명      : 
	 * 작성자                 : khKim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM020pop2.action")
	public ModelAndView wmsom010pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM020pop2");
	}
	
    /**
     * Method ID	: confirmOrder
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/confirmOrder.action")
    public ModelAndView confirmOrder(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.confirmOrder(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: confirmOrder
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/saveInOrder.action")
    public ModelAndView saveInOrder(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.saveInOrder(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    /**
     * Method ID	: saveInOrderPLT 
     * Method 설명	: PLT 환산 수량만큼 주문 확정 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/saveInOrderPLT.action")
    public ModelAndView saveInOrderPLT(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInOrderPLT(model);
			//mav = new ModelAndView("jsonView", service.saveInOrderPLT(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
    }
    
    /**
     * Method ID       : saveInOrderNewtong
     * Method 설명      : 뉴통 로케이션, 유통기한, 제조일자등 정보 등록후 주문등록
     * 작성자            : yangjiwoon
     * @param   model
     * @return
     */
    @RequestMapping("/WMSOM020/saveInOrderNewtong.action")
    public ModelAndView saveInOrderNewtong(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInOrderNewtong(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
    }
    
    
    /**
     * Method ID   : WMSOMPOP5
     * Method 설명  : 뉴통팝업
     * 작성자        : yangjiwoon
     * @param model
     * @return
     */
	@RequestMapping("/WMSOM020/WMSOMPOP5.action")
	public ModelAndView WMSOMPOP5(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM120pop5");
	}
	
	
	/**
     * Method ID   : selectNewtongOrd
     * Method 설명  : 뉴통팝업주문 조회
     * 작성자        : yangjiwoon
     * @param model
     * @return
     */
	@RequestMapping("/WMSOM020/searchWMSOMPOP5.action")
	public ModelAndView searchWMSOMPOP5(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectNewtongOrd(model));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
    
    
    @RequestMapping("/WMSOM021/saveInOrderVX.action")
    public ModelAndView saveInOrderVX(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.saveInOrderVX(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSOM021/saveInOrderVX_V2.action")
    public ModelAndView saveInOrderVX_V2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jsonView", service.saveInOrderVX_V2(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/listNew.action")
    public ModelAndView listNew(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listNew(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM121/listNew2.action")
    public ModelAndView listNew2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listNew2(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /*
    * Method ID	: wmsom120pop
    * Method 설명	: 
    * 작성자			: KHKIM
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSOM120pop.action")
   public ModelAndView wmsom120pop(Map<String, Object> model) throws Exception {
   	return new ModelAndView("winus/wmsom/WMSOM120pop");
   }
   
   @RequestMapping("/WMSOM120pop2.action")
   public ModelAndView WMSOM120pop2(Map<String, Object> model) throws Exception {
   	return new ModelAndView("winus/wmsom/WMSOM120pop2");
   }
   
   @RequestMapping("/WMSOM120pop3.action")
   public ModelAndView WMSOM120pop3(Map<String, Object> model) throws Exception {
   	return new ModelAndView("winus/wmsom/WMSOM120pop3");
   }
   
   /**
    * Method ID	: confirmOrder
    * Method 설명	: 
    * 작성자			: khkim
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSOM020/deleteInOrder.action")
   public ModelAndView deleteInOrder(Map<String, Object> model) throws Exception {
   	ModelAndView mav = null;
       
       try {
           mav = new ModelAndView("jsonView", service.deleteInOrder(model));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return mav;
   }
   
   /**
    * 주문관리 > 출고주문관리 주문 삭제
    * 미등록 상태만 삭제 가능
    * */
   @RequestMapping("/WMSOM020/deleteOutOrder.action")
   @ResponseBody
   public ModelAndView deleteOutOrder(Map<String, Object> model, @RequestBody Map<String, Object> reqMap) throws Exception {
   	ModelAndView mav = null;
       
       try {
           mav = new ModelAndView("jsonView", service.deleteOutOrder(model, reqMap));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return mav;
   }
   
   
   @RequestMapping("/WMSOM020/saveCustLotNo.action")
	public ModelAndView outPickingCancel(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCustLotNo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
	}
   
   @RequestMapping("/WMSOM020/saveCustLotNoUpdate.action")
	public ModelAndView saveCustLotNoUpdate(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCustLotNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOM120E2.action")
	public ModelAndView wmsom120e2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("winus/wmsom/WMSOM120E2");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOM120E3.action")
	public ModelAndView wmsom120e3(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("winus/wmsom/WMSOM120E3");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOM020/changOrderList.action")
	public ModelAndView changOrderList(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.changOrderList(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
	}
   
   
	@RequestMapping("/WMSOM020/saveInOrderOm.action")
	public ModelAndView saveInOrderOm(Map<String, Object> model){ 
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try{
			
			m = service.saveInOrderOm(model);
			
		}catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		
		return mav;
	}
	
	
	/*-
	 * Method ID    :WMSOM120pop4
	 * Method 설명      : 
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM020/WMSOM120pop4.action")
	public ModelAndView wmssp010t1_11(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM120pop4");
	}
	
	
	
	/*-
	 * Method ID    : omImgList
	 * Method 설명      : 
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM020/omImgList.action")
	public ModelAndView listT1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.omImgList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
