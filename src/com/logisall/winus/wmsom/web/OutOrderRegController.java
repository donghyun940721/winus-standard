package com.logisall.winus.wmsom.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.logisall.winus.common.service.CmBeanService;
import com.logisall.winus.wmscm.service.WMSCM011Service;
import com.logisall.winus.wmsms.service.WMSMS100Service;
import com.logisall.winus.wmsom.service.OutOrderRegService;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class OutOrderRegController {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
    private OutOrderRegService service;
	@Autowired
	private WMSMS100Service uomService;
	@Autowired
	private WMSCM011Service transCustService;
	@Autowired
    private CmBeanService mngCodeService;
	
	/**
     * Method ID	: OutOrderRegView
     * Method 설명	: 주문관리/출고주문등록V2
     * 작성자			: schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM/OutOrderReg.action")
    public ModelAndView OutOrderRegView(Map<String, Object> model) throws Exception{
		ModelAndView mv = new ModelAndView();
		String warehouseId = (String)model.get("SS_SVC_NO");
		model.put("srchKey", "CUST");
		Map<String, Object> custListMap = mngCodeService.selectMngCode(model);
		mv.addObject("custList", new Gson().toJson(custListMap)); //화주목록
		mv.addObject("uomList", new Gson().toJson(uomService.list(model))); //UOM 목록
		model.put("S_LC_ALL", "LC_ALL");
		model.put("S_LC_ID", warehouseId);
		model.put("rows", "5000");
//		mv.addObject("transCustList", new Gson().toJson(transCustService.list(model))); //거래처 목록
		mv.setViewName("winus/wmsom/outorderreg/WMSOM050_V2");
		return mv;
    }
    
    
    @RequestMapping("/WMSOM/outOrderCheck.action")
    @ResponseBody
	public ModelAndView outOrderCheck(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = service.outOrderCheck(model, reqMap);

		mav.addAllObjects(m);
		return mav;
	}
    
    /**
     * OMS 출고주문 주문등록
     * */
    @RequestMapping("/WMSOM/saveOutOrder.action")
    @ResponseBody
	public ModelAndView saveOutOrder(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = service.saveOutOrder(model, reqMap);

		mav.addAllObjects(m);
		return mav;
	}
}
