package com.logisall.winus.wmsom.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsom.service.WMSOM150Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM150Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSOM150Service")
    private WMSOM150Service service;
	
	/**
     * Method ID	: wmsom150
     * Method 설명	: 주문관리/출고주문관리v2(조회전용)
     * 작성자			: atomyoun
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM150.action")
    public ModelAndView wmsom150(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM150");
    }
    
    /**
     * Method ID	: wmsom150E2
     * Method 설명	: 출고LOT수정
     * 작성자			: atomyoun
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM150E2.action")
    public ModelAndView wmsom150E2(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM150E2");
    }
    
    /**
     * Method ID	: wmsom150E3
     * Method 설명	: 출고정보수정
     * 작성자			: atomyoun                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM150E3.action")
    public ModelAndView wmsom150E3(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM150E3");
    }
    
    /**
     * Method ID	: outList
     * Method 설명	: 출고주문관리(OMS) 화면 리스트
     * 작성자			: atomyoun
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM150/list.action")
    public ModelAndView outList(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.list(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /**
     * Method ID	: saveOutOrder
     * Method 설명	: 출고확정
     * 작성자			: atomyoun
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM150/saveOutOrder.action")
    public ModelAndView saveOutOrder(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jsonView", service.saveOutOrder(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
}
