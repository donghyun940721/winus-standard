package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface WFDInvoiceService {

	Map<String, Object> selectOrderList(Map<String, Object> model);

	Map<String, Object> selectOrderDetailList(Map<String, Object> model);

	Map<String, Object> updatePrintCount(Map<String, Object> model, Map<String, Object> reqMap);

}
