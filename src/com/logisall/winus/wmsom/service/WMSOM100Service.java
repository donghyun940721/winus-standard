package com.logisall.winus.wmsom.service;

import java.util.List;
import java.util.Map;

public interface WMSOM100Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2_H(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3_H(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3_D(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> getSyncList(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE3(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> deleteE2(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> deleteE3_H(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteE3(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> deleteYnOm(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> getUomList(Map<String, Object> model)  throws Exception;
    public List<Map<String, Object>> getTemplate(Map<String, Object> model)  throws Exception;
    public List<Map<String, Object>> getResult(Map<String, Object> model) throws Exception;
    public Map<String, Object> getTransCustList(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveSyncOrderJavaB2C(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSyncOrderJavaB2C_V2(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    
    public Map<String, Object> saveExcelOrderJavaB2C(Map<String, Object> model, List<Map<String, Object>> model2) throws Exception;
    public Map<String, Object> saveExcelOrderJavaB2CTrans(Map<String, Object> model, List<Map<String, Object>> model2) throws Exception;
 
    public Map<String, Object> listE2_H_V2(Map<String, Object> model) throws Exception;
}

