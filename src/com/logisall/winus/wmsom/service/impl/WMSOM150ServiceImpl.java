package com.logisall.winus.wmsom.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsom.service.WMSOM150Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOM150Service")
public class WMSOM150ServiceImpl implements WMSOM150Service{
	
	@Resource(name = "WMSOM150Dao")
    private WMSOM150Dao dao;
	
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
	}
	/**
     * 분할전
     * 대체 Method ID   : saveOutOrder
     * 대체 Method 설명    : 출고확정
     * 작성자                      : atomyoun
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
           
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
            	
	            for(int i = 0 ; i < insCnt ; i ++){
	                //저장, 수정
	                String[] ordSeq         = new String[insCnt];         
	                String[] ritemId        = new String[insCnt];     
	                String[] custLotNo      = new String[insCnt];     
	                String[] realInQty      = new String[insCnt];     
	                
	                String[] realOutQty     = new String[insCnt];                     
	                String[] makeDt         = new String[insCnt];         
	                String[] timePeriodDay  = new String[insCnt];     
	                String[] locYn          = new String[insCnt];     
	                String[] pdaCd          = new String[insCnt];     
	                
	                String[] workYn         = new String[insCnt];                
	                String[] rjType         = new String[insCnt];         
	                String[] realPltQty     = new String[insCnt];     
	                String[] realBoxQty     = new String[insCnt];     
	                String[] confYn         = new String[insCnt];     
	                
	                String[] unitAmt        = new String[insCnt];                
	                String[] amt            = new String[insCnt];         
	                String[] eaCapa         = new String[insCnt];     
	                String[] boxBarcode     = new String[insCnt];     
	                String[] inOrdUomId     = new String[insCnt];     
	                
	                String[] inWorkUomId 	= new String[insCnt]; 
	                String[] outOrdUomId    = new String[insCnt];       
	                String[] outWorkUomId	= new String[insCnt];
	                
	                String[] minUomId		= new String[insCnt];
	                
	                String[] inOrdQty       = new String[insCnt];
	                String[] inWorkOrdQty   = new String[insCnt];
	                
	                String[] outOrdQty      = new String[insCnt];
	                String[] outWorkOrdQty  = new String[insCnt];
	                String[] inDspQty  = new String[insCnt];
	                String[] outDspQty  = new String[insCnt];
	                
	                String[] refSubLotId    = new String[insCnt];
	                
	                String[] outOrdWeight      = new String[insCnt];
	                
	                String[] cntrId         = new String[insCnt];         
	                String[] cntrNo         = new String[insCnt];     
	                String[] cntrType       = new String[insCnt];
	                String[] cntrSealNo       = new String[insCnt];
	                
	                String[] ordDesc        = new String[insCnt];
	                String[] etc2           = new String[insCnt];
	                
	                //추가
	                String[] itemBestDate     = new String[insCnt];   //상품유효기간     
	                String[] itemBestDateEnd  = new String[insCnt];   //상품유효기간만료일	                
	                
	                ordSeq[0]           = null;        
	                ritemId[0]          = (String)model.get("I_RITEM_ID"+i);      
	                custLotNo[0]        = (String)model.get("I_CUST_LOT_NO"+i);      
	                realInQty[0]        = null;      
	                
	                realOutQty[0]       = null;                      
	                makeDt[0]           = (String)model.get("I_MAKE_DATE"+i);          
	                timePeriodDay[0]    = null;      
	                locYn[0]            = null;      
	                pdaCd[0]            = null;      
	                
	                workYn[0]           = null;                 
	                rjType[0]           = null;          
	                realPltQty[0]       = null;      
	                realBoxQty[0]       = null;      
	                confYn[0]           = null;      
	                
	                unitAmt[0]          = null;                 
	                amt[0]              = null;          
	                eaCapa[0]           = null;      
	                boxBarcode[0]       = null;      
	                inOrdUomId[0]       = null; 
	                inWorkUomId[0]      = null;
	                outOrdUomId[0]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);                 
	                outWorkUomId[0]     = (String)model.get("I_OUT_WORK_UOM_ID"+i);
	                
	                minUomId[0]       = null;
	                
	                inOrdQty[0]         = null;          
	                inWorkOrdQty[0]     = null;
	                outOrdQty[0]        = (String)model.get("I_OUT_ORD_QTY"+i);     
	                outWorkOrdQty[0]    = (String)model.get("I_OUT_WORK_ORD_QTY"+i);
	                inDspQty[0]			= null;
	                outDspQty[0]		= null;
	                refSubLotId[0]      = null; 
	                
	                outOrdWeight[0]     = null;      
	                                 
	                cntrId[0]           = null;          
	                cntrNo[0]           = null;      
	                cntrType[0]         = null;
	                cntrSealNo[0]         = null;
	                
	                ordDesc[0]          = (String)model.get("I_ORD_DESC"+i);
	                etc2[0]             = (String)model.get("I_ETC2"+i);
	                
	                itemBestDate[0]     = (String)model.get("I_ITEM_BEST_DATE"+i);
	                itemBestDateEnd[0]  = (String)model.get("I_VALID_DT"+i);
	                

	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                //main
	                modelIns.put("vrOrdId"          , null);
	                modelIns.put("inReqDt"          , null);
	                modelIns.put("inDt"             , null);
	                modelIns.put("custPoid"         , null);
	                
	                modelIns.put("custPoseq"        , null);
	                modelIns.put("orgOrdId"         , (String)model.get("I_OUT_ORG_ORD_ID"+i)); //ORG_ORD_NO
	                modelIns.put("orgOrdSeq"        , (String)model.get("I_OUT_ORD_DETAIL_NO"+i)); //ORD_DETAIL_NO
	                modelIns.put("inWhId"           , null);
	                modelIns.put("outWhId"          , null);
	                
	                modelIns.put("transCustId"      , null);
	                modelIns.put("vrCustId"         , (String)model.get("vrSrchCustId")); //"0000295694"
	                modelIns.put("pdaFinishYn"      , null);
	                modelIns.put("blNo"             , null);
	                modelIns.put("workStat"         , "100"); //"100"
	                
	                modelIns.put("ordType"          , "02"); //"01"
	                modelIns.put("ordSubtype"       , (String)model.get("I_OUT_ORD_SUBTYPE"+i)); //ORD_SUBTYPE //주문상세
	                modelIns.put("outReqDt"         , ((String)model.get("I_OUT_DLV_DT"+i)).replace("-", ""));  //DLV_DT //날짜이니까 아마 - replace 해야할텐데
	                modelIns.put("outDt"            , null);
	                modelIns.put("pdaStat"          , null);
	                
	                modelIns.put("workSeq"          , null);
	                modelIns.put("capaTot"          , "0"); //"0"
	                modelIns.put("kinOutYn"         , "N"); //"N"
	                modelIns.put("carConfYn"        , "N"); //"N"
	                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO)); //"0000003200"
	                
	                modelIns.put("tplOrdId"         , null);
	                modelIns.put("payYn"            , "N"); //"N"
	                modelIns.put("asnInReqDt"       , null);
	                modelIns.put("cntrNoM"          , null);
	                modelIns.put("cntrSealNoM"      , null);
	                	                
	                //sub
	                modelIns.put("ordSeq"           , ordSeq);
	                modelIns.put("ritemId"          , ritemId); //I_ritemId
	                modelIns.put("custLotNo"        , custLotNo);
	                modelIns.put("realInQty"        , realInQty);
	                
	                modelIns.put("realOutQty"       , realOutQty);
	                modelIns.put("makeDt"           , makeDt);
	                modelIns.put("timePeriodDay"    , timePeriodDay);
	                modelIns.put("locYn"            , locYn);
	                modelIns.put("pdaCd"            , pdaCd);
	                
	                modelIns.put("workYn"           , workYn);
	                modelIns.put("rjType"           , rjType);
	                modelIns.put("realPltQty"       , realPltQty);
	                modelIns.put("realBoxQty"       , realBoxQty);
	                modelIns.put("confYn"           , confYn);
	                
	                modelIns.put("unitAmt"          , unitAmt);
	                modelIns.put("amt"              , amt);
	                modelIns.put("eaCapa"           , eaCapa);
	                modelIns.put("boxBarcode"       , boxBarcode);
	                modelIns.put("inOrdUomId"       , inOrdUomId); //I_inOrdUomId
	                modelIns.put("inWorkUomId"   	, inWorkUomId); //I_inWorkUomId
	                
	                modelIns.put("outOrdUomId"      , outOrdUomId);
	                modelIns.put("outWorkUomId"  	, outWorkUomId);
	                modelIns.put("minUomId"  		, minUomId);
	                
	                modelIns.put("inOrdQty"         , inOrdQty); //I_inOrdQty
	                modelIns.put("inWorkOrdQty"     , inWorkOrdQty); //I_inWorkOrdQty
	                modelIns.put("outOrdQty"        , outOrdQty);
	                
	                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
	                modelIns.put("inDspQty"		    , inDspQty);
	                modelIns.put("outDspQty"		, outDspQty);
	                
	                modelIns.put("refSubLotId"      , refSubLotId);
	                modelIns.put("outOrdWeight"     , outOrdWeight);
	                
	                modelIns.put("cntrId"           , cntrId);
	                modelIns.put("cntrNo"           , cntrNo);
	                modelIns.put("cntrType"         , cntrType);
	                modelIns.put("cntrSealNo"       , cntrSealNo);
	                
	                modelIns.put("ordDesc"          , ordDesc); //I_ordDesc
	                modelIns.put("etc2"             , etc2);
	                modelIns.put("itemBestDate"     , itemBestDate);
	                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);
	                
	                //session 정보
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	
	                //dao                
//	                modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
	                
  	                //dao 변경 (2021.07.15)
	                //modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
	                //System.out.println((String)model.get("I_ST_GUBUN"+i));

	                
	                //dao 변경 (2021.11.08) 
	                //System.out.println("------------shipmentNo : " +(String)model.get("I_ORD_DIV"+i));
	                modelIns = (Map<String, Object>)dao.saveOutOrder(modelIns);
	                
	                ServiceUtil.isValidReturnCode("WMSOM020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));  
	            }
                              

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }
            
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

}
