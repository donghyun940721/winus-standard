package com.logisall.winus.wmsom.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsom.service.WMSOM020Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.logisall.ws.interfaces.wmsif.service.impl.WMSIF050Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.code.service.CodeParser;

@Service("WMSOM020Service")
public class WMSOM020ServiceImpl implements WMSOM020Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM020Dao")
    private WMSOM020Dao dao;

    @Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao2;

    @Resource(name = "WMSIF050Dao")
    private WMSIF050Dao dao3;

    /**
     * 
     * 대체 Method ID : list 대체 Method 설명 : 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
	Map<String, Object> map = new HashMap<String, Object>();
	if (model.get("page") == null) {
	    model.put("pageIndex", "1");
	} else {
	    model.put("pageIndex", model.get("page"));
	}
	if (model.get("rows") == null) {
	    model.put("pageSize", "20");
	} else {
	    model.put("pageSize", model.get("rows"));
	}
	map.put("LIST", dao.list(model));
	return map;
    }

    /**
     * 
     * 대체 Method ID : listDetail 대체 Method 설명 : 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
	Map<String, Object> map = new HashMap<String, Object>();
	if (model.get("page") == null) {
	    model.put("pageIndex", "1");
	} else {
	    model.put("pageIndex", model.get("page"));
	}
	if (model.get("rows") == null) {
	    model.put("pageSize", "20");
	} else {
	    model.put("pageSize", model.get("rows"));
	}
	map.put("LIST", dao.listDetail(model));
	return map;
    }

    /**
     * 
     * 대체 Method ID : confirmOrder 대체 Method 설명 : 작성자 : khkim
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> confirmOrder(Map<String, Object> model) throws Exception {
	Map<String, Object> map = new HashMap<String, Object>();

	map.put("I_INSERT_DATE", model.get("vrCalDlvDt"));
	map.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
	map.put("I_USER_NO", model.get("SS_USER_NO"));

	@SuppressWarnings("unchecked")
	Map<String, Object> returnMap = (Map<String, Object>) dao.confirmOrder(model);
	/*
	 * int errCode = Integer.parseInt(returnMap.get("O_MSG_CODE").toString());
	 * String errMsg = returnMap.get("O_MSG_NAME").toString();
	 * 
	 * map.put("errCode", errCode); map.put("errMsg", errMsg);
	 */

	return returnMap;
    }

    /**
     * 분할전 대체 Method ID : saveInOrder 대체 Method 설명 : 입고확정 작성자 : KHKIM
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	// log.info(model);
	try {

	    int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
	    if (insCnt > 0) {

		for (int i = 0; i < insCnt; i++) {
		    // 저장, 수정
		    String[] dsSubRowStatus = new String[insCnt];
		    String[] ordSeq = new String[insCnt];
		    String[] ritemId = new String[insCnt];
		    String[] custLotNo = new String[insCnt];
		    String[] realInQty = new String[insCnt];

		    String[] realOutQty = new String[insCnt];
		    String[] makeDt = new String[insCnt];
		    String[] timePeriodDay = new String[insCnt];
		    String[] locYn = new String[insCnt];
		    String[] pdaCd = new String[insCnt];

		    String[] workYn = new String[insCnt];
		    String[] rjType = new String[insCnt];
		    String[] realPltQty = new String[insCnt];
		    String[] realBoxQty = new String[insCnt];
		    String[] confYn = new String[insCnt];

		    String[] unitAmt = new String[insCnt];
		    String[] amt = new String[insCnt];
		    String[] eaCapa = new String[insCnt];
		    String[] boxBarcode = new String[insCnt];
		    String[] inOrdUomId = new String[insCnt];

		    String[] inWorkUomId = new String[insCnt];
		    String[] outOrdUomId = new String[insCnt];
		    String[] outWorkUomId = new String[insCnt];
		    String[] inOrdQty = new String[insCnt];
		    String[] inWorkOrdQty = new String[insCnt];

		    String[] outOrdQty = new String[insCnt];
		    String[] outWorkOrdQty = new String[insCnt];
		    String[] refSubLotId = new String[insCnt];
		    String[] dspId = new String[insCnt];
		    String[] carId = new String[insCnt];

		    String[] cntrId = new String[insCnt];
		    String[] cntrNo = new String[insCnt];
		    String[] cntrType = new String[insCnt];
		    String[] badQty = new String[insCnt];
		    String[] uomNm = new String[insCnt];

		    String[] unitPrice = new String[insCnt];
		    String[] whNm = new String[insCnt];
		    String[] itemKorNm = new String[insCnt];
		    String[] itemEngNm = new String[insCnt];
		    String[] repUomId = new String[insCnt];

		    String[] uomCd = new String[insCnt];
		    String[] uomId = new String[insCnt];
		    String[] repUomCd = new String[insCnt];
		    String[] repuomNm = new String[insCnt];
		    String[] itemGrpId = new String[insCnt];

		    String[] expiryDate = new String[insCnt];
		    String[] inOrdWeight = new String[insCnt];
		    String[] unitNo = new String[insCnt];
		    String[] ordDesc = new String[insCnt];
		    String[] validDt = new String[insCnt];

		    String[] etc2 = new String[insCnt];
		    String[] locCd = new String[insCnt];

		    String[] userMemo = new String[insCnt];

		    // 추가
		    String[] itemBestDate = new String[insCnt]; // 상품유효기간
		    String[] itemBestDateEnd = new String[insCnt]; // 상품유효기간만료일
		    String[] rtiNm = new String[insCnt]; // 물류기기명

		    dsSubRowStatus[0] = (String) model.get("I_ST_GUBUN" + i);
		    ordSeq[0] = null;
		    ritemId[0] = (String) model.get("I_RITEM_ID" + i);
		    custLotNo[0] = null;
		    realInQty[0] = null;

		    realOutQty[0] = null;
		    makeDt[0] = null;
		    timePeriodDay[0] = null;
		    locYn[0] = null;
		    pdaCd[0] = null;

		    workYn[0] = null;
		    rjType[0] = null;
		    realPltQty[0] = null;
		    realBoxQty[0] = null;
		    confYn[0] = null;

		    unitAmt[0] = (String) model.get("I_UNIT_AMT" + i);
		    amt[0] = (String) model.get("I_AMT" + i);
		    eaCapa[0] = null;
		    boxBarcode[0] = null;
		    inOrdUomId[0] = (String) model.get("I_IN_ORD_UOM_ID" + i);
		    inWorkUomId[0] = (String) model.get("I_IN_WORK_UOM_ID" + i);
		    outOrdUomId[0] = null;
		    outWorkUomId[0] = null;
		    inOrdQty[0] = (String) model.get("I_IN_ORD_QTY" + i);
		    inWorkOrdQty[0] = (String) model.get("I_IN_WORK_ORD_QTY" + i);
		    outOrdQty[0] = null;
		    outWorkOrdQty[0] = null;
		    refSubLotId[0] = null;
		    dspId[0] = null;

		    carId[0] = null;
		    cntrId[0] = null;
		    cntrNo[0] = null;
		    cntrType[0] = null;
		    badQty[0] = null;

		    uomNm[0] = null;
		    unitPrice[0] = null;
		    whNm[0] = null;
		    itemKorNm[0] = null;
		    itemEngNm[0] = null;

		    repUomId[0] = null;
		    uomCd[0] = null;
		    uomId[0] = null;
		    repUomCd[0] = null;
		    repuomNm[0] = null;

		    itemGrpId[0] = null;
		    expiryDate[0] = null;
		    inOrdWeight[0] = null;
		    unitNo[0] = null;
		    ordDesc[0] = null;

		    validDt[0] = (String) model.get("I_IN_VALID_DT" + i);

		    etc2[0] = null;
		    locCd[0] = null;

		    itemBestDateEnd[0] = (String) model.get("I_IN_VALID_DT" + i); // 유통기한

		    userMemo[0] = null;

		    // 프로시져에 보낼것들 다담는다
		    Map<String, Object> modelIns = new HashMap<String, Object>();

		    // main
		    modelIns.put("dsMain_rowStatus", null);
		    modelIns.put("vrOrdId", null);
		    modelIns.put("inReqDt", ((String) model.get("I_IN_DLV_DT" + i)).replace("-", "")); // 날짜이니까 아마 -
												       // replace 해야할텐데
		    modelIns.put("inDt", null);
		    modelIns.put("custPoid", null);

		    modelIns.put("custPoseq", null);
		    modelIns.put("orgOrdId", (String) model.get("I_IN_ORG_ORD_ID" + i));
		    modelIns.put("orgOrdSeq", (String) model.get("I_IN_ORD_DETAIL_NO" + i));
		    modelIns.put("vrWhId", null);
		    modelIns.put("outWhId", null);

		    modelIns.put("transCustId", null);
		    modelIns.put("vrCustId", (String) model.get("vrSrchCustId"));
		    modelIns.put("pdaFinishYn", null);
		    modelIns.put("blNo", null);
		    modelIns.put("workStat", "100");

		    modelIns.put("ordType", "01");
		    modelIns.put("ordSubtype", (String) model.get("I_IN_ORD_SUBTYPE" + i)); // 주문상세
		    modelIns.put("outReqDt", null);
		    modelIns.put("outDt", null);
		    modelIns.put("pdaStat", null);

		    modelIns.put("workSeq", null);
		    modelIns.put("capaTot", "0");
		    modelIns.put("kinOutYn", "N");
		    modelIns.put("carConfYn", "N");
		    modelIns.put("gvLcId", (String) model.get(ConstantIF.SS_SVC_NO));

		    modelIns.put("tplOrdId", null);
		    modelIns.put("approveYn", "N");
		    modelIns.put("payYn", "N");
		    modelIns.put("inCustId", (String) model.get("I_IN_CUST_ID" + i));
		    modelIns.put("inCustAddr", null);

		    modelIns.put("inCustEmpNm", null);
		    modelIns.put("inCustTel", null);
		    modelIns.put("lotType", (String) model.get("I_LOT_TYPE" + i));
		    modelIns.put("ownerCd", (String) model.get("I_OWNER_CD" + i));

		    // sub
		    modelIns.put("dsSub_rowStatus", dsSubRowStatus);
		    modelIns.put("ordSeq", ordSeq);
		    modelIns.put("ritemId", ritemId);
		    modelIns.put("custLotNo", custLotNo);
		    modelIns.put("realInQty", realInQty);

		    modelIns.put("realOutQty", realOutQty);
		    modelIns.put("makeDt", makeDt);
		    modelIns.put("timePeriodDay", timePeriodDay);
		    modelIns.put("locYn", locYn);
		    modelIns.put("pdaCd", pdaCd);

		    modelIns.put("workYn", workYn);
		    modelIns.put("rjType", rjType);
		    modelIns.put("realPltQty", realPltQty);
		    modelIns.put("realBoxQty", realBoxQty);
		    modelIns.put("confYn", confYn);

		    modelIns.put("unitAmt", unitAmt);
		    modelIns.put("amt", amt);
		    modelIns.put("eaCapa", eaCapa);
		    modelIns.put("boxBarcode", boxBarcode);
		    modelIns.put("inOrdUomId", inOrdUomId);
		    modelIns.put("inWorkUomId", inWorkUomId);

		    modelIns.put("outOrdUomId", outOrdUomId);
		    modelIns.put("outWorkUomId", outWorkUomId);
		    modelIns.put("inOrdQty", inOrdQty);
		    modelIns.put("inWorkOrdQty", inWorkOrdQty);
		    modelIns.put("outOrdQty", outOrdQty);

		    modelIns.put("outWorkOrdQty", outWorkOrdQty);
		    modelIns.put("refSubLotId", refSubLotId);
		    modelIns.put("dspId", dspId);
		    modelIns.put("carId", carId);
		    modelIns.put("cntrId", cntrId);

		    modelIns.put("cntrNo", cntrNo);
		    modelIns.put("cntrType", cntrType);
		    modelIns.put("badQty", badQty);
		    modelIns.put("uomNm", uomNm);
		    modelIns.put("unitPrice", unitPrice);

		    modelIns.put("whNm", whNm);
		    modelIns.put("itemKorNm", itemKorNm);
		    modelIns.put("itemEngNm", itemEngNm);
		    modelIns.put("repUomId", repUomId);
		    modelIns.put("uomCd", uomCd);

		    modelIns.put("uomId", uomId);
		    modelIns.put("repUomCd", repUomCd);
		    modelIns.put("repuomNm", repuomNm);
		    modelIns.put("itemGrpId", itemGrpId);
		    modelIns.put("expiryDate", expiryDate);

		    modelIns.put("inOrdWeight", inOrdWeight);
		    modelIns.put("unitNo", unitNo);
		    modelIns.put("ordDesc", ordDesc);
		    modelIns.put("validDt", validDt);
		    modelIns.put("etc2", etc2);
		    modelIns.put("locCd", locCd);

		    // 추가
		    modelIns.put("shipmentNo", (String) model.get("I_ORD_DIV" + i));

		    // 추가된거(아직프로시져는 안탐)
		    modelIns.put("itemBestDate", itemBestDate);
		    modelIns.put("itemBestDateEnd", itemBestDateEnd);
		    modelIns.put("rtiNm", rtiNm);

		    modelIns.put("userMemo", userMemo);

		    // session 정보
		    modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
		    modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));

		    // dao
//	                modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);

		    // dao 변경 (2021.07.15)
		    // modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
		    // System.out.println((String)model.get("I_ST_GUBUN"+i));

		    // dao 변경 (2021.11.08)
		    // System.out.println("------------shipmentNo : "
		    // +(String)model.get("I_ORD_DIV"+i));
		    modelIns = (Map<String, Object>) dao.saveInOrder_2048_21(modelIns);

		    ServiceUtil.isValidReturnCode("WMSOM020", String.valueOf(modelIns.get("O_MSG_CODE")),
			    (String) modelIns.get("O_MSG_NAME"));
		}

		// errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
		// errMsg = modelIns.get("O_MSG_NAME").toString();
	    }

	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", 1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    throw e;
	}
	return m;
    }

    /**
     * Method ID : saveInOrderPLT Method 설명 : PLT 환산 수량만큼 주문 확정 작성자 : yhku
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrderPLT(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	try {
	    int temCnt = Integer.parseInt(model.get("selectIds").toString());
	    System.out.println("selectIds size : " + temCnt);

	    if (temCnt > 0) {
		for (int i = 0; i < temCnt; i++) {
		    int size = Integer.parseInt((String) model.get("PLT_QTY" + i));
		    int orgOrdQty = Integer.parseInt((String) model.get("ORG_ORD_QTY" + i));
		    int repUomPerQty = Integer.parseInt((String) model.get("UOM_QTY_PER_PLT" + i));

		    String[] orgDetailNo = new String[size];
		    String[] inOrdQty = new String[size];
		    String[] orgOrdNo = new String[size];
		    String[] orgItemCd = new String[size];
		    String[] custLotNo = new String[size];
		    String[] custId = new String[size];
		    String[] itemBarCd = new String[size];
		    String[] ritemId = new String[size];
		    String[] lcId = new String[size];
		    String[] issueDate = new String[size];
		    String[] sapBarCd = new String[size];

		    Map<String, Object> modelDS = new HashMap<String, Object>();
		    for (int k = 0; k < size; k++) {

			int workQty = 0;
			if (k == size - 1) {
			    workQty = orgOrdQty - (repUomPerQty * k);
			} else {
			    workQty = repUomPerQty;
			}
			System.out.println("workQty : " + workQty);

			inOrdQty[k] = workQty + "";
			orgItemCd[k] = (String) model.get("ORG_ITEM_CD" + i);
			orgOrdNo[k] = (String) model.get("ORG_ORD_NO" + i);
			orgDetailNo[k] = (String) model.get("ORG_DETAIL_NO" + i);
			itemBarCd[k] = (String) model.get("ITEM_BAR_CD" + i);
			ritemId[k] = (String) model.get("RITEM_ID" + i);
			lcId[k] = (String) model.get("LC_ID" + i);
			custId[k] = (String) model.get("CUST_ID" + i);
			issueDate[k] = DateUtil.getCurrentDate("yyyyMMdd");
			sapBarCd[k] = "";

			int nextSeq = k + 1;
			custLotNo[k] = (String) model.get("ORG_ORD_NO" + i) + "-"
				+ (String) model.get("ORG_DETAIL_NO" + i) + "-" + nextSeq;
			/*
			 * modelDS.put("orgOrdId" , (String)model.get("ORG_ORD_NO"+i));
			 * modelDS.put("orgOrdSeq" , (String)model.get("ORG_DETAIL_NO"+i));
			 * modelDS.put("seq" , k+1); custLotNo[k] =
			 * (String)dao.getCustLotNoVer2(modelDS);
			 */

			/*
			 * System.out.println(k+" custLotNo : " + custLotNo[k]);
			 * System.out.println(k+" orgOrdNo : " + orgOrdNo[k]);
			 * System.out.println(k+" orgDetailNo : " + orgDetailNo[k]);
			 * System.out.println(k+" inOrdQty : " + inOrdQty[k]);
			 * System.out.println(k+" orgItemCd : " + orgItemCd[k]);
			 * System.out.println(k+" itemBarCd : " + itemBarCd[k]);
			 * System.out.println(k+" lcId : " + lcId[k]);
			 * System.out.println(k+" ritemId : " + ritemId[k]);
			 */

		    }

		    Map<String, Object> modelIns = new HashMap<String, Object>();
		    modelIns.put("iEpcCd", null);
		    modelIns.put("iLcId", lcId);
		    modelIns.put("iTransCustId", null);
		    modelIns.put("iOrdId", orgOrdNo);// 원주문번호
		    modelIns.put("iOrdSeq", orgDetailNo);

		    modelIns.put("iWorkWgt", null);
		    modelIns.put("iRefSubLotId", null);
		    modelIns.put("iSapBarcode", sapBarCd);
		    modelIns.put("iFromLocCd", null);
		    modelIns.put("iToLocCd", null);

		    modelIns.put("iChildCustLotNo", custLotNo);
		    modelIns.put("iMapItemQty", inOrdQty);
		    modelIns.put("iMapItemBarCd", itemBarCd);

		    modelIns.put("iWorkIp", (String) model.get(ConstantIF.SS_CLIENT_IP));
		    modelIns.put("iUserNo", (String) model.get(ConstantIF.SS_USER_NO));

		    // 프로시저 pk_wmsif050.sp_ord_divied_complete_mobile_2048
		    // modelIns = (Map<String,
		    // Object>)dao3.runSpOrdDiviedCompleteMobile2048(modelIns);

		    // 프로시저 PK_WMSOP021.SP_IN_OMS_ORD_DIVIDED_COMPLETE
		    modelIns = (Map<String, Object>) dao.saveOmsOrdPLTDividedComplete(modelIns);
		    ServiceUtil.isValidReturnCode("WMSOM020", String.valueOf(modelIns.get("O_MSG_CODE")),
			    (String) modelIns.get("O_MSG_NAME"));
		}
	    }
	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", -1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    m.put("errCnt", 0);
	    throw e;
	}
	return m;
    }

    /**
     * Method ID : saveInOrderNewtong Method 설명 : 뉴통 로케이션, 유통기한, 제조일자등 정보 등록후 주문등록
     * 작성자 : yangjiwoon
     * 
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> saveInOrderNewtong(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	try {
	    int temCnt = Integer.parseInt(model.get("selectIds").toString());
	    Map<String, Object> modelIns = new HashMap<String, Object>();
	    System.out.println("selectIds size : " + temCnt);

	    String[] orgDetailNo = new String[temCnt];
	    String[] inOrdQty = new String[temCnt];
	    String[] orgOrdNo = new String[temCnt];
	    String[] orgItemCd = new String[temCnt];
	    String[] custLotNo = new String[temCnt];
	    String[] custId = new String[temCnt];
	    String[] itemBarCd = new String[temCnt];
	    String[] ritemId = new String[temCnt];
	    String[] lcId = new String[temCnt];
	    String[] issueDate = new String[temCnt];
	    String[] sapBarCd = new String[temCnt];
	    String[] validDate = new String[temCnt];
	    String[] makeDate = new String[temCnt];
	    String[] locCd = new String[temCnt];

	    if (temCnt > 0) {
		int addSeq = 1;
		for (int i = 0; i < temCnt; i++) {
//                	 int size = Integer.parseInt((String)model.get("PLT_QTY"+i));
		    int orgOrdQty = Integer.parseInt((String) model.get("ORG_ORD_QTY" + i));
//                	 int repUomPerQty = Integer.parseInt((String)model.get("UOM_QTY_PER_PLT"+i));

		    inOrdQty[i] = (String) model.get("WORK_QTY" + i);
		    orgItemCd[i] = (String) model.get("ORG_ITEM_CD" + i);
		    orgOrdNo[i] = (String) model.get("ORG_ORD_NO" + i);
		    orgDetailNo[i] = (String) model.get("ORG_DETAIL_NO" + i);
		    itemBarCd[i] = (String) model.get("ITEM_BARCODE" + i);
		    ritemId[i] = (String) model.get("RITEM_ID" + i);
		    lcId[i] = (String) model.get("LC_ID" + i);
		    custId[i] = (String) model.get("CUST_ID" + i);
		    issueDate[i] = DateUtil.getCurrentDate("yyyyMMdd");
		    validDate[i] = model.get("VALID_DT" + i).toString().replaceAll("-", "");
		    makeDate[i] = (String) model.get("MAKE_DATE" + i);
		    locCd[i] = (String) model.get("LOC_CD" + i);
		    sapBarCd[i] = "";
		    int nextSeq = 1;
		    if (i != 0) {
			String a = orgDetailNo[i - 1];
			String b = orgDetailNo[i];
		    }

		    if (i != 0 && orgDetailNo[i - 1].equals(orgDetailNo[i])) {
			nextSeq += addSeq;
			addSeq++;
		    } else if (i != 0 && !orgDetailNo[i - 1].equals(orgDetailNo[i])) {
			addSeq = 1;
		    }

		    custLotNo[i] = (String) model.get("ORG_ORD_NO" + i) + "-" + (String) model.get("ORG_DETAIL_NO" + i)
			    + "-" + nextSeq;

		    modelIns.put("iLcId", lcId);
		    modelIns.put("iOrdId", orgOrdNo);// 원주문번호
		    modelIns.put("iOrdSeq", orgDetailNo);
		    modelIns.put("iCustId", custId);
		    modelIns.put("iRitemCd", ritemId);
		    modelIns.put("iItemBestDateEnd", validDate);
		    modelIns.put("iMakeDate", makeDate);
		    modelIns.put("iWorkQty", inOrdQty);
		    modelIns.put("iToLocCd", locCd);
		    modelIns.put("iBoxBarCd", null);
		    modelIns.put("iBoxInQty", inOrdQty);
		    modelIns.put("iItemBarCd", itemBarCd);
		    modelIns.put("iWorkMemo", null);
		    modelIns.put("iCustLotNo", custLotNo);
		    modelIns.put("iWorkIp", model.get(ConstantIF.SS_CLIENT_IP));
		    modelIns.put("iUserNo", model.get(ConstantIF.SS_USER_NO));

		}
		modelIns = (Map<String, Object>) dao.saveOmsOrdNewtongComplete(modelIns);
		ServiceUtil.isValidReturnCode("WMSOM020", String.valueOf(modelIns.get("O_MSG_CODE")),
			(String) modelIns.get("O_MSG_NAME"));
	    }
	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", -1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    m.put("errCnt", 0);
	    throw e;
	}
	return m;
    }

    /**
     * Method ID : selectNewtongOrd Method 설명 : 뉴통팝업주문 조회 작성자 : yangjiwoon
     * 
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> selectNewtongOrd(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<>();
	List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
	try {
	    int temCnt = Integer.parseInt(model.get("vrRowCount").toString());

	    if (temCnt > 0) {
		for (int i = 0; i < temCnt; i++) {
		    Map<String, String> modelIns = new HashMap<>();
		    modelIns.put("ORG_ORD_ID", model.get("ORG_ORD_NO" + i).toString());
		    modelIns.put("ORG_DETAIL_NO", model.get("ORG_DETAIL_NO" + i).toString());
		    modelIns.put("RITEM_ID", model.get("RITEM_ID" + i).toString());

		    Map<String, String> ordMap = (Map<String, String>) dao.selectNewtongOrd(modelIns).get(0);
		    int cnt = Integer.parseInt(String.valueOf(ordMap.get("REPEAT_CNT")));
		    int modQty = Integer.parseInt(String.valueOf(ordMap.get("MOD_QTY")));
		    int ordQty = Integer.parseInt(String.valueOf(ordMap.get("ORD_QTY")));
		    int uomQty = Integer.parseInt(String.valueOf(ordMap.get("UOM_QTY")));

		    if (cnt < 1 || (cnt == 1 && modQty == 0)) {
			returnList.add(ordMap);
		    } else {
			for (int j = 0; j <= cnt; j++) {
			    Map<String, String> addRow = new HashMap<>();
			    addRow.put("ORG_ORD_NO", ordMap.get("ORG_ORD_NO"));
			    addRow.put("ORD_DETAIL_NO", ordMap.get("ORD_DETAIL_NO"));
			    addRow.put("ITEM_CD", ordMap.get("ITEM_CD"));
			    addRow.put("ITEM_NM", ordMap.get("ITEM_NM"));
			    addRow.put("CUST_ID", ordMap.get("CUST_ID"));
			    addRow.put("ITEM_BARCODE", ordMap.get("ITEM_BARCODE"));
			    addRow.put("RITEM_ID", ordMap.get("RITEM_ID"));
			    addRow.put("LC_ID", ordMap.get("LC_ID"));
			    addRow.put("VALID_DT", ordMap.get("VALID_DT"));
			    addRow.put("MAKE_DATE", ordMap.get("MAKE_DATE"));
			    addRow.put("LOC_CD", ordMap.get("LOC_CD"));
			    addRow.put("CUST_LOT_NO", ordMap.get("CUST_LOT_NO"));
			    addRow.put("UOM2_CODE", ordMap.get("UOM2_CODE"));
			    // addRow.put("UOM_QTY", String.valueOf(ordMap.get("UOM_QTY")));

			    if (ordQty < uomQty) {
				addRow.put("ORD_QTY", String.valueOf(ordQty));
				addRow.put("WORK_QTY", String.valueOf(ordQty));
			    } else {
				addRow.put("ORD_QTY", String.valueOf(uomQty));
				addRow.put("WORK_QTY", String.valueOf(uomQty));
				ordQty -= uomQty;
			    }

			    returnList.add(addRow);
			}
		    }

		}
	    }
	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));
	    m.put("rows", returnList);
	} catch (Exception e) {
	    m.put("errCnt", 0);
	    throw e;
	}
	return m;

    }

    @Override
    public Map<String, Object> saveInOrderVX(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	// log.info(model);
	try {

	    int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
	    if (insCnt > 0) {

		for (int i = 0; i < insCnt; i++) {
		    // 저장, 수정
		    String[] dsSubRowStatus = new String[insCnt];
		    String[] ordSeq = new String[insCnt];
		    String[] ritemId = new String[insCnt];
		    String[] custLotNo = new String[insCnt];
		    String[] realInQty = new String[insCnt];

		    String[] realOutQty = new String[insCnt];
		    String[] makeDt = new String[insCnt];
		    String[] timePeriodDay = new String[insCnt];
		    String[] locYn = new String[insCnt];
		    String[] pdaCd = new String[insCnt];

		    String[] workYn = new String[insCnt];
		    String[] rjType = new String[insCnt];
		    String[] realPltQty = new String[insCnt];
		    String[] realBoxQty = new String[insCnt];
		    String[] confYn = new String[insCnt];

		    String[] unitAmt = new String[insCnt];
		    String[] amt = new String[insCnt];
		    String[] eaCapa = new String[insCnt];
		    String[] boxBarcode = new String[insCnt];
		    String[] inOrdUomId = new String[insCnt];

		    String[] inWorkUomId = new String[insCnt];
		    String[] outOrdUomId = new String[insCnt];
		    String[] outWorkUomId = new String[insCnt];
		    String[] inOrdQty = new String[insCnt];
		    String[] inWorkOrdQty = new String[insCnt];

		    String[] outOrdQty = new String[insCnt];
		    String[] outWorkOrdQty = new String[insCnt];
		    String[] refSubLotId = new String[insCnt];
		    String[] dspId = new String[insCnt];
		    String[] carId = new String[insCnt];

		    String[] cntrId = new String[insCnt];
		    String[] cntrNo = new String[insCnt];
		    String[] cntrType = new String[insCnt];
		    String[] badQty = new String[insCnt];
		    String[] uomNm = new String[insCnt];

		    String[] unitPrice = new String[insCnt];
		    String[] whNm = new String[insCnt];
		    String[] itemKorNm = new String[insCnt];
		    String[] itemEngNm = new String[insCnt];
		    String[] repUomId = new String[insCnt];

		    String[] uomCd = new String[insCnt];
		    String[] uomId = new String[insCnt];
		    String[] repUomCd = new String[insCnt];
		    String[] repuomNm = new String[insCnt];
		    String[] itemGrpId = new String[insCnt];

		    String[] expiryDate = new String[insCnt];
		    String[] inOrdWeight = new String[insCnt];
		    String[] unitNo = new String[insCnt];
		    String[] ordDesc = new String[insCnt];
		    String[] validDt = new String[insCnt];

		    String[] etc2 = new String[insCnt];
		    String[] locCd = new String[insCnt];

		    // sing09 2022-02-04 userMemo
		    String[] userMemo = new String[insCnt];

		    // 추가
		    String[] itemBestDate = new String[insCnt]; // 상품유효기간
		    String[] itemBestDateEnd = new String[insCnt]; // 상품유효기간만료일
		    String[] rtiNm = new String[insCnt]; // 물류기기명

		    dsSubRowStatus[0] = (String) model.get("I_ST_GUBUN" + i); // "INSERT"
		    ordSeq[0] = null;
		    ritemId[0] = (String) model.get("I_RITEM_ID" + i); // RITEM_ID
		    custLotNo[0] = null;
		    realInQty[0] = null;

		    realOutQty[0] = null;
		    makeDt[0] = null;
		    timePeriodDay[0] = null;
		    locYn[0] = null;
		    pdaCd[0] = null;

		    workYn[0] = null;
		    rjType[0] = null;
		    realPltQty[0] = null;
		    realBoxQty[0] = null;
		    confYn[0] = null;

		    unitAmt[0] = null;
		    amt[0] = null;
		    eaCapa[0] = null;
		    boxBarcode[0] = null;
		    inOrdUomId[0] = (String) model.get("I_IN_ORD_UOM_ID" + i); // UOM_ID
		    inWorkUomId[0] = (String) model.get("I_IN_WORK_UOM_ID" + i); // UOM_ID
		    outOrdUomId[0] = null;
		    outWorkUomId[0] = null;
		    inOrdQty[0] = (String) model.get("I_IN_ORD_QTY" + i); // ORD_QTY
		    inWorkOrdQty[0] = (String) model.get("I_IN_WORK_ORD_QTY" + i); // ORD_QTY
		    outOrdQty[0] = null;
		    outWorkOrdQty[0] = null;
		    refSubLotId[0] = null;
		    dspId[0] = null;

		    carId[0] = null;
		    cntrId[0] = null;
		    cntrNo[0] = null;
		    cntrType[0] = null;
		    badQty[0] = null;

		    uomNm[0] = null;
		    unitPrice[0] = null;
		    whNm[0] = null;
		    itemKorNm[0] = null;
		    itemEngNm[0] = null;

		    repUomId[0] = null;
		    uomCd[0] = null;
		    uomId[0] = null;
		    repUomCd[0] = null;
		    repuomNm[0] = null;

		    itemGrpId[0] = null;
		    expiryDate[0] = null;
		    inOrdWeight[0] = null;
		    unitNo[0] = null;
		    ordDesc[0] = (String) model.get("I_ORD_DESC" + i); // ORD_DESC

		    validDt[0] = (String) model.get("I_IN_VALID_DT" + i); // VALID_DT

		    etc2[0] = null;
		    locCd[0] = null;

		    userMemo[0] = null;

		    // 프로시져에 보낼것들 다담는다
		    Map<String, Object> modelIns = new HashMap<String, Object>();

		    // main
		    modelIns.put("dsMain_rowStatus", null);
		    modelIns.put("vrOrdId", null);
		    modelIns.put("inReqDt", ((String) model.get("I_IN_DLV_DT" + i)).replace("-", "")); // DLV_DT //날짜이니까
												       // 아마 - replace
												       // 해야할텐데
		    modelIns.put("inDt", null);
		    modelIns.put("custPoid", null);

		    modelIns.put("custPoseq", null);
		    modelIns.put("orgOrdId", (String) model.get("I_IN_ORG_ORD_ID" + i)); // ORG_ORD_NO
		    modelIns.put("orgOrdSeq", (String) model.get("I_IN_ORD_DETAIL_NO" + i)); // ORD_DETAIL_NO
		    modelIns.put("vrWhId", null);
		    modelIns.put("outWhId", null);

		    modelIns.put("transCustId", null);
		    modelIns.put("vrCustId", (String) model.get("vrSrchCustId")); // "0000295694"
		    modelIns.put("pdaFinishYn", null);
		    modelIns.put("blNo", null);
		    modelIns.put("workStat", "100"); // "100"

		    modelIns.put("ordType", "01"); // "01"
		    modelIns.put("ordSubtype", (String) model.get("I_IN_ORD_SUBTYPE" + i)); // ORD_SUBTYPE //주문상세
		    modelIns.put("outReqDt", null);
		    modelIns.put("outDt", null);
		    modelIns.put("pdaStat", null);

		    modelIns.put("workSeq", null);
		    modelIns.put("capaTot", "0"); // "0"
		    modelIns.put("kinOutYn", "N"); // "N"
		    modelIns.put("carConfYn", "N"); // "N"
		    modelIns.put("gvLcId", (String) model.get(ConstantIF.SS_SVC_NO)); // "0000003200"

		    modelIns.put("tplOrdId", null);
		    modelIns.put("approveYn", "N"); // "N"
		    modelIns.put("payYn", "N"); // "N"
		    modelIns.put("inCustId", (String) model.get("I_IN_CUST_ID" + i)); // PARTNER_ID
		    modelIns.put("inCustAddr", null);

		    modelIns.put("inCustEmpNm", null);
		    modelIns.put("inCustTel", null);
		    modelIns.put("lotType", null);
		    modelIns.put("ownerCd", null);

		    // sub
		    modelIns.put("dsSub_rowStatus", dsSubRowStatus); // I_dsSubRowStatus
		    modelIns.put("ordSeq", ordSeq);
		    modelIns.put("ritemId", ritemId); // I_ritemId
		    modelIns.put("custLotNo", custLotNo);
		    modelIns.put("realInQty", realInQty);

		    modelIns.put("realOutQty", realOutQty);
		    modelIns.put("makeDt", makeDt);
		    modelIns.put("timePeriodDay", timePeriodDay);
		    modelIns.put("locYn", locYn);
		    modelIns.put("pdaCd", pdaCd);

		    modelIns.put("workYn", workYn);
		    modelIns.put("rjType", rjType);
		    modelIns.put("realPltQty", realPltQty);
		    modelIns.put("realBoxQty", realBoxQty);
		    modelIns.put("confYn", confYn);

		    modelIns.put("unitAmt", unitAmt);
		    modelIns.put("amt", amt);
		    modelIns.put("eaCapa", eaCapa);
		    modelIns.put("boxBarcode", boxBarcode);
		    modelIns.put("inOrdUomId", inOrdUomId); // I_inOrdUomId
		    modelIns.put("inWorkUomId", inWorkUomId); // I_inWorkUomId

		    modelIns.put("outOrdUomId", outOrdUomId);
		    modelIns.put("outWorkUomId", outWorkUomId);
		    modelIns.put("inOrdQty", inOrdQty); // I_inOrdQty
		    modelIns.put("inWorkOrdQty", inWorkOrdQty); // I_inWorkOrdQty
		    modelIns.put("outOrdQty", outOrdQty);

		    modelIns.put("outWorkOrdQty", outWorkOrdQty);
		    modelIns.put("refSubLotId", refSubLotId);
		    modelIns.put("dspId", dspId);
		    modelIns.put("carId", carId);
		    modelIns.put("cntrId", cntrId);

		    modelIns.put("cntrNo", cntrNo);
		    modelIns.put("cntrType", cntrType);
		    modelIns.put("badQty", badQty);
		    modelIns.put("uomNm", uomNm);
		    modelIns.put("unitPrice", unitPrice);

		    modelIns.put("whNm", whNm);
		    modelIns.put("itemKorNm", itemKorNm);
		    modelIns.put("itemEngNm", itemEngNm);
		    modelIns.put("repUomId", repUomId);
		    modelIns.put("uomCd", uomCd);

		    modelIns.put("uomId", uomId);
		    modelIns.put("repUomCd", repUomCd);
		    modelIns.put("repuomNm", repuomNm);
		    modelIns.put("itemGrpId", itemGrpId);
		    modelIns.put("expiryDate", expiryDate);

		    modelIns.put("inOrdWeight", inOrdWeight);
		    modelIns.put("unitNo", unitNo);
		    modelIns.put("ordDesc", ordDesc); // I_ordDesc
		    modelIns.put("validDt", validDt); // I_validDt
		    modelIns.put("etc2", etc2);
		    modelIns.put("locCd", locCd);

		    modelIns.put("userMemo", userMemo);

		    // 추가
		    modelIns.put("shipmentNo", (String) model.get("I_ORD_DIV" + i)); // ORD_DIV

		    // 추가된거(아직프로시져는 안탐)
		    modelIns.put("itemBestDate", itemBestDate);
		    modelIns.put("itemBestDateEnd", itemBestDateEnd);
		    modelIns.put("rtiNm", rtiNm);

		    // session 정보
		    modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
		    modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));

		    // dao
//	                modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);

		    // dao 변경 (2021.07.15)
		    // modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
		    // System.out.println((String)model.get("I_ST_GUBUN"+i));

		    // dao 변경 (2021.11.08)
		    // System.out.println("------------shipmentNo : "
		    // +(String)model.get("I_ORD_DIV"+i));
		    modelIns = (Map<String, Object>) dao.saveInOrder_2048_21(modelIns);

		    ServiceUtil.isValidReturnCode("WMSOM020", String.valueOf(modelIns.get("O_MSG_CODE")),
			    (String) modelIns.get("O_MSG_NAME"));
		}

		// errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
		// errMsg = modelIns.get("O_MSG_NAME").toString();
	    }

	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", 1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    throw e;
	}
	return m;
    }

    /**
     * 
     * 대체 Method ID : listDetail 대체 Method 설명 : 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listNew(Map<String, Object> model) throws Exception {
	Map<String, Object> map = new HashMap<String, Object>();
	if (model.get("page") == null) {
	    model.put("pageIndex", "1");
	} else {
	    model.put("pageIndex", model.get("page"));
	}
	if (model.get("rows") == null) {
	    model.put("pageSize", "20");
	} else {
	    model.put("pageSize", model.get("rows"));
	}
	map.put("LIST", dao.listNew(model));
	return map;
    }

    /**
     * 
     * 대체 Method ID : listNew2 대체 Method 설명 : 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listNew2(Map<String, Object> model) throws Exception {
	Map<String, Object> map = new HashMap<String, Object>();
	if (model.get("page") == null) {
	    model.put("pageIndex", "1");
	} else {
	    model.put("pageIndex", model.get("page"));
	}
	if (model.get("rows") == null) {
	    model.put("pageSize", "20");
	} else {
	    model.put("pageSize", model.get("rows"));
	}
	map.put("LIST", dao.listNew2(model));
	return map;
    }

    /**
     * 분할전 대체 Method ID : deleteInOrder 대체 Method 설명 : 주문삭제 작성자 : KHKIM
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteInOrder(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	// log.info(model);
	try {

	    int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
	    if (insCnt > 0) {

		for (int i = 0; i < insCnt; i++) {
		    // 저장, 수정

		    // 프로시져에 보낼것들 다담는다
		    Map<String, Object> modelIns = new HashMap<String, Object>();

		    // main
		    modelIns.put("orgOrdId", (String) model.get("I_IN_ORG_ORD_ID" + i));
		    modelIns.put("orgOrdSeq", (String) model.get("I_IN_ORD_DETAIL_NO" + i));
		    modelIns.put("ordDt", (String) model.get("I_IN_ORD_DT" + i));

		    // session 정보
		    modelIns.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
		    modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
		    modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
		    // dao
		    try {
			dao.deleteInOrder(modelIns);
		    } catch (Exception e) {
			throw new BizException("삭제 실패");
		    }
		    // System.out.println((String)model.get("I_ST_GUBUN"+i));

		    // ServiceUtil.isValidReturnCode("WMSOM020",
		    // String.valueOf(modelIns.get("O_MSG_CODE")),
		    // (String)modelIns.get("O_MSG_NAME"));
		}

		// errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
		// errMsg = modelIns.get("O_MSG_NAME").toString();
	    }

	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", 1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    throw e;
	}
	return m;
    }

    @Override
    public Map<String, Object> deleteOutOrder(Map<String, Object> model, Map<String, Object> reqMap) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	try {
	    List<Map<String, Object>> orderList = (List<Map<String, Object>>) reqMap.get("orderList");
	    for (Map<String, Object> order : orderList) {
		order.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
		order.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
		order.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
		try {
		    dao.deleteOutOrder(order);
		} catch (Exception e) {
		    throw new BizException("삭제 실패");
		}
	    }
	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", 1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    throw e;
	}
	return m;
    }

    @Override
    public Map<String, Object> saveCustLotNo(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	try {
	    int temCnt = Integer.parseInt(model.get("selectIds").toString());

	    if (temCnt > 0) {
		for (int i = 0; i < temCnt; i++) {
		    int size = Integer.parseInt((String) model.get("ORG_ORD_QTY" + i));

		    String[] orgDetailNo = new String[size];
		    String[] orgOrdQty = new String[size];
		    String[] orgOrdNo = new String[size];
		    String[] orgItemCd = new String[size];
		    String[] custLotNo = new String[size];
		    String[] custId = new String[size];
		    String[] itemBarCd = new String[size];
		    String[] ritemId = new String[size];
		    String[] lcId = new String[size];
		    String[] issueDate = new String[size];

		    String[] mapItemBarCd = new String[size];
		    String[] makeLineNo = new String[size];

		    Map<String, Object> modelDS = new HashMap<String, Object>();
		    for (int k = 0; k < size; k++) {
			orgOrdQty[k] = "1";
			orgItemCd[k] = (String) model.get("ORG_ITEM_CD" + i);
			orgOrdNo[k] = (String) model.get("ORG_ORD_NO" + i);
			orgDetailNo[k] = (String) model.get("ORG_DETAIL_NO" + i);
			custId[k] = (String) model.get("CUST_ID" + i);
			itemBarCd[k] = (String) model.get("ITEM_BAR_CD" + i);
			ritemId[k] = (String) model.get("RITEM_ID" + i);
			lcId[k] = (String) model.get("LC_ID" + i);
			issueDate[k] = DateUtil.getCurrentDate("yyyyMMdd");

			mapItemBarCd[k] = "";
			makeLineNo[k] = "";

			// dao.getCustLotNo Map
			modelDS.put("lcId", (String) model.get("LC_ID" + i));
			modelDS.put("custId", (String) model.get("CUST_ID" + i));
			modelDS.put("orgItemCd", (String) model.get("ORG_ITEM_CD" + i));
			modelDS.put("orgGubun", (String) model.get("ORG_DETAIL_NO" + i) + k);

			custLotNo[k] = (String) dao.getCustLotNo(modelDS);
		    }

		    // 프로시저 wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate
		    Map<String, Object> modelIns = new HashMap<String, Object>();
		    modelIns.put("epcCd", null);
		    modelIns.put("lcId", lcId);
		    modelIns.put("ordId", orgOrdNo);
		    modelIns.put("ordSeq", orgDetailNo);
		    modelIns.put("custId", custId);

		    modelIns.put("ritemId", ritemId);
		    modelIns.put("issueDate", issueDate);
		    modelIns.put("makeDate", null);
		    modelIns.put("itemBestEndDate", null);
		    modelIns.put("workQty", null);

		    modelIns.put("makeLineNo", makeLineNo);
		    modelIns.put("itemBarCd", itemBarCd);
		    modelIns.put("childCustLotNo", custLotNo);
		    modelIns.put("mapItemQty", orgOrdQty);
		    modelIns.put("mapItemBarCd", mapItemBarCd);

		    modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
		    modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));

		    modelIns = (Map<String, Object>) dao.issueLabelOrdCom(modelIns);
		    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")),
			    (String) modelIns.get("O_MSG_NAME"));
		}
	    }
	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", -1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    m.put("errCnt", 0);
	    throw e;
	}
	return m;
    }

//    @Override
//    public Map<String, Object> saveCustLotNoUpdate(Map<String, Object> model) throws Exception {
//    	 Map<String, Object> m = new HashMap<String, Object>();
//    	 try{
//             int temCnt = Integer.parseInt(model.get("selectIds").toString());
//             
//             if(temCnt > 0){ // 2
//                 for(int i = 0 ; i < temCnt ; i ++){ // 0 1
//                	 int size = Integer.parseInt((String)model.get("ORG_ORD_QTY"+i));
//                	 
//                	 String[] orgDetailNo = new String[size];
//                     String[] orgOrdQty 	= new String[size];
//                     String[] orgOrdNo 		= new String[size];
//                     String[] orgItemCd 	= new String[size];
//                     String[] custLotNo 	= new String[size];
//                     String[] custId 		= new String[size];
//                     String[] itemBarCd	 	= new String[size];
//                     String[] ritemId 		= new String[size];
//                     String[] lcId			= new String[size];
//                     String[] issueDate			= new String[size];
//	                     
////                	 Map<String, Object> modelDS = new HashMap<String, Object>();
//                     for(int k = 0 ; k < size ; k++ ) {
//                    	orgOrdQty[k]   	= "1";
//	                   	orgItemCd[k]   	= (String)model.get("ORG_ITEM_CD"+i);
//	                   	orgOrdNo[k]    	= (String)model.get("ORG_ORD_NO"+i);
//	                   	orgDetailNo[k] 	= (String)model.get("ORG_DETAIL_NO"+i);
//	                   	custId[k]		= (String)model.get("CUST_ID"+i);
//	                   	itemBarCd[k]	= (String)model.get("ITEM_BAR_CD"+i);
//	                   	ritemId[k]		= (String)model.get("RITEM_ID"+i);
//	                   	lcId[k]			= (String)model.get("LC_ID"+i);
//	                   	issueDate[k]	= DateUtil.getCurrentDate("yyyyMMdd");
//	                   	custLotNo[k]	= (String)model.get("CUST_LOT_NO"+i);
//	                   	//dao.getCustLotNo Map
////	                   	modelDS.put("lcId"     		, (String)model.get("LC_ID"+i));
////	                   	modelDS.put("custId"     	, (String)model.get("CUST_ID"+i));
////	                   	modelDS.put("orgItemCd"    	, (String)model.get("ORG_ITEM_CD"+i));
////                        modelDS.put("orgGubun"    	, (String)model.get("ORG_DETAIL_NO"+i) + k);
//                        
////                    	custLotNo[k] = (String)dao.getCustLotNo(modelDS);
//                    }
//                     
//	                // 프로시저 wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate
//                    Map<String, Object> modelIns = new HashMap<String, Object>();
//                    modelIns.put("epcCd"   			, null);
//                    modelIns.put("lcId"    			, lcId);
//                    modelIns.put("ordId"   			, orgOrdNo);
//                    modelIns.put("ordSeq"   		, orgDetailNo);
//                    modelIns.put("custId"   		, custId);
//                    
//                    modelIns.put("ritemId"   		, ritemId);
//                    modelIns.put("issueDate" 		, issueDate);
//                    modelIns.put("makeDate"   		, null);
//                    modelIns.put("itemBestEndDate"  , null);
//                    modelIns.put("workQty"   		, null);
//                    
//                    modelIns.put("makeLineNo"   	, null);
//                    modelIns.put("itemBarCd"   		, itemBarCd);
//                    modelIns.put("childCustLotNo"   , custLotNo);
//                    modelIns.put("mapItemQty"   	, orgOrdQty);
//                    modelIns.put("mapItemBarCd"   	, null);
//                    
//                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
//                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
//                    
//                    modelIns = (Map<String, Object>)dao.issueLabelOrdCom(modelIns);
//                    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
//                 }
//             }   
//             m.put("errCnt", 0);
//             m.put("MSG", MessageResolver.getMessage("save.success"));
//             
//         } catch(BizException be) {
//             m.put("errCnt", -1);
//             m.put("MSG", be.getMessage() );
//             
//         } catch(Exception e){
//        	 m.put("errCnt", 0);
//             throw e;
//         }
//         return m;
//     }  

    @Override
    public Map<String, Object> saveCustLotNoUpdate(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	try {
	    int temCnt = Integer.parseInt(model.get("selectIds").toString());

	    if (temCnt > 0) {
		for (int i = 0; i < temCnt; i++) {
		    int size = Integer.parseInt((String) model.get("ORG_ORD_QTY" + i));

		    String[] orgDetailNo = new String[size];
		    String[] orgOrdQty = new String[size];
		    String[] orgOrdNo = new String[size];
		    String[] orgItemCd = new String[size];
		    String[] custLotNo = new String[size];
		    String[] custId = new String[size];
		    String[] itemBarCd = new String[size];
		    String[] ritemId = new String[size];
		    String[] lcId = new String[size];
		    String[] issueDate = new String[size];

		    String[] mapItemBarCd = new String[size];
		    String[] makeLineNo = new String[size];

		    Map<String, Object> modelDS = new HashMap<String, Object>();
		    for (int k = 0; k < size; k++) {
			orgOrdQty[k] = "1";
			orgItemCd[k] = (String) model.get("ORG_ITEM_CD" + i);
			orgOrdNo[k] = (String) model.get("ORG_ORD_NO" + i);
			orgDetailNo[k] = (String) model.get("ORG_DETAIL_NO" + i);
			custId[k] = (String) model.get("CUST_ID" + i);
			itemBarCd[k] = (String) model.get("ITEM_BAR_CD" + i);
			ritemId[k] = (String) model.get("RITEM_ID" + i);
			lcId[k] = (String) model.get("LC_ID" + i);
			issueDate[k] = DateUtil.getCurrentDate("yyyyMMdd");

			makeLineNo[k] = "";

			mapItemBarCd[k] = (String) model.get("MAP_ITEM_BAR_CD" + i);
//	                   	System.out.println("MAP_ITEM_BAR_CD : "+(String)model.get("MAP_ITEM_BAR_CD"+i));

			// dao.getCustLotNo Map
			modelDS.put("lcId", (String) model.get("LC_ID" + i));
			modelDS.put("custId", (String) model.get("CUST_ID" + i));
			modelDS.put("orgItemCd", (String) model.get("ORG_ITEM_CD" + i));
			modelDS.put("orgGubun", (String) model.get("ORG_DETAIL_NO" + i) + k);

			custLotNo[k] = (String) dao.getCustLotNo(modelDS);
//                    	System.out.println("LOT_NO : " + custLotNo[k]);
		    }

		    // 프로시저 wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate
		    Map<String, Object> modelIns = new HashMap<String, Object>();
		    modelIns.put("epcCd", null);
		    modelIns.put("lcId", lcId);
		    modelIns.put("ordId", orgOrdNo);
		    modelIns.put("ordSeq", orgDetailNo);
		    modelIns.put("custId", custId);

		    modelIns.put("ritemId", ritemId);
		    modelIns.put("issueDate", issueDate);
		    modelIns.put("makeDate", null);
		    modelIns.put("itemBestEndDate", null);
		    modelIns.put("workQty", null);

		    modelIns.put("makeLineNo", makeLineNo);
		    modelIns.put("itemBarCd", itemBarCd);
		    modelIns.put("childCustLotNo", custLotNo);
		    modelIns.put("mapItemQty", orgOrdQty);
		    modelIns.put("mapItemBarCd", mapItemBarCd);

		    modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
		    modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));

		    modelIns = (Map<String, Object>) dao.issueLabelOrdCom(modelIns);
		    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")),
			    (String) modelIns.get("O_MSG_NAME"));
		}
	    }
	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (BizException be) {
	    m.put("errCnt", -1);
	    m.put("MSG", be.getMessage());

	} catch (Exception e) {
	    m.put("errCnt", 0);
	    throw e;
	}
	return m;
    }

    @Override
    public Map<String, Object> changOrderList(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	try {
	    int temCnt = Integer.parseInt(model.get("selectIds").toString());

//             if(temCnt > 0){
	    for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {

		Map<String, Object> modelDS = new HashMap<String, Object>();
		// dao.getCustLotNo Map
		// WHERE 조건
		modelDS.put("LC_ID", (String) model.get("LC_ID" + i));
		modelDS.put("ORG_ORD_NO", (String) model.get("ORG_ORD_NO" + i));
		modelDS.put("ORG_DETAIL_NO", (String) model.get("ORG_DETAIL_NO" + i));
		modelDS.put("ORG_ITEM_CD", (String) model.get("ORG_ITEM_CD" + i));
		modelDS.put("ORG_RITEM_ID", (String) model.get("ORG_RITEM_ID" + i));

		// UPDATE 조건
		modelDS.put("ORD_QTY", (String) model.get("ORD_QTY" + i));
		modelDS.put("RITEM_ID", (String) model.get("RITEM_ID" + i));
		modelDS.put("DLV_DT", (String) model.get("DLV_DT" + i));
		modelDS.put("ITEM_CD", (String) model.get("ITEM_CD" + i));

		dao.changOrderList(modelDS);

	    }
//             }   
	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (Exception e) {
	    m.put("errCnt", 0);
	    throw e;
	}
	return m;
    }

    @Override
    public Map<String, Object> saveInOrderOm(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();

	int rowCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
	String prefix = "I_";

	try {

	    String[] no = new String[rowCnt];

	    String[] outReqDt = new String[rowCnt];
	    String[] inReqDt = new String[rowCnt]; // 입고일
	    String[] custOrdNo = new String[rowCnt]; // 원주문번호
	    String[] custOrdSeq = new String[rowCnt]; // 순번
	    String[] trustCustCd = new String[rowCnt];

	    String[] transCustCd = new String[rowCnt];
	    String[] transCustTel = new String[rowCnt];
	    String[] transReqDt = new String[rowCnt];
	    String[] custCd = new String[rowCnt]; // 화주코드
	    String[] ordQty = new String[rowCnt]; // 수량

	    String[] uomCd = new String[rowCnt]; // UOM
	    String[] sdeptCd = new String[rowCnt];
	    String[] salePerCd = new String[rowCnt];
	    String[] carCd = new String[rowCnt]; // 차량번호
	    String[] drvNm = new String[rowCnt]; // 기사명

	    String[] dlvSeq = new String[rowCnt];
	    String[] drvTel = new String[rowCnt]; // 기사연락처
	    String[] custLotNo = new String[rowCnt]; // Lot No
	    String[] blNo = new String[rowCnt]; // BL No
	    String[] recDt = new String[rowCnt];

	    String[] outWhCd = new String[rowCnt];
	    String[] inWhCd = new String[rowCnt]; // 입고창고
	    String[] makeDt = new String[rowCnt]; // 제조일자
	    String[] timePeriodDay = new String[rowCnt];
	    String[] workYn = new String[rowCnt];

	    String[] rjType = new String[rowCnt];
	    String[] locYn = new String[rowCnt];
	    String[] confYn = new String[rowCnt];
	    String[] eaCapa = new String[rowCnt];
	    String[] inOrdWeight = new String[rowCnt]; // 주문중량

	    String[] itemCd = new String[rowCnt]; // 상품코드
	    String[] itemNm = new String[rowCnt]; // 상품명
	    String[] transCustNm = new String[rowCnt];
	    String[] transCustAddr = new String[rowCnt];
	    String[] transEmpNm = new String[rowCnt];

	    String[] remark = new String[rowCnt]; // 비고
	    String[] transZipNo = new String[rowCnt];
	    String[] etc2 = new String[rowCnt]; // 비고2
	    String[] unitAmt = new String[rowCnt]; // 단가
	    String[] transBizNo = new String[rowCnt];

	    String[] inCustAddr = new String[rowCnt]; // 입고처주소(?)
	    String[] inCustCd = new String[rowCnt]; // 입고처 코드
	    String[] inCustNm = new String[rowCnt]; // 입고처 명
	    String[] inCustTel = new String[rowCnt]; // 입고담당자번호(?)
	    String[] inCustEmpNm = new String[rowCnt]; // 입고담당자(?)

	    String[] expiryDate = new String[rowCnt];
	    String[] salesCustNm = new String[rowCnt];
	    String[] zip = new String[rowCnt];
	    String[] addr = new String[rowCnt];
	    String[] addr2 = new String[rowCnt];

	    String[] phone1 = new String[rowCnt];
	    String[] etc1 = new String[rowCnt]; // 비고1(CROP)
	    String[] unitNo = new String[rowCnt]; // UNIT 번호
	    String[] timeDate = new String[rowCnt]; // 상품유효기간
	    String[] timeDateEnd = new String[rowCnt]; // 상품유효기간만료일

	    String[] timeUseEnd = new String[rowCnt]; // 소비가한만료일
	    String[] phone2 = new String[rowCnt]; // 고객전화번호2
	    String[] buyCustNm = new String[rowCnt]; // 주문자명
	    String[] buyPhone1 = new String[rowCnt]; // 주문자전화번호1
	    String[] salesCompanyNm = new String[rowCnt]; // salesCompanyNm

	    String[] ordDegree = new String[rowCnt]; // 주문등록차수
	    String[] bizCond = new String[rowCnt]; // 업태
	    String[] bizType = new String[rowCnt]; // 업종
	    String[] bizNo = new String[rowCnt]; // 사업자등록번호
	    String[] custType = new String[rowCnt]; // 화주타입

	    String[] dataSenderNm = new String[rowCnt]; // 쇼핑몰
	    String[] legacyOrgOrdNo = new String[rowCnt]; // 사방넷주문번호
	    String[] custSeq = new String[rowCnt]; // 템플릿구분
	    String[] lotType = new String[rowCnt]; // 재고lot속성
	    String[] ownerCd = new String[rowCnt]; // 재고소유자코드

	    String[] etd = new String[rowCnt]; // ETD
	    String[] eta = new String[rowCnt]; // ETA
	    String[] exchangerate = new String[rowCnt]; // 환율
	    String[] ordWeight = new String[rowCnt]; // 주문중량 (중복 : inOrdWeight?)
	    String[] cntrNo = new String[rowCnt]; // 컨테이너번호

	    String[] refNo = new String[rowCnt]; // 참조번호
	    String[] itemBarcode = new String[rowCnt]; // 상품바코드 (다중바코드 혹은 소분상품바코드)
	    String[] fromTimeZone = new String[rowCnt]; // 입항일자
	    String[] ordSubType = new String[rowCnt]; // 주문상세타입
	    String[] ccCd = new String[rowCnt]; // 화물관리번호

	    String[] um = new String[rowCnt]; // 단가 (중복: unitAmt?)
	    String[] cntrType = new String[rowCnt]; // 컨테이너구분
	    String[] locCd = new String[rowCnt]; // 로케이션

	    for (int rowIdx = 0; rowIdx < rowCnt; rowIdx++) {
		no[rowIdx] = Integer.toString(rowIdx + 1);

		outReqDt[rowIdx] = "";
		inReqDt[rowIdx] = (model.get("calInDt").toString().isEmpty()) ? ""
			: model.get("calInDt").toString().replace("-", "");
		custOrdNo[rowIdx] = model.get("vrOrgOrdId").toString();
		custOrdSeq[rowIdx] = Integer.toString((rowIdx + 1));
		trustCustCd[rowIdx] = "";

		transCustCd[rowIdx] = "";
		transCustTel[rowIdx] = "";
		transReqDt[rowIdx] = "";
		custCd[rowIdx] = model.get("vrSrchCustCd").toString();
		ordQty[rowIdx] = model.get(prefix + "IN_WORK_ORD_QTY" + rowIdx).toString();

		uomCd[rowIdx] = model.get(prefix + "UOM_CD" + rowIdx).toString();
		sdeptCd[rowIdx] = "";
		salePerCd[rowIdx] = "";
		carCd[rowIdx] = "";
		drvNm[rowIdx] = "";

		dlvSeq[rowIdx] = "";
		drvTel[rowIdx] = "";
		custLotNo[rowIdx] = model.get(prefix + "CUST_LOT_NO" + rowIdx).toString();
		blNo[rowIdx] = model.get("vrBlNo").toString();
		recDt[rowIdx] = "";

		outWhCd[rowIdx] = "";
		inWhCd[rowIdx] = model.get("vrSrchWhCd").toString();
		makeDt[rowIdx] = (model.get("makeDt").toString().isEmpty()) ? ""
			: model.get("makeDt").toString().replace("-", "");
		timePeriodDay[rowIdx] = "";
		workYn[rowIdx] = "";

		rjType[rowIdx] = "";
		locYn[rowIdx] = "";
		confYn[rowIdx] = "";
		eaCapa[rowIdx] = "";
		inOrdWeight[rowIdx] = model.get(prefix + "ORD_WEIGHT" + rowIdx).toString();

		itemCd[rowIdx] = model.get(prefix + "ITEM_CODE" + rowIdx).toString();
		itemNm[rowIdx] = model.get(prefix + "RITEM_NM" + rowIdx).toString();
		transCustNm[rowIdx] = "";
		transCustAddr[rowIdx] = "";
		transEmpNm[rowIdx] = "";

		remark[rowIdx] = "";
		transZipNo[rowIdx] = "";
		etc2[rowIdx] = "";
		unitAmt[rowIdx] = model.get(prefix + "UM" + rowIdx).toString();
		transBizNo[rowIdx] = "";

		inCustAddr[rowIdx] = model.get("vrInCustAddr").toString();
		inCustCd[rowIdx] = model.get("vrInCustCd").toString();
		inCustNm[rowIdx] = model.get("vrInCustNm").toString();
		inCustTel[rowIdx] = model.get("vrInCustTel").toString();
		inCustEmpNm[rowIdx] = model.get("vrInCustEmpNm").toString();

		expiryDate[rowIdx] = "";
		salesCustNm[rowIdx] = "";
		zip[rowIdx] = "";
		addr[rowIdx] = "";
		addr2[rowIdx] = "";

		phone1[rowIdx] = "";
		etc1[rowIdx] = model.get(prefix + "ETC1" + rowIdx).toString();
		unitNo[rowIdx] = model.get(prefix + "UNIT_NO" + rowIdx).toString();
		timeDate[rowIdx] = "";
		timeDateEnd[rowIdx] = "";

		timeUseEnd[rowIdx] = "";
		phone2[rowIdx] = "";
		buyCustNm[rowIdx] = "";
		buyPhone1[rowIdx] = "";
		salesCompanyNm[rowIdx] = "";

		ordDegree[rowIdx] = "";
		bizCond[rowIdx] = "";
		bizType[rowIdx] = "";
		bizNo[rowIdx] = "";
		custType[rowIdx] = "";

		dataSenderNm[rowIdx] = "";
		legacyOrgOrdNo[rowIdx] = "";
		custSeq[rowIdx] = "";
		lotType[rowIdx] = CodeParser.getCodeName("LOT02", model.get("vrOrderType").toString());
		ownerCd[rowIdx] = "";

		etd[rowIdx] = (model.get("etdDt").toString().isEmpty()) ? ""
			: model.get("etdDt").toString().replace("-", "");
		eta[rowIdx] = (model.get("etaDt").toString().isEmpty()) ? ""
			: model.get("etaDt").toString().replace("-", "");
		exchangerate[rowIdx] = "";
		ordWeight[rowIdx] = model.get(prefix + "ORD_WEIGHT" + rowIdx).toString();
		cntrNo[rowIdx] = model.get("vrCntrNoM").toString();

		refNo[rowIdx] = "";
		itemBarcode[rowIdx] = model.get(prefix + "ITEM_BARCODE" + rowIdx).toString();
		fromTimeZone[rowIdx] = (model.get("fromTimeZoneDt").toString().isEmpty()) ? ""
			: model.get("fromTimeZoneDt").toString().replace("-", "");
		ordSubType[rowIdx] = model.get("vrSrchOrderPhase").toString();
		ccCd[rowIdx] = model.get("vrccCdNo").toString();

		um[rowIdx] = model.get(prefix + "UM" + rowIdx).toString();
		cntrType[rowIdx] = "";
		locCd[rowIdx] = "";

	    }

//        	 dao.changOrderList(modelDS);
	    Map<String, Object> modelIns = new HashMap<String, Object>();

	    modelIns.put("vrOrdType", "01"); // 입고
	    modelIns.put("no", no);
	    modelIns.put("reqDt", inReqDt);
	    modelIns.put("custOrdNo", custOrdNo);
	    modelIns.put("custOrdSeq", custOrdSeq); // 5

	    modelIns.put("trustCustCd", trustCustCd);
	    modelIns.put("transCustCd", transCustCd);
	    modelIns.put("transCustTel", transCustTel);
	    modelIns.put("transReqDt", transReqDt);
	    modelIns.put("custCd", custCd); // 10

	    modelIns.put("ordQty", ordQty);
	    modelIns.put("uomCd", uomCd);
	    modelIns.put("sdeptCd", sdeptCd);
	    modelIns.put("salePerCd", salePerCd);
	    modelIns.put("carCd", carCd); // 15

	    modelIns.put("drvNm", drvNm);
	    modelIns.put("dlvSeq", dlvSeq);
	    modelIns.put("drvTel", drvTel);
	    modelIns.put("custLotNo", custLotNo);
	    modelIns.put("blNo", blNo); // 20

	    modelIns.put("recDt", recDt);
	    modelIns.put("whCd", inWhCd);
	    modelIns.put("makeDt", makeDt);
	    modelIns.put("timePeriodDay", timePeriodDay);
	    modelIns.put("workYn", workYn); // 25

	    modelIns.put("rjType", rjType);
	    modelIns.put("locYn", locYn);
	    modelIns.put("confYn", confYn);
	    modelIns.put("eaCapa", eaCapa);
	    modelIns.put("inOrdWeight", inOrdWeight); // 30

	    modelIns.put("itemCd", itemCd);
	    modelIns.put("itemNm", itemNm);
	    modelIns.put("transCustNm", transCustNm);
	    modelIns.put("transCustAddr", transCustAddr);
	    modelIns.put("transEmpNm", transEmpNm); // 35

	    modelIns.put("remark", remark);
	    modelIns.put("transZipNo", transZipNo);
	    modelIns.put("etc2", etc2);
	    modelIns.put("unitAmt", unitAmt);
	    modelIns.put("transBizNo", transBizNo); // 40

	    modelIns.put("inCustAddr", inCustAddr);
	    modelIns.put("inCustCd", inCustCd);
	    modelIns.put("inCustNm", inCustNm);
	    modelIns.put("inCustTel", inCustTel);
	    modelIns.put("inCustEmpNm", inCustEmpNm); // 45

	    modelIns.put("expiryDate", expiryDate);
	    modelIns.put("salesCustNm", salesCustNm);
	    modelIns.put("zip", zip);
	    modelIns.put("addr", addr);
	    modelIns.put("addr2", addr2); // 50

	    modelIns.put("phone1", phone1);
	    modelIns.put("etc1", etc1);
	    modelIns.put("unitNo", unitNo);
	    modelIns.put("phone2", phone2);
	    modelIns.put("buyCustNm", buyCustNm); // 55

	    modelIns.put("buyPhone1", buyPhone1);
	    modelIns.put("salesCompanyNm", salesCompanyNm);
	    modelIns.put("ordDegree", ordDegree);
	    modelIns.put("bizCond", bizCond);
	    modelIns.put("bizType", bizType); // 60

	    modelIns.put("bizNo", bizNo);
	    modelIns.put("custType", custType);
	    modelIns.put("dataSenderNm", dataSenderNm);
	    modelIns.put("legacyOrgOrdNo", legacyOrgOrdNo);
	    modelIns.put("custSeq", custSeq); // 65

	    modelIns.put("lotType", lotType);
	    modelIns.put("ownerCd", ownerCd);
	    modelIns.put("etd", etd);
	    modelIns.put("eta", eta);
	    modelIns.put("exchangerate", exchangerate); // 70

	    modelIns.put("ordWeight", ordWeight);
	    modelIns.put("cntrNo", cntrNo);
	    modelIns.put("refNo", refNo);
	    modelIns.put("itemBarCode", itemBarcode);
	    modelIns.put("fromTimeZone", fromTimeZone); // 75

	    modelIns.put("ordSubType", ordSubType);
	    modelIns.put("ccCd", ccCd);
	    modelIns.put("um", um);
	    modelIns.put("cntrType", cntrType);
	    modelIns.put("locCd", locCd); // 80

	    modelIns.put("LC_ID", model.get("SS_SVC_NO"));
	    modelIns.put("USER_NO", model.get("SS_USER_NO"));
	    modelIns.put("WORK_IP", model.get("SS_CLIENT_IP"));
	    // Total : 83

	    modelIns = (Map<String, Object>) dao.saveExcelOrderB2O(modelIns);

	    m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));

	} catch (Exception e) {
	    m.put("errCnt", 0);
	    throw e;
	}
	return m;
    }

    @Override
    public Map<String, Object> saveInOrderVX_V2(Map<String, Object> model) throws Exception {
	Map<String, Object> m = new HashMap<String, Object>();
	// log.info(model);
	try {
	    int tmpCnt = Integer.parseInt(model.get("I_selectIds").toString());

	    String[] key = new String[tmpCnt];
	    String[] reqDt = new String[tmpCnt];
	    String[] custOrdNo = new String[tmpCnt];
	    String[] custOrdSeq = new String[tmpCnt];
	    String[] trustCustCd = new String[tmpCnt];
	    String[] transCustCd = new String[tmpCnt];
	    String[] transCustTel = new String[tmpCnt];
	    String[] transReqDt = new String[tmpCnt];
	    String[] custCd = new String[tmpCnt];
	    String[] ordQty = new String[tmpCnt];
	    String[] uomCd = new String[tmpCnt];
	    String[] sdeptCd = new String[tmpCnt];
	    String[] salePerCd = new String[tmpCnt];
	    String[] carCd = new String[tmpCnt];
	    String[] drvNm = new String[tmpCnt];
	    String[] dlvSeq = new String[tmpCnt];
	    String[] drvTel = new String[tmpCnt];
	    String[] custLotNo = new String[tmpCnt];
	    String[] blNo = new String[tmpCnt];
	    String[] recDt = new String[tmpCnt];
	    String[] whCd = new String[tmpCnt];
	    String[] makeDt = new String[tmpCnt];
	    String[] timePeriodDay = new String[tmpCnt];
	    String[] workYn = new String[tmpCnt];
	    String[] rjType = new String[tmpCnt];
	    String[] locYn = new String[tmpCnt];
	    String[] confYn = new String[tmpCnt];
	    String[] eaCapa = new String[tmpCnt];
	    String[] inOrdWeight = new String[tmpCnt];
	    String[] itemCd = new String[tmpCnt];
	    String[] itemNm = new String[tmpCnt];
	    String[] transCustNm = new String[tmpCnt];
	    String[] transCustAddr = new String[tmpCnt];
	    String[] transEmpNm = new String[tmpCnt];
	    String[] remark = new String[tmpCnt];
	    String[] transZipNo = new String[tmpCnt];
	    String[] etc2 = new String[tmpCnt];
	    String[] unitAmt = new String[tmpCnt];
	    String[] transBizNo = new String[tmpCnt];
	    String[] inCustAddr = new String[tmpCnt];
	    String[] inCustCd = new String[tmpCnt];
	    String[] inCustNm = new String[tmpCnt];
	    String[] inCustTel = new String[tmpCnt];
	    String[] inCustEmpNm = new String[tmpCnt];
	    String[] expiryDate = new String[tmpCnt];
	    String[] salesCustNm = new String[tmpCnt];
	    String[] zip = new String[tmpCnt];
	    String[] addr = new String[tmpCnt];
	    String[] phone1 = new String[tmpCnt];
	    String[] etc1 = new String[tmpCnt];
	    String[] unitNo = new String[tmpCnt];
	    String[] salesCompanyNm = new String[tmpCnt];
	    String[] shipmentNo = new String[tmpCnt];
	    String[] lotType = new String[tmpCnt];
	    String[] ownerCd = new String[tmpCnt];

	    for (int i = 0; i < tmpCnt; i++) {

		String KEY = "";
		String OUT_REQ_DT = "";
		String CUST_ORD_NO = "";
		String CUST_ORD_SEQ = "";
		String TRUST_CUST_CD = "";
		String TRANS_CUST_CD = "";
		String TRANS_CUST_TEL = "";
		String TRANS_REQ_DT = "";
		String CUST_CD = "";
		String ORD_QTY = "";
		String UOM_CD = "";
		String SDEPT_CD = "";
		String SALE_PER_CD = "";
		String CAR_CD = "";
		String DRV_NM = "";
		String DLV_SEQ = "";
		String DRV_TEL = "";
		String CUST_LOT_NO = "";
		String BL_NO = "";
		String REC_DT = "";
		String WH_CD = "";
		String MAKE_DT = "";
		String TIME_PERIOD_DAY = "";
		String WORK_YN = "";
		String RJ_TYPE = "";
		String LOC_YN = "";
		String CONF_YN = "";
		String EA_CAPA = "";
		String IN_ORD_WEIGHT = "";
		String ITEM_CD = "";
		String ITEM_NM = "";
		String TRANS_CUST_NM = "";
		String TRANS_CUST_ADDR = "";
		String TRANS_EMP_NM = "";
		String REMARK = "";
		String TRANS_ZIP_NO = "";
		String ETC2 = "";
		String UNIT_AMT = "";
		String TRANS_BIZ_NO = "";
		String IN_CUST_ADDR = "";
		String IN_CUST_CD = "";
		String IN_CUST_NM = "";
		String IN_CUST_TEL = "";
		String IN_CUST_EMP_NM = "";
		String EXPIRY_DATE = "";
		String SALES_CUST_NM = "";
		String ZIP = "";
		String ADDR = "";
		String PHONE_1 = "";
		String ETC1 = "";
		String UNIT_NO = "";
		String SALES_COMPANY_NM = "";
		String SHIPMENT_NO = "";
		String LOT_TYPE = "";
		String OWNER_CD = "";

		// ==== 실제 들어가는 데이터
		key[i] = KEY;
		reqDt[i] = ((String) model.get("I_IN_DLV_DT" + i)).replace("-", "");
		custOrdNo[i] = (String) model.get("I_IN_ORG_ORD_ID" + i);
		custOrdSeq[i] = (String) model.get("I_IN_ORD_DETAIL_NO" + i);
		trustCustCd[i] = TRUST_CUST_CD;
		transCustCd[i] = TRANS_CUST_CD;
		transCustTel[i] = TRANS_CUST_TEL;
		transReqDt[i] = TRANS_REQ_DT;
		custCd[i] = (String) model.get("I_CUST_CD" + i);
		ordQty[i] = (String) model.get("I_IN_ORD_QTY" + i);
		uomCd[i] = (String) model.get("I_IN_UOM_CD" + i);
		sdeptCd[i] = SDEPT_CD;
		salePerCd[i] = SALE_PER_CD;
		carCd[i] = CAR_CD;
		drvNm[i] = DRV_NM;
		dlvSeq[i] = DLV_SEQ;
		drvTel[i] = DRV_TEL;
		custLotNo[i] = CUST_LOT_NO;
		blNo[i] = BL_NO;
		recDt[i] = REC_DT;
		whCd[i] = WH_CD;
		makeDt[i] = MAKE_DT;
		timePeriodDay[i] = TIME_PERIOD_DAY;
		workYn[i] = WORK_YN;
		rjType[i] = RJ_TYPE;
		locYn[i] = LOC_YN;
		confYn[i] = CONF_YN;
		eaCapa[i] = EA_CAPA;
		inOrdWeight[i] = IN_ORD_WEIGHT;
		itemCd[i] = (String) model.get("I_RITEM_CD" + i);
		itemNm[i] = (String) model.get("I_RITEM_NM" + i);
		transCustNm[i] = TRANS_CUST_NM;
		transCustAddr[i] = TRANS_CUST_ADDR;
		transEmpNm[i] = TRANS_EMP_NM;
		remark[i] = (String) model.get("I_ORD_DESC" + i);
		;
		transZipNo[i] = TRANS_ZIP_NO;
		etc2[i] = ETC2;
		unitAmt[i] = UNIT_AMT;
		transBizNo[i] = TRANS_BIZ_NO;
		inCustAddr[i] = IN_CUST_ADDR;
		inCustCd[i] = IN_CUST_CD;
		inCustNm[i] = IN_CUST_NM;
		inCustTel[i] = IN_CUST_TEL;
		inCustEmpNm[i] = IN_CUST_EMP_NM;
		expiryDate[i] = EXPIRY_DATE;
		salesCustNm[i] = SALES_CUST_NM;
		zip[i] = ZIP;
		addr[i] = ADDR;
		phone1[i] = PHONE_1;
		etc1[i] = ETC1;
		unitNo[i] = UNIT_NO;
		salesCompanyNm[i] = SALES_COMPANY_NM;
		shipmentNo[i] = SHIPMENT_NO;
		lotType[i] = LOT_TYPE;
		ownerCd[i] = OWNER_CD;

	    }

	    // 프로시져에 보낼것들 다담는다
	    Map<String, Object> modelIns = new HashMap<String, Object>();

	    modelIns.put("vrOrdType", "01");
	    modelIns.put("vrOrdSubtype", "20");
	    modelIns.put("key", key);
	    modelIns.put("reqDt", reqDt);
	    modelIns.put("custOrdNo", custOrdNo);

	    modelIns.put("custOrdSeq", custOrdSeq);
	    modelIns.put("trustCustCd", trustCustCd);
	    modelIns.put("transCustCd", transCustCd);
	    modelIns.put("transCustTel", transCustTel);
	    modelIns.put("transReqDt", transReqDt);

	    modelIns.put("custCd", custCd);
	    modelIns.put("ordQty", ordQty);
	    modelIns.put("uomCd", uomCd);
	    modelIns.put("sdeptCd", sdeptCd);
	    modelIns.put("salePerCd", salePerCd);

	    modelIns.put("carCd", carCd);
	    modelIns.put("drvNm", drvNm);
	    modelIns.put("dlvSeq", dlvSeq);
	    modelIns.put("drvTel", drvTel);
	    modelIns.put("custLotNo", custLotNo);

	    modelIns.put("blNo", blNo);
	    modelIns.put("recDt", recDt);
	    modelIns.put("whCd", whCd);
	    modelIns.put("makeDt", makeDt);
	    modelIns.put("timePeriodDay", timePeriodDay);

	    modelIns.put("workYn", workYn);
	    modelIns.put("rjType", rjType);
	    modelIns.put("locYn", locYn);
	    modelIns.put("confYn", confYn);
	    modelIns.put("eaCapa", eaCapa);

	    modelIns.put("inOrdWeight", inOrdWeight);
	    modelIns.put("itemCd", itemCd);
	    modelIns.put("itemNm", itemNm);
	    modelIns.put("transCustNm", transCustNm);
	    modelIns.put("transCustAddr", transCustAddr);

	    modelIns.put("transEmpNm", transEmpNm);
	    modelIns.put("remark", remark);
	    modelIns.put("transZipNo", transZipNo);
	    modelIns.put("etc2", etc2);
	    modelIns.put("unitAmt", unitAmt);

	    modelIns.put("transBizNo", transBizNo);
	    modelIns.put("inCustAddr", inCustAddr);
	    modelIns.put("inCustCd", inCustCd);
	    modelIns.put("inCustNm", inCustNm);
	    modelIns.put("inCustTel", inCustTel);

	    modelIns.put("inCustEmpNm", inCustEmpNm);
	    modelIns.put("expiryDate", expiryDate);
	    modelIns.put("salesCustNm", salesCustNm);
	    modelIns.put("zip", zip);
	    modelIns.put("addr", addr);

	    modelIns.put("phone1", phone1);
	    modelIns.put("etc1", etc1);
	    modelIns.put("unitNo", unitNo);
	    modelIns.put("salesCompanyNm", salesCompanyNm);
	    modelIns.put("shipmentNo", shipmentNo);

	    modelIns.put("lotType", lotType);
	    modelIns.put("ownerCd", ownerCd);

	    modelIns.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
	    modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
	    modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));

	    modelIns = (Map<String, Object>) dao2.saveExcelOrderOP(modelIns);
	    ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")),
		    (String) modelIns.get("O_MSG_NAME"));

	    m.put("MSG", MessageResolver.getMessage("save.success"));
	    m.put("MSG_ORA", "");
	    m.put("errCnt", 0);

	} catch (Exception e) {
	    if (log.isErrorEnabled()) {
		log.error("Fail to get result :", e);
	    }
	    m.put("errCnt", 1);
	    m.put("MSG", MessageResolver.getMessage("save.error"));
	}
	return m;
    }

    /**
     * Method ID : omImgList Method 설명 : 작성자 : yhku
     * 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> omImgList(Map<String, Object> model) throws Exception {
	Map<String, Object> map = new HashMap<String, Object>();
	String srchKey = (String) model.get("srchKey");
	if (srchKey.equals("IMG")) {
	    if (model.get("page") == null) {
		model.put("pageIndex", "1");
	    } else {
		model.put("pageIndex", model.get("page"));
	    }

	    if (model.get("rows") == null) {
		model.put("pageSize", "20");
	    } else {
		model.put("pageSize", model.get("rows"));
	    }

	    map.put("IMG", dao.omImgList(model));
	}
	return map;
    }
}
