package com.logisall.winus.wmsom.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.impl.WMSIF902Dao;
import com.logisall.winus.wmsom.service.OutOrderRegService;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
public class OutOrderRegServiceImpl implements OutOrderRegService{
	@Autowired private OutOrderRegDao outOrderRegDao;
	
//	@Autowired private WMSMS100Dao wmsms100Dao;
	
	@Autowired private WMSIF902Dao wmsif902Dao;
	
//	@Autowired private WMSCM011Service transCustService;

	@Override
	public Map<String, Object> outOrderCheck(Map<String, Object> model, Map<String, Object> reqMap) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		setSessionParam(model, reqMap);
		try {
			String custId = (String) reqMap.get("vrSrchCustId");
			if(custId == null ) {
				throw new BizException("화주를 선택해 주세요.");
			}
			model.put("CUST_ID", reqMap.get("vrSrchCustId"));
			List<Map<String, Object>> orderList = (List<Map<String, Object>>) reqMap.get("orderList");
			checkDuplicateOrders(model, orderList);
			checkOrderItem(model, orderList);
			checkTransCust(model, orderList);
			
			resultMap.put("errCnt", 0);
			resultMap.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (BizException e) {
			resultMap.put("errCnt", 1);
			resultMap.put("MSG", e.getMessage());
			throw e;
		} 
		return resultMap;
	}

	/**
	 * 출고주문 저장
	 * @throws Exception 
	 * */
	@Override
	public Map<String, Object> saveOutOrder(Map<String, Object> model, Map<String, Object> reqMap) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
//			List<Map<String, Object>> orderList = (List<Map<String, Object>>) reqMap.get("orderList");
			setSessionParam(model, reqMap);
			outOrderCheck(model, reqMap);
			List<Map<String, Object>> orderList = mappingOrderParams(model, reqMap);
			wmsif902Dao.insertBulkOrder(orderList);
		
			resultMap.put("errCnt", 0);
			resultMap.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (BizException | NumberFormatException e) {
			resultMap.put("errCnt", 1);
			resultMap.put("MSG", e.getMessage());
		} 

		return resultMap;
	}

	
	private List<Map<String, Object>> mappingOrderParams(Map<String, Object> model, Map<String, Object> reqMap) {
		List<Map<String, Object>> outOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
		String winusCustCd = (String )reqMap.get("vrSrchCustCd");
		String winusCustId = (String )reqMap.get("vrSrchCustId");
		String ordNoDivType = (String)reqMap.get("ordNoDivType"); //출고번호 생성 구분 기분
		String orderRegist = "100"; /* 주문등록 */
		String outboundStat = "02"; /* 출고 */
		String normalOrder = "30"; /* 정상출고 */
		String moveOrder = "134"; /* 이동출고 */
		String ordTypeB2B = "B2B"; /* 주문유형 B2B */
		String ifYn = "N";
		List<Map<String, Object>> outOrderGroupList = CommonUtil.createGroupByKey(ordNoDivType, outOrderList);
		for (Map<String, Object> outOrder : outOrderGroupList) {
			setSessionParam(model, outOrder);
			List<Map<String, Object>> children = (List<Map<String, Object>>) outOrder.get("children");

			model.put("pageSize", "10000");
			model.put("vrSrchCustCd", winusCustCd);

			outOrder.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
			outOrder.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
			outOrder.put("CUST_ID", winusCustId);
			outOrder.put("WORK_STAT", orderRegist);
			outOrder.put("ORD_TYPE", outboundStat);
			outOrder.put("ORD_SUBTYPE", normalOrder);
//			outOrder.put("OUT_REQ_DT", outOrder.get("INDI_DATE").toString());
			outOrder.put("ORD_BIZ_TYPE", ordTypeB2B);
			
			int ordSeq = 1;
			for (Map<String, Object> orderDetail : children) {
				orderDetail.putAll(model);
				setSessionParam(model, orderDetail);
				String rItemId = (String) orderDetail.get("RITEM_ID");		/* WINUS 상품고유코드 */
				Integer outOrdQty = calculateOutOrdQty(orderDetail);
				
				Integer workOrdQty = (Integer) orderDetail.get("OUT_WORK_ORD_QTY");/* 환산수량(EA) */
				String registOrderStatus = "100"; // 주문등록
				orderDetail.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
				orderDetail.put("CUST_ID", winusCustId);
				orderDetail.put("ORD_SEQ", ordSeq++);
				orderDetail.put("RITEM_ID", rItemId);
//					orderDetail.put("REAL_BOX_QTY", indiQty);
				orderDetail.put("WORK_STAT", registOrderStatus);
//					orderDetail.put("UNIT_AMT", unitAmt);
//					orderDetail.put("AMT", amt);
//					orderDetail.put("ADD_AMT", addAmt);
//					orderDetail.put("OUT_ORD_UOM_ID", repUomId);
//					orderDetail.put("OUT_WORK_UOM_ID", orderUomId);
				orderDetail.put("OUT_ORD_QTY", outOrdQty);
				orderDetail.put("OUT_WORK_ORD_QTY", workOrdQty);
//					orderDetail.put("ITEM_BEST_DATE", datTo);
//					orderDetail.put("ORDER_DESC", remark);
//				}
			}
		}
		
		return outOrderGroupList;
	}

	
	/*
	 * UOM 환산수량 계산
	 * */
	private Integer calculateOutOrdQty(Map<String, Object> orderDetail) {
		Integer workOrdQty = (Integer) orderDetail.get("OUT_WORK_ORD_QTY");/* 작업수량 */
		String uomCode = (String) orderDetail.get("OUT_WORK_UOM_CD"); /* 단위(UOM) */
		
		// UOM 코드와 변환 수량 매핑, ibatis Number 타입은 BigDecimal임
        Map<String, BigDecimal> eaToQtyMap = new HashMap<>();
        eaToQtyMap.put("EA", (BigDecimal) orderDetail.get("EA_EA_QTY"));
        eaToQtyMap.put("BOX", (BigDecimal) orderDetail.get("EA_BOX_QTY"));
        eaToQtyMap.put("PLT", (BigDecimal) orderDetail.get("EA_PLT_QTY"));
        eaToQtyMap.put("BOL", (BigDecimal) orderDetail.get("EA_BOL_QTY"));
		
        Integer eaToQty = eaToQtyMap.get(uomCode).intValue();
        
        if(workOrdQty == null || eaToQty == null) {
        	throw new NumberFormatException("작업수량 또는 환산수량 오류.");
        }
        
		return workOrdQty * eaToQty;
	}

	private void setSessionParam(Map<String, Object> model, Map<String, Object> reqMap) {
		String lc_id = model.get("SS_SVC_NO").toString();
		reqMap.put("SS_SVC_NO", lc_id);
		String user_no = model.get("SS_USER_NO").toString();
		reqMap.put("SS_USER_NO", user_no);
	}
	
	/*
	 * 거래처정보 등록되어 있는지 체크한다.
	 */
	private void checkTransCust(Map<String, Object> model, List<Map<String, Object>> orderList) throws Exception{
		String warehouseId = (String)model.get("SS_SVC_NO");
		//주문별 중복상품 코드 제거.
		List<String> custList = deduplicationColumn(orderList, "TRANS_CUST_CD");
		Map<String, Object> custMap = new HashMap<String, Object>();
    	setSessionParam(model, custMap);
    	
    	model.put("S_LC_ALL", "LC_ALL");
		model.put("S_LC_ID", warehouseId);
		model.put("S_CUST_CD_LIST", custList);
		model.put("S_PAGE_NUM", 1);
		model.put("S_PAGE_LEN", 5000);
		List<Map<String, Object>> chkCustList = outOrderRegDao.selectAllCustList(model);
		
    	for (String custCode : custList) {
			boolean isCustReg = isExistMapValue(custCode ,"CUST_CD", chkCustList);
			if(!isCustReg) {
				throw new BizException(custCode+ "는 미등록 거래처입니다.");
			}
		}
    	
    	mappingCustId(orderList, chkCustList);
	}
	
	
	/*
	 * 원주문번호가 등록되어 있는지 체크한다.
	 */
	private void checkDuplicateOrders(Map<String, Object> model, List<Map<String, Object>> orderList) throws BizException {
		//주문별 중복 원주문코드 제거.
		List<String> orgOrdIdList = deduplicationColumn(orderList, "ORG_ORD_ID");
		List<List<String>> orgOrdIdChunks = CommonUtil.splitList(orgOrdIdList, 1000);
		//in절 1000개씩 끊어서 주문정보 등록되어 있는지 체크
        for (List<String> orgOrdIdChunk : orgOrdIdChunks) {
        	Map<String, Object> orderMap = new HashMap<String, Object>();
        	orderMap.putAll(model);
        	orderMap.put("orgOrdIdList", orgOrdIdChunk);
        	setSessionParam(model, orderMap);
        	List<Map<String, Object>> checkOrderList = outOrderRegDao.selectOrderList(orderMap);
        	for (String orgOrdid : orgOrdIdList) {
				boolean isExistOrder = isExistMapValue(orgOrdid, "ORG_ORD_ID", checkOrderList);
				
				if(isExistOrder) {
					throw new BizException(orgOrdid+ "는 이미 등록된 주문입니다.");
				}
			}
        }
	}
	
	/*
	 * 상품의 중량정보가 등록되어 있는지 체크한다.
	 */
	private void checkOrderItem(Map<String, Object> model, List<Map<String, Object>> orderList) throws BizException{
		//주문별 중복상품 코드 제거.
		List<String> itemList = deduplicationColumn(orderList, "ITEM_CD");
		List<List<String>> orderItemChunks = CommonUtil.splitList(itemList, 1000);
		//아이템 1000개씩 끊어서 상품정보 등록되어 있는지 체크
        for (List<String> orderItemChunk : orderItemChunks) {
        	Map<String, Object> itemMap = new HashMap<String, Object>();
        	itemMap.putAll(model);
        	itemMap.put("itemList", orderItemChunk);
        	setSessionParam(model, itemMap);
        	List<Map<String, Object>> checkitemList = outOrderRegDao.selectItemList(itemMap);
        	for (String itemCode : itemList) {
				boolean isItemFlag = isExistMapValue(itemCode, "ITEM_CODE", checkitemList);
				
				if(!isItemFlag) {
					throw new BizException(itemCode+ "는 미등록 상품입니다.");
				}
			}
        	mappingItemId(orderList, checkitemList);
        }
	}

	/*
	 * 주문 컬럼 중 중복 항목컬럼 제거 후 리턴
	 * */
	private List<String> deduplicationColumn(List orderList, String key){
		Set<String> items = new HashSet<String>();
		List<String> itemList = new ArrayList<String>();
		for (Object order : orderList) {
			Map<String, Object> orderMap = (Map<String, Object>) order;
			items.add((String) orderMap.get(key));
		}
		for (String item : items) {
			itemList.add(item);
		}
		return itemList;
	}
	
	
	private void mappingItemId(List<Map<String, Object>> orderList ,List<Map<String, Object>> checkitemList) {
		// checkitemList를 ITEM_CODE별 그루핑
	    Map<String, Map<String, Object>> itemMappingMap = new HashMap<>();
	    for (Map<String, Object> item : checkitemList) {
	        String itemCode = (String) item.get("ITEM_CODE");
	        itemMappingMap.put(itemCode, item);
	    }
		for (Map<String, Object> order : orderList) {
    		String itemCode = (String) order.get("ITEM_CD");
    		 // itemMappingMap에서 데이터 검색 및 매핑
	        Map<String, Object> itemMap = itemMappingMap.get(itemCode);
	        if (itemMap != null) {
	        	order.put("RITEM_ID", itemMap.get("RITEM_ID"));
	 			order.put("ITEM_NM", itemMap.get("ITEM_NM"));
	 			order.put("OUT_ORD_UOM_ID", itemMap.get("REP_UOM_ID"));
	 			order.put("EA_EA_QTY", itemMap.get("EA_EA_QTY"));
	 			order.put("EA_BOX_QTY", itemMap.get("EA_BOX_QTY"));
	 			order.put("EA_PLT_QTY", itemMap.get("EA_PLT_QTY"));
	 			order.put("EA_BOL_QTY", itemMap.get("EA_BOL_QTY"));
	        }
	       
    		
//    		for (Map<String, Object> chkItem : checkitemList) {
//    			String chkItemCode =  (String) chkItem.get("ITEM_CODE");
//				if(chkItemCode.equals(itemCode)) {
//					order.put("RITEM_ID", chkItem.get("RITEM_ID"));
//					order.put("ITEM_NM", chkItem.get("ITEM_NM"));
//					order.put("OUT_ORD_UOM_ID", chkItem.get("REP_UOM_ID"));
//					break;
//				}
//			}
		}
	}
	
	private void mappingCustId(List<Map<String, Object>> orderList ,List<Map<String, Object>> chkCustList) {
	  	
		// chkCustList를 CUST_CD별 그루핑
	    Map<String, Map<String, Object>> transCustGroupMap = new HashMap<>();
	    for (Map<String, Object> chkCust : chkCustList) {
	        String custCd = (String) chkCust.get("CUST_CD");
	        transCustGroupMap.put(custCd, chkCust);
	    }
		
		for (Map<String, Object> order : orderList) {
    		String transCustCode = (String) order.get("TRANS_CUST_CD");
    		Map<String, Object> transCustMap = transCustGroupMap.get(transCustCode);
 	        if (transCustMap != null) {
				order.put("CUST_ID", transCustMap.get("TRUST_CUST_ID"));
				order.put("TRANS_CUST_ID", transCustMap.get("CUST_ID"));
 	        }
//    		for (Map<String, Object> chkcust : chkCustList) {
//    			String chkCustCode =  (String) chkcust.get("CUST_CD");
//				if(chkCustCode.equals(transCustCode)) {
//					order.put("CUST_ID", chkcust.get("TRUST_CUST_ID"));
//					order.put("TRANS_CUST_ID", chkcust.get("CUST_ID"));
//					break;
//				}
//			}
		}
	}
	
	
	private boolean isExistMapValue(String codeValue, String mapKey, List<Map<String, Object>> checkitemList) {
		boolean isExist = false;
		for (Map<String, Object> checkItemMap : checkitemList) {
			if(!checkItemMap.containsKey(mapKey)) {
				throw new IllegalArgumentException("mapKey가 없음. >"+mapKey);
			}
			String mapValue = (String) checkItemMap.get(mapKey);
			if(codeValue.equals(mapValue)) {
				isExist = true;
				break;
			}
		}
		
		return isExist;
	}

}
