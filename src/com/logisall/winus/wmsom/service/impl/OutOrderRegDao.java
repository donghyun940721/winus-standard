package com.logisall.winus.wmsom.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
@SuppressWarnings("unchecked")
public class OutOrderRegDao extends SqlMapAbstractDAO{
	private final Log log = LogFactory.getLog(this.getClass());
	
	
	public List<Map<String, Object>> selectItemList(Map<String, Object> reqMap) {
		return executeQueryForList("outorderreg.selectItemList", reqMap);
	}

   public List<Map<String, Object>> selectAllCustList(Map<String, Object> model) {
        return executeQueryForList("wmsms010.lcAllListStore", model);
    }
   
   public void insertBulkOrderList(List<Map<String, Object>> orderList) throws SQLException {
	   SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			String orderRegist = "100"; /* 주문등록 */
			String ordTypeB2B = "B2B"; /* 주문유형 B2B */
			
			sqlMapClient.startTransaction();
			sqlMapClient.startBatch();

			int batchSize = 300;
			int updateCnt = 0;
			for (int i = 0; i < orderList.size(); i++) {
				updateCnt++;
				Map<String, Object> product = orderList.get(i);
				sqlMapClient.update("outorderreg.insertHeader", product);
				sqlMapClient.update("outorderreg.insertDetail", product);

				if (i > 0 && i % batchSize == 0) {
					sqlMapClient.executeBatch();
					sqlMapClient.startBatch(); // Reset batch to avoid ORA-01000
				}
			}

			sqlMapClient.executeBatch();
			sqlMapClient.commitTransaction();
			log.info("전체 처리된 update 수: " + updateCnt);

		} catch (SQLException e) {
			sqlMapClient.endTransaction();
			log.error(e);
			throw new RuntimeSQLException();
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}

	public List<Map<String, Object>> selectOrderList(Map<String, Object> model) {
		return executeQueryForList("outorderreg.selectOrderList", model);
	}
}