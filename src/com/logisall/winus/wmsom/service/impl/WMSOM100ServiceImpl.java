package com.logisall.winus.wmsom.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsom.service.WMSOM100Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOM100Service")
public class WMSOM100ServiceImpl implements WMSOM100Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOM100Dao")
	private WMSOM100Dao dao;

	/**
	 * Method ID : listE1 Method 설명 : 임가공주문관리 조회 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE1(Map<String, Object> model)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}

		map.put("LIST", dao.listE1(model));
		return map;
	}

	/**
	 * Method ID : listE2_H Method 설명 : 임가공주문관리E2_header 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE2_H(Map<String, Object> model)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}

		map.put("LIST", dao.listE2_H(model));
		return map;
	}
	
	
	/**
	 * Method ID : listE2_H_V2 Method 설명 : 임가공주문관리E2_header 작성자 : KIH
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> listE2_H_V2(Map<String, Object> model)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<Object> orderItemList = dao.listE2_H_V2(model);
		List<Object> noRegistItemList = new ArrayList<Object>();
		if(orderItemList.size() > 0) {
		    noRegistItemList = findNoRegistWinusItem(model, orderItemList);
		}
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(noRegistItemList);
		map.put("LIST", wqrs);
		return map;
	}
	
	/**
	 * 주문테이블(WMSOM000) 의 상품코드가 상품테이블(WMSMS091) 에 등록되어있는지 확인한다.
	 * 등록되지 않은 상품리스트로 리턴..
	 * @param <T> 이긴한데 Map 형태여야함
	 * */
	private  <T> List<Object> findNoRegistWinusItem(Map<String, Object> model, List<T> orderItemList) {
	    List<Map<String, Object>> winusItemList = dao.listE2_ITEMLIST(model);
	    List<Object> noRegistItemList = new ArrayList<Object>();
	    
	    // orderItemList에서 orderItemCode 추출하여 winusItemList에 있는지 확인
	    for (T orderItem : orderItemList) {
	        Map<String, Object> orderItemMap = (Map<String, Object>) orderItem;
	        String orderItemCode = (String) orderItemMap.get("ORD_ITEM_CD");

	        boolean isRegistered = false;
	        // winusItemList에 해당 orderItemCode가 있는지 확인
	        for (Map<String, Object> itemMap : winusItemList) {
	            String itemCode = (String) itemMap.get("ITEM_CODE");
	            if (itemCode.equals(orderItemCode)) {
	                isRegistered = true;
	                break;
	            }
	        }
	        // winusItemList에 없는 경우에만 noRegistItemList에 추가
	        if (!isRegistered) {
	            noRegistItemList.add(orderItem);
	        }
	    }
	
//	    for (int i=0; i<orderItemList.size(); i++) {
//		Map<String, Object> orderItemMap = (Map<String, Object>) orderItemList.get(i);
//		String orderItemCode = (String) orderItemMap.get("ORD_ITEM_CD");
//		
//		noRegistItemList.add(orderItemMap);
//		
//		/* 주문관리 아이템코드와 위너스RITEM_ID 매핑 */
//		for (Map<String, Object> itemMap : winusItemList) {
//		    String itemCode = (String) itemMap.get("ITEM_CODE");
//		    if (itemCode.equals(orderItemCode)) {
//			noRegistItemList.remove(i);
//			break;
//		    }
//		}
//	    }
	    
	    return noRegistItemList;
	}
	

	/**
	 * Method ID : listE3_H Method 설명 : 임가공주문관리E3_header 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE3_H(Map<String, Object> model)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}

		map.put("LIST", dao.listE3_H(model));
		return map;
	}

	/**
	 * Method ID : listE3_D Method 설명 : 임가공주문관리E3_detail 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE3_D(Map<String, Object> model)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}

		map.put("LIST", dao.listE3_D(model));
		return map;
	}

	/**
	 * Method ID : saveE2 Method 설명 : 임가공주문관리_tab2_ 1, 세트상품 저장 -> 2. 부속상품 저장 작성자
	 * : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveE2(Map<String, Object> model)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			// 1. 프로시져에 보낼것들 다담는다 (세트상품 간편등록)
			Map<String, Object> modelIns = new HashMap<String, Object>();

			modelIns.put("I_LC_ID", model.get("SS_SVC_NO"));
			modelIns.put("I_CUST_ID", model.get("vrSrchCustId"));
			modelIns.put("I_ITEM_CD", model.get("vrSetItemCd"));
			modelIns.put(
					"I_ITEM_NM",
					HtmlUtils.htmlUnescape(HtmlUtils.htmlUnescape(model.get(
							"vrSetItemNm").toString())));
			modelIns.put("I_UOM_ID", (String)dao.getEAUomId(model)); // UOM 'EA'고정값(임시)

			modelIns.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
			modelIns.put("I_USER_NO", model.get("SS_USER_NO"));
			
			//validation 추가
			int validationCount = (int)dao.setItemValidationCount(modelIns);
			
			if (validationCount > 0){
			    m.put("MSG", "이미 등록된 세트상품입니다.");
                m.put("MSG_ORA", "");
                m.put("errCnt", -1);
			    
                return m;
			}
			
			// dao
			Map<String, Object> modelRst = (Map<String, Object>) dao.saveSetItem(modelIns);

			// 상품 간편 등록 성공
			if (modelRst.get("O_MSG_CODE").toString().equals("0")) {
				// 상품 RITEM_ID 조회
				Map<String, Object> itemMap = new HashMap<String, Object>();
				String setItemId = (String) dao.getSetItem(model);
				itemMap.put("RITEM_ID", setItemId);
				itemMap.put("SS_SVC_NO", model.get("SS_SVC_NO"));
				itemMap.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
				itemMap.put("ORD_MAKER", model.get("vrSetItemMakeNm"));

				// 2. 세트 상품 타입 Y 처리, 생산처 입력
				dao.updateSetItem(itemMap);

				// 3. 프로시져에 보낼것들 다담는다 (부속상품 생성)
				Map<String, Object> modelInsSub = new HashMap<String, Object>();

				for (int i = 0; i < Integer.parseInt(model.get("selectIds")
						.toString()); i++) {
					Map<String, Object> modelDt = new HashMap<String, Object>();
					modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));

					modelDt.put("UOM_ID", model.get("UOM_" + i));
					modelDt.put("QTY", model.get("QTY_" + i));
					modelDt.put("SET_RITEM_ID", setItemId);
					modelDt.put("PART_RITEM_ID", model.get("RITEM_ID_" + i));

					dao.savePartItem(modelDt);
				}

				dao.updateSetItemOm(modelIns);

				m.put("MSG", MessageResolver.getMessage("save.success"));
				m.put("MSG_ORA", "");
				m.put("errCnt", 0);
			} else {
				m.put("MSG", MessageResolver.getMessage("save.error"));
				m.put("MSG_ORA", "");
				m.put("errCnt", 1);
			}
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : saveE3 Method 설명 : 임가공주문관리_tab3 세트 부속상품 수정 및 저장 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveE3(Map<String, Object> model)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;

		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds")
					.toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();

				modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));

				modelDt.put("UOM_ID", model.get("UOM_" + i));
				modelDt.put("QTY", model.get("QTY_" + i));
				modelDt.put("SET_RITEM_ID", model.get("vrSetItemId"));
				modelDt.put("PART_RITEM_ID", model.get("PART_RITEM_ID_" + i));
				modelDt.put("SET_SEQ", model.get("SET_SEQ_" + i));

				if ("I".equals(model.get("ST_GUBUN_" + i))) {
					dao.savePartItem(modelDt);
				} else if ("U".equals(model.get("ST_GUBUN_" + i))) {
					dao.updatePartItem(modelDt);
				} else if ("D".equals(model.get("ST_GUBUN_" + i))) {
					dao.deletePartItem(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(
							MessageResolver.getMessage("save.error"));
				}
			}

			m.put("MSG", MessageResolver.getMessage("save.success"));
			m.put("MSG_ORA", "");
			m.put("errCnt", 0);
		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
	 * Method ID : deleteE2 
	 * Method 설명 : 임가공주문관리_tab2 세트 상품 삭제
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> deleteE2(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;

		try {
			int selectCnt = Integer.parseInt(model.get("selectIds").toString());
			
			if(selectCnt > 0){
				
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("REG_LC_ID", model.get("REG_LC_ID_0"));
				modelDt.put("REG_CUST_ID", model.get("REG_CUST_ID_0"));
				modelDt.put("USER_NO", model.get("SS_USER_NO"));

				List<String> regSeqList = new ArrayList<String>();

				for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
					regSeqList.add((String) model.get("ORD_ITEM_NM_" + i));
				}
				
				modelDt.put("vrSrchReqSeqArray", regSeqList);
	        	
	        	dao.deleteE2(modelDt);
	        	
				m.put("MSG", MessageResolver.getMessage("save.success"));
				m.put("MSG_ORA", "");
				m.put("errCnt", 0);

			}else{
				m.put("MSG", MessageResolver.getMessage("save.success"));
				m.put("MSG_ORA", "delete fail");
				m.put("errCnt", 1);
			}
		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
	 * Method ID : deleteE3_H 
	 * Method 설명 : 임가공주문관리_tab3 세트 상품 삭제
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> deleteE3_H(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;

		try {
			int selectCnt = Integer.parseInt(model.get("selectIds").toString());
			
			if(selectCnt > 0){
				
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("LC_ID", model.get("SS_SVC_NO"));
				modelDt.put("CUST_ID", model.get("vrSrchCustId"));
				modelDt.put("USER_NO", model.get("SS_USER_NO"));

				List<String> setItemIdList = new ArrayList<String>();
				List<String> setItemCdList = new ArrayList<String>();

				for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
					setItemIdList.add((String) model.get("SET_RITEM_ID_" + i));
					setItemCdList.add((String) model.get("SET_RITEM_CD_" + i));
				}
				
				modelDt.put("vrSrchSetItemIdArray", setItemIdList);
	        	
	        	dao.deleteE3_092(modelDt);
	        	
	        	dao.deleteE3_091(modelDt);
	        	
//	        	dao.deleteSetItemOm(modelDt);
	        	
				m.put("MSG", MessageResolver.getMessage("save.success"));
				m.put("MSG_ORA", "");
				m.put("errCnt", 0);

			}else{
				m.put("MSG", MessageResolver.getMessage("save.success"));
				m.put("MSG_ORA", "delete fail");
				m.put("errCnt", 1);
			}
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : deleteE3 Method 설명 : 임가공주문관리_tab3 세트 부속상품 수정 및 저장 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> deleteE3(Map<String, Object> model)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;

		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds")
					.toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();

				modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));

				modelDt.put("UOM_ID", model.get("UOM_" + i));
				modelDt.put("QTY", model.get("QTY_" + i));
				modelDt.put("SET_RITEM_ID", model.get("vrSetItemId"));
				modelDt.put("PART_RITEM_ID", model.get("PART_RITEM_ID_" + i));
				modelDt.put("SET_SEQ", model.get("SET_SEQ_" + i));

				if ("D".equals(model.get("ST_GUBUN_" + i))) {
					dao.deletePartItem(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(
							MessageResolver.getMessage("save.error"));
				}
			}

			m.put("MSG", MessageResolver.getMessage("save.success"));
			m.put("MSG_ORA", "");
			m.put("errCnt", 0);
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : deleteYnOm Method 설명 : 임가공주문관리_tab2_ 1, 세트상품 저장 -> 2. 부속상품 저장
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> deleteYnOm(Map<String, Object> model)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds")
					.toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
				modelDt.put("REG_LC_ID", (String) model.get("SS_SVC_NO"));
				modelDt.put("REG_CUST_ID",
						(String) model.get("REG_CUST_ID_" + i));
				modelDt.put("ORG_ORD_ID", (String) model.get("ORG_ORD_ID_" + i));
				modelDt.put("ORG_ORD_SEQ",
						(String) model.get("ORG_ORD_SEQ_" + i));

				dao.deleteYnOm(modelDt);
			}

			m.put("MSG", MessageResolver.getMessage("save.success"));
			m.put("MSG_ORA", "");
			m.put("errCnt", 0);

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : getUomList Method 설명 : UOM 리스트 조회 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> getUomList(Map<String, Object> model)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m.put("UOM", dao.getUomList(model)); // Template 조회
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : getTemplate Method 설명 : 저장된 업체별 템플릿 조회. 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Map<String, Object>> getTemplate(Map<String, Object> model)
			throws Exception {
		List<Map<String, Object>> m = new ArrayList();
		try {
			Map<String, Object> searchParam = new HashMap<String, Object>();
			searchParam.putAll(model);
			searchParam.put("vrTemplateType", model.get("vrTemplateType"));
			searchParam.put("vrCustSeq", model.get("vrCustSeq"));

			m.addAll(dao.getTemplateInfo(searchParam)); // Template 조회

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : getResult Method 설명 : 템플릿 정보에 따른 컬럼 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getResult(Map<String, Object> model)
			throws Exception {
		List<Map<String, Object>> list = new ArrayList();

		try {
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("vrTemplateId", model.get("vrTemplateId"));
			modelIns.put("vrCustSeq", model.get("vrCustSeq"));

			list.addAll(dao.searchTpCol(modelIns));

		} catch (Exception e) {
			throw e;
		}
		return list;
	}

	/**
	 * Method ID : getTransCustList Method 설명 : 거래처 정보 조회 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getTransCustList(Map<String, Object> model)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("TRANS", dao.getTransCustList(model));
		return map;
	}

	/**
	 * Method ID : getSyncList Method 설명 : 주문 동기화 대상 리스트 조회 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> getSyncList(Map<String, Object> model)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("LIST", dao.getSyncList(model));

		return map;
	}
	
	/**
	 * 
	 * 대체 Method ID : saveSyncOrderJavaB2C 대체 Method 설명 : 임가공주문관리 -> 주문동기화 작성자 :
	 * KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveSyncOrderJavaB2C(Map<String, Object> model)
			throws Exception {
		String custId = String.valueOf(model.get("vrCustCd"));

		Map<String, Object> m = new HashMap<String, Object>();

		List<Map<String, Object>> syncList = new ArrayList();
		syncList = dao.getSyncList(model);

		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String formatted_today = sdf.format(today);

		int insCnt = syncList.size();

		try {
			if (insCnt > 0) {
				String[] no = new String[insCnt];

				String[] outReqDt = new String[insCnt];
				String[] inReqDt = new String[insCnt];
				String[] custOrdNo = new String[insCnt];
				String[] custOrdSeq = new String[insCnt];
				String[] trustCustCd = new String[insCnt];

				String[] transCustCd = new String[insCnt];
				String[] transCustTel = new String[insCnt];
				String[] transReqDt = new String[insCnt];
				String[] custCd = new String[insCnt];
				String[] ordQty = new String[insCnt];

				String[] uomCd = new String[insCnt];
				String[] sdeptCd = new String[insCnt];
				String[] salePerCd = new String[insCnt];
				String[] carCd = new String[insCnt];
				String[] drvNm = new String[insCnt];

				String[] dlvSeq = new String[insCnt];
				String[] drvTel = new String[insCnt];
				String[] custLotNo = new String[insCnt];
				String[] blNo = new String[insCnt];
				String[] recDt = new String[insCnt];

				String[] outWhCd = new String[insCnt];
				String[] inWhCd = new String[insCnt];
				String[] makeDt = new String[insCnt];
				String[] timePeriodDay = new String[insCnt];
				String[] workYn = new String[insCnt];

				String[] rjType = new String[insCnt];
				String[] locYn = new String[insCnt];
				String[] confYn = new String[insCnt];
				String[] eaCapa = new String[insCnt];
				String[] inOrdWeight = new String[insCnt];

				String[] itemCd = new String[insCnt];
				String[] itemNm = new String[insCnt];
				String[] transCustNm = new String[insCnt];
				String[] transCustAddr = new String[insCnt];
				String[] transEmpNm = new String[insCnt];

				String[] remark = new String[insCnt];
				String[] transZipNo = new String[insCnt];
				String[] etc2 = new String[insCnt];
				String[] unitAmt = new String[insCnt];
				String[] transBizNo = new String[insCnt];

				String[] inCustAddr = new String[insCnt];
				String[] inCustCd = new String[insCnt];
				String[] inCustNm = new String[insCnt];
				String[] inCustTel = new String[insCnt];
				String[] inCustEmpNm = new String[insCnt];

				String[] expiryDate = new String[insCnt];
				String[] salesCustNm = new String[insCnt];
				String[] zip = new String[insCnt];
				String[] addr = new String[insCnt];
				String[] addr2 = new String[insCnt];

				String[] phone1 = new String[insCnt];
				String[] etc1 = new String[insCnt];
				String[] unitNo = new String[insCnt];
				String[] timeDate = new String[insCnt]; // 상품유효기간
				String[] timeDateEnd = new String[insCnt]; // 상품유효기간만료일

				String[] timeUseEnd = new String[insCnt]; // 소비가한만료일
				String[] phone2 = new String[insCnt]; // 고객전화번호2
				String[] buyCustNm = new String[insCnt]; // 주문자명
				String[] buyPhone1 = new String[insCnt]; // 주문자전화번호1
				String[] salesCompanyNm = new String[insCnt]; // salesCompanyNm

				String[] ordDegree = new String[insCnt]; // 주문등록차수
				String[] bizCond = new String[insCnt]; // 업태
				String[] bizType = new String[insCnt]; // 업종
				String[] bizNo = new String[insCnt]; // 사업자등록번호
				String[] custType = new String[insCnt]; // 화주타입

				String[] dataSenderNm = new String[insCnt]; // 쇼핑몰
				String[] legacyOrgOrdNo = new String[insCnt]; // 사방넷주문번호
				// String[] invoiceNo = new String[listBodyCnt]; //쇼핑몰

				String[] custSeq = new String[insCnt]; // 템플릿구분
				String[] ordDesc = new String[insCnt]; // ord_desc
				String[] dlvMsg1 = new String[insCnt]; // 배송메세지1
				String[] dlvMsg2 = new String[insCnt]; // 배송메세지2
				String[] locCd = new String[insCnt]; // 로케이션코드

				// String breakYnOrdDegree = "N";

				/*
				 * 2022.02.04 KSJ 사용안하는 속성은 소문자로 작성함
				 */
				for (int i = 0; i < insCnt; i++) {
					String NO = Integer.toString(i + 1);
					no[i] = NO;

					outReqDt[i] = (String) syncList.get(i).get("ORD_DT");
					inReqDt[i] = (String) syncList.get(i).get("inReqDt");
					custOrdNo[i] = (String) syncList.get(i).get("ORG_ORD_ID");
					custOrdSeq[i] = (String) syncList.get(i).get("ORG_ORD_SEQ");
					trustCustCd[i] = (String) syncList.get(i)
							.get("trustCustCd");

					transCustCd[i] = (String) syncList.get(i).get("ORD_MAKER");
					transCustTel[i] = (String) syncList.get(i).get(
							"transCustTel");
					transReqDt[i] = (String) syncList.get(i).get("transReqDt");
					custCd[i] = (String) syncList.get(i).get("REG_CUST_CD");
					ordQty[i] = String.valueOf(syncList.get(i).get("ORD_QTY"));

					uomCd[i] = (String) syncList.get(i).get("uomCd"); // 필요해보임
					sdeptCd[i] = (String) syncList.get(i).get("sdeptCd");
					salePerCd[i] = (String) syncList.get(i).get("salePerCd");
					carCd[i] = (String) syncList.get(i).get("carCd");
					drvNm[i] = (String) syncList.get(i).get("drvNm");

					dlvSeq[i] = (String) syncList.get(i).get("dlvSeq");
					drvTel[i] = (String) syncList.get(i).get("drvTel");
					custLotNo[i] = (String) syncList.get(i).get("custLotNo");
					blNo[i] = (String) syncList.get(i).get("blNo");
					recDt[i] = (String) syncList.get(i).get("recDt");

					outWhCd[i] = (String) syncList.get(i).get("outWhCd");
					inWhCd[i] = (String) syncList.get(i).get("inWhCd");
					makeDt[i] = (String) syncList.get(i).get("makeDt");
					timePeriodDay[i] = (String) syncList.get(i).get(
							"timePeriodDay");
					workYn[i] = (String) syncList.get(i).get("workYn");

					rjType[i] = (String) syncList.get(i).get("rjType");
					locYn[i] = (String) syncList.get(i).get("locYn");
					confYn[i] = (String) syncList.get(i).get("confYn");
					eaCapa[i] = (String) syncList.get(i).get("eaCapa");
					inOrdWeight[i] = (String) syncList.get(i)
							.get("inOrdWeight");

					itemCd[i] = (String) syncList.get(i).get("ORD_ITEM_CD");
					itemNm[i] = (String) syncList.get(i).get("ORD_ITEM_NM");
					transCustNm[i] = (String) syncList.get(i)
							.get("transCustNm");
					transCustAddr[i] = (String) syncList.get(i).get(
							"transCustAddr");
					transEmpNm[i] = (String) syncList.get(i).get("transEmpNm");

					remark[i] = (String) syncList.get(i).get("remark");
					transZipNo[i] = (String) syncList.get(i).get("transZipNo");
					etc2[i] = (String) syncList.get(i).get("ORD_ITEM_OP_NM");
					unitAmt[i] = (String) syncList.get(i).get("unitAmt");
					transBizNo[i] = (String) syncList.get(i).get("transBizNo");

					inCustAddr[i] = (String) syncList.get(i).get("inCustAddr");
					inCustCd[i] = (String) syncList.get(i).get("inCustCd");
					inCustNm[i] = (String) syncList.get(i).get("inCustNm");
					inCustTel[i] = (String) syncList.get(i).get("inCustTel");
					inCustEmpNm[i] = (String) syncList.get(i)
							.get("inCustEmpNm");

					expiryDate[i] = (String) syncList.get(i).get("expiryDate");
					salesCustNm[i] = (String) syncList.get(i).get(
							"TO_CUSTOMER_NM");
					zip[i] = (String) syncList.get(i).get("TO_ZIP");
					addr[i] = (String) syncList.get(i).get("TO_ADDRESS_1");
					addr2[i] = (String) syncList.get(i).get("TO_ADDRESS_2");
					phone1[i] = (String) syncList.get(i).get("TO_TEL_1");

					etc1[i] = (String) syncList.get(i).get("etc1");
					unitNo[i] = (String) syncList.get(i).get("unitNo");
					timeDate[i] = (String) syncList.get(i).get("timeDate");
					timeDateEnd[i] = (String) syncList.get(i)
							.get("timeDateEnd");
					timeUseEnd[i] = (String) syncList.get(i).get("timeUseEnd");

					phone2[i] = (String) syncList.get(i).get("TO_TEL_2");
					buyCustNm[i] = (String) syncList.get(i).get(
							"FROM_CUSTOMER_NM");
					buyPhone1[i] = (String) syncList.get(i).get("FROM_TEL_1");
					salesCompanyNm[i] = (String) syncList.get(i).get(
							"salesCompanyNm");
					ordDegree[i] = (String) syncList.get(i).get("ordDegree");
					bizCond[i] = (String) syncList.get(i).get("bizCond");
					bizType[i] = (String) syncList.get(i).get("bizType");
					bizNo[i] = (String) syncList.get(i).get("bizNo");
					custType[i] = (String) syncList.get(i).get("custType");

					dataSenderNm[i] = (String) syncList.get(i).get(
							"DATA_SENDER_NM");
					legacyOrgOrdNo[i] = (String) syncList.get(i).get(
							"legacyOrgOrdNo");

					custSeq[i] = (String) syncList.get(i).get("custSeq");

					ordDesc[i] = (String) syncList.get(i).get("ordDesc");
					dlvMsg1[i] = (String) syncList.get(i).get("DLV_DESC1");
					dlvMsg2[i] = (String) syncList.get(i).get("DLV_DESC2");
					locCd[i] = (String) syncList.get(i).get("LOC_CD");

				}

				// 프로시져에 보낼것들 다담는다
				Map<String, Object> modelIns = new HashMap<String, Object>();

				modelIns.put("vrOrdType", "O"); // 출고주문이기에 고정값 추후 수정해야할수 있음
				modelIns.put("no", no);

				modelIns.put("reqDt", outReqDt);
				modelIns.put("whCd", outWhCd);

				modelIns.put("custOrdNo", custOrdNo);
				modelIns.put("custOrdSeq", custOrdSeq);
				modelIns.put("trustCustCd", trustCustCd); // 5

				modelIns.put("transCustCd", transCustCd);
				modelIns.put("transCustTel", transCustTel);
				modelIns.put("transReqDt", transReqDt);
				modelIns.put("custCd", custCd);
				modelIns.put("ordQty", ordQty); // 10

				modelIns.put("uomCd", uomCd);
				modelIns.put("sdeptCd", sdeptCd);
				modelIns.put("salePerCd", salePerCd);
				modelIns.put("carCd", carCd);
				modelIns.put("drvNm", drvNm); // 15

				modelIns.put("dlvSeq", dlvSeq);
				modelIns.put("drvTel", drvTel);
				modelIns.put("custLotNo", custLotNo);
				modelIns.put("blNo", blNo);
				modelIns.put("recDt", recDt); // 20

				modelIns.put("makeDt", makeDt);
				modelIns.put("timePeriodDay", timePeriodDay);
				modelIns.put("workYn", workYn);

				modelIns.put("rjType", rjType);
				modelIns.put("locYn", locYn); // 25
				modelIns.put("confYn", confYn);
				modelIns.put("eaCapa", eaCapa);
				modelIns.put("inOrdWeight", inOrdWeight); // 28

				modelIns.put("itemCd", itemCd);
				modelIns.put("itemNm", itemNm);
				modelIns.put("transCustNm", transCustNm);
				modelIns.put("transCustAddr", transCustAddr);
				modelIns.put("transEmpNm", transEmpNm);

				modelIns.put("remark", remark);
				modelIns.put("transZipNo", transZipNo);
				modelIns.put("etc2", etc2);
				modelIns.put("unitAmt", unitAmt);
				modelIns.put("transBizNo", transBizNo);

				modelIns.put("inCustAddr", inCustAddr);
				modelIns.put("inCustCd", inCustCd);
				modelIns.put("inCustNm", inCustNm);
				modelIns.put("inCustTel", inCustTel);
				modelIns.put("inCustEmpNm", inCustEmpNm);

				modelIns.put("expiryDate", expiryDate);
				modelIns.put("salesCustNm", salesCustNm);
				modelIns.put("zip", zip);
				modelIns.put("addr", addr);
				modelIns.put("addr2", addr2);
				modelIns.put("phone1", phone1);

				modelIns.put("etc1", etc1);
				modelIns.put("unitNo", unitNo);
				modelIns.put("phone2", phone2);
				modelIns.put("buyCustNm", buyCustNm);
				modelIns.put("buyPhone1", buyPhone1);
				modelIns.put("salesCompanyNm", salesCompanyNm);

				modelIns.put("ordDegree", ordDegree);
				modelIns.put("bizCond", bizCond);
				modelIns.put("bizType", bizType);
				modelIns.put("bizNo", bizNo);
				modelIns.put("custType", custType);

				modelIns.put("dataSenderNm", dataSenderNm);
				modelIns.put("legacyOrgOrdNo", legacyOrgOrdNo);
				// modelIns.put("invoiceNo" , invoiceNo);
				modelIns.put("custSeq", custSeq);

				modelIns.put("ordDesc", ordDesc);
				modelIns.put("dlvMsg1", dlvMsg1);
				modelIns.put("dlvMsg2", dlvMsg2);
				modelIns.put("locCd", locCd);

				modelIns.put("LC_ID", model.get("SS_SVC_NO"));
				modelIns.put("WORK_IP", model.get("SS_CLIENT_IP"));
				modelIns.put("USER_NO", model.get("SS_USER_NO"));

				modelIns = (Map<String, Object>) dao
						.saveExcelOrderB2T(modelIns);

				if (modelIns.get("O_MSG_CODE").toString().equals("0")) {
					for (int j = 0; j < syncList.size(); ++j) {
						dao.updateWmsLink(syncList.get(j));
					}
				}
			}

			m.put("MSG", MessageResolver.getMessage("save.success"));
			m.put("MSG_ORA", "");
			m.put("errCnt", 0);

		} catch (Exception e) {
			throw e;
		}

		return m;
	}

	/**
	 * 
	 * 대체 Method ID : saveExcelOrderJavaB2C 대체 Method 설명 : B2C 임가공 템플릿 입력 (공통)
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveExcelOrderJavaB2C(Map<String, Object> model,
			List<Map<String, Object>> model2) throws Exception {

		List<Map<String, Object>> listHeader = model2;
		List<Map<String, Object>> listBody = (ArrayList) model.get("LIST");
		String custId = String.valueOf(model.get("vrCustCd"));

		int listHeaderCnt = listHeader.size();
		int listBodyCnt = listBody.size();

		Map<String, Object> m = new HashMap<String, Object>();

		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String formatted_today = sdf.format(today);

		try {
			if (listBodyCnt > 0) {
				String[] no = new String[listBodyCnt];

				String[] outReqDt = new String[listBodyCnt];
				String[] inReqDt = new String[listBodyCnt];
				String[] custOrdNo = new String[listBodyCnt];
				String[] custOrdSeq = new String[listBodyCnt];
				String[] trustCustCd = new String[listBodyCnt];

				String[] transCustCd = new String[listBodyCnt];
				String[] transCustTel = new String[listBodyCnt];
				String[] transReqDt = new String[listBodyCnt];
				String[] custCd = new String[listBodyCnt];
				String[] ordQty = new String[listBodyCnt];

				String[] uomCd = new String[listBodyCnt];
				String[] sdeptCd = new String[listBodyCnt];
				String[] salePerCd = new String[listBodyCnt];
				String[] carCd = new String[listBodyCnt];
				String[] drvNm = new String[listBodyCnt];

				String[] dlvSeq = new String[listBodyCnt];
				String[] drvTel = new String[listBodyCnt];
				String[] custLotNo = new String[listBodyCnt];
				String[] blNo = new String[listBodyCnt];
				String[] recDt = new String[listBodyCnt];

				String[] outWhCd = new String[listBodyCnt];
				String[] inWhCd = new String[listBodyCnt];
				String[] makeDt = new String[listBodyCnt];
				String[] timePeriodDay = new String[listBodyCnt];
				String[] workYn = new String[listBodyCnt];

				String[] rjType = new String[listBodyCnt];
				String[] locYn = new String[listBodyCnt];
				String[] confYn = new String[listBodyCnt];
				String[] eaCapa = new String[listBodyCnt];
				String[] inOrdWeight = new String[listBodyCnt];

				String[] itemCd = new String[listBodyCnt];
				String[] itemNm = new String[listBodyCnt];
				String[] transCustNm = new String[listBodyCnt];
				String[] transCustAddr = new String[listBodyCnt];
				String[] transEmpNm = new String[listBodyCnt];

				String[] remark = new String[listBodyCnt];
				String[] transZipNo = new String[listBodyCnt];
				String[] etc2 = new String[listBodyCnt];
				String[] unitAmt = new String[listBodyCnt];
				String[] transBizNo = new String[listBodyCnt];

				String[] inCustAddr = new String[listBodyCnt];
				String[] inCustCd = new String[listBodyCnt];
				String[] inCustNm = new String[listBodyCnt];
				String[] inCustTel = new String[listBodyCnt];
				String[] inCustEmpNm = new String[listBodyCnt];

				String[] expiryDate = new String[listBodyCnt];
				String[] salesCustNm = new String[listBodyCnt];
				String[] zip = new String[listBodyCnt];
				String[] addr = new String[listBodyCnt];
				String[] addr2 = new String[listBodyCnt];
				String[] phone1 = new String[listBodyCnt];

				String[] etc1 = new String[listBodyCnt];
				String[] unitNo = new String[listBodyCnt];
				String[] timeDate = new String[listBodyCnt]; // 상품유효기간
				String[] timeDateEnd = new String[listBodyCnt]; // 상품유효기간만료일
				String[] timeUseEnd = new String[listBodyCnt]; // 소비가한만료일

				String[] phone2 = new String[listBodyCnt]; // 고객전화번호2
				String[] buyCustNm = new String[listBodyCnt]; // 주문자명
				String[] buyPhone1 = new String[listBodyCnt]; // 주문자전화번호1
				String[] salesCompanyNm = new String[listBodyCnt]; // salesCompanyNm
				String[] ordDegree = new String[listBodyCnt]; // 주문등록차수
				String[] bizCond = new String[listBodyCnt]; // 업태
				String[] bizType = new String[listBodyCnt]; // 업종
				String[] bizNo = new String[listBodyCnt]; // 사업자등록번호
				String[] custType = new String[listBodyCnt]; // 화주타입

				String[] dataSenderNm = new String[listBodyCnt]; // 쇼핑몰
				String[] legacyOrgOrdNo = new String[listBodyCnt]; // 사방넷주문번호
				// String[] invoiceNo = new String[listBodyCnt]; //쇼핑몰

				String[] custSeq = new String[listBodyCnt]; // 템플릿구분
				String[] ordDesc = new String[listBodyCnt]; // ord_desc
				String[] dlvMsg1 = new String[listBodyCnt]; // 배송메세지1
				String[] dlvMsg2 = new String[listBodyCnt]; // 배송메세지2
				String[] locCd = new String[listBodyCnt]; 

				/* 컬럼 바인딩 제약조건 참조 flag */
				String breakYnOrdDegree = "N";

				for (int i = 0; i < listBodyCnt; i++) {
					String OUT_REQ_DT = "";
					String IN_REQ_DT = "";
					String CUST_ORD_NO = "";
					String CUST_ORD_SEQ = "";
					String TRUST_CUST_CD = "";
					String TRANS_CUST_CD = "";
					String TRANS_CUST_TEL = "";
					String TRANS_REQ_DT = "";
					String CUST_CD = "";
					String ORD_QTY = "";
					String UOM_CD = "";
					String SDEPT_CD = "";
					String SALE_PER_CD = "";
					String CAR_CD = "";
					String DRV_NM = "";
					String DLV_SEQ = "";
					String DRV_TEL = "";
					String CUST_LOT_NO = "";
					String BL_NO = "";
					String REC_DT = "";
					String OUT_WH_CD = "";
					String IN_WH_CD = "";
					String MAKE_DT = "";
					String TIME_PERIOD_DAY = "";
					String WORK_YN = "";
					String RJ_TYPE = "";
					String LOC_YN = "";
					String CONF_YN = "";
					String EA_CAPA = "";
					String IN_ORD_WEIGHT = "";
					String ITEM_CD = "";
					String ITEM_NM = "";
					String TRANS_CUST_NM = "";
					String TRANS_CUST_ADDR = "";
					String TRANS_EMP_NM = "";
					String REMARK = "";
					String TRANS_ZIP_NO = "";
					String ETC2 = "";
					String UNIT_AMT = "";
					String TRANS_BIZ_NO = "";
					String IN_CUST_ADDR = "";
					String IN_CUST_CD = "";
					String IN_CUST_NM = "";
					String IN_CUST_TEL = "";
					String IN_CUST_EMP_NM = "";
					String EXPIRY_DATE = "";
					String SALES_CUST_NM = "";
					String ZIP = "";
					String ADDR = "";
					String ADDR2 = "";
					String PHONE_1 = "";
					String ETC1 = "";
					String UNIT_NO = "";
					String TIME_DATE = "";
					String TIME_DATE_END = "";
					String TIME_USE_END = "";
					String PHONE_2 = "";
					String BUY_CUST_NM = "";
					String BUY_PHONE_1 = "";
					String SALES_COMPANY_NM = "";
					String ORD_DEGREE = "";
					String BIZ_COND = "";
					String BIZ_TYPE = "";
					String BIZ_NO = "";
					String CUST_TYPE = "";
					String DATA_SENDER_NM = "";
					String LEGACY_ORG_ORD_NO = "";
					// String INVOICE_NO ="";
					String CUST_SEQ = "";
					String ORD_DESC = "";
					String DLV_MSG1 = "";
					String DLV_MSG2 = "";
					String LOC_CD = "";

					/* 임가공 조합 대상 컬럼 정렬 및 value 가공 */
					StringBuilder builder = new StringBuilder();
					Map<Integer, String> combineCols = new HashMap<Integer, String>();

					// 거래처 코드 X, 임가공 조합 컬럼 O -> ex) 임가공조합컬럼1_임가공조합컬럼2
					for (int j = 0; j < listHeaderCnt; j++) {
						// 임가공 조합 순번 체크
						String bindColSeq = listHeader.get(j)
								.get("FORMAT_USE_SEQ").toString();
						String bindColNm = listHeader.get(j)
								.get("FORMAT_BIND_COL").toString();
						String bindColValue = listBody.get(i).get(bindColNm)
								.toString();

						if (!bindColSeq.equals("") && !bindColSeq.equals("0")) {
							combineCols.put(Integer.parseInt(bindColSeq),
									bindColValue);
						}
						;
					}

					Object[] mapKey = combineCols.keySet().toArray();
					Arrays.sort(mapKey);

					for (Integer key : combineCols.keySet()) {
						if (!combineCols.get(key).toString().equals("")) {
							builder.append(combineCols.get(key)).append("_");
						}
					}

					String SET_ITEM_NM = builder.toString().replaceFirst(".$",
							""); // 마지막 _ 제거

					for (int k = 0; k < listHeaderCnt; k++) {

						Map<String, Object> object = (Map<String, Object>) listBody
								.get(i);
						String bindColNm = String.valueOf(listHeader.get(k)
								.get("FORMAT_BIND_COL"));

						switch (bindColNm) {
						case "OUT_REQ_DT":
							OUT_REQ_DT = object.get("OUT_REQ_DT").toString();
							break;
						case "IN_REQ_DT":
							IN_REQ_DT = object.get("IN_REQ_DT").toString();
							break;
						case "CUST_ORD_NO":
							CUST_ORD_NO = object.get("CUST_ORD_NO").toString();
							break;
						case "CUST_ORD_SEQ":
							CUST_ORD_SEQ = object.get("CUST_ORD_SEQ")
									.toString();
							break;
						case "TRUST_CUST_CD":
							TRUST_CUST_CD = object.get("TRUST_CUST_CD")
									.toString();
							break;
						case "TRANS_CUST_TEL":
							TRANS_CUST_TEL = object.get("TRANS_CUST_TEL")
									.toString();
							break;
						case "TRANS_REQ_DT":
							TRANS_REQ_DT = object.get("TRANS_REQ_DT")
									.toString();
							break;
						case "CUST_CD":
							CUST_CD = object.get("CUST_CD").toString();
							break;
						case "ORD_QTY":
							ORD_QTY = object.get("ORD_QTY").toString();
							break;
						case "UOM_CD":
							UOM_CD = object.get("UOM_CD").toString();
							break;
						case "SDEPT_CD":
							SDEPT_CD = object.get("SDEPT_CD").toString();
							break;
						case "SALE_PER_CD":
							SALE_PER_CD = object.get("SALE_PER_CD").toString();
							break;
						case "CAR_CD":
							CAR_CD = object.get("CAR_CD").toString();
							break;
						case "DRV_NM":
							DRV_NM = object.get("DRV_NM").toString();
							break;
						case "DLV_SEQ":
							DLV_SEQ = object.get("DLV_SEQ").toString();
							break;
						case "DRV_TEL":
							DRV_TEL = object.get("DRV_TEL").toString();
							break;
						case "CUST_LOT_NO":
							CUST_LOT_NO = object.get("CUST_LOT_NO").toString();
							break;
						case "BL_NO":
							BL_NO = object.get("BL_NO").toString();
							break;
						case "REC_DT":
							REC_DT = object.get("REC_DT").toString();
							break;
						case "OUT_WH_CD":
							OUT_WH_CD = object.get("OUT_WH_CD").toString();
							break;
						case "IN_WH_CD":
							IN_WH_CD = object.get("IN_WH_CD").toString();
							break;
						case "MAKE_DT":
							MAKE_DT = object.get("MAKE_DT").toString();
							break;
						case "TIME_PERIOD_DAY":
							TIME_PERIOD_DAY = object.get("TIME_PERIOD_DAY")
									.toString();
							break;
						case "WORK_YN":
							WORK_YN = object.get("WORK_YN").toString();
							break;
						case "RJ_TYPE":
							RJ_TYPE = object.get("RJ_TYPE").toString();
							break;
						case "LOC_YN":
							LOC_YN = object.get("LOC_YN").toString();
							break;
						case "CONF_YN":
							CONF_YN = object.get("CONF_YN").toString();
							break;
						case "EA_CAPA":
							EA_CAPA = object.get("EA_CAPA").toString();
							break;
						case "IN_ORD_WEIGHT":
							IN_ORD_WEIGHT = object.get("IN_ORD_WEIGHT")
									.toString();
							break;
						case "ITEM_CD":
							ITEM_NM = object.get("ITEM_CD").toString();
							break;
						// 임가공 조합 코드가 존재할 경우
						case "ITEM_NM":
							if (!SET_ITEM_NM.equals("")) {
								ITEM_NM = SET_ITEM_NM; // 위 임가공 조합코드
							} else {
								ITEM_NM = object.get("ITEM_NM").toString(); // 엑셀
																			// 원본
																			// 상품코드
							}
							break;

						case "TRANS_CUST_ADDR":
							TRANS_CUST_ADDR = object.get("TRANS_CUST_ADDR")
									.toString();
							break;
						case "TRANS_EMP_NM":
							TRANS_EMP_NM = object.get("TRANS_EMP_NM")
									.toString();
							break;
						case "REMARK":
							REMARK = object.get("REMARK").toString();
							break;
						case "TRANS_ZIP_NO":
							TRANS_ZIP_NO = object.get("TRANS_ZIP_NO")
									.toString();
							break;
						case "ETC2":
							ETC2 = object.get("ETC2").toString();
							break;
						case "UNIT_AMT":
							UNIT_AMT = object.get("UNIT_AMT").toString();
							break;
						case "TRANS_BIZ_NO":
							TRANS_BIZ_NO = object.get("TRANS_BIZ_NO")
									.toString();
							break;
						case "IN_CUST_ADDR":
							IN_CUST_ADDR = object.get("IN_CUST_ADDR")
									.toString();
							break;
						case "IN_CUST_CD":
							IN_CUST_CD = object.get("IN_CUST_CD").toString();
							break;
						case "IN_CUST_NM":
							IN_CUST_NM = object.get("IN_CUST_NM").toString();
							break;
						case "IN_CUST_TEL":
							IN_CUST_TEL = object.get("IN_CUST_TEL").toString();
							break;
						case "IN_CUST_EMP_NM":
							IN_CUST_EMP_NM = object.get("IN_CUST_EMP_NM")
									.toString();
							break;
						case "EXPIRY_DATE":
							EXPIRY_DATE = object.get("EXPIRY_DATE").toString();
							break;
						case "SALES_CUST_NM":
							SALES_CUST_NM = object.get("SALES_CUST_NM")
									.toString();
							break;
						case "ZIP":
							ZIP = object.get("ZIP").toString();
							break;
						case "ADDR":
							ADDR = object.get("ADDR").toString();
							break;
						case "ADDR2":
							ADDR2 = object.get("ADDR2").toString();
							break;
						case "PHONE_1":
							PHONE_1 = object.get("PHONE_1").toString();
							break;
						case "ETC1":
							ETC1 = object.get("ETC1").toString();
							break;
						case "UNIT_NO":
							UNIT_NO = object.get("UNIT_NO").toString();
							break;
						case "TIME_DATE":
							TIME_DATE = object.get("TIME_DATE").toString();
							break;
						case "TIME_DATE_END":
							TIME_DATE_END = object.get("TIME_DATE_END")
									.toString();
							break;
						case "TIME_USE_END":
							TIME_USE_END = object.get("TIME_USE_END")
									.toString();
							break;
						case "PHONE_2":
							PHONE_2 = object.get("PHONE_2").toString();
							break;
						case "BUY_CUST_NM":
							BUY_CUST_NM = object.get("BUY_CUST_NM").toString();
							break;
						case "BUY_PHONE_1":
							BUY_PHONE_1 = object.get("BUY_PHONE_1").toString();
							break;
						case "ORD_DEGREE":
							ORD_DEGREE = object.get("ORD_DEGREE").toString();
							break;
						case "BIZ_COND":
							BIZ_COND = object.get("BIZ_COND").toString();
							break;
						case "BIZ_TYPE":
							BIZ_TYPE = object.get("BIZ_TYPE").toString();
							break;
						case "BIZ_NO":
							BIZ_NO = object.get("BIZ_NO").toString();
							break;
						case "CUST_TYPE":
							CUST_TYPE = object.get("CUST_TYPE").toString();
							break;
						case "DATA_SENDER_NM":
							DATA_SENDER_NM = object.get("DATA_SENDER_NM")
									.toString();
							break;
						case "LEGACY_ORG_ORD_NO":
							LEGACY_ORG_ORD_NO = object.get("LEGACY_ORG_ORD_NO")
									.toString();
							break;
						case "CUST_SEQ":
							CUST_SEQ = object.get("CUST_SEQ").toString();
							break;
						case "ORD_DESC":
							ORD_DESC = object.get("ORD_DESC").toString();
							break;
						case "DLV_MSG1":
							DLV_MSG1 = object.get("DLV_MSG1").toString();
							break;
						case "DLV_MSG2":
							DLV_MSG2 = object.get("DLV_MSG2").toString();
							break;
						case "SALES_COMPANY_NM":
							SALES_COMPANY_NM = object.get("SALES_COMPANY_NM")
									.toString();
							break;
						case "TRANS_CUST_NM":
							TRANS_CUST_NM = object.get("TRANS_CUST_NM")
									.toString();
							break;
						case "TRANS_CUST_CD":
							TRANS_CUST_CD = object.get("TRANS_CUST_CD")
									.toString();
							break;
						case "LOC_CD":
						    LOC_CD = object.get("LOC_CD").toString();
                            break;
						}

						CUST_CD = (String) model.get("vrCustCd");
						CUST_SEQ = (String) model.get("vrCustSeq");
						// 엑셀에 차수 컬럼 필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값
						// 바인딩)
						if (ORD_DEGREE.equals("")) {
							ORD_DEGREE = (String) model.get("vrSrchOrdDegree");
						}
					}

					String NO = Integer.toString(i + 1);
					no[i] = NO;

					outReqDt[i] = OUT_REQ_DT;
					inReqDt[i] = IN_REQ_DT;
					custOrdNo[i] = CUST_ORD_NO;
					custOrdSeq[i] = CUST_ORD_SEQ;
					trustCustCd[i] = TRUST_CUST_CD;

					transCustCd[i] = TRANS_CUST_CD;
					transCustTel[i] = TRANS_CUST_TEL;
					transReqDt[i] = TRANS_REQ_DT;
					custCd[i] = CUST_CD;
					ordQty[i] = ORD_QTY;

					uomCd[i] = UOM_CD;
					sdeptCd[i] = SDEPT_CD;
					salePerCd[i] = SALE_PER_CD;
					carCd[i] = CAR_CD;
					drvNm[i] = DRV_NM;

					dlvSeq[i] = DLV_SEQ;
					drvTel[i] = DRV_TEL;
					custLotNo[i] = CUST_LOT_NO;
					blNo[i] = BL_NO;
					recDt[i] = REC_DT;

					outWhCd[i] = OUT_WH_CD;
					inWhCd[i] = IN_WH_CD;
					makeDt[i] = MAKE_DT;
					timePeriodDay[i] = TIME_PERIOD_DAY;
					workYn[i] = WORK_YN;

					rjType[i] = RJ_TYPE;
					locYn[i] = LOC_YN;
					confYn[i] = CONF_YN;
					eaCapa[i] = EA_CAPA;
					inOrdWeight[i] = IN_ORD_WEIGHT;

					itemCd[i] = ITEM_CD;
					itemNm[i] = ITEM_NM;
					transCustNm[i] = TRANS_CUST_NM;
					transCustAddr[i] = TRANS_CUST_ADDR;
					transEmpNm[i] = TRANS_EMP_NM;

					remark[i] = REMARK;
					transZipNo[i] = TRANS_ZIP_NO;
					etc2[i] = ETC2;
					unitAmt[i] = UNIT_AMT;
					transBizNo[i] = TRANS_BIZ_NO;

					inCustAddr[i] = IN_CUST_ADDR;
					inCustCd[i] = IN_CUST_CD;
					inCustNm[i] = IN_CUST_NM;
					inCustTel[i] = IN_CUST_TEL;
					inCustEmpNm[i] = IN_CUST_EMP_NM;

					expiryDate[i] = EXPIRY_DATE;
					salesCustNm[i] = SALES_CUST_NM;
					zip[i] = ZIP;
					addr[i] = ADDR;
					addr2[i] = ADDR2;
					phone1[i] = PHONE_1;

					etc1[i] = ETC1;
					unitNo[i] = UNIT_NO;
					timeDate[i] = TIME_DATE;
					timeDateEnd[i] = TIME_DATE_END;
					timeUseEnd[i] = TIME_USE_END;

					phone2[i] = PHONE_2;
					buyCustNm[i] = BUY_CUST_NM;
					buyPhone1[i] = BUY_PHONE_1;
					salesCompanyNm[i] = SALES_COMPANY_NM;
					ordDegree[i] = ORD_DEGREE;
					bizCond[i] = BIZ_COND;
					bizType[i] = BIZ_TYPE;
					bizNo[i] = BIZ_NO;
					custType[i] = CUST_TYPE;

					dataSenderNm[i] = DATA_SENDER_NM;
					legacyOrgOrdNo[i] = LEGACY_ORG_ORD_NO;

					custSeq[i] = CUST_SEQ;

					ordDesc[i] = ORD_DESC;
					dlvMsg1[i] = DLV_MSG1;
					dlvMsg2[i] = DLV_MSG2;
					locCd[i] = LOC_CD;
				}

				/*------------------------------------------------------------------------ 
				 * 동일 원주문번호일 경우 SEQ 직접 계산 (엑셀 원본파일에 순번이 없음)
				 *------------------------------------------------------------------------
				 */

				String[] orgOrdIds = custOrdNo;
				String[] orgOrdSeqs = new String[orgOrdIds.length];

				int seq = 0;
				String prevOrgOrdId = orgOrdIds[0];
				for (int idx = 0; idx < orgOrdIds.length; ++idx) {
					if (prevOrgOrdId.equals(orgOrdIds[idx])) {
						seq += 1;
						orgOrdSeqs[idx] = String.valueOf(seq);
					} else {
						prevOrgOrdId = orgOrdIds[idx];
						seq = 1;
						orgOrdSeqs[idx] = String.valueOf(seq);
					}
				}

				custOrdSeq = orgOrdSeqs;
				/*
				 * --------------------------------------------------------------
				 * ---------
				 */

				// 프로시져에 보낼것들 다담는다
				Map<String, Object> modelIns = new HashMap<String, Object>();

				modelIns.put("vrOrdType", model.get("vrOrdType"));
				modelIns.put("no", no);
				if (model.get("vrOrdType").equals("I")) {
					modelIns.put("reqDt", inReqDt);
					modelIns.put("whCd", inWhCd);
				} else {
					modelIns.put("reqDt", outReqDt);
					modelIns.put("whCd", outWhCd);
				}
				modelIns.put("custOrdNo", custOrdNo);
				modelIns.put("custOrdSeq", custOrdSeq);
				modelIns.put("trustCustCd", trustCustCd); // 5

				modelIns.put("transCustCd", transCustCd);
				modelIns.put("transCustTel", transCustTel);
				modelIns.put("transReqDt", transReqDt);
				modelIns.put("custCd", custCd);
				modelIns.put("ordQty", ordQty); // 10

				modelIns.put("uomCd", uomCd);
				modelIns.put("sdeptCd", sdeptCd);
				modelIns.put("salePerCd", salePerCd);
				modelIns.put("carCd", carCd);
				modelIns.put("drvNm", drvNm); // 15

				modelIns.put("dlvSeq", dlvSeq);
				modelIns.put("drvTel", drvTel);
				modelIns.put("custLotNo", custLotNo);
				modelIns.put("blNo", blNo);
				modelIns.put("recDt", recDt); // 20

				modelIns.put("makeDt", makeDt);
				modelIns.put("timePeriodDay", timePeriodDay);
				modelIns.put("workYn", workYn);

				modelIns.put("rjType", rjType);
				modelIns.put("locYn", locYn); // 25
				modelIns.put("confYn", confYn);
				modelIns.put("eaCapa", eaCapa);
				modelIns.put("inOrdWeight", inOrdWeight); // 28

				modelIns.put("itemCd", itemCd);
				modelIns.put("itemNm", itemNm);
				modelIns.put("transCustNm", transCustNm);
				modelIns.put("transCustAddr", transCustAddr);
				modelIns.put("transEmpNm", transEmpNm);

				modelIns.put("remark", remark);
				modelIns.put("transZipNo", transZipNo);
				modelIns.put("etc2", etc2);
				modelIns.put("unitAmt", unitAmt);
				modelIns.put("transBizNo", transBizNo);

				modelIns.put("inCustAddr", inCustAddr);
				modelIns.put("inCustCd", inCustCd);
				modelIns.put("inCustNm", inCustNm);
				modelIns.put("inCustTel", inCustTel);
				modelIns.put("inCustEmpNm", inCustEmpNm);

				modelIns.put("expiryDate", expiryDate);
				modelIns.put("salesCustNm", salesCustNm);
				modelIns.put("zip", zip);
				modelIns.put("addr", addr);
				modelIns.put("addr2", addr2);
				modelIns.put("phone1", phone1);

				modelIns.put("etc1", etc1);
				modelIns.put("unitNo", unitNo);
				modelIns.put("phone2", phone2);
				modelIns.put("buyCustNm", buyCustNm);
				modelIns.put("buyPhone1", buyPhone1);
				modelIns.put("salesCompanyNm", salesCompanyNm);

				modelIns.put("ordDegree", ordDegree);
				modelIns.put("bizCond", bizCond);
				modelIns.put("bizType", bizType);
				modelIns.put("bizNo", bizNo);
				modelIns.put("custType", custType);

				modelIns.put("dataSenderNm", dataSenderNm);
				modelIns.put("legacyOrgOrdNo", legacyOrgOrdNo);
				// modelIns.put("invoiceNo" , invoiceNo);
				modelIns.put("custSeq", custSeq);

				modelIns.put("ordDesc", ordDesc);
				modelIns.put("dlvMsg1", dlvMsg1);
				modelIns.put("dlvMsg2", dlvMsg2);
				modelIns.put("locCd", locCd);

				modelIns.put("LC_ID", model.get("SS_SVC_NO"));
				modelIns.put("WORK_IP", model.get("SS_CLIENT_IP"));
				modelIns.put("USER_NO", model.get("SS_USER_NO"));
				// 일반 dao
				modelIns = (Map<String, Object>) dao
						.saveExcelOrderB2T(modelIns);

				ServiceUtil.isValidReturnCode("WMSOP030SE3",
						String.valueOf(modelIns.get("O_MSG_CODE")),
						(String) modelIns.get("O_MSG_NAME"));
				// m.put("O_CUR", modelIns.get("O_CUR"));
			}

			m.put("MSG", MessageResolver.getMessage("save.success"));
			m.put("MSG_ORA", "");
			m.put("errCnt", 0);

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * 
	 * 대체 Method ID : saveExcelOrderJavaB2CTrans 대체 Method 설명 : 거래처별 B2C 임가공 템플릿
	 * 입력 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
    public Map<String, Object> saveExcelOrderJavaB2CTrans(Map<String, Object> model,  List<Map<String, Object>> model2) throws Exception {
		
    	List<Map<String, Object>> listHeader = model2;
		List<Map<String, Object>> listBody = (ArrayList)model.get("LIST");
		String custId = String.valueOf(model.get("vrCustCd"));
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	Date today = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String formatted_today = sdf.format(today);

        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        	= new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
                String[] legacyOrgOrdNo	= new String[listBodyCnt];   //사방넷주문번호
//                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
                
                String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
                String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
                String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
                String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
                String[] locCd              =  new String[listBodyCnt];   //로케이션코드
                
                /* 컬럼 바인딩 제약조건 참조 flag */
                String breakYnOrdDegree = "N";
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= "";
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			String DATA_SENDER_NM = "";
        			String LEGACY_ORG_ORD_NO ="";
//        			String INVOICE_NO ="";
        			String CUST_SEQ = "";
        			String ORD_DESC = "";
        			String DLV_MSG1 = "";
        			String DLV_MSG2 = "";
        			String LOC_CD = "";
        			
        			/* 임가공 조합 대상 컬럼 정렬 및 value 가공  */
        			StringBuilder builder = new StringBuilder();
        			Map<Integer, String> combineCols = new HashMap<Integer, String>(); 
        			
        			// 거래처 코드가 존재할 경우 -> ex) 거래처코드_임가공조합컬럼1_임가공조합컬럼2
        			if(!model.get("vrSrchTransCustCd").toString().equals("")){
        				builder.append(model.get("vrSrchTransCustNm")).append("_");
        			}
        			
        			for(int j = 0 ; j < listHeaderCnt ; j++){
        				// 임가공 조합 순번 체크
        				String bindColSeq = listHeader.get(j).get("FORMAT_USE_SEQ").toString();
        				String bindColNm = listHeader.get(j).get("FORMAT_BIND_COL").toString();
        				String bindColValue = listBody.get(i).get(bindColNm).toString();
        				
        				if(!bindColSeq.equals("") && !bindColSeq.equals("0")){
        					combineCols.put(Integer.parseInt(bindColSeq), bindColValue);
        				};
        			}
        			
        			Object[] mapKey = combineCols.keySet().toArray();
    				Arrays.sort(mapKey);
    				
    				for(Integer key : combineCols.keySet()){
    					if(!combineCols.get(key).toString().equals("")){
    						builder.append(combineCols.get(key)).append("_");
    					}
    				}
        			
    				String SET_ITEM_NM = builder.toString().replaceFirst(".$",""); // 마지막 _ 제거
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				
        				Map<String, Object> object = (Map<String, Object>) listBody.get(i);
        				String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
        				
        				switch (bindColNm) {
	        				case "OUT_REQ_DT":OUT_REQ_DT= object.get("OUT_REQ_DT").toString(); break;
	        				case "IN_REQ_DT":IN_REQ_DT= object.get("IN_REQ_DT").toString(); break;
	        				case "CUST_ORD_NO":CUST_ORD_NO= object.get("CUST_ORD_NO").toString(); break;
	        				case "CUST_ORD_SEQ":CUST_ORD_SEQ= object.get("CUST_ORD_SEQ").toString(); break;
	        				case "TRUST_CUST_CD":TRUST_CUST_CD= object.get("TRUST_CUST_CD").toString(); break;
	        				// case "TRANS_CUST_CD":TRANS_CUST_CD= object.get("TRANS_CUST_CD").toString(); break;
	        				case "TRANS_CUST_TEL":TRANS_CUST_TEL= object.get("TRANS_CUST_TEL").toString(); break;
	        				case "TRANS_REQ_DT":TRANS_REQ_DT= object.get("TRANS_REQ_DT").toString(); break;
	        				case "CUST_CD":CUST_CD= object.get("CUST_CD").toString(); break;
	        				case "ORD_QTY":ORD_QTY= object.get("ORD_QTY").toString(); break;
	        				case "UOM_CD":UOM_CD= object.get("UOM_CD").toString(); break;
	        				case "SDEPT_CD":SDEPT_CD= object.get("SDEPT_CD").toString(); break;
	        				case "SALE_PER_CD":SALE_PER_CD= object.get("SALE_PER_CD").toString(); break;
	        				case "CAR_CD":CAR_CD= object.get("CAR_CD").toString(); break;
	        				case "DRV_NM":DRV_NM= object.get("DRV_NM").toString(); break;
	        				case "DLV_SEQ":DLV_SEQ= object.get("DLV_SEQ").toString(); break;
	        				case "DRV_TEL":DRV_TEL= object.get("DRV_TEL").toString(); break;
	        				case "CUST_LOT_NO":CUST_LOT_NO= object.get("CUST_LOT_NO").toString(); break;
	        				case "BL_NO":BL_NO= object.get("BL_NO").toString(); break;
	        				case "REC_DT":REC_DT= object.get("REC_DT").toString(); break;
	        				case "OUT_WH_CD":OUT_WH_CD= object.get("OUT_WH_CD").toString(); break;
	        				case "IN_WH_CD":IN_WH_CD= object.get("IN_WH_CD").toString(); break;
	        				case "MAKE_DT":MAKE_DT= object.get("MAKE_DT").toString(); break;
	        				case "TIME_PERIOD_DAY":TIME_PERIOD_DAY= object.get("TIME_PERIOD_DAY").toString(); break;
	        				case "WORK_YN":WORK_YN= object.get("WORK_YN").toString(); break;
	        				case "RJ_TYPE":RJ_TYPE= object.get("RJ_TYPE").toString(); break;
	        				case "LOC_YN":LOC_YN= object.get("LOC_YN").toString(); break;
	        				case "CONF_YN":CONF_YN= object.get("CONF_YN").toString(); break;
	        				case "EA_CAPA":EA_CAPA= object.get("EA_CAPA").toString(); break;
	        				case "IN_ORD_WEIGHT":IN_ORD_WEIGHT= object.get("IN_ORD_WEIGHT").toString(); break;
	        				// 임가공 조합 코드가 존재할 경우
	        				case "ITEM_NM":
	        						if(!SET_ITEM_NM.equals("")){
	        							ITEM_NM= SET_ITEM_NM;
	        						}else{
	        							ITEM_NM= object.get("ITEM_NM").toString();
	        						}
	        						break;
	        				case "ITEM_CD":ITEM_NM= object.get("ITEM_CD").toString(); break;
	        				//case "TRANS_CUST_NM":TRANS_CUST_NM= object.get("TRANS_CUST_NM").toString(); break;
	        				case "TRANS_CUST_ADDR":TRANS_CUST_ADDR= object.get("TRANS_CUST_ADDR").toString(); break;
	        				case "TRANS_EMP_NM":TRANS_EMP_NM= object.get("TRANS_EMP_NM").toString(); break;
	        				case "REMARK":REMARK= object.get("REMARK").toString(); break;
	        				case "TRANS_ZIP_NO":TRANS_ZIP_NO= object.get("TRANS_ZIP_NO").toString(); break;
	        				case "ETC2":ETC2= object.get("ETC2").toString(); break;
	        				case "UNIT_AMT":UNIT_AMT= object.get("UNIT_AMT").toString(); break;
	        				case "TRANS_BIZ_NO":TRANS_BIZ_NO= object.get("TRANS_BIZ_NO").toString(); break;
	        				case "IN_CUST_ADDR":IN_CUST_ADDR= object.get("IN_CUST_ADDR").toString(); break;
	        				case "IN_CUST_CD":IN_CUST_CD= object.get("IN_CUST_CD").toString(); break;
	        				case "IN_CUST_NM":IN_CUST_NM= object.get("IN_CUST_NM").toString(); break;
	        				case "IN_CUST_TEL":IN_CUST_TEL= object.get("IN_CUST_TEL").toString(); break;
	        				case "IN_CUST_EMP_NM":IN_CUST_EMP_NM= object.get("IN_CUST_EMP_NM").toString(); break;
	        				case "EXPIRY_DATE":EXPIRY_DATE= object.get("EXPIRY_DATE").toString(); break;
	        				case "SALES_CUST_NM":SALES_CUST_NM= object.get("SALES_CUST_NM").toString(); break;
	        				case "ZIP":ZIP= object.get("ZIP").toString(); break;
	        				case "ADDR":ADDR= object.get("ADDR").toString(); break;
	        				case "ADDR2":ADDR2= object.get("ADDR2").toString(); break;
	        				case "PHONE_1":PHONE_1= object.get("PHONE_1").toString(); break;
	        				case "ETC1":ETC1= object.get("ETC1").toString(); break;
	        				case "UNIT_NO":UNIT_NO= object.get("UNIT_NO").toString(); break;
	        				case "TIME_DATE":TIME_DATE= object.get("TIME_DATE").toString(); break;
	        				case "TIME_DATE_END":TIME_DATE_END= object.get("TIME_DATE_END").toString(); break;
	        				case "TIME_USE_END":TIME_USE_END= object.get("TIME_USE_END").toString(); break;
	        				case "PHONE_2":PHONE_2= object.get("PHONE_2").toString(); break;
	        				case "BUY_CUST_NM":BUY_CUST_NM= object.get("BUY_CUST_NM").toString(); break;
	        				case "BUY_PHONE_1":BUY_PHONE_1= object.get("BUY_PHONE_1").toString(); break;
	        				case "SALES_COMPANY_NM":SALES_COMPANY_NM= object.get("SALES_COMPANY_NM").toString(); break;
	        				case "ORD_DEGREE":ORD_DEGREE= object.get("ORD_DEGREE").toString(); break;
	        				case "BIZ_COND":BIZ_COND= object.get("BIZ_COND").toString(); break;
	        				case "BIZ_TYPE":BIZ_TYPE= object.get("BIZ_TYPE").toString(); break;
	        				case "BIZ_NO":BIZ_NO= object.get("BIZ_NO").toString(); break;
	        				case "CUST_TYPE":CUST_TYPE= object.get("CUST_TYPE").toString(); break;
	        				case "DATA_SENDER_NM":DATA_SENDER_NM = object.get("DATA_SENDER_NM").toString(); break;
	        				case "LEGACY_ORG_ORD_NO":LEGACY_ORG_ORD_NO = object.get("LEGACY_ORG_ORD_NO").toString(); break;
	        				case "CUST_SEQ":CUST_SEQ = object.get("CUST_SEQ").toString(); break;
	        				case "ORD_DESC":ORD_DESC = object.get("ORD_DESC").toString(); break;
	        				case "DLV_MSG1":DLV_MSG1 = object.get("DLV_MSG1").toString(); break;
	        				case "DLV_MSG2":DLV_MSG2= object.get("DLV_MSG2").toString(); break;
	        				case "LOC_CD":LOC_CD= object.get("LOC_CD").toString(); break;
						}
        				
        				TRANS_CUST_CD = (String)model.get("vrSrchTransCustCd");
        				TRANS_CUST_NM = (String)model.get("vrSrchTransCustNm");
        				
        				CUST_CD = (String)model.get("vrCustCd");
						// SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
						CUST_SEQ= (String)model.get("vrCustSeq");
						// 엑셀에 차수 컬럼 필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩)
						if(ORD_DEGREE.equals("")){
							ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
						}
        			}
        			
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                    
                    dataSenderNm[i]       		= DATA_SENDER_NM;
                    legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
                    
                    custSeq[i]					= CUST_SEQ;
                    
                    ordDesc[i]     			  	= ORD_DESC;
                    dlvMsg1[i]       			= DLV_MSG1;
                    dlvMsg2[i]       			= DLV_MSG2;
                    locCd[i]                  = LOC_CD;
                }
                
                /*------------------------------------------------------------------------ 
    			 * 동일 원주문번호일 경우 SEQ 직접 계산 (엑셀 원본파일에 순번이 없음)
    			 *------------------------------------------------------------------------
    			*/
    			
    			String [] orgOrdIds = custOrdNo;
    			String [] orgOrdSeqs = new String [orgOrdIds.length];
    			
    			
    			int seq = 0;
    			String prevOrgOrdId = orgOrdIds[0];
    			for(int idx = 0; idx < orgOrdIds.length; ++ idx){
    				if(prevOrgOrdId.equals(orgOrdIds[idx])){
    					seq += 1;
    					orgOrdSeqs[idx] = String.valueOf(seq);
    				}else{
    					prevOrgOrdId = orgOrdIds[idx];
    				    seq = 1;
						orgOrdSeqs[idx] = String.valueOf(seq);
    				}
    			}
    			
    			custOrdSeq = orgOrdSeqs;
                /* ----------------------------------------------------------------------- */
    			
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
               
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"		, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"		, bizCond);
                modelIns.put("bizType"		, bizType);
                modelIns.put("bizNo"		, bizNo);
                modelIns.put("custType"		, custType);
                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                modelIns.put("invoiceNo"		, invoiceNo);
                modelIns.put("custSeq"       	, custSeq);
                
                modelIns.put("ordDesc"       	, ordDesc);
                modelIns.put("dlvMsg1"       	, dlvMsg1);
                modelIns.put("dlvMsg2"       	, dlvMsg2);
                modelIns.put("locCd"            , locCd);
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2T(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e) {
           throw e;
        }
        return m;
    }

	/**
	 * 주문동기화 V2
	 * 임가공주문관리에서 체크박스 선택된 데이터로 저장한다.
	 * */
	@Override
	public Map<String, Object> saveSyncOrderJavaB2C_V2(Map<String, Object> model, Map<String, Object> reqMap)
		throws Exception {
	    Map<String, Object> m = new HashMap<String, Object>();
	    
	    try {
		List<Map<String, Object>> orderList = (List<Map<String, Object>>) reqMap.get("orderList");
		String vrSrchCustId = reqMap.get("vrSrchCustId").toString();
		model.put("vrSrchCustId", vrSrchCustId);
		
		/* 상품코드 등록여부 확인 */
		if(orderList.size() > 0) {
		    List<Object> noRegistItemList = findNoRegistWinusItem(model, orderList);
		    if(noRegistItemList.size() > 0) {
			List<String> roRegistItemCds = new ArrayList<>();
			for (Object object : noRegistItemList) {
			    Map<String, Object> noRegItemMap = (Map<String, Object>) object;
			    roRegistItemCds.add(noRegItemMap.get("ORD_ITEM_CD").toString());
			}
			throw new BizException("미등록 상품이 있습니다.\n미등록 상품코드 : "+roRegistItemCds);
		    }
		}
		
		// 프로시져에 보낼것들 다담는다
		Map<String, Object> modelIns = new HashMap<String, Object>();
		Map<String, String[]> orderParamMap = 
			CommonUtil.convertMapStringArrayFromListMap(
				orderList,
		                new String[]{
		                	"ORD_DT",
		                	"ORG_ORD_ID",
		                	"ORG_ORD_SEQ",
		                	"REG_CUST_ID",
		                	"REG_CUST_CD",
		                	"ORD_MAKER",
		                	"ORD_QTY",
		                	"ORD_ITEM_CD",
		                	"ORD_ITEM_NM",
		                	"ORD_ITEM_OP_NM",
		                	"TO_CUSTOMER_NM",
		                	"TO_ZIP",
		                	"TO_ADDRESS_1",
		                	"TO_ADDRESS_2",
		                	"TO_TEL_1",
		                	"TO_TEL_2",
		                	"FROM_CUSTMOR_NM",
		                	"FROM_TEL_1",
		                	"DATA_SENDER_NM",
		                	"DLV_DESC1",
		                	"DLV_DESC2",
		                	"LEGACY_ORG_ORD_ID"
		                    }
		            );
		modelIns.put("vrOrdType", "O"); // 출고주문이기에 고정값 추후 수정해야할수 있음
		int ordSize = orderList.size();
		String[] emptyValue = new String[ordSize];
		String[] plSqlKeys = new String[ordSize];
		for (int i = 0; i < ordSize; i++) {
		    plSqlKeys[i] = String.valueOf(i + 1);
		}
		modelIns.put("no", plSqlKeys);

		modelIns.put("reqDt", orderParamMap.get("ORD_DT"));
		modelIns.put("whCd", emptyValue);

		modelIns.put("custOrdNo", orderParamMap.get("ORG_ORD_ID"));
		modelIns.put("custOrdSeq", orderParamMap.get("ORG_ORD_SEQ"));
		modelIns.put("trustCustCd", emptyValue); // 5

		modelIns.put("transCustCd", orderParamMap.get("ORD_MAKER"));
		modelIns.put("transCustTel", emptyValue);
		modelIns.put("transReqDt", emptyValue);
		modelIns.put("custCd", orderParamMap.get("REG_CUST_CD"));
		modelIns.put("ordQty", orderParamMap.get("ORD_QTY")); // 10

		modelIns.put("uomCd", emptyValue);
		modelIns.put("sdeptCd", emptyValue);
		modelIns.put("salePerCd", emptyValue);
		modelIns.put("carCd", emptyValue);
		modelIns.put("drvNm", emptyValue); // 15

		modelIns.put("dlvSeq", emptyValue);
		modelIns.put("drvTel", emptyValue);
		modelIns.put("custLotNo", emptyValue);
		modelIns.put("blNo", emptyValue);
		modelIns.put("recDt", emptyValue); // 20

		modelIns.put("makeDt", emptyValue);
		modelIns.put("timePeriodDay", emptyValue);
		modelIns.put("workYn", emptyValue);

		modelIns.put("rjType", emptyValue);
		modelIns.put("locYn", emptyValue); // 25
		modelIns.put("confYn", emptyValue);
		modelIns.put("eaCapa", emptyValue);
		modelIns.put("inOrdWeight", emptyValue); // 28

		modelIns.put("itemCd", orderParamMap.get("ORD_ITEM_CD"));
		modelIns.put("itemNm", orderParamMap.get("ORD_ITEM_NM"));
		modelIns.put("transCustNm", emptyValue);
		modelIns.put("transCustAddr", emptyValue);
		modelIns.put("transEmpNm", emptyValue);

		modelIns.put("remark", emptyValue);
		modelIns.put("transZipNo", emptyValue);
		modelIns.put("etc2", orderParamMap.get("ORD_ITEM_OP_NM"));
		modelIns.put("unitAmt", emptyValue);
		modelIns.put("transBizNo", emptyValue);

		modelIns.put("inCustAddr", emptyValue);
		modelIns.put("inCustCd", emptyValue);
		modelIns.put("inCustNm", emptyValue);
		modelIns.put("inCustTel", emptyValue);
		modelIns.put("inCustEmpNm", emptyValue);

		modelIns.put("expiryDate", emptyValue);
		modelIns.put("salesCustNm", orderParamMap.get("TO_CUSTOMER_NM"));
		modelIns.put("zip", orderParamMap.get("TO_ZIP"));
		modelIns.put("addr", orderParamMap.get("TO_ADDRESS_1"));
		modelIns.put("addr2", orderParamMap.get("TO_ADDRESS_2"));
		modelIns.put("phone1", orderParamMap.get("TO_TEL_1"));

		modelIns.put("etc1", emptyValue);
		modelIns.put("unitNo", emptyValue);
		modelIns.put("phone2", orderParamMap.get("TO_TEL_2"));
		modelIns.put("buyCustNm", orderParamMap.get("FROM_CUSTMOR_NM"));
		modelIns.put("buyPhone1", orderParamMap.get("FROM_TEL_1"));
		modelIns.put("salesCompanyNm", emptyValue);

		modelIns.put("ordDegree", emptyValue);
		modelIns.put("bizCond", emptyValue);
		modelIns.put("bizType", emptyValue);
		modelIns.put("bizNo", emptyValue);
		modelIns.put("custType", emptyValue);

		modelIns.put("dataSenderNm", orderParamMap.get("DATA_SENDER_NM"));
		modelIns.put("legacyOrgOrdNo", orderParamMap.get("LEGACY_ORG_ORD_ID"));
		// modelIns.put("invoiceNo" , invoiceNo);
		modelIns.put("custSeq", emptyValue);

		modelIns.put("ordDesc", emptyValue);
		modelIns.put("dlvMsg1", orderParamMap.get("DLV_DESC1"));
		modelIns.put("dlvMsg2", orderParamMap.get("DLV_DESC2"));
		modelIns.put("locCd", emptyValue);

		modelIns.put("LC_ID", model.get("SS_SVC_NO"));
		modelIns.put("WORK_IP", model.get("SS_CLIENT_IP"));
		modelIns.put("USER_NO", model.get("SS_USER_NO"));
		modelIns = (Map<String, Object>) dao
			.saveExcelOrderB2T(modelIns);

        	if (modelIns.get("O_MSG_CODE").toString().equals("0")) {
        	    
        	    for (Map<String, Object> orderMap : orderList) {
        		dao.updateWmsLink(orderMap);
		    }
        	   
        	}
		
		m.put("MSG", MessageResolver.getMessage("save.success"));
		m.put("MSG_ORA", "");
		m.put("errCnt", 0);
	    } catch (Exception e) {
		throw e;
	    }
	    return m;
	}}
