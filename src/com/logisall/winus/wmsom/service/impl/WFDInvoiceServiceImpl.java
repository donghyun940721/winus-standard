package com.logisall.winus.wmsom.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.wmsom.service.WFDInvoiceService;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
public class WFDInvoiceServiceImpl implements WFDInvoiceService{
	private final Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private WFDInvoiceDao wfdInvoiceDao;

	/**
	 * 거래명세서 목록
	 * 주문별 배차정보 매핑
	 * */
	@Override
	public Map<String, Object> selectOrderList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String , Object>> orderList = wfdInvoiceDao.selectOrderList(model);
		
		List<List<Map<String, Object>>> splitOrders = CommonUtil.splitList(orderList, 1000);
		List<Map<String , Object>> returnOrderList = new ArrayList<Map<String,Object>>();
		try {
			for (List<Map<String, Object>> orders : splitOrders) {
				model.put("orderList", orders);
				List<Map<String, Object>> selectTmsLoadList = wfdInvoiceDao.selectTmsLoadList(model);
			
				mappingOrderToTmsLoad(returnOrderList, orders, selectTmsLoadList);
			}
			resultMap.put("errCnt", "0");
			resultMap.put("LIST", returnOrderList);
		} catch (Exception e) {
			log.error(e);
			resultMap.put("errCnt", "1");
			resultMap.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		return resultMap;
	}

	private void mappingOrderToTmsLoad(List<Map<String, Object>> returnOrderList, List<Map<String, Object>> orders,
			List<Map<String, Object>> selectTmsLoadList) {
		
		 // TMS Load 데이터를 맵으로 변환하여 검색을 빠르게 한다.
	    Map<String, Map<String, Object>> tmsLoadMap = new HashMap<>();
	    for (Map<String, Object> tmsLoad : selectTmsLoadList) {
	        String shipmentNo = (String) tmsLoad.get("SHPMNO");
	        tmsLoadMap.put(shipmentNo, tmsLoad);
	    }
	    
	    // Orders 리스트를 순회하며 데이터를 매핑한다.
	    for (Map<String, Object> order : orders) {
	        String ordOrderId = (String) order.get("ORG_ORD_ID");
	        String routDesc = "";
	        String truckNumber = "";
	        String driverName = "";
	        // TMS Load 맵에서 데이터 검색 및 매핑
	        Map<String, Object> tmsLoad = tmsLoadMap.get(ordOrderId);
	        if (tmsLoad != null) {
	        	routDesc = (String)tmsLoad.get("ROUTDESC");
	        	truckNumber = (String)tmsLoad.get("TRCNUM");
	        	driverName = (String)tmsLoad.get("DVNM");
	        }
	        order.put("ROUTDESC", routDesc);
            order.put("TRCNUM", truckNumber);
            order.put("DVNM", driverName);
	        returnOrderList.add(order);
	    }
	    
	}

	/**
	 * 주문 상세 조회
	 */
	@Override
	public Map<String, Object> selectOrderDetailList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> resultList= wfdInvoiceDao.selectOrderDetailList(model);
		resultMap.put("LIST", resultList);
		resultMap.put("LIST_SIZE", resultList.size());
		return resultMap;
	}

	
	/**
	 * 송장출력횟수 업데이트
	 * */
	@Override
	public Map<String, Object> updatePrintCount(Map<String, Object> model, Map<String, Object> reqMap) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		SessionListener.setSessionParams(reqMap);
		wfdInvoiceDao.updatePrintCount(reqMap);
		
		resultMap.put("errCnt", "0");
		resultMap.put("msg", MessageResolver.getMessage("save.success"));
		return resultMap;
	}
}
