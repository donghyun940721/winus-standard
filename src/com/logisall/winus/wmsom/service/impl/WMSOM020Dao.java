package com.logisall.winus.wmsom.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOM020Dao")
public class WMSOM020Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.list", model);
    }
    
    /**
     * Method ID  : listDetail
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.listDetail", model);
    }
    
    
    /**
     * Method ID  : listTel
     * Method 설명   : 
     * 작성자                : khkim
     * @param   model
     * @return
     */
    public GenericResultSet listCar(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.listCar", model);
    }
    
    /**
     * Method ID  : listTelDetail
     * Method 설명   : 
     * 작성자                : khkim
     * @param   model
     * @return
     */
    public GenericResultSet listTelDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.listTelDetail", model);
    }
    
	 /**
     * Method ID : confirmOrder 
     * Method 설명 : 입고확정- 날짜로만
     * 작성자 : khkim
     * 
     * @param model
     * @return
     */
    public Object confirmOrder(Map<String, Object> model) {
    	return executeUpdate("wmsom020.PK_WMSOM010.SP_EDIYA_WINUS_INSERT_ORDER", model);
    }
    
    /**
     * Method ID    : saveInOrder
     * Method 설명      : 입고확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object saveInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order", model);
        return model;
    }
    
    /**
     * Method ID    : saveInOrder
     * Method 설명      : 입고확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object saveInOrder_2048(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order_2048", model);
        return model;
    }
    
    
    /**
     * Method ID    : saveInOrder_2048_21
     * Method 설명      : 입고확정
     * 작성자                 : YHKU
     * @param   model
     * @return
     */
    public Object saveInOrder_2048_21(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop021.sp_new_insert_order_2048", model);
        return model;
    }
    
    /**
     * Method ID    : listNew
     * Method 설명      : 
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
	public Object listNew(Map<String, Object> model) {
		return executeQueryPageWq("wmsom020.listNew2", model);
	}
	
	/**
     * Method ID	: listNew2
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return
     */
	public Object listNew2(Map<String, Object> model) {
		return executeQueryPageWq("wmsom121.listNew2", model);
	}
	
	/**
     * Method ID    : deleteInOrder
     * Method 설명      : 입고확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
	public Object deleteInOrder(Map<String, Object> model) {
		return executeUpdate("wmsom020.deleteInOrder", model);
	}
	
	 public Object getCustLotNo(Map<String, Object> model){
        return executeView("wmsom020.getCustLotNo", model);
    }
	 
	public Object getCustLotNoVer2(Map<String, Object> model){
        return executeView("wmsom020.getCustLotNoVer2", model);
    }
	 
	 public Object issueLabelOrdCom(Map<String, Object> model){
        executeUpdate("wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate", model);
        return model;
    }
	 
	 public int changOrderList(Map<String, Object> model) {
			return executeUpdate("wmsom020.changOrderList", model);
		}
	 
	 
	/**
	 * Method ID   : saveExcelOrderB2O
	 * Method 설명    : 템플릿 입고 주문 저장 (pk_wmsop030O)
	 * 작성자              : dhkim
	 * @param   model
	 * @return
	 */
	public Object saveExcelOrderB2O(Map<String, Object> model){
	    executeUpdate("wmsom020.pk_wmsop030o.sp_insert_template", model);
	    return model;
	}
	 
	
    /**
     * Method ID : omImgList
     * Method 설명 : 
     * 작성자 : 	 yhku
     * @param model
     * @return
     */
    public Object omImgList(Map<String, Object> model){
        return executeQueryForList("wmsom020.omImgList", model);
    }
    
    
    
    /**
     * Method ID    : saveOmsOrdPLTDividedComplete
     * Method 설명      : 주문을 파렛트 수량으로 나눠서 주문 입력
     * 작성자                 : YHKU
     * @param   model
     * @return
     */
    public Object saveOmsOrdPLTDividedComplete(Map<String, Object> model){
        executeUpdate("wmsom020.pk_wmsop021.sp_in_oms_ord_divided_complete", model);
        return model;
    }
    
    
    /**
     * Method ID       : saveOmsOrdNewtongComplete
     * Method 설명      : 뉴통 로케이션, 유통기한, 제조일자등 정보 등록후 주문등록
     * 작성자            : yangjiwoon
     * @param   model
     * @return
     */
    public Object saveOmsOrdNewtongComplete(Map<String, Object> model){
        executeUpdate("wmsom020.pk_wmsop021.sp_oms_op_loc_day_insert_order", model);
        return model;
    }
    
    /**
     * Method ID  : selectNewtongOrd
     * Method 설명 : 뉴통팝업주문 조회
     * 작성자       : yangjiwoon
     * @param model
     * @return
     */
    public List selectNewtongOrd(Map<String, String> model){
        return executeQueryForList("wmsom020.selectNewtongOrd", model);
    }

    public void deleteOutOrder(Map<String, Object> model) {
	 executeUpdate("wmsom020.deleteOutOrder", model);	
    }
    
}


