package com.logisall.winus.wmsom.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
@SuppressWarnings(value = {"rawtypes", "unchecked"})
public class WFDInvoiceDao extends SqlMapAbstractDAO{	
	
	@Autowired
	private SqlMapClient sqlMapClientWinusIf;
	
	public List selectOrderList(Map<String, Object> param){
		return executeQueryForList("wfd-invoice.selectOrderList", param);
	}
	
	public List selectOrderDetailList(Map<String, Object> param){
		return executeQueryForList("wfd-invoice.selectOrderDetailList", param);
	}

	public List<Map<String, Object>> selectTmsLoadList(Map<String, Object> param) throws SQLException {
		return sqlMapClientWinusIf.queryForList("wfd-invoice.selectTmsLoadList", param);
	}

	public void updatePrintCount(Map<String, Object> param) {
		executeUpdate("wfd-invoice.updatePrintCount", param);
	}

}
