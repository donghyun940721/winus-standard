package com.logisall.winus.wmsom.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOM150Dao")
public class WMSOM150Dao extends SqlMapAbstractDAO{
	/**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : atomyoun
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsom150.list", model);
    }
    
    /**
     * Method ID    	: saveOutOrder
     * Method 설명       : 출고확정
     * 작성자              : schan
     * @param   model
     * @return
     */
    public Object saveOutOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_insert_order_2048", model);
        return model;
    }
}
