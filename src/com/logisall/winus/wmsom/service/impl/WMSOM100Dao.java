package com.logisall.winus.wmsom.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOM100Dao")
public class WMSOM100Dao extends SqlMapAbstractDAO{
	
	/**
	 * Method ID   	: listE1
	 * Method 설명 	: 임가공 주문관리 조회
	 * 작성자 			: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listE1(Map<String, Object> model) {
		return executeQueryPageWq("wmsom100.listE1", model);
	}
	
	/**
	 * Method ID   	: listE2_H
	 * Method 설명 	: 임가공 주문관리E2_header
	 * 작성자 			: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listE2_H(Map<String, Object> model) {
		return executeQueryPageWq("wmsom100.listE2_H", model);
	}
	
	/**
	 * Method ID   	: listE2_H_V2
	 * Method 설명 	: 임가공 주문관리E2_V2_header
	 * 작성자 			: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public List listE2_H_V2(Map<String, Object> model) {
		return executeQueryForList("wmsom100.listE2_H_V2", model);
	}
	
	public List<Map<String, Object>> listE2_ITEMLIST(Map<String, Object> model) {
		return executeQueryForList("wmsom100.listE2_ITEMLIST", model);
	}
	
	/**
	 * Method ID   	: listE3_H
	 * Method 설명 	: 임가공 주문관리E3_header
	 * 작성자 			: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listE3_H(Map<String, Object> model) {
		return executeQueryPageWq("wmsom100.listE3_H", model);
	}
	
	/**
	 * Method ID   	: listE3_D
	 * Method 설명 	: 임가공 주문관리E3_detail
	 * 작성자 			: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listE3_D(Map<String, Object> model) {
		return executeQueryPageWq("wmsom100.listE3_D", model);
	}
	
    /**
     * Method ID    	: updateSetItemOm
     * Method 설명   : WMSOM000 주문테이블 상품코드 업데이트
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object updateSetItemOm(Map<String, Object> model){
        executeUpdate("wmsom100.updateSetItemOm", model);
        return model;
    }
	
	 /**
     * Method ID    		 : getSetItem
     * Method 설명        : 세트상품 RITEM_ID 조회
     * 작성자                 : KSJ
     * @param   model
     * @return
     */
    public Object  getSetItem(Map<String, Object> model){
    	return executeQueryForObject("wmsom100.getSetItem", model);
    	//return executeQueryForList("wmsom100.getSetItem", model);
    }
    
    /**
	 * Method ID   	: getSyncList
	 * Method 설명 	: 주문 동기화 리스트 조회
	 * 작성자 			: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public List<Map<String, Object>> getSyncList(Map<String, Object> model) {
		return (List<Map<String, Object>>) executeQueryForList("wmsom100.getSyncList", model);
	}
    
    /**
     * Method ID    		 : getUomList
     * Method 설명        : UOM 리스트 조회
     * 작성자                 : KSJ
     * @param   model
     * @return
     */
    public Object  getUomList(Map<String, Object> model){
    	return executeQueryForList("wmsom100.getUomList", model);
    }
    
    /**
     * Method ID             : getUomList
     * Method 설명        : 임가공 세트 저장전 EA UOM ID 가져오기
     * 작성자                 : 
     * @param   model
     * @return
     */
    public Object getEAUomId(Map<String, Object> model){
        return executeQueryForObject("wmsom100.getEAUomId", model);
    }
    
    /**
     * Method ID             : getUomList
     * Method 설명        : 임가공 세트 저장전 validation 체크
     * 작성자                 : 
     * @param   model
     * @return
     */
    public Object setItemValidationCount(Map<String, Object> model){
        return executeQueryForObject("wmsom100.setItemValidationCount", model);
    }
    
	  /**
     * Method ID    	: saveSetItem
     * Method 설명   : 임가공 세트 상품 생성
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object saveSetItem(Map<String, Object> model){
        executeUpdate("wmsom100.pk_wmsms090e.sp_save_item", model);
        return model;
    }
    
    /**
     * Method ID    	: updateWmsLink
     * Method 설명   : WMSOM000 주문 WMS LINK 처리
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object updateWmsLink(Map<String, Object> model){
        return executeUpdate("wmsom100.updateWmsLink", model);
    }
    
    /**
     * Method ID    	: deleteYnOm
     * Method 설명   : WMSOM000 주문 WMS LINK 처리
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object deleteYnOm(Map<String, Object> model){
        return executeUpdate("wmsom100.deleteYnOm", model);
    }
    
    /**
     * Method ID    	: updateSetItem
     * Method 설명   : 임가공 세트 상품 -> 임가공 여부 Y 처리
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object updateSetItem(Map<String, Object> model){
        return executeUpdate("wmsom100.updateSetItem", model);
    }
    
    /**
     * Method ID    	: deleteSetItem91
     * Method 설명   	: 임가공 세트 상품 WMSMS091 삭제
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object deleteSetItem91(Map<String, Object> model){
        executeUpdate("wmsom100.deleteSetItem91", model);
        return model;
    }
    
    
    /**
     * Method ID    	: deleteSetItemE3
     * Method 설명   	: 임가공 세트 상품 WMSMS091 삭제
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws Exception 
     */
    public Object deleteSetItemE3(Map<String, Object> model) throws Exception{
    	SqlMapClient sqlMapClient = getSqlMapClient();
    	try {
    	    sqlMapClient.startTransaction();
    	    
    	    sqlMapClient.update("wmsom100.deleteE3_092", model);
    	    
    	    sqlMapClient.update("wmsom100.deleteE3_091", model);
    	    
    	    sqlMapClient.update("wmsom100.deleteSetItemOm", model);
    	 
    	    
    	    sqlMapClient.endTransaction();
    	} catch(Exception e) {
    	    sqlMapClient.endTransaction();
    	    e.printStackTrace();
    	    throw e;
    	    
    	} finally {
    	    if (sqlMapClient != null) {
    	        sqlMapClient.endTransaction();
    	    }
    	}
        return model;
    }
    
    /**
     * Method ID    	: deleteSetItemOm
     * Method 설명   	: 임가공 주문 세트상품코드 초기화 (세트상품 삭제시)
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object deleteSetItemOm(Map<String, Object> model){
        executeUpdate("wmsom100.deleteSetItemOm", model);
        return model;
    }
    
    
    /**
     * Method ID    	: deleteSetItem92
     * Method 설명   : 임가공 세트 상품 WMSMS092 삭제
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object deleteSetItem92(Map<String, Object> model){
        executeUpdate("wmsom100.deleteSetItem92", model);
        return model;
    }
    
    /**
     * Method ID    	: savePartItem
     * Method 설명   : 임가공 부속 상품 생성
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object savePartItem(Map<String, Object> model){
    	executeInsert("wmsom100.savePartItem", model);
        return model;
    }
    
    /**
     * Method ID    	: deleteE2
     * Method 설명   	: 임가공 대상 상품 (주문단) 삭제
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object deleteE2(Map<String, Object> model){
        executeUpdate("wmsom100.deleteE2", model);
        return model;
    }
    
    /**
     * Method ID    	: deleteE3_092
     * Method 설명   	: 임가공 세트 상품 삭제
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object deleteE3_092(Map<String, Object> model){
        executeUpdate("wmsom100.deleteE3_092", model);
        return model;
    }
    
    /**
     * Method ID    	: deleteE3_091
     * Method 설명   	: 상품 삭제
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object deleteE3_091(Map<String, Object> model){
        executeUpdate("wmsom100.deleteE3_091", model);
        return model;
    }
    
    /**
     * Method ID    	: updatePartItem
     * Method 설명   : 임가공 부속 상품 수정
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object updatePartItem(Map<String, Object> model){
        executeUpdate("wmsom100.updatePartItem", model);
        return model;
    }
    
    /**
     * Method ID    	: deletePartItem
     * Method 설명   : 임가공 부속 상품 삭제
     * 작성자            : KSJ
     * @param   model
     * @return
     */
    public Object deletePartItem(Map<String, Object> model){
        executeUpdate("wmsom100.deletePartItem", model);
        return model;
    }
    
    /**
     * Method ID   		 : getTemplateInfo
     * Method 설명      	 : 미리 등록한 납품처 별 템플릿 정보 가져오기.
     * 작성자                 : ykim
     * @param   model
     * @return
     */
    public List<Map<String, Object>> getTemplateInfo(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsom100.selectTemplateInfo", model);
    }
	
    /**
     * Method ID    		: searchTpCol
     * Method 설명       : 화주별 컬럼
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public List<Map<String, Object>>  searchTpCol(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsom100.sp_sel_tpcol", model);
    }
    
	/**
     * Method ID    		: saveExcelOrderB2T
     * Method 설명       : 템플릿 주문 저장 (pk_wmsop030T)
     * 작성자              	: KSJ
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2T(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030t.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    		 : getTransCustList
     * Method 설명        : 거래처 조회 
     * 작성자                 : KSJ
     * @param   model
     * @return
     */
    public Object  getTransCustList(Map<String, Object> model){
    	return executeQueryForList("wmsom100.getTransCustList", model);
    }
    
}


