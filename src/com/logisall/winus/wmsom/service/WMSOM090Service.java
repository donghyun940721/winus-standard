package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface WMSOM090Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItemTabs1(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSimpleOutOrderTemp(Map<String, Object> model) throws Exception;
    public Map<String, Object> ReqOrdSeq(Map<String, Object> model) throws Exception;
    public Map<String, Object> listOrdDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateOm090(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubQtyExchange(Map<String, Object> model) throws Exception;
    
    
}
