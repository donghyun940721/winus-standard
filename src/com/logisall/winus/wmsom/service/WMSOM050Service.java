package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface WMSOM050Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveOutOrder(Map<String, Object> model) throws Exception;
}
