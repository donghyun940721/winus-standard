package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface OutOrderRegService {

	Map<String, Object> outOrderCheck(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

	Map<String, Object> saveOutOrder(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

}
