package com.logisall.winus.frm.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;

@Configuration
public class PropertiesConfig {
  private static final Log log = LogFactory.getLog(PropertiesConfig.class);
  private static final String DEFAULT_CONFIG_RESOURCE = "/resource/config.properties";
  private static final String COMMON_APP_RESOURCE ="/resource/framework/common_app.properties"; 
 
  @Autowired
  private Environment environment;

  @Bean
  public String getEnvActiveProfile(Environment env) {
    String value = env.getProperty("spring.profiles.active");
    return value;
  }

  @Bean
  public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer(Environment environment) {
    PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();

    // spring.profiles.active 시스템 프로퍼티를 가져옴.
    String activeProfile = environment.getProperty("spring.profiles.active");
    log.info("spring.profiles.active=" + activeProfile);
    if (StringUtils.isEmpty(activeProfile)) {
      configurer.setLocations(
          new ClassPathResource(DEFAULT_CONFIG_RESOURCE), 
          new ClassPathResource(COMMON_APP_RESOURCE)
          );
    } else {
      String customResource = String.format("/resource/config-%s.properties", activeProfile);
      // ClassPathResource propertiesResource = new ClassPathResource(customResource);
      // if(!propertiesResource.exists()){
      // log.info(customResource + "is not exist");
      // configurer.setLocations(new ClassPathResource(DEFAULT_RESOURCE));
      // return configurer;
      // }
      configurer.setLocations(
          new ClassPathResource(customResource), 
          new ClassPathResource(COMMON_APP_RESOURCE)
          );
    }

    return configurer;
  }

}
