package com.logisall.winus.frm.exception;

import com.m2m.jdfw5x.message.Message;

/**
 * @author kwt
 * 중복 사용자 발생
 */
public class DupUserException extends UserException {

	private static final long serialVersionUID = 9051290468137959437L;
	
	public DupUserException() {
		super(Message.getMsg("login.status.logout.dup"));
	}
}
