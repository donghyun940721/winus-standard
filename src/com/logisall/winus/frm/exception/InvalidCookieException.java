package com.logisall.winus.frm.exception;

import com.m2m.jdfw5x.message.Message;

/**
 * @author kwt
 * 쿠키 위변조시 발생
 */
public class InvalidCookieException extends UserException {
	
	private static final long serialVersionUID = -977045067542659883L;

	public InvalidCookieException() {
		super(Message.getMsg("login.invalid.cookie"));
	}
	
}
