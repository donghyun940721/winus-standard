package com.logisall.winus.frm.exception;

public class NotFoundException extends BizException {

	private static final long serialVersionUID = -2034243564539231570L;
	
	public static final String DATA_NOT_FOUND = "데이터가 존재하지 않습니다.";
	public static final String REQUEST_KEY_NOT_FOUND = "선택한 데이터가 존재하지 않습니다.";
	public static final String ITEMCD_NOT_FOUND = "상품코드가 존재하지 않습니다.";
	public static final String ITEMID_NOT_FOUND = "상품ID가 존재하지 않습니다.";
	public static final String CUSTOMER_NAME_NOT_FOUND = "수취인이름이 존재하지 않습니다.";
	public static final String CUSTOMER_ADDR_NOT_FOUND = "수취인주소가 존재하지 않습니다.";
	public static final String CUSTOMER_TEL_NOT_FOUND = "수취인전화번호가 존재하지 않습니다.";

	public NotFoundException(String message) {
		super(message);
	}
}
