/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author indigo(HONG SIK)
 *
 */
public class DocDefaultVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String fontPath = "";  //PDF기본폰트(절대경로/true type font)
	private String titleFontPath = "";//PDF제목폰트(절대경로/true type font)
	private String etcFontPath = "";//PDF기타폰트(절대경로/true type font)
	private String serverPath = "";//파일 생성 경로(XML or PDF)
	private String fileName = "";//파일명(XML or PDF)
	private boolean copyYn = false; //사본여부
	
	
	public void setFontPath(String fontPath) {
		this.fontPath = fontPath;
	}


	public String getFontPath() {
		return fontPath;
	}


	public void setTitleFontPath(String titleFontPath) {
		this.titleFontPath = titleFontPath;
	}


	public String getTitleFontPath() {
		return titleFontPath;
	}


	public void setEtcFontPath(String etcFontPath) {
		this.etcFontPath = etcFontPath;
	}


	public String getEtcFontPath() {
		return etcFontPath;
	}


	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}


	public String getServerPath() {
		return serverPath;
	}
	
    public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFileName() {
		return fileName;
	}


	
	public void setCopyYn(boolean copyYn) {
		this.copyYn = copyYn;
	}


	public boolean isCopyYn() {
		return copyYn;
	}


	public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


}
