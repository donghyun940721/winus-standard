/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;

@SuppressWarnings("PMD")
public class DocPickingListDetailVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;
    
    private String seq              = "";   // No
    private String ord_id           = "";   // 주문번호
    private String ritem_nm         = "";   // 제품
    private String out_ord_qty      = "";   // 수량
    private String ord_weight       = "";   // 중량
    private String out_ord_uom_id   = "";   // UOM
    private String wh_nm            = "";   // 창고
    private String loc_nm           = "";   // 로케이션
    
    //왠지 모르지만 얘네도 조회함
    private String ritem_id         = "";   // 제품ID
    private String work_qty         = "";   // 작업수량
    private String real_out_qty     = "";   // 실제 나간수?
    private String load_qty         = "";   // 로드수??
    private String out_wh_id        = "";   // 창고id
    private String work_id          = "";   // 작업id
    private String loc_id           = "";   // 로케이션id
    private String work_type        = "";   // 작업타입
   
    public String getSeq() {
        return seq;
    }
    public void setSeq(String seq) {
        this.seq = seq;
    }
    public String getOrd_id() {
        return ord_id;
    }
    public void setOrd_id(String ord_id) {
        this.ord_id = ord_id;
    }
    public String getRitem_nm() {
        return ritem_nm;
    }
    public void setRitem_nm(String ritem_nm) {
        this.ritem_nm = ritem_nm;
    }
    public String getOut_ord_qty() {
        return out_ord_qty;
    }
    public void setOut_ord_qty(String out_ord_qty) {
        this.out_ord_qty = out_ord_qty;
    }
    public String getOrd_weight() {
        return ord_weight;
    }
    public void setOrd_weight(String ord_weight) {
        this.ord_weight = ord_weight;
    }
    public String getOut_ord_uom_id() {
        return out_ord_uom_id;
    }
    public void setOut_ord_uom_id(String out_ord_uom_id) {
        this.out_ord_uom_id = out_ord_uom_id;
    }
    public String getWh_nm() {
        return wh_nm;
    }
    public void setWh_nm(String wh_nm) {
        this.wh_nm = wh_nm;
    }
    public String getLoc_nm() {
        return loc_nm;
    }
    public void setLoc_nm(String loc_nm) {
        this.loc_nm = loc_nm;
    }
    public String getRitme_id() {
        return ritem_id;
    }
    public void setRitme_id(String ritme_id) {
        this.ritem_id = ritme_id;
    }
    public String getWork_qty() {
        return work_qty;
    }
    public void setWork_qty(String work_qty) {
        this.work_qty = work_qty;
    }
    public String getReal_out_qty() {
        return real_out_qty;
    }
    public void setReal_out_qty(String real_out_qty) {
        this.real_out_qty = real_out_qty;
    }
    public String getLoad_qty() {
        return load_qty;
    }
    public void setLoad_qty(String load_qty) {
        this.load_qty = load_qty;
    }
    public String getOut_wh_id() {
        return out_wh_id;
    }
    public void setOut_wh_id(String out_wh_id) {
        this.out_wh_id = out_wh_id;
    }
    public String getWork_id() {
        return work_id;
    }
    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }
    public String getLoc_id() {
        return loc_id;
    }
    public void setLoc_id(String loc_id) {
        this.loc_id = loc_id;
    }
    public String getWork_type() {
        return work_type;
    }
    public void setWork_type(String work_type) {
        this.work_type = work_type;
    }

}
