/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;


public class DocRptGRNWorkOrderListDetailVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;

	//PDF에서 보여줄내용
    private String ordId        = ""; //주문번호 
    private String ritemNm      = ""; //제품명
    private String inOrdQty     = ""; //수량
    private String inOrdUomId   = ""; //UOM
    private String inOrdWeight  = ""; //중량
    private String inWhNm       = ""; //창고
    private String locCd        = ""; //로케이션
    

    private String sysDt           = "";   //발행일자
    private String userNm          = "";   //발행자
    private String custNm          = "";   //화주
    
    //그외
    
    
    public String getOrdId() {
        return ordId;
    }
    
    public void setOrdId(String ordId) {
        this.ordId = ordId;
    }
    public String getRitemNm() {
        return ritemNm;
    }
    public void setRitemNm(String ritemNm) {
        this.ritemNm = ritemNm;
    }
    public String getInOrdQty() {
        return inOrdQty;
    }
    public void setInOrdQty(String inOrdQty) {
        this.inOrdQty = inOrdQty;
    }
    public String getInOrdUomId() {
        return inOrdUomId;
    }
    public void setInOrdUomId(String inOrdUomId) {
        this.inOrdUomId = inOrdUomId;
    }
    public String getInOrdWeight() {
        return inOrdWeight;
    }
    public void setInOrdWeight(String inOrdWeight) {
        this.inOrdWeight = inOrdWeight;
    }
    public String getInWhNm() {
        return inWhNm;
    }
    public void setInWhNm(String inWhNm) {
        this.inWhNm = inWhNm;
    }
    public String getLocCd() {
        return locCd;
    }
    public void setLocCd(String locCd) {
        this.locCd = locCd;
    }
    
    public String getSysDt() {
        return sysDt;
    }
    public void setSysDt(String sysDt) {
        this.sysDt = sysDt;
    }
    public String getUserNm() {
        return userNm;
    }
    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }
    public String getCustNm() {
        return custNm;
    }
    public void setCustNm(String custNm) {
        this.custNm = custNm;
    }
    

}
