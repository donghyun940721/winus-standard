package com.logisall.winus.frm.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import com.m2m.jdfw5x.egov.message.MessageResolver;

public class XmlTransform {

	private DecimalFormat df = null;

	public XmlTransform() {
		try {
			df = new DecimalFormat("#########.####");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * 
	 * Method ID : creatXmlString Method 설명 : 전자문서(XML) 원문보기, XML 내용을 스트링으로 읽어서
	 * 리턴 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("PMD")
	public Map<String, Object> creatXmlString(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String filepath = (String) model.get("FILEPATH");
		StringBuffer xml = new StringBuffer();
		
		File fi = new File(filepath);
		if (!fi.exists()) {
			map.put("MSG", MessageResolver.getMessage("file.check.nodata"));
		}

		BufferedReader bufferedReader = null;
		InputStreamReader inputStreamReader = null;
		String data = "";
		
		try {
			inputStreamReader = new InputStreamReader(new FileInputStream(fi), ConstantIF.ENCODING_TYPE_JSON_FILE);
			bufferedReader = new BufferedReader(inputStreamReader);
			
			while ((data = bufferedReader.readLine()) != null) {
				xml.append(data);
			}
			bufferedReader.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (Exception ie) {
				}
			}
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (Exception ise) {
				}
			}
		}

		map.put("XMLCONTENT", xml);
		return map;
	}

}