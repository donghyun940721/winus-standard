package com.logisall.winus.frm.common.util;

@SuppressWarnings("PMD")
public final class DESUtil {
	
	private DESUtil() {}
    
    public static String dataEncrypt(String inStr) {
        
        String retStr = "";
        if(inStr == null) return "";
        if(inStr.equals("")) return "";
        inStr = inStr.trim();
        retStr = com.gns.security.DES.instance().encrypt(inStr);
        
        return retStr;
    }

    public static String dataDecrypt(String inStr) {
        String retStr = "";
        if(inStr == null) return "";
        if(inStr.equals("")) return "";
        inStr = inStr.trim();
        retStr = com.gns.security.DES.instance().decrypt(inStr);

        return retStr;
    }

}
