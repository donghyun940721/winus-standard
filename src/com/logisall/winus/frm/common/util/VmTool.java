package com.logisall.winus.frm.common.util;

/**
 * @author minjae
 * 
 */
public final class VmTool {

	/**
	 * 문자열 자르기 value1 에서 value2 자리수 까지 자르기
	 * 
	 * @param String
	 *            str : 원문,
	 * @param int value1 : 자를 문자열의 시작 위치,
	 * @param int value2 : 자를 문자열의 마지막 위치
	 * @return String : value1 부터 value2 까지의 문자열
	 */
	public static String getSubstr(String str, int value1, int value2) {

		if (str == null || str.equals("")) {
			return "";
		}

		return str.substring(value1, value1 + value2);
	}
}