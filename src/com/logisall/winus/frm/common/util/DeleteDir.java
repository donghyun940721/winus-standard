package com.logisall.winus.frm.common.util;

import java.io.File; 

public class DeleteDir { 
	public static void main(String args[]) { 
		deleteDirectory(new File(args[0])); 
	} 
	
	public static boolean deleteDirectory(File path) { 
		if(!path.exists()) { 
			return false; 
		} 
		
		File[] files = path.listFiles(); 
		for (File file : files) { 
			if (file.isDirectory()) { 
				deleteDirectory(file); 
			} else { 
				file.delete(); 
			} 
		}
	    
		return path.delete(); 
	}
	
	public static boolean deleteZipFile(File base, File path) { 
		File deleteZipFile = new File(path + ".zip");
		File[] files = base.listFiles(); 
		
		for (File file : files) { 
		    if (!file.delete()) { 
		    	file.deleteOnExit(); 
		    } 
		} 
		
		return deleteZipFile.delete(); 
	}
} 

