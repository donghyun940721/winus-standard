package com.logisall.winus.frm.common.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpMethod;

public class HttpUtil {
	private static final Log log = LogFactory.getLog(HttpUtil.class);

	// Send JSON HTTP request
    public static String sendJsonHttpRequest(String url, HttpMethod httpMethod, String jsonBody){
        return sendHttpRequest(url, httpMethod, jsonBody, "application/json; charset=UTF-8");
    }
    
    public static String sendJsonHttpRequest(String url, HttpMethod httpMethod, String jsonBody, String authorization){
        return sendHttpRequest(url, httpMethod, jsonBody, "application/json; charset=UTF-8", authorization);
    }

    // Send x-www-form-urlencoded HTTP request
    public static String sendFormHttpRequest(String url, HttpMethod httpMethod, String formBody){
        return sendHttpRequest(url, httpMethod, formBody, "application/x-www-form-urlencoded; charset=UTF-8");
    }

    public static String sendHttpRequest(String url, HttpMethod httpMethod, String jsonBody, String contentType){
        try {
        	// HTTPS 설정 추가
            disableSslVerification();
            URL requestUrl = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();
//            HttpURLConnection connection = setHttpConnection(requestUrl);
            
            connection.setRequestMethod(httpMethod.toString());

            if (httpMethod == HttpMethod.POST || httpMethod == HttpMethod.PUT ) {
                connection.setDoOutput(true);
                
                connection.setRequestProperty("Content-Type", contentType);
                
                byte[] bytes = new byte[1024];
                byte[] content = jsonBody.getBytes("UTF-8");
                
                connection.setFixedLengthStreamingMode(content.length);
                connection.setRequestProperty("Content-Length", Integer.toString(content.length));
                try (
                        InputStream bin = new ByteArrayInputStream(content);
                        BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
                    ) {
                    int byteRead;
                    while((byteRead= bin.read(bytes)) >= 0){
                        bos.write(bytes,0,byteRead);
                    }
                    bos.flush();
                }
            }

            int statusCode = connection.getResponseCode();
            if (statusCode == 200) { // 200 OK
                try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    StringBuilder response = new StringBuilder();
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    return response.toString();
                }
            } else {
                throw new IOException("HTTP request failed with status code: " + statusCode);
            }
        } catch (IOException e) {
        	log.error(e);
            throw new RuntimeException("HTTP request failed: " + e.getMessage(), e);
        }
    }

    
    
    /**
     * 대체 Method ID		: disableSslVerification
     * 대체 Method 설명		: SSL 예외처리
     * 작성자				: W
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("TLSv1.2");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	 public static String sendHttpRequest(String url, HttpMethod httpMethod, String jsonBody, String contentType, String authorization){
	        try {
	        	// HTTPS 설정 추가
	            disableSslVerification();
	            URL requestUrl = new URL(url);
	            HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();
//	            HttpURLConnection connection = setHttpConnection(requestUrl);
	            
	            connection.setRequestMethod(httpMethod.toString());

	            if (httpMethod == HttpMethod.POST || httpMethod == HttpMethod.PUT ) {
	                connection.setDoOutput(true);
	                
	                connection.setRequestProperty("Content-Type", contentType);
	                connection.setRequestProperty("Authorization", authorization);
	                
	                byte[] bytes = new byte[1024];
	                byte[] content = jsonBody.getBytes("UTF-8");
	                
	                connection.setFixedLengthStreamingMode(content.length);
	                connection.setRequestProperty("Content-Length", Integer.toString(content.length));
	                try (
	                        InputStream bin = new ByteArrayInputStream(content);
	                        BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
	                    ) {
	                    int byteRead;
	                    while((byteRead= bin.read(bytes)) >= 0){
	                        bos.write(bytes,0,byteRead);
	                    }
	                    bos.flush();
	                }
	            }

	            int statusCode = connection.getResponseCode();
	            if (statusCode == 200) { // 200 OK
	                try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
	                    StringBuilder response = new StringBuilder();
	                    String inputLine;
	                    while ((inputLine = in.readLine()) != null) {
	                        response.append(inputLine);
	                    }
	                    return response.toString();
	                }
	            } else {
	                throw new IOException("HTTP request failed with status code: " + statusCode);
	            }
	        } catch (IOException e) {
	        	log.error(e);
	            throw new RuntimeException("HTTP request failed: " + e.getMessage(), e);
	        }
	    }
    
//	private static HttpURLConnection setHttpConnection(URL requestUrl)
//			throws IOException, NoSuchAlgorithmException, KeyManagementException {
//		HttpURLConnection connection;
//
//		log.info(requestUrl.getProtocol());
//		
////		if (requestUrl.getProtocol().equalsIgnoreCase("https")) {
//		    HttpsURLConnection httpsConnection = (HttpsURLConnection) requestUrl.openConnection();
//
//		    // TLS 1.2 지원 설정
//		    SSLContext sslContext = SSLContext.getInstance("SSL");
//		    sslContext.init(null, new TrustManager[]{new X509TrustManager() {
//		        public void checkClientTrusted(X509Certificate[] chain, String authType) {}
//		        public void checkServerTrusted(X509Certificate[] chain, String authType) {}
//		        public X509Certificate[] getAcceptedIssuers() { return null; }
//		    }}, new java.security.SecureRandom());
//
//		    httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());
//		    
//		    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
//				public boolean verify(String hostname, SSLSession session) {
//					return true;
//				}
//			});
//		    connection = httpsConnection;
////		} else {
////		    connection = (HttpURLConnection) requestUrl.openConnection();
////		}
//		return connection;
//	}


}

