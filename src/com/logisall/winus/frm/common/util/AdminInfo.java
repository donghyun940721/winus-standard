package com.logisall.winus.frm.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
/**
 * 시스템명 : 엠투엠글로벌
 * 패키지명 : com.m2m.jdfw5x.util.code.service
 * 클래스명 : CodeParser.java
 * 설명 :
 * @author  : 
 * @version : 1.0
 * @see
 *
 * == 개정이력(Modification Information) ==
 *   수정일        수정자      수정내용
 *  ---------   ---------   -------------------------------
 *
 *  -------------------------------------------------------
 */
@Component("AdminInfo")
public class AdminInfo {
	protected final Log log = LogFactory.getLog(this.getClass());
	
	public static String getLCAdmin() {
		try{
			return ConstantIF.LC_ADMIN;
		}catch (Exception e){ return "";}
	}	
	
	public static String getCustId() {
	    try{
	        return ConstantIF.CUST_ID;
	    }catch (Exception e){ return "";}
	}

	public static String getUserAdmin(int seq) {
        try{
            String userAdmin = "";
            for(int i=0; i<ConstantIF.USER_ADMIN.length; i++){
                if(seq == i){
                    userAdmin = ConstantIF.USER_ADMIN[i];
                    break;
                }
            }
            return userAdmin;
        }catch (Exception e){ return "";}
    }
}
