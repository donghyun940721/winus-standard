package com.logisall.winus.frm.common.util;

import java.security.MessageDigest;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.logisall.winus.frm.exception.BizException;

@SuppressWarnings("PMD")
public final class SHA256Util {
	protected static final Log log = LogFactory.getLog(SHA256Util.class);

    private SHA256Util() {}
    
    private static String generateSalt() {
        byte[] salt = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(salt);
        return Base64.encodeBase64String(salt);
    }
    
    public static String dataEncrypt(String inStr) throws BizException {
        if (StringUtils.isEmpty(inStr)) {
            throw new BizException("Input string is empty.");
        }
        String salt = generateSalt();
        String saltedPswd = inStr + salt; // {SALT + 실제 비밀번호} 암호화
        try {
            byte[] hashedBytes = MessageDigest.getInstance("SHA-256").digest(saltedPswd.getBytes());
            return salt + ":" +bytesToHex(hashedBytes); // db 저장되는 비밀번호 > {SALT:암호화된 (SALT+실제 비밀번호)}
        } catch (Exception e) {
            throw new BizException("SHA-256 ERROR.", e);
        }
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
        	sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
    
    // 입력값과 DB 저장된 비밀번호의 일치 여부 검사
    public static boolean verify(String inStr, String hashedStr) throws BizException {
        String[] splitStr = hashedStr.split(":"); // {SATL:암호화된 비밀번호} 형태
        if (splitStr.length != 2) {
            throw new BizException("SHA-256 VELIFY ERROR.");
        }
        
        String salt = splitStr[0];
        String hashedPswd = splitStr[1];
        String saltedInput = inStr + salt;
        String hashedInStr;
		try {
			hashedInStr = bytesToHex(MessageDigest.getInstance("SHA-256").digest(saltedInput.getBytes()));
		} catch (Exception e) {
            throw new BizException("SHA-256 ERROR.", e);
		}

        return hashedInStr.equals(hashedPswd);
    }
}
