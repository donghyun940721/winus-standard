package com.logisall.winus.frm.common.util;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.logisall.winus.frm.exception.BizException;

/**
 * @author minjae
 *
 */
public class SessionMn {
	private final static int sessionTime = -30;
	private static SessionMn	 instance;

	private Map<String, LoginInfo> sessionMap = new HashMap<String, LoginInfo>();

	public SessionMn() {
		// instance = null;
	}

	/**
	 * 인스턴스를 리턴 서비스 시작시 call 메서드.
	 *
	 * @return s_instance
	 */
	public static SessionMn getInstance() throws BizException {
		if (instance == null) {
			throw new BizException("SessionMn instance is null.");
		}
		return instance;
	}

	public static void init() throws Exception {
		if (instance == null) {
			instance = new SessionMn();
		}
	}

	/**
	 *  세션정보를 등록한다.
	 * @param loginInfo
	 */
	public void addInfo(LoginInfo loginInfo) {
		sessionMap.put(loginInfo.getId(), loginInfo);
	}

	/**
	 * 세션정보를 삭제한다.
	 * @param userId
	 */
	public void delInfo(String userId) {
		sessionMap.remove(userId);
	}

	/**
	 * 세션 맵을 체크 후, 로그인여부를 반환한다.
	 * @param userId
	 * @return
	 */
	public boolean isLoginUser(String userId) {
		recheckSession4check(userId);
		return sessionMap.containsKey(userId);
	}

	/**
	 * 세션맵을 체크한다.
	 */
	public void recheckSession() {
		LoginInfo loginInfo = null;
		for(String userId: sessionMap.keySet()) {
			loginInfo = sessionMap.get(userId);
			if(loginInfo != null) {
				Calendar current = Calendar.getInstance();
				current.add(Calendar.MINUTE, sessionTime);
				if(loginInfo.getLastRequestTime().compareTo(current) < 0) {
					delInfo(userId);
				} else {
					loginInfo.setLastRequestTime(Calendar.getInstance());
					addInfo(loginInfo);
				}
			}
		}
	}

	/**
	 * 입력받은 아이디에 해당하는 세션맵을 체크하고, 있을 경우 마지막 접속 시간을 갱신한다.
	 * @param userId
	 */
	public void recheckSession(String userId) {
		LoginInfo loginInfo = sessionMap.get(userId);
		if(loginInfo != null) {
			Calendar current = Calendar.getInstance();
			current.add(Calendar.MINUTE, sessionTime);
			if(loginInfo.getLastRequestTime().compareTo(current) < 0) {
				delInfo(userId);
			} else {
				loginInfo.setLastRequestTime(Calendar.getInstance());
				addInfo(loginInfo);
			}
		}
	}

	/**
	 * 로그인 체크만을 하기 위한 세션맵 체크
	 * @param userId
	 */
	public void recheckSession4check(String userId) {
		LoginInfo loginInfo = sessionMap.get(userId);
		if(loginInfo != null) {
			Calendar current = Calendar.getInstance();
			current.add(Calendar.MINUTE, sessionTime);
			if(loginInfo.getLastRequestTime().compareTo(current) < 0) {
				delInfo(userId);
			} else {
				addInfo(loginInfo);
			}
		}
	}
}
