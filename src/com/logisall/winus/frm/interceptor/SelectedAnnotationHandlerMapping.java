
package com.logisall.winus.frm.interceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.annotation.DefaultAnnotationHandlerMapping;

public class SelectedAnnotationHandlerMapping extends DefaultAnnotationHandlerMapping {

    protected Log log = LogFactory.getLog(SelectedAnnotationHandlerMapping.class);

    private List<String> urls;

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public String[] getFiltered(String[] strs) {
        if (strs == null) {
            return null;
        }

        List<String> tmpList = new ArrayList();
        int i = strs.length;
        for (int j = 0; j < i; j++) {
            String str = strs[j];
            for (String url : urls) {
                Pattern pattern = Pattern.compile(url, Pattern.CASE_INSENSITIVE);
                if (pattern.matcher(str).find()) {
                    tmpList.add(str);
                }
            }
        }
        return (String[])tmpList.toArray(new String[tmpList.size()]);
    }

    protected String[] determineUrlsForHandler(String s) {
        return getFiltered(super.determineUrlsForHandler(s));
    }

}
