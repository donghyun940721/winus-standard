package com.logisall.winus.frm.advisor;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.AfterReturningAdvice;

public class AfterMgrAdvice implements AfterReturningAdvice {
	
	protected Log log = LogFactory.getLog(AfterMgrAdvice.class);
	
	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		
		StringBuffer sb = new StringBuffer();
		sb.append("[ MODULE ] :").append(target.getClass().getName()).append(".").append(method.getName()).append(" END  ....");
		if (log.isDebugEnabled()) {
			log.debug(sb.toString());
		}
		
/*		
		if (target instanceof CommonMgrImpl) {
			CommonMgrImpl impl = (CommonMgrImpl)target;
			if (impl.getDbmsDao() != null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("module_nm", target.getClass().getName() + "." + method.getName());
				map.put("action_nm", "end");
				impl.getDbmsDao().setModule(map);
				
				map.put("module_nm", "");
				map.put("action_nm", "");
				impl.getDbmsDao().setModule(map);
			}
		}
		
*/		
	}
}
