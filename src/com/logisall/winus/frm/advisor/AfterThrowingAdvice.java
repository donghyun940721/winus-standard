package com.logisall.winus.frm.advisor;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;

public class AfterThrowingAdvice {
    private static final Log log = LogFactory.getLog(AfterThrowingAdvice.class);
    private static final int MAX_PARAMETERS_TO_LOG = 500; // 로깅할 최대 파라미터 수 설정

    @Autowired
    private HttpServletRequest request;
//	@Value("${url.dooray.webhook}")
//	private String DOORAY_WEBHOOK_URL;

    public void handleException(JoinPoint joinPoint, Throwable ex) throws IOException {
	// 예외 처리 로직을 여기에 작성
	if (ex instanceof UndeclaredThrowableException) {
	    Throwable realCause = ((UndeclaredThrowableException) ex).getUndeclaredThrowable();
	    log.error("Reflection error occurred in method: " + joinPoint.getSignature().toShortString(), realCause);
	}
	log.error("예외가 발생했습니다: " + ex.toString());
	log.error("예외내용: " + ex.getMessage());
	log.error("메서드 이름: " + joinPoint.getSignature().getDeclaringTypeName() + "."
		+ joinPoint.getSignature().getName());
	log.error("ip 주소: " + request.getRemoteAddr());

	// 메서드 파라미터 출력
	Object[] args = joinPoint.getArgs();
	for (Object arg : args) {
	    if (arg instanceof Map) {
		Map<String, Object> requestData = (Map) arg;
		Map<String, Object> filteredRequestData = new HashMap<String, Object>();
		if (requestData.size() > MAX_PARAMETERS_TO_LOG) {
		    log.error("요청한 파라미터 Key의 개수가 " + MAX_PARAMETERS_TO_LOG + "개 이상입니다. ");
		    continue;
		}

		for (Map.Entry<String, Object> entry : requestData.entrySet()) {
		    if (!(entry.getValue() instanceof List)) {
			// 리스트 형태의 값이 아닌 경우에만 추가
			filteredRequestData.put(entry.getKey(), entry.getValue());
		    } else {
			log.error("요청한 파라미터 정보: " + entry.getKey() + "는 LIST");
		    }

		}
		log.error("요청한 파라미터 정보: " + filteredRequestData);
	    }

	}
//    	DoorayBotNotificationDTO notificationDTO = setNotificationData(joinPoint, ex);
//    	HttpUtil.sendHttpRequest(DOORAY_WEBHOOK_URL, HttpMethod.POST, new Gson().toJson(notificationDTO));
    }

//	private DoorayBotNotificationDTO setNotificationData(JoinPoint joinPoint, Throwable ex) {
//        DoorayBotNotificationDTO botNotification = new DoorayBotNotificationDTO();
//        botNotification.setBotName("WINUS 에러 알림봇");
//        botNotification.setBotIconImage("https://static.dooray.com/static_images/dooray-bot.png");
//
//        DoorayBotNotificationDTO.Attachment attachment = new DoorayBotNotificationDTO.Attachment();
//        StringBuilder errMsg = new StringBuilder();
//        
//        errMsg.append("발생 IP: "+ request.getRemoteAddr()+"\n");
//        errMsg.append("예외내용: "+  ex.toString()+"\n");
//        errMsg.append("메서드명: "+ joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+"\n");
//        Object[] args = joinPoint.getArgs();
//        for (Object arg : args) {
//            if (arg instanceof Map) {
//            	Map requestData = (Map) arg;
//            	errMsg.append("요청한 파라미터 정보: "+arg);
//            }
//        }
//        attachment.setTitle("오류 발생");
//        attachment.setTitleLink("http://winus.logisall.com/WINUS/TMSYS140.action");
//        attachment.setText(errMsg.toString());
//        attachment.setColor("red");
//        botNotification.setAttachments(Collections.singletonList(attachment));
//		return botNotification;
//	}
}
