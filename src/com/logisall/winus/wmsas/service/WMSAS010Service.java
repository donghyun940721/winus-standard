package com.logisall.winus.wmsas.service;

import java.util.List;
import java.util.Map;


public interface WMSAS010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> subList(Map<String, Object> model) throws Exception;
    public Map<String, Object> orderList(Map<String, Object> model) throws Exception;
    public Map<String, Object> ordSubTypeList(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
}
