package com.logisall.winus.kiosk.web;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.google.gson.Gson;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.LoginInfo;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.frm.common.util.SessionMn;
import com.logisall.winus.meta.service.MetaUserRoleService;
import com.logisall.winus.kiosk.service.KIOSK010Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.client.service.JDFWClientInfo;
import com.m2m.jdfw5x.document.DocumentView;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.SessionUtil;

@Controller
public class KIOSK010Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "KIOSK010Service")
	private KIOSK010Service service;
	
	@Resource(name = "metaUserRoleService")
	private MetaUserRoleService service1;
	
    @Resource(name = "WMSIF000Service")
    private WMSIF000Service wmsif000service;
    
	
	
    /**
     * Method ID    : KIOSK010
     * Method 설명    : 
     * 작성자          : yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/kiosk/kioskMainDlv.action")
	public ModelAndView kioskMainDlv(HttpServletRequest request, Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/kiosk/style2/kioskMainDlv");
	}	
	
	@RequestMapping("/kiosk/kioskMainWork.action")
	public ModelAndView kioskMainWork(HttpServletRequest request, Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/kiosk/style2/kioskMainWork");
	}	
	
	@RequestMapping("/kiosk/KIOSK010.action")
	public ModelAndView kiosk010(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/kiosk/style2/KIOSK010");
	}
    
    

	/*-
	 * Method ID   : pdfView
	 * Method 설명    : PDF view
	 * 작성자               : yhku
	 * @param   model
	 * @return
	 */
	@RequestMapping("/kiosk/pdfView.action")
	public void pdfView(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			String pathDirectory = request.getParameter("pdfFilePath");
			// String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
			String pdfFilePath = (new StringBuffer()
					.append(ConstantIF.SERVER_PDF_PATH)
					.append(CommonUtil.convertSafePath(pathDirectory))).toString();
			String pdfFileName = request.getParameter("pdfFileName");

			// log.info("[ pdfFilePath ] :" + pdfFilePath);
			// log.info("[ pdfFileName ] :" + pdfFileName);
			DocumentView.getPDFView(request, response, new File(pdfFilePath, pdfFileName));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pdf view :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}
	
	

	@RequestMapping("/kiosk/PrintWorkForm.action")
	public ModelAndView PrintWorkForm( Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m =  new HashMap<String, Object>();
		
		String pathDirectory = "/KIOSK/LGES/";
		String pdfFilePath = (new StringBuffer().append(ConstantIF.FILE_ATTACH_STATEMENT_PATH).append(pathDirectory)).toString();
		//String baseUploadDir = ConstantIF.FILE_ATTACH_STATEMENT_PATH + "/KIOSK/LGES";		    
		//m.put("_fullPath", baseUploadDir);
		
		try {
			m.put("pdfFilePath", pdfFilePath);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}	

	
	
	@RequestMapping("/kiosk/DlvCarList.action")
	public ModelAndView DlvCarList( Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		m = service.getDlvCarList(model);
		mav.addAllObjects(m);
		return mav;
	}	

    
	@RequestMapping("/kiosk/DlvOrdListPage.action")
	public ModelAndView DlvOrdListPage( Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.getDlvOrdList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	  
	@RequestMapping("/kiosk/DlvOrdList.action")
	public ModelAndView DlvOrdList( Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		m = service.getDlvOrdList(model);
		mav.addAllObjects(m);
		return mav;
	}	
	

	
	
//	@RequestMapping("/main/popmain.action")
//	public ModelAndView detail(HttpServletRequest request, Map<String, Object> model) throws Exception {
//		Map<String, Object> popInfo = service.getPopupInfo(model);
//	    HttpSession session = request.getSession();
//        HashMap<String, Object> modelIns = new HashMap<String, Object>();
//        modelIns.put("S_SVC_NO", session.getAttribute("SS_SVC_NO"));
//        modelIns.put("S_USER_ID", session.getAttribute("SS_USER_ID"));
//        modelIns.put("S_SVC_TYPE", session.getAttribute("SS_SVC_TYPE"));
//        modelIns.put("S_AUTH_CD", session.getAttribute("SS_USER_TYPE"));
//        modelIns.put("S_LANG", session.getAttribute("SS_LANG"));
//        popInfo.put("menuInfo", (new JSONObject(service1.detail(modelIns))).toString());
//		return new ModelAndView("winus/common/tempblank", popInfo);
//	}	
	
	
	
}
