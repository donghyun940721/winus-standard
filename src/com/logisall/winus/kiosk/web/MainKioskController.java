package com.logisall.winus.kiosk.web;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.google.gson.Gson;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.LoginInfo;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.frm.common.util.SessionMn;
import com.logisall.winus.meta.service.MetaUserRoleService;
import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.client.service.JDFWClientInfo;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.SessionUtil;

@Controller
public class MainKioskController {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "TMSYS030Service")
	private TMSYS030Service service;
	
	@Resource(name = "metaUserRoleService")
	private MetaUserRoleService service1;
	
    @Resource(name = "WMSIF000Service")
    private WMSIF000Service wmsif000service;
    

    /**
     * Method ID    : kioskMn
     * Method 설명    : 
     * 작성자          : yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    

	@RequestMapping("/kiosk/login.action")
	public ModelAndView kioskMn(Map<String, Object> model) {
		ModelAndView mav = null;
		// String sid = (String)model.get("SID");
		// if(sid == null){
		mav = new ModelAndView("winus/kiosk/style2/login");
		// }else{
		// mav = new ModelAndView("winus/common/comAlert");
		// }

		return mav; // 맨 앞에 / 없음에 주의, .vm 없음에 주의
	}
  
    
	
	@RequestMapping("/kiosk/login_s.action")
	public ModelAndView login(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		Map<String, Object> user = null;
		ArrayList<Map<String, Object>> lcInfo = null;
		int check = 0;		// Login 여부 체크용 변수.
		boolean isSuccess = false;
		String reasonCd = "";

		try {
			String serviceId = (String) model.get("svcid");
			String operationId = (String) model.get("opid");

			boolean isLoginRequest = "LOGIN".equals(serviceId) && "loginUserConfirm".equals(operationId);

			// 사전처리
			String mySystemId = "WINUS";
			String gvSystemId = "n/a";
			String gvUserAuth = "n/a";

			// 타 시스템 유저 검사
			// boolean isExternalUser = !mySystemId.equals(gvSystemId) ;

			// 외부 사용자 경우
			// if(isExternalUser &&
			// !BlackList.getInstance().canAccess(mySystemId, gvSystemId,
			// gvUserAuth, serviceId, operationId)){
			// appContext.setResultMessage(-200, "허용되지 않은 외부접속입니다");
			// }

			// 로그인 요청이면?
			if (isLoginRequest) {
				if (null == model.get("USER_ID")) {
					return new ModelAndView("winus/kiosk/style2/login");
				}
				m = service.getDetail(model);		// <<<<<<<< 실질적인 로그인 Pass / Non Pass 처리 로직.
				
				
				isSuccess = (boolean) m.get("isSuccess");
				reasonCd = (String) m.get("reasonCd");
				System.out.println("isSuccess  : " + isSuccess);
				System.out.println("reasonCd  : " + reasonCd);
				if (isSuccess) {//user != null
					user = (Map<String, Object>) m.get("DETAIL");
					// 세션에 값 셋팅
					HttpSession session = request.getSession();
					// String user_ip = request.getRemoteAddr();
					// 인증에 성공한 경우 처리 해야 되는 부분
															//(String) user.get("CONNECT_YN")
					
//					//220516 로그인 재시도횟수 체크.
//					int loginRetryCnt = service.getLoginRetryCnt(user);
					
					
					boolean chek = SessionListener.checkLogin((String) user.get("LOGIN_YN"), (String) user.get("MULTI_USE_GB"), (String) user.get("LAST_CONT_IP"), JDFWClientInfo.getClntIP(request));
					//5회이상 로그인실패시 관리자 문의 메시지.
					if (chek == true ) {//&& loginRetryCnt < 5
						session.setAttribute("SS_USER_ID", user.get("USER_ID")); // 사용자ID
						session.setAttribute("SS_USER_NO", user.get("USER_NO")); // 사용자NO
						session.setAttribute("SS_USER_GB", user.get("USER_GB")); // 사용자구분
						session.setAttribute("SS_USER_NAME", user.get("USER_NM")); // 사용자이름
						session.setAttribute("SS_USER_TYPE", user.get("AUTH_CD")); // 사용자타입
						session.setAttribute("SS_SVC_NO", user.get("LC_ID")); // 서비스NO
						session.setAttribute("SS_SVC_NAME", user.get("LC_NM")); // 서비스이름
						session.setAttribute("SS_CMPY_CODE", user.get("CUST_CD"));// 사용자소속회사코드
						session.setAttribute("SS_CMPY_ID", user.get("CUST_ID"));// 사용자소속회사코드
						session.setAttribute("SS_CMPY_NAME", user.get("CUST_NM"));// 사용자소속회사명
						session.setAttribute("SS_SALES_CUST_ID", user.get("SALES_CUST_ID"));// 사용자소속회사명
						
						session.setAttribute("SS_LANG", user.get("LANG"));// 사용자언어
						session.setAttribute("SS_AUTH_LEVEL", user.get("AUTH_LEVEL").toString());// 레벨
						session.setAttribute("SS_CLIENT_CD", user.get("CLIENT_CD"));// 거래처(업체)코드
						session.setAttribute("SS_GIRD_NUM", user.get("USER_GRID_NUM"));// 그리드
						session.setAttribute("SS_SVC_USE_TY", user.get("LC_USE_TY")); // 물류센터운영타입
						session.setAttribute("SS_LC_CHANGE_YN", user.get("LC_CHANGE_YN")); // 물류센터변경여부
						session.setAttribute("SS_ITEM_FIX_YN", user.get("ITEM_FIX_YN")); // 특정상품조회권한
						session.setAttribute("SS_ITEM_GRP_ID", user.get("ITEM_GRP_ID")); // 특정상품군
						session.setAttribute("SS_SUB_PASSWORD", user.get("SUB_PASSWORD")); // 기타 비밀번호
						session.setAttribute("SS_PROC_AUTH", user.get("PROC_AUTH")); // 저장처리권한
						session.setAttribute("SS_USE_TRANS_CUST", user.get("USE_TRANS_CUST")); // 저장처리권한
						session.setAttribute("SS_TEL_NO", user.get("TEL_NO")); // 전화번호
						session.setAttribute("SS_MULTI_USE_GB", user.get("MULTI_USE_GB")); // 사용자NO
						session.setAttribute("SS_STOCK_ALERT_YN", user.get("STOCK_ALERT_YN")); // 재고부족알림구분
						session.setAttribute("SS_LC_CUST_NM", user.get("LC_CUST_NM")); // 3PL 물류센터명
						
						session.setAttribute("SS_M_DO", ConstantIF.encAES("KEY2020031312345"));
						session.setAttribute("SS_DETAIL_PS", ConstantIF.encAES("DE2020031354321"));
						
						List<Map<String, Object>> listMenu = service.list_menu();
				    	Gson gson         = new Gson();
				    	Map<String, Object> map = new HashMap<String, Object>();
				    	
				    	for(int i = 0; i < listMenu.size(); i++){
				    		map.put(String.valueOf(listMenu.get(i).get("PROGRAM_ID")), listMenu.get(i).get("PROGRAM_NM"));
				    	}
						String jsonString = gson.toJson(map);
						jsonString = JSONValue.escape(jsonString);
						session.setAttribute("SS_MENU_MAP", jsonString);
						
																						// 검색
																						// 설정
						session.setAttribute("SS_SESSION_ID", request.getSession().getId());// 세션아이디
						session.setAttribute("SS_CLIENT_IP", JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						if ("KR".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_KOREAN);
						} else if ("EN".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_ENGLISH);
						} else if ("JA".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_JAPANESE);
						} else if ("CH".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_CHINESE);
						} else if ("VN".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_VIETNAM);
						} else if ("ES".equals(user.get("LANG"))) {
                            session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_ESPANA);
                        }

						SessionUtil.setValue(request, ConstantIF.SS_USER_ID, user.get("USER_ID"));// 사용자ID
						SessionUtil.setValue(request, ConstantIF.SS_USER_NO, user.get("USER_NO"));// 사용자번호
						SessionUtil.setValue(request, ConstantIF.SS_USER_GB, user.get("USER_GB"));// 사용자구분
						SessionUtil.setValue(request, ConstantIF.SS_USER_NAME, user.get("USER_NM")); // 사용자이름
						SessionUtil.setValue(request, ConstantIF.SS_USER_TYPE, user.get("AUTH_CD"));// 사용자타입
						SessionUtil.setValue(request, ConstantIF.SS_SVC_NO, user.get("LC_ID"));// 서비스NO
						SessionUtil.setValue(request, ConstantIF.SS_SVC_NAME, user.get("LC_NM"));// 서비스D이름
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_CODE, user.get("CUST_CD"));// 사용자소속회사코드
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_ID, user.get("CUST_ID"));// 사용자소속회사코드
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_NAME, user.get("CUST_NM"));// 사용자소속회사명
						SessionUtil.setValue(request, ConstantIF.SS_LANG, user.get("LANG"));// 사용자언어
						SessionUtil.setValue(request, ConstantIF.SS_AUTH_LEVEL, user.get("AUTH_LEVEL").toString());// 레벨
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_CD, user.get("CLIENT_CD"));// 거래처(업체)코드
						SessionUtil.setValue(request, ConstantIF.SS_GIRD_NUM, user.get("USER_GRID_NUM"));// 거래처(업체)코드
						SessionUtil.setValue(request, ConstantIF.SS_SVC_USE_TY, user.get("LC_USE_TY"));// 서비스NO
						SessionUtil.setValue(request, ConstantIF.SS_LC_CHANGE_YN, user.get("LC_CHANGE_YN"));// 물류센터변경여부
						SessionUtil.setValue(request, ConstantIF.SS_ITEM_FIX_YN, user.get("ITEM_FIX_YN"));// 특정상품조회권한
						SessionUtil.setValue(request, ConstantIF.SS_ITEM_GRP_ID, user.get("ITEM_GRP_ID"));// 특정상품군
						SessionUtil.setValue(request, ConstantIF.SS_TEL_NO, user.get("TEL_NO"));// 전화번호
						SessionUtil.setValue(request, ConstantIF.SS_SUB_PASSWORD, user.get("SUB_PASSWORD"));// 기타 비밀번호
						SessionUtil.setValue(request, ConstantIF.SS_PROC_AUTH, user.get("PROC_AUTH"));// 저장처리권한
						SessionUtil.setValue(request, ConstantIF.SS_USE_TRANS_CUST, user.get("USE_TRANS_CUST"));// 저장처리권한
						SessionUtil.setValue(request, ConstantIF.SS_MULTI_USE_GB, user.get("MULTI_USE_GB"));// 사용자번호
						SessionUtil.setValue(request, ConstantIF.SS_STOCK_ALERT_YN, user.get("STOCK_ALERT_YN"));// 재고부족알림구분
						SessionUtil.setValue(request, ConstantIF.SS_LC_CUST_NM, user.get("LC_CUST_NM"));// 3PL 물류센터명
						SessionUtil.setValue(request, ConstantIF.SS_SALES_CUST_ID, user.get("SALES_CUST_ID"));// 3PL 물류센터명
						
						
						SessionUtil.setValue(request, ConstantIF.SS_M_DO, ConstantIF.encAES("KEY2020031312345"));
						SessionUtil.setValue(request, ConstantIF.SS_DETAIL_PS, ConstantIF.encAES("DE2020031354321"));
						
						// SessionUtil.setValue(request,ConstantIF.SS_DEPT_NAME,
						// user.get("DEPT_NAME"));//사용자소속부서명
						// SessionUtil.setValue(request,ConstantIF.SS_DEGREE,
						// user.get("DEGREE"));//사용자직급명
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_IP, JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						SessionUtil.setValue(request, ConstantIF.SS_SESSION_ID, request.getSession().getId());// (클라이언트)접속IP

						SessionUtil.setValue(request, ConstantIF.SS_LOGIN_DATE, new Date());
						SessionUtil.setValue(request, ConstantIF.SS_LOGIN_SESSION_TIME, 86400);

						if (user.get("LANG").equals("KR")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_KOREAN);// 사용자언어
																													// 타입
																													// 한국어
						} else if (user.get("LANG").equals("EN")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_ENGLISH);// 사용자언어
																														// 타입
																														// 영어
						} else if (user.get("LANG").equals("JA")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_JAPANESE);// 사용자언어
																														// 타입
																														// 일본어
						} else if (user.get("LANG").equals("CH")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_CHINESE);// 사용자언어
																														// 타입
																														// 중국어
						} else if (user.get("LANG").equals("VN")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_VIETNAM);// 사용자언어
																														// 타입
																														// 중국어
						}else if (user.get("LANG").equals("ES")) {
                            SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_ESPANA);
						}

						// CodeMngr.reload((String)user.get("LC_ID"));//시스템로그인
						// 성공시 해당 svcNo별 설정된 공통코드 메모리적재 2012-03-30 by indigo;
						check = 1;

						// 로그인 여부를 셋팅
						SessionMn.init();
						LoginInfo loginInfo = new LoginInfo();
						loginInfo.setId(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
						loginInfo.setLastRequestTime(Calendar.getInstance());
						SessionMn.getInstance().addInfo(loginInfo);

						// 로그인시 첫페이지 권한설정...
						model.put("SS_SVC_NO", user.get("LC_ID"));
						model.put("USER_NO", user.get("USER_NO"));
						model.put("USER_ID", user.get("USER_ID"));
						model.put("SS_SVC_USE_TY", user.get("LC_USE_TY"));
						model.put("SS_LC_CHANGE_YN", user.get("LC_CHANGE_YN"));
						model.put("SS_ITEM_FIX_YN", user.get("ITEM_FIX_YN"));
						model.put("SS_ITEM_GRP_ID", user.get("ITEM_GRP_ID"));
						model.put("SS_TEL_NO", user.get("TEL_NO"));
						model.put("SS_SUB_PASSWORD", user.get("SUB_PASSWORD"));
						model.put("SS_PROC_AUTH", user.get("PROC_AUTH"));
						model.put("SS_USE_TRANS_CUST", user.get("USE_TRANS_CUST"));
						model.put("WORK_IP", SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_IP));
						model.put("Session_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID)));

						model.put("AUTH_CD", user.get("AUTH_CD"));
						model.put("PROGRAM_ID", "TMSYS030");

						m.put("windowRoll", service.getRoll(model));

						// 멀티 물류센터 처리
						lcInfo = (ArrayList<Map<String, Object>>) service.selectLc(model).get("LCINFO");
						session.setAttribute("SS_LC_INFO", lcInfo);
//						// 로그인 시도횟수 초기화. 220513 kimzero
//						service.initLoginRetryCnt(model);
						// 접속여부 업데이트
						service.updateLogin(model);
						// 접속 히스토리
						service.updateLoginHistory(model);
					} else {
//						if(loginRetryCnt >= 5){ // 로그인 시도횟수 초과 == 16
//							check = 16;
//						}else{ // 이외 사유에 의한 로그인 불가 == 15
							check = 15;
//						}
						
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_IP, JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						SessionUtil.setValue(request, ConstantIF.SS_SESSION_ID, request.getSession().getId());// (클라이언트)접속IP

						SessionMn.init();
						SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
						SessionUtil.sessionClear(request);
						
						// 로그인시 첫페이지 권한설정...
						model.put("USER_NO", user.get("USER_NO"));
						model.put("WORK_IP", SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_IP));
						model.put("Session_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID)));

//						// 로그인 시도횟수 증가. 220513 kimzero
//						service.increaseLoginRetryCnt(model);
						
						// 접속여부 업데이트
						service.updateLogout(model);
						// 접속 히스토리
						service.updateLoginHistory(model);
					}
				} else {
					switch (reasonCd) {
					case "RETRY_CNT_OVER":
						check = 16;
						break;
					case "PWD_CHK":
						check = 17;
						break;
					case "PWD_DUEDATE_OVER":
						check = 18;
						break;
					case "INACTIVE_ID":
						check = 19;
						break;
					default:
						check = 0;
						break;
					}
				}
			} // 로그인일 경우 끝
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to login process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "로그인에 실패했습니다.등록되어 있지 않는 사용자입니다.");
			m.put("MSG", MessageResolver.getMessage("로그인실패"));
			check = 0;
		}
		//check == 1   => 정상로그인
		//check == 15  => 비정상로그인
		//check == 16  => 비정상로그인(로그인 시도횟수초과)
		//check == 17  => 비정상로그인(비밀번호 틀림)
		//check == 18  => 비정상로그인(유효기간 초과)
		//check == 19  => 비정상로그인(휴면계정)
		

		if (check == 1) {
			try {
				// mav = new ModelAndView("winus/main/main"); // 로그인 성공 시 메인
				// 화면으로 이동
				// mav = new ModelAndView("winus/tmsys/TMSY030Mn"); // 로그인 성공 시
				// 메인 화면으로 이동
				// mav = new ModelAndView("fta/stnd/stndHsCatgMn"); // HS검색 화면으로
				// 이동
				// mav = new ModelAndView("winus/stnd/stndHsCatgMn",
				// service.stndHsCatgCombo(model));
				model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
				String url = service.selectScreen(model);
				// System.out.println(url);
				if (url == null) {
					RequestDispatcher rd = request.getRequestDispatcher("/kiosk/style2/kioskMain.action");
					rd.forward(request, response);

				} else {
					mav = new ModelAndView("forward:" + url);
				}
				// String cls = (String)model.get("CLS");

				// if(cls == null){
				// mav = new ModelAndView("fta/stnd/stndHsCatgMn"); // HS검색 화면으로
				// 이동
				// mav = new ModelAndView("winus/stnd/stndHsCatgMn",
				// service.stndHsCatgCombo(model));
				// }else{
				// mav = new ModelAndView("winus/common/comAlert");
				// }
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to login process(forward) :", e);
				}
			}
		} else if (check == 15) {
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "이미 접속한 사용자가 있습니다.");
			m.put("MSG", MessageResolver.getMessage("이전에 비정상 종료로 로그아웃 되었습니다. 다시 로그인 하십시오."));
			
			mav = new ModelAndView("winus/kiosk/style2/login"); // 로그인 실패 시 다시 로그인 화면으로

		} else if(check == 16) {		//check == 16  => 비정상로그인(로그인 시도횟수초과)
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("보안정책에 의해 차단되었습니다. 로그인 재시도횟수 초과(운영팀 문의)"));

			mav = new ModelAndView("winus/kiosk/style2/login"); // 로그인 실패 시 다시 로그인 화면으로
		} else if(check == 17) {//check == 17  => 비정상로그인(비밀번호 틀림)
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("아이디/비밀번호를 확인해주세요.(연속 5회이상 실패시 차단)"));
			
			mav = new ModelAndView("winus/kiosk/style2/login"); // 로그인 실패 시 다시 로그인 화면으로
		} else if(check == 18) {//check == 18  => 비정상로그인(유효기간 초과)
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("보안정책에 의해 차단되었습니다. \\n비밀번호 유효기간 초과.\\n로그인화면에서 비밀번호 변경을 부탁드립니다."));
			
			mav = new ModelAndView("winus/kiosk/style2/login"); // 로그인 실패 시 다시 로그인 화면으로
		} else if(check == 19) {//check == 19  => 비정상로그인(휴면계정)
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("보안정책에 의해 차단되었습니다. 휴면계정(PW변경후 사용)"));
			
			mav = new ModelAndView("winus/kiosk/style2/login"); // 로그인 실패 시 다시 로그인 화면으로
		} else {
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "로그인에 실패했습니다.등록되어 있지 않는 사용자입니다.");
			m.put("MSG", MessageResolver.getMessage("로그인실패"));

			mav = new ModelAndView("winus/kiosk/style2/login"); // 로그인 실패 시 다시 로그인 화면으로
		}
		if (mav != null)
			mav.addAllObjects(m);
		return mav;
	}
	
	
	@RequestMapping("/kiosk/style1/kioskMain.action")
	public ModelAndView kioskMain1(HttpServletRequest request, Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/kiosk/style1/kioskMain");
	}	
	
	
	@RequestMapping("/kiosk/style2/kioskMain.action")
	public ModelAndView kioskMain2(HttpServletRequest request, Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/kiosk/style2/kioskMain");
	}	
	
}
