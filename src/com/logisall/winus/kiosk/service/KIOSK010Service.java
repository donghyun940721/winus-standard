package com.logisall.winus.kiosk.service;

import java.util.Map;



public interface KIOSK010Service {
	public Map<String, Object> getDlvCarList(Map<String, Object> model) throws Exception;
    public Map<String, Object> getDlvOrdList(Map<String, Object> model) throws Exception;
}
