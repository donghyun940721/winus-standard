package com.logisall.winus.kiosk.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.kiosk.service.KIOSK010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("KIOSK010Service")
public class KIOSK010ServiceImpl extends AbstractServiceImpl implements KIOSK010Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

   @Resource(name = "KIOSK010Dao")
    private KIOSK010Dao dao;

   /**
    * Method ID    : getDlvCarList
    * Method 설명      : 
    * 작성자                 : yhku
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> getDlvCarList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.getDlvCarList(model));
        return map;
    }
    
    /**
    * Method ID    : getDlvOrdList
    * Method 설명      : 
    * 작성자                 : yhku
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> getDlvOrdList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	
        	if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "20");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}
    		
            map.put("LIST", dao.getDlvOrdList(model));

        } catch (Exception e) {
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
	/**
	 * Method ID : selectReqPerson 
	 * Method 설명 : 셀렉트 박스 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
//	public Map<String, Object> detail(Map<String, Object> model) throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			
//			String boardId = (String) model.get("vrBoardId");			
//			model.put("BOARD_TYPE", "NOTICE");			
//			model.put("BOARD_IDX", boardId);
//			
//			int cnt = dao.updateCount(model);			
//			if ( cnt > 0) {
//				map.put("DETAIL", dao.selectBoardInfo(model));			
//				map.put("CLIENT", dao.selectClientList(model));
//				map.put("FILE", dao.selectFileInfoList(model));
//			}
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get List :", e);
//			}
//			map.put("MSG", MessageResolver.getMessage("list.error"));
//		}		
//		return map;
//	}
    
 
}
