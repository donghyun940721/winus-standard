package com.logisall.winus.kiosk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("KIOSK010Dao")
public class KIOSK010Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	
	/**
     * Method ID : getDlvCarList
     * Method 설명 : 
     * 작성자 : yhku
     *
     * @param model
     * @return
     */
    public Object getDlvCarList(Map<String, Object> model){
        return executeQueryForList("kiosk010.getDlvCarList", model);
    }
    
    /**
     * Method ID : getDlvOrdList
     * Method 설명 : 
     * 작성자 : yhku
     *
     * @param model
     * @return
     */
    public GenericResultSet getDlvOrdList(Map<String, Object> model){
        return executeQueryPageWq("kiosk010.getDlvOrdList", model);
    }
    
}
