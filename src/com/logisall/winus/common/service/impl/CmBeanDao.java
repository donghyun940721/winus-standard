package com.logisall.winus.common.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("CmBeanDao")
public class CmBeanDao extends SqlMapAbstractDAO {
	
	protected Log log = LogFactory.getLog(this.getClass());

/*
 *  작성자       : chSong
 *  srchKey : 명명            :구현: 파라미터 명
 * ----------------------------------------------------------------------------------
    CAR     : 차량            : O : vrSrchCarCd , vrSrchCarNm(X)
    CENTER  : 물류센터      : O : vrSrchCenterCd, vrSrchCenterNm
    CLIENT  : 고객사         : O : vrSrchCustCd, vrSrchCustNm, srchSubCode3, srchSubCode4
                             (srchSubCode3 => 100 : 고객사, 104 : 물류센터, 108 : 거래처 ) 
    CUST    : 화주            : O : vrSrchCustCd, vrSrchCustNm
    DEPT    : 부서            : X :
    DOCK    : 도크            : O : vrSrchDockCd, vrSrchDockNo, vrSrchDockNm
    EMPLOY  : 사원            : X :
    ITEM    : 상품            : O : vrSrchItemCd, vrSrchItemNm
    LOCATION : 로케이션    : O : vrSrchLocCd, vrSrchLocId
    UOM     : UOM      : O : vrSrchUomCd, vrSrchUomNm
    USER    : 사용자         : O : vrSrchUserId, vrSrchUserNm, srchSubCode3, srchSubCode5
    WH      : 창고            : O : vrSrchWhCd, vrSrchWhNm
    ZONE    : 존               : O : vrSrchZoneId, vrSrchZoneNm
    POOL    : 물류용기      : O : vrSrchPoolId, vrSrchPoolNm  -- 추가 (2015-07-27 기드온)
    ----------------------------------------------------------------------------------
 */
	
    /**
     * Method ID  : selectMngCodeWH
     * Method 설명  : selectMngCode 창고조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeWH(Map<String, Object> model){
        return executeQueryForList("wmsms040.selectMngCodeWH", model);
    }
    
    /**
     * Method ID  : selectMngCodeStore
     * Method 설명  : selectMngCode 화주조회
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectMngCodeStore(Map<String, Object> model){
        return executeQueryForList("wmsms010.selectMngCodeStore", model);
    }

    /**
     * Method ID  : selectMngCodeStore1
     * Method 설명  : selectMngCode 거래처 조회
     * 작성자             : dhkim
     * @param model
     * @return
     */
    public Object selectMngCodeStore1(Map<String, Object> model){
        return executeQueryForList("wmsms010.selectMngCodeStore1", model);
    }
    
    /**
     * Method ID  : selectMngCodeCUST
     * Method 설명  : selectMngCode 화주조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeCUST(Map<String, Object> model){
        return executeQueryForList("wmsms010.selectMngCodeCUST", model);
    }
    
    /**
     * Method ID  : selectMngCodeLCCUST
     * Method 설명  : 물류사 화주별 화주조회
     * 작성자             : schan
     * @param model
     * @return
     */
    public Object selectMngCodeLCCUST(Map<String, Object> model){
        return executeQueryForList("wmsms010.selectMngCodeLCCUST", model);
    }
    
    /**
     * Method ID  : selectMngCodeLCCUST
     * Method 설명  : 거래처ZONE 조회
     * 작성자             : schan
     * @param model
     * @return
     */
    public Object selectMngCodeCUSTZONE(Map<String, Object> model){
        return executeQueryForList("wmsms085.selectMngCodeCUSTZONE", model);
    }
    
    
    /**
     * Method ID  : selectMngCodeITEM
     * Method 설명  : selectMngCode 상품조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeITEM(Map<String, Object> model){
    	   // 대화물류일 경우 > 상품코드, 박스바코드 같이 조회 하도록
    	String svcNo = String.valueOf(model.get("SS_SVC_NO"));
        if(svcNo.equals("0000003541") || svcNo.equals("0000003721") || svcNo.equals("0000003722") || svcNo.equals("0000003720")){//대화물류 1층,2층,3층,보세
        	   return executeQueryForList("wmsms091.selectMngCodeITEMBOX", model);
        }else{
        	   return executeQueryForList("wmsms091.selectMngCodeITEM", model);
        }
    }
    
    /**
     * Method ID  : selectMngCodeITEM
     * Method 설명  : selectMngCode 상품조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeITEMEQUALS(Map<String, Object> model){
        return executeQueryForList("wmsms091.selectMngCodeITEMEQUALS", model);
    }
    
    /**
     * Method ID  : selectMngCodeCLIENT
     * Method 설명  : selectMngCode 고객사
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeCLIENT(Map<String, Object> model){
        return executeQueryForList("wmsms030.selectMngCodeCLIENT", model);
    }
    
    /**
     * Method ID  : selectMngCodeLOCATION
     * Method 설명  : selectMngCode 로케이션
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeLOCATION(Map<String, Object> model){
        return executeQueryForList("wmsms080.selectMngCodeLOCATION", model);
    }
    
    /**
     * Method ID  : selectMngCodeLOCATION_ONLY
     * Method 설명  : selectMngCode 로케이션
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeLOCATIONONLY(Map<String, Object> model){
        return executeQueryForList("wmsms080.selectMngCodeLOCATION_ONLY", model);
    }
    
    /**
     * Method ID  : selectMngCodeDOCK
     * Method 설명  : selectMngCode 도크장
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeDOCK(Map<String, Object> model){
        return executeQueryForList("wmsms120.selectMngCodeDOCK", model);
    }
    
    /**
     * Method ID  : selectMngCodeUOM
     * Method 설명  : selectMngCode UOM
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeUOM(Map<String, Object> model){
        return executeQueryForList("wmsms100.selectMngCodeUOM", model);
    }
    
    /**
     * Method ID  : selectMngCodeCENTER
     * Method 설명  : selectMngCode 센터
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeCENTER(Map<String, Object> model){
        return executeQueryForList("wmsms030.selectMngCodeCENTER", model);
    }
    
    /**
     * Method ID  : selectMngCodeUSER
     * Method 설명  : selectMngCode 사용자
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeUSER(Map<String, Object> model){
        return executeQueryForList("tmsys090.selectMngCodeUSER", model);
    }
    
    /**
     * Method ID  	 : selectMngCodeUSER2
     * Method 설명  	 : selectMngCode 사용자2
     * 						   1. tmsys090.selectMngCodeUSER  : WMSMS010 JOIN > 파악 x, 전체 조회되고 있는 걸로 판단됨 
							   2. tmsys090.selectMngCodeUSER2 : TMSYS091 JOIN > 물류센터에 속한 사용자만 조회
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public Object selectMngCodeUSER2(Map<String, Object> model){
        return executeQueryForList("tmsys090.selectMngCodeUSER2", model);
    }

    /**
     * Method ID  : selectMngCodeZONE
     * Method 설명  : selectMngCode ZONE
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeZONE(Map<String, Object> model){
        return executeQueryForList("wmsms081.selectMngCodeZONE", model);
    }
    
    /**
     * Method ID  : selectMngCodeCAR
     * Method 설명  : selectMngCode 차량
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeCAR(Map<String, Object> model){
        return executeQueryForList("wmsms050.selectMngCodeCAR", model);
    }
    
    /**
     * Method ID  : selectMngCodePOOL
     * Method 설명  : selectMngCode 물류용기
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectMngCodePOOL(Map<String, Object> model){
        return executeQueryForList("wmspl010.selectMngCodePOOL", model);
    }
    /**
     * Method ID  : selectMngCodeITEMGRP
     * Method 설명  : selectMngCode 상품군
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectMngCodeITEMGRP(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectMngCodeITEMGRP", model);
    }
    
    /**
     * Method ID  : selectMngCodeDEPT
     * Method 설명  : selectMngCode 계약부서
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeDEPT(Map<String, Object> model){
        return executeQueryForList("wmsms014.selectMngCodeDEPT", model);
    }
    
    /**
     * Method ID  : selectMngCodeEMPLOY
     * Method 설명  : selectMngCode 사원
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeEMPLOY(Map<String, Object> model){
        return executeQueryForList("wmsms015.selectMngCodeEMPLOY", model);
    }
    
    /**
     * Method ID  : selectMngCodeADMIN
     * Method 설명  : selectMngCode ADMIN
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeADMIN(Map<String, Object> model){
        return executeQueryForList("wmsms012.selectMngCodeADMIN", model);
    }
    
    /**
     * Method ID  : selectMngCodeDLVCUST
     * Method 설명  : selectMngCode DLVCUST
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectMngCodeDLVCUST(Map<String, Object> model){
        return executeQueryForList("wmssp010.selectMngCodeDLVCUST", model);
    }
    
    
    
    
    
    /**
     * Method ID  : selectFncCode2
     * Method 설명  : selectFncCode2
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectFncCode2(Map<String, Object> model){
        executeUpdate("wmsms012.pk_wmsms012.GET_FCN_CODE2", model);
        return model;
    }
    

    /**
     * Method ID  : selectMngCodePARTNER
     * Method 설명  : selectMngCode PARTNER
     * 작성자             : 김기하
     * @param model
     * @return
     */
    public Object selectMngCodePARTNER(Map<String, Object> model){
        return executeQueryForList("wmsms200.selectMngCodePARTNER", model);
    }
    
    /**
     * Method ID  : selectMngCodeTEL
     * Method 설명  : selectMngCode TEL
     * 작성자             : 김기하
     * @param model
     * @return
     */
    public Object selectMngCodeTEL(Map<String, Object> model){
        return executeQueryForList("wmsms201.selectMngCodeTEL", model);
    }
    
    /**
     * Method ID  : selectMngCodeITEMIF
     * Method 설명  : selectMngCode ITEMIF
     * 작성자             : 김기하
     * @param model
     * @return
     */
	public Object selectMngCodeITEMIF(Map<String, Object> model) {
		return executeQueryForList("wmsms202.selectMngCodeITEMIF", model);
	}
	/**
     * Method ID  : selectMngCodeDRIVER
     * Method 설명  : selectMngCodeDRIVER
     * 작성자             : 김기하
     * @param model
     * @return
     */
	public Object selectMngCodeDRIVER(Map<String, Object> model) {
		return executeQueryForList("wmsms051.selectMngCodeDRIVER", model);
	}
	
	/**
     * Method ID       : selectMngCodeCOUNTRY
     * Method 설명    : selectMngCodeCOUNTRY
     * 작성자             : KSJ
     * @param model
     * @return
     */
	public Object selectMngCodeCOUNTRY(Map<String, Object> model) {
		return executeQueryForList("wmstg731.selectMngCodeCOUNTRY", model);
	}
}
