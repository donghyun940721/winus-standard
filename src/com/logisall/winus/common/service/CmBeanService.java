package com.logisall.winus.common.service;

import java.util.Map;

public interface CmBeanService {
    public Map<String, Object> selectMngCode(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectFncCode(Map<String, Object> model) throws Exception;
}
