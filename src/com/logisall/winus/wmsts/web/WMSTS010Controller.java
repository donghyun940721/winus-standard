package com.logisall.winus.wmsts.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST010Service;
import com.logisall.winus.wmsts.service.WMSTS010Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSTS010Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTS010Service")
    private WMSTS010Service service;
    
    @Resource(name = "WMSST010Service")
	private WMSST010Service WMSST010service;
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	static final String[] COLUMN_NAME_WMSTS010E1_OUT = {
			"TRANS_CUST_CD", "CUST_LOT_NO"
		};
	
	static final String[] COLUMN_NAME_WMSTS010E1_IN = {
			"CUST_LOT_NO"
		};
    
    /**
     * Method ID	: WMSTS010
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSTS010.action")
    public ModelAndView wmsts010(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsts/WMSTS010", WMSST010service.selectBox(model) );
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTS010/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listE2
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTS010/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
   
	
	
	 /**
     * Method ID	: listE3
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTS010/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
	
	
    
	 /**
    * Method ID	: listE4
    * Method 설명	: 
    * 작성자			: yhku
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSTS010/listE4.action")
   public ModelAndView listE4(Map<String, Object> model) throws Exception {
   	ModelAndView mav = null;
       try {
           mav = new ModelAndView("jqGridJsonView", service.listE4(model));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return mav;
   }
   
	 /**
    * Method ID	: listSubE4
    * Method 설명	: 
    * 작성자			: yhku
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSTS010/listSubE4.action")
   public ModelAndView listSubE4(Map<String, Object> model) throws Exception {
   	ModelAndView mav = null;
       try {
           mav = new ModelAndView("jqGridJsonView", service.listSubE4(model));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return mav;
   }
   

  /**
  * Method ID	: listE0
  * Method 설명	: 
  * 작성자			: yjw
  * @param   model
  * @return  
  * @throws Exception 
  */
 @RequestMapping("/WMSTS010/listE0.action")
 public ModelAndView listE0(Map<String, Object> model) throws Exception {
 	ModelAndView mav = null;
     try {
         mav = new ModelAndView("jqGridJsonView", service.listE0(model));
     } catch (Exception e) {
         e.printStackTrace();
     }
     return mav;
 }
 
 /**
  * Method ID   : listE5
  * Method 설명   : 
  * 작성자         : 
  * @param   model
  * @return  
  * @throws Exception 
  */
 @RequestMapping("/WMSTS010/listE5.action")
 public ModelAndView listE5(Map<String, Object> model) throws Exception {
    ModelAndView mav = null;
     try {
         mav = new ModelAndView("jqGridJsonView", service.listE5(model));
     } catch (Exception e) {
         e.printStackTrace();
     }
     return mav;
 }
 
 /**
  * Method ID	: listE0
  * Method 설명	: 
  * 작성자			: yjw
  * @param   model
  * @return  
  * @throws Exception 
  */
 @RequestMapping("/WMSTS010/getItemList.action")
 public ModelAndView getItemList(Map<String, Object> model) throws Exception {
 	ModelAndView mav = null;
     try {
         mav = new ModelAndView("jqGridJsonView", service.getItemList(model));
     } catch (Exception e) {
         e.printStackTrace();
     }
     return mav;
 }
 
	@RequestMapping("/WMSTS010E1pop1.action")
	public ModelAndView wmsop030SE1pop1(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsts/WMSTS010E1pop1");
	}
 
	@RequestMapping("/WMSTS010E1pop2.action")
	public ModelAndView wmsop030SE1pop2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsts/WMSTS010E1pop2");
	}

	/*-
	 * Method ID	: excelUploadOut
	 * Method 설명	: SKON 출고 템플릿 입력 (세척완료)
	 * 작성자			: wl2258
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTS010E1/excelUploadOut.action")
	public ModelAndView excelUploadOut(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSTS010E1_OUT, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			m = service.uploadExcelOut(mapBody, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID	: excelUploadTransfer
	 * Method 설명	: SKON 재고이동(세척-출고) 템플릿 입력
	 * 작성자			: wl2258
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTS010E1/excelUploadTransfer.action")
	public ModelAndView excelUploadTransfer(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSTS010E1_IN, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			m = service.uploadExcelTransfer(mapBody, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID	: excelUploadIn
	 * Method 설명	: SKON 회수 입고 템플릿 입력
	 * 작성자			: wl2258
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTS010E1/excelUploadIn.action")
	public ModelAndView excelUploadIn(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSTS010E1_IN, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			m = service.uploadExcelIn(mapBody, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}

}

