package com.logisall.winus.wmsts.service;

import java.util.List;
import java.util.Map;

public interface WMSTS010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE0(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> getItemList(Map<String, Object> model) throws Exception;
    public Map<String, Object> uploadExcelOut(Map<String, Object> model, List<Map> list) throws Exception;
    public Map<String, Object> uploadExcelTransfer(Map<String, Object> model, List<Map> list) throws Exception;
    public Map<String, Object> uploadExcelIn(Map<String, Object> model, List<Map> list) throws Exception;
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
}
