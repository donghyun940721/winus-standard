package com.logisall.winus.wmsts.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTS010Dao")
public class WMSTS010Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        //return executeQueryPageWq("wmsts010.list", model);
    	return executeQueryWq("wmsts010.list", model);
    }
    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listE2", model);
    }
    
    /**
     * Method ID  : listE3
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listE3", model);
    }

    /**
     * Method ID  : listE4
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE4(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listE4", model);
    }
    
    /**
     * Method ID  : listSubE4
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listSubE4(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listSubE4", model);
    }
    
    
    /**
     * Method ID  : listE5
     * Method 설명   : 
     * 작성자                : 
     * @param   model
     * @return
     */
    public GenericResultSet listE5(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listE5", model);
    }
    
    /**
     * Method ID  : listE0
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listE0(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listE0", model);
    }
    
    /**
     * Method ID  : listE02
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listE02(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listE02", model);
    }
    
    
    /**
     * Method ID  : chkMapGbn
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public Object chkMapGbn(Map<String, Object> model) {
        return executeView("wmsts010.chkMapGbn", model);
    }
    
    /**
     * Method ID  : listSubE0
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet listSubE0(Map<String, Object> model) {
        return executeQueryWq("wmsts010.listSubE0", model);
    }
    
    /**
     * Method ID  : getItemList
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet getItemList(Map<String, Object> model) {
        return executeQueryWq("wmsts010.getItemList", model);
    }
    
    /**
     * Method ID  : getItemList2
     * Method 설명   : 
     * 작성자                : yjw
     * @param   model
     * @return
     */
    public GenericResultSet getItemList2(Map<String, Object> model) {
        return executeQueryWq("wmsts010.getItemList2", model);
    }
    
    /**
     * @meethod getSetRitemCd
     * @user wl2258
     */
    public List<Map<String, Object>> getSetRitemCd(Map<String, Object> model) {
    	return executeQueryForList("wmsts010.getSetRitemCd", model);
    }
    
    /**
     * @meethod getPartRitemLocQty
     * @user wl2258
     */
    public List<Map<String, Object>> getPartRitemLocQty(Map<String, Object> model) {
    	return executeQueryForList("wmsts010.getPartRitemLocQty", model);
    }
    
    /**
     * @meethod getTransCustCd
     * @user wl2258
     */
    public Map<String, Object> getTransCustCd(Map<String, Object> model) {
    	return (Map<String, Object>) executeQueryForObject("wmsts010.getTransCustCd", model);
    }
    
    /**
     * @meethod runOrdOutSerialTraceCompleteMobile 시리얼 출고
     * @user wl2258
     */
    public Object runOrdOutSerialTraceCompleteMobile(Map<String, Object> model) {
    	executeUpdate("pk_wmsif060.OrdOutSerialTraceCompleteMobile", model);
		return model;
    } 
    
    /**
     * @meethod runPcSavMovestock 재고이동
     * @user wl2258
     */
    public Object runPcSavMovestock(Map<String, Object> model){
        executeUpdate("wmsst040.pk_wmsst040.pc_sav_movestock", model);
        return model;
    }
    
    /**
     * @meethod getTransferInfo
     * @user wl2258
     */
    public List<Map<String, Object>> getTransferInfo(Map<String, Object> model) {
    	return executeQueryForList("wmsts010.getTransferInfo", model);
    }
    
    /**
     * @meethod getLocInfo
     * @user wl2258
     */
    public Map<String, Object> getLocInfo(Map<String, Object> model) {
    	return (Map<String, Object>) executeQueryForObject("wmsif070.selectTypeLocInfo", model);
    }
    
    /**
     * @meethod getFilteredSerialInfo
     * @user wl2258
     */
    public Map<String, Object> getFilteredSerialInfo(Map<String, Object> model) {
    	return (Map<String, Object>) executeQueryForObject("wmsts010.getFilteredSerialInfo", model);
    }
    
}


