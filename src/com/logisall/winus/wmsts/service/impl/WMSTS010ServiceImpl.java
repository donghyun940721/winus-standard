package com.logisall.winus.wmsts.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsts.service.WMSTS010Service;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.logisall.ws.interfaces.wmsif.service.impl.WMSIF050Dao;

/**
 * 
 */
/**
 * 
 */
@Service("WMSTS010Service")
public class WMSTS010ServiceImpl implements WMSTS010Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTS010Dao")
	private WMSTS010Dao dao;
	
	@Resource(name = "WMSIF050Dao")
	private WMSIF050Dao WMSIF050Dao;

	/**
	 * 
	 * 대체 Method ID : list 대체 Method 설명 : 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
//        if(model.get("page") == null) {
//            model.put("pageIndex", "1");
//        } else {
//            model.put("pageIndex", model.get("page"));
//        }
//        if(model.get("rows") == null) {
//            model.put("pageSize", "20");
//        } else {
//            model.put("pageSize", model.get("rows"));
//        }
		map.put("LIST", dao.list(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listE2 대체 Method 설명 : 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.listE2(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listE3 대체 Method 설명 : 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.listE3(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listE4 대체 Method 설명 : 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.listE4(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listSubE4 대체 Method 설명 : 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listSubE4(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.listSubE4(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listE0 대체 Method 설명 : 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listE0(Map<String, Object> model) throws Exception {
		String gbn = dao.chkMapGbn(model).toString();
		Map<String, Object> map = new HashMap<String, Object>();

		if (gbn.equals("N") && !dao.listE02(model).getList().isEmpty()) {
			map.put("LIST", dao.listE02(model));
		} else if (dao.listE02(model).getList().isEmpty()) {
			map.put("LIST", dao.listSubE0(model));
		} else if (dao.listE0(model).getList().isEmpty()) {
			map.put("LIST", dao.listSubE0(model));
		} else {
			map.put("LIST", dao.listE0(model));
		}
		return map;
	}
	
	/**
     * 
     * 대체 Method ID : listE0 대체 Method 설명 : 작성자 : yhku
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception {
        String gbn = dao.chkMapGbn(model).toString();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE5(model));
        return map;
    }

	/**
	 * 
	 * 대체 Method ID : getItemList 대체 Method 설명 : 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> getItemList(Map<String, Object> model) throws Exception {
		model.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
		String gbn = dao.chkMapGbn(model).toString();
		Map<String, Object> map = new HashMap<String, Object>();
		if (gbn.equals("N")) {
			map.put("LIST", dao.getItemList2(model));
		} else {
			map.put("LIST", dao.getItemList(model));
		}

		return map;
	}

	/**
	 * 
	 * 대체 Method ID : disableSslVerification 대체 Method 설명 : 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	private static void disableSslVerification() {
		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {

				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {

				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @method uploadExcelOut SKON 출고(세척 완료 대상) 템플릿 업로드
	 * @user wl2258
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String, Object> uploadExcelOut(Map<String, Object> model, List<Map> list) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (list.isEmpty()) {
				result.put("errCnt", 1);
				result.put("MSG", MessageResolver.getMessage("list.nodata"));
				throw new BizException(MessageResolver.getMessage("save.error"));
			}
			
			/**
			 * validation
			 * 1. 세트 상품 존재 유무
			 * 2. 시리얼 번호별 출고 로케이션 내 재고 유무
			 * 3. transCustCd 존재 유무
		 	*/
			List<Map<String, Object>> setPartRitemList = new ArrayList<>();
			List<Map<String, Object>> partRitemLocQtyList = new ArrayList<>();
			Map<String, Object> transCustIdVal = new HashMap<String, Object>();
			StringBuilder validationMsg = new StringBuilder();
			for (int i = 0; i < list.size(); i ++) {
				Map<String, Object> row = list.get(i);
				setPartRitemList = getSetPartRitem(model, row);
				transCustIdVal = getTransCustId(model, row);
				
				String custLotNo = (String) row.get("CUST_LOT_NO"); 
				partRitemLocQtyList = getPartRitemLocQty("10", custLotNo, model, setPartRitemList);
				
				if (!validSetRitemCd(setPartRitemList)) {
					validationMsg.append((i+2) + "행 SerialNo: ").append(custLotNo).append(" 존재하지 않는 세트 상품 코드입니다.\n");
				} else if (!validOutQty(partRitemLocQtyList)) {
					validationMsg.append((i+2) + "행 SerialNo: ").append(custLotNo).append(" 출고 로케이션에 재고가 부족합니다.\n");
				}

				if (!validMap(transCustIdVal)) {
					validationMsg.append((i+2) + "행 SerialNo : ")
						.append(custLotNo).append(" 존재하지 않는 거래처 코드입니다.")
						.append(" [거래처 코드 : " + (String) row.get("TRANS_CUST_CD") + "]\n");
				}
			}
			
			String msg = validationMsg.toString();
			if (StringUtils.isNotEmpty(msg)) {
				result.put("errCnt", 1);
				throw new BizException(msg);
			}

			List<String> lcId = new ArrayList<>();
			List<String> custId = new ArrayList<>();
			List<String> ritemId = new ArrayList<>();
			List<String> custLotNo = new ArrayList<>();
			List<String> sapBarcode = new ArrayList<>();
			List<String> workQty = new ArrayList<>();
			List<String> transCustId = new ArrayList<>();

			int partRitemSize = 0;
			for (Map<String, Object> row : list) {
				lcId.add((String) model.get(ConstantIF.SS_SVC_NO));
				custId.add((String) model.get("vrCustId"));
				ritemId.add((String) setPartRitemList.get(0).get("SET_RITEM_ID"));
				custLotNo.add((String) row.get("CUST_LOT_NO"));
				workQty.add("1");
				transCustId.add((String) transCustIdVal.get("CUST_ID"));
				sapBarcode.add(null);
			}

			Map<String, Object> modelIns = new HashMap<>();
			modelIns.put("iLcId", lcId.toArray(new String[partRitemSize]));
			modelIns.put("iCustId", custId.toArray(new String[partRitemSize]));
			modelIns.put("iRitemId", ritemId.toArray(new String[partRitemSize]));
			modelIns.put("iCustLotNo", custLotNo.toArray(new String[partRitemSize]));
			modelIns.put("iSapBarcode", sapBarcode.toArray(new String[partRitemSize]));
			modelIns.put("iWorkQty", workQty.toArray(new String[partRitemSize]));
			modelIns.put("iTransCustId", transCustId.toArray(new String[partRitemSize]));

			modelIns.put("iWorkIp", (String) model.get(ConstantIF.SS_CLIENT_IP));
			modelIns.put("iUserNo", (String) model.get(ConstantIF.SS_USER_NO));

			modelIns = (Map<String, Object>) dao.runOrdOutSerialTraceCompleteMobile(modelIns);

			InterfaceUtil.isValidReturnCode("WMSTS010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			result.put("errCnt", String.valueOf(modelIns.get("O_MSG_CODE")));
			result.put("MSG", (String) modelIns.get("O_MSG_NAME"));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw e;
		}

		return result;
	}

	
	/**
	 * @meethod getSetPartRitem 세트, 파트 상품 정보 조회
	 * @user wl2258
	 */
	private List<Map<String, Object>> getSetPartRitem(Map<String, Object> model, Map<String, Object> rowData) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
		param.put("CUST_ID", (String) model.get("vrCustId"));

		String custLotNo = (String) rowData.get("CUST_LOT_NO");
		String setRitemCd = "";
		if (custLotNo != null && custLotNo.contains("-")) {
			setRitemCd = custLotNo.split("-")[0];
		}

		param.put("SET_RITEM_CD", setRitemCd);
		return dao.getSetRitemCd(param);
	}
	
	
	/**
	 * @meethod validSetRitemCd 세트 상품 코드 존재 여부
	 * @user wl2258
	 */
	private boolean validSetRitemCd(List<Map<String, Object>> setPartRitemList) {
		boolean isValid = false;
		if (setPartRitemList != null && !setPartRitemList.isEmpty())
			isValid = true;
		return isValid;
	}

	/**
	 * @meethod getPartRitemLocQty 로케이션 재고 확인(locType)
	 * @user wl2258
	 */
	private List<Map<String, Object>> getPartRitemLocQty(String locType, String custLotNo, 
			Map<String, Object> model, List<Map<String, Object>> setPartRitemList) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
		param.put("CUST_ID", (String) model.get("vrCustId"));
		param.put("LOC_TYPE", locType);
		param.put("CUST_LOT_NO", custLotNo);
		param.put("SET_RITEM_ID", setPartRitemList.get(0).get("SET_RITEM_ID"));
		return dao.getPartRitemLocQty(param);
	}

	
	/**
	 * @meethod validOutQty 출고 로케이션 재고 여부 유효성 검사
	 * @user wl2258
	 */
	private boolean validOutQty(List<Map<String, Object>> partRitemLocQtyList) {
		boolean isValid = true;
		for (Map<String, Object> m : partRitemLocQtyList) {
			BigDecimal stockAbleQty = (BigDecimal) m.get("STOCK_ABLE_QTY");
			BigDecimal setRitemQty = (BigDecimal) m.get("QTY");
			if (stockAbleQty.intValue() != setRitemQty.intValue())
				isValid = false;
		}
		return isValid;
	}

	
	/**
	 * @meethod getTransCustId 거래처 ID 조회
	 * @user wl2258
	 */
	private Map<String, Object> getTransCustId(Map<String, Object> model, Map<String, Object> rowData) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("CUST_ID", (String) model.get("vrCustId"));
		param.put("TRANS_CUST_CD", (String) rowData.get("TRANS_CUST_CD"));

		return dao.getTransCustCd(param);
	}

	/**
	 * @meethod validMap 파라미터(Map) 유효성 검사
	 * @user wl2258
	 */
	private boolean validMap(Map<String, Object> map) {
		boolean isValid = false;
		if (map != null && !map.isEmpty()) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * @method uploadExcelTransfer 재고이동(세척>출고) 템플릿 업로드
	 * @user wl2258
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String, Object> uploadExcelTransfer(Map<String, Object> model, List<Map> list) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (list.isEmpty()) {
				result.put("errCnt", 1);
				result.put("MSG", MessageResolver.getMessage("list.nodata"));
				throw new BizException(MessageResolver.getMessage("save.error"));
			}
	
			/**
			 * validation
			 * 1. 세트 상품 존재 유무
			 * 2. 시리얼 번호별 세척 로케이션 내 재고 유무
			 */
			StringBuilder validationMsg = new StringBuilder();
			List<Map<String, Object>> setPartRitemList = new ArrayList<>();
			for (int i = 0; i < list.size(); i ++) {
				Map<String, Object> row = list.get(i);
				setPartRitemList = getSetPartRitem(model, row);
				
				String custLotNo = (String) row.get("CUST_LOT_NO"); 
				if (!validSetRitemCd(setPartRitemList)) {
					validationMsg.append((i+2) + "행 SerialNo: ").append(custLotNo).append(" 존재하지 않는 세트 상품 코드입니다.\n");
				} else if (!validCleanLocQty(custLotNo, model, setPartRitemList)) {
					validationMsg.append((i+2) + "행 SerialNo: ").append(custLotNo).append(" 세척 로케이션에 재고가 부족합니다.\n");
				}
			}
			
			String msg = validationMsg.toString();
			if (StringUtils.isNotEmpty(msg)) {
				result.put("errCnt", 1);
				throw new BizException(msg);
			}

			// 출고 로케이션 정보 조회
			Map<String, Object> locInfoModel = new HashMap<>();
			locInfoModel.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
			locInfoModel.put("LOC_TYPE", "10");
			Map<String, Object> outLocInfo = (Map<String, Object>) dao.getLocInfo(locInfoModel);

			List<String> workSeq = new ArrayList<>();
			List<String> fromLocCd = new ArrayList<>();
			List<String> ritemId = new ArrayList<>();
			List<String> subLotId = new ArrayList<>();
			List<String> uomId = new ArrayList<>();
			List<String> pltQty = new ArrayList<>();
			List<String> workStat = new ArrayList<>();
			List<String> itemWorkDt = new ArrayList<>();
			List<String> itemWorkTm = new ArrayList<>();
			List<String> makeDt = new ArrayList<>();
			List<String> custId = new ArrayList<>();
			List<String> custLotNo = new ArrayList<>();
			List<String> blNo = new ArrayList<>();
			List<String> stockId = new ArrayList<>();
			List<String> reasonCd = new ArrayList<>();
			List<String> toLocCd = new ArrayList<>();
			List<String> workDesc = new ArrayList<>();
			List<String> lotType = new ArrayList<>();
			List<String> ownerCd = new ArrayList<>();
			List<String> moveQty = new ArrayList<>();

			int transferSize = 0;
			for (Map<String, Object> row : list) {
				// 재고이동 대상 조회 (세척 로케이션)
				Map<String, Object> transferSearchModel = new HashMap<>();
				transferSearchModel.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
				transferSearchModel.put("CUST_LOT_NO", (String) row.get("CUST_LOT_NO"));
				transferSearchModel.put("LOC_TYPE", "140");
				List<Map<String, Object>> transferInfoList = dao.getTransferInfo(transferSearchModel);
				transferSize = transferInfoList.size();

				for (int i = 0; i < transferInfoList.size(); i++) {
					Map<String, Object> stockInfo = transferInfoList.get(i);
					workSeq.add(null);
					fromLocCd.add((String) stockInfo.get("LOC_CD"));
					ritemId.add((String) stockInfo.get("RITEM_ID"));
					subLotId.add((String) stockInfo.get("SUB_LOT_ID"));
					uomId.add((String) stockInfo.get("UOM_ID"));
					pltQty.add("0");
					workStat.add("FF");
					itemWorkDt.add((String) stockInfo.get("WORK_DT"));
					itemWorkTm.add(null);
					makeDt.add((String) stockInfo.get("MAKE_DT"));
					custId.add((String) stockInfo.get("CUST_ID"));
					custLotNo.add(null);
					blNo.add(null);
					stockId.add(null);

					// 출고 로케이션
					reasonCd.add("23");
					toLocCd.add((String) outLocInfo.get("LOC_CD"));

					workDesc.add(null);
					lotType.add((String) stockInfo.get("SUB_LOT_TYPE"));
					ownerCd.add((String) stockInfo.get("OWNER_CD"));
					moveQty.add((String) stockInfo.get("WORK_ABLE_QTY").toString());
				}
			}

			Map<String, Object> modelIns = new HashMap<>();
			modelIns.put("workSeq", workSeq.toArray(new String[transferSize]));
			modelIns.put("fromLocCd", fromLocCd.toArray(new String[transferSize]));
			modelIns.put("toLocCd", toLocCd.toArray(new String[transferSize]));
			modelIns.put("ritemId", ritemId.toArray(new String[transferSize]));
			modelIns.put("subLotId", subLotId.toArray(new String[transferSize]));
			modelIns.put("uomId", uomId.toArray(new String[transferSize]));
			modelIns.put("moveQty", moveQty.toArray(new String[transferSize]));
			modelIns.put("pltQty", pltQty.toArray(new String[transferSize]));
			modelIns.put("workStat", workStat.toArray(new String[transferSize]));
			modelIns.put("itemWorkDt", itemWorkDt.toArray(new String[transferSize]));
			modelIns.put("itemWorkTm", itemWorkTm.toArray(new String[transferSize]));
			modelIns.put("makeDt", makeDt.toArray(new String[transferSize]));
			modelIns.put("custId", custId.toArray(new String[transferSize]));
			modelIns.put("custLotNo", custLotNo.toArray(new String[transferSize]));
			modelIns.put("blNo", blNo.toArray(new String[transferSize]));
			modelIns.put("stockId", stockId.toArray(new String[transferSize]));
			modelIns.put("reasonCd", reasonCd.toArray(new String[transferSize]));
			modelIns.put("workDesc", workDesc.toArray(new String[transferSize]));
			modelIns.put("lotType", lotType.toArray(new String[transferSize]));
			modelIns.put("ownerCd", ownerCd.toArray(new String[transferSize]));

			modelIns.put("vrLcId", (String) model.get(ConstantIF.SS_SVC_NO));
			modelIns.put("gvUserIP", (String) model.get(ConstantIF.SS_CLIENT_IP));
			modelIns.put("gvUserNo", (String) model.get(ConstantIF.SS_USER_NO));

			modelIns = (Map<String, Object>) dao.runPcSavMovestock(modelIns);
			InterfaceUtil.isValidReturnCode("WMSTS010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			result.put("errCnt", modelIns.get("O_MSG_CODE"));
			result.put("MSG", (String) modelIns.get("O_MSG_NAME"));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
		return result;
	}

	/**
	 * @meethod validCleanLocQty 세척 로케이션 재고 여부 유효성 검사
	 * @user wl2258
	 */
	private boolean validCleanLocQty(String custLotNo, Map<String, Object> model, List<Map<String, Object>> setPartRitemList) {
		List<Map<String, Object>> result = new ArrayList<>();
		boolean isValid = false;

		String cleanLocType = "140";
		result = getPartRitemLocQty(cleanLocType, custLotNo, model, setPartRitemList);

		for (Map<String, Object> m : result) {
			BigDecimal stockAbleQty = (BigDecimal) m.get("STOCK_ABLE_QTY");
			if (stockAbleQty.intValue() > 0)
				isValid = true;
		}

		return isValid;
	}

	/**
	 * @method uploadExcelIn SKON 회수 입고 템플릿 업로드
	 * @user wl2258
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String, Object> uploadExcelIn(Map<String, Object> model, List<Map> list) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (list.isEmpty()) {
				result.put("errCnt", 1);
				result.put("MSG", MessageResolver.getMessage("list.nodata"));
				throw new BizException(MessageResolver.getMessage("save.error"));
			}
			
			/**
			 * validation
			 * 1. 세트 상품 존재 유무
			 * 2. WMSTS010 테이블 내 다른 센터 재고 유무
			 */
			List<Map<String, Object>> setPartRitemList = new ArrayList<>();
			Map<String, Object> serialInfo = new HashMap<>();
			StringBuilder validationMsg = new StringBuilder();
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> row = list.get(i);
				setPartRitemList = getSetPartRitem(model, row);
				serialInfo = getFilteredSerialInfo(model, row);
				
				String custLotNo = (String) row.get("CUST_LOT_NO"); 
				if (!validSetRitemCd(setPartRitemList)) {
					validationMsg.append((i+2) + "행 SerialNo: ").append(custLotNo).append(" 존재하지 않는 세트 상품 코드입니다.\n");
				} else if (!validMap(serialInfo)) {
					validationMsg.append((i+2) + "행 SerialNo: ").append(custLotNo).append(" 회수 입고할 재고가 존재하지 않습니다.\n");
				}
			}
			
			String msg = validationMsg.toString();
			if (StringUtils.isNotEmpty(msg)) {
				result.put("errCnt", 1);
				throw new BizException(msg);
			}

			String setItemYn = "";
			List<Map<String, Object>> partItemList = new ArrayList<>();

			for (Map<String, Object> m : list) {
				Map<String, Object> setItemModel = new HashMap<>();
				setItemModel.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
				setItemModel.put("CUST_ID", (String) model.get("vrCustId"));
				setItemModel.put("RITEM_ID", (String) setPartRitemList.get(0).get("SET_RITEM_ID"));
				setItemModel.put("CUST_LOT_NO", (String) m.get("CUST_LOT_NO"));
				setItemModel.put("IN_REQ_DT", (String) setPartRitemList.get(0).get("WORK_DT"));
				
				setItemYn = WMSIF050Dao.getSetItemYn(setItemModel); // 임가공 상품 여부
				
				if ("Y".equals(setItemYn)) {
					partItemList = (List<Map<String, Object>>) WMSIF050Dao.getPartItemList(setItemModel); // PART 상품 정보 조회
				} else {
					setItemModel.put("ITEM_CODE", (String) setPartRitemList.get(0).get("SET_RITEM_CD"));
					setItemModel.putAll((Map<String, Object>)WMSIF050Dao.getUomId(setItemModel));
				}
				
				int checkStockCount = (Integer) WMSIF050Dao.getSerialStockLotNoCount(setItemModel); // 현재고 조회
				
				if(checkStockCount != 0){
					InterfaceUtil.isValidReturnCode("WMSTS010", "-1", "Already IN STOCK SERIAL : " + (String) m.get("CUST_LOT_NO"));
				}
				
				// 임가공 상품 주문 등록
				Map<String, Object> saveInOrderModel = getParamMapSaveInOrder(model, m, partItemList, serialInfo);
				saveInOrderModel = (Map<String, Object>) WMSIF050Dao.saveInOrder(saveInOrderModel); 
				InterfaceUtil.isValidReturnCode("WMSTS010", String.valueOf(saveInOrderModel.get("O_MSG_CODE")), (String) saveInOrderModel.get("O_MSG_NAME"));
				
				saveInOrderModel.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
				saveInOrderModel.put("ORD_ID", (String) saveInOrderModel.get("O_MSG_NAME"));
				List<Map<String, Object>> ordDetailList = (List<Map<String, Object>>) WMSIF050Dao.getOrdDetail(saveInOrderModel); 
				
				// 로케이션 조회
				saveInOrderModel.put("LOC_TYPE", "140");
				saveInOrderModel.putAll((Map<String, Object>) WMSIF050Dao.getLocIdByLocType(saveInOrderModel));
				
				int ordDetailSize = ordDetailList.size();
				Map<String, Object> workModel = new HashMap<>();
	
				for (Map<String, Object> ordDetail : ordDetailList) {
					workModel = getParamMapInitWork(model, ordDetail);
					WMSIF050Dao.initWork(workModel);
					
					workModel.put("LOC_ID", (String) saveInOrderModel.get("LOC_ID"));
					workModel.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
					workModel.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
					
					// 로케이션 지정
					workModel = getParamMapSaveLoc(workModel, ordDetail);
					workModel = (Map<String, Object>) WMSIF050Dao.saveLoc(workModel);
					
					// 입고 확정
					workModel = getParamMapSaveInComplete(model, ordDetail);
					WMSIF050Dao.saveInComplete(workModel);
				}
				
				InterfaceUtil.isValidReturnCode("WMSTS010", String.valueOf(workModel.get("O_MSG_CODE")), (String) workModel.get("O_MSG_NAME"));
				
				// 시리얼 이력 저장 (WMSTS010/WMSTR010/WMSTW010)
				Map<String, Object> saveHistoryModel = new HashMap<>();
				saveHistoryModel.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
				saveHistoryModel.put("WORK_ID", WMSIF050Dao.getWorkId(saveHistoryModel));
				saveHistoryModel.put("CUST_ID", (String) model.get("vrCustId"));
				saveHistoryModel.put("ORD_ID", ordDetailList.get(0).get("ORD_ID"));
				saveHistoryModel.put("WORK_QTY", 1);
				saveHistoryModel.put("RITEM_ID", setPartRitemList.get(0).get("SET_RITEM_ID"));
				saveHistoryModel.put("SET_YN", setItemYn);
				saveHistoryModel.put("CUST_LOT_NO", (String) m.get("CUST_LOT_NO"));
				saveHistoryModel.put("HIS_LC_ID", (String)WMSIF050Dao.getStockHistory(saveHistoryModel));
				saveHistoryModel.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
				
				saveSerialHistory(saveHistoryModel);
			}
			
			result.put("errCnt", 0);
			result.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
		return result;
	}
	
	/**
	 * @meethod getFilteredSerialInfo WMSTS010 현재 센터 외 다른 센터의 재고 조회
	 * @user wl2258
	 */
	private Map<String, Object> getFilteredSerialInfo(Map<String, Object> model, Map<String, Object> rowData) {
		Map<String, Object> param = new HashMap<>();
		
		param.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
		param.put("SERIAL_NO", (String) rowData.get("CUST_LOT_NO"));
		
		return dao.getFilteredSerialInfo(param);
	}
	
	/**
	 * @meethod saveSerialHistory 시리얼 이력 저장
	 * @user wl2258
	 */
	private void saveSerialHistory(Map<String, Object> saveHistoryModel) {
	    WMSIF050Dao.deleteSerialInventory(saveHistoryModel); 		// WMSTS010
	    WMSIF050Dao.insertSerialInventory(saveHistoryModel); 	
	        
	    WMSIF050Dao.insertSerialWorkHistory(saveHistoryModel);		// WMSTW010
	    WMSIF050Dao.insertSerialHistoryTracking(saveHistoryModel);	// WMSTR010
	}
	
	private Map<String, Object> getParamMapSaveInOrder(Map<String, Object> model, Map<String, Object> rowData, 
			List<Map<String, Object>> partItemList, Map<String, Object> serialInfo) {

		try {
			int partItemSize = (partItemList != null) ? partItemList.size() : 0;

			Map<String, Object> modelIns = new HashMap<String, Object>();

			modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
			modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
			
			modelIns.put("vrOrdId", null);
			modelIns.put("inReqDt", (String) serialInfo.get("WORK_DT"));
			modelIns.put("workStat", "100");
			modelIns.put("ordType", "01");
			modelIns.put("ordSubtype", "20");

			modelIns.put("dsMain_rowStatus", null);
			modelIns.put("transCustId", null);
			modelIns.put("vrCustId", (String) model.get("vrCustId"));
			modelIns.put("gvLcId", (String) model.get(ConstantIF.SS_SVC_NO));

			String[] dsSubRowStatus = new String[partItemSize];
			String[] ordSeq = new String[partItemSize];
			String[] ritemId = new String[partItemSize];
			String[] custLotNo = new String[partItemSize];
			String[] inWorkOrdQty = new String[partItemSize];
			String[] inOrdUomId = new String[partItemSize];
			String[] inWorkUomId = new String[partItemSize];

			if (partItemList != null && partItemSize > 0) {
				for (int i = 0; i < partItemSize; i++) {
					String qty = String.valueOf(partItemList.get(i).get("QTY"));
					String partRitemId = (String) partItemList.get(i).get("PART_RITEM_ID");
					String uomId = (String) partItemList.get(i).get("REP_UOM_ID");

					dsSubRowStatus[i] = "INSERT";
					ordSeq[i] = String.valueOf(i+1);
					ritemId[i] = partRitemId;
					custLotNo[i] = (String) rowData.get("CUST_LOT_NO");
					inWorkOrdQty[i] = qty;
					inOrdUomId[i] = uomId;
					inWorkUomId[i] = uomId;
				}

				modelIns.put("realOutQty", new String[partItemSize]);
				modelIns.put("makeDt", new String[partItemSize]);
				modelIns.put("timePeriodDay", new String[partItemSize]);
				modelIns.put("locYn", new String[partItemSize]);
				modelIns.put("pdaCd", new String[partItemSize]);

				modelIns.put("workYn", new String[partItemSize]);
				modelIns.put("rjType", new String[partItemSize]);
				modelIns.put("realPltQty", new String[partItemSize]);
				modelIns.put("realBoxQty", new String[partItemSize]);
				modelIns.put("confYn", new String[partItemSize]);

				modelIns.put("unitAmt", new String[partItemSize]);
				modelIns.put("amt", new String[partItemSize]);
				modelIns.put("eaCapa", new String[partItemSize]);
				modelIns.put("boxBarcode", new String[partItemSize]);
				modelIns.put("outOrdUomId", new String[partItemSize]);

				modelIns.put("outWorkUomId", new String[partItemSize]);
				modelIns.put("inOrdQty", new String[partItemSize]);
				modelIns.put("outOrdQty", new String[partItemSize]);
				modelIns.put("outWorkOrdQty", new String[partItemSize]);
				modelIns.put("refSubLotId", new String[partItemSize]);

				modelIns.put("dspId", new String[partItemSize]);
				modelIns.put("carId", new String[partItemSize]);
				modelIns.put("cntrId", new String[partItemSize]);
				modelIns.put("cntrNo", new String[partItemSize]);
				modelIns.put("cntrType", new String[partItemSize]);

				modelIns.put("badQty", new String[partItemSize]);
				modelIns.put("uomNm", new String[partItemSize]);
				modelIns.put("unitPrice", new String[partItemSize]);
				modelIns.put("whNm", new String[partItemSize]);
				modelIns.put("itemKorNm", new String[partItemSize]);

				modelIns.put("itemEngNm", new String[partItemSize]);
				modelIns.put("repUomId", new String[partItemSize]);
				modelIns.put("uomCd", new String[partItemSize]);
				modelIns.put("uomIf", new String[partItemSize]);
				modelIns.put("repUomCd", new String[partItemSize]);

				modelIns.put("repUomNm", new String[partItemSize]);
				modelIns.put("itemGrpId", new String[partItemSize]);
				modelIns.put("expiryDate", new String[partItemSize]);
				modelIns.put("inOrdWeight", new String[partItemSize]);
				modelIns.put("rtiNm", new String[partItemSize]);

				modelIns.put("itemBestDate", new String[partItemSize]);
				modelIns.put("itemBestDateEnd", new String[partItemSize]);
				modelIns.put("unitNo", new String[partItemSize]);
				modelIns.put("ordDesc", new String[partItemSize]);
				modelIns.put("validDt", new String[partItemSize]);

				modelIns.put("etc2", new String[partItemSize]);
				modelIns.put("realInQty", new String[partItemSize]);
			}

			modelIns.put("dsSub_rowStatus", dsSubRowStatus);
			modelIns.put("ordSeq", ordSeq);
			modelIns.put("ritemId", ritemId);
			modelIns.put("custLotNo", custLotNo);
			modelIns.put("inWorkOrdQty", inWorkOrdQty);
			modelIns.put("inOrdUomId", inOrdUomId);
			modelIns.put("inWorkUomId", inWorkUomId);

			modelIns.put("inDt", "");
			modelIns.put("custPoid", "");
			modelIns.put("custPoseq", "");
			modelIns.put("orgOrdId", "");
			modelIns.put("orgOrdSeq", "");

			modelIns.put("vrWhId", "");
			modelIns.put("outWhId", "");
			modelIns.put("pdaFinishYn", "");
			modelIns.put("blNo", "");
			modelIns.put("outReqDt", "");

			modelIns.put("outDt", "");
			modelIns.put("pdaStat", "");
			modelIns.put("workSeq", "");
			modelIns.put("capaTot", "");
			modelIns.put("kinOutYn", "");

			modelIns.put("carConfYn", "");
			modelIns.put("tplOrdId", "");
			modelIns.put("approveYn", "");
			modelIns.put("payYn", "");
			modelIns.put("inCustId", "");

			modelIns.put("inCustAddr", "");
			modelIns.put("inCustEmpNm", "");
			modelIns.put("inCustTel", "");

			return modelIns;
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
	}
	
	private Map<String, Object> getParamMapInitWork(Map<String, Object> model, Map<String, Object> ordDetail) {
    	try {
            String ordId = String.valueOf(ordDetail.get("ORD_ID"));
            String ordSeq = String.valueOf(ordDetail.get("ORD_SEQ"));
            String qty = String.valueOf(ordDetail.get("IN_ORD_QTY"));
            	

            String[] ORD_ID = new String[]{ordId};
            String[] ORD_SEQ = new String[]{ordSeq};
            String[] QTY = new String[]{qty};

            Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
			modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
            
            modelIns.put("workType", "20");
            modelIns.put("workSubtype", "01");
            modelIns.put("ordId", ORD_ID);
            modelIns.put("ordSeq", ORD_SEQ);
            modelIns.put("qty", QTY);
            
            return modelIns;
    	}  catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
        }
	}
	
	private Map<String, Object> getParamMapSaveLoc(Map<String, Object> workModel, Map<String, Object> ordDetail) {
		try {
	        String[] WORK_ID = new String[]{String.valueOf(workModel.get("O_WORK_ID"))};
			String[] WORK_SEQ = new String[]{"1"};
			String[] WORK_SUBSEQ = new String[]{""};
			String[] PDA_CD = new String[]{String.valueOf(ordDetail.get("PDA_CD"))};
			String[] EMPLOYEE_ID = new String[]{""};
			
			String[] LOC_ID = new String[]{String.valueOf(workModel.get("LOC_ID"))};
			String[] RITEM_ID = new String[]{String.valueOf(ordDetail.get("RITEM_ID"))};
			String[] WORK_QTY = new String[]{String.valueOf(ordDetail.get("IN_ORD_QTY"))};
			String[] UOM_ID = new String[]{String.valueOf(ordDetail.get("IN_ORD_UOM_ID"))};
			String[] CONF_QTY = new String[]{String.valueOf(ordDetail.get("IN_ORD_QTY"))};
			
			String[] SUB_LOT_ID = new String[]{""};
			String[] STOCK_ID = new String[]{""};
			String[] REAL_PLT_QTY = new String[]{String.valueOf(ordDetail.get("REAL_PLT_QTY"))};
			String[] REAL_BOX_QTY = new String[]{String.valueOf(ordDetail.get("REAL_BOX_QTY"))};
			String[] ITEM_DATE_END = new String[]{String.valueOf(ordDetail.get("ITEM_BEST_DATE_END"))};
	        

            Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("WORK_IP",  (String) workModel.get("WORK_IP"));
			modelIns.put("USER_NO", (String) workModel.get("USER_NO"));
            
            modelIns.put("workId", WORK_ID);
            modelIns.put("workSeq", WORK_SEQ);
            modelIns.put("workSubseq", WORK_SUBSEQ);
            modelIns.put("pdaCd", PDA_CD);
            modelIns.put("employeeId", EMPLOYEE_ID);
			
            modelIns.put("locId", LOC_ID);
            modelIns.put("ritemId", RITEM_ID);
            modelIns.put("workQty", WORK_QTY);
            modelIns.put("uomId", UOM_ID);
            modelIns.put("confQty", CONF_QTY);
			
            modelIns.put("subLotId", SUB_LOT_ID);
            modelIns.put("stockId", STOCK_ID);
            modelIns.put("realPltQty", REAL_PLT_QTY);
            modelIns.put("realBoxQty", REAL_BOX_QTY);
            modelIns.put("itemDateEnd", ITEM_DATE_END);
            
            return modelIns;
    	}  catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
        }
	}
	
    private Map<String, Object> getParamMapSaveInComplete(Map<String, Object> model, Map<String, Object> ordDetail) {
    	try {       
	        String[] ORD_ID = new String[]{String.valueOf(ordDetail.get("ORD_ID"))};
			String[] ORD_SEQ = new String[]{String.valueOf(ordDetail.get("ORD_SEQ"))};
			String[] REF_SUB_LOT_ID = new String[]{""};
			String[] WORK_QTY = new String[]{String.valueOf(ordDetail.get("IN_ORD_QTY"))};

            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("ordId", ORD_ID);
            modelIns.put("ordSeq", ORD_SEQ);
            modelIns.put("refSubLotId", REF_SUB_LOT_ID);
            modelIns.put("workQty", WORK_QTY);
            
            modelIns.put("LC_ID", (String) model.get(ConstantIF.SS_SVC_NO));
			modelIns.put("WORK_IP", (String) model.get(ConstantIF.SS_CLIENT_IP));
			modelIns.put("USER_NO", (String) model.get(ConstantIF.SS_USER_NO));
            
            return modelIns;
    	}  catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
        }}

}
