package com.logisall.winus.wmslb.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmslb.service.WMSLB010Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSLB010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSLB010Service")
	private WMSLB010Service service;

 
    /**
     * Method ID   : wmsmo020
     * Method 설명    : RFID입고 화면
     * 작성자               : chSong
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSLB010.action")
    public ModelAndView wmslb010(Map<String, Object> model){
        return new ModelAndView("winus/wmslb/WMSLB010");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
    
    /**
     * Method ID   : wmsmo020
     * Method 설명    : RFID입고 화면
     * 작성자               : chSong
     * @param model
     * @return
     */
    @RequestMapping("/WMSLB010pop.action")
    public ModelAndView wmslb010pop(Map<String, Object> model){
        return new ModelAndView("winus/wmslb/WMSLB010pop");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
    
    /*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSLB010/save.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.save(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : expirationDate
	 * Method 설명      : 유통기한 조회
	 * 작성자                 : khkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSLB010/expirationDate.action")
	public ModelAndView expirationDate(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.expirationDate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
}
