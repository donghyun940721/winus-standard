package com.logisall.winus.wmslb.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;


@Repository("WMSLB010Dao")
public class WMSLB010Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 입고RFID관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public void save(Map<String, Object> model) {
        executeInsert("wmslb010.insert", model);
    }
    

    /**
     * Method ID    : list
     * Method 설명      : 입고RFID관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public int check(Map<String, Object> model) {
        return (int)executeView("wmslb010.check", model);
    }


	public Map expirationDate(Map<String, Object> model) {
		return (Map)executeView("wmslb010.expirationDate", model);
	}
}
