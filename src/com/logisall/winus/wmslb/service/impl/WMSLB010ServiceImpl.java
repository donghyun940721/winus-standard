package com.logisall.winus.wmslb.service.impl;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmslb.service.WMSLB010Service;



@Service("WMSLB010Service")
public class WMSLB010ServiceImpl implements WMSLB010Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSLB010Dao")
    private WMSLB010Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 입고RFID관리  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        try{
        	int result = dao.check(model);
            if(result > 0){
            	map.put("errCnt", 1);
            }else{
            	
            	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            	dateFormat.setLenient(true);
            	
            	if(model.containsKey("vrSrchReqDtFrom") && !StringUtils.isNotEmpty(model.get("vrSrchReqDtFrom").toString())){ 
            		dateFormat.parse(model.get("vrSrchReqDtFrom").toString());
            		throw new Exception("[Valid Fail] : Check Date Format (YYYY-MM-DD)");
            	}
            	
            	if(model.containsKey("vrSrchReqDtTo") && !StringUtils.isNotEmpty(model.get("vrSrchReqDtTo").toString())){ 
            		dateFormat.parse(model.get("vrSrchReqDtTo").toString());
            		throw new Exception("[Valid Fail] : Check Date Format (YYYY-MM-DD)");
            	}
            	
            	
            	dao.save(model);
            	map.put("errCnt", 0);
            }             

        }
        catch(Exception e)
        {
        	map.put("errCnt", 1);
            map.put("MSG", e.getMessage() );
        }
    	 
       
        return map; 
    }
    
    @Override
    public Map<String, Object> expirationDate(Map<String, Object> model)
    		throws Exception {
    	Map<String, Object> map = dao.expirationDate(model);
    	//map.put("expirationDate", expirationDate);
    	return map;
    }
}
