package com.logisall.winus.wmslb.service;

import java.util.Map;

public interface WMSLB010Service {
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public Map<String, Object> expirationDate(Map<String, Object> model) throws Exception;
}
