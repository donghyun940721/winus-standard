package com.logisall.winus.wmspd.service;

import java.util.List;
import java.util.Map;


public interface WMSPD100Service {
	public Map<String, Object> initService(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
}
