package com.logisall.winus.wmspd.service;

import java.util.Map;

public interface WMSPD021Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception;
    public Map<String, Object> autoOrderInsert(Map<String, Object> model) throws Exception;
    public Map<String, Object> initChkLoc(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertWmsst560(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelE2(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listExcelE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel2E4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel3E4(Map<String, Object> model) throws Exception;

    public Map<String, Object> updateE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteE2(Map<String, Object> model) throws Exception;
}

