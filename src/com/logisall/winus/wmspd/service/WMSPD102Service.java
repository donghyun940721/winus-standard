package com.logisall.winus.wmspd.service;

import java.util.List;
import java.util.Map;


public interface WMSPD102Service {
	public Map<String, Object> initService(Map<String, Object> model) throws Exception;
    
	public Map<String, Object> listT01(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT02(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT03(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT04(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSPD103listE01(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSPD103listE02(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSPD103listE03(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSPD103listE04(Map<String, Object> model) throws Exception;
    public Map<String, Object> WMSPD103listE05(Map<String, Object> model) throws Exception;
    
}
