package com.logisall.winus.wmspd.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmspd.service.WMSPD101Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPD101Service")
public class WMSPD101ServiceImpl implements WMSPD101Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPD101Dao")
    private WMSPD101Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSPD101 = {"COMPANY_CD", "USER_GB", "USER_ID"};
    
    /**
     * 
     * 대체 Method ID	: initService
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> initService(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("USERLIST", dao.initServiceUserList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: save
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                
                modelDt.put("COMPANY_CD"    	, model.get("COMPANY_CD"+i));
                modelDt.put("USER_GB"    		, model.get("USER_GB"+i));
                modelDt.put("USER_ID"    		, model.get("USER_ID"+i));
                modelDt.put("USER_ID_WEB"		, model.get("USER_ID_WEB"+i));
                modelDt.put("USER_ID_MOBILE"	, model.get("USER_ID_MOBILE"+i));
                
                modelDt.put("SYS_REQ_ID", model.get(ConstantIF.SS_USER_NO));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSPD101);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                    strGubun = "D";
                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            
            //한번이라도 "D"면 삭제
            if (strGubun == "D")
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
            else
            	m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
	 * Method ID	: getMobileUserList
	 * Method 설명	: 
	 * 작성자			: 
	 * @param 
	 * @return
	 */
    public Map<String, Object> getMobileUserList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("MUSERLIST", dao.getMobileUserList(model));
		return map;
	}
}
