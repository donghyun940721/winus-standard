package com.logisall.winus.wmspd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPD021Dao")
public class WMSPD021Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectUom
     * Method 설명  : UOM 데이터셋
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model){
        return executeQueryForList("wmsms100.selectUom", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 세트상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspd021.list", model);
    }
    
    /**
     * Method ID  : listSub
     * Method 설명  : 구성상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmspd021.listSub", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 구성상품 등록
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmspd021.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 수정
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmspd021.update", model);
    }  
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 삭제
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmspd021.delete", model);
    }
    
    /**
     * Method ID    : autoOrderInsert
     * Method 설명      : 주문입력
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object autoOrderInsert(Map<String, Object> model){
        executeUpdate("wmspd021.PK_WMSOP030.SP_OUT_AUTO_ORDER_INSERT_DT", model);
        return model;
    }
    
    /**
     * Method ID  : initChkLoc
     * Method 설명  : initChkLoc
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object initChkLoc(Map<String, Object> model){
        return executeQueryForList("wmspd021.initChkLoc", model);
    }
    
    /**
     * Method ID    : insertWmsst560
     * Method 설명  : insertWmsst560
     * 작성자       : KSJ
     * @param model
     * @return
     */
	public Object insertWmsst560(Map<String, Object> model) {
		return executeUpdate("wmspd021.insertWmsst560", model);
	}
	
    /**
     * Method ID    : insertWmsst560_getWorkId
     * Method 설명  : insertWmsst560_getWorkId
     * 작성자       : KSJ
     * @param model
     * @return
     */
	public String insertWmsst560_getWorkId(Map<String, Object> model) {
		return (String) executeQueryForObject("wmspd021.insertWmsst560_getWorkId", model);
	}
	
    /**
     * Method ID  	: listE2
     * Method 설명  : 생산계획
     * 작성자       : KSJ
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmspd021.listE2", model);
    }
    
    /**
     * Method ID    : updateE2
     * Method 설명  : updateE2
     * 작성자       : KSJ
     * @param model
     * @return
     */
	public Object updateE2(Map<String, Object> model) {
		return executeUpdate("wmspd021.updateE2", model);
	}
	
    /**
     * Method ID    : deleteE2
     * Method 설명  : deleteE2
     * 작성자       : KSJ
     * @param model
     * @return
     */
	public Object deleteE2(Map<String, Object> model) {
		return executeUpdate("wmspd021.deleteE2", model);
	}
	
    /**
     * Method ID  	: listE2Excel
     * Method 설명  : 생산계획
     * 작성자       : KSJ
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listE2Excel(Map<String, Object> model) {
        return executeQueryPageWq("wmspd021.listE2Excel", model);
    }

    /**
     * Method ID : sublist
     * Method 설명 : 현재고 서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model){
        return executeQueryPageWq("wmspd021.sublist", model);
    }

     /**
     * Method ID : sublist
     * Method 설명 : 현재고 서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublistExcel3(Map<String, Object> model){
        return executeQueryWq("wmspd021.sublistExcel", model);
    }

    /**
     * Method ID  : listE4
     * Method 설명  : 현재고 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listE4(Map<String, Object> model) {
        return executeQueryPageWq("wmspd021.listE4", model);
    }
    
    
}
