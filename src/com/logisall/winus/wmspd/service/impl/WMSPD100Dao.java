package com.logisall.winus.wmspd.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPD100Dao")
public class WMSPD100Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID	: initServiceUserList 
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
     public Object initServiceUserList(Map<String, Object> model){
     	return executeQueryForList("wmspd100.initServiceUserList", model);
     }
     
	/**
	 * Method ID	: list 
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmspd100.workedList", model);
	}
	
	/**
	 * Method ID	: insert 
	 * Method 설명	:  
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmspd100.insert", model);
	}

	/**
	 * Method ID	: update 
	 * Method 설명	:  
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmspd100.update", model);
	}

	/**
	 * Method ID	: delete 
	 * Method 설명	:  
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmspd100.delete", model);
	}
	
	/*-
     * Method ID : checkExistEmptyRow
     * Method 설명 : 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistEmptyRow(Map<String, Object> model) {
        return (String)executeView("wmspd100.checkExistEmptyRow", model);
    }
    
    /*-
     * Method ID : checkExistEmptyData
     * Method 설명 : 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistEmptyData(Map<String, Object> model) {
        return (String)executeView("wmspd100.checkExistEmptyData", model);
    }
    
	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : 대용량등록시 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if ((paramMap.get("USER_ID") != null && StringUtils.isNotEmpty(paramMap.get("USER_ID").toString()))
				&&  (paramMap.get("WORK_DT") != null && StringUtils.isNotEmpty(paramMap.get("WORK_DT").toString()))) {
					
					paramMap.put("COMPANY_CD", "SAEROPNL");
					paramMap.put("SYS_REQ_ID", (String)model.get(ConstantIF.SS_USER_NO));
					
					String checkExistData = checkExistEmptyData(paramMap);
    				if (checkExistData == null && StringUtils.isEmpty(checkExistData)) {
    					sqlMapClient.insert("wmspd100.insert", paramMap);
                    }else{
                    	sqlMapClient.update("wmspd100.update", paramMap);
                    }
				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
}