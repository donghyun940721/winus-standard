package com.logisall.winus.wmspd.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPD101Dao")
public class WMSPD101Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID	: initServiceUserList 
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
     public Object initServiceUserList(Map<String, Object> model){
     	return executeQueryForList("wmspd101.initServiceUserList", model);
     }
     
	/**
	 * Method ID	: list 
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmspd101.workedList", model);
	}
	
	/**
	 * Method ID	: insert 
	 * Method 설명	:  
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmspd101.insert", model);
	}

	/**
	 * Method ID	: update 
	 * Method 설명	:  
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmspd101.update", model);
	}

	/**
	 * Method ID	: delete 
	 * Method 설명	:  
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmspd101.delete", model);
	}
	
	/**
     * Method ID	: getMobileUserList
     * Method 설명	: 
     * 작성자			: 
     * @param model
     * @return
     */
    public Object getMobileUserList(Map<String, Object> model){
        return executeQueryForList("wmspd101.getMobileUserList", model);
    }
}