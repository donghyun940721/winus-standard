package com.logisall.winus.wmspd.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmspd.service.WMSPD010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPD010Service")
public class WMSPD010ServiceImpl implements WMSPD010Service{
    protected Log log = LogFactory.getLog(this.getClass());

    private final static String[] CHECK_VALIDATE_WMSPD010 = {"SET_RITEM_ID", "PART_RITEM_ID"};
    
    @Resource(name = "WMSPD010Dao")
    private WMSPD010Dao dao;
    
    
    /**
     * 대체 Method ID    : list
     * 대체 Method 설명      : 세트상품 화면 데이타셋
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("UOM", dao.selectUom(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    : list
     * 대체 Method 설명      : 세트상품 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    : listSub
     * 대체 Method 설명      : 구성상품 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {        	
        	String[] curData;
        	curData = new String[Integer.parseInt(model.get("selectIds").toString())];

        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                curData[i] = (String)model.get("CUR_DATA"+i);
            }
        	
            model.put("curData", curData);
            
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.listSub(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }    
    
    /**
     * 
     * 대체 Method ID   : saveSub
     * 대체 Method 설명    : 구성상품 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("selectIds"     , model.get("selectIds"));
                modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN"+i));
                modelDt.put("UOM_ID"        , model.get("UOM_ID"+i));           //화면필수
                modelDt.put("QTY"           , model.get("QTY"+i));              //화면필수
                modelDt.put("SET_RITEM_ID"  , model.get("SET_RITEM_ID"+i));     //db필수       //유효성체크
                modelDt.put("PART_RITEM_ID" , model.get("PART_RITEM_ID"+i));    //화면,db필수   //유효성체크
                modelDt.put("SET_SEQ"       , model.get("SET_SEQ"+i));          //db필수(insert시 생성)
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSPD010);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listSubExcel
     * 대체 Method 설명 : 구성상품 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSubExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listSub(model));
        
        return map;
    }
       
    /**
     * Method ID    : getSampleExcelDown
     * Method 설명      : 엑셀 샘플 다운
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Map<String, Object> > sample = new ArrayList<Map<String, Object>>();
        int colSize = Integer.parseInt(model.get("Col_Size").toString());
        int rowSize = Integer.parseInt(model.get("Row_Size").toString());
        
        for(int k = 0 ; k < rowSize ; k++){
        	if(k == 0){
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
                    sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
                    sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}else{
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
        			sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
        			sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}
        }
        Map<String, Object> sampleRow = new HashMap<String, Object>();
        sampleRow.put("rowSize", rowSize);
        
        map.put("LIST", getExcelSampleList(sample, sampleRow));
        return map;
    }
    
    private GenericResultSet getExcelSampleList(List<Map<String, Object>> list, Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		int pageIndex = 1;
		int pageSize = 1;
		int pageTotal = 1;
		int pageBlank = (int) Math.ceil(pageTotal / (double) pageSize);
		int rowSize = Integer.parseInt(model.get("rowSize").toString());
		
		List sampleList = new ArrayList<Map<String, Object>>();
		
		for(int k = 0 ; k < rowSize ; k++){
			Map<String, Object> sample = new HashMap<String, Object>();
			for (Map<String, Object> sampleInfo : list) {	
				Object key = sampleInfo.get(k+"_sampleCol");
				if (key != null && StringUtils.isNotEmpty(key.toString())) {
					sample.put(key.toString(), sampleInfo.get(k+"_sampleRow"));
				}
			}
			sampleList.add(sample);
		}
		
		wqrs.setCpage(pageIndex);
		wqrs.setTpage(pageBlank);
		wqrs.setTotCnt(pageTotal);
		wqrs.setList(sampleList);
		return wqrs;
	}
}
