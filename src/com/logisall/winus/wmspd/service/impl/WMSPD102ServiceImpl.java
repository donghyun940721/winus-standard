package com.logisall.winus.wmspd.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmspd.service.WMSPD102Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPD102Service")
public class WMSPD102ServiceImpl implements WMSPD102Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPD102Dao")
    private WMSPD102Dao dao;
    
    /**
     * 
     * 대체 Method ID	: initService
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> initService(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("USERLIST", dao.initServiceUserList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listT01
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listT01(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listT01(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listT02
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listT02(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listT02(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listT03
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listT03(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listT03(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listT04
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listT04(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listT04(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE01
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> WMSPD103listE01(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.WMSPD103listE01(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID	: listE01
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> WMSPD103listE02(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.WMSPD103listE02(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID	: listE01
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> WMSPD103listE03(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.WMSPD103listE03(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID	: listE01
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> WMSPD103listE04(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.WMSPD103listE04(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID	: listE01
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> WMSPD103listE05(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.WMSPD103listE05(model));
        return map;
    }
}
