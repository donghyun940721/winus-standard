package com.logisall.winus.wmspd.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPD102Dao")
public class WMSPD102Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID	: initServiceUserList 
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
     public Object initServiceUserList(Map<String, Object> model){
     	return executeQueryForList("wmspd102.initServiceUserList", model);
     }
     
	/**
	 * Method ID	: listT01
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listT01(Map<String, Object> model) {
		return executeQueryPageWq("wmspd102.listT01", model);
	}
	
	/**
	 * Method ID	: listT02
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listT02(Map<String, Object> model) {
		return executeQueryPageWq("wmspd102.listT02", model);
	}
	
	/**
	 * Method ID	: listT03
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listT03(Map<String, Object> model) {
		return executeQueryPageWq("wmspd102.listT03", model);
	}
	
	/**
	 * Method ID	: listT04
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listT04(Map<String, Object> model) {
		return executeQueryPageWq("wmspd102.listT04", model);
	}
	
	/**
	 * Method ID	: listT04
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet WMSPD103listE01(Map<String, Object> model) {
		return executeQueryWq("wmspd102.WMSPD103listE01", model);
	}

	/**
	 * Method ID	: listT04
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet WMSPD103listE02(Map<String, Object> model) {
		return executeQueryWq("wmspd102.WMSPD103listE02", model);
	}

	/**
	 * Method ID	: listT04
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet WMSPD103listE03(Map<String, Object> model) {
		return executeQueryWq("wmspd102.WMSPD103listE03", model);
	}

	/**
	 * Method ID	: listT04
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet WMSPD103listE04(Map<String, Object> model) {
		return executeQueryWq("wmspd102.WMSPD103listE04", model);
	}

	/**
	 * Method ID	: listT04
	 * Method 설명	: 
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet WMSPD103listE05(Map<String, Object> model) {
		return executeQueryWq("wmspd102.WMSPD103listE05", model);
	}
}