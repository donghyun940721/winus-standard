package com.logisall.winus.wmspd.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPD020Dao")
public class WMSPD020Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectUom
     * Method 설명  : UOM 데이터셋
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model){
        return executeQueryForList("wmsms100.selectUom", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 세트상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspd020.list", model);
    }
    
    /**
     * Method ID  : listSub
     * Method 설명  : 구성상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmspd020.listSub", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 구성상품 등록
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmspd020.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 수정
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmspd020.update", model);
    }  
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 삭제
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmspd020.delete", model);
    }
    
    /**
     * Method ID    : autoOrderInsert
     * Method 설명      : 주문입력
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object autoOrderInsert(Map<String, Object> model){
        executeUpdate("wmspd020.PK_WMSOP030.SP_OUT_AUTO_ORDER_INSERT_DT", model);
        return model;
    }
    
    /**
     * Method ID  : initChkLoc
     * Method 설명  : initChkLoc
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object initChkLoc(Map<String, Object> model){
        return executeQueryForList("wmspd020.initChkLoc", model);
    }
}
