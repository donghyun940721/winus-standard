package com.logisall.winus.wmspd.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmspd.service.WMSPD100Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPD100Service")
public class WMSPD100ServiceImpl implements WMSPD100Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPD100Dao")
    private WMSPD100Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSPD100 = {"COMPANY_CD", "WORK_DT", "USER_ID"};
    
    /**
     * 
     * 대체 Method ID	: initService
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> initService(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("USERLIST", dao.initServiceUserList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: save
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                
                modelDt.put("COMPANY_CD"    	, model.get("COMPANY_CD"+i));
                modelDt.put("WORK_DT"    		, model.get("WORK_DT"+i));
                modelDt.put("USER_ID"    		, model.get("USER_ID"+i));
                
                modelDt.put("T_IN_IBHA"			, model.get("T_IN_IBHA"+i));
                modelDt.put("T_IN_JEOGCHI"		, model.get("T_IN_JEOGCHI"+i));
                modelDt.put("T_IN_BOCHUNG"		, model.get("T_IN_BOCHUNG"+i));
                modelDt.put("T_IN_BANPUM"		, model.get("T_IN_BANPUM"+i));
                
                modelDt.put("T_OUT_PIKING"		, model.get("T_OUT_PIKING"+i));
                modelDt.put("T_OUT_GEOMSU"		, model.get("T_OUT_GEOMSU"+i));
                modelDt.put("T_OUT_POJANG"		, model.get("T_OUT_POJANG"+i));
                modelDt.put("T_OUT_CHULHA"		, model.get("T_OUT_CHULHA"+i));
                
                modelDt.put("T_ETC_JEONSAN"		, model.get("T_ETC_JEONSAN"+i));
                modelDt.put("T_ETC_BYEONGYEONG"	, model.get("T_ETC_BYEONGYEONG"+i));
                modelDt.put("T_ETC_JAEGO"		, model.get("T_ETC_JAEGO"+i));
                modelDt.put("T_ETC_SOBUN"		, model.get("T_ETC_SOBUN"+i));
                
                modelDt.put("SYS_REQ_ID", model.get(ConstantIF.SS_USER_NO));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSPD100);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                    strGubun = "D";
                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            
            //한번이라도 "D"면 삭제
            if (strGubun == "D")
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
            else
            	m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀업로드 저장
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
        String rtnMsg = "등록된 생산성ID(이름)가 아닙니다. : ";
        
            try{
            	Map<String, Object> paramMap = null;
            	for (int i = 0; i < list.size(); i++) {
    				paramMap = (Map) list.get(i);

    				String checkExistData = dao.checkExistEmptyRow(paramMap);
    				if (checkExistData == null && StringUtils.isEmpty(checkExistData)) {
    					rtnMsg += paramMap.get("USER_ID") + ",";
                    	errCnt++;
                    }
    				
    			}
            	
            	if(errCnt == 0){
            		dao.saveUploadData(model, list);
            		System.out.println(">> 결과 : 문제없음.");
            		m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                    m.put("MSG_ORA", "");
                    m.put("errCnt", errCnt);
            	}else{
            		System.out.println(">> 결과 : 잘못된 데이터가 있습니다.");
                    m.put("MSG", MessageResolver.getMessage("save.error"));
        			m.put("MSG_ORA", "Exception:"+rtnMsg);
        			m.put("errCnt", "1");
            	}
            } catch(Exception e){
                throw e;
            }
        return m;
    }
}
