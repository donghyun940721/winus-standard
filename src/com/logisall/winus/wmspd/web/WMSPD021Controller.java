package com.logisall.winus.wmspd.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmspd.service.WMSPD021Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSPD021Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPD021Service")
	private WMSPD021Service service;

	/*-
	 * Method ID    : wmspd020
	 * Method 설명      : 세트상품구성정보 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSPD021.action")
	public ModelAndView wmspd020(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmspd/WMSPD021", service.selectData(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 세트상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD021/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listSub
	 * Method 설명      : 구성상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD021/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveSub
	 * Method 설명      : 구성상품 저장수정
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD021/saveSub.action")
	public ModelAndView saveItem(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD021/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listSubExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to Excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("상품코드")   , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("수량")      , "2", "2", "0", "0", "100"},
                                   {"UOM"                               , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("생성일")     , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("생성자")     , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("수정일")     , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("수정자")     , "7", "7", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"ITEM_CODE"        , "S"},
                                    {"PART_RITEM_NM"    , "S"},
                                    {"QTY"              , "N"},
                                    {"UOM_ID"           , "S"},
                                    {"REG_DT"           , "S"},
                                    {"REG_NM"           , "S"},
                                    {"UPD_DT"           , "S"},
                                    {"UPD_NM"           , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("세트상품구성정보");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD021/sampleExcelDown.action")
	public void sampleExcelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getSampleExcelDown(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doSampleExcelDown2(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doSampleExcelDown2(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				String nName = MessageResolver.getText((String) model.get("Name_" + i)).replace("&lt;br&gt;", "").replace("&lt;/font&gt;&lt;br&gt;", "").replace("&lt;/font&gt;", "").replace("&lt;font color=&quot;#B1B1B1&quot;&gt;", "");
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{nName, i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
				valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
			}
			// 파일명
			String fileName = MessageResolver.getText("생산관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : autoOrderInsert.action
	 * Method 설명      : 주문입력
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD021/ordSave.action")
	public ModelAndView autoOrderInsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoOrderInsert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 화주 조회 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD021Q1.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmspd/WMSPD021Q1");
	}
	
	/*-
	 * Method ID : 
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSPD021E3.action")
	public ModelAndView wmspd020E3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmspd/WMSPD021E3");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD021E3/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int colSize = Integer.parseInt((String) model.get("colSize"));
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}

			int startRow = Integer.parseInt((String) model.get("startRow"));

			//List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 1000, 0);
			List list = ExcelReader.excelLimitRowReadByHandler(destination, cellName, 0, startRow, 1000, 0);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("LIST", list);

			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", map);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSPD021/initChkLoc.action")
	public ModelAndView initChkLoc(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.initChkLoc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : insertWmsst560
	 * Method 설명  : 주문입력
	 * 작성자       : 기드온
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD021/work.action")
	public ModelAndView insertWmsst560(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.insertWmsst560(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    	: listE2
	 * Method 설명      : 생산계획 조회...............................
	 * 작성자           : KSJ.............
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD021/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listExcelE2
	 * Method 설명  : 엑셀다운로드
	 * 작성자       : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD021/excelE2.action")
	public void listExcelE2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelE2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to Excel :", e);
			}
		}
	}
	protected void doExcelDownE2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("생산ID")   	, "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("생산SEQ")     	, "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("생산타입")     , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")     , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     	, "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("계획수량")     , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("완료수량")     , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("생성일")     	, "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("생성자")     	, "8", "8", "0", "0", "100"},
                                   {MessageResolver.getText("수정일")     	, "9", "9", "0", "0", "100"},
                                   {MessageResolver.getText("수정자")     	, "10", "10", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_ID"        	, "S"},
                                    {"WORK_SEQ"    		, "S"},
                                    {"WORK_TYPE"        , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    {"REQ_WORK_QTY"     , "N"},
                                    {"COM_WORK_QTY"     , "N"},
                                    {"REG_DT"           , "S"},
                                    {"REG_NM"           , "S"},
                                    {"UPD_DT"           , "S"},
                                    {"UPD_NM"           , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("세트상품구성정보");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * 
	 * Method ID    : updateE2
	 * Method 설명  : 생산 계획 데이터 수정
	 * 작성자       : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD021/updateE2.action")
	public ModelAndView updateE2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * 
	 * Method ID    : updateE2
	 * Method 설명  : 생산 계획 데이터 삭제
	 * 작성자       : KSJ
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD021/deleteE2.action")
	public ModelAndView deleteE2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcelE4
	 * Method �꽕紐� : �쁽�옱怨� �뿊���떎�슫
	 * �옉�꽦�옄 : 湲곕뱶�삩
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPD021/excelE4.action")
	public void listExcelE4(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelE4(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				String svcNo = String.valueOf(model.get("SS_SVC_NO"));
				// 대화물류
				if(svcNo.equals("0000003541") || svcNo.equals("0000003721") || svcNo.equals("0000003722") || svcNo.equals("0000003720")){
					this.doExcelDown_DH(response, grs);
				}else if(svcNo.equals("0000003620")){
					// 비즈컨설팅
					this.doExcelDown_Biz(response, grs);
				} else{
					this.doExcelDownE4(response, grs);
				}
				
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	/*-
	 * Method ID : listExcel2
	 * Method 설명 : 현재고 상세 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPD021/excel2E4.action")
	public void listExcel2E4(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel2E4(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown3(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	/*-
	 * Method ID : listExcel3
	 * Method 설명 : 현재고 상세 전체엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPD021/excel3E4.action")
	public void listExcel3E4(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel3E4(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown3(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	protected void doExcelDownE4(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")			, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")			, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품바코드")		, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")			, "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("전일재고")		, "6", "6", "0", "0", "200"},
								   {MessageResolver.getMessage("입고")			, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고")			, "8", "8", "0", "0", "200"},
								   {MessageResolver.getMessage("보관재고")		, "9", "9", "0", "0", "200"},
								   {MessageResolver.getMessage("리패킹재고")		, "10", "10", "0", "0", "200"},

                                   //{MessageResolver.getMessage("크로스도킹")	, "9", "9", "0", "0", "200"},
                                   //{MessageResolver.getMessage("이벤트출고")	, "10", "10", "0", "0", "200"},
                                   
                                   //{MessageResolver.getMessage("반품출고")		, "11", "11", "0", "0", "200"},
                                   //{MessageResolver.getMessage("정상재고")		, "12", "12", "0", "0", "200"},
                                   //{MessageResolver.getMessage("BOX수량")		, "13", "14", "0", "0", "200"},
                                   //{MessageResolver.getMessage("PLT수량")		, "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고")		, "11", "11", "0", "0", "200"},

                                   {MessageResolver.getMessage("출고가용재고")	, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("전체재고")      , "13", "13", "0", "0", "200"}
                                   //{MessageResolver.getMessage("AS재고")		, "17", "17", "0", "0", "200"},
                                   //{MessageResolver.getMessage("격리수량")		, "18", "18", "0", "0", "200"},
                                   //{MessageResolver.getMessage("금액")		, "19", "19", "0", "0", "200"},
                                   
                                   //{MessageResolver.getMessage("중량")		, "20", "20", "0", "0", "200"},
                                   //{MessageResolver.getMessage("UOM")		, "21", "21", "0", "0", "200"},
                                   
                                   //{MessageResolver.getMessage("적정재고")		, "22", "22", "0", "0", "200"},
                                   //{MessageResolver.getMessage("비고")		, "23", "23", "0", "0", "200"},
                                   //{MessageResolver.getMessage("브렌드")		, "24", "24", "0", "0", "200"},
                                   //{MessageResolver.getMessage("화주상품코드")	, "25", "25", "0", "0", "200"},
                                   //{MessageResolver.getMessage("사이즈")		, "26", "26", "0", "0", "200"},
                                   //{MessageResolver.getMessage("CBM")		, "27", "27", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"ITEM_BAR_CD"          , "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    {"IN_QTY"            	, "N"},
									{"OUT_QTY"           	, "N"},
									{"KEEP_QTY"           	, "N"},
									{"REPACKING_QTY"        , "N"},
                                    //{"CROSS_QTY"         	, "N"},
                                    //{"EVENT_QTY"         	, "N"},
                                    
                                    //{"RETURN_QTY"        	, "N"},
                                    //{"STOCK_QTY"        	, "N"},
                                    //{"BOX_QTY"           	, "N"},
                                    //{"PLT_QTY"           	, "N"},

                                    {"BAD_QTY"           	, "N"},
									{"FTA_QTY"           	, "N"},
									{"STOCK_TOTAL_QTY"      , "N"},

                                    //{"OUT_ABLE_QTY"         , "N"},
                                    //{"AS_QTY"            	, "N"},
                                    //{"ISO_QTY"            	, "N"},
                                    //{"TOTAL_PRICE"        	, "N"},
                                    //{"STOCK_WEIGHT"         , "N"},
                                    //{"UOM_NM"            	, "S"},
                                    
                                    //{"PROP_QTY"             , "N"},
                                    //{"REMARK"               , "S"},
                                    //{"MAKER_NM"             , "S"},
                                    //{"CUST_ITEM_CD"         , "S"},
                                    //{"ITEM_SIZE"            , "S"},
                                    //{"CBM_QTY"            	, "N"}
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	protected void doExcelDown_DH(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
								   {MessageResolver.getMessage("박스바코드")	, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입수")		, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("P/L")		, "7", "7", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("전일재고")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고")		, "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("일반출고")		, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("크로스도킹")	, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("이벤트출고")	, "12", "12", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("반품출고")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("정상재고")		, "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("BOX수량")		, "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")		, "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고")		, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("전체재고")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("AS재고")		, "20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("격리수량")		, "21", "21", "0", "0", "200"},
                                   {MessageResolver.getMessage("중량")		, "22", "22", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM")		, "23", "23", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("적정재고")		, "24", "24", "0", "0", "200"},
                                   {MessageResolver.getMessage("비고")		, "25", "25", "0", "0", "200"},
                                   {MessageResolver.getMessage("브렌드")		, "26", "26", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주상품코드")	, "27", "27", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "28", "28", "0", "0", "200"},
                                   {MessageResolver.getMessage("CBM")		, "29", "29", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
									{"BOX_BAR_CD"          	, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"UNIT_NM"  	, "N"},
                                    {"PLT_UOM"  	, "N"},
                                    
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    {"IN_QTY"            	, "N"},
                                    {"OUT_QTY"           	, "N"},
                                    {"CROSS_QTY"         	, "N"},
                                    {"EVENT_QTY"         	, "N"},
                                    
                                    {"RETURN_QTY"        	, "N"},
                                    {"STOCK_QTY"        	, "N"},
                                    {"BOX_QTY"           	, "N"},
                                    {"PLT_QTY"           	, "N"},
                                    {"BAD_QTY"           	, "N"},
                                    {"STOCK_TOTAL_QTY"      , "N"},	
                                    {"OUT_ABLE_QTY"         , "N"},
                                    {"AS_QTY"            	, "N"},
                                    {"ISO_QTY"            	, "N"},
                                    {"STOCK_WEIGHT"         , "N"},
                                    {"UOM_NM"            	, "S"},
                                    
                                    {"PROP_QTY"             , "N"},
                                    {"REMARK"               , "S"},
                                    {"MAKER_NM"             , "S"},
                                    {"CUST_ITEM_CD"         , "S"},
                                    {"ITEM_SIZE"            , "S"},
                                    {"CBM_QTY"            	, "N"}
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID : doExcelDown3
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown3(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자")	, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션")	, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")	, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")	, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고수량")	, "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UOM")	, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량")	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("B/L번호")	, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고")	, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호")	, "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("제품등급")	, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자")	, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("유효기간만료일")	, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("무게")	, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("UNIT번호")	, "14", "14", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"			, "S"},
                                    {"LOC_NM"			, "S"},
                                    {"RITEM_CD"			, "S"},
                                    {"RITEM_NM"			, "S"},
                                    {"STOCK_QTY"		, "N"},
                                    
                                    {"UOM_NM"			, "S"},
                                    {"REAL_PLT_QTY"		, "N"},
                                    {"BL_NO"			, "S"},
                                    {"WH_NM"			, "S"},
                                    {"CUST_LOT_NO"		, "S"},
                                    
                                    {"ITEM_CLASS"		, "S"},
                                    {"MAKE_DT"			, "S"},
                                    {"ITEM_BEST_DATE_END"		, "S"},
                                    {"STOCK_WEIGHT"		, "N"},
                                    {"UNIT_NO"		, "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("현재고조회상세");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            //wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }

	protected void doExcelDown_Biz(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품군")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("박스바코드")	, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("스타일")		, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("컬러")		    , "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("사이즈")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("전일재고")		, "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("입고")		    , "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고")		    , "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("정상재고")		, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고가용재고")	, "14", "14", "0", "0", "200"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"BOX_BAR_CD"          	, "S"},
                                    {"CUST_LEGACY_ITEM_CD"  , "S"},
                                    {"COLOR"          		, "S"},
                                    {"ITEM_SIZE"          	, "S"},
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    
                                    {"IN_QTY"            	, "N"},
                                    {"OUT_QTY"           	, "N"},
                                    {"STOCK_QTY"        	, "N"},
                                    {"BAD_QTY"           	, "N"},
                                    {"OUT_ABLE_QTY"         , "N"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
