package com.logisall.winus.wmspm.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPM091Dao")
public class WMSPM091Dao extends SqlMapAbstractDAO{
    
	/**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model) {
	return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    /**
     * Method ID  : list
     * Method 설명  : 상품군 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspm091.list", model);
    }
      
    /**
     * Method ID    : insert
     * Method 설명      : 상품군 등록
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmspm091.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 상품군 수정
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmspm091.update", model);
    }  
    
    /**
     * Method ID    : delete
     * Method 설명      : 상품군 삭제
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmspm091.delete", model);
    }
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }	    
}
