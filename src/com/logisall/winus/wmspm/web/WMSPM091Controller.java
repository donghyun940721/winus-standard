package com.logisall.winus.wmspm.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmspm.service.WMSPM091Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.winus.frm.common.util.FSUtil;

/**
 * @author iskim
 *
 */
@Controller
public class WMSPM091Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPM091Service")
    private WMSPM091Service service;    

	/*-
	 * Method ID : wmspm091 
	 * Method 설명 : 상품군 화면 
	 * 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSPM091.action")
	public ModelAndView wmspm091(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmspm/WMSPM091", service.selectData(model));
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 상품군 조회
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM091/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save 
	 * Method 설명 : 상품군 저장 
	 * 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM091/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 엑셀정보 취득 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPM091/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                    {MessageResolver.getText("포장STEP순번")	, "0", "0", "0", "0", "100"}
                                   ,{MessageResolver.getText("프린트수량")		, "1", "1", "0", "0", "100"}
                                   ,{MessageResolver.getText("상품수량")		, "2", "2", "0", "0", "100"}
                                   ,{MessageResolver.getText("등록일")			, "3", "3", "0", "0", "100"}
                                   ,{MessageResolver.getText("등록자")			, "4", "4", "0", "0", "100"}
                                   
                                   ,{MessageResolver.getText("입출고타입")		, "5", "5", "0", "0", "100"}
                                   ,{MessageResolver.getText("비용")			, "6", "6", "0", "0", "100"}
                                   ,{MessageResolver.getText("통화명")			, "7", "7", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                     {"PACKING_STEP_SEQ"	, "S"}
                                    ,{"PRINT_COUNT"			, "S"}
                                    ,{"PRINT_QTY"    		, "S"}
                                    ,{"REG_DT"     			, "S"}
                                    ,{"REG_NO"     			, "S"}
                                    
                                    ,{"ORD_TYPE"     		, "S"}
                                    ,{"COST"     			, "S"}
                                    ,{"CURRENCY_NAME"     	, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("포장단계구성관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
