package com.logisall.winus.ulndev;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

@RestController
@RequestMapping("/task")
public class TaskController {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "TaskService")
	private TaskService service;
	
	@RequestMapping(value = "/getRequestList.action", method=RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> getRequestList(HttpServletResponse response, @RequestParam Map<String,Object> map) throws Exception {
		Map<String,Object> res = service.getRequestList(map);
		System.out.println(ManagementFactory.getRuntimeMXBean().getName());	
		return res;
	}
	
	@RequestMapping(value = "/postRequest.action", method=RequestMethod.POST)
	@ResponseBody
	public Boolean postRequest(@RequestBody Map<String,String> map) throws Exception {
		Boolean res = service.postRequest(map);
		return res;
	}
	
	@RequestMapping(value = "/putRequest.action", method=RequestMethod.PUT)
	@ResponseBody
	public Boolean putRequest(@RequestBody Map<String,Object> map) throws Exception {
		Boolean res = service.putRequest(map);
		return res;
	}
	
	@RequestMapping(value = "/deleteRequest.action", method=RequestMethod.DELETE)
	@ResponseBody
	public Boolean deleteRequest(@RequestBody Map<String,Object> map) throws Exception {
		Boolean res = service.deleteRequest(map);
		return res;
	}
	
	
	@RequestMapping(value = "/getProcessList.action", method=RequestMethod.GET)
	@ResponseBody
	public List<Map<String,String>> getProcessList(@RequestParam Map<String,Object> map) throws Exception {
		List<Map<String,String>> res = service.getProcessList(map);
		return res;
	}
	
	@RequestMapping(value = "/postProcess.action", method=RequestMethod.POST)
	@ResponseBody
	public Boolean postProcess(@RequestBody Map<String,String> map) throws Exception {
		Boolean res = service.postProcess(map);
		return res;
	}
	
	@RequestMapping(value = "/putProcess.action", method=RequestMethod.PUT)
	@ResponseBody
	public Boolean putProcess(@RequestBody Map<String,Object> map) throws Exception {
		Boolean res = service.putProcess(map);
		return res;
	}
	
	@RequestMapping(value = "/deleteProcess.action", method=RequestMethod.DELETE)
	@ResponseBody
	public Boolean deleteProcess(@RequestBody Map<String,Object> map) throws Exception {
		Boolean res = service.deleteProcess(map);
		return res;
	}
	
	
	@RequestMapping(value = "/deleteSelectList.action", method=RequestMethod.DELETE)
	@ResponseBody
	public Boolean deleteSelectList(@RequestBody List<Map<String,Object>> mapList) throws Exception {
		Boolean res = service.deleteSelectList(mapList);
		return res;
	}
	
	@RequestMapping(value = "/putRequestAndProcess.action", method=RequestMethod.PUT)
	@ResponseBody
	public Boolean putRequestAndProcess (@RequestBody Map<String,Object> map) throws Exception {
		try{
			Map<String,Object>requestMap = (Map<String, Object>) map.get("requestRowData");
			Map<String,Object>processMap = (Map<String, Object>) map.get("processRowData");
			Boolean resProcess = service.putProcess(processMap);
			Boolean resRequest = service.putRequest(requestMap);
			
			return resRequest && resProcess;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	@RequestMapping(value = "/getExcelTableList.action", method=RequestMethod.GET)
	@ResponseBody
	public void getExcelTableList (HttpServletResponse response, @RequestParam Map<String,Object> map) throws Exception {
		BufferedOutputStream bos = null;
		SXSSFWorkbook sxssfWb = null;
		
		try{
			List<Map<String,String>> res = getExcelMapList(map);
			
			sxssfWb = getSXSSFWorkbook(res);	
			
			response.reset();
			response.setHeader("Content-Disposition", "attachment;filename="
					+ URLEncoder.encode("업무보고서", "UTF-8") + ".xlsx;");
			response.setHeader("Content-Transfer-Encoding","binary;");
			response.setContentType("application/download;charset=UTF-8");
			
			bos = new BufferedOutputStream(response.getOutputStream());
			
			sxssfWb.write(bos);
			
			bos.flush();
			
			
		}catch(Exception e){
			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
				e.printStackTrace();
				log.error("download fail");
			}
			
		}finally{
			sxssfWb.close();
			sxssfWb.dispose();
			if(bos != null){
				try{
					bos.close();
				}catch(Exception e){
					throw e;
				}
			}
		}
	}
	
	public List<Map<String,String>> getExcelMapList(Map<String,Object> map)throws Exception{
		List<Map<String,String>> res = null;
		try{
			res = new ArrayList<Map<String,String>>();
			
			Map<String,Object> resRequest = service.getRequestList(map);

			List requestList = (List) resRequest.get("rows");
			
			Iterator it = requestList.iterator();
			while(it.hasNext()){
				Map<String,String> requestValue = (Map<String, String>) it.next();
				Map<String,String> tempObj = new HashMap<String,String>();
				tempObj.put("REQUEST_PK",String.valueOf(requestValue.get("REQUEST_PK")));
				tempObj.put("REQUEST_NAME",requestValue.get("REQUEST_NAME"));
				tempObj.put("REQUEST_BODY",requestValue.get("REQUEST_BODY"));
				tempObj.put("REQUEST_DATE_C",requestValue.get("REQUEST_DATE_C"));
				tempObj.put("REQUEST_DEPARTMENT",requestValue.get("REQUEST_DEPARTMENT"));
				String request_state;
				switch(Integer.parseInt(String.valueOf(requestValue.get("REQUEST_STATE")))){
				case 0: request_state = "요청";
						break;
				case 1: request_state = "처리중";
						break;
				case 2: request_state = "처리완료";
						break;
				default: request_state = "";
						break;
				}
				tempObj.put("REQUEST_STATE",request_state);
				//postData insert
				Map<String,Object> tempMap = new HashMap<String,Object>();
				tempMap.put("requestId",String.valueOf(requestValue.get("REQUEST_PK")));
				tempMap.put("process_name",String.valueOf(map.get("process_name")));
				tempMap.put("process_start_date",String.valueOf(map.get("process_start_date")));
				tempMap.put("process_end_date",String.valueOf(map.get("process_end_date")));
				tempMap.put("search", true);
				tempMap.put("sidx","");
				List<Map<String,String>> resProcess = service.getProcessList(tempMap);
				Iterator<Map<String,String>> itp = resProcess.iterator();
				if (!itp.hasNext()){
					res.add(tempObj);
				}
				while(itp.hasNext()){
					tempObj = new HashMap<String,String>();
					Map<String,String> processValue = itp.next();
					tempObj.put("REQUEST_PK",String.valueOf(requestValue.get("REQUEST_PK")));
					tempObj.put("REQUEST_NAME",requestValue.get("REQUEST_NAME"));
					tempObj.put("REQUEST_BODY",requestValue.get("REQUEST_BODY"));
					tempObj.put("REQUEST_DATE_C",requestValue.get("REQUEST_DATE_C"));
					tempObj.put("REQUEST_DEPARTMENT",requestValue.get("REQUEST_DEPARTMENT"));
					tempObj.put("REQUEST_STATE",request_state);
					
					tempObj.put("REQUEST_PK",String.valueOf(processValue.get("REQUEST_PK")));
					tempObj.put("PROCESS_PK",String.valueOf(processValue.get("PROCESS_PK")));
					tempObj.put("PROCESS_NAME",processValue.get("PROCESS_NAME"));
					tempObj.put("PROCESS_BODY",processValue.get("PROCESS_BODY"));
					tempObj.put("PROCESS_DATE_C",processValue.get("PROCESS_DATE_C"));
					res.add(tempObj);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			
		}
		return res;
	}
	
	public SXSSFWorkbook getSXSSFWorkbook(List<Map<String,String>> map) throws Exception{
		//.xlsx 확장자 지원
		SXSSFWorkbook sxssfWb = null;
		SXSSFSheet sxssfSheet = null;
		SXSSFRow sxssfRow = null;
		SXSSFCell sxssfCell = null;
		
		List HeaderValue = Arrays.asList(new String[]{"요청pk","요청자 이름","요청 부서","요청 내용","요청 날짜","처리pk","처리자 이름","처리 내용","처리 날짜","요청 상태"});
		int RequestHeaderCount = 5;
		List BodyKey = Arrays.asList(new String[]{"REQUEST_PK","REQUEST_NAME","REQUEST_DEPARTMENT","REQUEST_BODY","REQUEST_DATE_C","PROCESS_PK","PROCESS_NAME","PROCESS_BODY","PROCESS_DATE_C","REQUEST_STATE"});
		int rowNo = 0; // 행 갯수 
		try {
			// 워크북 생성
			System.out.println("before create workbook");
			sxssfWb = new SXSSFWorkbook();
			System.out.println("after create workbook");
			// after
			sxssfSheet = sxssfWb.createSheet("업무보고");	//sheet 생성
			System.out.println("after create worksheet");
			// title 설정
			// font
			Font fontTitle = sxssfWb.createFont();	//font 생성
			fontTitle.setFontHeightInPoints((short)14); //폰트크기
			fontTitle.setBold(true); //Bold 유무
			// cellStyle
			CellStyle cellStyleTitle = sxssfWb.createCellStyle();	//cellStyle 생성
			cellStyleTitle.setAlignment(HorizontalAlignment.CENTER);
			cellStyleTitle.setFont(fontTitle);	//cellStyle에 폰트 설정
			
			cellStyleTitle.setBorderTop(BorderStyle.THICK);	//cellStyle 테두리 설정
			cellStyleTitle.setBorderBottom(BorderStyle.THICK);
			cellStyleTitle.setBorderLeft(BorderStyle.THICK);
			cellStyleTitle.setBorderRight(BorderStyle.THICK);
			
			sxssfRow = sxssfSheet.createRow(rowNo++);	//row 추가 및 생성
			SXSSFCell sxssfCellTitle = sxssfRow.createCell((short) 0);	//row에 cell 추가
			for(int i = 1; i < HeaderValue.size(); i++){
				sxssfCell = sxssfRow.createCell((short) i);
				sxssfCell.setCellStyle(cellStyleTitle);
			}
			//cell value
			sxssfCellTitle.setCellStyle(cellStyleTitle);	//cell에 style 입력
			sxssfCellTitle.setCellValue("ULN업무보고서");

			sxssfSheet.addMergedRegion(new CellRangeAddress(0, 0, 0, HeaderValue.size()-1)); //첫행, 마지막행, 첫열, 마지막열x( 0번째 행의 0~x번째 컬럼을 병합한다)
			
			// header 설정
			// font
			Font fontHeader = sxssfWb.createFont();	//font 생성
			fontHeader.setFontHeightInPoints((short)12); //폰트크기
			fontHeader.setBold(true); //Bold 유무
			// cellStyle
			CellStyle cellStyleHeader = sxssfWb.createCellStyle();	//cellStyle 생성
			cellStyleHeader.setAlignment(HorizontalAlignment.CENTER);
			cellStyleHeader.setFont(fontHeader);	//cellStyle에 폰트 설정
			cellStyleHeader.setBorderTop(BorderStyle.THICK);	//cellStyle 테두리 설정
			cellStyleHeader.setBorderBottom(BorderStyle.THICK);
			cellStyleHeader.setBorderLeft(BorderStyle.THIN);
			cellStyleHeader.setBorderRight(BorderStyle.THIN);
			
			CellStyle cellStyleHeaderStart = sxssfWb.createCellStyle();	//cellStyle 생성
			cellStyleHeaderStart.setAlignment(HorizontalAlignment.CENTER);
			cellStyleHeaderStart.setFont(fontHeader);	//cellStyle에 폰트 설정
			cellStyleHeaderStart.setBorderTop(BorderStyle.THICK);	//cellStyle 테두리 설정
			cellStyleHeaderStart.setBorderBottom(BorderStyle.THICK);
			cellStyleHeaderStart.setBorderLeft(BorderStyle.THICK);
			cellStyleHeaderStart.setBorderRight(BorderStyle.THIN);
			
			CellStyle cellStyleHeaderEnd = sxssfWb.createCellStyle();	//cellStyle 생성
			cellStyleHeaderEnd.setAlignment(HorizontalAlignment.CENTER);
			cellStyleHeaderEnd.setFont(fontHeader);	//cellStyle에 폰트 설정
			cellStyleHeaderEnd.setBorderTop(BorderStyle.THICK);	//cellStyle 테두리 설정
			cellStyleHeaderEnd.setBorderBottom(BorderStyle.THICK);
			cellStyleHeaderEnd.setBorderLeft(BorderStyle.THIN);
			cellStyleHeaderEnd.setBorderRight(BorderStyle.THICK);
			
			//sheet insert
			sxssfRow = sxssfSheet.createRow(rowNo++);	//row 추가 및 생성
			for (int colHeader = 0; colHeader<HeaderValue.size() ; colHeader++){
				sxssfCell = sxssfRow.createCell((short) colHeader);	//row에 cell 추가
				//cell value
				if(colHeader ==0){
					sxssfCell.setCellStyle(cellStyleHeaderStart); //cell에 style 입력
				}
				else if (colHeader == HeaderValue.size()-1){
					sxssfCell.setCellStyle(cellStyleHeaderEnd); //cell에 style 입력
				}
				else{
					sxssfCell.setCellStyle(cellStyleHeader); //cell에 style 입력
				}
				sxssfCell.setCellValue(HeaderValue.get(colHeader).toString()); // cell에 value 입력
			}
			//font.setFontName(HSSFFont.FONT_ARIAL); //폰트스타일
			
			// body 설정
			// font
			Font fontBody = sxssfWb.createFont();
			fontBody.setFontHeightInPoints((short)10); //폰트크기
			
			// cellStyle
			CellStyle cellStyleBody = sxssfWb.createCellStyle();
			cellStyleBody.setFont(fontBody);
			cellStyleBody.setVerticalAlignment(VerticalAlignment.CENTER);
			cellStyleBody.setBorderBottom(BorderStyle.THIN);
			cellStyleBody.setBorderLeft(BorderStyle.THIN);
			cellStyleBody.setBorderRight(BorderStyle.THIN);
			cellStyleBody.setWrapText(true);	//줄바꿈 처리
			// cellStyle
			CellStyle cellStyleBodyStart = sxssfWb.createCellStyle();
			cellStyleBodyStart.setVerticalAlignment(VerticalAlignment.CENTER);
			cellStyleBodyStart.setFont(fontBody);
			cellStyleBodyStart.setBorderBottom(BorderStyle.THIN);
			cellStyleBodyStart.setBorderLeft(BorderStyle.THICK);
			cellStyleBodyStart.setBorderRight(BorderStyle.THIN);
			cellStyleBodyStart.setWrapText(true);	//줄바꿈 처리
			// cellStyle
			CellStyle cellStyleBodyEnd = sxssfWb.createCellStyle();
			cellStyleBodyEnd.setVerticalAlignment(VerticalAlignment.CENTER);
			cellStyleBodyEnd.setFont(fontBody);
			cellStyleBodyEnd.setBorderBottom(BorderStyle.THIN);
			cellStyleBodyEnd.setBorderLeft(BorderStyle.THIN);
			cellStyleBodyEnd.setBorderRight(BorderStyle.THICK);
			cellStyleBodyEnd.setWrapText(true);	//줄바꿈 처리
			// cellStyle
			CellStyle cellStyleBodyLeftEdge = sxssfWb.createCellStyle();
			cellStyleBodyLeftEdge.setVerticalAlignment(VerticalAlignment.CENTER);
			cellStyleBodyLeftEdge.setFont(fontBody);
			cellStyleBodyLeftEdge.setBorderBottom(BorderStyle.THICK);
			cellStyleBodyLeftEdge.setBorderLeft(BorderStyle.THICK);
			cellStyleBodyLeftEdge.setBorderRight(BorderStyle.THIN);
			cellStyleBodyLeftEdge.setWrapText(true);	//줄바꿈 처리
			// cellStyle
			CellStyle cellStyleBodyRightEdge = sxssfWb.createCellStyle();
			cellStyleBodyRightEdge.setVerticalAlignment(VerticalAlignment.CENTER);
			cellStyleBodyRightEdge.setFont(fontBody);
			cellStyleBodyRightEdge.setBorderBottom(BorderStyle.THICK);
			cellStyleBodyRightEdge.setBorderLeft(BorderStyle.THIN);
			cellStyleBodyRightEdge.setBorderRight(BorderStyle.THICK);
			cellStyleBodyRightEdge.setWrapText(true);	//줄바꿈 처리
			// cellStyle
			CellStyle cellStyleBodyBottom = sxssfWb.createCellStyle();
			cellStyleBodyBottom.setVerticalAlignment(VerticalAlignment.CENTER);
			cellStyleBodyBottom.setFont(fontBody);
			cellStyleBodyBottom.setBorderBottom(BorderStyle.THICK);
			cellStyleBodyBottom.setBorderLeft(BorderStyle.THIN);
			cellStyleBodyBottom.setBorderRight(BorderStyle.THIN);
			cellStyleBodyBottom.setWrapText(true);	//줄바꿈 처리
			
			//sheet insert
			int count = 0;
			String beforePk = "";
			for (int i = 0; i<map.size(); i++){
				sxssfRow = sxssfSheet.createRow(rowNo++);	//row 추가 및 생성
				
				if (map.get(i).get("REQUEST_PK").equals(beforePk)){
					count++;
				}
				else{
					if(count != 0){
						for (int j = 0; j <RequestHeaderCount;j++){
							sxssfSheet.addMergedRegion(new CellRangeAddress((i+2)-1-count, (i+2)-1, j, j));
						}
						sxssfSheet.addMergedRegion(new CellRangeAddress((i+2)-1-count, (i+2)-1, HeaderValue.size()-1, HeaderValue.size()-1));
						count = 0;
					}
				}
				for (int j = 0 ; j < HeaderValue.size(); j++){
					sxssfCell = sxssfRow.createCell((short) j);	//row에 cell 추가
					if(j == 0){
						if(i == map.size()-1){
							sxssfCell.setCellStyle(cellStyleBodyLeftEdge);
						}
						else{
							sxssfCell.setCellStyle(cellStyleBodyStart);
						}
					}
					else if (j == HeaderValue.size()-1){
						if(i == map.size()-1){
							sxssfCell.setCellStyle(cellStyleBodyRightEdge);
						}
						else{
							sxssfCell.setCellStyle(cellStyleBodyEnd);
						}
					}
					else{
						if(i == map.size()-1){
							sxssfCell.setCellStyle(cellStyleBodyBottom);
						}
						else{
							sxssfCell.setCellStyle(cellStyleBody);
						}
					}
					String BodyValue = map.get(i).get(BodyKey.get(j));
					if (BodyValue == null){
						sxssfCell.setCellValue("");
					}
					else{
						sxssfCell.setCellValue(BodyValue);
					}
				}
				beforePk = map.get(i).get("REQUEST_PK");
			}
			if(count != 0){
				for (int j = 0; j <RequestHeaderCount;j++){
					sxssfSheet.addMergedRegion(new CellRangeAddress((map.size()+2)-1-count, (map.size()+2)-1, j, j));
				}
				sxssfSheet.addMergedRegion(new CellRangeAddress((map.size()+2)-1-count, (map.size()+2)-1, HeaderValue.size()-1, HeaderValue.size()-1));
			}
			
			// Sheet col 넓이 자동 설정
			sxssfSheet.trackAllColumnsForAutoSizing();
			for (int i = 0; i < HeaderValue.size(); i++){
				sxssfSheet.autoSizeColumn(i);
				sxssfSheet.setColumnWidth(i, (sxssfSheet.getColumnWidth(i))+(short)512); // i번째 컬럼 넓이 조절
			}
			return sxssfWb;
			
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{

		}
	}
}
