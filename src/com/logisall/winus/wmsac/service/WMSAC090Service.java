package com.logisall.winus.wmsac.service;

import java.util.Map;

public interface WMSAC090Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> check(Map<String, Object> model) throws Exception;
	public Map<String, Object> feeList(Map<String, Object> model) throws Exception;
}
