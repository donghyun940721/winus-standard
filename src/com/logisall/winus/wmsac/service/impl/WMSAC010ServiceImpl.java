package com.logisall.winus.wmsac.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC010Service")
public class WMSAC010ServiceImpl extends AbstractServiceImpl implements WMSAC010Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

   @Resource(name = "WMSAC010Dao")
    private WMSAC010Dao dao;

   /**
    * 대체 Method ID   : selectData
    * 대체 Method 설명    : 상품 목록 필요 데이타셋
    * 작성자                      : chsong
    * @param   model
    * @return
    * @throws  Exception
    */
   @Override
   public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       map.put("ITEMGRP", dao.selectItemGrp(model));
       return map;
   }
   
   /**
    * Method ID     : list
    * Method 설명       : 상품목록 조회
    * 작성자                  : chsong
    * @param   model
    * @return
    * @throws  Exception
    */
   public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       try {
           if (model.get("page") == null) {
               model.put("pageIndex", "1");
           } else {
               model.put("pageIndex", model.get("page"));
           }
           if (model.get("rows") == null) {
               model.put("pageSize", "20");
           } else {
               model.put("pageSize", model.get("rows"));
           }
           
           if("20".equals(model.get("SS_USER_GB"))){
               model.put("vrSrchCustId", model.get(ConstantIF.SS_CLIENT_CD));
           }
           map.put("LIST", dao.listE4(model));
       } catch (Exception e) {
           log.error(e.toString());
           map.put("MSG", MessageResolver.getMessage("list.error"));
       }
       return map;
   }
   
   /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : listSubDetail
     * 대체 Method 설명    : 청구단가계약 상단조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSubDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DETAIL", dao.listSubDetail(model));
        return map;
    }
    
    /**
     * Method ID    : listSub
     * Method 설명      : 청구단가계약 그리드 조회
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     @Override
     public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         loger.info(model);
         if(model.get("page") == null) {
             model.put("pageIndex", "1");
         } else {
             model.put("pageIndex", model.get("page"));
         }
         if(model.get("rows") == null) {
             model.put("pageSize", "20");
         } else {
             model.put("pageSize", model.get("rows"));
         }
         map.put("LIST", dao.listSub(model));
         return map;
     }
     
     /**
      * 대체 Method ID   : saveSub
      * 대체 Method 설명    : 청구단가계약 (저장,수정,삭제)
      * 작성자                      : chsong
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> saveSub(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         try{

             int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
             int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
             if(insCnt > 0){
                 //저장, 수정
                 String[] contDetailId  = new String[insCnt];                
                 String[] serviceCd     = new String[insCnt];      
                 String[] ritemId       = new String[insCnt];     
                 String[] uomId         = new String[insCnt];     
                 String[] freeTimeDay   = new String[insCnt];     
                 String[] unitAmt       = new String[insCnt];
                 String[] tansCustId    = new String[insCnt];
                 
                 for(int i = 0 ; i < insCnt ; i ++){
                     contDetailId[i]    = (String)model.get("I_CONT_DETAIL_ID"+i);               
                     serviceCd[i]       = (String)model.get("I_SERVICE_CD"+i);    
                     ritemId[i]         = (String)model.get("I_RITEM_ID"+i);      
                     uomId[i]           = (String)model.get("I_UOM_ID"+i);      
                     freeTimeDay[i]     = (String)model.get("I_FREE_TIME_DAY"+i);        
                     unitAmt[i]         = (String)model.get("I_UNIT_AMT"+i);      
                     tansCustId[i]      = (String)model.get("I_TRANS_CUST_ID"+i);
                 }
                 //프로시져에 보낼것들 다담는다
                 Map<String, Object> modelIns = new HashMap<String, Object>();
                 
                 //main
                 modelIns.put("contId"          , (String)model.get("vrContId"));
                 modelIns.put("gvLcId"          , (String)model.get(ConstantIF.SS_SVC_NO));
                 modelIns.put("custId"          , (String)model.get("vrCustId"));
                 modelIns.put("saleBuyGbn"      , (String)model.get("vrSaleBuyGb"));
                 modelIns.put("applyFrDt"       , (String)model.get("vrSrchReqDtFrom").toString().replace("-", ""));
                 
                 modelIns.put("applyToDt"       , (String)model.get("vrSrchReqDtTo").toString().replace("-", ""));
                 modelIns.put("accStd"          , (String)model.get("vrAccStd"));
                 modelIns.put("contDeptId"      , (String)model.get("vrContDeptId"));
                 modelIns.put("contEmployeeId"  , (String)model.get("vrContEmpId"));
                 modelIns.put("realDeptId"      , (String)model.get("vrRealDeptId"));
                 
                 modelIns.put("realEmployeeId"  , (String)model.get("vrRealEmpId"));
                 modelIns.put("collectEmployeeId"  , "");
                 modelIns.put("payDay"          , (String)model.get("vrPayDay"));
                 modelIns.put("payCd"           , (String)model.get("vrPayCd"));
                 modelIns.put("roundCd"         , (String)model.get("vrRoundCd"));
                 modelIns.put("endCutCd"        , (String)model.get("vrEndCutCd"));
                 
                 modelIns.put("confYn"          , (String)model.get("vrConfYn"));
                 modelIns.put("closingDay"  	, "");
                 
                 //sub
                 modelIns.put("contDetailId"    , contDetailId);
                 modelIns.put("serviceCd"       , serviceCd);
                 modelIns.put("ritemId"         , ritemId);
                 modelIns.put("uomId"           , uomId);
                 modelIns.put("freeTimeDay"     , freeTimeDay);
                 modelIns.put("unitAmt"         , unitAmt);
                 modelIns.put("tansCustId"      , tansCustId);
                 
                 //session 정보
                 modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                 modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                 //dao                
                 modelIns  = (Map<String, Object>)dao.saveSub(modelIns);
                 ServiceUtil.isValidReturnCode("WMSAC010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
             }
             //등록 수정 끝
             
             if(delCnt > 0 ){
                 String[] contDetailId     = new String[delCnt];             
                 for(int i = 0 ; i < delCnt ; i ++){
                     contDetailId[i]   = (String)model.get("D_CONT_DETAIL_ID"+i);                
                 }
                 
                 //프로시져에 보낼것들 다담는다
                 Map<String, Object> modelDel = new HashMap<String, Object>();
                 modelDel.put("contDetailId"       , contDetailId);

                 modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                 modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                 
                 modelDel = (Map<String, Object>)dao.deleteSub(modelDel);
                 ServiceUtil.isValidReturnCode("WMSAC010", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("save.success"));
             
         } catch(BizException be) {
             m.put("errCnt", 1);
             m.put("MSG", be.getMessage() );             
             
         } catch(Exception e){
             throw e;
         }
         return m;
     }  
     
     /**
      * Method ID    : listAccDt
      * Method 설명      : 거래처정산일설정 조회
      * 작성자                 : chsong
      * @param   model
      * @return 
      * @throws Exception 
      */
      @Override
      public Map<String, Object> listAccDt(Map<String, Object> model) throws Exception {
          Map<String, Object> map = new HashMap<String, Object>();
          loger.info(model);
          if(model.get("page") == null) {
              model.put("pageIndex", "1");
          } else {
              model.put("pageIndex", model.get("page"));
          }
          if(model.get("rows") == null) {
              model.put("pageSize", "20");
          } else {
              model.put("pageSize", model.get("rows"));
          }
          map.put("LIST", dao.listAccDt(model));
          return map;
      }
      
      /**
       * 대체 Method ID   : saveAccdt
       * 대체 Method 설명    : 거래처정산일 (저장,수정,삭제)
       * 작성자                      : chsong
       * @param model
       * @return
       * @throws Exception
       */
      @Override
      public Map<String, Object> saveAccdt(Map<String, Object> model) throws Exception {
          Map<String, Object> m = new HashMap<String, Object>();
          // loger.info(model);

          try{

              int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
              int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
              if(insCnt > 0){
                  //저장, 수정
                  String[] accDayId     = new String[insCnt];                
                  String[] contId       = new String[insCnt];      
                  
                  String[] accFrMon     = new String[insCnt];     
                  String[] accFrDay     = new String[insCnt];     
                  String[] accToMon     = new String[insCnt];     
                  
                  String[] accToDay     = new String[insCnt];                     
                  String[] accDay        = new String[insCnt];    
                  
                  for(int i = 0 ; i < insCnt ; i ++){
                      accDayId[i]   = (String)model.get("I_ACC_DAY_ID"+i);               
                      contId[i]     = (String)model.get("I_CONT_ID"+i);          
                      
                      accFrMon[i]   = (String)model.get("I_ACC_FR_MON"+i);      
                      accFrDay[i]   = (String)model.get("I_ACC_FR_DAY"+i);      
                      accToMon[i]   = (String)model.get("I_ACC_TO_MON"+i);        
                      accToDay[i]   = (String)model.get("I_ACC_TO_DAY"+i);                      
                      accDay[i]     = (String)model.get("I_ACC_DAY"+i);       
                  }
                  //프로시져에 보낼것들 다담는다
                  Map<String, Object> modelIns = new HashMap<String, Object>();
                  
                  //sub
                  modelIns.put("accDayId"       , accDayId);
                  modelIns.put("contId"         , contId);
                  modelIns.put("accFrMon"       , accFrMon);
                  modelIns.put("accFrDay"       , accFrDay);
                  modelIns.put("accToMon"       , accToMon);
                  
                  modelIns.put("accToDay"       , accToDay);
                  modelIns.put("accDay"         , accDay);
                 
                  //session 정보
                  modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                  modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                  //dao                
                  modelIns = (Map<String, Object>)dao.saveAccday(modelIns);
                  ServiceUtil.isValidReturnCode("WMSAC010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

              }
              //등록 수정 끝
              
              if(delCnt > 0 ){
                  String[] accDayId     = new String[delCnt];             
                  for(int i = 0 ; i < delCnt ; i ++){
                      accDayId[i]   = (String)model.get("D_ACC_DAY_ID"+i);                
                  }
                  
                  //프로시져에 보낼것들 다담는다
                  Map<String, Object> modelDel = new HashMap<String, Object>();
                  modelDel.put("accDayId"       , accDayId);

                  modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                  modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                  
                  modelDel = (Map<String, Object>)dao.deleteAccday(modelDel);
                  ServiceUtil.isValidReturnCode("WMSAC010", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
              }
              m.put("errCnt", 0);
              m.put("MSG", MessageResolver.getMessage("save.success"));
              
          } catch(BizException be) {
              m.put("errCnt", 1);
              m.put("MSG", be.getMessage() );              

          } catch(Exception e){
              throw e;
          }
          return m;
      }  
      
      
      /**
       * Method ID : saveExcelUpload saveSub
       * Method 설명 : 청구단가 엑셀 저장 
       * 작성자 : summer H
       * @param model
       * @return
       * @throws Exception
       */
      public Map<String, Object> saveExcelUpload(Map<String, Object> model, List list) throws Exception {
          Map<String, Object> m = new HashMap<String, Object>();
          int errCnt = 0;
          int insertCnt = (list != null)?list.size():0;
              try{
              	if(insertCnt > 0){
          			String[] applyToDt       	= new String[insertCnt];
          			String[] applyFromDt       	= new String[insertCnt];
          			String[] accStd     		= new String[insertCnt];
          			String[] saleBuyGbn       	= new String[insertCnt];
          			String[] payDay       		= new String[insertCnt];
          			
          			String[] rItemCd       		= new String[insertCnt];
          			String[] rItemGroupCd       = new String[insertCnt];
          			String[] unitPrice       	= new String[insertCnt];
          			String[] inOutCost       	= new String[insertCnt];
          			String[] stockCost       	= new String[insertCnt];
          			
          			String[] currencyNm       	= new String[insertCnt];
          			String[] transCustId      	= new String[insertCnt];
          			String[] payCd       		= new String[insertCnt];
          			String[] roundCd       		= new String[insertCnt];
          			String[] endCutCd       	= new String[insertCnt];
          	
          			String[] closingDay       	= new String[insertCnt];
          			String[] contDeptId      	= new String[insertCnt];
          			String[] contEmployeeId     = new String[insertCnt];
          			String[] conf_yn       		= new String[insertCnt];
          			String[] realDeptId       	= new String[insertCnt];
          			
          			String[] realEmployeeId     = new String[insertCnt];
          			String[] collectEmployeeId  = new String[insertCnt];
          			String[] approvalEmpId      = new String[insertCnt];
          			String[] groupComEmpId      = new String[insertCnt];
              		
          			
              		for(int i=0; i<insertCnt; i++){
              			Map<String, String> temp = (Map)list.get(i);
              			
              			 applyToDt[i]                  = (String)temp.get("APPLY_FR_DT");
              			 applyFromDt[i]                = (String)temp.get("APPLY_TO_DT");
              			 accStd[i]                     = (String)temp.get("ACC_STD");
               			 saleBuyGbn[i]                 = (String)temp.get("SALE_BUY_GBN");
              			 payDay[i]                     = (String)temp.get("PAY_DAY");
              			                       
              			 rItemCd[i]                    = (String)temp.get("RITEM_CD");
              			 rItemGroupCd[i]               = (String)temp.get("ITEM_GRP_CD");
              			 unitPrice[i]                  = (String)temp.get("UNIT_PRICE");
              			 inOutCost[i]                  = (String)temp.get("INOUT_COST");
              			 stockCost[i]                  = (String)temp.get("STOCK_COST");
              			
              			 currencyNm[i]                 = (String)temp.get("CURRENCY_NAME");
              			 transCustId[i]                = (String)temp.get("TRANS_CUST_ID");
              			 payCd[i]                      = (String)temp.get("PAY_CD");
              			 roundCd[i]                    = (String)temp.get("ROUND_CD");
              			 endCutCd[i]                   = (String)temp.get("END_CUT_CD");

              			 closingDay[i]                 = (String)temp.get("CLOSING_DAY");
              			 contDeptId[i]                 = (String)temp.get("CONT_DEPT_ID");
              			 contEmployeeId[i]             = (String)temp.get("CONT_EMPLOYEE_ID");
              			 conf_yn[i]                    = (String)temp.get("CONF_YN");
              			 realDeptId[i]                 = (String)temp.get("REAL_DEPT_ID");

              			 realEmployeeId[i]             = (String)temp.get("REAL_EMPLOYEE_ID");
              			 collectEmployeeId[i]          = (String)temp.get("COLLECT_EMPLOYEE_ID");
              			 approvalEmpId[i]              = (String)temp.get("APPROVAL_EMP_ID");
              			 groupComEmpId[i]              = (String)temp.get("GROUP_COM_EMP_ID");
              			
              		}
              		
              		m.put("APPLY_FR_DT"				, applyToDt);                  
 	                m.put("APPLY_TO_DT"				, applyFromDt);
 	                m.put("ACC_STD"					, accStd);
 	                m.put("SALE_BUY_GBN"			, saleBuyGbn);
 	                m.put("PAY_DAY"					, payDay);
 	                                                                   
 	                m.put("RITEM_CD"				, rItemCd);
 	                m.put("ITEM_GRP_NM"				, rItemGroupCd);
 	                m.put("UNIT_PRICE"				, unitPrice);
 	                m.put("INOUT_COST"				, inOutCost);
 	                m.put("STOCK_COST"				, stockCost);
 	                
 	                m.put("CURRENCY_NAME"			, currencyNm);
 	                m.put("TRANS_CUST_ID"			, transCustId);
 	                m.put("PAY_CD"					, payCd);
 	                m.put("ROUND_CD"				, roundCd);
 	                m.put("END_CUT_CD"				, endCutCd);
 	                
 	                m.put("CLOSING_DAY"				, closingDay);
 	                m.put("CONT_DEPT_ID"			, contDeptId);
 	                m.put("CONT_EMPLOYEE_ID"		, contEmployeeId);
 	                m.put("CONF_YN"					, conf_yn);
 	                m.put("REAL_DEPT_ID"			, realDeptId);
 	               
 	                m.put("REAL_EMPLOYEE_ID"		, realEmployeeId);
 	                m.put("COLLECT_EMPLOYEE_ID"		, collectEmployeeId);
 	                m.put("APPROVAL_EMP_ID"			, approvalEmpId);
 	                m.put("GROUP_COM_EMP_ID"		, groupComEmpId);
 	                m.put("OVERWRITE_YN"		, (String)model.get("chkDuplicationYn"));

	                m.put("CUST_ID"				, (String)model.get("vrCustId"));
	                m.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
	                m.put("WORK_IP"				, (String)model.get(ConstantIF.SS_CLIENT_IP));
	                m.put("USER_NO"				, (String)model.get(ConstantIF.SS_USER_NO));
	                
	  	            m = (Map<String, Object>)dao.saveSubExcel(m);
	  	            
                    ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
                    m.put("O_CUR", m.get("O_CUR"));  
              	}
              	
              	m.put("errCnt", 0);
              	m.put("MSG_ORA", "");
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                
              } catch(Exception e){
            	  throw e;
              }
               return m;
      }    
      
      
      
}
