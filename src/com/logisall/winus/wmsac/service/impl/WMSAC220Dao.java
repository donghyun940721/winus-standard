package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC220Dao")
public class WMSAC220Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }

    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    

//    public int ChkWMSAC220Cnt(Map<String, Object> model) throws Exception {
//		// TODO Auto-generated method stub
//		return (Integer) executeQueryForObject("wmsac220.ChkWMSAC220Cnt", model);
//	}
//
//    public Object wmsac220_list2(Map<String, Object> model) throws Exception {
//		// TODO Auto-generated method stub
//		return executeQueryPageWq("wmsac220.wmsac220_list2", model);
//	}
    
    public GenericResultSet list2(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac220.list2", model);
	} 
    
    public GenericResultSet list3(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac220.list3", model);
	}
    
}