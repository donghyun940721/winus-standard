package com.logisall.winus.wmsac.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC090Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC090Service")
public class WMSAC090ServiceImpl extends AbstractServiceImpl implements WMSAC090Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

   @Resource(name = "WMSAC090Dao")
    private WMSAC090Dao dao;

   /**
    * Method ID     : list
    * Method 설명       : 상품목록 조회
    * 작성자                  : chsong
    * @param   model
    * @return
    * @throws  Exception
    */
   public Map<String, Object> list(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       try {
           map.put("LIST", dao.list(model));
       } catch (Exception e) {
           log.error(e.toString());
           map.put("MSG", MessageResolver.getMessage("list.error"));
       }
       return map;
   }
     
     /**
      * 대체 Method ID   : saveSub
      * 대체 Method 설명    : 청구단가계약 (저장,수정,삭제)
      * 작성자                      : chsong
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> save(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         try{

             int cnt = Integer.parseInt(model.get("selectIds").toString());
             if(cnt > 0){
                 //저장, 수정
                 
                 for(int i = 0 ; i < cnt ; i ++){
                     String contId = (String)model.get("I_CONT_ID"+i);               
                     String custCd = (String)model.get("I_CUST_CD"+i);               
                     String ritemCd = (String)model.get("I_RITEM_CD"+i);               
                     String localSite = (String)model.get("I_LOCAL_SITE"+i);               
                     String serviceCd = (String)model.get("I_SERVICE_CD"+i);               
                     String dlvOrdStat = (String)model.get("I_DLV_ORD_STAT"+i);               
                     String upperUnitPrice = (String)model.get("I_UPPER_UNIT_PRICE"+i);               
                     String downUnitPrice = (String)model.get("I_DOWN_UNIT_PRICE"+i);               
                     String destUnitPrice = (String)model.get("I_DEST_UNIT_PRICE"+i);               
                     String custId = (String)model.get("I_CUST_ID"+i);               
                     String ritemId = (String)model.get("I_RITEM_ID"+i);               
                     String applyFrDt = (String)model.get("I_APPLY_FR_DT"+i);               
                     String applyToDt = (String)model.get("I_APPLY_TO_DT"+i);               
                     
                     Map<String, Object> modelIns = new HashMap<>();
                     modelIns.put("contId", contId);
                     modelIns.put("custCd", custCd);
                     modelIns.put("ritemCd", ritemCd);
                     modelIns.put("localSite", localSite);
                     modelIns.put("serviceCd", serviceCd);
                     modelIns.put("dlvOrdStat", dlvOrdStat);
                     modelIns.put("upperUnitPrice", upperUnitPrice);
                     modelIns.put("downUnitPrice", downUnitPrice);
                     modelIns.put("destUnitPrice", destUnitPrice);
                     modelIns.put("custId", custId);
                     modelIns.put("ritemId", ritemId);
                     modelIns.put("applyFrDt", applyFrDt);
                     modelIns.put("applyToDt", applyToDt);

                     modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                 modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
	                 if(StringUtils.isEmpty(contId)){
	                	//insert
	                	 dao.insert(modelIns);
	                 }else{
	                	//update
	                	 dao.update(modelIns);
	                 }
                 }
             }
             //등록 수정 끝
             
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("save.success"));
             
         } catch(Exception e){
        	 m.put("errCnt", 1);
             m.put("MSG", e.getMessage() );   
         }
         return m;
     }  
     
     /**
      * 대체 Method ID   : saveSub
      * 대체 Method 설명    : 청구단가계약 (저장,수정,삭제)
      * 작성자                      : chsong
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> check(Map<String, Object> model) throws Exception {
    	 Map<String, Object> map = new HashMap<String, Object>();
         map.put("CHECK", dao.check(model));
         return map;
     
     }
     
     /**
      * 
      * 대체 Method ID   : feeList
      * 대체 Method 설명    : 고객관리 조회
      * 작성자                      : chsong
      * @param   model
      * @return
      * @throws  Exception
      */
     @Override
     public Map<String, Object> feeList(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         if(model.get("page") == null) {
             model.put("pageIndex", "1");
         } else {
             model.put("pageIndex", model.get("page"));
         }
         if(model.get("rows") == null) {
             model.put("pageSize", "20");
         } else {
             model.put("pageSize", model.get("rows"));
         }
         
         List<String> cityArr = new ArrayList();
         String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
         for (String keyword : spSrchAddrLike ){
             cityArr.add(keyword);
         }
         model.put("cityArr", cityArr);
         
         List<String> transCustNmArr = new ArrayList();
         String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
         for (String keyword : spVrSrchTransCustNmLike ){
         	transCustNmArr.add(keyword);
         }
         model.put("transCustNmArr", transCustNmArr);
         
         List<String> vrSrchRitemCdArr = new ArrayList();
         String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
         for (String keyword : spVrSrchRitemCdLike ){
         	vrSrchRitemCdArr.add(keyword);
         }
         model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
         
         map.put("LIST", dao.feeList(model));
         return map;
     }
}
