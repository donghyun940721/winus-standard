package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC018Dao")
public class WMSAC018Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : itemAcList
     * Method 설명 : 상품 정산 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet itemAcList(Map<String, Object> model) {
        return executeQueryWq("wmsac018.itemAcList", model);
    }   
    
    /**
     * Method ID : acMstrList
     * Method 설명 : 정산 마스터 조회
     * 작성자 :  yhku
     * @param model
     * @return
     */
    public GenericResultSet acMstrList(Map<String, Object> model) {
        return executeQueryWq("wmsac018.acMstrList", model);
    }   
    
    
    /**
     * Method ID : acUnitAccList
     * Method 설명 : 정산 단위 단가 리스트
     * 작성자 :  yhku
     * @param model
     * @return
     */
    public GenericResultSet acUnitAccList(Map<String, Object> model) {
        return executeQueryWq("wmsac018.acUnitAccList", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    
    /**
     * Method ID    : save
     * Method 설명      : 청구단가계약 등록
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsac018.pk_wmsac018.sp_save_cont", model);
        return model;
    }
    
    /**
     * Method ID    : delete
     * Method 설명      : 청구단가계약 삭제
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object delete(Map<String, Object> model){
        executeUpdate("wmsac018.pk_wmsac018.sp_del_cont_detail", model);
        return model;
    }
    

    
    /**
     * Method ID    : itemAcSave
     * Method 설명      : 청구단가계약 상품일괄적용
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object itemAcSave(Map<String, Object> model){
        executeUpdate("wmsac018.pk_wmsac018.sp_save_item_cont", model);
        return model;
    }
    
    
    /**
     * Method ID    : itemAcDelete
     * Method 설명      : 청구단가계약 삭제
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object itemAcDelete(Map<String, Object> model){
        executeUpdate("wmsac018.pk_wmsac018.sp_del_item_cont", model);
        return model;
    }
    
    /**
     * Method ID    : saveSub
     * Method 설명      : 청구단가계약 등록 - 엑셀입력
     * 작성자                 : Summer Hyun
     * @param   model
     * @return
     */
    public Object saveSubExcel(Map<String, Object> model){
        executeUpdate("wmsac018.pk_wmsac018.sp_save_excel_cont", model);
        return model;
    }
    
}

