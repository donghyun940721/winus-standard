package com.logisall.winus.wmsac.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsac.service.WMSAC250Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSAC250Service")
public class WMSAC250ServiceImpl implements WMSAC250Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAC250Dao")
    private WMSAC250Dao dao;
    
    @Resource(name = "WMSAC240Dao")
    private WMSAC240Dao dao240;
    
    private final static String[] CHECK_VALIDATE_WMSAC250 = {"ORD_ID", "DLV_ADD_COST"};
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 납품확인 리스트 조회
     * 작성자                      : kimzero
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        String vrLcId = (String)model.get(ConstantIF.SS_SVC_NO);
        ArrayList<String> vrSrchLcId = new ArrayList<String>();
        //대화물류 전체 센터 조회
        if (vrLcId.equals("0000003541") || vrLcId.equals("0000003721") || vrLcId.equals("0000003722") || vrLcId.equals("0000003720")){
            vrSrchLcId.add("0000003541");
            vrSrchLcId.add("0000003721");
            vrSrchLcId.add("0000003722");
            vrSrchLcId.add("0000003720");
            vrLcId = "0000003541";
        }
        else {
            vrSrchLcId.add(vrLcId);
        }
        model.put("vrSrchLcId", vrSrchLcId);
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listCount
     * 대체 Method 설명    : 납품확인 리스트 조회
     * 작성자                      : kimzero
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCount(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        String vrLcId = (String)model.get(ConstantIF.SS_SVC_NO);
        ArrayList<String> vrSrchLcId = new ArrayList<String>();
        //대화물류 전체 센터 조회
        if (vrLcId.equals("0000003541") || vrLcId.equals("0000003721") || vrLcId.equals("0000003722") || vrLcId.equals("0000003720")){
            vrSrchLcId.add("0000003541");
            vrSrchLcId.add("0000003721");
            vrSrchLcId.add("0000003722");
            vrSrchLcId.add("0000003720");
            vrLcId = "0000003541";
        }
        else {
            vrSrchLcId.add(vrLcId);
        }
        model.put("vrSrchLcId", vrSrchLcId);
    	map.put("LIST", dao.listCount(model));
    	return map;
    }
    
	/*-
	 * Method ID    : list
	 * Method 설명      : 납품확인 메뉴 그리드 저장
	 * 작성자                 : kimzero
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int resultCount = 0;
        int errCount = 0;
        String errMessage = "";
        try{
        	 int checkedRowCnt = Integer.parseInt(model.get("selectIds").toString());
        	 
        	 if (checkedRowCnt > 0) {
        	     String vrLcId = (String)model.get(ConstantIF.SS_SVC_NO);
                 ArrayList<String> vrSrchLcId = new ArrayList<String>();
                 //대화물류 전체 센터 조회
                 if (vrLcId.equals("0000003541") || vrLcId.equals("0000003721") || vrLcId.equals("0000003722") || vrLcId.equals("0000003720")){
                     vrSrchLcId.add("0000003541");
                     vrSrchLcId.add("0000003721");
                     vrSrchLcId.add("0000003722");
                     vrSrchLcId.add("0000003720");
                     vrLcId = "0000003541";
                 }
                 else {
                     vrSrchLcId.add(vrLcId);
                 }
                 
                 Map<String, Object> modelIns = new HashMap<String, Object>();
                 ArrayList<String> initCust = new ArrayList<String>();
                 for(int i = 0 ; i < checkedRowCnt ; i ++){
                	 //STEP1. 키값제외 나머지 속성 먼저 셋팅.
                	 modelIns.put("WORK_DT"         , String.valueOf(model.get("WORK_DT" + i)));
                	 modelIns.put("TRANS_CUST_ID"   , String.valueOf(model.get("TRANS_CUST_ID" + i)));
                	 modelIns.put("TRANS_CUST_CD"   , String.valueOf(model.get("TRANS_CUST_CD" + i)));
                	 modelIns.put("TRANS_CUST_NM"   , String.valueOf(model.get("TRANS_CUST_NM" + i)));
                	 modelIns.put("CUST_ID"         , String.valueOf(model.get("CUST_ID" + i)));
                	 modelIns.put("CUST_CD"         , String.valueOf(model.get("CUST_CD" + i)));
                	 modelIns.put("CUST_NM"         , String.valueOf(model.get("CUST_NM" + i)));
                	 modelIns.put("CUST_BOX_QTY"    , String.valueOf(model.get("CUST_BOX_QTY" + i)));
                	 modelIns.put("CUST_PLT_QTY"    , String.valueOf(model.get("CUST_PLT_QTY" + i)));
                	 modelIns.put("DLV_QTY"         , String.valueOf(model.get("DLV_QTY" + i)));
                	 modelIns.put("DLV_PLT_QTY"     , String.valueOf(model.get("DLV_PLT_QTY" + i)));
                	 modelIns.put("P_M_QTY"         , String.valueOf(model.get("P_M_QTY" + i)));
                	 modelIns.put("NOT_DLV_RSN"     , String.valueOf(model.get("NOT_DLV_RSN" + i)));
                	 modelIns.put("NOT_DLV_DTI_RSN" , String.valueOf(model.get("NOT_DLV_DTI_RSN" + i)));
                	 modelIns.put("MEMO1"           , String.valueOf(model.get("MEMO1" + i)));
                	 modelIns.put("PROC_RST"        , String.valueOf(model.get("PROC_RST" + i)));
                	 modelIns.put("IN_PROC_DT"      , String.valueOf(model.get("IN_PROC_DT" + i)));
                	 modelIns.put("IN_PROC_QTY"     , String.valueOf(model.get("IN_PROC_QTY" + i)));
                	 modelIns.put("OUT_PROC_DT"     , String.valueOf(model.get("OUT_PROC_DT" + i)));
                	 modelIns.put("OUT_PROC_QTY"    , String.valueOf(model.get("OUT_PROC_QTY" + i)));
                	 modelIns.put("MEMO2"           , String.valueOf(model.get("MEMO2" + i)));
                	 modelIns.put("LC_ID"           , String.valueOf(model.get("LC_ID" + i)));
                	 modelIns.put("vrSrchLcId"      , vrSrchLcId);
                	 modelIns.put("vrLcId"          , vrLcId);
                	 modelIns.put("SS_SVC_NO"       , String.valueOf(model.get(ConstantIF.SS_SVC_NO)));
                                    	 
                	 // session 및 등록정보
                	 modelIns.put("USER_NO"			, String.valueOf(model.get(ConstantIF.SS_USER_NO)));
                	 modelIns.put("WORK_IP"			, String.valueOf(model.get(ConstantIF.SS_CLIENT_IP)));
                	 
                	//STEP2. 체크된 ROW별로 키값 셋팅.
                	 if(String.valueOf(model.get("ACC_WORK_ID" + i)).equals("null")){
                		 String nextIdVal = String.valueOf(dao.getFindAccId(modelIns));
                		 
                		 //STEP2-1. 화면에서 가지고 넘어온 ACC_WORK_ID 가 없는경우 기존 등록된 수탁증이 있는지 조회.
                		 if(nextIdVal.equals("null")){
                			 //STEP2-2. 1-1에서 조회한 기존 수탁증 ID가 없으면 새로 채번후 Header까지 같이 생성.
                			 //isNotExistHeader = true;// Header테이블에 데이터가 없으면 true => 생성.
                			 nextIdVal = String.valueOf(dao.getNextAccId());
                			 //화주코드가 TC로 시작일 경우 수탁증 detail init
                			 if(String.valueOf(model.get("CUST_CD"+i)).substring(0,2).equals("TC")){
                			     //detail init flag set
                			     initCust.add(String.valueOf(model.get("CUST_ID" + i)));
                			     Map<String, Object> model240 = new HashMap<String, Object>();
                			     model240.put("LC_ID", vrLcId);
                			     model240.put("vrCustCd", model.get("CUST_CD" + i));
                			     model240.put("vrWorkDt", model.get("WORK_DT" + i));
                			     model240.put("ACC_WORK_ID", nextIdVal);
                			     model240.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                			     model240.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
                			     try{
                			         dao240.initDetail(model240);
                			     }
                			     catch (Exception e){
                			         errCount++;
                                     errMessage += "\n(시스템에러) 관리자 문의 부탁드립니다. : " + modelIns.get("TRANS_CUST_NM")+ "/" + modelIns.get("CUST_NM");
                			         continue;
                			     }
                			 }
                			 
                		 }
                		 modelIns.put("ACC_WORK_ID"     , nextIdVal);
                	 }else{
                		 modelIns.put("ACC_WORK_ID"     , String.valueOf(model.get("ACC_WORK_ID" + i)));
                	 }
                	 
                	 //STEP2-3. 체크된 ROW ACC_WORK_SEQ값 조회.
                	 if(String.valueOf(model.get("ACC_WORK_SEQ" + i)).equals("null")){
                		 // ACC_WORK_SEQ가 null이면서 initCust가 아닐 경우 수탁증 중복 조회
                	     int nextSeqVal = Integer.parseInt(String.valueOf(dao.getCompAccSeq(modelIns)));
                		 if(nextSeqVal !=-1 && !initCust.contains(String.valueOf(model.get("CUST_ID" + i)))){
                			 errCount++;
                			 errMessage += "\n 중복된 데이터가 있습니다 :" + modelIns.get("TRANS_CUST_NM")+ "/" + modelIns.get("CUST_NM");
                			 continue;
                		 }
                		 else if (nextSeqVal == -1){
                		     nextSeqVal = Integer.parseInt(String.valueOf(dao.getNextAccSeq(modelIns))) + 1;
                		 }
                		 modelIns.put("ACC_WORK_SEQ"     , nextSeqVal);
                	 }else{
                		 modelIns.put("ACC_WORK_SEQ"     , String.valueOf(model.get("ACC_WORK_SEQ" + i)));
                	 }
                	 //해당 화주사별 거래처가 있는지 확인
                	 if(Integer.parseInt(String.valueOf(dao.isTransCustId(modelIns)))==0){
            			 errCount++;
            			 errMessage += "\n 해당 화주별 거래처가 존재하지않습니다 : " + modelIns.get("TRANS_CUST_NM")+ "/" + modelIns.get("CUST_NM");
            			 continue;
            		 }
                	 
            		 modelIns.put("ACC_TYPE"     , "D");
            		 modelIns.put("CONF_YN",model.get("confYn"));
            		 
            		 dao.saveDeliveryConfHeaderList(modelIns);
	            	 
	            	 resultCount += dao.saveDeliveryConfList(modelIns);
	            	 modelIns.clear();
                 }
        	 }
        	m.put("resultCount",resultCount);
            m.put("errCnt", errCount);
            m.put("MSG", errCount+"건의 에러가 발생하였습니다."+errMessage);            
            
        }  catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
     * Method ID    : confirm
     * Method 설명      : 납품확인 메뉴 > 납품확정
     * 작성자                 : kimzero
     * @param   model
     * @return  
     * @throws Exception 
     */
    @Override
    public int confirm(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int resultCount = 0;
    	try{
    		int checkedRowCnt = Integer.parseInt(model.get("selectIds").toString());
    		
    		if (checkedRowCnt > 0) {
    			
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    			for(int i = 0 ; i < checkedRowCnt ; i ++){
					modelIns.put("ACC_WORK_ID"     , String.valueOf(model.get("ACC_WORK_ID" + i)));
					modelIns.put("ACC_WORK_SEQ"    , String.valueOf(model.get("ACC_WORK_SEQ" + i)));
    				modelIns.put("WORK_DT"         , String.valueOf(model.get("WORK_DT" + i)));
    				modelIns.put("TRANS_CUST_ID"   , String.valueOf(model.get("TRANS_CUST_ID" + i)));
    				modelIns.put("CUST_ID"         , String.valueOf(model.get("CUST_ID" + i)));
    				modelIns.put("CUST_BOX_QTY"         , String.valueOf(model.get("CUST_BOX_QTY" + i)));
    				
    				// session 및 등록정보
    				modelIns.put("LC_ID"			, String.valueOf(model.get(ConstantIF.SS_SVC_NO)));
    				modelIns.put("USER_NO"			, String.valueOf(model.get(ConstantIF.SS_USER_NO)));
    				modelIns.put("WORK_IP"			, String.valueOf(model.get(ConstantIF.SS_CLIENT_IP)));
    				
    				resultCount += dao.updateDeliveryConfirm(modelIns);
    				modelIns.clear();
    			}
    		}
    		
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));            
    		
    	}  catch(Exception e){
    		throw e;
    	}
    	return resultCount;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : UOM목록 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀업로드 저장
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.saveUploadData(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
}
