package com.logisall.winus.wmsac.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC270Dao")
public class WMSAC270Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : listE1 
	 * Method 설명 : 통합정산 조회
	 * 작성자 : 
	 * 
	 * @param model
	 * @return
	 */
	public List listE1(Map<String, Object> model) {
		return executeQueryForList("wmsac270.listE1", model);
	}
	
	/**
     * Method ID : listE2 
     * Method 설명 : 택배출고 정산 조회
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public List listE2(Map<String, Object> model) {
        return executeQueryForList("wmsac270.listE2", model);
    }
    
    /**
     * Method ID : listE3
     * Method 설명 : 입고 정산 조회
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public List listE3(Map<String, Object> model) {
        return executeQueryForList("wmsac270.listE3", model);
    }
    
    /**
     * Method ID : listE4
     * Method 설명 : 재고 정산 조회
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public List listE4(Map<String, Object> model) {
        return executeQueryForList("wmsac270.listE4", model);
    }
    
    /**
     * Method ID : listE5
     * Method 설명 : 반품 정산 조회
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public List listE5(Map<String, Object> model) {
        return executeQueryForList("wmsac270.listE5", model);
    }
    
    /**
     * Method ID : listE6
     * Method 설명 : 기타비 정산 조회
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public List listE6(Map<String, Object> model) {
        return executeQueryForList("wmsac270.listE6", model);
    }
    
    /**
     * Method ID : checkInvcNoValidation
     * Method 설명 : 송장정보 validation check
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object checkInvcNoValidation(Map<String, Object> model) {
        return executeQueryForObject("wmsac270.checkInvcNoValidation", model);
    }
    
    /**
     * Method ID : getBoxIdToCd
     * Method 설명 : 내부박스코드 ID 확인
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object getBoxIdToCd(Map<String, Object> model) {
        return executeQueryForObject("wmsac270.getBoxIdToCd", model);
    }
    
    /**
     * Method ID : setAddInvcBoxGubun
     * Method 설명 : 추가송장 gubun 정보 업데이트
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object setAddInvcBoxGubun(Map<String, Object> model) {
        return executeUpdate("wmsac270.setAddInvcBoxGubun", model);
    }
    
    /**
     * Method ID : confOutAccData
     * Method 설명 : 출고박스정보 저장
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object confOutAccData(Map<String, Object> model) {
        return executeUpdate("wmsac270.confOutAccData", model);
    }
    
    /**
     * Method ID : saveTypeSHeader
     * Method 설명 : 보관 내역 저장(Header)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object saveTypeSHeader(Map<String, Object> model) {
        return executeUpdate("wmsac270.saveTypeSHeader", model);
    }
    
    /**
     * Method ID : saveTypeSDetail
     * Method 설명 : 보관 내역 저장(Detail)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object saveTypeSDetail(Map<String, Object> model) {
        return executeUpdate("wmsac270.saveTypeSDetail", model);
    }
    
    /**
     * Method ID : saveTypeRHeader
     * Method 설명 : 반품 내역 저장(Header)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object saveTypeRHeader(Map<String, Object> model) {
        return executeUpdate("wmsac270.saveTypeRHeader", model);
    }
    
    /**
     * Method ID : saveTypeRDetail
     * Method 설명 : 반품 내역 저장(Detail)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object saveTypeRDetail(Map<String, Object> model) {
        return executeUpdate("wmsac270.saveTypeRDetail", model);
    }
    
    /**
     * Method ID : getAccWorkId
     * Method 설명 : 정산 내역 ID확인
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object getAccWorkId(Map<String, Object> model) {
        return executeQueryForObject("wmsac270.getAccWorkId", model);
    }
    
    /**
     * Method ID : saveTypeCHeader
     * Method 설명 : 기타비 내역 저장(Header)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object saveTypeCHeader(Map<String, Object> model) {
        return executeUpdate("wmsac270.saveTypeCHeader", model);
    }
    
    /**
     * Method ID : saveTypeCDetail
     * Method 설명 : 기타비 내역 저장(Detail)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object saveTypeCDetail(Map<String, Object> model) {
        return executeUpdate("wmsac270.saveTypeCDetail", model);
    }
    
    /**
     * Method ID : deleteTypeCHeader
     * Method 설명 : 정산 내역 삭제(Header)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object deleteAccHeader(Map<String, Object> model) {
        return executeUpdate("wmsac270.deleteAccHeader", model);
    }
    
    /**
     * Method ID : deleteAccDetail
     * Method 설명 : 정산 내역 삭제(Detail)
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object deleteAccDetail(Map<String, Object> model) {
        return executeUpdate("wmsac270.deleteAccDetail", model);
    }
    
    /**
     * Method ID : getFileUploadId
     * Method 설명 : 파일 업로드 ID 채번
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object getFileUploadId(Map<String, Object> model) {
        return executeQueryForObject("wmsac270.getFileUploadId", model);
    }
    
    /**
     * Method ID : getFileSaveInfo
     * Method 설명 : 파일 저장 정보 조회
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public List getFileSaveInfo(Map<String, Object> model) {
        return executeQueryForList("wmsac270.getFileSaveInfo", model);
    }
    
    /**
     * Method ID : deleteFileInfo
     * Method 설명 : 첨부파일 정보 삭제
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object deleteFileInfo(Map<String, Object> model) {
        return executeUpdate("wmsac270.deleteFileInfo", model);
    }
    
    /**
     * Method ID : saveFileInfo
     * Method 설명 : 첨부파일 정보 저장
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object saveFileInfo(Map<String, Object> model) {
        return executeInsert("wmsac270.saveFileInfo", model);
    }
}
