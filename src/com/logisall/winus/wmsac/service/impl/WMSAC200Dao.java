package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC200Dao")
public class WMSAC200Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 인건비내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {    	
        return executeQueryPageWq("wmsac200.list", model);
    }   
    
    
    /**
     * Method ID : list2
     * Method 설명 : 운송내역조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet detail(Map<String, Object> model) {
        return executeQueryPageWq("wmsac200.detail", model);
    }   
    
    /**
     * Method ID : list3
     * Method 설명 : 부자재사용내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet listPool(Map<String, Object> model) {
        return executeQueryPageWq("wmsac200.list3", model);
    } 
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 인건비내역 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insert", modelDt);
	}
	
	/**
     * Method ID : insert2
     * Method 설명 : 운송비내역 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert2(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insert2", modelDt);
	}
	
	/**
     * Method ID : insert3
     * Method 설명 : 부자재내역 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert3(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insert3", modelDt);
	}	
	
	/**
     * Method ID : list0
     * Method 설명 : 보관비 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public GenericResultSet list0(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.list0", model);
	}
	
	  
	/**
     * Method ID : list18
     * Method 설명 : 입출고비 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
	public GenericResultSet list18(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.list18", model);
	}
    
	
	/**
     * Method ID : update3
     * Method 설명 : 부자재사용 내역3
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void update3(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.update3", modelDt);
	}
	
	/**
     * Method ID : delete2
     * Method 설명 : 운송비 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete2(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac200.delete2", modelDt);
	}	
	
	/**
     * Method ID : update
     * Method 설명 : 인건비 내역 업데이트
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void update(Map<String, Object> modelDt) throws Exception{
		// TODO Auto-generated method stub
		executeDelete("wmsac200.update", modelDt);
	}
	
	/**
     * Method ID : update2
     * Method 설명 : 운송비 내역 업데이트
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void update2(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac200.update2", modelDt);
	}
	
	/**
     * Method ID : manage
     * Method 설명 : 관리비 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object manage(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.manage", model);
	}
	
	/**
     * Method ID : insert7
     * Method 설명 : 관리비, 입출고비, 장비사용비 저장
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert7(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insert7", modelDt);
	}
	
	/**
     * Method ID : update7
     * Method 설명 : 관리비, 입출고비, 장비사용비 내역 업데이트
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void update7(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac200.update7", modelDt);		
	}
	
	/**
     * Method ID : delete7
     * Method 설명 : 관리비, 입출고비, 장비사용비 내역 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete7(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmsac200.delete7", modelDt);
	}
	
	/**
     * Method ID : store
     * Method 설명 : 입출고비 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object store(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.store", model);
	}
	
	/**
     * Method ID : use
     * Method 설명 : 장비사용비 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object use(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.use", model);
	}
	
	/**
     * Method ID : add
     * Method 설명 : 부가서비스 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object add(Map<String, Object> model) throws Exception{
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.add", model);
	}
	
	/**
     * Method ID : deduct
     * Method 설명 : 공제 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object deduct(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.deduct", model);
	}
	
	/**
     * Method ID : ChkWMSAC200Cnt
     * Method 설명 : wmsac200 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.ChkWMSAC200Cnt", model);
	}
	
	
	
	/**
     * Method ID : ChkWMSAC204Cnt
     * Method 설명 : wmsac204 체크
     * 작성자 : yhku
     * @param model
     * @return
     */
	public int ChkWMSAC204Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.ChkWMSAC204Cnt", model);
	}
	
	
	/**
     * Method ID : wmsac200_list0
     * Method 설명 : wmsac200 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object wmsac200_list0(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.wmsac200_list0", model);
	}
	
	/**
     * Method ID : wmsac204_list
     * Method 설명 : wmsac204 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Object wmsac204_list(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.wmsac204_list", model);
	}
	
	/**
     * Method ID : moveToWMSAC200
     * Method 설명 : wmsac200 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC200(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.moveToWMSAC200", model);
	}
	
	/**
     * Method ID : moveToWMSAC204
     * Method 설명 : wmsac204 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Object moveToWMSAC204(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.moveToWMSAC204", model);
	}
	
	/**
     * Method ID : deleteToWMSAC200
     * Method 설명 : wmsac200 삭제
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Object deleteToWMSAC200(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeDelete("wmsac200.deleteToWMSAC200", model);
	}
	
	/**
     * Method ID : deleteToWMSAC204
     * Method 설명 : wmsac200 삭제
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Object deleteToWMSAC204(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeDelete("wmsac200.deleteToWMSAC204", model);
	}
	
	
	/**
     * Method ID : ChkWMAC200Cnt
     * Method 설명 : wmsac200 체크(건바이건)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkCntRow(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.ChkCntRow", model);
	}
	
	/**
     * Method ID : ChkWMAC204Cnt
     * Method 설명 : wmsac204 체크(건바이건)
     * 작성자 : yhku
     * @param model
     * @return
     */
	public int ChkCnt204Row(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.ChkCnt204Row", model);
	}
	
	
	/**
     * Method ID    : update12
     * Method 설명         : 수량 수정
     * 작성자                        : chsong
     * @param   model
     * @return
     */
    public Object update12(Map<String, Object> model) throws Exception {
        executeUpdate("wmsac200.pk_wmsst050.sp_modify_acc_stock", model);
        return model;
    }
    
    
    
    /**
     * Method ID : chkDataCnt
     * Method 설명 : 거래명세서 각 화면별 데이터 Cnt 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object chkDataCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryWq("wmsac200.chkDataCnt", model);
	}
	
	
	  /**
     * Method ID : chkData204Cnt
     * Method 설명 : 거래명세서 각 화면별 데이터 Cnt 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object chkData204Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryWq("wmsac200.chkData204Cnt", model);
	}
	

	
	/**
     * Method ID : chkWMAC202ParcelCnt
     * Method 설명 : 택배정산 건수 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
	public int chkWMAC202ParcelCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.chkWMAC202ParcelCnt", model);
	}
	
	
	/**
     * Method ID : delWMSAC202Parcel
     * Method 설명 : (운송비)택배정보 삭제)
     * 작성자 : yhku
     * @param model
     * @return
     */
	public void delWMSAC202Parcel(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac200.delWMSAC202Parcel", modelDt);
	}
	
	
	/**
     * Method ID : moveToWMSAC202Parcel
     * Method 설명 : 택배정산 운송 데이터로 저장
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Object moveToWMSAC202Parcel(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.moveToWMSAC202Parcel", model);
	}
	
	
	
	/**
     * Method ID : ChkWMSAC202Cnt
     * Method 설명 : 운송테이블 데이터 건수 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC202Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.ChkWMAC202Cnt", model);
	}
	
	/**
     * Method ID : moveToWMSAC202
     * Method 설명 : 운송테이블 데이터 저장
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC202(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.moveToWMSAC202", model);
	}
	
	
	/**
     * Method ID : 택배정산자료 조회
     * Method 설명 : 
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Object list19(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.list19", model);
	}
	
	
	
	/**
     * Method ID : CJ택배 조회(남동공단 운송)
     * Method 설명 : CJ택배 조회(남동공단 운송)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list14(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.list14", model);
	}
	
	/**
     * Method ID : 인건비 삭제
     * Method 설명 : 인건비 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac200.delete", modelDt);		
	}
	
	/**
     * Method ID : 부자재비 삭제
     * Method 설명 : 부자재비 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete3(Map<String, Object> modelDt) throws Exception{
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		executeDelete("wmsac200.delete3", modelDt);
	}
	
	/**
     * Method ID : 반품내역 조회
     * Method 설명 : 반품내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list15(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.list15", model);
	}
	
	/**
     * Method ID : 도선료, 제주운임, 기타운임 조회
     * Method 설명 : 도선료, 제주운임, 기타운임 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void update14(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.update14", modelDt);
	}
	
	

	/**
     * Method ID : 운임 수정
     * Method 설명 : 운임 수정
     * 작성자 : yhku
     * @param model
     * @return
     */
	public void update19(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.update19", modelDt);
	}
	
	/**
     * Method ID : 택배 등록 Del
     * Method 설명 : 
     * 작성자 : yhku
     * @param model
     * @return
     */
	public void updateDel14(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.updateDel14", modelDt);
	}
	
	/**
     * Method ID : 택배정산내역서 Del
     * Method 설명 : 
     * 작성자 : yhku
     * @param model
     * @return
     */
	public void updateDel19(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.updateDel19", modelDt);
	}
	
	/**
     * Method ID : 택배내역 유무 체크
     * Method 설명 : 택배내역 유무 체크
     * 작성자 : yhku
     * @param model
     * @return
     */
	public int ChkWMSAC205_14Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.ChkWMSAC205_14Cnt", model);
	}
	
	/**
     * Method ID : 반품내역 등록
     * Method 설명 : 반품내역 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert15(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insert15", modelDt);
	}
	
	/**
     * Method ID : 반품내역 수정
     * Method 설명 : 반품내역 수정
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object update15(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.update15", modelDt);
	}
	
	/**
     * Method ID : 반품내역 삭제
     * Method 설명 : 반품내역 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object delete15(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.delete15", modelDt);
	}
	
	/**
     * Method ID : 합포장내역 조회(저장전목록)
     * Method 설명 : 합포장내역 조회(저장전목록)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list16(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.list16", model);
	}
	
	/**
     * Method ID : 합포장내역 저장 유무 체크
     * Method 설명 : 합포장내역 저장 유무 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC205Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.ChkWMAC205Cnt", model);
	}
	
	/**
     * Method ID : 합포장내역 조회(저장후목록)
     * Method 설명 : 합포장내역 조회(저장후목록)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object wmsac205_list16(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac200.wmsac205_list16", model);
	}
	
	/**
     * Method ID : 합포장내역 삭제(저장목록)
     * Method 설명 : 합포장내역 삭제(저장목록)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete205_pack(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac200.delete205_pack", model);
		
	}
	
	/**
     * Method ID : 합포장내역 저장
     * Method 설명 : 합포장내역 저장
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC205(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.moveToWMSAC205", model);
		
	}
	
	/**
     * Method ID : 이미지 파일 정보 저장
     * Method 설명 : 이미지 파일 정보 저장
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insertInfo(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.ImgInfoSave", model);
	}
	
	/**
     * Method ID : 이미지 파일 정보 저장
     * Method 설명 : 이미지 파일 정보 저장
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object fileUpload(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("tmsys900.fileInsert", modelDt);
	}
	
	/**
     * Method ID : 이미지 파일 정보 저장(인건비)
     * Method 설명 : 이미지 파일 정보 저장(인건비)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insertInfo_201(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insertInfo_201", model);
	}
	
	/**
     * Method ID : 이미지 파일 정보 저장(운송비)
     * Method 설명 : 이미지 파일 정보 저장(운송비)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insertInfo_202(Map<String, Object> model) throws Exception {		
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insertInfo_202", model);
	}
	
	/**
     * Method ID : 이미지 파일 정보 저장(부자재비)
     * Method 설명 : 이미지 파일 정보 저장(부자재비)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insertInfo_203(Map<String, Object> model) {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insertInfo_203", model);		
	}
	
	/**
     * Method ID : saveReport
     * Method 설명 : saveReport
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Map<String, Object> saveReport(Map<String, Object> modelIns) {
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.pk_wmsac100.sp_save_trxbill", modelIns);
        return modelIns;
	}
	
	
	/**
     * Method ID : deleteReport
     * Method 설명 : deleteReport
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Map<String, Object> deleteReport(Map<String, Object> modelIns) {
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.deleteReport", modelIns);
        return modelIns;
	}
	
	/**
     * Method ID : saveTaxInfo
     * Method 설명 : saveTaxInfo
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Map<String, Object> saveTaxInfo(Map<String, Object> modelIns) {
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.pg_tax_bill_sale.tax_sav", modelIns);
        return modelIns;
	}

	/**
     * Method ID : insert2Excel
     * Method 설명 : 운송비내역 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert2Excel(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac200.insert2Excel", modelDt);
	}
	
	/**
     * Method ID : selectTaxInfo
     * Method 설명 : 세금계산서  발행 정보
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public GenericResultSet selectTaxInfo(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryWq("wmsac200.selectTaxInfo", model);
	}
	
	/**
     * Method ID : selectTaxHDCnt
     * Method 설명 : 운송비내역 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int selectTaxHDCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.selectTaxHDCnt", model);
	}
	
	/**
     * Method ID : selectIssueEmp
     * Method 설명 : 발행자 사번 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public String selectIssueEmp(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (String) executeQueryForObject("wmsac200.selectIssueEmp", model);
	}
	
	/**
     * Method ID : selectIssueEmpAC16
     * Method 설명 : 수금담당자 사번 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
	public GenericResultSet selectIssueEmpAC16(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryWq("wmsac200.selectIssueEmpAC16", model);
	}
	
	
	/**
     * Method ID : updatetaxInfo
     * Method 설명 : 세금계산서 발행 이력 정보 업데이트(성공)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void updateTaxInfo(Map<String, Object> model) throws Exception {		
		// TODO Auto-generated method stub
		executeUpdate("wmsac200.updateTaxInfo", model);
	}

	/**
     * Method ID : chkTaxCnt
     * Method 설명 : chkTaxCnt
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int chkTaxCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac200.chkTaxCnt", model);
	}
}