package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsac.service.WMSAC020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC020Service")
public class WMSAC020ServiceImpl extends AbstractServiceImpl implements WMSAC020Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

   @Resource(name = "WMSAC020Dao")
    private WMSAC020Dao dao;

   /**
    * Method ID    : list
    * Method 설명      : 정산산출 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
        
    /**
     * 
     * Method ID   : save
     * Method 설명    : 정산산출 저장
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 1;
        String errMsg = MessageResolver.getMessage("save.error");
        try{
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("vrSrchReqDtFrom",  model.get("vrSrchReqDtFrom").toString().replace("-", ""));
            modelIns.put("vrSrchReqDtTo",  model.get("vrSrchReqDtTo").toString().replace("-", ""));
            
            modelIns.put("gvLcId", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.save(modelIns);
            errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
            errMsg = modelIns.get("O_MSG_NAME").toString();
            if(errCnt == 0){
                errMsg = MessageResolver.getMessage("save.success");
            }
            m.put("errCnt", errCnt);
            m.put("MSG", errMsg);
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID    : listExcel
     * Method 설명      : 정산산출 조회 엑셀
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         model.put("pageIndex", "1");
         model.put("pageSize", "60000");
         map.put("LIST", dao.list(model));
         return map;
     }
     
     /**
      * Method ID    : listSub
      * Method 설명      : 정산산출 상세 조회
      * 작성자                 : chsong
      * @param   model
      * @return 
      * @throws Exception 
      */
      @Override
      public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
          Map<String, Object> map = new HashMap<String, Object>();
          if(model.get("page") == null) {
              model.put("pageIndex", "1");
          } else {
              model.put("pageIndex", model.get("page"));
          }
          if(model.get("rows") == null) {
              model.put("pageSize", "20");
          } else {
              model.put("pageSize", model.get("rows"));
          }
          map.put("LIST", dao.listSub(model));
          return map;
      }
}
