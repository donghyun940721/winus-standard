package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC210Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC210Service")
public class WMSAC210ServiceImpl extends AbstractServiceImpl implements WMSAC210Service {
	protected Log loger = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC210Dao")
	private WMSAC210Dao dao;

   /**
    * 
    * 대체 Method ID   : list
    * 대체 Method 설명    : 고객관리 조회
    * 작성자                      : chsong
    * @param   model
    * @return
    * @throws  Exception
    */
   @Override
   public Map<String, Object> list(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       if(model.get("page") == null) {
           model.put("pageIndex", "1");
       } else {
           model.put("pageIndex", model.get("page"));
       }
       if(model.get("rows") == null) {
           model.put("pageSize", "20");
       } else {
           model.put("pageSize", model.get("rows"));
       }
       map.put("LIST", dao.list(model));
       return map;
   }
   
   /**
    * 대체 Method ID   : listExcel
    * 대체 Method 설명 : 고객관리 엑셀.
    * 작성자      : chsong
    * @param model
    * @return
    * @throws Exception
    */
   public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       
       model.put("pageIndex", "1");
       model.put("pageSize", "60000");
       
       map.put("LIST", dao.list(model));
       
       return map;
   }
}
