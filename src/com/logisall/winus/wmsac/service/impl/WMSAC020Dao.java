package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC020Dao")
public class WMSAC020Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
   /**
    * Method ID    : list
    * Method 설명      : 정산산출 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsac020.list", model);
    }
    
    /**
     * Method ID    : save
     * Method 설명      : 정산산출
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsac010.pk_wmsac010.sp_calc_period", model);
        return model;
    }
	
    /**
     * Method ID    : listSub
     * Method 설명      : 정산산출상세 조회
     * 작성자                 : chsong
     * @param   model
     * @return  
     */
     public GenericResultSet listSub(Map<String, Object> model) {
         return executeQueryPageWq("wmsac021.listSub", model);
     }
}
