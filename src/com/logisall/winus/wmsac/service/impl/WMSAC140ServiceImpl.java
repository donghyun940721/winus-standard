package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC140Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC140Service")
public class WMSAC140ServiceImpl extends AbstractServiceImpl implements WMSAC140Service {
	protected Log loger = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC140Dao")
	private WMSAC140Dao dao;

   /**
    * 
    * 대체 Method ID   : list
    * 대체 Method 설명    : 고객관리 조회
    * 작성자                      : chsong
    * @param   model
    * @return
    * @throws  Exception
    */
   @Override
   public Map<String, Object> list(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       if(model.get("page") == null) {
           model.put("pageIndex", "1");
       } else {
           model.put("pageIndex", model.get("page"));
       }
       if(model.get("rows") == null) {
           model.put("pageSize", "20");
       } else {
           model.put("pageSize", model.get("rows"));
       }
       map.put("LIST", dao.list(model));
       return map;
   }
   
   /**
    * 
    * 대체 Method ID   : save
    * 대체 Method 설명    : 
    * 작성자                      : chsong
    * @param model
    * @return
    * @throws Exception
    */
   @Override
   public Map<String, Object> save(Map<String, Object> model) throws Exception {
   	Map<String, Object> m = new HashMap<String, Object>();
       // log.info(model);
       try{
       	int insCnt = Integer.parseInt(model.get("selectIds").toString());
       	if(insCnt > 0){
       		//저장, 수정         
       		String[] stGubun        = new String[insCnt];
       		String[] salesCustId    = new String[insCnt];
       		String[] adjuctCalDt    = new String[insCnt];
       		String[] sourcePrice    = new String[insCnt];
       		String[] salesPrice     = new String[insCnt];
       		
       		String[] adjuctFeeDt    = new String[insCnt];
       		String[] serviceCharge  = new String[insCnt];

       		for(int i = 0 ; i < insCnt ; i ++){        
       			stGubun[i]       = (String)model.get("ST_GUBUN"+i);
       			salesCustId[i]   = (String)model.get("SALES_CUST_ID"+i);
       			adjuctCalDt[i]   = (String)model.get("ADJUCT_CAL_DT"+i);
       			sourcePrice[i]   = (String)model.get("SOURCE_PRICE"+i);
       			salesPrice[i]    = (String)model.get("SALES_PRICE"+i);
       			
       			adjuctFeeDt[i]   = (String)model.get("ADJUCT_FEE_DT"+i);
       			serviceCharge[i] = (String)model.get("SERVICE_CHARGE"+i);
       		}
       		   
       		//프로시져에 보낼것들 다담는다
       		Map<String, Object> modelIns = new HashMap<String, Object>();
       		modelIns.put("I_ST_GUBUN"        , stGubun);
       		modelIns.put("I_SALES_CUST_ID"   , salesCustId);
       		modelIns.put("I_ADJUCT_CAL_DT"   , adjuctCalDt);
       		modelIns.put("I_SOURCE_PRICE"    , sourcePrice);
       		modelIns.put("I_SALES_PRICE"     , salesPrice);
       		
       		modelIns.put("I_ADJUCT_FEE_DT"   , adjuctFeeDt);
       		modelIns.put("I_SERVICE_CHARGE"  , serviceCharge);

       		//session 정보
       		modelIns.put("I_LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
       		modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
       		modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
       		
       		modelIns = (Map<String, Object>)dao.save(modelIns);
       	    ServiceUtil.isValidReturnCode("WMSAC140", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
       	}

       	m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
 
       } catch(BizException be) {
           m.put("errCnt", 1);
           m.put("MSG", be.getMessage() );
           
       } catch(Exception e){
           throw e;
       }
       return m;
   }
   
   /**
    * 대체 Method ID   : listExcel
    * 대체 Method 설명 : 고객관리 엑셀.
    * 작성자      : chsong
    * @param model
    * @return
    * @throws Exception
    */
   public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       
       model.put("pageIndex", "1");
       model.put("pageSize", "60000");
       
       map.put("LIST", dao.list(model));
       
       return map;
   } 
   
   /**
    * Method ID : saveUploadData
    * Method 설명 : 엑셀업로드 저장
    * 작성자 : kwt
    * @param model
    * @return
    * @throws Exception
    */
   public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       int errCnt = 0;
       int insertCnt = (list != null)?list.size():0;
           try{            	
               dao.saveUploadData(model, list);
               
               m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
               m.put("MSG_ORA", "");
               m.put("errCnt", errCnt);
               
           } catch(Exception e){
               throw e;
           }
       return m;
   }
}
