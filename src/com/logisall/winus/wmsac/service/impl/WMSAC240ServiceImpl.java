package com.logisall.winus.wmsac.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsac.service.WMSAC240Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSAC240Service")
public class WMSAC240ServiceImpl implements WMSAC240Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAC240Dao")
    private WMSAC240Dao dao;

    
    /**
     * 
     * 대체 Method ID   : listHeader
     * 대체 Method 설명    : 화물수탁증 입력/조회 헤더그리드 조회
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listHeader(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        String vrLcId = (String)model.get(ConstantIF.SS_SVC_NO);
        ArrayList<String> vrSrchLcId = new ArrayList<String>();
        //대화물류 전체 센터 조회
        if (vrLcId.equals("0000003541") || vrLcId.equals("0000003721") || vrLcId.equals("0000003722") || vrLcId.equals("0000003720")){
            vrSrchLcId.add("0000003541");
            vrSrchLcId.add("0000003721");
            vrSrchLcId.add("0000003722");
            vrSrchLcId.add("0000003720");
        }
        else {
            vrSrchLcId.add(vrLcId);
        }
        model.put("vrSrchLcId", vrSrchLcId);
        map.put("LIST", dao.listHeader(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listDetail
     * 대체 Method 설명    : 화물수탁증 입력/조회 디테일 그리드 조회
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        String vrLcId = (String)model.get(ConstantIF.SS_SVC_NO);
        ArrayList<String> vrSrchLcId = new ArrayList<String>();
        //대화물류 전체 센터 조회
        if (vrLcId.equals("0000003541") || vrLcId.equals("0000003721") || vrLcId.equals("0000003722") || vrLcId.equals("0000003720")){
            vrSrchLcId.add("0000003541");
            vrSrchLcId.add("0000003721");
            vrSrchLcId.add("0000003722");
            vrSrchLcId.add("0000003720");
        }
        else {
            vrSrchLcId.add(vrLcId);
        }
        model.put("vrSrchLcId", vrSrchLcId);
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : ACCTypeList
     * 대체 Method 설명    : 기타비정산코드 조회
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> ACCTypeCList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.ACCTypeCList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listTypeC
     * 대체 Method 설명    : 기타비정산 그리드 조회
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listTypeC(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listTypeC(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveHeader
     * 대체 Method 설명    : 화물수탁증 입력/조회 헤더그리드 저장
     * 작성자                      : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveHeader(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		ArrayList<String> accWorkIdArr = new ArrayList<String>();
		try{
			int insCnt = Integer.parseInt(model.get("selectIds").toString());
			String vrAccType = (String)model.get("vrAccType"); 
			if (insCnt > 0) {
			    String vrLcId = (String)model.get(ConstantIF.SS_SVC_NO);
		        ArrayList<String> vrSrchLcId = new ArrayList<String>();
		        //대화물류 전체 센터 조회
		        if (vrLcId.equals("0000003541") || vrLcId.equals("0000003721") || vrLcId.equals("0000003722") || vrLcId.equals("0000003720")){
		            vrSrchLcId.add("0000003541");
		            vrSrchLcId.add("0000003721");
		            vrSrchLcId.add("0000003722");
		            vrSrchLcId.add("0000003720");
		            vrLcId = "0000003541";
		        }
		        else {
		            vrSrchLcId.add(vrLcId);
		        }
				//저장, 수정
				for(int i = 0 ; i < insCnt ; i ++){
					String flag = (String)model.get("SPDCOL_FLAG"+i);
					int compAccCount = 0;
					// insert
					if(flag.equals("A")){
						Map<String, Object> modelIns = new HashMap<String, Object>();
						String vrWorkDt = (String)model.get("WORK_DT"+i);
						String vrCustId = (String)model.get("CUST_ID"+i);
						String vrMemo1 = (String)model.get("MEMO1"+i);
						String vrCustCd = (String)model.get("CUST_CD"+i);
						modelIns.put("vrWorkDt", vrWorkDt);
						modelIns.put("vrCustId", vrCustId);//사용X
						modelIns.put("vrMemo1", vrMemo1);
						modelIns.put("vrCustCd", vrCustCd);
						modelIns.put("vrSrchLcId", vrSrchLcId);
						modelIns.put("LC_ID", vrLcId);
						modelIns.put("vrAccType", vrAccType);
						modelIns.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
						modelIns.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
						//중복데이터 조회
						compAccCount = (int)dao.getcompAccCount(modelIns);
						if(compAccCount > 0){
							m.put("errCnt", -1);
							m.put("MSG", "해당 수탁증이 존재합니다.");
							return m;
						}
						//헤더 그리드 저장
						String vrAccWorkId = (String) dao.insertHeader(modelIns);
						modelIns.put("ACC_WORK_ID", vrAccWorkId);
						accWorkIdArr.add(vrAccWorkId);
						//디테일 그리드 저장
						dao.initDetail(modelIns);
					}
					// update
					else if(flag.equals("E")){
						Map<String, Object> modelIns = new HashMap<String, Object>();
						String vrWorkDt = (String)model.get("WORK_DT"+i);
						String vrCustId = (String)model.get("CUST_ID"+i);
						String vrMemo1 = (String)model.get("MEMO1"+i);
						String vrCustCd = (String)model.get("CUST_CD"+i);
						String vrAccWorkId = (String)model.get("ACC_WORK_ID"+i);
						String lcId = (String)model.get("LC_ID"+i);
						accWorkIdArr.add(vrAccWorkId);
						modelIns.put("vrWorkDt", vrWorkDt);
						modelIns.put("vrCustId", vrCustId);
						modelIns.put("vrMemo1", vrMemo1);
						modelIns.put("vrCustCd", vrCustCd);
						modelIns.put("vrAccWorkId", vrAccWorkId);
						modelIns.put("vrSrchLcId", vrSrchLcId);
                        modelIns.put("LC_ID", lcId);
						modelIns.put("vrAccType", vrAccType);
						modelIns.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
						modelIns.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
						//중복데이터 조회
						compAccCount = (int)dao.getcompAccCount(modelIns);
						if(compAccCount > 0){
							m.put("errCnt", -1);
							m.put("MSG", "해당 수탁증이 존재합니다.");
							return m;
						}
						//헤더 그리드 저장
						dao.updateHeader(modelIns);
						//디테일 그리드 저장
						dao.updateHeaderDetail(modelIns);
					}
				}
			}
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("save.success"));
			m.put("accWorkIdArr", accWorkIdArr);
		} catch(Exception e){
			m.put("errCnt", -1);
			m.put("MSG", e.getMessage());
			throw e;
		}
		return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveDetail
     * 대체 Method 설명    : 화물수탁증 입력/조회 디테일그리드 저장
     * 작성자                      : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveDetail(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try{
			int insCnt = Integer.parseInt(model.get("selectIds").toString());
			if (insCnt > 0) {
				//수정
				for(int i = 0 ; i < insCnt ; i ++){
					// update
					Map<String, Object> modelIns = new HashMap<String, Object>();
					String vrCustBoxQty = (String)model.get("CUST_BOX_QTY"+i);
					String vrCustPltQty = (String)model.get("CUST_PLT_QTY"+i);
					String vrCustCarPltQty = (String)model.get("CUST_CAR_PLT_QTY"+i);
					String vrCustUploadPltQty = (String)model.get("CUST_UPLOAD_PLT_QTY"+i);
					String vrCustDownloadPltQty = (String)model.get("CUST_DOWNLOAD_PLT_QTY"+i);
					
					String vrSalesCost = (String)model.get("SALES_COST"+i);
					String vrMemo2 = (String)model.get("MEMO2"+i);
					String vrAccWorkId = (String)model.get("ACC_WORK_ID"+i);
					String vrAccWorkSeq = (String)model.get("ACC_WORK_SEQ"+i);
					String vrCustId = (String)model.get("CUST_ID"+i);
					
					String vrTransCustId = (String)model.get("TRANS_CUST_ID"+i);
					String vrWorkDt = (String)model.get("WORK_DT"+i);
					String lcId = (String)model.get("LC_ID"+i);
					
					modelIns.put("vrCustBoxQty", vrCustBoxQty);
					modelIns.put("vrCustPltQty", vrCustPltQty);
					modelIns.put("vrCustCarPltQty", vrCustCarPltQty);
					modelIns.put("vrCustUploadPltQty", vrCustUploadPltQty);
					modelIns.put("vrCustDownloadPltQty", vrCustDownloadPltQty);
					
					modelIns.put("vrSalesCost", vrSalesCost);
					modelIns.put("vrMemo2", vrMemo2);
					modelIns.put("vrAccWorkId", vrAccWorkId);
					modelIns.put("vrAccWorkSeq", vrAccWorkSeq);
					modelIns.put("vrCustId", vrCustId);
					
					modelIns.put("vrTransCustId", vrTransCustId);
					modelIns.put("vrWorkDt", vrWorkDt);
					
					modelIns.put("LC_ID", lcId);
					modelIns.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
					modelIns.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
					//헤더 그리드 저장
					dao.updateHeader(modelIns);
					//디테일 그리드 저장
					dao.updateDetail(modelIns);
				}
			}
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch(Exception e){
			m.put("errCnt", -1);
			m.put("MSG", e.getMessage());
			throw e;
		}
		return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveTypeC
     * 대체 Method 설명    : 기타비 정산 그리드 저장
     * 작성자                      : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveTypeC(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try{
			int insCnt = Integer.parseInt(model.get("selectIds").toString());
			String vrAccType = (String)model.get("vrAccType"); 
			if (insCnt > 0) {
				//저장, 수정
				for(int i = 0 ; i < insCnt ; i ++){
					String flag = (String)model.get("SPDCOL_FLAG"+i);
					
					Map<String, Object> valdateModel = new HashMap<String, Object>();
					valdateModel.put("CUST_ID",(String)model.get("CUST_ID"+i));
					valdateModel.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
					
					int cnt = (int)dao.checkValidationCustID(valdateModel);
					
					if(cnt < 1){
					    throw new Exception("세션정보가 일치하지않습니다.");
					}
					
					// insert
					if(flag.equals("A")){
						Map<String, Object> modelIns = new HashMap<String, Object>();
						String vrWorkDt = (String)model.get("WORK_DT"+i);
						String vrCustId = (String)model.get("CUST_ID"+i);
						String vrMemo1 = (String)model.get("MEMO1"+i);
						String vrMemo2 = (String)model.get("MEMO2"+i);
						String vrAccWorkSeq = (String)model.get("ACC_WORK_SEQ"+i);
						String vrSalesMemo = (String)model.get("SALES_MEMO"+i);
						//String vrSalesCost = (String)model.get("SALES_COST"+i);
						String vrCustBoxQty = (String)model.get("CUST_BOX_QTY"+i);
						String vrCustPltQty = (String)model.get("CUST_PLT_QTY"+i);
						
						modelIns.put("vrWorkDt", vrWorkDt);
						modelIns.put("vrCustId", vrCustId);
						modelIns.put("vrMemo1", vrMemo1);
						modelIns.put("vrMemo2", vrMemo2);
						modelIns.put("ACC_WORK_SEQ", vrAccWorkSeq);
						modelIns.put("SALES_MEMO", vrSalesMemo);
						//modelIns.put("SALES_COST", vrSalesCost);
						modelIns.put("CUST_BOX_QTY", vrCustBoxQty);
						modelIns.put("CUST_PLT_QTY", vrCustPltQty);
						modelIns.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
						modelIns.put("vrAccType", vrAccType);
						modelIns.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
						modelIns.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
			
						dao.insertTypeC(modelIns);
					}
					// update
					else if(flag.equals("E")){
						Map<String, Object> modelIns = new HashMap<String, Object>();
						String vrWorkDt = (String)model.get("WORK_DT"+i);
						String vrCustId = (String)model.get("CUST_ID"+i);
						String vrMemo1 = (String)model.get("MEMO1"+i);
						String vrMemo2 = (String)model.get("MEMO2"+i);
						String vrAccWorkSeq = (String)model.get("ACC_WORK_SEQ"+i);
						String vrSalesMemo = (String)model.get("SALES_MEMO"+i);
						//String vrSalesCost = (String)model.get("SALES_COST"+i);
						String vrCustBoxQty = (String)model.get("CUST_BOX_QTY"+i);
						String vrCustPltQty = (String)model.get("CUST_PLT_QTY"+i);
						String vrAccWorkId = (String)model.get("ACC_WORK_ID"+i);
						
						
						modelIns.put("vrWorkDt", vrWorkDt);
						modelIns.put("vrCustId", vrCustId);
						modelIns.put("vrMemo1", vrMemo1);
						modelIns.put("vrMemo2", vrMemo2);
						modelIns.put("vrAccWorkId", vrAccWorkId);
						modelIns.put("ACC_WORK_SEQ", vrAccWorkSeq);
						modelIns.put("SALES_MEMO", vrSalesMemo);
						modelIns.put("CUST_BOX_QTY", vrCustBoxQty);
						modelIns.put("CUST_PLT_QTY", vrCustPltQty);
						modelIns.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
						modelIns.put("vrAccType", vrAccType);
						modelIns.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
						modelIns.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
						
						dao.updateTypeC1(modelIns);
						dao.updateTypeC2(modelIns);
					}
					// delete
					else if(flag.equals("D")){
						Map<String, Object> modelIns = new HashMap<String, Object>();
						String vrAccWorkSeq = (String)model.get("ACC_WORK_SEQ"+i);
						String vrAccWorkId = (String)model.get("ACC_WORK_ID"+i);
						
						modelIns.put("vrAccWorkId", vrAccWorkId);
						modelIns.put("ACC_WORK_SEQ", vrAccWorkSeq);
						modelIns.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
						modelIns.put("vrAccType", vrAccType);
						modelIns.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
						modelIns.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
						
						dao.deleteTypeC1(modelIns);
						dao.deleteTypeC2(modelIns);
					}
					// confirm
					else if(flag.equals("C")){
						Map<String, Object> modelIns = new HashMap<String, Object>();
						String vrAccWorkSeq = (String)model.get("ACC_WORK_SEQ"+i);
						String vrAccWorkId = (String)model.get("ACC_WORK_ID"+i);
						
						modelIns.put("vrAccWorkId", vrAccWorkId);
						modelIns.put("ACC_WORK_SEQ", vrAccWorkSeq);
						modelIns.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
						modelIns.put("vrAccType", vrAccType);
						modelIns.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
						modelIns.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
						
						dao.confirmTypeC1(modelIns);
						dao.confirmTypeC2(modelIns);
					}
				}
			}
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch(Exception e){
			m.put("errCnt", -1);
			m.put("MSG", e.getMessage());
			throw e;
		}
		return m;
    }
    
    /**
     * 대체 Method ID   : listExcelHeader
     * 대체 Method 설명 : 대화물류 화물수탁증 입력/조회 헤더 엑셀 다운
     * 작성자      : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapModel = new HashMap<String, Object>();
        
        String vrLcId = (String)model.get(ConstantIF.SS_SVC_NO);
        ArrayList<String> vrSrchLcId = new ArrayList<String>();
        //대화물류 전체 센터 조회
        if (vrLcId.equals("0000003541") || vrLcId.equals("0000003721") || vrLcId.equals("0000003722") || vrLcId.equals("0000003720")){
            vrSrchLcId.add("0000003541");
            vrSrchLcId.add("0000003721");
            vrSrchLcId.add("0000003722");
            vrSrchLcId.add("0000003720");
        }
        else {
            vrSrchLcId.add(vrLcId);
        }
        mapModel.put("vrSrchLcId", vrSrchLcId);
        mapModel.put("ACC_TYPE", model.get("ACC_TYPE"));
        String type = (String)model.get("EXCEL_TYPE");
        int insCnt = Integer.parseInt(model.get("selectIds").toString());
        if (insCnt > 0) {
			//수정
            ArrayList arrAccWork = new ArrayList();
			for(int i = 0 ; i < insCnt ; i ++){
				Map<String, Object> modelIns = new HashMap<String, Object>();
				modelIns.put("WORK_DT",(String) model.get("WORK_DT"+i));
				modelIns.put("ACC_WORK_ID",(String) model.get("ACC_WORK_ID"+i));
				modelIns.put("CUST_ID",(String) model.get("CUST_ID"+i));
				arrAccWork.add(modelIns);
			}
			mapModel.put("arrAccWork",arrAccWork);
		}
        if(type.equals("Header")){
        	map.put("LIST", dao.listExcelHeader(mapModel));
		}
		else if(type.equals("Detail")){
			map.put("LIST", dao.listExcelDetail(mapModel));
		}
		else{
			return map;
		}
        return map;
    }
    
    
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀업로드 저장
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                //dao.saveUploadData(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
}
