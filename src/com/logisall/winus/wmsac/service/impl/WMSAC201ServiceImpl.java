package com.logisall.winus.wmsac.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC201Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC201Service")
public class WMSAC201ServiceImpl extends AbstractServiceImpl implements WMSAC201Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAC201Dao")
    private WMSAC201Dao dao;

   /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
	/**
     * Method ID : saveExcelInfo
     * Method 설명 : 엑셀읽기저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	int insertCnt = (list != null)?list.size():0;
            if(insertCnt > 0){
            	String[] SEQ 					= new String[insertCnt];
            	String[] DTDS_BUSINESS_NM		= new String[insertCnt];
            	String[] DTDS_BUSINESS_CD       = new String[insertCnt];
            	String[] DTDS_OFFICE_NM         = new String[insertCnt];
            	String[] DTDS_OFFICE_CD         = new String[insertCnt];
            	String[] DTDS_CLT_DLV_NM        = new String[insertCnt];
            	String[] DTDS_CLT_DLV_CD        = new String[insertCnt];
            	String[] INVC_NO                = new String[insertCnt];
            	String[] FRT_DV_CD              = new String[insertCnt];
            	String[] WORK_DV_CD             = new String[insertCnt];
            	
            	String[] RCPT_DV                = new String[insertCnt];
            	String[] DTDS_CLT_OFFICE_NM     = new String[insertCnt];
            	String[] DTDS_CLT_SITE_NM       = new String[insertCnt];
            	String[] DTDS_CLT_SITE_CD       = new String[insertCnt];
            	String[] DTDS_CLT_WORKER_CD     = new String[insertCnt];
            	String[] DTDS_CLT_WORKER_NM     = new String[insertCnt];
            	String[] DTDS_DLV_OFFICE_NM     = new String[insertCnt];
            	String[] DTDS_DLV_SITE_NM       = new String[insertCnt];
            	String[] DTDS_DLV_SITE_CD       = new String[insertCnt];
            	String[] DTDS_DLV_WORKER_CD     = new String[insertCnt];
            	
            	String[] DTDS_DLV_WORKER_NM     = new String[insertCnt];
            	String[] SENDR_NM               = new String[insertCnt];
            	String[] SENDR_ADDR             = new String[insertCnt];
            	String[] SENDR_TEL_NO1          = new String[insertCnt];
            	String[] RCVR_NM                = new String[insertCnt];
            	String[] RCVR_ADDR              = new String[insertCnt];
            	String[] RCVR_TEL_NO1           = new String[insertCnt];
            	String[] GDS_QTY                = new String[insertCnt];
            	String[] ETC01                  = new String[insertCnt];
            	String[] GDS_NM                 = new String[insertCnt];
            	String[] DEFAULT_FRT_COST       = new String[insertCnt];
            	
            	String[] SHIPMENT_FRT_COST      = new String[insertCnt];
            	String[] DLV_JEJU_FRT_COST      = new String[insertCnt];
            	String[] ETC_FRT_COST           = new String[insertCnt];
            	String[] TOTAL_FRT_COST         = new String[insertCnt];
            	String[] COD_COST               = new String[insertCnt];
            	String[] ORI_ORD_NO             = new String[insertCnt];
            	String[] CLT_DT                 = new String[insertCnt];
            	String[] DLV_DT                 = new String[insertCnt];
            	String[] RECEIVER_NM            = new String[insertCnt];
            	String[] FRT_CUST_NM            = new String[insertCnt];
            	
            	String[] FRT_CUST_CD            = new String[insertCnt];
            	String[] SND_CUST_NM            = new String[insertCnt];
            	String[] SND_CUST_CD            = new String[insertCnt];
            	
                Map<String, Object> paramMap = null;
                for (int i=0;i<list.size();i++) {
            		paramMap = (Map)list.get(i);
            		SEQ[i]					= (String)paramMap.get("O_SEQ");
            		DTDS_BUSINESS_NM[i]		= (String)paramMap.get("O_DTDS_BUSINESS_NM");
            		DTDS_BUSINESS_CD[i]		= (String)paramMap.get("O_DTDS_BUSINESS_CD");
            		DTDS_OFFICE_NM[i]		= (String)paramMap.get("O_DTDS_OFFICE_NM");
            		DTDS_OFFICE_CD[i]		= (String)paramMap.get("O_DTDS_OFFICE_CD");
            		DTDS_CLT_DLV_NM[i]		= (String)paramMap.get("O_DTDS_CLT_DLV_NM");
            		DTDS_CLT_DLV_CD[i]		= (String)paramMap.get("O_DTDS_CLT_DLV_CD");
            		INVC_NO[i]				= (String)paramMap.get("O_INVC_NO");
            		FRT_DV_CD[i]			= (String)paramMap.get("O_FRT_DV_CD");
            		WORK_DV_CD[i]			= (String)paramMap.get("O_WORK_DV_CD");
            		
            		RCPT_DV[i]				= (String)paramMap.get("O_RCPT_DV");
            		DTDS_CLT_OFFICE_NM[i]	= (String)paramMap.get("O_DTDS_CLT_OFFICE_NM");
            		DTDS_CLT_SITE_NM[i]		= (String)paramMap.get("O_DTDS_CLT_SITE_NM");
            		DTDS_CLT_SITE_CD[i]		= (String)paramMap.get("O_DTDS_CLT_SITE_CD");
            		DTDS_CLT_WORKER_CD[i]	= (String)paramMap.get("O_DTDS_CLT_WORKER_CD");
            		DTDS_CLT_WORKER_NM[i]	= (String)paramMap.get("O_DTDS_CLT_WORKER_NM");
            		DTDS_DLV_OFFICE_NM[i]	= (String)paramMap.get("O_DTDS_DLV_OFFICE_NM");
            		DTDS_DLV_SITE_NM[i]		= (String)paramMap.get("O_DTDS_DLV_SITE_NM");
            		DTDS_DLV_SITE_CD[i]		= (String)paramMap.get("O_DTDS_DLV_SITE_CD");
            		DTDS_DLV_WORKER_CD[i]	= (String)paramMap.get("O_DTDS_DLV_WORKER_CD");
            		
            		DTDS_DLV_WORKER_NM[i]	= (String)paramMap.get("O_DTDS_DLV_WORKER_NM");
            		SENDR_NM[i]				= (String)paramMap.get("O_SENDR_NM");
            		SENDR_ADDR[i]			= (String)paramMap.get("O_SENDR_ADDR");
            		SENDR_TEL_NO1[i]		= (String)paramMap.get("O_SENDR_TEL_NO1");
            		RCVR_NM[i]				= (String)paramMap.get("O_RCVR_NM");
            		RCVR_ADDR[i]			= (String)paramMap.get("O_RCVR_ADDR");
            		RCVR_TEL_NO1[i]			= (String)paramMap.get("O_RCVR_TEL_NO1");
            		GDS_QTY[i]				= (String)paramMap.get("O_GDS_QTY");
            		ETC01[i]				= (String)paramMap.get("O_ETC01");
            		GDS_NM[i]				= (String)paramMap.get("O_GDS_NM");
            		DEFAULT_FRT_COST[i]		= (String)paramMap.get("O_DEFAULT_FRT_COST");
            		
            		SHIPMENT_FRT_COST[i]	= (String)paramMap.get("O_SHIPMENT_FRT_COST");
            		DLV_JEJU_FRT_COST[i]	= (String)paramMap.get("O_DLV_JEJU_FRT_COST");
            		ETC_FRT_COST[i]			= (String)paramMap.get("O_ETC_FRT_COST");
            		TOTAL_FRT_COST[i]		= (String)paramMap.get("O_TOTAL_FRT_COST");
            		COD_COST[i]				= (String)paramMap.get("O_COD_COST");
            		ORI_ORD_NO[i]			= (String)paramMap.get("O_ORI_ORD_NO");
            		CLT_DT[i]				= (String)paramMap.get("O_CLT_DT");
            		DLV_DT[i]				= (String)paramMap.get("O_DLV_DT");
            		RECEIVER_NM[i]			= (String)paramMap.get("O_RECEIVER_NM");
            		FRT_CUST_NM[i]			= (String)paramMap.get("O_FRT_CUST_NM");
            		
            		FRT_CUST_CD[i]			= (String)paramMap.get("O_FRT_CUST_CD");
            		SND_CUST_NM[i]			= (String)paramMap.get("O_SND_CUST_NM");
            		SND_CUST_CD[i]			= (String)paramMap.get("O_SND_CUST_CD");
            	}

                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("I_SEQ"					, SEQ);
                modelIns.put("I_DTDS_BUSINESS_NM"		, DTDS_BUSINESS_NM);
                modelIns.put("I_DTDS_BUSINESS_CD"		, DTDS_BUSINESS_CD);
                modelIns.put("I_DTDS_OFFICE_NM"			, DTDS_OFFICE_NM);
                modelIns.put("I_DTDS_OFFICE_CD"			, DTDS_OFFICE_CD);
                modelIns.put("I_DTDS_CLT_DLV_NM"		, DTDS_CLT_DLV_NM);
                modelIns.put("I_DTDS_CLT_DLV_CD"		, DTDS_CLT_DLV_CD);
                modelIns.put("I_INVC_NO"				, INVC_NO);
                modelIns.put("I_FRT_DV_CD"				, FRT_DV_CD);
                modelIns.put("I_WORK_DV_CD"				, WORK_DV_CD);
                
                modelIns.put("I_RCPT_DV"				, RCPT_DV);
                modelIns.put("I_DTDS_CLT_OFFICE_NM"		, DTDS_CLT_OFFICE_NM);
                modelIns.put("I_DTDS_CLT_SITE_NM"		, DTDS_CLT_SITE_NM);
                modelIns.put("I_DTDS_CLT_SITE_CD"		, DTDS_CLT_SITE_CD);
                modelIns.put("I_DTDS_CLT_WORKER_CD"		, DTDS_CLT_WORKER_CD);
                modelIns.put("I_DTDS_CLT_WORKER_NM"		, DTDS_CLT_WORKER_NM);
                modelIns.put("I_DTDS_DLV_OFFICE_NM"		, DTDS_DLV_OFFICE_NM);
                modelIns.put("I_DTDS_DLV_SITE_NM"		, DTDS_DLV_SITE_NM);
                modelIns.put("I_DTDS_DLV_SITE_CD"		, DTDS_DLV_SITE_CD);
                modelIns.put("I_DTDS_DLV_WORKER_CD"		, DTDS_DLV_WORKER_CD);
                
                modelIns.put("I_DTDS_DLV_WORKER_NM"		, DTDS_DLV_WORKER_NM);
                modelIns.put("I_SENDR_NM"				, SENDR_NM);
                modelIns.put("I_SENDR_ADDR"				, SENDR_ADDR);
                modelIns.put("I_SENDR_TEL_NO1"			, SENDR_TEL_NO1);
                modelIns.put("I_RCVR_NM"				, RCVR_NM);
                modelIns.put("I_RCVR_ADDR"				, RCVR_ADDR);
                modelIns.put("I_RCVR_TEL_NO1"			, RCVR_TEL_NO1);
                modelIns.put("I_GDS_QTY"				, GDS_QTY);
                modelIns.put("I_ETC01"					, ETC01);
                modelIns.put("I_GDS_NM"					, GDS_NM);
                modelIns.put("I_DEFAULT_FRT_COST"		, DEFAULT_FRT_COST);
                
                modelIns.put("I_SHIPMENT_FRT_COST"		, SHIPMENT_FRT_COST);
                modelIns.put("I_DLV_JEJU_FRT_COST"		, DLV_JEJU_FRT_COST);
                modelIns.put("I_ETC_FRT_COST"			, ETC_FRT_COST);
                modelIns.put("I_TOTAL_FRT_COST"			, TOTAL_FRT_COST);
                modelIns.put("I_COD_COST"				, COD_COST);
                modelIns.put("I_ORI_ORD_NO"				, ORI_ORD_NO);
                modelIns.put("I_CLT_DT"					, CLT_DT);
                modelIns.put("I_DLV_DT"					, DLV_DT);
                modelIns.put("I_RECEIVER_NM"			, RECEIVER_NM);
                modelIns.put("I_FRT_CUST_NM"			, FRT_CUST_NM);
                
                modelIns.put("I_FRT_CUST_CD"			, FRT_CUST_CD);
                modelIns.put("I_SND_CUST_NM"			, SND_CUST_NM);
                modelIns.put("I_SND_CUST_CD"			, SND_CUST_CD);
                
                modelIns.put("I_CUST_ID"		, (String)model.get("custId"));
                modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao
                modelIns = (Map<String, Object>)dao.saveOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSAC201", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
        	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
			model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
			
            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
            
        } catch(Exception e){
            m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    

	/**
     * Method ID : saveExcel19
     * Method 설명 : 엑셀읽기저장
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcel19(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	int insertCnt = (list != null)?list.size():0;
            if(insertCnt > 0){
            	String[] CLT_DT                 = new String[insertCnt];
            	String[] DTDS_TYPE              = new String[insertCnt];
            	String[] WORK_DV_CD             = new String[insertCnt];
            	String[] INVC_NO                = new String[insertCnt];
            	String[] DTDS_BUSINESS_NM       = new String[insertCnt];
            	String[] DTDS_BUSINESS_CD       = new String[insertCnt];
            	String[] DTDS_OFFICE_NM         = new String[insertCnt];
            	String[] DTDS_OFFICE_CD         = new String[insertCnt];
            	String[] DTDS_OFFICE_TEL        = new String[insertCnt];
            	String[] DTDS_CLT_OFFICE_NM     = new String[insertCnt];
            	String[] DTDS_CLT_OFFICE_CD     = new String[insertCnt];
            	String[] DTDS_CLT_OFFICE_TEL    = new String[insertCnt];
            	String[] SENDR_NM               = new String[insertCnt];
            	String[] SENDR_TEL_NO1          = new String[insertCnt];
            	String[] SENDR_TEL_NO2          = new String[insertCnt];
            	String[] SENDR_POST             = new String[insertCnt];
            	String[] SENDR_ADDR             = new String[insertCnt];
            	String[] RCVR_NM                = new String[insertCnt];
            	String[] RCVR_TEL_NO1           = new String[insertCnt];
            	String[] RCVR_TEL_NO2           = new String[insertCnt];
            	String[] RCVR_POST              = new String[insertCnt];
            	String[] RCVR_ADDR              = new String[insertCnt];
            	String[] RCVR_ETC1              = new String[insertCnt];
            	String[] GDS_CD                 = new String[insertCnt];
            	String[] GDS_NM                 = new String[insertCnt];
            	String[] GDS_QTY                = new String[insertCnt];
            	String[] BOX_DV                 = new String[insertCnt];
            	String[] PACK_STAT              = new String[insertCnt];
            	String[] FRT_DV_CD              = new String[insertCnt];
            	String[] FRT_CUST_NM            = new String[insertCnt];
            	String[] FRT_CUST_CD            = new String[insertCnt];
            	String[] DEFAULT_FRT_COST       = new String[insertCnt];
            	String[] SHIPMENT_FRT_COST      = new String[insertCnt];
            	String[] DLV_JEJU_FRT_COST      = new String[insertCnt];
            	String[] ETC_FRT_COST           = new String[insertCnt];
            	String[] TOTAL_FRT_COST         = new String[insertCnt];
            	String[] ORI_ORD_NO             = new String[insertCnt];
            	String[] DLV_DT                 = new String[insertCnt];
            	String[] DLV_IN_DT              = new String[insertCnt];
            	String[] RCPT_DV                = new String[insertCnt];
            	String[] DTDS_CLT_DLV_NM        = new String[insertCnt];
            	String[] DTDS_CLT_DLV_CD        = new String[insertCnt];
            	String[] DTDS_CLT_DLV_TEL       = new String[insertCnt];
            	String[] DTDS_CLT_SITE_NM       = new String[insertCnt];
            	String[] DTDS_CLT_SITE_CD       = new String[insertCnt];
            	String[] DTDS_CLT_WORKER_NM     = new String[insertCnt];
            	String[] DTDS_CLT_WORKER_CD     = new String[insertCnt];
            	String[] DTDS_DLV_OFFICE_NM     = new String[insertCnt];
            	String[] DTDS_DLV_OFFICE_CD     = new String[insertCnt];
            	String[] DTDS_DLV_SITE_NM       = new String[insertCnt];
            	String[] DTDS_DLV_SITE_CD       = new String[insertCnt];
            	String[] DTDS_DLV_WORKER_NM     = new String[insertCnt];
            	String[] DTDS_DLV_WORKER_CD     = new String[insertCnt];
            	String[] DLV_BAD_TYPE           = new String[insertCnt];
            	String[] RECEIVER_NM            = new String[insertCnt];
            	String[] SND_CUST_NM            = new String[insertCnt];
            	String[] SND_CUST_CD            = new String[insertCnt];
            	String[] ETC                    = new String[insertCnt];
            	
                Map<String, Object> paramMap = null;
                for (int i=0;i<list.size();i++) {
            		paramMap = (Map)list.get(i);
            		CLT_DT[i]               = (String)paramMap.get("O_CLT_DT");
            		DTDS_TYPE[i]            = (String)paramMap.get("O_DTDS_TYPE");
            		WORK_DV_CD[i]           = (String)paramMap.get("O_WORK_DV_CD");
            		INVC_NO[i]              = (String)paramMap.get("O_INVC_NO");
            		DTDS_BUSINESS_NM[i]     = (String)paramMap.get("O_DTDS_BUSINESS_NM");
            		DTDS_BUSINESS_CD[i]     = (String)paramMap.get("O_DTDS_BUSINESS_CD");
            		DTDS_OFFICE_NM[i]       = (String)paramMap.get("O_DTDS_OFFICE_NM");
            		DTDS_OFFICE_CD[i]       = (String)paramMap.get("O_DTDS_OFFICE_CD");
            		DTDS_OFFICE_TEL[i]      = (String)paramMap.get("O_DTDS_OFFICE_TEL");
            		DTDS_CLT_OFFICE_NM[i]   = (String)paramMap.get("O_DTDS_CLT_OFFICE_NM");
            		DTDS_CLT_OFFICE_CD[i]   = (String)paramMap.get("O_DTDS_CLT_OFFICE_CD");
            		DTDS_CLT_OFFICE_TEL[i]  = (String)paramMap.get("O_DTDS_CLT_OFFICE_TEL");
            		SENDR_NM[i]             = (String)paramMap.get("O_SENDR_NM");
            		SENDR_TEL_NO1[i]        = (String)paramMap.get("O_SENDR_TEL_NO1");
            		SENDR_TEL_NO2[i]        = (String)paramMap.get("O_SENDR_TEL_NO2");
            		SENDR_POST[i]           = (String)paramMap.get("O_SENDR_POST");
            		SENDR_ADDR[i]           = (String)paramMap.get("O_SENDR_ADDR");
            		RCVR_NM[i]              = (String)paramMap.get("O_RCVR_NM");
            		RCVR_TEL_NO1[i]         = (String)paramMap.get("O_RCVR_TEL_NO1");
            		RCVR_TEL_NO2[i]         = (String)paramMap.get("O_RCVR_TEL_NO2");
            		RCVR_POST[i]            = (String)paramMap.get("O_RCVR_POST");
            		RCVR_ADDR[i]            = (String)paramMap.get("O_RCVR_ADDR");
            		RCVR_ETC1[i]            = (String)paramMap.get("O_RCVR_ETC1");
            		GDS_CD[i]               = (String)paramMap.get("O_GDS_CD");
            		GDS_NM[i]               = (String)paramMap.get("O_GDS_NM");
            		GDS_QTY[i]              = (String)paramMap.get("O_GDS_QTY");
            		BOX_DV[i]               = (String)paramMap.get("O_BOX_DV");
            		PACK_STAT[i]            = (String)paramMap.get("O_PACK_STAT");
            		FRT_DV_CD[i]            = (String)paramMap.get("O_FRT_DV_CD");
            		FRT_CUST_NM[i]          = (String)paramMap.get("O_FRT_CUST_NM");
            		FRT_CUST_CD[i]          = (String)paramMap.get("O_FRT_CUST_CD");
            		DEFAULT_FRT_COST[i]     = (String)paramMap.get("O_DEFAULT_FRT_COST");
            		SHIPMENT_FRT_COST[i]    = (String)paramMap.get("O_SHIPMENT_FRT_COST");
            		DLV_JEJU_FRT_COST[i]    = (String)paramMap.get("O_DLV_JEJU_FRT_COST");
            		ETC_FRT_COST[i]         = (String)paramMap.get("O_ETC_FRT_COST");
            		TOTAL_FRT_COST[i]       = (String)paramMap.get("O_TOTAL_FRT_COST");
            		ORI_ORD_NO[i]           = (String)paramMap.get("O_ORI_ORD_NO");
            		DLV_DT[i]               = (String)paramMap.get("O_DLV_DT");
            		DLV_IN_DT[i]            = (String)paramMap.get("O_DLV_IN_DT");
            		RCPT_DV[i]              = (String)paramMap.get("O_RCPT_DV");
            		DTDS_CLT_DLV_NM[i]      = (String)paramMap.get("O_DTDS_CLT_DLV_NM");
            		DTDS_CLT_DLV_CD[i]      = (String)paramMap.get("O_DTDS_CLT_DLV_CD");
            		DTDS_CLT_DLV_TEL[i]     = (String)paramMap.get("O_DTDS_CLT_DLV_TEL");
            		DTDS_CLT_SITE_NM[i]     = (String)paramMap.get("O_DTDS_CLT_SITE_NM");
            		DTDS_CLT_SITE_CD[i]     = (String)paramMap.get("O_DTDS_CLT_SITE_CD");
            		DTDS_CLT_WORKER_NM[i]   = (String)paramMap.get("O_DTDS_CLT_WORKER_NM");
            		DTDS_CLT_WORKER_CD[i]   = (String)paramMap.get("O_DTDS_CLT_WORKER_CD");
            		DTDS_DLV_OFFICE_NM[i]   = (String)paramMap.get("O_DTDS_DLV_OFFICE_NM");
            		DTDS_DLV_OFFICE_CD[i]   = (String)paramMap.get("O_DTDS_DLV_OFFICE_CD");
            		DTDS_DLV_SITE_NM[i]     = (String)paramMap.get("O_DTDS_DLV_SITE_NM");
            		DTDS_DLV_SITE_CD[i]     = (String)paramMap.get("O_DTDS_DLV_SITE_CD");
            		DTDS_DLV_WORKER_NM[i]   = (String)paramMap.get("O_DTDS_DLV_WORKER_NM");
            		DTDS_DLV_WORKER_CD[i]   = (String)paramMap.get("O_DTDS_DLV_WORKER_CD");
            		DLV_BAD_TYPE[i]         = (String)paramMap.get("O_DLV_BAD_TYPE");
            		RECEIVER_NM[i]          = (String)paramMap.get("O_RECEIVER_NM");
            		SND_CUST_NM[i]          = (String)paramMap.get("O_SND_CUST_NM");
            		SND_CUST_CD[i]          = (String)paramMap.get("O_SND_CUST_CD");
            		ETC[i]                  = (String)paramMap.get("O_ETC");
            	}

                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("I_CLT_DT"               , CLT_DT               );
                modelIns.put("I_DTDS_TYPE"            , DTDS_TYPE            );
                modelIns.put("I_WORK_DV_CD"           , WORK_DV_CD           );
                modelIns.put("I_INVC_NO"              , INVC_NO              );
                modelIns.put("I_DTDS_BUSINESS_NM"     , DTDS_BUSINESS_NM     );
                modelIns.put("I_DTDS_BUSINESS_CD"     , DTDS_BUSINESS_CD     );
                modelIns.put("I_DTDS_OFFICE_NM"       , DTDS_OFFICE_NM       );
                modelIns.put("I_DTDS_OFFICE_CD"       , DTDS_OFFICE_CD       );
                modelIns.put("I_DTDS_OFFICE_TEL"      , DTDS_OFFICE_TEL      );
                modelIns.put("I_DTDS_CLT_OFFICE_NM"   , DTDS_CLT_OFFICE_NM   );
                modelIns.put("I_DTDS_CLT_OFFICE_CD"   , DTDS_CLT_OFFICE_CD   );
                modelIns.put("I_DTDS_CLT_OFFICE_TEL"  , DTDS_CLT_OFFICE_TEL  );
                modelIns.put("I_SENDR_NM"             , SENDR_NM             );
                modelIns.put("I_SENDR_TEL_NO1"        , SENDR_TEL_NO1        );
                modelIns.put("I_SENDR_TEL_NO2"        , SENDR_TEL_NO2        );
                modelIns.put("I_SENDR_POST"           , SENDR_POST           );
                modelIns.put("I_SENDR_ADDR"           , SENDR_ADDR           );
                modelIns.put("I_RCVR_NM"              , RCVR_NM              );
                modelIns.put("I_RCVR_TEL_NO1"         , RCVR_TEL_NO1         );
                modelIns.put("I_RCVR_TEL_NO2"         , RCVR_TEL_NO2         );
                modelIns.put("I_RCVR_POST"            , RCVR_POST            );
                modelIns.put("I_RCVR_ADDR"            , RCVR_ADDR            );
                modelIns.put("I_RCVR_ETC1"            , RCVR_ETC1            );
                modelIns.put("I_GDS_CD"               , GDS_CD               );
                modelIns.put("I_GDS_NM"               , GDS_NM               );
                modelIns.put("I_GDS_QTY"              , GDS_QTY              );
                modelIns.put("I_BOX_DV"               , BOX_DV               );
                modelIns.put("I_PACK_STAT"            , PACK_STAT            );
                modelIns.put("I_FRT_DV_CD"            , FRT_DV_CD            );
                modelIns.put("I_FRT_CUST_NM"          , FRT_CUST_NM          );
                modelIns.put("I_FRT_CUST_CD"          , FRT_CUST_CD          );
                modelIns.put("I_DEFAULT_FRT_COST"     , DEFAULT_FRT_COST     );
                modelIns.put("I_SHIPMENT_FRT_COST"    , SHIPMENT_FRT_COST    );
                modelIns.put("I_DLV_JEJU_FRT_COST"    , DLV_JEJU_FRT_COST    );
                modelIns.put("I_ETC_FRT_COST"         , ETC_FRT_COST         );
                modelIns.put("I_TOTAL_FRT_COST"       , TOTAL_FRT_COST       );
                modelIns.put("I_ORI_ORD_NO"           , ORI_ORD_NO           );
                modelIns.put("I_DLV_DT"               , DLV_DT               );
                modelIns.put("I_DLV_IN_DT"            , DLV_IN_DT            );
                modelIns.put("I_RCPT_DV"              , RCPT_DV              );
                modelIns.put("I_DTDS_CLT_DLV_NM"      , DTDS_CLT_DLV_NM      );
                modelIns.put("I_DTDS_CLT_DLV_CD"      , DTDS_CLT_DLV_CD      );
                modelIns.put("I_DTDS_CLT_DLV_TEL"     , DTDS_CLT_DLV_TEL     );
                modelIns.put("I_DTDS_CLT_SITE_NM"     , DTDS_CLT_SITE_NM     );
                modelIns.put("I_DTDS_CLT_SITE_CD"     , DTDS_CLT_SITE_CD     );
                modelIns.put("I_DTDS_CLT_WORKER_CD"   , DTDS_CLT_WORKER_CD   );
                modelIns.put("I_DTDS_CLT_WORKER_NM"   , DTDS_CLT_WORKER_NM   );
                modelIns.put("I_DTDS_DLV_OFFICE_NM"   , DTDS_DLV_OFFICE_NM   );
                modelIns.put("I_DTDS_DLV_OFFICE_CD"   , DTDS_DLV_OFFICE_CD   );
                modelIns.put("I_DTDS_DLV_SITE_NM"     , DTDS_DLV_SITE_NM     );
                modelIns.put("I_DTDS_DLV_SITE_CD"     , DTDS_DLV_SITE_CD     );
                modelIns.put("I_DTDS_DLV_WORKER_NM"   , DTDS_DLV_WORKER_NM   );
                modelIns.put("I_DTDS_DLV_WORKER_CD"   , DTDS_DLV_WORKER_CD   );
                modelIns.put("I_DLV_BAD_TYPE"         , DLV_BAD_TYPE         );
                modelIns.put("I_RECEIVER_NM"          , RECEIVER_NM          );
                modelIns.put("I_SND_CUST_NM"          , SND_CUST_NM          );
                modelIns.put("I_SND_CUST_CD"          , SND_CUST_CD          );
                modelIns.put("I_ETC"                  , ETC                  );
                
                modelIns.put("I_CUST_ID"		, (String)model.get("custId"));
                modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao
                modelIns = (Map<String, Object>)dao.saveOrder19(modelIns);
                ServiceUtil.isValidReturnCode("WMSAC201", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
        	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
			model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
			
            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
            
        } catch(Exception e){
            m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    
    /**
     * Method ID  : excelDown
     * Method 설명    : 엑셀다운로드
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     @Override
     public Map<String, Object> excelDown(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         model.put("pageIndex", "1");
         model.put("pageSize", "60000");
         map.put("LIST", dao.list(model));
         return map;
     }
}
