package com.logisall.winus.wmsac.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC140Dao")
public class WMSAC140Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : list 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsac140.list", model);
	}

	/**
     * Method ID    : save
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsac140.pk_wmsac100.sp_saveDlvOrd", model);
        return model;
    }
    
    /**
	 * Method ID : saveUploadData 
	 * Method 설명 : UOM정보 대용량등록시 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if ((paramMap.get("SALES_CUST_ID") != null && StringUtils.isNotEmpty(paramMap.get("SALES_CUST_ID").toString()))) {

					//sqlMapClient.insert("wmsms100.insert_many", paramMap);
					executeUpdate("wmsac140e2.update_many", paramMap);

				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
}
