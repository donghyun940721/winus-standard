package com.logisall.winus.wmsac.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsac.service.WMSAC270Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSAC270Service")
public class WMSAC270ServiceImpl implements WMSAC270Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAC270Dao")
    private WMSAC270Dao dao;

    
    /**
     * 
     * 대체 Method ID   : listE1
     * 대체 Method 설명    : 통합정산 조회
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE1(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : listE2
     * 대체 Method 설명    : 택배출고 조회
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE2(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : uploadE2
     * 대체 Method 설명    : 출고박스 템플릿 업로드
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> uploadE2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            List<Map<String,Object>> list = (List<Map<String,Object>>) model.get("list");
            
            String[] checkValidationData = {"INVC_NO","CONF_BOX_CD"};
            
            for(Map<String,Object> data : list){
                data.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                data.put("CUST_ID", (String)model.get("custId"));
                data.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                for(String validationColumn : checkValidationData){
                    if(!data.containsKey(validationColumn) || data.get(validationColumn) == null ){
                        throw new BizException("입력되지않은 데이터가 있습니다.");
                    }
                }
                
                if((int)dao.checkInvcNoValidation(data) < 1){
                    throw new BizException("입력한 송장 정보가 없습니다 ["+data.get("INVC_NO")+"]" );
                }
                
                String confBoxId = (String)dao.getBoxIdToCd(data);
                
                if(confBoxId == null || confBoxId == ""){
                    throw new BizException("입력한 박스코드 정보가 없습니다 ["+data.get("CONF_BOX_CD")+"]" );
                }
                
                data.put("CONF_BOX_ID", confBoxId);
                
                dao.setAddInvcBoxGubun(data);
                
                dao.confOutAccData(data);

            }
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : listE3
     * 대체 Method 설명    : 입고정산 조회
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE3(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : listE4
     * 대체 Method 설명    : 보관비정산 조회
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE4(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveE4
     * 대체 Method 설명    : 보관내역 저장
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE4(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            List<Map<String,Object>> list = (List<Map<String,Object>>) model.get("list"); 
            
            for(Map<String,Object> data : list){
                data.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                data.put("CUST_ID", (String)model.get("custId"));
                data.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                if(!data.containsKey("ACC_WORK_ID") || data.get("ACC_WORK_ID") == null){
                    data.put("ACC_WORK_ID", dao.getAccWorkId(data));
                }
                dao.saveTypeSHeader(data);
                dao.saveTypeSDetail(data);
            }
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : listE5
     * 대체 Method 설명    : 반품 정산 조회
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE5(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveE5
     * 대체 Method 설명    : 반품내역 저장
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE5(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            List<Map<String,Object>> list = (List<Map<String,Object>>) model.get("list"); 
            
            for(Map<String,Object> data : list){
                data.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                data.put("CUST_ID", (String)model.get("custId"));
                data.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                if(!data.containsKey("ACC_WORK_ID") || data.get("ACC_WORK_ID") == null){
                    data.put("ACC_WORK_ID", dao.getAccWorkId(data));
                }
                dao.saveTypeRHeader(data);
                dao.saveTypeRDetail(data);
            }
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : listE6
     * 대체 Method 설명    : 기타비정산 조회
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE6(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveE6
     * 대체 Method 설명    : 기타비내역 저장
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE6(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            List<Map<String,Object>> list = (List<Map<String,Object>>) model.get("list"); 
            
            for(Map<String,Object> data : list){
                data.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                data.put("CUST_ID", (String)model.get("custId"));
                data.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                if(!data.containsKey("ACC_WORK_ID") || data.get("ACC_WORK_ID") == null){
                    data.put("ACC_WORK_ID", dao.getAccWorkId(data));
                }
                
                dao.saveTypeCHeader(data);
                dao.saveTypeCDetail(data);
            }
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : deleteAcc
     * 대체 Method 설명    : 정산 내역 삭제
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deleteAcc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            List<Map<String,Object>> list = (List<Map<String,Object>>) model.get("list"); 
            
            for(Map<String,Object> data : list){
                data.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                data.put("CUST_ID", (String)model.get("custId"));
                data.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                dao.deleteAccHeader(data);
                dao.deleteAccDetail(data);
            }
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : getFileUploadId
     * 대체 Method 설명    : 파일 업로드 ID 채번
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public String getFileUploadId(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            String result = (String)dao.getFileUploadId(model);
            return result;
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
    }
    
    /**
     * 
     * 대체 Method ID   : getAccWorkId
     * 대체 Method 설명    : 정산 작업 ID 채번
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public String getAccWorkId(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            String result = (String)dao.getAccWorkId(model);
            return result;
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
    }
    
    /**
     * 
     * 대체 Method ID   : saveFile
     * 대체 Method 설명    : 첨부파일 info 업데이트
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveFile(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            
            //기존 첨부파일 삭제 처리
            List<Map<String,Object>> fileInfoList = (List)dao.getFileSaveInfo(model);
            
            for(Map<String,Object> fileInfo : fileInfoList){
                String path = (String)fileInfo.get("FILE_PATH");
                String fileName = (String)fileInfo.get("FILE_ID");
                try{
                    File file = new File(path, fileName);
                    file.delete();
                    deleteFileInfo(fileInfo);
                }catch(Exception e){
                    e.printStackTrace();
                }
                
            }
            
            //첨부파일 정보 저장
            dao.saveFileInfo(model);
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : getFileSaveInfo
     * 대체 Method 설명    : 첨부파일 info 검색
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> getFileSaveInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            
            List<Map<String,Object>> fileInfoList = (List)dao.getFileSaveInfo(model);
            
            m.put("fileInfo",fileInfoList.get(0));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : deleteFileInfo
     * 대체 Method 설명    : 첨부파일 info 삭제
     * 작성자                      : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deleteFileInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            dao.deleteFileInfo(model);
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            m.put("errCnt", -1);
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    
    
    


    
    
}
