package com.logisall.winus.wmsac.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC240Dao")
public class WMSAC240Dao extends SqlMapAbstractDAO {
	
	/**
	 * Method ID : listHeader 
	 * Method 설명 : 화물수탁증입력/조회 헤더 리스트 조회
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listHeader(Map<String, Object> model) {
		return executeQueryPageWq("wmsac240.listHeader", model);
	}
	
	/**
	 * Method ID : listDetail 
	 * Method 설명 : 화물수탁증입력/조회 헤더 리스트 조회
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listDetail(Map<String, Object> model) {
		return executeQueryWq("wmsac240.listDetail", model);
	}
	
	
	/**
     * Method ID : ACCTypeList 
     * Method 설명 : 기타비 정산 코드 조회
     * 작성자 : schan
     * 
     * @param model
     * @return
     */
    public GenericResultSet ACCTypeCList(Map<String, Object> model) {
        return executeQueryWq("wmsac240.ACCTypeCList", model);
    }
	
	
	/**
	 * Method ID : listDetail 
	 * Method 설명 : 기타비 리스트 조회
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listTypeC(Map<String, Object> model) {
		return executeQueryPageWq("wmsac240.listTypeC", model);
	}
	
	/**
	 * Method ID : getcompAccCount 
	 * Method 설명 : 화물수탁증입력/조회 헤더 중복 조회
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object getcompAccCount(Map<String, Object> model) {
		return executeQueryForObject("wmsac240.getcompAccCount", model);
	}
	
	
	/**
	 * Method ID : insertHeader 
	 * Method 설명 : 화물수탁증입력/조회 헤더 리스트 추가
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object insertHeader(Map<String, Object> model) {
		return executeInsert("wmsac240.insertHeader", model);
	}

	/**
	 * Method ID : updateHeader 
	 * Method 설명 : 화물수탁증입력/조회 헤더 리스트 수정
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object updateHeader(Map<String, Object> model) {
		return executeUpdate("wmsac240.updateHeader", model);
	}
	
	/**
	 * Method ID : updateHeaderDetail 
	 * Method 설명 : 화물수탁증입력/조회 헤더 리스트 수정 후 디테일 리스트 수정
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object updateHeaderDetail(Map<String, Object> model) {
		return executeUpdate("wmsac240.updateHeaderDetail", model);
	}

	/**
	 * Method ID : deleteHeader 
	 * Method 설명 : 화물수탁증입력/조회 헤더 리스트 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteHeader(Map<String, Object> model) {
		return executeUpdate("wmsac240.deleteHeader", model);
	}
	
	/**
	 * Method ID : initDetail 
	 * Method 설명 : 화물수탁증입력/조회 디테일 리스트 초기화 
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object initDetail(Map<String, Object> model) {
		return executeInsert("wmsac240.initDetail", model);
	}
	
	/**
	 * Method ID : updateDetail 
	 * Method 설명 : 화물수탁증입력/조회 디테일 리스트 수정
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDetail(Map<String, Object> model) {
		return executeUpdate("wmsac240.updateDetail", model);
	}
	/**
	 * Method ID : listExcel 
	 * Method 설명 : 화물수탁증입력/조회 엑셀용 헤더 리스트 조회
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listExcelHeader(Map<String, Object> model) {
		return executeQueryWq("wmsac240.listExcelHeader", model);
	}
	/**
	 * Method ID : listExcel 
	 * Method 설명 : 화물수탁증입력/조회 엑셀용 디테일 리스트 조회
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listExcelDetail(Map<String, Object> model) {
		return executeQueryWq("wmsac240.listExcelHeader", model);
	}
	
	/**
     * Method ID : checkValidationCustID 
     * Method 설명 : 기타비 정산 추가
     * 작성자 : schan
     * 
     * @param model
     * @return
     */
    public Object checkValidationCustID(Map<String, Object> model) {
        return executeQueryForObject("wmsac240.checkValidationCustID", model);
    }
	
	/**
	 * Method ID : insertTypeC 
	 * Method 설명 : 기타비 정산 추가
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object insertTypeC(Map<String, Object> model) {
		return executeInsert("wmsac240.insertTypeC", model);
	}

	/**
	 * Method ID : updateTypeC1
	 * Method 설명 : 기타비 정산 수정 AC301
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object updateTypeC1(Map<String, Object> model) {
		return executeUpdate("wmsac240.updateTypeC1", model);
	}
	/**
	 * Method ID : updateTypeC2
	 * Method 설명 : 기타비 정산 수정 AC302
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object updateTypeC2(Map<String, Object> model) {
		return executeUpdate("wmsac240.updateTypeC2", model);
	}

	/**
	 * Method ID : deleteTypeC
	 * Method 설명 : 기타비 정산 삭제
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteTypeC1(Map<String, Object> model) {
		return executeUpdate("wmsac240.deleteTypeC1", model);
	}
	
	/**
	 * Method ID : deleteTypeC
	 * Method 설명 : 기타비 정산 삭제
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteTypeC2(Map<String, Object> model) {
		return executeUpdate("wmsac240.deleteTypeC2", model);
	}
	/**
	 * Method ID : confirmTypeC1
	 * Method 설명 : 기타비 정산 납품 확정
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object confirmTypeC1(Map<String, Object> model) {
		return executeUpdate("wmsac240.confirmTypeC1", model);
	}
	/**
	 * Method ID : confirmTypeC2
	 * Method 설명 : 기타비 정산 납품 확정
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object confirmTypeC2(Map<String, Object> model) {
		return executeUpdate("wmsac240.confirmTypeC2", model);
	}
	
}
