package com.logisall.winus.wmsac.service;

import java.util.Map;


public interface WMSAC202Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
	public Map<String, Object> list0(Map<String, Object> model) throws Exception;
	public Map<String, Object> listPool(Map<String, Object> model) throws Exception;
	public Map<String, Object> manage(Map<String, Object> model) throws Exception;
	public Map<String, Object> store(Map<String, Object> model) throws Exception;
	public Map<String, Object> save3(Map<String, Object> model) throws Exception;
	public Map<String, Object> save16(Map<String, Object> model) throws Exception;
	public Map<String, Object> save0(Map<String, Object> model) throws Exception;
	public Map<String, Object> chkCnt(Map<String, Object> model) throws Exception;
	public Map<String, Object> chkDataCnt(Map<String, Object> model) throws Exception;
	public Map<String, Object> list15(Map<String, Object> model) throws Exception;
	public Map<String, Object> save15(Map<String, Object> model) throws Exception;
	public Map<String, Object> list17(Map<String, Object> model) throws Exception;
	public Map<String, Object> save17(Map<String, Object> model) throws Exception;
	public Map<String, Object> add(Map<String, Object> model) throws Exception;
	public Map<String, Object> save10(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveReport(Map<String, Object> model)throws Exception;
}
