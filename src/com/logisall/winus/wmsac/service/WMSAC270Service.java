package com.logisall.winus.wmsac.service;

import java.util.List;
import java.util.Map;


public interface WMSAC270Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> uploadE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE6(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteAcc(Map<String, Object> model) throws Exception;
    public String getFileUploadId(Map<String, Object> model) throws Exception;
    public String getAccWorkId(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveFile(Map<String, Object> model) throws Exception;
    public Map<String, Object> getFileSaveInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteFileInfo(Map<String, Object> model) throws Exception;
    
}
