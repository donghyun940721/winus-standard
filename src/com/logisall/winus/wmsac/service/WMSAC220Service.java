package com.logisall.winus.wmsac.service;

import java.util.List;
import java.util.Map;


public interface WMSAC220Service {
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> list3(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
