package com.logisall.winus.wmsac.service;

import java.util.Map;



public interface WMSAC141Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> reportSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> reportList(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelT1(Map<String, Object> model) throws Exception;
    public Map<String, Object> getCustInfo(Map<String, Object> model) throws Exception;
}
