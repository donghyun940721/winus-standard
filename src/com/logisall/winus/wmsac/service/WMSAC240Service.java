package com.logisall.winus.wmsac.service;

import java.util.List;
import java.util.Map;


public interface WMSAC240Service {
    public Map<String, Object> listHeader(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> ACCTypeCList(Map<String, Object> model) throws Exception;
    public Map<String, Object> listTypeC(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveHeader(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveTypeC(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
}
