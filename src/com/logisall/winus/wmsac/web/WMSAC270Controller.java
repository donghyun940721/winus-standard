package com.logisall.winus.wmsac.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC270Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC270Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAC270Service")
    private WMSAC270Service service;
    
    // 택배정산 업로드 컬럼
    static final String[] CONF_OUT_ACC_DATA_COLUMN = {
        "INVC_NO",
        "CONF_BOX_CD",
    };
    
    // 반품정산 업로드 컬럼
    static final String[] RTN_ACC_DATA_COLUMN = {
        "WORK_DT",
        "RTN_INVC_NO",
        "ORG_INVC_NO",
        "IN_PROC_QTY",
        "IN_PROC_DT",
        "SALES_MEMO",
        "SALES_COST",
        "RTN_NM",
        "RTN_ADDR"
    };
    
    // 기타비 업로드 컬럼
    static final String[] ETC_ACC_DATA_COLUMN = {
        "WORK_DT",
        "MEMO1",
        "SALES_MEMO_NM",
        "SALES_COST",
        "MEMO2"
    };

    /**
     * Method ID	: wmsac270
     * Method 설명	: OLS 정산 관리 화면
     * 작성자			: schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSAC270.action")
    public ModelAndView WMSAC270(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsac/WMSAC270");
    }
	
    /**
     * Method ID	: listE1
     * Method 설명	: 통합정산 조회
     * 작성자			: schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/listE1.action")
    public ModelAndView listE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {        			
            mav = new ModelAndView("jsonView", service.listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : listE2
     * Method 설명    : 택배출고정산 조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {                   
            mav = new ModelAndView("jsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : uploadExcelE2
     * Method 설명    : 박스정보입력 템플릿 양식 다운로드
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/uploadExcelE2.action")
    public void uploadExcelE2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이

            GenericResultSet grs = new GenericResultSet();

            String[][] headerEx = new String[CONF_OUT_ACC_DATA_COLUMN.length][6];
            String[][] valueName = new String[CONF_OUT_ACC_DATA_COLUMN.length][2];
            for(int i = 0 ; i < CONF_OUT_ACC_DATA_COLUMN.length ; i++){
                String headerName = "";
                switch(CONF_OUT_ACC_DATA_COLUMN[i]){
                    case "INVC_NO" :
                        headerName = "송장번호";
                        break;
                    case "CONF_BOX_CD" :
                        headerName = "박스코드";
                        break;
                }
                
                headerEx[i][0] = headerName;
                headerEx[i][1] = Integer.toString(i);
                headerEx[i][2] = Integer.toString(i);
                headerEx[i][3] = "0";
                headerEx[i][4] = "0";
                headerEx[i][5] = "100";
                
                valueName[i][0] = CONF_OUT_ACC_DATA_COLUMN[i];
                valueName[i][1] = "S";
            }
            // 파일명
            String fileName = MessageResolver.getText("박스정보등록");
            // 시트명
            String sheetName = "Sheet1";
            // 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
            String marCk = "N";
            // ComUtil코드
            String etc = "";
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to download excel :", e);
            }
        }
    }
    
    /**
     * Method ID    : uploadE2
     * Method 설명    : 박스정보 템플릿 업로드
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/uploadE2.action", method = RequestMethod.POST)
    public ModelAndView uploadE2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");
            
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");

            String fileName = file.getOriginalFilename();
            String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

            // 디렉토리 존재유무 확인
            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);// 디렉토리생성
            }

            File destinationDir = new File(filePaths);

            File destination = File.createTempFile("excelTemp", fileName, destinationDir);
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
            
            int startRow = Integer.parseInt((String) model.get("startRow"));
            
            List list = (List)ExcelReader.excelLimitRowReadByHandler(destination, CONF_OUT_ACC_DATA_COLUMN, startRow);
            
            model.put("list",list);
            m = service.uploadE2(model);
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error")+e.getMessage());
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : listE3
     * Method 설명    : 입고정산조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {                   
            mav = new ModelAndView("jsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : listE4
     * Method 설명    : 보관정산조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/listE4.action")
    public ModelAndView listE4(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {                   
            mav = new ModelAndView("jsonView", service.listE4(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : saveE4
     * Method 설명    : 보관내역 저장
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/saveE4.action", method = RequestMethod.POST)
    public ModelAndView saveE4(Map<String,Object> model,@RequestBody HashMap<String, Object> model2) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            model.putAll(model2);
            m = service.saveE4(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : listE5
     * Method 설명    : 반품정산조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/listE5.action")
    public ModelAndView listE5(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {                   
            mav = new ModelAndView("jsonView", service.listE5(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : saveE5
     * Method 설명    : 반품내역 저장
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/saveE5.action", method = RequestMethod.POST)
    public ModelAndView saveE5(Map<String,Object> model,@RequestBody HashMap<String, Object> model2) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            model.putAll(model2);
            m = service.saveE5(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }

    /**
     * Method ID    : uploadExcelE5
     * Method 설명    : 반품내역 템플릿 양식 다운로드
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/uploadExcelE5.action")
    public void uploadExcelE5(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이

            GenericResultSet grs = new GenericResultSet();

            String[][] headerEx = new String[RTN_ACC_DATA_COLUMN.length][6];
            String[][] valueName = new String[RTN_ACC_DATA_COLUMN.length][2];
            for(int i = 0 ; i < RTN_ACC_DATA_COLUMN.length ; i++){
                String headerName = "";
                switch(RTN_ACC_DATA_COLUMN[i]){
                    case "RTN_INVC_NO" :
                        headerName = "반품송장";
                        break;
                    case "ORG_INVC_NO" :
                        headerName = "원송장";
                        break;
                    case "IN_PROC_QTY" :
                        headerName = "수량";
                        break;
                    case "IN_PROC_DT" :
                        headerName = "입고일자";
                        break;
                    case "SALES_MEMO" :
                        headerName = "비고";
                        break;
                    case "SALES_COST" :
                        headerName = "금액";
                        break;
                    case "RTN_NM" :
                        headerName = "반송자";
                        break;
                    case "RTN_ADDR" :
                        headerName = "반송주소";
                        break;
                    case "WORK_DT" :
                        headerName = "작업일자";
                        break;
                }
                
                headerEx[i][0] = headerName;
                headerEx[i][1] = Integer.toString(i);
                headerEx[i][2] = Integer.toString(i);
                headerEx[i][3] = "0";
                headerEx[i][4] = "0";
                headerEx[i][5] = "100";
                
                valueName[i][0] = RTN_ACC_DATA_COLUMN[i];
                valueName[i][1] = "S";
            }
            // 파일명
            String fileName = MessageResolver.getText("반품정산등록");
            // 시트명
            String sheetName = "Sheet1";
            // 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
            String marCk = "N";
            // ComUtil코드
            String etc = "";
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to download excel :", e);
            }
        }
    }
    
    /**
     * Method ID    : uploadE5
     * Method 설명    : 반품내역 템플릿 업로드
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/uploadE5.action", method = RequestMethod.POST)
    public ModelAndView uploadE5(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");
            
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");

            String fileName = file.getOriginalFilename();
            String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

            // 디렉토리 존재유무 확인
            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);// 디렉토리생성
            }

            File destinationDir = new File(filePaths);

            File destination = File.createTempFile("excelTemp", fileName, destinationDir);
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
            
            int startRow = Integer.parseInt((String) model.get("startRow"));
            
            List list = (List)ExcelReader.excelLimitRowReadByHandler(destination, RTN_ACC_DATA_COLUMN, startRow);
            
            model.put("list",list);
            m = service.saveE5(model);
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    
    /**
     * Method ID    : listE6
     * Method 설명    : 기타비 조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/listE6.action")
    public ModelAndView listE6(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {                   
            mav = new ModelAndView("jsonView", service.listE6(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : saveE6
     * Method 설명    : 기타비 저장
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/saveE6.action", method = RequestMethod.POST)
    public ModelAndView saveE6(Map<String,Object> model,@RequestBody HashMap<String, Object> model2) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            model.putAll(model2);
            m = service.saveE6(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : deleteAcc
     * Method 설명    : 정산내역 삭제
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/deleteAcc.action", method = RequestMethod.POST)
    public ModelAndView deleteAcc(Map<String,Object> model,@RequestBody HashMap<String, Object> model2) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            model.putAll(model2);
            m = service.deleteAcc(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : uploadExcelE6
     * Method 설명    : 기타비 템플릿 양식 다운로드
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSAC270/uploadExcelE6.action")
    public void uploadExcelE6(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이

            GenericResultSet grs = new GenericResultSet();

            String[][] headerEx = new String[ETC_ACC_DATA_COLUMN.length][6];
            String[][] valueName = new String[ETC_ACC_DATA_COLUMN.length][2];
            for(int i = 0 ; i < ETC_ACC_DATA_COLUMN.length ; i++){
                String headerName = "";
                switch(ETC_ACC_DATA_COLUMN[i]){
                    case "WORK_DT" :
                        headerName = "청구일자";
                        break;
                    case "MEMO1" :
                        headerName = "제목";
                        break;
                    case "SALES_MEMO_NM" :
                        headerName = "청구유형";
                        break;
                    case "SALES_COST" :
                        headerName = "금액";
                        break;
                    case "MEMO2" :
                        headerName = "내용";
                        break;
                }
                
                headerEx[i][0] = headerName;
                headerEx[i][1] = Integer.toString(i);
                headerEx[i][2] = Integer.toString(i);
                headerEx[i][3] = "0";
                headerEx[i][4] = "0";
                headerEx[i][5] = "100";
                
                valueName[i][0] = ETC_ACC_DATA_COLUMN[i];
                valueName[i][1] = "S";
            }
            // 파일명
            String fileName = MessageResolver.getText("기타비등록");
            // 시트명
            String sheetName = "Sheet1";
            // 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
            String marCk = "N";
            // ComUtil코드
            String etc = "";
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to download excel :", e);
            }
        }
    }
    
    /**
     * Method ID    : uploadE6
     * Method 설명    : 기타비 템플릿 업로드
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/uploadE6.action", method = RequestMethod.POST)
    public ModelAndView uploadE6(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");
            
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");

            String fileName = file.getOriginalFilename();
            String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

            // 디렉토리 존재유무 확인
            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);// 디렉토리생성
            }

            File destinationDir = new File(filePaths);

            File destination = File.createTempFile("excelTemp", fileName, destinationDir);
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
            
            int startRow = Integer.parseInt((String) model.get("startRow"));
            
            List<Map<String,Object>> list = (List)ExcelReader.excelLimitRowReadByHandler(destination, ETC_ACC_DATA_COLUMN, startRow);
            
            model.put("list",list);
            m = service.saveE6(model);
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : saveFormE6
     * Method 설명    : 기타비 폼 저장
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/saveFormE6.action", method = RequestMethod.POST)
    public ModelAndView saveFormE6(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");
            
            //정산 ID 채번
            if(!model.containsKey("ACC_WORK_ID") || model.get("ACC_WORK_ID").equals("")){
                model.put("ACC_WORK_ID",service.getAccWorkId(model));
            }
            
            //기타비 정산 데이터 저장
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("ACC_WORK_ID", model.get("ACC_WORK_ID"));
            modelIns.put("WORK_DT", model.get("WORK_DT"));
            modelIns.put("MEMO1", model.get("MEMO1"));
            modelIns.put("MEMO2", model.get("MEMO2"));
            modelIns.put("SALES_COST", model.get("SALES_COST"));
            modelIns.put("SALES_MEMO", model.get("SALES_MEMO"));
            
            List<Map<String,Object>> modelList = new ArrayList<Map<String,Object>>();
            modelList.add(modelIns);
            
            model.put("list", modelList);
            
            m = service.saveE6(model);
            
            //첨부 파일 저장 관련 로직
            if(request.getHeader("Content-Type") != null && request.getHeader("Content-Type").startsWith("multipart/form-data")){
                
                MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
                MultipartFile file = multipartRequest.getFile("txtFile");
                
                if(file != null){
                    
                    String fileName = file.getOriginalFilename();
                    String filePaths = ConstantIF.FILE_ATTACH_PATH +"ATCH_FILE\\WMSAC270\\"+(String)model.get(ConstantIF.SS_SVC_NO);
    
                    // 디렉토리 존재유무 확인
                    if (!FileHelper.existDirectory(filePaths)) {
                        FileHelper.createDirectorys(filePaths);// 디렉토리생성
                    }
                    
                    //파일이름에 추가 
                    String id = service.getFileUploadId(model);
                    fileName = id+"_"+fileName;
                    
                    File destinationFile = new File(filePaths, fileName);
                    
                    FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));
                    
                    //파일 info 저장
                    model.put("MNG_NO",id);
                    model.put("FILE_ID",fileName);
                    model.put("ATTACH_GB","ETCACC");
                    model.put("FILE_VALUE","wmsac270");
                    model.put("FILE_PATH",filePaths);
                    model.put("ORG_FILENAME",file.getOriginalFilename());
                    model.put("FILE_EXT",FilenameUtils.getExtension(fileName));
                    model.put("FILE_SIZE", destinationFile.length());
                    model.put("WORK_ORD_ID",model.get("ACC_WORK_ID"));
                    service.saveFile(model);
                }
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : downFileE6
     * Method 설명    : 첨부파일 다운로드
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/downFileE6.action")
    public void downFileE6(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            //파일 정보 가져오기
            m = service.getFileSaveInfo(model);
            
            if(m.get("errCnt").equals(0)){
                Map<String, Object> fileInfo = (Map<String,Object>) m.get("fileInfo");
                String fileId = (String)fileInfo.get("FILE_ID");
                String filePath = (String)fileInfo.get("FILE_PATH");
                String fileOrgName = (String)fileInfo.get("ORG_FILENAME");
                
                File file = new File(filePath, fileId);
                
                BufferedOutputStream bos = null;
                FileInputStream fin = null;
                
                try{
                    fin = new FileInputStream(file);
                    int ifilesize = (int)file.length();
                    byte[] b = new byte[ifilesize];
                    
                    
                    response.reset();
                    response.setHeader("Content-Disposition", "attachment;filename="
                            + URLEncoder.encode(fileOrgName, "UTF-8") + ";");
                    response.setHeader("Content-Transfer-Encoding","binary;");
                    response.setHeader("Pragma","no-cache;");
                    response.setHeader("Expires","-1;");
                    response.setContentLength(ifilesize);
                    response.setContentType("application/octet-stream");
                    bos = new BufferedOutputStream(response.getOutputStream());
                    fin.read(b);
                    bos.write(b,0,ifilesize);
                    bos.flush();
                } catch(Exception e){
                   e.printStackTrace(); 
                } finally{
                    if (bos != null) {
                        try {
                            bos.close();
                        } catch (Exception ie) {
                        }
                    }
                    if (fin != null) {
                        try {
                            fin.close();
                        } catch (Exception ise) {
                        }
                    }   
                }
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
    }
    
    /**
     * Method ID    : deleteFileE6
     * Method 설명    : 첨부파일 삭제
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value="/WMSAC270/deleteFileE6.action", method = RequestMethod.POST)
    public ModelAndView deleteFileE6(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.getFileSaveInfo(model);
            if(m.get("errCnt").equals(0)){
                Map<String, Object> fileInfo = (Map<String,Object>) m.get("fileInfo");
                String fileId = (String)fileInfo.get("FILE_ID");
                String filePath = (String)fileInfo.get("FILE_PATH");
                
                File file = new File(filePath, fileId);
                
                file.delete();
                
                m = service.deleteFileInfo(fileInfo);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save acc info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    

}

