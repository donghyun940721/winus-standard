package com.logisall.winus.wmsac.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsac.service.WMSAC010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC010Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	

	@Resource(name = "WMSAC010Service")
	private WMSAC010Service service;

	static final String[] COLUMN_NAME_WMSAC010 = {
		"APPLY_FR_DT", "APPLY_TO_DT","ACC_STD", "SALE_BUY_GBN", "PAY_DAY",				//5 
		"RITEM_CD", "ITEM_GRP_CD", "UNIT_PRICE", "INOUT_COST", "STOCK_COST",			//10
		"CURRENCY_NAME", "TRANS_CUST_ID", "PAY_CD", "ROUND_CD", "END_CUT_CD",			//15
		"CLOSING_DAY", "CONT_DEPT_ID", "CONT_EMPLOYEE_ID", "CONF_YN", "REAL_DEPT_ID", 	//20
		"REAL_EMPLOYEE_ID", "COLLECT_EMPLOYEE_ID", "APPROVAL_EMP_ID" ,"GROUP_COM_EMP_ID"//24
	};
	
	
	/*-
	 * Method ID    : wmsac010
	 * Method 설명      : 정산단가계약 화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSAC010.action")
	public ModelAndView wmsac020(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC010");
	}

	/*-
	 * Method ID    : wmscm091
	 * Method 설명      : 상품조회 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC010E4.action")
	public ModelAndView wmscm091(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC010E4", service.selectData(model));
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 상품조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC010E4/listE4.action")
	public ModelAndView listE4(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 정산산출 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmsac010e2
	 * Method 설명      : 청구단가계약 팝업화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E2.action")
	public ModelAndView wmsac010e2(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC010E2");
	}

	/*-
	 * Method ID    : listSubDetail
	 * Method 설명      : 청구단가계약 상단조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC010E2/listSubDetail.action")
	public ModelAndView listSubDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.listSubDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List sub detail :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listSub
	 * Method 설명      : 청구단가계약 그리드 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E2/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List(sub) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveSub
	 * Method 설명      : 청구단가계약 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010/saveSub.action")
	public ModelAndView saveSub(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save(sub) :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsac010e3
	 * Method 설명      : 거래처정산일설정 팝업화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E3.action")
	public ModelAndView wmsac010e3(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC010E3");
	}

	/*-
	 * Method ID    : listAccDt
	 * Method 설명      : 거래처정산일설정 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E3/listAccDt.action")
	public ModelAndView listAccDt(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listAccDt(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List(Acc) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveAccdt
	 * Method 설명      : 거래처정산일 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010/saveAccdt.action")
	public ModelAndView saveAccdt(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveAccdt(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save acc dt :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSAC010Q1
	 * Method 설명      : 청구단가 엑셀 입력 팝업
	 * 작성자                 : summer H
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC010Q1.action")
	public ModelAndView wmsms050E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC010Q1");
	}

	/*-
	 * Method ID  : ExcelUpload
	 * Method 설명  : 청구 단가 Excel 입력
	 * 작성자             : summer hyun
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC010/excelUpload.action")
	public ModelAndView excelUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSAC010, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSAC010, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("chkDuplicationYn"			,model.get("chkDuplicationYn"));
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			m = service.saveExcelUpload(mapBody, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
			//mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	
}
