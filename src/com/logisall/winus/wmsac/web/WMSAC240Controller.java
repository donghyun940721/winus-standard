package com.logisall.winus.wmsac.web;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC240Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC240Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAC240Service")
    private WMSAC240Service service;
    
	/*-
	 * @date : 2022.03.17
	 * Method ID    : WMSAC240
	 * Method 설명      : 대화물류 화물수탁증 입력/조회 화면
	 * 작성자                 : kimzero
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSAC240.action")
	public ModelAndView wmsac101(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC240");
	}

	/*-
	 * @date : 2022.03.18
	 * Method ID    : WMSAC260
	 * Method 설명      : 대화물류 기타비 정산
	 * 작성자                 : kimzero
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSAC260.action")
	public ModelAndView wmsac260(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC260");
	}
	
	/*-
	 * @date : 2022.03.17
	 * Method ID    : listHeader
	 * Method 설명      : 대화물류 화물수탁증 입력/조회 화면 헤더그리드 조회
	 * 작성자                 : kimzero
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC240/listHeader.action")
	public ModelAndView listHeader(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listHeader(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}

	/*-
	 * @date : 2022.03.17
	 * Method ID    : listDetail
	 * Method 설명      : 대화물류 화물수탁증 입력/조회 화면 디테일그리드 조회
	 * 작성자                 : kimzero
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC240/listDetail.action")
	public ModelAndView listDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	
	
	/**
     * @date : 2024.09.25
     * Method ID    : listTypeC
     * Method 설명      : 대화물류 기타비 정산 코드 조회 (FMSACC-)
     * 작성자                 : schan
     * @param   model
     * @return  ModelAndView
     * @throws Exception 
     */
    @RequestMapping("/WMSAC240/ACCTypeCList.action")
    public ModelAndView ACCTypeCList(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.ACCTypeCList(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to list :", e);
            }
        }
        return mav;
    }
	
	
	/*-
	 * @date : 2022.03.17
	 * Method ID    : listTypeC
	 * Method 설명      : 대화물류 기타비 정산 그리드 조회 (WMSAC260)
	 * 작성자                 : schan
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC240/listTypeC.action")
	public ModelAndView listTypeC(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listTypeC(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}

	/*-
	 * @date : 2022.03.17
	 * Method ID    : saveHeader
	 * Method 설명      : 대화물류 화물수탁증 입력/조회 헤더그리드 저장
	 * 작성자                 : schan
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC240/saveHeader.action")
	public ModelAndView saveHeader(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveHeader(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * @date : 2022.03.17
	 * Method ID    : saveDetail
	 * Method 설명      : 대화물류 화물수탁증 입력/조회 디테일그리드 저장
	 * 작성자                 : schan
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC240/saveDetail.action")
	public ModelAndView saveDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveDetail(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * @date : 2022.03.17
	 * Method ID    : saveTypeC
	 * Method 설명      : 대화물류 기타비정산 그리드 저장
	 * 작성자                 : schan
	 * @param   model
	 * @return  ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC240/saveTypeC.action")
	public ModelAndView saveTypeC(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveTypeC(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : excel
	 * Method 설명      : 대화물류 화물수탁증 입력/조회 엑셀 다운
	 * 작성자                 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC240/excel.action")
	public void excel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
        	String[][] headerEx = null;
        	String[][] valueName = null;
        	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
    		headerEx = new String[][] {
    			
				{MessageResolver.getText("납품일자")    , "0", "0", "0", "1", "100"},
				{MessageResolver.getText("고객사명")    , "1", "1", "0", "1", "100"},
				{MessageResolver.getText("고객사코드")    , "2", "2", "0", "1", "100"},
				{MessageResolver.getText("납품처명")    , "3", "3", "0", "1", "100"},
				{MessageResolver.getText("납품센터명")    , "4", "4", "0", "1", "100"},
				
				{MessageResolver.getText("고객사")    , "5", "8", "0", "0", "100"},
				{MessageResolver.getText("박스")    , "5", "5", "1", "1", "100"},
				{MessageResolver.getText("파렛트(DC,TRD,VIC)"), "6", "6", "1", "1", "100"},
				{MessageResolver.getText("매출액")    , "7", "7", "1", "1", "100"},
				{MessageResolver.getText("차량")    , "8", "8", "1", "1", "100"},
				
				{MessageResolver.getText("파렛트이동수량")    , "9", "10", "0", "0", "100"},
				{MessageResolver.getText("상차")    , "9", "9", "1", "1", "100"},
				{MessageResolver.getText("하차")    , "10", "10", "1", "1", "100"},
				
				{MessageResolver.getText("비고")    , "11", "11", "0", "1", "100"},
				{MessageResolver.getText("납품수량")    , "12", "12", "0", "1", "100"},
				
				{MessageResolver.getText("납품이상내역")    , "13", "16", "0", "0", "100"},
				{MessageResolver.getText("미납/과다수량")    , "13", "13", "1", "1", "100"},
				{MessageResolver.getText("미납사유")    , "14", "14", "1", "1", "100"},
				{MessageResolver.getText("상세내역")    , "15", "15", "1", "1", "100"},
				{MessageResolver.getText("통화자")    , "16", "16", "1", "1", "100"}
			};
			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
    		valueName = new String[][] {
				{"WORK_DT"   , "S"},
				{"CUST_NM"   , "S"},
				{"CUST_CD"   , "S"},
				{"TRANS_ZONE_NM"   , "S"},
				{"TRANS_CUST_NM"   , "S"},
				{"CUST_BOX_QTY"   , "S"},
				
				{"CUST_PLT_QTY"   , "S"},
				{"SALES_COST"   , "S"},
				{"CUST_CAR_PLT_QTY"   , "S"},
				{"CUST_UPLOAD_PLT_QTY"   , "S"},
				{"CUST_DOWNLOAD_PLT_QTY"   , "S"},
				
				{"MEMO2"   , "S"},
				{"DLV_QTY"   , "S"},
				{"P_M_QTY"   , "S"},
				{"NOT_DLV_RSN"   , "S"},
				{"NOT_DLV_DTI_RSN"   , "S"},
				
				{"CALL_USER"   , "S"}
			};    	
			// 파일명
    		long curTime = System.currentTimeMillis();
    		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyymmddhhmmss");
    		String timeStr = timeFormat.format(new Date(curTime));
			String fileName = MessageResolver.getText("수탁증") + "_" +timeStr;
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}


	/*-
	 * Method ID : wmsac240Q1
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSAC240Q1.action")
	public ModelAndView wmsac240Q1(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC240Q1");
	}
}
