package com.logisall.winus.tmsbd.web;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmsbd.service.TMSBD010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSBD010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSBD010Service")
	private TMSBD010Service service;

	/*-
	 * Method ID    : TMSBD010
	 * Method 설명      : 공지사항 화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	// @RequestMapping("/WINUS/TMSBD010.action")
	@RequestMapping("/WINUS/TMSBD010.action")
	// @RequestMapping("/WINUS/TMSYS130.action")
	public ModelAndView TMSBD010(Map<String, Object> model) {
		return new ModelAndView("winus/TMSBD/TMSBD010");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 공지사항 목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/TMSBD010/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : detail
	 * Method 설명 : 공지사항 상세 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/TMSBD010/detail.action")
	public ModelAndView detail(Map<String, Object> model) throws Exception {
		Map<String, Object> detailInfo = null;
		try {
			detailInfo = service.detail(model);
			if ( detailInfo != null) {
				Map<String, Object> boardInfo = (Map<String, Object>)detailInfo.get("DETAIL");
				
				if (boardInfo != null) {
					String boardId = (String) boardInfo.get("BOARD_ID");					
					String userNo = (String)model.get("SS_USER_NO");
					
					if ( boardInfo.get("BOARD_STEP") != null ) {
						BigDecimal boardStep = (BigDecimal) boardInfo.get("BOARD_STEP"); 						
						if ( userNo != null && userNo.equals(boardId)) {
							if ( boardStep.intValue() < 1 ) {
								return new ModelAndView("winus/TMSBD/TMSBD010E2", detailInfo);
								
							} else {
								boardInfo.put("REPLY_FLAG", "U");
								return new ModelAndView("winus/TMSBD/TMSBD010E4", detailInfo);
								
							}					
						}
					}
				}
			}			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return new ModelAndView("winus/TMSBD/TMSBD010E3", detailInfo);
	}
	
	/*-
	 * Method ID : pop
	 * Method 설명 : 공지사항 팝업 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/TMSBD010/pop.action")
	public ModelAndView pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/TMSBD/TMSBD010E5", service.detail(model));
	}	
	
	
	/*-
	 * Method ID : reply
	 * Method 설명 : 공지사항 답변 작성 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/TMSBD010/reply.action")
	public ModelAndView reply(Map<String, Object> model) throws Exception {		
		return new ModelAndView("winus/TMSBD/TMSBD010E4", service.detail(model));
	}

	
	/*-
	 * Method ID : save
	 * Method 설명 : 공지사항 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSBD010/save.action")
	public ModelAndView save(Map<String, Object> model) {
		
		log.info(model);
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}	
	
	/*-
	 * Method ID : saveReply
	 * Method 설명 : 공지사항 답변 저장
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSBD010/saveReply.action")
	public ModelAndView saveReply(Map<String, Object> model) {
		
		log.info(model);
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveReply(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : TMSBD010e2
	 * Method 설명      : 공지사항 상세화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/TMSBD010E2.action")
	public ModelAndView TMSBD010e2(Map<String, Object> model) {
		return new ModelAndView("winus/TMSBD/TMSBD010E2");
	}

	/*-
	 * Method ID    : TMSBD010e3
	 * Method 설명      : 거래처정산일설정 팝업화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/TMSBD010E3.action")
	public ModelAndView TMSBD010e3(Map<String, Object> model) {
		return new ModelAndView("winus/TMSBD/TMSBD010E3");
	}

}
