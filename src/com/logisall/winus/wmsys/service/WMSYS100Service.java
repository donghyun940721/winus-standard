package com.logisall.winus.wmsys.service;

import java.util.Map;


public interface WMSYS100Service {
	/**interfacebody*/
	public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveE1(Map<String, Object> model) throws Exception;
	public Map<String, Object> deleteE1(Map<String, Object> model) throws Exception;	
}
