package com.logisall.winus.wmsys.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.wmsys.service.WMSYS100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSYS100Service")
public class WMSYS100ServiceImpl extends AbstractServiceImpl implements WMSYS100Service {
    
    @Resource(name = "WMSYS100Dao")
    private WMSYS100Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE1(model));
        return map;
    }
    
    
    /**
     * Method ID  	: listE3
     * Method 설명    	: 로젠택배 송장접수이력 조회
     * 작성자              	: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE3(model));
        return map;
    }
    
    
    /**
     * Method ID   : listE2
     * Method 설명    : 한진택배 IF로그이력 조회
     * 작성자               : dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE2(model));
        return map;
    }
    
    
    /**
     * Method ID	: listE4
     * Method 설명    	: EAI 오류 테이블 조회
     * 작성자               	: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listE4(model));
        return map;
    }
   
    
	/**
     * 
     * 대체 Method ID	: saveE1
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> rowData = null;

        try{
        	JSONParser jsonParser 		= new JSONParser();
        	ArrayList<Object> gridData 	= (ArrayList<Object>) jsonParser.parse(model.get("gridData").toString());

        	for(Object dt : gridData){
        		rowData = new ObjectMapper().convertValue(dt, Map.class);        	
        		dao.saveE1(rowData);  
        	}       	    	        
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
            map.put("MSG_ORA", "");
            map.put("errCnt", 0);        	
        	
        }
        catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("save.error") );
        }
        
        
        return map;
    }

    
	/**
     * 
     * 대체 Method ID	: deleteE1
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deleteE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> rowData = null;

        try{
        	JSONParser jsonParser 		= new JSONParser();
        	ArrayList<Object> gridData 	= (ArrayList<Object>) jsonParser.parse(model.get("gridData").toString());

        	for(Object dt : gridData){
        		rowData = new ObjectMapper().convertValue(dt, Map.class);        	
        		dao.deleteE1(rowData);  
        	}       	    	        
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
            map.put("MSG_ORA", "");
            map.put("errCnt", 0);        	
        	
        }
        catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("save.error") );
        }
        
        
        return map;
    }
}
