package com.logisall.winus.wmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSYS040Dao")
public class WMSYS040Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 입출고주문 그리드관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsys040.listTmp", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 입출고주문 그리드관리 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){        
        return executeUpdate("wmsys040.pk_wmsys030.sp_save_template_grid", model);
    }
    
    /**
     * Method ID    : delete
     * Method 설명      : 입출고주문 그리드관리 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object delete(Map<String, Object> model){
        return executeUpdate("wmsys040.pk_wmsys030.sp_delete_template_grid", model);
    }
}
