package com.logisall.winus.wmsys.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSYS400Service")
public class WMSYS400ServiceImpl implements WMSYS400Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSYS400Dao")
    private WMSYS400Dao dao;

    
    public Map<String, Object> list(Map<String, Object> model){
    	
    	Map<String, Object> map = new HashMap<String, Object>();

		map.put("LIST", dao.list(model));    		
 	
    	return map;
    }
    
    public List<Object> list(Map<String, Object> model, String serviceNm, String separator1, String separator2, String separator3, String separator4, String separator5){
    	
    	Map<String, Object> map = new HashMap<String, Object>();
        		
		model.put("vrServiceNm", serviceNm);
		model.put("vrCompleteYn", "N");			
		model.put("vrSeparator1", separator1);
		model.put("vrSeparator2", separator2);
		model.put("vrSeparator3", separator3);
		model.put("vrSeparator4", separator4);
		model.put("vrSeparator5", separator5);	
		
//		return dao.list(model) ;	
		return dao.list(model).getList();
//    	return map;
    }
    

    public List<Map<String, Object>> listMap(Map<String, Object> model) throws Exception{
    	return dao.listMap(model);
    }
    
    public List<Map<String, Object>> listSeq(Map<String, Object> model) throws Exception{
        return dao.listSeq(model);
    }
    

    public List<Map<String, Object>> getLastWorkInfo(Map<String, Object> model) throws Exception{
        return dao.getLastWorkInfo(model);
    }
    
    
    
    public Boolean checkConcurrency(Map<String, Object> model){    	    
    	List concurcInfo; 
    	Boolean isWork = false;
        		
		model.put("vrServiceNm", "TEST_SERVICE123");
		model.put("vrCompleteYn", "N");			
		model.put("vrSeparator1", "");
		model.put("vrSeparator2", "");
		model.put("vrSeparator3", "");
		model.put("vrSeparator4", "");
		model.put("vrSeparator5", "");	
				
		concurcInfo = dao.list(model).getList();
		
		isWork = (concurcInfo.size() > 0);
 	
    	return isWork;
    }
    
    
    public Boolean checkConcurrency(Map<String, Object> model, String serviceNm, String separator1, String separator2, String separator3, String separator4, String separator5){
	        	
    	List concurcInfo; 
    	Boolean isWork = false;
        		
		model.put("vrServiceNm", serviceNm);
		model.put("vrCompleteYn", "N");			
		model.put("vrSeparator1", separator1);
		model.put("vrSeparator2", separator2);
		model.put("vrSeparator3", separator3);
		model.put("vrSeparator4", separator4);
		model.put("vrSeparator5", separator5);	
				
		concurcInfo = dao.list(model).getList();
		
		isWork = (concurcInfo.size() > 0);
 	
    	return isWork;
    }    
    

	
	/**
	 * 동시성 제어 작업구분 생성
	 * 
	 * 대체 Method ID		: insert
	 * 작성자				: dhkim
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> insert(Map<String, Object> model){
	
		Map<String, Object> map = new HashMap<String, Object>();
		
		try{
			Object concurcSeq = dao.getSequence(model);
				
			model.put("vrConcurcSeq", concurcSeq);
			model.put("vrServiceNm",  "d");
			model.put("vrSeparator1", "d");
			model.put("vrSeparator2", "d");
			model.put("vrSeparator3", "d");
			model.put("vrSeparator4", "d");
			model.put("vrSeparator5", "d");
			
			dao.save(model);
		    
			map.put("MSG", MessageResolver.getMessage("save.success"));
			map.put("MSG_ORA", "");
			map.put("errCnt", 0);   
			
		}catch(Exception e){        	
	    	map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
		}
		

		return map;
	}
	
	/**
	 * 동시성 제어 작업구분 생성
	 * 
	 * 대체 Method ID		: insert
	 * 작성자				: dhkim
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> insert(Map<String, Object> model, String serviceNm, String separator1, String separator2, String separator3, String separator4, String separator5){
	
		Map<String, Object> map = new HashMap<String, Object>();
		
		try{
			
			String seq = ((Map<String, Object>)dao.getSequence(model)).get("SEQ").toString();
			model.put("vrConcurcSeq", seq);
			model.put("vrServiceNm", serviceNm);
			model.put("vrSeparator1", separator1);
			model.put("vrSeparator2", separator2);
			model.put("vrSeparator3", separator3);
			model.put("vrSeparator4", separator4);
			model.put("vrSeparator5", separator5);
			
			dao.save(model);
		    
			map.put("MSG", MessageResolver.getMessage("save.success"));
			map.put("MSG_ORA", "");
			map.put("SEQ", seq);
			map.put("errCnt", 0);   
			
		}catch(Exception e){        	
	    	map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
		}
		

		return map;
	}
	
	

	/**
	 * 동시성 제어 작업구분 생성
	 * 
	 * 대체 Method ID		: insertDataSet
	 * 작성자				:  yhku
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> insertDataSet(Map<String, Object> model){
	
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			String seq = ((Map<String, Object>)dao.getSequence(model)).get("SEQ").toString();
			model.put("vrConcurcSeq"	, seq);
			dao.save(model);
		    
			map.put("MSG", MessageResolver.getMessage("save.success"));
			map.put("MSG_ORA", "");
			map.put("SEQ", seq);
			map.put("errCnt", 0);   
			
		}catch(Exception e){        	
	    	map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
		}
		
		return map;
	}
	
	
	
	/**
	 * 작업완료
	 * 
	 * 대체 Method ID		: complete
	 * 작성자				: dhkim
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> complete(Map<String, Object> model){
	
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> rowData = null;
		Map<String, Object> param = new HashMap<String, Object>();
		
		try{			
			JSONParser jsonParser 		= new JSONParser();
        	ArrayList<Object> gridData 	= (ArrayList<Object>) jsonParser.parse(model.get("gridData").toString());
        	
        	String userNo = model.get("SS_USER_NO").toString();
        	
        	for(Object dt : gridData){        		
        		rowData = new ObjectMapper().convertValue(dt, Map.class);
        		
        		param.put("SS_USER_NO", userNo);
        		param.put("vrSeq", rowData.get("SEQ"));        		
        		
        		dao.complete(param);	        		
        	}			
		    
			map.put("MSG", MessageResolver.getMessage("save.success"));
			map.put("MSG_ORA", "");
			map.put("errCnt", 0);   
			
		}catch(Exception e){        	
	    	map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
		}
		

		return map;
	}
	
	
	/**
	 * 작업완료
	 * 
	 * 대체 Method ID		: complete
	 * 작성자				: dhkim
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> complete(Map<String, Object> model, String seq){
	
		Map<String, Object> map = new HashMap<String, Object>();
		
		try{
	
			model.put("vrSeq", seq);
			
			dao.complete(model);
		    
			map.put("MSG", MessageResolver.getMessage("save.success"));
			map.put("MSG_ORA", "");
			map.put("errCnt", 0);   
			
		}catch(Exception e){        	
	    	map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
		}
		

		return map;
	}

}
