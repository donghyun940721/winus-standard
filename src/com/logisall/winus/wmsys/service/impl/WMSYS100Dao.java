package com.logisall.winus.wmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSYS100Dao")
public class WMSYS100Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : IF 이력조회 
     * 작성자                 : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
        return executeQueryPageWq("wmsys100.listE1", model);
    }
    
    
    /**
     * Method ID    : list
     * Method 설명      	: 한진택배 IF로그 조회
     * 작성자                	: chsong
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmsys100.listE2", model);
    }    
    
    
    /**
     * Method ID	: list
     * Method 설명	: 로젠택배 접수이력 조회
     * 작성자             		: dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryPageWq("wmsys100.listE3", model);
    }    
    
    
    /**
     * Method ID	: list
     * Method 설명	: EAI 오류 테이블 조회
     * 작성자             		: dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE4(Map<String, Object> model) {
        return executeQueryPageWq("wmsys100.listE4", model);
    }    
    
        
	public Object saveE1(Object model) {
		return executeUpdate("wmsys100.saveE1", model);
	}
	
	
	public Object deleteE1(Object model) {
		return executeUpdate("wmsys100.deleteE1", model);
	}     
     
}
