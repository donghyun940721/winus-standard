package com.logisall.winus.wmsys.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.common.util.XmlTransform;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsys.service.WMSYS300Service;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSYS300Service")
public class WMSYS300ServiceImpl implements WMSYS300Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSYS300Dao")
    private WMSYS300Dao dao;
    
  
    /**
     * Method ID : list
     * Method 설명 : 송신문서 내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
            model.put("S_SVC_NO", 	ConstantWSIF.WS_ADMIN_ID);
            model.put("SS_SVC_NO", 	ConstantWSIF.WS_ADMIN_ID);            
            
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 
     * Method ID   : insert_R
     * Method 설명 : 송수신문서 재처리
     * 작성자      :  기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
                
        try {
            model.put("D_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
            model.put("D_TR_STAT","R");
            model.put("D_ERR_CD","");
            model.put("D_MESSAGE","");
            model.put("INSERT_NO", model.get(ConstantIF.SS_USER_ID));
            dao.insert(model);
            
            Map<String, Object> modelDt = new HashMap<String, Object>();
            // modelDt.put("SVC_NO", model.get(ConstantIF.SS_SVC_NO));
            modelDt.put("SVC_NO", model.get(ConstantIF.LC_ADMIN));
            
            modelDt.put("TR_ID", model.get("D_TR_ID"));
            modelDt.put("TR_STAT", "RS");
            
            dao.updateTrStat(modelDt);
            
            map.put("MSG", MessageResolver.getMessage("complete"));
        } catch (Exception e) {
            e.printStackTrace();
            map.put("MSG", MessageResolver.getMessage("save.error", new String[] {}));
        }
        return map; 
    }
    
    /**
     * 
     * Method ID   : detail
     * Method 설명 : xml 원문보기
     * 작성자      :  기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        XmlTransform xfrm = new XmlTransform();
        
        String pathDirectory = (String)model.get("S_PATH");
        String fileName = (String)model.get("S_FILENAME");
        
        String filePath = (new StringBuffer()
        		.append(ConstantIF.TEMP_PATH)
        		.append(pathDirectory)
        		.append(File.separator)
        		.append(fileName)
        		).toString();
        
        model.put("FILEPATH", filePath);        
        map = (Map)(xfrm.creatXmlString(model));
                
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 물류용기관리 저장
     * 작성자                      : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> runProc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("I_SUBUL_DT"      , (String)model.get("PARAM_DATE_SET"+1));
            modelIns.put("I_LC_ID"         , (String)model.get("PARAM_LC_ID"+1));
            //modelIns.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
            //modelIns.put("WORD_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            //modelIns.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.runProc(modelIns);
            ServiceUtil.isValidReturnCode("WMSYS300", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : excelUploadRstApiImgGet
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> excelUploadRstApiImgGet(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
                Map<String, Object> paramMap = null;
    			for (int i = 0; i < list.size(); i++) {
    				paramMap = (Map) list.get(i);
    				Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("CUST_CD"	, paramMap.get("CUST_CD"));
					//modelSP.put("ORG_ORD_ID", paramMap.get("ORG_ORD_ID"));
    				
					//String dlvOrdId  = dao.checkExistData(modelSP);
					String dlvOrdId  = (String)paramMap.get("DLV_ORD_ID");
    				String serialUrl = (String)paramMap.get("SERIAL_IMG_PATH");
    				
    				String[] addDlvImgPath = paramMap.get("DLV_IMG_PATH").toString().split("\n");
		            for (int k = 0; k < addDlvImgPath.length; k++) {
		            	if (  dlvOrdId != null && !StringUtils.isEmpty(dlvOrdId)
		            	   && addDlvImgPath[k] != null && !StringUtils.isEmpty(addDlvImgPath[k])){
							// 1. 원주문번호 검색 후 이미지URL이 있으면 이미지 파일 저장.
		            		String ext       = ".JPEG";
		            		String fileId    = dlvOrdId + "_" + CommonUtil.getLocalDateTime().substring(0, 8) + "photo"+(k+1) + ext;
		            		String fileRoute = "E:/ATCH_FILE/INTERFACE/"+CommonUtil.getLocalDateTime().substring(2, 8)+"/"+dlvOrdId+"_"+CommonUtil.getLocalDateTime().substring(0, 8);
		            		String fileNewNm = "photo"+(k+1);
		            		String fileSize  = "0";
		            		String workOrdId = dlvOrdId;
		            		
		            		String imgUrl = addDlvImgPath[k];
		            		String dir    = "E:\\ATCH_FILE\\INTERFACE\\"+CommonUtil.getLocalDateTime().substring(2, 8)+"\\"+dlvOrdId+"_"+CommonUtil.getLocalDateTime().substring(0, 8);
		            		String serialFile = "photo0"+ext;
		            		String goodsFile  = "photo"+(k+1)+ext;
		            		String serialFileId = dlvOrdId + "_" + CommonUtil.getLocalDateTime().substring(0, 8) + serialFile;
		            		if(k == 0 && serialUrl != null && !StringUtils.isEmpty(serialUrl)){
		            			// 시리얼 이미지가 있으면 저장
		            			binaryImgSave(serialUrl, dir, serialFile);
		            			
		            			// 2. 이미지 파일 저장 완료 후 저장정보 DB Insert.	            		
			            		Map<String, Object> modelDt = new HashMap<String, Object>();
			            		modelDt.put("FILE_ID"		, serialFileId);
								modelDt.put("ATTACH_GB"		, "INTERFACE"); // 통합 HELPDESK 업로드
								modelDt.put("FILE_VALUE"	, "wmssp010"); // 기존코드 "tmsba230" 로
								modelDt.put("FILE_PATH"		, fileRoute); // 서버저장경로
								modelDt.put("ORG_FILENAME"	, serialFile); // 원본파일명
								modelDt.put("FILE_EXT"		, ext); // 파일 확장자
								modelDt.put("FILE_SIZE"		, fileSize);// 파일 사이즈
								modelDt.put("WORK_ORD_ID"	, workOrdId); // 원본파일명
								
								String fileMngNo = (String) dao.t2FileInsert(modelDt);
								dao.insertInfo(modelDt);
		            		}
		            		// 상품 이미지 저장
		            		binaryImgSave(imgUrl, dir, goodsFile);
		            		
		            		// 2. 이미지 파일 저장 완료 후 저장정보 DB Insert.	            		
		            		Map<String, Object> modelDt = new HashMap<String, Object>();
		            		modelDt.put("FILE_ID"		, fileId);
							modelDt.put("ATTACH_GB"		, "INTERFACE"); // 통합 HELPDESK 업로드
							modelDt.put("FILE_VALUE"	, "wmssp010"); // 기존코드 "tmsba230" 로
							modelDt.put("FILE_PATH"		, fileRoute); // 서버저장경로
							modelDt.put("ORG_FILENAME"	, fileNewNm + ext); // 원본파일명
							modelDt.put("FILE_EXT"		, ext); // 파일 확장자
							modelDt.put("FILE_SIZE"		, fileSize);// 파일 사이즈
							modelDt.put("WORK_ORD_ID"	, workOrdId); // 원본파일명
							
							String fileMngNo = (String) dao.t2FileInsert(modelDt);
							dao.insertInfo(modelDt);
							
		                    map.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
		                    map.put("MSG_ORA", "");
		                    map.put("errCnt", errCnt);
	                    }
		            }
    			}
                //dao.excelUploadRstApiImgGet(model, list);
                
            } catch(Exception e){
            	map.put("MSG", MessageResolver.getMessage("save.error"));
            	map.put("MSG_ORA", e.getMessage());
            	map.put("errCnt", "1");
                throw e;
            }
        return map;
    }    
    
	public void binaryImgSave(String imgUrl, String dir, String file) throws Exception{
		//하우저 인증키 정보 필수
        //개발서버
		//String key    = "OpenApiKey";
		//String value  = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1ZWVmZGM1My03ZDk4LTQyNzMtOGM5Yi0xMTcyZTE0MmNiNDAiLCJnIjoiZ3JwMTgwODAwMDA2Iiwic3YiOnsiMSI6MX19.pbbcziGaU_oNOSu_NoGXgcMT2kjvshk5isXjLYObsAHshSiWoC6QAxmOqG8fSoO8hwmRHP32Ld8anwnN6uefDDivleocAAZj3S-9RaM_xaKqJUJsbtOEtYaKt4u6ZBBUD8Xy-ZUdLgiID5qw-YfYSY6WeWVmmOuU3v0oc7Gn8e4";
        //운영서버
		String key    = "OpenApiKey";
		String value  = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIzZWY3YWY5OC0yMzYyLTQwY2MtYmNhYS1hYjE0OGFhYWQ2ZjIiLCJnIjoiZ3JwMTgwODAwMDE1Iiwic3YiOnsiMSI6MX19.UP9_Q1N30vUUTt3pr1EjZ3sybrpekqamlY2yHjl2a9oUuXiqss9T2_iBEh86VQXPCCkTuJYSgjMqjD36WLFaP2qJdFSK1ggdZVkYh-5mbcIA8r1P3XudG4gw1Qvsv9kSd84Jd3G-aF5Vt4Fa1icUpvyLmMBrlb9Jwa1_UX7-UHo";
        String rstUrl = imgUrl;
        
		URL url = new URL(rstUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
		con.setConnectTimeout(50000);	//서버에 연결  Timeout 설정
		con.setReadTimeout(50000);		// InputStream 읽기 Timeout 설정
		con.setRequestMethod("GET");
		con.setDoOutput(false); 
		con.setDoInput(true);
		
		//header 파라미터셋팅
		con.addRequestProperty(key, value);
		con.setRequestProperty("Content-Type", "application/json");
		
		if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
        	int len = con.getContentLength();
            byte[] tmpByte = new byte[len];
            
            File chkDir   = new File(dir);
            File savePath = new File(dir +"\\"+ file);
            
            if(!chkDir.isDirectory()){
            	// 해당 폴더가 없으면 생성
            	chkDir.mkdirs();
			}
            
            InputStream is = con.getInputStream();
            FileOutputStream fos = new FileOutputStream(savePath);

            int read;
            while(true) {
                read = is.read(tmpByte);
                if (read <= 0) {
                    break;
                }
                fos.write(tmpByte, 0, read); //file 생성
            }

            is.close();
            fos.close();
            con.disconnect();
		}
	}
	
	/**
     * Method ID : excelUploadCsOrdUpload
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	public Map<String, Object> excelUploadCsOrdUpload(Map<String, Object> model, List list) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       int errCnt = 0;
       int insertCnt = (list != null)?list.size():0;
           try{            	
               dao.excelUploadCsOrdUpload(model, list);
               
               m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
               m.put("MSG_ORA", "");
               m.put("errCnt", errCnt);
               
           } catch(Exception e){
               throw e;
           }
       return m;
   }
	
	    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJavaTEST
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaTEST(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		long start = System.currentTimeMillis();
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];     
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] salesCompanyNm	 	= new String[listBodyCnt];
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                String[] locCd     		= new String[listBodyCnt];   //로케이션코드
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT      = "";
        			String IN_REQ_DT       = "";
        			String CUST_ORD_NO     = "";
        			String CUST_ORD_SEQ    = "";
        			String TRUST_CUST_CD   = "";
        			String TRANS_CUST_CD   = "";
        			String TRANS_CUST_TEL  = "";
        			String TRANS_REQ_DT    = "";
        			String CUST_CD         = "";
        			String ORD_QTY         = "";
        			String UOM_CD          = "";
        			String SDEPT_CD        = "";
        			String SALE_PER_CD     = "";
        			String CAR_CD          = "";
        			String DRV_NM          = "";
        			String DLV_SEQ         = "";
        			String DRV_TEL         = "";
        			String CUST_LOT_NO     = "";
        			String BL_NO           = "";
        			String REC_DT          = "";
        			String OUT_WH_CD       = "";
        			String IN_WH_CD        = "";
        			String MAKE_DT         = "";
        			String TIME_PERIOD_DAY = "";
        			String WORK_YN         = "";
        			String RJ_TYPE         = "";
        			String LOC_YN          = "";
        			String CONF_YN         = "";
        			String EA_CAPA         = "";
        			String IN_ORD_WEIGHT   = "";
        			String ITEM_CD         = "";
        			String ITEM_NM         = "";
        			String TRANS_CUST_NM   = "";
        			String TRANS_CUST_ADDR = "";
        			String TRANS_EMP_NM    = "";
        			String REMARK          = "";
        			String TRANS_ZIP_NO    = "";
        			String ETC2            = "";
        			String UNIT_AMT        = "";
        			String TRANS_BIZ_NO    = "";
        			String IN_CUST_ADDR    = "";
        			String IN_CUST_CD      = "";
        			String IN_CUST_NM      = "";
        			String IN_CUST_TEL     = "";
        			String IN_CUST_EMP_NM  = "";
        			String EXPIRY_DATE     = "";
        			String SALES_CUST_NM   = "";
        			String ZIP             = "";
        			String ADDR            = "";
        			String PHONE_1         = "";
        			String ETC1            = "";
        			String UNIT_NO         = "";
        			String SALES_COMPANY_NM   = "";
        			String TIME_DATE       = "";
        			String TIME_DATE_END   = "";
        			String TIME_USE_END    = "";
        			String LOC_CD    	   = "";
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				//if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_CD = (String)model.get("vrCustCd");
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_CD")){LOC_CD = object.get("S_"+k).toString().replaceAll("\"", "");};

        			}
     				
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;    
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;     
                    
                    timeUseEnd[i]       = TIME_USE_END;
                    locCd[i]       		= LOC_CD;
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
               
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
               
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);      
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("phone1"       	, phone1);
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                
                modelIns.put("salesCompanyNm"    	, salesCompanyNm);
                
                modelIns.put("time_date"        , timeDate);
                modelIns.put("time_date_end"    , timeDateEnd);                
                modelIns.put("time_use_end"     , timeUseEnd);  
                modelIns.put("locCd"     		, locCd);  
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

                //dao
                long end = System.currentTimeMillis();
                modelIns = (Map<String, Object>)dao.saveExcelOrderJavaTEST(modelIns);
                String msg = (String)modelIns.get("msgDao");
                msg += ("\nConvert List to Parameter : " + (end-start)/1000 + "초");
                m.put("msgService",msg);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                m.put("O_CUR", modelIns.get("O_CUR"));                
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcelTest
     * Method 설명 : 송신문서 내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelTest(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
                      
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listExcelTest(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    public Map<String, Object> AES256DECRIPT(Map<String, Object> model) throws Exception{
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            String res = "";
            String key = "";
            String input = "";
            //AES256 키
            input = (String)model.get("input");
            key = (String)model.get("key");
            //암호화방식 선택
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            ByteBuffer buffer = ByteBuffer.wrap(Base64.decodeBase64(input));
        
            byte[] saltBytes = new byte[20];
            buffer.get(saltBytes, 0, saltBytes.length);
            byte[] ivBytes = new byte[cipher.getBlockSize()];
            buffer.get(ivBytes, 0, ivBytes.length);
            byte[] encryoptedTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes.length];
            buffer.get(encryoptedTextBytes);
            
            //키해제 알고리즘 방식
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            PBEKeySpec spec = new PBEKeySpec(key.toCharArray(), saltBytes, 1, 256);
        
            SecretKey secretKey = factory.generateSecret(spec);
            SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
        
            cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes));
        
            byte[] decryptedTextBytes = cipher.doFinal(encryoptedTextBytes);
            
            res = new String(decryptedTextBytes);
            
            res = URLDecoder.decode(res, "utf-8");

                      
            map.put("data", res);
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    public Map<String, Object> AES256ENCRIPT(Map<String, Object> model) throws Exception{
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            String res = "";
            String key = "";
            String input = "";
            //AES256 키
            input = (String)model.get("input");
            key = (String)model.get("key");
            
            byte bytes[] = new byte[20];
            SecureRandom random = new SecureRandom();
            random.nextBytes(bytes);
            byte[] saltBytes = bytes;
            
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            PBEKeySpec spec = new PBEKeySpec(key.toCharArray(), saltBytes, 1, 256);
            SecretKey secretKey = factory.generateSecret(spec);
            SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
            
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            AlgorithmParameters params = cipher.getParameters();
            
            byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
            
            byte[] encryptedTextBytes = cipher.doFinal(input.getBytes("UTF-8"));
            
            byte[] buffer = new byte[saltBytes.length + ivBytes.length + encryptedTextBytes.length];
            
            System.arraycopy(saltBytes, 0, buffer, 0, saltBytes.length);
            System.arraycopy(ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
            System.arraycopy(encryptedTextBytes, 0, buffer, saltBytes.length + ivBytes.length, encryptedTextBytes.length);
            
            
            res = Base64.encodeBase64String(buffer);
            map.put("data", res);
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 대체 Method ID   : stockAllInit
     * 대체 Method 설명    : 23/04/03
     * 작성자                    : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> stockAllInit(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("I_CUST_ID"   , (String)model.get("vrSrchCustId"));
            modelIns.put("I_LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_WORK_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("I_USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
            
            //System.out.println(modelIns.toString());
            modelIns = (Map<String, Object>)dao.stockAllInit(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSYS300", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : saveVideo
     * 대체 Method 설명    : 비디오 blob type db insert
     * 작성자                    : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveVideo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
            //modelIns.put("CUST_ID"     , (String)model.get("CUST_ID"));
            //modelIns.put("ORD_ID"     , (String)model.get("ORD_ID"));
            
            modelIns.put("VIDEO_ID"     , model.get("VIDEO_ID"));
            modelIns.put("URL"     , model.get("URL"));
            modelIns.put("FILE_NAME"     , model.get("FILE_NAME"));
            
            modelIns.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("WORK_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            
            modelIns = (Map<String, Object>)dao.saveVideo(modelIns);
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : getVideoID
     * Method 설명 : 비디오 ID 추출
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getVideoID(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("VIDEO_ID",(String)dao.getVideoID(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID : getVideoData
     * Method 설명 : 비디오 데이터 select
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getVideoData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = dao.getVideoData(model);
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}
