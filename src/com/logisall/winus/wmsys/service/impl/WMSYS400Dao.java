package com.logisall.winus.wmsys.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSYS400Dao")
public class WMSYS400Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	
    /*-
     * Method ID 	: save
     * Method 설명 	: 동시성제어 정보 저장 (신규생성)
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsys400.list", model);
	}
	
	
	
    /*-
     * Method ID 	: listMap
     * Method 설명 	: 동시성제어 정보 저장 (신규생성)
     * 작성자 			: yhku
     *
     * @param model
     * @return
     */
	public List<Map<String, Object>> listMap(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object>>)getSqlMapClientTemplate().queryForList("wmsys400.list", model);
    }
	
    
	/**
	 * Method ID : getSequence
	 * Method 설명 : 동시성제어 시퀀스 조회
	 * 작성자 : dhkim
	 * @param model
	 * @return
	 */
	public Object getSequence(Map<String, Object> model) {
		return executeView("wmsys400.getSeq", model);
	}	
	
	
    /*-
     * Method ID 	: save
     * Method 설명 	: 동시성제어 정보 저장 (신규생성)
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public Object save(Object model) {
		return executeUpdate("wmsys400.save", model);
	}
	
	
    /*-
     * Method ID 	: complete
     * Method 설명 	: 동시성제어 작업해제
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public Object complete(Object model) {
		return executeUpdate("wmsys400.complete", model);
	}
	
	/*-
     * Method ID    : listSeq
     * Method 설명    : 동시성제어 seq 작업조회
     * 작성자          : 
     *
     * @param model
     * @return
     */
    public List listSeq(Map<String, Object> model) {
        return executeQueryForList("wmsys400.listSeq", model);
    }
	
    
    /*-
     * Method ID    : getLastWorkInfo
     * Method 설명    : 동시성제어 마지막 작업건 조회
     * 작성자          : yhku
     *
     * @param model
     * @return
     */
    public List getLastWorkInfo(Map<String, Object> model) {
        return executeQueryForList("wmsys400.getLastWorkInfo", model);
    }
	
 
}
