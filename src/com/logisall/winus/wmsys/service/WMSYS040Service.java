package com.logisall.winus.wmsys.service;

import java.util.Map;



public interface WMSYS040Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
}
