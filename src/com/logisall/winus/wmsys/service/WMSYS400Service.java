package com.logisall.winus.wmsys.service;

import java.util.List;
import java.util.Map;

import com.m2m.jdfw5x.egov.database.GenericResultSet;

public interface WMSYS400Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public List<Map<String, Object>> listMap(Map<String, Object> model) throws Exception;
	public List<Object> list(Map<String, Object> model, String serviceNm, String separator1, String separator2, String separator3, String separator4, String separator5) throws Exception;
	public List<Map<String, Object>> listSeq(Map<String, Object> model) throws Exception;
	public List<Map<String, Object>> getLastWorkInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> insert(Map<String, Object> model) throws Exception;
    public Map<String, Object> insert(Map<String, Object> model, String serviceNm, String separator1, String separator2, String separator3, String separator4, String separator5) throws Exception;
    public Map<String, Object> complete(Map<String, Object> model) throws Exception;
    public Map<String, Object> complete(Map<String, Object> model, String seq) throws Exception;
    public Boolean checkConcurrency(Map<String, Object> model) throws Exception;
    public Boolean checkConcurrency(Map<String, Object> model, String serviceNm, String separator1, String separator2, String separator3, String separator4, String separator5) throws Exception;
    public Map<String, Object> insertDataSet(Map<String, Object> model) throws Exception;
}
