package com.logisall.winus.wmsys.service;

import java.util.List;
import java.util.Map;

public interface WMSYS300Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception;
    public Map<String, Object> runProc(Map<String, Object> model) throws Exception;
    // xml 원문보기
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelUploadRstApiImgGet(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> excelUploadCsOrdUpload(Map<String, Object> model, List list) throws Exception;
    
    public Map<String, Object> saveExcelOrderJavaTEST(Map<String, Object> model, Map<String, Object> model2) throws Exception;
    public Map<String, Object> listExcelTest(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> AES256DECRIPT(Map<String, Object> model) throws Exception;
    public Map<String, Object> AES256ENCRIPT(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> stockAllInit(Map<String, Object> model) throws Exception; //재고초기화
    
    public Map<String, Object> getVideoID(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveVideo(Map<String, Object> model) throws Exception;
    public Map<String, Object> getVideoData(Map<String, Object> model) throws Exception;
    
}
