package com.logisall.winus.wmsys.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsys.service.WMSYS020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSYS020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	//temp argu summer
	static final String[] COLUMN_NAME_WMSYS020 = {"PROP_KEY", "PROP_VAL_KR", "PROP_VAL_JA", "PROP_VAL_CZ", "PROP_VAL_US", "PROP_VAL_VN", "PROP_VAL_ES"};
	
	@Resource(name = "WMSYS020Service")
	private WMSYS020Service service;

	/*-
	 * Method ID   : tmsys020
	 * Method 설명 : 권한관리 화면
	 * 작성자      : chsong 04.13.02.31
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS020.action")
	public ModelAndView wmsys020(Map<String, Object> model, HttpServletRequest request) {
		return new ModelAndView("winus/wmsys/WMSYS020");
	}

	/*-
	 * Method ID   : listAuth
	 * Method 설명 : 권한목록 조회
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/listProperties.action")
	public ModelAndView listProperties(Map<String, Object> model, HttpServletRequest request) {

		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listProperties(model, request));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : save
	 * Method 설명 : 프로그램목록 등록,수정,삭제
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/saveProperties.action")
	public ModelAndView saveProperties(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveProperties(model, request);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : save
	 * Method 설명 : 프로그램목록 등록,수정,삭제
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/checkProperties.action")
	public ModelAndView checkProperties(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.checkProperties(model, request);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to check...", e);
			}

		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명  : 코드상세목록 엑셀다운로드
	 * 작성자       : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			model.put("rows", 60000);
			map = service.listProperties(model, request);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");

			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                    { MessageResolver.getText("키"), "0", "0", "0", "0", "400"},
                    { MessageResolver.getText("한국어"), "1", "1", "0", "0", "400"},
                    { MessageResolver.getText("일본어"), "2", "2", "0", "0", "400"},
                    { MessageResolver.getText("중국어"), "3", "3", "0", "0", "400"},
                    { MessageResolver.getText("영어"), "4", "4", "0", "0", "400"},
                    { MessageResolver.getText("베트남어"), "5", "5", "0", "0", "400"},
                    { MessageResolver.getText("스페인어"), "6", "6", "0", "0", "400"}
            };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                    {"PROP_KEY"     , "S"},
                    {"PROP_VAL_KR"  , "S"},
                    {"PROP_VAL_JA"  , "S"},
                    {"PROP_VAL_CZ"  , "S"},
                    {"PROP_VAL_US"  , "S"},
                    {"PROP_VAL_VN"  , "S"},
                    {"PROP_VAL_ES"  , "S"}
            }; 
            
            //파일명 기초코드 목록
            String fileName = MessageResolver.getText("다국어관리목록");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
        	if (log.isErrorEnabled()) {
               	log.error("fail to download excel file...", e);
            }
        }
    }      

    /*-
	 * Method ID  : WMSYS020EXCEL 열기
	 * Method 설명  : Excel 파일 업로드 - 업데이트
	 * 작성자             : summer H
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
    
    @RequestMapping("/WMSYS020EXCEL.action")
	public ModelAndView wmsop642e3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsys/WMSYS020EXCEL");
	}
    
    
	/*-
	 * Method ID  : inExcelFileUploadTmp
	 * Method 설명  : Excel 파일 업로드 - .property 업데이트
	 * 구동 중인 로컬 서버 아래 있는 property파일 수정 후 서버 재구동 (해당 파일 경로에서 직접 가져오기)
	 * C:\project\workspace\.metadata\.plugins\org.eclipse.wst.server.core\tmp1\wtpwebapps\WINUSUSR_NEW\WEB-INF\classes\resource\message
	 * 작성자             : summer H
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/inExcelFileUploadTmp.action")
	public ModelAndView inExcelFileUploadTmp(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());
		
			//2번째줄 부터 2000번째 줄까지 업로드
			//컬럼 순서 꼭 맞추기 {"PROP_KEY", "PROP_VAL_KR", "PROP_VAL_JA", "PROP_VAL_CZ", "PROP_VAL_US", "PROP_VAL_VN", "PROP_VAL_ES"};
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSYS020, 0, 1, 5000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSYS020, 0, 1, 5000, 0);
			System.out.println("test");
			
			m = service.savePropertiesExcel(list, request);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
    
    
}
