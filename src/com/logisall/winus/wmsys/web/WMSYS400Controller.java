package com.logisall.winus.wmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSYS400Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service service;

	/*-
	 * Method ID : mn 
	 * Method 설명 : 인터페이스관리 화면 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS400.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmsys/WMSYS400");
	}

	/*-
	 * Method ID : list 
	 * Method 설명 : 송신문서 내역 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS400/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : complete 
	 * Method 설명 : 동시성 제어 완료처리
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS400/complete.action")
	public ModelAndView complete(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.complete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		
		mav.addAllObjects(m);
		
		return mav;
	}
	

}
