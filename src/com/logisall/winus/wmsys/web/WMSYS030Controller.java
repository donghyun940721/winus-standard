package com.logisall.winus.wmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.logisall.winus.wmsys.service.WMSYS030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSYS030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSYS030Service")
	private WMSYS030Service service;

	/*-
	 * Method ID   : mn
	 * Method 설명 : 템플릿 메인
	 * 작성자      : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS030.action")
	public ModelAndView mn(Map<String, Object> model, HttpServletRequest request) {
		return new ModelAndView("winus/wmsys/WMSYS030");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 템플릿관리 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : selectStartRow
	 * Method 설명      : 템플릿 시작행 조회
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/selectStartRow.action")
	public ModelAndView selectStartRow(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.selectStartRow(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : updateStartRow
	 * Method 설명      : 템플릿 시작행 변경
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/updateStartRow.action")
	public ModelAndView updateStartRow(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateStartRow(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명      : 템플릿관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS030/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : delete
	 * Method 설명      : 템플릿관리 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS030/delete.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to delete...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : templateCount
	 * Method 설명      : 템플릿개수
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS030/templateList.action")
	public ModelAndView templateCount(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.templateList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : templateCount
	 * Method 설명      : 템플릿개수
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS030/defaultTemplate.action")
	public ModelAndView defaultTemplate(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.defaultTemplate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : saveTemplate
	 * Method 설명      : 공란 저장
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/saveTemplate.action")
	public ModelAndView saveTemplate(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveTemplate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : updateTemplate
	 * Method 설명      : 공란 저장시 기존 컬럼 view_seq 증가
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/updateTemplate.action")
	public ModelAndView updateTemplate(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateTemplate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : updateTemplateName
	 * Method 설명      : 템플릿 명칭 변경
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/updateTemplateName.action")
	public ModelAndView updateTemplateName(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateTemplateName(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : insertTemplateCopy
	 * Method 설명      : 템플릿 복사
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/insertTemplateCopy.action")
	public ModelAndView insertTemplateCopy(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.insertTemplateCopy(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : insertNewTemplate
	 * Method 설명      : 템플릿 생성
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/insertNewTemplate.action")
	public ModelAndView insertNewTemplate(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.insertNewTemplate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : deleteTemplate
	 * Method 설명      : 템플릿 삭제
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS030/deleteTemplate.action")
	public ModelAndView deleteTemplate(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteTemplate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
