package com.logisall.winus.wmsys.web;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsys.service.WMSYS300Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

/**
 * @author iskim
 *
 */
@Controller
public class WMSYS300Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSYS300E7 = {
		"CUST_CD", "DLV_ORD_ID", "SERIAL_IMG_PATH", "DLV_IMG_PATH"
	};
	static final String[] COLUMN_NAME_WMSYS300E7_2 = {
		"CUST_CD", "ORG_ORD_ID", "CS_REQ_DT", "SALES_CUST_NM", "TEXT_REQ_CONTENTS", "TEXT_RESULT"
	};
	
	@Resource(name = "WMSYS300Service")
	private WMSYS300Service service;

	/*-
	 * Method ID : mn 
	 * Method 설명 : 인터페이스관리 화면 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS300.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmsys/WMSYS300");
	}

	/*-
	 * Method ID : list 
	 * Method 설명 : 송신문서 내역 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : rinsert 
	 * Method 설명 : 송수신문서 재처리 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/rinsert.action")
	public ModelAndView rinsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.insertRetry(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to insert...", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : popOrgnXmlUp 
	 * Method 설명 : xml 원문 보기 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/PopOrgnXmlUp.action")
	public ModelAndView popOrgnXmlUp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("winus/wmscm/WMSCM999Q1");
		Map<String, Object> m = null;
		try {
			m = service.detail(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to pop...", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : pop
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300E4.action")
	public ModelAndView pop(Map<String, Object> model) {
		return new ModelAndView("winus/wmsys/WMSYS300E4");
	}

	/*-
	 * Method ID    : save
	 * Method 설명      : 물류용기군관리 저장
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS300/runProc.action")
	public ModelAndView runProc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.runProc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID  : excelUploadRstApiImgGet
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/excelUploadRstApiImgGet.action")
	public ModelAndView excelUploadRstApiImgGet(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSYS300E7, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSYS300E7, 0, startRow, 10000, 0);
			m = service.excelUploadRstApiImgGet(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	/*-
	 * Method ID  : excelUploadCsOrdUpload
	 * Method 설명  : 
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/excelUploadCsOrdUpload.action")
	public ModelAndView excelUploadCsOrdUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSYS300E7_2, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSYS300E7_2, 0, startRow, 10000, 0);
			m = service.excelUploadCsOrdUpload(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}

	/*-
	 * Method ID : 엑셀 업로드 테스트
	 * Method 설명 : 
	 * 작성자 : schan
	 *
	 * @param model
	 * @return
	 */
	@Resource(name = "WMSOP030Service")
	private WMSOP030Service service030;

	@RequestMapping("/WMSYS250E2/inExcelFileUploadJavaTEST.action")
	public ModelAndView excelupload(HttpServletRequest request,
			HttpServletResponse response, Map<String, Object> model,
			@RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;

		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName,
					destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(
					destination));

			/* 엑셀 데이터 추출 */
			int colSize = Integer.parseInt((String) model.get("colSize"));

			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			long start = System.currentTimeMillis();
			List list = ExcelReader.excelLimitRowReadByHandler(destination, cellName, 0,
					startRow, 100000, 0);
			long end = System.currentTimeMillis();
			if (list.size() < 100){
				System.out.println(list);
			}
			System.out.println("EXCEL READ TIME BY HANDLER : " + (end - start));

			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType", model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO", model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP", model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO", model.get("SS_USER_NO"));
			mapBody.put("vrCustCd", model.get("vrCustCd"));

			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service030.getResult(model);
			// run
			m = service.saveExcelOrderJavaTEST(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			String msg = (String)m.get("msgService"); 
			msg += "\nEXCEL READ TIME : " + (end - start)/1000 + "초";
			mav.addObject("msg", msg);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	@RequestMapping("/WMSYS250E2/excelDownTest.action")
	public void excelDownTest(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		ModelAndView mav = new ModelAndView("jsonView");
		try {
			long start = System.currentTimeMillis();
			map = service.listExcelTest(model);
			long end = System.currentTimeMillis();
			System.out.println("list time : " + (end - start)/1000);
			
			
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
		
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
	       try{
	           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
	           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
	           String[][] headerEx = {
	                                   {MessageResolver.getText("템플릿주문ID")   , "0", "0", "0", "0", "100"}
	                                   ,{MessageResolver.getText("물류센터ID")   , "1", "1", "0", "0", "100"}
	                                   ,{MessageResolver.getText("입하요청일")   , "2", "2", "0", "0", "100"}
	                                   ,{MessageResolver.getText("출하요청일")   , "3", "3", "0", "0", "100"}
	                                   ,{MessageResolver.getText("주문종류")   , "4", "4", "0", "0", "100"}
	                                   ,{MessageResolver.getText("거래처주문ID")   , "5", "5", "0", "0", "100"}
	                                   ,{MessageResolver.getText("거래처주문SEQ")   , "6", "6", "0", "0", "100"}
	                                   ,{MessageResolver.getText("원주문ID")   , "7", "7", "0", "0", "100"}
	                                   ,{MessageResolver.getText("원주문SEQ")   , "8", "8", "0", "0", "100"}
	                                   ,{MessageResolver.getText("상품코드")   , "9", "9", "0", "0", "100"}
	                                   ,{MessageResolver.getText("대표거래처코드")   , "10", "10", "0", "0", "100"}
	                                   ,{MessageResolver.getText("화주코드")   , "11", "11", "0", "0", "100"}
	                                   ,{MessageResolver.getText("배송처코드")   , "12", "12", "0", "0", "100"}
	                                   ,{MessageResolver.getText("배송처주소")   , "13", "13", "0", "0", "100"}
	                                   ,{MessageResolver.getText("배송요청일")   , "14", "14", "0", "0", "100"}
	                                   ,{MessageResolver.getText("거래처ID")   , "15", "15", "0", "0", "100"}
	                                   ,{MessageResolver.getText("주문수량")   , "16", "16", "0", "0", "100"}
	                                   ,{MessageResolver.getText("UOMID")   , "17", "17", "0", "0", "100"}
	                                   ,{MessageResolver.getText("단가")   , "18", "18", "0", "0", "100"}
	                                   ,{MessageResolver.getText("문서번호")   , "19", "19", "0", "0", "100"}
	                                   ,{MessageResolver.getText("원주문발송시스템명")   , "20", "20", "0", "0", "100"}
	                                   ,{MessageResolver.getText("사업부코드")   , "21", "21", "0", "0", "100"}
	                                   ,{MessageResolver.getText("담당영업사원번호")   , "22", "22", "0", "0", "100"}
	                                   ,{MessageResolver.getText("작업상태")   , "23", "23", "0", "0", "100"}
	                                   ,{MessageResolver.getText("차량코드")   , "24", "24", "0", "0", "100"}
	                                   ,{MessageResolver.getText("담당기사명")   , "25", "25", "0", "0", "100"}
	                                   ,{MessageResolver.getText("배송순위")   , "26", "26", "0", "0", "100"}
	                                   ,{MessageResolver.getText("기사휴대폰번호")   , "27", "27", "0", "0", "100"}
	                                   ,{MessageResolver.getText("고객회사LOT번호")   , "28", "28", "0", "0", "100"}
	                                   ,{MessageResolver.getText("BL번호")   , "29", "29", "0", "0", "100"}
	                                   ,{MessageResolver.getText("비고")   , "30", "30", "0", "0", "100"}
	                                   ,{MessageResolver.getText("수신일자")   , "31", "31", "0", "0", "100"}
	                                   ,{MessageResolver.getText("삭제여부")   , "32", "32", "0", "0", "100"}
	                                   ,{MessageResolver.getText("등록일")   , "33", "33", "0", "0", "100"}
	                                   ,{MessageResolver.getText("등록자번호")   , "34", "34", "0", "0", "100"}
	                                   ,{MessageResolver.getText("수정일")   , "35", "35", "0", "0", "100"}
	                                   ,{MessageResolver.getText("수정자번호")   , "36", "36", "0", "0", "100"}
	                                   ,{MessageResolver.getText("작업자IP")   , "37", "37", "0", "0", "100"}
	                                   ,{MessageResolver.getText("거래처우편번호")   , "38", "38", "0", "0", "100"}
	                                   ,{MessageResolver.getText("비고2")   , "39", "39", "0", "0", "100"}
	                                   ,{MessageResolver.getText("입하처주소")   , "40", "40", "0", "0", "100"}
	                                   ,{MessageResolver.getText("입하처코드")   , "41", "41", "0", "0", "100"}
	                                   ,{MessageResolver.getText("입하처명")   , "42", "42", "0", "0", "100"}
	                                   ,{MessageResolver.getText("입하처전화번호")   , "43", "43", "0", "0", "100"}
	                                   ,{MessageResolver.getText("입하처담당자")   , "44", "44", "0", "0", "100"}
	                                   ,{MessageResolver.getText("입하주문중량")   , "45", "45", "0", "0", "100"}
	                                   ,{MessageResolver.getText("출하주문중량")   , "46", "46", "0", "0", "100"}

	                                 };
	           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
	           String[][] valueName = {
		        		   {"TPL_ORD_ID"  , "S"},
		        		   {"LC_ID"  , "S"},
		        		   {"IN_REQ_DT"  , "S"},
		        		   {"OUT_REQ_DT"  , "S"},
		        		   {"ORD_TYPE"  , "S"},
		        		   {"CUST_POID"  , "S"},
		        		   {"CUST_POSEQ"  , "S"},
		        		   {"ORG_ORD_ID"  , "S"},
		        		   {"ORG_ORD_SEQ"  , "S"},
		        		   {"ITEM_CODE"  , "S"},
		        		   {"REPRESENT_CUST_CD"  , "S"},
		        		   {"TRUST_CUST_CD"  , "S"},
		        		   {"TRANS_CUST_CD"  , "S"},
		        		   {"TRANS_CUST_ADDR"  , "S"},
		        		   {"TRANS_REQ_DT"  , "S"},
		        		   {"CUST_ID"  , "S"},
		        		   {"ORD_QTY"  , "S"},
		        		   {"UOM_ID"  , "S"},
		        		   {"UNIT_AMT"  , "S"},
		        		   {"DOC_NO"  , "S"},
		        		   {"ORG_SYS_NM"  , "S"},
		        		   {"SDEPT_CD"  , "S"},
		        		   {"SALE_PER_CD"  , "S"},
		        		   {"WORK_STAT"  , "S"},
		        		   {"CAR_CD"  , "S"},
		        		   {"DRV_NM"  , "S"},
		        		   {"DLV_SEQ"  , "S"},
		        		   {"DRV_TEL"  , "S"},
		        		   {"CUST_LOT_NO"  , "S"},
		        		   {"BL_NO"  , "S"},
		        		   {"REMARK"  , "S"},
		        		   {"REC_DT"  , "S"},
		        		   {"DEL_YN"  , "S"},
		        		   {"REG_DT"  , "S"},
		        		   {"REG_NO"  , "S"},
		        		   {"UPD_DT"  , "S"},
		        		   {"UPD_NO"  , "S"},
		        		   {"WORK_IP"  , "S"},
		        		   {"TRANS_ZIP_NO"  , "S"},
		        		   {"ETC2"  , "S"},
		        		   {"IN_CUST_ADDR"  , "S"},
		        		   {"IN_CUST_CD"  , "S"},
		        		   {"IN_CUST_NM"  , "S"},
		        		   {"IN_CUST_TEL"  , "S"},
		        		   {"IN_CUST_EMP_NM"  , "S"},
		        		   {"IN_ORD_WEIGHT"  , "S"},
		        		   {"OUT_ORD_WEIGHT"  , "S"}
                                  }; 
	           
				// 파일명
				String fileName = MessageResolver.getText("출고관리");
				// 시트명
				String sheetName = "Sheet1";
				// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
				String marCk = "N";
				// ComUtil코드
				String etc = "";

				ExcelWriter wr = new ExcelWriter();
				wr.downExcelFileNew(grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("fail download Excel file...", e);
				}
			}
		}
	
	   /*-
     * Method ID    : AES256DECRIPT
     * Method 설명      : 
     * 작성자                 : 기드온
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSYS300/AES256DECRIPT.action")
    public ModelAndView AES256DECRIPT(Map<String, Object> model) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.AES256DECRIPT(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    /*-
  * Method ID    : AES256DECRIPT
  * Method 설명      : 
  * 작성자                 : 기드온
  * @param   model
  * @return  
  */
 @RequestMapping("/WMSYS300/AES256ENCRIPT.action")
 public ModelAndView AES256ENCRIPT(Map<String, Object> model) {
     ModelAndView mav = new ModelAndView("jsonView");
     Map<String, Object> m = new HashMap<String, Object>();
     try {
         m = service.AES256ENCRIPT(model);
     } catch (Exception e) {
         if (log.isErrorEnabled()) {
             log.error("Fail to save :", e);
         }
         m.put("MSG", MessageResolver.getMessage("save.error"));
     }
     mav.addAllObjects(m);
     return mav;
 }
 
 /*-
	 * Method ID    : stockAllInit
	 * Method 설명      : 재고초기화
	 * 작성자                 : summer hyun 23/04/03
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS300/stockAllInit.action")
	public ModelAndView stockAllInit(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.stockAllInit(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/**
     * Method ID    : saveVideo
     * Method 설명      : 웹캠영상 녹화 저장
     * 작성자                 : schan
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSYS300/saveVideo.action")
    public ModelAndView saveVideo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            request.setCharacterEncoding("utf-8");

            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");
            
            String date = (new SimpleDateFormat("yyyyMMdd")).format(System.currentTimeMillis());
            
            String fileName = file.getOriginalFilename();
            String filePaths = ConstantIF.FILE_ATTACH_PATH +"\\ATCH_FILE\\RealPacking\\"+date;
            //String hostUrl = request.getServerName();
            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);
            }
            
            //videoID 파일이름에 추가
            Map<String,Object> modelID = service.getVideoID(model);
            String id = (String)modelID.get("VIDEO_ID");
            fileName = "RealPacking"+id+"_"+fileName;
            
            File destinationFile = new File(filePaths, fileName);
            
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destinationFile));
            
            model.put("FILE_NAME",fileName);
            model.put("VIDEO_ID",id);
            model.put("URL",filePaths);
            service.saveVideo(model);
            
            m.put("ID", id);
            m.put("MSG", "SUCCESS");
            
            destinationFile.deleteOnExit();
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : getVideoData
     * Method 설명      : video 데이터 select
     * 작성자                 : schan
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSYS300/getVideoData.action")
    public void getVideoData(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        
        BufferedOutputStream bos = null;
        FileInputStream fin = null;
        
        try {
            
            Map<String,String[]> param = request.getParameterMap();
            
            if(param == null){
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            
            model.put("LC_ID",(String)request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
            model.put("VIDEO_ID",param.get("VideoId")[0]);
            
            m = service.getVideoData(model);
            
            String Url = (String)m.get("URL");
            String fileName = (String)m.get("FILE_NAME");
            
            File file = new File(Url, fileName);
            
            if (!file.isFile()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            
            fin = new FileInputStream(file);
            int fSize = (int)file.length();

            response.setContentType("application/octet-stream");
            response.setContentLength(fSize);
            response.setHeader("Content-Disposition", "attachment; filename="+fileName+";");
            
            byte[] bytes = new byte[1024];
            bos = new BufferedOutputStream(response.getOutputStream());
            
            while(fin.read(bytes) >= 0){
                bos.write(bytes);
            }
            
            bos.flush();
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
        }  finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (Exception ie) {
                }
            }
            if (fin != null) {
                try {
                    fin.close();
                } catch (Exception ise) {
                }
            }   
        }
        
        
    }
}
