package com.logisall.winus.wmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsys.service.WMSYS100Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSYS100Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSYS100Service")
    private WMSYS100Service service;
    
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
    
    /**
     * Method ID	: WMSYS100
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSYS100.action")
    public ModelAndView WMSYS100(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsys/WMSYS100");
    }
	
    /**
     * Method ID	: listE1
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSYS100/listE1.action")
    public ModelAndView listE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }    

    
    /**
     * Method ID	: listE2
     * Method 설명	: 한진택배 IF로그이력 조회
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSYS100/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }    
    
    
    /**
     * Method ID	: listE3
     * Method 설명	: 로젠 송장접수이력 조회
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSYS100/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }  
    
    
    /**
     * Method ID	: listE4
     * Method 설명	: EAI 오류 테이블 조회
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSYS100/listE4.action")
    public ModelAndView listE4(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE4(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }  
    
    
    /**
     * Method ID	: save
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSYS100/saveE1.action")
    public ModelAndView saveE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.saveE1(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    
    /**
     * Method ID	: deleteE1
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSYS100/deleteE1.action")
    public ModelAndView deleteE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.deleteE1(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
}
