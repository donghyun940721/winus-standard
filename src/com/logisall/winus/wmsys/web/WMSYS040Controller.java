package com.logisall.winus.wmsys.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS030Service;
import com.logisall.winus.wmsys.service.WMSYS040Service;
import com.logisall.winus.wmscm.service.WMSCM300Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSYS040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSYS040Service")
	private WMSYS040Service service;
	
	@Resource(name = "WMSMS030Service")
	private WMSMS030Service wmsmsService;
	
	@Resource(name = "WMSCM300Service")
	private WMSCM300Service WMSCM300Service;
	
	/*-
	 * Method ID   : mn
	 * Method 설명 : 입출고주문 그리드관리 메인
	 * 작성자      : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS040.action")
	public ModelAndView mn(Map<String, Object> model, HttpServletRequest request) {
		return new ModelAndView("winus/wmsys/WMSYS040");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 입출고주문 그리드관리 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS040/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명      : 입출고주문 그리드관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS040/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : delete
	 * Method 설명      : 입출고주문 그리드관리 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS040/delete.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to delete...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : setGridDefault
	 * Method 설명 : 신규 그리드관리 메인
	 * 작성자      : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS240.action")
	public ModelAndView setGridDefault(Map<String, Object> model, HttpServletRequest request) throws Exception {
		return new ModelAndView("winus/wmsys/WMSYS240", wmsmsService.selectLc(model));
	}
	
	/*-
	 * Method ID   : WMSYS240pop
	 * Method 설명 : 신규 그리드관리 엑셀업로드 팝업
	 * 작성자      : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS240pop.action")
	public ModelAndView WMSYS240pop(Map<String, Object> model, HttpServletRequest request) throws Exception {
		return new ModelAndView("winus/wmsys/WMSYS240pop");
	}
	
	/*-
	 * Method ID   : WMSYS240excelDown
	 * Method 설명 : 신규 그리드관리 엑셀다운로드
	 * 작성자      : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS240/excelDown.action")
	public void WMSYS240excelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if((String)model.get("vrLcId")!=null){
				if(((String)model.get("vrLcId")).equals("")){
					map = WMSCM300Service.originList(model);
				}
				else{
					model.put("LC_ID", (String)model.get("vrLcId"));
					map = WMSCM300Service.defaultList(model);
				}
			}
			else{
				map = WMSCM300Service.originList(model);
			}
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			grs.setTotCnt(grs.getList().size());
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("컬럼인덱스")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("항목순번")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("컬럼명")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("컬럼타입")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("컬럼사이즈")	, "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("배경색")		, "5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("글자정렬방향")		, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("읽기전용여부")		, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("표기여부")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("자동줄바꿈여부")	, "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("콤보리스트명")	, "10", "10", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("콤보연관컬럼")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("클릭함수")		, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("고정인덱스")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("구분")		, "14", "14", "0", "0", "200"},
                                   {MessageResolver.getMessage("ID")		, "15", "15", "0", "0", "200"},

                                   {MessageResolver.getMessage("템플릿명")	,	 "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("템플릿타입")	, "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("센터ID")		, "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주ID")		, "19", "19", "0", "0", "200"},
                                   {MessageResolver.getMessage("템플릿아이디")		, "20", "20", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"COLUMN_CODE"           	, "S"},
                                    {"COLUMN_SEQ"           	, "S"},
                                    {"COLUMN_NAME"          	, "S"},
                                    {"COLUMN_TYPE"           	, "S"},
                                    {"COLUMN_SIZE"       		, "S"},
                                    {"COLUMN_COLOR"          	, "S"},
                                    
                                    {"COLUMN_ALIGN"				, "S"},
                                    {"COLUMN_READ_ONLY"        	, "S"},
                                    {"COLUMN_VISIBLE"  			, "S"},
                                    {"COLUMN_AUTO_LINE_CHANGE" 	, "S"},
                                    {"COLUMN_COMBO"           	, "S"},
                                    
                                    {"COLUMN_COMBO_RELATED"   	, "S"},
                                    {"COLUMN_FUNCTION"         	, "S"},
                                    {"FIXED_COLUMN_INDEX"      	, "S"},
                                    {"ST_GUBUN" 		       	, "S"},
                                    {"ID"           			, "S"},
                                    
                                    {"TEMPLATE_NAME"           	, "S"},
                                    {"TEMPLATE_TYPE"           	, "S"},
                                    {"LC_ID"         			, "S"},
                                    {"CUST_ID"            		, "S"},
                                    {"TEMPLATE_ID"            	, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("현재 그리드 양식");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			//wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID   : WMSYS240inExcelUploadTemplate
	 * Method 설명 : 신규 그리드관리 엑셀업로드
	 * 작성자      : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS240/inExcelUploadTemplate.action")
	public ModelAndView WMSYS240inExcelUploadTemplate(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			model.put("WORK_IP", request.getRemoteAddr());

			//int startRow = Integer.parseInt((String) model.get("startRow"));
			int startRow = 1;
			
			List<Map> list = new ArrayList<Map>();
			// 
			String[] colNameArray = {
				"I_COLUMN_CODE"
				, "I_COLUMN_SEQ"
				, "I_COLUMN_NAME"
				, "I_COLUMN_TYPE"
				, "I_COLUMN_SIZE"
				, "I_COLUMN_COLOR"
				
				, "I_COLUMN_ALIGN"
				, "I_COLUMN_READ_ONLY"
				, "I_COLUMN_VISIBLE"
				, "I_COLUMN_AUTO_LINE_CHANGE"
				, "I_COLUMN_COMBO"
				
				, "I_COLUMN_COMBO_RELATED"
				, "I_COLUMN_FUNCTION"
				, "I_FIXED_COLUMN_INDEX"
				, "I_ST_GUBUN"
				, "I_ID"
				
				, "I_TEMPLATE_NAME"
				, "I_TEMPLATE_TYPE"
				, "I_LC_ID"
				, "I_CUST_ID"
				, "I_TEMPLATE_ID"
			};

			
			//list = ExcelReader.excelLimitRowRead(destination, colNameArray, 0, startRow, 10000, 0);
			list = ExcelReader.excelLimitRowReadByHandler(destination, colNameArray, 0, startRow, 10000, 0);
			
			m = WMSCM300Service.WMSYS240inExcelUploadTemplate(model, list);

			destination.deleteOnExit();
			mav = new ModelAndView("jsonView", m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
}
