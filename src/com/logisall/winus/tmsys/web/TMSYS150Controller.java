package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.tmsys.service.TMSYS150Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS150Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS150Service")
	private TMSYS150Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 사용자관리 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS150.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/tmsys/TMSYS150");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 사용자관리 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS150/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : saveT1
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS150T1/saveT1.action")
	public ModelAndView saveT1(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveT1(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS150/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : delete
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS150/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : /TMSYS150T1.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS150T1.action")
	public ModelAndView tmsys150t1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/tmsys/TMSYS150T1");
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS150/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("프로젝트")    		, "0", "0", "0", "0", "100"},        		
				            		{MessageResolver.getText("REQ_ID")    		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("REQ_SEQ")    		, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("작업단위명")    		, "3", "3", "0", "0", "100"},        		
            						{MessageResolver.getText("상세제목")    		, "4", "4", "0", "0", "100"},
				            		
            						{MessageResolver.getText("작업내용")    		, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("예정일FROM")    		, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("예정일TO")    		, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("담당자")    			, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("진척률%")    		, "9", "9", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("요청자")    			, "10", "10", "0", "0", "100"},
				            		{MessageResolver.getText("완료내용")    		, "11", "11", "0", "0", "100"},
				            		{MessageResolver.getText("완료일")    			, "12", "12", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"PROJECT"			, "S"},        		
				            		{"REQ_ID"			, "S"},
									{"REQ_SEQ"			, "S"},
				            		{"TITLE"			, "S"},        		
            						{"TITLE_SUB"		, "S"},
				            		
            						{"CONTENTS"			, "S"},
				            		{"EXP_DATE_FROM"	, "S"},
				            		{"EXP_DATE_TO"		, "S"},
				            		{"EMPLOYEE"			, "S"},
				            		{"PROGRESS"			, "S"},
				            		
				            		{"REQ_PERSON"		, "S"},
				            		{"COMMENTS"			, "S"},
				            		{"END_DATE"			, "S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("개발일정관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	
	/*-
	 * Method ID : wmsys250
	 * Method 설명 : 사용자관리 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS250.action")
	public ModelAndView wmsys250(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsys/WMSYS250");
	}
}
