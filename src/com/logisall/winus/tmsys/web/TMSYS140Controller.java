package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmsys.service.TMSYS140Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS140Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS140Service")
	private TMSYS140Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 로그내역 조회 화면
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS140.action")
	public ModelAndView mn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			
			System.out.println("hkmin");
			
			mav = new ModelAndView("winus/tmsys/TMSYS140");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 로그내역 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS140/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			
			System.out.println("model");
			
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID 	: listE2
	 * Method 설명 	: 로그내역 조회
	 * 작성자			: dhkim
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS140/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {						
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}   
	
	/*-
	 * Method ID 	: list
	 * Method 설명 	: 사용자 접속로그 정보
	 * 작성자 		: dhkim
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS140/listE3.action")
	public ModelAndView listE3(Map<String, Object> model) {
		ModelAndView mav = null;
		try {						
			mav = new ModelAndView("jqGridJsonView", service.listE3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}   
	
	/*-
	 * Method ID 	: GetStatisticsCount
	 * Method 설명 	: 사용자 접속로그 정보 (통계)
	 * 작성자 		: dhkim
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS140/getStatistics.action")
	public ModelAndView getStatistics(Map<String, Object> model) {
		ModelAndView mav = null;
		try {						
			mav = new ModelAndView("jqGridJsonView", service.getStatistics(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}   
	
	/*-
	 * Method ID 	: InsertE2
	 * Method 설명      	: 로그적재
	 * 작성자               	: dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS140/insertE2.action")
	public ModelAndView saveE2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.insertE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

}