package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.tmsys.service.TMSYS210Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS210Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS210Service")
	private TMSYS210Service service;

	/*-
	 * Method ID   : tmsys210
	 * Method 설명  : 개인정보마스킹관리 화면
	 * 작성자       : 
	 * @param  model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS210.action")
	public ModelAndView tmsys210(Map<String, Object> model) {
		return new ModelAndView("winus/tmsys/TMSYS210");
	}	
	
	/*-
	 * Method ID   : list
	 * Method 설명  : 개인정보마스킹 목록 조회
	 * 작성자        : 
	 * @param  model
	 * @return
	 */
	@RequestMapping("/TMSYS210/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID   	: save
	 * Method 설명  	: 개인정보마스킹 목록 저장
	 * 작성자       	 	: 
	 * @param  model
	 * @return
	 */
	@RequestMapping("/TMSYS210/save.action")
	public ModelAndView save(Map<String, Object> model) {				
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
