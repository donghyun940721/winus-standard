package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.tmsys.service.TMSYS040Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS040Service")
	private TMSYS040Service service;

	/*-
	 * Method ID   : tmsys040
	 * Method 설명  : 프로그램목록관리 화면
	 * 작성자       : chsong
	 * @param  model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS040.action")
	public ModelAndView tmsys040(Map<String, Object> model) {
		return new ModelAndView("winus/tmsys/TMSYS040");
	}

	/*-
	 * Method ID   : list
	 * Method 설명  : 프로그램목록 조회
	 * 작성자        : chsong
	 * @param  model
	 * @return
	 */
	@RequestMapping("/TMSYS040/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : save
	 * Method 설명  : 프로그램목록 등록,수정
	 * 작성자       : chsong
	 * @param  model
	 * @return
	 */
	@RequestMapping("/TMSYS040/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID  : delete
	 * Method 설명  : 프로그램목록 삭제
	 * 작성자       : chsong
	 * @param  model
	 * @return
	 */
	@RequestMapping("/TMSYS040/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	// /*-
	// * Method ID : save
	// * Method 설명 : 프로그램목록 등록,수정,삭제
	// * 작성자 : chsong
	// * @param model
	// * @return
	// */
	// @RequestMapping("/TMSYS040/save.action")
	// public ModelAndView save(Map<String, Object> model) throws Exception{
	// ModelAndView mav = new ModelAndView("jsonView");
	// Map<String, Object> m = null;
	// try{
	// m = service.save(model);
	// }catch(Exception e){
	// e.printStackTrace();
	// m = new HashMap<String, Object>();
	// m.put("MSG", MessageResolver.getMessage("save.error"));
	// }
	// mav.addAllObjects(m);
	// return mav;
	// }

	/*-
	 * Method ID   : listExcel
	 * Method 설명  : 프로그램목록 엑셀다운로드
	 * 작성자       : chsong
	 * @param  model
	 * @return
	 */
	@RequestMapping("/TMSYS040/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
			String[][] headerEx = {{MessageResolver.getMessage("화면구분"), "0", "0", "0", "0", "200"}, {MessageResolver.getMessage("화면ID"), "1", "1", "0", "0", "200"}, {MessageResolver.getMessage("화면명"), "2", "2", "0", "0", "300"},
					{MessageResolver.getMessage("화면View명"), "3", "3", "0", "0", "400"}, {"URL", "4", "4", "0", "0", "100"}, {MessageResolver.getMessage("비고"), "5", "5", "0", "0", "100"}};
			// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
			// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {{"TOP_MENU_CD", "S"}, {"PROGRAM_ID", "S"}, {"PROGRAM_NM", "S"}, {"VIEW_NM", "S"}, {"URL", "S"}, {"REMARK", "S"}};

			// 파일명
			String fileName = MessageResolver.getMessage("프로그램목록");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";
			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download excel file...", e);
			}
		}
	}
}
