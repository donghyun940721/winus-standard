package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmsys.service.TMSYS020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS020Service")
	private TMSYS020Service service;

	/*-
	 * Method ID   : tmsys020
	 * Method 설명 : 권한관리 화면
	 * 작성자      : chsong 04.13.02.31
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS020.action")
	public ModelAndView tmsys020(Map<String, Object> model) {
		return new ModelAndView("winus/tmsys/TMSYS020");
	}

	/*-
	 * Method ID   : selectAuthBox
	 * Method 설명 : 권한코드 selectbox
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS020/selectAuthBox.action")
	public ModelAndView selectAuthBox(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		m = service.selectAuthBox(model);
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : listAuth
	 * Method 설명 : 권한목록 조회
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS020/listAuth.action")
	public ModelAndView listAuth(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listAuth(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listTopMenu
	 * Method 설명 : 프로그램목록 조회
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS020/listTopMenu.action")
	public ModelAndView listTopMenu(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listTopMenu(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listMenuBtn
	 * Method 설명 : 권한관리 조회
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS020/listMenuBtn.action")
	public ModelAndView listMenuBtn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listMenuBtn(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : saveAuth
	 * Method 설명 : 권한목록 저장
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS020/saveAuth.action")
	public ModelAndView saveAuth(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveAuth(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save auth :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : saveTopMenu
	 * Method 설명 : 권한목록TOP 저장
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS020/saveTopMenu.action")
	public ModelAndView saveTopMenu(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveTopMenu(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : saveMenuBtn
	 * Method 설명 : 권한목록 MENU 저장
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS020/saveMenuBtn.action")
	public ModelAndView saveMenuBtn(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveMenuBtn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : deleteAuth
	 * Method 설명      : 권한목록 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS020/deleteAuth.action")
	public ModelAndView deleteAuth(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.deleteAuth(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete auth :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : deleteTopMenu
	 * Method 설명      : TopMenu 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS020/deleteTopMenu.action")
	public ModelAndView deleteTopMenu(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.deleteTopMenu(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete menu :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : deleteMenuBtn
	 * Method 설명      : 메뉴권한 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS020/deleteMenuBtn.action")
	public ModelAndView deleteMenuBtn(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.deleteMenuBtn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete menu :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
