package com.logisall.winus.tmsys.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.tmsys.service.TMSYS160Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.winus.frm.common.util.FSUtil;

/**
 * @author MonkeySeok
 *
 */
@Controller
public class TMSYS160Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSYS160Service")
    private TMSYS160Service service;
    
	static final String[] COLUMN_NAME_TMSYS160 = {
		 "ITEM_GRP_NM", "LC_ID", "IN_ZONE_ID", "ITEM_GRP_CD", "UP_ITEM_GRP_ID", "ITEM_GRP_TYPE"
	};	    

	/*-
	 * Method ID : tmsys160 
	 * Method 설명 : 사용자물류센터관리 화면
	 * 작성자 : MonkeySeok
	 * 날   짜 : 2020-04-22
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/TMSYS160.action")
	public ModelAndView tmsys160(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/tmsys/TMSYS160", service.selectData(model));
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 사용자물류센터관리 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS160/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save 
	 * Method 설명 : 사용자물류센터관리 저장 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS160/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 엑셀정보 취득 
	 * 작성자 : MonkeySeok
	 *
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/TMSYS160/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드 
	 * 작성자 : MonkeySeok
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                    {MessageResolver.getText("상품군타입")     , "0", "0", "0", "0", "100"}
                                   ,{MessageResolver.getText("상품군코드")     , "1", "1", "0", "0", "100"}
                                   ,{MessageResolver.getText("상품군명")       , "2", "2", "0", "0", "100"}
                                   ,{MessageResolver.getText("입고존")        , "3", "3", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                     {"ITEM_GRP_TYPE"  , "S"}
                                    ,{"ITEM_GRP_CD"    , "S"}
                                    ,{"ITEM_GRP_NM"    , "S"}
                                    ,{"IN_ZONE_ID"     , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("상품군정보관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID : TMSYS160E2
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/TMSYS160E2.action")
	public ModelAndView tmsys160E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/tmsys/TMSYS160E2");
	}
}
