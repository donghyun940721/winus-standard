package com.logisall.winus.tmsys.web;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.tmsys.service.TMSYS999Service;
import com.m2m.jdfw5x.document.DocumentView;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS999Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS999Service")
	private TMSYS999Service service;

	/*-
	 * Method ID   : tmsys999
	 * Method 설명 : PDF샘플화면
	 * 작성자      : chsong 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS999.action")
	public ModelAndView tmsys999(Map<String, Object> model) {
		return new ModelAndView("winus/tmsys/TMSYS999");
	}

	/*-
	 * Method ID   : tmsys999Err
	 * Method 설명 : PDF샘플화면 Err
	 * 작성자      : chsong 
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS999Err.action")
	public ModelAndView tmsys999Err(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("winus/tmsys/TMSYS999Err");
		return mav;
	}

	/*-
	 * Method ID   : pdf1Create
	 * Method 설명    : 작업지시서(피킹)_sample  생성
	 * 작성자               : chsong 
	 * @param   model
	 * @return
	 */
	@RequestMapping("/TMSYS999/pdf1Create.action")
	public ModelAndView pdf1Create(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;

		// 다국어에 따른 파일이름 변경
		String langName = MessageResolver.getText("작업지시서(피킹)");

		// 현재날짜를 문서에 붙인다
		// 추후 문서 제목구성에따라 수정할것!!
		// 현재는 임시로!!
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String dateName = dayTime.format(new Date(time));

		// String fileName =
		// ConstantIF.serverPdfPath+"sample/"+langName+dateName+"_sample.pdf";
		// String fileName = langName+dateName+"_sample";
		String fileName = langName + "_" + dateName;

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("langName", langName);
		model.put("dateName", dateName);
		try {
			m = service.getPdf(model);
			m.put("fileName", fileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create pdf file :", e);
			}
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : pdf1Open
	 * Method 설명    : 작업지시서(피킹)_sample 오픈
	 * 작성자               : chsong
	 * @param   model
	 * @return
	 */
	@RequestMapping("/TMSYS999/pdf1Open.action")
	public void pdf1Open(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// String fullFileName =
		// ConstantIF.serverPdfPath+"sample/"+request.getParameter("fileName")+".pdf";
		try {
			// File file = new File(fullFileName);
			File file = new File(ConstantIF.SERVER_PDF_PATH, request.getParameter("fileName") + ".pdf");

			DocumentView.getPDFView(request, response, file);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to open pdf :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}

	/*-
	 * Method ID : preView
	 * Method 설명 : PDF생성
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 */
	@RequestMapping("/TMSYS999/preViewCreate.action")
	public ModelAndView preViewCreate(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;

		String langName = MessageResolver.getText("입하작업지시서");

		// 현재날짜를 문서에 붙인다
		// 추후 문서 제목구성에따라 수정할것!!
		// 현재는 임시로!!
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String dateName = dayTime.format(new Date(time));

		// String fileName = ConstantIF.serverPdfPath+"test01/test01_VIEW.pdf";
		// String fileName = langName+dateName+"_sample";
		String fileName = langName + "_" + dateName;

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("langName", langName);
		model.put("dateName", dateName);

		try {
			m = service.getPreViewConf(model);
			m.put("fileName", fileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create preview pdf :", e);
			}
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : preView
	 * Method 설명 : PDF생성
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 */
	@RequestMapping("/TMSYS999/preViewOpen.action")
	public void preViewOpen(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// String fullFileName =
		// ConstantIF.serverPdfPath+"sample/"+request.getParameter("fileName")+".pdf";
		try {
			// service.preViewConf(model);
			// File file = new File(fullFileName);
			File file = new File(ConstantIF.SERVER_PDF_PATH, request.getParameter("fileName") + ".pdf");
			DocumentView.getPDFView(request, response, file);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to preview open :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}

}
