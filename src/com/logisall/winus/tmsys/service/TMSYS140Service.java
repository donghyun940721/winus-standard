package com.logisall.winus.tmsys.service;

import java.util.Map;


public interface TMSYS140Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> getStatistics(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertE2(Map<String, Object> model) throws Exception;
}
