package com.logisall.winus.tmsys.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Service;
import com.logisall.winus.frm.common.util.LoginInfo;
import com.google.gson.Gson;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;


import com.logisall.winus.frm.common.util.SHA256Util;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.frm.common.util.SessionMn;
import com.logisall.winus.frm.common.util.ValidatorUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.m2m.jdfw5x.client.service.JDFWClientInfo;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.SessionUtil;


@Service("TMSYS030Service")
public class TMSYS030ServiceImpl extends AbstractServiceImpl implements TMSYS030Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSYS030Dao")
    private TMSYS030Dao dao;
    
    ValidatorUtil vUtil = new ValidatorUtil();
    
    private final static String[] CHECK_CALLNUMBER_SAVE_TMSYS030 = {"SALES_CUST_NM", "PHONE_1"};
    
//    @Resource(name = "TMSYS020Dao")
//    private TMSYS020Dao dao2;


    /**
     * Method ID : list
     * Method 설명 : 사용자관리 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }         
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    
    /**
     * Method ID : listCenter
     * Method 설명 : 센터별 사용자관리 리스트 조회
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listCenter(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	loger.info(model);
    	try {
    		if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "100");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}         
    		
    		String connLcId = (String)model.get("SS_SVC_NO");
        	String[] authCds = null;
        	model.put("AUTH_CDs", authCds);
    		
    		
    		map.put("LIST", dao.listCenter(model));
    	} catch (Exception e) {
    		loger.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    		
    	}
    	
    	return map;
    }

    /**
     * Method ID : searchUserLc
     * Method 설명 : 사용자의 물류센터를 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> searchUserLc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        try {
            int rows = dao.searchUserLcCnt(model);
//              System.out.println("mmm");
              if (model.get("page") == null) {
                  model.put("pageIndex", "1");
              } else {
                  model.put("pageIndex", model.get("page"));
              }
   
                model.put("pageSize", String.valueOf(rows));
                model.put("rows", String.valueOf(rows));
          
//                System.out.println("11");
//                System.out.println(model);
            map.put("LIST", dao.searchUserLc(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    /**
     * Method ID : detail
     * Method 설명 : 사용자정보조회(로그인 검증)
     * 작성자 : 기드온
     * 수정 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	   int loginRetryCnt = dao.getLoginRetryCnt(model);//STEP1. 로그인 재시도횟수 조회.
        	   
        	   String inputPwd = (String)model.get("PSWD"); // 사용자가 입력한 패스워드
    		   String dbPwd = dao.detailPwd(model); // DB에 있는 패스워드

    		   if (SHA256Util.verify(inputPwd, dbPwd)){
    			   model.put("PSWD", dbPwd);
    			   map.put("DETAIL", dao.detail(model));
    			   map.put("inputPwd", inputPwd);
        		   map.put("isSuccess", true);
        		   map.put("reasonCd", "SUCCESS");
    			   // 로그인 재시도횟수를 초기화한다.
    			   // dao.initLoginRetryCnt(model);
    		   }else{
    			   // 로그인 재시도횟수를 증가한다.
    			   // dao.increaseLoginRetryCnt(model); 

        		   map.put("isSuccess", false);
        		   map.put("reasonCd", "PWD_CHK");
    		   }
    		   
        	   /*
        	   if(loginRetryCnt < 5){
        		   String inputPwd = (String)model.get("PSWD"); // 사용자가 입력한 패스워드
        		   String dbPwd = dao.detailPwd(model); // DB에 있는 패스워드
        		   dbPwd = DESUtil.dataDecrypt(dbPwd); // 암호화 해제
        		   
        		   if(inputPwd.equals(dbPwd)){
        			   model.put("PSWD", DESUtil.dataEncrypt(dbPwd));
        			   map.put("DETAIL", dao.detail(model));
            		   map.put("isSuccess", true);
            		   map.put("reasonCd", "SUCCESS");
        			   // 로그인 재시도횟수를 초기화한다.
        			   dao.initLoginRetryCnt(model);
        		   }else{
        			   // 로그인 재시도횟수를 증가한다.
        			   dao.increaseLoginRetryCnt(model); 

            		   map.put("isSuccess", false);
            		   map.put("reasonCd", "PWD_CHK");
        		   }
        		   
        	   }else if(loginRetryCnt >= 5){
        		   map.put("isSuccess", false);
        		   map.put("reasonCd", "RETRY_CNT_OVER");
        	   }
        	   */
        	   
        	   /*
        	   if(loginRetryCnt < 5){
        		   int pwdDueDate = dao.getPwdDueDate(model);//STEP2. 비밀번호 유효기간 체크.
        		   int lastLoginDate = dao.getLastLoginDate(model);//STEP2-1. 마지막 접속일 체크.
        		   String inputPwd = (String)model.get("PSWD"); // 사용자가 입력한 패스워드
        		   String dbPwd = dao.detailPwd(model); // DB에 있는 패스워드
        		   dbPwd = DESUtil.dataDecrypt(dbPwd); // 암호화 해제
        		   
        		   if(pwdDueDate < 90 && lastLoginDate < 90){//비밀번호 변경 90일 미만. ==> 성공
        			   if(60 < pwdDueDate && pwdDueDate < 90){
        				   int remainDate = 90 - pwdDueDate;
        				   map.put("MSG", MessageResolver.getMessage("비밀번호 유효기간이" + String.valueOf(remainDate) + "일 남았습니다. 비밀번호를 변경해주세요."));
        			   }
            		   if(inputPwd.equals(dbPwd)){
            			   model.put("PSWD", DESUtil.dataEncrypt(dbPwd));
            			   map.put("DETAIL", dao.detail(model));
                		   map.put("isSuccess", true);
                		   map.put("reasonCd", "SUCCESS");
            			   // 로그인 재시도횟수를 초기화한다.
            			   dao.initLoginRetryCnt(model);
            		   }else{
            			   // 로그인 재시도횟수를 증가한다.
            			   dao.increaseLoginRetryCnt(model); 

                		   map.put("isSuccess", false);
                		   map.put("reasonCd", "PWD_CHK");
            		   }
        		   }else if(pwdDueDate >= 90){//비밀번호 변경 90일 이상. ==> 실패. 변경처리.
        			   map.put("isSuccess", false);
            		   map.put("reasonCd", "PWD_DUEDATE_OVER");
        		   }else if(lastLoginDate >= 90){//마지막 접속후 90일 이상. ==> 실패. 변경처리.
        			   map.put("isSuccess", false);
            		   map.put("reasonCd", "INACTIVE_ID");
        		   }
        		   
        	   }else if(loginRetryCnt >= 5){
        		   map.put("isSuccess", false);
        		   map.put("reasonCd", "RETRY_CNT_OVER");
        	   }
        	   */
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : user_detail
     * Method 설명 : 사용자관리 사용자정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getUserDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapPswd = new HashMap<String, Object>();
        try {
            mapPswd = (Map)dao.detailPswd(model); // DB에 있는 패스워드
            map.put("DETAIL2", dao.userDetail(model)); // 유저정보 가져오기
            map.put("PSWD2", (String)mapPswd.get("USER_PASSWORD"));
            model.put("inKey", "COM01");
            model.put("USER_GB", model.get("SS_USER_GB"));
            model.put("USER_NO", model.get("SS_USER_NO"));
            
            map.put("UserGB", dao.selectUserGB(model));
//            Map<String, Object> mapData = (Map<String, Object>)dao2.selectUserAuthLevel(model);
//            model.put("AUTH_LEVEL", mapData.get("AUTH_LEVEL"));
            map.put("UserAuth", dao.selectAuthBox(model));
        }catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
   
    /**
     * Method ID : upsert
     * Method 설명 : 사용자관리 등록/수정
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> upsertUser(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(loger.isInfoEnabled()){
            loger.info(model);
        }
        try {
        	
        	//임시 센터별 분기 처리 
        	String connLcId = (String)model.get("SS_SVC_NO");
        	String authCode = "";
        	String[] lcIdArr = null;
        	
        	if(connLcId.equals("0000004080")||connLcId.equals("0000004220")||connLcId.equals("0000004320")){//MQ로지스틱스 내촌,백사 김포
        		lcIdArr = new String[]{connLcId}; 
        		authCode = "MA-310";
        	}else if(connLcId.equals("0000003541")||connLcId.equals("0000003721")||connLcId.equals("0000003722")||connLcId.equals("0000003720")){
        		lcIdArr = new String[]{"0000003541","0000003721","0000003722","0000003720"}; //대화물류1,2,3층,보세
        		authCode = "MA-248";
        	}else if(connLcId.equals("0000004640")){
        	    lcIdArr = new String[]{connLcId}; 
                authCode = "MA-374";
        	}else{
        	    lcIdArr = new String[]{connLcId}; 
        	    authCode = "MA-284";//화주(조회용) 권한
        	}

        	int checkedRowCnt = Integer.parseInt(model.get("selectIds").toString());
        	if(checkedRowCnt > 0){
    			Map<String, Object> inputMap = new HashMap<String, Object>();
    			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    				String userNo = (String)model.get("USER_NO"+i);
    				if(userNo.equals("null")){
    					userNo = (String)dao.getNextUserNo();
    				}
        			// 1.사용자정보를 등록한다. ===> 초기비밀번호는 아이디 동일하게 고정. 여러개정 일괄등록시 작업효율을 위해.
    				String clientCd = String.valueOf(model.get("CLIENT_CD"+i));
    				String custId = String.valueOf(model.get("CUST_ID"+i));
    				inputMap.put("USER_NO", 		userNo);
    				inputMap.put("USER_ID", 		(String)model.get("USER_ID"+i));
    				inputMap.put("USER_PASSWORD", 	SHA256Util.dataEncrypt((String)model.get("USER_ID"+i)));
    				inputMap.put("USER_NM", 		(String)model.get("USER_NM"+i));
    				inputMap.put("CLIENT_CD", 		custId);
    				inputMap.put("MOBILE_NO", 		(String)model.get("MOBILE_NO"+i));
    				inputMap.put("EMAIL_ADDR", 		(String)model.get("EMAIL_ADDR"+i));
    				inputMap.put("USER_GB",      (String)model.get("USER_GB"+i));
    				inputMap.put("SS_USER_NO", 		(String)model.get("SS_USER_NO"));
    				inputMap.put("SS_CLIENT_IP", 	(String)model.get("SS_CLIENT_IP"));
    				inputMap.put("AUTH_CD",     authCode);
    				
    				if(connLcId.equals("0000003541") || connLcId.equals("0000003721") || connLcId.equals("0000003722") || connLcId.equals("0000003720")){
    					if(clientCd.startsWith("TC") || clientCd.startsWith("HP")){
        					inputMap.put("AUTH_CD", 	"MA-248");
        				}else{
        					inputMap.put("AUTH_CD", 	"MA-247");
        				}
    				}
    					
        			dao.upsert(inputMap);//int
        			
        			//2. 사용자별 센터 생성.
        			int idx = 1;
        			String svcNo = (String)model.get("SS_SVC_NO");
        			for(String lcId : lcIdArr){
        				inputMap.put("D_LC_ID"			, lcId);
        				if(!lcId.equals("0000003541") && clientCd.startsWith("TC")){
        					continue;
        				}
        				if(lcId.equals(svcNo)){
        				    inputMap.put("D_CONNECT_YN"        , "Y");
                        }else {
                            inputMap.put("D_CONNECT_YN"       , "N");
        				}
        				inputMap.put("D_USER_LC_SEQ"	, idx);
        				dao.insertDC(inputMap);
        				//3. 사용자별 화주권한 생성
        				inputMap.put("SS_SVC_NO"			, lcId);
        				dao.insertUserCustAuth(inputMap);
        				idx++;
        			}
        			
    				inputMap.clear();
    			}
    			
    			map.put("MSG", MessageResolver.getMessage("insert.success"));
    			map.put("RESULT", model.get("USER_NO"));
    			map.put("USER_ID", model.get("USER_ID"));
    		}

           	 
        } catch (Exception e) {
            throw e;
        }
        return map;
    }

    /**
     * Method ID : initLoginCount
     * Method 설명 : 사용자관리 로그인 횟수 초기화
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> initLoginCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(loger.isInfoEnabled()){
            loger.info(model);
        }
        try {
        		int checkedRowCnt = Integer.parseInt(model.get("selectIds").toString());
        		//String[] lcIdArr = new String[]{"0000003541","0000003721","0000003722","0000003720"}; //1,2,3층,보세
        		if(checkedRowCnt > 0){
        			Map<String, Object> inputMap = new HashMap<String, Object>();
        			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
        				String userNo = (String)model.get("USER_NO"+i);
	        			// 1.사용자정보를 등록한다. ===> 초기비밀번호는 아이디 동일하게 고정. 여러개정 일괄등록시 작업효율을 위해.
        				inputMap.put("USER_NO", 		userNo);
        				inputMap.put("USER_ID", 		(String)model.get("USER_ID"+i));
        				inputMap.put("SS_USER_NO", 		(String)model.get("SS_USER_NO"));
        				inputMap.put("SS_CLIENT_IP", 	(String)model.get("SS_CLIENT_IP"));
        				
        				
	        			dao.initLoginCount(inputMap);//int
	        			
        				inputMap.clear();
        			}
        			
        			map.put("MSG", MessageResolver.getMessage("save.success"));
        			map.put("RESULT", model.get("USER_NO"));
        			map.put("USER_ID", model.get("USER_ID"));
        		}

           	 
        } catch (Exception e) {
            throw e;
        }
        return map;
    }
    
    /**
     * Method ID : upsertUserDc
     * Method 설명 : 센터별 계정 > 센터 등록.
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> upsertUserDc(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	if(loger.isInfoEnabled()){
    		loger.info(model);
    	}
    	try {
    		int checkedRowCnt = Integer.parseInt(model.get("selectIds").toString());
    		
    		if(checkedRowCnt > 0){
    			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    				// 1.사용자 물류센터를 등록한다.
    				Map<String, Object> modelDt = new HashMap<String, Object>();
    				modelDt.put("USER_NO"			, model.get("USER_NO"));
    				modelDt.put("D_LC_ID"			, model.get("D_LC_ID" +i));
    				modelDt.put("D_CONNECT_YN"	, model.get("D_CONNECT_YN" +i));
    				modelDt.put("D_USER_LC_SEQ"	, model.get("D_USER_LC_SEQ" +i));
    				modelDt.put("SS_CLIENT_IP"	, model.get("SS_CLIENT_IP"));
    				modelDt.put("SS_USER_NO"		, model.get("SS_USER_NO"));
    				modelDt.put("D_ITEM_GRP_ID"	, model.get("D_ITEM_GRP_ID" +i));
//        				dao.insertDC(modelDt);
    			}
    			
    			map.put("MSG", MessageResolver.getMessage("insert.success"));
    			map.put("USER_NO", model.get("USER_NO"));
    			map.put("USER_ID", model.get("USER_ID"));
    		}
    		
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	return map;
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 사용자관리 등록
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> insert(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	// Map<String, Object> mapPswd = new HashMap<String, Object>();
    	
    	if(loger.isInfoEnabled()){
    		loger.info(model);
    	}
    	try {
    		String isPassedRule = vUtil.pwValidator((String)model.get("PSWD"));
    		
    		if(isPassedRule == "success"){
    			model.put("USER_PASSWORD", SHA256Util.dataEncrypt((String)model.get("PSWD")));
    			String isuser = dao.userIdCheck(model);
    			if ("0".equals(isuser.substring(0, 1))) {
    				// 1.사용자정보를 등록한다.
    				dao.insert(model);
    				
    				// 2.사용자 물류센터를 등록한다.
    				for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    					Map<String, Object> modelDt = new HashMap<String, Object>();
    					modelDt.put("USER_NO"			, model.get("USER_NO"));
    					modelDt.put("D_LC_ID"			, model.get("D_LC_ID" +i));
    					modelDt.put("D_CONNECT_YN"	, model.get("D_CONNECT_YN" +i));
    					modelDt.put("D_USER_LC_SEQ"	, model.get("D_USER_LC_SEQ" +i));
    					modelDt.put("SS_CLIENT_IP"	, model.get("SS_CLIENT_IP"));
    					modelDt.put("SS_USER_NO"		, model.get("SS_USER_NO"));
    					modelDt.put("D_ITEM_GRP_ID"	, model.get("D_ITEM_GRP_ID" +i));
    					dao.insertDC(modelDt);
    				}
    				
    				map.put("MSG", MessageResolver.getMessage("insert.success"));
    				map.put("USER_NO", model.get("USER_NO"));
    				map.put("USER_ID", model.get("USER_ID"));
    				
    			} else {
    				map.put("MSG", MessageResolver.getMessage("userid.check"));
    			}
    		} else {
				map.put("MSG", MessageResolver.getMessage(isPassedRule));
			}
    	} catch (Exception e) {
    		throw e;
    	}
    	return map;
    }

    /**
     * Method ID : udpateUserInfo
     * Method 설명 : 센터별계정관리 > 사용자 수정
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateUserInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(loger.isInfoEnabled()){
            loger.info(model);
        }
        
        try {
        	Map<String, Object> inputMap = new HashMap<String, Object>();
        	inputMap.put("USER_NO", (String)model.get("user_no"));
        	inputMap.put("USER_PASSWORD", SHA256Util.dataEncrypt((String)model.get("password")));
        	inputMap.put("USER_NM", (String)model.get("user_nm"));
        	inputMap.put("MOBILE_NO", (String)model.get("mobile_no"));
        	inputMap.put("EMAIL_ADDR", (String)model.get("email_addr"));
        	inputMap.put("USER_GB", (String)model.get("user_gb"));
        	inputMap.put("SS_USER_NO", (String)model.get("upd_user_no"));
        	inputMap.put("SS_CLIENT_IP", (String)model.get("client_ip"));
        	
        	
        	// 1.사용자정보를 수정한다.
        	dao.updateUserInfo(inputMap);
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (Exception e) {
            throw e;
        }

        return map;
    }
    
    /**
     * Method ID : udpate
     * Method 설명 : 사용자관리 수정
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> udpate(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	if(loger.isInfoEnabled()){
    		loger.info(model);
    	}
    	
    	try {
            String password = (String) model.get("PSWD");
            String isPassedRule = "";
            
            // 비밀번호가 변경된 경우에만 처리
            if (!StringUtils.isEmpty(password)) {
                isPassedRule = vUtil.pwValidator(password); // 비밀번호 유효성 검사
                
                if ("success".equals(isPassedRule)) {
                    model.put("USER_PASSWORD", SHA256Util.dataEncrypt(password)); 
                    dao.updatePswd(model);
                } else {
                    map.put("MSG", MessageResolver.getMessage(isPassedRule));
                    return map; 
                }
            }
            
    		// 1.사용자정보를 수정한다.
    		dao.update(model);
    		
    		// 2.사용자 물류센터를 삭제한다.
    		dao.deleteDC(model);
    		
    		// 3.사용자 물류센터를 추가한다.
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			
    			modelDt.put("USER_NO"		, model.get("USER_NO"));
    			modelDt.put("D_LC_ID"		, model.get("D_LC_ID" +i));
    			modelDt.put("D_CONNECT_YN"	, model.get("D_CONNECT_YN" +i));
    			modelDt.put("D_USER_LC_SEQ"	, model.get("D_USER_LC_SEQ" +i));
    			modelDt.put("SS_CLIENT_IP"	, model.get("SS_CLIENT_IP"));
    			modelDt.put("SS_USER_NO"	, model.get("SS_USER_NO"));
    			modelDt.put("D_ITEM_GRP_ID"	, model.get("D_ITEM_GRP_ID" +i));
    			
    			dao.insertDC(modelDt);
    			
    		}
  
    		map.put("USER_NO", model.get("USER_NO"));
    		map.put("USER_ID", model.get("USER_ID"));
    		
    		map.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	
    	return map;
    }
    /**
     * Method ID : initLoginRetryCnt
     * Method 설명 : 로그인 재시도 횟수 초기화.
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> initLoginRetryCnt(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	if(loger.isInfoEnabled()){
    		loger.info(model);
    	}
    	
    	try {
    		model.put("USER_PASSWORD", SHA256Util.dataEncrypt((String)model.get("PSWD")));
    		// 1.로그인 재시도횟수를 초기화한다.
    		dao.initLoginRetryCnt(model);
    		
    		
    		map.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch (Exception e) {
    		throw e;
    	}
    	
    	return map;
    }
    /**
     * Method ID : increaseLoginRetryCnt
     * Method 설명 : 로그인 재시도 횟수 증가.
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> increaseLoginRetryCnt(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	if(loger.isInfoEnabled()){
    		loger.info(model);
    	}
    	
    	try {
    		model.put("USER_PASSWORD", SHA256Util.dataEncrypt((String)model.get("PSWD")));
    		// 1.로그인 재시도횟수를 증가한다.
    		dao.increaseLoginRetryCnt(model);
    		
    		
    		map.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch (Exception e) {
    		throw e;
    	}
    	
    	return map;
    }

    /**
     * Method ID : deleteUserInfo
     * Method 설명 : 센터별계정관리 > 사용자 삭제 
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> deleteUserInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        try {
            int checkedRowCnt = Integer.parseInt(model.get("selectIds").toString());
    		
    		if(checkedRowCnt > 0){
    			Map<String, Object> inputMap = new HashMap<String, Object>();
    			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
        			// 1.USER_NO로 DEL_YN = 'Y' 로 업데이트
    				inputMap.put("USER_NO", 		(String)model.get("USER_NO"+i));
    				inputMap.put("SS_USER_NO", 		(String)model.get("SS_USER_NO"));
    				inputMap.put("SS_CLIENT_IP", 	(String)model.get("SS_CLIENT_IP"));
    	            // 2.사용자정보를 삭제한다.          
    	            dao.deleteUserInfo(inputMap);
    				
    				inputMap.clear();
    			}
    			
    			map.put("MSG", MessageResolver.getMessage("delete.success"));
    			map.put("RESULT", model.get("USER_NO"));
    			map.put("USER_ID", model.get("USER_ID"));
    		}
            
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return map;
    }
 
    
    /**
     * Method ID : delete
     * Method 설명 : 사용자관리 삭제 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	loger.info(model);
    	try {
    		// 1.사용자아이디에 대한 사용자별윈도우권한(USER_WINDOW_ROLE)을 삭제한다.
    		//   dao.deleteUserWindowRole(model);
    		
    		// 2.사용자정보를 삭제한다.          
    		dao.delete(model);
    		map.put("MSG", MessageResolver.getMessage("delete.success"));
    	} catch (Exception e) {
    		loger.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("delete.error"));
    	}
    	return map;
    }
    
    /**
     * Method ID : userIdCheck
     * Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> checkUserId(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        String isuser = dao.userIdCheck(model);
        map.put("ISUSER", isuser.substring(0,1));
        map.put("USERID", isuser.substring(1));
        return map;
    }
    
    /**
     * Method ID : loginUpdate
     * Method 설명 : 로그인시 사용자 접속여부 업데이트 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLogin(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.loginUpdate(model); // 접속 Y 업데이트
        

        return map;
    }
    
    /**
     * Method ID : loginHistory
     * Method 설명 : 접속 히스토리 생성
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLoginHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        String conYn = dao.selectConnectYN(model);// 접속여부 검색
        model.put("CON_YN", conYn);
        dao.loginInsert(model); // 접속 히스토리 생성

        return map;
    }
    
    /**
     * Method ID : logoutUpdate
     * Method 설명 : 로그인아웃시 사용자 로그정보 업데이트 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLogout(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.logoutUpdate(model); // 접속 N 업데이트  

        return map;
    }
    
    /**
     * Method ID : reset
     * Method 설명 : 비밀번호 초기화
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateReset(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapPswd = new HashMap<String, Object>();
        //String pswd ="1111";
        
        String pswd ="winusdef1!";
        
        model.put("USER_PASSWORD", SHA256Util.dataEncrypt(pswd));
        dao.reset(model);
        
        mapPswd = (Map)dao.detailPswd(model); // DB에 있는 패스워드
        map.put("PSWD2", (String)mapPswd.get("USER_PASSWORD"));
        map.put("MSG", MessageResolver.getMessage("비밀번호가초기화되었습니다"));
        return map;
    }
    
    /**
     * Method ID : selectLc
     * Method 설명 : 멀티 물류센터 처리 
     * 작성자 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectLc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LCINFO", dao.selectLc(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectProgramInfo
     * Method 설명 : 프로그램ID추출 처리 
     * 작성자 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectProgramInfo(Map<String, Object> model) throws Exception{
        Map<String, Object> programInfo = dao.selectProgramInfo(model);
        return programInfo;
    }
    

    /**
     * Method ID : selectUserGB
     * Method 설명 : 셀렉트박스 유저구분
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectUserGB(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            model.put("inKey", "COM01");
            model.put("USER_GB", model.get("SS_USER_GB"));
            map.put("UserGB", dao.selectUserGB(model));
//            Map<String, Object> mapData = (Map<String, Object>)dao2.selectUserAuthLevel(model);
//            model.put("AUTH_LEVEL", mapData.get("AUTH_LEVEL"));
            map.put("UserAuth", dao.selectAuthBox(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectScreen
     * Method 설명 : 사용자 초기 화면 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public String selectScreen(Map<String, Object> model) throws Exception {
        String screenId = dao.selectScreen(model);
        return screenId;
    }
    
    /**
     * Method ID : detailRoll
     * Method 설명 : 권한 조회
     * 작성자 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getRoll(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("C_MENU_ID", model.get("PROGRAM_ID"));
        map.put("P_MENU_ID", model.get("P_MENU_ID"));
        
        try {
            map.put("windowRoll", dao.detailRoll(model));

        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : getLoginRetryCnt
     * Method 설명 : 로그인 재시도 횟수 체크
     * 작성자 : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    public int getLoginRetryCnt(Map<String, Object> model) throws Exception {
    	int cnt = 0;
    	try {
    		cnt = dao.getLoginRetryCnt(model);
    		
    	} catch (Exception e) {
    		loger.error(e.toString());
    	}
    	return cnt;
    }
    
    /**
     * Method ID  : saveLang
     * Method 설명  : 언어변경시 유저정보 언어 체인지
     * 작성자             : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveLang(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.langUpdate(model); // 언어 업데이트  

        return map;
    }
    
    /**
     * Method ID : getPopupInfo
     * Method 설명 : 팝업정보 조회
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getPopupInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("POPLIST", dao.selectPopInfo(model));

        } catch (Exception e) {
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }    
    
    public Map<String, Object> localTime(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("TIME")) {
			map.put("TIME", dao.localTime(model));
		}else if (srchKey.equals("SSID")) {
			map.put("SSID", dao.getRefreshSessionId(model));
		}
		return map;
	}
    
    public Map<String, Object> jsonPostCross_uPcRecodeList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try{
        	String paramId   = (String) model.get("id");
            String paramPass = (String) model.get("pass");
        	String body      = "id="+paramId+"&pass="+paramPass;
        	URL url          = new URL("https://centrex.uplus.co.kr/RestApi/recordlist");

        	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        	connection.setRequestMethod("POST");
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
        	connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        	OutputStream os = connection.getOutputStream();
        	os.write( body.getBytes("euc-kr") );
        	os.flush();
        	os.close();

        	BufferedReader reader = new BufferedReader( new InputStreamReader( connection.getInputStream()));
        	StringBuffer buffer = new StringBuffer(); 

            int read = 0; 
            char[] cbuff = new char[1024]; 
            
            while ((read = reader.read(cbuff)) > 0){
            	buffer.append(cbuff, 0, read); 
            }
            
        	reader.close();
        	map.put("DS", buffer.toString());
        } catch(Exception e){
        	map.put("DS", "ERR");
        }
    	return map;
	}
    
    public Map<String, Object> jsonPostCross_uPcCallhistory(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try{
        	String paramId       = (String) model.get("id");
            String paramPass     = (String) model.get("pass");
            String paramPage     = (String) model.get("page");
            String paramCallType = (String) model.get("calltype");
        	String body      = "id="+paramId+"&pass="+paramPass+"&page="+paramPage+"&calltype="+paramCallType;
        	URL url          = new URL("https://centrex.uplus.co.kr/RestApi/callhistory");

        	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        	connection.setRequestMethod("POST");
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
        	connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        	OutputStream os = connection.getOutputStream();
        	os.write( body.getBytes("euc-kr"));
        	os.flush();
        	os.close();

        	BufferedReader reader = new BufferedReader( new InputStreamReader( connection.getInputStream()));
        	StringBuffer buffer = new StringBuffer(); 

            int read = 0; 
            char[] cbuff = new char[1024]; 
            
            while ((read = reader.read(cbuff)) > 0){
            	buffer.append(cbuff, 0, read); 
            }
            
        	reader.close();
        	map.put("DS", buffer.toString());
        	//map.put("DS", reader.readLine().toString());
        } catch(Exception e){
        	map.put("DS", "ERR");
        }
    	return map;
	}
    
    /**
     * Method ID    : callUserInofo
     * Method 설명      : 청구단가계약 조회
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     @Override
     public Map<String, Object> callUserInofo(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         if(model.get("page") == null) {
             model.put("pageIndex", "1");
         } else {
             model.put("pageIndex", model.get("page"));
         }
         if(model.get("rows") == null) {
             model.put("pageSize", "20");
         } else {
             model.put("pageSize", model.get("rows"));
         }
         map.put("LIST", dao.callUserInofo(model));
         return map;
     }
     
     /**
      * 
      * 대체 Method ID   : callNumSave
      * 대체 Method 설명    : 고객 저장
      * 작성자                      : chsong
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> callNumSave(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int errCnt = 0;
         try{
             Map<String, Object> modelDt = new HashMap<String, Object>();
             modelDt.put("selectIds" 		, model.get("selectIds"));
             modelDt.put("SALES_CUST_NM"    , (String) model.get("SALES_CUST_NM"+0));
             modelDt.put("PHONE_1"     		, (String) model.get("PHONE_1"+0));
             modelDt.put("ZIP"     			, (String) model.get("ZIP"+0));
             modelDt.put("ADDR"     		, (String) model.get("ADDR"+0));
             modelDt.put("ETC1"    			, (String) model.get("ETC1"+0));
             modelDt.put("LC_ID"    		, model.get(ConstantIF.SS_SVC_NO));
             modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
             ServiceUtil.checkInputValidation(modelDt, CHECK_CALLNUMBER_SAVE_TMSYS030);
             dao.callNumSave(modelDt);
             m.put("errCnt", errCnt);
             m.put("MSG", MessageResolver.getMessage("save.success"));
             
         } catch (BizException be) {
             if (log.isInfoEnabled()) {
                 log.info(be.getMessage());
             }
             m.put("MSG", be.getMessage());
             
         } catch(Exception e){
             throw e;
         }
         return m;
     }
     
     public Map<String, Object> getAlertMsg(Map<String, Object> model) throws Exception {
 		Map<String, Object> map = new HashMap<String, Object>();
 		Map<String, Object> modelDt = new HashMap<String, Object>();
 		String srchKey = (String) model.get("srchKey");
 		if (srchKey.equals("GETAMSG")) {
 			map.put("GETAMSG", dao.getAlertMsg(model));
 			
 			modelDt.put("KEY" 		, model.get("KEY"));
 			modelDt.put("USER" 		, model.get("USER"));
 			dao.testSaveMsg(modelDt);
 		}
 		return map;
 	}
     
     /**
      * 
      * 대체 Method ID		: OnTimeoutReached
      * 대체 Method 설명	: winus 세션유지 call url
      * 작성자                     	: 
      * @param model
      * @return
      * @throws Exception
      */
	public Map<String, Object> OnTimeoutReached(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("LC_ID"	, model.get(ConstantIF.SS_SVC_NO));
		model.put("USER_NO"	, model.get(ConstantIF.SS_USER_NO));
		map.put("TIMEOUT", dao.OnTimeoutReached(model));
		return map;
	}
	
	/**
     * 
     * 대체 Method ID	: crossDomainHttpWs
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String sUrl		= "https://52.79.206.98:5101/" + cUrl;
        String jsonString		= (String)model.get("jsonString");
        String escapedJson		= StringEscapeUtils.unescapeHtml(jsonString);
        JSONTokener tokener		= new JSONTokener(escapedJson);
        JSONObject jsonObject 	= new JSONObject(tokener);
        
        try{
        	//ssl disable
        	disableSslVerification();
	        //System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "winus01" + ":" + "winus01";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = jsonObject.toString();
			//System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	public Map<String, Object> insertediya(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        // Map<String, Object> mapPswd = new HashMap<String, Object>();
        
        if(loger.isInfoEnabled()){
            loger.info(model);
        }
        try {
            model.put("USER_PASSWORD", SHA256Util.dataEncrypt((String)model.get("PSWD")));
            String isuser = dao.userIdCheck(model);
            if ("0".equals(isuser.substring(0, 1))) {
                // 1.사용자정보를 등록한다.
                
            	map.putAll(model);
                map.put("MSG", MessageResolver.getMessage("insert.success"));
                map.put("USER_NO", model.get("USER_NO"));
                map.put("USER_ID", model.get("USER_ID"));
                map.put("SS_USER_NO", "0000000000");
                map.put("ITEM_FIX_YN", "N");
                
                dao.insert(map);
                resultMap.put("ERROR", 0);
            } else {
            	resultMap.put("ERROR", 1);
            	resultMap.put("MSG", MessageResolver.getMessage("userid.check"));
            }
        } catch (Exception e) {
            throw e;
        }
        return resultMap;
    }
	
	
	/**
     * Method ID		 : listQ2
     * Method 설명 	 : 사용자 관리 팝업 -조회
     * 작성자 			 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }         
            map.put("LIST", dao.listQ2(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }

        return map;
    }
    
    /**
     * Method ID		 : listQ3
     * Method 설명 	 : 피킹 작업지시 -> 사용자 조회 팝업 - 화면 
     * 작성자 			 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }         
            map.put("LIST", dao.listQ3(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }

        return map;
    }
    
	/**
	 * 대체 Method ID   : list
	 * 대체 Method 설명 : 메뉴명 딕셔너리
	 * 작성자      : kimzero
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> list_menu() throws Exception {
		List<Map<String, Object>> list = new ArrayList<>();
		
		list = dao.list_menu();
		
		return list;
	}
	
	
	//코드 체크 함수
	public boolean check_code(String secret, long code, long t) throws InvalidKeyException, NoSuchAlgorithmException {
		Base32 codec = new Base32();
		byte[] decodedKey = codec.decode(secret);

		int window = 3;
		for (int i = -window; i <= window; ++i) {
			long hash = verify_code(decodedKey, t + i);

			if (hash == code) {
				return true;
			}
		}

		return false;
	}

	//코드 확인 함수
	public int verify_code(byte[] key, long t) throws NoSuchAlgorithmException, InvalidKeyException{
		byte[] data = new byte[8];
		long value = t;
		for (int i = 8; i-- > 0; value >>>= 8) {
			data[i] = (byte) value;
		}

		SecretKeySpec signKey = new SecretKeySpec(key, "HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(signKey);
		byte[] hash = mac.doFinal(data);

		int offset = hash[20 - 1] & 0xF;

		long truncatedHash = 0;
		for (int i = 0; i < 4; ++i) {
			truncatedHash <<= 8;
			truncatedHash |= (hash[offset + i] & 0xFF);
		}

		truncatedHash &= 0x7FFFFFFF;
		truncatedHash %= 1000000;

		return (int) truncatedHash;
	}

    /**
     * Method ID : selectOtpCodeCnt
     * Method 설명 : OTP 인증번호 유무 조회
     * 작성자 : 홍요섭
     * @param model
     * @return
     * @throws Exception
     */
    public int selectOtpCodeCnt(Map<String, Object> model) throws Exception {
    	int cnt = 0;
    	try {
    		cnt = dao.selectOtpCodeCnt(model);
    		
    	} catch (Exception e) {
    		loger.error(e.toString());
    	}
    	return cnt;
    }
    /**
     * Method ID : getOtpCode
     * Method 설명 : OTP 인증번호 조회
     * 작성자 : 홍요섭
     * @param model
     * @return
     * @throws Exception
     */
	public String getOtpCode(Map<String, Object> model) throws Exception{
		
		String otpCode = "";
		
		int UserOtpCodeCnt = dao.selectOtpCodeCnt(model);
		
		if(UserOtpCodeCnt == 0) {
			otpCode = setOtpCode();			
			model.put("OTP_CODE", otpCode);
			dao.insertOtp(model);
						
		}else {
			otpCode = selectOtpCode(model);
		}
		
		return otpCode;
		
	}
    /**
     * Method ID : setOtpCode
     * Method 설명 : OTP 인증번호 생성
     * 작성자 : 홍요섭
     * @param model
     * @return
     * @throws Exception
     */
	public String setOtpCode() throws Exception{
		

	    byte[] buffer = new byte[5 + 5 * 5];
	    new Random().nextBytes(buffer);
	    Base32 codec = new Base32();
	    byte[] secretKey = Arrays.copyOf(buffer, 10);  //16자 이상이여하므로 10으로 설정 필요
	    byte[] bEncodedKey = codec.encode(secretKey);
	      
	    //인증키 생성
	    String encodedKey = new String(bEncodedKey); 

		
		return encodedKey;
		
	}
    /**
     * Method ID : selectOtpCode
     * Method 설명 : 계정 OTP 인증번호 조회
     * 작성자 : 홍요섭
     * @param model
     * @return
     * @throws Exception
     */
	public String selectOtpCode(Map<String, Object> model) throws Exception{
		
		String UserOtpCode = "";
    	try {
    		UserOtpCode = dao.selectOtpCode(model);
    		
    	} catch (Exception e) {
    		loger.error(e.toString());
    	}
		
		return UserOtpCode;
		
	}
	
	
	public int loginSetup(Map<String, Object> model,  HttpServletRequest request) throws Exception{
		
		
		Map<String, Object> m = null;
		Map<String, Object> user = null;
		ArrayList<Map<String, Object>> lcInfo = null;
		int check = 0;		// Login 여부 체크용 변수.
		boolean isSuccess = false;
		String reasonCd = "";
		
		try {
			String serviceId = (String) model.get("svcid");
			String operationId = (String) model.get("opid");

			boolean isLoginRequest = "LOGIN".equals(serviceId) && "loginUserConfirm".equals(operationId);

			// 사전처리
			String mySystemId = "WINUS";
			String gvSystemId = "n/a";
			String gvUserAuth = "n/a";

			// 타 시스템 유저 검사
			// boolean isExternalUser = !mySystemId.equals(gvSystemId) ;

			// 외부 사용자 경우
			// if(isExternalUser &&
			// !BlackList.getInstance().canAccess(mySystemId, gvSystemId,
			// gvUserAuth, serviceId, operationId)){
			// appContext.setResultMessage(-200, "허용되지 않은 외부접속입니다");
			// }

			// 로그인 요청이면?
			if (isLoginRequest) {

				m = getDetail(model);		// <<<<<<<< 실질적인 로그인 Pass / Non Pass 처리 로직.
				
				
				isSuccess = (boolean) m.get("isSuccess");
				reasonCd = (String) m.get("reasonCd");
				if (isSuccess) {//user != null
					
					
					
					user = (Map<String, Object>) m.get("DETAIL");
					
				
					// 세션에 값 셋팅
					HttpSession session = request.getSession();
					// String user_ip = request.getRemoteAddr();
					// 인증에 성공한 경우 처리 해야 되는 부분
															//(String) user.get("CONNECT_YN")
					
//					//220516 로그인 재시도횟수 체크.
//					int loginRetryCnt = service.getLoginRetryCnt(user);
					
					
					boolean chek = SessionListener.checkLogin((String) user.get("LOGIN_YN"), (String) user.get("MULTI_USE_GB"), (String) user.get("LAST_CONT_IP"), JDFWClientInfo.getClntIP(request));
					//5회이상 로그인실패시 관리자 문의 메시지.
					if (chek == true ) {//&& loginRetryCnt < 5
						
					
						
						session.setAttribute("SS_USER_ID", user.get("USER_ID")); // 사용자ID
						session.setAttribute("SS_USER_NO", user.get("USER_NO")); // 사용자NO
						session.setAttribute("SS_USER_GB", user.get("USER_GB")); // 사용자구분
						session.setAttribute("SS_USER_NAME", user.get("USER_NM")); // 사용자이름
						session.setAttribute("SS_USER_TYPE", user.get("AUTH_CD")); // 사용자타입
						session.setAttribute("SS_SVC_NO", user.get("LC_ID")); // 서비스NO
						session.setAttribute("SS_SVC_NAME", user.get("LC_NM")); // 서비스이름
						session.setAttribute("SS_CMPY_CODE", user.get("CUST_CD"));// 사용자소속회사코드
						session.setAttribute("SS_CMPY_ID", user.get("CUST_ID"));// 사용자소속회사코드
						session.setAttribute("SS_CMPY_NAME", user.get("CUST_NM"));// 사용자소속회사명
						session.setAttribute("SS_SALES_CUST_ID", user.get("SALES_CUST_ID"));// 사용자소속회사명
						
						session.setAttribute("SS_LANG", user.get("LANG"));// 사용자언어
						session.setAttribute("SS_AUTH_LEVEL", user.get("AUTH_LEVEL").toString());// 레벨
						session.setAttribute("SS_CLIENT_CD", user.get("CLIENT_CD"));// 거래처(업체)코드
						session.setAttribute("SS_GIRD_NUM", user.get("USER_GRID_NUM"));// 그리드
						session.setAttribute("SS_SVC_USE_TY", user.get("LC_USE_TY")); // 물류센터운영타입
						session.setAttribute("SS_LC_CHANGE_YN", user.get("LC_CHANGE_YN")); // 물류센터변경여부
						session.setAttribute("SS_ITEM_FIX_YN", user.get("ITEM_FIX_YN")); // 특정상품조회권한
						session.setAttribute("SS_ITEM_GRP_ID", user.get("ITEM_GRP_ID")); // 특정상품군
						session.setAttribute("SS_SUB_PASSWORD", user.get("SUB_PASSWORD")); // 기타 비밀번호
						session.setAttribute("SS_PROC_AUTH", user.get("PROC_AUTH")); // 저장처리권한
						session.setAttribute("SS_USE_TRANS_CUST", user.get("USE_TRANS_CUST")); // 저장처리권한
						session.setAttribute("SS_TEL_NO", user.get("TEL_NO")); // 전화번호
						session.setAttribute("SS_MULTI_USE_GB", user.get("MULTI_USE_GB")); // 사용자NO
						session.setAttribute("SS_STOCK_ALERT_YN", user.get("STOCK_ALERT_YN")); // 재고부족알림구분
						session.setAttribute("SS_LC_CUST_NM", user.get("LC_CUST_NM")); // 3PL 물류센터명
						
						session.setAttribute("SS_M_DO", ConstantIF.encAES("KEY2020031312345"));
						session.setAttribute("SS_DETAIL_PS", ConstantIF.encAES("DE2020031354321"));
						
						List<Map<String, Object>> listMenu = list_menu();
				    	Gson gson         = new Gson();
				    	Map<String, Object> map = new HashMap<String, Object>();
				    	
				    	for(int i = 0; i < listMenu.size(); i++){
				    		map.put(String.valueOf(listMenu.get(i).get("PROGRAM_ID")), listMenu.get(i).get("PROGRAM_NM"));
				    	}
						String jsonString = gson.toJson(map);
						jsonString = JSONValue.escape(jsonString);
						session.setAttribute("SS_MENU_MAP", jsonString);
						
																						// 검색
																						// 설정
						session.setAttribute("SS_SESSION_ID", request.getSession().getId());// 세션아이디
						session.setAttribute("SS_CLIENT_IP", JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						if ("KR".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_KOREAN);
						} else if ("EN".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_ENGLISH);
						} else if ("JA".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_JAPANESE);
						} else if ("CH".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_CHINESE);
						} else if ("VN".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_VIETNAM);
						} else if ("ES".equals(user.get("LANG"))) {
                            session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_ESPANA);
                        }

						SessionUtil.setValue(request, ConstantIF.SS_USER_ID, user.get("USER_ID"));// 사용자ID
						SessionUtil.setValue(request, ConstantIF.SS_USER_NO, user.get("USER_NO"));// 사용자번호
						SessionUtil.setValue(request, ConstantIF.SS_USER_GB, user.get("USER_GB"));// 사용자구분
						SessionUtil.setValue(request, ConstantIF.SS_USER_NAME, user.get("USER_NM")); // 사용자이름
						SessionUtil.setValue(request, ConstantIF.SS_USER_TYPE, user.get("AUTH_CD"));// 사용자타입
						SessionUtil.setValue(request, ConstantIF.SS_SVC_NO, user.get("LC_ID"));// 서비스NO
						SessionUtil.setValue(request, ConstantIF.SS_SVC_NAME, user.get("LC_NM"));// 서비스D이름
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_CODE, user.get("CUST_CD"));// 사용자소속회사코드
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_ID, user.get("CUST_ID"));// 사용자소속회사코드
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_NAME, user.get("CUST_NM"));// 사용자소속회사명
						SessionUtil.setValue(request, ConstantIF.SS_LANG, user.get("LANG"));// 사용자언어
						SessionUtil.setValue(request, ConstantIF.SS_AUTH_LEVEL, user.get("AUTH_LEVEL").toString());// 레벨
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_CD, user.get("CLIENT_CD"));// 거래처(업체)코드
						SessionUtil.setValue(request, ConstantIF.SS_GIRD_NUM, user.get("USER_GRID_NUM"));// 거래처(업체)코드
						SessionUtil.setValue(request, ConstantIF.SS_SVC_USE_TY, user.get("LC_USE_TY"));// 서비스NO
						SessionUtil.setValue(request, ConstantIF.SS_LC_CHANGE_YN, user.get("LC_CHANGE_YN"));// 물류센터변경여부
						SessionUtil.setValue(request, ConstantIF.SS_ITEM_FIX_YN, user.get("ITEM_FIX_YN"));// 특정상품조회권한
						SessionUtil.setValue(request, ConstantIF.SS_ITEM_GRP_ID, user.get("ITEM_GRP_ID"));// 특정상품군
						SessionUtil.setValue(request, ConstantIF.SS_TEL_NO, user.get("TEL_NO"));// 전화번호
						SessionUtil.setValue(request, ConstantIF.SS_SUB_PASSWORD, user.get("SUB_PASSWORD"));// 기타 비밀번호
						SessionUtil.setValue(request, ConstantIF.SS_PROC_AUTH, user.get("PROC_AUTH"));// 저장처리권한
						SessionUtil.setValue(request, ConstantIF.SS_USE_TRANS_CUST, user.get("USE_TRANS_CUST"));// 저장처리권한
						SessionUtil.setValue(request, ConstantIF.SS_MULTI_USE_GB, user.get("MULTI_USE_GB"));// 사용자번호
						SessionUtil.setValue(request, ConstantIF.SS_STOCK_ALERT_YN, user.get("STOCK_ALERT_YN"));// 재고부족알림구분
						SessionUtil.setValue(request, ConstantIF.SS_LC_CUST_NM, user.get("LC_CUST_NM"));// 3PL 물류센터명
						SessionUtil.setValue(request, ConstantIF.SS_SALES_CUST_ID, user.get("SALES_CUST_ID"));// 3PL 물류센터명
						
						
						SessionUtil.setValue(request, ConstantIF.SS_M_DO, ConstantIF.encAES("KEY2020031312345"));
						SessionUtil.setValue(request, ConstantIF.SS_DETAIL_PS, ConstantIF.encAES("DE2020031354321"));
						
						// SessionUtil.setValue(request,ConstantIF.SS_DEPT_NAME,
						// user.get("DEPT_NAME"));//사용자소속부서명
						// SessionUtil.setValue(request,ConstantIF.SS_DEGREE,
						// user.get("DEGREE"));//사용자직급명
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_IP, JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						SessionUtil.setValue(request, ConstantIF.SS_SESSION_ID, request.getSession().getId());// (클라이언트)접속IP

						SessionUtil.setValue(request, ConstantIF.SS_LOGIN_DATE, new Date());
						SessionUtil.setValue(request, ConstantIF.SS_LOGIN_SESSION_TIME, 86400);

						if (user.get("LANG").equals("KR")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_KOREAN);// 사용자언어
																													// 타입
																													// 한국어
						} else if (user.get("LANG").equals("EN")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_ENGLISH);// 사용자언어
																														// 타입
																														// 영어
						} else if (user.get("LANG").equals("JA")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_JAPANESE);// 사용자언어
																														// 타입
																														// 일본어
						} else if (user.get("LANG").equals("CH")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_CHINESE);// 사용자언어
																														// 타입
																														// 중국어
						} else if (user.get("LANG").equals("VN")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_VIETNAM);// 사용자언어
																														// 타입
																														// 중국어
						}else if (user.get("LANG").equals("ES")) {
                            SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_ESPANA);
						}

						// CodeMngr.reload((String)user.get("LC_ID"));//시스템로그인
						// 성공시 해당 svcNo별 설정된 공통코드 메모리적재 2012-03-30 by indigo;
						check = 1;

						// 로그인 여부를 셋팅
						SessionMn.init();
						LoginInfo loginInfo = new LoginInfo();
						loginInfo.setId(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
						loginInfo.setLastRequestTime(Calendar.getInstance());
						SessionMn.getInstance().addInfo(loginInfo);

						// 로그인시 첫페이지 권한설정...
						model.put("SS_SVC_NO", user.get("LC_ID"));
						model.put("USER_NO", user.get("USER_NO"));
						model.put("USER_ID", user.get("USER_ID"));
						model.put("SS_SVC_USE_TY", user.get("LC_USE_TY"));
						model.put("SS_LC_CHANGE_YN", user.get("LC_CHANGE_YN"));
						model.put("SS_ITEM_FIX_YN", user.get("ITEM_FIX_YN"));
						model.put("SS_ITEM_GRP_ID", user.get("ITEM_GRP_ID"));
						model.put("SS_TEL_NO", user.get("TEL_NO"));
						model.put("SS_SUB_PASSWORD", user.get("SUB_PASSWORD"));
						model.put("SS_PROC_AUTH", user.get("PROC_AUTH"));
						model.put("SS_USE_TRANS_CUST", user.get("USE_TRANS_CUST"));
						model.put("WORK_IP", SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_IP));
						model.put("Session_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID)));

						model.put("AUTH_CD", user.get("AUTH_CD"));
						model.put("PROGRAM_ID", "TMSYS030");

						m.put("windowRoll", getRoll(model));

						// 멀티 물류센터 처리
						lcInfo = (ArrayList<Map<String, Object>>) selectLc(model).get("LCINFO");
						session.setAttribute("SS_LC_INFO", lcInfo);
//						// 로그인 시도횟수 초기화. 220513 kimzero
//						service.initLoginRetryCnt(model);
						// 접속여부 업데이트
						updateLogin(model);
						// 접속 히스토리
						updateLoginHistory(model);
					} else {
//						if(loginRetryCnt >= 5){ // 로그인 시도횟수 초과 == 16
//							check = 16;
//						}else{ // 이외 사유에 의한 로그인 불가 == 15
							check = 15;
//						}
						
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_IP, JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						SessionUtil.setValue(request, ConstantIF.SS_SESSION_ID, request.getSession().getId());// (클라이언트)접속IP

						SessionMn.init();
						SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
						SessionUtil.sessionClear(request);
						
						// 로그인시 첫페이지 권한설정...
						model.put("USER_NO", user.get("USER_NO"));
						model.put("WORK_IP", SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_IP));
						model.put("Session_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID)));

//						// 로그인 시도횟수 증가. 220513 kimzero
//						service.increaseLoginRetryCnt(model);
						
						// 접속여부 업데이트
						updateLogout(model);
						// 접속 히스토리
						updateLoginHistory(model);
					}
				} else {
					switch (reasonCd) {
					case "RETRY_CNT_OVER":
						check = 16;
						break;
					case "PWD_CHK":
						check = 17;
						break;
					case "PWD_DUEDATE_OVER":
						check = 18;
						break;
					case "INACTIVE_ID":
						check = 19;
						break;
					default:
						check = 0;
						break;
					}
				}
			} // 로그인일 경우 끝
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to login process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "로그인에 실패했습니다.등록되어 있지 않는 사용자입니다.");
			m.put("MSG", MessageResolver.getMessage("로그인실패"));
			check = 0;
		}	
		
		return check;
	}
	
}
