package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS140Dao")
public class TMSYS140Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID : list
     * Method 설명 : 로그내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	return executeQueryPageWq("tmsys140.list", model);
    }
    
	/**
     * Method ID 	: listE2
     * Method 설명 	: 로그내역 조회
     * 작성자 		: dhkim
     * @param model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	return executeQueryPageWq("tmsys140.listE2", model);
    }
    
	/**
     * Method ID 	: listE3
     * Method 설명 	: 사용자 접속로그 정보S
     * 작성자 		: dhkim
     * @param model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	return executeQueryPageWq("tmsys140.listE3", model);
    }
    
	/**
     * Method ID 	: getStatisticsCount
     * Method 설명 	: 사용자 접속로그 정보 (통계)
     * 작성자 		: dhkim
     * @param model
     * @return
     */
    public GenericResultSet getStatistics(Map<String, Object> model) {
    	return executeQueryPageWq("tmsys140.getStatistics", model);
    }
    
	/**
	 * Method ID 	: insertE2
	 * Method 설명 	: 로그적재 
	 * 작성자 			: dhkim
	 * 
	 * @param model
	 * @return
	 */
	public Object insertE2(Map<String, Object> model) {
		return executeInsert("tmsys140.insertE2", model);
	}
}
