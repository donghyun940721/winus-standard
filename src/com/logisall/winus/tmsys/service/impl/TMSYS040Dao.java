package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS040Dao")
public class TMSYS040Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
    /**
     * Method ID  : list
     * Method 설명  : 프로그램목록 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("tmsys080.list", model);
    }
	
    
    /**
     * Method ID  : overlapCheck
     * Method 설명  : 프로그램메뉴 등록전 ID중복체크
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Integer overlapCheck(Map<String, Object> model) {
        return (Integer)executeView("tmsys080.overlapCheck", model);
    }
    /**
     * Method ID  : insert
     * Method 설명  : 프로그램메뉴 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("tmsys080.insert", model);
    }
    
    /**
     * Method ID  : update
     * Method 설명  : 프로그램메뉴 수정
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("tmsys080.update", model);
    }
    
    /**
     * Method ID  : delete
     * Method 설명  : 프로그램메뉴 삭제
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object delete(Map<String, Object> model) {
        return executeDelete("tmsys080.delete", model);
    }
}
