package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.TMSYS040Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("TMSYS040Service")
public class TMSYS040ServiceImpl extends AbstractServiceImpl implements TMSYS040Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    private final static String[] CHECK_VALIDATE_TMSYS080 = {"PROGRAM_NM", "TOP_MENU_CD", "URL"};
    
    @Resource(name = "TMSYS040Dao")
    private TMSYS040Dao dao;

    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명  : 프로그램관리 목록 조회
     * 작성자           : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 대체 Method ID  : save
     * 대체 Method 설명  : 프로그램관리 목록 등록,수정
     * 작성자           : chsong
     * @param  model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        loger.info(model);
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("programId"    , model.get("PROGRAM_ID" + i));
                modelDt.put("programNm"    , model.get("PROGRAM_NM" + i));
                modelDt.put("topMenuCd"    , model.get("TOP_MENU_CD"+ i));
                modelDt.put("viewNm"       , model.get("VIEW_NM"   + i));
                modelDt.put("url"          , model.get("URL"       + i));
                modelDt.put("remark"       , model.get("REMARK"    + i));
                
                modelDt.put("WORK_IP"      , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("USER_ID"      , model.get(ConstantIF.SS_USER_ID));
                modelDt.put("ST_GUBUN"     , model.get("ST_GUBUN"   + i));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    //ID가 중복되는지 유효성검사
                    if(dao.overlapCheck(modelDt) == 0){
                        dao.insert(modelDt);
                    }else{
                        errCnt++;
                        throw new BizException(MessageResolver.getMessage("program.check"));
                    }
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else{
                    errCnt++;
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            e.printStackTrace();
            m.put("errCnt", errCnt);
            m.put("MSG", e.getMessage());
        }
        return m;
    }
    
    /**
     * 대체 Method ID  : delete
     * 대체 Method 설명  : 프로그램관리 삭제
     * 작성자                    : chsong
     * @param  model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        loger.info(model);
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("programId"    , model.get("PROGRAM_ID" + i));
                dao.delete(modelDt);
            }     
            m.put("errCnt" , 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
        } catch(Exception e){
            m.put("errCnt" , 1);
            m.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return m;
    }   
    
//    /**
//     * 
//     * 대체 Method ID   : save
//     * 대체 Method 설명  : 프로그램관리 목록 등록,수정,삭제 합쳐진거
//     * 작성자           : chsong
//     * @param  model
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public Map<String, Object> save(Map<String, Object> model) throws Exception {
//        Map<String, Object> m = new HashMap<String, Object>();
//        int errCnt = 0;
//        try{
//            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
//                Map<String, Object> modelDt = new HashMap<String, Object>();
//                modelDt.put("WORK_IP"       , model.get(ConstantIF.SS_CLIENT_IP));
//                modelDt.put("USER_ID"       , model.get(ConstantIF.SS_USER_ID));
//                modelDt.put("selectIds"     , model.get("selectIds"));
//                modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN" + i));
//                modelDt.put("programid"    , model.get("PROGRAM_ID" + i));
//                modelDt.put("programnm"    , model.get("PROGRAM_NM" + i));
//                modelDt.put("top_menucd"   , model.get("TOP_MENU_CD"+ i));
//                modelDt.put("view_nm"       , model.get("VIEW_NM"+ i));
//                modelDt.put("url"           , model.get("URL"+ i));
//                modelDt.put("remark"        , model.get("REMARK"+ i));
//                
//                //필수값 체크
//                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_TMSYS080);
//                
//                if("INSERT".equals(model.get("ST_GUBUN"+i))){
//                    //ID가 중복되는지 유효성검사
//                    if(dao.overlapCheck(modelDt) == 0){
//                        dao.insert(modelDt);
//                    }else{
//                        errCnt++;
//                        m.put("errCnt", errCnt);
//                        throw new BizException(MessageResolver.getMessage("program.check"));
//                    }
//                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
//                    dao.update(modelDt);
//                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
//                    dao.delete(modelDt);
//                }else{
//                    errCnt++;
//                    m.put("errCnt", errCnt);
//                    throw new BizException(MessageResolver.getMessage("save.error"));
//                }
//            }
//            m.put("errCnt", errCnt);
//            if(errCnt == 0){
//                m.put("MSG", MessageResolver.getMessage("save.success"));
//            }
//        } catch (BizException be) {
//            if (log.isInfoEnabled()) {
//                log.info(be.getMessage());
//            }
//            m.put("MSG", be.getMessage());
//        } catch(Exception e){
//            throw e;
//        }
//        return m;
//    }
    
    /**
     * 대체 Method ID  : listExcel
     * 대체 Method 설명 : 프로그램목록 엑셀.
     * 작성자          : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }
}
