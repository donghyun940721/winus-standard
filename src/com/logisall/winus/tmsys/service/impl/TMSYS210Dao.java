package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS210Dao")
public class TMSYS210Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID : list
     * Method 설명 : 개인정보마스킹 조회
     * 작성자 : 
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	return executeQueryPageWq("tmsys210.list", model);
    }
    
    
	/**
	 * Method ID	: update 
	 * Method 설명	: 개인정보마스킹 업데이트
	 * 작성자			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("tmsys210.update", model);
	}
	
	
	/**
	 * Method ID 	: insert 
	 * Method 설명 	: 개인정보마스킹 대상 등록
	 * 작성자 : 
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("tmsys210.insert", model);
	}
}
