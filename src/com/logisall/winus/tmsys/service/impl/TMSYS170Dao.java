package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS170Dao")
public class TMSYS170Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID : list
     * Method 설명 : 로그내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	return executeQueryPageWq("tmsys170.list", model);
    }
    
    
	/**
	 * Method ID	: update 
	 * Method 설명	: 버전 정보 업데이트
	 * 작성자			: donghyun
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("tmsys170.update", model);
	}
	
	
	/**
	 * Method ID 	: insert 
	 * Method 설명 	: 신규 버전관리 대상 등록
	 * 작성자 : dhkim
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("tmsys170.insert", model);
	}
}
