package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.TMSYS170Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("TMSYS170Service")
public class TMSYS170ServiceImpl extends AbstractServiceImpl implements TMSYS170Service {
    
    @Resource(name = "TMSYS170Dao")
    private TMSYS170Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 로그내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    
    /**
     * 
     * 대체 Method ID		: save
     * 대체 Method 설명	: 버전 변경내역 저장 
     * 작성자				: dhkim 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN"     + i));
                modelDt.put("APP_CD"        , model.get("APP_CD"       + i));
                modelDt.put("APP_NM"        , model.get("APP_NM"       + i));
                modelDt.put("APP_COMMENT"   , model.get("APP_COMMENT"  + i));
                modelDt.put("VERSION_CODE"  , model.get("VERSION_CODE" + i));
                modelDt.put("VERSION_NAME"  , model.get("VERSION_NAME" + i));			
			    
				if("UPDATE".equals(model.get("ST_GUBUN"+i))){
					dao.update(modelDt);					
				}
				else if("INSERT".equals(model.get("ST_GUBUN"+i))){
					dao.insert(modelDt);
				}				
				else{
                   errCnt++;
                   m.put("errCnt", errCnt);
                   throw new BizException(MessageResolver.getMessage("save.error"));                   
				}				
            }
            
            m.put("errCnt", errCnt);
            
            //한번이라도 "D"면 삭제
            if (strGubun == "D")
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
            else
            	m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        
        return m;
    }
}
