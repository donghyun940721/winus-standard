package com.logisall.winus.tmsys.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.tmsys.service.TMSYS140Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("TMSYS140Service")
public class TMSYS140ServiceImpl extends AbstractServiceImpl implements TMSYS140Service {
    
    @Resource(name = "TMSYS140Dao")
    private TMSYS140Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 로그내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID 	: listE2
     * Method 설명 	: 로그내역 조회
     * 작성자 			: dhkim
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.listE2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }  
    
    /**
     * Method ID 	: listE3
     * Method 설명 	: 사용자 접속로그 정보
     * 작성자 		: dhkim
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.listE3(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }       
    
    /**
     * Method ID 	: getStatisticsCount
     * Method 설명 	: 사용자 접속로그 정보 (통계)
     * 작성자 		: dhkim
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getStatistics(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.getStatistics(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }       
    
	/**
     * 
     * 대체 Method ID		: insertE2
     * 대체 Method 설명		: 로그적재 
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> insertE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelDt = new HashMap<String, Object>();

        try{        	
        	modelDt.put("LC_ID",          model.get(ConstantIF.SS_SVC_NO));
        	modelDt.put("CUST_ID",        model.get("CUST_ID"));
        	modelDt.put("CUSTOM_KEY1",    model.get("CUSTOM_KEY1"));
        	modelDt.put("CUSTOM_KEY2",    model.get("CUSTOM_KEY2"));
        	modelDt.put("SERVICE",        model.get("SERVICE"));
        	modelDt.put("FUNCTION",       model.get("FUNCTION"));
        	modelDt.put("LOG_LEVEL",      model.get("LOG_LEVEL"));
        	modelDt.put("LOG_TYPE",       model.get("LOG_TYPE"));
        	modelDt.put("LOG_DESC",       model.get("LOG_DESC"));
        	modelDt.put("ERROR_CODE",     model.get("ERROR_CODE"));
        	modelDt.put("ERROR_DESC",     model.get("ERROR_DESC"));
        	modelDt.put("REG_NO",         model.get(ConstantIF.SS_USER_NO));
        	modelDt.put("USER_DEFINED1",  model.get("USER_DEFINED1"));
        	modelDt.put("USER_DEFINED2",  model.get("USER_DEFINED2"));
        	modelDt.put("USER_DEFINED3",  model.get("USER_DEFINED3"));
        	modelDt.put("USER_DEFINED4",  model.get("USER_DEFINED4"));
        	modelDt.put("USER_DEFINED5",  model.get("USER_DEFINED5"));
        	
        	dao.insertE2(modelDt); 
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
            map.put("MSG_ORA", "");
            map.put("errCnt", 0);        	
        	
        }
        catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("save.error") );
        }
        
        
        return map;
    }
}
