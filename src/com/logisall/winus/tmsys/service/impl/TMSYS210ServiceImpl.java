package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.TMSYS210Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("TMSYS210Service")
public class TMSYS210ServiceImpl extends AbstractServiceImpl implements TMSYS210Service {
    
    @Resource(name = "TMSYS210Dao")
    private TMSYS210Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    
    /**
     * 
     * 대체 Method ID		: save
     * 대체 Method 설명	: 개인정보마스킹 저장 
     * 작성자				: 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                modelDt.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
                
                modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN"     + i));
                modelDt.put("LC_ID"        , model.get("LC_ID"       + i));
                modelDt.put("CUST_ID"        , model.get("CUST_ID"       + i));
                modelDt.put("EXCEL_MASK_YN"   , model.get("EXCEL_MASK_YN"  + i));
                modelDt.put("ORD_MASK_DATE_NUM"  , model.get("ORD_MASK_DATE_NUM" + i));
                modelDt.put("ORD_MASK_DATE_UNIT_CD"  , model.get("ORD_MASK_DATE_UNIT_CD" + i));			
                modelDt.put("PRSNDTA_ID"  , model.get("PRSNDTA_ID" + i));			
                modelDt.put("DEL_YN"  , model.get("DEL_YN" + i));			
			    
				if("UPDATE".equals(model.get("ST_GUBUN"+i))){
					dao.update(modelDt);					
				}
				else if("INSERT".equals(model.get("ST_GUBUN"+i))){
					dao.insert(modelDt);
				}else{
                   errCnt++;
                   m.put("errCnt", errCnt);
                   throw new BizException(MessageResolver.getMessage("save.error"));                   
				}				
            }
            
            m.put("errCnt", errCnt);
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        
        return m;
    }
}
