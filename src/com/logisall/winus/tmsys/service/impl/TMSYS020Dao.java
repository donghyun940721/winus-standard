package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS020Dao")
public class TMSYS020Dao extends SqlMapAbstractDAO {
protected Log log = LogFactory.getLog(this.getClass());
    
//    /**
//     * Method ID  : selectUserAuthLevel
//     * Method 설명  : 조회시 필요한 값 (권한레벨, 소속) 조회
//     * 작성자             : chsong
//     * @param model
//     * @return
//     */
//    public Object selectUserAuthLevel(Map<String, Object> model) {
//        return executeView("tmsys090.selectUserAuthLevel", model);
//    }
    
    /**
     * Method ID  : listAuth
     * Method 설명  : 권한코드 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listAuth(Map<String, Object> model) {
        return executeQueryPageWq("tmsys050.selectAuth", model);
    }
    
    /**
     * Method ID  : listTopMenu
     * Method 설명  : 권한코드 탑메뉴
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listTopMenu(Map<String, Object> model) {
        return executeQueryPageWq("tmsys060.selectTopMenu", model);
    }

    /**
     * Method ID  : listTopMenu
     * Method 설명  : 좌측하단메뉴레 해당하는 권한코드 레벨 갖고온다
     * 작성자             : chsong
     * @param model
     * @return
     */
    public String selectAuthLevel(Map<String, Object> model) {
        return (String)executeView("tmsys050.selectAuthLevel", model);
    }
    
    /**
     * Method ID  : listMenuBtn
     * Method 설명  : 권한코드 상세메뉴
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listMenuBtn(Map<String, Object> model) {
        return executeQueryPageWq("tmsys070.selectMenuBtn", model);
    }
    
    /**
     * Method ID  : insertAuth
     * Method 설명  : 권한코드 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertAuth(Map<String, Object> model) {
        return executeInsert("tmsys050.insertAuth", model);
    }
    
    /**
     * Method ID  : insertAuth
     * Method 설명  : 권한코드 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateAuth(Map<String, Object> model) {
        return executeInsert("tmsys050.updateAuth", model);
    }
    



    /**
     * Method ID  : deleteAuth050
     * Method 설명  : 권한삭제
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object deleteAuth050(Map<String, Object> model) {
        return executeDelete("tmsys050.deleteAuth", model);
    }
    
    /**
     * Method ID  : updateDelYn
     * Method 설명  : 권한삭제로인한 사용자관리 업데이트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateDelYn(Map<String, Object> model) {
        return executeUpdate("tmsys090.updateDelYn", model);
    }
    
    /**
     * Method ID  : selectTopCnt
     * Method 설명  : top메뉴 권한저장 전 존재유무확인
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Integer selectTopCnt(Map<String, Object> model){
        return (Integer)executeView("tmsys060.selectTopCnt", model);
    }
    
    /**
     * Method ID  : insertAuth060
     * Method 설명  : 권한별TOPMENU 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertAuth060(Map<String, Object> model){
        return executeInsert("tmsys060.insertAuth", model);
    }

    /**
     * Method ID  : updateAuth060
     * Method 설명  : 권한별TOPMENU 수정
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateAuth060(Map<String, Object> model){
        return executeUpdate("tmsys060.updateAuth", model);
    }   
    
    /**
     * Method ID  : deleteAuth060
     * Method 설명  : 권한별TOPMENU 삭제
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object deleteAuth060(Map<String, Object> model) {
        return executeDelete("tmsys060.deleteAuth", model);
    }
    
    /**
     * Method ID  : selectProgramCnt
     * Method 설명  : 프로그램 권한 저장전 존재유무 판단
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Integer selectProgramCnt(Map<String, Object> model){
        return (Integer)executeView("tmsys070.selectProgramCnt", model);
    }
    /**
     * Method ID  : insertAuth070
     * Method 설명  : 권한별프로그램 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertAuth070(Map<String, Object> model){
        return executeInsert("tmsys070.insertAuth", model);
    }

    /**
     * Method ID  : updateAuth070
     * Method 설명  : 권한별프로그램 수정
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateAuth070(Map<String, Object> model){
        return executeUpdate("tmsys070.updateAuth", model);
    }
    
    /**
     * Method ID  : deleteAuth070
     * Method 설명  : 권한별프로그램 삭제
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object deleteAuth070(Map<String, Object> model) {
        return executeDelete("tmsys070.deleteAuth", model);
    }
    
    /**
     * Method ID  : updateAuth070
     * Method 설명  : 권한별프로그램 삭제2 -> 최상위권한 제외한 나머지 권한사용자가 삭제시 업데이트 처리를한다.
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateBtnAuth070(Map<String, Object> model){
        return executeUpdate("tmsys070.updateBtnAuth", model);
    }
    
    /**
     * Method ID  : selectAuthBox
     * Method 설명  : 권한프로그램 셀렉트박스 설정 (추후 변경)
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectAuthBox(Map<String, Object> model){
        return executeQueryForList("tmsys050.selectAuthBox", model);
    }
    
}
