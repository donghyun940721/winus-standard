package com.logisall.winus.wmsdh.web;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsdh.service.WMSDH200Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSDH200Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSDH200Service")
    private WMSDH200Service service;
    
    /*-
	 * Method ID    : WMSDH200
	 * Method 설명      : PDA 통계조회
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSDH200.action")
	public ModelAndView WMSDH200(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsdh/WMSDH200");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 출고작업자 통계 조회
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDH200/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
}
