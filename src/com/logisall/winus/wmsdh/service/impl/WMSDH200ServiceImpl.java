package com.logisall.winus.wmsdh.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsdh.service.WMSDH200Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSDH200Service")
public class WMSDH200ServiceImpl implements WMSDH200Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSDH200Dao")
    private WMSDH200Dao dao;
	
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 출고작업자 통계 조회
     * 작성자                      : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        String LC_ID = (String)model.get(ConstantIF.SS_SVC_NO);
        ArrayList<String> LC_ID_ARR = new ArrayList<String>();
        if(LC_ID.equals("0000003541") || LC_ID.equals("0000003721") || LC_ID.equals("0000003722") || LC_ID.equals("0000003720")){
            LC_ID_ARR.add("0000003541");
            LC_ID_ARR.add("0000003721");
            LC_ID_ARR.add("0000003722");
            LC_ID_ARR.add("0000003720");
        }
        else{
            LC_ID_ARR.add(LC_ID);
        }
        model.put("LC_ID_ARR",LC_ID_ARR);
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
}
