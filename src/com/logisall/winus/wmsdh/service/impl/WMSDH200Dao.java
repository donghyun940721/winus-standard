package com.logisall.winus.wmsdh.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDH200Dao")
public class WMSDH200Dao extends SqlMapAbstractDAO {
	
	/**
	 * Method ID : list 
	 * Method 설명 : 출고작업자 통계 조회
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryWq("wmsdh200.list", model);
	}
}
