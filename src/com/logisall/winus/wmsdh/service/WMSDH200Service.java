package com.logisall.winus.wmsdh.service;

import java.util.Map;

public interface WMSDH200Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
