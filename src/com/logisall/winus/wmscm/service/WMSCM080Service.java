package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM080Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list02(Map<String, Object> model) throws Exception;
    public Map<String, Object> listClient(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listGrn(Map<String, Object> model) throws Exception;
    public Map<String, Object> listGroupByLot(Map<String, Object> model) throws Exception;    
}
