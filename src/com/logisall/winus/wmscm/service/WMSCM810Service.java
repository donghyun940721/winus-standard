package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM810Service {

    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
}
