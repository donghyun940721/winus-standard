package com.logisall.winus.wmscm.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmscm.service.WMSCM300Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM300Service")
public class WMSCM300ServiceImpl implements WMSCM300Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM300Dao")
    private WMSCM300Dao dao;

    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        GenericResultSet res;
    	List columnParser;
        
        
        try {
            long templateCount = dao.templateCount(model);
            if(templateCount < 1){
            	
            	int lcIdTemplateCount = dao.checkLcIdTemplate(model);
            	
            	if(lcIdTemplateCount == 1){
                	dao.insertLcIdTemplate(model);
                	dao.insertLcIdColumns(model);
                	
                	
            	}else if(lcIdTemplateCount < 1){
            		dao.insertDefaultTemplate(model);
                	dao.insertDefaultColumns(model);
            	}
            	
            	res = dao.list(model);
            }else{
            	res = dao.list(model);
            }
            	columnParser = res.getList();
	    		
        	for(int i=0; i<columnParser.size(); i++){
        		Map<String, String> tempMap = ((Map<String, String>)columnParser.get(i));    			
            		
            	for(String key : tempMap.keySet()){        			
            		if(key.equals("COLUMN_NAME") || key.equals("TEMPLATE_NAME")){
            			//System.out.println(tempMap.get(key));
                		//System.out.println(MessageResolver.getText(tempMap.get(key).toString()));
                			
                		tempMap.put(key, MessageResolver.getText(tempMap.get(key).toString()));
            		}        			        			    			
            	}  
        	}
        	res.setList(columnParser);

            map.put("LIST", res);
                        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }

    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> allList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		long templateCount = dao.templateCount(model);
    		if(templateCount < 1){
            	
            	int lcIdTemplateCount = dao.checkLcIdTemplate(model);
            	
            	if(lcIdTemplateCount == 1){
                	dao.insertLcIdTemplate(model);
                	dao.insertLcIdColumns(model);

            	}else if(lcIdTemplateCount < 1){
            		dao.insertDefaultTemplate(model);
                	dao.insertDefaultColumns(model);
            	}
            	
            	
            }
    		map.put("LIST", dao.allList(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    		
    	}
    	
    	return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 그리드 설정 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("id", model.get("id" + i));
	             modelDt.put("templateId", model.get("templateId" + i));
	             modelDt.put("columnCode", model.get("columnCode" + i));
	             modelDt.put("columnName", model.get("columnName" + i));
	             modelDt.put("columnType", model.get("columnType" + i));
	             modelDt.put("columnSeq", model.get("columnSeq" + i));
	             modelDt.put("columnSize", model.get("columnSize" + i));
	             modelDt.put("columnColor", model.get("columnColor" + i));
	             modelDt.put("columnAlign", model.get("columnAlign" + i));
	             modelDt.put("columnReadOnly", model.get("columnReadOnly" + i));
	             modelDt.put("columnVisible", model.get("columnVisible" + i));
	             modelDt.put("columnAutoLineChange", model.get("columnAutoLineChange" + i));
	             
	             modelDt.put("vrSrchCustCd", model.get("vrSrchCustCd" + i));
	             modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("REG_NO", model.get(ConstantIF.SS_USER_NO));
	             
	             dao.save(modelDt);
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 기본 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> defaultList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	GenericResultSet res;
    	List columnParser;
    	
    	if(!model.containsKey("LC_ID")){
    		String lcId = (String)model.get("SS_SVC_NO");
    		model.put("LC_ID", lcId);
    	}
        try {
        	long defaultTemplateCount = dao.defaultTemplateCount(model);
    		if(defaultTemplateCount < 1){            	
        		dao.createDefaultTemplate(model);
            	dao.createDefaultColumns(model);            	
            	     	
            	res = dao.originList(model);            	
            }
    		else{
    			res = dao.defaultList(model);
    		}
    		
    		columnParser = res.getList();
    		    		
    		for(int i=0; i<columnParser.size(); i++){
    			Map<String, String> tempMap = ((Map<String, String>)columnParser.get(i));    			
        		
        		for(String key : tempMap.keySet()){        			
        			if(key.equals("COLUMN_NAME")){
        				//System.out.println(tempMap.get(key));
            			//System.out.println(MessageResolver.getText(tempMap.get(key).toString()));
            			
            			tempMap.put(key, MessageResolver.getText(tempMap.get(key).toString()));
        			}        			        			    			
        		}  
    		}
    		
    		res.setList(columnParser);  		    		
    		map.put("LIST", res);
        		        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 기본 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> originList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
    		map.put("LIST", dao.originList(model));
        		        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : fixedColumn
     * Method 설명 : 그리드 고정 컬럼 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> fixColumnSave(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	            
        	model.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
        	model.put("REG_NO", model.get(ConstantIF.SS_USER_NO));
             
            dao.fixColumnSave(model);
	    	
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : templateList
     * Method 설명 :그리드 템플릿 기본 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> templateList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
        	map.put("LIST", dao.templateList(model));
                        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : forceUpdate
     * Method 설명 : 사용자 강제 컬럼 변경 업데이트
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> forceUpdate(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("id", model.get("id" + i));
	             modelDt.put("columnCode", model.get("columnCode" + i));
	             modelDt.put("lcId", model.get("lcId"));
	             modelDt.put("vrTemplateType", model.get("vrTemplateType"));
	             modelDt.put("UPD_NO", model.get(ConstantIF.SS_USER_NO));
	             
	             dao.forceUpdate(modelDt);
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    	
    	
    }
    
    /**
     * Method ID : list
     * Method 설명 : 그리드 설정 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> adminSave(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             
	             modelDt.put("id", model.get("id" + i));
	             modelDt.put("templateId", model.get("templateId" + i));
	             modelDt.put("columnCode", model.get("columnCode" + i));
	             modelDt.put("columnName", model.get("columnName" + i));
	             modelDt.put("columnType", model.get("columnType" + i));
	             modelDt.put("columnSeq", model.get("columnSeq" + i));
	             modelDt.put("columnSize", model.get("columnSize" + i));
	             modelDt.put("columnColor", model.get("columnColor" + i));
	             modelDt.put("columnAlign", model.get("columnAlign" + i));
	             modelDt.put("columnReadOnly", model.get("columnReadOnly" + i));
	             modelDt.put("columnVisible", model.get("columnVisible" + i));
	             modelDt.put("columnAutoLineChange", model.get("columnAutoLineChange" + i));
	             modelDt.put("columnCombo", model.get("columnCombo" + i));
	             modelDt.put("columnComboRelated", model.get("columnComboRelated" + i));
	             modelDt.put("columnFunction", model.get("columnFunction" + i));
	             
	             modelDt.put("vrSrchCustCd", model.get("vrSrchCustCd" + i));
	             modelDt.put("templateType", model.get("templateType"));
	             modelDt.put("LC_ID", model.get("lcId"));
	             modelDt.put("REG_NO", model.get(ConstantIF.SS_USER_NO));

	             String stGubun = (String)model.get("stGubun" + i);
	             if("S".equals(stGubun)){
	            	 dao.adminSave(modelDt);
	             }else if("I".equals(stGubun)){
	            	 dao.adminNewInsert(modelDt);
	             }
	             
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : createNewTemplate
     * Method 설명 :신규템플릿 추가
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> createNewTemplate(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
        	dao.createNewTemplate(model);
            map.put("templateId", model.get("templateId"));            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    

    /**
     * 
     * 대체 Method ID   	: saveExcelGridTemplate
     * 대체 Method 설명     : 그리드관리 템플릿 업로드 
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> WMSYS240inExcelUploadTemplate(Map<String, Object> model,  List list) throws Exception {
    	int listBodyCnt = (list != null)?list.size():0;
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		if(listBodyCnt > 0){
    			for(int i = 0 ; i < listBodyCnt ; i ++){
    				Map<String,String> temp = (Map<String,String>)list.get(i);
    				String COLUMN_CODE		= temp.get("I_COLUMN_CODE");
    				String COLUMN_SEQ		= temp.get("I_COLUMN_SEQ");
    				String COLUMN_NAME		= temp.get("I_COLUMN_NAME");
    				String COLUMN_TYPE		= temp.get("I_COLUMN_TYPE");    				
    				String COLUMN_SIZE		= temp.get("I_COLUMN_SIZE");
    				String COLUMN_COLOR		= temp.get("I_COLUMN_COLOR");
    				
    				String COLUMN_ALIGN		= temp.get("I_COLUMN_ALIGN");
    				String COLUMN_READ_ONLY		= temp.get("I_COLUMN_READ_ONLY");
    				String COLUMN_VISIBLE		= temp.get("I_COLUMN_VISIBLE");
    				String COLUMN_AUTO_LINE_CHANGE		= temp.get("I_COLUMN_AUTO_LINE_CHANGE");
    				String COLUMN_COMBO		= temp.get("I_COLUMN_COMBO");
    				
    				String COLUMN_COMBO_RELATED		= temp.get("I_COLUMN_COMBO_RELATED");
    				String COLUMN_FUNCTION		= temp.get("I_COLUMN_FUNCTION");
    				//String FIXED_COLUMN_INDEX		= temp.get("I_FIXED_COLUMN_INDEX");
    				String ST_GUBUN		= temp.get("I_ST_GUBUN");
    				String ID		= temp.get("I_ID");
    				
    				//String TEMPLATE_NAME		= temp.get("I_TEMPLATE_NAME");
    				String TEMPLATE_TYPE		= temp.get("I_TEMPLATE_TYPE");
    				//String LC_ID		= temp.get("I_LC_ID");
    				//String CUST_ID		= temp.get("I_CUST_ID");
    				String TEMPLATE_ID		= temp.get("I_TEMPLATE_ID");
    				
    				Map<String, Object> modelDt = new HashMap<String, Object>();
    				
		            modelDt.put("id", ID);
		            modelDt.put("templateId", TEMPLATE_ID);
		            modelDt.put("columnCode", COLUMN_CODE);
		            modelDt.put("columnName", COLUMN_NAME);
		            modelDt.put("columnType", COLUMN_TYPE);
		            modelDt.put("columnSeq", COLUMN_SEQ);
		            modelDt.put("columnSize", COLUMN_SIZE);
		            modelDt.put("columnColor", COLUMN_COLOR);
		            modelDt.put("columnAlign", COLUMN_ALIGN);
		            modelDt.put("columnReadOnly", COLUMN_READ_ONLY);
		            modelDt.put("columnVisible", COLUMN_VISIBLE);
		            modelDt.put("columnAutoLineChange", COLUMN_AUTO_LINE_CHANGE);
		            modelDt.put("columnCombo", COLUMN_COMBO);
		            modelDt.put("columnComboRelated", COLUMN_COMBO_RELATED);
		            modelDt.put("columnFunction", COLUMN_FUNCTION);
		            
		            modelDt.put("vrSrchCustCd", "");
		            modelDt.put("templateType", TEMPLATE_TYPE);
		            modelDt.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
		            modelDt.put("REG_NO", model.get(ConstantIF.SS_USER_NO));

		            String stGubun = ST_GUBUN;
		            if("S".equals(stGubun)){
		            	dao.adminSave(modelDt);
		            }else if("I".equals(stGubun)){
		            	dao.adminNewInsert(modelDt);
		            }
    			}   
    		}
    		//TEMPLATE_ID, TEMPLATE_TYPE => parameter로 받아오기.
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   throw e;
    	}
       
        return m;
          
    }
}
