package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM310Dao")
public class WMSCM310Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : list
	 * Method 설명 : 그리드 리스트 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmsms310.list", model));
		return wqrs;
	}
	
	
	/**
	 * Method ID : save
	 * Method 설명 : 그리드 템플릿 기본 컬럼 삽입
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void save(Map<String, Object> model) {
		executeUpdate("wmsms310.save", model);
		
	}
	
	public long custZoneCount(Map<String, Object> model){
		return ((Integer)executeQueryForObject("wmsms310.custZoneCount", model)).intValue();
	}


   
}
