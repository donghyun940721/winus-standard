package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM101Dao")
public class WMSCM101Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID  : list
     * Method 설명  : UOM조회
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms101.list", model);
    }   
}
