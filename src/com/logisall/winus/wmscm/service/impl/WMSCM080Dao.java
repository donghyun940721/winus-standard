package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM080Dao")
public class WMSCM080Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	 /**
     * Method ID    : list
     * Method 설명      : 로케이션 조회
     * 작성자                 : 기드온
     * @param   model
     * @return  
     * @throws Exception 
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms080.list", model);
    } 
    
    /**
     * Method ID    : list02
     * Method 설명      : 로케이션 조회
     * 작성자                 : 기드온
     * @param   model
     * @return  
     * @throws Exception 
     */
    public GenericResultSet list02(Map<String, Object> model) {
        return executeQueryPageWq("wmsms080.list02", model);
    }
    
    //
    /**
    * Method ID    : listGrn
    * Method 설명      : 로케이션 조회2
    * 작성자                 : chSong
    * @param   model
    * @return  
    * @throws Exception 
    */
   public GenericResultSet listGrn(Map<String, Object> model) {
       return executeQueryPageWq("wmsms080.listGrn", model);
   }   
   
   /**
   * Method ID    : listLoc
   * Method 설명      : 로케이션 조회 LOC
   * 작성자                 : chSong
   * @param   model
   * @return  
   * @throws Exception 
   */
   public GenericResultSet listLoc(Map<String, Object> model) {
       return executeQueryPageWq("wmsms080.listOnlyLoc", model);
   }   
   
	 /**
    * Method ID    : listGroupByLot
    * Method 설명      : 로케이션 조회
    * 작성자                 : 기드온
    * @param   model
    * @return  
    * @throws Exception 
    */
   public GenericResultSet listGroupByLot(Map<String, Object> model) {
       return executeQueryPageWq("wmsms080.list2", model);
   }   
      
}
