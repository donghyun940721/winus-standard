package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmscm.service.WMSCM081Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM081Service")
public class WMSCM081ServiceImpl implements WMSCM081Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM081Dao")
    private WMSCM081Dao dao;


    /**
     * Method ID     : list
     * Method 설명       : ZONE 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID     : listCust
     * Method 설명       : 거래처ZONE 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listCust(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listCust(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID     : save
     * Method 설명       : 거래처ZONE 신규 저장
     * 작성자                  : khkim
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] locId  = new String[tmpCnt];
                String[] locCd  = new String[tmpCnt];
                String[] zoneSeq  = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	locId[i]    = (String)model.get("LOC_ID_"+i);
                	locCd[i]   = (String)model.get("LOC_CD_"+i);
                	zoneSeq[i]   = (String)model.get("WORK_SEQ_"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("LOC_ID", locId);
                modelIns.put("LOC_CD", locCd);
                modelIns.put("ZONE_SEQ", zoneSeq);
            	
                
                modelIns.put("ZONE_NM", model.get("ZONE_NM"));
                
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
 
                m.put("MSG", MessageResolver.getMessage("insert.success"));
                m.put("errCnt", errCnt);
            
                
                modelIns = (Map<String, Object>)dao.save(modelIns);
                ServiceUtil.isValidReturnCode("WMSCM083", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
        	m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
