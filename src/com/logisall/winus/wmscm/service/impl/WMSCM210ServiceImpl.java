package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmscm.service.WMSCM210Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM210Service")
public class WMSCM210ServiceImpl implements WMSCM210Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM210Dao")
    private WMSCM210Dao dao;

    /**
     * Method ID   : getTp
     * Method 설명    : getTp
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getTp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();

            // session 및 필요정보
            modelIns.put("vrCustCd"         , model.get("vrCustCd")  );
            modelIns.put("vrTemplateType"   , model.get("vrTemplateType"));
            modelIns.put("vrTemplateFile"   , model.get("vrTemplateFile")   );
            modelIns.put("LC_ID"            , (String)model.get(ConstantIF.SS_SVC_NO)   );

            // dao
            modelIns = (Map<String, Object>)dao.getTp(modelIns);
            ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("DS_TP", modelIns.get("DS_TP"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   : getTp
     * Method 설명    : getTp
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getTpList(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();

            // session 및 필요정보
            modelIns.put("vrCustCd"         , model.get("vrCustCd")  );
            modelIns.put("vrTemplateType"   , model.get("vrTemplateType"));
            modelIns.put("vrTemplateFile"   , model.get("vrTemplateFile")   );
            modelIns.put("LC_ID"            , (String)model.get(ConstantIF.SS_SVC_NO)   );

            // dao
            modelIns = (Map<String, Object>)dao.getTpList(modelIns);
            ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("DS_TP", modelIns.get("DS_TP"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   : getTpListALL
     * Method 설명    : getTpListALL
     * 작성자               : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getTpListALL(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try {
    		Map<String, Object> modelIns = new HashMap<String, Object>();
    		
    		// session 및 필요정보
    		modelIns.put("vrCustCd"         , model.get("vrCustCd")  );
    		modelIns.put("vrTemplateType"   , model.get("vrTemplateType"));
    		modelIns.put("vrTemplateFile"   , model.get("vrTemplateFile")   );
    		modelIns.put("LC_ID"            , (String)model.get(ConstantIF.SS_SVC_NO)   );
    		
    		// dao
    		modelIns = (Map<String, Object>)dao.getTpListALL(modelIns);
    		ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    		
    		System.out.println("DS_TP : " + modelIns.get("DS_TP"));
    		
    		m.put("DS_TP", modelIns.get("DS_TP"));
    		m.put("errCnt", 0);
    		
    	} catch(BizException be) {
    		m.put("errCnt", 1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID   : search
     * Method 설명    : 템플릿 정보에 따른 컬럼
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("vrTemplateId"     , model.get("vrTemplateId")  );
            modelIns.put("vrCustSeq"        , model.get("vrCustSeq"));

            // dao
            modelIns = (Map<String, Object>)dao.search(modelIns);
            ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            //우선 여기서 다국어처리해놓자
            //.vm에서 다국어적용안되길래 여기서 했음
            List<Map<String, Object>> dsTpcolList = (List<Map<String, Object>>)modelIns.get("DS_TPCOL");
            for(int i = 0 ; i < dsTpcolList.size() ; i++){
               dsTpcolList.get(i).put("FORMAT_COL_NM", MessageResolver.getText((String)dsTpcolList.get(i).get("FORMAT_COL_NM")));
            }
            m.put("DS_TPCOL", modelIns.get("DS_TPCOL"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }

    /**
     * Method ID   : getTpGrid
     * Method 설명    : getTpGrid
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getTpGrid(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();

            // session 및 필요정보
            modelIns.put("vrCustCd"         , model.get("vrCustCd")  );
            modelIns.put("vrTemplateType"   , model.get("vrTemplateType"));
            modelIns.put("vrTemplateFile"   , model.get("vrTemplateFile")   );
            modelIns.put("LC_ID"            , (String)model.get(ConstantIF.SS_SVC_NO)   );

            // dao
            modelIns = (Map<String, Object>)dao.getTpGrid(modelIns);
            ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("DS_TP", modelIns.get("DS_TP_GRID"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    /**
     * Method ID   : searchGrid
     * Method 설명    : 템플릿 정보에 따른 컬럼
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getResultGrid(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("vrTemplateId"     , model.get("vrTemplateId")  );
            modelIns.put("vrCustSeq"        , model.get("vrCustSeq"));

            // dao
            modelIns = (Map<String, Object>)dao.searchGrid(modelIns);
            ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            //우선 여기서 다국어처리해놓자
            //.vm에서 다국어적용안되길래 여기서 했음
            List<Map<String, Object>> dsTpcolList = (List<Map<String, Object>>)modelIns.get("DS_TPCOL_GRID");
            for(int i = 0 ; i < dsTpcolList.size() ; i++){
               dsTpcolList.get(i).put("FORMAT_COL_NM", MessageResolver.getText((String)dsTpcolList.get(i).get("FORMAT_COL_NM")));
            }
            m.put("DS_TPCOL", modelIns.get("DS_TPCOL_GRID"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
}
