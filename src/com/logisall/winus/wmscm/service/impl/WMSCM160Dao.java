package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM160Dao")
public class WMSCM160Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
//    /**
//     * Method ID  : selectItemGrp
//     * Method 설명  : LCID마다 다른 ITEMGRP selectBox 값 조회
//     * 작성자             : chsong
//     * @param model
//     * @return
//     */
//    public Object selectItemGrp(Map<String, Object> model){
//        return executeQueryForList("wmsms094.selectItemGrp", model);
//    }
//    
    /**
     * Method ID  : list
     * Method 설명  : 물류용기 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspl010.popList", model);
    }   
}
