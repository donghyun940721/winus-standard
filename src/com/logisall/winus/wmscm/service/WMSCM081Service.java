package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM081Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listCust(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	
}
