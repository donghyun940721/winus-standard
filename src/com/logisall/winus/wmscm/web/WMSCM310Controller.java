package com.logisall.winus.wmscm.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM310Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM310Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM310Service")
	private WMSCM310Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 그리드 설정 매인화면
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM310.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception  {
		return new ModelAndView("winus/wmscm/WMSCM310Q1");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 그리드 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM310/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : list
	 * Method 설명 : 그리드 수정 저장
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM310/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
