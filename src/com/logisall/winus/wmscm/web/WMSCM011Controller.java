package com.logisall.winus.wmscm.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM011Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM011Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM011Service")
	private WMSCM011Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 화주 조회 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM011.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM011Q1");
	}

	/*-
	 * Method ID : mnv
	 * Method 설명 : 거래처 조회 팝업화면(대화물류)
	 * 작성자 : ykim
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM011Q2.action")
	public ModelAndView mn2(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM011Q2");
	}
	
	/*-
	 * Method ID : mn3
	 * Method 설명 : 화주 조회 (거래처 복사용)
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM011Q3.action")
	public ModelAndView mn3(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM011Q3");
	}
	/*-
	 * Method ID : mn4
	 * Method 설명 : 거래처ZONE 조회 
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM011Q4.action")
	public ModelAndView mn4(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM011Q4");
	}
	/*-
	 * Method ID : mn5
	 * Method 설명 : ZONE별 거래처 조회 
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM011Q5.action")
	public ModelAndView mn5(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM011Q5");
	}
	/*-
	 * Method ID : list
	 * Method 설명 : 화주 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM011/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listClient
	 * Method 설명 : 화주 리스트  팝업 자동조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/listClient.action")
	public ModelAndView listClient(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		m = service.listClient(model);
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : listClient
	 * Method 설명 : 거래처 Zone 별 거래처 조회
	 * 작성자      : schan
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/listCustByZone.action")
	public ModelAndView listCustByZone(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jqGridJsonView");
		Map<String, Object> m = null;
		m = service.listCustByZone(model);
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID   : getTypicalCust
	 * Method 설명 : 물류센터 별 대표화주값 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/getTypicalCust.action")
	public ModelAndView getTypicalCust(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getTypicalCust(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID   : getWmsMoveOrgOrdNo
	 * Method 설명 : 원주문번호 자동채번
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/getWmsMoveOrgOrdNo.action")
	public ModelAndView getWmsMoveOrgOrdNo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getWmsMoveOrgOrdNo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID   : getCustLcGb
	 * Method 설명 : 물류센터운영타입이 창고/배송센터 일 경우 : 화주선택 가능 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/getCustLcGb.action")
	public ModelAndView getCustLcGb(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustLcGb(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID   : getCustIdByLcId
	 * Method 설명 : LC_ID로 해당 코드의 화주ID 가져오기
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/getCustIdByLcId.action")
	public ModelAndView getCustIdByLcId(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustIdByLcId(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID   : getCustByRestApiKey
	 * Method 설명 : 화주별 RestApi 키
	 * 작성자      : 
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/getCustByRestApiKey.action")
	public ModelAndView getCustByRestApiKey(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustByRestApiKey(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : cjPop01
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM099CJ.action")
	public ModelAndView cjPop01(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM099CJ");
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 화주 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM011/listLcSync.action")
	public ModelAndView listLcSync(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listLcSync(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: getCustEtcCode
	 * Method 설명	: 화주별 기타마스터 코드 가져오기
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM011/getCustEtcCode.action")
	public ModelAndView getCustEtcCode(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustEtcCode(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
