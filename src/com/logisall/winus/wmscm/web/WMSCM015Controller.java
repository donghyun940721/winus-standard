package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM015Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM015Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM015Service")
	private WMSCM015Service service;

	/*-
	 * Method ID    : wmscm015
	 * Method 설명      : 사원 팝업
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM015.action")
	public ModelAndView wmscm014(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM015Q1");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 사원  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM015/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

}
