package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM120Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM120Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM120Service")
	private WMSCM120Service service;

	/*-
	 * Method ID    : wmscm120
	 * Method 설명      : 도크장 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM120.action")
	public ModelAndView wmscm100(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM120Q1");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 도크장 조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM120/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
}
