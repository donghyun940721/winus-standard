package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM810Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM810Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM810Service")
	private WMSCM810Service service;

	/*-
	 * Method ID    : wmscm810
	 * Method 설명      : 상품조회 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM810M1.action")
	public ModelAndView wmscm810(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM810M1", service.selectData(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 상품조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM810M1/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 화주 조회 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM811M1.action")
	public ModelAndView wmscm811(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM811M1");
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 화주 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM811M1/list.action")
	public ModelAndView list2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}
