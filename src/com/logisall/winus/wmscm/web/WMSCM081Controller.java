package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM081Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM081Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM081Service")
	private WMSCM081Service service;

	/*-
	 * Method ID    : wmscm091
	 * Method 설명      : Zone POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM081.action")
	public ModelAndView wmscm081(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM081Q1");
	}
	
	/*-
	 * Method ID    : wmscm083
	 * Method 설명      : Zone 신규 POP 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM083.action")
	public ModelAndView wmscm083(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM083Q1");
	}
	
	/*-
	 * Method ID    : wmscm091
	 * Method 설명      : 거래처Zone POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM085.action")
	public ModelAndView wmscm085(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM085Q1");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : Zone조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM081/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 거래처Zone조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM085/listCust.action")
	public ModelAndView listCust(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listCust(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : save 
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM083/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.save(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
}
