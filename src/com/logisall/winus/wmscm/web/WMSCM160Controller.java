package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM160Service;
import com.logisall.winus.wmsms.service.WMSPL010Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM160Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPL010Service")
	private WMSPL010Service service;

	/*-
	 * Method ID    : mn
	 * Method 설명      : 물류용기 팝업
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM160.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM160Q1", service.selectBox(model));
	}

	@Resource(name = "WMSCM160Service")
	private WMSCM160Service service2;

	/*-
	 * Method ID    : wmscm162
	 * Method 설명      : 물류용기 POP 화면(멀티셀렉트)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM162.action")
	public ModelAndView wmscm162(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM160Q2", service.selectBox(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 물류용기 조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM162/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service2.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

}
