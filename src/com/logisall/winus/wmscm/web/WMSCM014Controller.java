package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM014Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM014Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM014Service")
	private WMSCM014Service service;

	/*-
	 * Method ID    : wmscm014
	 * Method 설명      : 조직도 팝업
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM014.action")
	public ModelAndView wmscm014(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM014Q1");
	}

	/*-
	* Method ID    : list
	* Method 설명      : 조직도 리스트
	* 작성자                 : chSong
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSCM014/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			// mav = new ModelAndView("jsonView", service.list(model));
			mav = new ModelAndView("winus/wmscm/WMSCM014Q1Data", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
}
