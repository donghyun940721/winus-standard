package com.logisall.winus.wmsct.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSSP050Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSSP050Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP050Service")
    private WMSSP050Service service;
    
    /*-
	 * Method ID    : WMSSP050
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSSP050.action")
	public ModelAndView WMSSP050(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSSP050", service.selectBox(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP050/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSSP050/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
        	String[][] headerEx = {
			        			{MessageResolver.getText("화주")				, "0", "0", "0", "0", "100"}
			        			,{MessageResolver.getText("수취인")     		, "1", "1", "0", "0", "100"}
			        			,{MessageResolver.getText("순번")     		, "2", "2", "0", "0", "100"}
			        			,{MessageResolver.getText("이미지")     		, "3", "3", "0", "0", "100"}
			        			,{MessageResolver.getText("SMS")     		, "4", "4", "0", "0", "100"}
			        			,{MessageResolver.getText("배정자")     		, "5", "5", "0", "0", "100"}
			        			,{MessageResolver.getText("접수자")     		, "6", "6", "0", "0", "100"}
			        			,{MessageResolver.getText("해피콜상태")     	, "7", "7", "0", "0", "100"}
			        			,{MessageResolver.getText("고객주문번호")     	, "8", "8", "0", "0", "100"}
			        			,{MessageResolver.getText("발주일(주문)")     	, "9", "9", "0", "0", "100"}
			        			,{MessageResolver.getText("판매처")     		, "10", "10", "0", "0", "100"}
			        			,{MessageResolver.getText("수취인")     		, "11", "11", "0", "0", "100"}
			        			,{MessageResolver.getText("전화번호1")     	, "12", "12", "0", "0", "100"}
			        			,{MessageResolver.getText("전화번호2")     	, "13", "13", "0", "0", "100"}
			        			,{MessageResolver.getText("주소")     		, "14", "14", "0", "0", "100"}
			        			,{MessageResolver.getText("제품명")     		, "15", "15", "0", "0", "100"}
			        			,{MessageResolver.getText("제품코드")     		, "16", "16", "0", "0", "100"}
			        			,{MessageResolver.getText("수량")     		, "17", "17", "0", "0", "100"}
			        			,{MessageResolver.getText("배송메시지")     	, "18", "18", "0", "0", "100"}
			        			,{MessageResolver.getText("상담내역")     		, "19", "19", "0", "0", "100"}
			        			,{MessageResolver.getText("확인사항")     		, "20", "20", "0", "0", "100"}
			        			,{MessageResolver.getText("거래배송사")     	, "21", "21", "0", "0", "100"}
			        			,{MessageResolver.getText("설치유형")     		, "22", "22", "0", "0", "100"}
			        			,{MessageResolver.getText("취소코드")     		, "23", "23", "0", "0", "100"}
			        			,{MessageResolver.getText("취소사유")     		, "24", "24", "0", "0", "100"}
			        			,{MessageResolver.getText("수정일자")     		, "25", "25", "0", "0", "100"}
			        			,{MessageResolver.getText("문자발송일자")     	, "26", "26", "0", "0", "100"}
			        			,{MessageResolver.getText("등록구분")     		, "27", "27", "0", "0", "100"}
			        			,{MessageResolver.getText("배송상태")     		, "28", "28", "0", "0", "100"}
			                  };
			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {
								 {"TRUST_CUST_NM"		, "S"}
								,{"VIEW_DLV_CUSTOMER_NM", "S"}
								,{"ASCS_SEQ"			, "S"}
								,{"ETC_YN"				, "S"}
								,{"MSG_SND_YN"			, "S"}
								,{"HC_CONNECT_USER"		, "S"}
								,{"HC_REG_NAME"			, "S"}
								,{"COM_TYPE_NM"			, "S"}
								,{"ORG_ORD_ID"			, "S"}
								,{"BUYED_DT"			, "S"}
								,{"SALES_COMPANY_NM"	, "S"}
								,{"DLV_CUSTOMER_NM"		, "S"}
								,{"DLV_PHONE_1"			, "S"}
								,{"DLV_PHONE_2"			, "S"}
								,{"DLV_ADDR"			, "S"}
								,{"DLV_PRODUCT_NM"		, "S"}
								,{"DLV_PRODUCT_CD"		, "S"}
								,{"DLV_QTY"				, "S"}
								,{"ETC1"				, "S"}
								,{"HC_REG_CONTENTS"		, "S"}
								,{"HC_RESULT"			, "S"}
								,{"DLV_COMP_NM"			, "S"}
								,{"DLV_SET_TYPE_NM"		, "S"}
								,{"HC_CANCEL_CODE_NM"	, "S"}
								,{"HC_CANCEL_REASON"	, "S"}
								,{"HC_UPD_DT"			, "S"}
								,{"MSG_SND_DT"			, "S"}
								,{"TRN_DLV_ORD_TYPE"	, "S"}
								,{"TRN_DLV_ORD_STAT"	, "S"}
			                   }; 

			// 파일명
			String fileName = MessageResolver.getText("해피콜주문관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*
	 * Method ID    : listt1
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP050T1/list.action")
	public ModelAndView listT1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listT1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSSP050T1.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP050T1.action")
	public ModelAndView WMSSP050T1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSSP050T1", service.selectBoxT1(model));
	}
	
	/*-
	 * Method ID    : saveE3
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP050T1/save.action")
	public ModelAndView saveE3(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
