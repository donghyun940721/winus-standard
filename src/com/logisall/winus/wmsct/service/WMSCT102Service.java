package com.logisall.winus.wmsct.service;

import java.util.Map;

public interface WMSCT102Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
}
