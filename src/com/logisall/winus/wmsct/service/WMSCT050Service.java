package com.logisall.winus.wmsct.service;

import java.util.List;
import java.util.Map;

public interface WMSCT050Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
}