package com.logisall.winus.wmsct.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSSP060Dao")
public class WMSSP060Dao extends SqlMapAbstractDAO {
	
	/**
    * Method ID  : selectDrivers
    * Method 설명  : 기사정보 데이터셋
    * 작성자             : 기드온
    * @param model
    * @return
    */
    public Object selectDrivers(Map<String, Object> model){
    	return executeQueryForList("wmssp060.selectDrivers", model);
    }
	
	/**
	 * Method ID	: pickingList1 
	 * Method 설명	: 기사피킹리스트 
	 * 작성자			:
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet pickingList1(Map<String, Object> model) {
		return executeQueryPageWq("wmssp060.pickingList1", model);
	}
	
	/**
	 * Method ID	: pickingList2 
	 * Method 설명	: 기사피킹리스트 
	 * 작성자			:
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet pickingList2(Map<String, Object> model) {
		return executeQueryPageWq("wmssp060.pickingList2", model);
	}
}
