package com.logisall.winus.wmsct.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSSP050Dao")
public class WMSSP050Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : list 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmssp050.list", model);
	}
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }    
	
	/**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID  : selectDrivers
     * Method 설명  : 기사정보 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectAsCsLc(Map<String, Object> model){
     	return executeQueryForList("wmsct020.selectAsCsLc", model);
     }
	 /**
      * Method ID  : selectDrivers
      * Method 설명  : 기사정보 데이터셋
      * 작성자             : 기드온
      * @param model
      * @return
      */
      public Object selectDrivers(Map<String, Object> model){
      	return executeQueryForList("wmssp010.selectDrivers", model);
      }
	  /**
       * Method ID  : selectAsOrdItem
       * Method 설명  : 기사정보 데이터셋
       * 작성자             : 기드온
       * @param model
       * @return
       */
       public Object selectAsOrdItem(Map<String, Object> model){
       	return executeQueryForList("wmsct020.selectAsOrdItem", model);
       }
	/**
     * Method ID  : selectAsErr
     * Method 설명  : 기사정보 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectAsErr(Map<String, Object> model){
     	return executeQueryForList("wmsct020.selectAsErr", model);
     }
	 /**
     * Method ID  : selectDlvCust
     * Method 설명  : 배송처조회
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectDlvCust(Map<String, Object> model){
     	return executeQueryForList("wmssp050t1.selectDlvCust", model);
     }
     
     /**
 	 * Method ID : listT1
 	 * Method 설명 :  
 	 * 작성자 : chsong
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public GenericResultSet listT1(Map<String, Object> model) {
 		return executeQueryPageWq("wmssp050t1.listT1", model);
 	}
 	
    /**
   	 * Method ID : insert 
   	 * Method 설명 :
   	 * 작성자 : chsong
   	 * 
   	 * @param model
   	 * @return
   	 */
   	public Object insert(Map<String, Object> model) {
   		executeUpdate("wmssp050t1.toUpdateHcWMSCT010_connectUser", model);
   		return executeInsert("wmssp050t1.insert", model);
   	}
   	
   	/**
	 * Method ID : update 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		executeUpdate("wmssp050t1.toUpdateHcWMSCT010_connectUser", model);
		return executeUpdate("wmssp050t1.update", model);
	}
	
	/**
	 * Method ID : toUpdateHc WMSCT010, WMSSP010
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object toUpdateHcWMSCT010(Map<String, Object> model) {
		return executeUpdate("wmssp050t1.toUpdateHcWMSCT010", model);
	}
	public Object toUpdateHcWMSSP010(Map<String, Object> model) {
		return executeUpdate("wmssp050t1.toUpdateHcWMSSP010", model);
	}
}
