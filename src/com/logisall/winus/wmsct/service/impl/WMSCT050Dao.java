package com.logisall.winus.wmsct.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Repository("WMSCT050Dao")
public class WMSCT050Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
    * Method ID    : list
    * Method 설명      : 해피콜주문관리 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsct050.list", model);
    }
    
    /**
	 * Method ID : save 
	 * Method 설명 : 해피콜주문관리 저장
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object save(Map<String, Object> model) {
		return executeUpdate("wmssp050t1.toUpdateHcWMSCT010_connectUser", model);
	}
}
