package com.logisall.winus.wmsct.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSSP040Dao")
public class WMSSP040Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : list 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmssp010.list", model);
	}
	
	/**
    * Method ID  : selectDrivers
    * Method 설명  : 기사정보 데이터셋
    * 작성자             : 기드온
    * @param model
    * @return
    */
    public Object selectDrivers(Map<String, Object> model){
    	return executeQueryForList("wmssp010.selectDrivers", model);
    }
}
