package com.logisall.winus.wmsct.service;

import java.util.List;
import java.util.Map;


public interface WMSSP060Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickingList1(Map<String, Object> model) throws Exception;
    public Map<String, Object> pickingList2(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelPickingPop1(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelPickingPop2(Map<String, Object> model) throws Exception;
}
