package com.logisall.winus.wmsdv.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.wmsdv.service.WMSDV030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSDV030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSDV030Service")
	private WMSDV030Service service;

	/**
     * Method ID	: WMSDV033
     * Method 설명	: 온습도관리페이지 접속
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSDV033.action")
    public ModelAndView WMSDV033(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsms/WMSDV033");
    }
    
	@RequestMapping("/WMSDV033UPLOADPOP.action")
	public ModelAndView WMSDV033UPLOADPOP(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSDV033UPLOADPOP");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 템플릿관리 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDV030/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	
	@RequestMapping("/WMSDV030/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSDV030/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID	: WMSDV033.list2
     * Method 설명	: 온습도관리 조회
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSDV030/list2.action")
	public ModelAndView list2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID	: WMSDV033.save2
     * Method 설명	: 온습도관리 저장
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSDV030/save2.action")
	public ModelAndView save2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.save2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	 * Method ID	: WMSDV033.deleteDel
     * Method 설명	: 온습도관리 삭제 del_yn = 'y'
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/WMSDV030/deleteDel.action")
	public ModelAndView deleteDel(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.deleteDel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSDV030/update.action")
	public ModelAndView update(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.update(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	static final String[] COLUMN_NAME_WMSDV030 = {
		"WORK_DT","WORK_TIME","DEV_NUM","PORT_NUM","TEMP_VAL","HUMID_VAL"
	};
	
	@RequestMapping("/WMSDV030/excelUpload.action")
	public ModelAndView excelUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSDV030, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
//			mapBody.put("chkDuplicationYn" ,model.get("chkDuplicationYn"));
			mapBody.put("vrCustId"		   ,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"		   ,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	   ,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	   ,model.get("SS_USER_NO"));
			
			m = service.saveExcelUpload(mapBody, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
}
	
	
