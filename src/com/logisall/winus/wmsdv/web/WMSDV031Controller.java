package com.logisall.winus.wmsdv.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.common.service.CmBeanService;
import com.logisall.winus.wmsdv.service.WMSDV031Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSDV031Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSDV031Service")
	private WMSDV031Service service;
	
	@Resource(name = "CmBeanService")
	private CmBeanService serviceCM;

    @RequestMapping("/WINUS/WMSDV031.action")
    public ModelAndView WMSDV031(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsms/WMSDV031");
    }
    
	@RequestMapping("/WMSDV031UPLOADPOP.action")
	public ModelAndView WMSDV031UPLOADPOP(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSDV031UPLOADPOP");
	}

	@RequestMapping("/WMSDV031/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSDV031/listDetail.action")
	public ModelAndView listDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSDV031/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSDV031/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}	
	
	@RequestMapping("/WMSDV031/saveDetail.action")
	public ModelAndView saveDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveDetail(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSDV031/deleteDetail.action")
	public ModelAndView deleteDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.deleteDetail(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}	

	static final String[] COLUMN_NAME_WMSDV031 = {
		"WH_ID","WH_NM","ADAPTER_NUM","ADAPTER_NAME","SERIAL","DEVICE_LC","USE_YN","COMMENTS","D_PORT_NUM","D_SERIAL","D_DEVICE_LC","D_USE_YN","D_COMMENTS"
	};
	
	@RequestMapping("/WMSDV031/excelUpload.action")
	public ModelAndView excelUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSDV031, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("chkDuplicationYn" ,model.get("chkDuplicationYn"));
			mapBody.put("vrCustId"		   ,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"		   ,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	   ,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	   ,model.get("SS_USER_NO"));
			
			m = service.saveExcelUpload(mapBody, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
			//mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	@RequestMapping("/WMSDV031/selectWhList.action")
	public ModelAndView selectWhList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			Map<String, Object> map = serviceCM.selectMngCode(model);
			
			List<Map> rs = ( ArrayList<Map> ) map.get("WH");
			
			int rsSize = rs.size();
			String returnStr = "";
			for(int i = 0; i < rsSize; i++){
				// 2023-10-11 / ksg / 아래 형식대로 데이터를 만들어 스트링으로 return 해야 한다. 
				Map<String, Object> work = rs.get(i);
				if(i > 0) returnStr = returnStr + ";";
				returnStr = returnStr + work.get("ID") + ":" + work.get("NAME") + ":" + work.get("CODE");
			}
			
			map.put("WH", returnStr);
			mav = new ModelAndView("jsonView", map);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
	
	
