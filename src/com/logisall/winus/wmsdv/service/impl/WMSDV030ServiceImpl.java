package com.logisall.winus.wmsdv.service.impl;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsdv.service.WMSDV030Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSDV030Service")
public class WMSDV030ServiceImpl extends AbstractServiceImpl implements WMSDV030Service {
	protected Log log = LogFactory.getLog(this.getClass());
    @Resource(name = "WMSDV030Dao")
    private WMSDV030Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 템플릿관리 조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        return map;
    }    
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
             Map<String, Object> modelIns = new HashMap<String, Object>();

             // session 및 필요정보
             modelIns.put("workDt"      	   	, model.get("WORK_DT")  );
             modelIns.put("workSeq"   		   	, model.get("WORK_SEQ"));
             modelIns.put("devNum"  			, model.get("DEV_NUM")   );
             modelIns.put("tempVal"   			, model.get("TEMP_VAL")   );
             modelIns.put("humidVal"   			, model.get("HUMID_VAL")   );
             modelIns.put("regNo"   			, model.get("REG_NO")   );
             modelIns.put("updDt"   			, new Timestamp(System.currentTimeMillis())   );             
             
             //조회 후, 있으면 저장 없으면 수정
             
             if(model.get("SAVE_FLAG").equals("U"))
             {
            	 dao.update(modelIns);
            	 m.put("errCnt", 0);
             }
             else
             {
            	 //조회 후, 데이터가 이미 존재하면 error return(key : WORK_DT, WORK_SEQ)            	 
            	 modelIns.put("regDt"   		, new Timestamp(System.currentTimeMillis())   );    
            	 dao.insert(modelIns);
            	 m.put("errCnt", 0);
            	
             }
            
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
             
         }

         return m;
    }
    
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
   	 Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();

            // session 및 필요정보
            modelIns.put("workDt"         , model.get("WORK_DT")  );
            modelIns.put("workSeq"   ,		 model.get("WORK_SEQ"));
            
           	 dao.delete(modelIns);
           	 
            m.put("errCnt", 0);
            
        } catch(Exception ex) {
            m.put("errCnt", 1);
            m.put("MSG", ex.getMessage() );
            
        }

        return m;
   }
    
	/**
     * Method ID	: WMSDV033.list2
     * Method 설명	: 온습도관리 조회
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @Override
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list2(model));
        return map;
    }
    
	/**
     * Method ID	: WMSDV033.save2
     * Method 설명	: 온습도관리 저장
     * 작성자			: SUMMER
     * @param   model
     * @return  
     * @throws Exception 
     */
    @Override
    public Map<String, Object> save2(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
             Map<String, Object> modelIns = new HashMap<String, Object>();

             // session 및 필요정보
             modelIns.put("workDt"      	   	, model.get("WORK_DT"));
             modelIns.put("devNum"  			, model.get("DEV_NUM"));
             modelIns.put("portNum"  			, model.get("PORT_NUM"));
             modelIns.put("tempVal"   			, model.get("TEMP_VAL"));
             modelIns.put("workTime"   			, model.get("WORK_TIME"));
             modelIns.put("humidVal"   			, model.get("HUMID_VAL"));
             modelIns.put("custId"   			, model.get("CUST_ID"));
             modelIns.put("saveType"   			, model.get("SAVETYPE"));
             modelIns.put("regNo"   			, (String)model.get(ConstantIF.SS_USER_NO));
             modelIns.put("lcId"   				, (String)model.get(ConstantIF.SS_SVC_NO));
                      	 
            dao.insert2(modelIns);
            m.put("errCnt", 0);
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }
   
    /**
     * Method ID	: WMSDV033.deleteDel
     * Method 설명	: 온습도관리 삭제
     * 작성자			: SUMMER
     * @param   model
     * @return  
     * @throws Exception 
     */
    @Override
    public Map<String, Object> deleteDel(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 
        	 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){

                 for(int i = 0 ; i < tmpCnt ; i ++){
                	 Map<String, Object> modelIns = new HashMap<String, Object>();
                	 
                	 modelIns.put("workDt", (String)model.get("workDt"+i));
                     modelIns.put("workSeq",(String)model.get("workSeq"+i));
                     modelIns.put("lcId", 	(String)model.get(ConstantIF.SS_SVC_NO));
                     modelIns.put("custId",	(String)model.get("custId"));
                     modelIns.put("updNo", 	(String)model.get(ConstantIF.SS_USER_NO));
                     
                     dao.deleteDel(modelIns);
                     m.put("errCnt", 0);
                 }
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }
    
    @Override
    public Map<String, Object> update(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 
        	 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){

                 for(int i = 0 ; i < tmpCnt ; i ++){
                	 Map<String, Object> modelIns = new HashMap<String, Object>();
                	 
                	 modelIns.put("workDt", 	(String)model.get("workDt"+i));
                     modelIns.put("workSeq", 	(String)model.get("workSeq"+i));
                     modelIns.put("tempVal",	(String)model.get("tempVal"+i));
                     modelIns.put("humidVal",	(String)model.get("humidVal"+i));
                     modelIns.put("lcId", 		(String)model.get(ConstantIF.SS_SVC_NO));
                     modelIns.put("custId", 	(String)model.get("custId"));
                     modelIns.put("updNo", 		(String)model.get(ConstantIF.SS_USER_NO));
                     
                     dao.updateTempHumidVal(modelIns);
                     m.put("errCnt", 0);
                 }
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }

    @Override
    public Map<String, Object> saveExcelUpload(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int workRow = 0;
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
    	try{
        	for(int i=0; i<insertCnt; i++){
        		Map<String, Object> temp = (Map)list.get(i);
        		
            	Map<String, Object> workMap = new HashMap<String, Object>();
            	workMap.put("lcId"		, (String)model.get(ConstantIF.SS_SVC_NO));
            	workMap.put("custId"	, (String)model.get("vrCustId"));
            	workMap.put("workDt"	, (String)temp.get("WORK_DT"));
            	workMap.put("devNum"	, (String)temp.get("DEV_NUM"));
            	workMap.put("portNum"	, (String)temp.get("PORT_NUM"));
            	workMap.put("workTime"	, (String)temp.get("WORK_TIME"));
            	workMap.put("tempVal"	, (String)temp.get("TEMP_VAL"));
            	workMap.put("humidVal"	, (String)temp.get("HUMID_VAL"));
            	workMap.put("regNo"		, (String)model.get(ConstantIF.SS_USER_NO));
            	workMap.put("saveType"	, "E");
            	dao.insert2(workMap);

                workRow++;
        	}
        	m.put("errCnt", 0);
        	m.put("MSG_ORA", "/");
            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
        } catch(Exception e){
        	m.put("errCnt", 1);
        	m.put("MSG_ORA", "/");
            m.put("MSG", "I/F 처리오류. 중복데이터 발생! 엑셀 " + workRow + 1 + "번째 데이터 중복"); //+1 이유 : 1부터 시작
        }
    	return m;
    }
}
