package com.logisall.winus.wmsdv.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDV031Dao")
public class WMSDV031Dao extends SqlMapAbstractDAO {
	
	protected Log log = LogFactory.getLog(this.getClass());
    
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsdv031.list", model);  
    }
    
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsdv031.listDetail", model);  
    }
    
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsdv031.insert", model);
    }   
    
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsdv031.update", model);
    }   
    
    public Object delete(Map<String, Object> model) {
        return executeDelete("wmsdv031.delete", model);
    }  
    
    public Object insertDetail(Map<String, Object> model) {
        return executeInsert("wmsdv031.insertDetail", model);
    }   
    
    public Object updateDetail(Map<String, Object> model) {
        return executeUpdate("wmsdv031.updateDetail", model);
    }   
    
    public Object deleteDetail(Map<String, Object> model) {
        return executeDelete("wmsdv031.deleteDetail", model);
    }
    
    public String getWh_id(Map<String, Object> model) {
        return (String)executeView("wmsdv031.getWh_id", model);
    }    
    
    public Object saveExcelHeader(Map<String, Object> model) {
        return executeUpdate("wmsdv031.saveExcelHeader", model);
    }
    
    public Object saveExcelDetail(Map<String, Object> model) {
        return executeUpdate("wmsdv031.saveExcelDetail", model);
    }
}
