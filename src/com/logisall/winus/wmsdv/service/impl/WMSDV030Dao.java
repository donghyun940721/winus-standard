package com.logisall.winus.wmsdv.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDV030Dao")
public class WMSDV030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 템플릿관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsdv030.list", model);  
    }
    
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsdv030.update", model);
    }
    
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsdv030.insert", model);
    }
    
    public Object delete(Map<String, Object> model) {
        return executeDelete("wmsdv030.delete", model);
    }
    public int selectCnt(Map<String, Object> model){
    	int pageTotal = ((Integer) getSqlMapClientTemplate().queryForObject("wmsdv030.listCount", model)).intValue();
    	return pageTotal;
    }
    
    /**
     * Method ID	: WMSDV033.list2
     * Method 설명	: 온습도관리 조회
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    public GenericResultSet list2(Map<String, Object> model) {
        return executeQueryPageWq("wmsdv030.list2", model);  
    }
    
    /**
     * Method ID	: WMSDV033.insert2
     * Method 설명	: 온습도관리 삽입
     * 작성자			: SUMMER
     * @param   model
     * @return  
     * @throws Exception 
     */
    public Object insert2(Map<String, Object> model) {
        return executeInsert("wmsdv030.insert2", model);
    }
    
    /**
     * Method ID	: WMSDV033.deleteDel
     * Method 설명	: 온습도관리 삭제
     * 작성자			: SUMMER
     * @param   model
     * @return  
     * @throws Exception 
     */
    public Object deleteDel(Map<String, Object> model) {
        return executeDelete("wmsdv030.updateDel", model);
    }
    
    /**
     * Method ID	: WMSDV033.updateTempHumidVal
     * Method 설명	: 온습도관리 수정(온도, 습도)
     * 작성자			: KSG
     * @param   model
     * @return  
     * @throws Exception 
     */
    public Object updateTempHumidVal(Map<String, Object> model) {
        return executeUpdate("wmsdv030.updateTempHumidVal", model);
    }
}
