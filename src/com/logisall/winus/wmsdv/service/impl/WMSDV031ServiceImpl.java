package com.logisall.winus.wmsdv.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsdv.service.WMSDV031Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSDV031Service")
public class WMSDV031ServiceImpl extends AbstractServiceImpl implements WMSDV031Service {
	
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSDV031Dao")
    private WMSDV031Dao dao;

    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }
    
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageSize", "60000");
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){
                 for(int i = 0 ; i < tmpCnt ; i ++){
                	 Map<String, Object> modelIns = new HashMap<String, Object>();
                	 
                	 modelIns.put("CUST_ID", 	(String)model.get("CUST_ID"+i));
                     modelIns.put("WH_ID", 		(String)model.get("WH_ID"+i));
                     modelIns.put("ADAPTER_NUM",(String)model.get("ADAPTER_NUM"+i));
                     modelIns.put("ADAPTER_NAME",(String)model.get("ADAPTER_NAME"+i));
                	 modelIns.put("SERIAL", 	(String)model.get("SERIAL"+i));
                     modelIns.put("DEVICE_LC", 	(String)model.get("DEVICE_LC"+i));
                     modelIns.put("USE_YN", 	(String)model.get("USE_YN"+i));
                     modelIns.put("COMMENTS", 	(String)model.get("COMMENTS"+i));
                     modelIns.put("lcId", 		(String)model.get(ConstantIF.SS_SVC_NO));
                     modelIns.put("updNo",		(String)model.get(ConstantIF.SS_USER_NO));
                	 
                	 String colFlag = (String)model.get("SPDCOL_FLAG"+i);
                	 if(colFlag.equals("I")){ // 추가
                         dao.insert(modelIns);
                	 }else if(colFlag.equals("U")){ // 수정
                		 dao.update(modelIns);
                	 }
                 }
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }
    
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){

                 for(int i = 0 ; i < tmpCnt ; i ++){
                	 Map<String, Object> modelIns = new HashMap<String, Object>();
                	 
                     modelIns.put("ADAPTER_NUM",(String)model.get("ADAPTER_NUM"+i));
                     modelIns.put("lcId", 		(String)model.get(ConstantIF.SS_SVC_NO));
                     modelIns.put("updNo",		(String)model.get(ConstantIF.SS_USER_NO));
                     
                     dao.delete(modelIns);
                     m.put("errCnt", 0);
                 }
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }
    
    @Override
    public Map<String, Object> saveDetail(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){
                 for(int i = 0 ; i < tmpCnt ; i ++){
                	 Map<String, Object> modelIns = new HashMap<String, Object>();
                	 
                     modelIns.put("ADAPTER_NUM",(String)model.get("D_ADAPTER_NUM"+i));
                     modelIns.put("PORT_NUM",	(String)model.get("D_PORT_NUM"+i));
                	 modelIns.put("SERIAL", 	(String)model.get("D_SERIAL"+i));
                     modelIns.put("DEVICE_LC", 	(String)model.get("D_DEVICE_LC"+i));
                     modelIns.put("USE_YN", 	(String)model.get("D_USE_YN"+i));
                     modelIns.put("COMMENTS", 	(String)model.get("D_COMMENTS"+i));
                     modelIns.put("lcId", 		(String)model.get(ConstantIF.SS_SVC_NO));
                     modelIns.put("updNo",		(String)model.get(ConstantIF.SS_USER_NO));
                	 
                	 String colFlag = (String)model.get("SPDCOL_D_FLAG"+i);
                	 if(colFlag.equals("I")){ // 추가
                         dao.insertDetail(modelIns);
                	 }else if(colFlag.equals("U")){ // 수정
                		 dao.updateDetail(modelIns);
                	 }
                 }
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }
    
    @Override
    public Map<String, Object> deleteDetail(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try {
        	 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){

                 for(int i = 0 ; i < tmpCnt ; i ++){
                	 Map<String, Object> modelIns = new HashMap<String, Object>();
                	 
                     modelIns.put("ADAPTER_NUM",(String)model.get("D_ADAPTER_NUM"+i));
                     modelIns.put("lcId", 		(String)model.get(ConstantIF.SS_SVC_NO));
                     modelIns.put("updNo",		(String)model.get(ConstantIF.SS_USER_NO));
                     
                     dao.deleteDetail(modelIns);
                     m.put("errCnt", 0);
                 }
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("delete.success"));          	 
            
         } catch(Exception ex) {
             m.put("errCnt", 1);
             m.put("MSG", ex.getMessage() );
         }
         return m;
    }

    public Map<String, Object> saveExcelUpload(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int workRow = 0;
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
    	try{
    		Map<String, Object> beforeHeaderMap = new HashMap<String, Object>();
        	for(int i=0; i<insertCnt; i++){
        		Map<String, Object> temp = (Map)list.get(i);
        		
            	Map<String, Object> headerMap = new HashMap<String, Object>();
            	headerMap.put("lcId"		, (String)model.get(ConstantIF.SS_SVC_NO));                  
            	headerMap.put("CUST_ID"		, (String)model.get("vrCustId"));
            	headerMap.put("updNo"		, (String)model.get(ConstantIF.SS_USER_NO));
            	headerMap.put("ADAPTER_NUM"	, (String)temp.get("ADAPTER_NUM"));
            	headerMap.put("ADAPTER_NAME", (String)temp.get("ADAPTER_NAME"));
            	headerMap.put("SERIAL"		, (String)temp.get("SERIAL"));
            	headerMap.put("DEVICE_LC"	, (String)temp.get("DEVICE_LC"));
            	headerMap.put("USE_YN"		, (String)temp.get("USE_YN"));
            	headerMap.put("COMMENTS"	, (String)temp.get("COMMENTS"));
            	
            	Map<String, Object> detailMap = new HashMap<String, Object>();
            	detailMap.put("lcId"		, (String)model.get(ConstantIF.SS_SVC_NO));                  
            	detailMap.put("updNo"		, (String)model.get(ConstantIF.SS_USER_NO));
            	detailMap.put("ADAPTER_NUM"	, (String)temp.get("ADAPTER_NUM"));
            	detailMap.put("PORT_NUM"	, (String)temp.get("D_PORT_NUM"));
            	detailMap.put("SERIAL"		, (String)temp.get("D_SERIAL"));
            	detailMap.put("DEVICE_LC"	, (String)temp.get("D_DEVICE_LC"));
            	detailMap.put("USE_YN"		, (String)temp.get("D_USE_YN"));
            	detailMap.put("COMMENTS"	, (String)temp.get("D_COMMENTS"));
        		
        		if(i == 0){
            		String wh_id = (String)temp.get("WH_ID");
            		if(wh_id.equals("") || wh_id.isEmpty()){
            			Map<String, Object> workMap = new HashMap<String, Object>();
            			workMap.put("wh_nm", (String)temp.get("WH_NM"));
            			workMap.put("lc_id", (String)model.get(ConstantIF.SS_SVC_NO));
            			wh_id = dao.getWh_id(workMap);
            		}
        			headerMap.put("WH_ID", wh_id);
        			
        			beforeHeaderMap = headerMap;
                	dao.saveExcelHeader(headerMap);
        		}else{
        			String beforeAdapterNum = (String) beforeHeaderMap.get("ADAPTER_NUM");
        			if(!beforeAdapterNum.equals((String)temp.get("ADAPTER_NUM"))){
                		String wh_id = (String)temp.get("WH_ID");
                		if(wh_id.equals("") || wh_id.isEmpty()){
                			Map<String, Object> workMap = new HashMap<String, Object>();
                			workMap.put("wh_nm", (String)temp.get("WH_NM"));
                			workMap.put("lc_id", (String)model.get(ConstantIF.SS_SVC_NO));
                			wh_id = dao.getWh_id(workMap);
                		}
            			headerMap.put("WH_ID", wh_id);
        				
        				dao.saveExcelHeader(headerMap);
        				beforeHeaderMap = headerMap;
        			}
        		}
        		dao.saveExcelDetail(detailMap);  		
                workRow++;
        	}
        	m.put("errCnt", 0);
        	m.put("MSG_ORA", "/");
            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
        } catch(Exception e){
        	m.put("errCnt", 1);
        	m.put("MSG_ORA", "/");
            m.put("MSG", "I/F 처리오류. 중복데이터 발생! 엑셀 " + workRow + 1 + "번째 데이터 중복"); //+1 이유 : 1부터 시작
        }
    	return m;
    }
}
