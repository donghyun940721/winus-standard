package com.logisall.winus.wmsdv.service;

import java.util.List;
import java.util.Map;

public interface WMSDV030Service {	
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> save2(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteDel(Map<String, Object> model) throws Exception;
    public Map<String, Object> update(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveExcelUpload(Map<String, Object> model, List list) throws Exception;
}
