package com.logisall.winus.wmsdv.service;

import java.util.List;
import java.util.Map;

public interface WMSDV031Service {	
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteDetail(Map<String, Object> model) throws Exception;

    public Map<String, Object> saveExcelUpload(Map<String, Object> model, List list) throws Exception;
}
