package com.logisall.winus.test.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.logisall.winus.test.service.TestService;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;


@Controller
@RequestMapping("/test")
public class TestController {
  private final Log log = LogFactory.getLog(this.getClass());

  @Autowired
  private TestService testService;
  
  /*
   * 브라우저에서 입력하면 velocity를 통하여 document로 호출되기 떄문에 
   * fetch Api 나 포스트맨을 통하여 테스트 바랍니다.
   * */
  @RequestMapping("/selectWinusTest")
  @ResponseBody
  public ResponseEntity<ModelAndView> selectWinusTest(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
      m = testService.selectWinusTest(model, reqMap);
    } catch (Exception e) {
        m.put("MSG", MessageResolver.getMessage("save.error"));
        return new ResponseEntity<ModelAndView>(mav, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    mav.addAllObjects(m);
    return new ResponseEntity<ModelAndView>(mav, HttpStatus.OK);
  }
  
  
  @RequestMapping("/selectWinusIfTest")
  @ResponseBody
  public ResponseEntity<Map<String, Object>> selectWinustIfTest(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    Map<String, Object> m = new HashMap<String, Object>();
    try {
      m = testService.selectWinusIfTest(reqMap);
    } catch (Exception e) {
        m.put("MSG", MessageResolver.getMessage("save.error"));
        return new ResponseEntity<Map<String, Object>>(m, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<Map<String, Object>>(m, HttpStatus.OK);
  }
  
  @RequestMapping("/selectWinusIf2Test")
  @ResponseBody
  public ResponseEntity<Map<String, Object>> selectWinustIf2Test(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    Map<String, Object> m = new HashMap<String, Object>();
    try {
      m = testService.selectWinusIf2Test(reqMap);
    } catch (Exception e) {
        m.put("MSG", MessageResolver.getMessage("save.error"));
        return new ResponseEntity<Map<String, Object>>(m, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<Map<String, Object>>(m, HttpStatus.OK);
  }
}
