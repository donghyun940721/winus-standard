package com.logisall.winus.test.service;

import java.sql.SQLException;
import java.util.Map;

public interface TestService {

  Map<String, Object> selectWinusTest(Map<String, Object> model, Map<String, Object> reqMap) throws SQLException;

  Map<String, Object> selectWinusIfTest(Map<String, Object> reqMap) throws SQLException;

  Map<String, Object> selectWinusIf2Test(Map<String, Object> reqMap) throws SQLException;
}
