package com.logisall.winus.test.service;

import java.sql.SQLException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.logisall.winus.test.dao.TestDao;

@Service
public class TestServiceImpl implements TestService {

  @Autowired
  private TestDao testDao;
  
  @Override
  public Map<String, Object> selectWinusTest(Map<String, Object> model,
      Map<String, Object> reqMap) throws SQLException {
    reqMap.put("COL_1", "0000875497");
    Map<String, Object> resultMap = testDao.selectWinusTest(reqMap);
    return resultMap;
  }
  @Override
  public Map<String, Object> selectWinusIfTest(Map<String, Object> reqMap) throws SQLException{
    reqMap.put("IF_DOGO_MGMT_NO", "0000040506");
    Map<String, Object> resultMap = testDao.selectWinusIfTest(reqMap);
    return resultMap; 
  }
  @Override
  public Map<String, Object> selectWinusIf2Test(Map<String, Object> reqMap) throws SQLException{
    reqMap.put("NAME", "테스트");
    Map<String, Object> resultMap = testDao.selectWinusIf2Test(reqMap);
    return resultMap; 
  }
}
