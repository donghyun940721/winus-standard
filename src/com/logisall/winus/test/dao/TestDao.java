package com.logisall.winus.test.dao;

import java.sql.SQLException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
public class TestDao extends SqlMapAbstractDAO {
  
  //WINUSUSR
  @Autowired
  private SqlMapClient sqlMapClient;
  
  //WINUSUSR_IF
  @Autowired
  private SqlMapClient sqlMapClientWinusIf;
  
  //10.30.2.60.WINUSUSR(신규 인터페이스 DB)
  @Autowired
  @Qualifier("sqlMapClientWinusIf2")
  private SqlMapClient sqlMapClientWinusIf2;
  

  public Map<String, Object> selectWinusTest(Map<String, Object> reqMap) throws SQLException {
    /*
     * 리스트는 quertForList 호출하면 됨
     * List<Map<String, Object>> queryList = sqlMapClient.queryForList("test.selectWinusTest", reqMap);
     * 
     * */
    return (Map<String, Object>) sqlMapClient.queryForObject("test.selectWinusTest", reqMap);
  }
  
  public Map<String, Object> selectWinusIfTest(Map<String, Object> reqMap) throws SQLException{
    return (Map<String, Object>) sqlMapClientWinusIf.queryForObject("test.selectWinusIfTest", reqMap);
  }
  
  public Map<String, Object> selectWinusIf2Test(Map<String, Object> reqMap) throws SQLException {
    return (Map<String, Object>) sqlMapClientWinusIf2.queryForObject("test.selectWinusIf2Test", reqMap);
  }
  
}
