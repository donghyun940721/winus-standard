package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

import com.m2m.jdfw5x.egov.database.GenericResultSet;

public interface WMSOP920Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4Header(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4Detail(Map<String, Object> model) throws Exception;
    
    
    public Map<String, Object> listE1SummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2SummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3SummaryCount(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception;
    
    public Map<String, Object> outInvalidView(Map<String, Object> model) throws Exception;
    
}
