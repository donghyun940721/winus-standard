package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP520Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
//	public Map<String, Object> selectItemGrp98(Map<String, Object> model) throws Exception;
//	public Map<String, Object> selectItemGrp99(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE01(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE02(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub_T1(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> list01Excel(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveInsQtyStep(Map<String, Object> model) throws Exception;	
    public Map<String, Object> saveInsQty(Map<String, Object> model) throws Exception;	
}
