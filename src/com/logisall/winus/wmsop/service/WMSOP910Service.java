package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP910Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE1ExcelByTurkiye(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4Header(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4Detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE5Header(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE5Detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception;
    
    
    public Map<String, Object> listE1SummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2SummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3SummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE6SummaryCount(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> sendMfcOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> dasMake(Map<String, Object> model) throws Exception;
    public Map<String, Object> createDasDegreeFromOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> sendDasData(Map<String, Object> model) throws Exception;
    public Map<String, Object> dasDelete(Map<String, Object> model) throws Exception;
    public Map<String, Object> dasIfSend(Map<String, Object> model) throws Exception;
    public Map<String, Object> dasIfSendB2C(Map<String, Object> model) throws Exception;
	public Map<String, Object> dasIfSendNoInvcB2C(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> getDevOrdDegree(Map<String, Object> model) throws Exception;
	public Map<String, Object> makeDegreeB2C(Map<String, Object> model) throws Exception;
	public Map<String, Object> deleteDegree(Map<String, Object> model) throws Exception;
	public Map<String, Object> dasMakeOrderDegree(Map<String, Object> model) throws Exception;
}
