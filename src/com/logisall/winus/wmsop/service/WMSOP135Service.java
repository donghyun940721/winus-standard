package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

import com.logisall.winus.wmsop.vo.WMSOP135VO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;


public interface WMSOP135Service {
	
	
	public WMSOP135VO getOrdSeqInfo(Map<String, Object> model) throws Exception;
	
	public List<WMSOP135VO> getOrdSeqList(Map<String, Object> model) throws Exception;
	
	public List<WMSOP135VO> getOrdList(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> OrdSeqInfoTraceEAICall(Map<String, Object> model, WMSOP135VO vo) throws Exception;
	
	public Map<String, Object> OrdSeqListTraceEAICall(Map<String, Object> model, List<WMSOP135VO> list ) throws Exception;
	
	public Map<String, Object> updateOrdDesc(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> autoBestLocSaveMultiQtyList(Map<String, Object> model) throws Exception;

	public Map<String, Object> deliveryComplete(Map<String, Object> model) throws Exception;

	     
}
