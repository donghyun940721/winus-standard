package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

public interface WMSOP640Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvSummary(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvSummaryDiv(Map<String, Object> model) throws Exception;
	public Map<String, Object> list2ByDlvHistory(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvShipment(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> dlvSenderInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> boxNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> invcNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvSummaryExcel(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvShipDelete(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> custInfoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvIFExcel(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvIFExcelV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> deviceIfSend(Map<String, Object> model) throws Exception;
	public Map<String, Object> deviceIfListSearch(Map<String, Object> model) throws Exception;
	public Map<String, Object> getDasOrdDateByDegree(Map<String, Object> model) throws Exception;
	public Map<String, Object> dashBoard(Map<String, Object> model) throws Exception;
    public Map<String, Object> deviceIfSendV2(Map<String, Object> model) throws Exception;
}