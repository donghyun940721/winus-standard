package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.sql.*;
import sun.misc.*;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP010Service")
public class WMSOP010ServiceImpl implements WMSOP010Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP010Dao")
    private WMSOP010Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 입출고현황  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listSub
     * 대체 Method 설명    : 입출고현황 상세 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listSub(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveApprove
     * 대체 Method 설명    : 입출고현황 승인, 불가
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveApprove(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
            int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
                String[] ordId = new String[temCnt];
                for(int i = 0 ; i < temCnt ; i ++){
                    ordId[i] = (String)model.get("ORD_ID"+i);
                }       
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ORD_ID", ordId); 
                modelDt.put("vrType", (String)model.get("vrType"));
                modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt = (Map<String, Object>)dao.saveApprove(modelDt);
                ServiceUtil.isValidReturnCode("WMSOP010", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
            }   
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 입출고현황 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listExcel(model));
        return map;
    }  
    
    /**
     * 
     * 대체 Method ID   : saveConfirmQc
     * 대체 Method 설명    : QC확정
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveConfirmQc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
                String[] ordId = new String[temCnt];
                for(int i = 0 ; i < temCnt ; i ++){
                    ordId[i] = (String)model.get("ORD_ID"+i);
                }       
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ORD_ID", ordId); 
                modelDt.put("vrType", (String)model.get("vrType"));
                modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt = (Map<String, Object>)dao.saveConfirmQc(modelDt);
                ServiceUtil.isValidReturnCode("WMSOP010", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
                
            }   
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            

        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : inOutOrderCntInit
     * Method �ㅻ�    : 
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> inOutOrderCntInit(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DS_CNT", dao.inOutOrderCntInit(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : custIfInOrd
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custIfInOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        // Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con  = null;
        Statement stmt  = null;
        Statement stmtp = null;
        ResultSet rs    = null;
        ResultSet rsp   = null;
        
        try{
        	//db connect
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
        	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        	
        	//mssql db select
        	rs = stmt.executeQuery( "SELECT IF_ID"
			        			 + "      , CONVERT(VARCHAR(50), IF_DT, 111) AS IF_DT"
			        			 + "      , MOVE_NO"
			        			 + "      , MATERIALLOTID"
			        			 + "      , MATERIALLOTQTY"
			        			 + "      , FINAL_LOT_NO"
			        			 + "      , IN_BCR_NO"
			        			 + "      , OUT_BCR_NO"
			        			 + "      , PLANT_CD"
			        			 + "      , SL_CD"
			        			 + "      , ITEM_CD"
			        			 + "      , MOVE_COUNT"
			        			 + "      , MOVE_QTY"
			        			 + "      , REMARK"
			        			 + "      , CUD_FLG"
			        			 + "      , BIZ_REC_FLG"
			        			 + "      , BIZ_REC_DT"
			        			 + "      , CONVERT(VARCHAR(50), PRODDATE01, 111) AS PRODDATE01"
			        			 + "      , CONVERT(VARCHAR(50), PRODDATE02, 111) AS PRODDATE02"
			        			 + "   FROM UT_MATERIAL_MOVE_IF"
			        			 + "  WHERE CUD_FLG     = 'C'"
			        			 + "    AND BIZ_REC_FLG = 'N'"
        			             + ";");
        	//mssql get rowcount
        	rs.last();
        	int rowcount = rs.getRow();
        	rs.beforeFirst();
        	
        	if(rowcount > 0){
        		while(rs.next()) {
    				Map<String, Object> dsSet = new HashMap<String, Object>();
    				dsSet.put("IF_ID"			, rs.getString("IF_ID"));
    				dsSet.put("IF_DT"			, rs.getString("IF_DT"));
    				dsSet.put("MOVE_NO"			, rs.getString("MOVE_NO"));
    				dsSet.put("MATERIALLOTID"	, rs.getString("MATERIALLOTID"));
    				dsSet.put("MATERIALLOTQTY"	, rs.getString("MATERIALLOTQTY"));
    				dsSet.put("FINAL_LOT_NO"	, rs.getString("FINAL_LOT_NO"));
    				dsSet.put("IN_BCR_NO"		, rs.getString("IN_BCR_NO"));
    				dsSet.put("OUT_BCR_NO"		, rs.getString("OUT_BCR_NO"));
    				dsSet.put("PLANT_CD"		, rs.getString("PLANT_CD"));
    				dsSet.put("SL_CD"			, rs.getString("SL_CD"));
    				dsSet.put("ITEM_CD"			, rs.getString("ITEM_CD"));
    				dsSet.put("MOVE_COUNT"		, rs.getString("MOVE_COUNT"));
    				dsSet.put("MOVE_QTY"		, rs.getString("MOVE_QTY"));
    				dsSet.put("REMARK"			, rs.getString("REMARK"));
    				dsSet.put("CUD_FLG"			, rs.getString("CUD_FLG"));
    				dsSet.put("BIZ_REC_FLG"		, rs.getString("BIZ_REC_FLG"));
    				dsSet.put("PRODDATE01"		, rs.getString("PRODDATE01"));
    				dsSet.put("PRODDATE02"		, rs.getString("PRODDATE02"));
    				
    				//to oracle insert
    				String fileUploadId = (String) dao.custIfInOrd(dsSet);
    				
    				//mssql this row update
    				stmtp = con.createStatement();
    				stmtp.executeUpdate(" UPDATE UT_MATERIAL_MOVE_IF"
			                          + "    SET BIZ_REC_FLG   = 'Y'"
			                          + "      , BIZ_REC_DT    = GETDATE()"
			                          + "  WHERE IF_ID         = "+"'"+ rs.getString("IF_ID") +"'"
			                          + "    AND MATERIALLOTID = "+"'"+ rs.getString("MATERIALLOTID") +"'"
			                          + "    AND CUD_FLG       = 'C'"
			                          + ";");
    				stmtp.close();
    				
            	    ServiceUtil.isValidReturnCode("WMSOP010", "00", "SUCCESS");
            	}
        	}
        	
        	stmt.close();
            con.close();

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	stmt.close();
        	con.rollback();
        	con.close();
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : custIfOutOrdCancel
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custIfOutOrdCancel(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        // Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con  = null;
        Statement stmt  = null;
        Statement stmtp = null;
        ResultSet rs    = null;
        ResultSet rsp   = null;
        
        try{
        	//db connect
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
        	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        	
        	//mssql db select
        	rs = stmt.executeQuery( "SELECT IF_ID"
			        			 + "      , CONVERT(VARCHAR(50), IF_DT, 111) AS IF_DT"
			        			 + "      , DN_REQ_NO"
			        			 + "      , DN_REQ_SEQ"
			        			 + "      , MATERIALLOTID"
			        			 + "      , MATERIALLOTQTY"
			        			 + "      , FINAL_LOT_NO"
			        			 + "      , IN_BCR_NO"
			        			 + "      , OUT_BCR_NO"
			        			 + "      , SHIPNO"
			        			 + "      , BP_CD"
			        			 + "      , BP_NAME"
			        			 + "      , PLANT_CD"
			        			 + "      , SL_CD"
			        			 + "      , ITEM_CD"
			        			 + "      , REQ_COUNT"
			        			 + "      , REQ_QTY"
			        			 + "      , REMARK"
			        			 + "      , CUD_FLG"
			        			 + "      , BIZ_REC_FLG"
			        			 + "      , BIZ_REC_DT"
			        			 + "      , BIZ_REC_ERROR"
			        			 + "      , INSRT_USER_ID"
			        			 + "      , INSRT_DT"
			        			 + "   FROM UT_MATERIAL_SHIP_IF"
			        			 + "  WHERE CUD_FLG     IN ('D')"
			        			 + "    AND BIZ_REC_FLG = 'N'"
        			             + ";");
        	//mssql get rowcount
        	rs.last();
        	int rowcount = rs.getRow();
        	rs.beforeFirst();
        	
        	if(rowcount > 0){
        		while(rs.next()) {
    				Map<String, Object> dsSet = new HashMap<String, Object>();
    				dsSet.put("IF_ID"			, rs.getString("IF_ID"));
    				dsSet.put("IF_DT"			, rs.getString("IF_DT"));
    				dsSet.put("DN_REQ_NO"		, rs.getString("DN_REQ_NO"));
    				dsSet.put("DN_REQ_SEQ"		, rs.getString("DN_REQ_SEQ"));
    				dsSet.put("MATERIALLOTID"	, rs.getString("MATERIALLOTID"));
    				dsSet.put("MATERIALLOTQTY"	, rs.getString("MATERIALLOTQTY"));
    				dsSet.put("FINAL_LOT_NO"	, rs.getString("FINAL_LOT_NO"));
    				dsSet.put("IN_BCR_NO"		, rs.getString("IN_BCR_NO"));
    				dsSet.put("OUT_BCR_NO"		, rs.getString("OUT_BCR_NO"));
    				dsSet.put("SHIPNO"			, rs.getString("SHIPNO"));
    				dsSet.put("BP_CD"			, rs.getString("BP_CD"));
    				dsSet.put("BP_NAME"			, rs.getString("BP_NAME"));
    				dsSet.put("PLANT_CD"		, rs.getString("PLANT_CD"));
    				dsSet.put("SL_CD"			, rs.getString("SL_CD"));
    				dsSet.put("ITEM_CD"			, rs.getString("ITEM_CD"));
    				dsSet.put("REQ_COUNT"		, rs.getString("REQ_COUNT"));
    				dsSet.put("REQ_QTY"			, rs.getString("REQ_QTY"));
    				dsSet.put("REMARK"			, rs.getString("REMARK"));
    				dsSet.put("CUD_FLG"			, rs.getString("CUD_FLG"));
    				dsSet.put("BIZ_REC_FLG"		, rs.getString("BIZ_REC_FLG"));
    				dsSet.put("BIZ_REC_DT"		, rs.getString("BIZ_REC_DT"));
    				dsSet.put("BIZ_REC_ERROR"	, rs.getString("BIZ_REC_ERROR"));
    				dsSet.put("INSRT_USER_ID"	, rs.getString("INSRT_USER_ID"));
    				dsSet.put("INSRT_DT"		, rs.getString("INSRT_DT"));
    				
    				//wmsif102 CUD_FLG = D, delete, WHERE DN_REQ_NO, SHIPNO, ORD_INSERT_YN
    				String checkExistData = dao.checkExistEmptyOrd(dsSet);
    				if (checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
    					dao.custIfOutOrdDelete(dsSet);
                    }
    				
    				//mssql this row update
    				stmtp = con.createStatement();
    				stmtp.executeUpdate(" UPDATE UT_MATERIAL_SHIP_IF"
	                          + "    SET BIZ_REC_FLG = 'Y'"
	                          + "      , BIZ_REC_DT  = GETDATE()"
	                          + "  WHERE IF_ID       = "+"'"+ rs.getString("IF_ID") +"'"
	                          + "    AND DN_REQ_NO   = "+"'"+ rs.getString("DN_REQ_NO") +"'"
	                          + "    AND SHIPNO      = "+"'"+ rs.getString("SHIPNO") +"'"
	                          + "    AND CUD_FLG     IN ('D')"
	                          + ";");
    				
            	    ServiceUtil.isValidReturnCode("WMSOP010", "00", "SUCCESS");
            	}
        	}
        	
        	stmt.close();
            con.close();

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	stmt.close();
        	con.rollback();
        	con.close();
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : custIfOutOrd
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custIfOutOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        // Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con  = null;
        Statement stmt  = null;
        Statement stmtp = null;
        ResultSet rs    = null;
        ResultSet rsp   = null;
        
        try{
        	//db connect
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
        	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        	
        	//mssql db select
        	rs = stmt.executeQuery( "SELECT IF_ID"
			        			 + "      , CONVERT(VARCHAR(50), IF_DT, 111) AS IF_DT"
			        			 + "      , DN_REQ_NO"
			        			 + "      , DN_REQ_SEQ"
			        			 + "      , MATERIALLOTID"
			        			 + "      , MATERIALLOTQTY"
			        			 + "      , FINAL_LOT_NO"
			        			 + "      , IN_BCR_NO"
			        			 + "      , OUT_BCR_NO"
			        			 + "      , SHIPNO"
			        			 + "      , BP_CD"
			        			 + "      , BP_NAME"
			        			 + "      , PLANT_CD"
			        			 + "      , SL_CD"
			        			 + "      , ITEM_CD"
			        			 + "      , REQ_COUNT"
			        			 + "      , REQ_QTY"
			        			 + "      , REMARK"
			        			 + "      , CUD_FLG"
			        			 + "      , BIZ_REC_FLG"
			        			 + "      , BIZ_REC_DT"
			        			 + "      , BIZ_REC_ERROR"
			        			 + "      , INSRT_USER_ID"
			        			 + "      , INSRT_DT"
			        			 + "   FROM UT_MATERIAL_SHIP_IF"
			        			 + "  WHERE CUD_FLG     IN ('C')"
			        			 + "    AND BIZ_REC_FLG = 'N'"
        			             + ";");
        	//mssql get rowcount
        	rs.last();
        	int rowcount = rs.getRow();
        	rs.beforeFirst();
        	
        	if(rowcount > 0){
        		while(rs.next()) {
    				Map<String, Object> dsSet = new HashMap<String, Object>();
    				dsSet.put("IF_ID"			, rs.getString("IF_ID"));
    				dsSet.put("IF_DT"			, rs.getString("IF_DT"));
    				dsSet.put("DN_REQ_NO"		, rs.getString("DN_REQ_NO"));
    				dsSet.put("DN_REQ_SEQ"		, rs.getString("DN_REQ_SEQ"));
    				dsSet.put("MATERIALLOTID"	, rs.getString("MATERIALLOTID"));
    				dsSet.put("MATERIALLOTQTY"	, rs.getString("MATERIALLOTQTY"));
    				dsSet.put("FINAL_LOT_NO"	, rs.getString("FINAL_LOT_NO"));
    				dsSet.put("IN_BCR_NO"		, rs.getString("IN_BCR_NO"));
    				dsSet.put("OUT_BCR_NO"		, rs.getString("OUT_BCR_NO"));
    				dsSet.put("SHIPNO"			, rs.getString("SHIPNO"));
    				dsSet.put("BP_CD"			, rs.getString("BP_CD"));
    				dsSet.put("BP_NAME"			, rs.getString("BP_NAME"));
    				dsSet.put("PLANT_CD"		, rs.getString("PLANT_CD"));
    				dsSet.put("SL_CD"			, rs.getString("SL_CD"));
    				dsSet.put("ITEM_CD"			, rs.getString("ITEM_CD"));
    				dsSet.put("REQ_COUNT"		, rs.getString("REQ_COUNT"));
    				dsSet.put("REQ_QTY"			, rs.getString("REQ_QTY"));
    				dsSet.put("REMARK"			, rs.getString("REMARK"));
    				dsSet.put("CUD_FLG"			, rs.getString("CUD_FLG"));
    				dsSet.put("BIZ_REC_FLG"		, rs.getString("BIZ_REC_FLG"));
    				dsSet.put("BIZ_REC_DT"		, rs.getString("BIZ_REC_DT"));
    				dsSet.put("BIZ_REC_ERROR"	, rs.getString("BIZ_REC_ERROR"));
    				dsSet.put("INSRT_USER_ID"	, rs.getString("INSRT_USER_ID"));
    				dsSet.put("INSRT_DT"		, rs.getString("INSRT_DT"));
    				
    				//to oracle insert
    				String fileUploadId = (String) dao.custIfOutOrd(dsSet);
    				
    				//mssql this row update
    				stmtp = con.createStatement();
    				stmtp.executeUpdate(" UPDATE UT_MATERIAL_SHIP_IF"
			                          + "    SET BIZ_REC_FLG = 'Y'"
			                          + "      , BIZ_REC_DT  = GETDATE()"
			                          + "  WHERE IF_ID         = "+"'"+ rs.getString("IF_ID") +"'"
			                          + "    AND MATERIALLOTID = "+"'"+ rs.getString("MATERIALLOTID") +"'"
			                          + "    AND CUD_FLG       IN ('C')"
			                          + ";");
    				stmtp.close();
    				
            	    ServiceUtil.isValidReturnCode("WMSOP010", "00", "SUCCESS");
            	}
        	}
        	
        	stmt.close();
            con.close();

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	stmt.close();
        	con.rollback();
        	con.close();
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : tempListInOrd
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> tempListInOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.tempListInOrd(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : tempListOutOrd
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> tempListOutOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.tempListOutOrd(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : custIfOutOrdReturn
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custIfOutOrdReturn(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        
        // Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con  = null;
        Statement stmt  = null;
        Statement stmtp = null;
        ResultSet rs    = null;
        ResultSet rsp   = null;
        
        try{
        	//db connect
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
        	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        	
        	//mssql db select
        	rs = stmt.executeQuery( "SELECT IF_ID"
			        			 + "      , CONVERT(VARCHAR(50), IF_DT, 111) AS IF_DT"
			        			 + "      , MOVE_NO"
			        			 + "      , MATERIALLOTID"
			        			 + "      , MATERIALLOTQTY"
			        			 + "      , FINAL_LOT_NO"
			        			 + "      , IN_BCR_NO"
			        			 + "      , OUT_BCR_NO"
			        			 + "      , PLANT_CD"
			        			 + "      , SL_CD"
			        			 + "      , ITEM_CD"
			        			 + "      , MOVE_COUNT"
			        			 + "      , MOVE_QTY"
			        			 + "      , REMARK"
			        			 + "      , CUD_FLG"
			        			 + "      , BIZ_REC_FLG"
			        			 + "      , BIZ_REC_DT"
			        			 + "      , CONVERT(VARCHAR(50), PRODDATE01, 111) AS PRODDATE01"
			        			 + "      , CONVERT(VARCHAR(50), PRODDATE02, 111) AS PRODDATE02"
			        			 + "   FROM UT_MATERIAL_MOVE_IF"
			        			 + "  WHERE CUD_FLG     = 'R'"
			        			 + "    AND BIZ_REC_FLG = 'N'"
        			             + ";");
        	//mssql get rowcount
        	rs.last();
        	int rowcount = rs.getRow();
        	rs.beforeFirst();
        	
        	if(rowcount > 0){
        		while(rs.next()) {
    				Map<String, Object> dsSet = new HashMap<String, Object>();
    				dsSet.put("IF_ID"			, rs.getString("IF_ID"));
    				dsSet.put("IF_DT"			, rs.getString("IF_DT"));
    				dsSet.put("MOVE_NO"			, rs.getString("MOVE_NO"));
    				dsSet.put("MATERIALLOTID"	, rs.getString("MATERIALLOTID"));
    				dsSet.put("MATERIALLOTQTY"	, rs.getString("MATERIALLOTQTY"));
    				dsSet.put("FINAL_LOT_NO"	, rs.getString("FINAL_LOT_NO"));
    				dsSet.put("IN_BCR_NO"		, rs.getString("IN_BCR_NO"));
    				dsSet.put("OUT_BCR_NO"		, rs.getString("OUT_BCR_NO"));
    				dsSet.put("PLANT_CD"		, rs.getString("PLANT_CD"));
    				dsSet.put("SL_CD"			, rs.getString("SL_CD"));
    				dsSet.put("ITEM_CD"			, rs.getString("ITEM_CD"));
    				dsSet.put("MOVE_COUNT"		, rs.getString("MOVE_COUNT"));
    				dsSet.put("MOVE_QTY"		, rs.getString("MOVE_QTY"));
    				dsSet.put("REMARK"			, rs.getString("CUD_FLG") + rs.getString("REMARK"));
    				dsSet.put("CUD_FLG"			, "C"); //rs.getString("CUD_FLG")
    				dsSet.put("BIZ_REC_FLG"		, rs.getString("BIZ_REC_FLG"));
    				dsSet.put("PRODDATE01"		, rs.getString("PRODDATE01"));
    				dsSet.put("PRODDATE02"		, rs.getString("PRODDATE02"));
    				
    				dsSet.put("SHIPNO"			, rs.getString("MOVE_NO"));
    				dsSet.put("BP_CD"			, "ECOPROBMOUT");
    				dsSet.put("BP_NAME"			, "에코프로BM");
    				
    				//to oracle insert
    				String fileUploadId = (String) dao.custIfOutOrdReturn(dsSet);
    				
    				//mssql this row update
    				stmtp = con.createStatement();
    				stmtp.executeUpdate(" UPDATE UT_MATERIAL_MOVE_IF"
			                          + "    SET BIZ_REC_FLG   = 'Y'"
			                          + "      , BIZ_REC_DT    = GETDATE()"
			                          + "  WHERE IF_ID         = "+"'"+ rs.getString("IF_ID") +"'"
			                          + "    AND MATERIALLOTID = "+"'"+ rs.getString("MATERIALLOTID") +"'"
			                          + "    AND CUD_FLG       = 'R'"
			                          + ";");
    				stmtp.close();
    				
            	    ServiceUtil.isValidReturnCode("WMSOP010", "00", "SUCCESS");
            	}
        	}
        	
        	stmt.close();
            con.close();

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	stmt.close();
        	con.rollback();
        	con.close();
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : custIfOutOrdReturnCancel
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custIfOutOrdReturnCancel(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        
        // Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con  = null;
        Statement stmt  = null;
        Statement stmtp = null;
        ResultSet rs    = null;
        ResultSet rsp   = null;
        
        try{
        	//db connect
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
        	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        	
        	//mssql db select
        	rs = stmt.executeQuery( "SELECT IF_ID"
			        			 + "      , CONVERT(VARCHAR(50), IF_DT, 111) AS IF_DT"
			        			 + "      , MOVE_NO"
			        			 + "      , MATERIALLOTID"
			        			 + "      , MATERIALLOTQTY"
			        			 + "      , FINAL_LOT_NO"
			        			 + "      , IN_BCR_NO"
			        			 + "      , OUT_BCR_NO"
			        			 + "      , PLANT_CD"
			        			 + "      , SL_CD"
			        			 + "      , ITEM_CD"
			        			 + "      , MOVE_COUNT"
			        			 + "      , MOVE_QTY"
			        			 + "      , REMARK"
			        			 + "      , CUD_FLG"
			        			 + "      , BIZ_REC_FLG"
			        			 + "      , BIZ_REC_DT"
			        			 + "      , CONVERT(VARCHAR(50), PRODDATE01, 111) AS PRODDATE01"
			        			 + "      , CONVERT(VARCHAR(50), PRODDATE02, 111) AS PRODDATE02"
			        			 + "   FROM UT_MATERIAL_MOVE_IF"
			        			 + "  WHERE CUD_FLG     = 'D'"
			        			 + "    AND BIZ_REC_FLG = 'N'"
        			             + ";");
        	//mssql get rowcount
        	rs.last();
        	int rowcount = rs.getRow();
        	rs.beforeFirst();
        	
        	if(rowcount > 0){
        		while(rs.next()) {
    				Map<String, Object> dsSet = new HashMap<String, Object>();
    				dsSet.put("IF_ID"			, rs.getString("IF_ID"));
    				dsSet.put("IF_DT"			, rs.getString("IF_DT"));
    				dsSet.put("MOVE_NO"			, rs.getString("MOVE_NO"));
    				dsSet.put("MATERIALLOTID"	, rs.getString("MATERIALLOTID"));
    				dsSet.put("MATERIALLOTQTY"	, rs.getString("MATERIALLOTQTY"));
    				dsSet.put("FINAL_LOT_NO"	, rs.getString("FINAL_LOT_NO"));
    				dsSet.put("IN_BCR_NO"		, rs.getString("IN_BCR_NO"));
    				dsSet.put("OUT_BCR_NO"		, rs.getString("OUT_BCR_NO"));
    				dsSet.put("PLANT_CD"		, rs.getString("PLANT_CD"));
    				dsSet.put("SL_CD"			, rs.getString("SL_CD"));
    				dsSet.put("ITEM_CD"			, rs.getString("ITEM_CD"));
    				dsSet.put("MOVE_COUNT"		, rs.getString("MOVE_COUNT"));
    				dsSet.put("MOVE_QTY"		, rs.getString("MOVE_QTY"));
    				dsSet.put("REMARK"			, rs.getString("CUD_FLG") + rs.getString("REMARK"));
    				dsSet.put("CUD_FLG"			, "C"); //rs.getString("CUD_FLG")
    				dsSet.put("BIZ_REC_FLG"		, rs.getString("BIZ_REC_FLG"));
    				dsSet.put("PRODDATE01"		, rs.getString("PRODDATE01"));
    				dsSet.put("PRODDATE02"		, rs.getString("PRODDATE02"));
    				
    				dsSet.put("SHIPNO"			, rs.getString("MOVE_NO"));
    				dsSet.put("BP_CD"			, "ECOPROBMOUT");
    				dsSet.put("BP_NAME"			, "에코프로BM");
    				
    				//wmsif101 CUD_FLG = D, delete, WHERE MOVE_NO
    				String checkExistData = dao.checkExistEmptyOrdReturn(dsSet);
    				if (checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
    					dao.custIfOutOrdReturnDelete(dsSet);
                    }
    				
    				//mssql this row update
    				stmtp = con.createStatement();
    				stmtp.executeUpdate(" UPDATE UT_MATERIAL_MOVE_IF"
			                          + "    SET BIZ_REC_FLG   = 'Y'"
			                          + "      , BIZ_REC_DT    = GETDATE()"
			                          + "  WHERE IF_ID         = "+"'"+ rs.getString("IF_ID") +"'"
			                          + "    AND MATERIALLOTID = "+"'"+ rs.getString("MATERIALLOTID") +"'"
			                          + "    AND CUD_FLG       = 'D'"
			                          + ";");
    				stmtp.close();
    				
            	    ServiceUtil.isValidReturnCode("WMSOP010", "00", "SUCCESS");
            	}
        	}
        	
        	stmt.close();
            con.close();

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	stmt.close();
        	con.rollback();
        	con.close();
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
}
