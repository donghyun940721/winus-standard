package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.*;
import sun.misc.*;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP210Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP210Service")
public class WMSOP210ServiceImpl implements WMSOP210Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP210Dao")
    private WMSOP210Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 입출고현황  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        map.put("LIST", dao.list(model));
        return map;
    }
    
    @Override
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        map.put("LIST", dao.list2(model));
        return map;
    }
    
    @Override
    public Map<String, Object> updateAutoLabel(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try{
        	 List list = dao.rawListByList2(model);
        	 int tmpCnt = list.size();
        	 
        	 if(tmpCnt > 0){
        		 
        	 }
        	 if(tmpCnt > 0){           
                 
 	            String[] ORDER_NO = new String[tmpCnt];
 	            String[] ORDER_DATE = new String[tmpCnt];
 	            String[] FA_ISSUE_LABEL = new String[tmpCnt];
 	            String[] CUST_ID = new String[tmpCnt];
 	            
 	            
 	            for(int i = 0 ; i < tmpCnt ; i ++){
 	            	ORDER_NO[i]    			= (String)((Map<String, String>)list.get(i)).get("ORDER_NO");
                 	ORDER_DATE[i]   	 	= (String)((Map<String, String>)list.get(i)).get("ORDER_DATE");
                 	FA_ISSUE_LABEL[i]    	= (String)((Map<String, String>)list.get(i)).get("FA_ISSUE_LABEL");
                 	CUST_ID[i]   			= (String)((Map<String, String>)list.get(i)).get("CUST_ID");
                 	
//                 	System.out.println((String)((Map<String, String>)list.get(i)).get("ORDER_NO"));
//                 	System.out.println((String)((Map<String, String>)list.get(i)).get("ORDER_DATE"));
//                 	System.out.println((String)((Map<String, String>)list.get(i)).get("FA_ISSUE_LABEL"));
//                 	System.out.println((String)((Map<String, String>)list.get(i)).get("CUST_ID"));
                 	
                 	Map<String, Object> modelIns = new HashMap<String, Object>();

     	            modelIns.put("ORDER_NO", ORDER_NO[i]);
     	            modelIns.put("ORDER_DATE", ORDER_DATE[i]);
     	            modelIns.put("FA_ISSUE_LABEL", FA_ISSUE_LABEL[i]);
     	            modelIns.put("CUST_ID", CUST_ID[i]);

     	            // dao
     	            modelIns = (Map<String, Object>)dao.updateAutoLabel(modelIns);
                 }
 	            
             }
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("list.success"));
             
         } catch(Exception e){
         	if (log.isErrorEnabled()) {
 				log.error("Fail to get result :", e);
 			} 
             m.put("errCnt", 1);
             m.put("MSG", MessageResolver.getMessage("save.error") );
         }
         return m;
     }
        	 
//             if(temCnt > 0){
//                 String[] ordNo 			= new String[temCnt];
//                 String[] ordDate 			= new String[temCnt];
//                 String[] faIssueLabel 		= new String[temCnt];
//                 String[] custId 			= new String[temCnt];
//                 
//                 for(int i = 0 ; i < temCnt ; i ++){
//                	 Map<String, Object> modelDS = new HashMap<String, Object>();
////                	 
//	                	 ordNo[i]   			= (String)model.get("ORDER_NO"+i);
//	                	 ordDate[i]   			= (String)model.get("ORDER_DATE"+i);
//	                	 faIssueLabel[i]    	= (String)model.get("FA_ISSUE_LABEL"+i);
//	                	 custId[i] 				= (String)model.get("CUST_ID"+i);
//	                	 
////	                	 System.out.println("여기.-------" +  i);
////	                	 System.out.println((String)model.get("ORDER_NO"+i));
////	                	 System.out.println((String)model.get("ORDER_DATE"+i));
////	                	 System.out.println((String)model.get("FA_ISSUE_LABEL"+i));
////	                	 System.out.println((String)model.get("CUST_ID"+i));
//	                	 
//	                	modelDS.put("ORDER_NO"  	 , ordNo[i]);
//	                	modelDS.put("ORDER_DATE"     , ordDate[i]);
//	                	modelDS.put("FA_ISSUE_LABEL" , faIssueLabel[i]);
//	                	modelDS.put("CUST_ID"        , custId[i]);
//	 	                
//	 	                
//                    	dao.updateAutoLabel(modelDS);
//                    
//                 }      
//                 
//             }   
//             m.put("errCnt", 0);
//             m.put("MSG", MessageResolver.getMessage("save.success"));
//             
//         }catch(Exception e){
//             throw e;
//         }
//         return m;
//     }  
    
    
    
    
}
