package com.logisall.winus.wmsop.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gns.util.Log;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.HttpUtil;
import com.logisall.winus.wmsop.service.WMSOP630Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
public class WMSOP630ServiceImpl implements WMSOP630Service{
	@Autowired
	private WMSOP030Dao wmsop030Dao;
	
	@Autowired
	private WMSOP630Dao wmsop630Dao;
	
	@Override
	public Map<String, Object> saveAIPicking(Map<String, Object> model, Map<String, Object> reqBody, String uuid) {
		Map<String, Object> m = new HashMap<String, Object>();
        List<Map<String, Object>> ordList = (List<Map<String, Object>>) reqBody.get("ord_list");
        insertAiPickingOrder(uuid, model, ordList);
		return m;
	}
	
	@Override
	public Map<String, Object> requestAiPicking(String uuid) throws IOException{
		Map<String, Object> aiRequest = new HashMap<String, Object>();
		aiRequest.put("order_key", uuid);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonBody =  objectMapper.writeValueAsString(aiRequest);
		String responseJson = HttpUtil.sendJsonHttpRequest("http://10.100.3.178:8080/ai_picking", HttpMethod.POST, jsonBody);
		Map<String, Object> responseMap = objectMapper.readValue(responseJson, new TypeReference<HashMap<String, Object>>() {});
		return responseMap;
	}

//	private Map<String, Object> requestAiPicking(Map<String, Object> reqBody, List<Map<String, Object>> ordList)
//			throws IOException{
//		Map<String, Object> aiRequest = new HashMap<String, Object>();
//		// ORD_ID 값만 추출하여 새로운 리스트에 저장
//		List<String> aiReqOrdList = new ArrayList<String>();
//		for (Map<String, Object> ord : ordList) {
//		    String ordId = (String) ord.get("ORD_ID");
//		    if (ordId != null) {
//		    	aiReqOrdList.add(ordId);
//		    }
//		}
//		aiRequest.put("req_date", reqBody.get("req_date"));
//		aiRequest.put("ord_list", aiReqOrdList);
//
//		ObjectMapper objectMapper = new ObjectMapper();
//		String jsonBody =  objectMapper.writeValueAsString(aiRequest);
//		String responseJson = HttpUtil.sendJsonHttpRequest("http://10.100.3.178:8080/ai_picking", HttpMethod.POST, jsonBody);
//		Map<String, Object> responseMap = objectMapper.readValue(responseJson, new TypeReference<HashMap<String, Object>>() {});
//		return responseMap;
//	}

	private void insertAiPickingOrder(String uuid, Map<String, Object> model, List<Map<String, Object>> ordList) {
		for (Map<String, Object> ord : ordList) {
			ord.put("ORDER_KEY", uuid);
			ord.put("uuid"          , uuid);
			ord.put("LC_ID"         , model.get(ConstantIF.SS_SVC_NO));
			ord.put("ORG_ORD_ID"    , (String)ord.get("ORG_ORD_ID"));
			ord.put("ORD_ID"    , (String)ord.get("ORD_ID"));
			ord.put("ORD_SEQ"    , (String)ord.get("ORD_SEQ"));
			ord.put("USER_NO"   , model.get(ConstantIF.SS_USER_NO));
		}
		wmsop630Dao.insertAiPickingOrder(ordList);
	}

}
