package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP940Dao")
public class WMSOP940Dao extends SqlMapAbstractDAO{
	
	/**
	 * Method ID : listE01
	 * Method 설명 : 코텍 DAS 주문결과 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE01(Map<String, Object> model) {
        return executeQueryWq("wmsop940.listE01", model);
    }   
    
}
