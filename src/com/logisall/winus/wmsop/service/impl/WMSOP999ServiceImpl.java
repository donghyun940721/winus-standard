package com.logisall.winus.wmsop.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.impl.WMSMS090Dao;
import com.logisall.winus.wmsop.service.WMSOP999Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP999Service")
public class WMSOP999ServiceImpl implements WMSOP999Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP999Dao")
    private WMSOP999Dao dao;
    
    @Resource(name = "WMSMS090Dao")
	private WMSMS090Dao MSdao;
    
    /**
     * 대체 Method ID   : selectItemGrp
     * 대체 Method 설명    : 입고관리 화면에서 필요한 데이터 
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        log.info(model);
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
  
     /**
      * 대체 Method ID   : listOrderDetail
      * 대체 Method 설명    : 입고관리 상세상품조회
      * 작성자                      : chsong
      * @param   model
      * @return
      * @throws  Exception
      */
     @Override
     public Map<String, Object> listOrderDetail(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         map.put("DETAIL", dao.listOrderDetail(model));
         return map;
     }
     
     /**
      * 대체 Method ID   	: listOrderDetailOm
      * 대체 Method 설명    : 출고관리 OM 상세 조회
      * 작성자              : KSJ
      * @param   model
      * @return
      * @throws  Exception
      */
     @Override
     public Map<String, Object> listOrderDetailOm(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         map.put("DETAIL", dao.listOrderDetailOm(model));
         return map;
     }
     
    /**
     * 
     * 대체 Method ID   : listInOrderItem
     * 대체 Method 설명    : 입고관리 상세상품조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listInOrderItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        log.info(model);
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        GenericResultSet res = dao.listInOrderItem(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		//org.springframework.web.util.HtmlUtils.htmlUnescape()
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			System.out.println(object.get("1").toString());
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
        res.setList(list);
        map.put("LIST", res);
        
        return map;
    }
 
    /**
     * 
     * 대체 Method ID   : listOutOrderItem
     * 대체 Method 설명    : 출고관리 상세상품조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listOutOrderItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        log.info(model);
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        GenericResultSet res = dao.listOutOrderItem(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		//org.springframework.web.util.HtmlUtils.htmlUnescape()
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			System.out.println(object.get("1").toString());
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
        res.setList(list);
        map.put("LIST", res);
        
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listLocSearch
     * 대체 Method 설명    : 출고관리 로케이션조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listLocSearch(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        log.info(model);
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listLocSearch(model));
        return map;
    }   
    
    
    /**
     * 
     * 대체 Method ID   : saveInOrder
     * 대체 Method 설명    : 입고주문(저장,수정,삭제)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
//    @Override
//    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception {
//        Map<String, Object> m = new HashMap<String, Object>();
//        log.info(model);
//        int errCnt = 0;
//        String errMsg = "";
//        try{
//
//            int insCnt = Integer.parseInt(model.get("selectIds").toString());
//            
//            String[] dsSub_rowStatus = new String[insCnt];                
//            String[] ordSeq         = new String[insCnt];         
//            String[] ritemId        = new String[insCnt];     
//            String[] custLotNo      = new String[insCnt];     
//            String[] realInQty      = new String[insCnt];     
//            
//            String[] realOutQty     = new String[insCnt];                     
//            String[] makeDt         = new String[insCnt];         
//            String[] timePeriodDay  = new String[insCnt];     
//            String[] locYn          = new String[insCnt];     
//            String[] pdaCd          = new String[insCnt];     
//            
//            String[] workYn         = new String[insCnt];                
//            String[] rjType         = new String[insCnt];         
//            String[] realPltQty     = new String[insCnt];     
//            String[] realBoxQty     = new String[insCnt];     
//            String[] confYn         = new String[insCnt];     
//            
//            String[] unitAmt        = new String[insCnt];                
//            String[] amt            = new String[insCnt];         
//            String[] eaCapa         = new String[insCnt];     
//            String[] boxBarcode     = new String[insCnt];     
//            String[] inOrdUomId     = new String[insCnt];     
//            
//            String[] outOrdUomId    = new String[insCnt];                
//            String[] inOrdQty       = new String[insCnt];         
//            String[] outOrdQty      = new String[insCnt];     
//            String[] refSubLotId    = new String[insCnt];     
//            String[] dspId          = new String[insCnt];     
//            
//            String[] carId          = new String[insCnt];                
//            String[] cntrId         = new String[insCnt];         
//            String[] cntrNo         = new String[insCnt];     
//            String[] cntrType       = new String[insCnt];     
//            String[] badQty         = new String[insCnt];     
//            
//            String[] uomNm          = new String[insCnt];                
//            String[] unitPrice      = new String[insCnt];         
//            String[] whNm           = new String[insCnt];     
//            String[] itemKorNm      = new String[insCnt];     
//            String[] itemEngNm      = new String[insCnt];     
//            
//            String[] repUomId       = new String[insCnt];                
//            String[] uomCd          = new String[insCnt];         
//            String[] uomId          = new String[insCnt];     
//            String[] repUomCd       = new String[insCnt];     
//            String[] repuomNm       = new String[insCnt];     
//            
//            String[] itemGrpId      = new String[insCnt];                
//            String[] expiryDate     = new String[insCnt];         
//            String[] inOrdWeight    = new String[insCnt];     
//            String[] ordDesc        = new String[insCnt];     
//            String[] validDt        = new String[insCnt];     
//            
//            String[] etc2           = new String[insCnt];
//            
//            //추가
//            String[] time_date      = new String[insCnt];   //상품유효기간     
//            String[] time_date_end  = new String[insCnt];   //상품유효기간만료일
//            String[] time_use_end   = new String[insCnt];   //소비가한만료일
//            
//            for(int i = 0 ; i < insCnt ; i ++){
//                dsSub_rowStatus[i]  = (String)model.get("ST_GUBUN"+i);               
//                ordSeq[i]           = (String)model.get("ORD_SEQ"+i);          
//                ritemId[i]          = (String)model.get("RITEM_ID"+i);      
//                custLotNo[i]        = (String)model.get("CUST_LOT_NO"+i);      
//                realInQty[i]        = (String)model.get("REAL_IN_QTY"+i);      
//                
//                realOutQty[i]       = (String)model.get("REAL_OUT_QTY"+i);                      
//                makeDt[i]           = (String)model.get("MAKE_DT"+i);          
//                timePeriodDay[i]    = (String)model.get("TIME_PERIOD_DAY"+i);      
//                locYn[i]            = (String)model.get("LOC_YN"+i);      
//                pdaCd[i]            = (String)model.get("PDA_CD"+i);      
//                
//                workYn[i]           = (String)model.get("WORK_YN"+i);                 
//                rjType[i]           = (String)model.get("RJ_TYPE"+i);          
//                realPltQty[i]       = (String)model.get("REAL_PLT_QTY"+i);      
//                realBoxQty[i]       = (String)model.get("REAL_BOX_QTY"+i);      
//                confYn[i]           = (String)model.get("CONF_YN"+i);      
//                
//                unitAmt[i]          = (String)model.get("UNIT_AMT"+i);                 
//                amt[i]              = (String)model.get("AMT"+i);          
//                eaCapa[i]           = (String)model.get("EA_CAPA"+i);      
//                boxBarcode[i]       = (String)model.get("BOX_BARCODE"+i);      
//                inOrdUomId[i]       = (String)model.get("IN_ORD_UOM_ID"+i);      
//                
//                outOrdUomId[i]      = (String)model.get("OUT_ORD_UOM_ID"+i);                 
//                inOrdQty[i]         = (String)model.get("IN_ORD_QTY"+i);          
//                outOrdQty[i]        = (String)model.get("OUT_ORD_QTY"+i);      
//                refSubLotId[i]      = (String)model.get("REF_SUB_LOT_ID"+i);      
//                dspId[i]            = (String)model.get("DSP_ID"+i);      
//                
//                carId[i]            = (String)model.get("CAR_ID"+i);                 
//                cntrId[i]           = (String)model.get("CNTR_ID"+i);          
//                cntrNo[i]           = (String)model.get("CNTR_NO"+i);      
//                cntrType[i]         = (String)model.get("CNTR_TYPE"+i);      
//                badQty[i]           = (String)model.get("BAD_QTY"+i);      
//                
//                uomNm[i]            = (String)model.get("UOM_NM"+i);                 
//                unitPrice[i]        = (String)model.get("UNIT_PRICE"+i);          
//                whNm[i]             = (String)model.get("WH_NM"+i);      
//                itemKorNm[i]        = (String)model.get("ITEM_KOR_NM"+i);      
//                itemEngNm[i]        = (String)model.get("ITEM_ENG_NM"+i);      
//                
//                repUomId[i]         = (String)model.get("REP_UOM_ID"+i);                 
//                uomCd[i]            = (String)model.get("UOM_CD"+i);          
//                uomId[i]            = (String)model.get("UOM_IF"+i);      
//                repUomCd[i]         = (String)model.get("REP_UOM_CD"+i);      
//                repuomNm[i]         = (String)model.get("REP_UOM_NM"+i);      
//                
//                itemGrpId[i]        = (String)model.get("ITEM_GRP_ID"+i);                 
//                expiryDate[i]       = (String)model.get("EXPIRY_DATE"+i);          
//                inOrdWeight[i]      = (String)model.get("IN_ORD_WEIGHT"+i);      
//                ordDesc[i]          = (String)model.get("ORD_DESC"+i);      
//                validDt[i]          = (String)model.get("VALID_DT"+i);      
//                
//                etc2[i]             = (String)model.get("ETC2"+i); 
//                
//                //추가 
//                time_date[i]        = (String)model.get("TIME_DATE"+i);      
//                time_date_end[i]    = (String)model.get("TIME_DATE_END"+i);      
//                time_use_end[i]     = (String)model.get("TIME_USE_END"+i);   
//            }
//            //프로시져에 보낼것들 다담는다
//            Map<String, Object> modelIns = new HashMap<String, Object>();
//            
//            //main
//            modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
//            modelIns.put("vrOrdId"          , model.get("vrOrdId"));
//            modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
//            modelIns.put("inDt"             , model.get("inDt"));
//            modelIns.put("custPoid"         , model.get("custPoid"));
//            
//            modelIns.put("custPoseq"        , model.get("custPoseq"));
//            modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));
//            modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
//            modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
//            modelIns.put("outWhId"          , model.get("outWhId"));
//            
//            modelIns.put("transCustId"      , model.get("transCustId"));
//            modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
//            modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
//            modelIns.put("blNo"             , model.get("vrBlNo"));
//            modelIns.put("workStat"         , model.get("workStat"));
//            
//            modelIns.put("ordType"          , model.get("ordType"));
//            modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
//            modelIns.put("outReqDt"         , model.get("outReqDt"));
//            modelIns.put("outDt"            , model.get("outDt"));
//            modelIns.put("pdaStat"          , model.get("pdaStat"));
//            
//            modelIns.put("workSeq"          , model.get("workSeq"));
//            modelIns.put("capaTot"          , model.get("capaTot"));
//            modelIns.put("kinOutYn"         , model.get("kinOutYn"));
//            modelIns.put("carConfYn"        , model.get("carConfYn"));
//            modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
//            
//            modelIns.put("tplOrdId"         , model.get("tplOrdId"));
//            modelIns.put("approveYn"        , model.get("vrApproveYn"));
//            modelIns.put("payYn"            , model.get("payYn"));
//            modelIns.put("inCustId"         , model.get("vrInCustId"));
//            modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
//            
//            modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
//            modelIns.put("inCustTel"        , model.get("vrInCustTel"));
//            
//            //sub
//            modelIns.put("dsSub_rowStatus"  , dsSub_rowStatus);
//            modelIns.put("ordSeq"           , ordSeq);
//            modelIns.put("ritemId"          , ritemId);
//            modelIns.put("custLotNo"        , custLotNo);
//            modelIns.put("realInQty"        , realInQty);
//            
//            modelIns.put("realOutQty"       , realOutQty);
//            modelIns.put("makeDt"           , makeDt);
//            modelIns.put("timePeriodDay"    , timePeriodDay);
//            modelIns.put("locYn"            , locYn);
//            modelIns.put("pdaCd"            , pdaCd);
//            
//            modelIns.put("workYn"           , workYn);
//            modelIns.put("rjType"           , rjType);
//            modelIns.put("realPltQty"       , realPltQty);
//            modelIns.put("realBoxQty"       , realBoxQty);
//            modelIns.put("confYn"           , confYn);
//            
//            modelIns.put("unitAmt"          , unitAmt);
//            modelIns.put("amt"              , amt);
//            modelIns.put("eaCapa"           , eaCapa);
//            modelIns.put("boxBarcode"       , boxBarcode);
//            modelIns.put("inOrdUomId"       , inOrdUomId);
//            
//            modelIns.put("outOrdUomId"      , outOrdUomId);
//            modelIns.put("inOrdQty"         , inOrdQty);
//            modelIns.put("outOrdQty"        , outOrdQty);
//            modelIns.put("refSubLotId"      , refSubLotId);
//            modelIns.put("dspId"            , dspId);
//            
//            modelIns.put("carId"            , carId);
//            modelIns.put("cntrId"           , cntrId);
//            modelIns.put("cntrNo"           , cntrNo);
//            modelIns.put("cntrType"         , cntrType);
//            modelIns.put("badQty"           , badQty);
//            
//            modelIns.put("uomNm"            , uomNm);
//            modelIns.put("unitPrice"        , unitPrice);
//            modelIns.put("whNm"             , whNm);
//            modelIns.put("itemKorNm"        , itemKorNm);
//            modelIns.put("itemEngNm"        , itemEngNm);
//            
//            modelIns.put("repUomId"         , repUomId);
//            modelIns.put("uomCd"            , uomCd);
//            modelIns.put("uomId"            , uomId);
//            modelIns.put("repUomCd"         , repUomCd);
//            modelIns.put("repuomNm"         , repuomNm);
//            
//            modelIns.put("itemGrpId"        , itemGrpId);
//            modelIns.put("expiryDate"       , expiryDate);
//            modelIns.put("inOrdWeight"      , inOrdWeight); 
//            
//            modelIns.put("ordDesc"          , ordDesc);
//            modelIns.put("validDt"          , validDt);                
//            modelIns.put("etc2"             , etc2);
//            
//            //추가된거(아직프로시져는 안탐)
//            modelIns.put("time_date"        , time_date);
//            modelIns.put("time_date_end"    , time_date_end);                
//            modelIns.put("time_use_end"     , time_use_end);               
//            
//            //session 정보
//            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
//            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
//
//            //dao                
//            modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
//            errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
//            errMsg = modelIns.get("O_MSG_NAME").toString();
//        
//            if(errCnt == 1){
//                errMsg = MessageResolver.getMessage("save.success");
//            }
//            //등록 수정 끝
//                  
//            m.put("errCnt", errCnt);
//            m.put("MSG"   , errMsg);
//            
//        } catch(Exception e){
//            throw e;
//        }
//        return m;
//    }      
    
    /**
     * 
     * 대체 Method ID   : deleteInOrder
     * 대체 Method 설명    : 입고주문(삭제)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);

        try{

            int delCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(delCnt > 0 ){
                String[] ordId  = new String[delCnt];         
                String[] ordSeq = new String[delCnt]; 
                for(int i = 0 ; i < delCnt ; i ++){
                    ordId[i]  = (String)model.get("ORD_ID"+i);               
                    ordSeq[i] = (String)model.get("ORD_SEQ"+i);          
                }
  
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelDel = new HashMap<String, Object>();
                modelDel.put("ordId", ordId);
                modelDel.put("ordSeq", ordSeq);
  
                modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
  
                modelDel = (Map<String, Object>)dao.deleteInOrder(modelDel);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG"   , MessageResolver.getMessage("delete.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );        	
        	
    	} catch(Exception e){
            throw e;
        }
        return m;
    }    
    
    
    /**
     * 분할전
     * 대체 Method ID   : saveInOrder
     * 대체 Method 설명    : 입고주문(저장,수정,삭제)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
                //저장, 수정
                String[] dsSubRowStatus = new String[insCnt];                
                String[] ordSeq         = new String[insCnt];         
                String[] ritemId        = new String[insCnt];     
                String[] custLotNo      = new String[insCnt];     
                String[] realInQty      = new String[insCnt];     
                
                String[] realOutQty     = new String[insCnt];                     
                String[] makeDt         = new String[insCnt];         
                String[] timePeriodDay  = new String[insCnt];     
                String[] locYn          = new String[insCnt];     
                String[] pdaCd          = new String[insCnt];     
                
                String[] workYn         = new String[insCnt];                
                String[] rjType         = new String[insCnt];         
                String[] realPltQty     = new String[insCnt];     
                String[] realBoxQty     = new String[insCnt];     
                String[] confYn         = new String[insCnt];     
                
                String[] unitAmt        = new String[insCnt];                
                String[] amt            = new String[insCnt];         
                String[] eaCapa         = new String[insCnt];     
                String[] boxBarcode     = new String[insCnt];     
                String[] inOrdUomId     = new String[insCnt];     
                
                String[] inWorkUomId 	= new String[insCnt]; 
                String[] outOrdUomId    = new String[insCnt];       
                String[] outWorkUomId	= new String[insCnt]; 
                String[] inOrdQty       = new String[insCnt]; 
                String[] inWorkOrdQty   = new String[insCnt]; 
                
                String[] outOrdQty      = new String[insCnt];     
                String[] outWorkOrdQty  = new String[insCnt]; 
                String[] refSubLotId    = new String[insCnt];     
                String[] dspId          = new String[insCnt];     
                String[] carId          = new String[insCnt];                
                
                String[] cntrId         = new String[insCnt];         
                String[] cntrNo         = new String[insCnt];     
                String[] cntrType       = new String[insCnt];     
                String[] badQty         = new String[insCnt];     
                String[] uomNm          = new String[insCnt];                
                
                String[] unitPrice      = new String[insCnt];         
                String[] whNm           = new String[insCnt];     
                String[] itemKorNm      = new String[insCnt];     
                String[] itemEngNm      = new String[insCnt];     
                String[] repUomId       = new String[insCnt];                
                
                String[] uomCd          = new String[insCnt];         
                String[] uomId          = new String[insCnt];     
                String[] repUomCd       = new String[insCnt];     
                String[] repuomNm       = new String[insCnt];     
                String[] itemGrpId      = new String[insCnt];                
                
                String[] expiryDate     = new String[insCnt];         
                String[] inOrdWeight    = new String[insCnt];                 
                String[] unitNo         = new String[insCnt];
                String[] ordDesc        = new String[insCnt];
                String[] validDt        = new String[insCnt];
                
                String[] etc2           = new String[insCnt];
                
                //추가
                String[] itemBestDate     = new String[insCnt];   //상품유효기간     
                String[] itemBestDateEnd  = new String[insCnt];   //상품유효기간만료일
                String[] rtiNm            = new String[insCnt];   //물류기기명
                
                for(int i = 0 ; i < insCnt ; i ++){
                    dsSubRowStatus[i]  = (String)model.get("I_ST_GUBUN"+i);               
                    ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);          
                    ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
                    
                    realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
                    makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("I_LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
                    
                    workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
                    rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("I_CONF_YN"+i);      
                    
                    unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
                    amt[i]              = (String)model.get("I_AMT"+i);          
                    eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
                    inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);      
                    inWorkUomId[i]      = (String)model.get("I_IN_WORK_UOM_ID"+i);
                    outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);                 
                    outWorkUomId[i]     = (String)model.get("I_OUT_WORK_UOM_ID"+i);
                    inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);          
                    inWorkOrdQty[i]     = (String)model.get("I_IN_WORK_ORD_QTY"+i);
                    outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);     
                    outWorkOrdQty[i]    = (String)model.get("I_OUT_WORK_ORD_QTY"+i); 
                    refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
                    dspId[i]            = (String)model.get("I_DSP_ID"+i);      
                    
                    carId[i]            = (String)model.get("I_CAR_ID"+i);                 
                    cntrId[i]           = (String)model.get("I_CNTR_ID"+i);          
                    cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);      
                    cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);      
                    badQty[i]           = (String)model.get("I_BAD_QTY"+i);      
                    
                    uomNm[i]            = (String)model.get("I_UOM_NM"+i);                 
                    unitPrice[i]        = (String)model.get("I_UNIT_PRICE"+i);          
                    whNm[i]             = (String)model.get("I_WH_NM"+i);      
                    itemKorNm[i]        = (String)model.get("I_ITEM_KOR_NM"+i);      
                    itemEngNm[i]        = (String)model.get("I_ITEM_ENG_NM"+i);      
                    
                    repUomId[i]         = (String)model.get("I_REP_UOM_ID"+i);                 
                    uomCd[i]            = (String)model.get("I_UOM_CD"+i);          
                    uomId[i]            = (String)model.get("I_UOM_ID"+i);      
                    repUomCd[i]         = (String)model.get("I_REP_UOM_CD"+i);      
                    repuomNm[i]         = (String)model.get("I_REP_UOM_NM"+i);      
                    
                    itemGrpId[i]        = (String)model.get("I_ITEM_GRP_ID"+i);                 
                    expiryDate[i]       = (String)model.get("I_EXPIRY_DATE"+i);          
                    inOrdWeight[i]      = (String)model.get("I_IN_ORD_WEIGHT"+i);      
                    unitNo[i]           = (String)model.get("I_UNIT_NO"+i);      
                    ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);
                    
                    validDt[i]          = (String)model.get("I_VALID_DT"+i);      
                    
                    etc2[i]             = (String)model.get("I_ETC2"+i); 
                    
                    //추가 
                    itemBestDate[i]       = (String)model.get("I_ITEM_BEST_DATE"+i);      
                    itemBestDateEnd[i]    = (String)model.get("I_ITEM_BEST_DATE_END"+i);   
                    rtiNm[i]              = (String)model.get("I_RTI_NM"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
                modelIns.put("vrOrdId"          , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));
                
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));
                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
                modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
                modelIns.put("outWhId"          , model.get("outWhId"));
                
                modelIns.put("transCustId"      , model.get("transCustId"));
                modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
                modelIns.put("blNo"             , model.get("vrBlNo"));
                modelIns.put("workStat"         , model.get("workStat"));
                
                modelIns.put("ordType"          , model.get("ordType"));
                modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
                modelIns.put("outReqDt"         , model.get("outReqDt"));
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));
                
                modelIns.put("workSeq"          , model.get("workSeq"));
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("kinOutYn"));
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                modelIns.put("approveYn"        , model.get("vrApproveYn"));
                modelIns.put("payYn"            , model.get("payYn"));
                modelIns.put("inCustId"         , model.get("vrInCustId"));
                modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
                
                modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
                modelIns.put("inCustTel"        , model.get("vrInCustTel"));
                
                //sub
                modelIns.put("dsSub_rowStatus"  , dsSubRowStatus);
                modelIns.put("ordSeq"           , ordSeq);
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);
                
                modelIns.put("realOutQty"       , realOutQty);
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);
                
                modelIns.put("workYn"           , workYn);
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);
                
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inOrdUomId"       , inOrdUomId);
                modelIns.put("inWorkUomId"   	, inWorkUomId);
                
                modelIns.put("outOrdUomId"      , outOrdUomId);
                modelIns.put("outWorkUomId"  	, outWorkUomId);
                modelIns.put("inOrdQty"         , inOrdQty);
                modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
                modelIns.put("outOrdQty"        , outOrdQty);
                
                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
                modelIns.put("refSubLotId"      , refSubLotId);
                modelIns.put("dspId"            , dspId);                
                modelIns.put("carId"            , carId);
                modelIns.put("cntrId"           , cntrId);
                
                modelIns.put("cntrNo"           , cntrNo);
                modelIns.put("cntrType"         , cntrType);
                modelIns.put("badQty"           , badQty);
                modelIns.put("uomNm"            , uomNm);
                modelIns.put("unitPrice"        , unitPrice);
                
                modelIns.put("whNm"             , whNm);
                modelIns.put("itemKorNm"        , itemKorNm);
                modelIns.put("itemEngNm"        , itemEngNm);
                modelIns.put("repUomId"         , repUomId);
                modelIns.put("uomCd"            , uomCd);
                
                modelIns.put("uomId"            , uomId);
                modelIns.put("repUomCd"         , repUomCd);
                modelIns.put("repuomNm"         , repuomNm);
                modelIns.put("itemGrpId"        , itemGrpId);
                modelIns.put("expiryDate"       , expiryDate);
                
                modelIns.put("inOrdWeight"      , inOrdWeight); 
                modelIns.put("unitNo"           , unitNo);
                modelIns.put("ordDesc"          , ordDesc);
                modelIns.put("validDt"          , validDt);                
                modelIns.put("etc2"             , etc2);
                
                //추가된거(아직프로시져는 안탐)
                modelIns.put("itemBestDate"     , itemBestDate);
                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);               
                modelIns.put("rtiNm"            , rtiNm);
                
                
                //session 정보
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                //dao                
                modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }
            //등록 수정 끝            
            if(delCnt > 0 ){
                String[] ordId  = new String[delCnt];         
                String[] ordSeq = new String[delCnt]; 
                for(int i = 0 ; i < delCnt ; i ++){
                    ordId[i]  = (String)model.get("D_ORD_ID"+i);               
                    ordSeq[i] = (String)model.get("D_ORD_SEQ"+i);          
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelDel = new HashMap<String, Object>();
                modelDel.put("ordId", ordId);
                modelDel.put("ordSeq", ordSeq);
                
                modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                
                modelDel = (Map<String, Object>)dao.deleteInOrder(modelDel);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 간편입고주문상품  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSimpleInItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        log.info(model);
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : saveSimpleOrder
     * 대체 Method 설명    : 간편입고주문(저장)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSimpleInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            if(totCnt > 0){
                //저장, 수정
                String[] dsSubRowStatus = new String[totCnt];                
                String[] ordSeq         = new String[totCnt];         
                String[] ritemId        = new String[totCnt];     
                String[] custLotNo      = new String[totCnt];     
                String[] realInQty      = new String[totCnt];     
                
                String[] realOutQty     = new String[totCnt];                     
                String[] makeDt         = new String[totCnt];         
                String[] timePeriodDay  = new String[totCnt];     
                String[] locYn          = new String[totCnt];     
                String[] pdaCd          = new String[totCnt];     
                
                String[] workYn         = new String[totCnt];                
                String[] rjType         = new String[totCnt];         
                String[] realPltQty     = new String[totCnt];     
                String[] realBoxQty     = new String[totCnt];     
                String[] confYn         = new String[totCnt];     
                
                String[] unitAmt        = new String[totCnt];                
                String[] amt            = new String[totCnt];         
                String[] eaCapa         = new String[totCnt];     
                String[] boxBarcode     = new String[totCnt];     
                String[] inWorkUomId     = new String[totCnt];     
                
                String[] outOrdUomId    = new String[totCnt];                
                String[] inWorkOrdQty       = new String[totCnt];         
                String[] outOrdQty      = new String[totCnt];     
                String[] refSubLotId    = new String[totCnt];     
                String[] dspId          = new String[totCnt];     
                
                String[] carId          = new String[totCnt];                
                String[] cntrId         = new String[totCnt];         
                String[] cntrNo         = new String[totCnt];     
                String[] cntrType       = new String[totCnt];     
                String[] badQty         = new String[totCnt];     
                
                String[] uomNm          = new String[totCnt];                
                String[] unitPrice      = new String[totCnt];         
                //String[] whNm           = new String[totCnt];     
                //String[] itemKorNm      = new String[totCnt];     
                //String[] itemEngNm      = new String[totCnt];     
                
                String[] repUomId       = new String[totCnt];                
                String[] uomCd          = new String[totCnt];         
                String[] uomId          = new String[totCnt];     
                String[] repUomCd       = new String[totCnt];     
                String[] repuomNm       = new String[totCnt];     
                
                String[] itemGrpId      = new String[totCnt];                
                String[] expiryDate     = new String[totCnt];         
                String[] inOrdWeight    = new String[totCnt];     
                String[] ordDesc        = new String[totCnt];     
                String[] validDt        = new String[totCnt];     
                
                String[] etc2           = new String[totCnt];         
                String[] inOrdUomId     = new String[totCnt];
                
                //추가
                String[] itemBestDate      = new String[totCnt];   //상품유효기간     
                String[] itemBestDateEnd  = new String[totCnt];   //상품유효기간만료일
                
                
                for(int i = 0 ; i < totCnt ; i ++){
                    dsSubRowStatus[i]  = (String)model.get("ST_GUBUN"+i);               
                    ordSeq[i]           = (String)model.get("ORD_SEQ"+i);          
                    ritemId[i]          = (String)model.get("RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("REAL_IN_QTY"+i);      
                    
                    realOutQty[i]       = (String)model.get("REAL_OUT_QTY"+i);                      
                    makeDt[i]           = (String)model.get("MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("PDA_CD"+i);      
                    
                    workYn[i]           = (String)model.get("WORK_YN"+i);                 
                    rjType[i]           = (String)model.get("RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("CONF_YN"+i);      
                    
                    unitAmt[i]          = (String)model.get("UNIT_AMT"+i);                 
                    amt[i]              = (String)model.get("AMT"+i);          
                    eaCapa[i]           = (String)model.get("EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("BOX_BARCODE"+i);      
                    inWorkUomId[i]       = (String)model.get("IN_WORK_UOM_ID"+i);      
                    
                    outOrdUomId[i]      = (String)model.get("OUT_ORD_UOM_ID"+i);                 
                    inWorkOrdQty[i]         = (String)model.get("IN_WORK_ORD_QTY"+i);          
                    outOrdQty[i]        = (String)model.get("OUT_ORD_QTY"+i);      
                    refSubLotId[i]      = (String)model.get("REF_SUB_LOT_ID"+i);      
                    dspId[i]            = (String)model.get("DSP_ID"+i);      
                    
                    carId[i]            = (String)model.get("CAR_ID"+i);                 
                    cntrId[i]           = (String)model.get("CNTR_ID"+i);          
                    cntrNo[i]           = (String)model.get("CNTR_NO"+i);      
                    cntrType[i]         = (String)model.get("CNTR_TYPE"+i);      
                    badQty[i]           = (String)model.get("BAD_QTY"+i);      
                    
                    uomNm[i]            = (String)model.get("UOM_NM"+i);                 
                    unitPrice[i]        = (String)model.get("UNIT_PRICE"+i);          
                    //whNm[i]             = (String)model.get("WH_NM"+i);      
                    //itemKorNm[i]        = (String)model.get("ITEM_KOR_NM"+i);      
                    //itemEngNm[i]        = (String)model.get("ITEM_ENG_NM"+i);      
                    
                    repUomId[i]         = (String)model.get("REP_UOM_ID"+i);                 
                    uomCd[i]            = (String)model.get("UOM_CD"+i);          
                    uomId[i]            = (String)model.get("UOM_IF"+i);      
                    repUomCd[i]         = (String)model.get("REP_UOM_CD"+i);      
                    repuomNm[i]         = (String)model.get("REP_UOM_NM"+i);      
                    
                    itemGrpId[i]        = (String)model.get("ITEM_GRP_ID"+i);                 
                    expiryDate[i]       = (String)model.get("EXPIRY_DATE"+i);          
                    inOrdWeight[i]      = (String)model.get("IN_ORD_WEIGHT"+i);      
                    ordDesc[i]          = (String)model.get("ORD_DESC"+i);      
                    validDt[i]          = (String)model.get("VALID_DT"+i);      
                    
                    etc2[i]             = (String)model.get("ETC2"+i); 
                    inOrdUomId[i]       = (String)model.get("IN_ORD_UOM_ID"+i); 
                    
                    itemBestDate[i]     = (String)model.get("ITEM_BEST_DATE"+i);
                    
                    // itemBestDateEnd[i]  = (String)model.get("ITEM_BEST_DATE_END"+i);
                    if ( model.get("ITEM_BEST_DATE_END" +i) != null ) { 
                    	itemBestDateEnd[i] = ((String)model.get("ITEM_BEST_DATE_END" +i)).trim().replaceAll("-", "");
                    } else {
                    	itemBestDateEnd[i] = (String)model.get("ITEM_BEST_DATE_END" +i);
                    } 
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
                modelIns.put("vrOrdId"          , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));
                
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                modelIns.put("orgOrdId"         , model.get("orgOrdId"));
                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
                modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
                modelIns.put("outWhId"          , model.get("outWhId"));
                
                modelIns.put("transCustId"      , model.get("transCustId"));
                modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
                modelIns.put("blNo"             , model.get("vrBlNo"));
                modelIns.put("workStat"         , model.get("workStat"));
                
                modelIns.put("ordType"          , model.get("ordType"));
                modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
                modelIns.put("outReqDt"         , model.get("outReqDt"));
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));
                
                modelIns.put("workSeq"          , model.get("workSeq"));
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("kinOutYn"));
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                modelIns.put("approveYn"        , model.get("vrApproveYn"));
                modelIns.put("payYn"            , model.get("payYn"));
                modelIns.put("inCustId"         , model.get("vrInCustId"));
                modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
                
                modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
                modelIns.put("inCustTel"        , model.get("vrInCustTel"));
                
                //sub
                modelIns.put("dsSub_rowStatus"  , dsSubRowStatus);
                modelIns.put("ordSeq"           , ordSeq);
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);
                
                modelIns.put("realOutQty"       , realOutQty);
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);
                
                modelIns.put("workYn"           , workYn);
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);
                
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inWorkUomId"       , inWorkUomId);
                
                modelIns.put("outOrdUomId"      , outOrdUomId);
                modelIns.put("inWorkOrdQty"         , inWorkOrdQty);
                modelIns.put("outOrdQty"        , outOrdQty);
                modelIns.put("refSubLotId"      , refSubLotId);
                modelIns.put("dspId"            , dspId);
                
                modelIns.put("carId"            , carId);
                modelIns.put("cntrId"           , cntrId);
                modelIns.put("cntrNo"           , cntrNo);
                modelIns.put("cntrType"         , cntrType);
                modelIns.put("badQty"           , badQty);
                
                modelIns.put("uomNm"            , uomNm);
                modelIns.put("unitPrice"        , unitPrice);
                //modelIns.put("whNm"             , whNm);
                //modelIns.put("itemKorNm"        , itemKorNm);
                //modelIns.put("itemEngNm"        , itemEngNm);
                
                modelIns.put("repUomId"         , repUomId);
                modelIns.put("uomCd"            , uomCd);
                modelIns.put("uomId"            , uomId);
                modelIns.put("repUomCd"         , repUomCd);
                modelIns.put("repuomNm"         , repuomNm);
                
                modelIns.put("itemGrpId"        , itemGrpId);
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("inOrdWeight"      , inOrdWeight); 
                
                modelIns.put("ordDesc"          , ordDesc);
                modelIns.put("validDt"          , validDt);                
                modelIns.put("etc2"             , etc2);
                modelIns.put("inOrdUomId"       , inOrdUomId);
                
                //추가된거(아직프로시져는 안탐)
                modelIns.put("itemBestDate"     , itemBestDate);
                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);  
          
                //session 정보
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                //dao                
                modelIns = (Map<String, Object>)dao.saveSimpleInOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
        	m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
                        
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : saveOutOrder
     * 대체 Method 설명    : 출고주문(저장,수정,삭제)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
        	
            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            JsonParser jsonParser = new JsonParser();
            if(insCnt > 0){
                //저장, 수정         
                String[] ordSeq         = new String[insCnt];         
                String[] ritemId        = new String[insCnt];     
                String[] custLotNo      = new String[insCnt];     
                String[] realInQty      = new String[insCnt];     
                
                String[] realOutQty     = new String[insCnt];                     
                String[] makeDt         = new String[insCnt];         
                String[] timePeriodDay  = new String[insCnt];     
                String[] locYn          = new String[insCnt];     
                String[] pdaCd          = new String[insCnt];     
                
                String[] workYn         = new String[insCnt];                
                String[] rjType         = new String[insCnt];         
                String[] realPltQty     = new String[insCnt];     
                String[] realBoxQty     = new String[insCnt];     
                String[] confYn         = new String[insCnt];     
                
                //String[] epType        = new String[insCnt];                
                String[] unitAmt        = new String[insCnt];                
                String[] amt            = new String[insCnt];         
                String[] eaCapa         = new String[insCnt];     
                String[] boxBarcode     = new String[insCnt];     
                String[] inOrdUomId     = new String[insCnt];     
                String[] inWorkUomId     = new String[insCnt]; 
                
                String[] outOrdUomId    = new String[insCnt];
                String[] outWorkUomId    = new String[insCnt];  
                String[] minUomId       = new String[insCnt]; 
                String[] inOrdQty       = new String[insCnt];
                String[] inWorkOrdQty       = new String[insCnt];  
                String[] outOrdQty      = new String[insCnt]; 
                String[] outWorkOrdQty       = new String[insCnt];  
                String[] inDspQty       = new String[insCnt]; 
                
                String[] outDspQty      = new String[insCnt];   
                String[] refSubLotId    = new String[insCnt];   
                String[] outOrdWeight   = new String[insCnt];   
                String[] ordDesc        = new String[insCnt];   
                String[] etc2           = new String[insCnt];
                String[] userMemo		= new String[insCnt];
                
                //추가
                String[] itemBestDate       = new String[insCnt];   //상품유효기간     
                String[] itemBestDateEnd    = new String[insCnt];   //상품유효기간만료일
                
                String[] cntrId         = new String[insCnt];
                String[] cntrNo         = new String[insCnt];
                String[] cntrType       = new String[insCnt];
                String[] cntrSealNo     = new String[insCnt];
                                
                for(int i = 0 ; i < insCnt ; i ++){        
                    ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);          
                    ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
                    
                    realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
                    makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("I_LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
                    
                    workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
                    rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("I_CONF_YN"+i);      
                    
                    //epType[i]          = (String)model.get("I_EP_TYPE"+i);                 
                    unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
                    amt[i]              = (String)model.get("I_AMT"+i);          
                    eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
                    inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);
                    inWorkUomId[i]       = (String)model.get("I_IN_WORK_UOM_ID"+i); 
                    //inWorkUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i); 
                    outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);
                    outWorkUomId[i]      = (String)model.get("I_OUT_WORK_UOM_ID"+i);   
                    //outWorkUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i); 
                    minUomId[i]         = (String)model.get("I_MIN_UOM_ID"+i);                
                    inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
                    inWorkOrdQty[i]         = (String)model.get("I_IN_WORK_ORD_QTY"+i);
                    //inWorkOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
                    outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);    
                    outWorkOrdQty[i]        = (String)model.get("I_OUT_WORK_ORD_QTY"+i);
                    //outWorkOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);
                    inDspQty[i]         = (String)model.get("I_IN_DSP_QTY"+i);
                    
                    outDspQty[i]        = (String)model.get("I_OUT_DSP_QTY"+i);
                    refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
                    outOrdWeight[i]     = (String)model.get("I_OUT_ORD_WEIGHT"+i);     
                    ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);     
                    etc2[i]             = (String)model.get("I_ETC2"+i); 
                    // 대화물류 컬럼 추가 USER_MEMO
                    if(model.containsKey("I_USER_MEMO1"+i) && model.containsKey("I_USER_MEMO2"+i) && model.containsKey("I_USER_MEMO3"+i) && model.containsKey("I_USER_MEMO4"+i) &&model.containsKey("I_USER_MEMO5"+i) ){
                    	JSONObject memoJson = new JSONObject();
                    	memoJson.put("1",model.get("I_USER_MEMO1"+i));
                    	memoJson.put("2",model.get("I_USER_MEMO2"+i));
                    	memoJson.put("3",model.get("I_USER_MEMO3"+i));
                    	memoJson.put("4",model.get("I_USER_MEMO4"+i));
                    	memoJson.put("5",model.get("I_USER_MEMO5"+i));
                    	
                    	userMemo[i] = memoJson.toString();
                    	//userMemo[i]	= (String)model.get("I_USER_MEMO"+i);
                    }
                    else{
                    	userMemo[i]	= null;
                    }
                    
                    
                    //추가 
                    itemBestDate[i]     = (String)model.get("I_ITEM_BEST_DATE"+i);      
                    // itemBestDateEnd[i]  = (String)model.get("I_ITEM_BEST_DATE_END"+i);
                    
                    if ( model.get("I_ITEM_BEST_DATE_END" +i) != null ) { 
                    	itemBestDateEnd[i] = ((String)model.get("I_ITEM_BEST_DATE_END" +i)).trim().replaceAll("-", "");
                    } else {
                    	itemBestDateEnd[i] = (String)model.get("I_ITEM_BEST_DATE_END" +i);
                    }                    
                    
                    cntrId[i]           = (String)model.get("I_CNTR_ID"+i);
                    cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);
                    cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);
                    cntrSealNo[i]       = (String)model.get("I_CNTR_SEAL_NO"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("ordId"            , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("inReqDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));
                
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));  
                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
                modelIns.put("inWhId"           , model.get("inWhId"));
                modelIns.put("outWhId"          , model.get("vrOutWhId"));  
                
                modelIns.put("transCustId"      , model.get("vrTransCustId"));   
                modelIns.put("custId"           , model.get("vrSrchCustId"));   
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));    
                modelIns.put("blNo"             , model.get("vrBlNo"));         
                modelIns.put("workStat"         , model.get("vrWorkStat"));       //하드
                
                modelIns.put("ordType"          , model.get("vrOrdType"));            //하드
                modelIns.put("ordSubtype"       , model.get("vrOrdSubType"));   
                modelIns.put("outReqDt"         , model.get("vrCalOutReqDt").toString().replace("-", ""));  
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));
                
                modelIns.put("workSeq"          , model.get("workSeq"));
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("vrKinOutYn"));      
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                modelIns.put("payYn"            , model.get("vrPayYn"));
                modelIns.put("asnInReqDt"       , model.get("vrCalInReqDt").toString().replace("-", ""));  
                modelIns.put("cntrNoM"          , model.get("vrCntrNoM"));
                modelIns.put("cntrSealNoM"      , model.get("vrCntrSealNoM"));
                
                String calETD = "";
                String calETA = "";
                if ( model.get("vrCalETD") != null ) { 
                	calETD = model.get("vrCalETD").toString().replace("-","");
                }
                if ( model.get("vrCalETA") != null ) { 
                	calETA = model.get("vrCalETA").toString().replace("-","");
                }
                modelIns.put("cntrETD"			, calETD);
                modelIns.put("cntrETA"			, calETA);
                
                //sub
                modelIns.put("ordSeq"           , ordSeq);
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);
                modelIns.put("realOutQty"       , realOutQty);
                
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);
                modelIns.put("workYn"           , workYn);
                
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);
                modelIns.put("epType"           , model.get("vrEpType"));
                modelIns.put("unitAmt"          , unitAmt);
                
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inOrdUomId"       , inOrdUomId);
                modelIns.put("inWorkUomId"      , inWorkUomId);

                modelIns.put("outOrdUomId"      , outOrdUomId);
                modelIns.put("outWorkUomId"     , outWorkUomId);
                modelIns.put("minUomId"         , minUomId);
                modelIns.put("inOrdQty"         , inOrdQty);
                modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
                
                modelIns.put("outOrdQty"        , outOrdQty);
                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
                modelIns.put("inDspQty"         , inDspQty);
                modelIns.put("outDspQty"        , outDspQty);
                modelIns.put("refSubLotId"      , refSubLotId);
                
                modelIns.put("outOrdWeight"     , outOrdWeight);                
                modelIns.put("cntrId"           , cntrId);
                modelIns.put("cntrNo"           , cntrNo);
                modelIns.put("cntrType"         , cntrType);
                modelIns.put("cntrSealNo"       , cntrSealNo);
                
                modelIns.put("ordDesc"          , ordDesc);             
                modelIns.put("etc2"             , etc2);
                modelIns.put("userMemo"			, userMemo);
                //추가된거(아직프로시져는 안탐)
                modelIns.put("itemBestDate"     , itemBestDate);
                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);                
                
                //session 정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOutOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            //등록 수정 끝
            if(delCnt > 0 ){
                String[] ordId  = new String[delCnt];         
                String[] ordSeq = new String[delCnt]; 
                for(int i = 0 ; i < delCnt ; i ++){
                    ordId[i]  = (String)model.get("D_ORD_ID"+i);               
                    ordSeq[i] = (String)model.get("D_ORD_SEQ"+i);          
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelDel = new HashMap<String, Object>();
                modelDel.put("ordId", ordId);
                modelDel.put("ordSeq", ordSeq);

                modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                modelDel = (Map<String, Object>)dao.deleteOutOrder(modelDel);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
            }          
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
  
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    /**
     * 
     * 대체 Method ID   : saveOutOrderDH
     * 대체 Method 설명    : 출고주문(저장,수정,삭제) - 대화물류
     * 작성자                      : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutOrderDH(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	// log.info(model);
    	try{
    		
    		int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
    		int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
    		JsonParser jsonParser = new JsonParser();
    		if(insCnt > 0){
    			//저장, 수정         
    			String[] ordSeq         = new String[insCnt];         
    			String[] ritemId        = new String[insCnt];     
    			String[] transCustId    = new String[insCnt];     
    			String[] custLotNo      = new String[insCnt];     
    			String[] realInQty      = new String[insCnt];     
    			
    			String[] realOutQty     = new String[insCnt];                     
    			String[] makeDt         = new String[insCnt];         
    			String[] timePeriodDay  = new String[insCnt];     
    			String[] locYn          = new String[insCnt];     
    			String[] pdaCd          = new String[insCnt];     
    			
    			String[] workYn         = new String[insCnt];                
    			String[] rjType         = new String[insCnt];         
    			String[] realPltQty     = new String[insCnt];     
    			String[] realBoxQty     = new String[insCnt];     
    			String[] confYn         = new String[insCnt];     
    			
    			String[] epType        = new String[insCnt];                
    			String[] unitAmt        = new String[insCnt];                
    			String[] amt            = new String[insCnt];         
    			String[] eaCapa         = new String[insCnt];     
    			String[] boxBarcode     = new String[insCnt];     
    			String[] inOrdUomId     = new String[insCnt];     
    			String[] inWorkUomId     = new String[insCnt]; 
    			
    			String[] outOrdUomId    = new String[insCnt];
    			String[] outWorkUomId    = new String[insCnt];  
    			String[] minUomId       = new String[insCnt]; 
    			String[] inOrdQty       = new String[insCnt];
    			String[] inWorkOrdQty       = new String[insCnt];  
    			String[] outOrdQty      = new String[insCnt]; 
    			String[] outWorkOrdQty       = new String[insCnt];  
    			String[] inDspQty       = new String[insCnt]; 
    			
    			String[] outDspQty      = new String[insCnt];   
    			String[] refSubLotId    = new String[insCnt];   
    			String[] outOrdWeight   = new String[insCnt];   
    			String[] ordDesc        = new String[insCnt];   
    			String[] etc2           = new String[insCnt];
    			String[] userMemo		= new String[insCnt];
    			
    			//추가
    			String[] itemBestDate       = new String[insCnt];   //상품유효기간     
    			String[] itemBestDateEnd    = new String[insCnt];   //상품유효기간만료일
    			
    			String[] cntrId         = new String[insCnt];
    			String[] cntrNo         = new String[insCnt];
    			String[] cntrType       = new String[insCnt];
    			String[] cntrSealNo     = new String[insCnt];
    			
    			for(int i = 0 ; i < insCnt ; i ++){        
    				ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);          
    				ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
    				transCustId[i]      = (String)model.get("I_TRANS_CUST_ID"+i);      
    				custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
    				realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
    				
    				realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
    				makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
    				timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
    				locYn[i]            = (String)model.get("I_LOC_YN"+i);      
    				pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
    				
    				workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
    				rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
    				realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
    				realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
    				confYn[i]           = (String)model.get("I_CONF_YN"+i);      
    				
    				epType[i]          = (String)model.get("I_EP_TYPE"+i);                 
    				unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
    				amt[i]              = (String)model.get("I_AMT"+i);          
    				eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
    				boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
    				inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);
    				inWorkUomId[i]       = (String)model.get("I_IN_WORK_UOM_ID"+i); 
    				//inWorkUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i); 
    				outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);
    				outWorkUomId[i]      = (String)model.get("I_OUT_WORK_UOM_ID"+i);   
    				//outWorkUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i); 
    				minUomId[i]         = (String)model.get("I_MIN_UOM_ID"+i);                
    				inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
    				inWorkOrdQty[i]         = (String)model.get("I_IN_WORK_ORD_QTY"+i);
    				//inWorkOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
    				outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);    
    				outWorkOrdQty[i]        = (String)model.get("I_OUT_WORK_ORD_QTY"+i);
    				//outWorkOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);
    				inDspQty[i]         = (String)model.get("I_IN_DSP_QTY"+i);
    				
    				outDspQty[i]        = (String)model.get("I_OUT_DSP_QTY"+i);
    				refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
    				outOrdWeight[i]     = (String)model.get("I_OUT_ORD_WEIGHT"+i);     
    				ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);     
    				etc2[i]             = (String)model.get("I_ETC2"+i); 
    				// 대화물류 컬럼 추가 USER_MEMO
    				if(model.containsKey("I_USER_MEMO1"+i) && model.containsKey("I_USER_MEMO2"+i) && model.containsKey("I_USER_MEMO3"+i) && model.containsKey("I_USER_MEMO4"+i) &&model.containsKey("I_USER_MEMO5"+i) ){
    					JSONObject memoJson = new JSONObject();
    					memoJson.put("1",model.get("I_USER_MEMO1"+i));
    					memoJson.put("2",model.get("I_USER_MEMO2"+i));
    					memoJson.put("3",model.get("I_USER_MEMO3"+i));
    					memoJson.put("4",model.get("I_USER_MEMO4"+i));
    					memoJson.put("5",model.get("I_USER_MEMO5"+i));
    					
    					userMemo[i] = memoJson.toString();
    					//userMemo[i]	= (String)model.get("I_USER_MEMO"+i);
    				}
    				else{
    					userMemo[i]	= null;
    				}
    				
    				
    				//추가 
    				itemBestDate[i]     = (String)model.get("I_ITEM_BEST_DATE"+i);      
    				// itemBestDateEnd[i]  = (String)model.get("I_ITEM_BEST_DATE_END"+i);
    				
    				if ( model.get("I_ITEM_BEST_DATE_END" +i) != null ) { 
    					itemBestDateEnd[i] = ((String)model.get("I_ITEM_BEST_DATE_END" +i)).trim().replaceAll("-", "");
    				} else {
    					itemBestDateEnd[i] = (String)model.get("I_ITEM_BEST_DATE_END" +i);
    				}                    
    				
    				cntrId[i]           = (String)model.get("I_CNTR_ID"+i);
    				cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);
    				cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);
    				cntrSealNo[i]       = (String)model.get("I_CNTR_SEAL_NO"+i);
    			}
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    			
    			//main
    			modelIns.put("ordId"            , model.get("vrOrdId"));
    			modelIns.put("inReqDt"          , model.get("inReqDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
    			modelIns.put("inDt"             , model.get("inDt"));
    			modelIns.put("custPoid"         , model.get("custPoid"));
    			
    			modelIns.put("custPoseq"        , model.get("custPoseq"));
    			modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));  
    			modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
    			modelIns.put("inWhId"           , model.get("inWhId"));
    			modelIns.put("outWhId"          , model.get("vrOutWhId"));  
    			
    			modelIns.put("transCustId"      , model.get("vrTransCustId"));   
    			modelIns.put("custId"           , model.get("vrSrchCustId"));   
    			modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));    
    			modelIns.put("blNo"             , model.get("vrBlNo"));         
    			modelIns.put("workStat"         , model.get("vrWorkStat"));       //하드
    			
    			modelIns.put("ordType"          , model.get("vrOrdType"));            //하드
    			modelIns.put("ordSubtype"       , model.get("vrOrdSubType"));   
    			modelIns.put("outReqDt"         , model.get("vrCalOutReqDt").toString().replace("-", ""));  
    			modelIns.put("outDt"            , model.get("outDt"));
    			modelIns.put("pdaStat"          , model.get("pdaStat"));
    			
    			modelIns.put("workSeq"          , model.get("workSeq"));
    			modelIns.put("capaTot"          , model.get("capaTot"));
    			modelIns.put("kinOutYn"         , model.get("vrKinOutYn"));      
    			modelIns.put("carConfYn"        , model.get("carConfYn"));
    			modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
    			
    			modelIns.put("tplOrdId"         , model.get("tplOrdId"));
    			modelIns.put("payYn"            , model.get("vrPayYn"));
    			modelIns.put("asnInReqDt"       , model.get("vrCalInReqDt").toString().replace("-", ""));  
    			modelIns.put("cntrNoM"          , model.get("vrCntrNoM"));
    			modelIns.put("cntrSealNoM"      , model.get("vrCntrSealNoM"));
    			modelIns.put("epType"           , null);
    			modelIns.put("cntrETD"			, model.get("vrCalETD").toString().replace("-",""));
    			modelIns.put("cntrETA"			, model.get("vrCalETA").toString().replace("-",""));
    			
    			//sub
    			modelIns.put("ordSeq"           , ordSeq);
    			modelIns.put("ritemId"          , ritemId);
//    			modelIns.put("transCustId"      , null);
    			modelIns.put("custLotNo"        , custLotNo);
    			modelIns.put("realInQty"        , realInQty);
    			modelIns.put("realOutQty"       , realOutQty);
    			
    			modelIns.put("makeDt"           , makeDt);
    			modelIns.put("timePeriodDay"    , timePeriodDay);
    			modelIns.put("locYn"            , locYn);
    			modelIns.put("pdaCd"            , pdaCd);
    			modelIns.put("workYn"           , workYn);
    			
    			modelIns.put("rjType"           , rjType);
    			modelIns.put("realPltQty"       , realPltQty);
    			modelIns.put("realBoxQty"       , realBoxQty);
    			modelIns.put("confYn"           , confYn);
    			modelIns.put("unitAmt"          , unitAmt);
    			
    			modelIns.put("amt"              , amt);
    			modelIns.put("eaCapa"           , eaCapa);
    			modelIns.put("boxBarcode"       , boxBarcode);
    			modelIns.put("inOrdUomId"       , inOrdUomId);
    			modelIns.put("inWorkUomId"      , inWorkUomId);
    			
    			modelIns.put("outOrdUomId"      , outOrdUomId);
    			modelIns.put("outWorkUomId"     , outWorkUomId);
    			modelIns.put("minUomId"         , minUomId);
    			modelIns.put("inOrdQty"         , inOrdQty);
    			modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
    			
    			modelIns.put("outOrdQty"        , outOrdQty);
    			modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
    			modelIns.put("inDspQty"         , inDspQty);
    			modelIns.put("outDspQty"        , outDspQty);
    			modelIns.put("refSubLotId"      , refSubLotId);
    			
    			modelIns.put("outOrdWeight"     , outOrdWeight);                
    			modelIns.put("cntrId"           , cntrId);
    			modelIns.put("cntrNo"           , cntrNo);
    			modelIns.put("cntrType"         , cntrType);
    			modelIns.put("cntrSealNo"       , cntrSealNo);
    			
    			modelIns.put("ordDesc"          , ordDesc);             
    			modelIns.put("etc2"             , etc2);
    			modelIns.put("userMemo"			, userMemo);
    			//추가된거(아직프로시져는 안탐)
    			modelIns.put("itemBestDate"     , itemBestDate);
    			modelIns.put("itemBestDateEnd"  , itemBestDateEnd);   

    			modelIns.put("arrTransCustId"      , transCustId);
    			modelIns.put("arrEpType"           , epType);
    			//session 정보
    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			
    			//dao                
    			modelIns = (Map<String, Object>)dao.saveOutOrderDH(modelIns);
    			ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    		}
    		//등록 수정 끝
    		if(delCnt > 0 ){
    			String[] ordId  = new String[delCnt];         
    			String[] ordSeq = new String[delCnt]; 
    			for(int i = 0 ; i < delCnt ; i ++){
    				ordId[i]  = (String)model.get("D_ORD_ID"+i);               
    				ordSeq[i] = (String)model.get("D_ORD_SEQ"+i);          
    			}
    			
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelDel = new HashMap<String, Object>();
    			modelDel.put("ordId", ordId);
    			modelDel.put("ordSeq", ordSeq);
    			
    			modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			
    			modelDel = (Map<String, Object>)dao.deleteOutOrder(modelDel);
    			ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
    		}          
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(BizException be) {
    		m.put("errCnt", 1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : etcSaveOutOrder
     * 대체 Method 설명    : 아산물류센터 원주문, BL번호 출고주문(수정)
     * 작성자                      : wdy
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> etcSaveOutOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            //main
            modelIns.put("ordId"            , model.get("vrOrdId"));
            modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));     
            modelIns.put("blNo"             , model.get("vrBlNo"));
            modelIns.put("calOutReqDt"      , model.get("vrCalOutReqDt").toString().replace("-", "")); 
            modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                         
            //session 정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            //dao                
            modelIns = (Map<String, Object>)dao.etcSaveOutOrder(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                      
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
  
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : etcSaveInOrder
     * 대체 Method 설명    : 아산물류센터 입고일자(수정)
     * 작성자                      : wdy
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> etcSaveInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            //main
            modelIns.put("ordId"            , model.get("vrOrdId"));
            modelIns.put("calInDt"      	, model.get("calInDt").toString().replace("-", "")); 
            modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                         
            //session 정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            //dao                
            modelIns = (Map<String, Object>)dao.etcSaveInOrder(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                      
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
  
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : saveSimpleOutOrder
     * 대체 Method 설명    : 간편출고주문(저장)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSimpleOutOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            if(totCnt > 0){
                //저장, 수정            
                String[] ordSeq         = new String[totCnt];         
                String[] ritemId        = new String[totCnt];     
                String[] custLotNo      = new String[totCnt];     
                String[] realInQty      = new String[totCnt];     
                String[] realOutQty     = new String[totCnt];       //5
                
                String[] makeDt         = new String[totCnt];         
                String[] timePeriodDay  = new String[totCnt];     
                String[] locYn          = new String[totCnt];     
                String[] pdaCd          = new String[totCnt];                
                String[] workYn         = new String[totCnt];       //10
                
                String[] rjType         = new String[totCnt];         
                String[] realPltQty     = new String[totCnt];     
                String[] realBoxQty     = new String[totCnt];     
                String[] confYn         = new String[totCnt];   
                String[] unitAmt        = new String[totCnt];       //15
                
                String[] amt            = new String[totCnt];         
                String[] eaCapa         = new String[totCnt];     
                String[] boxBarcode     = new String[totCnt];     
                String[] inOrdUomId     = new String[totCnt];     
                String[] outOrdUomId    = new String[totCnt];       //20
                
                String[] minUomId       = new String[totCnt];
                String[] inOrdQty       = new String[totCnt];         
                String[] outWorkOrdQty      = new String[totCnt];   
                String[] inDspQty       = new String[totCnt];
                String[] outDspQty      = new String[totCnt];       //25
                                       
                String[] refSubLotId    = new String[totCnt];     
                String[] badQty         = new String[totCnt];     
                String[] uomNm          = new String[totCnt];                
                String[] unitPrice      = new String[totCnt];         
                String[] whNm           = new String[totCnt];       //30
               
                //String[] itemKorNm      = new String[totCnt];     
                //String[] itemEngNm      = new String[totCnt];      
                String[] repUomId       = new String[totCnt];                
                String[] uomCd          = new String[totCnt];         
                String[] uomId          = new String[totCnt];       //35
               
                String[] repUomCd       = new String[totCnt];     
                String[] repuomNm       = new String[totCnt];     
                String[] itemGrpId      = new String[totCnt];                
                String[] expiryDate     = new String[totCnt];       //39
                
                String[] ordDesc        = new String[totCnt];    
                String[] etc2           = new String[totCnt];       //2
                
                //추가
                String[] timeDate      = new String[totCnt];   //상품유효기간     
                String[] timeDateEnd  = new String[totCnt];   //상품유효기간만료일
                String[] timeUseEnd   = new String[totCnt];   //소비가한만료일
                
                
                for(int i = 0 ; i < totCnt ; i ++){             
                    ordSeq[i]           = (String)model.get("ORD_SEQ"+i);          
                    ritemId[i]          = (String)model.get("RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("REAL_IN_QTY"+i);                          
                    realOutQty[i]       = (String)model.get("REAL_OUT_QTY"+i);        
                    
                    makeDt[i]           = (String)model.get("MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("PDA_CD"+i);                          
                    workYn[i]           = (String)model.get("WORK_YN"+i);         
                    
                    rjType[i]           = (String)model.get("RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("CONF_YN"+i);                          
                    unitAmt[i]          = (String)model.get("UNIT_AMT"+i);       
                    
                    amt[i]              = (String)model.get("AMT"+i);          
                    eaCapa[i]           = (String)model.get("EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("BOX_BARCODE"+i);      
                    inOrdUomId[i]       = (String)model.get("IN_ORD_UOM_ID"+i);                        
                    outOrdUomId[i]      = (String)model.get("OUT_ORD_UOM_ID"+i);    
                    
                    minUomId[i]         = (String)model.get("MIN_UOM_ID"+i);    
                    inOrdQty[i]         = (String)model.get("IN_ORD_QTY"+i);          
                    outWorkOrdQty[i]        = (String)model.get("OUT_WORK_ORD_QTY"+i);    
                    inDspQty[i]         = (String)model.get("IN_DSP_QTY"+i);          
                    outDspQty[i]        = (String)model.get("OUT_DSP_QTY"+i);                        
                    
                    refSubLotId[i]      = (String)model.get("REF_SUB_LOT_ID"+i);      
                    badQty[i]           = (String)model.get("BAD_QTY"+i);                          
                    uomNm[i]            = (String)model.get("UOM_NM"+i);  
                    unitPrice[i]        = (String)model.get("UNIT_PRICE"+i);          
                    whNm[i]             = (String)model.get("WH_NM"+i);      
                    
                    //itemKorNm[i]        = (String)model.get("ITEM_KOR_NM"+i);      
                    //itemEngNm[i]        = (String)model.get("ITEM_ENG_NM"+i);  
                    repUomId[i]         = (String)model.get("REP_UOM_ID"+i);                 
                    uomCd[i]            = (String)model.get("UOM_CD"+i);          
                    uomId[i]            = (String)model.get("UOM_IF"+i);      
                    
                    repUomCd[i]         = (String)model.get("REP_UOM_CD"+i);      
                    repuomNm[i]         = (String)model.get("REP_UOM_NM"+i); 
                    itemGrpId[i]        = (String)model.get("ITEM_GRP_ID"+i);                 
                    expiryDate[i]       = (String)model.get("EXPIRY_DATE"+i);  
                    
                    ordDesc[i]          = (String)model.get("ORD_DESC"+i); 
                    etc2[i]             = (String)model.get("ETC2"+i); 
                    
                    timeDate[i]        = (String)model.get("TIME_DATE"+i);      
                    timeDateEnd[i]    = (String)model.get("TIME_DATE_END"+i);      
                    timeUseEnd[i]     = (String)model.get("TIME_USE_END"+i);  
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("ordId"            , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("inReqDt"));
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));                
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                
                modelIns.put("orgOrdId"         , model.get("orgOrdId"));
                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
                modelIns.put("inWhId"           , model.get("inWhId"));
                modelIns.put("outWhId"          , model.get("vrOutWhId"));             
                modelIns.put("transCustId"      , model.get("vrTransCustId"));
                
                modelIns.put("custId"           , model.get("vrSrchCustId"));       
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
                modelIns.put("blNo"             , model.get("blNo"));
                modelIns.put("workStat"         , model.get("workStat"));                
                modelIns.put("ordType"          , model.get("ordType"));
                
                modelIns.put("ordSubtype"       , model.get("vrOrdSubType"));   
                modelIns.put("outReqDt"         , model.get("vrCalOutReqDt2").toString().replace("-", ""));   
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));                
                modelIns.put("workSeq"          , model.get("workSeq"));
                
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("kinOutYn"));
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("lcId"             , (String)model.get(ConstantIF.SS_SVC_NO));                
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                
                modelIns.put("payYn"            , model.get("vrPayYn"));  
                
                //sub
                modelIns.put("ordSeq"           , ordSeq);
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);                
                modelIns.put("realOutQty"       , realOutQty);
                
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);                
                modelIns.put("workYn"           , workYn);
                
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);                
                modelIns.put("unitAmt"          , unitAmt);
                
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inOrdUomId"       , inOrdUomId);                
                modelIns.put("outOrdUomId"      , outOrdUomId);
                
                modelIns.put("minUomId"         , minUomId);
                modelIns.put("inOrdQty"         , inOrdQty);
                modelIns.put("outWorkOrdQty"        , outWorkOrdQty);
                modelIns.put("inDspQty"         , inDspQty);
                modelIns.put("outDspQty"        , outDspQty);
                
                modelIns.put("refSubLotId"      , refSubLotId);
                modelIns.put("badQty"           , badQty);                
                modelIns.put("uomNm"            , uomNm);
                modelIns.put("unitPrice"        , unitPrice);
                modelIns.put("whNm"             , whNm);
                
                //modelIns.put("itemKorNm"        , itemKorNm);
                //modelIns.put("itemEngNm"        , itemEngNm);                
                modelIns.put("repUomId"         , repUomId);
                modelIns.put("uomCd"            , uomCd);
                modelIns.put("uomId"            , uomId);
                
                modelIns.put("repUomCd"         , repUomCd);
                modelIns.put("repuomNm"         , repuomNm);                
                modelIns.put("itemGrpId"        , itemGrpId);
                modelIns.put("expiryDate"       , expiryDate);
                
                modelIns.put("ordDesc"          , ordDesc);          
                modelIns.put("etc2"             , etc2);
                
                //추가된거(아직프로시져는 안탐)
                modelIns.put("time_date"        , timeDate);
                modelIns.put("time_date_end"    , timeDateEnd);                
                modelIns.put("time_use_end"     , timeUseEnd);  
        
                //session 정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveSimpleOutOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }                     
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : etcSaveV2
     * 대체 Method 설명    : 아산물류센터 원주문, BL번호 출고주문(수정)
     * 작성자                      : wdy
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> etcSaveV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            //main
            modelIns.put("ordId"            , model.get("vrOrdId"));
            modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));     
            modelIns.put("transCustId"      , model.get("vrTransCustId"));
            modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
            //session 정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            //dao                
            dao.etcSaveV2(modelIns);
            modelIns.put("O_MSG_CODE"      , 0);
            modelIns.put("O_MSG_NAME"      , model.get("vrOrdId"));
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                      
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
  
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    

    /**
     * 분할전
     * 대체 Method ID   : saveInOrderV2
     * 대체 Method 설명    : 입고주문(저장,수정,삭제)V2
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrderV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
                //저장, 수정
                String[] dsSubRowStatus = new String[insCnt];                
                String[] ordSeq         = new String[insCnt];         
                String[] ritemId        = new String[insCnt];     
                String[] custLotNo      = new String[insCnt];     
                String[] realInQty      = new String[insCnt];     
                
                String[] realOutQty     = new String[insCnt];                     
                String[] makeDt         = new String[insCnt];         
                String[] timePeriodDay  = new String[insCnt];     
                String[] locYn          = new String[insCnt];     
                String[] pdaCd          = new String[insCnt];     
                
                String[] workYn         = new String[insCnt];                
                String[] rjType         = new String[insCnt];         
                String[] realPltQty     = new String[insCnt];     
                String[] realBoxQty     = new String[insCnt];     
                String[] confYn         = new String[insCnt];     
                
                String[] unitAmt        = new String[insCnt];                
                String[] amt            = new String[insCnt];         
                String[] eaCapa         = new String[insCnt];     
                String[] boxBarcode     = new String[insCnt];     
                String[] inOrdUomId     = new String[insCnt];     
                
                String[] inWorkUomId 	= new String[insCnt]; 
                String[] outOrdUomId    = new String[insCnt];       
                String[] outWorkUomId	= new String[insCnt]; 
                String[] inOrdQty       = new String[insCnt]; 
                String[] inWorkOrdQty   = new String[insCnt]; 
                
                String[] outOrdQty      = new String[insCnt];     
                String[] outWorkOrdQty  = new String[insCnt]; 
                String[] refSubLotId    = new String[insCnt];     
                String[] dspId          = new String[insCnt];     
                String[] carId          = new String[insCnt];                
                
                String[] cntrId         = new String[insCnt];         
                String[] cntrNo         = new String[insCnt];     
                String[] cntrType       = new String[insCnt];     
                String[] badQty         = new String[insCnt];     
                String[] uomNm          = new String[insCnt];                
                
                String[] unitPrice      = new String[insCnt];         
                String[] whNm           = new String[insCnt];     
                String[] itemKorNm      = new String[insCnt];     
                String[] itemEngNm      = new String[insCnt];     
                String[] repUomId       = new String[insCnt];                
                
                String[] uomCd          = new String[insCnt];         
                String[] uomId          = new String[insCnt];     
                String[] repUomCd       = new String[insCnt];     
                String[] repuomNm       = new String[insCnt];     
                String[] itemGrpId      = new String[insCnt];                
                
                String[] expiryDate     = new String[insCnt];         
                String[] inOrdWeight    = new String[insCnt];                 
                String[] unitNo         = new String[insCnt];
                String[] ordDesc        = new String[insCnt];
                String[] validDt        = new String[insCnt];
                
                String[] etc2           = new String[insCnt];
                
                //추가
                String[] itemBestDate     = new String[insCnt];   //상품유효기간     
                String[] itemBestDateEnd  = new String[insCnt];   //상품유효기간만료일
                String[] rtiNm            = new String[insCnt];   //물류기기명
                
                for(int i = 0 ; i < insCnt ; i ++){
                    dsSubRowStatus[i]  = (String)model.get("I_ST_GUBUN"+i);               
                    ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);          
                    ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
                    
                    realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
                    makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("I_LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
                    
                    workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
                    rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("I_CONF_YN"+i);      
                    
                    unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
                    amt[i]              = (String)model.get("I_AMT"+i);          
                    eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
                    inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);      
                    inWorkUomId[i]      = (String)model.get("I_IN_WORK_UOM_ID"+i);
                    outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);                 
                    outWorkUomId[i]     = (String)model.get("I_OUT_WORK_UOM_ID"+i);
                    inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);          
                    inWorkOrdQty[i]     = (String)model.get("I_IN_WORK_ORD_QTY"+i);
                    outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);     
                    outWorkOrdQty[i]    = (String)model.get("I_OUT_WORK_ORD_QTY"+i); 
                    refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
                    dspId[i]            = (String)model.get("I_DSP_ID"+i);      
                    
                    carId[i]            = (String)model.get("I_CAR_ID"+i);                 
                    cntrId[i]           = (String)model.get("I_CNTR_ID"+i);          
                    cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);      
                    cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);      
                    badQty[i]           = (String)model.get("I_BAD_QTY"+i);      
                    
                    uomNm[i]            = (String)model.get("I_UOM_NM"+i);                 
                    unitPrice[i]        = (String)model.get("I_UNIT_PRICE"+i);          
                    whNm[i]             = (String)model.get("I_WH_NM"+i);      
                    itemKorNm[i]        = (String)model.get("I_ITEM_KOR_NM"+i);      
                    itemEngNm[i]        = (String)model.get("I_ITEM_ENG_NM"+i);      
                    
                    repUomId[i]         = (String)model.get("I_REP_UOM_ID"+i);                 
                    uomCd[i]            = (String)model.get("I_UOM_CD"+i);          
                    uomId[i]            = (String)model.get("I_UOM_ID"+i);      
                    repUomCd[i]         = (String)model.get("I_REP_UOM_CD"+i);      
                    repuomNm[i]         = (String)model.get("I_REP_UOM_NM"+i);      
                    
                    itemGrpId[i]        = (String)model.get("I_ITEM_GRP_ID"+i);                 
                    expiryDate[i]       = (String)model.get("I_EXPIRY_DATE"+i);          
                    inOrdWeight[i]      = (String)model.get("I_IN_ORD_WEIGHT"+i);      
                    unitNo[i]           = (String)model.get("I_UNIT_NO"+i);      
                    ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);
                    
                    validDt[i]          = (String)model.get("I_VALID_DT"+i);      
                    
                    etc2[i]             = (String)model.get("I_ETC2"+i); 
                    
                    //추가 
                    itemBestDate[i]       = (String)model.get("I_ITEM_BEST_DATE"+i);      
                    itemBestDateEnd[i]    = (String)model.get("I_ITEM_BEST_DATE_END"+i);   
                    rtiNm[i]              = (String)model.get("I_RTI_NM"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
                modelIns.put("vrOrdId"          , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));
                
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));
                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
                modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
                modelIns.put("outWhId"          , model.get("outWhId"));
                
                modelIns.put("transCustId"      , model.get("transCustId"));
                modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
                modelIns.put("blNo"             , model.get("vrBlNo"));
                modelIns.put("workStat"         , model.get("workStat"));
                
                modelIns.put("ordType"          , model.get("ordType"));
                modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
                modelIns.put("outReqDt"         , model.get("outReqDt"));
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));
                
                modelIns.put("workSeq"          , model.get("workSeq"));
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("kinOutYn"));
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                modelIns.put("approveYn"        , model.get("vrApproveYn"));
                modelIns.put("payYn"            , model.get("payYn"));
                modelIns.put("inCustId"         , model.get("vrInCustId"));
                modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
                
                modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
                modelIns.put("inCustTel"        , model.get("vrInCustTel"));
                
                //sub
                modelIns.put("dsSub_rowStatus"  , dsSubRowStatus);
                modelIns.put("ordSeq"           , ordSeq);
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);
                
                modelIns.put("realOutQty"       , realOutQty);
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);
                
                modelIns.put("workYn"           , workYn);
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);
                
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inOrdUomId"       , inOrdUomId);
                modelIns.put("inWorkUomId"   	, inWorkUomId);
                
                modelIns.put("outOrdUomId"      , outOrdUomId);
                modelIns.put("outWorkUomId"  	, outWorkUomId);
                modelIns.put("inOrdQty"         , inOrdQty);
                modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
                modelIns.put("outOrdQty"        , outOrdQty);
                
                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
                modelIns.put("refSubLotId"      , refSubLotId);
                modelIns.put("dspId"            , dspId);                
                modelIns.put("carId"            , carId);
                modelIns.put("cntrId"           , cntrId);
                
                modelIns.put("cntrNo"           , cntrNo);
                modelIns.put("cntrType"         , cntrType);
                modelIns.put("badQty"           , badQty);
                modelIns.put("uomNm"            , uomNm);
                modelIns.put("unitPrice"        , unitPrice);
                
                modelIns.put("whNm"             , whNm);
                modelIns.put("itemKorNm"        , itemKorNm);
                modelIns.put("itemEngNm"        , itemEngNm);
                modelIns.put("repUomId"         , repUomId);
                modelIns.put("uomCd"            , uomCd);
                
                modelIns.put("uomId"            , uomId);
                modelIns.put("repUomCd"         , repUomCd);
                modelIns.put("repuomNm"         , repuomNm);
                modelIns.put("itemGrpId"        , itemGrpId);
                modelIns.put("expiryDate"       , expiryDate);
                
                modelIns.put("inOrdWeight"      , inOrdWeight); 
                modelIns.put("unitNo"           , unitNo);
                modelIns.put("ordDesc"          , ordDesc);
                modelIns.put("validDt"          , validDt);                
                modelIns.put("etc2"             , etc2);
                
                //추가된거(아직프로시져는 안탐)
                modelIns.put("itemBestDate"     , itemBestDate);
                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);               
                modelIns.put("rtiNm"            , rtiNm);
                
                
                //session 정보
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                //dao                
                modelIns = (Map<String, Object>)dao.saveInOrderV2(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }
            //등록 수정 끝            
            if(delCnt > 0 ){
                String[] ordId  = new String[delCnt];         
                String[] ordSeq = new String[delCnt]; 
                for(int i = 0 ; i < delCnt ; i ++){
                    ordId[i]  = (String)model.get("D_ORD_ID"+i);               
                    ordSeq[i] = (String)model.get("D_ORD_SEQ"+i);          
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelDel = new HashMap<String, Object>();
                modelDel.put("ordId", ordId);
                modelDel.put("ordSeq", ordSeq);
                
                modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                
                modelDel = (Map<String, Object>)dao.deleteInOrder(modelDel);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * 분할전
     * 대체 Method ID   : saveInOrderV3
     * 대체 Method 설명    : 입고주문(저장,수정,삭제)V3 + 로케이션추가
     * 작성자                      : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrderV3(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	// log.info(model);
    	try{
    		
    		int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
    		int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
    		JsonParser jsonParser = new JsonParser();
    		if(insCnt > 0){
    			//저장, 수정
    			String[] dsSubRowStatus = new String[insCnt];                
    			String[] ordSeq         = new String[insCnt];         
    			String[] ritemId        = new String[insCnt];     
    			String[] custLotNo      = new String[insCnt];     
    			String[] realInQty      = new String[insCnt];     
    			
    			String[] realOutQty     = new String[insCnt];                     
    			String[] makeDt         = new String[insCnt];         
    			String[] timePeriodDay  = new String[insCnt];     
    			String[] locYn          = new String[insCnt];     
    			String[] pdaCd          = new String[insCnt];     
    			
    			String[] workYn         = new String[insCnt];                
    			String[] rjType         = new String[insCnt];         
    			String[] realPltQty     = new String[insCnt];     
    			String[] realBoxQty     = new String[insCnt];     
    			String[] confYn         = new String[insCnt];     
    			
    			String[] unitAmt        = new String[insCnt];                
    			String[] amt            = new String[insCnt];         
    			String[] eaCapa         = new String[insCnt];     
    			String[] boxBarcode     = new String[insCnt];     
    			String[] inOrdUomId     = new String[insCnt];     
    			
    			String[] inWorkUomId 	= new String[insCnt]; 
    			String[] outOrdUomId    = new String[insCnt];       
    			String[] outWorkUomId	= new String[insCnt]; 
    			String[] inOrdQty       = new String[insCnt]; 
    			String[] inWorkOrdQty   = new String[insCnt]; 
    			
    			String[] outOrdQty      = new String[insCnt];     
    			String[] outWorkOrdQty  = new String[insCnt]; 
    			String[] refSubLotId    = new String[insCnt];     
    			String[] dspId          = new String[insCnt];     
    			String[] carId          = new String[insCnt];                
    			
    			String[] cntrId         = new String[insCnt];         
    			String[] cntrNo         = new String[insCnt];     
    			String[] cntrType       = new String[insCnt];     
    			String[] badQty         = new String[insCnt];     
    			String[] uomNm          = new String[insCnt];                
    			
    			String[] unitPrice      = new String[insCnt];         
    			String[] whNm           = new String[insCnt];     
    			String[] itemKorNm      = new String[insCnt];     
    			String[] itemEngNm      = new String[insCnt];     
    			String[] repUomId       = new String[insCnt];                
    			
    			String[] uomCd          = new String[insCnt];         
    			String[] uomId          = new String[insCnt];     
    			String[] repUomCd       = new String[insCnt];     
    			String[] repuomNm       = new String[insCnt];     
    			String[] itemGrpId      = new String[insCnt];                
    			
    			String[] expiryDate     = new String[insCnt];         
    			String[] inOrdWeight    = new String[insCnt];                 
    			String[] unitNo         = new String[insCnt];
    			String[] ordDesc        = new String[insCnt];
    			String[] validDt        = new String[insCnt];
    			
    			String[] etc2           = new String[insCnt];
    			String[] inLocCd 		= new String[insCnt]; 
    			String[] userMemo		= new String[insCnt];
    			//추가
    			String[] itemBestDate     = new String[insCnt];   //상품유효기간     
    			String[] itemBestDateEnd  = new String[insCnt];   //상품유효기간만료일
    			String[] rtiNm            = new String[insCnt];   //물류기기명
    			
    			String[] inOmDt           = new String[insCnt];
    			String[] dlvyCd           = new String[insCnt];
    			String[] itemCd           = new String[insCnt];
    			
    			for(int i = 0 ; i < insCnt ; i ++){
    				dsSubRowStatus[i]  = (String)model.get("I_ST_GUBUN"+i);               
    				ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);          
    				ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
    				custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
    				realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
    				
    				realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
    				makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
    				timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
    				locYn[i]            = (String)model.get("I_LOC_YN"+i);      
    				pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
    				
    				workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
    				rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
    				realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
    				realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
    				confYn[i]           = (String)model.get("I_CONF_YN"+i);      
    				
    				unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
    				amt[i]              = (String)model.get("I_AMT"+i);          
    				eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
    				boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
    				inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);      
    				inWorkUomId[i]      = (String)model.get("I_IN_WORK_UOM_ID"+i);
    				outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);                 
    				outWorkUomId[i]     = (String)model.get("I_OUT_WORK_UOM_ID"+i);
    				inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);          
    				inWorkOrdQty[i]     = (String)model.get("I_IN_WORK_ORD_QTY"+i);
    				outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);     
    				outWorkOrdQty[i]    = (String)model.get("I_OUT_WORK_ORD_QTY"+i); 
    				refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
    				dspId[i]            = (String)model.get("I_DSP_ID"+i);      
    				
    				carId[i]            = (String)model.get("I_CAR_ID"+i);                 
    				cntrId[i]           = (String)model.get("I_CNTR_ID"+i);          
    				cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);      
    				cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);      
    				badQty[i]           = (String)model.get("I_BAD_QTY"+i);      
    				
    				uomNm[i]            = (String)model.get("I_UOM_NM"+i);                 
    				unitPrice[i]        = (String)model.get("I_UNIT_PRICE"+i);          
    				whNm[i]             = (String)model.get("I_WH_NM"+i);      
    				itemKorNm[i]        = (String)model.get("I_ITEM_KOR_NM"+i);      
    				itemEngNm[i]        = (String)model.get("I_ITEM_ENG_NM"+i);      
    				                 
    				repUomId[i]         = (String)model.get("I_REP_UOM_ID"+i);                 
    				uomCd[i]            = (String)model.get("I_UOM_CD"+i);          
    				uomId[i]            = (String)model.get("I_UOM_ID"+i);      
    				repUomCd[i]         = (String)model.get("I_REP_UOM_CD"+i);      
    				repuomNm[i]         = (String)model.get("I_REP_UOM_NM"+i);      
    				
    				itemGrpId[i]        = (String)model.get("I_ITEM_GRP_ID"+i);                 
    				expiryDate[i]       = (String)model.get("I_EXPIRY_DATE"+i);          
    				inOrdWeight[i]      = (String)model.get("I_IN_ORD_WEIGHT"+i);      
    				unitNo[i]           = (String)model.get("I_UNIT_NO"+i);      
    				ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);
    				
    				validDt[i]          = (String)model.get("I_VALID_DT"+i);      
    				
    				etc2[i]             = (String)model.get("I_ETC2"+i); 
    				inOmDt[i]           = (String)model.get("calInDt").toString().replace("-", "");
    				dlvyCd[i]		    ="수기";
    				itemCd[i]           = (String)model.get("I_ITEM_CODE"+i);
    		
    				//대화물류 USER_MEMO 컬럼 추가
    				if(model.containsKey("I_USER_MEMO1"+i) && model.containsKey("I_USER_MEMO2"+i) && model.containsKey("I_USER_MEMO3"+i) && model.containsKey("I_USER_MEMO4"+i) &&model.containsKey("I_USER_MEMO5"+i) ){
                    	JSONObject memoJson = new JSONObject();
                    	memoJson.put("1",model.get("I_USER_MEMO1"+i));
                    	memoJson.put("2",model.get("I_USER_MEMO2"+i));
                    	memoJson.put("3",model.get("I_USER_MEMO3"+i));
                    	memoJson.put("4",model.get("I_USER_MEMO4"+i));
                    	memoJson.put("5",model.get("I_USER_MEMO5"+i));
                    	
                    	userMemo[i] = memoJson.toString();
                    	//userMemo[i]	= (String)model.get("I_USER_MEMO"+i);
                    }
    				else{
    					userMemo[i] = null;
    				}
    				inLocCd[i]          = (String)model.get("I_LOC_CD"+i);
    				
    				//추가 
    				itemBestDate[i]       = (String)model.get("I_ITEM_BEST_DATE"+i);      
    				itemBestDateEnd[i]    = (String)model.get("I_ITEM_BEST_DATE_END"+i);   
    				rtiNm[i]              = (String)model.get("I_RTI_NM"+i);
    			}
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    			
    			//main
    			modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
    			modelIns.put("vrOrdId"          , model.get("vrOrdId"));
    			modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
    			modelIns.put("inDt"             , model.get("inDt"));
    			modelIns.put("custPoid"         , model.get("custPoid"));
    			
    			modelIns.put("custPoseq"        , model.get("custPoseq"));
    			modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));
    			modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
    			modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
    			modelIns.put("outWhId"          , model.get("outWhId"));
    			
    			modelIns.put("transCustId"      , model.get("transCustId"));
    			modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
    			modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
    			modelIns.put("blNo"             , model.get("vrBlNo"));
    			modelIns.put("workStat"         , model.get("workStat"));
    			
    			modelIns.put("ordType"          , model.get("ordType"));
    			modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
    			modelIns.put("outReqDt"         , model.get("outReqDt"));
    			modelIns.put("outDt"            , model.get("outDt"));
    			modelIns.put("pdaStat"          , model.get("pdaStat"));
    			
    			modelIns.put("workSeq"          , model.get("workSeq"));
    			modelIns.put("capaTot"          , model.get("capaTot"));
    			modelIns.put("kinOutYn"         , model.get("kinOutYn"));
    			modelIns.put("carConfYn"        , model.get("carConfYn"));
    			modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
    			
    			modelIns.put("tplOrdId"         , model.get("tplOrdId"));
    			modelIns.put("approveYn"        , model.get("vrApproveYn"));
    			modelIns.put("payYn"            , model.get("payYn"));
    			modelIns.put("inCustId"         , model.get("vrInCustId"));
    			modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
    			
    			modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
    			modelIns.put("inCustTel"        , model.get("vrInCustTel"));
    			modelIns.put("cntrNoM"          , model.get("vrCntrNoM"));
    			
    			//sub
    			modelIns.put("dsSub_rowStatus"  , dsSubRowStatus);
    			modelIns.put("ordSeq"           , ordSeq);
    			modelIns.put("ritemId"          , ritemId);
    			modelIns.put("custLotNo"        , custLotNo);
    			modelIns.put("realInQty"        , realInQty);
    			
    			modelIns.put("realOutQty"       , realOutQty);
    			modelIns.put("makeDt"           , makeDt);
    			modelIns.put("timePeriodDay"    , timePeriodDay);
    			modelIns.put("locYn"            , locYn);
    			modelIns.put("pdaCd"            , pdaCd);
    			
    			modelIns.put("workYn"           , workYn);
    			modelIns.put("rjType"           , rjType);
    			modelIns.put("realPltQty"       , realPltQty);
    			modelIns.put("realBoxQty"       , realBoxQty);
    			modelIns.put("confYn"           , confYn);
    			
    			modelIns.put("unitAmt"          , unitAmt);
    			modelIns.put("amt"              , amt);
    			modelIns.put("eaCapa"           , eaCapa);
    			modelIns.put("boxBarcode"       , boxBarcode);
    			modelIns.put("inOrdUomId"       , inOrdUomId);
    			modelIns.put("inWorkUomId"   	, inWorkUomId);
    			
    			modelIns.put("outOrdUomId"      , outOrdUomId);
    			modelIns.put("outWorkUomId"  	, outWorkUomId);
    			modelIns.put("inOrdQty"         , inOrdQty);
    			modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
    			modelIns.put("outOrdQty"        , outOrdQty);
    			
    			modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
    			modelIns.put("refSubLotId"      , refSubLotId);
    			modelIns.put("dspId"            , dspId);                
    			modelIns.put("carId"            , carId);
    			modelIns.put("cntrId"           , cntrId);
    			
    			modelIns.put("cntrNo"           , cntrNo);
    			modelIns.put("cntrType"         , cntrType);
    			modelIns.put("badQty"           , badQty);
    			modelIns.put("uomNm"            , uomNm);
    			modelIns.put("unitPrice"        , unitPrice);
    			
    			modelIns.put("whNm"             , whNm);
    			modelIns.put("itemKorNm"        , itemKorNm);
    			modelIns.put("itemEngNm"        , itemEngNm);
    			modelIns.put("repUomId"         , repUomId);
    			modelIns.put("uomCd"            , uomCd);
    			
    			modelIns.put("uomId"            , uomId);
    			modelIns.put("repUomCd"         , repUomCd);
    			modelIns.put("repuomNm"         , repuomNm);
    			modelIns.put("itemGrpId"        , itemGrpId);
    			modelIns.put("expiryDate"       , expiryDate);
    			
    			modelIns.put("inOrdWeight"      , inOrdWeight); 
    			modelIns.put("unitNo"           , unitNo);
    			modelIns.put("ordDesc"          , ordDesc);
    			modelIns.put("validDt"          , validDt);                
    			modelIns.put("etc2"             , etc2);
    			
    			modelIns.put("userMemo"         , userMemo);
    			
    			modelIns.put("inLocCd"      	, inLocCd);
    			
    			//추가된거(아직프로시져는 안탐)
    			modelIns.put("itemBestDate"     , itemBestDate);
    			modelIns.put("itemBestDateEnd"  , itemBestDateEnd);               
    			modelIns.put("rtiNm"            , rtiNm);
    			modelIns.put("inOmDt"            , inOmDt);
    			modelIns.put("dlvyCd"            , dlvyCd);
    			modelIns.put("itemCd"            , itemCd);
    			
    			//session 정보
    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			
    			if("0000002860".equals((String)model.get(ConstantIF.SS_SVC_NO))) {
    				modelIns = (Map<String, Object>)dao.saveInOrderOv(modelIns);
    			}else {
    				modelIns = (Map<String, Object>)dao.saveInOrderV3(modelIns);	
    			}
    			//dao                
    			
    			ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                
    			
    			// errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
    			// errMsg = modelIns.get("O_MSG_NAME").toString();
    		}
    		//등록 수정 끝            
    		if(delCnt > 0 ){
    			String[] ordId  = new String[delCnt];         
    			String[] ordSeq = new String[delCnt]; 
    			for(int i = 0 ; i < delCnt ; i ++){
    				ordId[i]  = (String)model.get("D_ORD_ID"+i);               
    				ordSeq[i] = (String)model.get("D_ORD_SEQ"+i);          
    			}
    			
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelDel = new HashMap<String, Object>();
    			modelDel.put("ordId", ordId);
    			modelDel.put("ordSeq", ordSeq);
    			
    			modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			
    			modelDel = (Map<String, Object>)dao.deleteInOrder(modelDel);
    			ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
    		}
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(BizException be) {
    		m.put("errCnt", 1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }  
    
    /**
     * 
     * 대체 Method ID      : saveOutOrderOm
     * 대체 Method 설명    : 출고주문(OM) (저장,수정,삭제)
     * 작성자                    : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutOrderOm(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
           
            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            
            if(insCnt > 0){
                //저장, 수정         
                String[] ordSeq         = new String[insCnt];
                //String[] orgOrdSeq         = new String[insCnt];      
                String[] ritemId        = new String[insCnt];     
                String[] custLotNo      = new String[insCnt];     
                String[] realInQty      = new String[insCnt];     
                
                String[] realOutQty     = new String[insCnt];                     
                String[] makeDt         = new String[insCnt];         
                String[] timePeriodDay  = new String[insCnt];     
                String[] locYn          = new String[insCnt];     
                String[] pdaCd          = new String[insCnt];     
                
                String[] workYn         = new String[insCnt];                
                String[] rjType         = new String[insCnt];         
                String[] realPltQty     = new String[insCnt];     
                String[] realBoxQty     = new String[insCnt];     
                String[] confYn         = new String[insCnt];     
                
                //String[] epType        = new String[insCnt];                
                String[] unitAmt        = new String[insCnt];                
                String[] amt            = new String[insCnt];         
                String[] eaCapa         = new String[insCnt];     
                String[] boxBarcode     = new String[insCnt];     
                String[] inOrdUomId     = new String[insCnt];     
                String[] inWorkUomId     = new String[insCnt]; 
                
                String[] outOrdUomId    = new String[insCnt];
                String[] outWorkUomId    = new String[insCnt];  
                String[] minUomId       = new String[insCnt]; 
                String[] inOrdQty       = new String[insCnt];
                String[] inWorkOrdQty       = new String[insCnt];  
                String[] outOrdQty      = new String[insCnt]; 
                String[] outWorkOrdQty       = new String[insCnt];  
                String[] inDspQty       = new String[insCnt]; 
                
                String[] outDspQty      = new String[insCnt];   
                String[] refSubLotId    = new String[insCnt];   
                String[] outOrdWeight   = new String[insCnt];   
                String[] ordDesc        = new String[insCnt];   
                String[] etc2           = new String[insCnt];
                
                String[] zip                   = new String[insCnt];   
                String[] addr                   = new String[insCnt];   
                String[] addr2                = new String[insCnt];   
                String[] sales_cust_nm     = new String[insCnt];   
                String[] phone_1            = new String[insCnt];   
                
                String[] phone_2            = new String[insCnt];   
                String[] buy_phone_1      = new String[insCnt];   
                String[] buy_phone_2      = new String[insCnt];   
                String[] dlv_msg1           = new String[insCnt];   
                String[] dlv_msg2           = new String[insCnt];   
                
                //추가
                String[] itemBestDate       = new String[insCnt];   //상품유효기간     
                String[] itemBestDateEnd    = new String[insCnt];   //상품유효기간만료일
                
                String[] cntrId         = new String[insCnt];
                String[] cntrNo         = new String[insCnt];
                String[] cntrType       = new String[insCnt];
                String[] cntrSealNo     = new String[insCnt];
                
                String[] locCd         = new String[insCnt];
                
                for(int i = 0 ; i < insCnt ; i ++){        
                    ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);
                    //orgOrdSeq[i]      = (String)model.get("I_ORG_ORD_SEQ"+i);        
                    ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
                    
                    realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
                    makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("I_LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
                    
                    workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
                    rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("I_CONF_YN"+i);      
                    
                    //epType[i]          = (String)model.get("I_EP_TYPE"+i);                 
                    unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
                    amt[i]              = (String)model.get("I_AMT"+i);          
                    eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
                    inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);
                    inWorkUomId[i]       = (String)model.get("I_IN_WORK_UOM_ID"+i); 
                    //inWorkUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i); 
                    outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);
                    outWorkUomId[i]      = (String)model.get("I_OUT_WORK_UOM_ID"+i);   
                    //outWorkUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i); 
                    minUomId[i]         = (String)model.get("I_MIN_UOM_ID"+i);                
                    inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
                    inWorkOrdQty[i]         = (String)model.get("I_IN_WORK_ORD_QTY"+i);
                    //inWorkOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
                    outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);    
                    outWorkOrdQty[i]        = (String)model.get("I_OUT_WORK_ORD_QTY"+i);
                    //outWorkOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);
                    inDspQty[i]         = (String)model.get("I_IN_DSP_QTY"+i);
                    
                    outDspQty[i]        = (String)model.get("I_OUT_DSP_QTY"+i);
                    refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
                    outOrdWeight[i]     = (String)model.get("I_OUT_ORD_WEIGHT"+i);     
                    ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);     
                    etc2[i]             = (String)model.get("I_ETC2"+i);
                    
                    zip[i]                      = (String)model.get("vrSrchZip");
                    addr[i]                   = (String)model.get("vrSrchAddr1");
                    addr2[i]                   = (String)model.get("vrSrchAddr2");
                    sales_cust_nm[i]       = (String)model.get("vrSrchSalesCustNm");
                    phone_1[i]              = (String)model.get("vrSrchPhone1");
                    
                    phone_2[i]              = (String)model.get("vrSrchPhone2");
                    buy_phone_1[i]        = (String)model.get("vrSrchBuyPhone1");
                    buy_phone_2[i]        = (String)model.get("vrSrchBuyPhone2");
                    dlv_msg1[i]             = (String)model.get("vrSrchDlvMsg1");
                    dlv_msg2[i]             = (String)model.get("vrSrchDlvMsg2");
                    
                    //추가 
                    itemBestDate[i]     = (String)model.get("I_ITEM_BEST_DATE"+i);      
                    // itemBestDateEnd[i]  = (String)model.get("I_ITEM_BEST_DATE_END"+i);
                    
                    if ( model.get("I_ITEM_BEST_DATE_END" +i) != null ) { 
                       itemBestDateEnd[i] = ((String)model.get("I_ITEM_BEST_DATE_END" +i)).trim().replaceAll("-", "");
                    } else {
                       itemBestDateEnd[i] = (String)model.get("I_ITEM_BEST_DATE_END" +i);
                    }                    
                    
                    cntrId[i]           = (String)model.get("I_CNTR_ID"+i);
                    cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);
                    cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);
                    cntrSealNo[i]       = (String)model.get("I_CNTR_SEAL_NO"+i);
                    
                    locCd[i]       = (String)model.get("I_LOC_CD"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("ordId"            , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("inReqDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                
                modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));  
                modelIns.put("orgOrdSeq"        , "1"); // 현재 주문 건당 원주문번호 1개씩 할당 되고 있음 
                modelIns.put("inWhId"           , model.get("inWhId"));
                modelIns.put("outWhId"          , model.get("vrOutWhId"));  
                modelIns.put("transCustId"      , model.get("vrTransCustId")); 
                
                modelIns.put("custId"           , model.get("vrSrchCustId"));   
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));    
                modelIns.put("blNo"             , model.get("vrBlNo"));         
                modelIns.put("workStat"         , model.get("vrWorkStat"));       //하드
                modelIns.put("ordType"          , model.get("vrOrdType"));            //하드
                
                modelIns.put("ordSubtype"       , model.get("vrOrdSubType"));   
                modelIns.put("outReqDt"         , model.get("vrCalOutReqDt").toString().replace("-", ""));  
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));
                modelIns.put("workSeq"          , model.get("workSeq"));
                
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("vrKinOutYn"));      
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                
                modelIns.put("payYn"            , model.get("vrPayYn"));
                modelIns.put("asnInReqDt"       , model.get("vrCalInReqDt").toString().replace("-", ""));  
                modelIns.put("cntrNoM"          , model.get("vrCntrNoM"));
                modelIns.put("cntrSealNoM"      , model.get("vrCntrSealNoM"));
                
                //sub
                modelIns.put("ordSeq"           , ordSeq);
                // modelIns.put("orgOrdSeq"      , ordSeq); 
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);
                modelIns.put("realOutQty"       , realOutQty);
                
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);
                modelIns.put("workYn"           , workYn);
                
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);
                //modelIns.put("epType"           , model.get("vrEpType"));
                modelIns.put("unitAmt"          , unitAmt);
                
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inOrdUomId"       , inOrdUomId);
                modelIns.put("inWorkUomId"      , inWorkUomId);
                
                modelIns.put("outOrdUomId"      , outOrdUomId);
                modelIns.put("outWorkUomId"     , outWorkUomId);
                modelIns.put("minUomId"         , minUomId);
                modelIns.put("inOrdQty"         , inOrdQty);
                modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
                
                modelIns.put("outOrdQty"        , outOrdQty);
                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
                modelIns.put("inDspQty"         , inDspQty);
                modelIns.put("outDspQty"        , outDspQty);
                modelIns.put("refSubLotId"      , refSubLotId);
                
                modelIns.put("outOrdWeight"     , outOrdWeight);                
                modelIns.put("cntrId"           , cntrId);
                modelIns.put("cntrNo"           , cntrNo);
                modelIns.put("cntrType"         , cntrType);
                modelIns.put("cntrSealNo"       , cntrSealNo);
                
                modelIns.put("ordDesc"          , ordDesc);             
                modelIns.put("etc2"             , etc2);
                
                //추가된거(아직프로시져는 안탐)
                modelIns.put("itemBestDate"     , itemBestDate);
                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);
                
                modelIns.put("zip"                     , zip);
                modelIns.put("addr"                   , addr);
                modelIns.put("addr2"                 , addr2);
                modelIns.put("sales_cust_nm"      , sales_cust_nm);
                modelIns.put("phone_1"             , phone_1);
                
                modelIns.put("phone_2"              , phone_2);
                modelIns.put("buy_phone_1"        , buy_phone_1);
                modelIns.put("buy_phone_2"        , buy_phone_2);
                modelIns.put("dlv_msg1"             , dlv_msg1);
                modelIns.put("dlv_msg2"             , dlv_msg2);
                
                modelIns.put("locCd"            	   , locCd);
                modelIns.put("dataSenderNm"                   , model.get("vrSrchDataSenderNm"));
                
                modelIns.put("ordDegree"           , model.get("vrSrchOrdDegree"));
                
                //session 정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOutOrderOm(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            //등록 수정 끝
            if(delCnt > 0 ){
                String[] ordId  = new String[delCnt];         
                String[] ordSeq = new String[delCnt]; 
                for(int i = 0 ; i < delCnt ; i ++){
                    ordId[i]  = (String)model.get("D_ORD_ID"+i);               
                    ordSeq[i] = (String)model.get("D_ORD_SEQ"+i);          
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelDel = new HashMap<String, Object>();
                modelDel.put("ordId", ordId);
                modelDel.put("ordSeq", ordSeq);

                modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                 modelDel = (Map<String, Object>)dao.deleteOutOrder(modelDel);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
            }          
           m.put("errCnt", 0);
           m.put("MSG", MessageResolver.getMessage("save.success"));
  
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * Method ID    : saveIncust
	 * Method 설명      : 입고관리 주문팝업 입고처저장
	 * 작성자                 : summer
	 * @param   model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveIncust(Map<String, Object> model) throws Exception {
    	
    	 Map<String, Object> m = new HashMap<String, Object>();
         // log.info(model);
         try{
             //파람
             Map<String, Object> modelIns = new HashMap<String, Object>();
             //System.out.println(model.get("vrInCustId"));
             //main
             modelIns.put("ordId"            , model.get("vrOrdId"));
             modelIns.put("inCustCd"      	 , model.get("vrInCustId"));
             modelIns.put("lcId"           	 , (String)model.get(ConstantIF.SS_SVC_NO));
             //session 정보
             modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
             modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

             //dao                
             dao.saveIncust(modelIns);
   //          modelIns.put("O_MSG_CODE"      , 0);
    //         modelIns.put("O_MSG_NAME"      , model.get("vrOrdId"));
    //         ServiceUtil.isValidReturnCode("WMSOP999", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                       
 	        m.put("errCnt", 0);
 	        m.put("MSG", MessageResolver.getMessage("save.success"));
   
     
         } catch(Exception e){
             throw e;
         }
         return m;
    } 
    
    
    
    /**
	 * Method ID : save Method 설명 : 주문 첨부파일 저장  저장 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Map<String, Object> modelDt = new HashMap<String, Object>();
				// 확장자 잘라내기
				String str = (String) model.get("D_ATCH_FILE_NAME");
				System.out.println("D_ATCH_FILE_NAME : "+str);
				
				String ext = "." + str.substring((str.lastIndexOf('.') + 1));
				// 파일 ID 만들기
				int fileSeq = 1;
				String fileId = "";
				fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
				// 저장준비
				modelDt.put("FILE_ID", fileId);
				modelDt.put("ATTACH_GB", "ORDER"); // 통합 HELPDESK 업로드
				modelDt.put("WORK_ORD_ID", model.get("WORK_ORD_ID")); // 주문번호
				modelDt.put("FILE_VALUE", "wmsop010"); // 기존코드 "tmsba230" 로
														// 하드코딩되어 있음 파일구분값?
				modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE")); // 서버저장경로
				modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME")); // 원본파일명
				modelDt.put("FILE_EXT", ext); // 파일 확장자
				modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));// 파일 사이즈
				// 저장
				String fileMngNo = (String) MSdao.fileUpload(modelDt);
				// 리턴값 파일 id랑 파일경로
				model.put("IMAGE_ID", fileId);
				model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
				
			}

			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
	
	
	 /**
     * 분할전
     * 대체 Method ID   : saveInOrderV4
     * 대체 Method 설명    : saveInOrderV2(입고주문) 기준으로 1차 리팩토링...
     * 작성자                      : KIH
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrderV4(Map<String, Object> model) throws Exception {
      Map<String, Object> m = new HashMap<String, Object>();
      try{
        List orderList = (List) model.get("orderList");
        if(orderList == null) {
          throw new NullPointerException("입고등록할 주문이 없습니다.");
        }
        //프로시져에 보낼것들 다담는다
        Map<String, Object> modelIns = new HashMap<String, Object>();
        Map<String, String[]> orderDetailsMap = 
            CommonUtil.convertMapStringArrayFromListMap(
                orderList,
                new String[]{
                    "I_ST_GUBUN",
                    "I_ORD_SEQ",
                    "I_RITEM_ID",
                    "I_CUST_LOT_NO",
                    "I_REAL_IN_QTY",
                    "I_REAL_OUT_QTY",
                    "I_MAKE_DT",
                    "I_TIME_PERIOD_DAY",
                    "I_LOC_YN",
                    "I_PDA_CD",
                    "I_WORK_YN",
                    "I_RJ_TYPE",
                    "I_REAL_PLT_QTY",
                    "I_REAL_BOX_QTY",
                    "I_CONF_YN",
                    "I_UNIT_AMT",
                    "I_AMT",
                    "I_EA_CAPA",
                    "I_BOX_BARCODE",
                    "I_IN_ORD_UOM_ID",
                    "I_IN_WORK_UOM_ID",
                    "I_OUT_ORD_UOM_ID",
                    "I_OUT_WORK_UOM_ID",
                    "I_IN_ORD_QTY",
                    "I_IN_WORK_ORD_QTY",
                    "I_OUT_ORD_QTY",
                    "I_OUT_WORK_ORD_QTY",
                    "I_REF_SUB_LOT_ID",
                    "I_DSP_ID",
                    "I_CAR_ID",
                    "I_CNTR_ID",
                    "I_CNTR_NO",
                    "I_CNTR_TYPE",
                    "I_BAD_QTY",
                    "I_UOM_NM",
                    "I_UNIT_PRICE",
                    "I_WH_NM",
                    "I_ITEM_KOR_NM",
                    "I_ITEM_ENG_NM",
                    "I_REP_UOM_ID",
                    "I_UOM_CD",
                    "I_UOM_ID",
                    "I_REP_UOM_CD",
                    "I_REP_UOM_NM",
                    "I_ITEM_GRP_ID",
                    "I_EXPIRY_DATE",
                    "I_IN_ORD_WEIGHT",
                    "I_UNIT_NO",
                    "I_ORD_DESC",
                    "I_VALID_DT",
                    "I_ETC2",
                    "I_ITEM_BEST_DATE",
                    "I_ITEM_BEST_DATE_END",
                    "I_RTI_NM"
                    }
            );
        //header
        modelIns.putAll(model);
        modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
        modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
        modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));
        modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
        modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
        modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
        modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
        modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
        modelIns.put("approveYn"        , model.get("vrApproveYn"));
        modelIns.put("inCustId"         , model.get("vrInCustId"));
        modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
        modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
        modelIns.put("inCustTel"        , model.get("vrInCustTel"));
        
        //detail
        modelIns.put("dsSub_rowStatus"  , orderDetailsMap.get("I_ST_GUBUN"));
        modelIns.put("ordSeq"           , orderDetailsMap.get("I_ORD_SEQ"));
        modelIns.put("ritemId"          , orderDetailsMap.get("I_RITEM_ID"));
        modelIns.put("custLotNo"        , orderDetailsMap.get("I_CUST_LOT_NO"));
        modelIns.put("realInQty"        , orderDetailsMap.get("I_REAL_IN_QTY"));
        
        modelIns.put("realOutQty"       , orderDetailsMap.get("I_REAL_OUT_QTY"));
        modelIns.put("makeDt"           , orderDetailsMap.get("I_MAKE_DT"));
        modelIns.put("timePeriodDay"    , orderDetailsMap.get("I_TIME_PERIOD_DAY"));
        modelIns.put("locYn"            , orderDetailsMap.get("I_LOC_YN"));
        modelIns.put("pdaCd"            , orderDetailsMap.get("I_PDA_CD"));
        
        modelIns.put("workYn"           , orderDetailsMap.get("I_WORK_YN"));
        modelIns.put("rjType"           , orderDetailsMap.get("I_RJ_TYPE"));
        modelIns.put("realPltQty"       , orderDetailsMap.get("I_REAL_PLT_QTY"));
        modelIns.put("realBoxQty"       , orderDetailsMap.get("I_REAL_BOX_QTY"));
        modelIns.put("confYn"           , orderDetailsMap.get("I_CONF_YN"));
        
        modelIns.put("unitAmt"          , orderDetailsMap.get("I_UNIT_AMT"));
        modelIns.put("amt"              , orderDetailsMap.get("I_AMT"));
        modelIns.put("eaCapa"           , orderDetailsMap.get("I_EA_CAPA"));
        modelIns.put("boxBarcode"       , orderDetailsMap.get("I_BOX_BARCODE"));
        modelIns.put("inOrdUomId"       , orderDetailsMap.get("I_IN_ORD_UOM_ID"));
        modelIns.put("inWorkUomId"      , orderDetailsMap.get("I_IN_WORK_UOM_ID"));
        
        modelIns.put("outOrdUomId"      , orderDetailsMap.get("I_OUT_ORD_UOM_ID"));
        modelIns.put("outWorkUomId"     , orderDetailsMap.get("I_OUT_WORK_UOM_ID"));
        modelIns.put("inOrdQty"         , orderDetailsMap.get("I_IN_ORD_QTY"));
        modelIns.put("inWorkOrdQty"     , orderDetailsMap.get("I_IN_WORK_ORD_QTY"));
        modelIns.put("outOrdQty"        , orderDetailsMap.get("I_OUT_ORD_QTY"));
        
        modelIns.put("outWorkOrdQty"    , orderDetailsMap.get("I_OUT_WORK_ORD_QTY"));
        modelIns.put("refSubLotId"      , orderDetailsMap.get("I_REF_SUB_LOT_ID"));
        modelIns.put("dspId"            , orderDetailsMap.get("I_DSP_ID"));                
        modelIns.put("carId"            , orderDetailsMap.get("I_CAR_ID"));
        modelIns.put("cntrId"           , orderDetailsMap.get("I_CNTR_ID"));
        
        modelIns.put("cntrNo"           , orderDetailsMap.get("I_CNTR_NO"));
        modelIns.put("cntrType"         , orderDetailsMap.get("I_CNTR_TYPE"));
        modelIns.put("badQty"           , orderDetailsMap.get("I_BAD_QTY"));
        modelIns.put("uomNm"            , orderDetailsMap.get("I_UOM_NM"));
        modelIns.put("unitPrice"        , orderDetailsMap.get("I_UNIT_PRICE"));
        
        modelIns.put("whNm"             , orderDetailsMap.get("I_WH_NM"));
        modelIns.put("itemKorNm"        , orderDetailsMap.get("I_ITEM_KOR_NM"));
        modelIns.put("itemEngNm"        , orderDetailsMap.get("I_ITEM_ENG_NM"));
        modelIns.put("repUomId"         , orderDetailsMap.get("I_REP_UOM_ID"));
        modelIns.put("uomCd"            , orderDetailsMap.get("I_UOM_CD"));
        
        modelIns.put("uomId"            , orderDetailsMap.get("I_UOM_ID"));
        modelIns.put("repUomCd"         , orderDetailsMap.get("I_REP_UOM_CD"));
        modelIns.put("repuomNm"         , orderDetailsMap.get("I_REP_UOM_NM"));
        modelIns.put("itemGrpId"        , orderDetailsMap.get("I_ITEM_GRP_ID"));
        modelIns.put("expiryDate"       , orderDetailsMap.get("I_EXPIRY_DATE"));
        
        modelIns.put("inOrdWeight"      , orderDetailsMap.get("I_IN_ORD_WEIGHT")); 
        modelIns.put("unitNo"           , orderDetailsMap.get("I_UNIT_NO"));
        modelIns.put("ordDesc"          , orderDetailsMap.get("I_ORD_DESC"));
        modelIns.put("validDt"          , orderDetailsMap.get("I_VALID_DT"));                
        modelIns.put("etc2"             , orderDetailsMap.get("I_ETC2"));
        
        //추가된거(아직프로시져는 안탐)
        modelIns.put("itemBestDate"     , orderDetailsMap.get("I_ITEM_BEST_DATE"));
        modelIns.put("itemBestDateEnd"  , orderDetailsMap.get("I_ITEM_BEST_DATE_END"));               
        modelIns.put("rtiNm"            , orderDetailsMap.get("I_RTI_NM"));
        
        //session 정보
        modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            
        //저장, 수정
        //dao                
        modelIns = (Map<String, Object>)dao.saveInOrderV2(modelIns);
        ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
        m.put("errCnt", 0);
        m.put("MSG", MessageResolver.getMessage("save.success"));
      } catch(BizException be) {
        m.put("errCnt", 1);
        m.put("MSG", be.getMessage() );
      } catch(Exception e){
        throw e;
      }
      return m;
    }

}
