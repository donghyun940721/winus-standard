package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP021Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP021Service")
public class WMSOP021ServiceImpl implements WMSOP021Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP021Dao")
    private WMSOP021Dao dao;
    
    /**
     * 
     * Method ID   : selectPoolGrp
     * Method 설명    : 물류용기입고관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("POOLGRP", dao.selectPoolGrp(model));
        return map;
    }
    
    /**
     * Method ID   : list
     * Method 설명    : 물류용기입고관리  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * Method ID   : saveInOrder
     * Method 설명    : 입출고관리 > 출고관리(B2C) > 전체통합버튼 > 입고주문생성
     * 작성자               : wl2258
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       try{
    	   int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
    	   if(tmpCnt > 0){
    		   String[] lcId = new String[tmpCnt];
               String[] custId  = new String[tmpCnt];                
               String[] ritemId = new String[tmpCnt];
               String[] workQty = new String[tmpCnt];    
               String[] uomId = new String[tmpCnt];
               
               for(int i = 0 ; i < tmpCnt ; i ++){
            	   lcId[i] = (String)model.get("I_LC_ID_"+i);
            	   custId[i]    = (String)model.get("I_CUST_ID_"+i);               
            	   ritemId[i]   = (String)model.get("I_RITEM_ID_"+i);
            	   workQty[i]   = (String)model.get("I_WORK_QTY_"+i);   
            	   uomId[i]   = (String)model.get("I_UOM_ID_"+i);   
               }
               
               Map<String, Object> modelIns = new HashMap<String, Object>();
               modelIns.put("lcId", lcId);
               modelIns.put("custId", custId);
               modelIns.put("ritemId", ritemId);
               modelIns.put("uomId", uomId);
               modelIns.put("workQty", workQty);
               
               modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
               modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
               
               modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
               ServiceUtil.isValidReturnCode("WMSOP021", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    	   }
    	   m.put("errCnt", 0);
           m.put("MSG", MessageResolver.getMessage("save.success"));
           
       } catch(BizException be) {
           m.put("errCnt", -1);
           m.put("MSG", be.getMessage() );
           
       }catch(Exception e){
           throw e;
       }
       return m;
    }
}
