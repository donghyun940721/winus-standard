package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP022Dao")
public class WMSOP022Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    
	/**
     * Method ID    : serchRitemIdCnt
     * Method 설명      : 고객사 바코드를 통한 ritem_id cnt 검색
     * 작성자                 : 
     * @param   model
     * @return
     */
    public String itemSearchByBarcodeCnt(Map<String, Object> model) {
        return (String)executeView("wmsop022.itemSearchByBarcodeCnt", model);
        
    }
    
	/**
     * Method ID    : serchRitemId
     * Method 설명      : 고객사 바코드를 통한 ritem_id 검색
     * 작성자                 : 
     * @param   model
     * @return
     */
    public String itemSearchByBarcode(Map<String, Object> model) {
        return (String)executeView("wmsop022.itemSearchByBarcode", model);
        
    }
    
    /**
     * Method ID    : saveOrdSimpleIn
     * Method 설명      : 주문입력 및 간편 입고 확정 
     * 작성자                 : 
     * @param   model
     * @return
     */
    public Object saveOrdSimpleIn(Map<String, Object> model){
        executeUpdate("wmsop022.pk_wmsop021.save_ord_and_simple_in", model);
        return model;
    }

    
}
