package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP920Dao")
public class WMSOP920Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID    : listE1
     * Method 설명      : 입고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
    	String LC_ID = (String)model.get("LC_ID");
    	if (LC_ID != null && StringUtils.isNotEmpty(LC_ID) && LC_ID.equals("0000002940")){//kcc
    		return executeQueryPageWq("wmsop920.listE1Kcc", model);
    	}else{
    		return executeQueryPageWq("wmsop920.listE1", model);
    	}
    }
    
    /**
     * Method ID    : listE2
     * Method 설명      : 출고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	if(model.get("SS_SVC_NO").equals("0000003620")){ // 비즈컨설팅 물류센터일 경우 (임시)
    		return executeQueryPageWq("wmsop920.listE2Biz", model);
    	}else{
    		return executeQueryPageWq("wmsop920.listE2", model);
    	}
    }
    
    /**
     * Method ID    : listE3
     * Method 설명    : 화주별 출고관리  조회(OM)
     * 작성자               : KCR
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	if(model.get("SS_SVC_NO").equals("0000002860")){
    		return executeQueryPageWq("wmsop920.listE3Olive", model);
    	}else if(model.get("SS_SVC_NO").equals("0000003620")){ // 비즈컨설팅 물류센터일 경우 (임시)
    		return executeQueryPageWq("wmsop920.listE3Biz", model);
    	}else{
    		return executeQueryPageWq("wmsop920.listE3", model);
    	}
    }
    
    /**
     * Method ID    : listE5
     * Method 설명    : 올리브영 부분입고 조회
     * 작성자               : YOSEOP
     * @param   model
     * @return
     */
    public GenericResultSet listE5(Map<String, Object> model) {
    	
    	return executeQueryPageWq("wmsop920.listE5Olive", model);
    	
    }
    
    /**
     * Method ID    : listE4Header
     * Method 설명    : 출고관리 조회 헤더
     * 작성자               : KCR
     * @param   model
     * @return
     */
	public Object listE4Header(Map<String, Object> model) {
		List custs = list("wmsop920.listE4Header", model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	 /**
     * Method ID    : listE4Detail
     * Method 설명    : 출고관리 조회 디테일
     * 작성자               : KCR
     * @param   model
     * @return
     */
	public Object listE4Detail(Map<String, Object> model) {
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop920.listE4Detail", model));
    	return genericResultSet;
	}

	 /**
     * Method ID   		: listE1SummaryCount
     * Method 설명      : 입출고통합관리 -> 입고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE1SummaryCount(Map<String, Object> model) {
		return executeView("wmsop920.listE1SummaryCount", model);
	}
	
	 /**
     * Method ID   		: listE2SummaryCount
     * Method 설명      : 입출고통합관리 -> B2B 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE2SummaryCount(Map<String, Object> model) {
		if(model.get("SS_SVC_NO").equals("0000003620")){
			return executeView("wmsop920.listE2BizSummaryCount", model);
		}else{
			return executeView("wmsop920.listE2SummaryCount", model);
		}
	}
	
	 /**
     * Method ID   		: listE3SummaryCount
     * Method 설명      : 입출고통합관리 -> B2C 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE3SummaryCount(Map<String, Object> model) {
		if(model.get("SS_SVC_NO").equals("0000003620")){
			return executeView("wmsop920.listE3BizSummaryCount", model);
		}else{
			return executeView("wmsop920.listE3SummaryCount", model);
		}
	}

     /**
	 * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void insertCsv(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				String LC_ID = (String) model.get("strLcId");
				String CUST_ID = (String) model.get("strCustId");
				
				if (paramMap.get("ORG_ORD_ID") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("ORG_ORD_ID"))
    			     && paramMap.get("INVC_NO") != null && StringUtils.isNotEmpty((String) paramMap.get("INVC_NO"))
    			     && paramMap.get("TK_NO") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("TK_NO"))
    			     && StringUtils.isNotEmpty(LC_ID)
    			     //&& StringUtils.isNotEmpty(CUST_ID)
    			){					
					paramMap.put("LC_ID"	, LC_ID);
					sqlMapClient.update("wmsop920.insertCsvDHL", paramMap);
					sqlMapClient.update("wmsop920.insertCsvPS", paramMap);
    			}
			}
			
			sqlMapClient.commitTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/**
     * Method ID  : outInvalidView
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public Object outInvalidView(Map<String, Object> model){
        return executeQueryForList("wmsop920.outInvalidView", model);
    }
	
}
