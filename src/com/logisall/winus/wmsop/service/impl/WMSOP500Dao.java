package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP500Dao")
public class WMSOP500Dao extends SqlMapAbstractDAO{
	protected Log log = LogFactory.getLog(this.getClass());
	
	public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop500.list", model);
    }
	public GenericResultSet listHistory(Map<String, Object> model) {
		return executeQueryPageWq("wmsop600.listHistory", model);
	}

}
