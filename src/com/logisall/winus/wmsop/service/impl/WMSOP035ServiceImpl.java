package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP035Service;
import com.logisall.winus.wmsop.vo.WMSOP035VO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;

@Service("WMSOP035Service")
public class WMSOP035ServiceImpl implements WMSOP035Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP035Dao")
    private WMSOP035Dao dao;
    
  
    
    /**
     * Method ID   : list
     * Method 설명    : 출고관리  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.list(model));
        return map;
    }
    /**
     * Method ID   : searchUnshipped
     * Method 설명    : 올리브영 미발행내역 조회
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> searchUnshipped(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
		List list = dao.searchUnshipped(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		
    		String decryptdTel = "";
    		if(mapp.get("D_TEL") != null && !((String)mapp.get("D_TEL")).trim().equals("")){
    			decryptdTel = OliveAes256.decrpyt((String) mapp.get("D_TEL")); 
    		}
    		
    		String decryptdPost = "";
    		if(mapp.get("D_POST") != null && !((String)mapp.get("D_POST")).trim().equals("")){
    			decryptdPost = OliveAes256.decrpyt((String) mapp.get("D_POST")); 
    		}
    		
    		String decryptdAddress = "";
    		if(mapp.get("D_ADDRESS") != null && !((String)mapp.get("D_ADDRESS")).trim().equals("")){
    			decryptdAddress = OliveAes256.decrpyt((String) mapp.get("D_ADDRESS")); 
    		}
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		String decryptdCity = (String) mapp.get("D_CITY");
    		String decryptdProvince = (String) mapp.get("D_PROVINCE");
    		
    		String decryptjTel = "";
    		if(mapp.get("J_TEL") != null && !((String)mapp.get("J_TEL")).trim().equals("")){
    			decryptjTel = OliveAes256.decrpyt((String) mapp.get("J_TEL")); 
    		}
    		 
    		String decryptjPost = "";
    		if(mapp.get("J_POST") != null && !((String)mapp.get("J_POST")).trim().equals("")){
    			decryptjPost = OliveAes256.decrpyt((String) mapp.get("J_POST")); 
    		}
    		
    		String decryptjAddress = "";
    		if(mapp.get("J_ADDRESS") != null && !((String)mapp.get("J_ADDRESS")).trim().equals("")){
    			decryptjAddress = OliveAes256.decrpyt((String) mapp.get("J_ADDRESS")); 
    		}

    		String decryptjAddress2 = "";
    		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
    			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
    		}
    		
    		String jCity = (String)mapp.get("J_CITY");
    		String jProvince = (String)mapp.get("J_PROVINCE");
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_PROVINCE", decryptdProvince);
    		mapp.put("D_ADDRESS", decryptdAddress);
    		mapp.put("D_ADDRESS2", decryptdAddress2);
    		

    		mapp.put("J_TEL", decryptjTel);
    		mapp.put("J_POST", decryptjPost);
    		mapp.put("J_ADDRESS", decryptjAddress);
    		mapp.put("J_ADDRESS2", decryptjAddress2);
    		// String ordId = (String)model.get("ordId");
    		// mapp.put("ORD_ID", ordId);
    		
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", genericResultSet);
    	
    	return map;
    }
   
    @Override
    public Map<String, Object> dhlExcel(Map<String, Object> model) {
    	// TODO Auto-generated method stub
    	return null;
    }
    
    
    /*-
	 * Method ID   : getCustIdByLcId
	 * Method 설명 : LC_ID로 해당 코드의 화주ID 가져오기
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("NODATA")) {
			map.put("NODATA", dao.list2(model));
		}
		return map;
	}
    
    /**
     * Method ID 		: pdfDown
     * Method 설명 	: BLOB 송장 PDF 출력 
     * 작성자 			: KSJ
     * @param model
     * @return
     * @throws Exception
     */
	@Override
    public byte[] pdfDown(Map<String, Object> model, String company) throws Exception {
    	byte[] pdfBinary;
    	List<WMSOP035VO> pdfList =  dao.pdfDown(model, company);
    	
    	if(pdfList.size() > 1){
    		PDFMergerUtility pdfMergerUtility = new PDFMergerUtility();
    		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    		pdfMergerUtility.setDestinationStream(outputStream);
    		
    		for(WMSOP035VO vo : pdfList){
    			ByteArrayInputStream inputStream = new ByteArrayInputStream(vo.getGraphicImage());
    			pdfMergerUtility.addSource(inputStream);
    			inputStream.close();
    		}
    		pdfMergerUtility.mergeDocuments(null);
    		pdfBinary = outputStream.toByteArray();
    		outputStream.close();
    		
    	}else if(pdfList.size() == 1){
    		pdfBinary = pdfList.get(0).getGraphicImage();
    		
    	}else{
    		pdfBinary = new byte[0];
    	}
    	
    	
    	return pdfBinary;
    }
	
	/**
     * Method ID 		: pdfDown2
     * Method 설명 	: BLOB 송장 PDF 출력 - 특정 페이지만 출력 
     * 작성자 			: KSJ
     * @param model
     * @return
     * @throws Exception
     */
	@Override
    public byte[] pdfDown2(Map<String, Object> model, String company) throws Exception {
    	byte[] pdfBinary;
    	List<WMSOP035VO> pdfList =  dao.pdfDown(model, company);
    	
    	PDDocument pdfdoc = null;
    	ByteArrayInputStream iStream = new ByteArrayInputStream(pdfList.get(0).getGraphicImage());
    	pdfdoc = PDDocument.load(iStream);
    	pdfdoc.getPage(0);
    	
    	ByteArrayOutputStream oStream = new ByteArrayOutputStream();
    	// pdfdoc.save(OutputStream);

    	
    	if(pdfList.size() > 1){
    		PDFMergerUtility pdfMergerUtility = new PDFMergerUtility();
    		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    		pdfMergerUtility.setDestinationStream(outputStream);
    		
    		for(WMSOP035VO vo : pdfList){
    			ByteArrayInputStream inputStream = new ByteArrayInputStream(vo.getGraphicImage());
    			pdfMergerUtility.addSource(inputStream);
    			inputStream.close();
    		}
    		pdfMergerUtility.mergeDocuments(null);
    		pdfBinary = outputStream.toByteArray();
    		outputStream.close();
    		
    	}else if(pdfList.size() == 1){
    		pdfBinary = pdfList.get(0).getGraphicImage();
    		
    	}else{
    		pdfBinary = new byte[0];
    	}
    	
    	
    	return pdfBinary;
    }
    
	/**
     * 
     * 대체 Method ID   : outSaveComplete
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> outSaveComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){    
                String[] ordId   = new String[tmpCnt];
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);   
                	//System.out.println("I_ORD_ID : " + (String)model.get("I_ORD_ID"+i));
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);

                //session 및 등록정보
                modelIns.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
                //dao                
                modelIns = (Map<String, Object>)dao.outSaveComplete(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP035", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	     * 
	     * 대체 Method ID   : outSaveComplete
	     * 대체 Method 설명    : 
	     * 작성자                      : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
		 @Override
	    public Map<String, Object> outSaveCompleteV2(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
	            
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                String[] ordId = new String[]{(String) model.get("ordId")};
                
                modelIns.put("ordId", ordId);

                //session 및 등록정보
                modelIns.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
                //dao                
                modelIns = (Map<String, Object>)dao.outSaveComplete(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP035", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage());
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
	 
	 /**
     * 
     * 대체 Method ID   : list2DHL
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2DHL(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Object vrSrchOrdIds2 = model.get("vrSrchOrdIds2");
        if(vrSrchOrdIds2 != null && !"".equals(((String)vrSrchOrdIds2).trim())){
        	List<String> vrSrchOrdIdsArray = new ArrayList(Arrays.asList(((String)vrSrchOrdIds2).trim().split(";")));
        	for(int i = vrSrchOrdIdsArray.size() - 1; i >= 0 ; i--){
        		if(vrSrchOrdIdsArray.get(i).trim().equals("")){
        			vrSrchOrdIdsArray.remove(i);
        			
        		}
        		
        	}
        	
        	model.put("vrSrchOrdIdsArray", vrSrchOrdIdsArray);
        }
    	
        map.put("LIST", dao.list2DHL(model));
        return map;
    }
    
	 /**
     * 
     * 대체 Method ID   		: list2QXP
     * 대체 Method 설명    	: 수출신고정보
     * 작성자                     	: KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2QXP(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Object vrSrchOrdIds2 = model.get("vrSrchOrdIds2");
        if(vrSrchOrdIds2 != null && !"".equals(((String)vrSrchOrdIds2).trim())){
        	List<String> vrSrchOrdIdsArray = new ArrayList(Arrays.asList(((String)vrSrchOrdIds2).trim().split(";")));
        	for(int i = vrSrchOrdIdsArray.size() - 1; i >= 0 ; i--){
        		if(vrSrchOrdIdsArray.get(i).trim().equals("")){
        			vrSrchOrdIdsArray.remove(i);
        		}
        	}
        	model.put("vrSrchOrdIdsArray", vrSrchOrdIdsArray);
        }
    	
        map.put("LIST", dao.list2QXP(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list2EMS
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2EMS(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Object vrSrchOrdIds2 = model.get("vrSrchOrdIds2");
    	if(vrSrchOrdIds2 != null && !"".equals(((String)vrSrchOrdIds2).trim())){
    		List<String> vrSrchOrdIdsArray = new ArrayList(Arrays.asList(((String)vrSrchOrdIds2).trim().split(";")));
    		for(int i = vrSrchOrdIdsArray.size() - 1; i >= 0 ; i--){
    			if(vrSrchOrdIdsArray.get(i).trim().equals("")){
    				vrSrchOrdIdsArray.remove(i);
    				
    			}
    			
    		}
    		
    		model.put("vrSrchOrdIdsArray", vrSrchOrdIdsArray);
    	}
    	
    	map.put("LIST", dao.list2EMS(model));
    	return map;
    }
    /**
     * 
     * 대체 Method ID   : list2DHL
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> request2EMS(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Object vrSrchOrdIds2 = model.get("vrSrchOrdIds3");
        if(vrSrchOrdIds2 != null && !"".equals(((String)vrSrchOrdIds2).trim())){
        	List<String> vrSrchOrdIdsArray = new ArrayList(Arrays.asList(((String)vrSrchOrdIds2).trim().split(";")));
        	for(int i = vrSrchOrdIdsArray.size() - 1; i >= 0 ; i--){
        		if(vrSrchOrdIdsArray.get(i).trim().equals("")){
        			vrSrchOrdIdsArray.remove(i);
        			
        		}
        		
        	}
        	
        	model.put("vrSrchOrdIdsArray", vrSrchOrdIdsArray);
        }
    	
    	List list  = dao.request2EMS(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		Map<String, Object> smap = new HashMap<>();
    		String orgOrdId = (String) mapp.get("ORG_ORD_ID");
    		smap.put("orgOrdId", orgOrdId);
    		smap.put("SS_SVC_NO", (String)model.get("SS_SVC_NO"));
    		List detailList = dao.request2EMSDetail(smap);
    		StringBuilder nmBuilder = new StringBuilder();
    		StringBuilder qtyBuilder = new StringBuilder();
    		StringBuilder weightBuilder = new StringBuilder();
    		StringBuilder totBuilder = new StringBuilder();
    		StringBuilder hsBuilder = new StringBuilder();
    		StringBuilder fromBuilder = new StringBuilder();
    		
    		int sum = 0;
    		for(int j = 0; j < detailList.size(); j++){
    			
    			Map<String, Object> mapq = (Map)detailList.get(j);
    			String ordDesc = ((String)mapq.get("ORD_DESC"))
    					.replaceAll("#", "")
    					.replaceAll("\\+", "")
    					.replaceAll("'", "")
    					.replaceAll("-", "")
    					.replaceAll("!", "")
    					.replaceAll("\\?", "");
    			String ritemNm = ordDesc.length() > 32 ? ordDesc.substring(0, 32) : ordDesc;
    			nmBuilder.append(ritemNm + ";");
    			
    			String ordQty = String.valueOf(mapq.get("ORD_QTY"));
    			int qty = Integer.parseInt(ordQty);
    			sum += qty;
    			qtyBuilder.append(ordQty + ";");
    			totBuilder.append(String.valueOf(mapq.get("TOT")) + ";");
    			hsBuilder.append("3304999000;");
    			fromBuilder.append("KR;");
    			weightBuilder.append("450;");
    			
    		}
    		
    		//String width = "0";
    		//String height = "0";
    		//String length = "0";
    		String totalWeight = "0";
    		
    		
    		if(sum >= 1 && sum <= 4){
    			//width = "21";
    			//height = "11.5";
    			//length = "12";
    			totalWeight = "1000";
    		}else if(sum >= 5 && sum <= 7){
    			//width = "25";
    			//height = "16";
    			//length = "15";
    			totalWeight = "1500";
    			
    		}else if(sum >= 8 && sum <= 10){
    			//width = "22";
    			//height = "17";
    			//length = "28";
    			totalWeight = "2000";
    			
    		}else if(sum >= 11 && sum <= 12){
    			//width = "34";
    			//height = "22";
    			//length = "28";
    			totalWeight = "3200";
    			
    		}else if(sum >= 13){
    			//width = "40";
    			//height = "20";
    			//length = "15";
    			totalWeight = "3500";
    			
    		}
    		
    		//mapp.put("WIDTH", width);
    		//mapp.put("HEIGHT", height);
    		//mapp.put("LENGTH", length);
    		mapp.put("TOTAL_WEIGHT", totalWeight);
    		
    		mapp.put("RITEM_NM", nmBuilder.toString());
    		mapp.put("QTY", qtyBuilder.toString());
    		mapp.put("WEIGHT", weightBuilder.toString());
    		mapp.put("TOT", totBuilder.toString());
    		mapp.put("HS_CODE", hsBuilder.toString());
    		mapp.put("FROM", fromBuilder.toString());
    		mapp.put("UNIT", "");
    		mapp.put("INSURANCE_YN", "");
    		mapp.put("INSURANCE_PRICE", "");
    		
    		String dTel = (String)mapp.get("D_TEL");
    		String decryptdTel = OliveAes256.decrpyt(dTel);
    		
    		String dPost = (String)mapp.get("D_POST");
    		String decryptdPost = OliveAes256.decrpyt(dPost);
    		
    		String dAddress = (String)mapp.get("D_ADDRESS");
    		String decryptdAddress = OliveAes256.decrpyt(dAddress);
    		
    		String dCity = (String)mapp.get("D_CITY");
    		String dProvince = (String)mapp.get("D_PROVINCE");
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_ADDRESS", decryptdAddress + " " + decryptdAddress2 + ", " + dCity + ", " + dProvince);
    		mapp.put("D_ADDRESS2", "");
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", genericResultSet);
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveList2DHL
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveList2DHL(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ORG_ORD_NO"    , model.get("ORG_ORD_NO"+i));
                modelDt.put("ORD_DETAIL_NO" , model.get("ORD_DETAIL_NO"+i));
                modelDt.put("SS_USER_NO"	, (String)model.get(ConstantIF.SS_USER_NO));
                modelDt.put("SS_LC_ID"     	, (String)model.get(ConstantIF.SS_SVC_NO));
                
                dao.saveList2DHL(modelDt);
                
                m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("save.success"));
            }
            
        } catch(Exception e){
        	m.put("errCnt", -1);
            throw e;
            
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : list2DHL
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> emsAddress(Map<String, Object> model) throws Exception {
    	Map<String, Object> order = decryptOrderAddress(model);
		checkShipmentPublish(order);
    	return order;
    	    	
    }
    /*
     * 주문자정보 복호화
     * */
    @Override
    public Map<String, Object> decryptOrderAddress(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
    	
    	Map<String, Object> mapp = dao.emsAddress(model);
		
		String dTel = (String)mapp.get("D_TEL");
		String decryptdTel = OliveAes256.decrpyt(dTel);
		
		String dPost = (String)mapp.get("D_POST");
		String decryptdPost = OliveAes256.decrpyt(dPost);
		
		String dAddress = (String)mapp.get("D_ADDRESS");
		String decryptdAddress = OliveAes256.decrpyt(dAddress);
		
		String decryptdAddress2 = "";
		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
		}
		String dCity = (String)mapp.get("D_CITY");
		String dProvince = (String)mapp.get("D_PROVINCE");
		
		String dAddressTotal = decryptdAddress + " " + decryptdAddress2 + ", " + dCity + ", " + dProvince;

		String jTel = (String)mapp.get("J_TEL");
		String decryptjTel = OliveAes256.decrpyt(jTel);
		
		String jPost = (String)mapp.get("J_POST");
		String decryptjPost = OliveAes256.decrpyt(jPost);
		
		String jAddress = (String)mapp.get("J_ADDRESS");
		String decryptjAddress = OliveAes256.decrpyt(jAddress);
		
		String decryptjAddress2 = "";
		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
		}
		String jCity = (String)mapp.get("J_CITY");
		String jProvince = (String)mapp.get("J_PROVINCE");
		
		String jAddressTotal = decryptjAddress + " " + decryptjAddress2 + ", " + jCity + ", " + jProvince;
		
		map.put("D_TEL", decryptdTel);
		map.put("D_POST", decryptdPost);
		map.put("D_ADDRESS", dAddressTotal);

		map.put("J_TEL", decryptjTel);
		map.put("J_POST", decryptjPost);
		map.put("J_ADDRESS", jAddressTotal);
		String ordId = (String)model.get("ordId");
		map.put("ORD_ID", ordId);
		return map;
	}
    
    @Override
    public Map<String, Object> addressHistory(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List list  = dao.addressHistory(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		
    		String decryptdTel = "";
    		if(mapp.get("D_TEL") != null && !((String)mapp.get("D_TEL")).trim().equals("")){
    			decryptdTel = OliveAes256.decrpyt((String) mapp.get("D_TEL"));
    			
    			/*
    			
    			String middleMask = decryptdTel.substring(3, decryptdTel.length()-3);
    			String masking = "";
    			
    			for(int j=0; j<middleMask.length(); j++){
    				masking += "*";
    			}
    			decryptdTel = decryptdTel.substring(0,3)
    					+ masking
    					+ decryptdTel.substring(decryptdTel.length()-3, decryptdTel.length());
    			*/
    			
    		}
    		
    		String decryptdPost = "";
    		if(mapp.get("D_POST") != null && !((String)mapp.get("D_POST")).trim().equals("")){
    			decryptdPost = OliveAes256.decrpyt((String) mapp.get("D_POST"));
    			
    		}
    		
    		String decryptdAddress = "";
    		if(mapp.get("D_ADDRESS") != null && !((String)mapp.get("D_ADDRESS")).trim().equals("")){
    			decryptdAddress = OliveAes256.decrpyt((String) mapp.get("D_ADDRESS")); 
    		}
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		String decryptdCity = (String) mapp.get("D_CITY");
    		String decryptdProvince = (String) mapp.get("D_PROVINCE");
    		
    		String decryptjTel = "";
    		if(mapp.get("J_TEL") != null && !((String)mapp.get("J_TEL")).trim().equals("")){
    			decryptjTel = OliveAes256.decrpyt((String) mapp.get("J_TEL"));
    			
    			/*
    			String middleMask = decryptjTel.substring(3, decryptjTel.length()-3);
    			String masking = "";
    			
    			
    			for(int j=0; j<middleMask.length(); j++){
    				masking += "*";
    			}
    			decryptjTel = decryptjTel.substring(0,3)
    					+ masking
    					+ decryptjTel.substring(decryptjTel.length()-3, decryptjTel.length());
    			*/
    			
    		}
    		
    		 
    		String decryptjPost = "";
    		if(mapp.get("J_POST") != null && !((String)mapp.get("J_POST")).trim().equals("")){
    			decryptjPost = OliveAes256.decrpyt((String) mapp.get("J_POST")); 
    		}
    		
    		String decryptjAddress = "";
    		if(mapp.get("J_ADDRESS") != null && !((String)mapp.get("J_ADDRESS")).trim().equals("")){
    			decryptjAddress = OliveAes256.decrpyt((String) mapp.get("J_ADDRESS")); 
    		}

    		String decryptjAddress2 = "";
    		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
    			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
    		}
    		/*
    		String decryptDcustNm = "";
    		if(mapp.get("D_CUST_NM") != null && !((String)mapp.get("D_CUST_NM")).trim().equals("")){
    			decryptDcustNm = (String) mapp.get("D_CUST_NM");
    			String middleMask = decryptDcustNm.substring(3, decryptDcustNm.length()-3);
    			String masking = "";
    			
    			for(int j=0; j<middleMask.length(); j++){
    				masking += "*";
    			}
    			decryptDcustNm = decryptDcustNm.substring(0,3)
    					+ masking
    					+ decryptDcustNm.substring(decryptDcustNm.length()-3, decryptDcustNm.length());
    		}
    		*/
    		
    		String jCity = (String)mapp.get("J_CITY");
    		String jProvince = (String)mapp.get("J_PROVINCE");
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_PROVINCE", decryptdProvince);
    		mapp.put("D_ADDRESS", decryptdAddress);
    		mapp.put("D_ADDRESS2", decryptdAddress2);
    		
    		/* mapp.put("D_CUST_NM", decryptDcustNm); */
    		mapp.put("J_TEL", decryptjTel);
    		mapp.put("J_POST", decryptjPost);
    		mapp.put("J_ADDRESS", decryptjAddress);
    		mapp.put("J_ADDRESS2", decryptjAddress2);
    		String ordId = (String)model.get("ordId");
    		mapp.put("ORD_ID", ordId);
    		
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", list);
    	
    	return map;
    }
    
    @Override
    public Map<String, Object> addressOM010(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List list  = dao.addressHistory(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		
    		String decryptdTel = "";
    		if(mapp.get("D_TEL") != null && !((String)mapp.get("D_TEL")).trim().equals("")){
    			decryptdTel = OliveAes256.decrpyt((String) mapp.get("D_TEL")); 
    		}
    		
    		String decryptdPost = "";
    		if(mapp.get("D_POST") != null && !((String)mapp.get("D_POST")).trim().equals("")){
    			decryptdPost = OliveAes256.decrpyt((String) mapp.get("D_POST")); 
    		}
    		
    		String decryptdAddress = "";
    		if(mapp.get("D_ADDRESS") != null && !((String)mapp.get("D_ADDRESS")).trim().equals("")){
    			decryptdAddress = OliveAes256.decrpyt((String) mapp.get("D_ADDRESS")); 
    		}
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		String decryptdCity = (String) mapp.get("D_CITY");
    		String decryptdProvince = (String) mapp.get("D_PROVINCE");
    		
    		String decryptjTel = "";
    		if(mapp.get("J_TEL") != null && !((String)mapp.get("J_TEL")).trim().equals("")){
    			decryptjTel = OliveAes256.decrpyt((String) mapp.get("J_TEL")); 
    		}
    		 
    		String decryptjPost = "";
    		if(mapp.get("J_POST") != null && !((String)mapp.get("J_POST")).trim().equals("")){
    			decryptjPost = OliveAes256.decrpyt((String) mapp.get("J_POST")); 
    		}
    		
    		String decryptjAddress = "";
    		if(mapp.get("J_ADDRESS") != null && !((String)mapp.get("J_ADDRESS")).trim().equals("")){
    			decryptjAddress = OliveAes256.decrpyt((String) mapp.get("J_ADDRESS")); 
    		}

    		String decryptjAddress2 = "";
    		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
    			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
    		}
    		
    		String jCity = (String)mapp.get("J_CITY");
    		String jProvince = (String)mapp.get("J_PROVINCE");
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_PROVINCE", decryptdProvince);
    		mapp.put("D_ADDRESS", decryptdAddress);
    		mapp.put("D_ADDRESS2", decryptdAddress2);
    		

    		mapp.put("J_TEL", decryptjTel);
    		mapp.put("J_POST", decryptjPost);
    		mapp.put("J_ADDRESS", decryptjAddress);
    		mapp.put("J_ADDRESS2", decryptjAddress2);
    		String ordId = (String)model.get("ordId");
    		mapp.put("ORD_ID", ordId);
    		
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", list);
    	
    	return map;
    }
    
    private void checkShipmentPublish(Map<String, Object> model){
    	int count = dao.selectShipmentPublish(model);
    	if(count > 0){
    		dao.updateShipmentPublish(model);
    	}else{
    		dao.insertShipmentPublish(model);
    		
    	}
    	
    }
    
    @Override
    public Map<String, Object> export2EMS(Map<String, Object> model)
    		throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.export2EMS(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJava
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExport2EMS(List list) throws Exception {
        Map<String, Object> m = new HashMap<>();
        
    	dao.saveExport2EMS(list);
        
        m.put("MSG", MessageResolver.getMessage("save.success"));
        m.put("MSG_ORA", "");
        m.put("errCnt", 0);
            
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID      : saveExport2EMS_V2
     * 대체 Method 설명    : EMS 수출신고 엑셀업로드 ->  merge into처리 
     * 작성자              : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExport2EMS_V2(List list) throws Exception {
        Map<String, Object> m = new HashMap<>();
        
    	dao.saveExport2EMS_V2(list);
        
        m.put("MSG", MessageResolver.getMessage("save.success"));
        m.put("MSG_ORA", "");
        m.put("errCnt", 0);
            
        return m;
    } 
    
    @Override
    public String getOrdId(String orgOrdId) throws Exception {
    	return dao.getOrdId(orgOrdId);
    }
    
    @Override
    public int getEmsShipment(String ordId) throws Exception {
    	int cnt = dao.getEmsShipment(ordId);
    	return cnt;
    }
    
    @Override
    public boolean updateBlNo(List list) {
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> obj = (Map)list.get(i);
    		String blNo = (String)obj.get("NEW_BLNO");
    		if(blNo.startsWith("EG")){
    			dao.updateEmsBlNo(obj);
    			dao.deleteEmsTracking(obj);
    			
    		}else{
    			dao.updateDhlBlNo(obj);
    			dao.deleteDhlTracking(obj);
    			
    		}
    		dao.updateCommonBlNo(obj);
    		dao.deleteCommonTracking(obj);
    	}
    	return Boolean.valueOf(true);
    }
    
    @Override
    public Map<String, Object> checkBlNo(Map<String, Object> model)
    		throws Exception {
    	List<String> orgOrdIds = Arrays.asList("", ""); //개수만큼
    	List<String> blNos = Arrays.asList("", ""); //개수만큼
    	
    	System.out.println(orgOrdIds.size() + "  " + blNos.size());
    	int count = orgOrdIds.size();
    	
    	List<String> resultList = new ArrayList<>();
    	for(int i = 0; i < count; i++){
    		List<Map> result = dao.checkBlNo(orgOrdIds.get(i));
    		if(result.size() > 0){
    			String blNo = (String)result.get(0).get("BL_NO");
    			if(blNos.get(i).equals(blNo)){
    				resultList.add("이상없음 또는 UNIPASS 조회안됨");
    				//System.out.println("이상없음 또는 UNIPASS 확인 필요");
    			}else{
    				resultList.add("특송장번호 다름");
    				//System.out.println("특송장번호 다름");
    			}
    		}else{
    			resultList.add("접수내역 존재하지 않음");
    			//System.out.println("WMS전산에 접수내역 존재하지 않음");
    		}
    	}
    	
    	System.out.println(resultList.toString());
    	return null;
    }
    
    /**
     * Method ID   : listE5
     * Method 설명    : 올리브영 수출신고번호 or 운송장번호 누락 조회
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.listE5(model));
        return map;
    }
    
    /**
     * Method ID   : listE6
     * Method 설명    : 올리브영 운송장 출력 여부 조회
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        if(model.get("vrSrchShippingCompany6").equals("10")){
        	map.put("LIST", dao.listE6EMS(model));
        }else if(model.get("vrSrchShippingCompany6").equals("20")){
        	map.put("LIST", dao.listE6DHL(model));
        }else if(model.get("vrSrchShippingCompany6").equals("50")){
        	map.put("LIST", dao.listE6LXP(model));
        }else if(model.get("vrSrchShippingCompany6").equals("60")) {
        	map.put("LIST", dao.listE6FDX(model));
        }
        else{
        	map.put("LIST", dao.listE6QXP(model));
        }
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: selectPrintDt
     * 대체 Method 설명    : 올리브영 송장 출력 시간 조회
     * 작성자                    : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> selectPrintDt(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<>();
        List list  = dao.selectPrintDt(model);
        
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", list);
        
    	return map;
    } 
    
    /**
     * 
     * 대체 Method ID   : updatetPrintDt
     * 대체 Method 설명    : 올리브영 송장 출력 시간 업데이트
     * 작성자                      : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatetPrintDt(Map<String, Object> model) throws Exception {
    	
    	Map<String, Object> m = new HashMap<>();
    	
		m = (Map<String, Object>) dao.updatetPrintDt(model);
		m.put("errCnt", 0);
        
        return m;
    } 
    
    /*-
	 * Method ID		: crossDomainHttpQxpRequestPdf
	 * Method 설명	: 웹메소드 웹서비스 전송모듈
	 * 작성자			: W
	 */
	@Override
	public Map<String, Object> crossDomainHttpQxpRequestPdf(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod		= (String)model.get("cMethod");
        String cUrl				= (String)model.get("cUrl");
        String pOrgOrdId 	= (String)model.get("pOrgOrdId");
        String pBlNo 			= (String)model.get("pBlNo");
        m.put("BL_NO"			, pBlNo);
        m.put("ORG_ORD_ID"	, pOrgOrdId);
        
        String apiUrl 	= "https://52.79.206.98:5521/restv2/" + cUrl;
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("sUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "manage";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = "{\"ORG_ORD_ID\":\""+pOrgOrdId+"\"BL_NO : "+ pBlNo +" } ";
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/*-
	 * Method ID		: loginWcs
	 * Method 설명		: WCS 로그인 전송 모듈 (올리브영 MCS 연동)
	 * 작성자			: KSJ
	 */
	@Override
	public Map<String, Object> loginWcs() throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		JSONObject jsonData;
        String result = "false";
        String apiUrl 	= "http://10.30.1.13:9500/rest/man_login";
//        String apiUrl 	= "http://localhost:9500/rest/man_login";
        
        try{
        				
			//Json Data
			String jsonInputString = "{\"email\":\"admin@nearsolution.co.kr\",\"pass\":\"admin\"}";
			System.out.println("param jsonInputString : " + jsonInputString);
		
        	// disableSslVerification(); //ssl disable
			
			// CookieManager 설정
			CookieManager cookieManager = new CookieManager();
			cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
			CookieHandler.setDefault(cookieManager);
        	
        	// URL 설정
            URL url = new URL(apiUrl);
            
            // HttpURLConnection 객체 생성
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            
            // 요청 방식 설정 (GET, POST 등)
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type"	, "application/json");
            connection.setRequestProperty("Accept"			, "application/json");
			connection.setRequestProperty("x-domain-id"	, "logisall");
			
			// 데이터 body 설정  
			connection.setDoOutput(true);
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(jsonInputString);
            outputStream.flush();
            outputStream.close();
            
            // 응답 코드 확인
            int responseCode = connection.getResponseCode();
            System.out.println("응답 코드: " + responseCode);
            
            // 쿠키 저장소 가져오기
            CookieStore cookieStore = cookieManager.getCookieStore();
            
            
            // 응답 헤더에서 쿠키 추출
            Map<String, List<String>> headerFields = connection.getHeaderFields();
            List<String> cookiesHeader = headerFields.get("Set-Cookie");           
            
            if (cookiesHeader != null) {
                for (String cookie : cookiesHeader) {
                	if(!cookie.equals("")){
                		HttpCookie httpCookie = HttpCookie.parse(cookie).get(0);
                        cookieStore.add(url.toURI(), httpCookie);
                	}
                }
            }
            
            // 응답 데이터 읽기
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
            String line;
            StringBuilder response = new StringBuilder();
            
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            
            // 리더 종료
            reader.close();
            
            // 연결 종료
            connection.disconnect();
            
            // 응답 출력
            System.out.println("응답 내용:\n" + response.toString());
            
            // 쿠키 확인
            List<HttpCookie> cookies = cookieStore.getCookies();
            for (HttpCookie cookie : cookies) {
                System.out.println("쿠키 이름: " + cookie.getName());
                System.out.println("쿠키 값: " + cookie.getValue());
            }
            
            resultMap.put("responseCookies", cookieStore.getCookies());
            
            jsonData = new JSONObject(response.toString());
            if(jsonData.get("success").toString().equals("true")){
            	resultMap.put("response", "success");
            }else{
            	resultMap.put("response", "failed");
            }
            
        } catch(Exception e){
            throw e;
        }
        return resultMap;
	}
	
	/*-
	 * Method ID		: sendMcsOrder
	 * Method 설명		: WCS 웹서비스 전송모듈 (올리브영 MCS 연동)
	 * 작성자			: KSJ
	 */
	@Override
	public String sendMcsOrder(Map<String, Object> param) throws Exception {
		
		// 현재 날짜와 시간을 나타내는 Calendar 인스턴스 생성
        Calendar calendar = Calendar.getInstance();

        // 날짜를 yyyymmdd 형식으로 포맷팅하기 위한 SimpleDateFormat 인스턴스 생성
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        // 현재 날짜를 위에서 지정한 형식으로 포맷팅
        String formattedDate = formatter.format(calendar.getTime());
               
        JSONObject jsonData;
        String result = "false";
        String apiUrl = "http://10.30.1.13:9500/rest/mcs/order/send/" + formattedDate;
//        String apiUrl = "http://localhost:9500/rest/mcs/order/send/" + formattedDate;
        System.out.println(formattedDate);
        
        try{
        	
        	// disableSslVerification(); //ssl disable
        	        	
        	// URL 설정
            URL url = new URL(apiUrl);
            
            // HttpURLConnection 객체 생성
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // 쿠키 가져오기
            List<HttpCookie> cookies = (List<HttpCookie>) param.get("responseCookies");
            
            StringBuilder strCookie = new StringBuilder();
            
            // 쿠키를 헤더에 추가
            for (HttpCookie cookie : cookies) {
            	strCookie.append(cookie.getName()).append("=").append(cookie.getValue()).append(";");
            }
            
            // 요청 방식 설정 (GET, POST 등)
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type"	, "application/json");
            connection.setRequestProperty("Accept"			, "application/json");
            connection.setRequestProperty("Access-Control-Allow-Methods"			, "POST, PUT, GET, OPTIONS, DELETE");
            connection.setRequestProperty("Access-Control-Allow-Credentials"		, "true");
			connection.setRequestProperty("x-domain-id"		, "logisall");
			connection.setRequestProperty("Cookie"			, strCookie.toString());
            
            // 응답 코드 확인
            int responseCode = connection.getResponseCode();
            System.out.println("응답 코드: " + responseCode);
            
            // 응답 데이터 읽기
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
            String line;
            StringBuilder response = new StringBuilder();
            
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            
            reader.close();
            
            // 연결 종료
            connection.disconnect();
            
            jsonData = new JSONObject(response.toString());
            
            if(jsonData.get("retCode").equals(0)){
        		result = "success";
            }else{
        		result = "false";
        	}
            
            // 응답 출력
            System.out.println("응답 내용:\n" + response.toString());
            
        } catch(Exception e){
            throw e;
        }
        return result;
	}
	
	/*-
	 * Method ID		: cancelMcsOrder
	 * Method 설명		: WCS 웹서비스 전송모듈 (올리브영 MCS 연동)
	 * 작성자			: LCM
	 */
	@Override
	public String cancelMcsOrder(Map<String, Object> param, String orgOrdIds) throws Exception {
		
        JSONObject jsonData;
        String result = "false";
        String apiUrl = "http://10.30.1.13:9500/rest/mcs/order/cancel";
//        String apiUrl = "http://localhost:9500/rest/mcs/order/cancel";
        
        try{
        	
        	//Json Data
			String jsonInputString = "{\"orgOrdIds\":\""+orgOrdIds+"\"}";
			System.out.println("param jsonInputString : " + jsonInputString);
			
        	// disableSslVerification(); //ssl disable
        	        	
        	// URL 설정
            URL url = new URL(apiUrl);
            
            // HttpURLConnection 객체 생성
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // 쿠키 가져오기
            List<HttpCookie> cookies = (List<HttpCookie>) param.get("responseCookies");
            
            StringBuilder strCookie = new StringBuilder();
            
            // 쿠키를 헤더에 추가
            for (HttpCookie cookie : cookies) {
            	strCookie.append(cookie.getName()).append("=").append(cookie.getValue()).append(";");
            }
            
            // 요청 방식 설정 (GET, POST 등)
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type"	, "application/json");
            connection.setRequestProperty("Accept"			, "application/json");
            connection.setRequestProperty("Access-Control-Allow-Methods"			, "POST, PUT, GET, OPTIONS, DELETE");
            connection.setRequestProperty("Access-Control-Allow-Credentials"		, "true");
			connection.setRequestProperty("x-domain-id"		, "logisall");
			connection.setRequestProperty("Cookie"			, strCookie.toString());
            
			
			//body 추가
			connection.setDoOutput(true);
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(jsonInputString);
            outputStream.flush();
            outputStream.close();
            
            // 응답 코드 확인
            int responseCode = connection.getResponseCode();
            System.out.println("응답 코드: " + responseCode);
            
            // 응답 데이터 읽기
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
            String line;
            StringBuilder response = new StringBuilder();
            
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            
            reader.close();
            
            // 연결 종료
            connection.disconnect();
            
            jsonData = new JSONObject(response.toString());
            
            if(jsonData.get("retCode").equals(0)){
        		result = "success";
            }else{
        		result = "false";
        	}
            
            // 응답 출력
            System.out.println("응답 내용:\n" + response.toString());
            
        } catch(Exception e){
            throw e;
        }
        return result;
	}
	
	

	/**
     * 대체 Method ID		: disableSslVerification
     * 대체 Method 설명		: SSL 예외처리
     * 작성자				: W
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
    /**
     * 
     * 대체 Method ID   	: getShippingCompanyList
     * 대체 Method 설명    	: 올리브영 특송사 리스트
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> getShippingCompanyList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.getShippingCompanyList(model));
        return map;
    }
    
    @Override
    public int getSynnaraOrderCheck(Map<String, Object> model) throws Exception {
    	int cnt = dao.getSynnaraOrderCheck(model);
    	return cnt;
    }
    
    /**
     * 
     * 대체 Method ID   	: getSynnaraOrderList
     * 대체 Method 설명    	: 올리브영 신나라 주문 리스트
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> getSynnaraOrderList(Map<String, Object> model) throws Exception {
		  Map<String, Object> map = new HashMap<String, Object>();
	      if(model.get("page") == null) {
	          model.put("pageIndex", "1");
	      } else {
	          model.put("pageIndex", model.get("page"));
	      }
	      if(model.get("rows") == null) {
	          model.put("pageSize", "20");
	      } else {
	          model.put("pageSize", model.get("rows"));
	      }
	      
	      map.put("LIST", dao.getSynnaraOrderList(model));
	      return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: outBoundComplete
     * 대체 Method 설명    	: 올리브영 신나라 주문 리스트
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> outBoundComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<>();
    	dao.outBoundComplete(model);
    	
        m.put("MSG", MessageResolver.getMessage("save.success"));
        m.put("MSG_ORA", "");
        m.put("errCnt", "0");
            
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   	: updateDelYn
     * 대체 Method 설명    	: 올리브영 update delYn 
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	@Override
	public Map<String, Object> updateDelYn(Map<String, Object> model) throws Exception {
	    Map<String, Object> m = new HashMap<String, Object>();
	    
	    try{
	    	
	        int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	        if(tmpCnt > 0){
	        	List<String> ordId  = new ArrayList();
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
	            	ordId.add((String)model.get("ORD_ID"+i));
	            }
	            
	        	model.put("vrSrchOrdIdsArray", ordId);
	        	model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	        }
	        
	        m = (Map<String, Object>) dao.updateDelYn(model);
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
	        
	    } catch(Exception e){
	        throw e;
	    }
	    return m;
	}
	
	  /**
     * 
     * 대체 Method ID   	: sendPickingListRobot
     * 대체 Method 설명    	: 출고관리(올리브영) LG CNS 로봇 연동 테이블 피킹리스트 insert 
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	@Override
	public Map<String, Object> sendPickingListRobot(Map<String, Object> model) throws Exception {
	    Map<String, Object> m = new HashMap<String, Object>();
	    
	    try{
	    	  int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	          
	    	  if(tmpCnt > 0){
	          	List<String> ordId  = new ArrayList();
	              
	              for(int i = 0 ; i < tmpCnt ; i ++){
	              	ordId.add((String)model.get("ORD_ID_"+i));               
	              }
	              
	          	model.put("vrSrchOrdIdsArray", ordId);
	          }
	    	  
	    	  m = (Map<String, Object>) dao.sendPickingListRobot(model);
	    } catch(Exception e){
	        throw e;
	    }
	    
	    return m;
	}
	
	/**
     * Method ID : model
     * Method 설명 : DHL 수기 송장 접수
     * 작성자 : YSI
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> InvoiceReceipt(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        String OrdId = (String)model.get("ORD_ID");
        String blNo = (String)model.get("NEW_BLNO");
        String ShippingCompany = (String)model.get("SHIPPINGCOMPANY");
        String TrackingNumber = (String)model.get("NEW_TRACKING");
        String OrgOrdId = (String)model.get("ORG_ORD_ID");
        map.put("ORD_ID",OrdId);
        map.put("ORG_ORD_ID",OrgOrdId);
        map.put("NEW_BLNO", blNo);
        map.put("NEW_TRACKING", TrackingNumber);
        
        try {
        if(ShippingCompany.equals("DHL")) {
            dao.updateDhlBlNoTracking(map);
            dao.updateCommonBlNo(map);
        }else if(ShippingCompany.equals("EMS")){
            dao.updateEmsBlNo(map);
            dao.updateCommonBlNo(map);
        }                  
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
	@Override
	public Map<String, Object> selectUnsentInvoiceList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<>();
		try {
			resultMap = dao.selectUnsentInvoiceList(model);
		} catch(Exception e) {
			 log.error(e);
			 resultMap.put("errCnt", 1);
			 resultMap.put("MSG", e.getMessage());
		}
		return resultMap;
	}
}
