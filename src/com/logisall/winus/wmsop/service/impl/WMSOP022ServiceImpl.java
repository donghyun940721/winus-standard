package com.logisall.winus.wmsop.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP022Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP022Service")
public class WMSOP022ServiceImpl implements WMSOP022Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP022Dao")
    private WMSOP022Dao dao;

    /**
     * Method ID   : save
     * Method 설명    : 상품 검색 및 해당 상품 입고주문 처리 (완료까지, qty:1)
     * 작성자              : summer
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        // log.info(model);
        try{

        	//상품 정보 검색 
        	int resultCnt = Integer.parseInt(dao.itemSearchByBarcodeCnt(model));
        	
        	if(resultCnt == 0){
        		map.put("errCnt", 1);
            	map.put("MSG", "해당 고객 바코드로 조회 된 상품이 없습니다." );
        	}else if(resultCnt > 1){
        		map.put("errCnt", 1);
            	map.put("MSG", "해당 고객 바코드로 조회 된 상품이 여러 건 입니다." );
        	}else{
                String result = (String)dao.itemSearchByBarcode(model);
                System.out.println("====================> "+result);
                //해당 상품 1개 입고 주문 생성 및 완료 처리 
                if(result != "" && result != null){
                	
                	int cnt = 1;
                	String[] itemQty = new String[cnt];   
                	itemQty[0]  		= (String)model.get("vrOrdQty");

                	Map<String, Object> modelIns = new HashMap<String, Object>();
                	

                	modelIns.put("I_LC_ID"  		, (String)model.get("vrLcId"));
                	modelIns.put("I_ITEM_QTY"  		, itemQty);
                	modelIns.put("I_RITEM_ID"  		, result);
                	modelIns.put("I_ORD_SUBTYPE"  	, (String)model.get("vrordSubtype"));
                	
                	//session 정보
                    modelIns.put("USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                    modelIns.put("WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                    
                    System.out.println(Arrays.asList(modelIns));
                    
                    //dao                
                    modelIns = (Map<String, Object>)dao.saveOrdSimpleIn(modelIns);
                    
                    //프로시저 에러 처리 
                    String result_proc = String.valueOf(modelIns.get("O_MSG_CODE"));
                    if(result_proc == "-1"){
                    	map.put("errCnt", 1);
                    	map.put("MSG", (String)modelIns.get("O_MSG_NAME") );
                    }else{
                    	 map.put("errCnt", 0);
                         map.put("MSG", MessageResolver.getMessage("save.success"));
                    }
                    
                    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));  
                
                }else{
                	map.put("errCnt", 1);
                	map.put("MSG", "조회된 상품이 없습니다." );
                }
        	}
            
        } catch(BizException be) { 
        	map.put("errCnt", 1);
        	map.put("MSG", be.getMessage() );
        } catch(Exception e){
            throw e;
        }
        return map;
    }  
    

}
