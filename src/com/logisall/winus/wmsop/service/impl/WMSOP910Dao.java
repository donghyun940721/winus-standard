package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP910Dao")
public class WMSOP910Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID    : listE1
     * Method 설명      : 입고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
    	String LC_ID = (String)model.get("LC_ID");
    	if (LC_ID != null && StringUtils.isNotEmpty(LC_ID) && LC_ID.equals("0000002940")){//kcc
    		return executeQueryPageWq("wmsop910.listE1Kcc", model);
    	}else{
    		return executeQueryPageWq("wmsop910.listE1", model);
    	}
    }
    
    /**
     * Method ID    : listE2
     * Method 설명      : 출고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	if(model.get("SS_SVC_NO").equals("0000003620")){ // 비즈컨설팅 물류센터일 경우 (임시)
    		return executeQueryPageWq("wmsop910.listE2Biz", model);
    	}else if(model.get("SS_SVC_NO").equals("0000004240")){ // LG엔솔 센터일 경우 , 배차정보 속도 느릴 경우
    		return executeQueryPageWq("wmsop910.listE2CarCd", model);
    	}{
    		return executeQueryPageWq("wmsop910.listE2", model);
    	}
    }
    
    /**
     * Method ID    : listE3
     * Method 설명    : 화주별 출고관리  조회(OM)
     * 작성자               : KCR
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	if(model.get("SS_SVC_NO").equals("0000002860")){
    		return executeQueryPageWq("wmsop910.listE3Olive", model);
    	}else if(model.get("SS_SVC_NO").equals("0000003620")){ // 비즈컨설팅 물류센터일 경우 (임시)
    		return executeQueryPageWq("wmsop910.listE3Biz", model);
    	}else{
    		return executeQueryPageWq("wmsop910.listE3", model);
    	}
    }
    
    /**
     * Method ID    : listE4Header
     * Method 설명    : 출고관리 조회 헤더
     * 작성자               : KCR
     * @param   model
     * @return
     */
	public Object listE4Header(Map<String, Object> model) {
		List custs = list("wmsop910.listE4Header", model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
     /**
     * Method ID    : listE4Detail
     * Method 설명    : 출고관리 조회 디테일
     * 작성자               : KCR
     * @param   model
     * @return
     */
    public Object listE4Detail(Map<String, Object> model) {
        GenericResultSet genericResultSet = new GenericResultSet();
        genericResultSet.setList(list("wmsop910.listE4Detail", model));
        return genericResultSet;
    }
    
    /**
     * Method ID    : listE5Header
     * Method 설명    : 출고 B2C Header 조회
     * 작성자               : schan
     * @param   model
     * @return
     */
    public Object listE5Header(Map<String, Object> model) {
        return executeQueryPageWq("wmsop910.listE5Header", model);
    }
    
     /**
     * Method ID    : listE5Detail
     * Method 설명    : 출고 B2C Detail 조회
     * 작성자               : schan
     * @param   model
     * @return
     */
    public Object listE5Detail(Map<String, Object> model) {
        return executeQueryForList("wmsop910.listE5Detail", model);
    }
    
    /**
     * Method ID    : listE6
     * Method 설명      : 반품입고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listE6(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop910.listE6", model);
    }

	 /**
     * Method ID   		: listE1SummaryCount
     * Method 설명      : 입출고통합관리 -> 입고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE1SummaryCount(Map<String, Object> model) {
		return executeView("wmsop910.listE1SummaryCount", model);
	}
	
	 /**
     * Method ID   		: listE2SummaryCount
     * Method 설명      : 입출고통합관리 -> B2B 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE2SummaryCount(Map<String, Object> model) {
		if(model.get("SS_SVC_NO").equals("0000003620")){
			return executeView("wmsop910.listE2BizSummaryCount", model);
		}else{
			return executeView("wmsop910.listE2SummaryCount", model);
		}
	}
	
	 /**
     * Method ID   		: listE3SummaryCount
     * Method 설명      : 입출고통합관리 -> B2C 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE3SummaryCount(Map<String, Object> model) {
		if(model.get("SS_SVC_NO").equals("0000003620")){
			return executeView("wmsop910.listE3BizSummaryCount", model);
		}else{
			return executeView("wmsop910.listE3SummaryCount", model);
		}
	}
	
	 /**
     * Method ID   		: listE6SummaryCount
     * Method 설명      : 입출고통합관리 -> 반품입고관리 COUNT SUMMARY
     * 작성자           : 
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE6SummaryCount(Map<String, Object> model) {
		return executeView("wmsop910.listE6SummaryCount", model);
	}
	 
	/**
     * Method ID    	: sendMfcOrder
     * Method 설명      : 설비출고(창원)
     * 작성자           : ksj
     * @param   model
     * @return
     */
    public Object sendMfcOrder(Map<String, Object> model){
        executeUpdate("wmsop910.sp_wcs_auto_shipping_process", model);
        return model;
    }
    
    public List<Map<String, Object>> separateOrdId(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsop910.separateOrdId", model);
    }
    
    public Object dasmake(Map<String, Object> model){
        executeUpdate("wmsop910.dasmake", model);
        return model;
    }
    
    public Object getDasDegree(Map<String, Object> model){        
        return executeQueryForObject("wmsop910.getDasDegree", model);        
    }
    
    public int createDasDegreeFromOrder(Map<String, Object> model){
    	return executeUpdate("wmsop910.createDasDegreeFromOrder", model);        
    }   
    
	public Object sendDasData(Map<String, Object> model) {
		executeUpdate("wmsop910.sp_insert_das_coop_supply_info", model);
		return model;
	}

    public int createDasDegreeFromCoopOrder(Map<String, Object> model){
    	return executeUpdate("wmsop910.createDasDegreeFromCoopOrder", model);        
    }    

	public Object dasMakeOrderDegree(Map<String, Object> model) {
		 executeUpdate("wmsop910.dasMakeOrderDegree", model);
	     return model;
	}
    
    public Object dasmake_typeUpdate(Map<String, Object> model){
        executeUpdate("wmsop910.dasmake_typeUpdate", model);
        return model;
    }
    
    public Object dasMakeOrderDegree_typeUpdate(Map<String, Object> model){
        executeUpdate("wmsop910.dasMakeOrderDegree_typeUpdate", model);
        return model;
    }
    
	public int checkCanDeleteDas(Map<String, Object> model) {
		return (int)executeView("wmsop910.checkCanDeleteDas", model);
	}
	
	public int checkCanDeleteDas_one(Map<String, Object> model) {
		return (int)executeView("wmsop910.checkCanDeleteDas_one", model);
	}
    
    public Object dasDelete(Map<String, Object> model){
        executeUpdate("wmsop910.dasDelete", model);
        return model;
    }
    
    public Object dasDelete_if(Map<String, Object> model){
        executeUpdate("wmsop910.dasDelete_if", model);
        return model;
    }
    
    public Object dasdelete_typeUpdate(Map<String, Object> model){
        executeUpdate("wmsop910.dasdelete_typeUpdate", model);
        return model;
    }
	
    public List dasIfSendList(Map<String, Object> model) {
    	List rstList = list("wmsop910.dasIfSendList", model);
    	return rstList;
    }
    
	public List checkCanIFDas(Map<String, Object> model) {
		List rstList = list("wmsop910.checkCanIFDas", model);
		return rstList;
	}
    
    public Object deviceIfSendInsert(Map<String, Object> model) {
  		return executeInsert("wmsop910.deviceIfSendInsert", model);
  	}

	public List dasKotechIfSendListB2C(Map<String, Object> model) {
		List rstList = list("wmsop910.dasKotechIfSendListB2C", model);
    	return rstList;
	}

	public Object deviceIfSendInsertB2C(Map<String, Object> model) {
		return executeInsert("wmsop910.deviceIfSendInsertB2C", model);
		
	}

	public List dasKotechIfSendListNoInvcB2C(Map<String, Object> model) {
		List rstList = list("wmsop910.dasKotechIfSendListNoInvcB2C", model);
    	return rstList;
	}

	public List dasKotechIfSendListNoInvcNoCoopB2C(Map<String, Object> model) {
		List rstList = list("wmsop910.dasKotechIfSendListNoInvcNoCoopB2C", model);
    	return rstList;
	}
	
	public Object getDevOrdDegree(Map<String, Object> model){
        return executeQueryForList("wmsop910.getDevOrdDegree", model);
    }
	
    public List<Map<String, Object>> separateOrdIdB2C(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsop910.separateOrdIdB2C", model);
    }
    
    public Object degreeMakeB2C(Map<String, Object> model){
        executeUpdate("wmsop910.degreeMakeB2C", model);
        return model;
    }
    
    /*
     * 추후 분리 필요..
     * */
    public GenericResultSet listE1ExcelByTurkiye(Map<String, Object> model) {
    	return executeQueryPageWq("turkiye.listE1", model);
    }

}
