package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.components.headertoolbar.actions.MoveElementCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.impl.WMSMS090Dao;
import com.logisall.winus.wmsop.service.WMSOP910Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;




























/*SF*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.json.simple.JSONArray;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;

@Service("WMSOP910Service")
public class WMSOP910ServiceImpl implements WMSOP910Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP910Dao")
    private WMSOP910Dao dao;
    
	@Resource(name = "WMSMS090Dao")
	private WMSMS090Dao dao090;
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;
    
    /**
	 * 대체 Method ID : selectData 대체 Method 설명 : 필요 데이타셋 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ITEMGRP", dao090.selectItemGrp(model));
		return map;
	}
	
	
	
    /**
     * Method ID	: listE1
     * Method 설명	: 입고관리 조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        model.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
        
        GenericResultSet res = dao.listE1(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		//tempMemo = org.springframework.web.util.HtmlUtils.htmlUnescape(tempMemo);
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			System.out.println(object.get("1").toString());
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
        res.setList(list);
        map.put("LIST", res);
        return map;
    }
    
    /**
     * Method ID	: listE2
     * Method 설명	: 출고관리 조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        //등록차수
        if(model.get("selectCnt") != null && !model.get("selectCnt").equals("")){
        	int selectCnt = Integer.parseInt(model.get("selectCnt").toString());
  	        String[] ordDegreeValuesStrs = new String[selectCnt];
  	        
  	        if(selectCnt == 1){
  	        	ordDegreeValuesStrs[0] =  String.valueOf(model.get("vrSrchOrdDegreeSelectBox"));
  	        }else{
  	        	 ordDegreeValuesStrs =  (String[]) model.get("vrSrchOrdDegreeSelectBox");
  	        }
  	        
  	        if(selectCnt > 0){
  	        	List<String> vrOrdInsDtOrdDegreeList   = new ArrayList<String>();
  	        	List<String> vrOrdInsDtList   = new ArrayList<String>();
  	            for(int i = 0 ; i < selectCnt ; i ++){
  		        	String valueStr = (String) ordDegreeValuesStrs[i];
  		        	if(valueStr != null && !valueStr.equals("")){
  			        	String[] strArr = valueStr.split("_");
  			        	if(strArr.length>1){
  			        		//날짜 + 차주
  			        		vrOrdInsDtOrdDegreeList.add(valueStr);
  			        	}else{
  			        		//날짜
  			        		vrOrdInsDtList.add(valueStr);
  			        	}
  		        	}
  		        }
  	            
  	            if(vrOrdInsDtList.size() > 0 && vrOrdInsDtOrdDegreeList.size() > 0 ){
  	            	model.put("vrOrdInsDtOrdDegreeType","ALL");
  	            }else{
  	            	model.put("vrOrdInsDtOrdDegreeType", null);
  	            }
  	        	
  	        	model.put("vrOrdInsDtOrdDegreeList", vrOrdInsDtOrdDegreeList);
  	        	model.put("vrOrdInsDtList", vrOrdInsDtList);
  	        }
        }
      
        GenericResultSet res = dao.listE2(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			// 통합 출고관리(B2B) 결과 확인용 print 추후 삭제예정
			System.out.println("resList : " + (Map<String, String>)list.get(0));
			//System.out.println("REC_LOC : " + ((Map<String, String>)list.get(0)).get("P_REC_LOC"));
			//System.out.println("STOCK_UNIT_NO : " + ((Map<String, String>)list.get(0)).get("P_STOCK_UNIT_NO"));
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
				//System.out.println("REC_LOC : " + tempMap.get("REC_LOC"));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        			
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
        res.setList(list);
        map.put("LIST", res);
        return map;
    }

    /**
     * Method ID	: listE3
     * Method 설명	: 출고관리 조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        if(model.get("vrSrchOrdSubtype").equals("140")) {
            
            model.put("vrGubun1", "50"); //재고이동
            model.put("vrGubun2", "132"); //유통점출고
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
       	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
       	 String chekbox[] = {subtypeGubun3};
       	 
       	 model.put("chekbox", chekbox);
       } 
        
        if("on".equals(model.get("chkEmptyStock"))){
            model.put("chkEmptyStock", "1");
        }
        
        if("on".equals(model.get("chkBackOrder"))){
            model.put("chkBackOrder", "Y");
        }else{
        	model.put("chkBackOrder", "N");
        }
        
        List<String> vrSrchMulOrdIdArr = new ArrayList();
        String[] spVrSrchMulOrdId = model.get("vrSrchMulOrdId").toString().split(",");
        for (String keyword : spVrSrchMulOrdId ){
        	vrSrchMulOrdIdArr.add(keyword);
        }
        model.put("vrSrchMulOrdIdArr", vrSrchMulOrdIdArr);
        
        GenericResultSet res = dao.listE3(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		//org.springframework.web.util.HtmlUtils.htmlUnescape()
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			System.out.println(object.get("1").toString());
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
		
		res.setList(list);
        map.put("LIST", res);
        return map;
    }
    
    /**
     * Method ID   : listE4Header
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4Header(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("vrSrchOrdSubtype").equals("140")){
    		List<String> arrList = new ArrayList();
    		arrList.add("50");
    		arrList.add("132");
	        model.put("vrSrchOrdSubtypeArr", arrList);
    	}
    
        map.put("LIST", dao.listE4Header(model));
        return map;
    }
    
    /**
     * Method ID   : listE4Detail
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4Detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE4Detail(model));
        return map;
    }
    
    /**
     * Method ID   : listE5Header
     * Method 설명    : 출고 B2C Header 조회
     * 작성자               : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE5Header(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        int minRowNum = 0;
        int maxRowNum = 100000;
        try{
            int pageIndex = 1;
            if(model.get("page") != null) {
                pageIndex = Integer.parseInt((String)model.get("page"));
            }
            int pageSize = 20;
            if(model.get("rows") != null) {
                pageSize = Integer.parseInt((String)model.get("rows"));
            }
            minRowNum = ((pageIndex-1) * pageSize) + 1;
            maxRowNum = ((pageIndex) * pageSize);
        }catch(Exception e){
            e.printStackTrace();
        }
        model.put("MIN_ROW_NUM", minRowNum);
        model.put("MAX_ROW_NUM", maxRowNum);
        
        if(model.get("vrSrchOrdSubtype").equals("140")){
            List<String> arrList = new ArrayList();
            arrList.add("50");
            arrList.add("132");
            model.put("vrSrchOrdSubtypeArr", arrList);
        }
        
        List<String> vrSrchMulOrdIdArr = new ArrayList();
        String[] spVrSrchMulOrdId = model.get("vrSrchMulOrdId").toString().split(",");
        for (String keyword : spVrSrchMulOrdId ){
            vrSrchMulOrdIdArr.add(keyword);
        }
        model.put("vrSrchMulOrdIdArr", vrSrchMulOrdIdArr);
        
        map.put("rows", dao.listE5Header(model));
        return map;
    }
    
    /**
     * Method ID   : listE5Detail
     * Method 설명    : 출고 B2C Detail 조회
     * 작성자               : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE5Detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE5Detail(model));
        return map;
    }
    
    /**
     * Method ID	: listE6
     * Method 설명	: 반품입고관리 조회
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        model.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
        
        GenericResultSet res = dao.listE6(model);
        List list = res.getList();
//        
//        int len = list.size();
//		if(len > 0){
//			JsonParser jsonParser = new JsonParser();
//			for(int i = 0 ; i < len ; i++){
//				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
//	        	String tempMemo = tempMap.get("USER_MEMO");
//	        	if(tempMemo != null && !tempMemo.equals("")){
//	        		//tempMemo = org.springframework.web.util.HtmlUtils.htmlUnescape(tempMemo);
//	        		try{
//	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
//		        		if(!object.isJsonNull()){
//		        			System.out.println(object.get("1").toString());
//		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
//		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
//		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
//		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
//		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
//		        			
//		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
//		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
//		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
//		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
//		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
//		        		}
//	        		}catch(Exception e){
//	        			//System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
//	        			log.info("USER_MEMO ERROR : NOT JSONParser STRING");
//	        		}
//	        	}
//	        }
//		}
        res.setList(list);
        map.put("LIST", res);
        return map;
    }
    
    
    /**
     * Method ID   		: listE1SummaryCount
     * Method 설명      : 입출고통합관리 -> 입고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1SummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE1SummaryCount(model));
        return map;
    }
    
    /**
     * Method ID   		: listE2SummaryCount
     * Method 설명      : 입출고통합관리 -> B2B 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2SummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE2SummaryCount(model));
        return map;
    }
    
    
    /**
     * Method ID   		: listE3SummaryCount
     * Method 설명      : 입출고통합관리 -> B2C 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3SummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE3SummaryCount(model));
        return map;
    }
    
    /**
     * Method ID   		: listE6SummaryCount
     * Method 설명      : 입출고통합관리 -> 반품입고관리 COUNT SUMMARY
     * 작성자           : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE6SummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE6SummaryCount(model));
        return map;
    }
    
    /**
     * 
     * Method ID   		: sendMfcOrder
     * Method 설명    	: 설비출고(창원)
     * 작성자           : ksj
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> sendMfcOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg = MessageResolver.getMessage("save.error");
        
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] custLotNo = new String[tmpCnt];      
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID_"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ_"+i);
                    custLotNo[i]   = (String)model.get("CUST_LOT_NO_"+i);         
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("custLotNo", custLotNo);
                
                modelIns.put("LC_ID", (String)model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                modelIns = (Map<String, Object>)dao.sendMfcOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> dasMake(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	List arrayList_ordid = new ArrayList();
            	String old_ordId = "";
            	String ordId = "";
            	String custId = "";
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                for(int i = 0 ; i < tmpCnt ; i ++){
                	String ordId_work   = (String)model.get("ORD_ID"+i);               
                    custId  = (String)model.get("CUST_ID"+i); 
                    
                    arrayList_ordid.add(ordId_work);
                }
                
                //session 및 등록정보
                modelIns.put("LC_ID", LC_ID);
                modelIns.put("WORK_IP", WORK_IP);
                modelIns.put("USER_NO", USER_NO);
                
                //프로시져에 보낼것들 다담는다
                modelIns.put("ordId", arrayList_ordid);
                modelIns.put("custId", custId);
                
                List<Map<String,Object>> separateList = new ArrayList();
                separateList = dao.separateOrdId(modelIns);
                int separateListCnt = separateList.size();
                String workOrdIdList = "";
                int cnt = 120; // cell cnt : 120
                for(int i = 0 ; i < separateListCnt ; i ++){
                	Map<String,Object> workMap = (Map<String,Object>)separateList.get(i);
                	if(i % cnt == 0 || i == 0){
                		workOrdIdList = (String) workMap.get("ORD_ID_LIST");
                	}else{
                		workOrdIdList += ","  + (String) workMap.get("ORD_ID_LIST");
                	}
                	// 120번째 또는 마지막 반복문에서 das를 생성한다. 
                    if ((i != 0 && (i+1) % cnt == 0) || i == separateListCnt - 1) {
                        // 조건을 만족할 때 실행되는 코드
                    	System.out.println(workOrdIdList);
                    	modelIns.put("ordId", workOrdIdList);
                    	dao.dasmake(modelIns);
//                    	String ORD_DEVICE_TYPE = "06";
//                    	modelIns.put("ORD_DEVICE_TYPE", ORD_DEVICE_TYPE);
//                    	dao.dasmake_typeUpdate(modelIns);
                    	workOrdIdList = "";                                                                                                                
                    }
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> createDasDegreeFromOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();        
        
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();             	
        	Map<String, Object> resultSet = new HashMap<String, Object>();
        	String lcId = model.get("SS_SVC_NO").toString();
        	String selectedDevOrgDegree = model.get("vrDevOrgDegree").toString();
        	
            //session 및 등록정보
            modelIns.put("LC_ID", lcId);
            modelIns.put("WORK_IP", model.get("SS_CLIENT_IP").toString());
            modelIns.put("USER_NO", model.get("SS_USER_NO").toString());
            
            //프로시져에 보낼것들 다담는다
            modelIns.put("CUST_ID", model.get("vrCustId").toString());
            modelIns.put("ORD_DT", model.get("vrOrdDt").toString());            
            modelIns.put("ORD_DEGREE", model.get("vrOrdDegree").toString());
            modelIns.put("MAX_CELL_CNT", model.get("vrMaxCellCnt").toString());
            modelIns.put("MAX_BATCH_CNT", model.get("vrMaxBatchCnt").toString());
            
            int maxBatchCnt = Integer.parseInt((String) model.get("vrMaxBatchCnt"));                      
            
            for(int i = 0 ; i < maxBatchCnt ; i ++) {
            	Map<String,Object> dasDegree = (Map<String,Object>) dao.getDasDegree(modelIns);
            	Map<String, Object> params = new HashMap<String, Object>();            	//
            	int result = 0;           	
            	String devOrdDegree = selectedDevOrgDegree.isEmpty() ? dasDegree.get("DEV_ORG_DEGREE").toString() : selectedDevOrgDegree; // 선택한 설비차수에 추가 혹은 신규 생성
            	
            	params = modelIns;            	
            	params.put("DEV_ORG_DEGREE", devOrdDegree);            	
            	
            	/* KCP 동안성 (COOP) : ERP 정보 기준 차수 생성 */ 
            	if(lcId.equals("0000003540")) {
            		params.put("WAREKY", model.get("coopWorkKeyPopE1").toString());
            		result = dao.createDasDegreeFromCoopOrder(params);	
            	}
            	else {
            		result = dao.createDasDegreeFromOrder(params);
            	}            	
            	
            	// 할당 대상 주문이 없음.
            	if(result <= 0) {
            		break;
            	}
            	
            	resultSet.put(devOrdDegree, result);        
            }
            
            m.put("result", resultSet);
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));                     
			
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
        	m.put("errCnt", 1);      
			m.put("MSG", e.getMessage());            
        }
        
        return m;
    }
    
    @Override
    public Map<String, Object> sendDasData(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();             	
        	Map<String, Object> result = new HashMap<String, Object>();                   
            
            //session 및 등록정보
            modelIns.put("LC_ID",  model.get("SS_SVC_NO").toString());
            modelIns.put("WORK_IP", model.get("SS_CLIENT_IP").toString());
            modelIns.put("USER_NO", model.get("SS_USER_NO").toString());
            
            //프로시져에 보낼것들 다담는다
            modelIns.put("CUST_ID", model.get("CUST_ID").toString());
            modelIns.put("OUT_REQ_DT", model.get("OUT_REQ_DT").toString());            
            modelIns.put("DEV_ORD_DEGREE", model.get("DEV_ORD_DEGREE").toString());   
            
            result = (Map<String, Object>) dao.sendDasData(modelIns);         
            
            if(Integer.parseInt(result.get("O_MSG_CODE").toString()) < 0) {
            	m.put("errCnt", 1);
            	m.put("MSG", result.get("O_MSG_NAME").toString());            	
            }
            else{
            	m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("save.success"));	
            }

        } catch(Exception e){
            throw e;
        }
        
        return m;
    }
    
    @Override
    public Map<String, Object> dasMakeOrderDegree(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                
                //session 및 등록정보
                modelIns.put("LC_ID", LC_ID);
                modelIns.put("WORK_IP", WORK_IP);
                modelIns.put("USER_NO", USER_NO);
                modelIns.put("USER_NO", USER_NO);
                modelIns.put("USER_NO", USER_NO);
                modelIns.put("USER_NO", USER_NO);
                
                modelIns.put("ORDDEGREE", (String)model.get("ordDegree"));
                modelIns.put("ORDDEGREEDATE", (String)model.get("ordDegreeDate"));
                modelIns.put("CUSTID", (String)model.get("custId"));
                
            	dao.dasMakeOrderDegree(modelIns);
            	String ORD_DEVICE_TYPE = "06";
            	modelIns.put("ORD_DEVICE_TYPE", ORD_DEVICE_TYPE);
            	dao.dasMakeOrderDegree_typeUpdate(modelIns);
            	
            	

            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> dasDelete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	String vrDEV_ORD_DEGREE = "";
            	String vrORD_ID = "";
            	String DEV_ORD_DEGREE = (String)model.get("DEV_ORD_DEGREE"+0);
            	
                if(DEV_ORD_DEGREE == null ||DEV_ORD_DEGREE.isEmpty() || DEV_ORD_DEGREE.equals("")){
                	// 주문번호로 삭제
                	for(int i = 0 ; i < tmpCnt ; i ++){
                        String custId  = (String)model.get("CUST_ID"+i);
                        String ordId = (String)model.get("ORD_ID"+i);
                        
                        String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                        String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                        String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                    	if(i == 0){
                    		vrORD_ID = "'" + ordId + "'";
                    	}else {
                            if (!(vrORD_ID.contains("'" + ordId + "'"))) {
                            	vrORD_ID += ",'" + ordId + "'";
                            } 
                    	}
                    	
                    	if(i == tmpCnt-1){
    	                    //프로시져에 보낼것들 다담는다
    	                    modelIns.put("custId", custId);
    	                    modelIns.put("ordId", vrORD_ID);
    	                    
    	                    //session 및 등록정보
    	                    modelIns.put("LC_ID", LC_ID);
    	                    modelIns.put("WORK_IP", WORK_IP);
    	                    modelIns.put("USER_NO", USER_NO);
    	                    
    	                    int cntIfFlag = (int) dao.checkCanDeleteDas_one(modelIns);
    	                    if(cntIfFlag > 0){
    	                    	m.put("errCnt", -1);
    	                        m.put("MSG", "if 주문 생성(전송)된 주문번호가 있습니다");
    	                    	return m;
    	                    }else {
    	                    	dao.dasDelete(modelIns);
    	                    }
                    	}
                    }
                }else{
                	// 차수로 삭제
                	for(int i = 0 ; i < tmpCnt ; i ++){
                        String custId  = (String)model.get("CUST_ID"+i);
                        DEV_ORD_DEGREE = (String)model.get("DEV_ORD_DEGREE"+i);
                        
                        String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                        String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                        String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                    	if(i == 0){
                    		vrDEV_ORD_DEGREE = "'" + DEV_ORD_DEGREE + "'";
                    	}else {
                            if (!(vrDEV_ORD_DEGREE.contains("'" + DEV_ORD_DEGREE + "'"))) {
                            	vrDEV_ORD_DEGREE += ",'" + DEV_ORD_DEGREE + "'";
                            } 
                    	}
                    	
                    	if(i == tmpCnt-1){
    	                    //프로시져에 보낼것들 다담는다
    	                    modelIns.put("custId", custId);
    	                    modelIns.put("DEV_ORD_DEGREE", vrDEV_ORD_DEGREE);
    	                    
    	                    //session 및 등록정보
    	                    modelIns.put("LC_ID", LC_ID);
    	                    modelIns.put("WORK_IP", WORK_IP);
    	                    modelIns.put("USER_NO", USER_NO);
    	                    
    	                    int cntIfFlag = (int) dao.checkCanDeleteDas(modelIns);
    	                    if(cntIfFlag > 0){
    	                    	m.put("errCnt", -1);
    	                        m.put("MSG", "if 전송 완료된 차수가 있습니다");
    	                    	return m;
    	                    }else {
    	                    	dao.dasDelete_if(modelIns);
    	                    	dao.dasdelete_typeUpdate(modelIns);
    	                    	dao.dasDelete(modelIns);
    	                    }
                    	}
                    }
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> dasIfSend(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
        try{
        	String vrDEV_ORD_DEGREE = "";
        	String custId  = "";
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    custId  = (String)model.get("CUST_ID"+i);
                    String DEV_ORD_DEGREE = (String)model.get("DEV_ORD_DEGREE"+i);
                    if(DEV_ORD_DEGREE.isEmpty() || DEV_ORD_DEGREE.equals("")){
                    	m.put("errCnt", -1);
                        m.put("MSG", "차수가 없는 주문이 있습니다." );
                        return m;
                    }
                    
                	if(i == 0){
                		vrDEV_ORD_DEGREE = "'" + DEV_ORD_DEGREE + "'";
                	}else {
                        if (!(vrDEV_ORD_DEGREE.contains("'" + DEV_ORD_DEGREE + "'"))) {
                        	vrDEV_ORD_DEGREE += ",'" + DEV_ORD_DEGREE + "'";
                        } 
                	}
                }
                //프로시져에 보낼것들 다담는다
            	model.put("custId", custId);
            	model.put("DEV_ORD_DEGREE", vrDEV_ORD_DEGREE);
            	
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                
                //session 및 등록정보
            	model.put("LC_ID", LC_ID);
            	model.put("USER_NO", USER_NO);
            	model.put("WORK_IP", WORK_IP);
            	
            	//Das list select
            	List list = null;          	

            	list  = dao.dasIfSendList(model);

            	int listCnt = list.size();
            	
            	if(listCnt > 0){
            		
        			List list_chk = dao.checkCanIFDas(model);
            		int list_chkCnt = list_chk.size();
            		if(list_chkCnt > 0){
                        m.put("errCnt", -1);
                        String msg = "성화택배 지점정보에 저장되지 않은 출고처가 있습니다 ";
                        for(int i = 0 ; i < list_chkCnt ; i ++){
                        	String TRANS_CUST_CD = (String)((Map<String, String>)list_chk.get(i)).get("TRANS_CUST_CD");
                        	String TRANS_CUST_NM = (String)((Map<String, String>)list_chk.get(i)).get("TRANS_CUST_NM");
                        	
                        	msg += "\n";
                        	msg += TRANS_CUST_CD + "( " + TRANS_CUST_NM + " )";
                        }
                        m.put("MSG", MessageResolver.getMessage(msg));
                        return m;
            		}
            		
            		for(int i = 0 ; i < listCnt ; i ++){
            			Map<String, Object> modelIns = new HashMap<String, Object>();
            			modelIns.put("LC_ID", (String)((Map<String, String>)list.get(i)).get("LC_ID"));
            			modelIns.put("CUST_ID", (String)((Map<String, String>)list.get(i)).get("CUST_ID"));
            			modelIns.put("ORDER_DATE", (String)((Map<String, String>)list.get(i)).get("ORDER_DATE"));
            			modelIns.put("ORDER_WAVE", (String)((Map<String, String>)list.get(i)).get("ORDER_WAVE"));
            			modelIns.put("ORDER_TYPE", (String)((Map<String, String>)list.get(i)).get("ORDER_TYPE"));
            			modelIns.put("ORDER_NO", (String)((Map<String, String>)list.get(i)).get("ORDER_NO"));
            			modelIns.put("STORE_CD", (String)((Map<String, String>)list.get(i)).get("STORE_CD"));
            			modelIns.put("STORE_NM", (String)((Map<String, String>)list.get(i)).get("STORE_NM"));
            			modelIns.put("ITEM_CD", (String)((Map<String, String>)list.get(i)).get("ITEM_CD"));
            			modelIns.put("ITEM_NM", (String)((Map<String, String>)list.get(i)).get("ITEM_NM"));
            			modelIns.put("ITEM_QTY", (String)((Map<String, String>)list.get(i)).get("ITEM_QTY"));
            			modelIns.put("IF_FLAG", (String)((Map<String, String>)list.get(i)).get("IF_FLAG"));
            			modelIns.put("SH_RCV_ADDR", (String)((Map<String, String>)list.get(i)).get("SH_RCV_ADDR"));
            			modelIns.put("SH_RCV_TEL", (String)((Map<String, String>)list.get(i)).get("SH_RCV_TEL"));
            			modelIns.put("SH_RCV_NM", (String)((Map<String, String>)list.get(i)).get("SH_RCV_NM"));
            			modelIns.put("SH_REGION", (String)((Map<String, String>)list.get(i)).get("SH_REGION"));
            			modelIns.put("SH_BRAND_NM", (String)((Map<String, String>)list.get(i)).get("SH_BRAND_NM"));
            			modelIns.put("SH_BRAND_CD", (String)((Map<String, String>)list.get(i)).get("SH_BRAND_CD"));
            			modelIns.put("SH_SEND_DATE", (String)((Map<String, String>)list.get(i)).get("SH_SEND_DATE"));
            			modelIns.put("SH_SND_NM", (String)((Map<String, String>)list.get(i)).get("SH_SND_NM"));
            			modelIns.put("SH_SND_TEL", (String)((Map<String, String>)list.get(i)).get("SH_SND_TEL"));
            			modelIns.put("SH_BRAND", (String)((Map<String, String>)list.get(i)).get("SH_BRAND"));
            			modelIns.put("ORDER_SEQ", (String)((Map<String, String>)list.get(i)).get("ORDER_SEQ"));
            			modelIns.put("USER_NO", USER_NO);
            			
//                			Das i/f table insert 
            			dao.deviceIfSendInsert(modelIns);
            			
            			// DAS 주문 전송 시 해당 컬럼 업데이트로 변경 / 2024.11.15 / KSG
                    	String ORD_DEVICE_TYPE = "06";
                    	modelIns.put("ORD_DEVICE_TYPE", ORD_DEVICE_TYPE);
                    	dao.dasmake_typeUpdate(modelIns);
            		}
        		}

                m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("save.success"));
            }
        }catch(Exception e){
        	m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    
    @Override
    public Map<String, Object> dasIfSendB2C(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
        try{        	
        	String vrDEV_ORD_DEGREE = "";
        	String custId  = "";
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());        	
        	
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    custId  = (String)model.get("CUST_ID"+i);
                    String DEV_ORD_DEGREE = (String)model.get("DEV_ORD_DEGREE"+i);
                    if(DEV_ORD_DEGREE == null || DEV_ORD_DEGREE.isEmpty() || DEV_ORD_DEGREE.equals("")){
                    	m.put("errCnt", -1);
                        m.put("MSG", "차수가 없는 주문이 있습니다." );
                        return m;
                    }
                    
                	if(i == 0){
                		vrDEV_ORD_DEGREE = "'" + DEV_ORD_DEGREE + "'";
                	}else {
                        if (!(vrDEV_ORD_DEGREE.contains("'" + DEV_ORD_DEGREE + "'"))) {
                        	vrDEV_ORD_DEGREE += ",'" + DEV_ORD_DEGREE + "'";
                        } 
                	}
                }
                //프로시져에 보낼것들 다담는다
            	model.put("custId", custId);
            	model.put("DEV_ORD_DEGREE", vrDEV_ORD_DEGREE);
            	
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                
                //session 및 등록정보
            	model.put("LC_ID", LC_ID);
            	model.put("USER_NO", USER_NO);
            	model.put("WORK_IP", WORK_IP);
            	
            	//Das list select
            	List list = null;

            	list  = dao.dasKotechIfSendListB2C(model);

            	int listCnt = list.size();
            	
            	if(listCnt > 0){

        			for(int i = 0 ; i < listCnt ; i ++){
        				
        				Map<String, Object> modelIns = new HashMap<String, Object>();
            			modelIns.put("LC_ID", (String)((Map<String, String>)list.get(i)).get("LC_ID"));
            			modelIns.put("CUST_ID", (String)((Map<String, String>)list.get(i)).get("CUST_ID"));
            			modelIns.put("ORDER_DATE", (String)((Map<String, String>)list.get(i)).get("ORDER_DATE"));
            			modelIns.put("ORDER_WAVE", (String)((Map<String, String>)list.get(i)).get("ORDER_WAVE"));
            			modelIns.put("ORDER_TYPE", (String)((Map<String, String>)list.get(i)).get("ORDER_TYPE"));
            			modelIns.put("ORDER_NO", (String)((Map<String, String>)list.get(i)).get("ORDER_NO"));
            			modelIns.put("STORE_CD", (String)((Map<String, String>)list.get(i)).get("STORE_CD"));
            			modelIns.put("STORE_NM", (String)((Map<String, String>)list.get(i)).get("STORE_NM"));
            			modelIns.put("ITEM_CD", (String)((Map<String, String>)list.get(i)).get("ITEM_CD"));
            			modelIns.put("ITEM_NM", (String)((Map<String, String>)list.get(i)).get("ITEM_NM"));
            			modelIns.put("ITEM_QTY", (String)((Map<String, String>)list.get(i)).get("ITEM_QTY"));
            			modelIns.put("IF_FLAG", (String)((Map<String, String>)list.get(i)).get("IF_FLAG"));
            			//modelIns.put("RCV_ADDR", (String)((Map<String, String>)list.get(i)).get("RCV_ADDR"));
            			//modelIns.put("RCV_TEL", (String)((Map<String, String>)list.get(i)).get("RCV_TEL"));
            			modelIns.put("RCV_NM", (String)((Map<String, String>)list.get(i)).get("RCV_NM"));
            			modelIns.put("SEND_DATE", (String)((Map<String, String>)list.get(i)).get("SEND_DATE"));
            			modelIns.put("BRAND", (String)((Map<String, String>)list.get(i)).get("BRAND"));
            			modelIns.put("ORDER_SEQ", (String)((Map<String, String>)list.get(i)).get("ORDER_SEQ"));
            			modelIns.put("WARSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("WARSEQ")));
            			modelIns.put("DLVSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("DLVSEQ")));
            			modelIns.put("PTNSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("PTNSEQ")));
            			modelIns.put("PTNRNM", (String)((Map<String, String>)list.get(i)).get("PTNRNM"));
            			modelIns.put("WARENM", (String)((Map<String, String>)list.get(i)).get("WARENM"));
            			modelIns.put("DLVCNM", (String)((Map<String, String>)list.get(i)).get("DLVCNM"));
            			modelIns.put("MAX_PTNSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("MAX_PTNSEQ")));
            			modelIns.put("RQSHPD", (String)((Map<String, String>)list.get(i)).get("RQSHPD"));
            			modelIns.put("MAX_BOXSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("MAX_BOXSEQ")));
            			modelIns.put("BOXSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("BOXSEQ")));
            			modelIns.put("PARTNER_CD", (String)((Map<String, String>)list.get(i)).get("PARTNER_CD"));
            			modelIns.put("SUPPLY_ADDR", (String)((Map<String, String>)list.get(i)).get("SUPPLY_ADDR"));
            			modelIns.put("SUPPLY_TEL", String.valueOf(((Map<String, String>)list.get(i)).get("SUPPLY_TEL")));
            			//modelIns.put("RCV_NM", (String)((Map<String, String>)list.get(i)).get("D_CUST_NM"));
            			modelIns.put("RCV_TEL", (String)((Map<String, String>)list.get(i)).get("RCV_TEL"));
            			modelIns.put("RCV_HP", (String)((Map<String, String>)list.get(i)).get("D_TEL"));
            			modelIns.put("RCV_ADDR", (String)((Map<String, String>)list.get(i)).get("RCV_ADDR"));
            			modelIns.put("RCV_ADDR2", (String)((Map<String, String>)list.get(i)).get("RCV_ADDR2"));
            			modelIns.put("DLV_MSG", (String)((Map<String, String>)list.get(i)).get("DLV_MSG"));
            			modelIns.put("SND_NM", (String)((Map<String, String>)list.get(i)).get("ODNAME"));
            			modelIns.put("SND_TEL", String.valueOf(((Map<String, String>)list.get(i)).get("SND_TEL")));
            			//modelIns.put("SND_ADDR", (String)((Map<String, String>)list.get(i)).get("SND_ADDR"));
            			modelIns.put("SND_ADDR", (String)((Map<String, String>)list.get(i)).get("DLVADDR01"));
            			modelIns.put("TARN_GROUP", (String)((Map<String, String>)list.get(i)).get("TARN_GROUP"));
            			modelIns.put("TRAN_PRICE", String.valueOf(((Map<String, String>)list.get(i)).get("TRAN_PRICE")));
            			modelIns.put("TRAN_DS", (String)((Map<String, String>)list.get(i)).get("TRAN_DS"));
            			modelIns.put("DLV_BRAND_NM", (String)((Map<String, String>)list.get(i)).get("DLV_BRAND_NM"));
            			modelIns.put("DLV_EMP_NM", (String)((Map<String, String>)list.get(i)).get("DLV_EMP_NM"));
            			modelIns.put("DLV_SUBEMP_NM", (String)((Map<String, String>)list.get(i)).get("DLV_SUBEMP_NM"));
            			modelIns.put("STRG_NM", (String)((Map<String, String>)list.get(i)).get("STRG_NM"));
            			modelIns.put("ITEM_PRICE", String.valueOf(((Map<String, String>)list.get(i)).get("ITEM_PRICE")));
            			modelIns.put("PRIOCD", String.valueOf(((Map<String, String>)list.get(i)).get("PRIOCD")));
            			modelIns.put("ITEM_BOX", String.valueOf(((Map<String, String>)list.get(i)).get("ITEM_BOX")));
            			modelIns.put("ITEM_BARCODE", String.valueOf(((Map<String, String>)list.get(i)).get("ITEM_BARCODE")));
            			modelIns.put("INVO_NO", String.valueOf(((Map<String, String>)list.get(i)).get("INVO_NO")));
            			modelIns.put("CLSF_CD", String.valueOf(((Map<String, String>)list.get(i)).get("CLSF_CD")));
            			modelIns.put("SUBCLSF_CD", String.valueOf(((Map<String, String>)list.get(i)).get("SUBCLSF_CD")));
            			modelIns.put("ETC1", String.valueOf(((Map<String, String>)list.get(i)).get("ETC1")));
            			modelIns.put("BOX_BARCODE", String.valueOf(((Map<String, String>)list.get(i)).get("BOXBCD")));
            			modelIns.put("ITEM_ATM", String.valueOf(((Map<String, String>)list.get(i)).get("SALAMT")));
            			modelIns.put("ORDKEY", String.valueOf(((Map<String, String>)list.get(i)).get("ORG_ORD_ID")));

            			//Das i/f table insert 
            			dao.deviceIfSendInsertB2C(modelIns);
            		}
            	}
            	
                m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("save.success"));
            }
			
        }catch(Exception e){
        	m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> dasIfSendNoInvcB2C(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
        try{        	
        	String vrDEV_ORD_DEGREE = "";
        	String custId  = "";
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
        	
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    custId  = (String)model.get("CUST_ID"+i);
                    String DEV_ORD_DEGREE = (String)model.get("DEV_ORD_DEGREE"+i);
                    if(DEV_ORD_DEGREE == null || DEV_ORD_DEGREE.isEmpty() || DEV_ORD_DEGREE.equals("")){
                    	m.put("errCnt", -1);
                        m.put("MSG", "차수가 없는 주문이 있습니다." );
                        return m;
                    }
                    
                	if(i == 0){
                		vrDEV_ORD_DEGREE = "'" + DEV_ORD_DEGREE + "'";
                	}else {
                        if (!(vrDEV_ORD_DEGREE.contains("'" + DEV_ORD_DEGREE + "'"))) {
                        	vrDEV_ORD_DEGREE += ",'" + DEV_ORD_DEGREE + "'";
                        } 
                	}
                }
                //프로시져에 보낼것들 다담는다
            	model.put("custId", custId);
            	model.put("DEV_ORD_DEGREE", vrDEV_ORD_DEGREE);
            	
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                String custGubn = String.valueOf(model.get("custGubn"));
                
                
                //session 및 등록정보
            	model.put("LC_ID", LC_ID);
            	model.put("USER_NO", USER_NO);
            	model.put("WORK_IP", WORK_IP);
            	
            	//Das list select
            	List list = null;
            	
            	if(custGubn.equals("COOP")){
            		list  = dao.dasKotechIfSendListNoInvcB2C(model);
            	}else{
            		list  = dao.dasKotechIfSendListNoInvcNoCoopB2C(model);
            	}
            	

            	int listCnt = list.size();
            	
            	if(listCnt > 0){

        			for(int i = 0 ; i < listCnt ; i ++){
        				
            			Map<String, Object> modelIns = new HashMap<String, Object>();
            			modelIns.put("LC_ID", (String)((Map<String, String>)list.get(i)).get("LC_ID"));
            			modelIns.put("CUST_ID", (String)((Map<String, String>)list.get(i)).get("CUST_ID"));
            			modelIns.put("ORDER_DATE", (String)((Map<String, String>)list.get(i)).get("ORDER_DATE"));
            			modelIns.put("ORDER_WAVE", (String)((Map<String, String>)list.get(i)).get("ORDER_WAVE"));
            			modelIns.put("ORDER_TYPE", (String)((Map<String, String>)list.get(i)).get("ORDER_TYPE"));
            			modelIns.put("ORDER_NO", (String)((Map<String, String>)list.get(i)).get("ORDER_NO"));
            			modelIns.put("STORE_CD", (String)((Map<String, String>)list.get(i)).get("STORE_CD"));
            			modelIns.put("STORE_NM", (String)((Map<String, String>)list.get(i)).get("STORE_NM"));
            			modelIns.put("ITEM_CD", (String)((Map<String, String>)list.get(i)).get("ITEM_CD"));
            			modelIns.put("ITEM_NM", (String)((Map<String, String>)list.get(i)).get("ITEM_NM"));
            			modelIns.put("ITEM_QTY", (String)((Map<String, String>)list.get(i)).get("ITEM_QTY"));
            			modelIns.put("IF_FLAG", (String)((Map<String, String>)list.get(i)).get("IF_FLAG"));
            			//modelIns.put("RCV_ADDR", (String)((Map<String, String>)list.get(i)).get("RCV_ADDR"));
            			//modelIns.put("RCV_TEL", (String)((Map<String, String>)list.get(i)).get("RCV_TEL"));
            			//modelIns.put("RCV_NM", (String)((Map<String, String>)list.get(i)).get("RCV_NM"));
            			modelIns.put("SEND_DATE", (String)((Map<String, String>)list.get(i)).get("SEND_DATE"));
            			modelIns.put("BRAND", (String)((Map<String, String>)list.get(i)).get("BRAND"));
            			modelIns.put("ORDER_SEQ", (String)((Map<String, String>)list.get(i)).get("ORDER_SEQ"));
            			modelIns.put("WARSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("WARSEQ")));
            			modelIns.put("DLVSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("DLVSEQ")));
            			modelIns.put("PTNSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("PTNSEQ")));
            			modelIns.put("PTNRNM", (String)((Map<String, String>)list.get(i)).get("PTNRNM"));
            			modelIns.put("WARENM", (String)((Map<String, String>)list.get(i)).get("WARENM"));
            			modelIns.put("DLVCNM", (String)((Map<String, String>)list.get(i)).get("DLVCNM"));
            			modelIns.put("MAX_PTNSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("MAX_PTNSEQ")));
            			modelIns.put("RQSHPD", (String)((Map<String, String>)list.get(i)).get("RQSHPD"));
            			modelIns.put("MAX_BOXSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("MAX_BOXSEQ")));
            			modelIns.put("BOXSEQ", String.valueOf(((Map<String, String>)list.get(i)).get("BOXSEQ")));
            			modelIns.put("PARTNER_CD", (String)((Map<String, String>)list.get(i)).get("PARTNER_CD"));
            			modelIns.put("SUPPLY_ADDR", (String)((Map<String, String>)list.get(i)).get("SUPPLY_ADDR"));
            			modelIns.put("SUPPLY_TEL", String.valueOf(((Map<String, String>)list.get(i)).get("SUPPLY_TEL")));
            			modelIns.put("RCV_NM", (String)((Map<String, String>)list.get(i)).get("D_CUST_NM"));
            			modelIns.put("RCV_TEL", (String)((Map<String, String>)list.get(i)).get("D_TEL"));
            			modelIns.put("RCV_HP", (String)((Map<String, String>)list.get(i)).get("D_TEL"));
            			modelIns.put("RCV_ADDR", (String)((Map<String, String>)list.get(i)).get("RCV_ADDR"));
            			modelIns.put("RCV_ADDR2", (String)((Map<String, String>)list.get(i)).get("RCV_ADDR2"));
            			modelIns.put("DLV_MSG", (String)((Map<String, String>)list.get(i)).get("DLV_MSG1"));
            			modelIns.put("SND_NM", (String)((Map<String, String>)list.get(i)).get("SND_NM"));
            			//modelIns.put("SND_TEL", String.valueOf(((Map<String, String>)list.get(i)).get("SND_TEL")));
            			modelIns.put("SND_TEL", String.valueOf(((Map<String, String>)list.get(i)).get("SUPPLY_TEL")));
            			modelIns.put("SND_ADDR", (String)((Map<String, String>)list.get(i)).get("SND_ADDR"));
            			modelIns.put("TARN_GROUP", (String)((Map<String, String>)list.get(i)).get("TARN_GROUP"));
            			modelIns.put("TRAN_PRICE", String.valueOf(((Map<String, String>)list.get(i)).get("TRAN_PRICE")));
            			modelIns.put("TRAN_DS", (String)((Map<String, String>)list.get(i)).get("TRAN_DS"));
            			modelIns.put("DLV_BRAND_NM", (String)((Map<String, String>)list.get(i)).get("DLV_BRAND_NM"));
            			modelIns.put("DLV_EMP_NM", (String)((Map<String, String>)list.get(i)).get("DLV_EMP_NM"));
            			modelIns.put("DLV_SUBEMP_NM", (String)((Map<String, String>)list.get(i)).get("DLV_SUBEMP_NM"));
            			modelIns.put("STRG_NM", (String)((Map<String, String>)list.get(i)).get("STRG_NM"));
            			modelIns.put("ITEM_PRICE", String.valueOf(((Map<String, String>)list.get(i)).get("ITEM_PRICE")));
            			modelIns.put("PRIOCD", String.valueOf(((Map<String, String>)list.get(i)).get("PRIOCD")));
            			modelIns.put("ITEM_BOX", String.valueOf(((Map<String, String>)list.get(i)).get("ITEM_BOX")));
            			modelIns.put("ITEM_BARCODE", String.valueOf(((Map<String, String>)list.get(i)).get("ITEM_BARCODE")));
            			modelIns.put("DLV_BRAND_NM", String.valueOf(((Map<String, String>)list.get(i)).get("DLV_BRAND_NM")));
            			modelIns.put("DLV_EMP_NM", String.valueOf(((Map<String, String>)list.get(i)).get("DLV_EMP_NM")));
            			modelIns.put("DLV_SUBEMP_NM", String.valueOf(((Map<String, String>)list.get(i)).get("DLV_SUBEMP_NM")));
            			modelIns.put("BOX_BARCODE", String.valueOf(((Map<String, String>)list.get(i)).get("BOXBCD")));
            			//modelIns.put("ITEM_ATM", String.valueOf(((Map<String, String>)list.get(i)).get("SALAMT")));
            			modelIns.put("ORDKEY", String.valueOf(((Map<String, String>)list.get(i)).get("ORG_ORD_ID")));
            			
            			//Das i/f table insert 
            			dao.deviceIfSendInsertB2C(modelIns);
            		}
            	}
            	
                m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("save.success"));                
            }            
            
        }catch(Exception e){
        	m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    public Map<String, Object> getDevOrdDegree(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_DEV_ORD_DEGREE", dao.getDevOrdDegree(model));
		return map;
	}
    
    @Override
    public Map<String, Object> makeDegreeB2C(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	String old_ordId = "";
            	String ordId = "";
            	String custId = (String)model.get("CUST_ID"+0); 
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                
                String vrOrderByType = (String)model.get("mo_vrOrderByType");
                String vrOrdCnt = (String)model.get("mo_vrOrdCnt");
                
                ArrayList<String>  arrOrdid = new ArrayList();
                for(int i = 0 ; i < tmpCnt ; i ++){
                	String ordId_work   = (String)model.get("ORD_ID"+i);   
                	if(i == 0)arrOrdid.add(ordId_work);
                	if(!arrOrdid.contains(ordId_work))arrOrdid.add(ordId_work);
                }
                //session 및 등록정보
                modelIns.put("LC_ID", LC_ID);
                modelIns.put("WORK_IP", WORK_IP);
                modelIns.put("USER_NO", USER_NO);
                
                //프로시져에 보낼것들 다담는다
                modelIns.put("ordId", arrOrdid);
                modelIns.put("custId", custId);
                modelIns.put("vrOrderByType", vrOrderByType);
                
                List<Map<String,Object>> separateList = new ArrayList();
                separateList = dao.separateOrdIdB2C(modelIns);
                int separateListCnt = separateList.size();
                
                int cnt = Integer.parseInt(vrOrdCnt);
                List arrayList_ordid = new ArrayList();
                for(int i = 0 ; i < separateListCnt ; i ++){
                	Map<String,Object> workMap = (Map<String,Object>)separateList.get(i);
                	String workOrdIdList = (String) workMap.get("ORD_ID_LIST");
                	if(i % cnt == 0 || i == 0){
                		arrayList_ordid = new ArrayList();
                		arrayList_ordid.add(workOrdIdList);
                	}else{
                		arrayList_ordid.add(workOrdIdList);
                	}
                	// 120번째 또는 마지막 반복문에서 das를 생성한다. 
                	if (cnt == 1 || (i != 0 && (i+1)%cnt == 0) || i == separateListCnt - 1) {
                        // 조건을 만족할 때 실행되는 코드
                    	System.out.println(arrayList_ordid);
                    	modelIns.put("ordId", arrayList_ordid);
                    	dao.degreeMakeB2C(modelIns);
                    	workOrdIdList = "";
                    }
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> deleteDegree(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	String vrDEV_ORD_DEGREE = "";
                for(int i = 0 ; i < tmpCnt ; i ++){
                    String custId  = (String)model.get("CUST_ID"+i);
                    String DEV_ORD_DEGREE = (String)model.get("DEV_ORD_DEGREE"+i);
                    if(DEV_ORD_DEGREE.isEmpty() || DEV_ORD_DEGREE.equals("")){
                    	m.put("errCnt", -1);
                        m.put("MSG", "차수가 없는 주문이 있습니다." );
                        return m;
                    }
                    
                    String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                    String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                    String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                	if(i == 0){
                		vrDEV_ORD_DEGREE = "'" + DEV_ORD_DEGREE + "'";
                	}else {
                        if (!(vrDEV_ORD_DEGREE.contains("'" + DEV_ORD_DEGREE + "'"))) {
                        	vrDEV_ORD_DEGREE += ",'" + DEV_ORD_DEGREE + "'";
                        } 
                	}
                	
                	if(i == tmpCnt-1){
	                    //프로시져에 보낼것들 다담는다
	                    modelIns.put("custId", custId);
	                    modelIns.put("DEV_ORD_DEGREE", vrDEV_ORD_DEGREE);
	                    
	                    //session 및 등록정보
	                    modelIns.put("LC_ID", LC_ID);
	                    modelIns.put("WORK_IP", WORK_IP);
	                    modelIns.put("USER_NO", USER_NO);
	                    
                    	dao.dasDelete(modelIns);
                	}
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }



	@Override
	public Map<String, Object> listE1ExcelByTurkiye(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "1000");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        model.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
        
        GenericResultSet res = dao.listE1ExcelByTurkiye(model);
        List list = res.getList();
        
        res.setList(list);
        map.put("LIST", res);
        return map;
	}
}