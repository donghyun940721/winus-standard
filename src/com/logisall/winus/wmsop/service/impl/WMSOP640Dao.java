package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP640Dao")
public class WMSOP640Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID	: listByDlvSummary
     * Method 설명	: 택배주문조회
     * 작성자			: chsong
     * @param model
     * @return
     */
    public List rawListByDlvSummary(Map<String, Object> model) {
		List custs = list("wmsop640.listByDlvSummary", model);
    	return custs;
	}
	public Object listByDlvSummary(Map<String, Object> model) {
		List custs = rawListByDlvSummary(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID	: listByDlvSummaryDiv
     * Method 설명	: 택배주문조회
     * 작성자			: chsong
     * @param model
     * @return
     */
    public List rawListByDlvSummaryDiv(Map<String, Object> model) {
		List custs = list("wmsop641.listByDlvSummary", model);
    	return custs;
	}
	public Object listByDlvSummaryDiv(Map<String, Object> model) {
		List custs = rawListByDlvSummaryDiv(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID	: list2ByDlvHistory
     * Method 설명	: 택배이력엑셀등록조회
     * 작성자			: chsong
     * @param model
     * @return
     */
	public List rawList2ByDlvHistory(Map<String, Object> model) {
		List custs = list("wmsop640.list2ByDlvHistory", model);
    	return custs;
	}
    
	public Object list2ByDlvHistory(Map<String, Object> model) {
		List custs = rawList2ByDlvHistory(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
	 * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void insertCsv(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				String LC_ID = (String) model.get("strLcId");
				String CUST_ID = (String) model.get("strCustId");
				
				if (paramMap.get("ORG_ORD_NO") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("ORG_ORD_NO"))
				 && paramMap.get("ORD_DETAIL_NO") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("ORD_DETAIL_NO"))
    			 && paramMap.get("DLV_COMP_CD") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("DLV_COMP_CD"))
    			 && paramMap.get("INVC_NO") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("INVC_NO"))
    			 && StringUtils.isNotEmpty(LC_ID)
    			 && StringUtils.isNotEmpty(CUST_ID)
    			){
					//String custId = (String) sqlMapClient.insert("wmsms010.insertCsv", paramMap);
					paramMap.put("LC_ID"	, LC_ID);
					paramMap.put("CUST_ID"	, CUST_ID);
					
					/* date : 2022-02-03
					 * author : sing09 
					 * WMSOM010의 원주문번호로 조회하여 치환받아온 WMSOP011 ORD_ID, ORD_SEQ 값
					 */
					Map<String, Object> paramMap1 = null;
					paramMap1 = (Map) sqlMapClient.queryForObject("wmsop640.selectCsv", paramMap);
					paramMap.put("ORD_ID"	, paramMap1.get("ORD_ID"));
					paramMap.put("ORD_SEQ"	, paramMap1.get("ORD_SEQ"));
					sqlMapClient.insert("wmsop640.insertCsv", paramMap);
    			}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/**
     * Method ID  : dlvSenderInfo
     * Method 설명  : 택배배송 송화인정보
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object dlvSenderInfo(Map<String, Object> model){
        return executeQueryForList("wmsop640.dlvSenderInfo", model);
    }
    
    /**
	 * Method ID	: boxNoUpdate 
	 * Method 설명	: boxNoUpdate코드 수정 
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object boxNoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.boxNoUpdate", model);
	}
	
	/*-
     * Method ID : getOracleSysTimeStamp
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String getOracleSysTimeStamp(Map<String, Object> model) {
        return (String)executeView("wmsop640.getOracleSysTimeStamp", model);
    }
    
    /**
	 * Method ID : dlvPrintPoiNoUpdate 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
    public Object dlvPrintPoiNoUpdate(Map<String, Object> model){
        executeUpdate("wmsop640.pk_wmsdf010.sp_print_invc_update", model);
        return model;
    }
	
	public Object invcNoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.invcNoUpdate", model);
	}
	
	/**
	 * Method ID	: wmsdf110DelYnUpdate 
	 * Method 설명	: wmsdf110DelYnUpdate코드 수정 
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object wmsdf110DelYnUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.wmsdf110DelYnUpdate", model);
	}
	
	/**
     * Method ID	: getCustOrdDegree
     * Method 설명 	: 화주별 주문차수
     * @param model
     * @return
     */
    public Object getCustOrdDegree(Map<String, Object> model){
        return executeQueryForList("wmsop640.getCustOrdDegree", model);
    }
    

    public GenericResultSet listByDlvSummaryExcel(Map<String, Object> model) {
//		return executeQueryPageWq("wmsop640.listByDlvSummary", model);
		
		return executeQueryPageWq("wmsop640.listByDlvSummaryExcel", model);
		/*
		GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop030.pickingList", model));
    	return genericResultSet;
    	*/
	}
    
    /**
     * Method ID	: getCustInfo
     * Method 설명 	: 고객정보별 원주문번호 조회
     * @param model
     * @return
     */
    public Object getCustInfo(Map<String, Object> model){
        return executeQueryForList("wmsop640.getCustInfo", model);
    }
    
    /**
	 * Method ID	: custInfoUpdate 
	 * Method 설명	: custInfoUpdate
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object custInfoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.custInfoUpdate", model);
	}
	
	public GenericResultSet listByDlvIFExcel(Map<String, Object> model) {
		//return executeQueryPageWq("wmsop640.listByDlvIFExcel", model);
		return executeQueryPageWq("wmsop640.listByDlvIFExcelCJ", model);
	}

    public GenericResultSet listByDlvIFExcelV2(Map<String, Object> model) {
        return executeQueryPageWq("wmsop640.listByDlvIFExcelV2", model);
    }
    
	/**
     * Method ID	: deviceIfSendList
     * Method 설명	: DAS자료생성
     * 작성자			: chsong
     * @param model
     * @return
     */
    public List deviceIfSendList(Map<String, Object> model) {
        //List rstList = list("wmsop640.deviceIfSendList", model);
    	List rstList = list("wmsop640.deviceIfSendListCJ", model);
    	return rstList;
    }
    
    /**
     * Method ID	: deviceIfSendInsert
     * Method 설명	: DAS자료입력
     * 작성자			: chsong
     * @param model
     * @return
     */
    public Object deviceIfSendInsert(Map<String, Object> model) {
  		return executeInsert("wmsop640.deviceIfSendInsert", model);
  	}
    
    /**
     * Method ID	: deviceIfListSearch
     * Method 설명	: DAS자료조회
     * 작성자			:
     * @param model
     * @return
     */
    public GenericResultSet deviceIfListSearch(Map<String, Object> model) {
        return executeQueryPageWq("wmsop640.deviceIfListSearch", model);
    }
    public Object dashBoard(Map<String, Object> model){
        return executeQueryForList("wmsop640.dashBoard", model);
    }
    
    /**
     * Method ID	: getDasOrdDateByDegree
     * Method 설명 	: DAS생성차수
     * @param model
     * @return
     */
    public Object getDasOrdDateByDegree(Map<String, Object> model){
        return executeQueryForList("wmsop640.getDasOrdDateByDegree", model);
    }

    /**
     * Method ID    : deviceIfSendListV2
     * Method 설명    : DAS자료생성
     * 작성자          : sing09
     * @param model
     * @return
     */
    public List deviceIfSendListV2(Map<String, Object> model) {
        List rstList = list("wmsop640.deviceIfSendListV2", model);
        return rstList;
    }
    
    /**
     * Method ID    : deviceIfSendInsertV2
     * Method 설명    : DAS자료입력
     * 작성자          : sing09
     * @param model
     * @return
     */
    public Object deviceIfSendInsertV2(Map<String, Object> model) {
        return executeInsert("wmsop640.deviceIfSendInsertV2", model);
    }
    
}
