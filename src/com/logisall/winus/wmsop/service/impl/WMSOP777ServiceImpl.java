package com.logisall.winus.wmsop.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmsop.service.WMSOP777Service;

@Service("WMSOP777Service")
public class WMSOP777ServiceImpl implements WMSOP777Service {

    @Resource(name = "WMSOP777Dao")
    private WMSOP777Dao dao;
    
    /**
     * Method ID	: listE1
     * Method 설명	: 주문할당수량 조회
     * 작성자			: atomyoun
     * @param   model
     * @return
     * @throws  Exception
     */
    
	@Override
	public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
    	if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> vrSrchMulOrdIdArr = new ArrayList();
        String[] spVrSrchMulOrdId = model.get("vrSrchMulOrdId").toString().split(",");
        for (String keyword : spVrSrchMulOrdId ){
        	vrSrchMulOrdIdArr.add(keyword.trim());
        }
        model.put("vrSrchMulOrdIdArr", vrSrchMulOrdIdArr);
        map.put("LIST", dao.listE1(model));
        return map;
	}

    /**
     * Method ID	: listE2
     * Method 설명	: 주문할당수량(상품) 조회
     * 작성자			: atomyoun
     * @param   model
     * @return
     * @throws  Exception
     */
	@Override
	public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> vrSrchMulOrdIdArr2 = new ArrayList();
        String[] spVrSrchMulOrdId2 = model.get("vrSrchMulOrdId2").toString().split(",");
        for (String keyword : spVrSrchMulOrdId2 ){
        	vrSrchMulOrdIdArr2.add(keyword.trim());
        }
        model.put("vrSrchMulOrdIdArr2", vrSrchMulOrdIdArr2);
        map.put("LIST", dao.listE2(model));
        return map;
	}

	@Override
	public Map<String, Object> listE2OrdCount(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> vrSrchMulOrdIdArr2 = new ArrayList();
        String[] spVrSrchMulOrdId2 = model.get("vrSrchMulOrdId2").toString().split(",");
        for (String keyword : spVrSrchMulOrdId2 ){
        	vrSrchMulOrdIdArr2.add(keyword.trim());
        }
        model.put("vrSrchMulOrdIdArr2", vrSrchMulOrdIdArr2);
		map.put("LIST", dao.listE2OrdCount(model));
		return map;
	}

}
