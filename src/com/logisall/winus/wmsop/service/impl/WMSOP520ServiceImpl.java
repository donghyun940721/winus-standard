package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.impl.WMSMS011Dao;
import com.logisall.winus.wmsop.service.WMSOP520Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP520Service")
public class WMSOP520ServiceImpl implements WMSOP520Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSOP520Dao")
    private WMSOP520Dao dao;
    
    @Resource(name = "WMSMS011Dao")
    private WMSMS011Dao daoMS011;

    @Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao1;
    
    /**
     * Method ID   : selectItemGrp
     * Method 설명    : 출고관리 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        if(model.get("SS_SVC_NO").equals("0000001840")){
	        map.put("ITEMGRP98", dao.selectItemGrp98(model));
	        map.put("ITEMGRP99", dao.selectItemGrp99(model));
        }
        return map;
    }
	@Override
	public Map<String, Object> listE01(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.listE01(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	@Override
	public Map<String, Object> listE02(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//특수문자 처리
			model.put("vrSrchItemGrpId98_T2",org.springframework.web.util.HtmlUtils.htmlUnescape((String) model.get("vrSrchItemGrpId98_T2")));
			
			map.put("LIST", dao.listE02(model));
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}
	
	  /**
     * Method ID : listSub_T1
     * Method 설명 : 발주관리 구성품 조회
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub_T1(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "20");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}        
    		map.put("LIST", dao.listSub_T1(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
	/**
     * Method ID : listQ1
     * Method 설명 : 세트상품조회 팝업
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.listQ1(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
    	return map;
    }   
    
	@Override
	public Map<String, Object> list01Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE01(model));
        return map;
	}
	
	/**
	 *  Method ID 	: saveInsQtyStep
	 *  Method 설명  	: GS입고 (세트품기준발주 수량 자동계산)
	 *  작성자            	 : sing09
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveInsQtyStep(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try{
			int listBodyCnt = Integer.parseInt(model.get("selectIds").toString());
			JSONArray I_DATA = new JSONArray();
			if(listBodyCnt > 0){
				for (int i = 0; i < listBodyCnt; i++) {
					Map<String, Object> modelIns = new HashMap<String, Object>();
					modelIns.put("ITEM_CODE"		, model.get("ITEM_CODE_" + i));
					modelIns.put("INS_QTY"   		, model.get("INS_QTY_" + i));
					modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
					//세트품 5개 입력 시 해당 구성품 *5 해서 계산
					m.put("LIST", dao.saveInsQtyStep(modelIns));
					
					//saveInsQtyStep1 에서 계산해온 발주수량 JSONObject에 담기
					JSONObject jsonObject = new JSONObject(m);
					JSONObject jsonObject_step1 = jsonObject.getJSONObject("LIST");
					JSONArray  jsonObject_step2 = jsonObject_step1.getJSONArray("list");
					
					//jsonObject에 담은 모든 값 JSONArray : I_DATA 에 가공해서 모두 담기
					for(int j=0; j< jsonObject_step2.length(); j++){
						JSONObject jsonObject_step3 = jsonObject_step2.getJSONObject(j);
						JSONObject data = new JSONObject();
						data.put("L_RITEM",(String) jsonObject_step3.get("PART_RITEM_ID"));
						data.put("L_TRUST_CUST_NM",(String) jsonObject_step3.get("TRUST_CUST_NM"));
						data.put("L_QTY",String.valueOf(jsonObject_step3.get("QTY")));
						data.put("L_REQ_DT",(String) model.get("vrSrchReqDt"));
						data.put("L_REMARK",(String) model.get("REMARK_" + i));
						I_DATA.put(data);
					}
				}
				int listBodyCnt3 = I_DATA.length();
				if(listBodyCnt3 > 0){
					String[] no             = new String[listBodyCnt3];     
		            
		            String[] outReqDt       = new String[listBodyCnt3];         
		            String[] inReqDt        = new String[listBodyCnt3];     
		            String[] custOrdNo      = new String[listBodyCnt3];     
		            String[] custOrdSeq     = new String[listBodyCnt3];    
		            String[] trustCustCd    = new String[listBodyCnt3];     
		            
		            String[] transCustCd    = new String[listBodyCnt3];                     
		            String[] transCustTel   = new String[listBodyCnt3];         
		            String[] transReqDt     = new String[listBodyCnt3];     
		            String[] custCd         = new String[listBodyCnt3];     
		            String[] ordQty         = new String[listBodyCnt3];     
		            
		            String[] uomCd          = new String[listBodyCnt3];                
		            String[] sdeptCd        = new String[listBodyCnt3];         
		            String[] salePerCd      = new String[listBodyCnt3];     
		            String[] carCd          = new String[listBodyCnt3];     
		            String[] drvNm          = new String[listBodyCnt3];     
		            
		            String[] dlvSeq         = new String[listBodyCnt3];                
		            String[] drvTel         = new String[listBodyCnt3];         
		            String[] custLotNo      = new String[listBodyCnt3];     
		            String[] blNo           = new String[listBodyCnt3];     
		            String[] recDt          = new String[listBodyCnt3];     
		            
		            String[] outWhCd        = new String[listBodyCnt3];                
		            String[] inWhCd         = new String[listBodyCnt3];         
		            String[] makeDt         = new String[listBodyCnt3];     
		            String[] timePeriodDay  = new String[listBodyCnt3];     
		            String[] workYn         = new String[listBodyCnt3];     
		            
		            String[] rjType         = new String[listBodyCnt3];                
		            String[] locYn          = new String[listBodyCnt3];         
		            String[] confYn         = new String[listBodyCnt3];     
		            String[] eaCapa         = new String[listBodyCnt3];     
		            String[] inOrdWeight    = new String[listBodyCnt3];     
		            
		            String[] itemCd         = new String[listBodyCnt3];                
		            String[] itemNm         = new String[listBodyCnt3];         
		            String[] transCustNm    = new String[listBodyCnt3];     
		            String[] transCustAddr  = new String[listBodyCnt3];     
		            String[] transEmpNm     = new String[listBodyCnt3];     
		            
		            String[] remark         = new String[listBodyCnt3];                
		            String[] transZipNo     = new String[listBodyCnt3];         
		            String[] etc2           = new String[listBodyCnt3];     
		            String[] unitAmt        = new String[listBodyCnt3];     
		            String[] transBizNo     = new String[listBodyCnt3];     
		            
		            String[] inCustAddr     = new String[listBodyCnt3];                
		            String[] inCustCd       = new String[listBodyCnt3];         
		            String[] inCustNm       = new String[listBodyCnt3];     
		            String[] inCustTel      = new String[listBodyCnt3];     
		            String[] inCustEmpNm    = new String[listBodyCnt3];     
		            
		            String[] expiryDate     = new String[listBodyCnt3];
		            String[] salesCustNm    = new String[listBodyCnt3];
		            String[] zip     		= new String[listBodyCnt3];
		            String[] addr     		= new String[listBodyCnt3];
		            String[] phone1    	 	= new String[listBodyCnt3];
		            
		            String[] etc1    		= new String[listBodyCnt3];
		            String[] unitNo    		= new String[listBodyCnt3];               
		            String[] salesCompanyNm	 	= new String[listBodyCnt3];
		            String[] timeDate       = new String[listBodyCnt3];   //상품유효기간     
		            String[] timeDateEnd    = new String[listBodyCnt3];   //상품유효기간만료일
		            
		            String[] timeUseEnd     = new String[listBodyCnt3];   //소비가한만료일
		            String[] locCd     		= new String[listBodyCnt3];   //로케이션코드
		            String[] epType     	= new String[listBodyCnt3];   //납품유형
		            
		            String[] lotType     	= new String[listBodyCnt3];   //
		            String[] ownerCd     	= new String[listBodyCnt3];   //
		            
		            // I_ 로 시작하는 변수는 안담김
		            for (int i = 0; i < listBodyCnt3; i++) {
		            	String OUT_REQ_DT      = "";
//		    			String IN_REQ_DT       = "";
		    			String CUST_ORD_NO     = "";
		    			String CUST_ORD_SEQ    = "";
		    			String TRUST_CUST_CD   = "";
		    			String TRANS_CUST_CD   = "";
		    			String TRANS_CUST_TEL  = "";
		    			String TRANS_REQ_DT    = "";
//		    			String CUST_CD         = "";
//		    			String ORD_QTY         = "";
		    			String UOM_CD          = "";
		    			String SDEPT_CD        = "";
		    			String SALE_PER_CD     = "";
		    			String CAR_CD          = "";
		    			String DRV_NM          = "";
		    			String DLV_SEQ         = "";
		    			String DRV_TEL         = "";
		    			String CUST_LOT_NO     = "";
		    			String BL_NO           = "";
		    			String REC_DT          = "";
		    			String OUT_WH_CD       = "";
		    			String IN_WH_CD        = "";
		    			String MAKE_DT         = "";
		    			String TIME_PERIOD_DAY = "";
		    			String WORK_YN         = "";
		    			String RJ_TYPE         = "";
		    			String LOC_YN          = "";
		    			String CONF_YN         = "";
		    			String EA_CAPA         = "";
		    			String IN_ORD_WEIGHT   = "";
//		    			String ITEM_CD         = "";
		    			String ITEM_NM         = "";
		    			String TRANS_CUST_NM   = "";
		    			String TRANS_CUST_ADDR = "";
		    			String TRANS_EMP_NM    = "";
		    			String REMARK          = "";
		    			String TRANS_ZIP_NO    = "";
		    			String ETC2            = "";
		    			String UNIT_AMT        = "";
		    			String TRANS_BIZ_NO    = "";
		    			String IN_CUST_ADDR    = "";
		    			String IN_CUST_CD      = "";
		    			String IN_CUST_NM      = "";
		    			String IN_CUST_TEL     = "";
		    			String IN_CUST_EMP_NM  = "";
		    			String EXPIRY_DATE     = "";
		    			String SALES_CUST_NM   = "";
		    			String ZIP             = "";
		    			String ADDR            = "";
		    			String PHONE_1         = "";
		    			String ETC1            = "";
		    			String UNIT_NO         = "";
		    			String SALES_COMPANY_NM   = "";
		    			String TIME_DATE       = "";
		    			String TIME_DATE_END   = "";
		    			String TIME_USE_END    = "";
		    			String LOC_CD    	   = "";
		    			String EP_TYPE    	   = "";
		    			String LOT_TYPE    	   = "";
		    			String OWNER_CD    	   = "";
		    			
		            	String NO = Integer.toString(i+1);
		            	no[i]               = NO;
		                
		                outReqDt[i]         = OUT_REQ_DT;    
		                inReqDt[i]          = I_DATA.getJSONObject(i).getString("L_REQ_DT");
		                custOrdNo[i]        = CUST_ORD_NO;    
		                custOrdSeq[i]       = CUST_ORD_SEQ;    
		                trustCustCd[i]      = TRUST_CUST_CD;    
		                
		                transCustCd[i]      = TRANS_CUST_CD;    
		                transCustTel[i]     = TRANS_CUST_TEL;    
		                transReqDt[i]       = TRANS_REQ_DT;    
		                custCd[i]           = (String)model.get("vrCustCd");    
		                ordQty[i]           = I_DATA.getJSONObject(i).getString("L_QTY");
		                
		                uomCd[i]            = UOM_CD;    
		                sdeptCd[i]          = SDEPT_CD;    
		                salePerCd[i]        = SALE_PER_CD;    
		                carCd[i]            = CAR_CD;
		                drvNm[i]            = DRV_NM;
		                
		                dlvSeq[i]           = DLV_SEQ;    
		                drvTel[i]           = DRV_TEL;    
		                custLotNo[i]        = CUST_LOT_NO;    
		                blNo[i]             = BL_NO;    
		                recDt[i]            = REC_DT;    
		                
		                outWhCd[i]          = OUT_WH_CD;
		                inWhCd[i]           = IN_WH_CD;
		                makeDt[i]           = MAKE_DT;
		                timePeriodDay[i]    = TIME_PERIOD_DAY;
		                workYn[i]           = WORK_YN;
		                
		                rjType[i]           = RJ_TYPE;    
		                locYn[i]            = LOC_YN;    
		                confYn[i]           = CONF_YN;    
		                eaCapa[i]           = EA_CAPA;    
		                inOrdWeight[i]      = IN_ORD_WEIGHT;   
		                
		                itemCd[i]           = I_DATA.getJSONObject(i).getString("L_RITEM");
		                itemNm[i]           = ITEM_NM;    
		                transCustNm[i]      = TRANS_CUST_NM;    
		                transCustAddr[i]    = TRANS_CUST_ADDR;    
		                transEmpNm[i]       = TRANS_EMP_NM;    
		
		                remark[i]           = I_DATA.getJSONObject(i).getString("L_REMARK");
		                transZipNo[i]       = TRANS_ZIP_NO;    
		                etc2[i]             = ETC2;    
		                unitAmt[i]          = UNIT_AMT;    
		                transBizNo[i]       = TRANS_BIZ_NO;    
		                
		                inCustAddr[i]       = IN_CUST_ADDR;   
		                inCustCd[i]         = IN_CUST_CD; //I_DATA.getJSONObject(i).getString("L_TRUST_CUST_ID");
		                inCustNm[i]         = I_DATA.getJSONObject(i).getString("L_TRUST_CUST_NM");//IN_CUST_NM;    
		                inCustTel[i]        = IN_CUST_TEL;    
		                inCustEmpNm[i]      = IN_CUST_EMP_NM;    
		                
		                expiryDate[i]       = EXPIRY_DATE;
		                salesCustNm[i]      = SALES_CUST_NM;
		                zip[i]       		= ZIP;
		                addr[i]       		= ADDR;
		                phone1[i]       	= PHONE_1;
		                
		                etc1[i]      		= ETC1;
		                unitNo[i]      		= UNIT_NO;
		                salesCompanyNm[i]   = SALES_COMPANY_NM;
		                timeDate[i]         = TIME_DATE;      
		                timeDateEnd[i]      = TIME_DATE_END;     
		                
		                timeUseEnd[i]       = TIME_USE_END;
		                locCd[i]       		= LOC_CD;
		
		                epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
		                lotType[i]       	= LOT_TYPE;// 신규컬럼 추가
		                ownerCd[i]       	= OWNER_CD;// 신규컬럼 추가
		            }
		            
		         // 프로시져에 보낼것들 다담는다
		            Map<String, Object> modelIns2 = new HashMap<String, Object>();
		
		            modelIns2.put("no"  , no);
		            if(model.get("vrOrdType").equals("I")){
		            	modelIns2.put("reqDt"     	, inReqDt);
		            	modelIns2.put("whCd"      	, inWhCd);
		            }else{
		            	modelIns2.put("reqDt"     	, outReqDt);
		            	modelIns2.put("whCd"      	, outWhCd);
		            }
		            modelIns2.put("custOrdNo"    	, custOrdNo);
		            modelIns2.put("custOrdSeq"   	, custOrdSeq);
		            modelIns2.put("trustCustCd"  	, trustCustCd); //5
		            
		            modelIns2.put("transCustCd"  	, transCustCd);
		            modelIns2.put("transCustTel" 	, transCustTel);
		            modelIns2.put("transReqDt"   	, transReqDt);
		            modelIns2.put("custCd"       	, custCd);
		            modelIns2.put("ordQty"       	, ordQty);      //10
		            
		            modelIns2.put("uomCd"        	, uomCd);
		            modelIns2.put("sdeptCd"      	, sdeptCd);
		            modelIns2.put("salePerCd"    	, salePerCd);
		            modelIns2.put("carCd"        	, carCd);
		            modelIns2.put("drvNm"        	, drvNm);       //15
		            
		            modelIns2.put("dlvSeq"       	, dlvSeq);
		            modelIns2.put("drvTel"       	, drvTel);
		            modelIns2.put("custLotNo"    	, custLotNo);
		            modelIns2.put("blNo"         	, blNo);
		            modelIns2.put("recDt"        	, recDt);       //20
		            
		            modelIns2.put("makeDt"       	, makeDt);
		            modelIns2.put("timePeriodDay"	, timePeriodDay);
		            modelIns2.put("workYn"       	, workYn);                
		            modelIns2.put("rjType"       	, rjType);
		            modelIns2.put("locYn"        	, locYn);       //25
		            
		            modelIns2.put("confYn"       	, confYn);     
		            modelIns2.put("eaCapa"       	, eaCapa);
		            modelIns2.put("inOrdWeight"  	, inOrdWeight); //28
		            modelIns2.put("itemCd"           , itemCd);
		            modelIns2.put("itemNm"           , itemNm);
		            
		            modelIns2.put("transCustNm"      , transCustNm);
		            modelIns2.put("transCustAddr"    , transCustAddr);
		            modelIns2.put("transEmpNm"       , transEmpNm);
		            modelIns2.put("remark"           , remark);
		            modelIns2.put("transZipNo"       , transZipNo);
		           
		            modelIns2.put("etc2"             , etc2);
		            modelIns2.put("unitAmt"          , unitAmt);
		            modelIns2.put("transBizNo"       , transBizNo);
		            modelIns2.put("inCustAddr"       , inCustAddr);
		            modelIns2.put("inCustCd"         , inCustCd);
		           
		            modelIns2.put("inCustNm"         , inCustNm);                 
		            modelIns2.put("inCustTel"        , inCustTel);
		            modelIns2.put("inCustEmpNm"      , inCustEmpNm);      
		            modelIns2.put("expiryDate"       , expiryDate);
		            modelIns2.put("salesCustNm"      , salesCustNm);
		            
		            modelIns2.put("zip"       		, zip);
		            modelIns2.put("addr"       		, addr);
		            modelIns2.put("phone1"       	, phone1);
		            modelIns2.put("etc1"     	 	, etc1);
		            modelIns2.put("unitNo"     	 	, unitNo);
		            
		            modelIns2.put("salesCompanyNm"    	, salesCompanyNm);
		            
		            modelIns2.put("time_date"        , timeDate);
		            modelIns2.put("time_date_end"    , timeDateEnd);                
		            modelIns2.put("time_use_end"     , timeUseEnd);  
		            modelIns2.put("locCd"     		, locCd);  
		            modelIns2.put("vrOrdType"		, model.get("vrOrdType"));
		
		            modelIns2.put("epType"     		, epType);
		            
		            modelIns2.put("lotType"     	, lotType);  
		            modelIns2.put("ownerCd"     	, ownerCd);  
		            
		            modelIns2.put("LC_ID"    		, model.get("SS_SVC_NO"));   
		            modelIns2.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
		            modelIns2.put("USER_NO"  		, model.get("SS_USER_NO"));
	            
		            modelIns2 = (Map<String, Object>)dao1.saveExcelOrder(modelIns2);
		            ServiceUtil.isValidReturnCode("WMSOP520", String.valueOf(modelIns2.get("O_MSG_CODE")), (String)modelIns2.get("O_MSG_NAME"));
					
				}
			}
	        m.put("MSG", MessageResolver.getMessage("save.success"));
	        m.put("MSG_ORA", "");
	        m.put("errCnt", 0);
	    }catch(Exception e){
	    	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
	        m.put("errCnt", 1);
	        m.put("MSG", MessageResolver.getMessage("save.error") );
	    }
	    return m;
	}
	/**
     *  Method ID 	: saveInsQty
     *  Method 설명  	: GS발주등록
     *  작성자            	 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInsQty(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int listBodyCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(listBodyCnt > 0){
	            String[] no             = new String[listBodyCnt];     
	            
	            String[] outReqDt       = new String[listBodyCnt];         
	            String[] inReqDt        = new String[listBodyCnt];     
	            String[] custOrdNo      = new String[listBodyCnt];     
	            String[] custOrdSeq     = new String[listBodyCnt];    
	            String[] trustCustCd    = new String[listBodyCnt];     
	            
	            String[] transCustCd    = new String[listBodyCnt];                     
	            String[] transCustTel   = new String[listBodyCnt];         
	            String[] transReqDt     = new String[listBodyCnt];     
	            String[] custCd         = new String[listBodyCnt];     
	            String[] ordQty         = new String[listBodyCnt];     
	            
	            String[] uomCd          = new String[listBodyCnt];                
	            String[] sdeptCd        = new String[listBodyCnt];         
	            String[] salePerCd      = new String[listBodyCnt];     
	            String[] carCd          = new String[listBodyCnt];     
	            String[] drvNm          = new String[listBodyCnt];     
	            
	            String[] dlvSeq         = new String[listBodyCnt];                
	            String[] drvTel         = new String[listBodyCnt];         
	            String[] custLotNo      = new String[listBodyCnt];     
	            String[] blNo           = new String[listBodyCnt];     
	            String[] recDt          = new String[listBodyCnt];     
	            
	            String[] outWhCd        = new String[listBodyCnt];                
	            String[] inWhCd         = new String[listBodyCnt];         
	            String[] makeDt         = new String[listBodyCnt];     
	            String[] timePeriodDay  = new String[listBodyCnt];     
	            String[] workYn         = new String[listBodyCnt];     
	            
	            String[] rjType         = new String[listBodyCnt];                
	            String[] locYn          = new String[listBodyCnt];         
	            String[] confYn         = new String[listBodyCnt];     
	            String[] eaCapa         = new String[listBodyCnt];     
	            String[] inOrdWeight    = new String[listBodyCnt];     
	            
	            String[] itemCd         = new String[listBodyCnt];                
	            String[] itemNm         = new String[listBodyCnt];         
	            String[] transCustNm    = new String[listBodyCnt];     
	            String[] transCustAddr  = new String[listBodyCnt];     
	            String[] transEmpNm     = new String[listBodyCnt];     
	            
	            String[] remark         = new String[listBodyCnt];                
	            String[] transZipNo     = new String[listBodyCnt];         
	            String[] etc2           = new String[listBodyCnt];     
	            String[] unitAmt        = new String[listBodyCnt];     
	            String[] transBizNo     = new String[listBodyCnt];     
	            
	            String[] inCustAddr     = new String[listBodyCnt];                
	            String[] inCustCd       = new String[listBodyCnt];         
	            String[] inCustNm       = new String[listBodyCnt];     
	            String[] inCustTel      = new String[listBodyCnt];     
	            String[] inCustEmpNm    = new String[listBodyCnt];     
	            
	            String[] expiryDate     = new String[listBodyCnt];
	            String[] salesCustNm    = new String[listBodyCnt];
	            String[] zip     		= new String[listBodyCnt];
	            String[] addr     		= new String[listBodyCnt];
	            String[] phone1    	 	= new String[listBodyCnt];
	            
	            String[] etc1    		= new String[listBodyCnt];
	            String[] unitNo    		= new String[listBodyCnt];               
	            String[] salesCompanyNm	 	= new String[listBodyCnt];
	            String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
	            String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
	            
	            String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
	            String[] locCd     		= new String[listBodyCnt];   //로케이션코드
	            String[] epType     	= new String[listBodyCnt];   //납품유형
	            
//              String[] ordInsDt     	= new String[listBodyCnt];   //주문등록일 	2022-03-03
	            String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
                String[] ownerCd     	= new String[listBodyCnt];   //소유자	 	2022-03-04
                
	            // I_ 로 시작하는 변수는 안담김
	            for (int i = 0; i < listBodyCnt; i++) {
	            	String OUT_REQ_DT      = "";
	    			String IN_REQ_DT       = "";
	    			String CUST_ORD_NO     = "";
	    			String CUST_ORD_SEQ    = "";
	    			String TRUST_CUST_CD   = "";
	    			String TRANS_CUST_CD   = "";
	    			String TRANS_CUST_TEL  = "";
	    			String TRANS_REQ_DT    = "";
	    			String CUST_CD         = "";
	    			String ORD_QTY         = "";
	    			String UOM_CD          = "";
	    			String SDEPT_CD        = "";
	    			String SALE_PER_CD     = "";
	    			String CAR_CD          = "";
	    			String DRV_NM          = "";
	    			String DLV_SEQ         = "";
	    			String DRV_TEL         = "";
	    			String CUST_LOT_NO     = "";
	    			String BL_NO           = "";
	    			String REC_DT          = "";
	    			String OUT_WH_CD       = "";
	    			String IN_WH_CD        = "";
	    			String MAKE_DT         = "";
	    			String TIME_PERIOD_DAY = "";
	    			String WORK_YN         = "";
	    			String RJ_TYPE         = "";
	    			String LOC_YN          = "";
	    			String CONF_YN         = "";
	    			String EA_CAPA         = "";
	    			String IN_ORD_WEIGHT   = "";
	    			String ITEM_CD         = "";
	    			String ITEM_NM         = "";
	    			String TRANS_CUST_NM   = "";
	    			String TRANS_CUST_ADDR = "";
	    			String TRANS_EMP_NM    = "";
	    			String REMARK          = "";
	    			String TRANS_ZIP_NO    = "";
	    			String ETC2            = "";
	    			String UNIT_AMT        = "";
	    			String TRANS_BIZ_NO    = "";
	    			String IN_CUST_ADDR    = "";
	    			String IN_CUST_CD      = "";
	    			String IN_CUST_NM      = "";
	    			String IN_CUST_TEL     = "";
	    			String IN_CUST_EMP_NM  = "";
	    			String EXPIRY_DATE     = "";
	    			String SALES_CUST_NM   = "";
	    			String ZIP             = "";
	    			String ADDR            = "";
	    			String PHONE_1         = "";
	    			String ETC1            = "";
	    			String UNIT_NO         = "";
	    			String SALES_COMPANY_NM   = "";
	    			String TIME_DATE       = "";
	    			String TIME_DATE_END   = "";
	    			String TIME_USE_END    = "";
	    			String LOC_CD    	   = "";
	    			String EP_TYPE    	   = "";
	    			
//        			String ORD_INS_DT       = ""; // 템플릿 추가 sing09  2022-03-03
        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04
        			
	    			
	            	String NO = Integer.toString(i+1);
	            	no[i]               = NO;
	                
	                outReqDt[i]         = OUT_REQ_DT;    
	                inReqDt[i]          = (String)model.get("vrSrchReqDt");
	                custOrdNo[i]        = CUST_ORD_NO;    
	                custOrdSeq[i]       = CUST_ORD_SEQ;    
	                trustCustCd[i]      = TRUST_CUST_CD;    
	                
	                transCustCd[i]      = TRANS_CUST_CD;    
	                transCustTel[i]     = TRANS_CUST_TEL;    
	                transReqDt[i]       = TRANS_REQ_DT;    
	                custCd[i]           = (String)model.get("vrCustCd");    
	                ordQty[i]           = (String)model.get("INS_QTY_" + i);
	                
	                uomCd[i]            = UOM_CD;    
	                sdeptCd[i]          = SDEPT_CD;    
	                salePerCd[i]        = SALE_PER_CD;    
	                carCd[i]            = CAR_CD;
	                drvNm[i]            = DRV_NM;
	                
	                dlvSeq[i]           = DLV_SEQ;    
	                drvTel[i]           = DRV_TEL;    
	                custLotNo[i]        = CUST_LOT_NO;    
	                blNo[i]             = BL_NO;    
	                recDt[i]            = REC_DT;    
	                
	                outWhCd[i]          = OUT_WH_CD;
	                inWhCd[i]           = IN_WH_CD;
	                makeDt[i]           = MAKE_DT;
	                timePeriodDay[i]    = TIME_PERIOD_DAY;
	                workYn[i]           = WORK_YN;
	                
	                rjType[i]           = RJ_TYPE;    
	                locYn[i]            = LOC_YN;    
	                confYn[i]           = CONF_YN;    
	                eaCapa[i]           = EA_CAPA;    
	                inOrdWeight[i]      = IN_ORD_WEIGHT;   
	                
	                itemCd[i]           = (String)model.get("ITEM_CODE_" + i);
	                itemNm[i]           = ITEM_NM;    
	                transCustNm[i]      = TRANS_CUST_NM;    
	                transCustAddr[i]    = TRANS_CUST_ADDR;    
	                transEmpNm[i]       = TRANS_EMP_NM;    
	
	                remark[i]           = (String)model.get("REMARK_" + i);    
	                transZipNo[i]       = TRANS_ZIP_NO;    
	                etc2[i]             = ETC2;    
	                unitAmt[i]          = UNIT_AMT;    
	                transBizNo[i]       = TRANS_BIZ_NO;    
	                
	                inCustAddr[i]       = IN_CUST_ADDR;   
	                inCustCd[i]         = (String)model.get("TRUST_CUST_NM_" + i);
	                inCustNm[i]         = IN_CUST_NM;    
	                inCustTel[i]        = IN_CUST_TEL;    
	                inCustEmpNm[i]      = IN_CUST_EMP_NM;    
	                
	                expiryDate[i]       = (String)model.get("INS_EXPIRY_DATE_" + i);
	                salesCustNm[i]      = SALES_CUST_NM;
	                zip[i]       		= ZIP;
	                addr[i]       		= ADDR;
	                phone1[i]       	= PHONE_1;
	                
	                etc1[i]      		= ETC1;
	                unitNo[i]      		= UNIT_NO;
	                salesCompanyNm[i]   = SALES_COMPANY_NM;
	                timeDate[i]         = TIME_DATE;      
	                timeDateEnd[i]      = TIME_DATE_END;     
	                
	                timeUseEnd[i]       = TIME_USE_END;
	                locCd[i]       		= LOC_CD;
	
	                epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
	                
//                  ordInsDt[i]       	= ORD_INS_DT;	//주문등록일 신규컬럼 추가
                  lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
                  ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가
                  
                //거래처 체크 
      	        Map<String, Object> modelCust = new HashMap<String, Object>();
      	        modelCust.put("TRUST_CUST_ID"   , model.get("vrCustId"));
      	        modelCust.put("CUST_CD"     	, (String)model.get("TRUST_CUST_NM_" + i));
      	        modelCust.put("CUST_NM"     	, (String)model.get("TRUST_CUST_NM_" + i));
      	        modelCust.put("REG_NO"  		, (String)model.get(ConstantIF.SS_USER_NO));
		          int checkCdCnt = dao.checkTxtCustId(modelCust);
		          if(checkCdCnt > 0){
		          }else{
		          	String wmsms010CustId = (String) dao.insertWmsms010(modelCust);
		          	modelCust.put("CUST_ID_I"  		, wmsms010CustId);
		          	modelCust.put("BEST_DATE_YN"  	, "N");
		          	modelCust.put("CUST_INOUT_TYPE" , "1");
		          	dao.insertWmsms011(modelCust);
		          }
	            }
	          
	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();
	
	            modelIns.put("no"  , no);
	            if(model.get("vrOrdType").equals("I")){
	                modelIns.put("reqDt"     	, inReqDt);
	                modelIns.put("whCd"      	, inWhCd);
	            }else{
	                modelIns.put("reqDt"     	, outReqDt);
	                modelIns.put("whCd"      	, outWhCd);
	            }
	            modelIns.put("custOrdNo"    	, custOrdNo);
	            modelIns.put("custOrdSeq"   	, custOrdSeq);
	            modelIns.put("trustCustCd"  	, trustCustCd); //5
	            
	            modelIns.put("transCustCd"  	, transCustCd);
	            modelIns.put("transCustTel" 	, transCustTel);
	            modelIns.put("transReqDt"   	, transReqDt);
	            modelIns.put("custCd"       	, custCd);
	            modelIns.put("ordQty"       	, ordQty);      //10
	            
	            modelIns.put("uomCd"        	, uomCd);
	            modelIns.put("sdeptCd"      	, sdeptCd);
	            modelIns.put("salePerCd"    	, salePerCd);
	            modelIns.put("carCd"        	, carCd);
	            modelIns.put("drvNm"        	, drvNm);       //15
	            
	            modelIns.put("dlvSeq"       	, dlvSeq);
	            modelIns.put("drvTel"       	, drvTel);
	            modelIns.put("custLotNo"    	, custLotNo);
	            modelIns.put("blNo"         	, blNo);
	            modelIns.put("recDt"        	, recDt);       //20
	            
	            modelIns.put("makeDt"       	, makeDt);
	            modelIns.put("timePeriodDay"	, timePeriodDay);
	            modelIns.put("workYn"       	, workYn);                
	            modelIns.put("rjType"       	, rjType);
	            modelIns.put("locYn"        	, locYn);       //25
	            
	            modelIns.put("confYn"       	, confYn);     
	            modelIns.put("eaCapa"       	, eaCapa);
	            modelIns.put("inOrdWeight"  	, inOrdWeight); //28
	            modelIns.put("itemCd"           , itemCd);
	            modelIns.put("itemNm"           , itemNm);
	            
	            modelIns.put("transCustNm"      , transCustNm);
	            modelIns.put("transCustAddr"    , transCustAddr);
	            modelIns.put("transEmpNm"       , transEmpNm);
	            modelIns.put("remark"           , remark);
	            modelIns.put("transZipNo"       , transZipNo);
	           
	            modelIns.put("etc2"             , etc2);
	            modelIns.put("unitAmt"          , unitAmt);
	            modelIns.put("transBizNo"       , transBizNo);
	            modelIns.put("inCustAddr"       , inCustAddr);
	            modelIns.put("inCustCd"         , inCustCd);
	           
	            modelIns.put("inCustNm"         , inCustNm);                 
	            modelIns.put("inCustTel"        , inCustTel);
	            modelIns.put("inCustEmpNm"      , inCustEmpNm);      
	            modelIns.put("expiryDate"       , expiryDate);
	            modelIns.put("salesCustNm"      , salesCustNm);
	            
	            modelIns.put("zip"       		, zip);
	            modelIns.put("addr"       		, addr);
	            modelIns.put("phone1"       	, phone1);
	            modelIns.put("etc1"     	 	, etc1);
	            modelIns.put("unitNo"     	 	, unitNo);
	            
	            modelIns.put("salesCompanyNm"    	, salesCompanyNm);
	            
	            modelIns.put("time_date"        , timeDate);
	            modelIns.put("time_date_end"    , timeDateEnd);                
	            modelIns.put("time_use_end"     , timeUseEnd);  
	            modelIns.put("locCd"     		, locCd);  
	            modelIns.put("vrOrdType"		, model.get("vrOrdType"));
	
	            modelIns.put("epType"     		, epType);  
                //sing09 주석해제
//              modelIns.put("ordInsDt"     	, ordInsDt);  
	            modelIns.put("lotType"     		, lotType);  
	            modelIns.put("ownerCd"     		, ownerCd);  
	            
	            modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
	            modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
	            modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
	            
            modelIns = (Map<String, Object>)dao1.saveExcelOrder(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP520", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
}