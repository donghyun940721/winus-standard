package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP940Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSOP940Service")
public class WMSOP940ServiceImpl extends AbstractServiceImpl implements WMSOP940Service{

	@Resource(name = "WMSOP940Dao")
    private WMSOP940Dao dao;
	
	/**
     * Method ID : listE01
     * Method 설명 : 대화물류 IF 조회 - 거래처
     * 작성자 : 
     * @param model
     * @return
     */
	public Map<String, Object> listE01(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE01(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

}
