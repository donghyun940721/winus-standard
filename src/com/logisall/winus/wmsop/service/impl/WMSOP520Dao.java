package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP520Dao")
public class WMSOP520Dao extends SqlMapAbstractDAO{

    public GenericResultSet listE01(Map<String, Object> model) throws Exception {
		return executeQueryPageWq("wmsop520.list_T1_GS", model);
	}
    public GenericResultSet listE02(Map<String, Object> model) throws Exception {
    	return executeQueryPageWq("wmsop520.list_T2_GS", model);
    }
    /**
     * Method ID : listSub_T1
     * Method 설명 : 발주관리 구성품 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listSub_T1(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop520.listSub_T1", model);
    }   
    /**
     * Method ID : listQ1
     * Method 설명 : 세트상품조회 팝업
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listQ1(Map<String, Object> model) {
        return executeQueryPageWq("wmsop521.listQ1", model);
    }   
	/**
	 * Method ID : saveInsQtyStep
	 * Method 설명 : GS발주 시 세트품 기준 계산
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
    public Object saveInsQtyStep(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop520.saveInsQtyStep", model);
	}   
    
    /**
	 * Method ID : saveInsQty
	 * Method 설명 : GS발주등록
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void saveInsQty(Map<String, Object> model) {
		executeUpdate("wmsop520.saveInsQty", model);
		
	}
	
	 /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID  : selectItemGrp99
     * Method 설명  : GS리테일 상품 업체
     * 작성자             : sing09
     * @param model
     * @return
     */
    public Object selectItemGrp98(Map<String, Object> model){
    	return executeQueryForList("wmsms094.listGSR98", model);
    }
    /**
     * Method ID  : selectItemGrp99
     * Method 설명  : GS리테일 상품 소구분
     * 작성자             : sing09
     * @param model
     * @return
     */
    public Object selectItemGrp99(Map<String, Object> model){
    	return executeQueryForList("wmsms094.listGSR99", model);
    }
    
    /**
	 * Method ID : insertWmsms010 
	 * Method 설명 : 거래처등록 
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object insertWmsms010(Map<String, Object> model) {
		return executeInsert("wmsms010.insert2", model);
	}

	/**
	 * Method ID : insertWmsms011 
	 * Method 설명 : 거래처등록 
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object insertWmsms011(Map<String, Object> model) {
		return executeInsert("wmsms011.insert", model);
	}
	 public int checkTxtCustId(Map<String, Object> model) {
    	// 중복검사
    	return (Integer) executeQueryForObject("wmsms010.checkTxtCustId", model);
    }
}
