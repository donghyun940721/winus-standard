package com.logisall.winus.wmsop.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.logisall.winus.wmsop.vo.WMSOP035VO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP035Dao")
public class WMSOP035Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 출고관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop035.list", model);
    }
   
    /**
     * Method ID  : list2
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object list2(Map<String, Object> model){
        return executeQueryForList("wmsop035.list2", model);
    }
    
	public List<WMSOP035VO> pdfDown(Map<String, Object> model, String company) {
		if(company.equals("DHL")){
			return executeQueryForList("wmsop035.pdfDownDHL", model);	
		}else if(company.equals("FEDEX")) {
			return executeQueryForList("wmsop035.pdfDownFEDEX", model);
		}
		else {
			return executeQueryForList("wmsop035.pdfDownQXP", model);
		}
	}
	
	/**
     * Method ID    : outSaveComplete
     * Method 설명      : 
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object outSaveComplete(Map<String, Object> model){
        executeUpdate("pk_wmsif060.sp_ord_id_out_complete", model);
        return model;
    }

	public List searchUnshipped(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.searchUnshipped", model);;
		return list;
	}
    
	/**
	 * Method ID : list2DHL
	 * Method 설명 : 수출신고정보
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list2DHL(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.list2DHL", model);;
		genericResultSet.setList(list);
		return genericResultSet;
	}
	
	/**
	 * Method ID 		: list2QXP
	 * Method 설명 	: 수출신고정보
	 * 작성자 			: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list2QXP(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.list2QXP", model);;
		genericResultSet.setList(list);
		return genericResultSet;
	}
	
	public List request2EMS(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.request2EMS", model);
		return list;
	}

	public List request2EMSDetail(Map<String, Object> smap) {
		List list = executeQueryForList("wmsop035.request2EMSDetail", smap);
		return list;
	}
	
	/**
	 * Method ID : saveList2DHL 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object saveList2DHL(Map<String, Object> model) {
		return executeUpdate("wmsop035.saveList2DHL", model);
	}

	public GenericResultSet list2EMS(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.list2EMS", model);;
		genericResultSet.setList(list);
		return genericResultSet;
	}

	public Map<String, Object> emsAddress(Map<String, Object> model) {
		return (Map<String, Object>)executeQueryForObject("wmsop035.emsAddress", model);
	}
	
	public List addressHistory(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.addressHistory", model);;
		return list;
	}
	
	public List addressOM010(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.addressOM010", model);;
		return list;
	}

	public int selectShipmentPublish(Map<String, Object> model) {
		return (int)executeView("wmsop035.selectShipmentPublish", model);
	}

	public void updateShipmentPublish(Map<String, Object> model) {
		executeUpdate("wmsop035.updateShipmentPublish", model);
		
	}

	public void insertShipmentPublish(Map<String, Object> model) {
		executeUpdate("wmsop035.insertShipmentPublish", model);
		
	}

	public Object export2EMS(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.export2EMS", model);;
		genericResultSet.setList(list);
		return genericResultSet;
	}

	public Object saveExport2EMS(List list) {
		return executeInsert("wmsop035.saveExport2EMS", list);
	}
	
	public Object saveExport2EMS_V2(List list) {
		return executeUpdate("wmsop035.saveExport2EMS_V2", list);
	}
	

	public String getOrdId(String orgOrdId) {
		return (String)executeView("wmsop035.getOrdId", orgOrdId);
	}
	
	public int getEmsShipment(String ordId) {
		return (int)executeView("wmsop035.getEmsShipment", ordId);
	}

	public List<Map> checkBlNo(String obj) {
		return executeQueryForList("wmsop035.checkBlNo", obj);
	}

	public void updateEmsBlNo(Map<String, Object> obj) {
		executeUpdate("wmsop035.updateEmsBlNo", obj);
		
	}

	public void updateDhlBlNo(Map<String, Object> obj) {
		executeUpdate("wmsop035.updateDhlBlNo", obj);
		
	}
	public void updateDhlBlNoTracking(Map<String, Object> obj) {
        executeUpdate("wmsop035.updateDhlBlNoTracking", obj);
        
    }

	public void updateCommonBlNo(Map<String, Object> obj) {
		executeUpdate("wmsop035.updateCommonBlNo", obj);
		
	}
	public void deleteEmsTracking(Map<String, Object> obj) {
		executeUpdate("wmsop035.deleteEmsTracking", obj);
		
	}
	
	public void deleteDhlTracking(Map<String, Object> obj) {
		executeUpdate("wmsop035.deleteDhlTracking", obj);
		
	}
	
	public void deleteCommonTracking(Map<String, Object> obj) {
		executeUpdate("wmsop035.deleteCommonTracking", obj);
		
	}
	
	 /**
     * Method ID  : listE5
     * Method 설명  : 올리브영 수출신고번호 or 운송장번호 누락 조회
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public GenericResultSet listE5(Map<String, Object> model){
    	if(model.get("vrSrchShippingCompany5").equals("10")){
    		return executeQueryPageWq("wmsop035.listE5EMS", model);
    	}else if(model.get("vrSrchShippingCompany5").equals("20")){
    		return executeQueryPageWq("wmsop035.listE5DHL", model);	
    	}else if(model.get("vrSrchShippingCompany5").equals("30")) {
    		return executeQueryPageWq("wmsop035.listE5QXP", model);
    	}else if(model.get("vrSrchShippingCompany5").equals("60")) {
    		return executeQueryPageWq("wmsop035.listE5FDX", model);
    	}
    	else{
    		return executeQueryPageWq("wmsop035.listE5", model);
    	}
    }
    
    /**
     * Method ID  : listE6EMS
     * Method 설명  : 올리브영 운송장 출력 여부 조회 (EMS)
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public Object listE6EMS(Map<String, Object> model){
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.listE6EMS", model);
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  : listE6DHL
     * Method 설명  : 올리브영 운송장 출력 여부 조회 (DHL)
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public Object listE6DHL(Map<String, Object> model){
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.listE6DHL", model);
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  : listE6QXP
     * Method 설명  : 올리브영 운송장 출력 여부 조회 (QXP)
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public Object listE6QXP(Map<String, Object> model){
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.listE6QXP", model);
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  : listE6LXP
     * Method 설명  : 올리브영 운송장 출력 여부 조회 (QXP)
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public Object listE6LXP(Map<String, Object> model){
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.listE6LXP", model);
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  : listE6FDX
     * Method 설명  : 올리브영 운송장 출력 여부 조회 (FDX)
     * 작성자             : HYS
     * @param model
     * @return
     */
    public Object listE6FDX(Map<String, Object> model){
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.listE6FDX", model);
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  	: selectPrintDt
     * Method 설명  	: 올리브영 운송장 출력 시간 조회
     * 작성자            : KSJ
     * @param model
     * @return
     */
    public List selectPrintDt(Map<String, Object> model){
    	List list = executeQueryForList("wmsop035.selectPrintDt", model);
		return list;
    }
    
    /**
     * Method ID  	 : updatetPrintDt
     * Method 설명  	 : 올리브영 운송장 출력 시간 업데이트 
     * 작성자             : KSJ
     * @param model
     * @return
     */
	public Object updatetPrintDt(Map<String, Object> model) {
		executeUpdate("wmsop035.updatePrintDt", model);
		executeUpdate("wmsop035.insertPrintHistory", model);
		
		/*
		if(model.get("shippingCompany").equals("10")){
			executeUpdate("wmsop035.updatePrintDtEms", model);
		}else{
			executeUpdate("wmsop035.updatePrintDtDhl", model);
		}
		*/
		
		return model;
	}
	
	/**
	 * Method ID 	: getShippingCompanyList 
	 * Method 설명 	: 올리브영 특송사 리스트
	 * 작성자       : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet getShippingCompanyList(Map<String, Object> model) {
		return executeQueryPageWq("wmsop035.getShippingCompanyList", model);
	}
	
	
    /**
     * Method ID  	 	: getSynnaraOrderCheck
     * Method 설명  	: 올리브영 - 신나라 주문 체크 
     * 작성자           : KSJ
     * @param model
     * @return
     */
	public int getSynnaraOrderCheck(Map<String, Object> model) {
		return (int) executeView("wmsop035.getSynnaraOrderCheck", model);
	}
	
	 /**
     * Method ID  		: getSynnaraOrderList
     * Method 설명  	: 올리브영 - 신나라 주문 조회
     * 작성자           : KSJ
     * @param model
     * @return
     */
    public GenericResultSet getSynnaraOrderList(Map<String, Object> model){
    	// return executeQueryPageWq("wmsop035.getSynnaraOrderList", model);
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.getSynnaraOrderList", model);
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  		: outBoundComplete
     * Method 설명  	: 올리브영 - 신나라 주문 출고확정처리
     * 작성자           : KSJ
     * @param model
     * @return
     */
	public Object outBoundComplete(Map<String, Object> model) {
		executeUpdate("wmsop035.outBoundCompleteOp010", model);
		executeUpdate("wmsop035.outBoundCompleteOp011", model);
		// model.put("queryErrCnt","0");
		return model;
	}
	
    /**
     * Method ID  		: updateDelYn
     * Method 설명  	: 올리브영 - update delYn
     * 작성자           : KSJ
     * @param model
     * @return
     */
    public Object updateDelYn(Map<String, Object> model){
        executeUpdate("wmsop035.updateDelYn", model);
        return model;
    }   
    
    /**
     * Method ID  	 : sendPickingListRobot
     * Method 설명   : 출고관리(올리브영) LG CNS 로봇 연동 테이블 피킹리스트 insert 
     * 작성자        : KSJ
     * @param model
     * @return
     */
	public Object sendPickingListRobot(Map<String, Object> model) {
		
		int maxInsertDegree = (int) executeView("wmsop035.getMaxInsertDegree", model);
		model.put("vrSrchInsertDegree", maxInsertDegree);
		
		executeUpdate("wmsop035.sendPickingListRobot", model);
		return model;
	}
	
	public Map<String, Object> selectUnsentInvoiceList(Map<String, Object> model) {
		List<Map<String, Object>> resultList = executeQueryForList("wmsop035.selectUnsentInvoiceList", model);
		model.put("LIST", resultList);
		return model;
	}
}
