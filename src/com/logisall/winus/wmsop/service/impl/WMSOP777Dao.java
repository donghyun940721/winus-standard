package com.logisall.winus.wmsop.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP777Dao")
public class WMSOP777Dao extends SqlMapAbstractDAO{
    
    /**
     * Method ID    : listE1
     * Method 설명      : 주문할당수량 조회
     * 작성자                 : atomyoun
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop777.listE1", model);
    }
    
    /**
     * Method ID    : listE2
     * Method 설명      : 주문할당수량(상품) 조회
     * 작성자                 : atomyoun
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop777.listE2", model);
    }
    
    /**
     * Method ID    : listE2Count
     * Method 설명      : 주문건수 & 주문seq수
     * 작성자                 : atomyoun
     * @param   model
     * @return
     */
    public Object listE2OrdCount(Map<String, Object> model) {
    	return executeView("wmsop777.listE2OrdCount", model);
    }
    

}
