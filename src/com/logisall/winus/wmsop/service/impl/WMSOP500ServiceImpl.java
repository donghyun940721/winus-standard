package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmsop.service.WMSOP500Service;

@Service("WMSOP500Service")
public class WMSOP500ServiceImpl implements WMSOP500Service {
	protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP500Dao")
    private WMSOP500Dao dao;
    
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.list(model));
		return map;
	}
	
	public Map<String, Object> listHistory(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.listHistory(model));
		return map;
	}

}
