package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.api.oywcs.dao.OliveWcsApiDao;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.frm.exception.NotFoundException;
import com.logisall.winus.wmsif.service.impl.WMSIF610Dao;
import com.logisall.winus.wmsop.dto.SaveInitOrdersDTO;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP030Service")
public class WMSOP030ServiceImpl implements WMSOP030Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao;
    
    @Resource(name = "WMSOP520Dao")
    private WMSOP520Dao dao520;
    
    @Resource(name = "WMSOP640Dao")
    private WMSOP640Dao dao640;
    
	@Resource(name = "WMSIF610Dao")
	private WMSIF610Dao dao610;
	
	@Autowired
	private OliveWcsApiDao oliveWcsApiDao;
    
    /**
     * Method ID   : selectItemGrp
     * Method 설명    : 출고관리 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID   : list
     * Method 설명    : 출고관리  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.list(model));
        return map;
    }
    /**
     * Method ID   : listCountDH
     * Method 설명    : 출고관리  작업구분 카운트(대화물류)
     * 작성자               : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCountDH(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        //등록차수 
        if(model.get("selectCnt") != null && !model.get("selectCnt").equals("")){
	        int selectCnt = Integer.parseInt(model.get("selectCnt").toString());
	        String[] ordDegreeValuesStrs = new String[selectCnt];
//	        if(model.get("selectItemCnt") != null && !model.get("selectItemCnt").equals("")){
//	        	int itemCnt = Integer.parseInt(model.get("selectItemCnt").toString());
//	        	String[] srchRitemCdList = new String[itemCnt];
//	        	if(itemCnt == 1){
//	        		srchRitemCdList[0] = String.valueOf(model.get("vrSrchRitemCd"));
//	        	}else{
//	        		srchRitemCdList = (String[])model.get("vrSrchRitemCd");
//	        	}
//	        	model.put("srchRitemCdList", srchRitemCdList);
//	        }
	        
	        if(selectCnt == 1){
	        	ordDegreeValuesStrs[0] =  String.valueOf(model.get("vrSrchOrdDegreeSelectBox"));
	        }else{
	        	 ordDegreeValuesStrs =  (String[]) model.get("vrSrchOrdDegreeSelectBox");
	        }
	        
	        if(selectCnt > 0){
	        	List<String> vrOrdInsDtOrdDegreeList   = new ArrayList<String>();
	        	List<String> vrOrdInsDtList   = new ArrayList<String>();
	            for(int i = 0 ; i < selectCnt ; i ++){
		        	String valueStr = (String) ordDegreeValuesStrs[i];
		        	if(valueStr != null && !valueStr.equals("")){
			        	String[] strArr = valueStr.split("_");
			        	if(strArr.length>1){
			        		//날짜 + 차주
			        		vrOrdInsDtOrdDegreeList.add(valueStr);
			        	}else{
			        		//날짜
			        		vrOrdInsDtList.add(valueStr);
			        	}
		        	}
		        }
	            
	            
	            if(vrOrdInsDtList.size() > 0 && vrOrdInsDtOrdDegreeList.size() > 0 ){
	            	model.put("vrOrdInsDtOrdDegreeType","ALL");
	            }else{
	            	model.put("vrOrdInsDtOrdDegreeType", null);
	            }
	        	
	        	model.put("vrOrdInsDtOrdDegreeList", vrOrdInsDtOrdDegreeList);
	        	model.put("vrOrdInsDtList", vrOrdInsDtList);
	        }
        }
        
        map.put("LIST", dao.listCountDH(model));
        return map;
    }
    /**
     * Method ID   : listCountOmDH
     * Method 설명    : 출고관리(B2C)  작업구분 카운트(대화물류)
     * 작성자               : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCountOmDH(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listCountOmDH(model));
        return map;
    }
    /**
     * Method ID   : listByCust
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCust(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        if(model.get("vrSrchOrdSubtype").equals("140")) {
            
            model.put("vrGubun1", "50"); //재고이동
            model.put("vrGubun2", "132"); //유통점출고
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
       	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
       	 String chekbox[] = {subtypeGubun3};
       	 
       	 model.put("chekbox", chekbox);
       } 
        
        map.put("LIST", dao.listByCust(model));
        return map;
    }
    
    /**
     * Method ID   : listByCustCJ
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustCJ(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listByCustCJ(model));
        return map;
    }
    
    /**
     * Method ID   : listByCustSF
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustSF(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listByCustSF(model));
        return map;
    }
    
    /**
     * Method ID : inspectionList
     * Method 설명 : 내역 조회 (올리브영)
     * 작성자 : seongjun Kwon
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> inspectionList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {            

        	String lc_id = (String)model.get(ConstantIF.SS_SVC_NO);
        	if(lc_id.equals("0000002860")){/*CJ올리브영 물류센터*/
        		map.put("LIST", dao.inspectionListOlive(model));
        	}else{
        		map.put("LIST", dao.inspectionList(model));
        	}
        		
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID   : deleteOrder
     * Method 설명    : 출고주문삭제
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //int errCnt = 1;
        String errMsg = MessageResolver.getMessage("delete.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] orgOrdId = new String[tmpCnt];  
                String[] legacyOrgOrdNo = new String[tmpCnt];  
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    				= (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   				= (String)model.get("ORD_SEQ"+i);
                    orgOrdId[i]   			= (String)model.get("ORG_ORD_ID"+i);
                    legacyOrgOrdNo[i]   = (String)model.get("LEGACY_ORG_ORD_NO"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("orgOrdId", orgOrdId);
                modelIns.put("legacyOrgOrdNo", legacyOrgOrdNo);
                
                //session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

				//dao
                modelIns = (Map<String, Object>)dao.deleteOrder(modelIns);	

                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * 
     * Method ID   : listExcel
     * Method 설명    : 입출고현황 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        // formData Key 값 동기화 - 출고관리(화주별_OM) > (v2.0)
        if(model.containsKey("vrSrchReqDtFrom2"))	{ model.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom2").toString()); }
        if(model.containsKey("vrSrchReqDtTo2"))		{ model.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo2").toString()); }
        if(model.containsKey("vrSrchCustCd3"))		{ model.put("vrSrchCustCd", model.get("vrSrchCustCd3").toString()); }
        if(model.containsKey("vrSrchCustNm3"))		{ model.put("vrSrchCustNm", model.get("vrSrchCustNm3").toString()); }
        if(model.containsKey("vrSrchCustId3"))		{ model.put("vrSrchCustId", model.get("vrSrchCustId3").toString()); }
        
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * Method ID   : listExcelNew
     * Method 설명    : 출고관리(카카오 SAP) 엑셀
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcelNew(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listNew(model));
        return map;
    }
    
    
    /**
     * 
     * Method ID   : listExcelPlt
     * Method 설명    : 물류용기 출고 엑셀용조회
     * 작성자                      : smics
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcelPlt(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listPlt(model));
        return map;
    } 
    
    /**
     * 대체 Method ID   : saveAlloc
     * 대체 Method 설명    : 할당처리,취소
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveAlloc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
            int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
                String[] ordId = new String[temCnt];
                String[] ordSeq = new String[temCnt];
                for(int i = 0 ; i < temCnt ; i ++){
                    ordId[i]   = (String)model.get("ORD_ID"+i);
                    ordSeq[i]  = (String)model.get("ORD_SEQ"+i);
                            
                }       
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ordId", ordId); 
                modelDt.put("ordSeq", ordSeq); 

                modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                if(model.get("vrType").equals("Y")){            //할당처리
                    modelDt = (Map<String, Object>)dao.saveOrdAlloc(modelDt);
                }else if(model.get("vrType").equals("X")){      //할당취소
                    modelDt = (Map<String, Object>)dao.saveOrdCancAlloc(modelDt);
                }
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));                                
            }   
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            

        }catch(Exception e){
            throw e;
        }
        return m;
    }  
//    saveOrdPick
    
    /**
     * 대체 Method ID   : saveOrdPick
     * 대체 Method 설명    : 피킹확정
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOrdPick(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
            int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
                String[] ordId = new String[temCnt];
                String[] ordSeq = new String[temCnt];
                for(int i = 0 ; i < temCnt ; i ++){
                    ordId[i]   = (String)model.get("ORD_ID"+i);
                    ordSeq[i]  = (String)model.get("ORD_SEQ"+i);
                            
                }       
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ordId", ordId); 
                modelDt.put("ordSeq", ordSeq); 
                
                modelDt.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO)   );
                modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                modelDt = (Map<String, Object>)dao.saveOrdPick(modelDt);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
                                
            }               
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * 대체 Method ID   : saveCancelPick
     * 대체 Method 설명    : 피킹취소
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveCancelPick(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
                String[] ordId = new String[temCnt];
                String[] ordSeq = new String[temCnt];
                String[] pickingWorkId = new String[temCnt];
                for(int i = 0 ; i < temCnt ; i ++){
                    ordId[i]   = (String)model.get("ORD_ID"+i);
                    ordSeq[i]  = (String)model.get("ORD_SEQ"+i);
                    pickingWorkId[i] = (String)model.get("PICKING_WORK_ID"+i);
                }       
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ordId", ordId); 
                modelDt.put("ordSeq", ordSeq); 
                modelDt.put("pickingWorkId", pickingWorkId);
                
                modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                modelDt = (Map<String, Object>)dao.saveCancelPick(modelDt);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
                
            }   
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * 
     * Method ID   : saveInComplete
     * Method 설명    : 출고확정
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] workQty = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
                    workQty[i]    		= null;  
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("workQty", workQty);
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOutComplete(modelIns);
                ServiceUtil.isValidReturnCodeNew("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : saveOutCompleteOrdMulti
     * Method 설명    : 출고확정(주문별)
     * 작성자               : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutCompleteOrdMulti(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", (Object)ordId);
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOutCompleteOrdMulti(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : saveOutCompleteDH
     * Method 설명    : 출고확정(대화물류) - 수탁증 자동생성 로직 추가
     * 작성자               : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutCompleteDH(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	try{
    		
    		int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
    		if(tmpCnt > 0){
    			String[] ordId  = new String[tmpCnt];                
    			String[] ordSeq = new String[tmpCnt];
    			String[] workQty = new String[tmpCnt];
    			
    			for(int i = 0 ; i < tmpCnt ; i ++){
    				ordId[i]    = (String)model.get("ORD_ID"+i);               
    				ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
    				workQty[i]    		= null;  
    			}
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    			
    			modelIns.put("ordId", ordId);
    			modelIns.put("ordSeq", ordSeq);
    			modelIns.put("workQty", workQty);
    			
    			//session 및 등록정보
    			modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			
    			//dao                
    			modelIns = (Map<String, Object>)dao.saveOutComplete(modelIns);
    			
    			ServiceUtil.isValidReturnCodeNew("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    			if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
    				modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
    				dao.saveOutCompleteCreateAcc(modelIns);
    				dao.saveOutCompleteCreateAccEtc(modelIns);
    			}
    			ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    			
    		}
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(BizException be) {
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * Method ID   : outUpdateZero
     * Method 설명    : 출고0처리확정
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> outUpdateZero(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i); 
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.outUpdateZero(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : cancelOutReReceiving
     * Method 설명    : 출고확정취소 처리 후 자동 재입고
     * 작성자               : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> cancelOutReReceiving(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] custId = new String[tmpCnt];
                String[] workQty = new String[tmpCnt];
                
                String[] resetOrdId  = new String[tmpCnt];                
                String[] resetOrdSeq = new String[tmpCnt];
                String[] resetWorkStat = new String[tmpCnt];
                String[] resetCustId = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
                    custId[i]   = (String)model.get("CUST_ID"+i);
                    workQty[i]    		= null;  
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                Map<String, Object> modelIns_cancel = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("custId", custId);
                modelIns.put("workQty", workQty);
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.cancelOutReReceiving(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	Map<String, Object> modelRs = new HashMap<String, Object>();
                	
                	modelRs.put("ordId", (String)model.get("ORD_ID"+i));
                	modelRs.put("ordSeq", (String)model.get("ORD_SEQ"+i));
                	
                	resetOrdId[i]    = (String)model.get("ORD_ID"+i);               
                    resetOrdSeq[i]   = (String)model.get("ORD_SEQ"+i);
                    resetWorkStat[i] = (String)dao.getCancelWorkStat(modelRs);
                    resetCustId[i]   = (String)model.get("CUST_ID"+i);
                    
                }
                
                modelIns_cancel.put("ordId", resetOrdId);
                modelIns_cancel.put("ordSeq", resetOrdSeq);
                modelIns_cancel.put("resetWorkStat", resetWorkStat);
                modelIns_cancel.put("resetCustId", resetCustId);
                
                //session 및 등록정보
                modelIns_cancel.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns_cancel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns_cancel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                String vLc_id = (String)model.get(ConstantIF.SS_SVC_NO);
                //출고취소처리된 주문 초기화
                // 2023-11-08 / kakao vx 출고 취소 처리 수정사항 /출고 취소 된 주문 '출고 취소' 상태 유지
                if("0000003200".equals(vLc_id)
//                		|| "0000001262".equals(vLc_id)
                	){
                	modelIns_cancel.put("O_MSG_CODE",0);
                	modelIns_cancel.put("O_MSG_NAME","SUCCESS");
                }else{
                	modelIns_cancel = (Map<String, Object>)dao.outOrdCancelReset(modelIns_cancel);
                }
                if("0000003541".equals(vLc_id) || "0000003721".equals(vLc_id) || "0000003722".equals(vLc_id) || "0000003720".equals(vLc_id) || "0000001262".equals(vLc_id)){
                    Map<String, Object> modelAcc = new HashMap<String, Object>();
                    String[] accOrdId = new String[tmpCnt];
                    String[] accOrdSeq = new String[tmpCnt];
                    String[] accCustId = new String[tmpCnt];
                    for(int i = 0 ; i < tmpCnt ; i ++){                        
                        accOrdId[i]    = (String)model.get("ORD_ID"+i);               
                        accOrdSeq[i]   = (String)model.get("ORD_SEQ"+i);
                        accCustId[i]   = (String)model.get("CUST_ID"+i);
                    }
                    modelAcc.put("ORD_ID", accOrdId);
                    modelAcc.put("ORD_SEQ", accOrdSeq);
                    modelAcc.put("CUST_ID", accCustId);
                    modelAcc.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                    modelAcc.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelAcc.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                    
                    dao.saveOutCompleteCreateAccByOrder(modelAcc);
                }
                
                ServiceUtil.isValidReturnCode("WMSOP031", String.valueOf(modelIns_cancel.get("O_MSG_CODE")), (String)modelIns_cancel.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : simpleInO2O
     * Method 설명    : O2O 입고
     * 작성자               : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> simpleInO2O(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		
    		int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
    		if(tmpCnt > 0){
    			String[] ordId  = new String[tmpCnt];                
    			String[] ordSeq = new String[tmpCnt];
    			String[] custId = new String[tmpCnt];
    			String[] workQty = new String[tmpCnt];
    			
    			for(int i = 0 ; i < tmpCnt ; i ++){
    				ordId[i]    = (String)model.get("ORD_ID"+i);               
    				ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
    				custId[i]   = (String)model.get("CUST_ID"+i);
    				workQty[i]    		= null;  
    			}
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    			
    			modelIns.put("ordId", ordId);
    			modelIns.put("ordSeq", ordSeq);
    			modelIns.put("custId", custId);
    			modelIns.put("workQty", workQty);
    			
    			//session 및 등록정보
    			modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			
    			//dao                
    			modelIns = (Map<String, Object>)dao.simpleInO2O(modelIns);
    			ServiceUtil.isValidReturnCode("WMSIF108", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    			
    		}
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(BizException be) {
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * Method ID   : saveSimpleOut
     * Method 설명    : 간편출고 저장
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSimpleOut(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg = MessageResolver.getMessage("save.error");
        
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                }
                //프로시져에 보낼것들 다담는다
//                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns = (Map<String, Object>)dao.saveSimpleOut(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            
            m.put("ERROR_MSG", (String)modelIns.get("O_MSG_NAME"));
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * Method ID   : ordCancelWorkApparel
     * Method 설명    : 주문취소
     * 작성자               : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> ordCancelWorkApparel(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg = MessageResolver.getMessage("save.error");
        
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                }
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);

    			//session 및 등록정보
    			modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    			modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns = (Map<String, Object>)dao.ordCancelWorkApparel(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            
            m.put("ERROR_MSG", (String)modelIns.get("O_MSG_NAME"));
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * Method ID   : saveSimpleOutSatori
     * Method 설명    : 간편출고 저장
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSimpleOutSatori(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg = MessageResolver.getMessage("save.error");
        
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] LcId = new String[tmpCnt]; 
                String[] RitemId = new String[tmpCnt]; 
                String[] CustLotNo = new String[tmpCnt];
                String[] SapBarcode = new String[tmpCnt];
                String[] OutOrdQty = new String[tmpCnt]; 
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
                    LcId[i]   = (String)model.get("LC_ID"+i);      
                    RitemId[i]    = (String)model.get("RITEM_ID"+i);               
                    CustLotNo[i]   = (String)model.get("CUST_LOT_NO"+i);
                    SapBarcode[i]   = (String)model.get("NULL");
                    OutOrdQty[i]    = (String)model.get("OUT_ORD_QTY"+i);               
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();          
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("LcId", LcId);
                modelIns.put("RitemId", RitemId);
                modelIns.put("CustLotNo", CustLotNo);
                modelIns.put("SapBarcode", SapBarcode);
                modelIns.put("OutOrdQty", OutOrdQty);
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                log.info(modelIns);
                modelIns = (Map<String, Object>)dao.runSpOrdInsSimpleOutSatori(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
          
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));          
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );      
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
  
    
    /**
     * 
     * Method ID   : saveSimpleOutDH
     * Method 설명    : 간편출고 저장(대화) => 수탁증생성 추가
     * 작성자               : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSimpleOutDH(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg = MessageResolver.getMessage("save.error");
        
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns = (Map<String, Object>)dao.saveSimpleOut(modelIns);
                
    			//session 및 등록정보
    			modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    			if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
    				modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
    				dao.saveOutCompleteCreateAcc(modelIns);
    				dao.saveOutCompleteCreateAccEtc(modelIns);
    			}
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : combineOrder
     * Method 설명    : 발주 합치기(대화물류 요청기능)
     * 작성자               : ykim
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> combineOrder(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	try{
    		
    		int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
    		if(tmpCnt > 0){
    			String[] ordId  = new String[tmpCnt];                
    			String[] ordSeq = new String[tmpCnt];          
    			
    			for(int i = 0 ; i < tmpCnt ; i ++){
    				ordId[i]    = (String)model.get("ORD_ID"+i);               
    				ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
    			}
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    			
    			modelIns.put("ORD_ID", ordId);
    			modelIns.put("ORD_SEQ", ordSeq);
    			modelIns.put("ORD_DEGREE", (String)model.get("vrOrdDegree"));
    			modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    			modelIns = (Map<String, Object>)dao.combineOrder(modelIns);
    			ServiceUtil.isValidReturnCode("WMSOP030 > [발주합치기]", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    		}
    		
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(BizException be) {
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * Method ID   : ordWorkHistory
     * Method 설명    : 작업이력조회 (대화물류)
     * 작성자               : dhkim
     * @param model
     * @return
     * @throws Exception
     */    
    @Override
    public Map<String, Object> ordWorkHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if(model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            m.put("LIST", dao.ordWorkHistory(model));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     * Method ID    : getSampleExcelDown
     * Method 설명      : 엑셀 샘플 다운
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Map<String, Object> > sample = new ArrayList<Map<String, Object>>();
        int colSize = Integer.parseInt(model.get("Col_Size").toString());
        
        for(int i = 0 ; i < colSize ; i++){
            Map<String, Object> sampleMap = new HashMap<String, Object>(); 
            sampleMap.put("sampleCol", (String)model.get("Col_"+i));
            if(CommonUtil.isNull((String)model.get("Row_"+i))){
                sampleMap.put("sampleRow", "");
            }else{
                sampleMap.put("sampleRow", (String)model.get("Row_"+i));
            }
            sample.add(sampleMap);
        }
        map.put("LIST", getExcelSampleList(sample));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrder
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            if(totCnt > 0){
                String[] no             = new String[totCnt];     
                
                String[] outReqDt       = new String[totCnt];         
                String[] inReqDt        = new String[totCnt];     
                String[] custOrdNo      = new String[totCnt];     
                String[] custOrdSeq     = new String[totCnt];    
                String[] trustCustCd    = new String[totCnt];     
                
                String[] transCustCd    = new String[totCnt];                     
                String[] transCustTel   = new String[totCnt];         
                String[] transReqDt     = new String[totCnt];     
                String[] custCd         = new String[totCnt];     
                String[] ordQty         = new String[totCnt];     
                
                String[] uomCd          = new String[totCnt];                
                String[] sdeptCd        = new String[totCnt];         
                String[] salePerCd      = new String[totCnt];     
                String[] carCd          = new String[totCnt];     
                String[] drvNm          = new String[totCnt];     
                
                String[] dlvSeq         = new String[totCnt];                
                String[] drvTel         = new String[totCnt];         
                String[] custLotNo      = new String[totCnt];     
                String[] blNo           = new String[totCnt];     
                String[] recDt          = new String[totCnt];     
                
                String[] outWhCd        = new String[totCnt];                
                String[] inWhCd         = new String[totCnt];         
                String[] makeDt         = new String[totCnt];     
                String[] timePeriodDay  = new String[totCnt];     
                String[] workYn         = new String[totCnt];     
                
                String[] rjType         = new String[totCnt];                
                String[] locYn          = new String[totCnt];         
                String[] confYn         = new String[totCnt];     
                String[] eaCapa         = new String[totCnt];     
                String[] inOrdWeight    = new String[totCnt];     
                
                // String[] outOrdWeight    = new String[totCnt];     
                
                String[] itemCd         = new String[totCnt];                
                String[] itemNm         = new String[totCnt];         
                String[] transCustNm    = new String[totCnt];     
                String[] transCustAddr  = new String[totCnt];     
                String[] transEmpNm     = new String[totCnt];     
                
                String[] remark         = new String[totCnt];                
                String[] transZipNo     = new String[totCnt];         
                String[] etc2           = new String[totCnt];     
                String[] unitAmt        = new String[totCnt];     
                String[] transBizNo     = new String[totCnt];     
                
                String[] inCustAddr     = new String[totCnt];                
                String[] inCustCd       = new String[totCnt];         
                String[] inCustNm       = new String[totCnt];     
                String[] inCustTel      = new String[totCnt];     
                String[] inCustEmpNm    = new String[totCnt];     
                
                String[] expiryDate     = new String[totCnt];
                
                String[] salesCustNm    = new String[totCnt];
                String[] zip     		= new String[totCnt];
                String[] addr     		= new String[totCnt];
                String[] phone1    	 	= new String[totCnt];
                String[] etc1    		= new String[totCnt];
                
                String[] unitNo    		= new String[totCnt];
                String[] salesCompanyNm = new String[totCnt];
                String[] locCd         = new String[totCnt];
                
                String[] epType         = new String[totCnt];
                
                String[] lotType         = new String[totCnt];
                String[] ownerCd         = new String[totCnt];


                
                for(int i = 0 ; i < totCnt ; i ++){
                    no[i]               = (String)model.get("no"+i);    
                    
                    outReqDt[i]         = (String)model.get("outReqDt"+i);    
                    inReqDt[i]          = (String)model.get("inReqDt"+i);    
                    custOrdNo[i]        = (String)model.get("custOrdNo"+i);    
                    custOrdSeq[i]       = (String)model.get("custOrdSeq"+i);    
                    trustCustCd[i]      = (String)model.get("trustCustCd"+i);    
                    
                    transCustCd[i]      = (String)model.get("transCustCd"+i);    
                    transCustTel[i]     = (String)model.get("transCustTel"+i);    
                    transReqDt[i]       = (String)model.get("transReqDt"+i);    
                    custCd[i]           = (String)model.get("custCd"+i);    
                    ordQty[i]           = (String)model.get("ordQty"+i);    
                    
                    uomCd[i]            = (String)model.get("uomCd"+i);    
                    sdeptCd[i]          = (String)model.get("sdeptCd"+i);    
                    salePerCd[i]        = (String)model.get("salePerCd"+i);    
                    carCd[i]            = (String)model.get("carCd"+i);    
                    drvNm[i]            = (String)model.get("drvNm"+i);    
                    
                    dlvSeq[i]           = (String)model.get("dlvSeq"+i);    
                    drvTel[i]           = (String)model.get("drvTel"+i);    
                    custLotNo[i]        = (String)model.get("custLotNo"+i);    
                    blNo[i]             = (String)model.get("blNo"+i);    
                    recDt[i]            = (String)model.get("recDt"+i);    
                    
                    outWhCd[i]          = (String)model.get("outWhCd"+i);    
                    inWhCd[i]           = (String)model.get("inWhCd"+i);    
                    makeDt[i]           = (String)model.get("makeDt"+i);    
                    timePeriodDay[i]    = (String)model.get("timePeriodDay"+i);    
                    workYn[i]           = (String)model.get("workYn"+i);    
                    
                    rjType[i]           = (String)model.get("rjType"+i);    
                    locYn[i]            = (String)model.get("locYn"+i);    
                    confYn[i]           = (String)model.get("confYn"+i);    
                    eaCapa[i]           = (String)model.get("eaCapa"+i);    
                    inOrdWeight[i]      = (String)model.get("inOrdWeight"+i);   
                    
                    // outOrdWeight[i]      = (String)model.get("outOrdWeight"+i);   
                    
                    itemCd[i]           = (String)model.get("itemCd"+i);    
                    itemNm[i]           = (String)model.get("itemNm"+i);    
                    transCustNm[i]      = (String)model.get("transCustNm"+i);    
                    transCustAddr[i]    = (String)model.get("transCustAddr"+i);    
                    transEmpNm[i]       = (String)model.get("transEmpNm"+i);    

                    remark[i]           = (String)model.get("remark"+i);    
                    transZipNo[i]       = (String)model.get("transZipNo"+i);    
                    etc2[i]             = (String)model.get("etc2"+i);    
                    unitAmt[i]          = (String)model.get("unitAmt"+i);    
                    transBizNo[i]       = (String)model.get("transBizNo"+i);    
                    
                    inCustAddr[i]       = (String)model.get("inCustAddr"+i);    
                    inCustCd[i]         = (String)model.get("inCustCd"+i);    
                    inCustNm[i]         = (String)model.get("inCustNm"+i);    
                    inCustTel[i]        = (String)model.get("inCustTel"+i);    
                    inCustEmpNm[i]      = (String)model.get("inCustEmpNm"+i);    
                    
                    expiryDate[i]       = (String)model.get("expiryDate"+i);
                    
                    salesCustNm[i]      = (String)model.get("salesCustNm"+i);
                    zip[i]       		= (String)model.get("zip"+i);
                    addr[i]       		= (String)model.get("addr"+i);
                    phone1[i]       	= (String)model.get("phone1"+i);
                    etc1[i]      		= (String)model.get("etc1"+i);
                    
                    unitNo[i]      		= (String)model.get("unitNo"+i);
                    salesCompanyNm[i]   = (String)model.get("salesCompanyNm"+i);
                    locCd[i]           = (String)model.get("locCd"+i);
                    
                    epType[i]           = (String)model.get("epType"+i);
                    
                    lotType[i]           = (String)model.get("lotType"+i);
                    ownerCd[i]           = (String)model.get("ownerCd"+i);
                    
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("no"  , no);
                
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     , inReqDt);
                    modelIns.put("whCd"      , inWhCd);
                }else{
                    modelIns.put("reqDt"     , outReqDt);
                    modelIns.put("whCd"      , outWhCd);
                }
                
                modelIns.put("custOrdNo"    , custOrdNo);
                modelIns.put("custOrdSeq"   , custOrdSeq);
                modelIns.put("trustCustCd"  , trustCustCd); //5
                
                modelIns.put("transCustCd"  , transCustCd);
                modelIns.put("transCustTel" , transCustTel);
                modelIns.put("transReqDt"   , transReqDt);
                modelIns.put("custCd"       , custCd);
                modelIns.put("ordQty"       , ordQty);      //10
                
                modelIns.put("uomCd"        , uomCd);
                modelIns.put("sdeptCd"      , sdeptCd);
                modelIns.put("salePerCd"    , salePerCd);
                modelIns.put("carCd"        , carCd);
                modelIns.put("drvNm"        , drvNm);       //15
                
                modelIns.put("dlvSeq"       , dlvSeq);
                modelIns.put("drvTel"       , drvTel);
                modelIns.put("custLotNo"    , custLotNo);
                modelIns.put("blNo"         , blNo);
                modelIns.put("recDt"        , recDt);       //20
                
                modelIns.put("makeDt"       , makeDt);
                modelIns.put("timePeriodDay", timePeriodDay);
                modelIns.put("workYn"       , workYn);                
                modelIns.put("rjType"       , rjType);
                modelIns.put("locYn"        , locYn);       //25
                
                modelIns.put("confYn"       , confYn);     
                modelIns.put("eaCapa"       , eaCapa);
                modelIns.put("inOrdWeight"  , inOrdWeight); //28
                
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);      
                
                modelIns.put("expiryDate"       , expiryDate);
                
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("phone1"       	, phone1);
                modelIns.put("etc1"     	 	, etc1);
                
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("salesCompanyNm"           , salesCompanyNm);
                modelIns.put("locCd"           , locCd);
                
                modelIns.put("epType"           , epType);
                
                modelIns.put("lotType"           , lotType);
                modelIns.put("ownerCd"           , ownerCd);
                  
          
                //session 정보 및 String 타입
                modelIns.put("vrOrdType", model.get("vrOrdType"));
                modelIns.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO));   
                modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));  
                modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveExcelOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                                
                m.put("O_CUR", modelIns.get("O_CUR"));                
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
      
    /**
     * 
     * 대체 Method ID   : saveExcelOrderSAP
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderSAP(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            if(totCnt > 0){
                String[] no             = new String[totCnt];     
                
                String[] outReqDt       = new String[totCnt];         
                String[] inReqDt        = new String[totCnt];     
                String[] custOrdNo      = new String[totCnt];     
                String[] custOrdSeq     = new String[totCnt];    
                String[] trustCustCd    = new String[totCnt];     
                
                String[] transCustCd    = new String[totCnt];                     
                String[] transCustTel   = new String[totCnt];         
                String[] transReqDt     = new String[totCnt];     
                String[] custCd         = new String[totCnt];     
                String[] ordQty         = new String[totCnt];     
                
                String[] uomCd          = new String[totCnt];                
                String[] sdeptCd        = new String[totCnt];         
                String[] salePerCd      = new String[totCnt];     
                String[] carCd          = new String[totCnt];     
                String[] drvNm          = new String[totCnt];     
                
                String[] dlvSeq         = new String[totCnt];                
                String[] drvTel         = new String[totCnt];         
                String[] custLotNo      = new String[totCnt];     
                String[] blNo           = new String[totCnt];     
                String[] recDt          = new String[totCnt];     
                
                String[] outWhCd        = new String[totCnt];                
                String[] inWhCd         = new String[totCnt];         
                String[] makeDt         = new String[totCnt];     
                String[] timePeriodDay  = new String[totCnt];     
                String[] workYn         = new String[totCnt];     
                
                String[] rjType         = new String[totCnt];                
                String[] locYn          = new String[totCnt];         
                String[] confYn         = new String[totCnt];     
                String[] eaCapa         = new String[totCnt];     
                String[] inOrdWeight    = new String[totCnt];     
                
                // String[] outOrdWeight    = new String[totCnt];     
                
                String[] itemCd         = new String[totCnt];                
                String[] itemNm         = new String[totCnt];         
                String[] transCustNm    = new String[totCnt];     
                String[] transCustAddr  = new String[totCnt];     
                String[] transEmpNm     = new String[totCnt];     
                
                String[] remark         = new String[totCnt];                
                String[] transZipNo     = new String[totCnt];         
                String[] etc2           = new String[totCnt];     
                String[] unitAmt        = new String[totCnt];     
                String[] transBizNo     = new String[totCnt];     
                
                String[] inCustAddr     = new String[totCnt];                
                String[] inCustCd       = new String[totCnt];         
                String[] inCustNm       = new String[totCnt];     
                String[] inCustTel      = new String[totCnt];     
                String[] inCustEmpNm    = new String[totCnt];     
                
                String[] expiryDate     = new String[totCnt];
                
                String[] salesCustNm    = new String[totCnt];
                String[] zip     		= new String[totCnt];
                String[] addr     		= new String[totCnt];
                String[] addr2     		= new String[totCnt];
                String[] phone1    	 	= new String[totCnt];
                String[] etc1    		= new String[totCnt];
                
                String[] deliveryNo    	= new String[totCnt];
                String[] shipmentNo    	= new String[totCnt];
                
                //추가
                String[] timeDate      = new String[totCnt];   //상품유효기간     
                String[] timeDateEnd  = new String[totCnt];   //상품유효기간만료일
                String[] timeUseEnd   = new String[totCnt];   //소비가한만료일
                
                
                for(int i = 0 ; i < totCnt ; i ++){
                    no[i]               = (String)model.get("no"+i);    
                    
                    outReqDt[i]         = (String)model.get("outReqDt"+i);    
                    inReqDt[i]          = (String)model.get("inReqDt"+i);    
                    custOrdNo[i]        = (String)model.get("custOrdNo"+i);    
                    custOrdSeq[i]       = (String)model.get("custOrdSeq"+i);    
                    trustCustCd[i]      = (String)model.get("trustCustCd"+i);    
                    
                    transCustCd[i]      = (String)model.get("transCustCd"+i);    
                    transCustTel[i]     = (String)model.get("transCustTel"+i);    
                    transReqDt[i]       = (String)model.get("transReqDt"+i);    
                    custCd[i]           = (String)model.get("custCd"+i);    
                    ordQty[i]           = (String)model.get("ordQty"+i);    
                    
                    uomCd[i]            = (String)model.get("uomCd"+i);    
                    sdeptCd[i]          = (String)model.get("sdeptCd"+i);    
                    salePerCd[i]        = (String)model.get("salePerCd"+i);    
                    carCd[i]            = (String)model.get("carCd"+i);    
                    drvNm[i]            = (String)model.get("drvNm"+i);    
                    
                    dlvSeq[i]           = (String)model.get("dlvSeq"+i);    
                    drvTel[i]           = (String)model.get("drvTel"+i);    
                    custLotNo[i]        = (String)model.get("custLotNo"+i);    
                    blNo[i]             = (String)model.get("blNo"+i);    
                    recDt[i]            = (String)model.get("recDt"+i);    
                    
                    outWhCd[i]          = (String)model.get("outWhCd"+i);    
                    inWhCd[i]           = (String)model.get("inWhCd"+i);    
                    makeDt[i]           = (String)model.get("makeDt"+i);    
                    timePeriodDay[i]    = (String)model.get("timePeriodDay"+i);    
                    workYn[i]           = (String)model.get("workYn"+i);    
                    
                    rjType[i]           = (String)model.get("rjType"+i);    
                    locYn[i]            = (String)model.get("locYn"+i);    
                    confYn[i]           = (String)model.get("confYn"+i);    
                    eaCapa[i]           = (String)model.get("eaCapa"+i);    
                    inOrdWeight[i]      = (String)model.get("inOrdWeight"+i);   
                    
                    // outOrdWeight[i]      = (String)model.get("outOrdWeight"+i);   
                    
                    itemCd[i]           = (String)model.get("itemCd"+i);    
                    itemNm[i]           = (String)model.get("itemNm"+i);    
                    transCustNm[i]      = (String)model.get("transCustNm"+i);    
                    transCustAddr[i]    = (String)model.get("transCustAddr"+i);    
                    transEmpNm[i]       = (String)model.get("transEmpNm"+i);    

                    remark[i]           = (String)model.get("remark"+i);    
                    transZipNo[i]       = (String)model.get("transZipNo"+i);    
                    etc2[i]             = (String)model.get("etc2"+i);    
                    unitAmt[i]          = (String)model.get("unitAmt"+i);    
                    transBizNo[i]       = (String)model.get("transBizNo"+i);    
                    
                    inCustAddr[i]       = (String)model.get("inCustAddr"+i);    
                    inCustCd[i]         = (String)model.get("inCustCd"+i);    
                    inCustNm[i]         = (String)model.get("inCustNm"+i);    
                    inCustTel[i]        = (String)model.get("inCustTel"+i);    
                    inCustEmpNm[i]      = (String)model.get("inCustEmpNm"+i);    
                    
                    expiryDate[i]       = (String)model.get("expiryDate"+i);
                    
                    salesCustNm[i]      = (String)model.get("salesCustNm"+i);
                    zip[i]       		= (String)model.get("zip"+i);
                    addr[i]       		= (String)model.get("addr"+i);
                    addr2[i]       		= (String)model.get("addr2"+i);
                    phone1[i]       	= (String)model.get("phone1"+i);
                    etc1[i]      		= (String)model.get("etc1"+i);
                    
                    deliveryNo[i]      	= (String)model.get("deliveryNo"+i);
                    shipmentNo[i]      	= (String)model.get("shipmentNo"+i);
                    
                    timeDate[i]        = (String)model.get("TIME_DATE"+i);      
                    timeDateEnd[i]    = (String)model.get("TIME_DATE_END"+i);      
                    timeUseEnd[i]     = (String)model.get("TIME_USE_END"+i);  
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("no"  , no);
                
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     , inReqDt);
                    modelIns.put("whCd"      , inWhCd);
                }else{
                    modelIns.put("reqDt"     , outReqDt);
                    modelIns.put("whCd"      , outWhCd);
                }
                
                modelIns.put("custOrdNo"    , custOrdNo);
                modelIns.put("custOrdSeq"   , custOrdSeq);
                modelIns.put("trustCustCd"  , trustCustCd); //5
                
                modelIns.put("transCustCd"  , transCustCd);
                modelIns.put("transCustTel" , transCustTel);
                modelIns.put("transReqDt"   , transReqDt);
                modelIns.put("custCd"       , custCd);
                modelIns.put("ordQty"       , ordQty);      //10
                
                modelIns.put("uomCd"        , uomCd);
                modelIns.put("sdeptCd"      , sdeptCd);
                modelIns.put("salePerCd"    , salePerCd);
                modelIns.put("carCd"        , carCd);
                modelIns.put("drvNm"        , drvNm);       //15
                
                modelIns.put("dlvSeq"       , dlvSeq);
                modelIns.put("drvTel"       , drvTel);
                modelIns.put("custLotNo"    , custLotNo);
                modelIns.put("blNo"         , blNo);
                modelIns.put("recDt"        , recDt);       //20
                
                modelIns.put("makeDt"       , makeDt);
                modelIns.put("timePeriodDay", timePeriodDay);
                modelIns.put("workYn"       , workYn);                
                modelIns.put("rjType"       , rjType);
                modelIns.put("locYn"        , locYn);       //25
                
                modelIns.put("confYn"       , confYn);     
                modelIns.put("eaCapa"       , eaCapa);
                modelIns.put("inOrdWeight"  , inOrdWeight); //28
                
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);      
                
                modelIns.put("expiryDate"       , expiryDate);
                
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                modelIns.put("etc1"     	 	, etc1);
                
                modelIns.put("deliveryNo"     	, deliveryNo);
                modelIns.put("shipmentNo"     	, shipmentNo);
                
                //추가된거(아직프로시져는 안탐)
                modelIns.put("time_date"        , timeDate);
                modelIns.put("time_date_end"    , timeDateEnd);                
                modelIns.put("time_use_end"     , timeUseEnd);  
          
                //session 정보 및 String 타입
                modelIns.put("vrOrdType", model.get("vrOrdType"));
                modelIns.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO));   
                modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));  
                modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveExcelOrderSAP(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030S", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                                
                m.put("O_CUR", modelIns.get("O_CUR"));                
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * Method ID   : listRackSearch
     * Method 설명    : 긴급RACK보충  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listRackSearch(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listRackSearch(model));
        return map;
    }
    
    
	private GenericResultSet getExcelSampleList(List<Map<String, Object>> list) {
		GenericResultSet wqrs = new GenericResultSet();
		int pageIndex = 1;
		int pageSize = 1;
		int pageTotal = 1;
		int pageBlank = (int) Math.ceil(pageTotal / (double) pageSize);
		
		List sampleList = new ArrayList<Map<String, Object>>();
		Map<String, Object> sample = new HashMap<String, Object>();
		for (Map<String, Object> sampleInfo : list) {
			Object key = sampleInfo.get("sampleCol");
			if (key != null && StringUtils.isNotEmpty(key.toString())) {
				sample.put(key.toString(), sampleInfo.get("sampleRow"));				
			}
		}
		sampleList.add(sample);		
		wqrs.setCpage(pageIndex);
		wqrs.setTpage(pageBlank);
		wqrs.setTotCnt(pageTotal);
		wqrs.setList(sampleList);
		return wqrs;
	}
	
	/**
     * 
     * 대체 Method ID   : autoBestLocSave
     * 대체 Method 설명    : 아산물류센터 로케이션추천 자동
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoBestLocSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);  
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.autoBestLocSave(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	     * 
	     * 대체 Method ID   : ordDelSetReordInsert
	     * 대체 Method 설명    : 주문재등록
	     * 작성자                      : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
		 @Override
	    public Map<String, Object> ordDelSetReordInsert(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
	            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	            if(tmpCnt > 0){           
	                String[] ordId 		= new String[tmpCnt];
	                String[] ordSeq 	= new String[tmpCnt];
	                String[] lcId 		= new String[tmpCnt];
	                String[] ritemId 	= new String[tmpCnt];
	                
	                String[] custLotNo 	= new String[tmpCnt];
	                String[] sapBarcode = new String[tmpCnt];
	                String[] workQty 	= new String[tmpCnt];
	                
	                for(int i = 0 ; i < tmpCnt ; i ++){
	                	ordId[i]		= (String)model.get("I_ORD_ID"+i);
	                	ordSeq[i]		= (String)model.get("I_ORD_SEQ"+i);
	                	lcId[i]			= (String)model.get(ConstantIF.SS_SVC_NO);
	                	ritemId[i]		= (String)model.get("I_RITEM_ID"+i);
	                	
	                	custLotNo[i]	= (String)model.get("I_CUST_LOT_NO"+i);
	                	sapBarcode[i]	= (String)model.get("I_SAP_BARCODE"+i);
	                	workQty[i]		= (String)model.get("I_WORK_QTY"+i);
	                }
	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                modelIns.put("I_ORD_ID"			, ordId);
	                modelIns.put("I_ORD_SEQ"		, ordSeq);
	                modelIns.put("I_LC_ID"			, lcId);
	                modelIns.put("I_RITEM_ID"		, ritemId);
	                
	                modelIns.put("I_CUST_LOT_NO"	, custLotNo);
	                modelIns.put("I_SAP_BARCODE"	, sapBarcode);
	                modelIns.put("I_WORK_QTY"		, workQty);

	                //session 및 등록정보
	                //modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                
	                //dao                
	                modelIns = (Map<String, Object>)dao.ordDelSetReordInsert(modelIns);
	                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
		 
	 /**
     * 
     * 대체 Method ID   : asnSave
     * 대체 Method 설명    : ASN생성버튼
     * 작성자                      : smics
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> asnSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);  
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                // ASN 센터별 상품 동기화 진행 
                dao.asnSyncItem(modelIns);

                //dao                
                modelIns = (Map<String, Object>)dao.asnSave(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
     * 대체 Method ID   : WorkUpdateOrder
     * 대체 Method 설명      : 작업지시 팝업
     * 작성자                          : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> WorkUpdateOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "300");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            //작업구분 : 작업완료 일 경우 조회 쿼리 분기
            if (model.get("vrSrchOrderPhase").equals("03")) {
            	//작업완료
            	map.put("LIST", dao.WorkUpdateOrder_Complete(model));
            } else {
            	//의뢰상태, 진행중, 주문전체
            	map.put("LIST", dao.WorkUpdateOrder(model));
            }
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	    
    /**
     * 
     * 대체 Method ID   : saveWorkOrder
     * 대체 Method 설명    : 작업지시 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveWorkOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ORD_ID"  	, model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"   , model.get("ORD_SEQ"+i));
                
                //System.out.println(model.get("ORD_ID"+i) +"//"+ model.get("ORD_SEQ"+i));
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.updateWorkOrder(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    /**
     * Method ID   : changeMapping
     * Method 설명    : 입출고매핑전환
     * 작성자               : MonkeySeok
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> changeMapping(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //int errCnt = 1;
        String errMsg = MessageResolver.getMessage("delete.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId      = new String[tmpCnt];                
                String[] ordSeq     = new String[tmpCnt];               
                String[] lcId       = new String[tmpCnt];                
                String[] ritemId    = new String[tmpCnt];            
                String[] custLotNo  = new String[tmpCnt];      
                String[] sapBarcode = new String[tmpCnt];          
                String[] workQty    = new String[tmpCnt];                
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]      = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]     = (String)model.get("ORD_SEQ"+i);        
                    lcId[i]       = (String)model.get("LC_ID"+i);             
                    ritemId[i]    = (String)model.get("RITEM_ID"+i);             
                    custLotNo[i]  = (String)model.get("CUST_LOT_NO"+i);        
                    sapBarcode[i] = (String)model.get("SAP_BARCODE"+i);        
                    workQty[i]    = (String)model.get("WORK_QTY"+i);              
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId"     , ordId);
                modelIns.put("ordSeq"    , ordSeq);
                modelIns.put("lcId"      , lcId);
                modelIns.put("ritemId"   , ritemId);
                modelIns.put("custLotNo" , custLotNo);
                modelIns.put("sapBarcode", sapBarcode);
                modelIns.put("workQty"   , workQty);
                
                //session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.changeMapping(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
	 * 
	 * 대체 Method ID : adminOrderDelete
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> adminOrderDelete(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			// 삭제
			int lcIdGubun = Integer.parseInt(model.get("vrSrchLcId").toString());
			
			Map<String, Object> modelDt = new HashMap<String, Object>();
			modelDt.put("USER_ID", model.get(ConstantIF.SS_USER_ID));
			modelDt.put("vrSrchLcId", model.get("vrSrchLcId"));
			System.out.println("=============== : " + model.get("vrSrchLcId"));
			
			if(lcIdGubun > 0){
				dao.adminOrderDelete(modelDt);
			} else {
				errCnt++;
				m.put("errCnt", errCnt);
				throw new BizException(MessageResolver.getMessage("save.error"));
			}
			
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
     * 대체 Method ID    : selectBox
     * 대체 Method 설명      : 물류용기관리 화면 데이타셋
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("POOL", dao.selectPool(model));
        return map;
    }
    
    /**
     * Method ID    : getExcelDown2
     * Method 설명      : 엑셀 샘플 다운
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> getExcelDown2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Map<String, Object> > sample = new ArrayList<Map<String, Object>>();
        int colSize = Integer.parseInt(model.get("Col_Size").toString());
        int rowSize = Integer.parseInt(model.get("Row_Size").toString());
        
        for(int k = 0 ; k < rowSize ; k++){
        	if(k == 0){
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
                    sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
                    sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}else{
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
        			sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
        			sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}
        }
        Map<String, Object> sampleRow = new HashMap<String, Object>();
        sampleRow.put("rowSize", rowSize);
        
        map.put("LIST", getExceList2(sample, sampleRow));
        return map;
    }
    
    private GenericResultSet getExceList2(List<Map<String, Object>> list, Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		int pageIndex = 1;
		int pageSize = 1;
		int pageTotal = 1;
		int pageBlank = (int) Math.ceil(pageTotal / (double) pageSize);
		int rowSize = Integer.parseInt(model.get("rowSize").toString());
		
		List sampleList = new ArrayList<Map<String, Object>>();
		
		for(int k = 0 ; k < rowSize ; k++){
			Map<String, Object> sample = new HashMap<String, Object>();
			for (Map<String, Object> sampleInfo : list) {	
				Object key = sampleInfo.get(k+"_sampleCol");
				if (key != null && StringUtils.isNotEmpty(key.toString())) {
					sample.put(key.toString(), sampleInfo.get(k+"_sampleRow"));
				}
			}
			sampleList.add(sample);
		}
		
		wqrs.setCpage(pageIndex);
		wqrs.setTpage(pageBlank);
		wqrs.setTotCnt(pageTotal);
		wqrs.setList(sampleList);
		return wqrs;
	}
    
    /**
     * 
     * Method ID   : outOrderCntInit
     * Method ?ㅻ?    : 
     * ???깆??                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> outOrderCntInit(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DS_CNT", dao.outOrderCntInit(model));
        return map;
    }
    
    /**
     * 
     * Method ID   : outWorkingCntInit
     * Method ?ㅻ?    : 
     * ???깆??                      : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> outWorkingCntInit(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DS_CNT", dao.outWorkingCntInit(model));
        return map;
    }
    
    /*-
	 * Method ID   : customerInfo
	 * Method 설명 : 고객정보 상세조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> customerInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("CUSTOMR_INFO")) {
			map.put("CUSTOMR_INFO", dao.customerInfo(model));
		}
		return map;
	}
    
    /**
     * 
     * 대체 Method ID   : ifOutOrd
     * 대체 Method 설명    : 출고주문(저장,수정,삭제)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> ifOutOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
                //저장, 수정
                String[] ifId 			= new String[insCnt];                
                String[] ifDt         	= new String[insCnt];         
                String[] dnReqNo        = new String[insCnt];     
                String[] materialLotId  = new String[insCnt];   
                
                for(int i = 0 ; i < insCnt ; i ++){
                	ifId[i]  			= (String)model.get("I_IF_ID"+i);               
                	ifDt[i]             = (String)model.get("I_IF_DT"+i);          
                	dnReqNo[i]          = (String)model.get("I_DN_REQ_NO"+i);      
                	materialLotId[i]	= (String)model.get("I_MATERIALLOTID"+i);      
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                Map<String, Object> modelInsConf = new HashMap<String, Object>();
                
                //main
                modelIns.put("I_IF_ID"        	, ifId);
                modelIns.put("I_IF_DT"        	, ifDt);
                modelIns.put("I_DN_REQ_NO"      , dnReqNo);
                modelIns.put("I_MATERIALLOTID"	, materialLotId);
                modelIns.put("I_LC_ID"          , (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_CUST_ID"        , model.get("vrSrchCustId"));
                //session 정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelInsConf = (Map<String, Object>)dao.ifOutOrd(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelInsConf.get("O_MSG_CODE")), (String)modelInsConf.get("O_MSG_NAME"));                

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveDlvNo
     * 대체 Method 설명    : 송장번호저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveDlvNo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            if(totCnt > 0){
                String[] outOrdId      		= new String[totCnt];         
                String[] outOrdSeq     		= new String[totCnt];     
                String[] iSalesCustNm  		= new String[totCnt];     
                String[] iAddr     	   		= new String[totCnt];    
                String[] iPhone1      	 	= new String[totCnt];     
                String[] iDeliveryCompany   = new String[totCnt];
                String[] iDeliveryNo   		= new String[totCnt];
                String[] iOrdDesc   		= new String[totCnt]; 
                
                for(int i = 0 ; i < totCnt ; i ++){
                	outOrdId[i]     	 = (String)model.get("outOrdId"+i);    
                	outOrdSeq[i]    	 = (String)model.get("outOrdSeq"+i);    
                	iSalesCustNm[i] 	 = (String)model.get("iSalesCustNm"+i);    
                	iAddr[i]        	 = (String)model.get("iAddr"+i);    
                	iPhone1[i]     		 = (String)model.get("iPhone1"+i);    
                	iDeliveryCompany[i]  = (String)model.get("iDeliveryCompany"+i);
                	iDeliveryNo[i]  	 = (String)model.get("iDeliveryNo"+i);
                	iOrdDesc[i]  	 	 = (String)model.get("iOrdDesc"+i);
                }
                
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("outOrdId"  		, outOrdId);
                modelIns.put("outOrdSeq"    	, outOrdSeq);
                modelIns.put("iSalesCustNm"     , iSalesCustNm);
                modelIns.put("iAddr"    		, iAddr);
                modelIns.put("iPhone1"   		, iPhone1);
                modelIns.put("iDeliveryCompany" , iDeliveryCompany);
                modelIns.put("iDeliveryNo"  	, iDeliveryNo);
                modelIns.put("iOrdDesc"  		, iOrdDesc);
          
                //session 정보 및 String 타입
                modelIns.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO));   
                modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));  
                modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveDlvNo(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                                
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : listExcelDlvNoTemp
     * Method 설명    : 입출고현황 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcelDlvNoTemp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listByCust(model));
        return map;
    } 
    
    /**
     * Method ID   : search
     * Method 설명    : 템플릿 정보에 따른 컬럼
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("vrTemplateId"     , model.get("vrTemplateId")  );
            modelIns.put("vrCustSeq"        , model.get("vrCustSeq"));

            // dao
            modelIns = (Map<String, Object>)dao.searchTpCol(modelIns);
            ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            //우선 여기서 다국어처리해놓자
            //.vm에서 다국어적용안되길래 여기서 했음
            List<Map<String, Object>> dsTpcolList = (List<Map<String, Object>>)modelIns.get("DS_TPCOL");
            for(int i = 0 ; i < dsTpcolList.size() ; i++){
               dsTpcolList.get(i).put("FORMAT_COL_NM", MessageResolver.getText((String)dsTpcolList.get(i).get("FORMAT_COL_NM")));
            }
            m.put("DS_TPCOL", modelIns.get("DS_TPCOL"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJava
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJava(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];     
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] salesCompanyNm	 	= new String[listBodyCnt];
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                String[] locCd     		= new String[listBodyCnt];   //로케이션코드
                String[] epType     	= new String[listBodyCnt];   //납품유형
                
//                String[] ordInsDt     	= new String[listBodyCnt];   //주문등록일 	2022-03-03
                String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
                String[] ownerCd     	= new String[listBodyCnt];   //소유자	 	2022-03-04
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT      = "";
        			String IN_REQ_DT       = "";
        			String CUST_ORD_NO     = "";
        			String CUST_ORD_SEQ    = "";
        			String TRUST_CUST_CD   = "";
        			String TRANS_CUST_CD   = "";
        			String TRANS_CUST_TEL  = "";
        			String TRANS_REQ_DT    = "";
        			String CUST_CD         = "";
        			String ORD_QTY         = "";
        			String UOM_CD          = "";
        			String SDEPT_CD        = "";
        			String SALE_PER_CD     = "";
        			String CAR_CD          = "";
        			String DRV_NM          = "";
        			String DLV_SEQ         = "";
        			String DRV_TEL         = "";
        			String CUST_LOT_NO     = "";
        			String BL_NO           = "";
        			String REC_DT          = "";
        			String OUT_WH_CD       = "";
        			String IN_WH_CD        = "";
        			String MAKE_DT         = "";
        			String TIME_PERIOD_DAY = "";
        			String WORK_YN         = "";
        			String RJ_TYPE         = "";
        			String LOC_YN          = "";
        			String CONF_YN         = "";
        			String EA_CAPA         = "";
        			String IN_ORD_WEIGHT   = "";
        			String ITEM_CD         = "";
        			String ITEM_NM         = "";
        			String TRANS_CUST_NM   = "";
        			String TRANS_CUST_ADDR = "";
        			String TRANS_EMP_NM    = "";
        			String REMARK          = "";
        			String TRANS_ZIP_NO    = "";
        			String ETC2            = "";
        			String UNIT_AMT        = "";
        			String TRANS_BIZ_NO    = "";
        			String IN_CUST_ADDR    = "";
        			String IN_CUST_CD      = "";
        			String IN_CUST_NM      = "";
        			String IN_CUST_TEL     = "";
        			String IN_CUST_EMP_NM  = "";
        			String EXPIRY_DATE     = "";
        			String SALES_CUST_NM   = "";
        			String ZIP             = "";
        			String ADDR            = "";
        			String PHONE_1         = "";
        			String ETC1            = "";
        			String UNIT_NO         = "";
        			String SALES_COMPANY_NM   = "";
        			String TIME_DATE       = "";
        			String TIME_DATE_END   = "";
        			String TIME_USE_END    = "";
        			String LOC_CD    	   = "";
        			String EP_TYPE    	   = "";
        			
//        			String ORD_INS_DT       = ""; // 템플릿 추가 sing09  2022-03-03
        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL")==null){continue;};//null skip
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				//if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_CD = (String)model.get("vrCustCd");
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_CD")){LOC_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EP_TYPE")){EP_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_INS_DT")){ORD_INS_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOT_TYPE")){LOT_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OWNER_CD")){OWNER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};


        			}
     				
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;    
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;     
                    
                    timeUseEnd[i]       = TIME_USE_END;
                    locCd[i]       		= LOC_CD;

                    epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
                    
//                    ordInsDt[i]       	= ORD_INS_DT;	//주문등록일 신규컬럼 추가
                    lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
                    ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
               
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
               
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);      
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("phone1"       	, phone1);
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                
                modelIns.put("salesCompanyNm"    	, salesCompanyNm);
                
                modelIns.put("time_date"        , timeDate);
                modelIns.put("time_date_end"    , timeDateEnd);                
                modelIns.put("time_use_end"     , timeUseEnd);  
                modelIns.put("locCd"     		, locCd);  
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));

                modelIns.put("epType"     		, epType);  
                //sing09 주석해제
//                modelIns.put("ordInsDt"     	, ordInsDt);  
                modelIns.put("lotType"     		, lotType);  
                modelIns.put("ownerCd"     		, ownerCd);  
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

                //dao                
                modelIns = (Map<String, Object>)dao.saveExcelOrder(modelIns);
                
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                m.put("O_CUR", modelIns.get("O_CUR"));                
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    
    /**1961
     * Method ID   : getTemplate
     * Method 설명    : 저장된 업체별 템플릿 조회.
     * 작성자               : ykim
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getTemplate(Map<String, Object> model) throws Exception {
    	List<Map<String, Object>> m = new ArrayList();
    	try {
    		Map<String, Object> searchParam = new HashMap<String, Object>();
    		searchParam.putAll(model);
    		searchParam.put("vrTemplateType"     	, model.get("vrTemplateType")  );
    		searchParam.put("vrCustSeq"        		, model.get("vrCustSeq"));
    		
    		m.addAll(dao.getTemplateInfo(searchParam));		//Template 조회
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
    /**1961
     * Method ID   : getTemplateALL
     * Method 설명    : 
     * 작성자               : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getTemplateALL(Map<String, Object> model) throws Exception {
    	List<Map<String, Object>> m = new ArrayList();
    	try {
    		Map<String, Object> searchParam = new HashMap<String, Object>();
    		searchParam.putAll(model);
    		searchParam.put("vrTemplateType"     	, model.get("vrTemplateType")  );
    		searchParam.put("vrCustSeq"        		, model.get("vrCustSeq"));
    		
    		m.addAll(dao.getTemplateInfoALL(searchParam));		//Template 조회
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }


    /**
     * Method ID   : getTemplateType
     * Method 설명    : 저장된 업체별 템플릿 조회.
     * 작성자               : ykim
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getTemplateType(Map<String, Object> model) throws Exception {
    	List<Map<String, Object>> m = new ArrayList();
    	try {
    		Map<String, Object> searchParam = new HashMap<String, Object>();
    		searchParam.putAll(model);
    		searchParam.put("vrTemplateType"     	, model.get("vrTemplateType")  );
    		searchParam.put("vrCustSeq"        		, model.get("vrCustSeq"));
    		
    		m.addAll(dao.getTemplateType(searchParam));		//Template 조회
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID   : getTemplateWithBlank
     * Method 설명    : 저장된 업체별 템플릿 조회.
     * 작성자               : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getTemplateWithBlank(Map<String, Object> model) throws Exception {
        List<Map<String, Object>> m = new ArrayList();
        try {
            Map<String, Object> searchParam = new HashMap<String, Object>();
            searchParam.putAll(model);
            searchParam.put("vrTemplateId"        , model.get("vrTemplateId")  );
            searchParam.put("vrCustSeq"             , model.get("vrCustSeq"));
            
            int templateCnt = dao.getTemplateCount(searchParam);
            
            if (templateCnt > 0){
                m.addAll(dao.getTemplateWithBlank(searchParam));
            }
            else {
                m.addAll(dao.getTemplateDefault(searchParam));
            }
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }




    /**
     * 
     * 대체 Method ID   : saveExcelOrderJavaAllowBlank
     * 대체 Method 설명    : 템플릿 주문 저장 - 공란허용 로직
     * 작성자                      : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaAllowBlank(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		List<Map<String, Object>> listBody = (ArrayList)model.get("LIST");
		String custId = String.valueOf(model.get("vrCustCd"));
		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt < 1){
            	m.put("MSG", MessageResolver.getMessage("save.error"));
                m.put("MSG_ORA", "데이터가 없습니다.");
                m.put("errCnt", 1);
            	return m;
            }
            String[] no             = new String[listBodyCnt];     
            
            String[] outReqDt       = new String[listBodyCnt];         
            String[] inReqDt        = new String[listBodyCnt];     
            String[] custOrdNo      = new String[listBodyCnt];     
            String[] custOrdSeq     = new String[listBodyCnt];    
            String[] trustCustCd    = new String[listBodyCnt];     
            
            String[] transCustCd    = new String[listBodyCnt];                     
            String[] transCustTel   = new String[listBodyCnt];         
            String[] transReqDt     = new String[listBodyCnt];     
            String[] custCd         = new String[listBodyCnt];     
            String[] ordQty         = new String[listBodyCnt];     
            
            String[] uomCd          = new String[listBodyCnt];                
            String[] sdeptCd        = new String[listBodyCnt];         
            String[] salePerCd      = new String[listBodyCnt];     
            String[] carCd          = new String[listBodyCnt];     
            String[] drvNm          = new String[listBodyCnt];     
            
            String[] dlvSeq         = new String[listBodyCnt];                
            String[] drvTel         = new String[listBodyCnt];         
            String[] custLotNo      = new String[listBodyCnt];     
            String[] blNo           = new String[listBodyCnt];     
            String[] recDt          = new String[listBodyCnt];     
            
            String[] outWhCd        = new String[listBodyCnt];                
            String[] inWhCd         = new String[listBodyCnt];         
            String[] makeDt         = new String[listBodyCnt];     
            String[] timePeriodDay  = new String[listBodyCnt];     
            String[] workYn         = new String[listBodyCnt];     
            
            String[] rjType         = new String[listBodyCnt];                
            String[] locYn          = new String[listBodyCnt];         
            String[] confYn         = new String[listBodyCnt];     
            String[] eaCapa         = new String[listBodyCnt];     
            String[] inOrdWeight    = new String[listBodyCnt];     
            
            String[] itemCd         = new String[listBodyCnt];                
            String[] itemNm         = new String[listBodyCnt];         
            String[] transCustNm    = new String[listBodyCnt];     
            String[] transCustAddr  = new String[listBodyCnt];     
            String[] transEmpNm     = new String[listBodyCnt];     
            
            String[] remark         = new String[listBodyCnt];                
            String[] transZipNo     = new String[listBodyCnt];         
            String[] etc2           = new String[listBodyCnt];     
            String[] unitAmt        = new String[listBodyCnt];     
            String[] transBizNo     = new String[listBodyCnt];     
            
            String[] inCustAddr     = new String[listBodyCnt];                
            String[] inCustCd       = new String[listBodyCnt];         
            String[] inCustNm       = new String[listBodyCnt];     
            String[] inCustTel      = new String[listBodyCnt];     
            String[] inCustEmpNm    = new String[listBodyCnt];     
            
            String[] expiryDate     = new String[listBodyCnt];
            String[] salesCustNm    = new String[listBodyCnt];
            String[] zip     		= new String[listBodyCnt];
            String[] addr     		= new String[listBodyCnt];
            String[] phone1    	 	= new String[listBodyCnt];
            
            String[] etc1    		= new String[listBodyCnt];
            String[] unitNo    		= new String[listBodyCnt];               
            String[] salesCompanyNm	 	= new String[listBodyCnt];
            String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
            String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
            
            String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
            String[] locCd     		= new String[listBodyCnt];   //로케이션코드
            
            String[] epType     	= new String[listBodyCnt];   //납품유형
            
            
//                String[] ordInsDt     	= new String[listBodyCnt];   //주문등록일	2022-03-03
            String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
            String[] ownerCd     	= new String[listBodyCnt];   //소유자		2022-03-04
            
            for(int i = 0 ; i < listBodyCnt ; i ++){
            	String OUT_REQ_DT      = "";
    			String IN_REQ_DT       = "";
    			String CUST_ORD_NO     = "";
    			String CUST_ORD_SEQ    = "";
    			String TRUST_CUST_CD   = "";
    			String TRANS_CUST_CD   = "";
    			String TRANS_CUST_TEL  = "";
    			String TRANS_REQ_DT    = "";
    			String CUST_CD         = "";
    			String ORD_QTY         = "";
    			String UOM_CD          = "";
    			String SDEPT_CD        = "";
    			String SALE_PER_CD     = "";
    			String CAR_CD          = "";
    			String DRV_NM          = "";
    			String DLV_SEQ         = "";
    			String DRV_TEL         = "";
    			String CUST_LOT_NO     = "";
    			String BL_NO           = "";
    			String REC_DT          = "";
    			String OUT_WH_CD       = "";
    			String IN_WH_CD        = "";
    			String MAKE_DT         = "";
    			String TIME_PERIOD_DAY = "";
    			String WORK_YN         = "";
    			String RJ_TYPE         = "";
    			String LOC_YN          = "";
    			String CONF_YN         = "";
    			String EA_CAPA         = "";
    			String IN_ORD_WEIGHT   = "";
    			String ITEM_CD         = "";
    			String ITEM_NM         = "";
    			String TRANS_CUST_NM   = "";
    			String TRANS_CUST_ADDR = "";
    			String TRANS_EMP_NM    = "";
    			String REMARK          = "";
    			String TRANS_ZIP_NO    = "";
    			String ETC2            = "";
    			String UNIT_AMT        = "";
    			String TRANS_BIZ_NO    = "";
    			String IN_CUST_ADDR    = "";
    			String IN_CUST_CD      = "";
    			String IN_CUST_NM      = "";
    			String IN_CUST_TEL     = "";
    			String IN_CUST_EMP_NM  = "";
    			String EXPIRY_DATE     = "";
    			String SALES_CUST_NM   = "";
    			String ZIP             = "";
    			String ADDR            = "";
    			String PHONE_1         = "";
    			String ETC1            = "";
    			String UNIT_NO         = "";
    			String SALES_COMPANY_NM   = "";
    			String TIME_DATE       = "";
    			String TIME_DATE_END   = "";
    			String TIME_USE_END    = "";
    			String LOC_CD    	   = "";
    			String EP_TYPE    	   = "";
    			
//        			String ORD_INS_DT         = ""; // 템플릿 추가 sing09  2022-03-03
    			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
    			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

    			
    			for(int k = 0 ; k < listHeaderCnt ; k++){
    				Map<String, Object> object = listBody.get(i);
    				String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
    				switch (bindColNm) {
					case "OUT_REQ_DT" 			: OUT_REQ_DT        	= replaceStr(object, k, "OUT_REQ_DT"); break;
					case "IN_REQ_DT"			: IN_REQ_DT				= replaceStr(object, k, "IN_REQ_DT"); break;
					case "CUST_ORD_NO"			: CUST_ORD_NO			= replaceStr(object, k, "CUST_ORD_NO"); break;
					case "CUST_ORD_SEQ"			: CUST_ORD_SEQ			= replaceStr(object, k, "CUST_ORD_SEQ"); break;
					case "TRUST_CUST_CD"		: TRUST_CUST_CD			= replaceStr(object, k, "TRUST_CUST_CD"); break;
					case "TRANS_CUST_CD"		: TRANS_CUST_CD			= replaceStr(object, k, "TRANS_CUST_CD"); break;
					case "TRANS_CUST_TEL"		: TRANS_CUST_TEL		= replaceStr(object, k, "TRANS_CUST_TEL"); break;
					case "TRANS_REQ_DT"			: TRANS_REQ_DT			= replaceStr(object, k, "TRANS_REQ_DT"); break;
					case "ORD_QTY"				: ORD_QTY				= replaceStr(object, k, "ORD_QTY"); break;
					case "UOM_CD"				: UOM_CD				= replaceStr(object, k, "UOM_CD"); break;
					case "SDEPT_CD"				: SDEPT_CD				= replaceStr(object, k, "SDEPT_CD"); break;
					case "SALE_PER_CD"			: SALE_PER_CD			= replaceStr(object, k, "SALE_PER_CD"); break;
					case "CAR_CD"				: CAR_CD				= replaceStr(object, k, "CAR_CD"); break;
					case "DRV_NM"				: DRV_NM				= replaceStr(object, k, "DRV_NM"); break;
					case "DLV_SEQ"				: DLV_SEQ				= replaceStr(object, k, "DLV_SEQ"); break;
					case "DRV_TEL"				: DRV_TEL				= replaceStr(object, k, "DRV_TEL"); break;
					case "CUST_LOT_NO"			: CUST_LOT_NO			= replaceStr(object, k, "CUST_LOT_NO"); break;
					case "BL_NO"				: BL_NO					= replaceStr(object, k, "BL_NO"); break;
					case "REC_DT"				: REC_DT				= replaceStr(object, k, "REC_DT"); break;
					case "OUT_WH_CD"			: OUT_WH_CD				= replaceStr(object, k, "OUT_WH_CD"); break;
					case "IN_WH_CD"				: IN_WH_CD				= replaceStr(object, k, "IN_WH_CD"); break;
					case "MAKE_DT"				: MAKE_DT				= replaceStr(object, k, "MAKE_DT"); break;
					case "TIME_PERIOD_DAY"		: TIME_PERIOD_DAY		= replaceStr(object, k, "TIME_PERIOD_DAY"); break;
					case "WORK_YN"				: WORK_YN				= replaceStr(object, k, "WORK_YN"); break;
					case "RJ_TYPE"				: RJ_TYPE				= replaceStr(object, k, "RJ_TYPE"); break;
					case "LOC_YN"				: LOC_YN				= replaceStr(object, k, "LOC_YN"); break;
					case "CONF_YN"				: CONF_YN				= replaceStr(object, k, "CONF_YN"); break;
					case "EA_CAPA"				: EA_CAPA				= replaceStr(object, k, "EA_CAPA"); break;
					case "IN_ORD_WEIGHT"		: IN_ORD_WEIGHT			= replaceStr(object, k, "IN_ORD_WEIGHT"); break;
					case "ITEM_CD"				: ITEM_CD				= replaceStr(object, k, "ITEM_CD"); break;
					case "ITEM_NM"				: ITEM_NM				= replaceStr(object, k, "ITEM_NM"); break;
					case "TRANS_CUST_NM"		: TRANS_CUST_NM			= replaceStr(object, k, "TRANS_CUST_NM"); break;
					case "TRANS_CUST_ADDR"		: TRANS_CUST_ADDR		= replaceStr(object, k, "TRANS_CUST_ADDR"); break;
					case "TRANS_EMP_NM"			: TRANS_EMP_NM			= replaceStr(object, k, "TRANS_EMP_NM"); break;
					case "REMARK"				: REMARK				= replaceStr(object, k, "REMARK"); break;
					case "TRANS_ZIP_NO"			: TRANS_ZIP_NO			= replaceStr(object, k, "TRANS_ZIP_NO"); break;
					case "ETC2"					: ETC2					= replaceStr(object, k, "ETC2"); break;
					case "UNIT_AMT"				: UNIT_AMT				= replaceStr(object, k, "UNIT_AMT"); break;
					case "TRANS_BIZ_NO"			: TRANS_BIZ_NO			= replaceStr(object, k, "TRANS_BIZ_NO"); break;
					case "IN_CUST_ADDR"			: IN_CUST_ADDR			= replaceStr(object, k, "IN_CUST_ADDR"); break;
					case "IN_CUST_CD"			: IN_CUST_CD			= replaceStr(object, k, "IN_CUST_CD"); break;
					case "IN_CUST_NM"			: IN_CUST_NM			= replaceStr(object, k, "IN_CUST_NM"); break;
					case "IN_CUST_TEL"			: IN_CUST_TEL			= replaceStr(object, k, "IN_CUST_TEL"); break;
					case "IN_CUST_EMP_NM"		: IN_CUST_EMP_NM		= replaceStr(object, k, "IN_CUST_EMP_NM"); break;
					case "EXPIRY_DATE"			: EXPIRY_DATE			= replaceStr(object, k, "EXPIRY_DATE"); break;
					case "SALES_CUST_NM"		: SALES_CUST_NM			= replaceStr(object, k, "SALES_CUST_NM"); break;
					case "ZIP"					: ZIP					= replaceStr(object, k, "ZIP"); break;
					case "ADDR"					: ADDR					= replaceStr(object, k, "ADDR"); break;
					case "PHONE_1"				: PHONE_1				= replaceStr(object, k, "PHONE_1"); break;
					case "ETC1"					: ETC1					= replaceStr(object, k, "ETC1"); break;
					case "UNIT_NO"				: UNIT_NO				= replaceStr(object, k, "UNIT_NO"); break;
					case "SALES_COMPANY_NM"		: SALES_COMPANY_NM		= replaceStr(object, k, "SALES_COMPANY_NM"); break;
					case "TIME_DATE"			: TIME_DATE				= replaceStr(object, k, "TIME_DATE"); break;
					case "TIME_DATE_END"		: TIME_DATE_END			= replaceStr(object, k, "TIME_DATE_END"); break;
					case "TIME_USE_END"			: TIME_USE_END			= replaceStr(object, k, "TIME_USE_END"); break;
					case "LOC_CD"				: LOC_CD				= replaceStr(object, k, "LOC_CD"); break;	
					case "EP_TYPE"				: EP_TYPE				= replaceStr(object, k, "EP_TYPE"); break;	
					
//						case "ORD_INS_DT"			: ORD_INS_DT			= replaceStr(object, k, "ORD_INS_DT"); break;	
					case "LOT_TYPE"				: LOT_TYPE				= replaceStr(object, k, "LOT_TYPE"); break;	
					case "OWNER_CD"				: OWNER_CD				= replaceStr(object, k, "OWNER_CD"); break;	

					default:
						CUST_CD = custId;
					}
    			}
 				
    			String NO = Integer.toString(i+1);
            	no[i]               = NO;
                
                outReqDt[i]         = OUT_REQ_DT.replaceAll("[^\\d]", "");    
                inReqDt[i]          = IN_REQ_DT;    
                custOrdNo[i]        = CUST_ORD_NO;    
                custOrdSeq[i]       = CUST_ORD_SEQ;    
                trustCustCd[i]      = TRUST_CUST_CD;    
                
                transCustCd[i]      = TRANS_CUST_CD;    
                transCustTel[i]     = TRANS_CUST_TEL;    
                transReqDt[i]       = TRANS_REQ_DT;    
                custCd[i]           = CUST_CD;    
                ordQty[i]           = ORD_QTY;    
                
                uomCd[i]            = UOM_CD;    
                sdeptCd[i]          = SDEPT_CD;    
                salePerCd[i]        = SALE_PER_CD;    
                carCd[i]            = CAR_CD;
                drvNm[i]            = DRV_NM;
                
                dlvSeq[i]           = DLV_SEQ;    
                drvTel[i]           = DRV_TEL;    
                custLotNo[i]        = CUST_LOT_NO;    
                blNo[i]             = BL_NO;    
                recDt[i]            = REC_DT;    
                
                outWhCd[i]          = OUT_WH_CD;
                inWhCd[i]           = IN_WH_CD;
                makeDt[i]           = MAKE_DT;
                timePeriodDay[i]    = TIME_PERIOD_DAY;
                workYn[i]           = WORK_YN;
                
                rjType[i]           = RJ_TYPE;    
                locYn[i]            = LOC_YN;    
                confYn[i]           = CONF_YN;    
                eaCapa[i]           = EA_CAPA;    
                inOrdWeight[i]      = IN_ORD_WEIGHT;   
                
                itemCd[i]           = ITEM_CD;    
                itemNm[i]           = ITEM_NM;    
                transCustNm[i]      = TRANS_CUST_NM;    
                transCustAddr[i]    = TRANS_CUST_ADDR;    
                transEmpNm[i]       = TRANS_EMP_NM;    

                remark[i]           = REMARK;    
                transZipNo[i]       = TRANS_ZIP_NO;    
                etc2[i]             = ETC2;    
                unitAmt[i]          = UNIT_AMT.replaceAll("[^\\d|.]", "");    
                transBizNo[i]       = TRANS_BIZ_NO;    
                
                inCustAddr[i]       = IN_CUST_ADDR;   
                inCustCd[i]         = IN_CUST_CD;    
                inCustNm[i]         = IN_CUST_NM;    
                inCustTel[i]        = IN_CUST_TEL;    
                inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                
                expiryDate[i]       = EXPIRY_DATE;
                salesCustNm[i]      = SALES_CUST_NM;
                zip[i]       		= ZIP;
                addr[i]       		= ADDR;
                phone1[i]       	= PHONE_1;
                
                etc1[i]      		= ETC1;
                unitNo[i]      		= UNIT_NO;
                salesCompanyNm[i]   = SALES_COMPANY_NM;
                timeDate[i]         = TIME_DATE;      
                timeDateEnd[i]      = TIME_DATE_END;     
                
                timeUseEnd[i]       = TIME_USE_END;
                locCd[i]       		= LOC_CD;
                epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
                
//                    ordInsDt[i]       	= ORD_INS_DT;	//주문등록일 신규컬럼 추가
                lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
                ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가

                //거래처 체크 GS..
                //GS리테일 거래처 오기입 시 거래처 생성.
                if(model.get("SS_SVC_NO").equals("0000001840") && !"".equals(IN_CUST_CD)){
                    Map<String, Object> modelCust = new HashMap<String, Object>();
                    modelCust.put("TRUST_CUST_ID"   , model.get("vrCustId"));
                    modelCust.put("CUST_CD"     	, IN_CUST_CD);
                    modelCust.put("CUST_NM"     	, IN_CUST_CD);
                    modelCust.put("REG_NO"  		, model.get("SS_USER_NO"));
                    int checkCdCnt = dao520.checkTxtCustId(modelCust);
                    if(checkCdCnt > 0){
                    }else{
                    	String wmsms010CustId = (String) dao520.insertWmsms010(modelCust);
                    	modelCust.put("CUST_ID_I"  		, wmsms010CustId);
                    	modelCust.put("BEST_DATE_YN"  	, "N");
                    	modelCust.put("CUST_INOUT_TYPE" , "1");
                    	dao520.insertWmsms011(modelCust);
                    }
                }
            }
            
            
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("no"  , no);
            if(model.get("vrOrdType").equals("I")){
                modelIns.put("reqDt"     	, inReqDt);
                modelIns.put("whCd"      	, inWhCd);
            }else{
                modelIns.put("reqDt"     	, outReqDt);
                modelIns.put("whCd"      	, outWhCd);
            }
            modelIns.put("custOrdNo"    	, custOrdNo);
            modelIns.put("custOrdSeq"   	, custOrdSeq);
            modelIns.put("trustCustCd"  	, trustCustCd); //5
            
            modelIns.put("transCustCd"  	, transCustCd);
            modelIns.put("transCustTel" 	, transCustTel);
            modelIns.put("transReqDt"   	, transReqDt);
            modelIns.put("custCd"       	, custCd);
            modelIns.put("ordQty"       	, ordQty);      //10
            
            modelIns.put("uomCd"        	, uomCd);
            modelIns.put("sdeptCd"      	, sdeptCd);
            modelIns.put("salePerCd"    	, salePerCd);
            modelIns.put("carCd"        	, carCd);
            modelIns.put("drvNm"        	, drvNm);       //15
            
            modelIns.put("dlvSeq"       	, dlvSeq);
            modelIns.put("drvTel"       	, drvTel);
            modelIns.put("custLotNo"    	, custLotNo);
            modelIns.put("blNo"         	, blNo);
            modelIns.put("recDt"        	, recDt);       //20
            
            modelIns.put("makeDt"       	, makeDt);
            modelIns.put("timePeriodDay"	, timePeriodDay);
            modelIns.put("workYn"       	, workYn);                
            modelIns.put("rjType"       	, rjType);
            modelIns.put("locYn"        	, locYn);       //25
            
            modelIns.put("confYn"       	, confYn);     
            modelIns.put("eaCapa"       	, eaCapa);
            modelIns.put("inOrdWeight"  	, inOrdWeight); //28
            modelIns.put("itemCd"           , itemCd);
            modelIns.put("itemNm"           , itemNm);
            
            modelIns.put("transCustNm"      , transCustNm);
            modelIns.put("transCustAddr"    , transCustAddr);
            modelIns.put("transEmpNm"       , transEmpNm);
            modelIns.put("remark"           , remark);
            modelIns.put("transZipNo"       , transZipNo);
           
            modelIns.put("etc2"             , etc2);
            modelIns.put("unitAmt"          , unitAmt);
            modelIns.put("transBizNo"       , transBizNo);
            modelIns.put("inCustAddr"       , inCustAddr);
            modelIns.put("inCustCd"         , inCustCd);
           
            modelIns.put("inCustNm"         , inCustNm);                 
            modelIns.put("inCustTel"        , inCustTel);
            modelIns.put("inCustEmpNm"      , inCustEmpNm);      
            modelIns.put("expiryDate"       , expiryDate);
            modelIns.put("salesCustNm"      , salesCustNm);
            
            modelIns.put("zip"       		, zip);
            modelIns.put("addr"       		, addr);
            modelIns.put("phone1"       	, phone1);
            modelIns.put("etc1"     	 	, etc1);
            modelIns.put("unitNo"     	 	, unitNo);
            
            modelIns.put("salesCompanyNm"    	, salesCompanyNm);
            
            modelIns.put("time_date"        , timeDate);
            modelIns.put("time_date_end"    , timeDateEnd);                
            modelIns.put("time_use_end"     , timeUseEnd);  
            modelIns.put("locCd"     		, locCd);  
            modelIns.put("epType"     		, epType);  
            
//                modelIns.put("ordInsDt"     	, ordInsDt);  
            modelIns.put("lotType"     		, lotType);  
            modelIns.put("ownerCd"     		, ownerCd);  
            
            modelIns.put("vrOrdType"		, model.get("vrOrdType"));
            
            modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
            modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
            modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
            
            //반품입고관리로 입력 시 
            String vrOrdSubType = (String) model.get("vrOrdSubType");
            if(vrOrdSubType != null){
            	modelIns.put("vrOrdSubType"		, model.get("vrOrdSubType"));
            }


            //dao                
            modelIns = (Map<String, Object>)dao.saveExcelOrder(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("O_CUR", modelIns.get("O_CUR"));
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**2442
     * 
     * 대체 Method ID   : saveExcelOrderJavaCust
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : ykim
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaCust(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
//		String jsonString = gson.toJson(model);
//		String sendData   = new StringBuffer().append(jsonString).toString();
//		
//		JsonParser Parser   = new JsonParser();
//		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
//		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");
    	List<Map<String, Object>> listBody = (ArrayList)model.get("LIST");
    	String custId = String.valueOf(model.get("vrCustCd"));
    	List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
    	
    	int listHeaderCnt = listHeader.size();
    	int listBodyCnt   = listBody.size();
    	
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		if(listBodyCnt > 0){
    			String[] no             = new String[listBodyCnt];     
    			
    			String[] outReqDt       = new String[listBodyCnt];         
    			String[] inReqDt        = new String[listBodyCnt];     
    			String[] custOrdNo      = new String[listBodyCnt];     
    			String[] custOrdSeq     = new String[listBodyCnt];    
    			String[] trustCustCd    = new String[listBodyCnt];     
    			
    			String[] transCustCd    = new String[listBodyCnt];                     
    			String[] transCustTel   = new String[listBodyCnt];         
    			String[] transReqDt     = new String[listBodyCnt];     
    			String[] custCd         = new String[listBodyCnt];     
    			String[] ordQty         = new String[listBodyCnt];     
    			
    			String[] uomCd          = new String[listBodyCnt];                
    			String[] sdeptCd        = new String[listBodyCnt];         
    			String[] salePerCd      = new String[listBodyCnt];     
    			String[] carCd          = new String[listBodyCnt];     
    			String[] drvNm          = new String[listBodyCnt];     
    			
    			String[] dlvSeq         = new String[listBodyCnt];                
    			String[] drvTel         = new String[listBodyCnt];         
    			String[] custLotNo      = new String[listBodyCnt];     
    			String[] blNo           = new String[listBodyCnt];     
    			String[] recDt          = new String[listBodyCnt];     
    			
    			String[] outWhCd        = new String[listBodyCnt];                
    			String[] inWhCd         = new String[listBodyCnt];         
    			String[] makeDt         = new String[listBodyCnt];     
    			String[] timePeriodDay  = new String[listBodyCnt];     
    			String[] workYn         = new String[listBodyCnt];     
    			
    			String[] rjType         = new String[listBodyCnt];                
    			String[] locYn          = new String[listBodyCnt];         
    			String[] confYn         = new String[listBodyCnt];     
    			String[] eaCapa         = new String[listBodyCnt];     
    			String[] inOrdWeight    = new String[listBodyCnt];     
    			
    			String[] itemCd         = new String[listBodyCnt];                
    			String[] itemNm         = new String[listBodyCnt];         
    			String[] transCustNm    = new String[listBodyCnt];     
    			String[] transCustAddr  = new String[listBodyCnt];     
    			String[] transEmpNm     = new String[listBodyCnt];     
    			
    			String[] remark         = new String[listBodyCnt];                
    			String[] transZipNo     = new String[listBodyCnt];         
    			String[] etc2           = new String[listBodyCnt];     
    			String[] unitAmt        = new String[listBodyCnt];     
    			String[] transBizNo     = new String[listBodyCnt];     
    			
    			String[] inCustAddr     = new String[listBodyCnt];                
    			String[] inCustCd       = new String[listBodyCnt];         
    			String[] inCustNm       = new String[listBodyCnt];     
    			String[] inCustTel      = new String[listBodyCnt];     
    			String[] inCustEmpNm    = new String[listBodyCnt];     
    			
    			String[] expiryDate     = new String[listBodyCnt];
    			String[] salesCustNm    = new String[listBodyCnt];
    			String[] zip     		= new String[listBodyCnt];
    			String[] addr     		= new String[listBodyCnt];
    			String[] phone1    	 	= new String[listBodyCnt];
    			
    			String[] etc1    		= new String[listBodyCnt];
    			String[] unitNo    		= new String[listBodyCnt];               
    			String[] salesCompanyNm	 	= new String[listBodyCnt];
    			String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
    			String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
    			
    			String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
    			String[] locCd     		= new String[listBodyCnt];   //로케이션코드
    			
    			String[] epType     	= new String[listBodyCnt];   //납품유형
    			
    			
//                String[] ordInsDt     	= new String[listBodyCnt];   //주문등록일	2022-03-03
String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
String[] ownerCd     	= new String[listBodyCnt];   //소유자		2022-03-04

for(int i = 0 ; i < listBodyCnt ; i ++){
	String OUT_REQ_DT      = "";
	String IN_REQ_DT       = "";
	String CUST_ORD_NO     = "";
	String CUST_ORD_SEQ    = "";
	String TRUST_CUST_CD   = "";
	String TRANS_CUST_CD   = "";
	String TRANS_CUST_TEL  = "";
	String TRANS_REQ_DT    = "";
	String CUST_CD         = "";
	String ORD_QTY         = "";
	String UOM_CD          = "";
	String SDEPT_CD        = "";
	String SALE_PER_CD     = "";
	String CAR_CD          = "";
	String DRV_NM          = "";
	String DLV_SEQ         = "";
	String DRV_TEL         = "";
	String CUST_LOT_NO     = "";
	String BL_NO           = "";
	String REC_DT          = "";
	String OUT_WH_CD       = "";
	String IN_WH_CD        = "";
	String MAKE_DT         = "";
	String TIME_PERIOD_DAY = "";
	String WORK_YN         = "";
	String RJ_TYPE         = "";
	String LOC_YN          = "";
	String CONF_YN         = "";
	String EA_CAPA         = "";
	String IN_ORD_WEIGHT   = "";
	String ITEM_CD         = "";
	String ITEM_NM         = "";
	String TRANS_CUST_NM   = "";
	String TRANS_CUST_ADDR = "";
	String TRANS_EMP_NM    = "";
	String REMARK          = "";
	String TRANS_ZIP_NO    = "";
	String ETC2            = "";
	String UNIT_AMT        = "";
	String TRANS_BIZ_NO    = "";
	String IN_CUST_ADDR    = "";
	String IN_CUST_CD      = "";
	String IN_CUST_NM      = "";
	String IN_CUST_TEL     = "";
	String IN_CUST_EMP_NM  = "";
	String EXPIRY_DATE     = "";
	String SALES_CUST_NM   = "";
	String ZIP             = "";
	String ADDR            = "";
	String PHONE_1         = "";
	String ETC1            = "";
	String UNIT_NO         = "";
	String SALES_COMPANY_NM   = "";
	String TIME_DATE       = "";
	String TIME_DATE_END   = "";
	String TIME_USE_END    = "";
	String LOC_CD    	   = "";
	String EP_TYPE    	   = "";
	
//        			String ORD_INS_DT         = ""; // 템플릿 추가 sing09  2022-03-03
	String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
	String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04
	
	
	for(int k = 0 ; k < listHeaderCnt ; k++){
		Map<String, Object> object = listBody.get(i);
		String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
		switch (bindColNm) {
		case "OUT_REQ_DT" 			: OUT_REQ_DT        	= replaceStr(object, k, "OUT_REQ_DT"); break;
		case "IN_REQ_DT"			: IN_REQ_DT				= replaceStr(object, k, "IN_REQ_DT"); break;
		case "CUST_ORD_NO"			: CUST_ORD_NO			= replaceStr(object, k, "CUST_ORD_NO"); break;
		case "CUST_ORD_SEQ"			: CUST_ORD_SEQ			= replaceStr(object, k, "CUST_ORD_SEQ"); break;
		case "TRUST_CUST_CD"		: TRUST_CUST_CD			= replaceStr(object, k, "TRUST_CUST_CD"); break;
		case "TRANS_CUST_CD"		: TRANS_CUST_CD			= replaceStr(object, k, "TRANS_CUST_CD"); break;
		case "TRANS_CUST_TEL"		: TRANS_CUST_TEL		= replaceStr(object, k, "TRANS_CUST_TEL"); break;
		case "TRANS_REQ_DT"			: TRANS_REQ_DT			= replaceStr(object, k, "TRANS_REQ_DT"); break;
		case "ORD_QTY"				: ORD_QTY				= replaceStr(object, k, "ORD_QTY"); break;
		case "UOM_CD"				: UOM_CD				= replaceStr(object, k, "UOM_CD"); break;
		case "SDEPT_CD"				: SDEPT_CD				= replaceStr(object, k, "SDEPT_CD"); break;
		case "SALE_PER_CD"			: SALE_PER_CD			= replaceStr(object, k, "SALE_PER_CD"); break;
		case "CAR_CD"				: CAR_CD				= replaceStr(object, k, "CAR_CD"); break;
		case "DRV_NM"				: DRV_NM				= replaceStr(object, k, "DRV_NM"); break;
		case "DLV_SEQ"				: DLV_SEQ				= replaceStr(object, k, "DLV_SEQ"); break;
		case "DRV_TEL"				: DRV_TEL				= replaceStr(object, k, "DRV_TEL"); break;
		case "CUST_LOT_NO"			: CUST_LOT_NO			= replaceStr(object, k, "CUST_LOT_NO"); break;
		case "BL_NO"				: BL_NO					= replaceStr(object, k, "BL_NO"); break;
		case "REC_DT"				: REC_DT				= replaceStr(object, k, "REC_DT"); break;
		case "OUT_WH_CD"			: OUT_WH_CD				= replaceStr(object, k, "OUT_WH_CD"); break;
		case "IN_WH_CD"				: IN_WH_CD				= replaceStr(object, k, "IN_WH_CD"); break;
		case "MAKE_DT"				: MAKE_DT				= replaceStr(object, k, "MAKE_DT"); break;
		case "TIME_PERIOD_DAY"		: TIME_PERIOD_DAY		= replaceStr(object, k, "TIME_PERIOD_DAY"); break;
		case "WORK_YN"				: WORK_YN				= replaceStr(object, k, "WORK_YN"); break;
		case "RJ_TYPE"				: RJ_TYPE				= replaceStr(object, k, "RJ_TYPE"); break;
		case "LOC_YN"				: LOC_YN				= replaceStr(object, k, "LOC_YN"); break;
		case "CONF_YN"				: CONF_YN				= replaceStr(object, k, "CONF_YN"); break;
		case "EA_CAPA"				: EA_CAPA				= replaceStr(object, k, "EA_CAPA"); break;
		case "IN_ORD_WEIGHT"		: IN_ORD_WEIGHT			= replaceStr(object, k, "IN_ORD_WEIGHT"); break;
		case "ITEM_CD"				: ITEM_CD				= replaceStr(object, k, "ITEM_CD"); break;
		case "ITEM_NM"				: ITEM_NM				= replaceStr(object, k, "ITEM_NM"); break;
		case "TRANS_CUST_NM"		: TRANS_CUST_NM			= replaceStr(object, k, "TRANS_CUST_NM"); break;
		case "TRANS_CUST_ADDR"		: TRANS_CUST_ADDR		= replaceStr(object, k, "TRANS_CUST_ADDR"); break;
		case "TRANS_EMP_NM"			: TRANS_EMP_NM			= replaceStr(object, k, "TRANS_EMP_NM"); break;
		case "REMARK"				: REMARK				= replaceStr(object, k, "REMARK"); break;
		case "TRANS_ZIP_NO"			: TRANS_ZIP_NO			= replaceStr(object, k, "TRANS_ZIP_NO"); break;
		case "ETC2"					: ETC2					= replaceStr(object, k, "ETC2"); break;
		case "UNIT_AMT"				: UNIT_AMT				= replaceStr(object, k, "UNIT_AMT"); break;
		case "TRANS_BIZ_NO"			: TRANS_BIZ_NO			= replaceStr(object, k, "TRANS_BIZ_NO"); break;
		case "IN_CUST_ADDR"			: IN_CUST_ADDR			= replaceStr(object, k, "IN_CUST_ADDR"); break;
		case "IN_CUST_CD"			: IN_CUST_CD			= replaceStr(object, k, "IN_CUST_CD"); break;
		case "IN_CUST_NM"			: IN_CUST_NM			= replaceStr(object, k, "IN_CUST_NM"); break;
		case "IN_CUST_TEL"			: IN_CUST_TEL			= replaceStr(object, k, "IN_CUST_TEL"); break;
		case "IN_CUST_EMP_NM"		: IN_CUST_EMP_NM		= replaceStr(object, k, "IN_CUST_EMP_NM"); break;
		case "EXPIRY_DATE"			: EXPIRY_DATE			= replaceStr(object, k, "EXPIRY_DATE"); break;
		case "SALES_CUST_NM"		: SALES_CUST_NM			= replaceStr(object, k, "SALES_CUST_NM"); break;
		case "ZIP"					: ZIP					= replaceStr(object, k, "ZIP"); break;
		case "ADDR"					: ADDR					= replaceStr(object, k, "ADDR"); break;
		case "PHONE_1"				: PHONE_1				= replaceStr(object, k, "PHONE_1"); break;
		case "ETC1"					: ETC1					= replaceStr(object, k, "ETC1"); break;
		case "UNIT_NO"				: UNIT_NO				= replaceStr(object, k, "UNIT_NO"); break;
		case "SALES_COMPANY_NM"		: SALES_COMPANY_NM		= replaceStr(object, k, "SALES_COMPANY_NM"); break;
		case "TIME_DATE"			: TIME_DATE				= replaceStr(object, k, "TIME_DATE"); break;
		case "TIME_DATE_END"		: TIME_DATE_END			= replaceStr(object, k, "TIME_DATE_END"); break;
		case "TIME_USE_END"			: TIME_USE_END			= replaceStr(object, k, "TIME_USE_END"); break;
		case "LOC_CD"				: LOC_CD				= replaceStr(object, k, "LOC_CD"); break;	
		case "EP_TYPE"				: EP_TYPE				= replaceStr(object, k, "EP_TYPE"); break;	
		
//						case "ORD_INS_DT"			: ORD_INS_DT			= replaceStr(object, k, "ORD_INS_DT"); break;	
		case "LOT_TYPE"				: LOT_TYPE				= replaceStr(object, k, "LOT_TYPE"); break;	
		case "OWNER_CD"				: OWNER_CD				= replaceStr(object, k, "OWNER_CD"); break;	
		
		default:
			CUST_CD = custId;
			
			//HACK!! 메가마트는 거래처 1개라 예외로 고정 -> 거래처 추가시 템플릿 변경요청필요.
			boolean isMega = (boolean)model.get("isMega");
			if(isMega){
				TRANS_CUST_CD = "M1";
			}
			// 올리브영은 템플릿이 점코드가 이름과 합쳐진 형태여서 분리 로직 추가.
			boolean isOY = (boolean)model.get("isOY");
			if(isOY){
				String[] spltStr = String.valueOf(object.get("TRANS_CUST_CD")).split(" ");
				
				TRANS_CUST_CD = spltStr[0].replace("[", "").replace("]", "");
			}
			break;
		}
	}
	
	String NO = Integer.toString(i+1);
	no[i]               = NO;
	
	outReqDt[i]         = OUT_REQ_DT.replaceAll("[^\\d]", "");    
	inReqDt[i]          = IN_REQ_DT;    
	custOrdNo[i]        = CUST_ORD_NO;    
	custOrdSeq[i]       = CUST_ORD_SEQ;    
	trustCustCd[i]      = TRUST_CUST_CD;    
	
	transCustCd[i]      = TRANS_CUST_CD;    
	transCustTel[i]     = TRANS_CUST_TEL;    
	transReqDt[i]       = TRANS_REQ_DT;    
	custCd[i]           = CUST_CD;    
	ordQty[i]           = ORD_QTY;    
	
	uomCd[i]            = UOM_CD;    
	sdeptCd[i]          = SDEPT_CD;    
	salePerCd[i]        = SALE_PER_CD;    
	carCd[i]            = CAR_CD;
	drvNm[i]            = DRV_NM;
	
	dlvSeq[i]           = DLV_SEQ;    
	drvTel[i]           = DRV_TEL;    
	custLotNo[i]        = CUST_LOT_NO;    
	blNo[i]             = BL_NO;    
	recDt[i]            = REC_DT;    
	
	outWhCd[i]          = OUT_WH_CD;
	inWhCd[i]           = IN_WH_CD;
	makeDt[i]           = MAKE_DT;
	timePeriodDay[i]    = TIME_PERIOD_DAY;
	workYn[i]           = WORK_YN;
	
	rjType[i]           = RJ_TYPE;    
	locYn[i]            = LOC_YN;    
	confYn[i]           = CONF_YN;    
	eaCapa[i]           = EA_CAPA;    
	inOrdWeight[i]      = IN_ORD_WEIGHT;   
	
	itemCd[i]           = ITEM_CD;    
	itemNm[i]           = ITEM_NM;    
	transCustNm[i]      = TRANS_CUST_NM;    
	transCustAddr[i]    = TRANS_CUST_ADDR;    
	transEmpNm[i]       = TRANS_EMP_NM;    
	
	remark[i]           = REMARK;    
	transZipNo[i]       = TRANS_ZIP_NO;    
	etc2[i]             = ETC2;    
	unitAmt[i]          = UNIT_AMT.replaceAll("[^\\d]", "");    
	transBizNo[i]       = TRANS_BIZ_NO;    
	
	inCustAddr[i]       = IN_CUST_ADDR;   
	inCustCd[i]         = IN_CUST_CD;    
	inCustNm[i]         = IN_CUST_NM;    
	inCustTel[i]        = IN_CUST_TEL;    
	inCustEmpNm[i]      = IN_CUST_EMP_NM;    
	
	expiryDate[i]       = EXPIRY_DATE;
	salesCustNm[i]      = SALES_CUST_NM;
	zip[i]       		= ZIP;
	addr[i]       		= ADDR;
	phone1[i]       	= PHONE_1;
	
	etc1[i]      		= ETC1;
	unitNo[i]      		= UNIT_NO;
	salesCompanyNm[i]   = SALES_COMPANY_NM;
	timeDate[i]         = TIME_DATE;      
	timeDateEnd[i]      = TIME_DATE_END;     
	
	timeUseEnd[i]       = TIME_USE_END;
	locCd[i]       		= LOC_CD;
	epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
	
//                    ordInsDt[i]       	= ORD_INS_DT;	//주문등록일 신규컬럼 추가
	lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
	ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가
	
}

//프로시져에 보낼것들 다담는다
Map<String, Object> modelIns = new HashMap<String, Object>();

modelIns.put("no"  , no);
if(model.get("vrOrdType").equals("I")){
	modelIns.put("reqDt"     	, inReqDt);
	modelIns.put("whCd"      	, inWhCd);
}else{
	modelIns.put("reqDt"     	, outReqDt);
	modelIns.put("whCd"      	, outWhCd);
}
modelIns.put("custOrdNo"    	, custOrdNo);
modelIns.put("custOrdSeq"   	, custOrdSeq);
modelIns.put("trustCustCd"  	, trustCustCd); //5

modelIns.put("transCustCd"  	, transCustCd);
modelIns.put("transCustTel" 	, transCustTel);
modelIns.put("transReqDt"   	, transReqDt);
modelIns.put("custCd"       	, custCd);
modelIns.put("ordQty"       	, ordQty);      //10

modelIns.put("uomCd"        	, uomCd);
modelIns.put("sdeptCd"      	, sdeptCd);
modelIns.put("salePerCd"    	, salePerCd);
modelIns.put("carCd"        	, carCd);
modelIns.put("drvNm"        	, drvNm);       //15

modelIns.put("dlvSeq"       	, dlvSeq);
modelIns.put("drvTel"       	, drvTel);
modelIns.put("custLotNo"    	, custLotNo);
modelIns.put("blNo"         	, blNo);
modelIns.put("recDt"        	, recDt);       //20

modelIns.put("makeDt"       	, makeDt);
modelIns.put("timePeriodDay"	, timePeriodDay);
modelIns.put("workYn"       	, workYn);                
modelIns.put("rjType"       	, rjType);
modelIns.put("locYn"        	, locYn);       //25

modelIns.put("confYn"       	, confYn);     
modelIns.put("eaCapa"       	, eaCapa);
modelIns.put("inOrdWeight"  	, inOrdWeight); //28
modelIns.put("itemCd"           , itemCd);
modelIns.put("itemNm"           , itemNm);

modelIns.put("transCustNm"      , transCustNm);
modelIns.put("transCustAddr"    , transCustAddr);
modelIns.put("transEmpNm"       , transEmpNm);
modelIns.put("remark"           , remark);
modelIns.put("transZipNo"       , transZipNo);

modelIns.put("etc2"             , etc2);
modelIns.put("unitAmt"          , unitAmt);
modelIns.put("transBizNo"       , transBizNo);
modelIns.put("inCustAddr"       , inCustAddr);
modelIns.put("inCustCd"         , inCustCd);

modelIns.put("inCustNm"         , inCustNm);                 
modelIns.put("inCustTel"        , inCustTel);
modelIns.put("inCustEmpNm"      , inCustEmpNm);      
modelIns.put("expiryDate"       , expiryDate);
modelIns.put("salesCustNm"      , salesCustNm);

modelIns.put("zip"       		, zip);
modelIns.put("addr"       		, addr);
modelIns.put("phone1"       	, phone1);
modelIns.put("etc1"     	 	, etc1);
modelIns.put("unitNo"     	 	, unitNo);

modelIns.put("salesCompanyNm"    	, salesCompanyNm);

modelIns.put("time_date"        , timeDate);
modelIns.put("time_date_end"    , timeDateEnd);                
modelIns.put("time_use_end"     , timeUseEnd);  
modelIns.put("locCd"     		, locCd);  
modelIns.put("epType"     		, epType);  

//                modelIns.put("ordInsDt"     	, ordInsDt);  
modelIns.put("lotType"     		, lotType);  
modelIns.put("ownerCd"     		, ownerCd);  

modelIns.put("vrOrdType"		, model.get("vrOrdType"));
modelIns.put("vrOrdSubType"		, model.get("vrOrdSubType"));

modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

//dao                
modelIns = (Map<String, Object>)dao.saveExcelOrder_AS(modelIns);

ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
m.put("O_CUR", modelIns.get("O_CUR"));                
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }


	public String replaceStr(Map<String, Object> obj, int idx, String bindColNm){
    	String strResult = String.valueOf(obj.get("S_" + idx));
    	
    	if(strResult.equals("null")){
    		strResult = String.valueOf(obj.get(bindColNm));
    		if(strResult.equals("null")){
    			strResult = "";
    		}
    	}
    	
    	return strResult;
    }
    
    
	/**
     * 
     * 대체 Method ID    : saveExcelOrderJavaB2T_DH
     * 대체 Method 설명   : B2C 템플릿 업로드 (대화물류 전용)
     * 작성자             : kimzero
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaB2T_DH(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
		List<Map<String, Object>> listBody = (ArrayList)model.get("LIST");
		String custId = String.valueOf(model.get("vrCustCd"));
		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	Date today = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String vrSrchOrdDegreeDate = (String)model.get("vrSrchOrdDegreeDate");
    	String formatted_today = sdf.format(today);
    	if(vrSrchOrdDegreeDate != null && !vrSrchOrdDegreeDate.equals("")){
    	    formatted_today = vrSrchOrdDegreeDate;
    	}
    	
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm	= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
                String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
//                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
                
                String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
                String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
                String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
                String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
                
//                String[] lotType     		= new String[listBodyCnt];   //LOT속성 	2022-03-04
//                String[] ownerCd     		= new String[listBodyCnt];   //소유자	2022-03-04

                
                /* 컬럼 바인딩 제약조건 참조 flag */
                String breakYnOrdDegree = "N";
                
                String beforeCustOrdNo = "";
                int beforeCustOrdSeq = 0;
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= formatted_today;//출고 예정일 -> 업로드 날짜
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			String DATA_SENDER_NM = "";
        			String LEGACY_ORG_ORD_NO ="";
//        			String INVOICE_NO ="";
        			String CUST_SEQ = "";
        			String ORD_DESC = "";
        			String DLV_MSG1 = "";
        			String DLV_MSG2 = "";
        			
//        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
//        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				Map<String, Object> object = listBody.get(i);
        				String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
        				
        				switch (bindColNm) {
        				//case "OUT_REQ_DT"             :  OUT_REQ_DT = replaceStr(object, k, "OUT_REQ_DT"); break;
        				case "IN_REQ_DT"              :  IN_REQ_DT = replaceStr(object, k, "IN_REQ_DT"); break;
        				case "CUST_ORD_NO"            :  CUST_ORD_NO = replaceStr(object, k, "CUST_ORD_NO"); break;
        				case "CUST_ORD_SEQ"           :  CUST_ORD_SEQ = replaceStr(object, k, "CUST_ORD_SEQ"); break;
        				case "TRUST_CUST_CD"          :  TRUST_CUST_CD = replaceStr(object, k, "TRUST_CUST_CD"); break;
        				case "TRANS_CUST_CD"          :  TRANS_CUST_CD = replaceStr(object, k, "TRANS_CUST_CD"); break;
        				case "TRANS_CUST_TEL"         :  TRANS_CUST_TEL = replaceStr(object, k, "TRANS_CUST_TEL"); break;
        				case "TRANS_REQ_DT"           :  TRANS_REQ_DT = replaceStr(object, k, "TRANS_REQ_DT"); break;
        				
                        case "ORD_QTY"                :  ORD_QTY = replaceStr(object, k, "ORD_QTY"); break;
        				case "UOM_CD"                 :  UOM_CD = replaceStr(object, k, "UOM_CD"); break;
        				case "SDEPT_CD"               :  SDEPT_CD = replaceStr(object, k, "SDEPT_CD"); break;
        				case "SALE_PER_CD"            :  SALE_PER_CD = replaceStr(object, k, "SALE_PER_CD"); break;
        				case "CAR_CD"                 :  CAR_CD = replaceStr(object, k, "CAR_CD"); break;
        				case "DRV_NM"                 :  DRV_NM = replaceStr(object, k, "DRV_NM"); break;
        				case "DLV_SEQ"                :  DLV_SEQ = replaceStr(object, k, "DLV_SEQ"); break;
        				case "DRV_TEL"                :  DRV_TEL = replaceStr(object, k, "DRV_TEL"); break;
        				case "CUST_LOT_NO"            :  CUST_LOT_NO = replaceStr(object, k, "CUST_LOT_NO"); break;
        				case "BL_NO"                  :  BL_NO = replaceStr(object, k, "BL_NO"); break;
        				case "REC_DT"                 :  REC_DT = replaceStr(object, k, "REC_DT"); break;
        				case "OUT_WH_CD"              :  OUT_WH_CD = replaceStr(object, k, "OUT_WH_CD"); break;
        				case "IN_WH_CD"               :  IN_WH_CD = replaceStr(object, k, "IN_WH_CD"); break;
        				case "MAKE_DT"                :  MAKE_DT = replaceStr(object, k, "MAKE_DT"); break;
        				case "TIME_PERIOD_DAY"        :  TIME_PERIOD_DAY = replaceStr(object, k, "TIME_PERIOD_DAY"); break;
        				case "WORK_YN"                :  WORK_YN = replaceStr(object, k, "WORK_YN"); break;
        				case "RJ_TYPE"                :  RJ_TYPE = replaceStr(object, k, "RJ_TYPE"); break;
        				case "LOC_YN"                 :  LOC_YN = replaceStr(object, k, "LOC_YN"); break;
        				case "CONF_YN"                :  CONF_YN = replaceStr(object, k, "CONF_YN"); break;
        				case "EA_CAPA"                :  EA_CAPA = replaceStr(object, k, "EA_CAPA"); break;
        				case "IN_ORD_WEIGHT"          :  IN_ORD_WEIGHT = replaceStr(object, k, "IN_ORD_WEIGHT"); break;
        				case "ITEM_CD"                :  ITEM_CD = replaceStr(object, k, "ITEM_CD"); break;
        				case "ITEM_NM"                :  ITEM_NM = replaceStr(object, k, "ITEM_NM"); break;
        				case "TRANS_CUST_NM"          :  TRANS_CUST_NM = replaceStr(object, k, "TRANS_CUST_NM"); break;
        				case "TRANS_CUST_ADDR"        :  TRANS_CUST_ADDR = replaceStr(object, k, "TRANS_CUST_ADDR"); break;
        				case "TRANS_EMP_NM"           :  TRANS_EMP_NM = replaceStr(object, k, "TRANS_EMP_NM"); break;
        				case "REMARK"                 :  REMARK = replaceStr(object, k, "REMARK"); break;
        				case "TRANS_ZIP_NO"           :  TRANS_ZIP_NO = replaceStr(object, k, "TRANS_ZIP_NO"); break;
        				case "ETC2"                   :  ETC2 = replaceStr(object, k, "ETC2"); break;
        				case "UNIT_AMT"               :  UNIT_AMT = replaceStr(object, k, "UNIT_AMT"); break;
        				case "TRANS_BIZ_NO"           :  TRANS_BIZ_NO = replaceStr(object, k, "TRANS_BIZ_NO"); break;
        				case "IN_CUST_ADDR"           :  IN_CUST_ADDR = replaceStr(object, k, "IN_CUST_ADDR"); break;
        				case "IN_CUST_CD"             :  IN_CUST_CD = replaceStr(object, k, "IN_CUST_CD"); break;
        				case "IN_CUST_NM"             :  IN_CUST_NM = replaceStr(object, k, "IN_CUST_NM"); break;
        				case "IN_CUST_TEL"            :  IN_CUST_TEL = replaceStr(object, k, "IN_CUST_TEL"); break;
        				case "IN_CUST_EMP_NM"         :  IN_CUST_EMP_NM = replaceStr(object, k, "IN_CUST_EMP_NM"); break;
        				case "EXPIRY_DATE"            :  EXPIRY_DATE = replaceStr(object, k, "EXPIRY_DATE"); break;
        				case "SALES_CUST_NM"          :  SALES_CUST_NM = replaceStr(object, k, "SALES_CUST_NM"); break;
        				case "ZIP"                    :  ZIP = replaceStr(object, k, "ZIP"); break;
        				case "ADDR"                   :  ADDR = replaceStr(object, k, "ADDR"); break;
        				case "ADDR2"                  :  ADDR = replaceStr(object, k, "ADDR2"); break;
        				case "PHONE_1"                :  PHONE_1 = replaceStr(object, k, "PHONE_1"); break;
        				case "ETC1"                   :  ETC1 = replaceStr(object, k, "ETC1"); break;
        				case "UNIT_NO"                :  UNIT_NO = replaceStr(object, k, "UNIT_NO"); break;
        				case "TIME_DATE"              :  TIME_DATE = replaceStr(object, k, "TIME_DATE"); break;
        				case "TIME_DATE_END"          :  TIME_DATE_END = replaceStr(object, k, "TIME_DATE_END"); break;
        				case "TIME_USE_END"           :  TIME_USE_END = replaceStr(object, k, "TIME_USE_END"); break;
        				case "PHONE_2"                :  PHONE_2 = replaceStr(object, k, "PHONE_2"); break;
        				case "BUY_CUST_NM"            :  BUY_CUST_NM = replaceStr(object, k, "BUY_CUST_NM"); break;
        				case "BUY_PHONE_1"            :  BUY_PHONE_1 = replaceStr(object, k, "BUY_PHONE_1"); break;
        				
                        case "BIZ_COND"               :  BIZ_COND = replaceStr(object, k, "BIZ_COND"); break;
        				case "BIZ_TYPE"               :  BIZ_TYPE = replaceStr(object, k, "BIZ_TYPE"); break;
        				case "BIZ_NO"                 :  BIZ_NO = replaceStr(object, k, "BIZ_NO"); break;
        				case "CUST_TYPE"              :  CUST_TYPE = replaceStr(object, k, "CUST_TYPE"); break;
        				
        				case "DATA_SENDER_NM"         :  DATA_SENDER_NM = replaceStr(object, k, "DATA_SENDER_NM"); break;
        				case "LEGACY_ORG_ORD_NO"      :  LEGACY_ORG_ORD_NO = replaceStr(object, k, "LEGACY_ORG_ORD_NO"); break;
        				case "ORD_DESC"               :  ORD_DESC = replaceStr(object, k, "ORD_DESC"); break;
        				case "DLV_MSG1"               :  DLV_MSG1 = replaceStr(object, k, "DLV_MSG1"); break;
        				case "DLV_MSG2"               :  DLV_MSG2 = replaceStr(object, k, "DLV_MSG2"); break;
        				
//        				case "LOT_TYPE"               :  LOT_TYPE = replaceStr(object, k, "LOT_TYPE"); break;
//        				case "OWNER_CD"               :  OWNER_CD = replaceStr(object, k, "OWNER_CD"); break;
        				
        				case "ORD_DEGREE"             :  ORD_DEGREE = replaceStr(object, k, "ORD_DEGREE");

						default:
							CUST_CD = (String)model.get("vrSrchCustCd");
							SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
							CUST_SEQ= (String)model.get("vrCustSeq");
							/* 엑셀에 차수 컬럼 필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩)*/
							if(ORD_DEGREE.equals("")){
								ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
							}
							
							break;
						}
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;
                    
                    if(!beforeCustOrdNo.equals(CUST_ORD_NO)){
                        beforeCustOrdNo = CUST_ORD_NO;
                        if(CUST_ORD_SEQ.equals("")){
                            beforeCustOrdSeq = 1;
                            custOrdSeq[i]    = "1";
                        }
                        else{
                            beforeCustOrdSeq = Integer.parseInt(CUST_ORD_SEQ);
                            custOrdSeq[i]       = CUST_ORD_SEQ;
                        }
                    }
                    else{
                        if(CUST_ORD_SEQ.equals("")){
                            beforeCustOrdSeq++;
                            custOrdSeq[i]    = Integer.toString(beforeCustOrdSeq);
                        }
                        else{
                            beforeCustOrdSeq = Integer.parseInt(CUST_ORD_SEQ);
                            custOrdSeq[i]       = CUST_ORD_SEQ;
                        }
                        
                    }
                    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                    
                    dataSenderNm[i]       		= DATA_SENDER_NM;
                    legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
//                    invoiceNo[i]				= INVOICE_NO;
                    
                    custSeq[i]					= CUST_SEQ;
                    
                    ordDesc[i]     			  	= ORD_DESC;
                    dlvMsg1[i]       			= DLV_MSG1;
                    dlvMsg2[i]       			= DLV_MSG2;
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
               
                
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("vrOrdSubtype"		, model.get("vrOrdSubtype"));
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"	, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"		    , bizCond);
                modelIns.put("bizType"		    , bizType);
                modelIns.put("bizNo"		    , bizNo);
                modelIns.put("custType"		    , custType);
                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                modelIns.put("invoiceNo"		, invoiceNo);
                modelIns.put("custSeq"       	, custSeq);
                
                modelIns.put("ordDesc"       	, ordDesc);
                modelIns.put("dlvMsg1"       	, dlvMsg1);
                modelIns.put("dlvMsg2"       	, dlvMsg2);
                
//                modelIns.put("lotType"     		, lotType);  
//                modelIns.put("ownerCd"     		, ownerCd);  
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2TS(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                
                if(model.get("vrSrchOrdDegreeNm") != null && !model.get("vrSrchOrdDegreeNm").equals("")){
                    dao.updateOrderDegreeNm(model);
                }
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e) {
           throw e;
        }
        /*
        catch(Exception e){
        	m.put("errCnt", -1);
        	m.put("MSG", e.getMessage());
            m.put("MSG_ORA", e.getMessage());
        }
        */
        return m;
    } 
    
    
    
    
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJavaB2C
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaB2C(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo		= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= "";
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_CD = (String)model.get("vrSrchCustCd");
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR2")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_2")){PHONE_2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_CUST_NM")){BUY_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_PHONE_1")){BUY_PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DEGREE")){ORD_DEGREE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_COND")){BIZ_COND = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_TYPE")){BIZ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_NO")){BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_TYPE")){CUST_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]       	= SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"		, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"			, bizCond);
                modelIns.put("bizType"			, bizType);
                modelIns.put("bizNo"			, bizNo);
                modelIns.put("custType"		, custType);
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

                // 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2C(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG_ORA", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJavaB2C
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaB2D(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo		= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= "";
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_CD = (String)model.get("vrSrchCustCd");
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR2")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_2")){PHONE_2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_CUST_NM")){BUY_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_PHONE_1")){BUY_PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DEGREE")){ORD_DEGREE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_COND")){BIZ_COND = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_TYPE")){BIZ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_NO")){BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_TYPE")){CUST_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]       	= SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"		, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"		, bizCond);
                modelIns.put("bizType"		, bizType);
                modelIns.put("bizNo"		, bizNo);
                modelIns.put("custType"		, custType);
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

                // 올푸드 dao
            	 modelIns = (Map<String, Object>)dao.saveExcelOrderB2D(modelIns); // 아직 메소드명 안정해짐
                
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG_ORA", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : autoDeleteLocSave
     * 대체 Method 설명    : 로케이션지정 삭제
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoDeleteLocSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                String[] ordSeq   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);  
                	ordSeq[i]    = (String)model.get("I_ORD_SEQ"+i);  
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao                
                modelIns = (Map<String, Object>)dao.autoDeleteLocSave(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	     * 
	     * 대체 Method ID   : autoBestLocSaveMulti
	     * 대체 Method 설명    : 아산물류센터 로케이션추천 자동
	     * 작성자                      : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
		 @Override
	    public Map<String, Object> autoBestLocSaveMulti(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{

	            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	            if(tmpCnt > 0){           
	                String[] ordId   = new String[tmpCnt];
	                String[] ordSeq  = new String[tmpCnt];
	                
	                for(int i = 0 ; i < tmpCnt ; i ++){
	                	ordId[i]    = (String)model.get("I_ORD_ID"+i);
	                	ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
	                }
	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                modelIns.put("ordId" , ordId);
	                modelIns.put("ordSeq", ordSeq);

	                //session 및 등록정보
	                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	                //dao                
	                modelIns = (Map<String, Object>)dao.autoBestLocSaveMulti(modelIns);
	                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
		 /**
		  * 
		  * 대체 Method ID   : autoLocMulti
		  * 대체 Method 설명    : 대화물류 > 로케이션지정(부분할당)
		  * 작성자                 : kimzero
		  * @param model
		  * @return
		  * @throws Exception
		  */
		 @Override
		 public Map<String, Object> autoLocMulti(Map<String, Object> model) throws Exception {
			 Map<String, Object> m = new HashMap<String, Object>();
			 
			 try{
				 
				 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
				 if(tmpCnt > 0){           
					 String[] ordId   = new String[tmpCnt];
					 String[] ordSeq  = new String[tmpCnt];
					 
					 for(int i = 0 ; i < tmpCnt ; i ++){
						 ordId[i]    = (String)model.get("I_ORD_ID"+i);
						 ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
					 }
					 //프로시져에 보낼것들 다담는다
					 Map<String, Object> modelIns = new HashMap<String, Object>();
					 
					 modelIns.put("ordId" , ordId);
					 modelIns.put("ordSeq", ordSeq);
					 
					 //session 및 등록정보
					 modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
					 modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
					 modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
					 
					 //dao                
					 modelIns = (Map<String, Object>)dao.autoLocMulti(modelIns);
					 ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
				 }
				 m.put("errCnt", 0);
				 m.put("MSG", MessageResolver.getMessage("save.success"));
				 
			 } catch(BizException be) {
				 m.put("errCnt", -1);
				 m.put("MSG", be.getMessage() );
				 
			 } catch(Exception e){
				 throw e;
			 }
			 return m;
		 }
	
		 
   /**
     * 
     * 대체 Method ID   : autoBestLocSaveForDPS
     * 대체 Method 설명    : DPS 로케이션추천 자동
     * 작성자                    : dhkim
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoBestLocSaveForDPS(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                String[] ordSeq  = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);
                	ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId" , ordId);
                modelIns.put("ordSeq", ordSeq);

                modelIns.put("DPS_USE_YN", (String)model.get("DPS_YN"));
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.autoBestLocSaveForDPS(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
	 
    /**
    * 
    * 대체 Method ID   : changeOrderToDAS
    * 대체 Method 설명    : DAS 주문변경
    * 작성자                    : schan
    * @param model
    * @return
    * @throws Exception
    */
    @Override
    public Map<String, Object> changeOrderToDAS(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                //String[] ordSeq  = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("I_ORD_ID"+i);
                    //ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId" , ordId);
                //modelIns.put("ordSeq", ordSeq);
                
                //modelIns.put("DPS_USE_YN", (String)model.get("DPS_YN"));
                
                //session 및 등록정보
                modelIns.put("I_CUST_ID", (String)model.get("I_CUST_ID"));
                modelIns.put("I_OUT_REQ_DT", (String)model.get("I_OUT_REQ_DT"));
                modelIns.put("I_LC_ID", (String)model.get("I_LC_ID"));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao                
                modelIns = (Map<String, Object>)dao.changeOrderToDAS(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                m.put("MSG", (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : changeOrderToDASCommon
     * 대체 Method 설명    : DAS 주문변경
     * 작성자                    : schan
     * @param model
     * @return
     * @throws Exception
     */
     @Override
     public Map<String, Object> changeOrderToDASCommon(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         try{
             int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){           
                 String[] ordId   = new String[tmpCnt];
                 //String[] ordSeq  = new String[tmpCnt];
                 
                 for(int i = 0 ; i < tmpCnt ; i ++){
                     ordId[i]    = (String)model.get("I_ORD_ID"+i);
                     //ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
                 }
                 //프로시져에 보낼것들 다담는다
                 Map<String, Object> modelIns = new HashMap<String, Object>();
                 
                 modelIns.put("ordId" , ordId);
                 //modelIns.put("ordSeq", ordSeq);
                 
                 //modelIns.put("DPS_USE_YN", (String)model.get("DPS_YN"));
                 
                 //session 및 등록정보
                 modelIns.put("I_CUST_ID", (String)model.get("I_CUST_ID"));
                 modelIns.put("I_OUT_REQ_DT", (String)model.get("I_OUT_REQ_DT"));
                 modelIns.put("I_LC_ID", (String)model.get("I_LC_ID"));
                 modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                 modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                 
                 //dao                
                 modelIns = (Map<String, Object>)dao.changeOrderToDASCommon(modelIns);
                 ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                 m.put("MSG", (String)modelIns.get("O_MSG_NAME"));
             }
             m.put("errCnt", 0);
             
         } catch(BizException be) {
             m.put("errCnt", -1);
             m.put("MSG", be.getMessage() );
             
         } catch(Exception e){
             throw e;
         }
         return m;
     }
    /**
    * 
    * 대체 Method ID   : cancelOrderToDAS
    * 대체 Method 설명    : DAS 주문변경 취소
    * 작성자                    : schan
    * @param model
    * @return
    * @throws Exception
    */
    @Override
    public Map<String, Object> cancelOrderToDAS(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                //String[] ordSeq  = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("I_ORD_ID"+i);
                    //ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId" , ordId);
                //modelIns.put("ordSeq", ordSeq);
                
                //modelIns.put("DPS_USE_YN", (String)model.get("DPS_YN"));
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao                
                modelIns = (Map<String, Object>)dao.cancelOrderToDAS(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveOrderToDAS
     * 대체 Method 설명    : DAS 주문 삽입
     * 작성자                    : schan
     * @param model
     * @return
     * @throws Exception
     */
     @Override
     public Map<String, Object> saveOrderToDAS(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         try{
             int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
             if(tmpCnt > 0){           
                 String[] ordId   = new String[tmpCnt];
                 //String[] ordSeq  = new String[tmpCnt];
                 
                 for(int i = 0 ; i < tmpCnt ; i ++){
                     ordId[i]    = (String)model.get("I_ORD_ID"+i);
                     //ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
                 }
                 //프로시져에 보낼것들 다담는다
                 Map<String, Object> modelIns = new HashMap<String, Object>();
                 
                 modelIns.put("ordId" , ordId);
                 //modelIns.put("ordSeq", ordSeq);
                 
                 //modelIns.put("DPS_USE_YN", (String)model.get("DPS_YN"));
                 
                 //session 및 등록정보
                 modelIns.put("I_CUST_ID", (String)model.get("I_CUST_ID"));
                 modelIns.put("I_OUT_REQ_DT", (String)model.get("I_OUT_REQ_DT"));
                 modelIns.put("I_LC_ID", (String)model.get("I_LC_ID"));
                 modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                 modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                 
                 //dao                
                 modelIns = (Map<String, Object>)dao.saveOrderToDAS(modelIns);
                 ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                 m.put("MSG", (String)modelIns.get("O_MSG_NAME"));
             }
             m.put("errCnt", 0);
             
         } catch(BizException be) {
             m.put("errCnt", -1);
             m.put("MSG", be.getMessage() );
             
         } catch(Exception e){
             throw e;
         }
         return m;
     }
     
     /**
      * 
      * 대체 Method ID   : deleteOrderToDAS
      * 대체 Method 설명    : DAS 주문 삭제
      * 작성자                    : schan
      * @param model
      * @return
      * @throws Exception
      */
      @Override
      public Map<String, Object> deleteOrderToDAS(Map<String, Object> model) throws Exception {
          Map<String, Object> m = new HashMap<String, Object>();
          try{
              int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
              if(tmpCnt > 0){           
                  String[] ordId   = new String[tmpCnt];
                  //String[] ordSeq  = new String[tmpCnt];
                  
                  for(int i = 0 ; i < tmpCnt ; i ++){
                      ordId[i]    = (String)model.get("I_ORD_ID"+i);
                      //ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
                  }
                  //프로시져에 보낼것들 다담는다
                  Map<String, Object> modelIns = new HashMap<String, Object>();
                  
                  modelIns.put("ordId" , ordId);
                  //modelIns.put("ordSeq", ordSeq);
                  
                  //modelIns.put("DPS_USE_YN", (String)model.get("DPS_YN"));
                  
                  //session 및 등록정보
                  modelIns.put("I_CUST_ID", (String)model.get("I_CUST_ID"));
                  modelIns.put("I_LC_ID", (String)model.get("I_LC_ID"));
                  modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                  modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                  
                  //dao                
                  modelIns = (Map<String, Object>)dao.deleteOrderToDAS(modelIns);
                  ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                  m.put("MSG", (String)modelIns.get("O_MSG_NAME"));
              }
              m.put("errCnt", 0);
              
          } catch(BizException be) {
              m.put("errCnt", -1);
              m.put("MSG", be.getMessage() );
              
          } catch(Exception e){
              throw e;
          }
          return m;
      }
 
		 
  	/*-
  	 * Method ID	: bigDataLoc
  	 * Method 설명 	: 로케이션추천지정 (빅데이터)
  	 * 작성자                 	: dhkim
  	 * @param   model
  	 * @return  
  	 */
	 @Override
    public Map<String, Object> bigDataLoc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                String[] outReqDt   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("I_ORD_ID"+i);
                    outReqDt[i] = (String)model.get("I_OUT_REQ_DT"+i); 
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("I_ORD_ID" , ordId);
                modelIns.put("I_OUT_REQ_DT", outReqDt);               

                //session 및 등록정보
                modelIns.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_CUST_ID" , model.get("custId"));
                modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.bigDataLoc(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /*-
	     * Method ID    : autoLocSimple
	     * Method 설명    : 로케이션추천지정 (대용량)
	     * 작성자                  : 
	     * @param   model
	     * @return  
	     */
	     @Override
	    public Map<String, Object> autoLocSimple(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
	            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	            if(tmpCnt > 0){           
	                String[] ordId   = new String[tmpCnt];
	                String[] ordSeq   = new String[tmpCnt];
	                
	                for(int i = 0 ; i < tmpCnt ; i ++){
	                    ordId[i]    = (String)model.get("I_ORD_ID"+i);
	                    ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i); 
	                }
	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                modelIns.put("I_ORD_ID" , ordId);
	                modelIns.put("I_ORD_SEQ", ordSeq);               

	                //session 및 등록정보
	                modelIns.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put("I_CUST_ID" , model.get("custId"));
	                modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	                //dao                
	                modelIns = (Map<String, Object>)dao.autoLocSimple(modelIns);
	                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
	 
	 
	 /**
     * 
     * 대체 Method ID   : autoBestLocSaveMultiV2
     * 대체 Method 설명    : 아산물류센터 로케이션추천 자동V2
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoBestLocSaveMultiV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId" , ordId);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.autoBestLocSaveMultiV2(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	     * 
	     * 대체 Method ID   : autoBestLocSaveMultiPart
	     * 대체 Method 설명    : 로케이션추천 부분재고지정
	     * 작성자                      : schan
	     * @param model
	     * @return
	     * @throws Exception
	     */
		 @Override
	    public Map<String, Object> autoBestLocSaveMultiPart(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
		        int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
		        if(tmpCnt > 0){
		            String[] ordId  = new String[tmpCnt];                
		            String[] ordSeq = new String[tmpCnt];
		            
		            for(int i = 0 ; i < tmpCnt ; i ++){
		                ordId[i]    = (String)model.get("I_ORD_ID"+i);               
		                ordSeq[i]   = (String)model.get("I_ORD_SEQ"+i);
		            }
		            
		            //프로시져에 보낼것들 다담는다
		            Map<String, Object> modelIns = new HashMap<String, Object>();
		            
		            modelIns.put("ordId", ordId);
		            modelIns.put("ordSeq", ordSeq);
		            
		            //session 및 등록정보
		            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
		            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		
		            //dao                
		            modelIns = (Map<String, Object>)dao.autoBestLocSaveMultiPart(modelIns);
		            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
		        }
		        m.put("errCnt", 0);
				m.put("MSG", MessageResolver.getMessage("save.success"));
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
	 
	 /**
	  * 
	  * 대체 Method ID   : autoLocMulti
	  * 대체 Method 설명    : 대화물류 > 로케이션지정(일괄할당)
	  * 작성자                 : kimzero
	  * @param model
	  * @return
	  * @throws Exception
	  */
	 @Override
	 public Map<String, Object> autoLocOrdMulti(Map<String, Object> model) throws Exception {
		 Map<String, Object> m = new HashMap<String, Object>();
		 
		 try{
			 
			 int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
			 if(tmpCnt > 0){           
				 String[] ordId   = new String[tmpCnt];
				 
				 for(int i = 0 ; i < tmpCnt ; i ++){
					 ordId[i]    = (String)model.get("I_ORD_ID"+i);
				 }
				 //프로시져에 보낼것들 다담는다
				 Map<String, Object> modelIns = new HashMap<String, Object>();
				 
				 modelIns.put("ordId" , ordId);
				 
				 //session 및 등록정보
				 modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
				 modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				 modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				 
				 //dao                
				 modelIns = (Map<String, Object>)dao.autoLocOrdMulti(modelIns);
				 ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
			 }
			 m.put("errCnt", 0);
			 m.put("MSG", MessageResolver.getMessage("save.success"));
			 
		 } catch(BizException be) {
			 m.put("errCnt", -1);
			 m.put("MSG", be.getMessage() );
			 
		 } catch(Exception e){
			 throw e;
		 }
		 return m;
	 }

	 /**
     * 
     * Method ID   : listExcelB2C
     * Method 설명    : 입출고현황 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcelB2C(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listExcelB2C(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : saveCjDataInsert
     * 대체 Method 설명    : 할당처리,취소
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveCjDataInsert(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
            int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
            	String[] dlvCompCd	= new String[temCnt];
            	String[] ordId		= new String[temCnt];
                String[] ordSeq		= new String[temCnt];
                String[] custId		= new String[temCnt];
                String[] mpackGb	= new String[temCnt];
                String[] mpackQty	= new String[temCnt];
                String[] boxTypeCd	= new String[temCnt];
                
                String[] salesCustNm	= new String[temCnt];
                String[] phone1			= new String[temCnt];
                String[] addr			= new String[temCnt];
                String[] outOrdQty		= new String[temCnt];
                String[] tranOrdQty		= new String[temCnt];
                
                for(int i = 0 ; i < temCnt ; i ++){
                	dlvCompCd[i]	= (String)model.get("DLV_COMP_CD"+i);
                	ordId[i]		= (String)model.get("ORD_ID"+i);
                    ordSeq[i]		= (String)model.get("ORD_SEQ"+i);
                    custId[i]		= (String)model.get("CUST_ID"+i);
                    mpackGb[i]		= (String)model.get("MPACK_GB"+i);
                    mpackQty[i]		= (String)model.get("MPACK_QTY"+i);
                    boxTypeCd[i]	= (String)model.get("BOX_TYPE_CD"+i);
                    
                    salesCustNm[i]	= (String)model.get("SALES_CUST_NM"+i);
                    phone1[i]		= (String)model.get("PHONE_1"+i);
                    addr[i]			= (String)model.get("ADDR"+i);
                    outOrdQty[i]	= (String)model.get("OUT_ORD_QTY"+i);
                    tranOrdQty[i]	= (String)model.get("TRAN_ORD_QTY"+i);
                }
                
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("I_DLV_COMP_CD"		, dlvCompCd);
                modelDt.put("I_ORD_ID"			, ordId);
                modelDt.put("I_ORD_SEQ"			, ordSeq);
                modelDt.put("I_CUST_ID"			, custId);
                modelDt.put("I_MPACK_GB"		, mpackGb);
                modelDt.put("I_MPACK_QTY"		, mpackQty);
                modelDt.put("I_BOX_TYPE_CD"		, boxTypeCd);
                
                modelDt.put("I_SALES_CUST_NM"	, salesCustNm);
                modelDt.put("I_PHONE_1"			, phone1);
                modelDt.put("I_ADDR"			, addr);
                modelDt.put("I_OUT_ORD_QTY"		, outOrdQty);
                modelDt.put("I_TRAN_ORD_QTY"	, tranOrdQty);

                //modelDt.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("I_LC_ID"		, (String)model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                
                modelDt = (Map<String, Object>)dao.saveCjDataInsert(modelDt);
                ServiceUtil.isValidReturnCode("WNSOP030", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));                                
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            

        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   : listByCustCJ_setParam
     * Method 설명    : 
     * 작성자               : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustCJ_setParam(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listByCustCJ_setParam(model));
        return map;
    }
    
    /**
     * Method ID   : listByCustCJ_ordInvcNoConf
     * Method 설명    : 
     * 작성자               : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustCJ_ordInvcNoConf(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listByCustCJ_ordInvcNoConf(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : cjConfListInsertToWMSDF020
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> cjConfListInsertToWMSDF020(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	//json Parser
        	Gson gson 			= new Gson();
			String jsonString	= gson.toJson(model);
			String sendData	= new StringBuffer().append(jsonString).toString();
			
			JsonParser Parser			= new JsonParser();
			JsonObject jsonObj		= (JsonObject) Parser.parse(sendData);
			JsonObject jsonObjList	= (JsonObject) jsonObj.get("LIST");
			JsonArray dataSetArr	= (JsonArray) jsonObjList.get("list");
            
			//data Size
			int tmpCnt = dataSetArr.size();
			if(tmpCnt > 0){
				//return param 초기화
	            //String[] parcelId	= new String[tmpCnt];
	            //String[] parcelse	= new String[tmpCnt];
	            
	            //package param 초기화
	            String[] custId 			= new String[tmpCnt];
	            String[] rcptYmd 			= new String[tmpCnt];
	            String[] custUseNo			= new String[tmpCnt];
	            String[] rcptDv 			= new String[tmpCnt];
	            String[] workDvCd 			= new String[tmpCnt];
	            
	            String[] reqDvCd 			= new String[tmpCnt];
	            String[] mpckKey 			= new String[tmpCnt];
	            String[] mpckSeq 			= new String[tmpCnt];
	            String[] calDvCd 			= new String[tmpCnt];
	            String[] frtDvCd 			= new String[tmpCnt];
	            
	            String[] cntrItemCd 		= new String[tmpCnt];
	            String[] boxTypeCd 			= new String[tmpCnt];
	            String[] boxQty 			= new String[tmpCnt];
	            String[] custMgmtDlcmCd 	= new String[tmpCnt];
	            String[] sendrNm 			= new String[tmpCnt];
	            
	            String[] sendrTelNo1 		= new String[tmpCnt];
	            String[] sendrTelNo2 		= new String[tmpCnt];
	            String[] sendrTelNo3 		= new String[tmpCnt];
	            String[] sendrZipNo 		= new String[tmpCnt];
	            String[] sendrAddr 			= new String[tmpCnt];
	            
	            String[] sendrDetailAddr	= new String[tmpCnt];
	            String[] rcvrNm 			= new String[tmpCnt];
	            String[] rcvrTelNo1 		= new String[tmpCnt];
	            String[] rcvrTelNo2 		= new String[tmpCnt];
	            String[] rcvrTelNo3 		= new String[tmpCnt];
	            
	            String[] rcvrZipNo 			= new String[tmpCnt];
	            String[] rcvrAddr 			= new String[tmpCnt];
	            String[] rcvrDetailAddr 	= new String[tmpCnt];
	            String[] oriInvcNo 			= new String[tmpCnt];
	            String[] oriOrdNo 			= new String[tmpCnt];
	            
	            String[] prtSt 				= new String[tmpCnt];
	            String[] gdsCd 				= new String[tmpCnt];
	            String[] gdsNm 				= new String[tmpCnt];
	            String[] dlvDv 				= new String[tmpCnt];
	            String[] rcptErrYn 			= new String[tmpCnt];
	            
	            String[] eaiPrgsSt 			= new String[tmpCnt];
	            String[] ordDesc 			= new String[tmpCnt];
	            
	            /* CJ택배 송장발행용 주소정제 soap I/F 전송전문 출력 : xml */
	            String xml = "";
	            xml += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" ";
	            xml += " xmlns:web=\"http://webservice.address.nplus.doortodoor.co.kr/\"> ";
	            xml += "<soapenv:Header/>";
	            xml += "<soapenv:Body>";
	            xml += "<web:getAddressInformationByValue>";
	            
	            for (int i = 0; i < tmpCnt; i++) {
					JsonObject object = (JsonObject) dataSetArr.get(i);
					
					custId[i] 				= object.get("CUST_ID").toString().replaceAll("\"", "");
					rcptYmd[i] 				= object.get("RCPT_YMD").toString().replaceAll("\"", "");
					custUseNo[i] 			= object.get("CUST_USE_NO").toString().replaceAll("\"", "");
					rcptDv[i] 				= object.get("RCPT_DV").toString().replaceAll("\"", "");
					workDvCd[i] 			= object.get("WORK_DV_CD").toString().replaceAll("\"", "");
					
					reqDvCd[i] 				= object.get("REQ_DV_CD").toString().replaceAll("\"", "");
					mpckKey[i] 				= object.get("MPCK_KEY").toString().replaceAll("\"", "");
					mpckSeq[i] 				= object.get("MPCK_SEQ").toString().replaceAll("\"", "");
					calDvCd[i] 				= object.get("CAL_DV_CD").toString().replaceAll("\"", "");
					frtDvCd[i] 				= object.get("FRT_DV_CD").toString().replaceAll("\"", "");
					
					cntrItemCd[i] 			= object.get("CNTR_ITEM_CD").toString().replaceAll("\"", "");
					boxTypeCd[i] 			= object.get("BOX_TYPE_CD").toString().replaceAll("\"", "");
					boxQty[i] 				= object.get("BOX_QTY").toString().replaceAll("\"", "");
					custMgmtDlcmCd[i] 		= object.get("CUST_MGMT_DLCM_CD").toString().replaceAll("\"", "");
					sendrNm[i] 				= object.get("SENDR_NM").toString().replaceAll("\"", "");
					
					sendrTelNo1[i] 			= object.get("SENDR_TEL_NO1").toString().replaceAll("\"", "");
					sendrTelNo2[i] 			= object.get("SENDR_TEL_NO2").toString().replaceAll("\"", "");
					sendrTelNo3[i] 			= object.get("SENDR_TEL_NO3").toString().replaceAll("\"", "");
					sendrZipNo[i] 			= object.get("SENDR_ZIP_NO").toString().replaceAll("\"", "");
					sendrAddr[i] 			= object.get("SENDR_ADDR").toString().replaceAll("\"", "");
					
					sendrDetailAddr[i] 		= object.get("SENDR_DETAIL_ADDR").toString().replaceAll("\"", "");
					rcvrNm[i] 				= object.get("RCVR_NM").toString().replaceAll("\"", "");
					rcvrTelNo1[i] 			= object.get("RCVR_TEL_NO1").toString().replaceAll("\"", "");
					rcvrTelNo2[i] 			= object.get("RCVR_TEL_NO2").toString().replaceAll("\"", "");
					rcvrTelNo3[i] 			= object.get("RCVR_TEL_NO3").toString().replaceAll("\"", "");
					
					rcvrZipNo[i] 			= object.get("RCVR_ZIP_NO").toString().replaceAll("\"", "");
					rcvrAddr[i]			 	= object.get("RCVR_ADDR").toString().replaceAll("\"", "");
					rcvrDetailAddr[i] 		= object.get("RCVR_DETAIL_ADDR").toString().replaceAll("\"", "");
					oriInvcNo[i] 			= object.get("ORI_INVC_NO").toString().replaceAll("\"", "");
					oriOrdNo[i] 			= object.get("ORI_ORD_NO").toString().replaceAll("\"", "");
					
					prtSt[i] 				= object.get("PRT_ST").toString().replaceAll("\"", "");
					gdsCd[i] 				= object.get("GDS_CD").toString().replaceAll("\"", "");
					gdsNm[i] 				= object.get("GDS_NM").toString().replaceAll("\"", "");
					dlvDv[i] 				= object.get("DLV_DV").toString().replaceAll("\"", "");
					rcptErrYn[i] 			= object.get("RCPT_ERR_YN").toString().replaceAll("\"", "");
					
					eaiPrgsSt[i] 			= object.get("EAI_PRGS_ST").toString().replaceAll("\"", "");
					ordDesc[i] 				= object.get("ORD_DESC").toString().replaceAll("\"", "");
					
					/* CJ택배 송장발행용 주소정제 soap I/F 전송전문 출력 : xml */
					xml += "<arg0>";
					xml += "<boxTyp>"+object.get("BOX_TYPE_CD").toString().replaceAll("\"", "")+"</boxTyp>";
		            xml += "<clntMgmCustCd>"+object.get("CUST_ID").toString().replaceAll("\"", "")+"</clntMgmCustCd>";
		            xml += "<clntNum>"+object.get("CUST_ID").toString().replaceAll("\"", "")+"</clntNum>";
		            xml += "<cntrLarcCd>"+object.get("CNTR_ITEM_CD").toString().replaceAll("\"", "")+"</cntrLarcCd>";
		            xml += "<fareDiv>"+object.get("FRT_DV_CD").toString().replaceAll("\"", "")+"</fareDiv>";
		            xml += "<orderNo>"+object.get("CUST_USE_NO").toString().replaceAll("\"", "")+"</orderNo>";
		            xml += "<prngDivCd>"+object.get("RCPT_DV").toString().replaceAll("\"", "")+"</prngDivCd>";
		            xml += "<rcvrAddr>"+object.get("RCVR_ADDR").toString().replaceAll("\"", "").replaceAll("&", "n")+" "+object.get("RCVR_DETAIL_ADDR").toString().replaceAll("\"", "").replaceAll("&", "n")+"</rcvrAddr>";
		            xml += "<sndprsnAddr>"+object.get("SENDR_ADDR").toString().replaceAll("\"", "").replaceAll("&", "n")+" "+object.get("SENDR_DETAIL_ADDR").toString().replaceAll("\"", "").replaceAll("&", "n")+"</sndprsnAddr>";
		            xml += "</arg0>";
	            }
	            
	            /* CJ택배 송장발행용 주소정제 soap I/F 전송전문 출력 : xml */
	            xml += "</web:getAddressInformationByValue>";
	            xml += "</soapenv:Body>";
	            xml += "</soapenv:Envelope>";
	            
	            //프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();
	            modelIns.put("I_CUST_ID"			,custId);
	            modelIns.put("I_RCPT_YMD"			,rcptYmd);
	            modelIns.put("I_CUST_USE_NO"		,custUseNo);
	            modelIns.put("I_RCPT_DV"			,rcptDv);
	            modelIns.put("I_WORK_DV_CD"			,workDvCd);
	            
	            modelIns.put("I_REQ_DV_CD"			,reqDvCd);
	            modelIns.put("I_MPCK_KEY"			,mpckKey);
	            modelIns.put("I_MPCK_SEQ"			,mpckSeq);
	            modelIns.put("I_CAL_DV_CD"			,calDvCd);
	            modelIns.put("I_FRT_DV_CD"			,frtDvCd);
	            
	            modelIns.put("I_CNTR_ITEM_CD"		,cntrItemCd);
	            modelIns.put("I_BOX_TYPE_CD"		,boxTypeCd);
	            modelIns.put("I_BOX_QTY"			,boxQty);
	            modelIns.put("I_CUST_MGMT_DLCM_CD"	,custMgmtDlcmCd);
	            modelIns.put("I_SENDR_NM"			,sendrNm);
	            
	            modelIns.put("I_SENDR_TEL_NO1"		,sendrTelNo1);
	            modelIns.put("I_SENDR_TEL_NO2"		,sendrTelNo2);
	            modelIns.put("I_SENDR_TEL_NO3"		,sendrTelNo3);
	            modelIns.put("I_SENDR_ZIP_NO"		,sendrZipNo);
	            modelIns.put("I_SENDR_ADDR"			,sendrAddr);
	            
	            modelIns.put("I_SENDR_DETAIL_ADDR"	,sendrDetailAddr);
	            modelIns.put("I_RCVR_NM"			,rcvrNm);
	            modelIns.put("I_RCVR_TEL_NO1"		,rcvrTelNo1);
	            modelIns.put("I_RCVR_TEL_NO2"		,rcvrTelNo2);
	            modelIns.put("I_RCVR_TEL_NO3"		,rcvrTelNo3);
	            
	            modelIns.put("I_RCVR_ZIP_NO"		,rcvrZipNo);
	            modelIns.put("I_RCVR_ADDR"			,rcvrAddr);
	            modelIns.put("I_RCVR_DETAIL_ADDR"	,rcvrDetailAddr);
	            modelIns.put("I_ORI_INVC_NO"		,oriInvcNo);
	            modelIns.put("I_ORI_ORD_NO"			,oriOrdNo);
	            
	            modelIns.put("I_PRT_ST"				,prtSt);
	            modelIns.put("I_GDS_CD"				,gdsCd);
	            modelIns.put("I_GDS_NM"				,gdsNm);
	            modelIns.put("I_DLV_DV"				,dlvDv);
	            modelIns.put("I_RCPT_ERR_YN"		,rcptErrYn);
	            
	            modelIns.put("I_EAI_PRGS_ST"		,eaiPrgsSt);
	            modelIns.put("I_ORD_DESC"			,ordDesc);

	            //session 및 등록정보
	            modelIns.put("I_IF_ID"			, (String)model.get("IF_ID"));
	            modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
	            modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
	           
	            /* CJ택배 송장발행용 주소정제 soap I/F 출력 및 입력 시작 */
	            Map<String, Object> modelSoap = new HashMap<String, Object>();
	            HttpURLConnection con = null;
    	        URL url = null; 
    	        String sUrl = "http://address.doortodoor.co.kr/address/address_webservice.korex?wsdl";
    	        String result = "";
    	        
    	        System.out.println("CJ대한통운 WSDL 주소정제 접속 시작");
    	        
    	        try {
    	        	url = new URL(sUrl);
    	        	con = (HttpURLConnection) url.openConnection();
    	        	con.setDoInput(true);
    	        	con.setDoOutput(true);
    	        	con.setUseCaches(false);
    	        	con.setRequestMethod("POST");
    	        	con.setConnectTimeout(10000);
    	        	con.setAllowUserInteraction(true);
    	        	con.setRequestProperty("Content-type", "text/xml;charset=utf-8");
    	        	
    	        	PrintWriter pw = new PrintWriter(new OutputStreamWriter(con.getOutputStream(), "utf-8"));
    	        	
    	        	pw.write(xml);
    	        	pw.flush();
    	        	
    	        	System.out.println("CJ대한통운 WSDL 주소정제 접속중");
    	        	
    	        	int resCode = 0;
    	        	resCode = con.getResponseCode();
    	        	System.out.println("CJ대한통운 WSDL 주소정제 접속완료");
    	        	System.out.println("CJ대한통운 주소정제 resCode: " + resCode);
    	        	StringBuffer resp = new StringBuffer();
    	        	if(resCode < 400){
    	        		String line;
    	        		BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
    	        		
    	        		while ((line = br.readLine()) != null) {
    	        			//System.out.println(">> "+line);
    	        			resp.append(line);
    	        		}
    	        		
    	        		pw.close();
    	        		br.close();
    	        		//xml파싱
    	        		result = resp.toString();
    	        		String inputJSON = RestApiUtil.xmlToJsonStrReturn(result);
    	        		//System.out.println("doortodoor >> "+inputJSON);
    	        		
    	        		/* 주소정제 완료 후 WINUS DB INSERT */
    	        		modelSoap = cjAddressInformationByValue(inputJSON);
    	        	}else{
    	        		//result = con.getResponseMessage();
    	        		m.put("errCnt", -1);
        	            m.put("MSG", "주소정제 오류. 코드:"+resCode+" (관리자문의)");
    	        	}
    	        }catch(BizException e){
    	        	e.printStackTrace();
    	        	m.put("errCnt", -1);
    	            m.put("MSG", "주소정제 오류. (관리자문의)");
    	        }
	            
    	        /* 주소정제 입력 완료 후 문제가 없을 경우 인터페이스 데이터 입력 */
    	        if(modelSoap.get("errCnt").equals(0)){
    	            modelIns = (Map<String, Object>)dao.saveCjConfInsert(modelIns);
    	        }else{
    	        	m.put("errCnt", -1);
    	            m.put("MSG", "주소정제 후 주문인터페이스 입력 오류.  (e.-1)(관리자문의)");
    	        }
    	        ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
			}else{
				m.put("errCnt", -2);
	            m.put("MSG", "주문 할 자료가 없습니다. (e.-2)");
			}
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	/**
	* 
	* 대체 Method ID   : cjConfListInsertToV_RCPT_WINUS010
	* 대체 Method 설명    :
	* 작성자                      : chsong
	* @param model
	* @return
	* @throws Exception
	*/
	@Override
    public Map<String, Object> cjConfListInsertToV_RCPT_WINUS010(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try {
        	// Declare the JDBC objects.
        	String connectionUrl	= "jdbc:oracle:thin:@210.98.159.153:1523:OPENDBT"; 
            Connection con			= null;
            PreparedStatement stmt	= null;
            System.out.println("CJ shipment connect DB (1/2)");
            
            //db connect
            con	= DriverManager.getConnection(connectionUrl,"winus","winusdev!#$1");
            //stmt	= con.createStatement();
            System.out.println("CJ shipment connect DB (2/2)");
            
            //json Parser
        	Gson gson 			= new Gson();
			String jsonString	= gson.toJson(model);
			String sendData		= new StringBuffer().append(jsonString).toString();
			
			JsonParser Parser		= new JsonParser();
			JsonObject jsonObj		= (JsonObject) Parser.parse(sendData);
			JsonObject jsonObjList	= (JsonObject) jsonObj.get("LIST");
			JsonArray dataSetArr	= (JsonArray) jsonObjList.get("list");
        	
			//data Size
			int tmpCnt = dataSetArr.size();
			
			//return param 초기화
            //String[] parcelId	= new String[tmpCnt];
            //String[] parcelse	= new String[tmpCnt];
			DateFormat dateFormatYMD = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date now = new Date();
			String vSYSDATE = dateFormatYMD.format(now);
			
			String sql = "INSERT INTO V_RCPT_WINUS010(CUST_ID ,RCPT_YMD ,CUST_USE_NO ,RCPT_DV ,WORK_DV_CD ,REQ_DV_CD ,MPCK_KEY ,MPCK_SEQ ,CAL_DV_CD ,FRT_DV_CD"
					   + ",CNTR_ITEM_CD ,BOX_TYPE_CD ,BOX_QTY ,CUST_MGMT_DLCM_CD ,SENDR_NM ,SENDR_TEL_NO1 ,SENDR_TEL_NO2 ,SENDR_TEL_NO3 ,SENDR_ZIP_NO ,SENDR_ADDR"
					   + ",SENDR_DETAIL_ADDR ,RCVR_NM ,RCVR_TEL_NO1 ,RCVR_TEL_NO2 ,RCVR_TEL_NO3 ,RCVR_ZIP_NO ,RCVR_ADDR ,RCVR_DETAIL_ADDR ,INVC_NO ,ORI_INVC_NO"
					   + ",ORI_ORD_NO ,PRT_ST ,GDS_CD ,GDS_NM ,DLV_DV ,RCPT_ERR_YN ,EAI_PRGS_ST ,REG_EMP_ID ,REG_DTIME ,MODI_EMP_ID"
					   + ",MODI_DTIME) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
            for (int i = 0; i < tmpCnt; i++) {
				JsonObject object = (JsonObject) dataSetArr.get(i);
				
				//oracle V_RCPT_WINUS010 Insert
				stmt = con.prepareStatement(sql);
				stmt.setString( 1, object.get("CUST_ID").toString().replaceAll("\\\"", ""));
				stmt.setString( 2, object.get("RCPT_YMD").toString().replaceAll("\\\"", ""));
				stmt.setString( 3, object.get("CUST_USE_NO").toString().replaceAll("\\\"", ""));
				stmt.setString( 4, object.get("RCPT_DV").toString().replaceAll("\\\"", ""));
				stmt.setString( 5, object.get("WORK_DV_CD").toString().replaceAll("\\\"", ""));
				stmt.setString( 6, object.get("REQ_DV_CD").toString().replaceAll("\\\"", ""));
				stmt.setString( 7, object.get("MPCK_KEY").toString().replaceAll("\\\"", ""));
				stmt.setString( 8, object.get("MPCK_SEQ").toString().replaceAll("\\\"", ""));
				stmt.setString( 9, object.get("CAL_DV_CD").toString().replaceAll("\\\"", ""));
				stmt.setString(10, object.get("FRT_DV_CD").toString().replaceAll("\\\"", ""));
				
				stmt.setString(11, object.get("CNTR_ITEM_CD").toString().replaceAll("\\\"", ""));
				stmt.setString(12, object.get("BOX_TYPE_CD").toString().replaceAll("\\\"", ""));
				stmt.setString(13, object.get("BOX_QTY").toString().replaceAll("\\\"", ""));
				stmt.setString(14, object.get("CUST_MGMT_DLCM_CD").toString().replaceAll("\\\"", ""));
				stmt.setString(15, object.get("SENDR_NM").toString().replaceAll("\\\"", ""));
				stmt.setString(16, object.get("SENDR_TEL_NO1").toString().replaceAll("\\\"", ""));
				stmt.setString(17, object.get("SENDR_TEL_NO2").toString().replaceAll("\\\"", ""));
				stmt.setString(18, object.get("SENDR_TEL_NO3").toString().replaceAll("\\\"", ""));
				stmt.setString(19, object.get("SENDR_ZIP_NO").toString().replaceAll("\\\"", ""));
				stmt.setString(20, object.get("SENDR_ADDR").toString().replaceAll("\\\"", ""));
				
				stmt.setString(21, object.get("SENDR_DETAIL_ADDR").toString().replaceAll("\\\"", ""));
				stmt.setString(22, object.get("RCVR_NM").toString().replaceAll("\\\"", ""));
				stmt.setString(23, object.get("RCVR_TEL_NO1").toString().replaceAll("\\\"", ""));
				stmt.setString(24, object.get("RCVR_TEL_NO2").toString().replaceAll("\\\"", ""));
				stmt.setString(25, object.get("RCVR_TEL_NO3").toString().replaceAll("\\\"", ""));
				stmt.setString(26, object.get("RCVR_ZIP_NO").toString().replaceAll("\\\"", ""));
				stmt.setString(27, object.get("RCVR_ADDR").toString().replaceAll("\\\"", ""));
				stmt.setString(28, object.get("RCVR_DETAIL_ADDR").toString().replaceAll("\\\"", ""));
				stmt.setString(29, object.get("INVC_NO").toString().replaceAll("\\\"", ""));
				stmt.setString(30, object.get("ORI_INVC_NO").toString().replaceAll("\\\"", ""));
				
				stmt.setString(31, object.get("ORI_ORD_NO").toString().replaceAll("\\\"", ""));
				stmt.setString(32, object.get("PRT_ST").toString().replaceAll("\\\"", ""));
				stmt.setString(33, object.get("GDS_CD").toString().replaceAll("\\\"", ""));
				stmt.setString(34, object.get("GDS_NM").toString().replaceAll("\\\"", ""));
				stmt.setString(35, object.get("DLV_DV").toString().replaceAll("\\\"", ""));
				stmt.setString(36, object.get("RCPT_ERR_YN").toString().replaceAll("\\\"", ""));
				stmt.setString(37, object.get("EAI_PRGS_ST").toString().replaceAll("\\\"", ""));
				stmt.setString(38, object.get("REG_EMP_ID").toString().replaceAll("\\\"", ""));
				stmt.setTimestamp(39, java.sql.Timestamp.valueOf(vSYSDATE));
				stmt.setString(40, object.get("MODI_EMP_ID").toString().replaceAll("\\\"", ""));
				
				stmt.setTimestamp(41, java.sql.Timestamp.valueOf(vSYSDATE));
				
				stmt.executeUpdate();
				System.out.println("CJ executeUpdate 성공 :" + i);
				
				/*
				stmt = con.createStatement();
				stmt.executeUpdate(" INSERT INTO V_RCPT_WINUS010("
										  + "          CUST_ID"
										  + "          ,RCPT_YMD"
										  + "          ,CUST_USE_NO"
										  + "          ,RCPT_DV"
										  + "          ,WORK_DV_CD"
										
										  + "          ,REQ_DV_CD"
										  + "          ,MPCK_KEY"
										  + "          ,MPCK_SEQ"
										  + "          ,CAL_DV_CD"
										  + "          ,FRT_DV_CD"
										
										  + "          ,CNTR_ITEM_CD"
										  + "          ,BOX_TYPE_CD"
										  + "          ,BOX_QTY"
										  + "          ,CUST_MGMT_DLCM_CD"
										  + "          ,SENDR_NM"
										
										  + "          ,SENDR_TEL_NO1"
										  + "          ,SENDR_TEL_NO2"
										  + "          ,SENDR_TEL_NO3"
										  + "          ,SENDR_ZIP_NO"
										  + "          ,SENDR_ADDR"
										
										  + "          ,SENDR_DETAIL_ADDR"
										  + "          ,RCVR_NM"
										  + "          ,RCVR_TEL_NO1"
										  + "          ,RCVR_TEL_NO2"
										  + "          ,RCVR_TEL_NO3"
										
										  + "          ,RCVR_ZIP_NO"
										  + "          ,RCVR_ADDR"
										  + "          ,RCVR_DETAIL_ADDR"
										  + "          ,INVC_NO"
										  + "          ,ORI_INVC_NO"
										
										  + "          ,ORI_ORD_NO"
										  + "          ,PRT_ST"
										  + "          ,GDS_CD"
										  + "          ,GDS_NM"
										  + "          ,DLV_DV"
										
										  + "          ,RCPT_ERR_YN"
										  + "          ,EAI_PRGS_ST"
										    
										  + "          ,REG_EMP_ID"
										  + "          ,REG_DTIME"
										  + "          ,MODI_EMP_ID"
										  + "          ,MODI_DTIME"
									      + ")VALUES("
									      + "		 REPLACE('"+ object.get("CUST_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCPT_YMD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("CUST_USE_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCPT_DV")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("WORK_DV_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("REQ_DV_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("MPCK_KEY")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("MPCK_SEQ")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("CAL_DV_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("FRT_DV_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("CNTR_ITEM_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("BOX_TYPE_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("BOX_QTY")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("CUST_MGMT_DLCM_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("SENDR_NM")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("SENDR_TEL_NO1")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("SENDR_TEL_NO2")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("SENDR_TEL_NO3")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("SENDR_ZIP_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("SENDR_ADDR")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("SENDR_DETAIL_ADDR")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCVR_NM")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCVR_TEL_NO1")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCVR_TEL_NO2")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCVR_TEL_NO3")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("RCVR_ZIP_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCVR_ADDR")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("RCVR_DETAIL_ADDR")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("INVC_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("ORI_INVC_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("ORI_ORD_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("PRT_ST")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("GDS_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("GDS_NM")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("DLV_DV")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("RCPT_ERR_YN")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, REPLACE('"+ object.get("EAI_PRGS_ST")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      
									      + "		, REPLACE('"+ object.get("REG_EMP_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, SYSDATE"
									      + "		, REPLACE('"+ object.get("MODI_EMP_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')"
									      + "		, SYSDATE"
										  + ")"
				);
				*/
				stmt.close();
            }

			stmt.close();
            con.close();
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }  catch(Exception e){
        	m.put("errCnt", -1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
            throw e;
        }
        return m;
    }
		
	 /**
     * 
     * 대체 Method ID   : cjConfListUpdateToWMSDF020
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> cjConfListUpdateToWMSDF020(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("I_IF_ID"			, (String)model.get("vrSrchIfId"));
            modelIns.put("I_CUST_ID"		, (String)model.get("CUST_ID")); //CJ고객코드 CUST_ID
            
            //session 및 등록정보
            modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
            
            //dao
            modelIns = (Map<String, Object>)dao.saveCjConfUpdate(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
			 
	 /**
     * 
     * 대체 Method ID   : godomallOrderUpdate
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> godomallOrderUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String result = "";
        int errCnt = 0;
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	HttpURLConnection con = null;
    	        URL url = null; 
    	        String sUrl = (String)model.get("url"); // 연결할 주소
    	         
    	         try {
    	        	 /*
    	        	  * 일괄전송 시 xml 전송
    	        	 StringBuffer sb      =  new StringBuffer();
    	        	 sb.append("<?xml version='1.0' encoding='utf-8'?>");
    	        	 sb.append("<data>");
    	        	 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    	        		 sb.append("<statusData idx=" + (i+1) + ">");
    	        		 sb.append("		<orderNo>"+ (String)model.get("ORG_ORD_ID"+i) +"</orderNo>");
    	        		 sb.append("		<sno>"+ (String)model.get("UNIT_NO"+i) +"</sno>");
    	        		 sb.append("		<orderStatus>g2</orderStatus>"); //주문상태코드
    	        		 sb.append("		<invoiceNo>"+ (String)model.get("DELIVERY_NO"+i) +"</invoiceNo>");
    	        		 sb.append("</statusData>");
    	        	 }
    	        	 sb.append("</data>");
    	        	 System.out.println(sb);
		            */
    	        	 
    	        	 url = new URL(sUrl);
    	             con = (HttpURLConnection) url.openConnection();
    	             con.setDoInput(true);
    	             con.setDoOutput(true);
    	             con.setUseCaches(false);
    	             con.setRequestMethod("POST");
    	             con.setConnectTimeout(10000);
    	             con.setAllowUserInteraction(true);
    	             con.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
    	             
    	             StringBuffer sb = new StringBuffer();
    	             sb.append("key").append("=").append((String)model.get("key")).append("&");
    	             sb.append("partner_key").append("=").append((String)model.get("partner_key")).append("&");
    	             sb.append("orderNo").append("=").append((String)model.get("ORG_ORD_ID"+i)).append("&");
    	             sb.append("sno").append("=").append((String)model.get("UNIT_NO"+i)).append("&");
    	             sb.append("orderStatus").append("=").append("d1").append("&");
    	             sb.append("invoiceCompanySno").append("=").append(8).append("&"); //대한통운
    	             sb.append("invoiceNo").append("=").append((String)model.get("DELIVERY_NO"+i));
    	             
    	             PrintWriter pw = new PrintWriter(new OutputStreamWriter(con.getOutputStream(), "utf-8"));
    	             pw.write(sb.toString());
    	             pw.flush();
    	             
    	             int resCode = 0;
    	             resCode = con.getResponseCode();
    	             
    	             StringBuffer resp = new StringBuffer();
    	             if(resCode < 400){
    	                 String line;
    	                 
    	                 BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
    	                 while ((line = br.readLine()) != null) {
    	                     System.out.println(">> "+line);
    	                     resp.append(line);
    	                 }
    	                 
    	                 pw.close();
    	                 br.close();
    	                 
    	                 //xml파싱
    	                 result = resp.toString();
    	             }else{
    	                 result = con.getResponseMessage();
    	             }
    	         }catch(Exception e){
    	             e.printStackTrace();
    	         }
            }
           
            m.put("errCnt", errCnt);
            m.put("RST", result);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        }  catch(Exception e){
            throw e;
        }
        
        return m;
    }
    
    /**
	* 
	* 대체 Method ID   : cjAddressInformationByValue
	* 대체 Method 설명    :
	* 작성자                      : chsong
	* @param model
	* @return
	* @throws Exception
	*/
	@Override
    public Map<String, Object> cjAddressInformationByValue(String xml) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try {
			String jsonString	= xml;
			String sendData		= new StringBuffer().append(jsonString).toString();
			
			JsonParser Parser		  = new JsonParser();
			JsonObject jsonObj		  = (JsonObject) Parser.parse(sendData);
			JsonObject jsonObjEnvelop = (JsonObject) jsonObj.get("S:Envelope");
			JsonObject jsonObjBody	  = (JsonObject) jsonObjEnvelop.get("S:Body");
			JsonObject jsonObjValueResponse	= (JsonObject) jsonObjBody.get("ns2:getAddressInformationByValueResponse");
			JsonArray dataSetArr	  = (JsonArray) jsonObjValueResponse.get("return");
        	
			//data Size
			int tmpCnt = dataSetArr.size();
			String[] clntMgmCustCd			= new String[tmpCnt];
			String[] clntNum				= new String[tmpCnt];
			String[] orderNo				= new String[tmpCnt];
			
			String[] rcvrNewAddrDtl			= new String[tmpCnt];
			String[] bscFare				= new String[tmpCnt];
			String[] rcvrAddr				= new String[tmpCnt];
			String[] rcvrShortAddr			= new String[tmpCnt];
			String[] fareDiv				= new String[tmpCnt];
			String[] gthPreArrBranNm		= new String[tmpCnt];
			String[] rcvrNewAddr			= new String[tmpCnt];
			String[] sndprNewAddr			= new String[tmpCnt];
			String[] rcvrNewAddrYn			= new String[tmpCnt];
			String[] errorMsg				= new String[tmpCnt];
			String[] sndprNewAddrYn			= new String[tmpCnt];
			String[] rcvrClsfAddr			= new String[tmpCnt];
			String[] dlvClsfNm				= new String[tmpCnt];
			String[] gthPreArrBranCd		= new String[tmpCnt];
			String[] sndprOldAddrDtl		= new String[tmpCnt];
			String[] dlvPreArrBranCd		= new String[tmpCnt];
			String[] gthClsfCd				= new String[tmpCnt];
			String[] gthPreArrEmpNm			= new String[tmpCnt];
			String[] dlvPreArrBranShortNm	= new String[tmpCnt];
			String[] sndprClsfAddr			= new String[tmpCnt];
			String[] sndprsnAddr			= new String[tmpCnt];
			String[] gthClsfNm				= new String[tmpCnt];
			String[] dlvPreArrEmpNm			= new String[tmpCnt];
			String[] rcvrOldAddrDtl			= new String[tmpCnt];
			String[] errorCd				= new String[tmpCnt];
			String[] sndprNewAddrDtl		= new String[tmpCnt];
			String[] jejuFare				= new String[tmpCnt];
			String[] ferryFare				= new String[tmpCnt];
			String[] rcvrEtcAddr			= new String[tmpCnt];
			String[] dealFare				= new String[tmpCnt];
			String[] sndprOldAddr			= new String[tmpCnt];
			String[] gthSubClsfCd			= new String[tmpCnt];
			String[] dlvPreArrEmpNickNm		= new String[tmpCnt];
			String[] gthPreArrEmpNickNm		= new String[tmpCnt];
			String[] dlvPreArrEmpNum		= new String[tmpCnt];
			String[] sndprEtcAddr			= new String[tmpCnt];
			String[] dlvPreArrBranNm		= new String[tmpCnt];
			String[] gthPreArrBranShortNm	= new String[tmpCnt];
			String[] rcvrZipnum				= new String[tmpCnt];
			String[] prngDivCd				= new String[tmpCnt];
			String[] sndprShortAddr			= new String[tmpCnt];
			String[] sndprZipnum			= new String[tmpCnt];
			String[] rcvrOldAddr			= new String[tmpCnt];
			String[] cntrLarcCd				= new String[tmpCnt];
			String[] dlvSubClsfCd			= new String[tmpCnt];
			String[] dlvClsfCd				= new String[tmpCnt];
			String[] gthPreArrEmpNum		= new String[tmpCnt];
			
            for (int i = 0; i < tmpCnt; i++) {
				JsonObject object = (JsonObject) dataSetArr.get(i);
				clntMgmCustCd[i] 		= object.get("clntMgmCustCd").toString().replaceAll("\\\"", "");
				clntNum[i] 				= object.get("clntNum").toString().replaceAll("\\\"", "");
				orderNo[i] 				= object.get("orderNo").toString().replaceAll("\\\"", "");
				
				try{rcvrNewAddrDtl[i] 		= object.get("rcvrNewAddrDtl").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrNewAddrDtl[i] = "";}
				try{bscFare[i] 				= object.get("bscFare").toString().replaceAll("\\\"", "");}catch (Exception e){bscFare[i] = "";}
				try{rcvrAddr[i] 			= object.get("rcvrAddr").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrAddr[i] = "";}
				try{rcvrShortAddr[i] 		= object.get("rcvrShortAddr").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrShortAddr[i] = "";}
				try{fareDiv[i] 				= object.get("fareDiv").toString().replaceAll("\\\"", "");}catch (Exception e){fareDiv[i] = "";}
				try{gthPreArrBranNm[i] 		= object.get("gthPreArrBranNm").toString().replaceAll("\\\"", "");}catch (Exception e){gthPreArrBranNm[i] = "";}
				try{rcvrNewAddr[i] 			= object.get("rcvrNewAddr").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrNewAddr[i] = "";}
				try{sndprNewAddr[i] 		= object.get("sndprNewAddr").toString().replaceAll("\\\"", "");}catch (Exception e){sndprNewAddr[i] = "";}
				try{rcvrNewAddrYn[i] 		= object.get("rcvrNewAddrYn").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrNewAddrYn[i] = "";}
				try{errorMsg[i] 			= object.get("errorMsg").toString().replaceAll("\\\"", "");}catch (Exception e){errorMsg[i] = "";}
				try{sndprNewAddrYn[i] 		= object.get("sndprNewAddrYn").toString().replaceAll("\\\"", "");}catch (Exception e){sndprNewAddrYn[i] = "";}
				try{rcvrClsfAddr[i] 		= object.get("rcvrClsfAddr").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrClsfAddr[i] = "";}
				try{dlvClsfNm[i] 			= object.get("dlvClsfNm").toString().replaceAll("\\\"", "");}catch (Exception e){dlvClsfNm[i] = "";}
				try{gthPreArrBranCd[i] 		= object.get("gthPreArrBranCd").toString().replaceAll("\\\"", "");}catch (Exception e){gthPreArrBranCd[i] = "";}
				try{sndprOldAddrDtl[i] 		= object.get("sndprOldAddrDtl").toString().replaceAll("\\\"", "");}catch (Exception e){sndprOldAddrDtl[i] = "";}
				try{dlvPreArrBranCd[i] 		= object.get("dlvPreArrBranCd").toString().replaceAll("\\\"", "");}catch (Exception e){dlvPreArrBranCd[i] = "";}
				try{gthClsfCd[i] 			= object.get("gthClsfCd").toString().replaceAll("\\\"", "");}catch (Exception e){gthClsfCd[i] = "";}
				try{gthPreArrEmpNm[i] 		= object.get("gthPreArrEmpNm").toString().replaceAll("\\\"", "");}catch (Exception e){gthPreArrEmpNm[i] = "";}
				try{dlvPreArrBranShortNm[i] = object.get("dlvPreArrBranShortNm").toString().replaceAll("\\\"", "");}catch (Exception e){dlvPreArrBranShortNm[i] = "";}
				try{sndprClsfAddr[i] 		= object.get("sndprClsfAddr").toString().replaceAll("\\\"", "");}catch (Exception e){sndprClsfAddr[i] = "";}
				try{sndprsnAddr[i] 			= object.get("sndprsnAddr").toString().replaceAll("\\\"", "");}catch (Exception e){sndprsnAddr[i] = "";}
				try{gthClsfNm[i] 			= object.get("gthClsfNm").toString().replaceAll("\\\"", "");}catch (Exception e){gthClsfNm[i] = "";}
				try{dlvPreArrEmpNm[i] 		= object.get("dlvPreArrEmpNm").toString().replaceAll("\\\"", "");}catch (Exception e){dlvPreArrEmpNm[i] = "";}
				try{rcvrOldAddrDtl[i] 		= object.get("rcvrOldAddrDtl").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrOldAddrDtl[i] = "";}
				try{errorCd[i] 				= object.get("errorCd").toString().replaceAll("\\\"", "");}catch (Exception e){errorCd[i] = "";}
				try{sndprNewAddrDtl[i] 		= object.get("sndprNewAddrDtl").toString().replaceAll("\\\"", "");}catch (Exception e){sndprNewAddrDtl[i] = "";}
				try{jejuFare[i] 			= object.get("jejuFare").toString().replaceAll("\\\"", "");}catch (Exception e){jejuFare[i] = "";}
				try{ferryFare[i] 			= object.get("ferryFare").toString().replaceAll("\\\"", "");}catch (Exception e){ferryFare[i] = "";}
				try{rcvrEtcAddr[i] 			= object.get("rcvrEtcAddr").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrEtcAddr[i] = "";}
				try{dealFare[i] 			= object.get("dealFare").toString().replaceAll("\\\"", "");}catch (Exception e){dealFare[i] = "";}
				try{sndprOldAddr[i] 		= object.get("sndprOldAddr").toString().replaceAll("\\\"", "");}catch (Exception e){sndprOldAddr[i] = "";}
				try{gthSubClsfCd[i] 		= object.get("gthSubClsfCd").toString().replaceAll("\\\"", "");}catch (Exception e){gthSubClsfCd[i] = "";}
				try{dlvPreArrEmpNickNm[i] 	= object.get("dlvPreArrEmpNickNm").toString().replaceAll("\\\"", "");}catch (Exception e){dlvPreArrEmpNickNm[i] = "";}
				try{gthPreArrEmpNickNm[i] 	= object.get("gthPreArrEmpNickNm").toString().replaceAll("\\\"", "");}catch (Exception e){gthPreArrEmpNickNm[i] = "";}
				try{dlvPreArrEmpNum[i] 		= object.get("dlvPreArrEmpNum").toString().replaceAll("\\\"", "");}catch (Exception e){dlvPreArrEmpNum[i] = "";}
				try{sndprEtcAddr[i] 		= object.get("sndprEtcAddr").toString().replaceAll("\\\"", "");}catch (Exception e){sndprEtcAddr[i] = "";}
				try{dlvPreArrBranNm[i] 		= object.get("dlvPreArrBranNm").toString().replaceAll("\\\"", "");}catch (Exception e){dlvPreArrBranNm[i] = "";}
				try{gthPreArrBranShortNm[i] = object.get("gthPreArrBranShortNm").toString().replaceAll("\\\"", "");}catch (Exception e){gthPreArrBranShortNm[i] = "";}
				try{rcvrZipnum[i] 			= object.get("rcvrZipnum").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrZipnum[i] = "";}
				try{prngDivCd[i] 			= object.get("prngDivCd").toString().replaceAll("\\\"", "");}catch (Exception e){prngDivCd[i] = "";}
				try{sndprShortAddr[i] 		= object.get("sndprShortAddr").toString().replaceAll("\\\"", "");}catch (Exception e){sndprShortAddr[i] = "";}
				try{sndprZipnum[i] 			= object.get("sndprZipnum").toString().replaceAll("\\\"", "");}catch (Exception e){sndprZipnum[i] = "";}
				try{rcvrOldAddr[i] 			= object.get("rcvrOldAddr").toString().replaceAll("\\\"", "");}catch (Exception e){rcvrOldAddr[i] = "";}
				try{cntrLarcCd[i] 			= object.get("cntrLarcCd").toString().replaceAll("\\\"", "");}catch (Exception e){cntrLarcCd[i] = "";}
				try{dlvSubClsfCd[i] 		= object.get("dlvSubClsfCd").toString().replaceAll("\\\"", "");}catch (Exception e){dlvSubClsfCd[i] = "";}
				try{dlvClsfCd[i] 			= object.get("dlvClsfCd").toString().replaceAll("\\\"", "");}catch (Exception e){dlvClsfCd[i] = "";}
				try{gthPreArrEmpNum[i] 		= object.get("gthPreArrEmpNum").toString().replaceAll("\\\"", "");}catch (Exception e){gthPreArrEmpNum[i] = "";}
            }
            
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("I_CLNT_MGM_CUST_CD"			,clntMgmCustCd);
            modelIns.put("I_CLNT_NUM"					,clntNum);
            modelIns.put("I_ORDER_NO"					,orderNo);
            
            modelIns.put("I_RCVR_NEW_ADDR_DTL"			,rcvrNewAddrDtl);
            modelIns.put("I_BSC_FARE"					,bscFare);
            modelIns.put("I_RCVR_ADDR"					,rcvrAddr);
            modelIns.put("I_RCVR_SHORT_ADDR"			,rcvrShortAddr);
            modelIns.put("I_FARE_DIV"					,fareDiv);
            modelIns.put("I_GTH_PRE_ARR_BRAN_NM"		,gthPreArrBranNm);
            modelIns.put("I_RCVR_NEW_ADDR"				,rcvrNewAddr);
            modelIns.put("I_SNDPR_NEW_ADDR"				,sndprNewAddr);
            modelIns.put("I_RCVR_NEW_ADDR_YN"			,rcvrNewAddrYn);
            modelIns.put("I_ERROR_MSG"					,errorMsg);
            modelIns.put("I_SNDPR_NEW_ADDR_YN"			,sndprNewAddrYn);
            modelIns.put("I_RCVR_CLSF_ADDR"				,rcvrClsfAddr);
            modelIns.put("I_DLV_CLSF_NM"				,dlvClsfNm);
            modelIns.put("I_GTH_PRE_ARR_BRAN_CD"		,gthPreArrBranCd);
            modelIns.put("I_SNDPR_OLD_ADDR_DTL"			,sndprOldAddrDtl);
            modelIns.put("I_DLV_PRE_ARR_BRAN_CD"		,dlvPreArrBranCd);
            modelIns.put("I_GTH_CLSF_CD"				,gthClsfCd);
            modelIns.put("I_GTH_PRE_ARR_EMP_NM"			,gthPreArrEmpNm);
            modelIns.put("I_DLV_PRE_ARR_BRAN_SHORT_NM"	,dlvPreArrBranShortNm);
            modelIns.put("I_SNDPR_CLSF_ADDR"			,sndprClsfAddr);
            modelIns.put("I_SNDPRSN_ADDR"				,sndprsnAddr);
            modelIns.put("I_GTH_CLSF_NM"				,gthClsfNm);
            modelIns.put("I_DLV_PRE_ARR_EMP_NM"			,dlvPreArrEmpNm);
            modelIns.put("I_RCVR_OLD_ADDR_DTL"			,rcvrOldAddrDtl);
            modelIns.put("I_ERROR_CD"					,errorCd);
            modelIns.put("I_SNDPR_NEW_ADDR_DTL"			,sndprNewAddrDtl);
            modelIns.put("I_JEJU_FARE"					,jejuFare);
            modelIns.put("I_FERRY_FARE"					,ferryFare);
            modelIns.put("I_RCVR_ETC_ADDR"				,rcvrEtcAddr);
            modelIns.put("I_DEAL_FARE"					,dealFare);
            modelIns.put("I_SNDPR_OLD_ADDR"				,sndprOldAddr);
            modelIns.put("I_GTH_SUB_CLSF_CD"			,gthSubClsfCd);
            modelIns.put("I_DLV_PRE_ARR_EMP_NICK_NM"	,dlvPreArrEmpNickNm);
            modelIns.put("I_GTH_PRE_ARR_EMP_NICK_NM"	,gthPreArrEmpNickNm);
            modelIns.put("I_DLV_PRE_ARR_EMP_NUM"		,dlvPreArrEmpNum);
            modelIns.put("I_SNDPR_ETC_ADDR"				,sndprEtcAddr);
            modelIns.put("I_DLV_PRE_ARR_BRAN_NM"		,dlvPreArrBranNm);
            modelIns.put("I_GTH_PRE_ARR_BRAN_SHORT_NM"	,gthPreArrBranShortNm);
            modelIns.put("I_RCVR_ZIPNUM"				,rcvrZipnum);
            modelIns.put("I_PRNG_DIV_CD"				,prngDivCd);
            modelIns.put("I_SNDPR_SHORT_ADDR"			,sndprShortAddr);
            modelIns.put("I_SNDPR_ZIPNUM"				,sndprZipnum);
            modelIns.put("I_RCVR_OLD_ADDR"				,rcvrOldAddr);
            modelIns.put("I_CNTR_LARC_CD"				,cntrLarcCd);
            modelIns.put("I_DLV_SUB_CLSF_CD"			,dlvSubClsfCd);
            modelIns.put("I_DLV_CLSF_CD"				,dlvClsfCd);
            modelIns.put("I_GTH_PRE_ARR_EMP_NUM"		,gthPreArrEmpNum);
            
            modelIns = (Map<String, Object>)dao.cjAddressInformationByValue(modelIns);
			ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
			
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }  catch(Exception e){
        	m.put("errCnt", -1);
            //m.put("MSG", MessageResolver.getMessage("save.error") );
            m.put("MSG", "주소정제 오류. (관리자문의, 패키지업력 시)");
            throw e;
        }
        return m;
    }
	
	/**
     * 대체 Method ID   : saveSfDataInsert
     * 대체 Method 설명    : 할당처리,취소
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSfDataInsert(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	int temCnt = Integer.parseInt(model.get("selectIds").toString());
        	int firstRowCnt = Integer.parseInt(model.get("firstRowCnt").toString());
        	
            if(temCnt > 0){
            	String[] dlvCompCd	= new String[firstRowCnt];
            	String[] ordDate	= new String[firstRowCnt];
            	String[] boxTypeCd	= new String[firstRowCnt];
            	String[] ordId		= new String[firstRowCnt];
            	String[] ordSeq		= new String[firstRowCnt];
            	String[] dlvInvcNo	= new String[firstRowCnt];
            	String[] printUrl	= new String[firstRowCnt];
            	String[] invoiceUrl	= new String[firstRowCnt];
            	
            	int dbInsertCnt = 0;
            	StringBuffer carGo = new StringBuffer();
            	
            	for(int i = 0 ; i < temCnt ; i ++){
            		if(i == 0){
            			carGo.append("<Cargo");
            			carGo.append("	goods_code='"+(String)model.get("RITEM_CD"+i)+"'");
            			carGo.append("	name='"+(String)model.get("RITEM_NM"+i)+"'");
            			carGo.append("	count='"+(String)model.get("OUT_ORD_QTY"+i)+"'");
            			carGo.append("	unit='件'");
            			carGo.append("	amount='"+(String)model.get("UNIT_PRICE"+i)+"'");
            			carGo.append("	source_area ='"+(String)model.get("COUNTRY_ORIGIN"+i)+"'");
            			carGo.append("/>");
            			
            			//주문이 단 1건일 경우
            			if(temCnt == 1){
            				JaxWsProxyFactoryBean svr = new JaxWsProxyFactoryBean();
                    		svr.setServiceClass(OrderWebService.class);
                    		svr.setAddress("http://osms.sf-express.com/osms/services/OrderWebService?wsdl");
                    		OrderWebService orderWebService = svr.create(OrderWebService.class);
                    		StringBuffer sb      =  new StringBuffer();
                    		sb.append("<?xml version='1.0' encoding='utf-8'?>");
                    		sb.append("<Request service='apiOrderService' lang='en'>");
                    		sb.append("<Head>OSMS_8269</Head>");
                    		sb.append("<Body>");
                    		sb.append("<Order");
                    		sb.append("	reference_no1='"+(String)model.get("ORG_ORD_ID"+i)+"'");
                    		sb.append("	express_type='229'"); //E-commerce Express-CD
                    		sb.append("	parcel_quantity='1'");
                    		sb.append("	pay_method='3'"); //3PL
                    		sb.append("	currency='KRW'"); //?
                    		sb.append("	j_contact='"+(String)model.get("CUST_CD"+i)+"'");
                    		sb.append("	j_tel='"+(String)model.get("CUST_FR_TEL"+i)+"'");
                    		sb.append("	j_province='"+(String)model.get("CUST_FR_SUB_CITY"+i)+"'");
                    		sb.append("	j_city='"+(String)model.get("CUST_FR_STATE"+i)+"'");
                    		sb.append("	j_address='"+(String)model.get("CUST_FR_ADDR"+i)+"'");
                    		sb.append("	j_country='KR'"); //발송국가
                    		sb.append("	j_post_code='"+(String)model.get("CUST_FR_ZIP"+i)+"'");
                    		sb.append("	d_contact='"+(String)model.get("SALES_CUST_NM"+i)+"'");
                    		sb.append("	receiver_type='1'"); //personal shipment 고정
                    		sb.append("	order_cert_type='"+(String)model.get("ETC2"+i)+"'");
                    		sb.append("	order_cert_no='"+(String)model.get("CUST_ID_NO"+i)+"'");
                    		sb.append("	d_province='"+(String)model.get("STATE"+i)+"'");
                    		sb.append("	d_city='"+(String)model.get("SUB_CITY"+i)+"'");
                    		sb.append("	d_address='"+(String)model.get("ADDR"+i)+"'");
                    		sb.append("	d_tel='"+(String)model.get("PHONE_1"+i)+"'");
                    		sb.append("	d_country='CN'");
                    		sb.append("	d_post_code='"+(String)model.get("ZIP"+i)+"'");
                    		sb.append("	custid='0825004596'"); //"+(String)model.get("CUST_FR_INTERFACE_KEY"+i)+"
                    		sb.append("	tax_pay_type='1'>");
                    		sb.append(carGo.toString());
                    		sb.append("</Order>");
                    		sb.append("</Body>");
                    		sb.append("</Request>");
                    		
                    		/*고정 시작*/
                    		String xml = sb.toString();
                    		//System.out.println("$$$$$$$$$$$$$$$$ xml START $$$$$$$$$$$$$");
                    		//System.out.println(xml);
                    		//System.out.println("$$$$$$$$$$$$$$$$ xml END $$$$$$$$$$$$$");
                    		
                    		String checkWord ="561b32d1798e40cc";
                    		String CustomerCode ="OSMS_8269"; 
                    		String data = encodeBase64(xml);		
                    		String md5 = DigestUtils.md5Hex(xml + checkWord);
                    		String validateStr = encodeBase64(md5);
                    		String result = orderWebService.sfexpressService(data, validateStr, CustomerCode);
                    		//System.out.println("# result>> : " + result);
                    		
                    		//고정 끝
                    		JSONObject xmlJSONObj = XML.toJSONObject(result, true);
                            String jsonString	  = xmlJSONObj.toString(4);
                			String sendData		  = new StringBuffer().append(jsonString).toString();
                			JsonParser Parser     = new JsonParser();
                			JsonObject jsonObj			= (JsonObject) Parser.parse(sendData);
                			JsonObject jsonObjResponse	= (JsonObject) jsonObj.get("Response");
                			JsonObject jsonObjBody		= (JsonObject) jsonObjResponse.get("Body");
                			JsonObject OrderResponse	= (JsonObject) jsonObjBody.get("OrderResponse");
                			//System.out.println("winus주문번호: " + (String)model.get("ORD_ID"+i));
                			//System.out.println("원주문번호: " + (String)model.get("ORG_ORD_ID"+i));
                			//System.out.println("RTN winus주문번호: "+OrderResponse.get("customerOrderNo").toString().replaceAll("\"", ""));
                			//System.out.println("RTN SF송장번호: "+OrderResponse.get("mailNo").toString().replaceAll("\"", ""));
                			//System.out.println("RTN printUrl: "+OrderResponse.get("printUrl").toString().replaceAll("\"", ""));
                			//System.out.println("RTN invoiceUrl: "+OrderResponse.get("invoiceUrl").toString().replaceAll("\"", ""));
                			
                			dlvCompCd[dbInsertCnt]	= (String)model.get("DLV_COMP_CD"+i);
                			ordDate[dbInsertCnt]	= (String)model.get("ORD_DATE"+i);
                			boxTypeCd[dbInsertCnt]	= (String)model.get("BOX_TYPE_CD"+i);
                        	ordId[dbInsertCnt]		= (String)model.get("ORG_ORD_ID"+i);
                            ordSeq[dbInsertCnt]		= "1";
                            dlvInvcNo[dbInsertCnt]	= OrderResponse.get("mailNo").toString().replaceAll("\"", "");
                            printUrl[dbInsertCnt]	= OrderResponse.get("printUrl").toString().replaceAll("\"", "");
                            invoiceUrl[dbInsertCnt]	= OrderResponse.get("invoiceUrl").toString().replaceAll("\"", "");
            			}
            			
            		//2row 부터
            		}else if(i > 0){
            			carGo.append("<Cargo");
            			carGo.append("	goods_code='"+(String)model.get("RITEM_CD"+i)+"'");
            			carGo.append("	name='"+(String)model.get("RITEM_NM"+i)+"'");
            			carGo.append("	count='"+(String)model.get("OUT_ORD_QTY"+i)+"'");
            			carGo.append("	unit='件'");
            			carGo.append("	amount='"+(String)model.get("UNIT_PRICE"+i)+"'");
            			carGo.append("	source_area ='"+(String)model.get("COUNTRY_ORIGIN"+i)+"'");
            			carGo.append("/>");
            			
            			/* SF 주문전송 */
            			if(model.get("SF_ORD_SEQ"+i).equals(model.get("SEQ_MAX"+i))){
            				JaxWsProxyFactoryBean svr = new JaxWsProxyFactoryBean();
                    		svr.setServiceClass(OrderWebService.class);
                    		svr.setAddress("http://osms.sf-express.com/osms/services/OrderWebService?wsdl");
                    		OrderWebService orderWebService = svr.create(OrderWebService.class);
                    		StringBuffer sb      =  new StringBuffer();
                    		sb.append("<?xml version='1.0' encoding='utf-8'?>");
                    		sb.append("<Request service='apiOrderService' lang='en'>");
                    		sb.append("<Head>OSMS_8269</Head>");
                    		sb.append("<Body>");
                    		sb.append("<Order");
                    		sb.append("	reference_no1='"+(String)model.get("ORG_ORD_ID"+i)+"'");
                    		sb.append("	express_type='229'"); //E-commerce Express-CD
                    		sb.append("	parcel_quantity='1'");
                    		sb.append("	pay_method='3'"); //3PL
                    		sb.append("	currency='KRW'"); //?
                    		sb.append("	j_contact='"+(String)model.get("CUST_CD"+i)+"'");
                    		sb.append("	j_tel='"+(String)model.get("CUST_FR_TEL"+i)+"'");
                    		sb.append("	j_province='"+(String)model.get("CUST_FR_SUB_CITY"+i)+"'");
                    		sb.append("	j_city='"+(String)model.get("CUST_FR_STATE"+i)+"'");
                    		sb.append("	j_address='"+(String)model.get("CUST_FR_ADDR"+i)+"'");
                    		sb.append("	j_country='KR'"); //발송국가
                    		sb.append("	j_post_code='"+(String)model.get("CUST_FR_ZIP"+i)+"'");
                    		sb.append("	d_contact='"+(String)model.get("SALES_CUST_NM"+i)+"'");
                    		sb.append("	receiver_type='1'"); //personal shipment 고정
                    		sb.append("	order_cert_type='"+(String)model.get("ETC2"+i)+"'");
                    		sb.append("	order_cert_no='"+(String)model.get("CUST_ID_NO"+i)+"'");
                    		sb.append("	d_province='"+(String)model.get("STATE"+i)+"'");
                    		sb.append("	d_city='"+(String)model.get("SUB_CITY"+i)+"'");
                    		sb.append("	d_address='"+(String)model.get("ADDR"+i)+"'");
                    		sb.append("	d_tel='"+(String)model.get("PHONE_1"+i)+"'");
                    		sb.append("	d_country='CN'");
                    		sb.append("	d_post_code='"+(String)model.get("ZIP"+i)+"'");
                    		sb.append("	custid='0825004596'"); //"+(String)model.get("CUST_FR_INTERFACE_KEY"+i)+"
                    		sb.append("	tax_pay_type='1'>");
                    		sb.append(carGo.toString());
                    		sb.append("</Order>");
                    		sb.append("</Body>");
                    		sb.append("</Request>");
                    		
                    		/*고정 시작*/
                    		String xml = sb.toString();
                    		//System.out.println("$$$$$$$$$$$$$$$$ xml START $$$$$$$$$$$$$");
                    		//System.out.println(xml);
                    		//System.out.println("$$$$$$$$$$$$$$$$ xml END $$$$$$$$$$$$$");
                    		
                    		String checkWord ="561b32d1798e40cc";
                    		String CustomerCode ="OSMS_8269"; 
                    		String data = encodeBase64(xml);		
                    		String md5 = DigestUtils.md5Hex(xml + checkWord);
                    		String validateStr = encodeBase64(md5);
                    		String result = orderWebService.sfexpressService(data, validateStr, CustomerCode);
                    		//System.out.println("# result>> : " + result);
                    		
                    		//고정 끝
                    		JSONObject xmlJSONObj = XML.toJSONObject(result, true);
                            String jsonString	  = xmlJSONObj.toString(4);
                			String sendData		  = new StringBuffer().append(jsonString).toString();
                			JsonParser Parser     = new JsonParser();
                			JsonObject jsonObj			= (JsonObject) Parser.parse(sendData);
                			JsonObject jsonObjResponse	= (JsonObject) jsonObj.get("Response");
                			JsonObject jsonObjBody		= (JsonObject) jsonObjResponse.get("Body");
                			JsonObject OrderResponse	= (JsonObject) jsonObjBody.get("OrderResponse");
                			//System.out.println("winus주문번호: " + (String)model.get("ORD_ID"+i));
                			//System.out.println("원주문번호: " + (String)model.get("ORG_ORD_ID"+i));
                			//System.out.println("RTN winus주문번호: "+OrderResponse.get("customerOrderNo").toString().replaceAll("\"", ""));
                			//System.out.println("RTN SF송장번호: "+OrderResponse.get("mailNo").toString().replaceAll("\"", ""));
                			//System.out.println("RTN printUrl: "+OrderResponse.get("printUrl").toString().replaceAll("\"", ""));
                			//System.out.println("RTN invoiceUrl: "+OrderResponse.get("invoiceUrl").toString().replaceAll("\"", ""));
                			
                			dlvCompCd[dbInsertCnt]	= (String)model.get("DLV_COMP_CD"+i);
                			ordDate[dbInsertCnt]	= (String)model.get("ORD_DATE"+i);
                			boxTypeCd[dbInsertCnt]	= (String)model.get("BOX_TYPE_CD"+i);
                        	ordId[dbInsertCnt]		= (String)model.get("ORG_ORD_ID"+i);
                            ordSeq[dbInsertCnt]		= "1";
                            dlvInvcNo[dbInsertCnt]	= OrderResponse.get("mailNo").toString().replaceAll("\"", "");
                            printUrl[dbInsertCnt]	= OrderResponse.get("printUrl").toString().replaceAll("\"", "");
                            invoiceUrl[dbInsertCnt]	= OrderResponse.get("invoiceUrl").toString().replaceAll("\"", "");
                            dbInsertCnt++;
                            
                            //초기화
                            carGo.setLength(0);
            			}
            		}
            	}
            	
            	Map<String, Object> modelDt = new HashMap<String, Object>();
            	modelDt.put("I_ORD_ID"			, ordId);
            	modelDt.put("I_ORD_SEQ"			, ordSeq);
                modelDt.put("I_DLV_INVC_NO"		, dlvInvcNo);
                modelDt.put("I_PRINT_URL"		, printUrl);
                modelDt.put("I_INVOICE_URL"		, invoiceUrl);
                modelDt.put("I_DLV_COM_CD"		, dlvCompCd);
                modelDt.put("I_ORD_DATE"		, ordDate);
                modelDt.put("I_BOX_TYPE_CD"		, boxTypeCd);
                
                modelDt.put("I_LC_ID"		, (String)model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                
                modelDt = (Map<String, Object>)dao.saveSfDataInsert(modelDt);
    			ServiceUtil.isValidReturnCode("WNSOP030", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            

        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : saveSfDataCancel
     * 대체 Method 설명    : 취소
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSfDataCancel(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	int temCnt = Integer.parseInt(model.get("selectIds").toString());
        	
            if(temCnt > 0){
            	for(int i = 0 ; i < temCnt ; i ++){
            		JaxWsProxyFactoryBean svr = new JaxWsProxyFactoryBean();
            		svr.setServiceClass(OrderWebService.class);	
            		svr.setAddress("http://osms.sf-express.com/osms/services/OrderWebService?wsdl");
            		OrderWebService orderWebService = svr.create(OrderWebService.class);
            		StringBuffer sb      =  new StringBuffer();
            		//주문취소
            		sb.append("<?xml version='1.0' encoding='utf-8'?>");
            		sb.append("<Request service='CancelOrderService' lang='zh-CN'>");
            		sb.append("<Head>OSMS_8269</Head>");
            		sb.append("<Body>");
            		sb.append("<CancelOrder mailno='"+(String)model.get("INVC_NO"+i)+"'/>"); //mailno
            		sb.append("</Body>");
            		sb.append("</Request>");
            		
            		//고정 시작
            		String xml = sb.toString();
            		//System.out.println("xml>>*****************");
            		System.out.println("xml>> : " + xml);
            		//System.out.println("xml>>*****************");
            		
            		String checkWord ="561b32d1798e40cc";
            		String CustomerCode ="OSMS_8269"; 
            		String data = encodeBase64(xml);		
            		String md5 = DigestUtils.md5Hex(xml + checkWord);
            		String validateStr = encodeBase64(md5);
            		String result = orderWebService.sfexpressService(data, validateStr, CustomerCode);
            		//System.out.println("result>> : " + result);
            		//고정 끝
            		
            		JSONObject xmlJSONObj = XML.toJSONObject(result, true);
                    String jsonString	  = xmlJSONObj.toString(4);
        			String sendData		  = new StringBuffer().append(jsonString).toString();
        			JsonParser Parser     = new JsonParser();
        			JsonObject jsonObj			= (JsonObject) Parser.parse(sendData);
        			JsonObject jsonObjResponse	= (JsonObject) jsonObj.get("Response");
        			JsonObject jsonObjBody		= (JsonObject) jsonObjResponse.get("CancelOrderResponse");
        			
        			System.out.println("RTN result: "+jsonObjBody.get("result").toString().replaceAll("\"", ""));
        			System.out.println("RTN message: "+jsonObjBody.get("message").toString().replaceAll("\"", ""));
        			
        			if(jsonObjBody.get("result").toString().replaceAll("\"", "").equals("true")){
        				ServiceUtil.isValidReturnCode("WNSOP030", String.valueOf("0"), MessageResolver.getMessage("save.success"));
        			}else{
        				ServiceUtil.isValidReturnCode("WNSOP030", String.valueOf("-1"), jsonObjBody.get("message").toString().replaceAll("\"", ""));
        			}
            	}
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            

        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
	 * Base64
	 * @param orderData
	 * @return
	 */
	private static String encodeBase64(String orderData) {
		String result = "";
		Base64 base64encoder = new Base64();
		try {
			result = new String(base64encoder.encode(orderData.getBytes("UTF-8")),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
     * Method ID   : listByCustCJReturn_setParam
     * Method 설명    : 
     * 작성자               : 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustCJReturn_setParam(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        int temCnt = Integer.parseInt(model.get("selectIds").toString());
        List<String> tranParcelIdArr  = new ArrayList();
        List<String> tranParcelSeqArr = new ArrayList();
        if(temCnt > 0){
            for(int i = 0 ; i < temCnt ; i ++){
            	tranParcelIdArr.add((String)model.get("PARCEL_ID"+i));
            	tranParcelSeqArr.add((String)model.get("PARCEL_SEQ"+i));
            }
        }
        model.put("tranParcelIdArr", tranParcelIdArr);
        model.put("tranParcelSeqArr", tranParcelSeqArr);
        
        map.put("LIST", dao.listByCustCJ_setParam(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : cjReturnListInsertToWMSDF020
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> cjReturnListInsertToWMSDF020(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	//json Parser
        	Gson gson 			= new Gson();
			String jsonString	= gson.toJson(model);
			String sendData	= new StringBuffer().append(jsonString).toString();
			
			JsonParser Parser			= new JsonParser();
			JsonObject jsonObj		= (JsonObject) Parser.parse(sendData);
			JsonObject jsonObjList	= (JsonObject) jsonObj.get("LIST");
			JsonArray dataSetArr	= (JsonArray) jsonObjList.get("list");
            
			//data Size
			int tmpCnt = dataSetArr.size();
			if(tmpCnt > 0){
				//return param 초기화
	            //String[] parcelId	= new String[tmpCnt];
	            //String[] parcelse	= new String[tmpCnt];
	            
	            //package param 초기화
	            String[] custId 			= new String[tmpCnt];
	            String[] rcptYmd 			= new String[tmpCnt];
	            String[] custUseNo			= new String[tmpCnt];
	            String[] rcptDv 			= new String[tmpCnt];
	            String[] workDvCd 			= new String[tmpCnt];
	            
	            String[] reqDvCd 			= new String[tmpCnt];
	            String[] mpckKey 			= new String[tmpCnt];
	            String[] mpckSeq 			= new String[tmpCnt];
	            String[] calDvCd 			= new String[tmpCnt];
	            String[] frtDvCd 			= new String[tmpCnt];
	            
	            String[] cntrItemCd 		= new String[tmpCnt];
	            String[] boxTypeCd 			= new String[tmpCnt];
	            String[] boxQty 			= new String[tmpCnt];
	            String[] custMgmtDlcmCd 	= new String[tmpCnt];
	            String[] sendrNm 			= new String[tmpCnt];
	            
	            String[] sendrTelNo1 		= new String[tmpCnt];
	            String[] sendrTelNo2 		= new String[tmpCnt];
	            String[] sendrTelNo3 		= new String[tmpCnt];
	            String[] sendrZipNo 		= new String[tmpCnt];
	            String[] sendrAddr 			= new String[tmpCnt];
	            
	            String[] sendrDetailAddr	= new String[tmpCnt];
	            String[] rcvrNm 			= new String[tmpCnt];
	            String[] rcvrTelNo1 		= new String[tmpCnt];
	            String[] rcvrTelNo2 		= new String[tmpCnt];
	            String[] rcvrTelNo3 		= new String[tmpCnt];
	            
	            String[] rcvrZipNo 			= new String[tmpCnt];
	            String[] rcvrAddr 			= new String[tmpCnt];
	            String[] rcvrDetailAddr 	= new String[tmpCnt];
	            String[] invcNo				= new String[tmpCnt];
	            String[] oriInvcNo 			= new String[tmpCnt];
	            
	            String[] oriOrdNo 			= new String[tmpCnt];
	            String[] prtSt 				= new String[tmpCnt];
	            String[] gdsCd 				= new String[tmpCnt];
	            String[] gdsNm 				= new String[tmpCnt];
	            String[] dlvDv 				= new String[tmpCnt];
	            
	            String[] rcptErrYn 			= new String[tmpCnt];
	            String[] eaiPrgsSt 			= new String[tmpCnt];
	            String[] ordDesc 			= new String[tmpCnt];
	            
	            for (int i = 0; i < tmpCnt; i++) {
					JsonObject object = (JsonObject) dataSetArr.get(i);
					
					custId[i] 				= object.get("CUST_ID").toString().replaceAll("\"", "");
					rcptYmd[i] 				= object.get("RCPT_YMD").toString().replaceAll("\"", "");
					custUseNo[i] 			= "R"+object.get("CUST_USE_NO").toString().replaceAll("\"", "");/*반품:앞에 R 붙이기*/
					rcptDv[i] 				= "02".toString().replaceAll("\"", "");/*반품:02*///object.get("RCPT_DV").toString().replaceAll("\"", "");
					workDvCd[i] 			= object.get("WORK_DV_CD").toString().replaceAll("\"", "");
					
					reqDvCd[i] 				= object.get("REQ_DV_CD").toString().replaceAll("\"", "");
					mpckKey[i] 				= "R"+object.get("MPCK_KEY").toString().replaceAll("\"", "");/*반품:앞에 R 붙이기*/
					mpckSeq[i] 				= object.get("MPCK_SEQ").toString().replaceAll("\"", "");
					calDvCd[i] 				= object.get("CAL_DV_CD").toString().replaceAll("\"", "");
					frtDvCd[i] 				= object.get("FRT_DV_CD").toString().replaceAll("\"", "");
					
					cntrItemCd[i] 			= object.get("CNTR_ITEM_CD").toString().replaceAll("\"", "");
					boxTypeCd[i] 			= object.get("BOX_TYPE_CD").toString().replaceAll("\"", "");
					boxQty[i] 				= object.get("BOX_QTY").toString().replaceAll("\"", "");
					custMgmtDlcmCd[i] 		= object.get("CUST_MGMT_DLCM_CD").toString().replaceAll("\"", "");
					sendrNm[i] 				= object.get("RCVR_NM").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					
					sendrTelNo1[i] 			= object.get("RCVR_TEL_NO1").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					sendrTelNo2[i] 			= object.get("RCVR_TEL_NO2").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					sendrTelNo3[i] 			= object.get("RCVR_TEL_NO3").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					sendrZipNo[i] 			= object.get("RCVR_ZIP_NO").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					sendrAddr[i] 			= object.get("RCVR_ADDR").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					
					sendrDetailAddr[i] 		= object.get("RCVR_DETAIL_ADDR").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					rcvrNm[i] 				= object.get("SENDR_NM").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					rcvrTelNo1[i] 			= object.get("SENDR_TEL_NO1").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					rcvrTelNo2[i] 			= object.get("SENDR_TEL_NO2").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					rcvrTelNo3[i] 			= object.get("SENDR_TEL_NO3").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					
					rcvrZipNo[i] 			= object.get("SENDR_ZIP_NO").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					rcvrAddr[i]			 	= object.get("SENDR_ADDR").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					rcvrDetailAddr[i] 		= object.get("SENDR_DETAIL_ADDR").toString().replaceAll("\"", "");/*반품:수신발신 반대로*/
					invcNo[i] 				= "";
					oriInvcNo[i] 			= object.get("INVC_NO").toString().replaceAll("\"", "");/*반품:원송장번호로 입력*/
					
					oriOrdNo[i] 			= object.get("CUST_USE_NO").toString().replaceAll("\"", "");/*반품:원주문번호로 입력*/
					prtSt[i] 				= object.get("PRT_ST").toString().replaceAll("\"", "");
					gdsCd[i] 				= object.get("GDS_CD").toString().replaceAll("\"", "");
					gdsNm[i] 				= object.get("GDS_NM").toString().replaceAll("\"", "");
					dlvDv[i] 				= object.get("DLV_DV").toString().replaceAll("\"", "");
					
					rcptErrYn[i] 			= object.get("RCPT_ERR_YN").toString().replaceAll("\"", "");
					eaiPrgsSt[i] 			= object.get("EAI_PRGS_ST").toString().replaceAll("\"", "");
					ordDesc[i] 				= object.get("ORD_DESC").toString().replaceAll("\"", "");
	            }
	            
	            //프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();
	            modelIns.put("I_CUST_ID"			,custId);
	            modelIns.put("I_RCPT_YMD"			,rcptYmd);
	            modelIns.put("I_CUST_USE_NO"		,custUseNo);
	            modelIns.put("I_RCPT_DV"			,rcptDv);
	            modelIns.put("I_WORK_DV_CD"			,workDvCd);
	            
	            modelIns.put("I_REQ_DV_CD"			,reqDvCd);
	            modelIns.put("I_MPCK_KEY"			,mpckKey);
	            modelIns.put("I_MPCK_SEQ"			,mpckSeq);
	            modelIns.put("I_CAL_DV_CD"			,calDvCd);
	            modelIns.put("I_FRT_DV_CD"			,frtDvCd);
	            
	            modelIns.put("I_CNTR_ITEM_CD"		,cntrItemCd);
	            modelIns.put("I_BOX_TYPE_CD"		,boxTypeCd);
	            modelIns.put("I_BOX_QTY"			,boxQty);
	            modelIns.put("I_CUST_MGMT_DLCM_CD"	,custMgmtDlcmCd);
	            modelIns.put("I_SENDR_NM"			,sendrNm);
	            
	            modelIns.put("I_SENDR_TEL_NO1"		,sendrTelNo1);
	            modelIns.put("I_SENDR_TEL_NO2"		,sendrTelNo2);
	            modelIns.put("I_SENDR_TEL_NO3"		,sendrTelNo3);
	            modelIns.put("I_SENDR_ZIP_NO"		,sendrZipNo);
	            modelIns.put("I_SENDR_ADDR"			,sendrAddr);
	            
	            modelIns.put("I_SENDR_DETAIL_ADDR"	,sendrDetailAddr);
	            modelIns.put("I_RCVR_NM"			,rcvrNm);
	            modelIns.put("I_RCVR_TEL_NO1"		,rcvrTelNo1);
	            modelIns.put("I_RCVR_TEL_NO2"		,rcvrTelNo2);
	            modelIns.put("I_RCVR_TEL_NO3"		,rcvrTelNo3);
	            
	            modelIns.put("I_RCVR_ZIP_NO"		,rcvrZipNo);
	            modelIns.put("I_RCVR_ADDR"			,rcvrAddr);
	            modelIns.put("I_RCVR_DETAIL_ADDR"	,rcvrDetailAddr);
	            modelIns.put("I_INVC_NO"			,invcNo);
	            modelIns.put("I_ORI_INVC_NO"		,oriInvcNo);
	            
	            modelIns.put("I_ORI_ORD_NO"			,oriOrdNo);
	            modelIns.put("I_PRT_ST"				,prtSt);
	            modelIns.put("I_GDS_CD"				,gdsCd);
	            modelIns.put("I_GDS_NM"				,gdsNm);
	            modelIns.put("I_DLV_DV"				,dlvDv);
	            
	            modelIns.put("I_RCPT_ERR_YN"		,rcptErrYn);
	            modelIns.put("I_EAI_PRGS_ST"		,eaiPrgsSt);
	            modelIns.put("I_ORD_DESC"			,ordDesc);

	            //session 및 등록정보
	            modelIns.put("I_IF_ID"			, (String)model.get("IF_ID"));
	            modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
	            modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));

	            //dao
	            modelIns = (Map<String, Object>)dao.saveCjReturnInsert(modelIns);
    	        ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
			}else{
				m.put("errCnt", -2);
	            m.put("MSG", "주문 할 자료가 없습니다. (e.-2)");
			}
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	/**
     * 
     * 대체 Method ID   : cjReturnListUpdateToWMSDF020
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> cjReturnListUpdateToWMSDF020(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("I_IF_ID"			, (String)model.get("vrSrchIfId"));
            modelIns.put("I_CUST_ID"		, (String)model.get("CUST_ID")); //CJ고객코드 CUST_ID
            
            //session 및 등록정보
            modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
            
            //dao
            modelIns = (Map<String, Object>)dao.saveCjReturnUpdate(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	/**
     * 대체 Method ID   : invoicPdfDownloadSF
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> invoicPdfDownloadSF(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String rtnUrl = null;
        
        try{
        	int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){           	
            	for(int i = 0 ; i < temCnt ; i ++){
            		JaxWsProxyFactoryBean svr = new JaxWsProxyFactoryBean();
            		svr.setServiceClass(OrderWebService.class);	
            		svr.setAddress("http://osms.sf-express.com/osms/services/OrderWebService?wsdl");
            		OrderWebService orderWebService = svr.create(OrderWebService.class);
            		StringBuffer sb      =  new StringBuffer();
            		sb.append("<?xml version='1.0' encoding='utf-8'?>");
            		sb.append("<Request service='PrintOrderService'>");
            		sb.append("<Head>OSMS_8269</Head>");
            		sb.append("<Body>");
            		sb.append("<PrintOrder");
            		sb.append("	printType='1'");
            		//sb.append("	mailno='SF1011635620951,SF1011635621168'");
            		sb.append("	mailno='"+(String)model.get("INVC_NO"+i)+"'");
            		sb.append("/>");
            		sb.append("</Body>");
            		sb.append("</Request>");
            		
            		/*고정 시작*/
            		String xml = sb.toString();
            		//System.out.println("xml>>*****************");
            		//System.out.println("xml>> : " + xml);
            		//System.out.println("xml>>*****************");
            		String checkWord ="561b32d1798e40cc";
            		String CustomerCode ="OSMS_8269"; 
            		String data = encodeBase64(xml);		
            		String md5 = DigestUtils.md5Hex(xml + checkWord);
            		String validateStr = encodeBase64(md5);
            		String result = orderWebService.sfexpressService(data, validateStr, CustomerCode);
            		//System.out.println("result>> : " + result);
            		
            		/*고정 끝*/
            		JSONObject xmlJSONObj = XML.toJSONObject(result, true);
                    String jsonString	  = xmlJSONObj.toString(4);
        			String sendData		  = new StringBuffer().append(jsonString).toString();
        			JsonParser Parser     = new JsonParser();
        			JsonObject jsonObj			= (JsonObject) Parser.parse(sendData);
        			JsonObject jsonObjResponse	= (JsonObject) jsonObj.get("Response");
        			JsonObject jsonObjBody		= (JsonObject) jsonObjResponse.get("Body");
        			JsonObject OrderResponse	= (JsonObject) jsonObjBody.get("PrintOrderResponse");
        			
        			//System.out.println("RTN Url: "+OrderResponse.get("url").toString().replaceAll("\"", ""));
        			rtnUrl = OrderResponse.get("url").toString().replaceAll("\"", "");
            	}
            	
    			m.put("rtnUrl", rtnUrl);
                m.put("errCnt", 0);
            }else{
            	m.put("rtnUrl", "There is no invoice number requested.");
                m.put("errCnt", -2);
            }
        } catch(Exception e){
        	m.put("rtnUrl", "Server error");
            m.put("errCnt", -1);
            throw e;
        }
        return m;
        
        /*
        try{
        	int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
                PDFMergerUtility pdfMerger = new PDFMergerUtility();
                pdfMerger.setDestinationFileName(ConstantIF.FILE_ATTACH_WINUSTEMP_PATH+"PRINT_TEMP/"+"SFmerged.pdf");

                PDDocumentInformation documentInformation = new PDDocumentInformation();
                documentInformation.setTitle("Apache PdfBox Merge PDF Documents");
                documentInformation.setCreator("memorynotfound.com");
                documentInformation.setSubject("Merging PDF documents with Apache PDF Box");
                
            	for(int i = 0 ; i < temCnt ; i ++){
            		String printUrl = (String)model.get("PRINT_URL"+i);
            		String savePath = ConstantIF.FILE_ATTACH_WINUSTEMP_PATH+"PRINT_TEMP/"
            						+(String)model.get("DLV_COMP_CD"+i)+"/"
            						+(String)model.get("ORD_DATE"+i)+"/"
            						+(String)model.get("ORD_ID"+i)+"/"
            						+(String)model.get("ORD_SEQ"+i)+"/"
            						+(String)model.get("INVC_NO"+i)+".pdf";
            		
            		URL url = new URL(printUrl);
                    File destination_file = new File(savePath);
                    FileUtils.copyURLToFile(url, destination_file);
                    
                    pdfMerger.addSource(new File(savePath));
            	}
            	
            	pdfMerger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
            }
      	
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("PDF", "PRINT_TEMP/"+"SFmerged.pdf");
        } catch(Exception e){
    		m.put("errCnt", -1);
            m.put("MSG", "ERR");  
            throw e;
        } finally {
        	//완료 후 merged.pdf 작업파일 삭제
        	//String path = ConstantIF.TEMP_PATH+"PRINT_TEMP";
        	//deleteFile(path);
        }
        return m;
        */
    }
    
    public static void deleteFile(String path){
    	File deleteFolder = new File(path);
    	
    	if(deleteFolder.exists()){
    	File[] deleteFolderList = deleteFolder.listFiles();
    	
	    	for(int i=0; i < deleteFolderList.length; i++){
		    	if(deleteFolderList[i].isFile()){
		    		deleteFolderList[i].delete();
		    	}else{
		    		deleteFile(deleteFolderList[i].getPath());
		    	}
		    	deleteFolderList[i].delete();
	    	}
	    	deleteFolder.delete();
    	}
    }
    
    /**
     * Method ID   : listByCustSummary
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustSummary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
    	
        if(model.get("vrSrchOrdSubtype").equals("140")){
    		List<String> arrList = new ArrayList();
    		arrList.add("50");
    		arrList.add("132");
	        
	        model.put("vrSrchOrdSubtypeArr", arrList);
    	}
        
        map.put("LIST", dao.listByCustSummary(model));
        return map;
    }
    
    
    /**
     * Method ID   : listByCustDetail
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByCustDetail(model));
        return map;
    }
    
    /**
     * Method ID   : listCountByCust
     * Method 설명    : 화주별 출고관리카운트 조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCountByCust(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("vrSrchOrdSubtype").equals("140")){
    		List<String> arrList = new ArrayList();
    		arrList.add("50");
    		arrList.add("132");
	        
	        model.put("vrSrchOrdSubtypeArr", arrList);
    	}
       
        map.put("LIST", dao.listCountByCust(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : autoBestLocSaveMultiV
     * 대체 Method 설명    : 로케이션추천 자동V3
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoBestLocSaveMultiV3(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
        	
        	
        	List list = dao.rawListByCustSummary(model);
        	
            int tmpCnt = list.size();
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId" , ordId);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.autoBestLocSaveMultiV2(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
     * 
     * 대체 Method ID   : autoDeleteLocSaveV2
     * 대체 Method 설명    : 로케이션지정 삭제
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoDeleteLocSaveV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

        	List list = dao.rawListByCustSummary(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                String[] ordSeq   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                    //log.info("AAAAAAAAAAAAAA:"+(String)model.get("I_ORD_SEQ"+i));
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
                
                
                //dao                
                modelIns = (Map<String, Object>)dao.autoDeleteLocSave(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	     * 
	     * 대체 Method ID   : deleteLocOrdMulti
	     * 대체 Method 설명    : 로케이션지정 삭제(주문번호별)
	     * 작성자                      : schan
	     * @param model
	     * @return
	     * @throws Exception
	     */
	     @Override
	    public Map<String, Object> deleteLocOrdMulti(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
	            int tmpCnt = Integer.parseInt((String)model.get("selectIds"));
	            if(tmpCnt > 0){           
	                String[] ordId   = new String[tmpCnt];
	                
	                for(int i = 0 ; i < tmpCnt ; i ++){
	                    ordId[i]    = (String)model.get("I_ORD_ID"+i);
	                }
	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                modelIns.put("ordId", ordId);

	                //session 및 등록정보
	                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                
	                //dao                
	                modelIns = (Map<String, Object>)dao.deleteLocOrdMulti(modelIns);
	                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
	 
	 /**
	     * 
	     * Method ID   : saveInComplete
	     * Method 설명    : 출고확정
	     * 작성자               : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> saveOutCompleteV2(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
	        	List list = dao.rawListByCustSummary(model);
	        	int tmpCnt = list.size();
	            if(tmpCnt > 0){           
	            
	                String[] ordId  = new String[tmpCnt];                
	                String[] ordSeq = new String[tmpCnt];
	                String[] workQty = new String[tmpCnt];
	                
	                for(int i = 0 ; i < tmpCnt ; i ++){
	                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
	                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
	                    workQty[i]    		= null;  
	                    //System.out.println(ordId[i]+ " " + ordSeq[i] + " " + workQty[i]);
	                    
	                }
	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                modelIns.put("ordId", ordId);
	                modelIns.put("ordSeq", ordSeq);
	                modelIns.put("workQty", workQty);
	                
	                //session 및 등록정보
	                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	                //dao                
	                modelIns = (Map<String, Object>)dao.saveOutComplete(modelIns);
	                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
	    
	    /**
	     * 
	     * Method ID   : saveInComplete
	     * Method 설명    : 출고확정
	     * 작성자               : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> saveOutCompleteV3(Map<String, Object> model) throws Exception {
	    	Map<String, Object> m = new HashMap<String, Object>();
	    	
	    	try{
	    		model.put("vrSrchAddressValid", "1");
	    		List list = dao.rawListByOliveSummary(model);
	    		int tmpCnt = list.size();
	    		if(tmpCnt > 0){           
	    			
	    			String[] ordId  = new String[tmpCnt];                
	    			String[] ordSeq = new String[tmpCnt];
	    			String[] workQty = new String[tmpCnt];
	    			
	    			for(int i = 0 ; i < tmpCnt ; i ++){
	    				ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
	    				ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
	    				workQty[i]    		= null;  
	    				//System.out.println(ordId[i]+ " " + ordSeq[i] + " " + workQty[i]);
	    				
	    			}
	    			//프로시져에 보낼것들 다담는다
	    			Map<String, Object> modelIns = new HashMap<String, Object>();
	    			
	    			modelIns.put("ordId", ordId);
	    			modelIns.put("ordSeq", ordSeq);
	    			modelIns.put("workQty", workQty);
	    			
	    			//session 및 등록정보
	    			modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	    			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	    			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	    			
	    			//dao                
	    			modelIns = (Map<String, Object>)dao.saveOutComplete(modelIns);
	    			ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	    		}
	    		m.put("errCnt", 0);
	    		m.put("MSG", MessageResolver.getMessage("save.success"));
	    		
	    	} catch(BizException be) {
	    		m.put("errCnt", -1);
	    		m.put("MSG", be.getMessage() );
	    		
	    	} catch(Exception e){
	    		throw e;
	    	}
	    	return m;
	    }
	    
	    
	    /**
	     *  Method ID  : updatePickingTotalV2 
	     *  Method 설명  : 토탈피킹리스트발행
	     *  작성자             : KHKIM
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> updatePickingTotalV2(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();
	        try{
	        	
	        	int workStatCnt = 0;
	        	int chkIdsCnt = 0;
	        	List list = dao.rawListByCustSummary(model);
	        	int tmpCnt = list.size();
	            if(tmpCnt > 0){           
	            
		            String[] ordId = new String[tmpCnt];
		            String[] ordSeq = new String[tmpCnt];
		            
		            
		            for(int i = 0 ; i < tmpCnt ; i ++){
	                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
	                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");

	                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
	                    int outOrdQty = Integer.parseInt(String.valueOf(((Map<String, String>)list.get(i)).get("OUT_ORD_QTY")));
	                    
	                  
	                    if(Integer.parseInt(workStat) == 450){
	            			workStatCnt++;
	            		}
	            		
	            		if(Integer.parseInt(workStat) < 230){
	            			if(0 < outOrdQty){
	            				chkIdsCnt++;
	            			}
	            		}
	                  
	                    
	                	//System.out.println(ordId[i] + "::::" + ordSeq[i]);
	                }
		            
		            if(workStatCnt > 0){
		            	if(tmpCnt != workStatCnt){
		            		String msg = "매핑 작업시 모든 상태가 매핑 이어야 합니다.";
		            		throw new BizException(msg);
		            	}
		            }
		            
		            if(chkIdsCnt > 0){
		        		String msg = "피킹리스트발행시로케이션지정은필수입니다";
		        		throw new BizException(msg);
		            }
		            
		            // 프로시져에 보낼것들 다담는다
		            Map<String, Object> modelIns = new HashMap<String, Object>();
	
		            modelIns.put("ordId", ordId);
		            modelIns.put("ordSeq", ordSeq);
	
		            // session 및 등록정보
		            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
		            // dao
		            modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
		            ServiceUtil.isValidReturnCode("WMOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
	  	    	
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("list.success"));
	            
	        } catch(BizException be) {
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", be);
				}       	
	            m.put("errCnt", 1);
	            m.put("MSG", be.getMessage() );
	            
	        }catch(Exception e){
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
	            m.put("errCnt", 1);
	            m.put("MSG", MessageResolver.getMessage("save.error") );
	        }
	        return m;
	    
	    }
	    
	    /**
	     * 
	     * Method ID   : batch
	     * Method 설명    : 배치
	     * 작성자               : KHKIM
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> batch(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
	        	
	        	
	        	
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //modelIns.put("ordId", ordId);
                //modelIns.put("ordSeq", ordSeq);
                //modelIns.put("workQty", workQty);

                
                //session 및 등록정보
                modelIns.put("BATCH_DATE", (String)model.get("batchDate"));
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                System.out.println(modelIns.toString());

                modelIns = (Map<String, Object>)dao.batch(modelIns);
                
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
	    
	    /**
	     * 
	     * Method ID   : listByCustNoCount
	     * Method 설명    : listByCustNoCount
	     * 작성자               : KHKIM
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> listByCustNoCount(Map<String, Object> model) throws Exception {
	    	Map<String, Object> map = new HashMap<String, Object>();
	    	
	    	/*
	    	 * 
	    	 * 출고관리 신규 조회화면 -> 주문상세 (검색조건) -> 유통점출고(재고이동포함) 선택했을 경우 자동으로 132(유통점출고), 50(재고이동) 2개 in조건 걸수있도록 수정
	    	 * 
	    	 */
	    	if(model.get("vrSrchOrdSubtype").equals("140")){
	    		List<String> arrList = new ArrayList();
	    		arrList.add("50");
	    		arrList.add("132");
		        
		        model.put("vrSrchOrdSubtypeArr", arrList);
	    	}
	        
	        map.put("LIST", dao.listCountByCustNoCount(model));
	        return map;
	    }
	    
	    /**
	     * 
	     * Method ID   : listExcel
	     * Method 설명    : 입출고현황 엑셀용조회
	     * 작성자                      : chsong
	     * @param   model
	     * @return
	     * @throws  Exception
	     */
	    @Override
	    public Map<String, Object> pickingListExcel(Map<String, Object> model) throws Exception {
	    	Map<String, Object> map = new HashMap<String, Object>();
			model.put("pageIndex", "1");
			model.put("pageSize", "60000");
			map.put("LIST", dao.pickingList(model));
			return map;
	    }
	    
	    /**
	     * 
	     * 대체 Method ID   : outInvalidView
	     * 대체 Method 설명    :
	     * 작성자                      : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
		public Map<String, Object> outInvalidView(Map<String, Object> model) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String srchKey = (String) model.get("srchKey");
			if (srchKey.equals("DS_INVALID")) {
				map.put("DS_INVALID", dao.outInvalidView(model));
			}
			return map;
		}
		
		/**
	     *  Method ID  : confirmPickingTotal 
	     *  Method 설명  : 토탈피킹리스트 확정
	     *  작성자             : KHKIM
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> confirmPickingTotal(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();
	        try{
	        	
	        	List list = dao.rawListByCustSummary(model);
	        	int tmpCnt = list.size();
	            int workStatCnt = 0;
	        	if(tmpCnt > 0){           
	            
		            String[] ordId = new String[tmpCnt];
		            String[] ordSeq = new String[tmpCnt];
		            
		            
		            for(int i = 0 ; i < tmpCnt ; i ++){
	                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
	                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
	                	

	                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
	                    
	                  
	                    if(Integer.parseInt(workStat) < 230 || Integer.parseInt(workStat) > 990){
	            			workStatCnt++;
	            		}
	            		
	                    
	                	//System.out.println(ordId[i] + "::::" + ordSeq[i]);
	                }
		            
		            if(workStatCnt > 0){
		            	
	            		String msg = "모든 상태가  피킹리스트 발행 이어야 합니다.";
	            		throw new BizException(msg);
		          
		            }

	
		            // 프로시져에 보낼것들 다담는다
		            Map<String, Object> modelIns = new HashMap<String, Object>();
	
		            modelIns.put("ordId", ordId);
		            modelIns.put("ordSeq", ordSeq);
		            
	                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
		            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
		            // dao
		            modelIns = (Map<String, Object>)dao.confirmPickingTotal(modelIns);
		            ServiceUtil.isValidReturnCode("WMOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
	  	    	
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("list.success"));
	            
	        } catch(BizException be) {
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", be);
				}       	
	            m.put("errCnt", 1);
	            m.put("MSG", be.getMessage() );
	            
	        }catch(Exception e){
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
	            m.put("errCnt", 1);
	            m.put("MSG", MessageResolver.getMessage("save.error") );
	        }
	        return m;
	    
	    }
	    
	    /*-
		 * Method ID    : WMSOP030E11
		 * Method 설명   : 운영관리 > 출고관리 > 파일업로드 화면 및 기능
		 * 작성자         : KCR
		 * @param   model
		 * @return  
		 */
	    
	    public Map<String, Object> saveFile(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			String flag = (String) model.get("FLAG");
			
			// log.info("############ flag :" + flag);

//			if ("".equals(flag)) { // 등록
				List<String> fileMngNoList = null;
				if ( model.get("D_ATCH_FILE_NAME") != null ) {	
					Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
					Object attachFileState = model.get("FILE_STATE");
					fileMngNoList = new ArrayList<String>();
										
					if (attachFileInfo instanceof String[]) {
						for (int i=0; i< ((String[])attachFileInfo).length; i++) {							
							String str = ((String[])attachFileInfo)[i];
							String state = ((String[])attachFileState)[i];
							
							// log.info("############ state :" + state);
							
							if ( str != null && StringUtils.isNotEmpty(str)
									&& state != null && "I".equals(state) ) {
								
								Map<String, Object> modelDt = new HashMap<String, Object>();
								
//								String fileRoute = ((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
//								String fileSize = ((String[])model.get("FILE_SIZE"))[i];								
								String ext = "." + str.substring((str.lastIndexOf('.') + 1));							
//								String fileId = CommonUtil.getLocalDateTime() + i + ext;
								String date = CommonUtil.getToday();
								String ordId = (String)model.get("workOrdId"); // "";
								
								String path = ConstantWSIF.WMS_IMG_PATH + DateUtil.getDateByPattern2() +"/"+ordId+"_"+date;
//								String path = ConstantWSIF.WMS_IMG_PATH + DateUtil.getDateByPattern2() +"/"+ key;
								
								
//								modelDt.put("FILE_ID", fileId);
								modelDt.put("FILE_ID", ordId+"_"+date+String.format("photo%d.jpg", i));
								modelDt.put("ATTACH_GB", "WMS");
								modelDt.put("FILE_VALUE", "WMSOP");
								modelDt.put("FILE_PATH", path);
//								modelDt.put("ORG_FILENAME", str);
								modelDt.put("ORG_FILENAME", String.format("photo%d.jpg", i));
								modelDt.put("FILE_EXT", ext);
								modelDt.put("WORK_ORD_ID", ordId);
								
								System.out.println("path?? ------ "+path);
//								modelDt.put("FILE_SIZE", fileSize);
								System.out.println("여기찍힘???");
								String fileMngNo = (String) dao.fileUpload(modelDt);
								
								// log.info("############ fileMngNo :" + fileMngNo);
								fileMngNoList.add(fileMngNo);
							}
						}
					} 
				}
				
//				if (fileMngNoList != null) {
//					int fileSize = fileMngNoList.size();
//					switch (fileSize){
//						case 1:
//							model.put("ATTACH_FILE_NO", fileMngNoList.get(0)); 
//							break;						
//						case 2:
//							model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
//							model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
//							break;
//						case 3:
//							model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
//							model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
//							model.put("ATTACH_FILE_NO_2", fileMngNoList.get(2));
//							break;
//						default:							
//							break;
//					}
//				}
//				
				// log.info("############ model :" + model);
//				String boardIdx = (String) dao.insert(model);
				
				// log.info("############ boardIdx :" + boardIdx);
//				if ( boardIdx != null ) {
					
//					List<Map<String, Object> > custInfoList = null;
//					String disType = (String)model.get("DIS_GBN");
//				}

				map.put("MSG", MessageResolver.getMessage("insert.success"));
//			}
		} catch (Exception e) {
			throw e;
		}
		return map;
	}    
	    
	/*-
	 * Method ID    : WMSOP030
	 * Method 설명   : 운영관리 > 출고관리 > 작업중 해제
	 * 작성자         : MonkeySeok
	 * 날짜 : 2021-02-18
	 * @param   model
	 * @return  
	 */
	@Override
	public Map<String, Object> workingUnlock(Map<String, Object> model) throws Exception {
	    Map<String, Object> m = new HashMap<String, Object>();
	
	    try{
	
	        int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	        if(tmpCnt > 0){
	            String[] ordId  = new String[tmpCnt];                
	            String[] ordSeq = new String[tmpCnt];
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
	                ordId[i]    = (String)model.get("ORD_ID"+i);               
	                ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
	            }
	            
	            //프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();
	            
	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);
	            
	            //session 및 등록정보
	            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
	            //dao                
	            modelIns = (Map<String, Object>)dao.workingUnlock(modelIns);
	            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	        }
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
	        
	    } catch(BizException be) {
	        m.put("errCnt", -1);
	        m.put("MSG", be.getMessage() );
	        
	    } catch(Exception e){
	        throw e;
	    }
	    return m;
	}
	
	/*-
	 * Method ID    : totalWorkingUnlock
	 * Method 설명   : 출고관리(올리브영) 헤더 테이블 작업중 해제
	 * 작성자         : Seongjun kwon
	 * 날짜 : 2021-08-12
	 * @param   model
	 * @return  
	 */
	@Override
	public Map<String, Object> totalWorkingUnlock(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
	    Map<String, Object> m = new HashMap<String, Object>();
	    try{
	    	String [] ordIds   = model.get("vrOrdId").toString().split(",");
	    	
	    	for(int i = 0; i < ordIds.length; ++i){
	    		map.put("vrOrdId", ordIds[i]);
	    		dao.totalWorkingUnlock(map);
	    	}
	    
	    	m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
	    }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
	    }
	    
	    return m; 
	}
	    
	/*-
	 * Method ID	: getPickingLog
	 * Method 설명	: 피킹리스트 LOG 발행 리스트 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> getPickingLog(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("PICKINGLOG", dao.getPickingLog(model));
		return map;
	}
    
    /**
     * Method ID   : listByOlive
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByOlive(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listByOlive(model));
        return map;
    }
    
    /*-
	 * Method ID	: productOliveSoldOut
	 * Method 설명	: 피킹리스트 LOG 발행 리스트 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    @Override
    public Map<String, Object> productOliveSoldOut(Map<String, Object> model){
		Map<String, Object> map = new HashMap<String, Object>();
		
		int count = Integer.parseInt((String)model.get("selectIds"));
		List<String> list = new ArrayList<>();
		int alocQty = 0;
		for(int i = 0; i < count; i++){
			int outOrdQty = Integer.parseInt((String)model.get("OUT_ORD_QTY" + i));
			Map<String, Object> locateMap = new HashMap<String, Object>();
			String ritemId = (String)model.get("RITEM_ID" + i);
			String ordId = (String)model.get("ORD_ID" + i);
			String ordSeq = (String)model.get("ORD_SEQ" + i);
			locateMap.put("ordId", ordId);
			locateMap.put("ordSeq", ordSeq);
			locateMap.put("ritemId", ritemId);
			locateMap.put("vrSrchCustId", (String)model.get("vrSrchCustId"));
			locateMap.put("SS_SVC_NO", (String)model.get("SS_SVC_NO"));
			locateMap.put("SS_USER_NO", (String)model.get("SS_USER_NO"));
			//int locatedQty = dao.locatedQty(locateMap);
			
			//alocQty = outOrdQty;
			/*
			if(locatedQty <= 0){
				alocQty = outOrdQty;
			}else{
				alocQty = outOrdQty - locatedQty;
			}
			*/
			locateMap.put("alocQty", outOrdQty);
			dao.productOliveSoldOut(locateMap);
		}
		return map;
	}
    /*-
     * Method ID	: cancelProductOliveSoldOut
     * Method 설명	: 피킹리스트 LOG 발행 리스트 조회
     * 작성자      : 기드온
     * @param 
     * @return
     */
    @Override
    public Map<String, Object> cancelProductOliveSoldOut(Map<String, Object> model){
    	Map<String, Object> map = new HashMap<String, Object>();
    	int count = Integer.parseInt((String)model.get("selectIds"));
		List<String> list = new ArrayList<>();
		for(int i = 0; i < count; i++){
			Map<String, Object> locateMap = new HashMap<String, Object>();
			String ordId = (String)model.get("ORD_ID" + i);
			String ordSeq = (String)model.get("ORD_SEQ" + i);
			locateMap.put("ordId", ordId);
			locateMap.put("ordSeq", ordSeq);
			locateMap.put("SS_SVC_NO", (String)model.get("SS_SVC_NO"));
			locateMap.put("SS_USER_NO", (String)model.get("SS_USER_NO"));
			dao.cancelProductOliveSoldOut(locateMap);
		}
    	return map;
    }
    /**
     * Method ID   : listByCustSummary
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByOliveSummary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        String vrSrchOrgOrdIdList = model.get("vrSrchOrgOrdIdList").toString();
        if (!vrSrchOrgOrdIdList.isEmpty()) {
        	String[] orgOrdIdList = vrSrchOrgOrdIdList.trim().split("\r");
        	model.put("orgOrdIdList", orgOrdIdList);
        }
        
        List<Object> oliveOrderList = selectOliveListByWcsWork(model);
        
        GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(oliveOrderList);
        map.put("LIST", genericResultSet);
        return map;
    }

    /*
     * wcs 작업상태 올리브영 쿼리랑 매핑
     * */
	private List<Object> selectOliveListByWcsWork(Map<String, Object> model) {
		String vrSrchOrderPhase  = (String) model.get("vrSrchOrderPhase");//03 작업완료,04 전체 ,31 피킹확정
		List<Object> oliveOrderList = dao.rawListByOliveSummary(model);
        switch (vrSrchOrderPhase) {
        	case "03":
            case "04":
            case "31":
            	List<Map<String, Object>> wcsWorkList = oliveWcsApiDao.selectWorkList(model);
                Map<String, Object> wcsWorkMap = new HashMap<>();
                for (Map<String, Object> work : wcsWorkList) {
                	wcsWorkMap.put((String) work.get("ORDR_NO"), (String)work.get("ORDR_STAT_NM"));
                }
                for (Object orderObj : oliveOrderList) {
        			Map<String, Object> order = (Map<String, Object>) orderObj;
        			String orgOrdId = (String) order.get("ORG_ORD_ID");
        			order.put("STATUS", wcsWorkMap.get(orgOrdId));
        		}
                break;
        }
       
		return oliveOrderList;
	}
    
    
    /**
     * Method ID   : listByCustDetail
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByOliveDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("LIST", dao.listByOliveDetail(model));
        return map;
    }
    
    /**
     * Method ID   : listCountByCust
     * Method 설명    : 화주별 출고관리카운트 조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCountByOlive(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        String vrSrchOrgOrdIdList = model.get("vrSrchOrgOrdIdList").toString();
        if (!vrSrchOrgOrdIdList.isEmpty()) {
        	String[] orgOrdIdList = vrSrchOrgOrdIdList.trim().split("\r");
        	model.put("orgOrdIdList", orgOrdIdList);
        }
        
        map.put("LIST", dao.listCountByOlive(model));
        return map;
    }
    
    /** 
     * 
     * 대체 Method ID   : autoBestLocSaveMultiV
     * 대체 Method 설명    : 로케이션추천 자동V3
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoBestLocSaveMultiOlive(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();


  	  
        try{
        	
            int selectIdsCnt = Integer.parseInt(model.get("selectIds").toString());
          	 
            String ordIds = "";
            
			
        	  if(selectIdsCnt > 0){
              	String ordId = "";  
                  
                  for(int i = 0 ; i < selectIdsCnt ; i ++){
                  	
                	  ordId = (String)model.get("ORD_ID_"+i);
                	  ordIds += "'" + ordId + "'";
                	  if(i < selectIdsCnt -1 )
                	  {
                		  ordIds += ",";
                	  }
                  }
                  model.put("ORD_IDs", ordIds);
              }
        	  
        	  
        	model.put("vrSrchAddressValid", "1");
        	List list = dao.rawListByOliveSummary(model);
        	
            int tmpCnt = list.size();
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId" , ordId);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.autoBestLocSaveMultiV2(modelIns);
                
                /*
                if(model.get("vrSrchOrderPhase") == null || "01".equals((String)model.get("vrSrchOrderPhase")) || "04".equals((String)model.get("vrSrchOrderPhase"))){
                	model.put("vrSrchOrderPhase", "01");
                	model.put("vrOrdSeqNeed", "Y");
                	
                	
                	List failedList = dao.rawListByCustSummary(model);
                	for(int i = 0; i < failedList.size(); i++){
                		String failedOrdId = (String)((Map<String, String>)failedList.get(i)).get("ORD_ID");
                		String failedOrdSeq = (String)((Map<String, String>)failedList.get(i)).get("ORD_SEQ");
                		
                		System.out.println(failedOrdId + "  " + failedOrdSeq);
                		Map<String, Object> locateMap = new HashMap<String, Object>();
                		locateMap.put("ordId", failedOrdId);
                		locateMap.put("ordSeq", failedOrdSeq);
                		locateMap.put("SS_SVC_NO", (String)model.get("SS_SVC_NO"));
                		locateMap.put("SS_USER_NO", (String)model.get("SS_USER_NO"));
                		//dao.productOliveSoldOut(locateMap);
                	}
                	
                	
                }
                */
                
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            

        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	     *  Method ID  : updatePickingTotalV2 
	     *  Method 설명  : 토탈피킹리스트발행
	     *  작성자             : KHKIM
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> updatePickingTotalOlive(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();
	        try{
	        	
	        	int workStatCnt = 0;
	        	int chkIdsCnt = 0;
	        	model.put("vrSrchAddressValid", "1");
	        	List list = dao.rawListByOliveSummary(model);
	        	int tmpCnt = list.size();
	            if(tmpCnt > 0){           
	            
		            String[] ordId = new String[tmpCnt];
		            String[] ordSeq = new String[tmpCnt];
		            
		            
		            for(int i = 0 ; i < tmpCnt ; i ++){
	                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
	                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");

	                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
	                    int outOrdQty = Integer.parseInt(String.valueOf(((Map<String, String>)list.get(i)).get("OUT_ORD_QTY")));
	                    
	                  
	                    if(Integer.parseInt(workStat) == 450){
	            			workStatCnt++;
	            		}
	            		
	            		if(Integer.parseInt(workStat) < 230){
	            			if(0 < outOrdQty){
	            				chkIdsCnt++;
	            			}
	            		}
	                  
	                    
	                	//System.out.println(ordId[i] + "::::" + ordSeq[i]);
	                }
		            
		            if(workStatCnt > 0){
		            	if(tmpCnt != workStatCnt){
		            		String msg = "매핑 작업시 모든 상태가 매핑 이어야 합니다.";
		            		throw new BizException(msg);
		            	}
		            }
		            
		            if(chkIdsCnt > 0){
		        		String msg = "피킹리스트발행시로케이션지정은필수입니다";
		        		throw new BizException(msg);
		            }
		            
		            // 프로시져에 보낼것들 다담는다
		            Map<String, Object> modelIns = new HashMap<String, Object>();
	
		            modelIns.put("ordId", ordId);
		            modelIns.put("ordSeq", ordSeq);
	
		            // session 및 등록정보
		            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
		            // dao
		            modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
		            ServiceUtil.isValidReturnCode("WMOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
	  	    	
	            }
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("list.success"));
	            
	        } catch(BizException be) {
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", be);
				}       	
	            m.put("errCnt", 1);
	            m.put("MSG", be.getMessage() );
	            
	        }catch(Exception e){
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
	            m.put("errCnt", 1);
	            m.put("MSG", MessageResolver.getMessage("save.error") );
	        }
	        return m;
	    
	    }
	    
	    /**
         *  Method ID  : updatePickingTotalV2 1000건 제한으로 인한 uuid 조회 
         *  Method 설명  : 토탈피킹리스트발행
         *  작성자             : KHKIM(수정),YSI
         * @param model
         * @return
         * @throws Exception
         */
        @Override
        public Map<String, Object> updatePickingTotalOliveAddUuid(Map<String, Object> model) throws Exception {
            Map<String, Object> m = new HashMap<String, Object>();
            try{
                
                int workStatCnt = 0;
                int chkIdsCnt = 0;
                model.put("vrSrchAddressValid", "1");
                List list = dao.rawListByOliveSummaryAddUuid(model);
                int tmpCnt = list.size();
                if(tmpCnt > 0){           
                
                    String[] ordId = new String[tmpCnt];
                    String[] ordSeq = new String[tmpCnt];
                    
                    
                    for(int i = 0 ; i < tmpCnt ; i ++){
                        ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                        ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");

                        String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
                        int outOrdQty = Integer.parseInt(String.valueOf(((Map<String, String>)list.get(i)).get("OUT_ORD_QTY")));
                        
                      
                        if(Integer.parseInt(workStat) == 450){
                            workStatCnt++;
                        }
                        
                        if(Integer.parseInt(workStat) < 230){
                            if(0 < outOrdQty){
                                chkIdsCnt++;
                            }
                        }
                      
                        
                        //System.out.println(ordId[i] + "::::" + ordSeq[i]);
                    }
                    
                    if(workStatCnt > 0){
                        if(tmpCnt != workStatCnt){
                            String msg = "매핑 작업시 모든 상태가 매핑 이어야 합니다.";
                            throw new BizException(msg);
                        }
                    }
                    
                    if(chkIdsCnt > 0){
                        String msg = "피킹리스트발행시로케이션지정은필수입니다";
                        throw new BizException(msg);
                    }
                    
                    // 프로시져에 보낼것들 다담는다
                    Map<String, Object> modelIns = new HashMap<String, Object>();
    
                    modelIns.put("ordId", ordId);
                    modelIns.put("ordSeq", ordSeq);
    
                    // session 및 등록정보
                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    
                    // dao
                    modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
                    ServiceUtil.isValidReturnCode("WMOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
                
                }
                m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("list.success"));
                
            } catch(BizException be) {
                if (log.isErrorEnabled()) {
                    log.error("Fail to get result :", be);
                }           
                m.put("errCnt", 1);
                m.put("MSG", be.getMessage() );
                
            }catch(Exception e){
                if (log.isErrorEnabled()) {
                    log.error("Fail to get result :", e);
                } 
                m.put("errCnt", 1);
                m.put("MSG", MessageResolver.getMessage("save.error") );
            }
            return m;
        
        }
	    
	    /**
         *  Method ID  : updatePickingTotalV2 
         *  Method 설명  : 토탈피킹리스트발행
         *  작성자             : KHKIM
         * @param model
         * @return
         * @throws Exception
         */
        @Override
        public Map<String, Object> updatePickingTotalOliveRobot(Map<String, Object> model) throws Exception {
            Map<String, Object> m = new HashMap<String, Object>();
            try{
                
                int workStatCnt = 0;
                int chkIdsCnt = 0;
                model.put("vrSrchAddressValid", "1");
                List list = dao.rawListByOliveSummary(model);
                int tmpCnt = list.size();
                if(tmpCnt > 0){           
                
                    String[] ordId = new String[tmpCnt];
                    String[] ordSeq = new String[tmpCnt];
                    
                    
                    for(int i = 0 ; i < tmpCnt ; i ++){
                        ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                        ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");

                        String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
                        int outOrdQty = Integer.parseInt(String.valueOf(((Map<String, String>)list.get(i)).get("OUT_ORD_QTY")));
                        
                      
                        if(Integer.parseInt(workStat) == 450){
                            workStatCnt++;
                        }
                        
                        if(Integer.parseInt(workStat) < 230){
                            if(0 < outOrdQty){
                                chkIdsCnt++;
                            }
                        }
                      
                        
                        //System.out.println(ordId[i] + "::::" + ordSeq[i]);
                    }
                    
                    if(workStatCnt > 0){
                        if(tmpCnt != workStatCnt){
                            String msg = "매핑 작업시 모든 상태가 매핑 이어야 합니다.";
                            throw new BizException(msg);
                        }
                    }
                    
                    if(chkIdsCnt > 0){
                        String msg = "피킹리스트발행시로케이션지정은필수입니다";
                        throw new BizException(msg);
                    }
                    
                    // 프로시져에 보낼것들 다담는다
                    Map<String, Object> modelIns = new HashMap<String, Object>();
    
                    modelIns.put("ordId", ordId);
                    modelIns.put("ordSeq", ordSeq);
    
                    // session 및 등록정보
                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    
                    // dao
                    modelIns = (Map<String, Object>)dao.updatePickingTotalRobot(modelIns);
                    ServiceUtil.isValidReturnCode("WMOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
                
                }
                m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("list.success"));
                
            } catch(BizException be) {
                if (log.isErrorEnabled()) {
                    log.error("Fail to get result :", be);
                }           
                m.put("errCnt", 1);
                m.put("MSG", be.getMessage() );
                
            }catch(Exception e){
                if (log.isErrorEnabled()) {
                    log.error("Fail to get result :", e);
                } 
                m.put("errCnt", 1);
                m.put("MSG", MessageResolver.getMessage("save.error") );
            }
            return m;
        
        }
	    
	    /**
	     * Method ID   : listCountByCust
	     * Method 설명    : 화주별 출고관리카운트 조회
	     * 작성자               : KHKIM
	     * @param   model
	     * @return
	     * @throws  Exception
	     */
	    @Override
	    public Map<String, Object> searchAddressOlive(Map<String, Object> model) throws Exception {
	        Map<String, Object> map = new HashMap<String, Object>();
	        
	        
	        if(model.get("page") == null) {
	            model.put("pageIndex", "1");
	        } else {
	            model.put("pageIndex", model.get("page"));
	        }
	        if(model.get("rows") == null) {
	            model.put("pageSize", "20");
	        } else {
	            model.put("pageSize", model.get("rows"));
	        }
	        GenericResultSet set = dao.searchAddressOlive(model);
	        map.put("LIST", set);
	        return map;
	    }
	    
	    /**
	     * Method ID   : listCountByCust
	     * Method 설명    : 화주별 출고관리카운트 조회
	     * 작성자               : KHKIM
	     * @param   model
	     * @return
	     * @throws  Exception
	     */
	    @Override
	    public Map<String, Object> updateAddressOlive(Map<String, Object> model) throws Exception {
	    	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	for(int i = 0; i < tmpCnt; i++){
            		Map<String, Object> map = new HashMap<String, Object>();
            		
            		String encrpytdAddress = OliveAes256.encrypt((String)model.get("dAddress_" + i));
            		String encrpytdAddress2 = "";
            		if(model.get("dAddress2_" + i) != null && !((String)model.get("dAddress2_" + i)).trim().equals("")){
            			encrpytdAddress2 = OliveAes256.encrypt((String)model.get("dAddress2_" + i));
            		}
            		String encrpytdCity = (String)model.get("dCity_" + i);
            		String encrpytdCountry = (String)model.get("dCountry_" + i);
            		String encrpytdPost = OliveAes256.encrypt((String)model.get("dPost_" + i));
            		
            		String orgOrdId = (String)model.get("orgOrdId_" + i);
            		String encrpytdTel = OliveAes256.encrypt((String)model.get("dTel_" + i));
            		String encrpytdCustNm = (String)model.get("dCustNm_" + i);
            		
            		String dProvince = (String)model.get("dProvince_" + i);
            		
            		map.put("dCountry", encrpytdCountry);
            		map.put("dAddress", encrpytdAddress);
            		map.put("dAddress2", encrpytdAddress2);
            		map.put("dCity", encrpytdCity);
            		map.put("dPost", encrpytdPost);
            		map.put("dTel", encrpytdTel);
            		map.put("dCustNm", encrpytdCustNm);
            		map.put("dProvince", dProvince);
            		map.put("vrOrgOrdId", orgOrdId);
            		map.put("vrShippingCompany", model.get("shipping_company"));
            		map.put("SS_SVC_NO", model.get("SS_SVC_NO"));
            		map.put("SS_USER_NO", model.get("SS_USER_NO"));
            		
            		dao.updateAddressOlive(map);
            		dao.insertAddressHistoryOlive(map);
            	}
		        
            }
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("result", "success");
	        return resultMap;
	    }
	    
	    /**
	    * Method ID : saveUploadData
	    * Method 설명 : 엑셀업로드 저장
	    * 작성자 : kwt
	    * @param model
	    * @return
	    * @throws Exception
	    */
	   public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
	       Map<String, Object> m = new HashMap<String, Object>();
	       int errCnt = 0;
	       int insertCnt = (list != null)?list.size():0;
	           try{            	
	               dao.saveUploadData(model, list);
	               
	               m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
	               m.put("MSG_ORA", "");
	               m.put("errCnt", errCnt);
	               
	           } catch(Exception e){
	               throw e;
	           }
	       return m;
	   }
	   
	    /**
	     * 
	     * 대체 Method ID		: noneBillingFlag
	     * 대체 Method 설명		: 
	     * 작성자				: chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> noneBillingFlag(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();
	        int errCnt = 0;
	        try{
	        	Map<String, Object> modelDt = new HashMap<String, Object>();
		        modelDt.put("ORD_ID"  	, model.get("ORD_ID"));
		        //System.out.println("ORD_ID :: " + model.get("ORD_ID"));
		        dao.noneBillingFlag(modelDt);
		        
		        m.put("errCnt", errCnt);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(Exception e){
	        	m.put("errCnt", 1);
	            throw e;
	        }

	        return m;
	    }
	    
	    /**
	     * 
	     * 대체 Method ID		: etcMsgByCategoryNo
	     * 대체 Method 설명		: 
	     * 작성자				: chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> etcMsgByCategoryNo(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();
	        int errCnt = 0;
	        try{
	        	Map<String, Object> modelDt = new HashMap<String, Object>();
		        modelDt.put("ORD_ID"  					, model.get("ORD_ID"));
		        modelDt.put("ETC_MSG_BY_CATEGORY_NO"	, model.get("ETC_MSG_BY_CATEGORY_NO"));
		        System.out.println("ORD_ID :: " + model.get("ORD_ID"));
		        System.out.println("ETC_MSG_BY_CATEGORY_NO :: " + model.get("ETC_MSG_BY_CATEGORY_NO"));
		        dao.etcMsgByCategoryNo(modelDt);
		        
		        m.put("errCnt", errCnt);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(Exception e){
	        	m.put("errCnt", 1);
	            throw e;
	        }

	        return m;
	    }
	    
	    @Override
	    public Map<String, Object> updateInvalidAddressOlive(
	    		Map<String, Object> model) throws Exception {
	    	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	for(int i = 0; i < tmpCnt; i++){
            		Map<String, Object> map = new HashMap<String, Object>();
            		
            		String ordId = (String)model.get("ordId_" + i);
            		map.put("vrOrdId", ordId);
            		map.put("SS_SVC_NO", model.get("SS_SVC_NO"));
            		
            		if(model.get("shipping_company").equals("20")){
            			dao.updateInvalidAddressOliveDHL(map);
            		}else if(model.get("shipping_company").equals("30")){
            			dao.updateInvalidAddressOliveQXP(map);
            		}
            	}
            }
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("result", "success");
	        return resultMap;
	    }
	    
	    
	    /**
	     * 
	     * 대체 Method ID    : saveExcelOrderJavaB2T
	     * 대체 Method 설명   : 템플릿 주문 저장
	     * 작성자             : kcr
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> saveExcelOrderJavaB2T(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
	    	Gson gson         = new Gson();
			String jsonString = gson.toJson(model);
			String sendData   = new StringBuffer().append(jsonString).toString();
			
			JsonParser Parser   = new JsonParser();
			JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
			JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

			List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
			
			int listHeaderCnt = listHeader.size();
			int listBodyCnt   = listBody.size();
			
	    	Map<String, Object> m = new HashMap<String, Object>();
	        try{
	            if(listBodyCnt > 0){
	                String[] no             = new String[listBodyCnt];     
	                
	                String[] outReqDt       = new String[listBodyCnt];         
	                String[] inReqDt        = new String[listBodyCnt];     
	                String[] custOrdNo      = new String[listBodyCnt];     
	                String[] custOrdSeq     = new String[listBodyCnt];    
	                String[] trustCustCd    = new String[listBodyCnt];     
	                
	                String[] transCustCd    = new String[listBodyCnt];                     
	                String[] transCustTel   = new String[listBodyCnt];         
	                String[] transReqDt     = new String[listBodyCnt];     
	                String[] custCd         = new String[listBodyCnt];
	                String[] ordQty         = new String[listBodyCnt];     
	                
	                String[] uomCd          = new String[listBodyCnt];                
	                String[] sdeptCd        = new String[listBodyCnt];         
	                String[] salePerCd      = new String[listBodyCnt];     
	                String[] carCd          = new String[listBodyCnt];     
	                String[] drvNm          = new String[listBodyCnt];     
	                
	                String[] dlvSeq         = new String[listBodyCnt];                
	                String[] drvTel         = new String[listBodyCnt];         
	                String[] custLotNo      = new String[listBodyCnt];     
	                String[] blNo           = new String[listBodyCnt];     
	                String[] recDt          = new String[listBodyCnt];     
	                
	                String[] outWhCd        = new String[listBodyCnt];                
	                String[] inWhCd         = new String[listBodyCnt];         
	                String[] makeDt         = new String[listBodyCnt];     
	                String[] timePeriodDay  = new String[listBodyCnt];     
	                String[] workYn         = new String[listBodyCnt];     
	                
	                String[] rjType         = new String[listBodyCnt];                
	                String[] locYn          = new String[listBodyCnt];         
	                String[] confYn         = new String[listBodyCnt];     
	                String[] eaCapa         = new String[listBodyCnt];     
	                String[] inOrdWeight    = new String[listBodyCnt];     
	                
	                String[] itemCd         = new String[listBodyCnt];                
	                String[] itemNm         = new String[listBodyCnt];         
	                String[] transCustNm    = new String[listBodyCnt];     
	                String[] transCustAddr  = new String[listBodyCnt];     
	                String[] transEmpNm     = new String[listBodyCnt];     
	                
	                String[] remark         = new String[listBodyCnt];                
	                String[] transZipNo     = new String[listBodyCnt];         
	                String[] etc2           = new String[listBodyCnt];     
	                String[] unitAmt        = new String[listBodyCnt];     
	                String[] transBizNo     = new String[listBodyCnt];     
	                
	                String[] inCustAddr     = new String[listBodyCnt];                
	                String[] inCustCd       = new String[listBodyCnt];         
	                String[] inCustNm       = new String[listBodyCnt];     
	                String[] inCustTel      = new String[listBodyCnt];     
	                String[] inCustEmpNm    = new String[listBodyCnt];     
	                
	                String[] expiryDate     = new String[listBodyCnt];
	                String[] salesCustNm    = new String[listBodyCnt];
	                String[] zip     		= new String[listBodyCnt];
	                String[] addr     		= new String[listBodyCnt];
	                String[] addr2     		= new String[listBodyCnt];
	                String[] phone1    	 	= new String[listBodyCnt];
	                
	                String[] etc1    		= new String[listBodyCnt];
	                String[] unitNo    		= new String[listBodyCnt];               
	                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
	                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
	                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
	                
	                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
	                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
	                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
	                String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
	                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
	                String[] bizCond		= new String[listBodyCnt];   //업태
	                String[] bizType		= new String[listBodyCnt];   //업종
	                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
	                String[] custType		= new String[listBodyCnt];   //화주타입
	                
	                String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
	                String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
//	                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
	                
	                String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
	                String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
	                String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
	                String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
	                String[] locCd              = new String[listBodyCnt];
	                
//	                String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
//	                String[] ownerCd     	= new String[listBodyCnt];   //소유자	2022-03-04

	                
	                /* 컬럼 바인딩 제약조건 참조 flag */
	                String breakYnOrdDegree = "N";
	                
	                for(int i = 0 ; i < listBodyCnt ; i ++){
	                	String OUT_REQ_DT		= "";
	        			String IN_REQ_DT		= "";
	        			String CUST_ORD_NO		= "";
	        			String CUST_ORD_SEQ		= "";
	        			String TRUST_CUST_CD	= "";
	        			String TRANS_CUST_CD	= "";
	        			String TRANS_CUST_TEL	= "";
	        			String TRANS_REQ_DT		= "";
	        			String CUST_CD			= "";
	        			String ORD_QTY			= "";
	        			String UOM_CD			= "";
	        			String SDEPT_CD			= "";
	        			String SALE_PER_CD		= "";
	        			String CAR_CD			= "";
	        			String DRV_NM			= "";
	        			String DLV_SEQ			= "";
	        			String DRV_TEL			= "";
	        			String CUST_LOT_NO		= "";
	        			String BL_NO			= "";
	        			String REC_DT			= "";
	        			String OUT_WH_CD		= "";
	        			String IN_WH_CD			= "";
	        			String MAKE_DT			= "";
	        			String TIME_PERIOD_DAY	= "";
	        			String WORK_YN			= "";
	        			String RJ_TYPE			= "";
	        			String LOC_YN			= "";
	        			String CONF_YN			= "";
	        			String EA_CAPA			= "";
	        			String IN_ORD_WEIGHT	= "";
	        			String ITEM_CD			= "";
	        			String ITEM_NM			= "";
	        			String TRANS_CUST_NM	= "";
	        			String TRANS_CUST_ADDR	= "";
	        			String TRANS_EMP_NM		= "";
	        			String REMARK			= "";
	        			String TRANS_ZIP_NO		= "";
	        			String ETC2				= "";
	        			String UNIT_AMT			= "";
	        			String TRANS_BIZ_NO		= "";
	        			String IN_CUST_ADDR		= "";
	        			String IN_CUST_CD		= "";
	        			String IN_CUST_NM		= "";
	        			String IN_CUST_TEL		= "";
	        			String IN_CUST_EMP_NM	= "";
	        			String EXPIRY_DATE		= "";
	        			String SALES_CUST_NM	= "";
	        			String ZIP				= "";
	        			String ADDR				= "";
	        			String ADDR2			= "";
	        			String PHONE_1			= "";
	        			String ETC1				= "";
	        			String UNIT_NO			= "";
	        			String TIME_DATE		= "";
	        			String TIME_DATE_END	= "";
	        			String TIME_USE_END		= "";
	        			String PHONE_2			= "";
	        			String BUY_CUST_NM		= "";
	        			String BUY_PHONE_1		= "";
	        			String SALES_COMPANY_NM		= "";
	        			String ORD_DEGREE		= "";
	        			String BIZ_COND		= "";
	        			String BIZ_TYPE		= "";
	        			String BIZ_NO		= "";
	        			String CUST_TYPE		= "";
	        			String DATA_SENDER_NM = "";
	        			String LEGACY_ORG_ORD_NO ="";
//	        			String INVOICE_NO ="";
	        			String CUST_SEQ = "";
	        			String ORD_DESC = "";
	        			String DLV_MSG1 = "";
	        			String DLV_MSG2 = "";
	        			String LOC_CD = "";
	        			
//	        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
//	        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

	        			
	        			for(int k = 0 ; k < listHeaderCnt ; k++){
	        				if(listHeader.get(k).get("FORMAT_BIND_COL") == null) continue; // 공란일 경우 skip
	        				if(listHeader.get(k).get("FORMAT_VIEW_SEQ") == null) continue; // FORMAT_VIEW_SEQ null 경우 skip
	        				
	        				JsonObject object = (JsonObject) listBody.get(i);
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
//	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				CUST_CD = (String)model.get("vrSrchCustCd");
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR2")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_2")){PHONE_2 = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_CUST_NM")){BUY_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_PHONE_1")){BUY_PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
//	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
	        				
	        				/* 엑셀에 차수 컬럼 사용 시 엑셀필드값 바인딩(필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩), 차수 컬럼 미사용 시 화면(vrSrchOrdDegree)selectBox값 바인딩  */
	        				if(breakYnOrdDegree.equals("N")){ORD_DEGREE = (String)model.get("vrSrchOrdDegree");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DEGREE")){
	        					if(object.get("S_"+k).toString().replaceAll("\"", "").length() == 0){
	        						ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
	        					}else{
	        						ORD_DEGREE = object.get("S_"+k).toString().replaceAll("\"", "");
	        					}
	        					breakYnOrdDegree = "Y";
	        				};
	        				
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_COND")){BIZ_COND = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_TYPE")){BIZ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_NO")){BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_TYPE")){CUST_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DATA_SENDER_NM")){DATA_SENDER_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LEGACY_ORG_ORD_NO")){LEGACY_ORG_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DESC")){ORD_DESC = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_MSG1")){DLV_MSG1 = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_MSG2")){DLV_MSG2 = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_CD")){LOC_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				
//	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOT_TYPE")){LOT_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
//	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OWNER_CD")){OWNER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
//	        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("INVOICE_NO")){INVOICE_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
	        				CUST_SEQ= (String)model.get("vrCustSeq");
	        				
//	        				System.out.println("SALES_COMPANY_NM : "+(String)model.get("SALES_COMPANY_NM"));
//	        				System.out.println("ITEM_CD : "+(String)model.get("ITEM_CD"));
	        				
	        			}
	                    
	        			String NO = Integer.toString(i+1);
	                	no[i]               = NO;
	                    
	                    outReqDt[i]         = OUT_REQ_DT;    
	                    inReqDt[i]          = IN_REQ_DT;    
	                    custOrdNo[i]        = CUST_ORD_NO;    
	                    custOrdSeq[i]       = CUST_ORD_SEQ;    
	                    trustCustCd[i]      = TRUST_CUST_CD;    
	                    
	                    transCustCd[i]      = TRANS_CUST_CD;    
	                    transCustTel[i]     = TRANS_CUST_TEL;    
	                    transReqDt[i]       = TRANS_REQ_DT;    
	                    custCd[i]           = CUST_CD;
	                    ordQty[i]           = ORD_QTY;    
	                    
	                    uomCd[i]            = UOM_CD;    
	                    sdeptCd[i]          = SDEPT_CD;    
	                    salePerCd[i]        = SALE_PER_CD;    
	                    carCd[i]            = CAR_CD;
	                    drvNm[i]            = DRV_NM;
	                    
	                    dlvSeq[i]           = DLV_SEQ;    
	                    drvTel[i]           = DRV_TEL;    
	                    custLotNo[i]        = CUST_LOT_NO;    
	                    blNo[i]             = BL_NO;    
	                    recDt[i]            = REC_DT;    
	                    
	                    outWhCd[i]          = OUT_WH_CD;
	                    inWhCd[i]           = IN_WH_CD;
	                    makeDt[i]           = MAKE_DT;
	                    timePeriodDay[i]    = TIME_PERIOD_DAY;
	                    workYn[i]           = WORK_YN;
	                    
	                    rjType[i]           = RJ_TYPE;    
	                    locYn[i]            = LOC_YN;    
	                    confYn[i]           = CONF_YN;    
	                    eaCapa[i]           = EA_CAPA;    
	                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
	                    
	                    itemCd[i]           = ITEM_CD;    
	                    itemNm[i]           = ITEM_NM;    
	                    transCustNm[i]      = TRANS_CUST_NM;    
	                    transCustAddr[i]    = TRANS_CUST_ADDR;    
	                    transEmpNm[i]       = TRANS_EMP_NM;    

	                    remark[i]           = REMARK;    
	                    transZipNo[i]       = TRANS_ZIP_NO;    
	                    etc2[i]             = ETC2;    
	                    unitAmt[i]          = UNIT_AMT;    
	                    transBizNo[i]       = TRANS_BIZ_NO;    
	                    
	                    inCustAddr[i]       = IN_CUST_ADDR;   
	                    inCustCd[i]         = IN_CUST_CD;    
	                    inCustNm[i]         = IN_CUST_NM;    
	                    inCustTel[i]        = IN_CUST_TEL;    
	                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
	                    
	                    expiryDate[i]       = EXPIRY_DATE;
	                    salesCustNm[i]      = SALES_CUST_NM;
	                    zip[i]       		= ZIP;
	                    addr[i]       		= ADDR;
	                    addr2[i]       		= ADDR2;
	                    phone1[i]       	= PHONE_1;
	                    
	                    etc1[i]      		= ETC1;
	                    unitNo[i]      		= UNIT_NO;
	                    timeDate[i]         = TIME_DATE;      
	                    timeDateEnd[i]      = TIME_DATE_END;      
	                    timeUseEnd[i]       = TIME_USE_END;  
	                    
	                    phone2[i]       	= PHONE_2;     
	                    buyCustNm[i]       	= BUY_CUST_NM;     
	                    buyPhone1[i]       	= BUY_PHONE_1;
	                    salesCompanyNm[i]   = SALES_COMPANY_NM;
	                    ordDegree[i]       	= ORD_DEGREE;
	                    bizCond[i]       	= BIZ_COND;
	                    bizType[i]       	= BIZ_TYPE;
	                    bizNo[i]       		= BIZ_NO;
	                    custType[i]       	= CUST_TYPE;
	                    
	                    dataSenderNm[i]       		= DATA_SENDER_NM;
	                    legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
//	                    invoiceNo[i]				= INVOICE_NO;
	                    
	                    custSeq[i]					= CUST_SEQ;
	                    
	                    ordDesc[i]     			  	= ORD_DESC;
	                    dlvMsg1[i]       			= DLV_MSG1;
	                    dlvMsg2[i]       			= DLV_MSG2;
	                    locCd[i]                    = LOC_CD;
	                    
//	                    lotType[i]       			= LOT_TYPE;		//LOT속성 신규컬럼 추가
//	                    ownerCd[i]       			= OWNER_CD;		//소유자 신규컬럼 추가

//	                    System.out.println("salesCompanyNm : --- " + salesCompanyNm[i]);
//	                    System.out.println("dataSenderNm : --- " + dataSenderNm[i].getBytes().length);
	                }
	                
	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	               
	                
	                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
	                modelIns.put("no"  , no);
	                if(model.get("vrOrdType").equals("I")){
	                    modelIns.put("reqDt"     	, inReqDt);
	                    modelIns.put("whCd"      	, inWhCd);
	                }else{
	                    modelIns.put("reqDt"     	, outReqDt);
	                    modelIns.put("whCd"      	, outWhCd);
	                }
	                modelIns.put("custOrdNo"    	, custOrdNo);
	                modelIns.put("custOrdSeq"   	, custOrdSeq);
	                modelIns.put("trustCustCd"  	, trustCustCd); //5
	                
	                modelIns.put("transCustCd"  	, transCustCd);
	                modelIns.put("transCustTel" 	, transCustTel);
	                modelIns.put("transReqDt"   	, transReqDt);
	                modelIns.put("custCd"       	, custCd);
	                modelIns.put("ordQty"       	, ordQty);      //10
	                
	                modelIns.put("uomCd"        	, uomCd);
	                modelIns.put("sdeptCd"      	, sdeptCd);
	                modelIns.put("salePerCd"    	, salePerCd);
	                modelIns.put("carCd"        	, carCd);
	                modelIns.put("drvNm"        	, drvNm);       //15
	                
	                modelIns.put("dlvSeq"       	, dlvSeq);
	                modelIns.put("drvTel"       	, drvTel);
	                modelIns.put("custLotNo"    	, custLotNo);
	                modelIns.put("blNo"         	, blNo);
	                modelIns.put("recDt"        	, recDt);       //20
	                
	                modelIns.put("makeDt"       	, makeDt);
	                modelIns.put("timePeriodDay"	, timePeriodDay);
	                modelIns.put("workYn"       	, workYn);
	                
	                modelIns.put("rjType"       	, rjType);
	                modelIns.put("locYn"        	, locYn);       //25
	                modelIns.put("confYn"       	, confYn);     
	                modelIns.put("eaCapa"       	, eaCapa);
	                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
	                
	                modelIns.put("itemCd"           , itemCd);
	                modelIns.put("itemNm"           , itemNm);
	                modelIns.put("transCustNm"      , transCustNm);
	                modelIns.put("transCustAddr"    , transCustAddr);
	                modelIns.put("transEmpNm"       , transEmpNm);
	                
	                modelIns.put("remark"           , remark);
	                modelIns.put("transZipNo"       , transZipNo);
	                modelIns.put("etc2"             , etc2);
	                modelIns.put("unitAmt"          , unitAmt);
	                modelIns.put("transBizNo"       , transBizNo);
	                
	                modelIns.put("inCustAddr"       , inCustAddr);
	                modelIns.put("inCustCd"         , inCustCd);
	                modelIns.put("inCustNm"         , inCustNm);                 
	                modelIns.put("inCustTel"        , inCustTel);
	                modelIns.put("inCustEmpNm"      , inCustEmpNm);
	                
	                modelIns.put("expiryDate"       , expiryDate);
	                modelIns.put("salesCustNm"      , salesCustNm);
	                modelIns.put("zip"       		, zip);
	                modelIns.put("addr"       		, addr);
	                modelIns.put("addr2"       		, addr2);
	                modelIns.put("phone1"       	, phone1);
	                
	                modelIns.put("etc1"     	 	, etc1);
	                modelIns.put("unitNo"     	 	, unitNo);
	                modelIns.put("phone2"			, phone2);  
	                modelIns.put("buyCustNm"		, buyCustNm);  
	                modelIns.put("buyPhone1"		, buyPhone1);
	                modelIns.put("salesCompanyNm"		, salesCompanyNm);

	                modelIns.put("ordDegree"		, ordDegree);
	                modelIns.put("bizCond"		, bizCond);
	                modelIns.put("bizType"		, bizType);
	                modelIns.put("bizNo"		, bizNo);
	                modelIns.put("custType"		, custType);
	                
	                modelIns.put("dataSenderNm"		, dataSenderNm);
	                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//	                modelIns.put("invoiceNo"		, invoiceNo);
	                modelIns.put("custSeq"       	, custSeq);
	                
	                modelIns.put("ordDesc"       	, ordDesc);
	                modelIns.put("dlvMsg1"       	, dlvMsg1);
	                modelIns.put("dlvMsg2"       	, dlvMsg2);
	                modelIns.put("locCd"            , locCd);
	                //sing09 주석해제
//	                modelIns.put("lotType"     		, lotType);  
//	                modelIns.put("ownerCd"     		, ownerCd);  
	                
	                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
	                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
	                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//	                 일반 dao
	            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2T(modelIns);	
	                
	                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	                //m.put("O_CUR", modelIns.get("O_CUR"));       
	            }
	            
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            m.put("MSG_ORA", "");
	            m.put("errCnt", 0);
	            
	        }catch(Exception e) {
	           throw e;
	        }
	        /*
	        catch(Exception e){
	        	m.put("errCnt", -1);
	        	m.put("MSG", e.getMessage());
	            m.put("MSG_ORA", e.getMessage());
	        }
	        */
	        return m;
	    } 
	    
	    
	    /**
	     * 
	     * 대체 Method ID    	: importExceFile
	     * 대체 Method 설명   	: 템플릿 주문 저장 (검증 후 제거)
	     * 작성자             			: dhkim
	     * TO-DO			: 검증 후 제거
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> importExceFile(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
	    	Gson gson         = new Gson();
			String jsonString = gson.toJson(model);
			String sendData   = new StringBuffer().append(jsonString).toString();
			
			JsonParser Parser   = new JsonParser();
			JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
			JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

			List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
			
			int listHeaderCnt = listHeader.size();
			int listBodyCnt   = listBody.size();
			
			long beforeTime = System.currentTimeMillis();
			long paramTime = System.currentTimeMillis();
			long insertTime = System.currentTimeMillis();
			long excuteTime = System.currentTimeMillis();
			
	    	Map<String, Object> m = new HashMap<String, Object>();
	        try{
	            
	            String uploadSeq = dao.getUploadSeqTemplateTable();
	            
	            int result = 0;
	            List list = (List)model.get("LIST");
	            List<Map<String,Object>> batchList = new ArrayList();
	            for(int i = 0 ; i < list.size() ; i++){
	                Map<String,Object> map = (Map<String,Object>)list.get(i);
	                map.put("UPLOAD_SEQ", uploadSeq);
	                map.put("TEMPLATE_SEQ", i+1);
	                map.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	                map.put("CUST_ID", model.get("vrSrchCustId"));
	                map.put("TEMPLATE_ID",model.get("vrCustSeq"));
	                map.put("REG_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                batchList.add(map);
	            }
	            paramTime = System.currentTimeMillis() - beforeTime;
                
                beforeTime = System.currentTimeMillis();
                if(model.get("vrOrdType").equals("I")){
                    result = dao.insertTemplateInOrderTable(list);
                }else{
                    result = dao.insertTemplateOutOrderTable(list);
                }
                
                insertTime = System.currentTimeMillis() - beforeTime;
                if(result == 0){
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("MSG_ORA", "");
                    m.put("errCnt", -1);
                    m.put("paramTime", paramTime);
                    m.put("insertTime", insertTime);
                    m.put("excuteTime", 0);
                    return m;
                }
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("I_ORD_TYPE", model.get("vrOrdType"));
                modelIns.put("I_UPLOAD_SEQ", uploadSeq);
                modelIns.put("I_REQ_DT", "");
                modelIns.put("I_LC_ID", model.get("SS_SVC_NO"));
                modelIns.put("I_CUST_ID", model.get("vrSrchCustId"));
                
                modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                
                beforeTime = System.currentTimeMillis();
                if(model.get("vrOrdType").equals("I")){
                    modelIns = (Map<String, Object>)dao.saveTableOrderNTTInOrder(modelIns);
                }
                else{
                    modelIns = (Map<String, Object>)dao.saveTableOrderNTTB2C(modelIns);
                }
                excuteTime = System.currentTimeMillis() - beforeTime;
                
                m.put("O_CUR", modelIns.get("O_CUR"));
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            m.put("MSG_ORA", "");
	            m.put("errCnt", 0);
	            m.put("paramTime", paramTime);
	            m.put("insertTime", insertTime);
	            m.put("excuteTime", excuteTime);
	            
	        }catch(BizException be){
	            m.put("errCnt", -1);
                m.put("MSG", be.getMessage());
                m.put("MSG_ORA", be.getMessage());
                
	        }
	        catch(Exception e) {
	           throw e;
	        }
	        /*
	        catch(Exception e){
	        	m.put("errCnt", -1);
	        	m.put("MSG", e.getMessage());
	            m.put("MSG_ORA", e.getMessage());
	        }
	        */
	        return m;
	    } 
	    
	    @Override
	    public Map<String, Object> outByListOm(Map<String, Object> model) throws Exception {
	        Map<String, Object> map = new HashMap<String, Object>();
	        if(model.get("page") == null) {
	            model.put("pageIndex", "1");
	        } else {
	            model.put("pageIndex", model.get("page"));
	        }
	        if(model.get("rows") == null) {
	            model.put("pageSize", "20");
	        } else {
	            model.put("pageSize", model.get("rows"));
	        }
	        
	        if(model.get("vrSrchOrdSubtype").equals("140")) {
	            
	            model.put("vrGubun1", "50"); //재고이동
	            model.put("vrGubun2", "132"); //유통점출고
	            
	            String subtypeGubun1 = (String)model.get("vrGubun1");
	            String subtypeGubun2 = (String)model.get("vrGubun2");
	            
	            String chekbox[] = {subtypeGubun1, subtypeGubun2};
	            model.put("chekbox", chekbox);
	            
	        }else if(!model.get("vrSrchOrdSubtype").equals("")){
	       	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
	       	 String chekbox[] = {subtypeGubun3};
	       	 
	       	 model.put("chekbox", chekbox);
	       } 
	        
	        if("on".equals(model.get("chkEmptyStock"))){
                model.put("chkEmptyStock", "1");
            }
	        
	        if("on".equals(model.get("chkBackOrder"))){
                model.put("chkBackOrder", "Y");
            }else{
            	model.put("chkBackOrder", "N");
            }
	        
	        GenericResultSet res = dao.outByListOm(model);
	        List list = res.getList();
	        
	        int len = list.size();
			if(len > 0){
				JsonParser jsonParser = new JsonParser();
				for(int i = 0 ; i < len ; i++){
					Map<String, String> tempMap = ((Map<String, String>)list.get(i));
		        	String tempMemo = tempMap.get("USER_MEMO");
		        	if(tempMemo != null && !tempMemo.equals("")){
		        		//org.springframework.web.util.HtmlUtils.htmlUnescape()
		        		try{
		        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
			        		if(!object.isJsonNull()){
			        			System.out.println(object.get("1").toString());
			        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
			        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
			        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
			        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
			        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
			        			
			        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
			        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
			        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
			        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
			        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
			        		}
		        		}catch(Exception e){
		        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
		        		}
		        	}
		        }
			}
			
			res.setList(list);
	        map.put("LIST", res);
	        return map;
	    }
	    public Map<String, Object> outByListOmDH(Map<String, Object> model) throws Exception {
	    	Map<String, Object> map = new HashMap<String, Object>();
	    	if(model.get("page") == null) {
	    		model.put("pageIndex", "1");
	    	} else {
	    		model.put("pageIndex", model.get("page"));
	    	}
	    	if(model.get("rows") == null) {
	    		model.put("pageSize", "20");
	    	} else {
	    		model.put("pageSize", model.get("rows"));
	    	}
	    	
	    	if(model.get("vrSrchOrdSubtype").equals("140")) {
	    		
	    		model.put("vrGubun1", "50"); //재고이동
	    		model.put("vrGubun2", "132"); //유통점출고
	    		
	    		String subtypeGubun1 = (String)model.get("vrGubun1");
	    		String subtypeGubun2 = (String)model.get("vrGubun2");
	    		
	    		String chekbox[] = {subtypeGubun1, subtypeGubun2};
	    		model.put("chekbox", chekbox);
	    		
	    	}else if(!model.get("vrSrchOrdSubtype").equals("")){
	    		String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
	    		String chekbox[] = {subtypeGubun3};
	    		
	    		model.put("chekbox", chekbox);
	    	} 
	    	
	    	if("on".equals(model.get("chkEmptyStock"))){
	    		model.put("chkEmptyStock", "1");
	    	}
	    	
	    	if("on".equals(model.get("chkBackOrder"))){
	    		model.put("chkBackOrder", "Y");
	    	}else{
	    		model.put("chkBackOrder", "N");
	    	}
	    	
	    	GenericResultSet res = dao.outByListOmDH(model);
	    	List list = res.getList();
	    	
	    	int len = list.size();
	    	if(len > 0){
	    		JsonParser jsonParser = new JsonParser();
	    		for(int i = 0 ; i < len ; i++){
	    			Map<String, String> tempMap = ((Map<String, String>)list.get(i));
	    			String tempMemo = tempMap.get("USER_MEMO");
	    			if(tempMemo != null && !tempMemo.equals("")){
	    				//org.springframework.web.util.HtmlUtils.htmlUnescape()
	    				try{
	    					JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
	    					if(!object.isJsonNull()){
	    						System.out.println(object.get("1").toString());
	    						String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
	    						String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
	    						String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
	    						String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
	    						String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
	    						
	    						tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
	    						tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
	    						tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
	    						tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
	    						tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
	    					}
	    				}catch(Exception e){
	    					System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	    				}
	    			}
	    		}
	    	}
	    	
	    	res.setList(list);
	    	map.put("LIST", res);
	    	return map;
	    }
	    
	    @Override
	    public Map<String, Object> outListByDelivery(Map<String, Object> model) throws Exception {
	        Map<String, Object> map = new HashMap<String, Object>();
	        if(model.get("page") == null) {
	            model.put("pageIndex", "1");
	        } else {
	            model.put("pageIndex", model.get("page"));
	        }
	        if(model.get("rows") == null) {
	            model.put("pageSize", "20");
	        } else {
	            model.put("pageSize", model.get("rows"));
	        }
	        
	        if(model.get("vrSrchOrdSubtype").equals("140")) {
	            
	            model.put("vrGubun1", "50"); //재고이동
	            model.put("vrGubun2", "132"); //유통점출고
	            
	            String subtypeGubun1 = (String)model.get("vrGubun1");
	            String subtypeGubun2 = (String)model.get("vrGubun2");
	            
	            String chekbox[] = {subtypeGubun1, subtypeGubun2};
	            model.put("chekbox", chekbox);
	            
	        }else if(!model.get("vrSrchOrdSubtype").equals("")){
	       	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
	       	 String chekbox[] = {subtypeGubun3};
	       	 
	       	 model.put("chekbox", chekbox);
	       } 
	        
	        map.put("LIST", dao.outListByDelivery(model));
	        return map;
	    }
	    
	    @Override
	    public Map<String, Object> listNew(Map<String, Object> model) throws Exception {
	        Map<String, Object> map = new HashMap<String, Object>();
	        
	        if(model.get("page") == null) {
	            model.put("pageIndex", "1");
	        } else {
	            model.put("pageIndex", model.get("page"));
	        }
	        if(model.get("rows") == null) {
	            model.put("pageSize", "20");
	        } else {
	            model.put("pageSize", model.get("rows"));
	        }
	        
//	        if(model.get("selectItemCnt") != null && !model.get("selectItemCnt").equals("")){
//	        	int itemCnt = Integer.parseInt(model.get("selectItemCnt").toString());
//	        	String[] srchRitemCdList = new String[itemCnt];
//	        	if(itemCnt == 1){
//	        		srchRitemCdList[0] = String.valueOf(model.get("vrSrchRitemCd"));
//	        	}else{
//	        		srchRitemCdList = (String[])model.get("vrSrchRitemCd");
//	        	}
//	        	model.put("srchRitemCdList", srchRitemCdList);
//	        }

	        //등록차수
	        if(model.get("selectCnt") != null && !model.get("selectCnt").equals("")){
	        	int selectCnt = Integer.parseInt(model.get("selectCnt").toString());
	  	        String[] ordDegreeValuesStrs = new String[selectCnt];
	  	        
	  	        if(selectCnt == 1){
	  	        	ordDegreeValuesStrs[0] =  String.valueOf(model.get("vrSrchOrdDegreeSelectBox"));
	  	        }else{
	  	        	 ordDegreeValuesStrs =  (String[]) model.get("vrSrchOrdDegreeSelectBox");
	  	        }
	  	        
	  	        if(selectCnt > 0){
	  	        	List<String> vrOrdInsDtOrdDegreeList   = new ArrayList<String>();
	  	        	List<String> vrOrdInsDtList   = new ArrayList<String>();
	  	            for(int i = 0 ; i < selectCnt ; i ++){
	  		        	String valueStr = (String) ordDegreeValuesStrs[i];
	  		        	if(valueStr != null && !valueStr.equals("")){
	  			        	String[] strArr = valueStr.split("_");
	  			        	if(strArr.length>1){
	  			        		//날짜 + 차주
	  			        		vrOrdInsDtOrdDegreeList.add(valueStr);
	  			        	}else{
	  			        		//날짜
	  			        		vrOrdInsDtList.add(valueStr);
	  			        	}
	  		        	}
	  		        }
	  	            
	  	            
	  	            if(vrOrdInsDtList.size() > 0 && vrOrdInsDtOrdDegreeList.size() > 0 ){
	  	            	model.put("vrOrdInsDtOrdDegreeType","ALL");
	  	            }else{
	  	            	model.put("vrOrdInsDtOrdDegreeType", null);
	  	            }
	  	        	
	  	        	model.put("vrOrdInsDtOrdDegreeList", vrOrdInsDtOrdDegreeList);
	  	        	model.put("vrOrdInsDtList", vrOrdInsDtList);
	  	        }
	        }
	      
	        
	        
	        GenericResultSet res = dao.listNew(model);
	        List list = res.getList();
	        
	        int len = list.size();
			if(len > 0){
				// 통합 출고관리(B2B) 결과 확인용 print 추후 삭제예정
				//System.out.println("resList : " + (Map<String, String>)list.get(0));
				//System.out.println("REC_LOC : " + ((Map<String, String>)list.get(0)).get("P_REC_LOC"));
				//System.out.println("STOCK_UNIT_NO : " + ((Map<String, String>)list.get(0)).get("P_STOCK_UNIT_NO"));
				JsonParser jsonParser = new JsonParser();
				for(int i = 0 ; i < len ; i++){
					Map<String, String> tempMap = ((Map<String, String>)list.get(i));
					//System.out.println("REC_LOC : " + tempMap.get("REC_LOC"));
		        	String tempMemo = tempMap.get("USER_MEMO");
		        	if(tempMemo != null && !tempMemo.equals("")){
		        		try{
		        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
			        		if(!object.isJsonNull()){
			        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
			        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
			        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
			        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
			        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
			        			
			        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
			        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
			        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
			        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
			        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
			        			
			        		}
		        		}catch(Exception e){
		        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
		        		}
		        	}
		        }
			}
	        res.setList(list);
	        map.put("LIST", res);
	        return map;
	    }
	    
	    /**
	     * 대체 Method ID   : asnList
	     * 대체 Method 설명    : ASN 내역 확인 (전송, 오류)
	     * 작성자                      : seongjun kwon
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> asnList(Map<String, Object> model) throws Exception {
	        Map<String, Object> map = new HashMap<String, Object>();
	        
	        if(model.get("page") == null) {
	            model.put("pageIndex", "1");
	        } else {
	            model.put("pageIndex", model.get("page"));
	        }
	        if(model.get("rows") == null) {
	            model.put("pageSize", "50");
	        } else {
	            model.put("pageSize", model.get("rows"));
	        }
	        map.put("LIST", dao.asnList(model));
	        
	        return map;
	    }
	    
	    /**
	     * 대체 Method ID   : outPickingCancel
	     * 대체 Method 설명    : 피킹취소(선택)
	     * 작성자                      : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public Map<String, Object> outPickingCancel(Map<String, Object> model) throws Exception {
	    	 Map<String, Object> m = new HashMap<String, Object>();
	         try{
	             int temCnt = Integer.parseInt(model.get("selectIds").toString());
	             if(temCnt > 0){
	                 String[] ordId = new String[temCnt];
	                 String[] ordSeq = new String[temCnt];
	                 String[] pickingWorkId = new String[temCnt];
//	                 String pickingWorkId = new String();
	                 
	                 for(int i = 0 ; i < temCnt ; i ++){
	                	 Map<String, Object> modelDS = new HashMap<String, Object>();
	                	 
	                     ordId[i]   = (String)model.get("ORD_ID"+i);
	                     ordSeq[i]  = (String)model.get("ORD_SEQ"+i);
	                     
	                     modelDS.put("ordId"    , ordId[i]);
	                     modelDS.put("ordSeq"    , ordSeq[i]);
	                     modelDS.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO));
	                     
	                     pickingWorkId[i] = (String)dao.getPickingWorkId(modelDS);
	                     
//	                     System.out.println(pickingWorkId[0]);
	                     
	                 }      
	                 
	                 Map<String, Object> modelDt = new HashMap<String, Object>();
	                 modelDt.put("ordId", ordId); 
	                 modelDt.put("ordSeq", ordSeq); 
	                 modelDt.put("pickingWorkId", pickingWorkId);
	                 
	                 modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                 modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                 
	                 modelDt = (Map<String, Object>)dao.saveCancelPick(modelDt);
	                 ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
                     
	                 
	             }   
	             m.put("errCnt", 0);
	             m.put("MSG", MessageResolver.getMessage("save.success"));
	             
	         } catch(BizException be) {
	             m.put("errCnt", -1);
	             m.put("MSG", be.getMessage() );
	             
	         }catch(Exception e){
	             throw e;
	         }
	         return m;
	     }  
    
    /**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	

    @Override
    public Map<String, Object> outListByKcc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        if(model.get("vrSrchOrdSubtype").equals("140")) {
            
            model.put("vrGubun1", "50"); //재고이동
            model.put("vrGubun2", "132"); //유통점출고
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(model.get("vrSrchOrdSubtype").equals("148")) {//B2C
            
            model.put("vrGubun1", "134"); //이동출고
            model.put("vrGubun2", "143"); //택배출고(온라인)
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(model.get("vrSrchOrdSubtype").equals("149")) {//B2P
            
            model.put("vrGubun1", "30"); //정상출고
            model.put("vrGubun2", "142"); //택배출고(오프라인)
            model.put("vrGubun3", "144"); //고객자차
            model.put("vrGubun4", "145"); //택배출고(착불)
            
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            String subtypeGubun3 = (String)model.get("vrGubun3");
            String subtypeGubun4 = (String)model.get("vrGubun4");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2, subtypeGubun3, subtypeGubun4};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
       	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
       	 String chekbox[] = {subtypeGubun3};
       	 
       	 model.put("chekbox", chekbox);
       } 
        
        map.put("LIST", dao.outListByKcc(model));
        return map;
    }
    
    
    
    @Override
	public Map<String, Object> outTallyCancel(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				System.out.println("--여기?");
				//session 정보
				modelDt.put("ORD_ID",  model.get("ORD_ID" + i));
				modelDt.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO) );
//					
//				System.out.println(modelDt.get("ORD_ID"));
//				System.out.println(modelDt.get("LC_ID"));
				dao.updateOp011Cancel(modelDt);					//WMSOP011 UPDATE
				dao.updateOp010Cancel(modelDt);					//WMSOP010 UPDATE
				m.put("MSG", MessageResolver.getMessage("변경 되었습니다"));
				m.put("errCnt", 0);
			}
			
		}  catch (Exception e) {
            log.error(e.toString());
            m.put("MSG", MessageResolver.getMessage("list.error"));
        }
		return m;
	}
    
    /**
     * 
     * 대체 Method ID    : saveExcelOrderJavaB2O
     * 대체 Method 설명   : 템플릿 주문 저장
     * 작성자             : kcr
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaB2O(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                
                String[] phone1    	 	= new String[listBodyCnt];                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm	= new String[listBodyCnt];   //salesCompanyNm
                
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수                
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입     
                
                String[] dataSenderNm	= new String[listBodyCnt];   //쇼핑몰                
                String[] legacyOrgOrdNo	= new String[listBodyCnt];   //사방넷주문번호                
                String[] custSeq 		=  new String[listBodyCnt];  //템플릿구분                                               
                String[] lotType		= new String[listBodyCnt];	 // 재고lot속성                
                String[] ownerCd		= new String[listBodyCnt];	 // 재고소유자코드
                
                String[] etd			= new String[listBodyCnt];	 // ETD
                String[] eta			= new String[listBodyCnt];	 // ETA
                String[] exchangerate	= new String[listBodyCnt];	 // 환율
                String[] ordWeight		= new String[listBodyCnt];	 // 주문중량                
                String[] cntrNo			= new String[listBodyCnt];	 // 컨테이너번호
                
                String[] refNo			= new String[listBodyCnt];	 // 참조번호
                String[] itemBarcode	= new String[listBodyCnt];	 // 상품바코드 (다중바코드 혹은 소분상품바코드)
                String[] fromTimeZone	= new String[listBodyCnt];	 // 입항일자
                String[] ordSubType		= new String[listBodyCnt];	 // 주문상세타입                
                String[] ccCd			= new String[listBodyCnt];	 // 화물관리번호
                
                String[] um				= new String[listBodyCnt];	 // 단가
                String[] cntrType		= new String[listBodyCnt];	 // 컨테이너구분
                String[] locCd			= new String[listBodyCnt];	 // 로케이션
                
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	
                	String POI_NO			= "";
                	
                	String OUT_REQ_DT		= "";
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			
        			String UOM_CD			= "";        			
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			
        			String PHONE_1			= "";        			
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM	= "";
        			
        			String ORD_DEGREE		= "";
        			String BIZ_COND			= "";
        			String BIZ_TYPE			= "";
        			String BIZ_NO			= "";
        			String CUST_TYPE		= "";
        			
        			String DATA_SENDER_NM 		= "";
        			String LEGACY_ORG_ORD_NO 	= "";
        			String CUST_SEQ 			= "";        		
        			String LOT_TYPE		    	= "";        			
        			String OWNER_CD		    	= "";
        			
        			String ETD			    = "";
        			String ETA			    = "";
        			String EXCHANGE_RATE    = "";
        			String ORD_WEIGHT	    = "";        			
        			String CNTR_NO		    = "";
        			
        			String REF_NO		    = "";
        			String ITEM_BARCODE		= "";
        			String FROM_TIME_ZONE 	= "";
        			String ORD_SUBTYP		= "";
        			String CC_CD			= "";
        			
        			String UM				= "";  
        			String CNTR_TYPE		= ""; 
        			String LOC_CD			= "";
        			
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL")==null){continue;};//null skip
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_CD = (String)model.get("vrSrchCustCd");
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR2")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_2")){PHONE_2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_CUST_NM")){BUY_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_PHONE_1")){BUY_PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DEGREE")){ORD_DEGREE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_COND")){BIZ_COND = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_TYPE")){BIZ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_NO")){BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_TYPE")){CUST_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DATA_SENDER_NM")){DATA_SENDER_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LEGACY_ORG_ORD_NO")){LEGACY_ORG_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("POI_NO")){POI_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOT_TYPE")){LOT_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OWER_CD")){OWNER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETD")){ETD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETA")){ETA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXCHANGE_RATE")){EXCHANGE_RATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_WEIGHT")){ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CNTR_NO")){CNTR_NO = object.get("S_"+k).toString().replaceAll("\"", "");};        				
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REF_NO")){REF_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_BARCODE")){ITEM_BARCODE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("FROM_TIME_ZONE")){FROM_TIME_ZONE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_SUBTYP")){ORD_SUBTYP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CC_CD")){CC_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CNTR_TYPE")){CNTR_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_CD")){LOC_CD = object.get("S_"+k).toString().replaceAll("\"", "");};        				
        				
        				
        				CUST_SEQ= (String)model.get("vrCustSeq");
        				
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    
                    phone1[i]       	= PHONE_1;         
                    //KAKAO VX 일경우 ETC1 엑셀업로드에서 ETC2로 입력 >> ETC2 -> ORD_DESC
                    etc1[i]             = model.get("SS_SVC_NO").equals("0000003200") ? ETC2 : ETC1;  
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;
                    
                    timeUseEnd[i]       = TIME_USE_END;                      
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    
                    ordDegree[i]       	= ORD_DEGREE;                    
                    bizCond[i]       	= BIZ_COND;                    
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                    
                    dataSenderNm[i]     = DATA_SENDER_NM;
                    //GFC 일경우 legacyOrgOrdNo(고객사주문번호)는 엑셀업로드에서 POI_NO로 입력
                    legacyOrgOrdNo[i] 	= model.get("SS_SVC_NO").equals("0000003420") ? POI_NO : LEGACY_ORG_ORD_NO;                             
                    custSeq[i]			= CUST_SEQ;                    
                    lotType[i]          = LOT_TYPE;                    
                    ownerCd[i]          = OWNER_CD;                    
                    
                    etd[i]            	= ETD;
                    eta[i]            	= ETA;                    
                    exchangerate[i]   	= EXCHANGE_RATE;                    
                    ordWeight[i]      	= ORD_WEIGHT;                    
                    cntrNo[i]         	= CNTR_NO;
                    
                    refNo[i]          	= REF_NO;                    
                    itemBarcode[i]      = ITEM_BARCODE;
                    fromTimeZone[i]     = FROM_TIME_ZONE;
                    ordSubType[i]       = ORD_SUBTYP;
                    ccCd[i]             = CC_CD;
                    
                    um[i]				= UM;
                    cntrType[i] 		= CNTR_TYPE;
                    locCd[i]			= LOC_CD;
                    
//                    System.out.println("CNTR_TYPE : " + CNTR_TYPE);
//                    System.out.println("LOC_CD : " + LOC_CD);
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                              
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("no" 				, no);
                modelIns.put("reqDt"     		, model.get("vrOrdType").equals("I") ? inReqDt : outReqDt);                                                
                modelIns.put("custOrdNo"    	, custOrdNo);		            
                modelIns.put("custOrdSeq"   	, custOrdSeq);	    // 5        
                
                modelIns.put("trustCustCd"  	, trustCustCd);                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);		              
                modelIns.put("custCd"       	, custCd);          // 10       
                
                modelIns.put("ordQty"       	, ordQty);                   
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);		              
                modelIns.put("carCd"        	, carCd);           // 15       
                
                modelIns.put("drvNm"        	, drvNm);                     
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);		              
                modelIns.put("blNo"         	, blNo);            // 20      
                
                modelIns.put("recDt"        	, recDt);
                modelIns.put("whCd"      		, model.get("vrOrdType").equals("I") ? inWhCd : outWhCd);
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);      	// 25          
                
                modelIns.put("rjType"       	, rjType);                
                modelIns.put("locYn"        	, locYn);       
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); 	// 30
                
                modelIns.put("itemCd"           , itemCd);                
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);		// 35
                
                modelIns.put("remark"           , remark);                
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);   	// 40
                
                modelIns.put("inCustAddr"       , inCustAddr);                
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);		// 45
                
                modelIns.put("expiryDate"       , expiryDate);                
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);     		// 50
                
                modelIns.put("phone1"       	, phone1);                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);		// 55
                
                modelIns.put("buyPhone1"		, buyPhone1);                
                modelIns.put("salesCompanyNm"	, salesCompanyNm);
                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"			, bizCond);
                modelIns.put("bizType"			, bizType);			// 60
                
                modelIns.put("bizNo"			, bizNo);                
                modelIns.put("custType"			, custType);                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
                modelIns.put("custSeq"       	, custSeq);			// 65
                
                modelIns.put("lotType"      	, lotType);                
                modelIns.put("ownerCd"      	, ownerCd);
                modelIns.put("etd"       		, etd);
                modelIns.put("eta"       		, eta);
                modelIns.put("exchangerate" 	, exchangerate);	// 70
                
                modelIns.put("ordWeight"    	, ordWeight);                
                modelIns.put("cntrNo"       	, cntrNo);
                modelIns.put("refNo"       		, refNo);
                modelIns.put("itemBarCode"  	, itemBarcode);       	                                                               
                modelIns.put("fromTimeZone"  	, fromTimeZone);	// 75
                
                modelIns.put("ordSubType"  		, ordSubType);
                modelIns.put("ccCd"  		    , ccCd);			
                modelIns.put("um"  		    	, um);			            
                modelIns.put("cntrType"  		, cntrType);			            
                modelIns.put("locCd"  			, locCd);		    // 80        
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));		                
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));                    
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));	
                // Total : 83
                

                // 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2O(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG_ORA", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * Method ID   : listByCustSummary
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByHeader(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByHeader(model));
        return map;
    }
    
    
    /**
     * Method ID   : listKccByHeader
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               :  yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByKccHeader(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("vrSrchOrdSubtype").equals("148")) {//B2C
            
            model.put("vrGubun1", "134"); //이동출고
            model.put("vrGubun2", "143"); //택배출고(온라인)
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(model.get("vrSrchOrdSubtype").equals("149")) {//B2P
            
            model.put("vrGubun1", "30"); //정상출고
            model.put("vrGubun2", "142"); //택배출고(오프라인)
            model.put("vrGubun3", "144"); //고객자차
            model.put("vrGubun4", "145"); //택배출고(착불)
            
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            String subtypeGubun3 = (String)model.get("vrGubun3");
            String subtypeGubun4 = (String)model.get("vrGubun4");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2, subtypeGubun3, subtypeGubun4};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
          	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
           	 String chekbox[] = {subtypeGubun3};
           	 
           	 model.put("chekbox", chekbox);
        }
 
        map.put("LIST", dao.listByKccHeader(model));
        return map;
    }
    
    
    
    
    /**
     * Method ID   : listByKccDetail
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByKccDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByKccDetail(model));
        return map;
    }
    
    
    /**
     * Method ID   : listCountByCust
     * Method 설명    : 화주별 출고관리카운트 조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByKccCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("vrSrchOrdSubtype").equals("148")) {//B2C
            
            model.put("vrGubun1", "134"); //이동출고
            model.put("vrGubun2", "143"); //택배출고(온라인)
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(model.get("vrSrchOrdSubtype").equals("149")) {//B2P
            
            model.put("vrGubun1", "30"); //정상출고
            model.put("vrGubun2", "142"); //택배출고(오프라인)
            model.put("vrGubun3", "144"); //고객자차
            model.put("vrGubun4", "145"); //택배출고(착불)
            
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            String subtypeGubun3 = (String)model.get("vrGubun3");
            String subtypeGubun4 = (String)model.get("vrGubun4");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2, subtypeGubun3, subtypeGubun4};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
          	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
           	 String chekbox[] = {subtypeGubun3};
           	 
           	 model.put("chekbox", chekbox);
        }
        
        
        map.put("LIST", dao.listByKccCount(model));
        return map;
    }
    
    /**
	 * 
	 * Method ID : listExcelOM
	 * Method 설명 : 출고관리 화주별 OM 엑셀 조회 
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listExcelOM(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("pageIndex", "1");
		model.put("pageSize", "60000");
		map.put("LIST", dao.outByListOmExcel(model));
		return map;
	}
	
    /**
	 * 
	 * Method ID : listExcelOMLingTea
	 * Method 설명 : 출고관리 화주별 OM 엑셀 조회 > 링티물류센터
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listExcelOMLingTea(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("pageIndex", "1");
		model.put("pageSize", "60000");
		map.put("LIST", dao.outByListOmExcelLingTea(model));
		return map;
	}
	
	
   /**
	 * 
	 * Method ID : nCodeExcelList
	 * Method 설명 : 엔코드 실적 내역  엑셀다운로드
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> nCodeExcelList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("pageIndex", "1");
		model.put("pageSize", "60000");
		map.put("LIST", dao.nCodeExcelList(model));
		return map;
	}
	
	/*
	 * Method ID   : deleteOrder
     * Method 설명    : 출고주문삭제
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteOrderNcode(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        String errMsg = MessageResolver.getMessage("delete.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] orgOrdId = new String[tmpCnt];  
                String[] legacyOrgOrdNo = new String[tmpCnt];
                String[] outReqDt = new String[tmpCnt];
                String[] ordDetailNo = new String[tmpCnt];  
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    				= (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   				= (String)model.get("ORD_SEQ"+i);
                    orgOrdId[i]   			= (String)model.get("ORG_ORD_ID"+i);
                    legacyOrgOrdNo[i]   = (String)model.get("LEGACY_ORG_ORD_NO"+i);
                    outReqDt[i]   			= (String)model.get("OUT_REQ_DT"+i);
                    ordDetailNo[i]   		= (String)model.get("ORD_DETAIL_NO"+i);
                }
                
                for(int i = 0; i < tmpCnt; ++i ){
                    Map<String, Object> modelIns = new HashMap<String, Object>();
                    
                    modelIns.put("SS_SVC_NO", model.get("SS_SVC_NO"));
                    modelIns.put("ORD_ID", ordId[i]);
                    modelIns.put("ORD_SEQ", ordSeq[i]);
                    modelIns.put("ORG_ORD_NO", orgOrdId[i]);
                    modelIns.put("LEGACY_ORG_ORD_NO", legacyOrgOrdNo[i]);
                    modelIns.put("OUT_REQ_DT", outReqDt[i]);
                    modelIns.put("ORD_DETAIL_NO", ordDetailNo[i]);
                    
                    modelIns = (Map<String, Object>)dao.deleteOrderNcode(modelIns);	
                }
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * 
     * Method ID         : pickingWorkOrder
     * Method 설명      : 피킹 작업지시
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> pickingWorkOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("workUserNo", (String)model.get("workUserNo"));
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.pickingWorkOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> outListByDeliveryV2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        if(model.get("vrSrchOrdSubtype").equals("140")) {
            
            model.put("vrGubun1", "50"); //재고이동
            model.put("vrGubun2", "132"); //유통점출고
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
       	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
       	 String chekbox[] = {subtypeGubun3};
       	 
       	 model.put("chekbox", chekbox);
       } 
        
        map.put("LIST", dao.outListByDeliveryV2(model));
        return map;
    }
    
    @Override
    public Map<String, Object> listByOliveV2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listByOliveV2(model));
        return map;
    }  
    
    /**
     * 
     * Method ID	: saveOutInOrdSimpleComplete
     * Method 설명	: 출고입고통합심플저장
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutInOrdSimpleComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg = MessageResolver.getMessage("save.error");
        
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  	= new String[tmpCnt];                
                String[] ordSeq 	= new String[tmpCnt];
                String[] orgOrdId 	= new String[tmpCnt];
                String[] custOrdSeq	= new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]		= (String)model.get("ORD_ID"+i);
                    ordSeq[i]		= (String)model.get("ORD_SEQ"+i);
                    orgOrdId[i]		= (String)model.get("ORG_ORD_ID2"+i);
                    custOrdSeq[i]	= (String)model.get("CUST_ORD_SEQ"+i);
                }
                
                String I_COMP_ORD_FLAG = "";
                I_COMP_ORD_FLAG = (String)model.get("COMP_ORD_FLAG");
                
                //1. 출고 (간편출고, 로케이션지정 자동(상품마스터 정책))
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("ordId"			, ordId);
                modelIns.put("ordSeq"			, ordSeq);
                modelIns.put("orgOrdId"			, orgOrdId);
                modelIns.put("custOrdSeq"		, custOrdSeq);
                modelIns.put("COMP_ORD_FLAG"	, I_COMP_ORD_FLAG);
                modelIns.put("LC_ID"    	, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                modelIns = (Map<String, Object>)dao.saveOutInOrdSimpleComplete(modelIns);
                
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     *  Method ID	: saveOutInOrdInsertJava
     *  Method 설명	: 출고입고일괄등록030T
     *  작성자            	 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutInOrdInsertJava(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            // === 실제 들어가는 데이터 변수
            String[] no             = new String[tmpCnt];
            String[] eIdxOut 		= new String[tmpCnt];
            String[] eIdxIn 		= new String[tmpCnt];
            String[] eIdxSeq 		= new String[tmpCnt];
            String[] eProductName 	= new String[tmpCnt];
            String[] eSapCustOut 	= new String[tmpCnt];
            String[] eSapCustIn 	= new String[tmpCnt];
            String[] eReceiveName 	= new String[tmpCnt];
            String[] eReceiveTel 	= new String[tmpCnt];
            String[] eReceiveCel 	= new String[tmpCnt];
            String[] eReceiveAddr 	= new String[tmpCnt];
            String[] eDelvMsg 		= new String[tmpCnt];
            String[] eOutReqDt 		= new String[tmpCnt];
            String[] eReceiveZipcode= new String[tmpCnt];
            String[] eCompaynyCoodsCd = new String[tmpCnt];
            String[] eSaleSnt 		= new String[tmpCnt];
            String[] eMallId 		= new String[tmpCnt];
            String[] eOrderId 		= new String[tmpCnt];
            String[] eOrdDegree 	= new String[tmpCnt];
            String[] eQtipTp 		= new String[tmpCnt];
            String[] eCustCd 		= new String[tmpCnt];
            String[] eUnitNo 		= new String[tmpCnt];
            String[] eOutWhCd 		= new String[tmpCnt];

            //== NULL 처리를 위한 데이터 변수
            String[] outReqDt       = new String[tmpCnt];         
            String[] inReqDt        = new String[tmpCnt];     
            String[] custOrdNo      = new String[tmpCnt];     
            String[] custOrdSeq     = new String[tmpCnt];    
            String[] trustCustCd    = new String[tmpCnt];     
            
            String[] transCustCd    = new String[tmpCnt];                     
            String[] transCustTel   = new String[tmpCnt];         
            String[] transReqDt     = new String[tmpCnt];     
            String[] custCd         = new String[tmpCnt];
            String[] ordQty         = new String[tmpCnt];     
            
            String[] uomCd          = new String[tmpCnt];                
            String[] sdeptCd        = new String[tmpCnt];         
            String[] salePerCd      = new String[tmpCnt];     
            String[] carCd          = new String[tmpCnt];     
            String[] drvNm          = new String[tmpCnt];     
            
            String[] dlvSeq         = new String[tmpCnt];                
            String[] drvTel         = new String[tmpCnt];         
            String[] custLotNo      = new String[tmpCnt];     
            String[] blNo           = new String[tmpCnt];     
            String[] recDt          = new String[tmpCnt];     
            
            String[] outWhCd        = new String[tmpCnt];                
            String[] inWhCd         = new String[tmpCnt];         
            String[] makeDt         = new String[tmpCnt];     
            String[] timePeriodDay  = new String[tmpCnt];     
            String[] workYn         = new String[tmpCnt];     
            
            String[] rjType         = new String[tmpCnt];                
            String[] locYn          = new String[tmpCnt];         
            String[] confYn         = new String[tmpCnt];     
            String[] eaCapa         = new String[tmpCnt];     
            String[] inOrdWeight    = new String[tmpCnt];     
            
            String[] itemCd         = new String[tmpCnt];                
            String[] itemNm         = new String[tmpCnt];         
            String[] transCustNm    = new String[tmpCnt];     
            String[] transCustAddr  = new String[tmpCnt];     
            String[] transEmpNm     = new String[tmpCnt];     
            
            String[] remark         = new String[tmpCnt];                
            String[] transZipNo     = new String[tmpCnt];         
            String[] etc2           = new String[tmpCnt];     
            String[] unitAmt        = new String[tmpCnt];     
            String[] transBizNo     = new String[tmpCnt];     
            
            String[] inCustAddr     = new String[tmpCnt];                
            String[] inCustCd       = new String[tmpCnt];         
            String[] inCustNm       = new String[tmpCnt];     
            String[] inCustTel      = new String[tmpCnt];     
            String[] inCustEmpNm    = new String[tmpCnt];     
            
            String[] expiryDate     = new String[tmpCnt];
            String[] salesCustNm    = new String[tmpCnt];
            String[] zip     		= new String[tmpCnt];
            String[] addr     		= new String[tmpCnt];
            String[] addr2     		= new String[tmpCnt];
            String[] phone1    	 	= new String[tmpCnt];
            
            String[] etc1    		= new String[tmpCnt];
            String[] unitNo    		= new String[tmpCnt];               
            String[] timeDate       = new String[tmpCnt];   //상품유효기간     
            String[] timeDateEnd    = new String[tmpCnt];   //상품유효기간만료일
            String[] timeUseEnd     = new String[tmpCnt];   //소비가한만료일
            
            String[] phone2			= new String[tmpCnt];   //고객전화번호2
            String[] buyCustNm		= new String[tmpCnt];   //주문자명
            String[] buyPhone1		= new String[tmpCnt];   //주문자전화번호1
            String[] salesCompanyNm	= new String[tmpCnt];   //salesCompanyNm
            String[] ordDegree		= new String[tmpCnt];   //주문등록차수
            String[] bizCond		= new String[tmpCnt];   //업태
            String[] bizType		= new String[tmpCnt];   //업종
            String[] bizNo			= new String[tmpCnt];   //사업자등록번호
            String[] custType		= new String[tmpCnt];   //화주타입
            
            String[] dataSenderNm		= new String[tmpCnt];   //쇼핑몰
            String[] legacyOrgOrdNo		= new String[tmpCnt];   //사방넷주문번호
            
            String[] custSeq 			=  new String[tmpCnt];  
            String[] ordDesc 			=  new String[tmpCnt];   //ord_desc
            String[] dlvMsg1 			=  new String[tmpCnt];   //배송메세지1
            String[] dlvMsg2 			=  new String[tmpCnt];   //배송메세지2
            String[] locCd            =  new String[tmpCnt];   //로케이션코드
            
            // I_ 로 시작하는 변수는 안담김
            for (int i = 0; i < tmpCnt; i++) {
            	 // === NULL 처리를 위한 데이터 변수
            	String OUT_REQ_DT		= "";
    			String IN_REQ_DT		= "";
    			String CUST_ORD_NO		= "";
    			String CUST_ORD_SEQ		= "";
    			String TRUST_CUST_CD	= "";
    			String TRANS_CUST_CD	= "";
    			String TRANS_CUST_TEL	= "";
    			String TRANS_REQ_DT		= "";
    			String CUST_CD			= "";
    			String ORD_QTY			= "";
    			String UOM_CD			= "";
    			String SDEPT_CD			= "";
    			String SALE_PER_CD		= "";
    			String CAR_CD			= "";
    			String DRV_NM			= "";
    			String DLV_SEQ			= "";
    			String DRV_TEL			= "";
    			String CUST_LOT_NO		= "";
    			String BL_NO			= "";
    			String REC_DT			= "";
    			String OUT_WH_CD		= "";
    			String IN_WH_CD			= "";
    			String MAKE_DT			= "";
    			String TIME_PERIOD_DAY	= "";
    			String WORK_YN			= "";
    			String RJ_TYPE			= "";
    			String LOC_YN			= "";
    			String CONF_YN			= "";
    			String EA_CAPA			= "";
    			String IN_ORD_WEIGHT	= "";
    			String ITEM_CD			= "";
    			String ITEM_NM			= "";
    			String TRANS_CUST_NM	= "";
    			String TRANS_CUST_ADDR	= "";
    			String TRANS_EMP_NM		= "";
    			String REMARK			= "";
    			String TRANS_ZIP_NO		= "";
    			String ETC2				= "";
    			String UNIT_AMT			= "";
    			String TRANS_BIZ_NO		= "";
    			String IN_CUST_ADDR		= "";
    			String IN_CUST_CD		= "";
    			String IN_CUST_NM		= "";
    			String IN_CUST_TEL		= "";
    			String IN_CUST_EMP_NM	= "";
    			String EXPIRY_DATE		= "";
    			String SALES_CUST_NM	= "";
    			String ZIP				= "";
    			String ADDR				= "";
    			String ADDR2			= "";
    			String PHONE_1			= "";
    			String ETC1				= "";
    			String UNIT_NO			= "";
    			String TIME_DATE		= "";
    			String TIME_DATE_END	= "";
    			String TIME_USE_END		= "";
    			String PHONE_2			= "";
    			String BUY_CUST_NM		= "";
    			String BUY_PHONE_1		= "";
    			String SALES_COMPANY_NM	= "";
    			String ORD_DEGREE		= "";
    			String BIZ_COND			= "";
    			String BIZ_TYPE			= "";
    			String BIZ_NO			= "";
    			String CUST_TYPE		= "";
    			String DATA_SENDER_NM 	= "";
    			String LEGACY_ORG_ORD_NO ="";
    			String CUST_SEQ 		= "";
    			String ORD_DESC = "";
    			String DLV_MSG1 = "";
    			String DLV_MSG2 = "";
    			String LOC_CD = "";
    			
    			//==== 실제 들어가는 데이터
            	String NO = Integer.toString(i+1);
            	
            	no[i]               	= NO;
            	eIdxOut[i] 				= (String)model.get("IDX_OUT_" + i);
            	eIdxIn[i] 				= (String)model.get("IDX_IN_" + i);
            	eIdxSeq[i] 				= (String)model.get("IDX_SEQ_" + i);
            	eProductName[i] 		= (String)model.get("P_PRODUCT_NAME_" + i);
            	eSapCustOut[i] 			= (String)model.get("colSAP_CUST_FROM_" + i); //FROM_ORD_DESC
            	eSapCustIn[i] 			= (String)model.get("colSAP_CUST_TO_" + i); //TO_ORD_DESC
            	eReceiveName[i] 		= (String)model.get("RECEIVE_NAME_" + i);
            	eReceiveTel[i] 			= (String)model.get("RECEIVE_TEL_" + i);
            	eReceiveCel[i]			= (String)model.get("RECEIVE_CEL_" + i);
            	eReceiveAddr[i] 		= (String)model.get("RECEIVE_ADDR_" + i);
                
            	eDelvMsg[i] 			= (String)model.get("DELV_MSG_" + i);
            	eOutReqDt[i] 			= (String)model.get("OUT_REQ_DT" + i);
            	eReceiveZipcode[i] 		= (String)model.get("RECEIVE_ZIPCODE_" + i);
            	eCompaynyCoodsCd[i] 	= (String)model.get("COMPAYNY_GOODS_CD_" + i);
            	eSaleSnt[i] 			= (String)model.get("SALE_CNT_" + i);
            	eMallId[i] 				= (String)model.get("MALL_ID_" + i);
            	eOrderId[i] 			= (String)model.get("ORDER_ID_" + i);
            	eOrdDegree[i] 			= (String)model.get("colORD_DEGREE_" + i);
            	eCustCd[i] 				= (String)model.get("colCUST_CD_" + i);
            	eUnitNo[i] 				= (String)model.get("colUNIT_NO_" + i);
            	eOutWhCd[i] 			= (String)model.get("colOUT_WH_CD_" + i);
            	eQtipTp[i] 				= (String)model.get("colQTIO_TP_" + i);
            	
            	//==== NULL 처리 데이터
            	outReqDt[i]         = OUT_REQ_DT;    
                inReqDt[i]          = IN_REQ_DT;    
                custOrdNo[i]        = CUST_ORD_NO;    
                custOrdSeq[i]       = CUST_ORD_SEQ;    
                trustCustCd[i]      = TRUST_CUST_CD;    
                
                transCustCd[i]      = TRANS_CUST_CD;    
                transCustTel[i]     = TRANS_CUST_TEL;    
                transReqDt[i]       = TRANS_REQ_DT;    
                custCd[i]           = CUST_CD;
                ordQty[i]           = ORD_QTY;    
                
                uomCd[i]            = UOM_CD;    
                sdeptCd[i]          = SDEPT_CD;    
                salePerCd[i]        = SALE_PER_CD;    
                carCd[i]            = CAR_CD;
                drvNm[i]            = DRV_NM;
                
                dlvSeq[i]           = DLV_SEQ;    
                drvTel[i]           = DRV_TEL;    
                custLotNo[i]        = CUST_LOT_NO;    
                blNo[i]             = BL_NO;    
                recDt[i]            = REC_DT;    
                
                outWhCd[i]          = OUT_WH_CD;
                inWhCd[i]           = IN_WH_CD;
                makeDt[i]           = MAKE_DT;
                timePeriodDay[i]    = TIME_PERIOD_DAY;
                workYn[i]           = WORK_YN;
                
                rjType[i]           = RJ_TYPE;    
                locYn[i]            = LOC_YN;    
                confYn[i]           = CONF_YN;    
                eaCapa[i]           = EA_CAPA;    
                inOrdWeight[i]      = IN_ORD_WEIGHT;   
                
                itemCd[i]           = ITEM_CD;    
                itemNm[i]           = ITEM_NM;    
                transCustNm[i]      = TRANS_CUST_NM;    
                transCustAddr[i]    = TRANS_CUST_ADDR;    
                transEmpNm[i]       = TRANS_EMP_NM;    

                remark[i]           = REMARK;    
                transZipNo[i]       = TRANS_ZIP_NO;    
                etc2[i]             = ETC2;    
                unitAmt[i]          = UNIT_AMT;    
                transBizNo[i]       = TRANS_BIZ_NO;    
                
                inCustAddr[i]       = IN_CUST_ADDR;   
                inCustCd[i]         = IN_CUST_CD;    
                inCustNm[i]         = IN_CUST_NM;    
                inCustTel[i]        = IN_CUST_TEL;    
                inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                
                expiryDate[i]       = EXPIRY_DATE;
                salesCustNm[i]      = SALES_CUST_NM;
                zip[i]       		= ZIP;
                addr[i]       		= ADDR;
                addr2[i]       		= ADDR2;
                phone1[i]       	= PHONE_1;
                
                etc1[i]      		= ETC1;
                unitNo[i]      		= UNIT_NO;
                timeDate[i]         = TIME_DATE;      
                timeDateEnd[i]      = TIME_DATE_END;      
                timeUseEnd[i]       = TIME_USE_END;  
                
                phone2[i]       	= PHONE_2;     
                buyCustNm[i]       	= BUY_CUST_NM;     
                buyPhone1[i]       	= BUY_PHONE_1;
                salesCompanyNm[i]   = SALES_COMPANY_NM;
                ordDegree[i]       	= ORD_DEGREE;
                bizCond[i]       	= BIZ_COND;
                bizType[i]       	= BIZ_TYPE;
                bizNo[i]       		= BIZ_NO;
                custType[i]       	= CUST_TYPE;
                
                dataSenderNm[i]     = DATA_SENDER_NM;
                legacyOrgOrdNo[i]   = LEGACY_ORG_ORD_NO;
                custSeq[i]			= CUST_SEQ;

                ordDesc[i]     		= ORD_DESC;
                dlvMsg1[i]       	= DLV_MSG1;
                dlvMsg2[i]       	= DLV_MSG2;
                locCd[i]            = LOC_CD;
                
            }
          
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelInsOut = new HashMap<String, Object>();
            Map<String, Object> modelInsIn 	= new HashMap<String, Object>();

            //1.출고등록 Map
            modelInsOut.put("vrOrdType"			, "02"); // 주문타입  : 출고
            modelInsOut.put("no"  				, no);
            modelInsOut.put("reqDt"				, eOutReqDt);
            modelInsOut.put("whCd"				, eOutWhCd);
            modelInsOut.put("custOrdNo"			, eIdxOut);

            modelInsOut.put("custOrdSeq"   		, eIdxSeq);
            modelInsOut.put("trustCustCd"  		, trustCustCd); 
            modelInsOut.put("transCustCd"  		, transCustCd);
            modelInsOut.put("transCustTel" 		, transCustTel);
            modelInsOut.put("transReqDt"   		, transReqDt);

            modelInsOut.put("custCd"			, eCustCd);
            modelInsOut.put("ordQty"			, eSaleSnt);
            modelInsOut.put("uomCd"        		, uomCd);
            modelInsOut.put("sdeptCd"      		, sdeptCd);
            modelInsOut.put("salePerCd"    		, salePerCd);

            modelInsOut.put("carCd"        		, carCd);
            modelInsOut.put("drvNm"        		, drvNm);       
            modelInsOut.put("dlvSeq"       		, dlvSeq);
            modelInsOut.put("drvTel"       		, drvTel);
            modelInsOut.put("custLotNo"    		, custLotNo);

            modelInsOut.put("blNo"         		, blNo);
            modelInsOut.put("recDt"        		, recDt);      
            modelInsOut.put("makeDt"       		, makeDt);
            modelInsOut.put("timePeriodDay"		, timePeriodDay);
            modelInsOut.put("workYn"       		, workYn);

            modelInsOut.put("rjType"       		, rjType);
            modelInsOut.put("locYn"        		, locYn);    
            modelInsOut.put("confYn"       		, confYn);     
            modelInsOut.put("eaCapa"       		, eaCapa);
            modelInsOut.put("inOrdWeight"  		, inOrdWeight); 

            modelInsOut.put("itemCd"			, eCompaynyCoodsCd);
            modelInsOut.put("itemNm"			, eProductName);
            modelInsOut.put("transCustNm"		, transCustNm);
            modelInsOut.put("transCustAddr"		, transCustAddr);
            modelInsOut.put("transEmpNm"		, transEmpNm);

            modelInsOut.put("remark"        	, remark);
            modelInsOut.put("transZipNo"    	, transZipNo);
            modelInsOut.put("etc2"          	, eSapCustOut); // WMSOM010.ORD_DESC 와 매핑 : 출고저장위치
            modelInsOut.put("unitAmt"       	, unitAmt);
            modelInsOut.put("transBizNo"    	, transBizNo);

            modelInsOut.put("inCustAddr"    	, inCustAddr);
            modelInsOut.put("inCustCd"      	, inCustCd);
            modelInsOut.put("inCustNm"      	, inCustNm);                 
            modelInsOut.put("inCustTel"     	, inCustTel);
            modelInsOut.put("inCustEmpNm"   	, inCustEmpNm);

            modelInsOut.put("expiryDate"    	, expiryDate);
            modelInsOut.put("salesCustNm"   	, eReceiveName);
            modelInsOut.put("zip"				, eReceiveZipcode);
            modelInsOut.put("addr"       		, eReceiveAddr);
            modelInsOut.put("addr2"       		, addr2);

            modelInsOut.put("phone1"			, eReceiveTel);
            modelInsOut.put("etc1"				, eDelvMsg);
            modelInsOut.put("unitNo"			, eUnitNo);
            modelInsOut.put("phone2"			, eReceiveCel);
            modelInsOut.put("buyCustNm"			, buyCustNm);  

            modelInsOut.put("buyPhone1"			, buyPhone1);
            modelInsOut.put("salesCompanyNm"	, eQtipTp);
            modelInsOut.put("ordDegree"			, eOrdDegree);
            modelInsOut.put("bizCond"			, bizCond);
            modelInsOut.put("bizType"			, bizType);

            modelInsOut.put("bizNo"				, bizNo);
            modelInsOut.put("custType"			, custType);
            modelInsOut.put("dataSenderNm"		, eMallId);
            modelInsOut.put("legacyOrgOrdNo"	, eOrderId);
            modelInsOut.put("custSeq"       	, custSeq);
            
            modelInsOut.put("ordDesc"       	, ordDesc);
            modelInsOut.put("dlvMsg1"       	, dlvMsg1);
            modelInsOut.put("dlvMsg2"       	, dlvMsg2);
            modelInsOut.put("locCd"              , locCd);
            
            modelInsOut.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
            modelInsOut.put("WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelInsOut.put("USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
            
            //2.입고등록 Map
            modelInsIn.put("vrOrdType"			, "01"); // 주문타입  : 입고
            modelInsIn.put("no"  				, no);
            modelInsIn.put("reqDt"				, eOutReqDt);
            modelInsIn.put("whCd"				, eOutWhCd);
            modelInsIn.put("custOrdNo"			, eIdxIn);

            modelInsIn.put("custOrdSeq"   		, eIdxSeq);
            modelInsIn.put("trustCustCd"  		, trustCustCd); 
            modelInsIn.put("transCustCd"  		, transCustCd);
            modelInsIn.put("transCustTel" 		, transCustTel);
            modelInsIn.put("transReqDt"   		, transReqDt);

            modelInsIn.put("custCd"				, eCustCd);
            modelInsIn.put("ordQty"				, eSaleSnt);
            modelInsIn.put("uomCd"        		, uomCd);
            modelInsIn.put("sdeptCd"      		, sdeptCd);
            modelInsIn.put("salePerCd"    		, salePerCd);

            modelInsIn.put("carCd"        		, carCd);
            modelInsIn.put("drvNm"        		, drvNm);       
            modelInsIn.put("dlvSeq"       		, dlvSeq);
            modelInsIn.put("drvTel"       		, drvTel);
            modelInsIn.put("custLotNo"    		, custLotNo);

            modelInsIn.put("blNo"         		, blNo);
            modelInsIn.put("recDt"        		, recDt);      
            modelInsIn.put("makeDt"       		, makeDt);
            modelInsIn.put("timePeriodDay"		, timePeriodDay);
            modelInsIn.put("workYn"       		, workYn);

            modelInsIn.put("rjType"       		, rjType);
            modelInsIn.put("locYn"        		, locYn);    
            modelInsIn.put("confYn"       		, confYn);     
            modelInsIn.put("eaCapa"       		, eaCapa);
            modelInsIn.put("inOrdWeight"  		, inOrdWeight); 

            modelInsIn.put("itemCd"				, eCompaynyCoodsCd);
            modelInsIn.put("itemNm"				, eProductName);
            modelInsIn.put("transCustNm"		, transCustNm);
            modelInsIn.put("transCustAddr"		, transCustAddr);
            modelInsIn.put("transEmpNm"			, transEmpNm);

            modelInsIn.put("remark"				, remark);
            modelInsIn.put("transZipNo"			, transZipNo);
            modelInsIn.put("etc2"				, eSapCustIn); // WMSOM010.ORD_DESC 와 매핑 : 입고저장위치
            modelInsIn.put("unitAmt"			, unitAmt);
            modelInsIn.put("transBizNo"			, transBizNo);

            modelInsIn.put("inCustAddr"			, inCustAddr);
            modelInsIn.put("inCustCd"			, inCustCd);
            modelInsIn.put("inCustNm"			, inCustNm);                 
            modelInsIn.put("inCustTel"			, inCustTel);
            modelInsIn.put("inCustEmpNm"		, inCustEmpNm);

            modelInsIn.put("expiryDate"			, expiryDate);
            modelInsIn.put("salesCustNm"		, eReceiveName);
            modelInsIn.put("zip"				, eReceiveZipcode);
            modelInsIn.put("addr"       		, eReceiveAddr);
            modelInsIn.put("addr2"       		, addr2);

            modelInsIn.put("phone1"				, eReceiveTel);
            modelInsIn.put("etc1"				, eDelvMsg);
            modelInsIn.put("unitNo"				, eUnitNo);
            modelInsIn.put("phone2"				, eReceiveCel);
            modelInsIn.put("buyCustNm"			, buyCustNm);  

            modelInsIn.put("buyPhone1"			, buyPhone1);
            modelInsIn.put("salesCompanyNm"		, eQtipTp);
            modelInsIn.put("ordDegree"			, eOrdDegree);
            modelInsIn.put("bizCond"			, bizCond);
            modelInsIn.put("bizType"			, bizType);

            modelInsIn.put("bizNo"				, bizNo);
            modelInsIn.put("custType"			, custType);
            modelInsIn.put("dataSenderNm"		, eMallId);
            modelInsIn.put("legacyOrgOrdNo"		, eOrderId);
            modelInsIn.put("custSeq"			, custSeq);
            
            modelInsIn.put("ordDesc"			, ordDesc);
            modelInsIn.put("dlvMsg1"			, dlvMsg1);
            modelInsIn.put("dlvMsg2"			, dlvMsg2);
            modelInsIn.put("locCd"              , locCd);
            
            modelInsIn.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
            modelInsIn.put("WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelInsIn.put("USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
            
            //3.출고등록 PK_WMSOP030T
            modelInsOut = (Map<String, Object>)dao.saveExcelOrderB2T(modelInsOut);
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelInsOut.get("O_MSG_CODE")), (String)modelInsOut.get("O_MSG_NAME"));
            
            if(String.valueOf(modelInsOut.get("O_MSG_CODE")).equals("0")){
            	modelInsIn = (Map<String, Object>)dao.saveExcelOrderB2T(modelInsIn);
            	ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelInsIn.get("O_MSG_CODE")), (String)modelInsIn.get("O_MSG_NAME"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID    : saveExcelOrderJavaB2TKAKAOVX
     * 대체 Method 설명   : 템플릿 주문 저장 - 카카오VX 전용
     * 작성자             : kcr
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaB2TKAKAOVX(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
                String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
//                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
                
                String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
                String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
                String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
                String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
                String[] locCd              =  new String[listBodyCnt];   //로케이션코드
                
//                String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
//                String[] ownerCd     	= new String[listBodyCnt];   //소유자	2022-03-04

                
                /* 컬럼 바인딩 제약조건 참조 flag */
                String breakYnOrdDegree = "N";
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= "";
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			String DATA_SENDER_NM = "";
        			String LEGACY_ORG_ORD_NO ="";
//        			String INVOICE_NO ="";
        			String CUST_SEQ = "";
        			String ORD_DESC = "";
        			String DLV_MSG1 = "";
        			String DLV_MSG2 = "";
        			String LOC_CD = "";
        			
//        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
//        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_CD = (String)model.get("vrSrchCustCd");
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR2")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_2")){PHONE_2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_CUST_NM")){BUY_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_PHONE_1")){BUY_PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
        				
        				/* 엑셀에 차수 컬럼 사용 시 엑셀필드값 바인딩(필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩), 차수 컬럼 미사용 시 화면(vrSrchOrdDegree)selectBox값 바인딩  */
        				if(breakYnOrdDegree.equals("N")){ORD_DEGREE = (String)model.get("vrSrchOrdDegree");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DEGREE")){
        					if(object.get("S_"+k).toString().replaceAll("\"", "").length() == 0){
        						ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
        					}else{
        						ORD_DEGREE = object.get("S_"+k).toString().replaceAll("\"", "");
        					}
        					breakYnOrdDegree = "Y";
        				};
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_COND")){BIZ_COND = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_TYPE")){BIZ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_NO")){BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_TYPE")){CUST_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DATA_SENDER_NM")){DATA_SENDER_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LEGACY_ORG_ORD_NO")){LEGACY_ORG_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DESC")){ORD_DESC = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_MSG1")){DLV_MSG1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_MSG2")){DLV_MSG2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_CD")){LOC_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOT_TYPE")){LOT_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OWNER_CD")){OWNER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("INVOICE_NO")){INVOICE_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_SEQ= (String)model.get("vrCustSeq");
        				
//        				System.out.println("SALES_COMPANY_NM : "+(String)model.get("SALES_COMPANY_NM"));
        				
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = ETC2; //REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                    
                    dataSenderNm[i]       		= DATA_SENDER_NM;
                    legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
//                    invoiceNo[i]				= INVOICE_NO;
                    
                    custSeq[i]					= CUST_SEQ;
                    
                    ordDesc[i]     			  	= ORD_DESC;
                    dlvMsg1[i]       			= DLV_MSG1;
                    dlvMsg2[i]       			= DLV_MSG2;
                    locCd[i]                    = LOC_CD;
                    
//                    lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
//                    ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가

//                    System.out.println("salesCompanyNm : --- " + salesCompanyNm[i]);
//                    System.out.println("dataSenderNm : --- " + dataSenderNm[i].getBytes().length);
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
               
                
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"		, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"		, bizCond);
                modelIns.put("bizType"		, bizType);
                modelIns.put("bizNo"		, bizNo);
                modelIns.put("custType"		, custType);
                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                modelIns.put("invoiceNo"		, invoiceNo);
                modelIns.put("custSeq"       	, custSeq);
                
                modelIns.put("ordDesc"       	, ordDesc);
                modelIns.put("dlvMsg1"       	, dlvMsg1);
                modelIns.put("dlvMsg2"       	, dlvMsg2);
                modelIns.put("locCd"            , locCd);
                
//                modelIns.put("lotType"     		, lotType);  
//                modelIns.put("ownerCd"     		, ownerCd);  
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2T(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e) {
           throw e;
        }
        /*
        catch(Exception e){
        	m.put("errCnt", -1);
        	m.put("MSG", e.getMessage());
            m.put("MSG_ORA", e.getMessage());
        }
        */
        return m;
    }
    /**
     * 
     * 대체 Method ID    : saveExcelOrderJavaB2TS
     * 대체 Method 설명   : 템플릿 주문 저장 _ 주문상세 ORD_SUBTYPE 추가
     * 작성자             : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaB2TS(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
                String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
//                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
                
                String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
                String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
                String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
                String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
                
//                String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
//                String[] ownerCd     	= new String[listBodyCnt];   //소유자	2022-03-04

                
                /* 컬럼 바인딩 제약조건 참조 flag */
                String breakYnOrdDegree = "N";
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= "";
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			String DATA_SENDER_NM = "";
        			String LEGACY_ORG_ORD_NO ="";
        			String CUST_SEQ = "";
        			String ORD_DESC = "";
        			String DLV_MSG1 = "";
        			String DLV_MSG2 = "";
        			
//        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
//        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_CD = (String)model.get("vrSrchCustCd");
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR2")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_2")){PHONE_2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_CUST_NM")){BUY_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BUY_PHONE_1")){BUY_PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_COMPANY_NM")){SALES_COMPANY_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
        				
        				/* 엑셀에 차수 컬럼 사용 시 엑셀필드값 바인딩(필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩), 차수 컬럼 미사용 시 화면(vrSrchOrdDegree)selectBox값 바인딩  */
        				if(breakYnOrdDegree.equals("N")){ORD_DEGREE = (String)model.get("vrSrchOrdDegree");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DEGREE")){
        					if(object.get("S_"+k).toString().replaceAll("\"", "").length() == 0){
        						ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
        					}else{
        						ORD_DEGREE = object.get("S_"+k).toString().replaceAll("\"", "");
        					}
        					breakYnOrdDegree = "Y";
        				};
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_COND")){BIZ_COND = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_TYPE")){BIZ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BIZ_NO")){BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_TYPE")){CUST_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DATA_SENDER_NM")){DATA_SENDER_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LEGACY_ORG_ORD_NO")){LEGACY_ORG_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_DESC")){ORD_DESC = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_MSG1")){DLV_MSG1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_MSG2")){DLV_MSG2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOT_TYPE")){LOT_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OWNER_CD")){OWNER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
//        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("INVOICE_NO")){INVOICE_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				CUST_SEQ= (String)model.get("vrCustSeq");
        				
//        				System.out.println("SALES_COMPANY_NM : "+(String)model.get("SALES_COMPANY_NM"));
        				
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                    
                    dataSenderNm[i]       		= DATA_SENDER_NM;
                    legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
//                    invoiceNo[i]				= INVOICE_NO;
                    
                    custSeq[i]					= CUST_SEQ;
                    
                    ordDesc[i]     			  	= ORD_DESC;
                    dlvMsg1[i]       			= DLV_MSG1;
                    dlvMsg2[i]       			= DLV_MSG2;
                    
//                    lotType[i]       			= LOT_TYPE;		//LOT속성 신규컬럼 추가
//                    ownerCd[i]       			= OWNER_CD;		//소유자 신규컬럼 추가

//                    System.out.println("salesCompanyNm : --- " + salesCompanyNm[i]);
//                    System.out.println("dataSenderNm : --- " + dataSenderNm[i].getBytes().length);
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
               
                
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("vrOrdSubtype"		, model.get("vrOrdSubtype"));
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"		, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"		, bizCond);
                modelIns.put("bizType"		, bizType);
                modelIns.put("bizNo"		, bizNo);
                modelIns.put("custType"		, custType);
                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                modelIns.put("invoiceNo"		, invoiceNo);
                modelIns.put("custSeq"       	, custSeq);
                
                modelIns.put("ordDesc"       	, ordDesc);
                modelIns.put("dlvMsg1"       	, dlvMsg1);
                modelIns.put("dlvMsg2"       	, dlvMsg2);
                //sing09 주석해제
//                modelIns.put("lotType"     		, lotType);  
//                modelIns.put("ownerCd"     		, ownerCd);  
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2TS(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3TS", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e) {
           throw e;
        }
        return m;
    } 
    
    /*-
	 * Method ID	: getCustOrdDegreeList
	 * Method 설명	: 화주별 주문차수
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustOrdDegreeList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_ORD_DEGREE", dao.getCustOrdDegreeList(model));
		return map;
	}
    
     /*-
	 * Method ID	: getCutomerOrderInfo
	 * Method 설명	: 고객 주문 정보 조회
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCutomerOrderInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderInfo", dao.getCutomerOrderInfo(model));
		map.put("csInfo", dao.getCutomerCsInfo(model));
		return map;
	}
    
    /*-
  	 * Method ID	: custInfoSave
  	 * Method 설명	: 고객 주문 정보 수정
  	 * @param 
  	 * @return
  	 */
    @Override
    public Map<String, Object> custInfoSave(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            // web
            modelDt.put("vrSrchOrgOrdId" 	, model.get("vrSrchOrgOrdId") 		== null ? "": model.get("vrSrchOrgOrdId"));		// 원주문번호
            modelDt.put("vrSrchBuyCustNm" 	, model.get("vrSrchBuyCustNm") 		== null ? "": model.get("vrSrchBuyCustNm"));		// 수취인명
            modelDt.put("vrSrchTel" 		, model.get("vrSrchTel") 			== null ? "": model.get("vrSrchTel"));					// 전화번호
            modelDt.put("vrSrchMobile" 		, model.get("vrSrchMobile") 		== null ? "": model.get("vrSrchMobile"));					// 전화번호
            modelDt.put("vrSrchPost" 		, model.get("vrSrchPost") 			== null ? "": model.get("vrSrchPost"));				// 우편번호
            modelDt.put("vrSrchAddr" 		, model.get("vrSrchAddr") 			== null ? "": model.get("vrSrchAddr"));				// 주소
            modelDt.put("vrSrchCustId"    	, model.get("vrSrchCustId") 		== null ? "": model.get("vrSrchCustId"));
            
            modelDt.put("vrSrchDlvMsg" 		, model.get("vrSrchDlvMsg") 		 == null ? "": HtmlUtils.htmlUnescape(HtmlUtils.htmlUnescape(model.get("encodeDlvMsg").toString()))); 	// 배송메시지
            
            // session
            modelDt.put("REG_NO"    		, model.get(ConstantIF.SS_USER_NO));
		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
		    modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
		    
		    
            
        	dao.custInfoSave(modelDt);
        
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   	: saveExcelDelOrderJava
     * 대체 Method 설명     : B2C 템플릿 주문 삭제
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelDelOrderJava(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	List<Map<String, Object>> listBody = (ArrayList)model.get("LIST");
    	String custId = String.valueOf(model.get("vrCustCd"));
    	List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
    	
    	int listHeaderCnt = listHeader.size();
    	int listBodyCnt   = listBody.size();
    	
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	Date today = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String formatted_today = sdf.format(today);
    	
    	try{
    		if(listBodyCnt > 0){
    			String[] no             = new String[listBodyCnt];     
    			
    			String[] outReqDt       = new String[listBodyCnt];         
    			String[] inReqDt        = new String[listBodyCnt];     
    			String[] custOrdNo      = new String[listBodyCnt];     
    			String[] custOrdSeq     = new String[listBodyCnt];    
    			String[] trustCustCd    = new String[listBodyCnt];     
    			
    			String[] transCustCd    = new String[listBodyCnt];                     
    			String[] transCustTel   = new String[listBodyCnt];         
    			String[] transReqDt     = new String[listBodyCnt];     
    			String[] custCd         = new String[listBodyCnt];
    			String[] ordQty         = new String[listBodyCnt];     
    			
    			String[] uomCd          = new String[listBodyCnt];                
    			String[] sdeptCd        = new String[listBodyCnt];         
    			String[] salePerCd      = new String[listBodyCnt];     
    			String[] carCd          = new String[listBodyCnt];     
    			String[] drvNm          = new String[listBodyCnt];     
    			
    			String[] dlvSeq         = new String[listBodyCnt];                
    			String[] drvTel         = new String[listBodyCnt];         
    			String[] custLotNo      = new String[listBodyCnt];     
    			String[] blNo           = new String[listBodyCnt];     
    			String[] recDt          = new String[listBodyCnt];     
    			
    			String[] outWhCd        = new String[listBodyCnt];                
    			String[] inWhCd         = new String[listBodyCnt];         
    			String[] makeDt         = new String[listBodyCnt];     
    			String[] timePeriodDay  = new String[listBodyCnt];     
    			String[] workYn         = new String[listBodyCnt];     
    			
    			String[] rjType         = new String[listBodyCnt];                
    			String[] locYn          = new String[listBodyCnt];         
    			String[] confYn         = new String[listBodyCnt];     
    			String[] eaCapa         = new String[listBodyCnt];     
    			String[] inOrdWeight    = new String[listBodyCnt];     
    			
    			String[] itemCd         = new String[listBodyCnt];                
    			String[] itemNm         = new String[listBodyCnt];         
    			String[] transCustNm    = new String[listBodyCnt];     
    			String[] transCustAddr  = new String[listBodyCnt];     
    			String[] transEmpNm     = new String[listBodyCnt];     
    			
    			String[] remark         = new String[listBodyCnt];                
    			String[] transZipNo     = new String[listBodyCnt];         
    			String[] etc2           = new String[listBodyCnt];     
    			String[] unitAmt        = new String[listBodyCnt];     
    			String[] transBizNo     = new String[listBodyCnt];     
    			
    			String[] inCustAddr     = new String[listBodyCnt];                
    			String[] inCustCd       = new String[listBodyCnt];         
    			String[] inCustNm       = new String[listBodyCnt];     
    			String[] inCustTel      = new String[listBodyCnt];     
    			String[] inCustEmpNm    = new String[listBodyCnt];     
    			
    			String[] expiryDate     = new String[listBodyCnt];
    			String[] salesCustNm    = new String[listBodyCnt];
    			String[] zip     		= new String[listBodyCnt];
    			String[] addr     		= new String[listBodyCnt];
    			String[] addr2     		= new String[listBodyCnt];
    			String[] phone1    	 	= new String[listBodyCnt];
    			
    			String[] etc1    		= new String[listBodyCnt];
    			String[] unitNo    		= new String[listBodyCnt];               
    			String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
    			String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
    			String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
    			
    			String[] phone2			= new String[listBodyCnt];   //고객전화번호2
    			String[] buyCustNm		= new String[listBodyCnt];   //주문자명
    			String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
    			String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
    			String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
    			String[] bizCond		= new String[listBodyCnt];   //업태
    			String[] bizType		= new String[listBodyCnt];   //업종
    			String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
    			String[] custType		= new String[listBodyCnt];   //화주타입
    			
    			String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
    			String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
//                    String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
    			
    			String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
    			String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
    			String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
    			String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
    			
//                    String[] lotType     		= new String[listBodyCnt];   //LOT속성 	2022-03-04
//                    String[] ownerCd     		= new String[listBodyCnt];   //소유자	2022-03-04

    			
    			/* 컬럼 바인딩 제약조건 참조 flag */
    			String breakYnOrdDegree = "N";
    			
    			for(int i = 0 ; i < listBodyCnt ; i ++){
    				String OUT_REQ_DT		= formatted_today;//출고 예정일 -> 업로드 날짜
    				String IN_REQ_DT		= "";
    				String CUST_ORD_NO		= "";
    				String CUST_ORD_SEQ		= "";
    				String TRUST_CUST_CD	= "";
    				String TRANS_CUST_CD	= "";
    				String TRANS_CUST_TEL	= "";
    				String TRANS_REQ_DT		= "";
    				String CUST_CD			= "";
    				String ORD_QTY			= "";
    				String UOM_CD			= "";
    				String SDEPT_CD			= "";
    				String SALE_PER_CD		= "";
    				String CAR_CD			= "";
    				String DRV_NM			= "";
    				String DLV_SEQ			= "";
    				String DRV_TEL			= "";
    				String CUST_LOT_NO		= "";
    				String BL_NO			= "";
    				String REC_DT			= "";
    				String OUT_WH_CD		= "";
    				String IN_WH_CD			= "";
    				String MAKE_DT			= "";
    				String TIME_PERIOD_DAY	= "";
    				String WORK_YN			= "";
    				String RJ_TYPE			= "";
    				String LOC_YN			= "";
    				String CONF_YN			= "";
    				String EA_CAPA			= "";
    				String IN_ORD_WEIGHT	= "";
    				String ITEM_CD			= "";
    				String ITEM_NM			= "";
    				String TRANS_CUST_NM	= "";
    				String TRANS_CUST_ADDR	= "";
    				String TRANS_EMP_NM		= "";
    				String REMARK			= "";
    				String TRANS_ZIP_NO		= "";
    				String ETC2				= "";
    				String UNIT_AMT			= "";
    				String TRANS_BIZ_NO		= "";
    				String IN_CUST_ADDR		= "";
    				String IN_CUST_CD		= "";
    				String IN_CUST_NM		= "";
    				String IN_CUST_TEL		= "";
    				String IN_CUST_EMP_NM	= "";
    				String EXPIRY_DATE		= "";
    				String SALES_CUST_NM	= "";
    				String ZIP				= "";
    				String ADDR				= "";
    				String ADDR2			= "";
    				String PHONE_1			= "";
    				String ETC1				= "";
    				String UNIT_NO			= "";
    				String TIME_DATE		= "";
    				String TIME_DATE_END	= "";
    				String TIME_USE_END		= "";
    				String PHONE_2			= "";
    				String BUY_CUST_NM		= "";
    				String BUY_PHONE_1		= "";
    				String SALES_COMPANY_NM		= "";
    				String ORD_DEGREE		= "";
    				String BIZ_COND		= "";
    				String BIZ_TYPE		= "";
    				String BIZ_NO		= "";
    				String CUST_TYPE		= "";
    				String DATA_SENDER_NM = "";
    				String LEGACY_ORG_ORD_NO ="";
//            			String INVOICE_NO ="";
    				String CUST_SEQ = "";
    				String ORD_DESC = "";
    				String DLV_MSG1 = "";
    				String DLV_MSG2 = "";
    				
//            			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
//            			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

    				
    				for(int k = 0 ; k < listHeaderCnt ; k++){
    					Map<String, Object> object = listBody.get(i);
    					String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
    					
    					switch (bindColNm) {
    					//case "OUT_REQ_DT"             :  OUT_REQ_DT = replaceStr(object, k, "OUT_REQ_DT"); break;
    					case "IN_REQ_DT"              :  IN_REQ_DT = replaceStr(object, k, "IN_REQ_DT"); break;
    					case "CUST_ORD_NO"            :  CUST_ORD_NO = replaceStr(object, k, "CUST_ORD_NO"); break;
    					case "CUST_ORD_SEQ"           :  CUST_ORD_SEQ = replaceStr(object, k, "CUST_ORD_SEQ"); break;
    					case "TRUST_CUST_CD"          :  TRUST_CUST_CD = replaceStr(object, k, "TRUST_CUST_CD"); break;
    					case "TRANS_CUST_CD"          :  TRANS_CUST_CD = replaceStr(object, k, "TRANS_CUST_CD"); break;
    					case "TRANS_CUST_TEL"         :  TRANS_CUST_TEL = replaceStr(object, k, "TRANS_CUST_TEL"); break;
    					case "TRANS_REQ_DT"           :  TRANS_REQ_DT = replaceStr(object, k, "TRANS_REQ_DT"); break;
    					
    					case "ORD_QTY"                :  ORD_QTY = replaceStr(object, k, "ORD_QTY"); break;
    					case "UOM_CD"                 :  UOM_CD = replaceStr(object, k, "UOM_CD"); break;
    					case "SDEPT_CD"               :  SDEPT_CD = replaceStr(object, k, "SDEPT_CD"); break;
    					case "SALE_PER_CD"            :  SALE_PER_CD = replaceStr(object, k, "SALE_PER_CD"); break;
    					case "CAR_CD"                 :  CAR_CD = replaceStr(object, k, "CAR_CD"); break;
    					case "DRV_NM"                 :  DRV_NM = replaceStr(object, k, "DRV_NM"); break;
    					case "DLV_SEQ"                :  DLV_SEQ = replaceStr(object, k, "DLV_SEQ"); break;
    					case "DRV_TEL"                :  DRV_TEL = replaceStr(object, k, "DRV_TEL"); break;
    					case "CUST_LOT_NO"            :  CUST_LOT_NO = replaceStr(object, k, "CUST_LOT_NO"); break;
    					case "BL_NO"                  :  BL_NO = replaceStr(object, k, "BL_NO"); break;
    					case "REC_DT"                 :  REC_DT = replaceStr(object, k, "REC_DT"); break;
    					case "OUT_WH_CD"              :  OUT_WH_CD = replaceStr(object, k, "OUT_WH_CD"); break;
    					case "IN_WH_CD"               :  IN_WH_CD = replaceStr(object, k, "IN_WH_CD"); break;
    					case "MAKE_DT"                :  MAKE_DT = replaceStr(object, k, "MAKE_DT"); break;
    					case "TIME_PERIOD_DAY"        :  TIME_PERIOD_DAY = replaceStr(object, k, "TIME_PERIOD_DAY"); break;
    					case "WORK_YN"                :  WORK_YN = replaceStr(object, k, "WORK_YN"); break;
    					case "RJ_TYPE"                :  RJ_TYPE = replaceStr(object, k, "RJ_TYPE"); break;
    					case "LOC_YN"                 :  LOC_YN = replaceStr(object, k, "LOC_YN"); break;
    					case "CONF_YN"                :  CONF_YN = replaceStr(object, k, "CONF_YN"); break;
    					case "EA_CAPA"                :  EA_CAPA = replaceStr(object, k, "EA_CAPA"); break;
    					case "IN_ORD_WEIGHT"          :  IN_ORD_WEIGHT = replaceStr(object, k, "IN_ORD_WEIGHT"); break;
    					case "ITEM_CD"                :  ITEM_CD = replaceStr(object, k, "ITEM_CD"); break;
    					case "ITEM_NM"                :  ITEM_NM = replaceStr(object, k, "ITEM_NM"); break;
    					case "TRANS_CUST_NM"          :  TRANS_CUST_NM = replaceStr(object, k, "TRANS_CUST_NM"); break;
    					case "TRANS_CUST_ADDR"        :  TRANS_CUST_ADDR = replaceStr(object, k, "TRANS_CUST_ADDR"); break;
    					case "TRANS_EMP_NM"           :  TRANS_EMP_NM = replaceStr(object, k, "TRANS_EMP_NM"); break;
    					case "REMARK"                 :  REMARK = replaceStr(object, k, "REMARK"); break;
    					case "TRANS_ZIP_NO"           :  TRANS_ZIP_NO = replaceStr(object, k, "TRANS_ZIP_NO"); break;
    					case "ETC2"                   :  ETC2 = replaceStr(object, k, "ETC2"); break;
    					case "UNIT_AMT"               :  UNIT_AMT = replaceStr(object, k, "UNIT_AMT"); break;
    					case "TRANS_BIZ_NO"           :  TRANS_BIZ_NO = replaceStr(object, k, "TRANS_BIZ_NO"); break;
    					case "IN_CUST_ADDR"           :  IN_CUST_ADDR = replaceStr(object, k, "IN_CUST_ADDR"); break;
    					case "IN_CUST_CD"             :  IN_CUST_CD = replaceStr(object, k, "IN_CUST_CD"); break;
    					case "IN_CUST_NM"             :  IN_CUST_NM = replaceStr(object, k, "IN_CUST_NM"); break;
    					case "IN_CUST_TEL"            :  IN_CUST_TEL = replaceStr(object, k, "IN_CUST_TEL"); break;
    					case "IN_CUST_EMP_NM"         :  IN_CUST_EMP_NM = replaceStr(object, k, "IN_CUST_EMP_NM"); break;
    					case "EXPIRY_DATE"            :  EXPIRY_DATE = replaceStr(object, k, "EXPIRY_DATE"); break;
    					case "SALES_CUST_NM"          :  SALES_CUST_NM = replaceStr(object, k, "SALES_CUST_NM"); break;
    					case "ZIP"                    :  ZIP = replaceStr(object, k, "ZIP"); break;
    					case "ADDR"                   :  ADDR = replaceStr(object, k, "ADDR"); break;
    					case "ADDR2"                  :  ADDR = replaceStr(object, k, "ADDR2"); break;
    					case "PHONE_1"                :  PHONE_1 = replaceStr(object, k, "PHONE_1"); break;
    					case "ETC1"                   :  ETC1 = replaceStr(object, k, "ETC1"); break;
    					case "UNIT_NO"                :  UNIT_NO = replaceStr(object, k, "UNIT_NO"); break;
    					case "TIME_DATE"              :  TIME_DATE = replaceStr(object, k, "TIME_DATE"); break;
    					case "TIME_DATE_END"          :  TIME_DATE_END = replaceStr(object, k, "TIME_DATE_END"); break;
    					case "TIME_USE_END"           :  TIME_USE_END = replaceStr(object, k, "TIME_USE_END"); break;
    					case "PHONE_2"                :  PHONE_2 = replaceStr(object, k, "PHONE_2"); break;
    					case "BUY_CUST_NM"            :  BUY_CUST_NM = replaceStr(object, k, "BUY_CUST_NM"); break;
    					case "BUY_PHONE_1"            :  BUY_PHONE_1 = replaceStr(object, k, "BUY_PHONE_1"); break;
    					
    					case "BIZ_COND"               :  BIZ_COND = replaceStr(object, k, "BIZ_COND"); break;
    					case "BIZ_TYPE"               :  BIZ_TYPE = replaceStr(object, k, "BIZ_TYPE"); break;
    					case "BIZ_NO"                 :  BIZ_NO = replaceStr(object, k, "BIZ_NO"); break;
    					case "CUST_TYPE"              :  CUST_TYPE = replaceStr(object, k, "CUST_TYPE"); break;
    					
    					case "DATA_SENDER_NM"         :  DATA_SENDER_NM = replaceStr(object, k, "DATA_SENDER_NM"); break;
    					case "LEGACY_ORG_ORD_NO"      :  LEGACY_ORG_ORD_NO = replaceStr(object, k, "LEGACY_ORG_ORD_NO"); break;
    					case "ORD_DESC"               :  ORD_DESC = replaceStr(object, k, "ORD_DESC"); break;
    					case "DLV_MSG1"               :  DLV_MSG1 = replaceStr(object, k, "DLV_MSG1"); break;
    					case "DLV_MSG2"               :  DLV_MSG2 = replaceStr(object, k, "DLV_MSG2"); break;
    					
//            				case "LOT_TYPE"               :  LOT_TYPE = replaceStr(object, k, "LOT_TYPE"); break;
//            				case "OWNER_CD"               :  OWNER_CD = replaceStr(object, k, "OWNER_CD"); break;
    					
    					case "ORD_DEGREE"             :  ORD_DEGREE = replaceStr(object, k, "ORD_DEGREE");

    					default:
    						CUST_CD = (String)model.get("vrCustCd");
    						SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
    						CUST_SEQ= (String)model.get("vrCustSeq");
    						/* 엑셀에 차수 컬럼 필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩)*/
    						/* 주문삭제 : 차수 필요없어서 우선 주석처리 */
    						/*
    						if(ORD_DEGREE.equals("")){
    							ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
    						}
    						*/
    						break;
    					}
    				}
    				
    				String NO = Integer.toString(i+1);
    				no[i]               = NO;
    				
    				outReqDt[i]         = OUT_REQ_DT;    
    				inReqDt[i]          = IN_REQ_DT;    
    				custOrdNo[i]        = CUST_ORD_NO;    
    				custOrdSeq[i]       = CUST_ORD_SEQ;    
    				trustCustCd[i]      = TRUST_CUST_CD;    
    				
    				transCustCd[i]      = TRANS_CUST_CD;    
    				transCustTel[i]     = TRANS_CUST_TEL;    
    				transReqDt[i]       = TRANS_REQ_DT;    
    				custCd[i]           = CUST_CD;
    				ordQty[i]           = ORD_QTY;    
    				
    				uomCd[i]            = UOM_CD;    
    				sdeptCd[i]          = SDEPT_CD;    
    				salePerCd[i]        = SALE_PER_CD;    
    				carCd[i]            = CAR_CD;
    				drvNm[i]            = DRV_NM;
    				
    				dlvSeq[i]           = DLV_SEQ;    
    				drvTel[i]           = DRV_TEL;    
    				custLotNo[i]        = CUST_LOT_NO;    
    				blNo[i]             = BL_NO;    
    				recDt[i]            = REC_DT;    
    				
    				outWhCd[i]          = OUT_WH_CD;
    				inWhCd[i]           = IN_WH_CD;
    				makeDt[i]           = MAKE_DT;
    				timePeriodDay[i]    = TIME_PERIOD_DAY;
    				workYn[i]           = WORK_YN;
    				
    				rjType[i]           = RJ_TYPE;    
    				locYn[i]            = LOC_YN;    
    				confYn[i]           = CONF_YN;    
    				eaCapa[i]           = EA_CAPA;    
    				inOrdWeight[i]      = IN_ORD_WEIGHT;   
    				
    				itemCd[i]           = ITEM_CD;    
    				itemNm[i]           = ITEM_NM;    
    				transCustNm[i]      = TRANS_CUST_NM;    
    				transCustAddr[i]    = TRANS_CUST_ADDR;    
    				transEmpNm[i]       = TRANS_EMP_NM;    

    				remark[i]           = REMARK;    
    				transZipNo[i]       = TRANS_ZIP_NO;    
    				etc2[i]             = ETC2;    
    				unitAmt[i]          = UNIT_AMT;    
    				transBizNo[i]       = TRANS_BIZ_NO;    
    				
    				inCustAddr[i]       = IN_CUST_ADDR;   
    				inCustCd[i]         = IN_CUST_CD;    
    				inCustNm[i]         = IN_CUST_NM;    
    				inCustTel[i]        = IN_CUST_TEL;    
    				inCustEmpNm[i]      = IN_CUST_EMP_NM;    
    				
    				expiryDate[i]       = EXPIRY_DATE;
    				salesCustNm[i]      = SALES_CUST_NM;
    				zip[i]       		= ZIP;
    				addr[i]       		= ADDR;
    				addr2[i]       		= ADDR2;
    				phone1[i]       	= PHONE_1;
    				
    				etc1[i]      		= ETC1;
    				unitNo[i]      		= UNIT_NO;
    				timeDate[i]         = TIME_DATE;      
    				timeDateEnd[i]      = TIME_DATE_END;      
    				timeUseEnd[i]       = TIME_USE_END;  
    				
    				phone2[i]       	= PHONE_2;     
    				buyCustNm[i]       	= BUY_CUST_NM;     
    				buyPhone1[i]       	= BUY_PHONE_1;
    				salesCompanyNm[i]   = SALES_COMPANY_NM;
    				ordDegree[i]       	= ORD_DEGREE;
    				bizCond[i]       	= BIZ_COND;
    				bizType[i]       	= BIZ_TYPE;
    				bizNo[i]       		= BIZ_NO;
    				custType[i]       	= CUST_TYPE;
    				
    				dataSenderNm[i]       		= DATA_SENDER_NM;
    				legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
//                        invoiceNo[i]				= INVOICE_NO;
    				
    				custSeq[i]					= CUST_SEQ;
    				
    				ordDesc[i]     			  	= ORD_DESC;
    				dlvMsg1[i]       			= DLV_MSG1;
    				dlvMsg2[i]       			= DLV_MSG2;
    				
//                        lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
//                        ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가

//                        System.out.println("salesCompanyNm : --- " + salesCompanyNm[i]);
//                        System.out.println("dataSenderNm : --- " + dataSenderNm[i].getBytes().length);
    			}
    			
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    		   
    			
    			modelIns.put("vrOrdType"		, model.get("vrOrdType"));
    			modelIns.put("vrOrdSubtype"		, "153");
    			
    			modelIns.put("no"  , no);
    			if(model.get("vrOrdType").equals("I")){
    				modelIns.put("reqDt"     	, inReqDt);
    				modelIns.put("whCd"      	, inWhCd);
    			}else{
    				modelIns.put("reqDt"     	, outReqDt);
    				modelIns.put("whCd"      	, outWhCd);
    			}
    			modelIns.put("custOrdNo"    	, custOrdNo);
    			modelIns.put("custOrdSeq"   	, custOrdSeq);
    			modelIns.put("trustCustCd"  	, trustCustCd); //5
    			
    			modelIns.put("transCustCd"  	, transCustCd);
    			modelIns.put("transCustTel" 	, transCustTel);
    			modelIns.put("transReqDt"   	, transReqDt);
    			modelIns.put("custCd"       	, custCd);
    			modelIns.put("ordQty"       	, ordQty);      //10
    			
    			modelIns.put("uomCd"        	, uomCd);
    			modelIns.put("sdeptCd"      	, sdeptCd);
    			modelIns.put("salePerCd"    	, salePerCd);
    			modelIns.put("carCd"        	, carCd);
    			modelIns.put("drvNm"        	, drvNm);       //15
    			
    			modelIns.put("dlvSeq"       	, dlvSeq);
    			modelIns.put("drvTel"       	, drvTel);
    			modelIns.put("custLotNo"    	, custLotNo);
    			modelIns.put("blNo"         	, blNo);
    			modelIns.put("recDt"        	, recDt);       //20
    			
    			modelIns.put("makeDt"       	, makeDt);
    			modelIns.put("timePeriodDay"	, timePeriodDay);
    			modelIns.put("workYn"       	, workYn);
    			
    			modelIns.put("rjType"       	, rjType);
    			modelIns.put("locYn"        	, locYn);       //25
    			modelIns.put("confYn"       	, confYn);     
    			modelIns.put("eaCapa"       	, eaCapa);
    			modelIns.put("inOrdWeight"  	, inOrdWeight); //28
    			
    			modelIns.put("itemCd"           , itemCd);
    			modelIns.put("itemNm"           , itemNm);
    			modelIns.put("transCustNm"      , transCustNm);
    			modelIns.put("transCustAddr"    , transCustAddr);
    			modelIns.put("transEmpNm"       , transEmpNm);
    			
    			modelIns.put("remark"           , remark);
    			modelIns.put("transZipNo"       , transZipNo);
    			modelIns.put("etc2"             , etc2);
    			modelIns.put("unitAmt"          , unitAmt);
    			modelIns.put("transBizNo"       , transBizNo);
    			
    			modelIns.put("inCustAddr"       , inCustAddr);
    			modelIns.put("inCustCd"         , inCustCd);
    			modelIns.put("inCustNm"         , inCustNm);                 
    			modelIns.put("inCustTel"        , inCustTel);
    			modelIns.put("inCustEmpNm"      , inCustEmpNm);
    			
    			modelIns.put("expiryDate"       , expiryDate);
    			modelIns.put("salesCustNm"      , salesCustNm);
    			modelIns.put("zip"       		, zip);
    			modelIns.put("addr"       		, addr);
    			modelIns.put("addr2"       		, addr2);
    			modelIns.put("phone1"       	, phone1);
    			
    			modelIns.put("etc1"     	 	, etc1);
    			modelIns.put("unitNo"     	 	, unitNo);
    			modelIns.put("phone2"			, phone2);  
    			modelIns.put("buyCustNm"		, buyCustNm);  
    			modelIns.put("buyPhone1"		, buyPhone1);
    			modelIns.put("salesCompanyNm"		, salesCompanyNm);

    			modelIns.put("ordDegree"		, ordDegree);
    			modelIns.put("bizCond"		, bizCond);
    			modelIns.put("bizType"		, bizType);
    			modelIns.put("bizNo"		, bizNo);
    			modelIns.put("custType"		, custType);
    			
    			modelIns.put("dataSenderNm"		, dataSenderNm);
    			modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                    modelIns.put("invoiceNo"		, invoiceNo);
    			modelIns.put("custSeq"       	, custSeq);
    			
    			modelIns.put("ordDesc"       	, ordDesc);
    			modelIns.put("dlvMsg1"       	, dlvMsg1);
    			modelIns.put("dlvMsg2"       	, dlvMsg2);
    			
//                    modelIns.put("lotType"     		, lotType);  
//                    modelIns.put("ownerCd"     		, ownerCd);  
    			
    			modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
    			modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
    			modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                     일반 dao
    			modelIns = (Map<String, Object>)dao.saveExcelOrderB2TS(modelIns);	
    			
    			ServiceUtil.isValidReturnCode("WMSOP030SE4DH", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    			//m.put("O_CUR", modelIns.get("O_CUR"));       
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   	: saveExcelDelOrderJavaSimple
     * 대체 Method 설명     : 출고주문 삭제 업로드 
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelDelOrderJavaSimple(Map<String, Object> model,  List list) throws Exception {
    	
    	int listBodyCnt = (list != null)?list.size():0;
    	
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	Date today = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String formatted_today = sdf.format(today);
    	
    	try{
    		if(listBodyCnt > 0){
    			String[] no             = new String[listBodyCnt];     
    			
    			String[] outReqDt       = new String[listBodyCnt];         
    			String[] inReqDt        = new String[listBodyCnt];     
    			String[] custOrdNo      = new String[listBodyCnt];     
    			String[] custOrdSeq     = new String[listBodyCnt];    
    			String[] trustCustCd    = new String[listBodyCnt];     
    			
    			String[] transCustCd    = new String[listBodyCnt];                     
    			String[] transCustTel   = new String[listBodyCnt];         
    			String[] transReqDt     = new String[listBodyCnt];     
    			String[] custCd         = new String[listBodyCnt];
    			String[] ordQty         = new String[listBodyCnt];     
    			
    			String[] uomCd          = new String[listBodyCnt];                
    			String[] sdeptCd        = new String[listBodyCnt];         
    			String[] salePerCd      = new String[listBodyCnt];     
    			String[] carCd          = new String[listBodyCnt];     
    			String[] drvNm          = new String[listBodyCnt];     
    			
    			String[] dlvSeq         = new String[listBodyCnt];                
    			String[] drvTel         = new String[listBodyCnt];         
    			String[] custLotNo      = new String[listBodyCnt];     
    			String[] blNo           = new String[listBodyCnt];     
    			String[] recDt          = new String[listBodyCnt];     
    			
    			String[] outWhCd        = new String[listBodyCnt];                
    			String[] inWhCd         = new String[listBodyCnt];         
    			String[] makeDt         = new String[listBodyCnt];     
    			String[] timePeriodDay  = new String[listBodyCnt];     
    			String[] workYn         = new String[listBodyCnt];     
    			
    			String[] rjType         = new String[listBodyCnt];                
    			String[] locYn          = new String[listBodyCnt];         
    			String[] confYn         = new String[listBodyCnt];     
    			String[] eaCapa         = new String[listBodyCnt];     
    			String[] inOrdWeight    = new String[listBodyCnt];     
    			
    			String[] itemCd         = new String[listBodyCnt];                
    			String[] itemNm         = new String[listBodyCnt];         
    			String[] transCustNm    = new String[listBodyCnt];     
    			String[] transCustAddr  = new String[listBodyCnt];     
    			String[] transEmpNm     = new String[listBodyCnt];     
    			
    			String[] remark         = new String[listBodyCnt];                
    			String[] transZipNo     = new String[listBodyCnt];         
    			String[] etc2           = new String[listBodyCnt];     
    			String[] unitAmt        = new String[listBodyCnt];     
    			String[] transBizNo     = new String[listBodyCnt];     
    			
    			String[] inCustAddr     = new String[listBodyCnt];                
    			String[] inCustCd       = new String[listBodyCnt];         
    			String[] inCustNm       = new String[listBodyCnt];     
    			String[] inCustTel      = new String[listBodyCnt];     
    			String[] inCustEmpNm    = new String[listBodyCnt];     
    			
    			String[] expiryDate     = new String[listBodyCnt];
    			String[] salesCustNm    = new String[listBodyCnt];
    			String[] zip     		= new String[listBodyCnt];
    			String[] addr     		= new String[listBodyCnt];
    			String[] addr2     		= new String[listBodyCnt];
    			String[] phone1    	 	= new String[listBodyCnt];
    			
    			String[] etc1    		= new String[listBodyCnt];
    			String[] unitNo    		= new String[listBodyCnt];               
    			String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
    			String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
    			String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
    			
    			String[] phone2			= new String[listBodyCnt];   //고객전화번호2
    			String[] buyCustNm		= new String[listBodyCnt];   //주문자명
    			String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
    			String[] salesCompanyNm		= new String[listBodyCnt];   //salesCompanyNm
    			String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
    			String[] bizCond		= new String[listBodyCnt];   //업태
    			String[] bizType		= new String[listBodyCnt];   //업종
    			String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
    			String[] custType		= new String[listBodyCnt];   //화주타입
    			
    			String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
    			String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
    			
    			String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
    			String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
    			String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
    			String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
    			
    			
    			/* 컬럼 바인딩 제약조건 참조 flag */
    			String breakYnOrdDegree = "N";
    			
    			for(int i = 0 ; i < listBodyCnt ; i ++){
    				String OUT_REQ_DT		= formatted_today;//출고 예정일 -> 업로드 날짜
    				String IN_REQ_DT		= "";
    				String CUST_ORD_NO		= "";
    				String CUST_ORD_SEQ		= "";
    				String TRUST_CUST_CD	= "";
    				String TRANS_CUST_CD	= "";
    				String TRANS_CUST_TEL	= "";
    				String TRANS_REQ_DT		= "";
    				String CUST_CD			= "";
    				String ORD_QTY			= "";
    				String UOM_CD			= "";
    				String SDEPT_CD			= "";
    				String SALE_PER_CD		= "";
    				String CAR_CD			= "";
    				String DRV_NM			= "";
    				String DLV_SEQ			= "";
    				String DRV_TEL			= "";
    				String CUST_LOT_NO		= "";
    				String BL_NO			= "";
    				String REC_DT			= "";
    				String OUT_WH_CD		= "";
    				String IN_WH_CD			= "";
    				String MAKE_DT			= "";
    				String TIME_PERIOD_DAY	= "";
    				String WORK_YN			= "";
    				String RJ_TYPE			= "";
    				String LOC_YN			= "";
    				String CONF_YN			= "";
    				String EA_CAPA			= "";
    				String IN_ORD_WEIGHT	= "";
    				String ITEM_CD			= "";
    				String ITEM_NM			= "";
    				String TRANS_CUST_NM	= "";
    				String TRANS_CUST_ADDR	= "";
    				String TRANS_EMP_NM		= "";
    				String REMARK			= "";
    				String TRANS_ZIP_NO		= "";
    				String ETC2				= "";
    				String UNIT_AMT			= "";
    				String TRANS_BIZ_NO		= "";
    				String IN_CUST_ADDR		= "";
    				String IN_CUST_CD		= "";
    				String IN_CUST_NM		= "";
    				String IN_CUST_TEL		= "";
    				String IN_CUST_EMP_NM	= "";
    				String EXPIRY_DATE		= "";
    				String SALES_CUST_NM	= "";
    				String ZIP				= "";
    				String ADDR				= "";
    				String ADDR2			= "";
    				String PHONE_1			= "";
    				String ETC1				= "";
    				String UNIT_NO			= "";
    				String TIME_DATE		= "";
    				String TIME_DATE_END	= "";
    				String TIME_USE_END		= "";
    				String PHONE_2			= "";
    				String BUY_CUST_NM		= "";
    				String BUY_PHONE_1		= "";
    				String SALES_COMPANY_NM		= "";
    				String ORD_DEGREE		= "";
    				String BIZ_COND		= "";
    				String BIZ_TYPE		= "";
    				String BIZ_NO		= "";
    				String CUST_TYPE		= "";
    				String DATA_SENDER_NM = "";
    				String LEGACY_ORG_ORD_NO ="";
    				String CUST_SEQ = "";
    				String ORD_DESC = "";
    				String DLV_MSG1 = "";
    				String DLV_MSG2 = "";

    				String NO = Integer.toString(i+1);
    				no[i]               = NO;
    				
    				outReqDt[i]         = OUT_REQ_DT;    
    				inReqDt[i]          = IN_REQ_DT;    
    				custOrdNo[i]        = CUST_ORD_NO;    
    				custOrdSeq[i]       = CUST_ORD_SEQ;    
    				trustCustCd[i]      = TRUST_CUST_CD;    
    				
    				transCustCd[i]      = TRANS_CUST_CD;    
    				transCustTel[i]     = TRANS_CUST_TEL;    
    				transReqDt[i]       = TRANS_REQ_DT;    
    				custCd[i]           = CUST_CD;
    				ordQty[i]           = ORD_QTY;    
    				
    				uomCd[i]            = UOM_CD;    
    				sdeptCd[i]          = SDEPT_CD;    
    				salePerCd[i]        = SALE_PER_CD;    
    				carCd[i]            = CAR_CD;
    				drvNm[i]            = DRV_NM;
    				
    				dlvSeq[i]           = DLV_SEQ;    
    				drvTel[i]           = DRV_TEL;    
    				custLotNo[i]        = CUST_LOT_NO;    
    				blNo[i]             = BL_NO;    
    				recDt[i]            = REC_DT;    
    				
    				outWhCd[i]          = OUT_WH_CD;
    				inWhCd[i]           = IN_WH_CD;
    				makeDt[i]           = MAKE_DT;
    				timePeriodDay[i]    = TIME_PERIOD_DAY;
    				workYn[i]           = WORK_YN;
    				
    				rjType[i]           = RJ_TYPE;    
    				locYn[i]            = LOC_YN;    
    				confYn[i]           = CONF_YN;    
    				eaCapa[i]           = EA_CAPA;    
    				inOrdWeight[i]      = IN_ORD_WEIGHT;   
    				
    				itemCd[i]           = ITEM_CD;    
    				itemNm[i]           = ITEM_NM;    
    				transCustNm[i]      = TRANS_CUST_NM;    
    				transCustAddr[i]    = TRANS_CUST_ADDR;    
    				transEmpNm[i]       = TRANS_EMP_NM;    

    				remark[i]           = REMARK;    
    				transZipNo[i]       = TRANS_ZIP_NO;    
    				etc2[i]             = ETC2;    
    				unitAmt[i]          = UNIT_AMT;    
    				transBizNo[i]       = TRANS_BIZ_NO;    
    				
    				inCustAddr[i]       = IN_CUST_ADDR;   
    				inCustCd[i]         = IN_CUST_CD;    
    				inCustNm[i]         = IN_CUST_NM;    
    				inCustTel[i]        = IN_CUST_TEL;    
    				inCustEmpNm[i]      = IN_CUST_EMP_NM;    
    				
    				expiryDate[i]       = EXPIRY_DATE;
    				salesCustNm[i]      = SALES_CUST_NM;
    				zip[i]       		= ZIP;
    				addr[i]       		= ADDR;
    				addr2[i]       		= ADDR2;
    				phone1[i]       	= PHONE_1;
    				
    				etc1[i]      		= ETC1;
    				unitNo[i]      		= UNIT_NO;
    				timeDate[i]         = TIME_DATE;      
    				timeDateEnd[i]      = TIME_DATE_END;      
    				timeUseEnd[i]       = TIME_USE_END;  
    				
    				phone2[i]       	= PHONE_2;     
    				buyCustNm[i]       	= BUY_CUST_NM;     
    				buyPhone1[i]       	= BUY_PHONE_1;
    				salesCompanyNm[i]   = SALES_COMPANY_NM;
    				ordDegree[i]       	= ORD_DEGREE;
    				bizCond[i]       	= BIZ_COND;
    				bizType[i]       	= BIZ_TYPE;
    				bizNo[i]       		= BIZ_NO;
    				custType[i]       	= CUST_TYPE;
    				
    				dataSenderNm[i]       		= DATA_SENDER_NM;
    				legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
    				custSeq[i]					= CUST_SEQ;
    				
    				ordDesc[i]     			  	= ORD_DESC;
    				dlvMsg1[i]       			= DLV_MSG1;
    				dlvMsg2[i]       			= DLV_MSG2;
    				
    			}
    			
    			//프로시져에 보낼것들 다담는다
    			Map<String, Object> modelIns = new HashMap<String, Object>();
    		   
    			
    			modelIns.put("vrOrdType"		, model.get("vrOrdType"));
    			modelIns.put("vrOrdSubtype"		, "153");
    			
    			modelIns.put("no"  , no);
    			if(model.get("vrOrdType").equals("I")){
    				modelIns.put("reqDt"     	, inReqDt);
    				modelIns.put("whCd"      	, inWhCd);
    			}else{
    				modelIns.put("reqDt"     	, outReqDt);
    				modelIns.put("whCd"      	, outWhCd);
    			}
    			modelIns.put("custOrdNo"    	, custOrdNo);
    			modelIns.put("custOrdSeq"   	, custOrdSeq);
    			modelIns.put("trustCustCd"  	, trustCustCd); //5
    			
    			modelIns.put("transCustCd"  	, transCustCd);
    			modelIns.put("transCustTel" 	, transCustTel);
    			modelIns.put("transReqDt"   	, transReqDt);
    			modelIns.put("custCd"       	, custCd);
    			modelIns.put("ordQty"       	, ordQty);      //10
    			
    			modelIns.put("uomCd"        	, uomCd);
    			modelIns.put("sdeptCd"      	, sdeptCd);
    			modelIns.put("salePerCd"    	, salePerCd);
    			modelIns.put("carCd"        	, carCd);
    			modelIns.put("drvNm"        	, drvNm);       //15
    			
    			modelIns.put("dlvSeq"       	, dlvSeq);
    			modelIns.put("drvTel"       	, drvTel);
    			modelIns.put("custLotNo"    	, custLotNo);
    			modelIns.put("blNo"         	, blNo);
    			modelIns.put("recDt"        	, recDt);       //20
    			
    			modelIns.put("makeDt"       	, makeDt);
    			modelIns.put("timePeriodDay"	, timePeriodDay);
    			modelIns.put("workYn"       	, workYn);
    			
    			modelIns.put("rjType"       	, rjType);
    			modelIns.put("locYn"        	, locYn);       //25
    			modelIns.put("confYn"       	, confYn);     
    			modelIns.put("eaCapa"       	, eaCapa);
    			modelIns.put("inOrdWeight"  	, inOrdWeight); //28
    			
    			modelIns.put("itemCd"           , itemCd);
    			modelIns.put("itemNm"           , itemNm);
    			modelIns.put("transCustNm"      , transCustNm);
    			modelIns.put("transCustAddr"    , transCustAddr);
    			modelIns.put("transEmpNm"       , transEmpNm);
    			
    			modelIns.put("remark"           , remark);
    			modelIns.put("transZipNo"       , transZipNo);
    			modelIns.put("etc2"             , etc2);
    			modelIns.put("unitAmt"          , unitAmt);
    			modelIns.put("transBizNo"       , transBizNo);
    			
    			modelIns.put("inCustAddr"       , inCustAddr);
    			modelIns.put("inCustCd"         , inCustCd);
    			modelIns.put("inCustNm"         , inCustNm);                 
    			modelIns.put("inCustTel"        , inCustTel);
    			modelIns.put("inCustEmpNm"      , inCustEmpNm);
    			
    			modelIns.put("expiryDate"       , expiryDate);
    			modelIns.put("salesCustNm"      , salesCustNm);
    			modelIns.put("zip"       		, zip);
    			modelIns.put("addr"       		, addr);
    			modelIns.put("addr2"       		, addr2);
    			modelIns.put("phone1"       	, phone1);
    			
    			modelIns.put("etc1"     	 	, etc1);
    			modelIns.put("unitNo"     	 	, unitNo);
    			modelIns.put("phone2"			, phone2);  
    			modelIns.put("buyCustNm"		, buyCustNm);  
    			modelIns.put("buyPhone1"		, buyPhone1);
    			modelIns.put("salesCompanyNm"		, salesCompanyNm);

    			modelIns.put("ordDegree"		, ordDegree);
    			modelIns.put("bizCond"		, bizCond);
    			modelIns.put("bizType"		, bizType);
    			modelIns.put("bizNo"		, bizNo);
    			modelIns.put("custType"		, custType);
    			
    			modelIns.put("dataSenderNm"		, dataSenderNm);
    			modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                    modelIns.put("invoiceNo"		, invoiceNo);
    			modelIns.put("custSeq"       	, custSeq);
    			
    			modelIns.put("ordDesc"       	, ordDesc);
    			modelIns.put("dlvMsg1"       	, dlvMsg1);
    			modelIns.put("dlvMsg2"       	, dlvMsg2);
    			
//                    modelIns.put("lotType"     		, lotType);  
//                    modelIns.put("ownerCd"     		, ownerCd);  
    			
    			modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
    			modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
    			modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                     일반 dao
    			modelIns = (Map<String, Object>)dao.saveExcelOrderB2TS(modelIns);	
    			
    			ServiceUtil.isValidReturnCode("WMSOP030SE4DH", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    			//m.put("O_CUR", modelIns.get("O_CUR"));       
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   throw e;
    	}
       
        return m;
          
    }
    
    
    /**
     * Method ID         : getTemplateInfoV2
     * Method 설명       : 저장된 템플릿 정보 조회.
     * 작성자            : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getTemplateInfoV2(Map<String, Object> model) throws Exception {
    	List<Map<String, Object>> m = new ArrayList();
    	try {
    		Map<String, Object> searchParam = new HashMap<String, Object>();
    		searchParam.putAll(model);
    		searchParam.put("vrTemplateType"     	, model.get("vrTemplateType"));
    		searchParam.put("vrCustSeq"        		, model.get("vrCustSeq"));
    		
    		m.addAll(dao.getTemplateInfoV2(searchParam));		//Template 조회
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID     : saveExcelOrderJavaCommonB2T
     * 대체 Method 설명   : B2C 템플릿 업로드 - 공통 (공란적용)
     * 작성자             : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaCommonB2T(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	
    	List list = (ArrayList)model.get("LIST");
    	
    	// 비즈컨설팅 B2C 로직 (임시)
    	if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("O")){ 
    		// 수취인명으로 SORT
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("SALES_CUST_NM");
    				String name2 = (String) o2.get("SALES_CUST_NM");
    				return name1.compareTo(name2);
    			}
    		}); 
    		
    		Map<String, Object> prevModel = (Map<String, Object>) list.get(0);
    		String pCustomerInfo = (String) prevModel.get("SALES_CUST_NM") + prevModel.get("ADDR");
    		
    		int cnt = 1;
    		SimpleDateFormat format1 = new SimpleDateFormat("yyMMddHHmmss");
    		String orgOrdId = format1.format (System.currentTimeMillis()) + "-"; 
    		
    		for(int i = 0; i < list.size(); ++i){
    			Map<String, Object> loopModel = (Map<String, Object>) list.get(i);
    			String lCustomerInfo = (String) loopModel.get("SALES_CUST_NM") + loopModel.get("ADDR");
    			
    			if(!pCustomerInfo.trim().equals(lCustomerInfo.trim())){
    				pCustomerInfo = (String) loopModel.get("SALES_CUST_NM") + loopModel.get("ADDR");
    				cnt += 1;
    			}
    			loopModel.put("CUST_ORD_NO", orgOrdId + cnt);
    		}
    	}
    	
		List<Map<String, Object>> listBody = list;
		String custId = String.valueOf(model.get("vrCustCd"));
		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	Date today = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String formatted_today = sdf.format(today);
    	
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm	= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
                String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
//                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
                
                String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
                String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
                String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
                String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
                String[] locCd              =  new String[listBodyCnt];   //로케이션코드
                
//                String[] lotType     		= new String[listBodyCnt];   //LOT속성 	2022-03-04
//                String[] ownerCd     		= new String[listBodyCnt];   //소유자	2022-03-04

                
                /* 컬럼 바인딩 제약조건 참조 flag */
                String breakYnOrdDegree = "N";
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= formatted_today;//출고 예정일 -> 업로드 날짜
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			String DATA_SENDER_NM = "";
        			String LEGACY_ORG_ORD_NO ="";
//        			String INVOICE_NO ="";
        			String CUST_SEQ = "";
        			String ORD_DESC = "";
        			String DLV_MSG1 = "";
        			String DLV_MSG2 = "";
        			String LOC_CD = "";
        			
//        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
//        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				Map<String, Object> object = listBody.get(i);
        				String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
        				
        				switch (bindColNm) {
        				//case "OUT_REQ_DT"             :  OUT_REQ_DT = replaceStr(object, k, "OUT_REQ_DT"); break;
        				case "IN_REQ_DT"              :  IN_REQ_DT = replaceStr(object, k, "IN_REQ_DT"); break;
        				case "CUST_ORD_NO"            :  CUST_ORD_NO = replaceStr(object, k, "CUST_ORD_NO"); break;
        				case "CUST_ORD_SEQ"           :  CUST_ORD_SEQ = replaceStr(object, k, "CUST_ORD_SEQ"); break;
        				case "TRUST_CUST_CD"          :  TRUST_CUST_CD = replaceStr(object, k, "TRUST_CUST_CD"); break;
        				case "TRANS_CUST_CD"          :  TRANS_CUST_CD = replaceStr(object, k, "TRANS_CUST_CD"); break;
        				case "TRANS_CUST_TEL"         :  TRANS_CUST_TEL = replaceStr(object, k, "TRANS_CUST_TEL"); break;
        				case "TRANS_REQ_DT"           :  TRANS_REQ_DT = replaceStr(object, k, "TRANS_REQ_DT"); break;
        				
                        case "ORD_QTY"                :  ORD_QTY = replaceStr(object, k, "ORD_QTY"); break;
        				case "UOM_CD"                 :  UOM_CD = replaceStr(object, k, "UOM_CD"); break;
        				case "SDEPT_CD"               :  SDEPT_CD = replaceStr(object, k, "SDEPT_CD"); break;
        				case "SALE_PER_CD"            :  SALE_PER_CD = replaceStr(object, k, "SALE_PER_CD"); break;
        				case "CAR_CD"                 :  CAR_CD = replaceStr(object, k, "CAR_CD"); break;
        				case "DRV_NM"                 :  DRV_NM = replaceStr(object, k, "DRV_NM"); break;
        				case "DLV_SEQ"                :  DLV_SEQ = replaceStr(object, k, "DLV_SEQ"); break;
        				case "DRV_TEL"                :  DRV_TEL = replaceStr(object, k, "DRV_TEL"); break;
        				case "CUST_LOT_NO"            :  CUST_LOT_NO = replaceStr(object, k, "CUST_LOT_NO"); break;
        				case "BL_NO"                  :  BL_NO = replaceStr(object, k, "BL_NO"); break;
        				case "REC_DT"                 :  REC_DT = replaceStr(object, k, "REC_DT"); break;
        				case "OUT_WH_CD"              :  OUT_WH_CD = replaceStr(object, k, "OUT_WH_CD"); break;
        				case "IN_WH_CD"               :  IN_WH_CD = replaceStr(object, k, "IN_WH_CD"); break;
        				case "MAKE_DT"                :  MAKE_DT = replaceStr(object, k, "MAKE_DT"); break;
        				case "TIME_PERIOD_DAY"        :  TIME_PERIOD_DAY = replaceStr(object, k, "TIME_PERIOD_DAY"); break;
        				case "WORK_YN"                :  WORK_YN = replaceStr(object, k, "WORK_YN"); break;
        				case "RJ_TYPE"                :  RJ_TYPE = replaceStr(object, k, "RJ_TYPE"); break;
        				case "LOC_YN"                 :  LOC_YN = replaceStr(object, k, "LOC_YN"); break;
        				case "CONF_YN"                :  CONF_YN = replaceStr(object, k, "CONF_YN"); break;
        				case "EA_CAPA"                :  EA_CAPA = replaceStr(object, k, "EA_CAPA"); break;
        				case "IN_ORD_WEIGHT"          :  IN_ORD_WEIGHT = replaceStr(object, k, "IN_ORD_WEIGHT"); break;
        				case "ITEM_CD"                :  ITEM_CD = replaceStr(object, k, "ITEM_CD"); break;
        				case "ITEM_NM"                :  ITEM_NM = replaceStr(object, k, "ITEM_NM"); break;
        				case "TRANS_CUST_NM"          :  TRANS_CUST_NM = replaceStr(object, k, "TRANS_CUST_NM"); break;
        				case "TRANS_CUST_ADDR"        :  TRANS_CUST_ADDR = replaceStr(object, k, "TRANS_CUST_ADDR"); break;
        				case "TRANS_EMP_NM"           :  TRANS_EMP_NM = replaceStr(object, k, "TRANS_EMP_NM"); break;
        				case "REMARK"                 :  REMARK = replaceStr(object, k, "REMARK"); break;
        				case "TRANS_ZIP_NO"           :  TRANS_ZIP_NO = replaceStr(object, k, "TRANS_ZIP_NO"); break;
        				case "ETC2"                   :  ETC2 = replaceStr(object, k, "ETC2"); break;
        				case "UNIT_AMT"               :  UNIT_AMT = replaceStr(object, k, "UNIT_AMT"); break;
        				case "TRANS_BIZ_NO"           :  TRANS_BIZ_NO = replaceStr(object, k, "TRANS_BIZ_NO"); break;
        				case "IN_CUST_ADDR"           :  IN_CUST_ADDR = replaceStr(object, k, "IN_CUST_ADDR"); break;
        				case "IN_CUST_CD"             :  IN_CUST_CD = replaceStr(object, k, "IN_CUST_CD"); break;
        				case "IN_CUST_NM"             :  IN_CUST_NM = replaceStr(object, k, "IN_CUST_NM"); break;
        				case "IN_CUST_TEL"            :  IN_CUST_TEL = replaceStr(object, k, "IN_CUST_TEL"); break;
        				case "IN_CUST_EMP_NM"         :  IN_CUST_EMP_NM = replaceStr(object, k, "IN_CUST_EMP_NM"); break;
        				case "EXPIRY_DATE"            :  EXPIRY_DATE = replaceStr(object, k, "EXPIRY_DATE"); break;
        				case "SALES_CUST_NM"          :  SALES_CUST_NM = replaceStr(object, k, "SALES_CUST_NM"); break;
        				case "ZIP"                    :  ZIP = replaceStr(object, k, "ZIP"); break;
        				case "ADDR"                   :  ADDR = replaceStr(object, k, "ADDR"); break;
        				case "ADDR2"                  :  ADDR = replaceStr(object, k, "ADDR2"); break;
        				case "PHONE_1"                :  PHONE_1 = replaceStr(object, k, "PHONE_1"); break;
        				case "ETC1"                   :  ETC1 = replaceStr(object, k, "ETC1"); break;
        				case "UNIT_NO"                :  UNIT_NO = replaceStr(object, k, "UNIT_NO"); break;
        				case "TIME_DATE"              :  TIME_DATE = replaceStr(object, k, "TIME_DATE"); break;
        				case "TIME_DATE_END"          :  TIME_DATE_END = replaceStr(object, k, "TIME_DATE_END"); break;
        				case "TIME_USE_END"           :  TIME_USE_END = replaceStr(object, k, "TIME_USE_END"); break;
        				case "PHONE_2"                :  PHONE_2 = replaceStr(object, k, "PHONE_2"); break;
        				case "BUY_CUST_NM"            :  BUY_CUST_NM = replaceStr(object, k, "BUY_CUST_NM"); break;
        				case "BUY_PHONE_1"            :  BUY_PHONE_1 = replaceStr(object, k, "BUY_PHONE_1"); break;
        				
                        case "BIZ_COND"               :  BIZ_COND = replaceStr(object, k, "BIZ_COND"); break;
        				case "BIZ_TYPE"               :  BIZ_TYPE = replaceStr(object, k, "BIZ_TYPE"); break;
        				case "BIZ_NO"                 :  BIZ_NO = replaceStr(object, k, "BIZ_NO"); break;
        				case "CUST_TYPE"              :  CUST_TYPE = replaceStr(object, k, "CUST_TYPE"); break;
        				
        				case "DATA_SENDER_NM"         :  DATA_SENDER_NM = replaceStr(object, k, "DATA_SENDER_NM"); break;
        				case "LEGACY_ORG_ORD_NO"      :  LEGACY_ORG_ORD_NO = replaceStr(object, k, "LEGACY_ORG_ORD_NO"); break;
        				case "ORD_DESC"               :  ORD_DESC = replaceStr(object, k, "ORD_DESC"); break;
        				case "DLV_MSG1"               :  DLV_MSG1 = replaceStr(object, k, "DLV_MSG1"); break;
        				case "DLV_MSG2"               :  DLV_MSG2 = replaceStr(object, k, "DLV_MSG2"); break;
        				case "LOC_CD"                 :  LOC_CD = replaceStr(object, k, "LOC_CD"); break;
        				
//        				case "LOT_TYPE"               :  LOT_TYPE = replaceStr(object, k, "LOT_TYPE"); break;
//        				case "OWNER_CD"               :  OWNER_CD = replaceStr(object, k, "OWNER_CD"); break;
        				
        				case "ORD_DEGREE"             :  ORD_DEGREE = replaceStr(object, k, "ORD_DEGREE");

						default:
							CUST_CD = (String)model.get("vrCustCd");
							SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
							CUST_SEQ= (String)model.get("vrCustSeq");
							/* 엑셀에 차수 컬럼 필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩)*/
							if(ORD_DEGREE.equals("")){
								ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
							}
							
							break;
						}
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                    
                    dataSenderNm[i]       		= DATA_SENDER_NM;
                    legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
//                    invoiceNo[i]				= INVOICE_NO;
                    
                    custSeq[i]					= CUST_SEQ;
                    
                    ordDesc[i]     			  	= ORD_DESC;
                    dlvMsg1[i]       			= DLV_MSG1;
                    dlvMsg2[i]       			= DLV_MSG2;
                    locCd[i]                    = LOC_CD;
                    
//                    lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
//                    ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가

                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
               
                
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"	, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"		    , bizCond);
                modelIns.put("bizType"		    , bizType);
                modelIns.put("bizNo"		    , bizNo);
                modelIns.put("custType"		    , custType);
                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                modelIns.put("invoiceNo"		, invoiceNo);
                modelIns.put("custSeq"       	, custSeq);
                
                modelIns.put("ordDesc"       	, ordDesc);
                modelIns.put("dlvMsg1"       	, dlvMsg1);
                modelIns.put("dlvMsg2"       	, dlvMsg2);
                modelIns.put("locCd"            , locCd);
                
//                modelIns.put("lotType"     		, lotType);  
//                modelIns.put("ownerCd"     		, ownerCd);  
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2T(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e) {
           throw e;
        }
        /*
        catch(Exception e){
        	m.put("errCnt", -1);
        	m.put("MSG", e.getMessage());
            m.put("MSG_ORA", e.getMessage());
        }
        */
        return m;
    } 
    
    
    /**
     * 
     * 대체 Method ID   	: saveExcelOrderJavaAllowBlankALL
     * 대체 Method 설명     : 템플릿 주문 저장 - 공란허용 로직 + 주문유형 + 화주통합
     * 작성자               : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaAllowBlankALL(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
    	List list = (ArrayList)model.get("LIST");
    	
    	// 비즈컨설팅 B2B 거래처 정렬
    	if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("O")){  
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("TRANS_CUST_CD");
    				String name2 = (String) o2.get("TRANS_CUST_CD");
    				return name1.compareTo(name2);
    			}
    		});
    	} 
    	// 비즈컨설팅 신상입고
    	else if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("I") && model.get("vrOrdSubType").equals("20")){
    		
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("ITEM_CD");
    				String name2 = (String) o2.get("ITEM_CD");
    				return name1.compareTo(name2);
    			}
    		});
    		
    		HashMap keyMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
    			// 입고처와 LOT NO가 없는 상품들만 수량 합침
    			if((loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals("")) ||(!loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals(""))){
    				
    				String itemCd_Etc2 = String.valueOf(loopMap.get("ITEM_CD"));
    				
    				if(keyMap.containsKey(itemCd_Etc2)){
    					String n1 = (String) keyMap.get(itemCd_Etc2);
    					String n2 = (String) loopMap.get("ORD_QTY");
    					keyMap.put(itemCd_Etc2, String.valueOf(Integer.parseInt(n1) + Integer.parseInt(n2)));
    				}else{
    					keyMap.put(itemCd_Etc2, loopMap.get("ORD_QTY"));
    				}
    			}
    		}
    		
    		List rstList = new ArrayList();
    		HashMap rstMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
    			if((loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals("")) ||(!loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals(""))){
    				
    				String itemCd_Etc2 = String.valueOf(loopMap.get("ITEM_CD"));
    				
    				if(!rstMap.containsKey(itemCd_Etc2)){
    					loopMap.put("ORD_QTY", keyMap.get(itemCd_Etc2));
    					rstMap.put((String) itemCd_Etc2, keyMap.get(loopMap.get("ITEM_CD")));
    					rstList.add(loopMap);
    				}
    			}else{
    				rstList.add(loopMap);
    			}
    		}
    		
    		list = rstList;
    	}
    	// 비즈컨설팅 매장반품입고 로직
    	else if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("I") && model.get("vrOrdSubType").equals("159")){
    		
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("ITEM_CD");
    				String name2 = (String) o2.get("ITEM_CD");
    				return name1.compareTo(name2);
    			}
    		});
    		
    		HashMap keyMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
    			if(keyMap.containsKey(loopMap.get("ITEM_CD"))){
    				String n1 = (String) keyMap.get(loopMap.get("ITEM_CD"));
    				String n2 = (String) loopMap.get("ORD_QTY");
    				keyMap.put(loopMap.get("ITEM_CD"), String.valueOf(Integer.parseInt(n1) + Integer.parseInt(n2)));
    			}else{
    				keyMap.put(loopMap.get("ITEM_CD"), loopMap.get("ORD_QTY"));
    			}
    		}
    		
    		List rstList = new ArrayList();
    		HashMap rstMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
    			if(!rstMap.containsKey(loopMap.get("ITEM_CD"))){
    				loopMap.put("ORD_QTY", keyMap.get(loopMap.get("ITEM_CD")));
    				rstMap.put((String) loopMap.get("ITEM_CD"), keyMap.get(loopMap.get("ITEM_CD")));
    				rstList.add(loopMap);
    			}
    		}
    		
    		list = rstList;
    	}
    	
    	List<Map<String, Object>> listBody = list;
    	String custId = String.valueOf(model.get("vrCustCd"));
    	
    	List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
    	
    	int listHeaderCnt = listHeader.size();
    	int listBodyCnt   = listBody.size();
    	
    	
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		if(listBodyCnt > 0){
    			String[] no             = new String[listBodyCnt];     
    			
    			String[] outReqDt       = new String[listBodyCnt];         
    			String[] inReqDt        = new String[listBodyCnt];     
    			String[] custOrdNo      = new String[listBodyCnt];     
    			String[] custOrdSeq     = new String[listBodyCnt];    
    			String[] trustCustCd    = new String[listBodyCnt];     
    			
    			String[] transCustCd    = new String[listBodyCnt];                     
    			String[] transCustTel   = new String[listBodyCnt];         
    			String[] transReqDt     = new String[listBodyCnt];     
    			String[] custCd         = new String[listBodyCnt];     
    			String[] ordQty         = new String[listBodyCnt];     
    			
    			String[] uomCd          = new String[listBodyCnt];                
    			String[] sdeptCd        = new String[listBodyCnt];         
    			String[] salePerCd      = new String[listBodyCnt];     
    			String[] carCd          = new String[listBodyCnt];     
    			String[] drvNm          = new String[listBodyCnt];     
    			
    			String[] dlvSeq         = new String[listBodyCnt];                
    			String[] drvTel         = new String[listBodyCnt];         
    			String[] custLotNo      = new String[listBodyCnt];     
    			String[] blNo           = new String[listBodyCnt];     
    			String[] recDt          = new String[listBodyCnt];     
    			
    			String[] outWhCd        = new String[listBodyCnt];                
    			String[] inWhCd         = new String[listBodyCnt];         
    			String[] makeDt         = new String[listBodyCnt];     
    			String[] timePeriodDay  = new String[listBodyCnt];     
    			String[] workYn         = new String[listBodyCnt];     
    			
    			String[] rjType         = new String[listBodyCnt];                
    			String[] locYn          = new String[listBodyCnt];         
    			String[] confYn         = new String[listBodyCnt];     
    			String[] eaCapa         = new String[listBodyCnt];     
    			String[] inOrdWeight    = new String[listBodyCnt];     
    			
    			String[] itemCd         = new String[listBodyCnt];                
    			String[] itemNm         = new String[listBodyCnt];         
    			String[] transCustNm    = new String[listBodyCnt];     
    			String[] transCustAddr  = new String[listBodyCnt];     
    			String[] transEmpNm     = new String[listBodyCnt];     
    			
    			String[] remark         = new String[listBodyCnt];                
    			String[] transZipNo     = new String[listBodyCnt];         
    			String[] etc2           = new String[listBodyCnt];     
    			String[] unitAmt        = new String[listBodyCnt];     
    			String[] transBizNo     = new String[listBodyCnt];     
    			
    			String[] inCustAddr     = new String[listBodyCnt];                
    			String[] inCustCd       = new String[listBodyCnt];         
    			String[] inCustNm       = new String[listBodyCnt];     
    			String[] inCustTel      = new String[listBodyCnt];     
    			String[] inCustEmpNm    = new String[listBodyCnt];     
    			
    			String[] expiryDate     = new String[listBodyCnt];
    			String[] salesCustNm    = new String[listBodyCnt];
    			String[] zip     		= new String[listBodyCnt];
    			String[] addr     		= new String[listBodyCnt];
    			String[] phone1    	 	= new String[listBodyCnt];
    			
    			String[] etc1    		= new String[listBodyCnt];
    			String[] unitNo    		= new String[listBodyCnt];               
    			String[] salesCompanyNm	 	= new String[listBodyCnt];
    			String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
    			String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
    			
    			String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
    			String[] locCd     		= new String[listBodyCnt];   //로케이션코드
    			
    			String[] epType     	= new String[listBodyCnt];   //납품유형
				String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
				String[] ownerCd     	= new String[listBodyCnt];   //소유자		2022-03-04
				
				
				String[] custLegacyItemCd = new String[listBodyCnt]; //화주 상품코드	2022-05-24
				String[] itemBarcode    = new String[listBodyCnt];   //상품 바코드		2022-05-24
				String[] itemSize     	= new String[listBodyCnt];   //상품 사이즈		2022-05-24
				String[] color     		= new String[listBodyCnt];   //상품 색상		2022-05-24
				
				for(int i = 0 ; i < listBodyCnt ; i ++){
					String OUT_REQ_DT      = "";
					String IN_REQ_DT       = "";
					String CUST_ORD_NO     = "";
					String CUST_ORD_SEQ    = "";
					String TRUST_CUST_CD   = "";
					String TRANS_CUST_CD   = "";
					String TRANS_CUST_TEL  = "";
					String TRANS_REQ_DT    = "";
					String CUST_CD         = "";
					String ORD_QTY         = "";
					String UOM_CD          = "";
					String SDEPT_CD        = "";
					String SALE_PER_CD     = "";
					String CAR_CD          = "";
					String DRV_NM          = "";
					String DLV_SEQ         = "";
					String DRV_TEL         = "";
					String CUST_LOT_NO     = "";
					String BL_NO           = "";
					String REC_DT          = "";
					String OUT_WH_CD       = "";
					String IN_WH_CD        = "";
					String MAKE_DT         = "";
					String TIME_PERIOD_DAY = "";
					String WORK_YN         = "";
					String RJ_TYPE         = "";
					String LOC_YN          = "";
					String CONF_YN         = "";
					String EA_CAPA         = "";
					String IN_ORD_WEIGHT   = "";
					String ITEM_CD         = "";
					String ITEM_NM         = "";
					String TRANS_CUST_NM   = "";
					String TRANS_CUST_ADDR = "";
					String TRANS_EMP_NM    = "";
					String REMARK          = "";
					String TRANS_ZIP_NO    = "";
					String ETC2            = "";
					String UNIT_AMT        = "";
					String TRANS_BIZ_NO    = "";
					String IN_CUST_ADDR    = "";
					String IN_CUST_CD      = "";
					String IN_CUST_NM      = "";
					String IN_CUST_TEL     = "";
					String IN_CUST_EMP_NM  = "";
					String EXPIRY_DATE     = "";
					String SALES_CUST_NM   = "";
					String ZIP             = "";
					String ADDR            = "";
					String PHONE_1         = "";
					String ETC1            = "";
					String UNIT_NO         = "";
					String SALES_COMPANY_NM   = "";
					String TIME_DATE       = "";
					String TIME_DATE_END   = "";
					String TIME_USE_END    = "";
					String LOC_CD    	   = "";
					String EP_TYPE    	   = "";
					
					String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
					String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04
					
					String CUST_LEGACY_ITEM_CD = "";
					String ITEM_BARCODE		   = "";
					String ITEM_SIZE		   = "";
					String COLOR			   = "";
					
					for(int k = 0 ; k < listHeaderCnt ; k++){
						Map<String, Object> object = listBody.get(i);
						String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
						switch (bindColNm) {
						case "OUT_REQ_DT" 			: OUT_REQ_DT        	= replaceStr(object, k, "OUT_REQ_DT"); break;
						case "IN_REQ_DT"			: IN_REQ_DT				= replaceStr(object, k, "IN_REQ_DT"); break;
						case "CUST_ORD_NO"			: CUST_ORD_NO			= replaceStr(object, k, "CUST_ORD_NO"); break;
						case "CUST_ORD_SEQ"			: CUST_ORD_SEQ			= replaceStr(object, k, "CUST_ORD_SEQ"); break;
						case "TRUST_CUST_CD"		: TRUST_CUST_CD			= replaceStr(object, k, "TRUST_CUST_CD"); break;
						case "TRANS_CUST_CD"		: TRANS_CUST_CD			= replaceStr(object, k, "TRANS_CUST_CD"); break;
						case "TRANS_CUST_TEL"		: TRANS_CUST_TEL		= replaceStr(object, k, "TRANS_CUST_TEL"); break;
						case "TRANS_REQ_DT"			: TRANS_REQ_DT			= replaceStr(object, k, "TRANS_REQ_DT"); break;
						case "ORD_QTY"				: ORD_QTY				= replaceStr(object, k, "ORD_QTY"); break;
						case "UOM_CD"				: UOM_CD				= replaceStr(object, k, "UOM_CD"); break;
						case "SDEPT_CD"				: SDEPT_CD				= replaceStr(object, k, "SDEPT_CD"); break;
						case "SALE_PER_CD"			: SALE_PER_CD			= replaceStr(object, k, "SALE_PER_CD"); break;
						case "CAR_CD"				: CAR_CD				= replaceStr(object, k, "CAR_CD"); break;
						case "DRV_NM"				: DRV_NM				= replaceStr(object, k, "DRV_NM"); break;
						case "DLV_SEQ"				: DLV_SEQ				= replaceStr(object, k, "DLV_SEQ"); break;
						case "DRV_TEL"				: DRV_TEL				= replaceStr(object, k, "DRV_TEL"); break;
						case "CUST_LOT_NO"			: CUST_LOT_NO			= replaceStr(object, k, "CUST_LOT_NO"); break;
						case "BL_NO"				: BL_NO					= replaceStr(object, k, "BL_NO"); break;
						case "REC_DT"				: REC_DT				= replaceStr(object, k, "REC_DT"); break;
						case "OUT_WH_CD"			: OUT_WH_CD				= replaceStr(object, k, "OUT_WH_CD"); break;
						case "IN_WH_CD"				: IN_WH_CD				= replaceStr(object, k, "IN_WH_CD"); break;
						case "MAKE_DT"				: MAKE_DT				= replaceStr(object, k, "MAKE_DT"); break;
						case "TIME_PERIOD_DAY"		: TIME_PERIOD_DAY		= replaceStr(object, k, "TIME_PERIOD_DAY"); break;
						case "WORK_YN"				: WORK_YN				= replaceStr(object, k, "WORK_YN"); break;
						case "RJ_TYPE"				: RJ_TYPE				= replaceStr(object, k, "RJ_TYPE"); break;
						case "LOC_YN"				: LOC_YN				= replaceStr(object, k, "LOC_YN"); break;
						case "CONF_YN"				: CONF_YN				= replaceStr(object, k, "CONF_YN"); break;
						case "EA_CAPA"				: EA_CAPA				= replaceStr(object, k, "EA_CAPA"); break;
						case "IN_ORD_WEIGHT"		: IN_ORD_WEIGHT			= replaceStr(object, k, "IN_ORD_WEIGHT"); break;
						case "ITEM_CD"				: ITEM_CD				= replaceStr(object, k, "ITEM_CD"); break;
						case "ITEM_NM"				: ITEM_NM				= replaceStr(object, k, "ITEM_NM"); break;
						case "TRANS_CUST_NM"		: TRANS_CUST_NM			= replaceStr(object, k, "TRANS_CUST_NM"); break;
						case "TRANS_CUST_ADDR"		: TRANS_CUST_ADDR		= replaceStr(object, k, "TRANS_CUST_ADDR"); break;
						case "TRANS_EMP_NM"			: TRANS_EMP_NM			= replaceStr(object, k, "TRANS_EMP_NM"); break;
						case "REMARK"				: REMARK				= replaceStr(object, k, "REMARK"); break;
						case "TRANS_ZIP_NO"			: TRANS_ZIP_NO			= replaceStr(object, k, "TRANS_ZIP_NO"); break;
						case "ETC2"					: ETC2					= replaceStr(object, k, "ETC2"); break;
						case "UNIT_AMT"				: UNIT_AMT				= replaceStr(object, k, "UNIT_AMT"); break;
						case "TRANS_BIZ_NO"			: TRANS_BIZ_NO			= replaceStr(object, k, "TRANS_BIZ_NO"); break;
						case "IN_CUST_ADDR"			: IN_CUST_ADDR			= replaceStr(object, k, "IN_CUST_ADDR"); break;
						case "IN_CUST_CD"			: IN_CUST_CD			= replaceStr(object, k, "IN_CUST_CD"); break;
						case "IN_CUST_NM"			: IN_CUST_NM			= replaceStr(object, k, "IN_CUST_NM"); break;
						case "IN_CUST_TEL"			: IN_CUST_TEL			= replaceStr(object, k, "IN_CUST_TEL"); break;
						case "IN_CUST_EMP_NM"		: IN_CUST_EMP_NM		= replaceStr(object, k, "IN_CUST_EMP_NM"); break;
						case "EXPIRY_DATE"			: EXPIRY_DATE			= replaceStr(object, k, "EXPIRY_DATE"); break;
						case "SALES_CUST_NM"		: SALES_CUST_NM			= replaceStr(object, k, "SALES_CUST_NM"); break;
						case "ZIP"					: ZIP					= replaceStr(object, k, "ZIP"); break;
						case "ADDR"					: ADDR					= replaceStr(object, k, "ADDR"); break;
						case "PHONE_1"				: PHONE_1				= replaceStr(object, k, "PHONE_1"); break;
						case "ETC1"					: ETC1					= replaceStr(object, k, "ETC1"); break;
						case "UNIT_NO"				: UNIT_NO				= replaceStr(object, k, "UNIT_NO"); break;
						case "SALES_COMPANY_NM"		: SALES_COMPANY_NM		= replaceStr(object, k, "SALES_COMPANY_NM"); break;
						case "TIME_DATE"			: TIME_DATE				= replaceStr(object, k, "TIME_DATE"); break;
						case "TIME_DATE_END"		: TIME_DATE_END			= replaceStr(object, k, "TIME_DATE_END"); break;
						case "TIME_USE_END"			: TIME_USE_END			= replaceStr(object, k, "TIME_USE_END"); break;
						case "LOC_CD"				: LOC_CD				= replaceStr(object, k, "LOC_CD"); break;	
						case "EP_TYPE"				: EP_TYPE				= replaceStr(object, k, "EP_TYPE"); break;	
						
						case "LOT_TYPE"				: LOT_TYPE				= replaceStr(object, k, "LOT_TYPE"); break;	
						case "OWNER_CD"				: OWNER_CD				= replaceStr(object, k, "OWNER_CD"); break;
						
						case "CUST_LEGACY_ITEM_CD"	: CUST_LEGACY_ITEM_CD	= replaceStr(object, k, "CUST_LEGACY_ITEM_CD"); break;
						case "ITEM_BARCODE"			: ITEM_BARCODE			= replaceStr(object, k, "ITEM_BARCODE"); break;
						case "ITEM_SIZE"			: ITEM_SIZE				= replaceStr(object, k, "ITEM_SIZE"); break;
						case "COLOR"				: COLOR					= replaceStr(object, k, "COLOR"); break;
						case "CUST_CD"				: CUST_CD				= replaceStr(object, k, "CUST_CD"); break;
						}
					}
					
					String NO = Integer.toString(i+1);
					no[i]               = NO;
					
					outReqDt[i]         = OUT_REQ_DT.replaceAll("[^\\d]", "");    
					inReqDt[i]          = IN_REQ_DT;    
					custOrdNo[i]        = CUST_ORD_NO;    
					custOrdSeq[i]       = CUST_ORD_SEQ;    
					trustCustCd[i]      = TRUST_CUST_CD;    
					
					transCustCd[i]      = TRANS_CUST_CD;    
					transCustTel[i]     = TRANS_CUST_TEL;    
					transReqDt[i]       = TRANS_REQ_DT;    
					custCd[i]           = CUST_CD;    
					ordQty[i]           = ORD_QTY;    
					
					uomCd[i]            = UOM_CD;    
					sdeptCd[i]          = SDEPT_CD;    
					salePerCd[i]        = SALE_PER_CD;    
					carCd[i]            = CAR_CD;
					drvNm[i]            = DRV_NM;
					
					dlvSeq[i]           = DLV_SEQ;    
					drvTel[i]           = DRV_TEL;    
					custLotNo[i]        = CUST_LOT_NO;    
					blNo[i]             = BL_NO;    
					recDt[i]            = REC_DT;    
					
					outWhCd[i]          = OUT_WH_CD;
					inWhCd[i]           = IN_WH_CD;
					makeDt[i]           = MAKE_DT;
					timePeriodDay[i]    = TIME_PERIOD_DAY;
					workYn[i]           = WORK_YN;
					
					rjType[i]           = RJ_TYPE;    
					locYn[i]            = LOC_YN;    
					confYn[i]           = CONF_YN;    
					eaCapa[i]           = EA_CAPA;    
					inOrdWeight[i]      = IN_ORD_WEIGHT;   
					
					itemCd[i]           = ITEM_CD;    
					itemNm[i]           = ITEM_NM;    
					transCustNm[i]      = TRANS_CUST_NM;    
					transCustAddr[i]    = TRANS_CUST_ADDR;    
					transEmpNm[i]       = TRANS_EMP_NM;    
					
					remark[i]           = REMARK;    
					transZipNo[i]       = TRANS_ZIP_NO;    
					etc2[i]             = ETC2;    
					unitAmt[i]          = UNIT_AMT.replaceAll("[^\\d]", "");    
					transBizNo[i]       = TRANS_BIZ_NO;    
					
					inCustAddr[i]       = IN_CUST_ADDR;   
					inCustCd[i]         = IN_CUST_CD;    
					inCustNm[i]         = IN_CUST_NM;    
					inCustTel[i]        = IN_CUST_TEL;    
					inCustEmpNm[i]      = IN_CUST_EMP_NM;    
					
					expiryDate[i]       = EXPIRY_DATE;
					salesCustNm[i]      = SALES_CUST_NM;
					zip[i]       		= ZIP;
					addr[i]       		= ADDR;
					phone1[i]       	= PHONE_1;
					
					etc1[i]      		= ETC1;
					unitNo[i]      		= UNIT_NO;
					salesCompanyNm[i]   = SALES_COMPANY_NM;
					timeDate[i]         = TIME_DATE;      
					timeDateEnd[i]      = TIME_DATE_END;     
					
					timeUseEnd[i]       = TIME_USE_END;
					locCd[i]       		= LOC_CD;
					epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
					
					lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
					ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가
					
					custLegacyItemCd[i]	= CUST_LEGACY_ITEM_CD;	//화주 상품코드 신규컬럼 추가
					itemBarcode[i]      = ITEM_BARCODE;		//상품 바코드 신규컬럼 추가
					itemSize[i]       	= ITEM_SIZE;			//상품 사이즈 신규컬럼 추가
					color[i]       		= COLOR;				//상품 색상 신규컬럼 추가
					
					// 비즈컨설팅 반품입고(온라인, 매장) 상품명 생성(화주상품코드 + 컬러 + 사이즈), UOM : EA, QTY : 1
					// 158 : 온라인반품, 159 : 매장반품
					// 상품코드 = 상품바코드 생성
					if(model.get("SS_SVC_NO").equals("0000003620")){
						if(model.get("vrOrdType").equals("I")){
							StringBuilder builder = new StringBuilder();
							itemBarcode[i] = itemCd[i];
							
							if(model.get("vrOrdSubType").equals("158")){
								itemNm[i]  		  = builder.append(CUST_LEGACY_ITEM_CD).append(" ").append(COLOR).append(" ").append(ITEM_SIZE).toString();
								ordQty[i]           = "1";    
								uomCd[i]            = "EA";    
							}else if(model.get("vrOrdSubType").equals("159")){
								itemNm[i]  		  = builder.append(CUST_LEGACY_ITEM_CD).append(" ").append(COLOR).append(" ").append(ITEM_SIZE).toString();
								uomCd[i]            = "EA";    
							}else if(model.get("vrOrdSubType").equals("20")){
								itemNm[i]  		  = builder.append(CUST_LEGACY_ITEM_CD).append(" ").append(COLOR).append(" ").append(ITEM_SIZE).toString();
								uomCd[i]            = "EA";    
								ownerCd[i]          = inCustCd[i]; 
							}
						}else if(model.get("vrOrdType").equals("O")){
							uomCd[i]            = "EA";    
						}
						
					}
				}
				
				//프로시져에 보낼것들 다담는다
				Map<String, Object> modelIns = new HashMap<String, Object>();
				
				modelIns.put("no"  , no);
				if(model.get("vrOrdType").equals("I")){
					modelIns.put("reqDt"     	, inReqDt);
					modelIns.put("whCd"      	, inWhCd);
				}else{
					modelIns.put("reqDt"     	, outReqDt);
					modelIns.put("whCd"      	, outWhCd);
				}
				
				modelIns.put("custOrdNo"    	, custOrdNo);
				modelIns.put("custOrdSeq"   	, custOrdSeq);
				modelIns.put("trustCustCd"  	, trustCustCd); //5
				
				modelIns.put("transCustCd"  	, transCustCd);
				modelIns.put("transCustTel" 	, transCustTel);
				modelIns.put("transReqDt"   	, transReqDt);
				modelIns.put("custCd"       	, custCd);
				modelIns.put("ordQty"       	, ordQty);      //10
				
				modelIns.put("uomCd"        	, uomCd);
				modelIns.put("sdeptCd"      	, sdeptCd);
				modelIns.put("salePerCd"    	, salePerCd);
				modelIns.put("carCd"        	, carCd);
				modelIns.put("drvNm"        	, drvNm);       //15
				
				modelIns.put("dlvSeq"       	, dlvSeq);
				modelIns.put("drvTel"       	, drvTel);
				modelIns.put("custLotNo"    	, custLotNo);
				modelIns.put("blNo"         	, blNo);
				modelIns.put("recDt"        	, recDt);       //20
				
				modelIns.put("makeDt"       	, makeDt);
				modelIns.put("timePeriodDay"	, timePeriodDay);
				modelIns.put("workYn"       	, workYn);                
				modelIns.put("rjType"       	, rjType);
				modelIns.put("locYn"        	, locYn);       //25
				
				modelIns.put("confYn"       	, confYn);     
				modelIns.put("eaCapa"       	, eaCapa);
				modelIns.put("inOrdWeight"  	, inOrdWeight); //28
				modelIns.put("itemCd"           , itemCd);
				modelIns.put("itemNm"           , itemNm);
				
				modelIns.put("transCustNm"      , transCustNm);
				modelIns.put("transCustAddr"    , transCustAddr);
				modelIns.put("transEmpNm"       , transEmpNm);
				modelIns.put("remark"           , remark);
				modelIns.put("transZipNo"       , transZipNo);
				
				modelIns.put("etc2"             , etc2);
				modelIns.put("unitAmt"          , unitAmt);
				modelIns.put("transBizNo"       , transBizNo);
				modelIns.put("inCustAddr"       , inCustAddr);
				modelIns.put("inCustCd"         , inCustCd);
				
				modelIns.put("inCustNm"         , inCustNm);                 
				modelIns.put("inCustTel"        , inCustTel);
				modelIns.put("inCustEmpNm"      , inCustEmpNm);      
				modelIns.put("expiryDate"       , expiryDate);
				modelIns.put("salesCustNm"      , salesCustNm);
				
				modelIns.put("zip"       		, zip);
				modelIns.put("addr"       		, addr);
				modelIns.put("phone1"       	, phone1);
				modelIns.put("etc1"     	 	, etc1);
				modelIns.put("unitNo"     	 	, unitNo);
				
				modelIns.put("salesCompanyNm"    	, salesCompanyNm);
				
				modelIns.put("time_date"        , timeDate);
				modelIns.put("time_date_end"    , timeDateEnd);                
				modelIns.put("time_use_end"     , timeUseEnd);  
				modelIns.put("locCd"     		, locCd);  
				modelIns.put("epType"     		, epType);  
				
				modelIns.put("lotType"     		, lotType);  
				modelIns.put("ownerCd"     		, ownerCd);
				
				modelIns.put("custLegacyItemCd" , custLegacyItemCd);  
				modelIns.put("itemBarcode"     	, itemBarcode);  
				modelIns.put("itemSize"     	, itemSize);  
				modelIns.put("color"     		, color);  
				
				modelIns.put("vrOrdType"		, model.get("vrOrdType"));
				modelIns.put("vrOrdSubType"		, model.get("vrOrdSubType"));
				
				modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
				modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
				modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
				
				//dao                
				modelIns = (Map<String, Object>)dao.saveExcelOrder_AS(modelIns);
				ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
				m.put("O_CUR", modelIns.get("O_CUR"));                
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    /**
     * 
     * 대체 Method ID   	: saveExcelOrderJavaAllowBlank_AS
     * 대체 Method 설명     : 템플릿 주문 저장 - 공란허용 로직 + 주문유형
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaAllowBlank_AS(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
    	List list = (ArrayList)model.get("LIST");
    	
    	// 비즈컨설팅 B2B 거래처 정렬
    	if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("O")){  
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("TRANS_CUST_CD");
    				String name2 = (String) o2.get("TRANS_CUST_CD");
    				return name1.compareTo(name2);
    			}
    		});
    	} 
    	// 비즈컨설팅 신상입고
    	else if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("I") && model.get("vrOrdSubType").equals("20")){
    		
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("ITEM_CD");
    				String name2 = (String) o2.get("ITEM_CD");
    				return name1.compareTo(name2);
    			}
    		});
    		
    		HashMap keyMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
    			// 입고처와 LOT NO가 없는 상품들만 수량 합침
    			if((loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals("")) ||(!loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals(""))){
    				
    				String itemCd_Etc2 = String.valueOf(loopMap.get("ITEM_CD"));
    				
    				if(keyMap.containsKey(itemCd_Etc2)){
        				String n1 = (String) keyMap.get(itemCd_Etc2);
        				String n2 = (String) loopMap.get("ORD_QTY");
        				keyMap.put(itemCd_Etc2, String.valueOf(Integer.parseInt(n1) + Integer.parseInt(n2)));
        			}else{
        				keyMap.put(itemCd_Etc2, loopMap.get("ORD_QTY"));
        			}
    			}
    		}
    		
    		List rstList = new ArrayList();
    		HashMap rstMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
    			if((loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals("")) ||(!loopMap.get("CUST_LOT_NO").equals("") && loopMap.get("IN_CUST_CD").equals(""))){
    				
    				String itemCd_Etc2 = String.valueOf(loopMap.get("ITEM_CD"));
    				
    				if(!rstMap.containsKey(itemCd_Etc2)){
        				loopMap.put("ORD_QTY", keyMap.get(itemCd_Etc2));
        				rstMap.put((String) itemCd_Etc2, keyMap.get(loopMap.get("ITEM_CD")));
        				rstList.add(loopMap);
        			}
    			}else{
    				rstList.add(loopMap);
    			}
    		}
    		
    		list = rstList;
    	}
    	// 비즈컨설팅 매장반품입고 로직(임시 제외, 다시 사용할 가능성 있음)
		/*else if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("I") && model.get("vrOrdSubType").equals("159")){
    		
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("ITEM_CD");
    				String name2 = (String) o2.get("ITEM_CD");
    				return name1.compareTo(name2);
    			}
    		});
    		
    		HashMap keyMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
				if(keyMap.containsKey(loopMap.get("ITEM_CD"))){
    				String n1 = (String) keyMap.get(loopMap.get("ITEM_CD"));
    				String n2 = (String) loopMap.get("ORD_QTY");
    				keyMap.put(loopMap.get("ITEM_CD"), String.valueOf(Integer.parseInt(n1) + Integer.parseInt(n2)));
    			}else{
    				keyMap.put(loopMap.get("ITEM_CD"), loopMap.get("ORD_QTY"));
    			}
    		}
    		
    		List rstList = new ArrayList();
    		HashMap rstMap = new HashMap<>();
    		for(int i = 0; i < list.size(); ++i){
    			HashMap<String, Object> loopMap = (HashMap<String, Object>) list.get(i);
				if(!rstMap.containsKey(loopMap.get("ITEM_CD"))){
    				loopMap.put("ORD_QTY", keyMap.get(loopMap.get("ITEM_CD")));
    				rstMap.put((String) loopMap.get("ITEM_CD"), keyMap.get(loopMap.get("ITEM_CD")));
    				rstList.add(loopMap);
    			}
    		}
    		
    		list = rstList;
    	}*/
    	
		List<Map<String, Object>> listBody = list;
		String custId = String.valueOf(model.get("vrCustCd"));
		
		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];     
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] salesCompanyNm	 	= new String[listBodyCnt];
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                String[] locCd     		= new String[listBodyCnt];   //로케이션코드
                
                String[] epType     	= new String[listBodyCnt];   //납품유형
                
                
//                String[] ordInsDt     	= new String[listBodyCnt];   //주문등록일	2022-03-03
                String[] lotType     	= new String[listBodyCnt];   //LOT속성 	2022-03-04
                String[] ownerCd     	= new String[listBodyCnt];   //소유자		2022-03-04
                
                
                String[] custLegacyItemCd = new String[listBodyCnt]; //화주 상품코드	2022-05-24
                String[] itemBarcode    = new String[listBodyCnt];   //상품 바코드		2022-05-24
                String[] itemSize     	= new String[listBodyCnt];   //상품 사이즈		2022-05-24
                String[] color     		= new String[listBodyCnt];   //상품 색상		2022-05-24
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT      = "";
        			String IN_REQ_DT       = "";
        			String CUST_ORD_NO     = "";
        			String CUST_ORD_SEQ    = "";
        			String TRUST_CUST_CD   = "";
        			String TRANS_CUST_CD   = "";
        			String TRANS_CUST_TEL  = "";
        			String TRANS_REQ_DT    = "";
        			String CUST_CD         = "";
        			String ORD_QTY         = "";
        			String UOM_CD          = "";
        			String SDEPT_CD        = "";
        			String SALE_PER_CD     = "";
        			String CAR_CD          = "";
        			String DRV_NM          = "";
        			String DLV_SEQ         = "";
        			String DRV_TEL         = "";
        			String CUST_LOT_NO     = "";
        			String BL_NO           = "";
        			String REC_DT          = "";
        			String OUT_WH_CD       = "";
        			String IN_WH_CD        = "";
        			String MAKE_DT         = "";
        			String TIME_PERIOD_DAY = "";
        			String WORK_YN         = "";
        			String RJ_TYPE         = "";
        			String LOC_YN          = "";
        			String CONF_YN         = "";
        			String EA_CAPA         = "";
        			String IN_ORD_WEIGHT   = "";
        			String ITEM_CD         = "";
        			String ITEM_NM         = "";
        			String TRANS_CUST_NM   = "";
        			String TRANS_CUST_ADDR = "";
        			String TRANS_EMP_NM    = "";
        			String REMARK          = "";
        			String TRANS_ZIP_NO    = "";
        			String ETC2            = "";
        			String UNIT_AMT        = "";
        			String TRANS_BIZ_NO    = "";
        			String IN_CUST_ADDR    = "";
        			String IN_CUST_CD      = "";
        			String IN_CUST_NM      = "";
        			String IN_CUST_TEL     = "";
        			String IN_CUST_EMP_NM  = "";
        			String EXPIRY_DATE     = "";
        			String SALES_CUST_NM   = "";
        			String ZIP             = "";
        			String ADDR            = "";
        			String PHONE_1         = "";
        			String ETC1            = "";
        			String UNIT_NO         = "";
        			String SALES_COMPANY_NM   = "";
        			String TIME_DATE       = "";
        			String TIME_DATE_END   = "";
        			String TIME_USE_END    = "";
        			String LOC_CD    	   = "";
        			String EP_TYPE    	   = "";
        			
//        			String ORD_INS_DT         = ""; // 템플릿 추가 sing09  2022-03-03
        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

        			String CUST_LEGACY_ITEM_CD = "";
        			String ITEM_BARCODE		   = "";
        			String ITEM_SIZE		   = "";
        			String COLOR			   = "";
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				Map<String, Object> object = listBody.get(i);
        				String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
        				switch (bindColNm) {
						case "OUT_REQ_DT" 			: OUT_REQ_DT        	= replaceStr(object, k, "OUT_REQ_DT"); break;
						case "IN_REQ_DT"			: IN_REQ_DT				= replaceStr(object, k, "IN_REQ_DT"); break;
						case "CUST_ORD_NO"			: CUST_ORD_NO			= replaceStr(object, k, "CUST_ORD_NO"); break;
						case "CUST_ORD_SEQ"			: CUST_ORD_SEQ			= replaceStr(object, k, "CUST_ORD_SEQ"); break;
						case "TRUST_CUST_CD"		: TRUST_CUST_CD			= replaceStr(object, k, "TRUST_CUST_CD"); break;
						case "TRANS_CUST_CD"		: TRANS_CUST_CD			= replaceStr(object, k, "TRANS_CUST_CD"); break;
						case "TRANS_CUST_TEL"		: TRANS_CUST_TEL		= replaceStr(object, k, "TRANS_CUST_TEL"); break;
						case "TRANS_REQ_DT"			: TRANS_REQ_DT			= replaceStr(object, k, "TRANS_REQ_DT"); break;
						case "ORD_QTY"				: ORD_QTY				= replaceStr(object, k, "ORD_QTY"); break;
						case "UOM_CD"				: UOM_CD				= replaceStr(object, k, "UOM_CD"); break;
						case "SDEPT_CD"				: SDEPT_CD				= replaceStr(object, k, "SDEPT_CD"); break;
						case "SALE_PER_CD"			: SALE_PER_CD			= replaceStr(object, k, "SALE_PER_CD"); break;
						case "CAR_CD"				: CAR_CD				= replaceStr(object, k, "CAR_CD"); break;
						case "DRV_NM"				: DRV_NM				= replaceStr(object, k, "DRV_NM"); break;
						case "DLV_SEQ"				: DLV_SEQ				= replaceStr(object, k, "DLV_SEQ"); break;
						case "DRV_TEL"				: DRV_TEL				= replaceStr(object, k, "DRV_TEL"); break;
						case "CUST_LOT_NO"			: CUST_LOT_NO			= replaceStr(object, k, "CUST_LOT_NO"); break;
						case "BL_NO"				: BL_NO					= replaceStr(object, k, "BL_NO"); break;
						case "REC_DT"				: REC_DT				= replaceStr(object, k, "REC_DT"); break;
						case "OUT_WH_CD"			: OUT_WH_CD				= replaceStr(object, k, "OUT_WH_CD"); break;
						case "IN_WH_CD"				: IN_WH_CD				= replaceStr(object, k, "IN_WH_CD"); break;
						case "MAKE_DT"				: MAKE_DT				= replaceStr(object, k, "MAKE_DT"); break;
						case "TIME_PERIOD_DAY"		: TIME_PERIOD_DAY		= replaceStr(object, k, "TIME_PERIOD_DAY"); break;
						case "WORK_YN"				: WORK_YN				= replaceStr(object, k, "WORK_YN"); break;
						case "RJ_TYPE"				: RJ_TYPE				= replaceStr(object, k, "RJ_TYPE"); break;
						case "LOC_YN"				: LOC_YN				= replaceStr(object, k, "LOC_YN"); break;
						case "CONF_YN"				: CONF_YN				= replaceStr(object, k, "CONF_YN"); break;
						case "EA_CAPA"				: EA_CAPA				= replaceStr(object, k, "EA_CAPA"); break;
						case "IN_ORD_WEIGHT"		: IN_ORD_WEIGHT			= replaceStr(object, k, "IN_ORD_WEIGHT"); break;
						case "ITEM_CD"				: ITEM_CD				= replaceStr(object, k, "ITEM_CD"); break;
						case "ITEM_NM"				: ITEM_NM				= replaceStr(object, k, "ITEM_NM"); break;
						case "TRANS_CUST_NM"		: TRANS_CUST_NM			= replaceStr(object, k, "TRANS_CUST_NM"); break;
						case "TRANS_CUST_ADDR"		: TRANS_CUST_ADDR		= replaceStr(object, k, "TRANS_CUST_ADDR"); break;
						case "TRANS_EMP_NM"			: TRANS_EMP_NM			= replaceStr(object, k, "TRANS_EMP_NM"); break;
						case "REMARK"				: REMARK				= replaceStr(object, k, "REMARK"); break;
						case "TRANS_ZIP_NO"			: TRANS_ZIP_NO			= replaceStr(object, k, "TRANS_ZIP_NO"); break;
						case "ETC2"					: ETC2					= replaceStr(object, k, "ETC2"); break;
						case "UNIT_AMT"				: UNIT_AMT				= replaceStr(object, k, "UNIT_AMT"); break;
						case "TRANS_BIZ_NO"			: TRANS_BIZ_NO			= replaceStr(object, k, "TRANS_BIZ_NO"); break;
						case "IN_CUST_ADDR"			: IN_CUST_ADDR			= replaceStr(object, k, "IN_CUST_ADDR"); break;
						case "IN_CUST_CD"			: IN_CUST_CD			= replaceStr(object, k, "IN_CUST_CD"); break;
						case "IN_CUST_NM"			: IN_CUST_NM			= replaceStr(object, k, "IN_CUST_NM"); break;
						case "IN_CUST_TEL"			: IN_CUST_TEL			= replaceStr(object, k, "IN_CUST_TEL"); break;
						case "IN_CUST_EMP_NM"		: IN_CUST_EMP_NM		= replaceStr(object, k, "IN_CUST_EMP_NM"); break;
						case "EXPIRY_DATE"			: EXPIRY_DATE			= replaceStr(object, k, "EXPIRY_DATE"); break;
						case "SALES_CUST_NM"		: SALES_CUST_NM			= replaceStr(object, k, "SALES_CUST_NM"); break;
						case "ZIP"					: ZIP					= replaceStr(object, k, "ZIP"); break;
						case "ADDR"					: ADDR					= replaceStr(object, k, "ADDR"); break;
						case "PHONE_1"				: PHONE_1				= replaceStr(object, k, "PHONE_1"); break;
						case "ETC1"					: ETC1					= replaceStr(object, k, "ETC1"); break;
						case "UNIT_NO"				: UNIT_NO				= replaceStr(object, k, "UNIT_NO"); break;
						case "SALES_COMPANY_NM"		: SALES_COMPANY_NM		= replaceStr(object, k, "SALES_COMPANY_NM"); break;
						case "TIME_DATE"			: TIME_DATE				= replaceStr(object, k, "TIME_DATE"); break;
						case "TIME_DATE_END"		: TIME_DATE_END			= replaceStr(object, k, "TIME_DATE_END"); break;
						case "TIME_USE_END"			: TIME_USE_END			= replaceStr(object, k, "TIME_USE_END"); break;
						case "LOC_CD"				: LOC_CD				= replaceStr(object, k, "LOC_CD"); break;	
						case "EP_TYPE"				: EP_TYPE				= replaceStr(object, k, "EP_TYPE"); break;	
						
//						case "ORD_INS_DT"			: ORD_INS_DT			= replaceStr(object, k, "ORD_INS_DT"); break;	
						case "LOT_TYPE"				: LOT_TYPE				= replaceStr(object, k, "LOT_TYPE"); break;	
						case "OWNER_CD"				: OWNER_CD				= replaceStr(object, k, "OWNER_CD"); break;
						
						case "CUST_LEGACY_ITEM_CD"	: CUST_LEGACY_ITEM_CD	= replaceStr(object, k, "CUST_LEGACY_ITEM_CD"); break;
						case "ITEM_BARCODE"			: ITEM_BARCODE			= replaceStr(object, k, "ITEM_BARCODE"); break;
						case "ITEM_SIZE"			: ITEM_SIZE				= replaceStr(object, k, "ITEM_SIZE"); break;
						case "COLOR"				: COLOR					= replaceStr(object, k, "COLOR"); break;

						default:
							CUST_CD = custId;
						}
        			}
     				
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT.replaceAll("[^\\d]", "");    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;    
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT.replaceAll("[^\\d]", "");    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;     
                    
                    timeUseEnd[i]       = TIME_USE_END;
                    locCd[i]       		= LOC_CD;
                    epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
                    
//                    ordInsDt[i]       	= ORD_INS_DT;	//주문등록일 신규컬럼 추가
                    lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
                    ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가
                    
                    custLegacyItemCd[i]	= CUST_LEGACY_ITEM_CD;	//화주 상품코드 신규컬럼 추가
                    itemBarcode[i]       	= ITEM_BARCODE;		//상품 바코드 신규컬럼 추가
                    itemSize[i]       	= ITEM_SIZE;			//상품 사이즈 신규컬럼 추가
                    color[i]       		= COLOR;				//상품 색상 신규컬럼 추가
                    
                	
                    // 비즈컨설팅 반품입고(온라인, 매장) 상품명 생성(화주상품코드 + 컬러 + 사이즈), UOM : EA, QTY : 1
                    // 158 : 온라인반품, 159 : 매장반품
                    // 상품코드 = 상품바코드 생성
                	if(model.get("SS_SVC_NO").equals("0000003620")){
                		if(model.get("vrOrdType").equals("I")){
                			StringBuilder builder = new StringBuilder();
                			itemBarcode[i] = itemCd[i];
                			
                			if(model.get("vrOrdSubType").equals("158")){
                      		  	itemNm[i]  		  = builder.append(CUST_LEGACY_ITEM_CD).append(" ").append(COLOR).append(" ").append(ITEM_SIZE).toString();
                      		  	ordQty[i]           = "1";    
                                uomCd[i]            = "EA";    
	                  		}else if(model.get("vrOrdSubType").equals("159")){
	                  			itemNm[i]  		  = builder.append(CUST_LEGACY_ITEM_CD).append(" ").append(COLOR).append(" ").append(ITEM_SIZE).toString();
	                            uomCd[i]            = "EA";    
	                  		}else if(model.get("vrOrdSubType").equals("20")){
	                  			itemNm[i]  		  = builder.append(CUST_LEGACY_ITEM_CD).append(" ").append(COLOR).append(" ").append(ITEM_SIZE).toString();
	      	              	    uomCd[i]            = "EA";    
	      	              	    ownerCd[i]          = inCustCd[i]; 
	                  		}
                		}else if(model.get("vrOrdType").equals("O")){
                			 uomCd[i]            = "EA";    
                		}
                		
                	}
                }
                
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
               
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
               
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);      
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("phone1"       	, phone1);
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                
                modelIns.put("salesCompanyNm"    	, salesCompanyNm);
                
                modelIns.put("time_date"        , timeDate);
                modelIns.put("time_date_end"    , timeDateEnd);                
                modelIns.put("time_use_end"     , timeUseEnd);  
                modelIns.put("locCd"     		, locCd);  
                modelIns.put("epType"     		, epType);  
                
//                modelIns.put("ordInsDt"     	, ordInsDt);  
                modelIns.put("lotType"     		, lotType);  
                modelIns.put("ownerCd"     		, ownerCd);
                
                modelIns.put("custLegacyItemCd" , custLegacyItemCd);  
                modelIns.put("itemBarcode"     	, itemBarcode);  
                modelIns.put("itemSize"     	, itemSize);  
                modelIns.put("color"     		, color);  
                
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                modelIns.put("vrOrdSubType"		, model.get("vrOrdSubType"));
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

                //dao                
                modelIns = (Map<String, Object>)dao.saveExcelOrder_AS(modelIns);
                
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                m.put("O_CUR", modelIns.get("O_CUR"));                
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * 대체 Method ID   	: combineOrdDegree_B2B
     * 대체 Method 설명     : 주문차수 합치기 B2B
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> combineOrdDegree_B2B(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                // List<String> ordIdList = new ArrayList();
                HashSet<String> ordIdList = new HashSet();
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordIdList.add((String)model.get("ORD_ID"+i));
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                // ordId
                modelIns.put("ordIdList", new ArrayList<>(ordIdList));
                modelIns.put("ordDegree", dao.getNewOrdDegree(model));
                modelIns.put("ordDegreeNm", (String)  model.get("vrOrdDegreeNm"));

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao                
                modelIns = (Map<String, Object>)dao.combineOrdDegree_OP(modelIns);
                // ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
     * 
     * 대체 Method ID   	: combineOrdDegree_B2C
     * 대체 Method 설명     : 주문차수 합치기 B2C
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> combineOrdDegree_B2C(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                //List<String> ordIdList = new ArrayList();
                HashSet<String> ordIdList = new HashSet();
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordIdList.add((String)model.get("ORD_ID"+i));
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                // ordId
                modelIns.put("ordIdList", new ArrayList<>(ordIdList));
                modelIns.put("ordDegree", dao.getNewOrdDegree(model));
                modelIns.put("ordDegreeNm", (String)  model.get("vrOrdDegreeNm"));

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao
                modelIns = (Map<String, Object>)dao.combineOrdDegree_OP(modelIns);
                modelIns = (Map<String, Object>)dao.combineOrdDegree_OM(modelIns);
                //ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
		 
	 /**
     * 
     * 대체 Method ID   	: decomposeOrdDegree_B2B
     * 대체 Method 설명     : 주문차수 분해 B2B
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> decomposeOrdDegree_B2B(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                // List<String> ordIdList = new ArrayList();
                HashSet<String> ordIdList = new HashSet();
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordIdList.add((String)model.get("ORD_ID"+i));
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                // ordId
                modelIns.put("ordIdList", new ArrayList<>(ordIdList));
                modelIns.put("ordDegree", dao.getNewOrdDegree(model));
                modelIns.put("ordDegreeNm", (String)  model.get("vrOrdDegreeNm"));

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao                
                modelIns = (Map<String, Object>)dao.decomposeOrdDegree_OP(modelIns);
                // ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
		 
	 /**
     * 
     * 대체 Method ID   	: decomposeOrdDegree_B2C
     * 대체 Method 설명     : 주문차수 분해 B2C
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> decomposeOrdDegree_B2C(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                //List<String> ordIdList = new ArrayList();
                HashSet<String> ordIdList = new HashSet();
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordIdList.add((String)model.get("ORD_ID"+i));
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                // ordId
                modelIns.put("ordIdList", new ArrayList<>(ordIdList));
                modelIns.put("ordDegree", dao.getNewOrdDegree(model));
                modelIns.put("ordDegreeNm", (String)  model.get("vrOrdDegreeNm"));

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao
                modelIns = (Map<String, Object>)dao.decomposeOrdDegree_OP(modelIns);
                modelIns = (Map<String, Object>)dao.decomposeOrdDegree_OM(modelIns);
                //ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 

	/**
     * 
     * 대체 Method ID     : saveExcelOrderJavaCommonB2C_TS
     * 대체 Method 설명   : B2C 템플릿 업로드 - 공통 (공란적용)
     * 작성자             : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaCommonB2C_TS(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	
    	List list = (ArrayList)model.get("LIST");
    	
    	// 비즈컨설팅 B2C 로직 (임시)
    	if(model.get("SS_SVC_NO").equals("0000003620") && model.get("vrOrdType").equals("O")){ 
    		// 수취인명으로 SORT
    		Collections.sort(list, new Comparator<HashMap<String, Object>>() {
    			@Override
    			public int compare(HashMap<String, Object> o1, HashMap<String, Object> o2) {
    				String name1 = (String) o1.get("SALES_CUST_NM");
    				String name2 = (String) o2.get("SALES_CUST_NM");
    				return name1.compareTo(name2);
    			}
    		}); 
    		
    		Map<String, Object> prevModel = (Map<String, Object>) list.get(0);
    		String pCustomerInfo = (String) prevModel.get("SALES_CUST_NM") + prevModel.get("ADDR");
    		
    		int cnt = 1;
    		SimpleDateFormat format1 = new SimpleDateFormat("yyMMddHHmmss");
    		String orgOrdId = format1.format (System.currentTimeMillis()) + "-"; 
    		
    		for(int i = 0; i < list.size(); ++i){
    			Map<String, Object> loopModel = (Map<String, Object>) list.get(i);
    			String lCustomerInfo = (String) loopModel.get("SALES_CUST_NM") + loopModel.get("ADDR");
    			
    			if(!pCustomerInfo.trim().equals(lCustomerInfo.trim())){
    				pCustomerInfo = (String) loopModel.get("SALES_CUST_NM") + loopModel.get("ADDR");
    				cnt += 1;
    			}
    			loopModel.put("CUST_ORD_NO", orgOrdId + cnt);
    		}
    	}
    	
		List<Map<String, Object>> listBody = list;
		String custId = String.valueOf(model.get("vrCustCd"));
		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	Date today = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String formatted_today = sdf.format(today);
    	
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] addr2     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];               
                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                String[] phone2			= new String[listBodyCnt];   //고객전화번호2
                String[] buyCustNm		= new String[listBodyCnt];   //주문자명
                String[] buyPhone1		= new String[listBodyCnt];   //주문자전화번호1
                String[] salesCompanyNm	= new String[listBodyCnt];   //salesCompanyNm
                String[] ordDegree		= new String[listBodyCnt];   //주문등록차수
                String[] bizCond		= new String[listBodyCnt];   //업태
                String[] bizType		= new String[listBodyCnt];   //업종
                String[] bizNo			= new String[listBodyCnt];   //사업자등록번호
                String[] custType		= new String[listBodyCnt];   //화주타입
                
                String[] dataSenderNm		= new String[listBodyCnt];   //쇼핑몰
                String[] legacyOrgOrdNo		= new String[listBodyCnt];   //사방넷주문번호
//                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
                
                String[] custSeq 			=  new String[listBodyCnt];   //템플릿구분
                String[] ordDesc 			=  new String[listBodyCnt];   //ord_desc
                String[] dlvMsg1 			=  new String[listBodyCnt];   //배송메세지1
                String[] dlvMsg2 			=  new String[listBodyCnt];   //배송메세지2
                
//                String[] lotType     		= new String[listBodyCnt];   //LOT속성 	2022-03-04
//                String[] ownerCd     		= new String[listBodyCnt];   //소유자	2022-03-04

                
                /* 컬럼 바인딩 제약조건 참조 flag */
                String breakYnOrdDegree = "N";
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT		= formatted_today;//출고 예정일 -> 업로드 날짜
        			String IN_REQ_DT		= "";
        			String CUST_ORD_NO		= "";
        			String CUST_ORD_SEQ		= "";
        			String TRUST_CUST_CD	= "";
        			String TRANS_CUST_CD	= "";
        			String TRANS_CUST_TEL	= "";
        			String TRANS_REQ_DT		= "";
        			String CUST_CD			= "";
        			String ORD_QTY			= "";
        			String UOM_CD			= "";
        			String SDEPT_CD			= "";
        			String SALE_PER_CD		= "";
        			String CAR_CD			= "";
        			String DRV_NM			= "";
        			String DLV_SEQ			= "";
        			String DRV_TEL			= "";
        			String CUST_LOT_NO		= "";
        			String BL_NO			= "";
        			String REC_DT			= "";
        			String OUT_WH_CD		= "";
        			String IN_WH_CD			= "";
        			String MAKE_DT			= "";
        			String TIME_PERIOD_DAY	= "";
        			String WORK_YN			= "";
        			String RJ_TYPE			= "";
        			String LOC_YN			= "";
        			String CONF_YN			= "";
        			String EA_CAPA			= "";
        			String IN_ORD_WEIGHT	= "";
        			String ITEM_CD			= "";
        			String ITEM_NM			= "";
        			String TRANS_CUST_NM	= "";
        			String TRANS_CUST_ADDR	= "";
        			String TRANS_EMP_NM		= "";
        			String REMARK			= "";
        			String TRANS_ZIP_NO		= "";
        			String ETC2				= "";
        			String UNIT_AMT			= "";
        			String TRANS_BIZ_NO		= "";
        			String IN_CUST_ADDR		= "";
        			String IN_CUST_CD		= "";
        			String IN_CUST_NM		= "";
        			String IN_CUST_TEL		= "";
        			String IN_CUST_EMP_NM	= "";
        			String EXPIRY_DATE		= "";
        			String SALES_CUST_NM	= "";
        			String ZIP				= "";
        			String ADDR				= "";
        			String ADDR2			= "";
        			String PHONE_1			= "";
        			String ETC1				= "";
        			String UNIT_NO			= "";
        			String TIME_DATE		= "";
        			String TIME_DATE_END	= "";
        			String TIME_USE_END		= "";
        			String PHONE_2			= "";
        			String BUY_CUST_NM		= "";
        			String BUY_PHONE_1		= "";
        			String SALES_COMPANY_NM		= "";
        			String ORD_DEGREE		= "";
        			String BIZ_COND		= "";
        			String BIZ_TYPE		= "";
        			String BIZ_NO		= "";
        			String CUST_TYPE		= "";
        			String DATA_SENDER_NM = "";
        			String LEGACY_ORG_ORD_NO ="";
//        			String INVOICE_NO ="";
        			String CUST_SEQ = "";
        			String ORD_DESC = "";
        			String DLV_MSG1 = "";
        			String DLV_MSG2 = "";
        			
//        			String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
//        			String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04

        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				Map<String, Object> object = listBody.get(i);
        				String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
        				
        				switch (bindColNm) {
        				//case "OUT_REQ_DT"             :  OUT_REQ_DT = replaceStr(object, k, "OUT_REQ_DT"); break;
        				case "IN_REQ_DT"              :  IN_REQ_DT = replaceStr(object, k, "IN_REQ_DT"); break;
        				case "CUST_ORD_NO"            :  CUST_ORD_NO = replaceStr(object, k, "CUST_ORD_NO"); break;
        				case "CUST_ORD_SEQ"           :  CUST_ORD_SEQ = replaceStr(object, k, "CUST_ORD_SEQ"); break;
        				case "TRUST_CUST_CD"          :  TRUST_CUST_CD = replaceStr(object, k, "TRUST_CUST_CD"); break;
        				case "TRANS_CUST_CD"          :  TRANS_CUST_CD = replaceStr(object, k, "TRANS_CUST_CD"); break;
        				case "TRANS_CUST_TEL"         :  TRANS_CUST_TEL = replaceStr(object, k, "TRANS_CUST_TEL"); break;
        				case "TRANS_REQ_DT"           :  TRANS_REQ_DT = replaceStr(object, k, "TRANS_REQ_DT"); break;
        				
                        case "ORD_QTY"                :  ORD_QTY = replaceStr(object, k, "ORD_QTY"); break;
        				case "UOM_CD"                 :  UOM_CD = replaceStr(object, k, "UOM_CD"); break;
        				case "SDEPT_CD"               :  SDEPT_CD = replaceStr(object, k, "SDEPT_CD"); break;
        				case "SALE_PER_CD"            :  SALE_PER_CD = replaceStr(object, k, "SALE_PER_CD"); break;
        				case "CAR_CD"                 :  CAR_CD = replaceStr(object, k, "CAR_CD"); break;
        				case "DRV_NM"                 :  DRV_NM = replaceStr(object, k, "DRV_NM"); break;
        				case "DLV_SEQ"                :  DLV_SEQ = replaceStr(object, k, "DLV_SEQ"); break;
        				case "DRV_TEL"                :  DRV_TEL = replaceStr(object, k, "DRV_TEL"); break;
        				case "CUST_LOT_NO"            :  CUST_LOT_NO = replaceStr(object, k, "CUST_LOT_NO"); break;
        				case "BL_NO"                  :  BL_NO = replaceStr(object, k, "BL_NO"); break;
        				case "REC_DT"                 :  REC_DT = replaceStr(object, k, "REC_DT"); break;
        				case "OUT_WH_CD"              :  OUT_WH_CD = replaceStr(object, k, "OUT_WH_CD"); break;
        				case "IN_WH_CD"               :  IN_WH_CD = replaceStr(object, k, "IN_WH_CD"); break;
        				case "MAKE_DT"                :  MAKE_DT = replaceStr(object, k, "MAKE_DT"); break;
        				case "TIME_PERIOD_DAY"        :  TIME_PERIOD_DAY = replaceStr(object, k, "TIME_PERIOD_DAY"); break;
        				case "WORK_YN"                :  WORK_YN = replaceStr(object, k, "WORK_YN"); break;
        				case "RJ_TYPE"                :  RJ_TYPE = replaceStr(object, k, "RJ_TYPE"); break;
        				case "LOC_YN"                 :  LOC_YN = replaceStr(object, k, "LOC_YN"); break;
        				case "CONF_YN"                :  CONF_YN = replaceStr(object, k, "CONF_YN"); break;
        				case "EA_CAPA"                :  EA_CAPA = replaceStr(object, k, "EA_CAPA"); break;
        				case "IN_ORD_WEIGHT"          :  IN_ORD_WEIGHT = replaceStr(object, k, "IN_ORD_WEIGHT"); break;
        				case "ITEM_CD"                :  ITEM_CD = replaceStr(object, k, "ITEM_CD"); break;
        				case "ITEM_NM"                :  ITEM_NM = replaceStr(object, k, "ITEM_NM"); break;
        				case "TRANS_CUST_NM"          :  TRANS_CUST_NM = replaceStr(object, k, "TRANS_CUST_NM"); break;
        				case "TRANS_CUST_ADDR"        :  TRANS_CUST_ADDR = replaceStr(object, k, "TRANS_CUST_ADDR"); break;
        				case "TRANS_EMP_NM"           :  TRANS_EMP_NM = replaceStr(object, k, "TRANS_EMP_NM"); break;
        				case "REMARK"                 :  REMARK = replaceStr(object, k, "REMARK"); break;
        				case "TRANS_ZIP_NO"           :  TRANS_ZIP_NO = replaceStr(object, k, "TRANS_ZIP_NO"); break;
        				case "ETC2"                   :  ETC2 = replaceStr(object, k, "ETC2"); break;
        				case "UNIT_AMT"               :  UNIT_AMT = replaceStr(object, k, "UNIT_AMT"); break;
        				case "TRANS_BIZ_NO"           :  TRANS_BIZ_NO = replaceStr(object, k, "TRANS_BIZ_NO"); break;
        				case "IN_CUST_ADDR"           :  IN_CUST_ADDR = replaceStr(object, k, "IN_CUST_ADDR"); break;
        				case "IN_CUST_CD"             :  IN_CUST_CD = replaceStr(object, k, "IN_CUST_CD"); break;
        				case "IN_CUST_NM"             :  IN_CUST_NM = replaceStr(object, k, "IN_CUST_NM"); break;
        				case "IN_CUST_TEL"            :  IN_CUST_TEL = replaceStr(object, k, "IN_CUST_TEL"); break;
        				case "IN_CUST_EMP_NM"         :  IN_CUST_EMP_NM = replaceStr(object, k, "IN_CUST_EMP_NM"); break;
        				case "EXPIRY_DATE"            :  EXPIRY_DATE = replaceStr(object, k, "EXPIRY_DATE"); break;
        				case "SALES_CUST_NM"          :  SALES_CUST_NM = replaceStr(object, k, "SALES_CUST_NM"); break;
        				case "ZIP"                    :  ZIP = replaceStr(object, k, "ZIP"); break;
        				case "ADDR"                   :  ADDR = replaceStr(object, k, "ADDR"); break;
        				case "ADDR2"                  :  ADDR = replaceStr(object, k, "ADDR2"); break;
        				case "PHONE_1"                :  PHONE_1 = replaceStr(object, k, "PHONE_1"); break;
        				case "ETC1"                   :  ETC1 = replaceStr(object, k, "ETC1"); break;
        				case "UNIT_NO"                :  UNIT_NO = replaceStr(object, k, "UNIT_NO"); break;
        				case "TIME_DATE"              :  TIME_DATE = replaceStr(object, k, "TIME_DATE"); break;
        				case "TIME_DATE_END"          :  TIME_DATE_END = replaceStr(object, k, "TIME_DATE_END"); break;
        				case "TIME_USE_END"           :  TIME_USE_END = replaceStr(object, k, "TIME_USE_END"); break;
        				case "PHONE_2"                :  PHONE_2 = replaceStr(object, k, "PHONE_2"); break;
        				case "BUY_CUST_NM"            :  BUY_CUST_NM = replaceStr(object, k, "BUY_CUST_NM"); break;
        				case "BUY_PHONE_1"            :  BUY_PHONE_1 = replaceStr(object, k, "BUY_PHONE_1"); break;
        				
                        case "BIZ_COND"               :  BIZ_COND = replaceStr(object, k, "BIZ_COND"); break;
        				case "BIZ_TYPE"               :  BIZ_TYPE = replaceStr(object, k, "BIZ_TYPE"); break;
        				case "BIZ_NO"                 :  BIZ_NO = replaceStr(object, k, "BIZ_NO"); break;
        				case "CUST_TYPE"              :  CUST_TYPE = replaceStr(object, k, "CUST_TYPE"); break;
        				
        				case "DATA_SENDER_NM"         :  DATA_SENDER_NM = replaceStr(object, k, "DATA_SENDER_NM"); break;
        				case "LEGACY_ORG_ORD_NO"      :  LEGACY_ORG_ORD_NO = replaceStr(object, k, "LEGACY_ORG_ORD_NO"); break;
        				case "ORD_DESC"               :  ORD_DESC = replaceStr(object, k, "ORD_DESC"); break;
        				case "DLV_MSG1"               :  DLV_MSG1 = replaceStr(object, k, "DLV_MSG1"); break;
        				case "DLV_MSG2"               :  DLV_MSG2 = replaceStr(object, k, "DLV_MSG2"); break;
        				
//        				case "LOT_TYPE"               :  LOT_TYPE = replaceStr(object, k, "LOT_TYPE"); break;
//        				case "OWNER_CD"               :  OWNER_CD = replaceStr(object, k, "OWNER_CD"); break;
        				
        				case "ORD_DEGREE"             :  ORD_DEGREE = replaceStr(object, k, "ORD_DEGREE");

						default:
							CUST_CD = (String)model.get("vrCustCd");
							SALES_COMPANY_NM = (String)model.get("vrCustSeqNm");
							CUST_SEQ= (String)model.get("vrCustSeq");
							/* 엑셀에 차수 컬럼 필드값이 Null일 경우 화면(vrSrchOrdDegree)selectBox값 바인딩)*/
							if(ORD_DEGREE.equals("")){
								ORD_DEGREE = (String)model.get("vrSrchOrdDegree");
							}
							
							break;
						}
        			}
                    
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    addr2[i]       		= ADDR2;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                    
                    phone2[i]       	= PHONE_2;     
                    buyCustNm[i]       	= BUY_CUST_NM;     
                    buyPhone1[i]       	= BUY_PHONE_1;
                    salesCompanyNm[i]   = SALES_COMPANY_NM;
                    ordDegree[i]       	= ORD_DEGREE;
                    bizCond[i]       	= BIZ_COND;
                    bizType[i]       	= BIZ_TYPE;
                    bizNo[i]       		= BIZ_NO;
                    custType[i]       	= CUST_TYPE;
                    
                    dataSenderNm[i]       		= DATA_SENDER_NM;
                    legacyOrgOrdNo[i]       	= LEGACY_ORG_ORD_NO;
//                    invoiceNo[i]				= INVOICE_NO;
                    
                    custSeq[i]					= CUST_SEQ;
                    
                    ordDesc[i]     			  	= ORD_DESC;
                    dlvMsg1[i]       			= DLV_MSG1;
                    dlvMsg2[i]       			= DLV_MSG2;
                    
//                    lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
//                    ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가

                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"	, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"		    , bizCond);
                modelIns.put("bizType"		    , bizType);
                modelIns.put("bizNo"		    , bizNo);
                modelIns.put("custType"		    , custType);
                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                modelIns.put("invoiceNo"		, invoiceNo);
                modelIns.put("custSeq"       	, custSeq);
                
                modelIns.put("ordDesc"       	, ordDesc);
                modelIns.put("dlvMsg1"       	, dlvMsg1);
                modelIns.put("dlvMsg2"       	, dlvMsg2);
                
//                modelIns.put("lotType"     		, lotType);  
//                modelIns.put("ownerCd"     		, ownerCd);  

				modelIns.put("vrOrdType"		, model.get("vrOrdType"));
				modelIns.put("vrOrdSubtype"		, model.get("vrOrdSubType")); // type 소문자임 주의
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
//                 일반 dao
            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2C_TS(modelIns);	
                
                ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                //m.put("O_CUR", modelIns.get("O_CUR"));       
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e) {
           throw e;
        }
        return m;
    } 
    
    
    /**
     * Method ID   		: saveListRtnOrderJavaB2TS
     * Method 설명      : 반품접수
     * 작성자           : yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveListRtnOrderJavaB2TS(Map<String, Object> model) throws Exception {
    	   Map<String, Object> m = new HashMap<String, Object>();
           try{

               int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
               // 기본프로시저 
               String[] no                 = new String[tmpCnt]; //no(처리)
               String[] reqDt              = new String[tmpCnt]; //입고요청일자(처리)
               String[] custOrdNo          = new String[tmpCnt]; //원주문번호(처리)
               String[] custOrdSeq         = new String[tmpCnt]; //원주문seq(처리)

               String[] trustCustCd        = new String[tmpCnt];
               String[] transCustCd        = new String[tmpCnt];
               String[] transCustTel       = new String[tmpCnt];
               String[] transReqDt         = new String[tmpCnt];
               String[] custCd             = new String[tmpCnt]; //화주코드(처리)

               String[] ordQty             = new String[tmpCnt]; //수량(처리) 
               String[] uomCd              = new String[tmpCnt];
               String[] sdeptCd            = new String[tmpCnt];
               String[] salePerCd          = new String[tmpCnt];
               String[] carCd              = new String[tmpCnt];

               String[] drvNm              = new String[tmpCnt];
               String[] dlvSeq             = new String[tmpCnt];
               String[] drvTel             = new String[tmpCnt];
               String[] custLotNo          = new String[tmpCnt];
               String[] blNo               = new String[tmpCnt];

               String[] recDt              = new String[tmpCnt];
               String[] whCd               = new String[tmpCnt];
               String[] makeDt             = new String[tmpCnt];
               String[] timePeriodDay      = new String[tmpCnt];
               String[] workYn             = new String[tmpCnt];

               String[] rjType             = new String[tmpCnt]; //반품사유
               String[] locYn              = new String[tmpCnt];
               String[] confYn             = new String[tmpCnt];
               String[] eaCapa             = new String[tmpCnt];
               String[] inOrdWeight        = new String[tmpCnt];

               String[] itemCd             = new String[tmpCnt]; //상품코드(처리)
               String[] itemNm             = new String[tmpCnt]; //상품명 (처리)
               String[] transCustNm        = new String[tmpCnt];
               String[] transCustAddr      = new String[tmpCnt];
               String[] transEmpNm         = new String[tmpCnt];

               String[] remark             = new String[tmpCnt];
               String[] transZipNo         = new String[tmpCnt];
               String[] etc2               = new String[tmpCnt];
               String[] unitAmt            = new String[tmpCnt];
               String[] transBizNo         = new String[tmpCnt];

               String[] inCustAddr         = new String[tmpCnt];
               String[] inCustCd           = new String[tmpCnt];
               String[] inCustNm           = new String[tmpCnt];
               String[] inCustTel          = new String[tmpCnt];
               String[] inCustEmpNm        = new String[tmpCnt];

               String[] expiryDate         = new String[tmpCnt];
               String[] salesCustNm        = new String[tmpCnt]; //수령인명(처리)
               String[] zip                = new String[tmpCnt]; //수령인우편번호(처리)
               String[] addr               = new String[tmpCnt]; //수령인주소(처리)
               String[] addr2              = new String[tmpCnt];

               String[] phone1             = new String[tmpCnt]; //수령인핸드폰번호(처리)
               String[] etc1               = new String[tmpCnt]; 
               String[] unitNo             = new String[tmpCnt]; // : B2C(처리)
               String[] phone2             = new String[tmpCnt]; //수령인 번호2 (처리)
               String[] buyCustNm          = new String[tmpCnt]; 

               String[] buyPhone1          = new String[tmpCnt]; 
               String[] salesCompanyNm     = new String[tmpCnt]; //eQtipTp ???
               String[] ordDegree          = new String[tmpCnt]; //등록차수(처리)
               String[] bizCond            = new String[tmpCnt]; //업태
               String[] bizType            = new String[tmpCnt]; //업종

               String[] bizNo              = new String[tmpCnt]; //사업자등록번호
               String[] custType           = new String[tmpCnt]; //화주타입
               String[] dataSenderNm       = new String[tmpCnt]; //쇼핑몰(처리)
               String[] legacyOrgOrdNo     = new String[tmpCnt]; //쇼핑몰주문번호(처리)
               String[] custSeq            = new String[tmpCnt];

               String[] ordDesc            = new String[tmpCnt]; //ord_desc
               String[] dlvMsg1            = new String[tmpCnt]; //배송메세지(처리)
               String[] dlvMsg2            = new String[tmpCnt];

               String[] LC_ID              = new String[tmpCnt];
               String[] USER_NO            = new String[tmpCnt];
               String[] WORK_IP            = new String[tmpCnt];
               

               String[] MSG_COCD            = new String[tmpCnt]; //callback용
               String[] MSG_NAME            = new String[tmpCnt]; //callback용

               for (int i = 0; i < tmpCnt; i++) {
               	String NO                  = Integer.toString(i+1);//no(처리)
               	String REQ_DT              = ""; //sysdate
               	String CUST_ORD_NO         = (String)model.get("I_CUST_ORD_NO" + i)+"_"+(String)model.get("I_ORD_TYPE");//원주문번호(처리) 반품  :RI 추가 
               	String CUST_ORD_SEQ        = (String)model.get("I_CUST_ORD_SEQ" + i);//원주문seq(처리)
               	                                
               	String TRUST_CUST_CD       = "";
               	String TRANS_CUST_CD       = "";
               	String TRANS_CUST_TEL      = "";
               	String TRANS_REQ_DT        = "";
               	String CUST_CD             = (String)model.get("I_CUST_CD" + i);//화주코드(처리)
               	                                
               	String ORD_QTY             = (String)model.get("I_ORD_QTY" + i);//출고수량(처리)
               	String UOM_CD              = (String)model.get("I_UOM_CD" + i);
               	String SDEPT_CD            = "";
               	String SALE_PER_CD         = "";
               	String CAR_CD              = "";
               	                                
               	String DRV_NM              = "";
               	String DLV_SEQ             = "";
               	String DRV_TEL             = "";
               	String CUST_LOT_NO         = "";
               	String BL_NO               = "";
               	                                
               	String REC_DT              = "";
               	String WH_CD               = "";
               	String MAKE_DT             = "";
               	String TIME_PERIOD_DAY     = "";
               	String WORK_YN             = "";
               	                                
               	String RJ_TYPE             = (String)model.get("I_RJ_TYPE" + i);
               	String LOC_YN              = "";
               	String CONF_YN             = "";
               	String EA_CAPA             = "";
               	String IN_ORD_WEIGHT       = "";
               	                                
               	String ITEM_CD             = (String)model.get("I_ITEM_CD" + i);//상품코드(처리)
               	String ITEM_NM             = (String)model.get("I_ITEM_NM" + i);//상품명(처리)
               	String TRANS_CUST_NM       = "";
               	String TRANS_CUST_ADDR     = "";
               	String TRANS_EMP_NM        = "";
               	                                
               	String REMARK              = ""; // op11 : ord_desc
               	String TRANS_ZIP_NO        = "";
               	String ETC2                = ""; // om: ord_Desc , OP11 : ETC2
               	String UNIT_AMT            = "";
               	String TRANS_BIZ_NO        = "";
               	                                
               	String IN_CUST_ADDR        = "";
               	String IN_CUST_CD          = "";
               	String IN_CUST_NM          = "";
               	String IN_CUST_TEL         = "";
               	String IN_CUST_EMP_NM      = "";
               	                                
               	String EXPIRY_DATE         = "";
               	String SALES_CUST_NM       = (String)model.get("I_SALES_CUST_NM" + i);//수령인명(처리)
               	String ZIP                 = (String)model.get("I_ZIP" + i);//수령인우편번호(처리)
               	String ADDR                = (String)model.get("I_ADDR" + i);//수령인주소(처리)
               	String ADDR2               = "";
               	                                
               	String PHONE1              = (String)model.get("I_PHONE1" + i);//수령인핸드폰번호(처리)
               	String ETC1                = "";
               	String UNIT_NO             = "";
               	String PHONE2              = "";
               	String BUY_CUST_NM         = "";
               	                                
               	String BUY_PHONE1          = "";
               	String SALES_COMPANY_NM    = "";//om : QTIO_TP???
               	String ORD_DEGREE          = "";//등록차수(처리)
               	String BIZ_COND            = "";//업태
               	String BIZ_TYPE            = "";//업종
               	                                
               	String BIZ_NO              = "";//사업자등록번호
               	String CUST_TYPE           = "";//화주타입
               	String DATA_SENDER_NM      = "";//쇼핑몰(처리)
               	String LEGACY_ORG_ORD_NO   = "";//
               	String CUST_SEQ            = "";//
               	                                
               	String ORD_DESC            = "";//ord_desc (X)
               	String DLV_MSG1            = "";//배송메세지(처리)
               	String DLV_MSG2            = "";
               	
               	
               	//변수에 담음. 
               	no[i]                      = NO;                 //no(처리)
               	reqDt[i]                   = REQ_DT;             //출고요청일자(처리)
               	custOrdNo[i]               = CUST_ORD_NO;        //원주문번호(처리)
               	custOrdSeq[i]              = CUST_ORD_SEQ;       //원주문seq(처리)
               	                             
               	trustCustCd[i]             = TRUST_CUST_CD;
               	transCustCd[i]             = TRANS_CUST_CD;
               	transCustTel[i]            = TRANS_CUST_TEL;
               	transReqDt[i]              = TRANS_REQ_DT;
               	custCd[i]                  = CUST_CD;            //화주코드(처리)
               	                             
               	ordQty[i]                  = ORD_QTY;            //출고수량(처리)
               	uomCd[i]                   = UOM_CD;
               	sdeptCd[i]                 = SDEPT_CD;
               	salePerCd[i]               = SALE_PER_CD;
               	carCd[i]                   = CAR_CD;
               	                             
               	drvNm[i]                   = DRV_NM;
               	dlvSeq[i]                  = DLV_SEQ;
               	drvTel[i]                  = DRV_TEL;
               	custLotNo[i]               = CUST_LOT_NO;        // 고객로뜨번호 
               	blNo[i]                    = BL_NO;
               	                             
               	recDt[i]                   = REC_DT;
               	whCd[i]                    = WH_CD;
               	makeDt[i]                  = MAKE_DT;
               	timePeriodDay[i]           = TIME_PERIOD_DAY;
               	workYn[i]                  = WORK_YN;
               	                             
               	rjType[i]                  = RJ_TYPE;
               	locYn[i]                   = LOC_YN;
               	confYn[i]                  = CONF_YN;
               	eaCapa[i]                  = EA_CAPA;
               	inOrdWeight[i]             = IN_ORD_WEIGHT;
               	                             
               	itemCd[i]                  = ITEM_CD;            //상품코드(처리)
               	itemNm[i]                  = ITEM_NM;            //상품명(처리)
               	transCustNm[i]             = TRANS_CUST_NM;
               	transCustAddr[i]           = TRANS_CUST_ADDR;
               	transEmpNm[i]              = TRANS_EMP_NM;
               	                             
               	remark[i]                  = REMARK;
               	transZipNo[i]              = TRANS_ZIP_NO;
               	etc2[i]                    = ETC2;
               	unitAmt[i]                 = UNIT_AMT;
               	transBizNo[i]              = TRANS_BIZ_NO;
               	                             
               	inCustAddr[i]              = IN_CUST_ADDR;
               	inCustCd[i]                = IN_CUST_CD;
               	inCustNm[i]                = IN_CUST_NM;
               	inCustTel[i]               = IN_CUST_TEL;
               	inCustEmpNm[i]             = IN_CUST_EMP_NM;
               	                             
               	expiryDate[i]              = EXPIRY_DATE;
               	salesCustNm[i]             = SALES_CUST_NM;      //수령인명(처리)
               	zip[i]                     = ZIP;                //수령인우편번호(처리)
               	addr[i]                    = ADDR;               //수령인주소(처리)
               	addr2[i]                   = ADDR2;
               	                             
               	phone1[i]                  = PHONE1;             //수령인핸드폰번호(처리)
               	etc1[i]                    = ETC1;
               	unitNo[i]                  = UNIT_NO;            //:B2C(처리)
               	phone2[i]                  = PHONE2;             //수령인번호2(처리)
               	buyCustNm[i]               = BUY_CUST_NM;
               	                             
               	buyPhone1[i]               = BUY_PHONE1;
               	salesCompanyNm[i]          = SALES_COMPANY_NM;   //eQtipTp???
               	ordDegree[i]               = ORD_DEGREE;         //등록차수(처리)
               	bizCond[i]                 = BIZ_COND;           //업태
               	bizType[i]                 = BIZ_TYPE;           //업종
               	                             
               	bizNo[i]                   = BIZ_NO;             //사업자등록번호
               	custType[i]                = CUST_TYPE;          //화주타입
               	dataSenderNm[i]            = DATA_SENDER_NM;     //쇼핑몰(처리)
               	legacyOrgOrdNo[i]          = LEGACY_ORG_ORD_NO;  //쇼핑몰주문번호(처리)
               	custSeq[i]                 = CUST_SEQ;
               	                             
               	ordDesc[i]                 = ORD_DESC;           //ord_desc
               	dlvMsg1[i]                 = DLV_MSG1;           //배송메세지(처리)
               	dlvMsg2[i]                 = DLV_MSG2;
              
               	                                                     
               }
             
               // 프로시져에 보낼것들 다담는다
               Map<String, Object> modelIns = new HashMap<String, Object>();

               modelIns.put("vrOrdType"		     , (String)model.get("I_ORD_TYPE")); //VARCHAR 주문타입 
               modelIns.put("vrOrdSubtype"       , "" 				);//VARCHAR 출고유형(처리)                                            
               modelIns.put("no"                 , no               );//no(처리)                                                  
               modelIns.put("reqDt"              , reqDt            );//출고요청일자(처리)                                        
               modelIns.put("custOrdNo"          , custOrdNo        );//원주문번호(처리)                                          
               modelIns.put("custOrdSeq"         , custOrdSeq       );//원주문seq(처리)                                           
                                                                          
               modelIns.put("trustCustCd"        , trustCustCd      );                                                            
               modelIns.put("transCustCd"        , transCustCd      );                                                            
               modelIns.put("transCustTel"       , transCustTel     );                                                            
               modelIns.put("transReqDt"         , transReqDt       );                                                            
               modelIns.put("custCd"             , custCd           );//화주코드(처리)                                            
                                                                          
               modelIns.put("ordQty"             , ordQty           );//출고수량(처리)                                            
               modelIns.put("uomCd"              , uomCd            );                                                            
               modelIns.put("sdeptCd"            , sdeptCd          );                                                            
               modelIns.put("salePerCd"          , salePerCd        );                                                            
               modelIns.put("carCd"              , carCd            );                                                            
                                                                          
               modelIns.put("drvNm"              , drvNm            );                                                            
               modelIns.put("dlvSeq"             , dlvSeq           );                                                            
               modelIns.put("drvTel"             , drvTel           );                                                            
               modelIns.put("custLotNo"          , custLotNo        );                                                            
               modelIns.put("blNo"               , blNo             );                                                            
                                                                          
               modelIns.put("recDt"              , recDt            );                                                            
               modelIns.put("whCd"               , whCd             );                                                            
               modelIns.put("makeDt"             , makeDt           );                                                            
               modelIns.put("timePeriodDay"      , timePeriodDay    );                                                            
               modelIns.put("workYn"             , workYn           );                                                            
                                                                          
               modelIns.put("rjType"             , rjType           );                                                            
               modelIns.put("locYn"              , locYn            );                                                            
               modelIns.put("confYn"             , confYn           );                                                            
               modelIns.put("eaCapa"             , eaCapa           );                                                            
               modelIns.put("inOrdWeight"        , inOrdWeight      );                                                            
                                                                          
               modelIns.put("itemCd"             , itemCd           );//상품코드(처리)                                            
               modelIns.put("itemNm"             , itemNm           );//상품명(처리)                                              
               modelIns.put("transCustNm"        , transCustNm      );                                                            
               modelIns.put("transCustAddr"      , transCustAddr    );                                                            
               modelIns.put("transEmpNm"         , transEmpNm       );                                                            
                                                                          
               modelIns.put("remark"             , remark           );                                                            
               modelIns.put("transZipNo"         , transZipNo       );                                                            
               modelIns.put("etc2"               , etc2             );                                                            
               modelIns.put("unitAmt"            , unitAmt          );                                                            
               modelIns.put("transBizNo"         , transBizNo       );                                                            
                                                                          
               modelIns.put("inCustAddr"         , inCustAddr       );                                                            
               modelIns.put("inCustCd"           , inCustCd         );                                                            
               modelIns.put("inCustNm"           , inCustNm         );                                                            
               modelIns.put("inCustTel"          , inCustTel        );                                                            
               modelIns.put("inCustEmpNm"        , inCustEmpNm      );                                                            
                                                                          
               modelIns.put("expiryDate"         , expiryDate       );                                                            
               modelIns.put("salesCustNm"        , salesCustNm      );//수령인명(처리)                                            
               modelIns.put("zip"                , zip              );//수령인우편번호(처리)                                      
               modelIns.put("addr"               , addr             );//수령인주소(처리)                                          
               modelIns.put("addr2"              , addr2            );                                                            
                                                                          
               modelIns.put("phone1"             , phone1           );//수령인핸드폰번호(처리)                                    
               modelIns.put("etc1"               , etc1             );                                                            
               modelIns.put("unitNo"             , unitNo           );//:B2C(처리)                                                
               modelIns.put("phone2"             , phone2           );//수령인번호2(처리)                                         
               modelIns.put("buyCustNm"          , buyCustNm        );                                                            
                                                                          
               modelIns.put("buyPhone1"          , buyPhone1        );                                                            
               modelIns.put("salesCompanyNm"     , salesCompanyNm   );//eQtipTp???                                                
               modelIns.put("ordDegree"          , ordDegree        );//등록차수(처리)                                            
               modelIns.put("bizCond"            , bizCond          );//업태                                                      
               modelIns.put("bizType"            , bizType          );//업종                                                      
                                                                          
               modelIns.put("bizNo"              , bizNo            );//사업자등록번호                                            
               modelIns.put("custType"           , custType         );//화주타입                                                  
               modelIns.put("dataSenderNm"       , dataSenderNm     );//쇼핑몰(처리)                                              
               modelIns.put("legacyOrgOrdNo"     , legacyOrgOrdNo   );//쇼핑몰주문번호(처리)                                      
               modelIns.put("custSeq"            , custSeq          );                                                            
                                                                          
               modelIns.put("ordDesc"            , ordDesc          );//ord_desc                                                  
               modelIns.put("dlvMsg1"            , dlvMsg1          );//배송메세지(처리)                                          
               modelIns.put("dlvMsg2"            , dlvMsg2          );                                                            
                                                                          
               modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
               modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
               modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
               
               System.out.println(modelIns);
               
               modelIns = (Map<String, Object>)dao.saveExcelOrderB2TS(modelIns);
               ServiceUtil.isValidReturnCode("WMSIF701", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

               
               m.put("MSG", MessageResolver.getMessage("save.success"));
               m.put("MSG_ORA", "");
               m.put("errCnt", 0);
               
           }catch(Exception e){
           	if (log.isErrorEnabled()) {
   				log.error("Fail to get result :", e);
   			} 
               m.put("errCnt", 1);
               m.put("MSG", MessageResolver.getMessage("save.error") );
           }
           return m;
    }
    
    
    /**2442
     * 
     * 대체 Method ID   : saveListRtnOrderJavaB2AS
     * 대체 Method 설명    : 반품접수
     * 작성자                      : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveListRtnOrderJavaB2AS(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		 	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
    		 	
	String[] no             = new String[tmpCnt];      //no(처리)
    			
    			String[] outReqDt       = new String[tmpCnt];   
    			String[] inReqDt        = new String[tmpCnt];     
    			String[] custOrdNo      = new String[tmpCnt];     //원주문번호(처리)
    			String[] custOrdSeq     = new String[tmpCnt];    //원주문seq(처리)
    			String[] trustCustCd    = new String[tmpCnt];     
    			
    			String[] transCustCd    = new String[tmpCnt];                     
    			String[] transCustTel   = new String[tmpCnt];         
    			String[] transReqDt     = new String[tmpCnt];     
    			String[] custCd         = new String[tmpCnt];      //화주코드(처리)
    			String[] ordQty         = new String[tmpCnt];     //수량(처리) 
    			
    			String[] uomCd          = new String[tmpCnt];                
    			String[] sdeptCd        = new String[tmpCnt];         
    			String[] salePerCd      = new String[tmpCnt];     
    			String[] carCd          = new String[tmpCnt];     
    			String[] drvNm          = new String[tmpCnt];     
    			
    			String[] dlvSeq         = new String[tmpCnt];                
    			String[] drvTel         = new String[tmpCnt];         
    			String[] custLotNo      = new String[tmpCnt];     
    			String[] blNo           = new String[tmpCnt];     
    			String[] recDt          = new String[tmpCnt];     
    			
    			String[] outWhCd        = new String[tmpCnt];                
    			String[] inWhCd         = new String[tmpCnt];         
    			String[] makeDt         = new String[tmpCnt];     
    			String[] timePeriodDay  = new String[tmpCnt];     
    			String[] workYn         = new String[tmpCnt];     
    			
    			String[] rjType         = new String[tmpCnt];        //반품사유         
    			String[] locYn          = new String[tmpCnt];         
    			String[] confYn         = new String[tmpCnt];     
    			String[] eaCapa         = new String[tmpCnt];     
    			String[] inOrdWeight    = new String[tmpCnt];     
    			
    			String[] itemCd         = new String[tmpCnt];        //상품코드(처리)        
    			String[] itemNm         = new String[tmpCnt];        //상품명 (처리) 
    			String[] transCustNm    = new String[tmpCnt];     
    			String[] transCustAddr  = new String[tmpCnt];     
    			String[] transEmpNm     = new String[tmpCnt];     
    			
    			String[] remark         = new String[tmpCnt];                
    			String[] transZipNo     = new String[tmpCnt];         
    			String[] etc2           = new String[tmpCnt];     
    			String[] unitAmt        = new String[tmpCnt];     
    			String[] transBizNo     = new String[tmpCnt];     
    			
    			String[] inCustAddr     = new String[tmpCnt];                
    			String[] inCustCd       = new String[tmpCnt];         
    			String[] inCustNm       = new String[tmpCnt];     
    			String[] inCustTel      = new String[tmpCnt];     
    			String[] inCustEmpNm    = new String[tmpCnt];     
    			
    			String[] expiryDate     = new String[tmpCnt];
    			String[] salesCustNm    = new String[tmpCnt]; //수령인명(처리)
    			String[] zip     		= new String[tmpCnt]; //수령인우편번호(처리)
    			String[] addr     		= new String[tmpCnt]; //수령인주소(처리)
    			String[] phone1    	 	= new String[tmpCnt];//수령인핸드폰번호(처리)
    			
    			String[] etc1    		= new String[tmpCnt];
    			String[] unitNo    		= new String[tmpCnt];                
    			String[] salesCompanyNm	 	= new String[tmpCnt];
    			String[] timeDate       = new String[tmpCnt];   //상품유효기간     
    			String[] timeDateEnd    = new String[tmpCnt];   //상품유효기간만료일
    			
    			String[] timeUseEnd     = new String[tmpCnt];   //소비가한만료일
    			String[] locCd     		= new String[tmpCnt];   //로케이션코드
    			
    			String[] epType     	= new String[tmpCnt];   //납품유형
    			
    			
//                String[] ordInsDt     	= new String[tmpCnt];   //주문등록일	2022-03-03
				String[] lotType     	= new String[tmpCnt];   //LOT속성 	2022-03-04
				String[] ownerCd     	= new String[tmpCnt];   //소유자		2022-03-04
				
				for(int i = 0 ; i < tmpCnt ; i ++){
				 	String NO                  = Integer.toString(i+1);//no(처리)
					String OUT_REQ_DT      = "";
					String IN_REQ_DT       = "";  //sysdate
					String CUST_ORD_NO     = (String)model.get("I_CUST_ORD_NO" + i)+"_"+(String)model.get("I_ORD_SUBTYPE");//원주문번호(처리) 반품  :22 추가 
					String CUST_ORD_SEQ    = (String)model.get("I_CUST_ORD_SEQ" + i);//원주문seq(처리)
					String TRUST_CUST_CD   = "";
					String TRANS_CUST_CD   = "";
					String TRANS_CUST_TEL  = "";
					String TRANS_REQ_DT    = "";
					String CUST_CD         = (String)model.get("I_CUST_CD" + i);//화주코드(처리)
					String ORD_QTY         = (String)model.get("I_ORD_QTY" + i);//출고수량(처리)
					String UOM_CD          = (String)model.get("I_UOM_CD" + i);
					String SDEPT_CD        = "";
					String SALE_PER_CD     = "";
					String CAR_CD          = "";
					String DRV_NM          = "";
					String DLV_SEQ         = "";
					String DRV_TEL         = "";
					String CUST_LOT_NO     = "";
					String BL_NO           = "";
					String REC_DT          = "";
					String OUT_WH_CD       = "";
					String IN_WH_CD        = "";
					String MAKE_DT         = "";
					String TIME_PERIOD_DAY = "";
					String WORK_YN         = "";
					String RJ_TYPE         = (String)model.get("I_RJ_TYPE" + i);
					String LOC_YN          = "";
					String CONF_YN         = "";
					String EA_CAPA         = "";
					String IN_ORD_WEIGHT   = "";
					String ITEM_CD         = (String)model.get("I_ITEM_CD" + i);//상품코드(처리)
					String ITEM_NM         = (String)model.get("I_ITEM_NM" + i);//상품명(처리)
					String TRANS_CUST_NM   = "";
					String TRANS_CUST_ADDR = "";
					String TRANS_EMP_NM    = "";
					String REMARK          = "";
					String TRANS_ZIP_NO    = "";
					String ETC2            = "";
					String UNIT_AMT        = "";
					String TRANS_BIZ_NO    = "";
					String IN_CUST_ADDR    = "";
					String IN_CUST_CD      = "";
					String IN_CUST_NM      = "";
					String IN_CUST_TEL     = "";
					String IN_CUST_EMP_NM  = "";
					String EXPIRY_DATE     = "";
					String SALES_CUST_NM   = (String)model.get("I_SALES_CUST_NM" + i);//수령인명(처리)
					String ZIP             = (String)model.get("I_ZIP" + i);//수령인우편번호(처리)
					String ADDR            = (String)model.get("I_ADDR" + i);//수령인주소(처리)
					String PHONE_1         = (String)model.get("I_PHONE1" + i);//수령인핸드폰번호(처리)
					String ETC1            = "";
					String UNIT_NO         = "";
					String SALES_COMPANY_NM   = "";
					String TIME_DATE       = "";
					String TIME_DATE_END   = "";
					String TIME_USE_END    = "";
					String LOC_CD    	   = "";
					String EP_TYPE    	   = "";
					
				//        			String ORD_INS_DT         = ""; // 템플릿 추가 sing09  2022-03-03
					String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
					String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04
					
					
				 	//변수에 담음. 
					no[i]               = NO;
					outReqDt[i]         = OUT_REQ_DT.replaceAll("[^\\d]", "");    
					inReqDt[i]          = IN_REQ_DT.replaceAll("[^\\d]", "");    
					custOrdNo[i]        = CUST_ORD_NO;    
					custOrdSeq[i]       = CUST_ORD_SEQ;    
					trustCustCd[i]      = TRUST_CUST_CD;    
					
					transCustCd[i]      = TRANS_CUST_CD;    
					transCustTel[i]     = TRANS_CUST_TEL;    
					transReqDt[i]       = TRANS_REQ_DT;    
					custCd[i]           = CUST_CD;    
					ordQty[i]           = ORD_QTY;    
					
					uomCd[i]            = UOM_CD;    
					sdeptCd[i]          = SDEPT_CD;    
					salePerCd[i]        = SALE_PER_CD;    
					carCd[i]            = CAR_CD;
					drvNm[i]            = DRV_NM;
					
					dlvSeq[i]           = DLV_SEQ;    
					drvTel[i]           = DRV_TEL;    
					custLotNo[i]        = CUST_LOT_NO;    
					blNo[i]             = BL_NO;    
					recDt[i]            = REC_DT;    
					
					outWhCd[i]          = OUT_WH_CD;
					inWhCd[i]           = IN_WH_CD;
					makeDt[i]           = MAKE_DT;
					timePeriodDay[i]    = TIME_PERIOD_DAY;
					workYn[i]           = WORK_YN;
					
					rjType[i]           = RJ_TYPE;    
					locYn[i]            = LOC_YN;    
					confYn[i]           = CONF_YN;    
					eaCapa[i]           = EA_CAPA;    
					inOrdWeight[i]      = IN_ORD_WEIGHT;   
					
					itemCd[i]           = ITEM_CD;    
					itemNm[i]           = ITEM_NM;    
					transCustNm[i]      = TRANS_CUST_NM;    
					transCustAddr[i]    = TRANS_CUST_ADDR;    
					transEmpNm[i]       = TRANS_EMP_NM;    
					
					remark[i]           = REMARK;    
					transZipNo[i]       = TRANS_ZIP_NO;    
					etc2[i]             = ETC2;    
					unitAmt[i]          = UNIT_AMT.replaceAll("[^\\d]", "");    
					transBizNo[i]       = TRANS_BIZ_NO;    
					
					inCustAddr[i]       = IN_CUST_ADDR;   
					inCustCd[i]         = IN_CUST_CD;    
					inCustNm[i]         = IN_CUST_NM;    
					inCustTel[i]        = IN_CUST_TEL;    
					inCustEmpNm[i]      = IN_CUST_EMP_NM;    
					
					expiryDate[i]       = EXPIRY_DATE;
					salesCustNm[i]      = SALES_CUST_NM;
					zip[i]       		= ZIP;
					addr[i]       		= ADDR;
					phone1[i]       	= PHONE_1;
					
					etc1[i]      		= ETC1;
					unitNo[i]      		= UNIT_NO;
					salesCompanyNm[i]   = SALES_COMPANY_NM;
					timeDate[i]         = TIME_DATE;      
					timeDateEnd[i]      = TIME_DATE_END;     
					
					timeUseEnd[i]       = TIME_USE_END;
					locCd[i]       		= LOC_CD;
					epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
					
				//                    ordInsDt[i]       	= ORD_INS_DT;	//주문등록일 신규컬럼 추가
					lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
					ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가
					
				}
				
				//프로시져에 보낼것들 다담는다
				Map<String, Object> modelIns = new HashMap<String, Object>();
				
				modelIns.put("no"  , no);
				if(model.get("I_ORD_TYPE").equals("I") || model.get("I_ORD_TYPE").equals("01")){
					modelIns.put("reqDt"     	, inReqDt);
					modelIns.put("whCd"      	, inWhCd);
				}else{
					modelIns.put("reqDt"     	, outReqDt);
					modelIns.put("whCd"      	, outWhCd);
				}
				modelIns.put("custOrdNo"    	, custOrdNo);
				modelIns.put("custOrdSeq"   	, custOrdSeq);
				modelIns.put("trustCustCd"  	, trustCustCd); //5
				
				modelIns.put("transCustCd"  	, transCustCd);
				modelIns.put("transCustTel" 	, transCustTel);
				modelIns.put("transReqDt"   	, transReqDt);
				modelIns.put("custCd"       	, custCd);
				modelIns.put("ordQty"       	, ordQty);      //10
				
				modelIns.put("uomCd"        	, uomCd);
				modelIns.put("sdeptCd"      	, sdeptCd);
				modelIns.put("salePerCd"    	, salePerCd);
				modelIns.put("carCd"        	, carCd);
				modelIns.put("drvNm"        	, drvNm);       //15
				
				modelIns.put("dlvSeq"       	, dlvSeq);
				modelIns.put("drvTel"       	, drvTel);
				modelIns.put("custLotNo"    	, custLotNo);
				modelIns.put("blNo"         	, blNo);
				modelIns.put("recDt"        	, recDt);       //20
				
				modelIns.put("makeDt"       	, makeDt);
				modelIns.put("timePeriodDay"	, timePeriodDay);
				modelIns.put("workYn"       	, workYn);                
				modelIns.put("rjType"       	, rjType);
				modelIns.put("locYn"        	, locYn);       //25
				
				modelIns.put("confYn"       	, confYn);     
				modelIns.put("eaCapa"       	, eaCapa);
				modelIns.put("inOrdWeight"  	, inOrdWeight); //28
				modelIns.put("itemCd"           , itemCd);
				modelIns.put("itemNm"           , itemNm);
				
				modelIns.put("transCustNm"      , transCustNm);
				modelIns.put("transCustAddr"    , transCustAddr);
				modelIns.put("transEmpNm"       , transEmpNm);
				modelIns.put("remark"           , remark);
				modelIns.put("transZipNo"       , transZipNo);
				
				modelIns.put("etc2"             , etc2);
				modelIns.put("unitAmt"          , unitAmt);
				modelIns.put("transBizNo"       , transBizNo);
				modelIns.put("inCustAddr"       , inCustAddr);
				modelIns.put("inCustCd"         , inCustCd);
				
				modelIns.put("inCustNm"         , inCustNm);                 
				modelIns.put("inCustTel"        , inCustTel);
				modelIns.put("inCustEmpNm"      , inCustEmpNm);      
				modelIns.put("expiryDate"       , expiryDate);
				modelIns.put("salesCustNm"      , salesCustNm);
				
				modelIns.put("zip"       		, zip);
				modelIns.put("addr"       		, addr);
				modelIns.put("phone1"       	, phone1);
				modelIns.put("etc1"     	 	, etc1);
				modelIns.put("unitNo"     	 	, unitNo);
				
				modelIns.put("salesCompanyNm"    	, salesCompanyNm);
				
				modelIns.put("time_date"        , timeDate);
				modelIns.put("time_date_end"    , timeDateEnd);                
				modelIns.put("time_use_end"     , timeUseEnd);  
				modelIns.put("locCd"     		, locCd);  
				modelIns.put("epType"     		, epType);  
				
				//                modelIns.put("ordInsDt"     	, ordInsDt);  
				modelIns.put("lotType"     		, lotType);  
				modelIns.put("ownerCd"     		, ownerCd);  
				
				modelIns.put("vrOrdType"		, model.get("I_ORD_TYPE"));
				modelIns.put("vrOrdSubType"		, model.get("I_ORD_SUBTYPE"));
				
				modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
				modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
				modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
				
				//dao                
				modelIns = (Map<String, Object>)dao.saveExcelOrder_AS(modelIns);
				ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
				
				m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("MSG_ORA", "");
                m.put("errCnt", 0);          
    		
    		
    	} catch(Exception e){
    		if (log.isErrorEnabled()) {
   				log.error("Fail to get result :", e);
   			} 
    		m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
    		throw e;
    	}
    	return m;
    }
	
	/**
     * Method ID : saveBoxRecom
     * Method 설명 : 박스추천 (CBM)
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveBoxRecom(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
    		
    		String[] eOrgOrdId 				= new String[tmpCnt];
    		
    		for (int i = 0; i < tmpCnt; i++) {
    			eOrgOrdId[i] 			= (String)model.get("I_ORG_ORD_ID" + i);
    		}
    		Map<String, Object> modelIns = new HashMap<String, Object>();
    		
    		ArrayList<String> arrayList = new ArrayList<>();
            for(String data : eOrgOrdId){
                if(!arrayList.contains(data))
                    arrayList.add(data);
            }
            String[] array = arrayList.toArray(new String[arrayList.size()]);
            
    		modelIns.put("I_IDX"	 , array); 
    		modelIns.put("I_CUST_ID", (String)model.get("I_CUST_ID"));
    		modelIns.put("I_LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO)); 
    		modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    		modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    		
    		modelIns = (Map<String, Object>)dao.spSaveBoxRecom(modelIns);
    		ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    		
    		map.put("MSG", MessageResolver.getMessage("save.success"));
    		map.put("MSG_ORA", "");
    		map.put("errCnt", 0);
    	} catch(BizException be) {
			map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("errCnt", -1);
    		map.put("MSG_ORA", be.getMessage());
        }catch (Exception e) {
    		throw e;
    	}
    	
    	return map;
    }

    /*-
     * Method ID    : selectHistoryInsert
     * Method 설명    : 피킹리스트 발행전 UUID 데이터저장 및 히스토리
     * 작성자          : ysi, schan(수정)
     * @param model
     * @return  
     */
    @Override
    public Map<String, Object> selectPickingListHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            String uuid = UUID.randomUUID().toString();
 
            for (int i = 0; i < tmpCnt; i++) {
                Map<String, Object> obj = new HashMap<String, Object>();
                obj.put("uuid"          , uuid);
                obj.put("LC_ID"         , (String)model.get("LC_ID" + i));
                obj.put("ORG_ORD_ID"    , (String)model.get("I_ORG_ORD_ID" + i));
                obj.put("ORD_ID"    , (String)model.get("I_ORD_ID" + i));
                obj.put("ORD_SEQ"    , (String)model.get("I_ORD_SEQ" + i));
                obj.put("RITEM_ID"    , (String)model.get("I_RITEM_ID" + i));
                obj.put("WORK_QTY"    , (String)model.get("I_WORK_QTY" + i));
                obj.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
                
                obj = (Map<String, Object>)dao.selectPickingListHistory(obj);
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            m.put("UUID", uuid);
            m.put("MSG_ORA", "");
        }catch(Exception e){
            if (log.isErrorEnabled()) {
                log.error("Fail to get result :", e);
            } 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : insertPickingListLog(핑거스캔전 리스트출력)
     * Method 설명 : 핑거스캔전 리스트LOG 저장
     * 작성자 : LEE
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> insertPickingListLog(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            
            String strParameter = (String)model.get("strParameter");
            String parameter[]   = strParameter.split(",");
            
            modelIns.put("I_VRSRP_GB", parameter[0].equals("")? null : parameter[0]);
            modelIns.put("I_VR_SRCH_CUST_CD", parameter[1].equals("")? null : parameter[1]);
            modelIns.put("I_VR_SRCH_CUST_ID", parameter[2].equals("")? null : parameter[2]);
            modelIns.put("I_VR_SRCH_WH_NM", parameter[3].equals("")? null : parameter[3]);
            
            modelIns.put("I_VR_SRCH_ORDER_ID",parameter[4].equals("")? null :parameter[4].replace('@', ','));
            modelIns.put("I_CMB_SRCH_TYPE", parameter[5].equals("")? null : parameter[5]);
            modelIns.put("I_VR_SRCH_REQ_DT_FROM", parameter[6].equals("")? null : parameter[6]);
            modelIns.put("I_VR_SRCH_REQ_DT_TO", parameter[7].equals("")? null : parameter[7]);
            modelIns.put("I_VR_SRCH_CUST_LOT_NO", parameter[8].equals("")? null : parameter[8]);
            modelIns.put("I_VR_SRCH_ORD_SUBTYPE",parameter[9].equals("")? null : parameter[9]);
            
            modelIns.put("I_TXT_SRCH_CUST_ID", parameter[10].equals("")? null : parameter[10]);
            modelIns.put("I_VR_SRCH_ZONE_CD", parameter[11].equals("")? null : parameter[11]);
            modelIns.put("I_VR_SRCH_ITEM_GRP_ID", parameter[12].equals("")? null : parameter[12]);
            modelIns.put("I_VR_SRCH_ORG_ORD_ID", parameter[13].equals("")? null : parameter[13]);
            modelIns.put("I_VR_SRCH_SALES_CUST_NM", parameter[14].equals("")? null : parameter[14]);
            modelIns.put("I_VR_SRCH_RITEM_ID", parameter[15].equals("")? null : parameter[15]);
            modelIns.put("I_VR_SRCH_INSERT_SOURCE", parameter[16].equals("")? null : parameter[16]);
            modelIns.put("I_USER_NAME", parameter[17].equals("")? null : parameter[17]);
            modelIns.put("I_VR_SRCH_ORDER_PHASE", parameter[18].equals("")? null : parameter[18]);
            modelIns.put("I_VR_SRCH_PICKING_LOG", parameter[19].equals("")? null : parameter[19]);
            
            modelIns.put("I_VR_SRCH_SHIPPING_COMPANY", parameter[20].equals("")? null : parameter[20]);
            modelIns.put("I_VR_SRCH_SYNNARA_ORD_YN", parameter[21].equals("")? null : parameter[21]);
            modelIns.put("I_VR_SRCH_ORDER_BY_SKU_CNT", parameter[22].equals("")? null : parameter[22]);
            modelIns.put("I_VR_SRCH_ORDER_BY_MAX_QTY", parameter[23].equals("")? null : parameter[23]);
            modelIns.put("I_VR_SRCH_CONTAIN_RITEM_ID", parameter[24].equals("")? null : parameter[24]);
            modelIns.put("I_VR_SRCH_ORD_BIZ_TYPE", parameter[25].equals("")? null : parameter[25]);
            modelIns.put("I_VR_SRCH_UUID", parameter[26].equals("")? null : parameter[26]);
            
            modelIns = (Map<String, Object>)dao.spInsertPickingListLog(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            
            map.put("O_SYSDATE",modelIns.get("O_SYSDATE"));
            map.put("O_MSG_CODE",modelIns.get("O_MSG_CODE"));
            map.put("O_MSG_NAME",modelIns.get("O_MSG_NAME"));
        } catch(BizException be) {
            
        }catch (Exception e) {
            throw e;
        }
        
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJavaAllowBlankOlive
     * 대체 Method 설명    : 올리브영 입고박스 템플릿 주문 저장 - 공란허용 로직
     * 작성자                      : kimzero(수정), YSI
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJavaAllowBlankOlive(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
        Gson gson         = new Gson();
        List<Map<String, Object>> listBody = (ArrayList)model.get("LIST");
        String custId = String.valueOf(model.get("vrCustCd"));
        List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
        
        int listHeaderCnt = listHeader.size();
        int listBodyCnt   = listBody.size();
        
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt < 1){
                m.put("MSG", MessageResolver.getMessage("save.error"));
                m.put("MSG_ORA", "데이터가 없습니다.");
                m.put("errCnt", 1);
                return m;
            }
                
            String[] inReqDt        = new String[listBodyCnt];          
            String[] ordQty         = new String[listBodyCnt];           
            String[] itemCd         = new String[listBodyCnt];                
            String[] etc2           = new String[listBodyCnt];     
            
            for(int i = 0 ; i < listBodyCnt ; i ++){
                String IN_REQ_DT       = "";
                String ORD_QTY         = "";
                String ITEM_CD         = "";
                String ETC2            = "";
                
                for(int k = 0 ; k < listHeaderCnt ; k++){
                    Map<String, Object> object = listBody.get(i);
                    String bindColNm = String.valueOf(listHeader.get(k).get("FORMAT_BIND_COL"));
                    switch (bindColNm) {
                    case "IN_REQ_DT"            : IN_REQ_DT             = replaceStr(object, k, "IN_REQ_DT"); break;
                    case "ORD_QTY"              : ORD_QTY               = replaceStr(object, k, "ORD_QTY"); break;
                    case "ITEM_CD"              : ITEM_CD               = replaceStr(object, k, "ITEM_CD"); break;
                    case "ETC2"                 : ETC2                  = replaceStr(object, k, "ETC2"); break;
                    
                    }
                }
                
                String NO = Integer.toString(i+1);

                inReqDt[i]          = IN_REQ_DT;    
                ordQty[i]           = ORD_QTY;         
                itemCd[i]           = ITEM_CD;     
                etc2[i]             = ETC2;       
            }         
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("reqDt"            , inReqDt);
            modelIns.put("ordQty"           , ordQty);     
            modelIns.put("itemCd"           , itemCd);
            modelIns.put("etc2"             , etc2);
            
            modelIns.put("LC_ID"            , model.get("SS_SVC_NO"));   
            modelIns.put("WORK_IP"          , model.get("SS_CLIENT_IP"));  
            modelIns.put("USER_NO"          , model.get("SS_USER_NO"));

            //dao                
            modelIns = (Map<String, Object>)dao.saveExcelOrderOlive(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("O_CUR", modelIns.get("O_CUR"));
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> dasMake(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	String old_ordId = "";
            	String ordId = "";
            	String custId = "";
                String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                for(int i = 0 ; i < tmpCnt ; i ++){
                	String ordId_work   = (String)model.get("ORD_ID"+i);               
                    custId  = (String)model.get("CUST_ID"+i); 

                	int go01 = 0; // 0 : stop, 1 : go
                	if(i == 0){
                		go01 = 1;
                		ordId = "'";
                	}else {
                		if(!ordId_work.equals(old_ordId)){
                			ordId = ordId + ",'";
                			go01 = 1;
                		}
                	}
                	
                	if(go01 == 1){
                		ordId = ordId + ordId_work + "'";
                		old_ordId = ordId_work;
                	}
                }
                //프로시져에 보낼것들 다담는다
                modelIns.put("ordId", ordId);
                modelIns.put("custId", custId);
                
                //session 및 등록정보
                modelIns.put("LC_ID", LC_ID);
                modelIns.put("WORK_IP", WORK_IP);
                modelIns.put("USER_NO", USER_NO);
                
                modelIns = (Map<String, Object>)dao.dasmake(modelIns);
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> dasDelete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	Map<String, Object> modelIns = new HashMap<String, Object>();
        	
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	String old_DEV_ORD_DEGREE = "";
                for(int i = 0 ; i < tmpCnt ; i ++){
                    String custId  = (String)model.get("CUST_ID"+i);
                    String DEV_ORD_DEGREE = (String)model.get("DEV_ORD_DEGREE"+i);
                    if(DEV_ORD_DEGREE.isEmpty() || DEV_ORD_DEGREE.equals("")){
                    	m.put("errCnt", -1);
                        m.put("MSG", "차수가 없는 주문이 있습니다." );
                        return m;
                    }
                    
                    String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
                    String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
                    String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
                	int go01 = 0; // 0 : stop, 1 : go
                	if(i == 0){
                		go01 = 1;
                		old_DEV_ORD_DEGREE = DEV_ORD_DEGREE;
                	}else {
                		if(!DEV_ORD_DEGREE.equals(old_DEV_ORD_DEGREE)){
                			go01 = 1;
                		}
                	}
                	
                	if(go01 == 1){
	                    //프로시져에 보낼것들 다담는다
	                    modelIns.put("custId", custId);
	                    modelIns.put("DEV_ORD_DEGREE", DEV_ORD_DEGREE);
	                    
	                    //session 및 등록정보
	                    modelIns.put("LC_ID", LC_ID);
	                    modelIns.put("WORK_IP", WORK_IP);
	                    modelIns.put("USER_NO", USER_NO);
	                    
	                    dao.dasDelete_if(modelIns);
	                    modelIns = (Map<String, Object>)dao.dasDelete(modelIns);
                	}
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> dasIfSend(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	String custId = "";
        	String old_DEV_ORD_DEGREE = "";
        	String DEV_ORD_DEGREE = "";
        	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            for(int i = 0 ; i < tmpCnt ; i ++){
            	String DEV_ORD_DEGREE_work   = (String)model.get("DEV_ORD_DEGREE"+i);               
                custId  = (String)model.get("CUST_ID"+i); 

            	int go01 = 0; // 0 : stop, 1 : go
            	if(i == 0){
            		go01 = 1;
            		DEV_ORD_DEGREE = "'";
            	}else {
            		if(!DEV_ORD_DEGREE_work.equals(old_DEV_ORD_DEGREE)){
            			DEV_ORD_DEGREE = DEV_ORD_DEGREE + ",'";
            			go01 = 1;
            		}
            	}
            	
            	if(go01 == 1){
            		DEV_ORD_DEGREE = DEV_ORD_DEGREE + DEV_ORD_DEGREE_work + "'";
            		old_DEV_ORD_DEGREE = DEV_ORD_DEGREE_work;
            	}
            }
        	
            //프로시져에 보낼것들 다담는다
        	model.put("custId", custId);
        	model.put("DEV_ORD_DEGREE", DEV_ORD_DEGREE);
        	
            String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
            String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
            
            //session 및 등록정보
        	model.put("LC_ID", LC_ID);
        	model.put("USER_NO", USER_NO);
        	
        	//Das list select
        	List list  = dao.dasIfSendList(model);
        	int listCnt = list.size();
        	
        	if(listCnt > 0){
        		for(int i = 0 ; i < listCnt ; i ++){
        			Map<String, Object> modelIns = new HashMap<String, Object>();
        			modelIns.put("WMS_ORD_ID", (String)((Map<String, String>)list.get(i)).get("WMS_ORD_ID"));
        			modelIns.put("WMS_ORD_SEQ", (String)((Map<String, String>)list.get(i)).get("WMS_ORD_SEQ"));
        			modelIns.put("DAS_ORD_DT", (String)((Map<String, String>)list.get(i)).get("DAS_ORD_DT"));
        			modelIns.put("DAS_ORD_DEGREE", (String)((Map<String, String>)list.get(i)).get("DAS_ORD_DEGREE"));
        			modelIns.put("ITEM_NAME", (String)((Map<String, String>)list.get(i)).get("ITEM_NAME"));
        			modelIns.put("ITEM_CODE", (String)((Map<String, String>)list.get(i)).get("ITEM_CODE"));
        			modelIns.put("ITEM_BAR_CODE", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE"));
        			modelIns.put("ITEM_BAR_CODE2", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE2"));
        			modelIns.put("ITEM_BAR_CODE3", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE3"));
        			modelIns.put("ORD_QTY", (String)((Map<String, String>)list.get(i)).get("ORD_QTY"));
        			modelIns.put("INVC_NO", (String)((Map<String, String>)list.get(i)).get("INVC_NO"));
        			modelIns.put("INVC_ADD_FLAG", (String)((Map<String, String>)list.get(i)).get("INVC_ADD_FLAG"));
        			modelIns.put("ORG_ORD_ID", (String)((Map<String, String>)list.get(i)).get("ORG_ORD_ID"));
        			modelIns.put("RCVR_NM", (String)((Map<String, String>)list.get(i)).get("RCVR_NM"));
        			modelIns.put("RCVR_TEL", (String)((Map<String, String>)list.get(i)).get("RCVR_TEL"));
        			modelIns.put("ZIP_NO", (String)((Map<String, String>)list.get(i)).get("ZIP_NO"));
        			modelIns.put("RCVR_ADDR", (String)((Map<String, String>)list.get(i)).get("RCVR_ADDR"));
        			modelIns.put("RCVR_DETAIL_ADDR", (String)((Map<String, String>)list.get(i)).get("RCVR_DETAIL_ADDR"));
        			modelIns.put("DATA_SENDER_NM", (String)((Map<String, String>)list.get(i)).get("DATA_SENDER_NM"));
        			modelIns.put("LEGACY_ORG_ORD_NO", (String)((Map<String, String>)list.get(i)).get("LEGACY_ORG_ORD_NO"));
        			modelIns.put("P_CLNTNUM", (String)((Map<String, String>)list.get(i)).get("P_CLNTNUM"));
        			modelIns.put("P_CLNTMGMCUSTCD", (String)((Map<String, String>)list.get(i)).get("P_CLNTMGMCUSTCD"));
        			modelIns.put("P_PRNGDIVCD", (String)((Map<String, String>)list.get(i)).get("P_PRNGDIVCD"));
        			modelIns.put("P_CGOSTS", (String)((Map<String, String>)list.get(i)).get("P_CGOSTS"));
        			modelIns.put("P_ADDRESS", (String)((Map<String, String>)list.get(i)).get("P_ADDRESS"));
        			modelIns.put("P_ZIPNUM", (String)((Map<String, String>)list.get(i)).get("P_ZIPNUM"));
        			modelIns.put("P_ZIPID", (String)((Map<String, String>)list.get(i)).get("P_ZIPID"));
        			modelIns.put("P_OLDADDRESS", (String)((Map<String, String>)list.get(i)).get("P_OLDADDRESS"));
        			modelIns.put("P_OLDADDRESSDTL", (String)((Map<String, String>)list.get(i)).get("P_OLDADDRESSDTL"));
        			modelIns.put("P_NEWADDRESS", (String)((Map<String, String>)list.get(i)).get("P_NEWADDRESS"));
        			modelIns.put("P_NESADDRESSDTL", (String)((Map<String, String>)list.get(i)).get("P_NESADDRESSDTL"));
        			modelIns.put("P_ETCADDR", (String)((Map<String, String>)list.get(i)).get("P_ETCADDR"));
        			modelIns.put("P_SHORTADDR", (String)((Map<String, String>)list.get(i)).get("P_SHORTADDR"));
        			modelIns.put("P_CLSFADDR", (String)((Map<String, String>)list.get(i)).get("P_CLSFADDR"));
        			modelIns.put("P_CLLDLVBRANCD", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVBRANCD"));
        			modelIns.put("P_CLLDLVBRANNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVBRANNM"));
        			modelIns.put("P_CLLDLCBRANSHORTNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLCBRANSHORTNM"));
        			modelIns.put("P_CLLDLVEMPNUM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNUM"));
        			modelIns.put("P_CLLDLVEMPNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNM"));
        			modelIns.put("P_CLLDLVEMPNICKNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNICKNM"));
        			modelIns.put("P_CLSFCD", (String)((Map<String, String>)list.get(i)).get("P_CLSFCD"));
        			modelIns.put("P_CLSFNM", (String)((Map<String, String>)list.get(i)).get("P_CLSFNM"));
        			modelIns.put("P_SUBCLSFCD", (String)((Map<String, String>)list.get(i)).get("P_SUBCLSFCD"));
        			modelIns.put("P_RSPSDIV", (String)((Map<String, String>)list.get(i)).get("P_RSPSDIV"));
        			modelIns.put("P_NEWADDRYN", (String)((Map<String, String>)list.get(i)).get("P_NEWADDRYN"));
        			modelIns.put("P_ERRORCD", (String)((Map<String, String>)list.get(i)).get("P_ERRORCD"));
        			modelIns.put("P_ERRORMSG", (String)((Map<String, String>)list.get(i)).get("P_ERRORMSG"));
        			modelIns.put("PRINT_THIS_NO", (String)((Map<String, String>)list.get(i)).get("PRINT_THIS_NO"));
        			modelIns.put("PRINT_CNT", (String)((Map<String, String>)list.get(i)).get("PRINT_CNT"));
        			modelIns.put("PRINT_LAST_DATE", (String)((Map<String, String>)list.get(i)).get("PRINT_LAST_DATE"));
        			modelIns.put("PRINT_LAST_NAME", (String)((Map<String, String>)list.get(i)).get("PRINT_LAST_NAME"));
        			modelIns.put("INPUT_ORD_SEQ", (String)((Map<String, String>)list.get(i)).get("INPUT_ORD_SEQ"));
        			modelIns.put("PARCEL_ORD_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_ORD_TY"));
        			modelIns.put("PARCEL_PAY_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_PAY_TY"));
        			modelIns.put("PARCEL_BOX_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_BOX_TY"));
        			modelIns.put("PARCEL_ETC_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_ETC_TY"));
        			modelIns.put("PARCEL_COM_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_COM_TY"));        			
        			modelIns.put("PARCEL_COM_TY_SEQ", (String)((Map<String, String>)list.get(i)).get("PARCEL_COM_TY_SEQ"));
        			modelIns.put("CUST_USE_NO", (String)((Map<String, String>)list.get(i)).get("CUST_USE_NO"));
        			modelIns.put("ORG_INVC_NO", (String)((Map<String, String>)list.get(i)).get("ORG_INVC_NO"));
        			modelIns.put("DEL_YN", (String)((Map<String, String>)list.get(i)).get("DEL_YN"));
        			modelIns.put("REG_DT", (String)((Map<String, String>)list.get(i)).get("REG_DT"));
        			modelIns.put("REG_NO", (String)((Map<String, String>)list.get(i)).get("REG_NO"));
        			modelIns.put("DLV_MSG1", (String)((Map<String, String>)list.get(i)).get("DLV_MSG1"));
        			modelIns.put("SYS_COMPLETE_FLAG", (String)((Map<String, String>)list.get(i)).get("SYS_COMPLETE_FLAG"));
        			
        			//Das i/f table insert 
        			dao640.deviceIfSendInsert(modelIns);
        		}
        	}
        	Date nowDate = new Date();
    		SimpleDateFormat  dateFormat = new  SimpleDateFormat("yyyy-MM-dd");
    		String strNowDate = dateFormat.format(nowDate); 
			String jsonInputHis = "생성일자 : "+strNowDate+" , 생성 ROW : "+listCnt;
			
        	//i/f his table insert 
        	Map<String, Object> modelHis = new HashMap<String, Object>();
        	modelHis.put("LC_ID"			, "0000003200");
        	modelHis.put("CUST_ID"			, "0000295694");
        	modelHis.put("INTERFACE_CODE"	, "TASK_DAS_REQ"); 
			modelHis.put("INTERFACE_ETC1"	, "조건처리"); 
			modelHis.put("gubunHis1"		, "PASS"); 
			modelHis.put("INTERFACE_ETC2"	, jsonInputHis); 
			modelHis.put("INTERFACE_ETC3"	, "200"); 
			modelHis.put("USER_NO"			, model.get(ConstantIF.SS_USER_NO)); 
			//카카오 VX 인터페이스 수동처리 이력 생성
			dao610.insert(modelHis);
        	
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
        	m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
	@Override
    public Map<String, Object> updatePickingTotalKakao(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
        try{
        	List list = dao.devOrdDegreeOrdSeqList(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
	            String[] ordId = new String[tmpCnt];
	            String[] ordSeq = new String[tmpCnt];
	            for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");

                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
            		if(Integer.parseInt(workStat) < 230){
            			String msg = "피킹리스트발행시로케이션지정은필수입니다";
    	        		throw new BizException(msg);
            		}
                }
	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();

	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);

	            // session 및 등록정보
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	            
	            // dao
	            modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
	            ServiceUtil.isValidReturnCode("WMOP338", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
	}
	
	/**
     * Method ID : parcelRecomDlvWithTempType
     * Method 설명 : 배송 품온타입별 박스 추천 로직
     * 작성자 : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> parcelRecomDlvWithTempType(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try {
            
            List<Map<String,Object>> ordId =  (List)model.get("ordId");
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("LC_ID", (String)model.get("LC_ID"));
            modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
            modelIns.put("ORD_ID", ordId.toArray(new String[ordId.size()]));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.parcelRecomDlvWithTempType(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));

        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage());

        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
	
	 /**
     * Method ID   : searchUnshipped
     * Method 설명    : 올리브영 미발행내역 조회
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> searchUnItem(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
		List list = dao.searchUnItem(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		
    		String InreqDt = (String) mapp.get("IN_REQ_DT");
    		String ItemCd = (String) mapp.get("ITEM_CD");
    		String Ordqtyd = (String)mapp.get("ORD_QTY");
    		String DlvyCd = (String)mapp.get("DLVY_CD");
    		String ItemNm = (String)mapp.get("ITEM_NM");
    		
    		mapp.put("IN_REQ_DT", InreqDt);
    		mapp.put("ITEM_CD", ItemCd);
    		mapp.put("ORD_QTY", Ordqtyd);
    		mapp.put("DLVY_CD", DlvyCd);
    		mapp.put("ITEM_NM", ItemNm);

    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", genericResultSet);
    	
    	return map;
    }
    
    /**
     * 대체 Method ID    : searchZoneId
     * 대체 Method 설명      : 출고추천타입:28 - 존 id (SHP_B2B_01/ SHP_B2C_01) 
     * 작성자                        : yhku  
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> searchZoneId(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIn = new HashMap<String, Object>();
        modelIn.put("SS_SVC_NO"     , model.get(ConstantIF.SS_SVC_NO));     //LC_ID
        modelIn.put("vrZoneNm"     , model.get("vrZoneNm"));     //vrZoneNm
        map.put("ZONE", dao.searchZoneId(modelIn));
        return map;
    }
    
    
    @Override
	public Map<String, Object> saveInitOrders(Map<String, Object> model, Map<String, Object> requestMap) throws BizException, Exception {

		Map<String, Object> m = new HashMap<String, Object>();
		// 1. dto 생성
		model.putAll(requestMap);
		SaveInitOrdersDTO dto = prepareProcedureInput(model, SaveInitOrdersDTO.class);
		
		// 2. not null 컬럼 검증 (필요 시)
		int reqCnt = Integer.parseInt(model.get("listSize").toString());
//		for (int i = 0; i < reqCnt; i++) { // dto 배열필드의 길이. 배열인덱스로 비교
//			// 상품코드
//			if (validateNull(dto.getI_ITEM_CD()[i])) {
//				int rowIdx = Integer.parseInt(dto.getI_KEY()[i]);
//				throw new NotFoundException((rowIdx + 1) + "번째 행: " + NotFoundException.ITEMCD_NOT_FOUND);
//			}
//		}
		
		// 2.5. 기존 프로시저 HashMap을 받아서 일단 변환함.
		Map<String, Object> modelIns = convertDtoToMap(dto);
		// 3. 저장. (실패 시 전체 취소 처리 방식)
		Map<String, Object> result = (Map<String, Object>) dao.saveExcelOrderB2O(modelIns);
		// 4. 검증
		ServiceUtil.isValidReturnCode("WMSOP030SE3", String.valueOf(modelIns.get("O_MSG_CODE")),
				result.get("O_MSG_NAME").toString());
		
		m.put("MSG", MessageResolver.getMessage("save.success"));
		m.put("MSG_ORA", "");
		m.put("errCnt", 0);

		return m;
	}
    
	/**
	 * Method 설명 : 요청데이터 정제 ( map → dto). 요청데이터별 DTO에 매핑할 때, 분기문 추가하고 사용.
	 * 
	 * @author ryu512
	 * @param <T>
	 * @param model
	 * @param dto
	 * @return
	 * @throws NotFoundException, Exception
	 */
	private <T> T prepareProcedureInput(Map<String, Object> model, Class<T> dto) throws NotFoundException, Exception {

		// 요청데이터에 listSize 키값 필요 
		int cnt = Integer.valueOf(model.containsKey("listSize") ? model.get("listSize").toString() : "0");
		if (cnt == 0)
			throw new NotFoundException(NotFoundException.REQUEST_KEY_NOT_FOUND);

		if (dto.equals(SaveInitOrdersDTO.class)) {
			SaveInitOrdersDTO initDto = new SaveInitOrdersDTO(cnt);
			initDto.from(model);

			return dto.cast(initDto);
		} else {
			throw new IllegalArgumentException("지원하지 않는 DTO 타입입니다.");
		}
	}
	
	/**
	 * Method 설명  :: null 들어가면 안되는 컬럼 서비스단에서 파악하기 위한 메서드  
	 * @author ryu512
	 * @param str
	 * @return
	 */
	public boolean validateNull(String str) {
		return str == null || str.equals("");
	}
	
	/**
	 * Method 설명 : dto로 DB에 넘기려다 기존 프로시저 건들지 않기 위해 다시 Map으로 전환하기 위한 메서드
	 * 
	 * @author ryu512
	 * @param dto
	 * @return
	 */
    public Map<String, Object> convertDtoToMap(Object dto) throws IllegalAccessException {
        
    	if (dto == null) {
            throw new IllegalArgumentException("DTO 객체가 null입니다.");
        }

        Map<String, Object> modelIns = new HashMap<>();
        Class<?> clazz = dto.getClass();

        while (clazz != null) { // 부모 클래스 필드까지 포함
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true); // private 필드 접근 가능하도록 설정
                modelIns.put(field.getName(), field.get(dto));
            }
            clazz = clazz.getSuperclass();
        }

        return modelIns;
    }
    
}

