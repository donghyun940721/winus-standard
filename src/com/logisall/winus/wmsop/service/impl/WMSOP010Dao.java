package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP010Dao")
public class WMSOP010Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 입출고현황 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.list", model);
    }
    
    /**
     * Method ID    : listSub
     * Method 설명      : 입출고현황 상세 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.list", model);
    }
    
    /**
     * Method ID    : saveApprove
     * Method 설명      : 입출고현황 승인, 불가
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveApprove(Map<String, Object> model){
        executeUpdate("wmsop010.pk_wmsop010.sp_approve_order", model);
        return model;
    }
    
    /**
     * Method ID    : listExcel
     * Method 설명      : 입출고현황 엑셀용조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listExcel(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.listExcel", model);
    }
    
    /**
     * Method ID    : saveConfirmQc
     * Method 설명      : QC확정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveConfirmQc(Map<String, Object> model){
        executeUpdate("wmsop010.pk_wmsop010.sp_qc_confirm", model);
        return model;
    }
    
    /**
     * Method ID    : inOutOrderCntInit
     * Method �ㅻ�      : 
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object inOutOrderCntInit(Map<String, Object> model) {
        return executeView("wmsop010.inOutOrderCntInit", model);
    }
    
    /**
     * Method ID : custIfInOrd
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object custIfInOrd(Map<String, Object> model) {
        return executeInsert("wmsop010.custIfInOrd", model);
        
    }
    
    /**
     * Method ID : custIfOutOrd
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object custIfOutOrd(Map<String, Object> model) {
        return executeInsert("wmsop010.custIfOutOrd", model);
        
    }
    
    /**
     * Method ID    : tempListInOrd
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet tempListInOrd(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.tempListInOrd", model);
    }
    /**
     * Method ID    : tempListOutOrd
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet tempListOutOrd(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.tempListOutOrd", model);
    }
    
    /*-
     * Method ID : checkExistEmptyOrd
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistEmptyOrd(Map<String, Object> model) {
        return (String)executeView("wmsop010.checkExistEmptyOrd", model);
    }
    /**
	 * Method ID : custIfOutOrdDelete
	 * @param model
	 * @return
	 */
	public Object custIfOutOrdDelete(Map<String, Object> model) {
		return executeDelete("wmsop010.custIfOutOrdDelete", model);
	}
	
	/**
     * Method ID : custIfOutOrdReturn
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object custIfOutOrdReturn(Map<String, Object> model) {
        return executeInsert("wmsop010.custIfOutOrdReturn", model);
        
    }
    
    /*-
     * Method ID : checkExistEmptyOrdReturn
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistEmptyOrdReturn(Map<String, Object> model) {
        return (String)executeView("wmsop010.checkExistEmptyOrdReturn", model);
    }
    /**
	 * Method ID : custIfOutOrdReturnDelete
	 * @param model
	 * @return
	 */
	public Object custIfOutOrdReturnDelete(Map<String, Object> model) {
		return executeDelete("wmsop010.custIfOutOrdReturnDelete", model);
	}
}
