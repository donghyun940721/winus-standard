package com.logisall.winus.wmsop.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP030Dao")
public class WMSOP030Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 출고관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outList", model);
    }
    
    /**
     * Method ID    : listCountDH
     * Method 설명      : 출고관리 조회 작업구분 조회
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public GenericResultSet listCountDH(Map<String, Object> model) {
        return executeQueryWq("wmsop010.listCountDH", model);
    }
    /**
     * Method ID    : listCountOmDH
     * Method 설명      : 출고관리 조회 작업구분 조회
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public GenericResultSet listCountOmDH(Map<String, Object> model) {
        return executeQueryWq("wmsop010.listCountOmDH", model);
    }
    
    /**
     * Method ID    : listByCust
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     */
    public GenericResultSet listByCust(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outByCustList", model);
    }
    
    /**
     * Method ID    : listByCustCJ
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     */
    public GenericResultSet listByCustCJ(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outByCustListCJ", model);
    }
    
    /**
     * Method ID    : listByCustSF
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     */
    public GenericResultSet listByCustSF(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outByCustListSF", model);
    }
    
    /**
     * Method ID    : list(PLT)
     * Method 설명      : 출고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listPlt(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outPoolList", model);
    }
    
    /**
     * Method ID    : inspectionListOlive
     * Method 설명      : 검수내역 조회 (올리브영)
     * 작성자                 : seongjun kwon
     * @param   model
     * @return
     */
    public GenericResultSet inspectionListOlive(Map<String, Object> model) {
        return executeQueryPageWq("wmsop030.inspectionListOlive", model);
    }
    
    /**
     * Method ID    : inspectionList
     * Method 설명      : 검수내역 조회
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public GenericResultSet inspectionList(Map<String, Object> model) {
        return executeQueryPageWq("wmsop030.inspectionList", model);
    }
    
    /**
     * Method ID    : deleteOrder
     * Method 설명      : 출고관리 출고주문 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_delete_order", model);
        return model;
    }
    
    
    /**
     * Method ID    : deleteOrderNcode
     * Method 설명      : 출고관리 화주별OM -> 엔코드 출고주문 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteOrderNcode(Map<String, Object> model){
    	
    	executeUpdate("wmsop030.deleteOrderNcodeOP011", model);
    	
    	Integer count = (Integer) executeQueryForObject("wmsop030.selectNcodeOP011Count", model);
    	if(count == 0){
    		executeUpdate("wmsop030.deleteOrderNcodeOP010", model);	
    	}
        
        executeUpdate("wmsop030.deleteOrderNcodeOM010", model);
        
        return model;
    }
    
    
    
    /**
     * Method ID    : saveOrdAlloc
     * Method 설명      : 할당처리
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOrdAlloc(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_allocate", model);
        return model;
    }
    
    /**
     * Method ID    : saveOrdCancAlloc
     * Method 설명      : 할당취소
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOrdCancAlloc(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_cancel_allocate", model);
        return model;
    }
    
    /**
     * Method ID    : saveOrdPick
     * Method 설명      : 피킹확정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOrdPick(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_picking", model);
        return model;
    }
    
    /**
     * Method ID    : saveCancelPick
     * Method 설명      : 피킹취소
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveCancelPick(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_cancel_picking", model);
        return model;
    }
    
    public Object getPickingWorkId(Map<String, Object> model){
        return executeView("wmsop030.getPickingWorkId", model);
    }
	
    /**
     * Method ID    : saveOutComplete
     * Method 설명      : 출고확정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOutComplete(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_out_complete", model);
        return model;
    }
    
    /**
     * Method ID    : saveOutCompleteOrdMulti
     * Method 설명      : 출고확정(주문별)
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public Object saveOutCompleteOrdMulti(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_out_complete_ord_multi", model);
        return model;
    }
    
    /**
     * Method ID    : saveOutCompleteCreateAcc
     * Method 설명      : 출고확정후 수탁증생성
     * 작성자                 : kimzero
     * @param   model
     * @return
     */
    public Object saveOutCompleteCreateAcc(Map<String, Object> model){
    	executeUpdate("wmsop030.pk_wmsac010.sp_acc_dahwa", model);
    	return model;
    }
    
    /**
     * Method ID    : saveOutCompleteCreateAccEtc
     * Method 설명      : 출고확정후 기타비생성
     * 작성자                 : kimzero
     * @param   model
     * @return
     */
    public Object saveOutCompleteCreateAccEtc(Map<String, Object> model){
    	executeUpdate("wmsop030.pk_wmsac010.sp_acc_dahwa_etc", model);
    	return model;
    }
    
    /**
     * Method ID    : saveOutCompleteCreateAccByOrder
     * Method 설명      : 주문별 수탁증 재생성
     * 작성자                 : 
     * @param   model
     * @return
     */
    public Object saveOutCompleteCreateAccByOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsac010.sp_acc_dahwa_order", model);
        return model;
    }
    
    /**
     * Method ID    : saveOutCompleteCreateAccMenual
     * Method 설명      : 날짜별 수탁증 재생성(메뉴얼)
     * 작성자                 : 
     * @param   model
     * @return
     */
    public Object saveOutCompleteCreateAccMenual(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsac010.sp_acc_dahwa_manual", model);
        return model;
    }
    
    

    /**
     * Method ID    : outUpdateZero
     * Method 설명      : 출고0처리확정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object outUpdateZero(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.outUpdateZero", model);
        return model;
    }
    
    /**
     * Method ID    : cancelOutReReceiving
     * Method 설명      : 출고확정취소 처리 후 자동 재입고
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object cancelOutReReceiving(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_cancel_out_re_receiving", model);
        return model;
    }
    
    /**
     * Method ID    : simpleInO2O
     * Method 설명      : O2O입고
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object simpleInO2O(Map<String, Object> model){
    	executeUpdate("wmsop030.pk_wmsif108.sp_ord_o2o_simple_in", model);
    	return model;
    }

    /**
     * Method ID    : saveSimpleOut
     * Method 설명      : 간편출고
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSimpleOut(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_simple_out", model);
        return model;
    }
    
    

    /**
     * Method ID    : ordCancelWorkApparel
     * Method 설명      : 주문취소
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object ordCancelWorkApparel(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_ord_cancel_work_apparel", model);
        return model;
    }
    
    
    
    /**
     * Method ID    : combineOrder
     * Method 설명      : 발주합치기(대화물류 요청기능)
     * 작성자                 : ykim
     * @param   model
     * @return
     */
    public Object combineOrder(Map<String, Object> model){
    	executeUpdate("wmsop030.pk_wmsop030.sp_combine_order", model);
    	return model;
    }
    
    /*-
     * Method ID : 
     * Method 설명 : 출고 주문 입력 후 간편 출고
     * DATE : 2023-04-40
     * 작성자 : BRAD07
     *
     * @param model
     * @return
     */
    public Object runSpOrdInsSimpleOutSatori(Map<String, Object> model) {
        executeUpdate("wmsop030.pk_wmsop030.sp_ord_ins_simple_out_satori", model);
        return model;
    }   
    
    /**
     * Method ID    : ordWorkHistory
     * Method 설명      : 작업이력조회 (대화물류)
     * 작성자                 : dhkim
     * @param   model
     * @return
     */
//    public Object ordWorkHistory(Map<String, Object> model){    	
//    	return executeQueryForList("wmsop030.ordWorkHistory", model);    	
//    }
    
    public GenericResultSet ordWorkHistory(Map<String, Object> model){
    	return executeQueryPageWq("wmsop030.ordWorkHistory", model);
    }
    
    /**
     * Method ID    : saveExcelOrder
     * Method 설명      : 템플릿 주문 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveExcelOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030a.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    : saveExcelOrderB2C
     * Method 설명      : 템플릿 주문 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2C(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030c.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    	: saveExcelOrderB2T
     * Method 설명       : 템플릿 주문 저장 (pk_wmsop030T)
     * 작성자              : kcr
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2T(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030t.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID	: importExceFileB2C
     * Method 설명      	: 템플릿 주문 저장 (검증 후 제거)
     * 작성자              		: dhkim
     * TO-DO		: 검증 후 제거
     * @param   model
     * @return
     */
    public Object importExceFileB2C(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030nt.sp_insert_template", model);
        return model;
    }
    
    public String getUploadSeqTemplateTable(){
        return (String)executeView("wmsop030.get_upload_seq_from_insert_wmstp_out_temp",null);
    }
    
    
    /**
     * Method ID    : insertTemplateOutOrderTable
     * Method 설명       : 엑셀 템플릿 임시테이블 batch insert
     * 작성자              : schan
     * @param list
     * @return int
     */
    public int insertTemplateOutOrderTable(List list){
        return executeInsertBatch("wmsop030.insert_wmstp_out_temp",list, 5000);
    }
    
    /**
     * Method ID    : insertTemplateInOrderTable
     * Method 설명       : 엑셀 템플릿 임시테이블 batch insert
     * 작성자              : schan
     * @param list
     * @return int
     */
    public int insertTemplateInOrderTable(List list){
        return executeInsertBatch("wmsop030.insert_wmstp_in_temp",list, 5000);
    }
    
    /**
     * Method ID    : saveTableOrderNTTB2C
     * Method 설명       : 임시테이블 주문삽입 
     * 작성자              : schan
     * @param   model
     * @return object
     */
    public Object saveTableOrderNTTB2C(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030ntt.sp_insert_template_b2c", model);
        return model;
    }
    
    /**
     * Method ID    : saveTableOrderNTTInOrder
     * Method 설명       : 임시테이블 주문삽입 
     * 작성자              : schan
     * @param   model
     * @return object
     */
    public Object saveTableOrderNTTInOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030ntt.sp_insert_template_inorder", model);
        return model;
    }
    
    
    /**
     * Method ID    	: saveExcelOrderB2TS
     * Method 설명       : 템플릿 주문 저장 (PK_WMSOP030TS) 주문상세타입 추가 ORD_SUBTYPE
     * 작성자              : sing09
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2TS(Map<String, Object> model){
    	executeUpdate("wmsop030.pk_wmsop030ts.sp_insert_template", model);
    	return model;
    }
    
    /**
     * Method ID        : saveExcelOrderB2TS
     * Method 설명       : 템플릿 주문 저장 후 주문차수명 업데이트
     * 작성자              : schan
     * @param   model
     * @return
     */
    public Object updateOrderDegreeNm(Map<String, Object> model){
        executeUpdate("wmsop030.updateOrderDegreeNm", model);
        return model;
    }
    
    
    
    /**
     * Method ID    	: saveExcelOrderB2O
     * Method 설명       : 템플릿 주문 저장 (pk_wmsop030O)
     * 작성자              : kcr
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2O(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030o.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    : saveExcelOrderB2D
     * Method 설명      : 템플릿 주문 저장 (올푸드용)
     * 작성자                 : seongjun
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2D(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030d.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    : saveExcelOrderSAP
     * Method 설명      : 템플릿 주문 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveExcelOrderSAP(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030s.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    : listRackSearch
     * Method 설명      : 긴급RACK보충  조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listRackSearch(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.listRack", model);
    }
    
    /**
     * Method ID    : autoBestLocSave
     * Method 설명      : 출고관리 출고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object autoBestLocSave(Map<String, Object> model){
//        executeUpdate("wmsop030.pk_wmsop030.sp_auto_location", model);
    	executeUpdate("wmsop001.pk_wmsop001.sp_auto_location", model);
        return model;
    }
    
    /**
     * Method ID    : ordDelSetReordInsert
     * Method 설명      : 주문재등록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object ordDelSetReordInsert(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_ord_del_set_reord_insert", model);
        return model;
    }
    
    /**
     * Method ID    : asnSave
     * Method 설명      : ASN생성버튼
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public Object asnSave(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_re_snd_asn", model);
        return model;
    }
    
    /**
     * Method ID    	: asnSyncItem
     * Method 설명      : ASN 센터 상품 동기화
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public void asnSyncItem(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_asn_sync_lc_item", model);
    }
    
    /**
     * Method ID  : WorkUpdateOrder 
     * Method 설명   : 작업지시 팝업
     * 작성자                 : chsong
     * @param model
     * @return
     */
    public GenericResultSet WorkUpdateOrder(Map<String, Object> model){
        return executeQueryPageWq("wmsop030.WorkUpdateOrder", model);
    }
    
    /**
     * Method ID  : WorkUpdateOrder_Complete 
     * Method 설명   : 작업지시 : 작업완료 팝업
     * 작성자                 : chsong
     * @param model
     * @return
     */
    public GenericResultSet WorkUpdateOrder_Complete(Map<String, Object> model){
        return executeQueryPageWq("wmsop030.WorkUpdateOrder_Complete", model);
    }
    
    /**
	 * Method ID : updateWorkOrder 
	 * Method 설명 : 작업지시 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateWorkOrder(Map<String, Object> model) {
		return executeUpdate("wmsop030.updateWorkOrder", model);
	}
    
    /**
     * Method ID    : changeMapping
     * Method 설명      : 입출고매핑전환
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object changeMapping(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.changeMapping", model);
        return model;
    }
	
	/**
     * Method ID : adminOrderDelete
     * 
     * @param model
     * @return
     */
    public Object adminOrderDelete(Map<String, Object> model) {
	return executeUpdate("wmsop030.adminOrderDelete", model);
    }
    
    /**
     * Method ID  : selectPool
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmsmo907.selectPool", model);
    }
    
    /**
     * Method ID    : outOrderCntInit
     * Method �ㅻ�      : 
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object outOrderCntInit(Map<String, Object> model) {
        return executeView("wmsop030.outOrderCntInit", model);
    }
    
    /**
     * Method ID    : outWorkingCntInit
     * Method �ㅻ�      : 
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object outWorkingCntInit(Map<String, Object> model) {
        return executeView("wmsop030.outWorkingCntInit", model);
    }
    
    /**
     * Method ID  : customerInfo
     * Method 설명  : 고객정보 상세조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object customerInfo(Map<String, Object> model){
        return executeQueryForList("wmsop030.customerInfo", model);
    }
    
    /**
     * Method ID    : ifOutOrd
     * Method 설명      : 출고관리 출고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object ifOutOrd(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsif100.sp_out_insert_order", model);
        return model;
    }
    
    /**
     * Method ID    : saveDlvNo
     * Method 설명      : 템플릿 주문 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveDlvNo(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030e.sp_update_template", model);
        return model;
    }
    
    /**
     * Method ID    : searchTpCol
     * Method 설명      : 화주별 컬럼
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object searchTpCol(Map<String, Object> model){
        executeUpdate("wmscm210.pk_wmscm210.sp_sel_tpcol", model);
        return model;
    }
    /**
     * Method ID    : getTemplateInfo
     * Method 설명      : 미리 등록한 납품처 별 템플릿 정보 가져오기.
     * 작성자                 : ykim
     * @param   model
     * @return
     */
    public List<Map<String, Object>> getTemplateInfo(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsop030.selectTemplateInfo", model);
    }
    
    /**
     * Method ID    : getTemplateInfoALL
     * Method 설명      : 
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public List<Map<String, Object>> getTemplateInfoALL(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsop030.selectTemplateInfoALL", model);
    }
    
    /**
     * Method ID    : getTemplateType
     * Method 설명      : 미리 등록한 납품처 별 템플릿 정보 가져오기.
     * 작성자                 : ykim
     * @param   model
     * @return
     */
    public List<Map<String, Object>> getTemplateType(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsop030.selectTemplateType", model);
    }
    
    /**
     * Method ID    : getTemplateCount
     * Method 설명      : 템플릿 select전 count확인
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public int getTemplateCount(Map<String, Object> model){
        return (int)executeView("wmsop030.selectTemplateTypeWithBalnkCount", model);
    }
    /**
     * Method ID    : getTemplateDefault
     * Method 설명      : default 템플릿 select
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public List<Map<String, Object>> getTemplateDefault(Map<String, Object> model){
        return (List<Map<String, Object>>)executeQueryForList("wmsop030.selectTemplateTypeDefault", model);
    }
    
    /**
     * Method ID    : getTemplateWithBlank
     * Method 설명      : 템플릿 select
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public List<Map<String, Object>> getTemplateWithBlank(Map<String, Object> model){
        return (List<Map<String, Object>>)executeQueryForList("wmsop030.selectTemplateTypeWithBalnk", model);
    }
    
    /**
     * Method ID    : autoDeleteLocSave
     * Method 설명      : 로케이션 지정 삭제
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object autoDeleteLocSave(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop000.sp_del_loc_multi", model);
        return model;
    }
    /**
     * Method ID    : deleteLocOrdMulti
     * Method 설명      : 로케이션 지정 삭제(주문번호별)
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public Object deleteLocOrdMulti(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop000.sp_del_loc_ord_multi", model);
        return model;
    }
    
    /**
     * Method ID    : autoBestLocSaveMulti
     * Method 설명      : 출고관리 출고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object autoBestLocSaveMulti(Map<String, Object> model){
//        executeUpdate("wmsop030.pk_wmsop030.sp_auto_location_multi", model);
        executeUpdate("wmsop001.pk_wmsop001.sp_auto_location_multi", model);
        return model;
    }
    
    
    /**
     * Method ID    : autoBestLocSaveForDPS
     * Method 설명      : DPS 로케이션 추천등록
     * 작성자                : 
     * @param   model
     * @return
     */
    public Object autoBestLocSaveForDPS(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001.sp_auto_location_dps", model);
        return model;
    }
    
    /**
     * Method ID    : changeOrderToDAS
     * Method 설명      : DAS 주문변경
     * 작성자                : 
     * @param   model
     * @return
     */
    public Object changeOrderToDAS(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmswcs010.sp_change_order_to_das", model);
        return model;
    }
    
    /**
     * Method ID    : changeOrderToDASCommon
     * Method 설명      : DAS 주문변경
     * 작성자                : saveOrderToDAS로변경 및 삭제예정(23-10-12)
     * @param   model
     * @return
     */
    public Object changeOrderToDASCommon(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmswcs010.sp_change_order_to_das_common", model);
        return model;
    }
    
    /**
     * Method ID    : cancelOrderToDAS
     * Method 설명      : DAS 주문변경 취소
     * 작성자                : 
     * @param   model
     * @return
     */
    public Object cancelOrderToDAS(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmswcs010.sp_cancel_order_to_das", model);
        return model;
    }
    
    /**
     * Method ID    : saveOrderToDAS
     * Method 설명      : DAS 주문 삽입
     * 작성자                : 
     * @param   model
     * @return
     */
    public Object saveOrderToDAS(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmswcs010.sp_save_order_to_das_if", model);
        return model;
    }
    
    /**
     * Method ID    : deleteOrderToDAS
     * Method 설명      : DAS 주문 삭제
     * 작성자                : 
     * @param   model
     * @return
     */
    public Object deleteOrderToDAS(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmswcs010.sp_delete_order_to_das_if", model);
        return model;
    }
    
    /**
     * Method ID    : autoLocOrdMulti
     * Method 설명      : 대화물류 > 로케이션지정(일괄할당)
	 * 작성자                 : kimzero
     * @param   model
     * @return
     */
    public Object autoLocOrdMulti(Map<String, Object> model){
    	executeUpdate("wmsop001.pk_wmsop001.sp_auto_location_multiV2", model);
    	return model;
    }
    
  	/*-
  	 * Method ID	: bigDataLoc
  	 * Method 설명 	: 로케이션추천지정 (빅데이터)
  	 * 작성자                 	: dhkim
  	 * @param   model
  	 * @return  
  	 */
    public Object bigDataLoc(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop001.sp_bigdata_loc", model);
        return model;
    }
    
    /**
     * Method ID    : autoLocSimple
     * Method 설명    : 로케이션추천지정 (대용량)
     * 작성자                  : schan
     * @param   model
     * @return  
     */
    public Object autoLocSimple(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop001.sp_auto_loc_simple", model);
        return model;
    }
    
    /**
     * Method ID    : autoBestLocSaveMultiV2
     * Method 설명      : 출고관리 출고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object autoBestLocSaveMultiV2(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001.sp_auto_location_multiV2", model);
        return model;
    }
    
    /**
     * Method ID    : autoBestLocSaveMultiPart
     * Method 설명      : 로케이션 추천지정(부분재고)
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public Object autoBestLocSaveMultiPart(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001.sp_auto_location_multi_part", model);
        return model;
    }
    
    
    /**
     * Method ID    : autoLocMulti
     * Method 설명      : 대화물류 > 로케이션지정(일괄할당)
	 * 작성자                 : kimzero
     * @param   model
     * @return
     */
    public Object autoLocMulti(Map<String, Object> model){
    	executeUpdate("wmsop001.pk_wmsop001.sp_auto_location_multi", model);
    	return model;
    }
    
    /**
     * Method ID    : listB2C
     * Method 설명      : 출고관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listExcelB2C(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outListB2C", model);
    }
    
    /**
     * Method ID    : saveCjDataInsert
     * Method 설명      : 할당취소
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveCjDataInsert(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsdf010.sp_parcel_create", model);
        return model;
    }
    
    /**
     * Method ID    : listByCustCJ_setParam
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     */
    public GenericResultSet listByCustCJ_setParam(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.listByCustCJ_setParam", model);
    }
    
    /**
     * Method ID    : listByCustCJ_setParam
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     */
    public GenericResultSet listByCustCJ_ordInvcNoConf(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.listByCustCJ_ordInvcNoConf", model);
    }
    
    /**
     * Method ID    : saveCjConfInsert
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveCjConfInsert(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsdf010.sp_conf_invc_ord", model);
        return model;
    }
    
    /**
     * Method ID    : saveCjConfUpdate
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveCjConfUpdate(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsdf010.sp_conf_invc_ord_update", model);
        return model;
    }
    
    /**
     * Method ID    : cjAddressInformationByValue
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object cjAddressInformationByValue(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsdf010.sp_cj_address_info_by_value", model);
        return model;
    }
    
    /**
     * Method ID    : saveSfDataInsert
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSfDataInsert(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsdf010.sp_sfData_insert", model);
        return model;
    }
    
    /**
     * Method ID    : saveCjReturnInsert
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveCjReturnInsert(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsdf010.sp_return_invc_ord", model);
        return model;
    }
    
    /**
     * Method ID    : saveCjReturnUpdate
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveCjReturnUpdate(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsdf010.sp_return_invc_ord_update", model);
        return model;
    }
    
    public List rawListByCustSummary(Map<String, Object> model) {
		List custs = list("wmsop030.listByCustSummary", model);
    	return custs;
	}
    
	public Object listByCustSummary(Map<String, Object> model) {
		List custs = rawListByCustSummary(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}

	public Object listByCustDetail(Map<String, Object> model) {
		
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop030.listByCustDetail", model));
    	return genericResultSet;
	}

	public Object listCountByCust(Map<String, Object> model) {
		
		return executeView("wmsop030.listCountByCust", model);
	}
	
    /**
     * Method ID    : updatePickingTotal
     * Method 설명      : 토탈피킹리스트발행
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object updatePickingTotal(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_total_print_picking", model);
        return model;
    }
    
    /**
     * Method ID    : updatePickingTotal
     * Method 설명      : 토탈피킹리스트발행
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object updatePickingTotalRobot(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_total_print_picking_robot", model);
        return model;
    }

	public Object batch(Map<String, Object> model) {
		executeUpdate("wmsop030.PK_WMSDV020.SP_DEVICE_BATCH_WORK", model);
        return model;
	}

	public Object listCountByCustNoCount(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop030.listByCustNoCount", model));
    	return genericResultSet;
	}
	
	public GenericResultSet pickingList(Map<String, Object> model) {
		return executeQueryWq("wmsop030.pickingList", model);
		/*
		GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop030.pickingList", model));
    	return genericResultSet;
    	*/
	}
	
	/**
     * Method ID  : outInvalidView
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public Object outInvalidView(Map<String, Object> model){
        return executeQueryForList("wmsop330.outInvalidView", model);
    }
        
    /**
     * Method ID    : confirmPickingTotal
     * Method 설명      : 토탈피킹리스트확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object confirmPickingTotal(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.SP_PICKING", model);
        return model;
    }
    
	/*-
	 * Method ID    : WMSOP030E11
	 * Method 설명   : 운영관리 > 출고관리 > 파일업로드 화면 및 기능
	 * 작성자         : KCR
	 * @param   model
	 * @return  
	 */
    
    public Object fileUpload(Map<String, Object> model) {
    	 return executeInsert("wmsop030.fileInsert", model);
    } 
    
    /*-
     * Method ID    : WMSOP030
     * Method 설명   : 운영관리 > 출고관리 > 작업중 해제
     * 작성자         : MonkeySeok
     * 날짜 : 2021-02-18
     * @param   model
     * @return  
     */
    public Object workingUnlock(Map<String, Object> model){
        executeUpdate("wmsop030.workingUnlock", model);
        return model;
    }   
    
    /*-
	 * Method ID    : totalWorkingUnlock
	 * Method 설명   : 출고관리(올리브영) 헤더 테이블 작업중 해제
	 * 작성자         : Seongjun kwon
	 * 날짜 : 2021-08-12
	 * @param   model
	 * @return  
	 */
    public Object totalWorkingUnlock(Map<String, Object> model){
        executeUpdate("wmsop030.totalWorkingUnlock", model);
        return model;
    }
    
    /**
     * Method ID	: getPickingLog
     * Method 설명	: 피킹리스트 LOG 발행 리스트 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getPickingLog(Map<String, Object> model){
        return executeQueryForList("wmsop030.getPickingLog", model);
    }
    
    /**
     * Method ID	: listByOLive
     * Method 설명	: 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listByOlive(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.listByOlive", model);
    }

	public Object productOliveSoldOut(Map<String, Object> model) {
        return executeUpdate("wmsop030.productOliveSoldOut", model);
	}

	public Object cancelProductOliveSoldOut(Map<String, Object> model) {
        return executeUpdate("wmsop030.cancelProductOliveSoldOut", model);
	}
	
	public int locatedQty(Map<String, Object> model) {
		return (int)executeView("wmsop030.locatedQty", model);
	}

	public List rawListByOliveSummary(Map<String, Object> model) {
		List custs = list("wmsop030.listByOliveSummary", model);
    	return custs;
	}
	
	public List rawListByOliveSummaryAddUuid(Map<String, Object> model) {
        List custs = list("wmsop030.listByOliveSummaryAddUuid", model);
        return custs;
    }
    
	public Object listByOliveSummary(Map<String, Object> model) {
		List custs = rawListByOliveSummary(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}

	public Object listByOliveDetail(Map<String, Object> model) {
		
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop030.listByOliveDetail", model));
    	return genericResultSet;
	}

	public Object listCountByOlive(Map<String, Object> model) {
		return executeView("wmsop030.listCountByOlive", model);
	}

	public GenericResultSet searchAddressOlive(Map<String, Object> model) {
		return executeQueryPageWq("wmsop030.searchAddressOlive", model);
	}

	public Object updateAddressOlive(Map<String, Object> model) {
		return executeUpdate("wmsop030.updateAddressOlive", model);
	}
	public Object insertAddressHistoryOlive(Map<String, Object> model) {
		return executeInsert("wmsop030.insertAddressHistoryOlive", model);
	}
	
	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : 엑셀업로드 저장
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			
			String[] ordId  = new String[list.size()];
            String[] ordSeq = new String[list.size()];
            String[] locCd  = new String[list.size()];
            
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if ( (paramMap.get("I_ORD_ID")  != null && StringUtils.isNotEmpty(paramMap.get("I_ORD_ID").toString()))
				   &&(paramMap.get("I_ORD_SEQ") != null && StringUtils.isNotEmpty(paramMap.get("I_ORD_SEQ").toString()))
				   &&(paramMap.get("I_LOC_CD")  != null && StringUtils.isNotEmpty(paramMap.get("I_LOC_CD").toString())) ) {
					
					ordId[i]  = (String)paramMap.get("I_ORD_ID");
                	ordSeq[i] = (String)paramMap.get("I_ORD_SEQ");
                	locCd[i]  = (String)paramMap.get("I_LOC_CD");
				}
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("ordId" , ordId);
            modelIns.put("ordSeq", ordSeq);
            modelIns.put("locCd" , locCd);

            //session 및 등록정보
            modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            //dao                
            //modelIns = (Map<String, Object>)dao.autoBestLocSave(modelIns);
            executeUpdate("wmsop001_sable7.pk_wmsop001_sable7.sp_excel_out_loc", modelIns);
            //ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/**
	 * Method ID : noneBillingFlag 
	 * Method 설명 :  수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object noneBillingFlag(Map<String, Object> model) {
		return executeUpdate("wmsop030.noneBillingFlag", model);
	}

	/**
	 * Method ID : etcMsgByCategoryNo 
	 * Method 설명 :  수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object etcMsgByCategoryNo(Map<String, Object> model) {
		return executeUpdate("wmsop030.etcMsgByCategoryNo", model);
	}
	
	public Object updateInvalidAddressOliveDHL(Map<String, Object> model) {
		return executeUpdate("wmsop030.updateInvalidAddressOliveDHL", model);
	}
	
	public Object updateInvalidAddressOliveQXP(Map<String, Object> model) {
		return executeUpdate("wmsop030.updateInvalidAddressOliveQXP", model);
	}
	
	 /**
     * Method ID    : listByOm
     * Method 설명    : 화주별 출고관리  조회(OM)
     * 작성자               : KCR
     * @param   model
     * @return
     */
    public GenericResultSet outByListOm(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop010.outByListOm", model);
    }
    
    /**
     * Method ID    : outByListOmDH
     * Method 설명    : 화주별 출고관리  조회(대화물류)
     * 작성자               : kimzero
     * @param   model
     * @return
     */
    public GenericResultSet outByListOmDH(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop010.outByListOmDH", model);
    }
    
    public GenericResultSet outListByDelivery(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outListByDelivery", model);
    }
    
    public GenericResultSet outListByKcc(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outListByKcc", model);
    }
    
    public GenericResultSet listNew(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outListNew", model);
    }
    
    public GenericResultSet asnList(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.asnList", model);
    }
    
    public Object updateOp011Cancel(Map<String, Object> model) {
		return executeUpdate("wmsop030.updateOp011Cancel", model);
	}
    
    public Object updateOp010Cancel(Map<String, Object> model) {
		return executeUpdate("wmsop030.updateOp010Cancel", model);
	}
    
    public List rawlistByHeader(Map<String, Object> model) {
		List custs = list("wmsop030.listByHeader", model);
    	return custs;
	}
    
	public Object listByHeader(Map<String, Object> model) {
		List custs = rawlistByHeader(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	public List rawlistByKccHeader(Map<String, Object> model) {
		List custs = list("wmsop030.listByKccHeader", model);
    	return custs;
	}
 
	
	
	public Object listByKccHeader(Map<String, Object> model) {
		List custs = rawlistByKccHeader(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	public Object listByKccDetail(Map<String, Object> model) {
		
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop030.listByKccDetail", model));
    	return genericResultSet;
	}
	
	public Object listByKccCount(Map<String, Object> model) {
		return executeView("wmsop030.listByKccCount", model);
	}
	
	 /**
     * Method ID    : outByListOmExcel
     * Method 설명    : 화주별 출고관리  조회(OM) 엑셀용
     * 작성자               : KSJ
     * @param   model
     * @return
     */
    public GenericResultSet outByListOmExcel(Map<String, Object> model) {
		return executeQueryPageWq("wmsop010.outByListOmExcel", model);
    }
    
	 /**
     * Method ID    : outByListOmExcel
     * Method 설명    : 화주별 출고관리  조회(OM) 엑셀용
     * 작성자               : KSJ
     * @param   model
     * @return
     */
    public GenericResultSet outByListOmExcelLingTea(Map<String, Object> model) {
    	// 링티 물류센터일 경우 
		return executeQueryPageWq("wmsop010.outByListOmExcelLingTea", model);
    }
    
    /**
     * Method ID    : nCodeExcelList
     * Method 설명    : 엔코드 실적 내역 다운로드 
     * 작성자               : KSJ
     * @param   model
     * @return
     */
    public GenericResultSet nCodeExcelList(Map<String, Object> model) {
        return executeQueryPageWq("wmsop030.nCodeExcelList", model);
    }
    
    /**
     * Method ID    	   : pickingWorkOrder
     * Method 설명      : 피킹 -> 작업지시
     * 작성자               : KSJ
     * @param   model
     * @return
     */
    public Object pickingWorkOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_picking_work_order", model);
        return model;
    }
    
    public GenericResultSet outListByDeliveryV2(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outListByDeliveryV2", model);
    }
    
    public GenericResultSet listByOliveV2(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.listByOliveV2", model);
    }
    
    /**
     * Method ID	: saveOutInOrdSimpleComplete
     * Method 설명	: 출고입고통합심플저장
     * 작성자			: chsong
     * @param   model
     * @return
     */
    public Object saveOutInOrdSimpleComplete(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsif108.sp_outInOrdSimpleComplete", model);
        return model;
    }
    
    
    /**
     * Method ID	: getCustOrdDegree
     * Method 설명 	: 화주별 주문차수
     * @param model
     * @return
     */
    public Object getCustOrdDegreeList(Map<String, Object> model){
        return executeQueryForList("wmsop030.getCustOrdDegreeList", model);
    }


     /**
     * Method ID	: getCutomerOrderInfo
     * Method 설명 	: 고객 주문 정보 조회
     * @param model
     * @return
     */
    public Object getCutomerOrderInfo(Map<String, Object> model){
        return executeQueryForObject("wmsop030.getCutomerOrderInfo", model);
    }
    
    /**
    * Method ID	: getCutomerOrderInfo
    * Method 설명 	: 고객 주문 정보 조회
    * @param model
    * @return
    */
   public Object getCutomerCsInfo(Map<String, Object> model){
       return executeQueryForObject("wmsas010.listDetail", model);
   }
   
   /**
	 * Method ID 	: custInfoSave 
	 * Method 설명 	: 고객 주문 정보 수정
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object custInfoSave(Map<String, Object> model) {
		return executeUpdate("wmsop030.custInfoSave", model);
	}
	
	/**
     * Method ID    : outOrdCancelReset
     * Method 설명      : 출고확정취소 처리, 자동 재입고 후 해당 주문 RESET
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object outOrdCancelReset(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop031.sp_out_ord_cancel_reset", model);
        return model;
    }
    
    public Object getCancelWorkStat(Map<String, Object> model){
        return executeView("wmsop030.getCancelWorkStat", model);
    }
    
    /**
     * Method ID    	: getTemplateInfoV2
     * Method 설명      : 템플릿 설정 정보 조회(공란적용)
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public List<Map<String, Object>> getTemplateInfoV2(Map<String, Object> model){
    	return (List<Map<String, Object>>)executeQueryForList("wmsop030.getTemplateInfoV2", model);
    }
    
    /**
     * Method ID    	: saveExcelOrder_AS
     * Method 설명      : 템플릿 주문 저장(PK_WMSOP030AS.SP_INSERT_TEMPLATE)
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object saveExcelOrder_AS(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030as.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    	: combineOrdDegree_OP
     * Method 설명      : 주문차수 합치기 OP010
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object combineOrdDegree_OP(Map<String, Object> model){
    	executeUpdate("wmsop030.combineOrdDegree_op010", model);
    	return model;
    }
    
    /**
     * Method ID    	: combineOrdDegree_OM
     * Method 설명      : 주문차수 합치기 OM010
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object combineOrdDegree_OM(Map<String, Object> model){
    	executeUpdate("wmsop030.combineOrdDegree_om010", model);
    	return model;
    }
    
    /**
     * Method ID		: getNewOrdDegree
     * Method 설명		: 주문차수 가져오기 (자동생성)
     * 작성자           : KSJ
     * @param model
     * @return
     */
    public String getNewOrdDegree(Map<String, Object> model){
        return (String) executeQueryForObject("wmsop030.getNewOrdDegree", model);
    }
    
    /**
     * Method ID    	: decomposeOrdDegree_OP
     * Method 설명      : 주문차수 분해 OP010
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object decomposeOrdDegree_OP(Map<String, Object> model){
    	executeUpdate("wmsop030.decomposeOrdDegree_op010", model);
    	return model;
    }
    
    /**
     * Method ID    	: decomposeOrdDegree_OM
     * Method 설명      : 주문차수 분해 OM010
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object decomposeOrdDegree_OM(Map<String, Object> model){
    	executeUpdate("wmsop030.decomposeOrdDegree_om010", model);
    	return model;
    }

      /**
     * Method ID    	: saveExcelOrderB2C_TS
     * Method 설명      : 템플릿 주문 저장(PK_WMSOP030TS.SP_INSERT_TEMPLATE)
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2C_TS(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030ts.sp_insert_template", model);
        return model;
    }
    
    /**
     * Method ID    : saveExcelOrderOP
     * Method 설명      : OM > OP생성
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object saveExcelOrderOP(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030e.sp_insert_template", model);
        return model;
    }

     /**
     * Method ID    : spSaveBoxRecom
     * Method 설명  : 박스추천 프로시저 실행 원주문번호 기준
     * 작성자       : sing09
     * @param   model
     * @return
     */
    public Object spSaveBoxRecom(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsif108.sp_sabangnet_save_callback_step2", model);
        return model;
    }
    
    /*-
     * Method ID    : selectHistoryInsert
     * Method 설명    : 피킹리스트 발행전 UUID 데이터저장 및 히스토리
     * 작성자          : ysi, schan(수정)
     * @param model
     * @return  
     */
    public Object selectPickingListHistory(Map<String, Object> model){
        executeUpdate("wmsop030.selectPickingListHistory", model);
        return model;
    }
    
    /**
     * Method ID : insertPickingListLog(핑거스캔전 리스트출력)
     * Method 설명 : 핑거스캔전 리스트LOG 저장
     * 작성자 : LEE
     * @param model
     * @return
     * @throws Exception
     */
    public Object spInsertPickingListLog(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop031_temp_1.sp_insert_picking_list_log", model);
        return model;
    }
    
    /**
     * Method ID : saveExcelOrderOlive(박스번호맵핑)
     * Method 설명 : 박스번호맵핑 EXCEL 업로드
     * 작성자 : YSI
     * @param model
     * @return
     * @throws Exception
     */
    public Object saveExcelOrderOlive(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop031_temp_1.sp_insert_boxNumber", model);
        return model;
    }
    
    public Object dasmake(Map<String, Object> model){
        executeUpdate("wmsop030.dasmake", model);
        return model;
    }

    public Object dasDelete(Map<String, Object> model){
        executeUpdate("wmsop030.dasDelete", model);
        return model;
    }

    public Object dasDelete_if(Map<String, Object> model){
        executeUpdate("wmsop030.dasDelete_if", model);
        return model;
    }

    public List dasIfSendList(Map<String, Object> model) {
    	List rstList = list("wmsop030.dasIfSendList", model);
    	return rstList;
    }
    
    public List devOrdDegreeOrdSeqList(Map<String, Object> model) {
		List custs = list("wmsop030.devOrdDegreeOrdSeqList", model);
    	return custs;
	}
    
    public Object parcelRecomDlvWithTempType(Map<String, Object> model) {
        executeUpdate("wmsop030.pk_wmsop030ntt.sp_parcel_recom_dlv_with_temp_type", model);
        return model;
    }
    
    /*
    * Method ID : searchUnshipped
    * Method 설명 : 올리브영 미입고 내역 조회
    * 작성자 : brad07
    * @param model
    * @return
    * @throws Exception
    */
	public List searchUnItem(Map<String, Object> model) {
		List list = executeQueryForList("wmsop030.searchUnItem", model);;
		return list;
	}
	
	public Object searchZoneId(Map<String, Object> model){
	    return executeQueryForList("wmsop030.searchZoneId", model);
	}
}
