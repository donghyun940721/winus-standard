package com.logisall.winus.wmsop.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
public class WMSOP630Dao extends SqlMapAbstractDAO{
	private final Log log = LogFactory.getLog(this.getClass());
	private final int BATCH_SIZE = 500;
	
	@Autowired
	private SqlMapClient sqlMapClient;
	
	public void insertAiPickingOrder(List<Map<String, Object>> outOrderList){
		String[] statementNames = {
				"wmsop030.selectPickingListHistory",
				"wmsop630.insertAiPickingOrder"
				};
		List[] dataLists = new List[]{
				outOrderList,
				outOrderList
			};
		executeInsertBatch(statementNames, dataLists, BATCH_SIZE);
	}
	
//	public void insertPickingLog(List<Map<String, Object>> outOrderList){
//		executeInsertBatch("wmsop030.selectPickingListHistory", outOrderList, BATCH_SIZE);
//	}
	 
	
}
