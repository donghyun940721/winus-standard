package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.impl.WMSIF610Dao;
import com.logisall.winus.wmsop.service.WMSOP640Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
/*SF*/

@Service("WMSOP640Service")
public class WMSOP640ServiceImpl implements WMSOP640Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP640Dao")
    private WMSOP640Dao dao;
    
	@Resource(name = "WMSIF610Dao")
	private WMSIF610Dao dao610;
    
    /**
     * Method ID	: selectItemGrp
     * Method 설명	: 출고관리 화면에서 필요한 데이터
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID	: listByDlvSummary
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByDlvSummary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByDlvSummary(model));
        return map;
    }
    
    /**
     * Method ID	: listByDlvSummaryDiv
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByDlvSummaryDiv(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByDlvSummaryDiv(model));
        return map;
    }
    
    /**
     * Method ID	: list2ByDlvHistory
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2ByDlvHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.list2ByDlvHistory(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: DlvShipment
     * 대체 Method 설명	: 택배접수
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipment(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	//송장 추가발행 시 기 발행 송장번호 삭제 로직은 배제한다.
        	if("N".equals(model.get("ADD_FLAG").toString())){
    			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            		//체크박스 전체에서 송장 접수를 했을 때 이미 송장번호가 있으면 CJ접수 시 WMSDF110 테이블의 이력 삭제 선행 처리 (DEL_YN = Y)
            		try{
                		if (model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
                			Map<String, Object> modelDel = new HashMap<String, Object>();
                			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
                			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
                			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
                			modelDel.put("DLV_COMP_CD"	, "04");
                            
                            dao.wmsdf110DelYnUpdate(modelDel);
                		}
                    } catch(Exception e){
                        throw e;
                    }
    			}
        	}
        	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                // 분리접수 구분
            	String sUrl = "";
                if("NORMAL".equals(model.get("DIV_FLAG").toString())){
                	//String sUrl = "https://172.31.10.253:5201/PARCEL/CJLOGIS/ORDER"; //개발
                    sUrl = "https://52.79.206.98:5201/PARCEL/CJLOGIS/ORDER"; //운영
                }else if("DIV".equals(model.get("DIV_FLAG").toString())){
                	//String sUrl = "https://172.31.10.253:5201/PARCEL/CJLOGIS/ORDER_DIV"; //개발
                    sUrl = "https://52.79.206.98:5201/PARCEL/CJLOGIS/ORDER_DIV"; //운영
                }
                
            	try{
            		//1)N:일반, 2)R:일반이지만 단포검수 추가송장만, 3)Y:추가송장버튼
                	if("N".equals(model.get("ADD_FLAG").toString())){
            			//일반송장발행 시
                		//PROC_FLAG = 1 일 경우만  : (고객명/주소/전화번호가 동일 한 1row만 처리) 
                    	if("1".equals(model.get("PROC_FLAG"+i))){                  		
                        	//Json Data
                			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
        				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
        				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
                									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
                									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
                									+					",\"PARCEL_BOX_TY\":\""		+model.get("PARCEL_BOX_TY").toString()+"\""
                									+					",\"PARCEL_ETC_TY\":\""		+model.get("PARCEL_ETC_TY").toString()+"\""
                									+					",\"FR_DATE\":\""			+model.get("FR_DATE").toString()+"\""
                									+					",\"TO_DATE\":\""			+model.get("TO_DATE").toString()+"\""
                									+					",\"ORD_DEGREE\":\""		+model.get("ORD_DEGREE").toString()+"\""
                									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
                									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
                									+					",\"ADD_NROW\":\""			+"-1"+"\""
                									
                									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
                									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
                									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
                									+					",\"RCVR_ADDR\":\""			+(String)model.get("RCVR_ADDR"+i)+"\""
                									+					",\"RCVR_DETAIL_ADDR\":\""	+(String)model.get("RCVR_DETAIL_ADDR"+i)+"\""
                									+					",\"ORD_ID\":\""			+(String)model.get("ORD_ID"+i)+"\""
                									+					",\"ORD_SEQ\":\""			+(String)model.get("ORD_SEQ"+i)+"\""
                									+					",\"DATA_SENDER_NM\":\""	+(String)model.get("DATA_SENDER_NM"+i)+"\"}}";
                			//System.out.println(jsonInputString);
                			
                        	//ssl disable
                        	disableSslVerification();
                	        //System.out.println("sUrl : " + sUrl);
                	        
                	        URL url = null; 
                	        url = new URL(sUrl);
                	        
                	        HttpsURLConnection con = null;
                        	con = (HttpsURLConnection) url.openConnection();
                        	
                        	//웹페이지 로그인 권한 적용
                        	String userpass = "eaiuser01" + ":" + "eaiuser01";
                        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

                        	con.setRequestProperty ("Authorization", basicAuth);
                        	con.setDoInput(true);
                        	con.setDoOutput(true);
                        	con.setRequestMethod("POST");
                        	con.setConnectTimeout(0);
                        	con.setReadTimeout(0);
                        	con.setRequestProperty("Content-Type", "application/json");
                			con.setRequestProperty("Accept", "application/json");
                			
                			//JSON 보내는 Output stream
                	        try(OutputStream os = con.getOutputStream()) {
                	            byte[] input = jsonInputString.getBytes("utf-8");
                	            os.write(input, 0, input.length);
                	        }
                	        
                	        //Response data 받는 부분
                	        
                	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                	            StringBuilder response = new StringBuilder();
                	            String responseLine = null;
                	            while ((responseLine = br.readLine()) != null) {
                	                response.append(responseLine.trim());
                	            }
                	            
                	            //System.out.println(response.toString());
                	            
                	            JSONObject jsonData = new JSONObject(response.toString());
                	            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
                	            m.put("header"	, docResponse.get("header")); // Y, E
                	            m.put("message"	, docResponse.get("message"));// msg
                	            
                                //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
                                //m.put("rst"	, response.toString());
                	        }
                        	con.disconnect();
                    	}
                		
                	}else if("Y".equals(model.get("ADD_FLAG").toString())){
                		//추가송장발행 시
                    	//Json Data
            			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
    				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
    				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
            									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
            									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
            									+					",\"PARCEL_BOX_TY\":\""		+model.get("PARCEL_BOX_TY").toString()+"\""
            									+					",\"PARCEL_ETC_TY\":\""		+model.get("PARCEL_ETC_TY").toString()+"\""
            									+					",\"FR_DATE\":\""			+model.get("FR_DATE").toString()+"\""
            									+					",\"TO_DATE\":\""			+model.get("TO_DATE").toString()+"\""
            									+					",\"ORD_DEGREE\":\""		+model.get("ORD_DEGREE").toString()+"\""
            									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
            									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
            									+					",\"ADD_NROW\":\""			+"-1"+"\""
            									
            									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
            									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
            									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
            									+					",\"RCVR_ADDR\":\""			+(String)model.get("RCVR_ADDR"+i)+"\""
            									+					",\"RCVR_DETAIL_ADDR\":\""	+(String)model.get("RCVR_DETAIL_ADDR"+i)+"\""
            									+					",\"ORD_ID\":\""			+(String)model.get("ORD_ID"+i)+"\""
            									+					",\"ORD_SEQ\":\""			+(String)model.get("ORD_SEQ"+i)+"\""
            									+					",\"DATA_SENDER_NM\":\""	+(String)model.get("DATA_SENDER_NM"+i)+"\"}}";
            			//System.out.println(jsonInputString);
            			
                    	//ssl disable
                    	disableSslVerification();
            	        //System.out.println("sUrl : " + sUrl);
            	        
            	        URL url = null; 
            	        url = new URL(sUrl);
            	        
            	        HttpsURLConnection con = null;
                    	con = (HttpsURLConnection) url.openConnection();
                    	
                    	//웹페이지 로그인 권한 적용
                    	String userpass = "eaiuser01" + ":" + "eaiuser01";
                    	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

                    	con.setRequestProperty ("Authorization", basicAuth);
                    	con.setDoInput(true);
                    	con.setDoOutput(true);
                    	con.setRequestMethod("POST");
                    	con.setConnectTimeout(0);
                    	con.setReadTimeout(0);
                    	con.setRequestProperty("Content-Type", "application/json");
            			con.setRequestProperty("Accept", "application/json");
            			
            			//JSON 보내는 Output stream
            	        try(OutputStream os = con.getOutputStream()) {
            	            byte[] input = jsonInputString.getBytes("utf-8");
            	            os.write(input, 0, input.length);
            	        }
            	        
            	        //Response data 받는 부분
            	        
            	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            	            StringBuilder response = new StringBuilder();
            	            String responseLine = null;
            	            while ((responseLine = br.readLine()) != null) {
            	                response.append(responseLine.trim());
            	            }
            	            
            	            //System.out.println(response.toString());
            	            
            	            JSONObject jsonData = new JSONObject(response.toString());
            	            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
            	            m.put("header"	, docResponse.get("header")); // Y, E
            	            m.put("message"	, docResponse.get("message"));// msg
            	            
                            //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
                            //m.put("rst"	, response.toString());
            	        }
                    	con.disconnect();
                    	
                	}else if("R".equals(model.get("ADD_FLAG").toString())){
                		//일반 주문의 경우 상품마스터 기준 상품관리TYPE이 개체 인 상품은 주문수량만큼 송장 지동 추가발행
                		//TYPE_ST : 상품정보관리의 상품관리TYPE 이 S 인것 만
                		if("S".equals(model.get("TYPE_ST"+i))){
                			//상품별 주문수량만큼 송장 지동 추가발행
                			for(int j = 0 ; j < Integer.parseInt((String)model.get("ORD_QTY"+i)) ; j ++){
                            	//Json Data
                    			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
            				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
            				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
                    									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
                    									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
                    									+					",\"PARCEL_BOX_TY\":\""		+model.get("PARCEL_BOX_TY").toString()+"\""
                    									+					",\"PARCEL_ETC_TY\":\""		+model.get("PARCEL_ETC_TY").toString()+"\""
                    									+					",\"FR_DATE\":\""			+model.get("FR_DATE").toString()+"\""
                    									+					",\"TO_DATE\":\""			+model.get("TO_DATE").toString()+"\""
                    									+					",\"ORD_DEGREE\":\""		+model.get("ORD_DEGREE").toString()+"\""
                    									+					",\"ADD_FLAG\":\""			+"Y"+"\""
                    									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
                    									+					",\"ADD_NROW\":\""			+Integer.toString(j+1)+"/"+model.get("ORD_QTY"+i)+"\""
                    									
                    									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
                    									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
                    									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
                    									+					",\"RCVR_ADDR\":\""			+(String)model.get("RCVR_ADDR"+i)+"\""
                    									+					",\"RCVR_DETAIL_ADDR\":\""	+(String)model.get("RCVR_DETAIL_ADDR"+i)+"\""
                    									+					",\"ORD_ID\":\""			+(String)model.get("ORD_ID"+i)+"\""
                    									+					",\"ORD_SEQ\":\""			+(String)model.get("ORD_SEQ"+i)+"\""
                    									+					",\"DATA_SENDER_NM\":\""	+(String)model.get("DATA_SENDER_NM"+i)+"\"}}";
                    			//System.out.println(jsonInputString);
                    			
                            	//ssl disable
                            	disableSslVerification();
                    	        //System.out.println("sUrl : " + sUrl);
                    	        
                    	        URL url = null; 
                    	        url = new URL(sUrl);
                    	        
                    	        HttpsURLConnection con = null;
                            	con = (HttpsURLConnection) url.openConnection();
                            	
                            	//웹페이지 로그인 권한 적용
                            	String userpass = "eaiuser01" + ":" + "eaiuser01";
                            	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

                            	con.setRequestProperty ("Authorization", basicAuth);
                            	con.setDoInput(true);
                            	con.setDoOutput(true);
                            	con.setRequestMethod("POST");
                            	con.setConnectTimeout(0);
                            	con.setReadTimeout(0);
                            	con.setRequestProperty("Content-Type", "application/json");
                    			con.setRequestProperty("Accept", "application/json");
                    			
                    			//JSON 보내는 Output stream
                    	        try(OutputStream os = con.getOutputStream()) {
                    	            byte[] input = jsonInputString.getBytes("utf-8");
                    	            os.write(input, 0, input.length);
                    	        }
                    	        
                    	        //Response data 받는 부분
                    	        
                    	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    	            StringBuilder response = new StringBuilder();
                    	            String responseLine = null;
                    	            while ((responseLine = br.readLine()) != null) {
                    	                response.append(responseLine.trim());
                    	            }
                    	            
                    	            //System.out.println(response.toString());
                    	            
                    	            JSONObject jsonData = new JSONObject(response.toString());
                    	            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
                    	            m.put("header"	, docResponse.get("header")); // Y, E
                    	            m.put("message"	, docResponse.get("message"));// msg
                    	            
                                    //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
                                    //m.put("rst"	, response.toString());
                    	        }
                            	con.disconnect();
                    		}
                		}else{
                			//주문중에 상품관리TYPE 이 S 없을 경우 성공처리
                			m.put("header"	, "Y"); // Y, E
            	            m.put("message"	, "[성공] 접수완료.");// msg
                		}
                	}
                } catch(Exception e){
                    throw e;
                }
            }
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID	: disableSslVerification
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	/**
     * Method ID : saveCsv
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.insertCsv(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
    
    /*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> dlvSenderInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DLV_SENDER_INFO", dao.dlvSenderInfo(model));
		return map;
	}
    
    /**
     * 
     * 대체 Method ID		: boxNoUpdate
     * 대체 Method 설명		: boxNoUpdate코드 저장
     * 작성자				: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> boxNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("ORD_ID"    	, model.get("ORD_ID"));
            modelDt.put("ORD_SEQ"    	, model.get("ORD_SEQ"));
            modelDt.put("TRACKING_NO"	, model.get("TRACKING_NO"));
            
            dao.boxNoUpdate(modelDt);
            m.put("errCnt", "0");
            
        } catch(Exception e){
        	m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    public Map<String, Object> dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelSP = new HashMap<String, Object>();
        
        String getTimeStamp = dao.getOracleSysTimeStamp(modelSP);
		if (getTimeStamp != null && !StringUtils.isEmpty(getTimeStamp)) {
			try{
				int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
                String[] ordId  		= new String[tmpCnt];                
                String[] ordSeq 		= new String[tmpCnt];
                String[] invcNo 		= new String[tmpCnt];
                
	            for(int i = 0 ; i < tmpCnt ; i ++){
	            	ordId[i]    	= (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   	= (String)model.get("ORD_SEQ"+i);
                    invcNo[i]   	= (String)model.get("INVC_NO"+i);
	            }
	            
	            //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId"		, ordId);
                modelIns.put("ordSeq"		, ordSeq);
                modelIns.put("invcNo"		, invcNo);
                modelIns.put("printThisNo"	, getTimeStamp);
                modelIns.put("LC_ID"		, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.dlvPrintPoiNoUpdate(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP640", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                
	            m.put("MSG"			, MessageResolver.getMessage("save.success"));
	            m.put("POI_NO_YN"	, "Y");
	            m.put("POI_NO"		, getTimeStamp);
	            
	        } catch(Exception e){
	        	m.put("MSG"			, MessageResolver.getMessage("save.error"));
	            m.put("POI_NO_YN"	, "N");
	            m.put("POI_NO"		, "");
	            throw e;
	        }
        }
        return m;
    }
    
    

    @Override
    public Map<String, Object> invcNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                //WHERE 조건
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                
                modelDt.put("ORD_ID"    	  , model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"     	  , model.get("ORD_SEQ"+i));
                modelDt.put("DLV_COMP_CD"     , model.get("DLV_COMP_CD"+i));
                modelDt.put("ORG_INVC_NO"     , model.get("ORG_INVC_NO"+i));
                
                //UPDATE 조건
                modelDt.put("INVC_NO"     	  , model.get("INVC_NO"+i));
                
                
//                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS100);
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.invcNoUpdate(modelDt);                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
	 * Method ID	: getCustOrdDegree
	 * Method 설명	: 화주별 주문차수
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_ORD_DEGREE", dao.getCustOrdDegree(model));
		return map;
	}
    
    @Override
    public Map<String, Object> listByDlvSummaryExcel(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
		model.put("pageIndex", "1");
		model.put("pageSize", "60000");
		map.put("LIST", dao.listByDlvSummaryExcel(model));
		return map;
    }
    
    /**
     * 
     * 대체 Method ID	: DlvShipDelete
     * 대체 Method 설명	: 택배접수
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipDelete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	//송장 추가발행 시 기 발행 송장번호 삭제 로직은 배제한다.
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
        		//체크박스 전체에서 송장 접수를 했을 때 이미 송장번호가 있으면 CJ접수 시 WMSDF110 테이블의 이력 삭제 선행 처리 (DEL_YN = Y)
        		try{
            		if (model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
            			Map<String, Object> modelDel = new HashMap<String, Object>();
            			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
            			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
            			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
            			modelDel.put("DLV_COMP_CD"	, "04");
                        
                        dao.wmsdf110DelYnUpdate(modelDel);
            		}
                } catch(Exception e){
                    throw e;
                }
            }
    		
    		m.put("header"	, "Y"); // Y, E
            m.put("message"	, "[성공] 삭제완료.");// msg
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
	 * Method ID	: getCustInfo
	 * Method 설명	: 고객정보별 원주문번호 조회
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_ORG_ORD_NO", dao.getCustInfo(model));
		return map;
	}
    
    /**
     * 
     * 대체 Method ID		: custInfoUpdate
     * 대체 Method 설명		: custInfoUpdate
     * 작성자				: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custInfoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	Map<String, Object> modelDt = new HashMap<String, Object>();
        	modelDt.put("vrSrchCustId"    	, model.get("vrSrchCustId"));
            modelDt.put("custInfoName"    	, model.get("custInfoName"));
            modelDt.put("custInfoTelNo"    	, model.get("custInfoTelNo"));
            modelDt.put("custInfoAddr1"    	, model.get("custInfoAddr1"));
            modelDt.put("custInfoAddr2"    	, model.get("custInfoAddr2"));
            modelDt.put("custInfoZipNo"    	, model.get("custInfoZipNo"));
            modelDt.put("custInfoOrgOrdIds" , model.get("custInfoOrgOrdIds"));
            modelDt.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
            
            System.out.println(model.get("custInfoOrgOrdIds"));
            List<String> orgOrdNoArr = new ArrayList();
            String[] custInfoOrgOrdIds = model.get("custInfoOrgOrdIds").toString().split(",");
            for (String keyword : custInfoOrgOrdIds ){
            	orgOrdNoArr.add(keyword);
            }
            modelDt.put("orgOrdNoArr", orgOrdNoArr);
            
            dao.custInfoUpdate(modelDt);
            m.put("errCnt", "0");
            
        } catch(Exception e){
        	m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> listByDlvIFExcel(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	int tmpCnt = Integer.parseInt(model.get("tmpCnt").toString());
    	if(tmpCnt > 0){
    		String[] invcNo = new String[tmpCnt];
    		for(int i = 0 ; i < tmpCnt ; i ++){
    			invcNo[i] = model.get("SPDCOL_INVC_NO"+i).toString();
    		}
    		model.put("invcNo", invcNo);
    	}
		model.put("pageIndex", "1");
		model.put("pageSize", "60000");
		map.put("LIST", dao.listByDlvIFExcel(model));
		return map;
    }
    
    @Override
    public Map<String, Object> listByDlvIFExcelV2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        int tmpCnt = Integer.parseInt(model.get("tmpCnt").toString());
        if(tmpCnt > 0){
            String[] invcNo = new String[tmpCnt];
            for(int i = 0 ; i < tmpCnt ; i ++){
                invcNo[i] = model.get("SPDCOL_INVC_NO"+i).toString();
            }
            model.put("invcNo", invcNo);
        }
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listByDlvIFExcelV2(model));
        return map;
    }
    
    /**
     * Method ID	: deviceIfSend
     * Method 설명	: DAS자료생성
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deviceIfSend(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	int tmpCnt = Integer.parseInt(model.get("tmpCnt").toString());
        	if(tmpCnt > 0){
        		String[] invcNo = new String[tmpCnt];
        		for(int i = 0 ; i < tmpCnt ; i ++){
        			invcNo[i] = model.get("SPDCOL_INVC_NO"+i).toString();
        		}
        		model.put("invcNo", invcNo);
        	}
        	model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        	
        	//Das list select
        	List list  = dao.deviceIfSendList(model);
        	int listCnt = list.size();
        	
        	if(listCnt > 0){
        		for(int i = 0 ; i < listCnt ; i ++){
        			Map<String, Object> modelIns = new HashMap<String, Object>();
        			modelIns.put("WMS_ORD_ID", (String)((Map<String, String>)list.get(i)).get("WMS_ORD_ID"));
        			modelIns.put("WMS_ORD_SEQ", (String)((Map<String, String>)list.get(i)).get("WMS_ORD_SEQ"));
        			modelIns.put("DAS_ORD_DT", (String)((Map<String, String>)list.get(i)).get("DAS_ORD_DT"));
        			modelIns.put("DAS_ORD_DEGREE", (String)((Map<String, String>)list.get(i)).get("DAS_ORD_DEGREE"));
        			modelIns.put("ITEM_NAME", (String)((Map<String, String>)list.get(i)).get("ITEM_NAME"));
        			modelIns.put("ITEM_CODE", (String)((Map<String, String>)list.get(i)).get("ITEM_CODE"));
        			modelIns.put("ITEM_BAR_CODE", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE"));
        			modelIns.put("ITEM_BAR_CODE2", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE2"));
        			modelIns.put("ITEM_BAR_CODE3", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE3"));
        			modelIns.put("ORD_QTY", (String)((Map<String, String>)list.get(i)).get("ORD_QTY"));
        			modelIns.put("INVC_NO", (String)((Map<String, String>)list.get(i)).get("INVC_NO"));
        			modelIns.put("INVC_ADD_FLAG", (String)((Map<String, String>)list.get(i)).get("INVC_ADD_FLAG"));
        			modelIns.put("ORG_ORD_ID", (String)((Map<String, String>)list.get(i)).get("ORG_ORD_ID"));
        			modelIns.put("RCVR_NM", (String)((Map<String, String>)list.get(i)).get("RCVR_NM"));
        			modelIns.put("RCVR_TEL", (String)((Map<String, String>)list.get(i)).get("RCVR_TEL"));
        			modelIns.put("ZIP_NO", (String)((Map<String, String>)list.get(i)).get("ZIP_NO"));
        			modelIns.put("RCVR_ADDR", (String)((Map<String, String>)list.get(i)).get("RCVR_ADDR"));
        			modelIns.put("RCVR_DETAIL_ADDR", (String)((Map<String, String>)list.get(i)).get("RCVR_DETAIL_ADDR"));
        			modelIns.put("DATA_SENDER_NM", (String)((Map<String, String>)list.get(i)).get("DATA_SENDER_NM"));
        			modelIns.put("LEGACY_ORG_ORD_NO", (String)((Map<String, String>)list.get(i)).get("LEGACY_ORG_ORD_NO"));
        			modelIns.put("P_CLNTNUM", (String)((Map<String, String>)list.get(i)).get("P_CLNTNUM"));
        			modelIns.put("P_CLNTMGMCUSTCD", (String)((Map<String, String>)list.get(i)).get("P_CLNTMGMCUSTCD"));
        			modelIns.put("P_PRNGDIVCD", (String)((Map<String, String>)list.get(i)).get("P_PRNGDIVCD"));
        			modelIns.put("P_CGOSTS", (String)((Map<String, String>)list.get(i)).get("P_CGOSTS"));
        			modelIns.put("P_ADDRESS", (String)((Map<String, String>)list.get(i)).get("P_ADDRESS"));
        			modelIns.put("P_ZIPNUM", (String)((Map<String, String>)list.get(i)).get("P_ZIPNUM"));
        			modelIns.put("P_ZIPID", (String)((Map<String, String>)list.get(i)).get("P_ZIPID"));
        			modelIns.put("P_OLDADDRESS", (String)((Map<String, String>)list.get(i)).get("P_OLDADDRESS"));
        			modelIns.put("P_OLDADDRESSDTL", (String)((Map<String, String>)list.get(i)).get("P_OLDADDRESSDTL"));
        			modelIns.put("P_NEWADDRESS", (String)((Map<String, String>)list.get(i)).get("P_NEWADDRESS"));
        			modelIns.put("P_NESADDRESSDTL", (String)((Map<String, String>)list.get(i)).get("P_NESADDRESSDTL"));
        			modelIns.put("P_ETCADDR", (String)((Map<String, String>)list.get(i)).get("P_ETCADDR"));
        			modelIns.put("P_SHORTADDR", (String)((Map<String, String>)list.get(i)).get("P_SHORTADDR"));
        			modelIns.put("P_CLSFADDR", (String)((Map<String, String>)list.get(i)).get("P_CLSFADDR"));
        			modelIns.put("P_CLLDLVBRANCD", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVBRANCD"));
        			modelIns.put("P_CLLDLVBRANNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVBRANNM"));
        			modelIns.put("P_CLLDLCBRANSHORTNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLCBRANSHORTNM"));
        			modelIns.put("P_CLLDLVEMPNUM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNUM"));
        			modelIns.put("P_CLLDLVEMPNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNM"));
        			modelIns.put("P_CLLDLVEMPNICKNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNICKNM"));
        			modelIns.put("P_CLSFCD", (String)((Map<String, String>)list.get(i)).get("P_CLSFCD"));
        			modelIns.put("P_CLSFNM", (String)((Map<String, String>)list.get(i)).get("P_CLSFNM"));
        			modelIns.put("P_SUBCLSFCD", (String)((Map<String, String>)list.get(i)).get("P_SUBCLSFCD"));
        			modelIns.put("P_RSPSDIV", (String)((Map<String, String>)list.get(i)).get("P_RSPSDIV"));
        			modelIns.put("P_NEWADDRYN", (String)((Map<String, String>)list.get(i)).get("P_NEWADDRYN"));
        			modelIns.put("P_ERRORCD", (String)((Map<String, String>)list.get(i)).get("P_ERRORCD"));
        			modelIns.put("P_ERRORMSG", (String)((Map<String, String>)list.get(i)).get("P_ERRORMSG"));
        			modelIns.put("PRINT_THIS_NO", (String)((Map<String, String>)list.get(i)).get("PRINT_THIS_NO"));
        			modelIns.put("PRINT_CNT", (String)((Map<String, String>)list.get(i)).get("PRINT_CNT"));
        			modelIns.put("PRINT_LAST_DATE", (String)((Map<String, String>)list.get(i)).get("PRINT_LAST_DATE"));
        			modelIns.put("PRINT_LAST_NAME", (String)((Map<String, String>)list.get(i)).get("PRINT_LAST_NAME"));
        			modelIns.put("INPUT_ORD_SEQ", (String)((Map<String, String>)list.get(i)).get("INPUT_ORD_SEQ"));
        			modelIns.put("PARCEL_ORD_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_ORD_TY"));
        			modelIns.put("PARCEL_PAY_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_PAY_TY"));
        			modelIns.put("PARCEL_BOX_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_BOX_TY"));
        			modelIns.put("PARCEL_ETC_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_ETC_TY"));
        			modelIns.put("PARCEL_COM_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_COM_TY"));        			
        			modelIns.put("PARCEL_COM_TY_SEQ", (String)((Map<String, String>)list.get(i)).get("PARCEL_COM_TY_SEQ"));
        			modelIns.put("CUST_USE_NO", (String)((Map<String, String>)list.get(i)).get("CUST_USE_NO"));
        			modelIns.put("ORG_INVC_NO", (String)((Map<String, String>)list.get(i)).get("ORG_INVC_NO"));
        			modelIns.put("DEL_YN", (String)((Map<String, String>)list.get(i)).get("DEL_YN"));
        			modelIns.put("REG_DT", (String)((Map<String, String>)list.get(i)).get("REG_DT"));
        			modelIns.put("REG_NO", (String)((Map<String, String>)list.get(i)).get("REG_NO"));
        			modelIns.put("DLV_MSG1", (String)((Map<String, String>)list.get(i)).get("DLV_MSG1"));
        			modelIns.put("SYS_COMPLETE_FLAG", (String)((Map<String, String>)list.get(i)).get("SYS_COMPLETE_FLAG"));
        			
        			//Das i/f table insert 
        			dao.deviceIfSendInsert(modelIns);
        		}
        	}
        	
        	Date nowDate = new Date();
    		SimpleDateFormat  dateFormat = new  SimpleDateFormat("yyyy-MM-dd");
    		String strNowDate = dateFormat.format(nowDate); 
			String jsonInputHis = "생성일자 : "+strNowDate+" , 생성 ROW : "+listCnt;
			
        	//i/f his table insert 
        	Map<String, Object> modelHis = new HashMap<String, Object>();
        	modelHis.put("LC_ID"			, "0000003200");
        	modelHis.put("CUST_ID"			, "0000295694");
        	modelHis.put("INTERFACE_CODE"	, "TASK_DAS_REQ"); 
			modelHis.put("INTERFACE_ETC1"	, "조건처리"); 
			modelHis.put("gubunHis1"		, "PASS"); 
			modelHis.put("INTERFACE_ETC2"	, jsonInputHis); 
			modelHis.put("INTERFACE_ETC3"	, "200"); 
			modelHis.put("USER_NO"			, model.get(ConstantIF.SS_USER_NO)); 
			//카카오 VX 인터페이스 수동처리 이력 생성
			dao610.insert(modelHis);
        	
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
        	m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID	: deviceIfListSearch
     * Method 설명	: DAS자료조회
     * 작성자			:
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> deviceIfListSearch(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.deviceIfListSearch(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
    	return map;
    }
    public Map<String, Object> dashBoard(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_DAS_DBOARD", dao.dashBoard(model));
		return map;
	}
    
    /*-
	 * Method ID	: getDasOrdDateByDegree
	 * Method 설명	: DAS생성차수
	 * @param 
	 * @return
	 */
    public Map<String, Object> getDasOrdDateByDegree(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_DAS_DEGREE", dao.getDasOrdDateByDegree(model));
		return map;
	}
    
    
    /**
     * Method ID    : deviceIfSendVX
     * Method 설명    : DAS자료생성 KVX
     * 작성자          : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deviceIfSendV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            int tmpCnt = Integer.parseInt(model.get("tmpCnt").toString());
            if(tmpCnt > 0){
                String[] invcNo = new String[tmpCnt];
                for(int i = 0 ; i < tmpCnt ; i ++){
                    invcNo[i] = model.get("SPDCOL_INVC_NO"+i).toString();
                }
                model.put("invcNo", invcNo);
            }
            model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            //Das list select
            List list  = dao.deviceIfSendListV2(model);
            int listCnt = list.size();
            
            if(listCnt > 0){
                for(int i = 0 ; i < listCnt ; i ++){
                    Map<String, Object> modelIns = new HashMap<String, Object>();
                    modelIns.put("WMS_ORD_ID", (String)((Map<String, String>)list.get(i)).get("WMS_ORD_ID"));
                    modelIns.put("WMS_ORD_SEQ", (String)((Map<String, String>)list.get(i)).get("WMS_ORD_SEQ"));
                    modelIns.put("DAS_ORD_DT", (String)((Map<String, String>)list.get(i)).get("DAS_ORD_DT"));
                    modelIns.put("DAS_ORD_DEGREE", (String)((Map<String, String>)list.get(i)).get("DAS_ORD_DEGREE"));
                    modelIns.put("ITEM_NAME", (String)((Map<String, String>)list.get(i)).get("ITEM_NAME"));
                    modelIns.put("ITEM_CODE", (String)((Map<String, String>)list.get(i)).get("ITEM_CODE"));
                    modelIns.put("ITEM_BAR_CODE", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE"));
                    modelIns.put("ITEM_BAR_CODE2", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE2"));
                    modelIns.put("ITEM_BAR_CODE3", (String)((Map<String, String>)list.get(i)).get("ITEM_BAR_CODE3"));
                    modelIns.put("ORD_QTY", (String)((Map<String, String>)list.get(i)).get("ORD_QTY"));
                    modelIns.put("INVC_NO", (String)((Map<String, String>)list.get(i)).get("INVC_NO"));
                    modelIns.put("INVC_ADD_FLAG", (String)((Map<String, String>)list.get(i)).get("INVC_ADD_FLAG"));
                    modelIns.put("ORG_ORD_ID", (String)((Map<String, String>)list.get(i)).get("ORG_ORD_ID"));
                    modelIns.put("RCVR_NM", (String)((Map<String, String>)list.get(i)).get("RCVR_NM"));
                    modelIns.put("RCVR_TEL", (String)((Map<String, String>)list.get(i)).get("RCVR_TEL"));
                    modelIns.put("ZIP_NO", (String)((Map<String, String>)list.get(i)).get("ZIP_NO"));
                    modelIns.put("RCVR_ADDR", (String)((Map<String, String>)list.get(i)).get("RCVR_ADDR"));
                    modelIns.put("RCVR_DETAIL_ADDR", (String)((Map<String, String>)list.get(i)).get("RCVR_DETAIL_ADDR"));
                    modelIns.put("DATA_SENDER_NM", (String)((Map<String, String>)list.get(i)).get("DATA_SENDER_NM"));
                    modelIns.put("LEGACY_ORG_ORD_NO", (String)((Map<String, String>)list.get(i)).get("LEGACY_ORG_ORD_NO"));
                    modelIns.put("P_CLNTNUM", (String)((Map<String, String>)list.get(i)).get("P_CLNTNUM"));
                    modelIns.put("P_CLNTMGMCUSTCD", (String)((Map<String, String>)list.get(i)).get("P_CLNTMGMCUSTCD"));
                    modelIns.put("P_PRNGDIVCD", (String)((Map<String, String>)list.get(i)).get("P_PRNGDIVCD"));
                    modelIns.put("P_CGOSTS", (String)((Map<String, String>)list.get(i)).get("P_CGOSTS"));
                    modelIns.put("P_ADDRESS", (String)((Map<String, String>)list.get(i)).get("P_ADDRESS"));
                    modelIns.put("P_ZIPNUM", (String)((Map<String, String>)list.get(i)).get("P_ZIPNUM"));
                    modelIns.put("P_ZIPID", (String)((Map<String, String>)list.get(i)).get("P_ZIPID"));
                    modelIns.put("P_OLDADDRESS", (String)((Map<String, String>)list.get(i)).get("P_OLDADDRESS"));
                    modelIns.put("P_OLDADDRESSDTL", (String)((Map<String, String>)list.get(i)).get("P_OLDADDRESSDTL"));
                    modelIns.put("P_NEWADDRESS", (String)((Map<String, String>)list.get(i)).get("P_NEWADDRESS"));
                    modelIns.put("P_NESADDRESSDTL", (String)((Map<String, String>)list.get(i)).get("P_NESADDRESSDTL"));
                    modelIns.put("P_ETCADDR", (String)((Map<String, String>)list.get(i)).get("P_ETCADDR"));
                    modelIns.put("P_SHORTADDR", (String)((Map<String, String>)list.get(i)).get("P_SHORTADDR"));
                    modelIns.put("P_CLSFADDR", (String)((Map<String, String>)list.get(i)).get("P_CLSFADDR"));
                    modelIns.put("P_CLLDLVBRANCD", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVBRANCD"));
                    modelIns.put("P_CLLDLVBRANNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVBRANNM"));
                    modelIns.put("P_CLLDLCBRANSHORTNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLCBRANSHORTNM"));
                    modelIns.put("P_CLLDLVEMPNUM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNUM"));
                    modelIns.put("P_CLLDLVEMPNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNM"));
                    modelIns.put("P_CLLDLVEMPNICKNM", (String)((Map<String, String>)list.get(i)).get("P_CLLDLVEMPNICKNM"));
                    modelIns.put("P_CLSFCD", (String)((Map<String, String>)list.get(i)).get("P_CLSFCD"));
                    modelIns.put("P_CLSFNM", (String)((Map<String, String>)list.get(i)).get("P_CLSFNM"));
                    modelIns.put("P_SUBCLSFCD", (String)((Map<String, String>)list.get(i)).get("P_SUBCLSFCD"));
                    modelIns.put("P_RSPSDIV", (String)((Map<String, String>)list.get(i)).get("P_RSPSDIV"));
                    modelIns.put("P_NEWADDRYN", (String)((Map<String, String>)list.get(i)).get("P_NEWADDRYN"));
                    modelIns.put("P_ERRORCD", (String)((Map<String, String>)list.get(i)).get("P_ERRORCD"));
                    modelIns.put("P_ERRORMSG", (String)((Map<String, String>)list.get(i)).get("P_ERRORMSG"));
                    modelIns.put("PRINT_THIS_NO", (String)((Map<String, String>)list.get(i)).get("PRINT_THIS_NO"));
                    modelIns.put("PRINT_CNT", (String)((Map<String, String>)list.get(i)).get("PRINT_CNT"));
                    modelIns.put("PRINT_LAST_DATE", (String)((Map<String, String>)list.get(i)).get("PRINT_LAST_DATE"));
                    modelIns.put("PRINT_LAST_NAME", (String)((Map<String, String>)list.get(i)).get("PRINT_LAST_NAME"));
                    modelIns.put("INPUT_ORD_SEQ", (String)((Map<String, String>)list.get(i)).get("INPUT_ORD_SEQ"));
                    modelIns.put("PARCEL_ORD_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_ORD_TY"));
                    modelIns.put("PARCEL_PAY_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_PAY_TY"));
                    modelIns.put("PARCEL_BOX_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_BOX_TY"));
                    modelIns.put("PARCEL_ETC_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_ETC_TY"));
                    modelIns.put("PARCEL_COM_TY", (String)((Map<String, String>)list.get(i)).get("PARCEL_COM_TY"));                 
                    modelIns.put("PARCEL_COM_TY_SEQ", (String)((Map<String, String>)list.get(i)).get("PARCEL_COM_TY_SEQ"));
                    modelIns.put("CUST_USE_NO", (String)((Map<String, String>)list.get(i)).get("CUST_USE_NO"));
                    modelIns.put("ORG_INVC_NO", (String)((Map<String, String>)list.get(i)).get("ORG_INVC_NO"));
                    modelIns.put("DEL_YN", (String)((Map<String, String>)list.get(i)).get("DEL_YN"));
                    modelIns.put("REG_DT", (String)((Map<String, String>)list.get(i)).get("REG_DT"));
                    modelIns.put("REG_NO", (String)((Map<String, String>)list.get(i)).get("REG_NO"));
                    modelIns.put("SYS_COMPLETE_FLAG", (String)((Map<String, String>)list.get(i)).get("SYS_COMPLETE_FLAG"));
                    
                    //Das i/f table insert 
                    dao.deviceIfSendInsertV2(modelIns);
                }
            }
            
            Date nowDate = new Date();
            SimpleDateFormat  dateFormat = new  SimpleDateFormat("yyyy-MM-dd");
            String strNowDate = dateFormat.format(nowDate); 
            String jsonInputHis = "생성일자 : "+strNowDate+" , 생성 ROW : "+listCnt;
            
            //i/f his table insert 
            Map<String, Object> modelHis = new HashMap<String, Object>();
            modelHis.put("LC_ID"            , "0000003200");
            modelHis.put("CUST_ID"          , "0000295694");
            modelHis.put("INTERFACE_CODE"   , "TASK_DAS_REQ"); 
            modelHis.put("INTERFACE_ETC1"   , "조건처리"); 
            modelHis.put("gubunHis1"        , "PASS"); 
            modelHis.put("INTERFACE_ETC2"   , jsonInputHis); 
            modelHis.put("INTERFACE_ETC3"   , "200"); 
            modelHis.put("USER_NO"          , model.get(ConstantIF.SS_USER_NO)); 
            //카카오 VX 인터페이스 수동처리 이력 생성
            dao610.insert(modelHis);
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            m.put("errCnt", "1");
            m.put("MSG", e.getMessage());
            throw e;
        }
        return m;
    }
}