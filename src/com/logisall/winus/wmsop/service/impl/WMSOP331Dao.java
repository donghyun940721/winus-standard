package com.logisall.winus.wmsop.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP331Dao")
public class WMSOP331Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID  : rawListByCustSummary
     * Method 설명  : main
     * 작성자             : chsong
     * @param model
     * @return
     */
    public List rawListByCustSummary(Map<String, Object> model) {
		List custs = list("wmsop331.listByCustSummary", model);
    	return custs;
	}
	public Object listByCustSummary(Map<String, Object> model) {
		List custs = rawListByCustSummary(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID  : rawListByCustSummaryPoi
     * Method 설명  : main
     * 작성자             : chsong
     * @param model
     * @return
     */
    public List rawListByCustSummaryPoi(Map<String, Object> model) {
		List custs = list("wmsop331.listByCustSummaryPoi", model);
    	return custs;
	}
	public Object listByCustSummaryPoi(Map<String, Object> model) {
		List custs = rawListByCustSummaryPoi(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}

	/**
     * Method ID  : listByCustDetail
     * Method 설명  : detail
     * 작성자             : chsong
     * @param model
     * @return
     */
	public Object listByCustDetail(Map<String, Object> model) {
		
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop331.listByCustDetail", model));
    	return genericResultSet;
	}
	
	/**
     * Method ID  : billingListDetail
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet billingListDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsop331.billingListDetail", model);
    }
    
    /**
	 * Method ID	: popupList
	 * Method 설명	: 그리드 리스트 조회
	 * 작성자			: 
	 * @param model
	 * @return
	 */
	public GenericResultSet popupList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmsop331.popupList", model));
		return wqrs;
	}
	
	/**
	 * Method ID	: popupSave 
	 * Method 설명	:  
	 * 작성자 			: 
	 * 
	 * @param model
	 * @return
	 */
	public Object popupSave(Map<String, Object> model) {
		return executeUpdate("wmsop331.popupSave", model);
	}
}
