package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP777Service {
	  public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
	  public Map<String, Object> listE2(Map<String, Object> model) throws Exception;	
	  public Map<String, Object> listE2OrdCount(Map<String, Object> model) throws Exception;	
}
