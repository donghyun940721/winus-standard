package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveApprove(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveConfirmQc(Map<String, Object> model) throws Exception;
    public Map<String, Object> inOutOrderCntInit(Map<String, Object> model) throws Exception;
    public Map<String, Object> custIfInOrd(Map<String, Object> model) throws Exception;
    public Map<String, Object> custIfOutOrd(Map<String, Object> model) throws Exception;
    public Map<String, Object> custIfOutOrdCancel(Map<String, Object> model) throws Exception;
    public Map<String, Object> custIfOutOrdReturn(Map<String, Object> model) throws Exception;
    public Map<String, Object> custIfOutOrdReturnCancel(Map<String, Object> model) throws Exception;
    public Map<String, Object> tempListInOrd(Map<String, Object> model) throws Exception;
    public Map<String, Object> tempListOutOrd(Map<String, Object> model) throws Exception;
}
