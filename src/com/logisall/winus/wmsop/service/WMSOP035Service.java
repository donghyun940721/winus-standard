package com.logisall.winus.wmsop.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONObject;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;

public interface WMSOP035Service {

	public byte[] pdfDown(Map<String, Object> model, String company) throws Exception;
	
	public byte[] pdfDown2(Map<String, Object> model, String company) throws Exception;

	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> list2DHL(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> list2QXP(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> request2EMS(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> list2(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> outSaveComplete(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> outSaveCompleteV2(Map<String, Object> model) throws Exception;

	public Map<String, Object> searchUnshipped(Map<String, Object> model) throws Exception;

	public Map<String, Object> dhlExcel(Map<String, Object> model) throws Exception;

	public Map<String, Object> saveList2DHL(Map<String, Object> model) throws Exception;

	public Map<String, Object> list2EMS(Map<String, Object> model) throws Exception;

	public Map<String, Object> emsAddress(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> addressHistory(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> addressOM010(Map<String, Object> model) throws Exception;

	public Map<String, Object> export2EMS(Map<String, Object> model) throws Exception;

	public Map<String, Object> saveExport2EMS(List list) throws Exception;
	
	public Map<String, Object> saveExport2EMS_V2(List list) throws Exception;

	public String getOrdId(String orgOrdId) throws Exception;
	
	public int getEmsShipment(String ordId) throws Exception;

	public boolean updateBlNo(List list) throws Exception;

	public Map<String, Object> checkBlNo(Map<String, Object> model) throws Exception;

	public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> listE6(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> selectPrintDt(Map<String, Object> mode) throws Exception;
	
	public Map<String, Object> updatetPrintDt(Map<String, Object> mode) throws Exception;
	
	public Map<String, Object> crossDomainHttpQxpRequestPdf(Map<String, Object> mode) throws Exception;
	
	public String sendMcsOrder(Map<String, Object> map) throws Exception;
	
	public String cancelMcsOrder(Map<String, Object> map, String orgOrdIds) throws Exception;
	
	public Map<String, Object> loginWcs() throws Exception;
	
	public Map<String, Object> getShippingCompanyList(Map<String, Object> mode) throws Exception; 
	
	public int getSynnaraOrderCheck(Map<String, Object> mode) throws Exception;
	
	public Map<String, Object> getSynnaraOrderList(Map<String, Object> mode) throws Exception;
	
	public Map<String, Object> outBoundComplete(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> updateDelYn(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> sendPickingListRobot(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> InvoiceReceipt(Map<String, Object> model)throws Exception;

	Map<String, Object> decryptOrderAddress(Map<String, Object> model) throws Exception;

	Map<String, Object> selectUnsentInvoiceList(Map<String, Object> model);
}
