package com.logisall.winus.wmsop.service;

import java.io.IOException;
import java.util.Map;

import com.logisall.api.oywcs.dto.OrdrListWrapper;

public interface WMSOP630Service {

	Map<String, Object> saveAIPicking(Map<String, Object> model, Map<String, Object> reqBody, String uuid);
	
	Map<String, Object> requestAiPicking(String uuid) throws IOException;

}
