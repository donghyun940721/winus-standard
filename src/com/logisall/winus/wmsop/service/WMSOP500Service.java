package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP500Service {
	
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> listHistory(Map<String, Object> model) throws Exception;
	
}
