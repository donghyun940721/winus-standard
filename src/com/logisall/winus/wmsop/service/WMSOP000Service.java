package com.logisall.winus.wmsop.service;

import java.util.Map;


public interface WMSOP000Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> getCenterRule(Map<String, Object> model) throws Exception;
    public Map<String, Object> getAssertRule(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listCarPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDsp(Map<String, Object> model) throws Exception;        
    public Map<String, Object> listNewDsp(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveDsp(Map<String, Object> model) throws Exception;    
    public Map<String, Object> deleteDsp(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelDsp(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listConfirmDsp(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveConfirmCarIn(Map<String, Object> model) throws Exception;    
    public Map<String, Object> saveSetDock(Map<String, Object> model) throws Exception;   

    public Map<String, Object> listLoc(Map<String, Object> model) throws Exception;
    public Map<String, Object> listLocV2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLocInitType(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLocInitTypeV2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listLocSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLoc(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteLoc(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateLocAutoIn(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateLocAutoOut(Map<String, Object> model) throws Exception;    
    public Map<String, Object> saveLocChangeWorkQty(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveSetWorkDate(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> updateSetLocAutoIn(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateSetLocAutoOut(Map<String, Object> model) throws Exception; 
    
    public Map<String, Object> listTransporDsp(Map<String, Object> model) throws Exception;
    public Map<String, Object> listTransCustIdDsp(Map<String, Object> model) throws Exception;

    public Map<String, Object> saveTransDsp(Map<String, Object> model) throws Exception;    
}
