package com.logisall.winus.wmsop.dto;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveInitOrdersDTO {
	
	private String vrOrdType;
	private String[] no;
	private String[] reqDt;
	private String[] custOrdNo;
	private String[] custOrdSeq;

	private String[] trustCustCd;
	private String[] transCustCd;
	private String[] transCustTel;
	private String[] transReqDt;
	private String[] custCd;
     
	private String[] ordQty;
	private String[] uomCd;
	private String[] sdeptCd;
	private String[] salePerCd;
	private String[] carCd;
     
	private String[] drvNm;
	private String[] dlvSeq;
	private String[] drvTel;
	private String[] custLotNo;
	private String[] blNo;
     
	private String[] recDt;
	private String[] whCd;
	private String[] makeDt;
	private String[] timePeriodDay;
	private String[] workYn;
     
	private String[] rjType;
	private String[] locYn;
	private String[] confYn;
	private String[] eaCapa;
	private String[] inOrdWeight;
     
	private String[] itemCd;
	private String[] itemNm;
	private String[] transCustNm;
	private String[] transCustAddr;
	private String[] transEmpNm;		//35
     
	private String[] remark;
	private String[] transZipNo;
	private String[] etc2;
	private String[] unitAmt;
	private String[] transBizNo;		//40
     
	private String[] inCustAddr;
	private String[] inCustCd;
	private String[] inCustNm;
	private String[] inCustTel;
	private String[] inCustEmpNm;		//45
     
	private String[] expiryDate;
	private String[] salesCustNm;
	private String[] zip;
	private String[] addr;
	private String[] addr2;				//50
     
	private String[] phone1;
	private String[] etc1;
	private String[] unitNo;
	private String[] phone2;
	private String[] buyCustNm;			//55

	private String[] buyPhone1;
	private String[] salesCompanyNm;
	private String[] ordDegree;
	private String[] bizCond;
	private String[] bizType;			//60
	
	private String[] bizNo;
	private String[] custType;
	private String[] dataSenderNm;
	private String[] legacyOrgOrdNo;
	private String[] custSeq;			//65
	
	private String[] lotType;
	private String[] ownerCd;
	private String[] etd;
	private String[] eta;
	private String[] exchangerate;		//70
	
	private String[] ordWeight;
	private String[] cntrNo;
	private String[] refNo;
	private String[] itemBarCode;
	private String[] fromTimeZone;		//75
	
	private String[] ordSubType;
	private String[] ccCd;
	private String[] um;
	private String[] cntrType;
	private String[] locCd;				//80
	
	private String LC_ID;
	private String USER_NO;
	private String WORK_IP;
	
	
	public SaveInitOrdersDTO(int cnt) {
		
		this.no = new String[cnt];  
		this.reqDt = new String[cnt];  
		this.custOrdNo = new String[cnt];  
		this.custOrdSeq = new String[cnt];  
		this.trustCustCd = new String[cnt];  
		this.transCustCd = new String[cnt];  
		this.transCustTel = new String[cnt];  
		this.transReqDt = new String[cnt];  
		this.custCd = new String[cnt];  
		this.ordQty = new String[cnt];  
		this.uomCd = new String[cnt];  
		this.sdeptCd = new String[cnt];  
		this.salePerCd = new String[cnt];  
		this.carCd = new String[cnt];  
		this.drvNm = new String[cnt];  
		this.dlvSeq = new String[cnt];  
		this.drvTel = new String[cnt];  
		this.custLotNo = new String[cnt];  
		this.blNo = new String[cnt];  
		this.recDt = new String[cnt];  
		this.whCd = new String[cnt];  
		this.makeDt = new String[cnt];  
		this.timePeriodDay = new String[cnt];  
		this.workYn = new String[cnt];  
		this.rjType = new String[cnt];  
		this.locYn = new String[cnt];  
		this.confYn = new String[cnt];  
		this.eaCapa = new String[cnt];  
		this.inOrdWeight = new String[cnt];  
		this.itemCd = new String[cnt];  
		this.itemNm = new String[cnt];  
		this.transCustNm = new String[cnt];  
		this.transCustAddr = new String[cnt];  
		this.transEmpNm = new String[cnt];  
		this.remark = new String[cnt];  
		this.transZipNo = new String[cnt];  
		this.etc2 = new String[cnt];  
		this.unitAmt = new String[cnt];  
		this.transBizNo = new String[cnt];  
		this.inCustAddr = new String[cnt];  
		this.inCustCd = new String[cnt];  
		this.inCustNm = new String[cnt];  
		this.inCustTel = new String[cnt];  
		this.inCustEmpNm = new String[cnt];  
		this.expiryDate = new String[cnt];  
		this.salesCustNm = new String[cnt];  
		this.zip = new String[cnt];  
		this.addr = new String[cnt];  
		this.addr2 = new String[cnt];  
		this.phone1 = new String[cnt];  
		this.etc1 = new String[cnt];  
		this.unitNo = new String[cnt];  
		this.phone2 = new String[cnt];  
		this.buyCustNm = new String[cnt];  
		this.buyPhone1 = new String[cnt];  
		this.salesCompanyNm = new String[cnt];  
		this.ordDegree = new String[cnt];  
		this.bizCond = new String[cnt];  
		this.bizType = new String[cnt];  
		this.bizNo = new String[cnt];  
		this.custType = new String[cnt];  
		this.dataSenderNm = new String[cnt];  
		this.legacyOrgOrdNo = new String[cnt];  
		this.custSeq = new String[cnt];  
		this.lotType = new String[cnt];  
		this.ownerCd = new String[cnt];  
		this.etd = new String[cnt];  
		this.eta = new String[cnt];  
		this.exchangerate = new String[cnt];  
		this.ordWeight = new String[cnt];  
		this.cntrNo = new String[cnt];  
		this.refNo = new String[cnt];  
		this.itemBarCode = new String[cnt];  
		this.fromTimeZone = new String[cnt];  
		this.ordSubType = new String[cnt];  
		this.ccCd = new String[cnt];  
		this.um = new String[cnt];  
		this.cntrType = new String[cnt];  
		this.locCd = new String[cnt];
	}
	
	public SaveInitOrdersDTO from(Map<String, Object> model) throws JsonParseException, JsonMappingException, IOException {
		
		int cnt = Integer.parseInt(model.get("listSize").toString());
		SaveInitOrdersDTO dto = new SaveInitOrdersDTO(cnt);
		
		ObjectMapper om = new ObjectMapper();
		List<Map<String, Object>> orders = om.convertValue(model.get("list"), new TypeReference<List<Map<String, Object>>>() {});

		vrOrdType = getValue(model, "vrOrdType");
		
		for (int i = 0; i < cnt; i++) {
			Map<String, Object> order = orders.get(i);
			
			no[i] = Integer.toString(i+1);
			reqDt[i] = vrOrdType.equals("I") ? getValue(order, "IN_REQ_DT") : getValue(order, "OUT_REQ_DT");	//입고 출고 구분
			custOrdNo[i] = getValue(order, "CUST_ORD_NO");         
			custOrdSeq[i] = getValue(order, "CUST_ORD_SEQ");
			                   
			trustCustCd[i] = getValue(order, "TRUST_CUST_CD");
			transCustCd[i] = getValue(order, "TRANS_CUST_CD");
			transCustTel[i] = getValue(order, "TRANS_CUST_TEL");
			transReqDt[i] = getValue(order, "TRANS_REQ_DT");
			custCd[i] = getValue(model, "vrSrchCustCd");				// 화주: 검색화주
			                   
			ordQty[i] = getValue(order, "ORD_QTY");
			uomCd[i] = getValue(order, "UOM_CD");
			sdeptCd[i] = getValue(order, "SDEPT_CD");
			salePerCd[i] = getValue(order, "SALE_PER_CD");
			carCd[i] = getValue(order, "CAR_CD");
			                   
			drvNm[i] = getValue(order, "DRV_NM");
			dlvSeq[i] = getValue(order, "DLV_SEQ");
			drvTel[i] = getValue(order, "DRV_TEL");
			custLotNo[i] = getValue(order, "CUST_LOT_NO");
			blNo[i] = getValue(order, "BL_NO");
			                   
			recDt[i] = getValue(order, "REC_DT");
			whCd[i] = vrOrdType.equals("I") ? getValue(order, "IN_WH_CD") : getValue(order, "OUT_WH_CD");
			makeDt[i] = getValue(order, "MAKE_DT");
			timePeriodDay[i] = getValue(order, "TIME_PERIOD_DAY");
			workYn[i] = getValue(order, "WORK_YN");
			                   
			rjType[i] = getValue(order, "RJ_TYPE");
			locYn[i] = getValue(order, "LOC_YN");
			confYn[i] = getValue(order, "CONF_YN");
			eaCapa[i] = getValue(order, "EA_CAPA");
			inOrdWeight[i] = getValue(order, "IN_ORD_WEIGHT");
			                   
			itemCd[i] = getValue(order, "ITEM_CD");
			itemNm[i] = getValue(order, "ITEM_NM");
			transCustNm[i] = getValue(order, "TRANS_CUST_NM");
			transCustAddr[i] = getValue(order, "TRANS_CUST_ADDR");
			transEmpNm[i] = getValue(order, "TRANS_EMP_NM");
			                   
			remark[i] = getValue(order, "REMARK");
			transZipNo[i] = getValue(order, "TRANS_ZIP_NO");
			etc2[i] = getValue(order, "ETC2");
			unitAmt[i] = getValue(order, "UNIT_AMT");
			transBizNo[i] = getValue(order, "TRANS_BIZ_NO");
			                   
			inCustAddr[i] = getValue(order, "IN_CUST_ADDR");
			inCustCd[i] = getValue(order, "IN_CUST_CD");
			inCustNm[i] = getValue(order, "IN_CUST_NM");
			inCustTel[i] = getValue(order, "IN_CUST_TEL");
			inCustEmpNm[i] = getValue(order, "IN_CUST_EMP_NM");
			                   
			expiryDate[i] = getValue(order, "EXPIRY_DATE");
			salesCustNm[i] = getValue(order, "SALES_CUST_NM");
			zip[i] = getValue(order, "ZIP");
			addr[i] = getValue(order, "ADDR");
			addr2[i] = getValue(order, "ADDR2");
			                   
			phone1[i] = getValue(order, "PHONE_1");
			//KAKAO VX 일경우 ETC1 엑셀업로드에서 ETC2로 입력 >> ETC2 -> ORD_DESC
			etc1[i] = model.get("SS_SVC_NO").equals("0000003200") ? getValue(order, "ETC2") : getValue(order, "ETC1");
			unitNo[i] = getValue(order, "UNIT_NO");
			phone2[i] = getValue(order, "PHONE_2");
			buyCustNm[i] = getValue(order, "BUY_CUST_NM");
			                   
			buyPhone1[i] = getValue(order, "BUY_PHONE_1");
			salesCompanyNm[i] = getValue(order, "SALES_COMPANY_NM");
			ordDegree[i] = getValue(order, "ORD_DEGREE");
			bizCond[i] = getValue(order, "BIZ_COND");
			bizType[i] = getValue(order, "BIZ_TYPE");
			                   
			bizNo[i] = getValue(order, "BIZ_NO");
			custType[i] = getValue(order, "CUST_TYPE");
			dataSenderNm[i] = getValue(order, "DATA_SENDER_NM");
			//GFC 일경우 legacyOrgOrdNo(고객사주문번호)는 엑셀업로드에서 POI_NO로 입력
			legacyOrgOrdNo[i] = model.get("SS_SVC_NO").equals("0000003420") ? getValue(order, "POI_NO") : getValue(order, "LEGACY_ORG_ORD_NO");
			custSeq[i] = getValue(model, "CUST_SEQ");		// model에서 꺼냄
			                   
			lotType[i] = getValue(order, "LOT_TYPE");
			ownerCd[i] = getValue(order, "OWNER_CD");
			etd[i] = getValue(order, "ETD");
			eta[i] = getValue(order, "ETA");
			exchangerate[i] = getValue(order, "EXCHANGE_RATE");
			                   
			ordWeight[i] = getValue(order, "ORD_WEIGHT");
			cntrNo[i] = getValue(order, "CNTR_NO");
			refNo[i] = getValue(order, "REF_NO");
			itemBarCode[i] = getValue(order, "ITEM_BARCODE");
			fromTimeZone[i] = getValue(order, "FROM_TIME_ZONE");
			                   
			ordSubType[i] = getValue(order, "ORD_SUBTYPE");
			ccCd[i] = getValue(order, "CC_CD");
			um[i] = getValue(order, "UNIT_AMT");
			cntrType[i] = getValue(order, "CNTR_TYPE");
			locCd[i] = getValue(order, "LOC_CD");		
		}
		
		// 주문데이터 공통 필드
		vrOrdType = getValue(model, "vrOrdType");
		LC_ID = getValue(model, "SS_SVC_NO");
		USER_NO = getValue(model, "SS_USER_NO");
		WORK_IP = getValue(model, "SS_CLIENT_IP");
				
		return dto;
	}
	
    /**
     * Method 설명         : 값 있으면 뽑고, 없으면 빈 문자열로 초기화. null 방지용
     * @author ryu512
     * @param value
     * @return
     */
	private String getValue(Map<String, Object> map, String key) {
		
		return map.containsKey(key) && map.get(key) != null ? map.get(key).toString() : "";
	}

	// Getter, Setter (lombok 미적용 ,, 우선 조치)
	public String getVrOrdType() {
		return vrOrdType;
	}

	public String[] getNo() {
		return no;
	}

	public String[] getReqDt() {
		return reqDt;
	}

	public String[] getCustOrdNo() {
		return custOrdNo;
	}

	public String[] getCustOrdSeq() {
		return custOrdSeq;
	}

	public String[] getTrustCustCd() {
		return trustCustCd;
	}

	public String[] getTransCustCd() {
		return transCustCd;
	}

	public String[] getTransCustTel() {
		return transCustTel;
	}

	public String[] getTransReqDt() {
		return transReqDt;
	}

	public String[] getCustCd() {
		return custCd;
	}

	public String[] getOrdQty() {
		return ordQty;
	}

	public String[] getUomCd() {
		return uomCd;
	}

	public String[] getSdeptCd() {
		return sdeptCd;
	}

	public String[] getSalePerCd() {
		return salePerCd;
	}

	public String[] getCarCd() {
		return carCd;
	}

	public String[] getDrvNm() {
		return drvNm;
	}

	public String[] getDlvSeq() {
		return dlvSeq;
	}

	public String[] getDrvTel() {
		return drvTel;
	}

	public String[] getCustLotNo() {
		return custLotNo;
	}

	public String[] getBlNo() {
		return blNo;
	}

	public String[] getRecDt() {
		return recDt;
	}

	public String[] getWhCd() {
		return whCd;
	}

	public String[] getMakeDt() {
		return makeDt;
	}

	public String[] getTimePeriodDay() {
		return timePeriodDay;
	}

	public String[] getWorkYn() {
		return workYn;
	}

	public String[] getRjType() {
		return rjType;
	}

	public String[] getLocYn() {
		return locYn;
	}

	public String[] getConfYn() {
		return confYn;
	}

	public String[] getEaCapa() {
		return eaCapa;
	}

	public String[] getInOrdWeight() {
		return inOrdWeight;
	}

	public String[] getItemCd() {
		return itemCd;
	}

	public String[] getItemNm() {
		return itemNm;
	}

	public String[] getTransCustNm() {
		return transCustNm;
	}

	public String[] getTransCustAddr() {
		return transCustAddr;
	}

	public String[] getTransEmpNm() {
		return transEmpNm;
	}

	public String[] getRemark() {
		return remark;
	}

	public String[] getTransZipNo() {
		return transZipNo;
	}

	public String[] getEtc2() {
		return etc2;
	}

	public String[] getUnitAmt() {
		return unitAmt;
	}

	public String[] getTransBizNo() {
		return transBizNo;
	}

	public String[] getInCustAddr() {
		return inCustAddr;
	}

	public String[] getInCustCd() {
		return inCustCd;
	}

	public String[] getInCustNm() {
		return inCustNm;
	}

	public String[] getInCustTel() {
		return inCustTel;
	}

	public String[] getInCustEmpNm() {
		return inCustEmpNm;
	}

	public String[] getExpiryDate() {
		return expiryDate;
	}

	public String[] getSalesCustNm() {
		return salesCustNm;
	}

	public String[] getZip() {
		return zip;
	}

	public String[] getAddr() {
		return addr;
	}

	public String[] getAddr2() {
		return addr2;
	}

	public String[] getPhone1() {
		return phone1;
	}

	public String[] getEtc1() {
		return etc1;
	}

	public String[] getUnitNo() {
		return unitNo;
	}

	public String[] getPhone2() {
		return phone2;
	}

	public String[] getBuyCustNm() {
		return buyCustNm;
	}

	public String[] getBuyPhone1() {
		return buyPhone1;
	}

	public String[] getSalesCompanyNm() {
		return salesCompanyNm;
	}

	public String[] getOrdDegree() {
		return ordDegree;
	}

	public String[] getBizCond() {
		return bizCond;
	}

	public String[] getBizType() {
		return bizType;
	}

	public String[] getBizNo() {
		return bizNo;
	}

	public String[] getCustType() {
		return custType;
	}

	public String[] getDataSenderNm() {
		return dataSenderNm;
	}

	public String[] getLegacyOrgOrdNo() {
		return legacyOrgOrdNo;
	}

	public String[] getCustSeq() {
		return custSeq;
	}

	public String[] getLotType() {
		return lotType;
	}

	public String[] getOwnerCd() {
		return ownerCd;
	}

	public String[] getEtd() {
		return etd;
	}

	public String[] getEta() {
		return eta;
	}

	public String[] getExchangerate() {
		return exchangerate;
	}

	public String[] getOrdWeight() {
		return ordWeight;
	}

	public String[] getCntrNo() {
		return cntrNo;
	}

	public String[] getRefNo() {
		return refNo;
	}

	public String[] getItemBarCode() {
		return itemBarCode;
	}

	public String[] getFromTimeZone() {
		return fromTimeZone;
	}

	public String[] getOrdSubType() {
		return ordSubType;
	}

	public String[] getCcCd() {
		return ccCd;
	}

	public String[] getUm() {
		return um;
	}

	public String[] getCntrType() {
		return cntrType;
	}

	public String[] getLocCd() {
		return locCd;
	}

	public String getLC_ID() {
		return LC_ID;
	}

	public String getUSER_NO() {
		return USER_NO;
	}

	public String getWORK_IP() {
		return WORK_IP;
	}

}
