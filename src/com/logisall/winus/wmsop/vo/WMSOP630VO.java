package com.logisall.winus.wmsop.vo;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.logisall.winus.frm.common.util.OliveAes256;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;

public class WMSOP630VO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2518464732146069890L;
	
	private String orgOrdId;
	private String ordId;
	private String invalidYn;
	private String dCountry;
	private String dCity;
	private String dPost;
	private String dAddress;
	private String dAddress2;
	private String dProvince;
	private String dTel;
	private String dCustNm;
	
	public String getOrgOrdId() {
		return orgOrdId;
	}
	public void setOrgOrdId(String orgOrdId) {
		this.orgOrdId = orgOrdId;
	}
	
	public String getdCountry() {
		return dCountry;
	}
	public void setdCountry(String dCountry) {
		this.dCountry = dCountry;
	}
	public String getdCity() {
		return dCity;
	}
	public void setdCity(String dCity) {
		this.dCity = dCity;
	}
	public String getdPost() {
		return dPost;
	}
	public void setdPost(String dPost) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, Base64DecodingException {
		String decrpytdPost = OliveAes256.decrpyt(dPost);
		this.dPost = decrpytdPost;
	}
	public String getdAddress() {
		return dAddress;
	}
	public void setdAddress(String dAddress) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, Base64DecodingException {
		String decrpytdAddress = OliveAes256.decrpyt(dAddress);
		this.dAddress = decrpytdAddress;
	}
	public String getdAddress2() {
		return dAddress2;
	}
	public void setdAddress2(String dAddress2) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, Base64DecodingException {
		String decrpytdAddress2 = "";
		if(dAddress2 != null && !dAddress2.trim().equals("")){
			decrpytdAddress2 = OliveAes256.decrpyt(dAddress2);
			
		}
		
		this.dAddress2 = decrpytdAddress2;
	}
	public String getdTel() {
		return dTel;
	}
	public void setdTel(String dTel) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, Base64DecodingException {
		String decrpytdTel = OliveAes256.decrpyt(dTel);
		/*
		String middleMask = decrpytdTel.substring(3, decrpytdTel.length()-3);
		String masking = "";
		
		for(int i=0; i<middleMask.length(); i++){
			masking += "*";
		}
		decrpytdTel = decrpytdTel.substring(0,3)
				+ masking
				+ decrpytdTel.substring(decrpytdTel.length()-3, decrpytdTel.length());
		*/
		
		this.dTel = decrpytdTel;
	}
	public String getdCustNm() {
		return dCustNm; 
	}
	public void setdCustNm(String dCustNm) {
		/*
		String middleMask = dCustNm.substring(3, dCustNm.length()-3);
		String masking = "";
		
		for(int i=0; i<middleMask.length(); i++){
			masking += "*";
		}
		dCustNm = dCustNm.substring(0,3)
				+ masking
				+ dCustNm.substring(dCustNm.length()-3, dCustNm.length());
		*/
		this.dCustNm = dCustNm;
	}
	
	public String getdProvince() {
		return dProvince;
	}
	public void setdProvince(String dProvince) {
		this.dProvince = dProvince;
	}
	
	public String getOrdId() {
		return ordId;
	}
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	public String getInvalidYn() {
		return invalidYn;
	}
	public void setInvalidYn(String invalidYn) {
		this.invalidYn = invalidYn;
	}
	
	
	
}
