package com.logisall.winus.wmsop.web;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.logisall.winus.wmsop.service.WMSOP940Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP940Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSOP940Service")
	private WMSOP940Service service;

	@Resource(name = "WMSOP642Service")
	private WMSOP642Service WMSOP642_service;
	
	//EAI 통신용
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	/**
	 * Method ID : WMSOP940
	 * Method 설명 : 코텍다스 인터페이스 관리 페이지 연결
	 * 작성자 : 
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WINUS/WMSOP940.action")
	public ModelAndView wmsif402(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP940");
	}
	
		
	/**
	 * Method ID : listE01
	 * Method 설명 : DAS 주문결과 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 **/
	
	@RequestMapping("/WMSOP940/listE01.action")
	public ModelAndView listE01(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	//E01
	/**
	 * Method ID	: sendProductInfoIF
	 * Method 설명	: 다스 코텍전자  송장 추가 접수 결과 전송
	 * 작성자			: 
	 * @param   model
	 * @return  
	 */
	
	@RequestMapping("/WMSOP940/sendProductInfoIF.action")
	public ModelAndView sendProductInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			Map<String, Object> modelIns = new HashMap<String,Object>();
			Map<String, Object> WMSOP940Model = new HashMap<String, Object>();
			int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
			String invcNo = "";
			//String parcelYn = model.get("PARCEL_YN").toString();
			//String parcelPrevYn = model.get("PARCELPREV_YN").toString();

			// time set
			//long beforeTime = System.currentTimeMillis();
			//errLog = "택배 송장 발행";
			//택배 송장 발행
			for(int i = 0 ; i < tmpCnt; i++){
				WMSOP940Model.put("LC_ID"+i, (String)request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
				WMSOP940Model.put("CUST_ID"+i, (String)model.get("CUST_ID"));
				WMSOP940Model.put("TRACKING_NO"+i, (String)model.get("BOX_NO"+i));//박스번호
				WMSOP940Model.put("DENSE_FLAG"+i, (String)model.get("DENSE_FLAG"+i)); //송장단위로 그룹 넘버링
				WMSOP940Model.put("ORD_QTY"+i, (String)model.get("ITEM_QTY"+i)); //박스에 담기는 수량
				WMSOP940Model.put("ORD_ID"+i, (String)model.get("ORDER_CD"+i));
				WMSOP940Model.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
				WMSOP940Model.put("SET_INVC_NO"+i, (String)model.get("INVO_NO"+i)); //송장
				WMSOP940Model.put("PARCEL_BOX_TY"+i, (String)model.get("PARCEL_BOX_TY"+i)); //박스코드 (기준코드 : PARCEL_BOX      택배박스구분(CJ) 참조)
		    }
			WMSOP940Model.put("selectIds", (String)model.get("selectIds"));
			WMSOP940Model.put("PARCEL_SEQ_YN", "Y"); //'Y' : 고정
			WMSOP940Model.put("PARCEL_COM_TY", "04"); //대한통운 '04'
			WMSOP940Model.put("PARCEL_COM_TY_SEQ", "1"); // wmsdf001의 대한통운의 순번
			WMSOP940Model.put("PARCEL_ORD_TY", "01"); // '01'
			WMSOP940Model.put("PARCEL_PAY_TY", "03"); //신용 : 03
			WMSOP940Model.put("PARCEL_ETC_TY", "01"); //''
			WMSOP940Model.put("PARCEL_PAY_PRICE", "");
			WMSOP940Model.put("ADD_FLAG", "N"); //'N' : 고정
			WMSOP940Model.put("DIV_FLAG", "DIV"); //'DIV' : 고정
		   
			WMSOP940Model.put("SS_CLIENT_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			WMSOP940Model.put("SS_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		   
		    String hostUrl = request.getServerName();
		    WMSOP940Model.put("hostUrl", hostUrl);
		   
		    //System.out.println(WMSOP940Model);
		    m = WMSOP642_service.DlvInvcNoOrderCommTotal(WMSOP940Model);
		    
			//m = WMSOP642_service.DlvInvcNoOrderCommTotal(WMSOP642Model);
            //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
            //beforeTime = System.currentTimeMillis();
			/*if(!m.get("header").toString().equals("Y")){
				m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(1) : " + m.get("message"));
         		mav.addAllObjects(m);
        		return mav;
			}*/
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
}
