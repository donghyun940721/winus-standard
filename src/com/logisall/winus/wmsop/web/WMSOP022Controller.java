package com.logisall.winus.wmsop.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsop.service.WMSOP022Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP022Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP022Service")
	private WMSOP022Service service;

 
    /**
     * Method ID   : WMSOP022
     * Method 설명    : 입고 및 라벨출력(리본굿즈)
     * 작성자               : SUMMER H
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSOP022.action")
    public ModelAndView WMSOP022(Map<String, Object> model){
        return new ModelAndView("winus/wmsop/WMSOP022");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
    
    /**
     * Method ID   : save
     * Method 설명    : 상품 검색 및 해당 상품 입고주문 처리 (완료까지, qty:1)
     * 작성자              : summer
     * @param   model
     * @return
     * @throws  Exception
     */
   	@RequestMapping("/WMSOP022/save.action")
   	public ModelAndView save(Map<String, Object> model) throws Exception {
   		ModelAndView mav = null;
   		try {
   			mav = new ModelAndView("jsonView", service.save(model));
   		} catch (Exception e) {
   			if (log.isErrorEnabled()) {
   				log.error("Fail to list :", e);
   			}
   		}
   		return mav;
   	}
    

}
