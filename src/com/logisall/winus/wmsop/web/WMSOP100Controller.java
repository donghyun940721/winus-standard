package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ExcelWriter;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsop.service.WMSOP100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP100Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP100Service")
	private WMSOP100Service service;

	static final String[] COLUMN_NAME_WMSOP100 = {
		  "O_SEQ"
		, "O_DLV_COM_ID"
		, "O_SALES_COMPANY_ID"
		, "O_ORG_ORD_ID"
		, "O_BUY_CUST_NM"
		, "O_BUY_PHONE_1"
		, "O_BUY_PHONE_2"
		, "O_CUSTOMER_NM"
		, "O_PHONE_1"
		, "O_PHONE_2"
		, "O_ZIP"
		, "O_ADDR"
		, "O_PRODUCT"
		, "O_DLV_ORD_TYPE"
		, "O_QTY"
		, "O_DLV_SET_DT"
		, "O_ETC1"
		, "O_MEMO"
		, "O_BUYED_DT"
		, "O_DLV_DT"
		, "O_DLV_ORD_STAT"
		, "O_PRODUCT_NM"
		, "O_SERIAL_NO"
		, "O_CASE1"
		, "O_CASE2"
		, "O_GUARANTEE_PERIOD_MM"
	};
	
	/*-
	 * Method ID    : WMSOP100
	 * Method 설명      : 엑셀주문입력
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP100.action")
	public ModelAndView WMSOP100(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100");
	}

	/*-
	 * Method ID    : WMSOP101
	 * Method 설명      : 엑셀주문입력 주문수정
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP101.action")
	public ModelAndView WMSOP101(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP101");
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 공지사항 목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP100E2
	 * Method 설명      : 배송주문 엑셀등록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E2.action")
	public ModelAndView WMSOP100E2(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E2");
	}
	
	/*-
	 * Method ID    : WMSOP100E3
	 * Method 설명      : 배송주문 일반등록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E3.action")
	public ModelAndView WMSOP100E3(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E3");
	}
	
	/*-
	 * Method ID    : saveE2
	 * Method 설명      : 엑셀업로드
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("WMSOP100E2/save.action")
	public ModelAndView saveE2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP100/uploadExcelInfo.action")
	public ModelAndView uploadExcelInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP100 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP100, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSOP100, 0, startRow, 10000, 0);
			m = service.saveExcelInfo(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : excelDownCustom
	 * Method 설명   : Type별 주문정보 엑셀다운로드
	 */
	@RequestMapping("/WMSOP100/excelDownCustom.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDownCustom(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownCustom(response, grs, (String)model.get("uploadTitle"), (String)model.get("custCd"));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDownCustom(HttpServletResponse response, GenericResultSet grs, String uploadTitle, String custCd) {
        try{
            if(custCd.equals("GISAN")){
            	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
                                       {MessageResolver.getMessage("발주일자")		, "0", "0", "0", "0", "200"},
                                       {MessageResolver.getMessage("구분")		, "1", "1", "0", "0", "200"},
                                       {MessageResolver.getMessage("인수자")		, "2", "2", "0", "0", "200"},
                                       {MessageResolver.getMessage("제품명")		, "3", "3", "0", "0", "200"},
                                       {MessageResolver.getMessage("색상")		, "4", "4", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("수량")		, "5", "5", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송비-선불")	, "6", "6", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송비-착불")	, "7", "7", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송일")		, "8", "8", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송기사")		, "9", "9", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("주소")		, "10", "10", "0", "0", "200"},
                                       {MessageResolver.getMessage("전화번호")		, "11", "11", "0", "0", "200"},
                                       {MessageResolver.getMessage("휴대번호")		, "12", "12", "0", "0", "200"},
                                       {MessageResolver.getMessage("기타사항")		, "13", "13", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송예정일")	, "14", "14", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("쇼핑몰명")		, "15", "15", "0", "0", "200"},
                                       {MessageResolver.getMessage("구성품")		, "16", "16", "0", "0", "200"},
                                       {MessageResolver.getMessage("주문번호")		, "17", "17", "0", "0", "200"},
                                       {MessageResolver.getMessage("쇼핑몰발주번호")	, "18", "18", "0", "0", "200"},
                                       {MessageResolver.getMessage("통행료")		, "19", "19", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("외곽추가요금")	, "20", "20", "0", "0", "200"}
                                      };
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
                                        {"BUYED_DT"           	, "S"},
                                        {"WORK_STAT"           	, "S"},
                                        {"SALES_CUST_NM"       	, "S"},
                                        {"PRODUCT"          	, "S"},
                                        {""          			, "S"},
                                        
                                        {"QTY"          		, "S"},
                                        {""          			, "S"},
                                        {""          			, "S"},
                                        {"DLV_DT"          		, "S"},
                                        {""          			, "S"},
                                        
                                        {"ADDR"          		, "S"},
                                        {"PHONE_1"          	, "S"},
                                        {"PHONE_2"          	, "S"},
                                        {"ETC1"          		, "S"},
                                        {"DLV_REQ_DT"          	, "S"},
                                        
                                        {"SALES_COMPANY_NM"     , "S"},
                                        {""          			, "S"},
                                        {"ORG_ORD_ID"          	, "S"},
                                        {""          			, "S"},
                                        {""          			, "S"},
                                        
                                        {""          			, "S"}
                                       }; 
                
    			// 파일명
    			String fileName = MessageResolver.getText(uploadTitle);
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";

    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
            }else if(custCd.equals("HOWSER")){
            	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
                						{MessageResolver.getMessage("자사주문번호")		, "0", "0", "0", "0", "200"},               
                						{MessageResolver.getMessage("수취인명")		, "1", "1", "0", "0", "200"},
                                        {MessageResolver.getMessage("수취인연락처1")		, "2", "2", "0", "0", "200"},
                                        {MessageResolver.getMessage("수취인연락처2")		, "3", "3", "0", "0", "200"},
                                        {MessageResolver.getMessage("수취인인주소")		, "4", "4", "0", "0", "200"},
                		
                                        {MessageResolver.getMessage("판매처")			, "5", "5", "0", "0", "200"}, 
                                        {MessageResolver.getMessage("의뢰타입")		, "6", "6", "0", "0", "200"},
                                        {MessageResolver.getMessage("메시지")			, "7", "7", "0", "0", "200"},
                                        {MessageResolver.getMessage("현장수금액")		, "8", "8", "0", "0", "200"},
                                        {MessageResolver.getMessage("입고예정일")		, "9", "9", "0", "0", "200"},
                                        
                                        {MessageResolver.getMessage("고객계약확인서")	, "10", "10", "0", "0", "200"},
                                        {MessageResolver.getMessage("상코드")			, "11", "11", "0", "0", "200"},
                                        {MessageResolver.getMessage("수량")			, "12", "12", "0", "0", "200"}
                                      };
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
                						{"SALES_CUST_ID"       	, "S"},
                						{"SALES_CUST_NM"        , "S"},
                                        {"PHONE_1"          	, "S"},
                                        {"PHONE_2"          	, "S"},
                                        {"ADDR"          		, "S"},
                                        
                                        {"SALES_COMPANY_NM"     , "S"},
                                        {"WORK_STAT"          	, "S"},
                                        {"ETC1"     			, "S"},
                                        {""          			, "S"},
                                        {""          			, "S"},
                                        
                                        {""          			, "S"},
                                        {"PRODUCT"          	, "S"},
                                        {"QTY"          		, "S"}
                                       }; 
                
    			// 파일명
    			String fileName = MessageResolver.getText(uploadTitle);
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";
    			
    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            }
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : keyInSaveInfo
	 * Method 설명      : 배송주문 일반저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/keyInSaveInfo.action")
	public ModelAndView keyInSaveInfo(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.keyInSaveInfo(model, COLUMN_NAME_WMSOP100);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP102
	 * Method 설명      : 배송주문등록(H/D)
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP102.action")
	public ModelAndView WMSOP102(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP102");
	}
	
	@RequestMapping("/WMSOP102E2.action")
	public ModelAndView WMSOP102E2(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP102E2");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP102/uploadExcelInfoMulti.action")
	public ModelAndView uploadExcelInfoMulti(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP100 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP100, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSOP100, 0, startRow, 10000, 0);
			m = service.saveExcelInfoMulti(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
}
