package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP920Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP920Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	static final String[] COLUMN_NAME_WMSOP920 = {"ORG_ORD_ID", "INVC_NO", "TK_NO", "ORD_ID"};

	@Resource(name = "WMSOP920Service")
	private WMSOP920Service service;
	
	/*-
	 * Method ID    : WMSOP920
	 * Method 설명      : 입/출고(통합)화면
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP920.action")
	public ModelAndView WMSOP920(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP920",  service.selectData(model));
	}
	
	/*-
	 * Method ID    : listE1
     * Method 설명      : 입고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP920/listE1.action")
	public ModelAndView listE1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE2
     * Method 설명      : 출고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP920/listE2.action")
	public ModelAndView listNew(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE3
	 * Method 설명      : 화주별 출고관리 조회(OM)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920/listE3.action")
	public ModelAndView listE3(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE5
	 * Method 설명      : 부분입고 관리
	 * 작성자                 : YOSEOP
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920/listE5.action")
	public ModelAndView listE5(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE5(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE4Header
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920/listE4Header.action")
	public ModelAndView listE4Header(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4Header(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE4Detail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920/listE4Detail.action")
	public ModelAndView listE4Detail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4Detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE1SummaryCount
	 * Method 설명      : 입출고통합관리 -> 입고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920/listE1SummaryCount.action")
	public ModelAndView listE1SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE1SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE2SummaryCount
	 * Method 설명      : 입출고통합관리 -> B2B 출고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920/listE2SummaryCount.action")
	public ModelAndView listE2SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE2SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE3SummaryCount
	 * Method 설명      : 입출고통합관리 -> B2C 출고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920/listE3SummaryCount.action")
	public ModelAndView listE3SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE3SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}


	/*-
	 * Method ID : inspectionList
	 * Method 설명 : 검수내역 조회 화면(올리브영)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP920pop3.action")
	public ModelAndView inspectionListView(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP920pop3");
	}
	
	
	/*-
	 * Method ID    : /WMSOP920pop4(330)
	 * Method 설명      : 올리브영 주소 변경 이력 조회 팝업
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920pop4.action")
	public ModelAndView WMSOP920pop4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP920pop4");
	}
	
	/*-
	 * Method ID    : /WMSOP920pop(330)
	 * Method 설명      : 
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920pop.action")
	public ModelAndView WMSOP920pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP920pop");
	}
	/*-
	 * Method ID    : /WMSOP920pop2(330)
	 * Method 설명      : 
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920pop2.action")
	public ModelAndView WMSOP920pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP920pop2");
	}
	/*-
	 * Method ID	: /WMSOP920popSplmtList
	 * Method 설명	: 보충지시서 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP920popSplmtList.action")
	public ModelAndView WMSOP920popSplmtList(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP920popSplmtList");
	}

	@RequestMapping("/WMSOP920E2_TP.action")
	public ModelAndView wmsop920E2_TP(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP920E2_TP");
	}

	@RequestMapping("/WMSOP920/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP920, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSOP920, 0, startRow, 10000, 0);
			
			for(int i =0 ; i<list.size() ; i++){
				Map<String, Object> hashMap = new HashMap<String, Object>();
				System.out.println(list);
				hashMap = list.get(i);
				int rowNum = i+2;

				String fieldOrgOrdId = (String) hashMap.get(COLUMN_NAME_WMSOP920[0]);
				if(fieldOrgOrdId == null || fieldOrgOrdId.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP920[0]+"(원주문번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldInvcNo = (String) hashMap.get(COLUMN_NAME_WMSOP920[1]);
				if(fieldInvcNo == null || fieldInvcNo.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP920[1]+"(송장번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldTkNo = (String) hashMap.get(COLUMN_NAME_WMSOP920[2]);
				if(fieldTkNo == null || fieldTkNo.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP920[2]+"(추적번호)는 필수 값입니다. 라인 : "+rowNum);
				}
			}
			
			m = service.saveCsv(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP920/outInvalidView.action")
    public ModelAndView deviceInvalidView(Map<String, Object> model) {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jsonView", service.outInvalidView(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get Manage Code :", e);
            }
        }
        return mav;
    }
}