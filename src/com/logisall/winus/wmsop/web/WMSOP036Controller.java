package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.Provider.Service;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP036Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP036Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP036Service")
	private WMSOP036Service service;
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service ifService;
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP036.action")
	public ModelAndView wmsop035(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP036");
	}
	
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리V2 화면
	 * 작성자                 : kih
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP036_V2.action")
	public ModelAndView wmsop035V2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP036_V2");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036pop1.action")
	public ModelAndView wmsop035pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP036pop1");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036pop2.action")
	public ModelAndView wmsop035pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP036pop2");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036pop3.action")
	public ModelAndView wmsop035pop3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP036pop3");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036pop4.action")
	public ModelAndView wmsop035pop4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP036pop4");
	}
	
	/*-
	 * Method ID    : wmsop035pop6
	 * Method 설명      :  송장조회(올리브영) -> 운송장 출력여부 조회 -> 운송장 출력 이력 조회 팝업
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036pop6.action")
	public ModelAndView wmsop035pop6(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP036pop6");
	}
	
	/*-
	 * Method ID    : wmsop035pop7
	 * Method 설명      :  송장조회(올리브영) -> 미전송 송장조회
	 * 작성자                 : wl2258
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036pop7.action")
	public ModelAndView wmsop035pop7(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/wmsop035pop7");
	}
	
	/*-
	 * Method ID    	: list
	 * Method 설명      : 송장조회(올리브영) 조회
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/listMain.action")
	public ModelAndView listMain(Map<String, Object> model) throws Exception {
		ModelAndView mav = null; 
		
		try {
		

			mav = new ModelAndView("jqGridJsonView", service.list(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}

		return mav;
		
	}		
	
	/*-
	 * Method ID    : 
	 * Method 설명      :  조회 카운트
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/listMainCount.action")
	public ModelAndView listMainCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listMainCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : searchAddressValide
	 * Method 설명      : 출고주문주소정보 조회
	 * 작성자                 : HYS
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/searchAddressValide.action")
	public ModelAndView searchAddressValide(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jqGridJsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.searchAddressValide(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : updateAddress
	 * Method 설명   : 주소변경
	 * 작성자         : HYS
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/updateAddress.action")
	public ModelAndView updateAddressOlive(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateAddress(model);
			
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : insertShipment
	 * Method 설명      : 송장조회(올리브영) EMS, DHL 송장접수 (현장용)
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/addressInvalid.action")
	public ModelAndView addressInvalid(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			
			Map<String, Object> map = new HashMap<>();
			
			String cMethod		= "POST";
			String cUrl			= "WEVERSE/ADDRESSVALIDATE";
//			String cUrl			= "OLIVE2/SHIPPING_REQUEST_TEST";
			String pOrdId 		= "";
			
			map.put("cMethod"		, cMethod);
			map.put("cUrl"		    , cUrl);
			map.put("pOrdId"		, pOrdId);
				
			m = ifService.crossDomainHttpWs5400New(map);
			
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : orderCompleteUpdate
	 * Method 설명   : 출고확정
	 * 작성자         : HYS
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/orderCompleteUpdate.action")
	public ModelAndView orderCompleteUpdate(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.orderCompleteUpdate(model);
			
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    	: list2
	 * Method 설명      : 송장조회(DHL WEVERSE) 조회
	 * 작성자           : HYS
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/list2.action")
	public ModelAndView list2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> errMsg = new HashMap<>();	
		Map<String, Object> errMsg2 = new HashMap<>();
		try {
		
			m = service.selectOutOrder(model);
	        int listSize = 0;
	        if (m.containsKey("LIST")) {
	            listSize = ((List<?>) m.get("LIST")).size();
	        }
			
			
	        //주문있음
			if(listSize > 0) {
		
		 
				String rstJon = m.get("LIST").toString();
		        JsonParser parser = new JsonParser();
		        JsonArray jsonArry = (JsonArray)parser.parse(rstJon);
		        
		        JsonObject jsonObject = jsonArry.get(0).getAsJsonObject();
		        //주소API 검증 유무 N:검증완료,else 미검증
		        String addressInvalidYn = jsonObject.get("ADDRESS_VALID_CODE").getAsString();
		        // Y DHL 송장 등록완료, N 송장 없음
		        String invoiceYn = jsonObject.get("INVOICE_YN").getAsString();
		        // 송장 출력 횟수 > 0 이면 한번이라도 출력된 이력이 있음
		        int printCnt = Integer.parseInt(jsonObject.get("PRINT_CNT").getAsString());
		        
		        //100 주문등록, 460 검수완료 , 990 출고완료
		        String workState = jsonObject.get("ORD_PRGRS_STAT").getAsString();

		        //주소유효성 검사 완료 if#1
				if(addressInvalidYn.equals("N")) {
		
					//발급된 송장이 있음 if#2
					if(invoiceYn.equals("Y")) {
						//if#3 송장이 출력된 이력이 있음(첫 출력이 아니어서 재출력 alert)
						if(printCnt > 0) {
							
							
						// if#3 송장 출력된적없음(첫출력)	
						}else {
							
							
						}
				
						jsonObject.addProperty("IFRESULT", "S");					
						jsonObject.addProperty("IFMESSAGE", "");	
						errMsg.put("DT", jsonObject.toString());
						
						mav.addAllObjects(errMsg);
						
						
					//송장발급안됌	if#2
					}else {
						
							Map<String, Object> map = new HashMap<>();
							
							String cMethod		= "POST";
							String cUrl			= "WEVERSE/CREATESHIPMENT";
							String pOrdId 		= (String)model.get("vrSrchOrgOrdId2");
							
							map.put("cMethod"		, cMethod);
							map.put("cUrl"		    , cUrl);
					
							String jsonInputString = "{\"ORG_ORD_ID\":\""+pOrdId+"\"}";
							System.out.println("param jsonInputString : " + jsonInputString);
							
							map.put("data"	, jsonInputString);
						    
								
							m = ifService.crossDomainHttpWs5400New2(map);
							
						
							String rstJson = m.get("RST").toString();
					        JsonParser parser2 = new JsonParser();
					        JsonElement element = parser2.parse(rstJson);
					        JsonObject jsonObject2 = element.getAsJsonObject();
					        JsonObject output = jsonObject2.getAsJsonObject("OUTPUT");
	
					        String ifResult = output.get("IFRESULT").getAsString();
					        String ifMessage = output.get("IFMESSAGE").getAsString();
					       
					        
					        System.out.println("IFRESULT: " + ifResult);
					        System.out.println("IFMESSAGE: " + ifMessage);
							
					        
					       // JsonObject jsonObject3 = new JsonObject();
							//jsonObject3.addProperty("IFRESULT", ifResult);
							//jsonObject3.addProperty("IFMESSAGE", ifMessage);
							//errMsg.put("DT", jsonObject3.toString());
					        
					        if(ifResult.equals("S")) {					    
					        	 String waybillNo = output.get("BL_NO").getAsString();	
								jsonObject.addProperty("IFRESULT", "S");					
								jsonObject.addProperty("IFMESSAGE", "");	
								jsonObject.addProperty("WAYBILL_NO", waybillNo);	
								errMsg.put("DT", jsonObject.toString());
								
								mav.addAllObjects(errMsg);
					       
					        	
					        }else {
								jsonObject.addProperty("IFRESULT", "E");					
								jsonObject.addProperty("IFMESSAGE", ifMessage);	
								errMsg.put("DT", jsonObject.toString());
								
								mav.addAllObjects(errMsg);
					        	
					        }
					        
					}
					
					
		
				//주소유효성 검증 미완료 
				}else {
					jsonObject.addProperty("IFRESULT", "E");					
					jsonObject.addProperty("IFMESSAGE", "주소검증이 완료되지 않은 주문입니다.");	
					errMsg.put("DT", jsonObject.toString());
					
					mav.addAllObjects(errMsg);
				}
				
			//주문없음
			}else {
				
				System.out.println("주문없어!");
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("IFRESULT", "E");
				jsonObject.addProperty("IFMESSAGE", "해당 주문이 존재하지 않습니다");
				
				errMsg.put("DT", jsonObject.toString());
				mav.addAllObjects(errMsg);

				return mav;
				
				
			}
			
			
			
	        

		
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("RST", MessageResolver.getMessage("save.error"));
			mav.addAllObjects(m);
		}
		
		return mav;
		
	}	
	
	
	   /*-
	    * Method ID        : invoicePrintingDHL
	    * Method 설명       :  DHL 송장 출력  (검수확정 -> 프린트시간 업데이트 -> PDF 출력)
	    * 작성자            : HYS
	    * @param   model
	    * @return  
	    */
	   @RequestMapping("/WMSOP036/invoicePrintingDHL.action")
	   public ModelAndView  invoicePrintingDHL(Map<String, Object> model) throws Exception { // @ResponseBody byte[]
	      ModelAndView mav = new ModelAndView("jsonView");
	      
	      Map<String, Object> m = new HashMap<String, Object>();
	      try {
	    	    if(model.get("WORK_STAT").equals("100")){ // 출고확정상태가 아닐 경우
	    	    
	    	    	
					m.put("scanUpdate", service.scanUpdate(model)); // 단건 출고확정 처리	
				}
	    	  
	    	  	m = service.updatetPrintDt(model); // 송장 출력 기록 저장
	    	  	m.put("pdfData", encodeBase64toString(service.pdfDown(model, "DHL")));
	      }
	        catch (Exception e) {
	         if (log.isErrorEnabled()) {
	            log.error("Fail to invoicePrintingDHL :"+ e.getMessage());
	         }
	      }
	      
	      mav.addAllObjects(m);
	      return mav;
	   }
	   
	   
		
		/*-
		 * Method ID    	: multiInsertShipment
		 * Method 설명      : 송장 일괄접수
		 * 작성자           : HYS
		 * @param   model
		 * @return  
		 * @throws Exception 
		 */
		@RequestMapping("/WMSOP036/multiInsertShipment.action")
		public ModelAndView multiInsertShipment(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
			ModelAndView mav = new ModelAndView("jsonView");
			Map<String, Object> m = new HashMap<String, Object>();
			try {
				StringBuilder strbd = new StringBuilder();
				int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	            if(tmpCnt > 0){
	                for(int i = 0 ; i < tmpCnt ; i ++){
	                	Map<String, Object> map = new HashMap<>();
	        			String cMethod			= "POST";
	        			String cUrl				= "WEVERSE/CREATESHIPMENT";
//	        			String cUrl			= "OLIVE2/SHIPPING_REQUEST_TEST";
	        			map.put("cMethod"		, cMethod);
	        			map.put("cUrl"			, cUrl);
	        			
	        			String jsonInputString = "{\"ORG_ORD_ID\":\""+ (String) model.get("ORG_ORD_NO_"+i) +"\"}";
	        			
	        			map.put("data"	, jsonInputString);
	        		    
	        			strbd.append(i+1).append("번 ").append((String) model.get("ORG_ORD_NO_"+i)).append("\n").append(ifService.crossDomainHttpWs5400New2(map).get("RST")).append("\n");
	                }
	            }else{
	        		m.put("MSG", MessageResolver.getMessage("save.error"));
	    			m.put("errCnt", "1");
	            }
				System.out.println(strbd.toString());
	            m.put("RST",strbd.toString());
				mav.addAllObjects(m);
				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to insert info :", e);
				}
				m = new HashMap<String, Object>();
				m.put("MSG", MessageResolver.getMessage("save.error"));
				m.put("MSG_ORA", e.getMessage());
				m.put("errCnt", "1");
				mav.addAllObjects(m);
			}
			return mav;
		}
	
		
	    @RequestMapping("/WMSOP036/selectReissueInvoiceList.action")
	    public ResponseEntity<Map<String, Object>> selectReissueInvoiceList(Map<String, Object> model) {
			Map<String, Object> resultMap = new HashMap<>();
			resultMap = service.selectReissueInvoiceList(model);
		  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
		}
		
		/*-
		 * Method ID    : list2DHL
		 * Method 설명      : 수출신고정보
		 * 작성자                 : HYS
		 * @param   model
		 * @return  
		 * @throws Exception 
		 */
		@RequestMapping("/WMSOP036/list2DHL.action")
		public ModelAndView list2DHL(Map<String, Object> model) throws Exception {
			ModelAndView mav = null;
			try {
				System.out.println(model.get("vrSrchlcid").toString());
		
				mav = new ModelAndView("jqGridJsonView", service.list2DHL(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to list :", e);
				}
			}
			return mav;
		}
		
		@RequestMapping("/WMSOP036/selectDailyWork.action")
		public ModelAndView selectDailyWork(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				mav = new ModelAndView("jqGridJsonView", service.selectDailyWork(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}
	/*-
	 * Method ID    : list2
	 * Method 설명      : 출고관리  조회
	 * 작성자                 : KSJ 
	 * 같은 uri 이거 왜 두개임..?
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
//	@RequestMapping("/WMSOP036/list2.action")
//	public ModelAndView list2(Map<String, Object> model) throws Exception {
//		ModelAndView mav = null;
//
//		try {
//			Map<String, Object> searchResult = service.list(model);
//			mav = new ModelAndView("jqGridJsonView", searchResult);
//			// mav.getModel().get("LIST").
////			if(searchResult.get("LIST").)
//			// mav.addObject("PrintResut", )
//			
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get list :", e);
//			}
//		}
//		return mav;
//	}
	/*-
	 * Method ID    : searchUnshipped
	 * Method 설명      : 올리브영 미발행 내역 조회
	 * 작성자                 : chSong -> KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/searchUnshipped.action")
	public ModelAndView searchUnshipped(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jqGridJsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.searchUnshipped(model);
			//m.put("totCnt", 10);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : getCustIdByLcId
	 * Method 설명 : LC_ID로 해당 코드의 화주ID 가져오기
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 
	@RequestMapping("/WMSOP036/list5.action")
	public ModelAndView getCustIdByLcId(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
*/

	/*-
	 * Method ID : poplist
	 * Method 설명 : 세트상품 조회 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	/*
	@RequestMapping("/WMSOP036/pdfDown.action")
	public @ResponseBody byte[] pdfDown(Map<String, Object> model) throws Exception {
		model.put("ordIds", ((String)model.get("ordIds")).replace("&apos;", "'"));
		return service.pdfDown(model);
	}
	*/
	
	
	/*-
	 * Method ID    : 
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/outSaveComplete.action")
	public ModelAndView outSaveComplete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.outSaveComplete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    		: invoicePrintingEMS
	 * Method 설명       :  EMS 송장 출력  (출고확정 -> 프린트시간 업데이트 -> PDF 출력)
	 * 작성자                : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/invoicePrintingEMS.action")
	public ModelAndView invoicePrintingEMS(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			if(!model.get("WORK_STAT").equals("990")){ // 출고확정상태가 아닐 경우
				m.put("outSaveComplete", service.outSaveCompleteV2(model)); // 단건 출고확정 처리	
			}
			
			m.put("updatetPrintDt", service.updatetPrintDt(model));
			m.put("emsAddress", service.emsAddress(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("ERR_MSG", e.getMessage());
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    		: invoicePrintingEMS
	 * Method 설명       :  EMS 송장 출력  (출고확정 -> 프린트시간 업데이트 -> PDF 출력)
	 * 작성자                : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/invoicePrintingPantos.action")
	public ModelAndView invoicePrintingPantos(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			if(!model.get("WORK_STAT").equals("990")){ // 출고확정상태가 아닐 경우
				m.put("outSaveComplete", service.outSaveCompleteV2(model)); // 단건 출고확정 처리	
			}
			
			m.put("updatetPrintDt", service.updatetPrintDt(model));
//			m.put("emsAddress", service.emsAddress(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("ERR_MSG", e.getMessage());
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
    /**
	 * encodeBase64toString
	 * @param data
	 * @return
	 */
	private static String encodeBase64toString(byte[] data) {
		String result = "";
		Base64 base64encoder = new Base64();
		try {
			result = new String(base64encoder.encode(data),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	


	   /*-
	    * Method ID        : invoicePrintingFedex
	    * Method 설명       :  DHL 송장 출력  (출고확정 -> 프린트시간 업데이트 -> PDF 출력)
	    * 작성자            : KSJ
	    * @param   model
	    * @return  
	    */
	   @RequestMapping("/WMSOP036/invoicePrintingFedex.action")
	   public ModelAndView  invoicePrintingFedex(Map<String, Object> model) throws Exception { // @ResponseBody byte[]
	      ModelAndView mav = new ModelAndView("jsonView");
	      
	      Map<String, Object> m = new HashMap<String, Object>();
	      try {
	    	  /**
	    	    if(!model.get("WORK_STAT").equals("990")){ // 출고확정상태가 아닐 경우
					m.put("outSaveComplete", service.outSaveCompleteV2(model)); // 단건 출고확정 처리	
				}
	    	  **/
	    	  	m = service.updatetPrintDt(model); // 송장 출력 기록 저장
	    	  	m.put("pdfData", encodeBase64toString(service.pdfDown(model, "FEDEX")));
	      }
	        catch (Exception e) {
	         if (log.isErrorEnabled()) {
	            log.error("Fail to invoicePrintingFedex :"+ e.getMessage());
	         }
	      }
	      
	      mav.addAllObjects(m);
	      return mav;
	   }	   
	
	/*-
	 * Method ID    		: invoicePrintingQXP
	 * Method 설명       :  QXP 송장 출력  (출고확정 -> 프린트시간 업데이트 -> PDF 출력)
	 * 작성자                : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/invoicePrintingQXP.action")
	public ModelAndView  invoicePrintingQXP(Map<String, Object> model) throws Exception { // @ResponseBody byte[]
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			if(!model.get("WORK_STAT").equals("990")){ // 출고확정상태가 아닐 경우
				m.put("outSaveComplete", service.outSaveCompleteV2(model)); // 단건 출고확정 처리	
			}
			
			if(model.get("requestYn").equals("Y")){
				Map<String, Object> map = new HashMap<>();
				String cMethod		= "POST";
				String cUrl				= "QXPRESS/QXPRESS_PDF_REQUEST";
				map.put("cMethod"		, cMethod);
				map.put("cUrl"				, cUrl);
				
				String jsonInputString = "{\"BL_NO\":\""+model.get("BL_NO")+"\",\"ORG_ORD_ID\":\""+ model.get("ORG_ORD_ID") +"\"}";
				System.out.println("param jsonInputString : " + jsonInputString);
				
				map.put("data"	, jsonInputString);
			    
				ifService.crossDomainHttpWs5400New2(map);
			}
		
			m.put("updatetPrintDt", service.updatetPrintDt(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to invoicePrintingDHL :"+ e.getMessage());
			}
		}
		m.put("pdfData", encodeBase64toString(service.pdfDown(model, "QXP")));
		mav.addAllObjects(m);
		
		return mav;

	}
	
	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP036/dhlExcel.action")
	public void excelDown2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.dhlExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	

	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
       try{
    	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
        //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
    	   String[][] headerEx = {
		                		 {MessageResolver.getText("예약구분")				, "0", "0", "0", "0", "100"}
	                            ,{MessageResolver.getText("집하예정일")				, "1", "1", "0", "0", "100"}
	                            ,{MessageResolver.getText("보내는사람")				, "2", "2", "0", "0", "100"}
	                            ,{MessageResolver.getText("보내는분연락처")			, "3", "3", "0", "0", "100"}
	                            ,{MessageResolver.getText("판매자 연락처")			, "4", "4", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("보내는분우편번호")			, "5", "5", "0", "0", "100"}
	                            ,{MessageResolver.getText("택배 처리점소명")			, "6", "6", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분성명")				, "7", "7", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분전화번호")			, "8", "8", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분기타연락처")			, "9", "9", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("받는분우편번호")			, "10", "10", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분주소(전체및분할)")		, "11", "11", "0", "0", "100"}
	                            ,{MessageResolver.getText("운송장번호")				, "12", "12", "0", "0", "100"}
	                            ,{MessageResolver.getText("고객주문번호")			, "13", "13", "0", "0", "100"}
	                            ,{MessageResolver.getText("품목명")				, "14", "14", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("수량")					, "15", "15", "0", "0", "100"}
	                            ,{MessageResolver.getText("아이스팩 추가 개수")			, "16", "16", "0", "0", "100"}
	                            ,{MessageResolver.getText("박스수량")				, "17", "17", "0", "0", "100"}
	                            ,{MessageResolver.getText("박스타입")				, "18", "18", "0", "0", "100"}
	                            ,{MessageResolver.getText("기본운임")				, "19", "19", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("배송메세지1")			, "20", "20", "0", "0", "100"}
	                            ,{MessageResolver.getText("배송메세지2")			, "21", "21", "0", "0", "100"}
	                            ,{MessageResolver.getText("운임구분")				, "22", "22", "0", "0", "100"}
	                            ,{MessageResolver.getText("총중량")				, "23", "23", "0", "0", "100"}
                              };
        //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
    	   String[][] valueName = {
		                		 {"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"SALES_CUST_NM_FROM"	, "S"}
	                            ,{"PHONE_2_FROM"		, "S"}
	                            ,{"TEL"					, "S"}
	                            
	                            ,{"ISNULL"				, "S"}
	                            ,{"DEALT_BRAN_NM"		, "S"}
	                            ,{"SALES_CUST_NM_TO"	, "S"}
	                            ,{"PHONE_1"				, "S"}
	                            ,{"PHONE_2_TO"			, "S"}
	                            
	                            ,{"ISNULL"				, "S"}
	                            ,{"ADDR"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"SALES_CUST_NO"		, "S"}
	                            ,{"ITEM_NAME"			, "S"}
	                            
	                            ,{"ORD_QTY"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            
	                            ,{"ORD_DESC"			, "S"}
	                            ,{"ETC1"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"SUM_REAL_OUT_WEIGHT"	, "S"}
                               }; 
                
    			// 파일명
                String fileName = MessageResolver.getText("CJ대한통운");
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";

    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	

	
	/*-
	 * Method ID    : list2DHL
	 * Method 설명      : 수출신고정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/list2EMS.action")
	public ModelAndView list2EMS(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2EMS(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    		 : list2QXP
	 * Method 설명        : 수출신고정보
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/list2QXP.action")
	public ModelAndView list2QXP(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2QXP(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list2DHL
	 * Method 설명      : 수출신고정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/request2EMS.action")
	public ModelAndView request2EMS(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.request2EMS(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : list2DHL
	 * Method 설명      : 수출신고정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/export2EMS.action")
	public ModelAndView export2EMS(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.export2EMS(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : saveList2DHL
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/saveList2DHL.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveList2DHL(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : EMS 엑셀업로드 후 송장발급
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/saveExport2EMS.action")
	public ModelAndView saveExport2EMS(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			String[] cellName = new String[11];
			cellName[0] = "SEQ";
			cellName[1] = "EXP_DCLR_NO";
			cellName[2] = "ORG_ORD_ID";
			cellName[3] = "D_COUNTRY";
			cellName[4] = "J_CUST_NM";
			cellName[5] = "CUSTOM_CLEARANCE_NUMBER";
			cellName[6] = "D_CUST_NM";
			cellName[7] = "ACPT_DT";
			cellName[8] = "LOAD_DTY_TM_IM";
			cellName[9] = "PRICE";
			cellName[10] = "AGENCY";
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 10000, 0);
			List list = ExcelReader.excelLimitRowReadByHandler(destination, cellName, 0, startRow, 10000, 0);
			int listSize = list.size();
			for(int i = listSize - 1; i >= 0; i--){
				Object seq = ((Map)list.get(i)).get("SEQ");
				if(seq == null || "".equals((String)seq)){
					list.remove(i);
				}else{
					String ordId = service.getOrdId((String)((Map)list.get(i)).get("ORG_ORD_ID"));
					((Map)list.get(i)).put("ORD_ID", ordId);
				}
			}
			
			//run
			// m = service.saveExport2EMS(list);
			m = service.saveExport2EMS_V2(list);
			
			int listReSize = list.size();
			for(int i = 0; i < listReSize; i++){
				
				int shipmentCnt = service.getEmsShipment((String)((Map)list.get(i)).get("ORD_ID"));
				
				if(shipmentCnt == 0){
					
					Map<String, Object> map = new HashMap<>();
					String cMethod		= "POST";
					String cUrl			= "OLIVE2/SHIPPING_REQUEST_EMS";
//					String cUrl			= "OLIVE2/SHIPPING_REQUEST_TOTAL";
//					String cUrl			= "OLIVE2/SHIPPING_REQUEST_TEST";
					String pOrdId		= (String)((Map)list.get(i)).get("ORD_ID");
					map.put("cMethod"		, cMethod);
					map.put("cUrl"			, cUrl);
					map.put("pOrdId"		, pOrdId);
					
					ifService.crossDomainHttpWs5400New(map);
				}				
			}
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : address
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/emsAddress.action")
	public ModelAndView emsAddress(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.emsAddress(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : addressHistory
	 * Method 설명      : WMSOM010History 주소 변경 이력 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/addressHistory.action")
	public ModelAndView addressHistory(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.addressHistory(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : addressOM010
	 * Method 설명      : WMSOM010 원주문 최근 주소 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/addressOM010.action")
	public ModelAndView addressOM010(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.addressOM010(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : address
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/checkBlNo.action")
	public ModelAndView checkBlNo(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.checkBlNo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : address
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/updateBlNo.action")
	public ModelAndView updateBlNo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			String[] cellName = new String[2];
			cellName[0] = "ORG_ORD_ID";
			cellName[1] = "NEW_BLNO";
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 10000, 0);
			List list = ExcelReader.excelLimitRowReadByHandler(destination, cellName, 0, startRow, 10000, 0);
			int listSize = list.size();
			for(int i = listSize - 1; i >= 0; i--){
				Object seq = ((Map)list.get(i)).get("ORG_ORD_ID");
				if(seq == null || "".equals((String)seq)){
					list.remove(i);
				}else{
					String ordId = service.getOrdId((String)((Map)list.get(i)).get("ORG_ORD_ID"));
					((Map)list.get(i)).put("ORD_ID", ordId);
				}
			}
			//run
			boolean result = service.updateBlNo(list);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
			if(result){
				int listReSize = list.size();
				for(int i = 0; i < listReSize; i++){
					Map<String, Object> map = new HashMap<>();
					String cMethod		= "POST";
					String cUrl			= "WINUS.OLIVE:OLIVE_RS/TRACKING_REQUEST_TOTAL";
					String pOrdId		= (String)((Map)list.get(i)).get("ORD_ID");
					map.put("cMethod"		, cMethod);
					map.put("cUrl"		, cUrl);
					map.put("pOrdId"		, pOrdId);
					
					ifService.crossDomainHttpWsMobile(map);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : insertShipment
	 * Method 설명      : 송장조회(올리브영) EMS, DHL 송장접수 (현장용)
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/insertShipment.action")
	public ModelAndView insertShipment(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			
			Map<String, Object> map = new HashMap<>();
			
			String cMethod		= "POST";
			String cUrl			= "OLIVE2/SHIPPING_REQUEST_TOTAL";
//			String cUrl			= "OLIVE2/SHIPPING_REQUEST_TEST";
			String pOrdId 		= (String)model.get("pOrdId");
			
			map.put("cMethod"		, cMethod);
			map.put("cUrl"		    , cUrl);
			map.put("pOrdId"		, pOrdId);
				
			m = ifService.crossDomainHttpWs5400New(map);
			
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}

	
	/*-
	 * Method ID    : listE5
	 * Method 설명      : 올리브영 수출신고번호 or 운송장번호 누락 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/listE5.action")
	public ModelAndView listE5(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listE5(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE6
	 * Method 설명      : 올리브영 운송장 출력여부 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/listE6.action")
	public ModelAndView listE6(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listE6(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : selectPrintDt
	 * Method 설명      : 올리브영 운송장 출력 시간 업데이트
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/updatetPrintDt.action")
	public ModelAndView updatetPrintDt(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updatetPrintDt(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    	: selectPrintDt
	 * Method 설명      : 올리브영 운송장 출력 시간 조회
	 * 작성자           : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/selectPrintDt.action")
	public ModelAndView selectPrintDt(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			 m = service.selectPrintDt(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    	: getShippingCompanyList
	 * Method 설명      : 올리브영 특송사 리스트 
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/getShippingCompanyList.action")
	public ModelAndView ordSubTypeList(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.getShippingCompanyList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP036/transferShippingCompany.action")
	public ModelAndView transferShippingCompanyV2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			
			Map<String, Object> map = new HashMap<>();
			Map<String, Object> reqMap = new HashMap<>();
			
			String cMethod			= "POST";
			String cUrl				= "OLIVE2/SHIPPING_TRANSFER";
//			String cUrl				= "OLIVE2/SHIPPING_TRANSFER_TEST";
			String pOrdId 			= (String) model.get("ORD_ID");
			String pOrgOrdId 		= (String) model.get("ORG_ORD_ID");
			String pShippingCompany = (String) model.get("SHIPPING_COMPANY");
			String pWorkStat = (String) model.get("WORK_STAT");
			String pUpdNo 			= (String) model.get("SS_USER_NO");
			
			map.put("cMethod"				, cMethod);
			map.put("cUrl"		    		, cUrl);
			map.put("ordId"				, pOrdId);
			map.put("orgOrdId"				, pOrgOrdId);
			map.put("shippingCompany"		, pShippingCompany);
			map.put("workStat"		, pWorkStat);
			map.put("updNo"				, pUpdNo);
			reqMap.put("Request", map);
			 // ObjectMapper를 사용하여 Map을 JSON 문자열로 변환
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonInputString = objectMapper.writeValueAsString(reqMap);
			
			map.put("data"	, jsonInputString);	
			m = ifService.crossDomainHttpWs5400New2(map);
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: transferShippingCompany
	 * Method 설명      : 특송사 변환
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	/*
	@RequestMapping("/WMSOP036/transferShippingCompany.action")
	public ModelAndView transferShippingCompany(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			
			Map<String, Object> map = new HashMap<>();
			
			String cMethod			= "POST";
			String cUrl				= "WINUS.OLIVE:OLIVE_RS/SHIPPING_TRANSFER";
			String pOrdId 			= (String) model.get("ORD_ID");
			String pOrgOrdId 		= (String) model.get("ORG_ORD_ID");
			String pShippingCompany = (String) model.get("SHIPPING_COMPANY");
			String pUpdNo 			= (String) model.get("SS_USER_NO");
			
			map.put("cMethod"				, cMethod);
			map.put("cUrl"		    		, cUrl);
			map.put("pOrdId"				, pOrdId);
			map.put("pOrgOrdId"				, pOrgOrdId);
			map.put("pShippingCompany"		, pShippingCompany);
			map.put("pUpdNo"				, pUpdNo);
				
			m = ifService.crossDomainHttpWsOlive(map);
			
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	*/
	
	/*-
	 * Method ID    	: insertShipmentV2
	 * Method 설명      : 신나라 주문 접수
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP036/insertShipmentV2.action")
	public ModelAndView insertShipmentV2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod		= "POST";
			String cUrl			= "OLIVE2/SHIPPING_REQUEST_TOTAL";
//			String cUrl			= "OLIVE2/SHIPPING_REQUEST_TEST";
			String pOrdId 		= service.getOrdId((String) model.get("ORG_ORD_ID"));
			model.put("pOrdId", pOrdId);
			
			Map<String, Object> map = new HashMap<>();
			map = service.outBoundComplete(model);
			//map.put("errCnt","0");
			
			if(map.get("errCnt").equals("0")){
				map = new HashMap<>();
				map.put("cMethod"		, cMethod);
				map.put("cUrl"		    , cUrl);
				
				String jsonInputString = "{\"ordId\":\""+pOrdId+"\",\"boxQty\":\""+ model.get("BOX_QTY") +"\"}";
				System.out.println("param jsonInputString : " + jsonInputString);
				
				map.put("data"	, jsonInputString);
				m = ifService.crossDomainHttpWs5400New2(map);
				
			}else{
				m = new HashMap<String, Object>();
				m.put("MSG", MessageResolver.getMessage("save.error"));
				m.put("errCnt", "1");
			}
			
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		
		return mav;
	}
	
	
	/*-
	 * Method ID      : updateDelYn
	 * Method 설명    : 출고관리(올리브영) update delYn
	 * 작성자         : KSJ
	 * 날짜 : 2022.08.24
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/updateDelYn.action")
	public ModelAndView updateDelYn(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateDelYn(model);
			 m.put("errCnt", 0);
		        m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("ERROR :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID      : sendPickingListRobot
	 * Method 설명    : 출고관리(올리브영) LG CNS 로봇 연동 테이블 피킹리스트 insert 
	 * 작성자         : KSJ
	 * 날짜 : 2022.08.24
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/sendPickingListRobot.action")
	public ModelAndView sendPickingListRobot(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			//m = service.sendPickingListRobot(model);
			
			m = (Map<String, Object>) service.sendPickingListRobot(model);
			
			Map<String, Object> login_rst = service.loginWcs();
	    	
	    	if(login_rst.get("response").equals("success")){
				
		        String sendOrder_rst = service.sendMcsOrder(login_rst);
		        if(sendOrder_rst.equals("success")){
		        	
		        	 m.put("errCnt", 0);
				     m.put("MSG", MessageResolver.getMessage("save.success"));
		        	
		        }else{
			        m.put("errCnt", 1);
			        m.put("MSG", "wcs login successful but order sending failed");
		        }
	    		
	    	}else{
		        m.put("errCnt", 1);
		        m.put("MSG", "wcs login failed");
	    	}
			
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("ERROR :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID      : sendPickingListRobot
	 * Method 설명    : 출고관리(올리브영) LG CNS 로봇 연동 테이블 피킹리스트 cancel 요청 
	 * 작성자         : LCM
	 * 날짜 : 2023.07.24
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP036/cancelPickingListRobot.action")
	public ModelAndView cancelPickingListRobot(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			//m = service.sendPickingListRobot(model);
			
			String orgOrdIds = model.get("orgOrdIds").toString();
			
			
			Map<String, Object> login_rst = service.loginWcs();	    	
		
			
	    	if(login_rst.get("response").equals("success")){
				
		        String cancelOrder_rst = service.cancelMcsOrder(login_rst, orgOrdIds);
		        if(cancelOrder_rst.equals("success")){
		        	
		        	 m.put("errCnt", 0);
				     m.put("MSG", MessageResolver.getMessage("save.success"));
		        	
		        }else{
			        m.put("errCnt", 1);
			        m.put("MSG", "wcs login successful but order sending failed");
		        }
	    		
	    	}else{
		        m.put("errCnt", 1);
		        m.put("MSG", "wcs login failed");
	    	}
			
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("ERROR :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
     * Method ID : 수기송장접수 업데이트
     * Method 설명 : 수기송장
     * 작성자 : YSI
     * @param model
     * @return
     */
    @RequestMapping("/WMSOP036/InvoiceReceipt.action")
    public ModelAndView InvoiceReceipt(Map<String, Object> model) throws Exception  {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jsonView", service.InvoiceReceipt(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    

}