package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP010Service")
	private WMSOP010Service service;

	/*-
	 * Method ID    : wmsop010
	 * Method 설명      : 입출고현황 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP010.action")
	public ModelAndView wmsop010(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP010");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 입출고현황  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP010/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listSub
	 * Method 설명      : 입출고현황 상세 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP010/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list(sub) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveApprove
	 * Method 설명      : 입출고현황 승인, 불가
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP010/saveApprove.action")
	public ModelAndView saveApprove(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveApprove(model);
			// 에러메세지 save.error 로 통일할경우
			// if(Integer.parseInt(m.get("errCnt").toString()) != 0){
			// throw new Exception();
			// }
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP010/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("상태")        , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("주문번호")     , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("주문종류")     , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("작업상태")     , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("주문상세")     , "4", "4", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("화주")        , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("요청일자")     , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("작업일자")     , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("창고")        , "8", "8", "0", "0", "100"},
                                   {MessageResolver.getText("배송처")       , "9", "9", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("긴급여부")     , "10", "10", "0", "0", "100"},
                                   {MessageResolver.getText("배차확정여부")  , "11", "11", "0", "0", "100"},
                                   {MessageResolver.getText("PDA작업여부")  , "12", "12", "0", "0", "100"},
                                   {MessageResolver.getText("PDA작업상태")  , "13", "13", "0", "0", "100"},
                                   {MessageResolver.getText("BL번호")      , "14", "14", "0", "0", "100"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"APPROVE_YN"    , "S"},
                                    {"ORD_ID"        , "S"},
                                    {"ORD_TYPE"      , "S"},
                                    {"WORK_STAT"     , "S"},
                                    {"ORD_SUBTYPE"   , "S"},
                                    
                                    {"CUST_NM"       , "S"},
                                    {"REQ_DT"        , "S"},
                                    {"DT"            , "S"},
                                    {"WH_NM"         , "S"},
                                    {"TRANS_CUST_NM" , "S"},
                                    
                                    {"KIN_OUT_YN"    , "S"},
                                    {"CAR_CONF_YN"   , "S"},
                                    {"PDA_FINISH_YN" , "S"},
                                    {"PDA_STAT"      , "S"},
                                    {"BL_NO"         , "S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("입출고현황");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : saveConfirmQc
	 * Method 설명      : QC확정 (아직 프로시져없음)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP010/saveConfirmQc.action")
	public ModelAndView saveConfirmQc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveConfirmQc(model);
			// 에러메세지 save.error 로 통일할경우
			// if(Integer.parseInt(m.get("errCnt").toString()) != 0){
			// throw new Exception();
			// }
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save confirm :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : inOutOrderCntInit
	* Method 占썬�삼옙      : 
	* 占쏙옙占쎄�占쏙옙                 : chSong
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSOP010/inOutOrderCntInit.action")
	public ModelAndView inOutOrderCntInit(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.inOutOrderCntInit(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get lcinfolist :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP010/custIfInOrd.action")
	public ModelAndView custIfInOrd(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.custIfInOrd(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	@RequestMapping("/WMSOP010/custIfOutOrd.action")
	public ModelAndView custIfOutOrd(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.custIfOutOrd(model);
			m = service.custIfOutOrdCancel(model);
			
			m = service.custIfOutOrdReturn(model);
			m = service.custIfOutOrdReturnCancel(model);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : tempInOrd
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP020/tempInOrd.action")
	public ModelAndView tempInOrd(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP020IF");
	}
	/*-
	 * Method ID : tempOutOrd
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030/tempOutOrd.action")
	public ModelAndView tempOutOrd(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP030IF");
	}
	
	/*-
	 * Method ID    : tempListInOrd
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP020/tempListInOrd.action")
	public ModelAndView tempListInOrd(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.tempListInOrd(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : tempListOutOrd
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/tempListOutOrd.action")
	public ModelAndView tempListOutOrd(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.tempListOutOrd(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
}
