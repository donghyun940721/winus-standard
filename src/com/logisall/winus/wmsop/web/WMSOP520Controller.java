package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP520Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP520Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP520Service")
	private WMSOP520Service service;
	
	/*-
	 * Method ID : WMSOP520
	 * Method 설명 : 
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSOP520.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsop/WMSOP520", service.selectItemGrp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP520/list_T1_GS.action")
	public ModelAndView list01(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	@RequestMapping("/WMSOP520/list_T2_GS.action")
	public ModelAndView list02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listSub_T1
	 * Method 설명      : 발주관리 구성품 조회
	 * 작성자                 : 이성중
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP520/listSub_T1.action")
	public ModelAndView listSub_T1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub_T1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	/*-
	 * Method ID : mn1
	 * Method 설명 : 배송이력추적 화면
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP521.action")
	public ModelAndView mn1(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP521Q1");
	}
	
	/*-
	 * Method ID    : listQ1
	 * Method 설명      : 배송이력추적 조회
	 * 작성자                 : 이성중
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP521/listQ1.action")
	public ModelAndView listQ1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listQ1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	@RequestMapping("/WMSOP520/excelDown01.action")
	public void listExcel0(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.list01Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown01(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID     : saveInsQty_T1
	 * Method 설명      	 : GS입고  (세트품기준발주 수량 자동계산)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP520/saveInsQty_T1.action")
	public ModelAndView saveInsQty_T1(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveInsQtyStep(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID     : saveInsQty_T2
	 * Method 설명      	 : GS입고(개별발주)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP520/saveInsQty_T2.action")
	public ModelAndView saveInsQty_T2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.saveInsQty(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	protected void doExcelDown01(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("품목코드")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("품명")   	, "1", "1", "0", "0", "250"},
				            		{MessageResolver.getText("ERP출고 주문 수량")  , "2", "2", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"RITEM_CD" 		,"S"},
				            		{"RITEM_NM" 		,"S"},
				            		{"OUT_ORD_QTY" 	,"NR"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("출고주문대비 재고미보유 항목");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

}