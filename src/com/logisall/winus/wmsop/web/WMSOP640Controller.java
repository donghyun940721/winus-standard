package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP640Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;
import org.apache.commons.lang.StringUtils;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;

@Controller
public class WMSOP640Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSOP640 = {"ORG_ORD_NO", "ORD_DETAIL_NO", "DLV_COMP_CD", "INVC_NO", "SWEET_TRACKER_DLV_CD", "DLV_PRICE", "ORD_DATE"};

	
	@Resource(name = "WMSOP640Service")
	private WMSOP640Service service;
	
	@Resource(name = "WMSOP642Service")
	private WMSOP642Service WMSOP642Service;
	
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;

	/*-
	 * Method ID    : wmsop640
	 * Method 설명      : 택배접수
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP640.action")
	public ModelAndView wmsop640(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP640", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP641.action")
	public ModelAndView wmsop641(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP641", service.selectItemGrp(model));
	}
	@RequestMapping("/WMSOP640pop2.action")
	public ModelAndView wmsom010pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP640pop2");
	}
	
	/*-
	 * Method ID    : listByDlvSummary
	 * Method 설명	: 출고관리(송장발행화면)
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/listByDlvSummary.action")
	public ModelAndView listByDlvSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByDlvSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByDlvSummaryDiv
	 * Method 설명	: 출고관리(송장발행화면)
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP641/listByDlvSummary.action")
	public ModelAndView listByDlvSummaryDiv(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByDlvSummaryDiv(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list2ByDlvHistory
	 * Method 설명	: 출고관리(택배접수이력엑셀등록기록조회)
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/list2ByDlvHistory.action")
	public ModelAndView list2ByDlvHistory(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list2ByDlvHistory(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: DlvShipment640
	 * Method 설명	: 택배접수
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/DlvShipment.action")
	public ModelAndView DlvShipment640(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			if(model.get("PARCEL_COM_TY").equals("04")){
				m = service.DlvShipment(model);
				
				//일반 주문의 경우 상품마스터 기준 상품관리TYPE이 개체 인 상품은 주문수량만큼 송장 지동 추가발행
				if("NORMAL".equals(model.get("DIV_FLAG").toString()) && "N".equals(model.get("ADD_FLAG").toString())){
					model.put("ADD_FLAG", "R");
					System.out.println("rolling order : nRow!");
					m = service.DlvShipment(model);
				}
			}else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : wmsop640e3
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSOP640E3.action")
	public ModelAndView wmsop640e3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP640E3");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP640/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			//List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP640, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(destination, COLUMN_NAME_WMSOP640, 0, startRow, 10000, 0);
			m = service.saveCsv(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP640/dlvSenderInfo.action")
	public ModelAndView dlvSenderInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.dlvSenderInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : boxNoUpdate
	 * Method 설명	: boxNoUpdate
	 * 작성자			: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/boxNoUpdate.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.boxNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: dlvPrintPoiNoUpdate
	 * Method 설명	: 택배접수
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/dlvPrintPoiNoUpdate.action")
	public ModelAndView dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dlvPrintPoiNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP640/invcNoUpdate.action")
	public ModelAndView invcNoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.invcNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: getCustOrdDegree
	 * Method 설명	: 화주별 주문차수
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP640/getCustOrdDegree.action")
	public ModelAndView getCustOrdDegree(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustOrdDegree(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	@RequestMapping("/WMSOP640/listByDlvSummaryExcel.action")
	public void listByDlvSummaryExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listByDlvSummaryExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.listeExcelDown(response, grs,(String)model.get("fName"));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void listeExcelDown(HttpServletResponse response, GenericResultSet grs, String fName) {
       try{
           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
           String[][] headerEx = {
                                   {MessageResolver.getText("주문번호")   		, "0", "0", "0", "0", "100"}
                                  ,{MessageResolver.getText("상품명")   		, "1", "1", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분이름")  		, "2", "2", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분전화번호")   	, "3", "3", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분핸드폰번호")  , "4", "4", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분주소")   	, "5", "5", "0", "0", "100"}
                                  ,{MessageResolver.getText("배송메시지")   	, "6", "6", "0", "0", "100"}
                                  ,{MessageResolver.getText("주문일자")  		, "7", "7", "0", "0", "100"}
                                  ,{MessageResolver.getText("우편번호")   		, "8", "8", "0", "0", "100"}
                                  ,{MessageResolver.getText("우편번호")   		, "9", "9", "0", "0", "100"}
                                  ,{MessageResolver.getText("상품코드")  		, "10", "10", "0", "0", "100"}
                                  ,{MessageResolver.getText("수량")   		, "11", "11", "0", "0", "100"}
                                  ,{MessageResolver.getText("쇼핑몰")   		, "12", "12", "0", "0", "100"}
                                  ,{MessageResolver.getText("사방넷주문번호")   	, "13", "13", "0", "0", "100"}
                                  ,{MessageResolver.getText("운송장번호")   	, "14", "14", "0", "0", "100"}
                                 };
           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
           String[][] valueName = {
                                   {"ORG_ORD_ID"  			, "S"},
                                   {"ITEM_NM"  				, "S"},
                                   {"RCVR_NM"  				, "S"},
                                   {"RCVR_TEL"  			, "S"},
                                   {"RCVR_TEL2" 			, "S"},
                                   {"ADDR"  				, "S"},
                                   {"REMARK_1"  			, "S"},
                                   {"ORD_DT"  				, "S"},
                                   {"ZIP"  					, "S"},
                                   {"ZIP1"  				, "S"},
                                   {"ITEM_CODE"  			, "S"},
                                   {"ORD_QTY"  				, "N"},
                                   {"DATA_SENDER_NM"  		, "S"},
                                   {"LEGACY_ORG_ORD_NO"  	, "S"},
                                   {"INVC_NO"				, "S"}
                                  }; 
           
			// 파일명
			String fileName = fName;
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	@RequestMapping("/WMSOP640/listByDlvIFExcel.action")
	public void listByDlvIFExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		Date today = new Date();
	    SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat time = new SimpleDateFormat("hhmmssa");
	        
		try {
			map = service.listByDlvIFExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.listeExcelDown2(response, grs, date.format(today)+"_"+time.format(today)+".xlsx");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	@RequestMapping("/WMSOP640/listByDlvIFExcelV2.action")
	public void listByDlvIFExcelV2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
	    Map<String, Object> map = new HashMap<String, Object>();
	    Date today = new Date();
	    SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat time = new SimpleDateFormat("hhmmssa");
	    
	    try {
	        map = service.listByDlvIFExcelV2(model);
	        GenericResultSet grs = (GenericResultSet) map.get("LIST");
	        if (grs.getTotCnt() > 0) {
	            this.listeExcelDown2(response, grs, date.format(today)+"_"+time.format(today)+".xlsx");
	        }
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to download excel :", e);
	        }
	    }
	}
	
	protected void listeExcelDown2(HttpServletResponse response, GenericResultSet grs, String fName) {
       try{
           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
           String[][] headerEx = {
				        		    {MessageResolver.getText("OUT_REQ_DT"), "0", "0", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ITEM_NAME"), "1", "1", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ITEM_CODE"), "2", "2", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ITEM_BAR_CODE"), "3", "3", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ITEM_BAR_CODE2"), "4", "4", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ITEM_BAR_CODE3"), "5", "5", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ORD_QTY"), "6", "6", "0", "0", "100"}
				        		   ,{MessageResolver.getText("INVC_NO"), "7", "7", "0", "0", "100"}
				        		   ,{MessageResolver.getText("INVC_ADD_FLAG"), "8", "8", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ORG_ORD_ID"), "9", "9", "0", "0", "100"}
				        		   ,{MessageResolver.getText("RCVR_NM"), "10", "10", "0", "0", "100"}
				        		   ,{MessageResolver.getText("RCVR_TEL"), "11", "11", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ZIP_NO"), "12", "12", "0", "0", "100"}
				        		   ,{MessageResolver.getText("RCVR_ADDR"), "13", "13", "0", "0", "100"}
				        		   ,{MessageResolver.getText("RCVR_DETAIL_ADDR"), "14", "14", "0", "0", "100"}
				        		   ,{MessageResolver.getText("DATA_SENDER_NM"), "15", "15", "0", "0", "100"}
				        		   ,{MessageResolver.getText("LEGACY_ORG_ORD_NO"), "16", "16", "0", "0", "100"}
				        		   ,{MessageResolver.getText("WMS_ORD_ID"), "17", "17", "0", "0", "100"}
				        		   ,{MessageResolver.getText("WMS_ORD_SEQ"), "18", "18", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLNTNUM"), "19", "19", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLNTMGMCUSTCD"), "20", "20", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_PRNGDIVCD"), "21", "21", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CGOSTS"), "22", "22", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_ADDRESS"), "23", "23", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_ZIPNUM"), "24", "24", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_ZIPID"), "25", "25", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_OLDADDRESS"), "26", "26", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_OLDADDRESSDTL"), "27", "27", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_NEWADDRESS"), "28", "28", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_NESADDRESSDTL"), "29", "29", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_ETCADDR"), "30", "30", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_SHORTADDR"), "31", "31", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLSFADDR"), "32", "32", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLLDLVBRANCD"), "33", "33", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLLDLVBRANNM"), "34", "34", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLLDLCBRANSHORTNM"), "35", "35", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLLDLVEMPNUM"), "36", "36", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLLDLVEMPNM"), "37", "37", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLLDLVEMPNICKNM"), "38", "38", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLSFCD"), "39", "39", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_CLSFNM"), "40", "40", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_SUBCLSFCD"), "41", "41", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_RSPSDIV"), "42", "42", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_NEWADDRYN"), "43", "43", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_ERRORCD"), "44", "44", "0", "0", "100"}
				        		   ,{MessageResolver.getText("P_ERRORMSG"), "45", "45", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PRINT_THIS_NO"), "46", "46", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PRINT_CNT"), "47", "47", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PRINT_LAST_DATE"), "48", "48", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PRINT_LAST_NAME"), "49", "49", "0", "0", "100"}
				        		   ,{MessageResolver.getText("INVC_ADD_FLAG"), "50", "50", "0", "0", "100"}
				        		   ,{MessageResolver.getText("SWEET_TRACKER_IF_MSG"), "51", "51", "0", "0", "100"}
				        		   ,{MessageResolver.getText("SWEET_TRACKER_IF_DT"), "52", "52", "0", "0", "100"}
				        		   ,{MessageResolver.getText("INPUT_ORD_SEQ"), "53", "53", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PARCEL_ORD_TY"), "54", "54", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PARCEL_PAY_TY"), "55", "55", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PARCEL_BOX_TY"), "56", "56", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PARCEL_ETC_TY"), "57", "57", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PARCEL_COM_TY"), "58", "58", "0", "0", "100"}
				        		   ,{MessageResolver.getText("PARCEL_COM_TY_SEQ"), "59", "59", "0", "0", "100"}
				        		   ,{MessageResolver.getText("CUST_USE_NO"), "60", "60", "0", "0", "100"}
				        		   ,{MessageResolver.getText("ORG_INVC_NO"), "61", "61", "0", "0", "100"}
                                 };
           
           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
           String[][] valueName = {
					        		   {"OUT_REQ_DT"  			, "S"},
					        		   {"ITEM_NAME"  			, "S"},
					        		   {"ITEM_CODE"  			, "S"},
					        		   {"ITEM_BAR_CODE"  		, "S"},
					        		   {"ITEM_BAR_CODE2"  		, "S"},
					        		   {"ITEM_BAR_CODE3"  		, "S"},
					        		   {"ORD_QTY"  				, "S"},
					        		   {"INVC_NO"  				, "S"},
					        		   {"INVC_ADD_FLAG"  		, "S"},
					        		   {"ORG_ORD_ID"  			, "S"},
					        		   {"RCVR_NM"  				, "S"},
					        		   {"RCVR_TEL"  			, "S"},
					        		   {"ZIP_NO"  				, "S"},
					        		   {"RCVR_ADDR"  			, "S"},
					        		   {"RCVR_DETAIL_ADDR"  	, "S"},
					        		   {"DATA_SENDER_NM"  		, "S"},
					        		   {"LEGACY_ORG_ORD_NO"  	, "S"},
					        		   {"WMS_ORD_ID"  			, "S"},
					        		   {"WMS_ORD_SEQ"  			, "S"},
					        		   {"P_CLNTNUM"  			, "S"},
					        		   {"P_CLNTMGMCUSTCD"  		, "S"},
					        		   {"P_PRNGDIVCD"  			, "S"},
					        		   {"P_CGOSTS"  			, "S"},
					        		   {"P_ADDRESS"  			, "S"},
					        		   {"P_ZIPNUM"  			, "S"},
					        		   {"P_ZIPID"  				, "S"},
					        		   {"P_OLDADDRESS"  		, "S"},
					        		   {"P_OLDADDRESSDTL"  		, "S"},
					        		   {"P_NEWADDRESS"  		, "S"},
					        		   {"P_NESADDRESSDTL"  		, "S"},
					        		   {"P_ETCADDR"  			, "S"},
					        		   {"P_SHORTADDR"  			, "S"},
					        		   {"P_CLSFADDR"  			, "S"},
					        		   {"P_CLLDLVBRANCD"  		, "S"},
					        		   {"P_CLLDLVBRANNM"  		, "S"},
					        		   {"P_CLLDLCBRANSHORTNM"  	, "S"},
					        		   {"P_CLLDLVEMPNUM"  		, "S"},
					        		   {"P_CLLDLVEMPNM"  		, "S"},
					        		   {"P_CLLDLVEMPNICKNM"  	, "S"},
					        		   {"P_CLSFCD"  			, "S"},
					        		   {"P_CLSFNM"  			, "S"},
					        		   {"P_SUBCLSFCD"  			, "S"},
					        		   {"P_RSPSDIV"  			, "S"},
					        		   {"P_NEWADDRYN"  			, "S"},
					        		   {"P_ERRORCD"  			, "S"},
					        		   {"P_ERRORMSG"  			, "S"},
					        		   {"PRINT_THIS_NO"  		, "S"},
					        		   {"PRINT_CNT"  			, "S"},
					        		   {"PRINT_LAST_DATE"  		, "S"},
					        		   {"PRINT_LAST_NAME"  		, "S"},
					        		   {"INVC_ADD_FLAG"  		, "S"},
					        		   {"SWEET_TRACKER_IF_MSG"  , "S"},
					        		   {"SWEET_TRACKER_IF_DT"  	, "S"},
					        		   {"INPUT_ORD_SEQ"  		, "S"},
					        		   {"PARCEL_ORD_TY"  		, "S"},
					        		   {"PARCEL_PAY_TY"  		, "S"},
					        		   {"PARCEL_BOX_TY"  		, "S"},
					        		   {"PARCEL_ETC_TY"  		, "S"},
					        		   {"PARCEL_COM_TY"  		, "S"},
					        		   {"PARCEL_COM_TY_SEQ"  	, "S"},
					        		   {"CUST_USE_NO"  			, "S"},
					        		   {"ORG_INVC_NO"  			, "S"}
					        		   
                                  }; 
           
			// 파일명
			String fileName = fName;
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
			//wr.downExcelFile("C:\\", grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID	: DlvShipDelete
	 * Method 설명	: 택배송장삭제
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/DlvShipDelete.action")
	public ModelAndView DlvShipDelete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			if(model.get("PARCEL_COM_TY").equals("04")){
				m = service.DlvShipDelete(model);
			}else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: getCustInfo
	 * Method 설명	: 고객정보별 원주문번호 조회
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP640/getCustInfo.action")
	public ModelAndView getCustInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : custInfoUpdate
	 * Method 설명	: custInfoUpdate
	 * 작성자			: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/custInfoUpdate.action")
	public ModelAndView custInfoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.custInfoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : deviceIfSend
	 * Method 설명	: DAS자료생성
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/deviceIfSend.action")
	public ModelAndView deviceIfSend(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		
		try {
			m = service.deviceIfSend(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : deviceIfList
	 * Method 설명	: DAS자료조회팝업
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/deviceIfList.action")
	public ModelAndView deviceIfList(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP640Q1");
	}
	
	/*-
	 * Method ID	: deviceIfListSearch
	 * Method 설명	: DAS자료조회
	 * 작성자			: 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/deviceIfListSearch.action")
	public ModelAndView deviceIfListSearch(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.deviceIfListSearch(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	@RequestMapping("/WMSOP640/dashBoard.action")
	public ModelAndView dashBoard(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.dashBoard(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: getDasOrdDateByDegree
	 * Method 설명	: DAS생성차수
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP640/getDasOrdDateByDegree.action")
	public ModelAndView getDasOrdDateByDegree(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getDasOrdDateByDegree(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}

    /*-
     * Method ID    : deviceIfSendV2
     * Method 설명    : DAS자료생성
     * 작성자                         : sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP640/deviceIfSendV2.action")
    public ModelAndView deviceIfSendVX(Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = null;
        
        try {
            m = service.deviceIfSendV2(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save menu :", e);
            }
            m = new HashMap<String, Object>();
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    
	/*-
	 * Method ID	: DlvInvcNoOrderTotal_1
	 * Method 설명	: 택배접수 (임시 동시성제어 로그적재)
	 * 작성자                 	: dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/DlvInvcNoOrderTotal_1.action")
	public ModelAndView DlvInvcNoOrderTotal_1(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		/** 동시성 제어 변수 선언 */
		List<Object> concurcInfo;
    	String concurcSeq;    	
    	String serviceNm	= "/WMSOP640/DlvInvcNoOrderTotal_1.action";
    	
    	String ccSeparator1 = ""; // 화주
    	String ccSeparator2 = ""; // 주문차수
    	String ccSeparator3 = model.get("PARCEL_COM_TY").toString();  // 택배구분
    	String ccSeparator4 = "";
    	String ccSeparator5 = "";
    	
    	if(model.containsKey("CUST_ID0")){
    		ccSeparator1 = StringUtils.isEmpty(model.get("CUST_ID0").toString()) ? "All" : model.get("CUST_ID0").toString();			// 화주
    	}
    	
    	if(model.containsKey("ORD_DEGREE")){
    		ccSeparator2 = StringUtils.isEmpty(model.get("ORD_DEGREE").toString()) ? "All" : model.get("ORD_DEGREE").toString();		// 주문차수
    	}
    	
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			
        	/** 동시성 제어 (유효성 검사) */
//        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);      
//        	
//        	if(concurcInfo.size() > 0){
//        		String excecptionMsg = "\n[Concurrency Exception]\n";        		
//        		        		
//        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
//        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
//        		
//        		throw new Exception(excecptionMsg);
//        	}
        		
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString(); 
        	
        	
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			 if(model.get("PARCEL_COM_TY").equals("04") 
					 	|| model.get("PARCEL_COM_TY").equals("06")
					 	|| model.get("PARCEL_COM_TY").equals("08") ){
				//eai 정보 말아서 1번 송신
				m = WMSOP642Service.DlvInvcNoOrderCommTotal(model);		
				
			 }else if(model.get("PARCEL_COM_TY").equals("05")){
				m = WMSOP642Service.DlvShipmentHanjin(model);		
				
			 }else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
			 
			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);	
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
}
