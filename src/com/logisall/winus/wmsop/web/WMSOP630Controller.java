package com.logisall.winus.wmsop.web;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.api.oywcs.dto.OrdrListWrapper;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsop.service.WMSOP630Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP630Controller {
	private final Log log = LogFactory.getLog(this.getClass());
	@Autowired
	private WMSOP030Service wmsop030Service;
	
	@Autowired
	private WMSOP630Service wmsop630Service;
	
	
	@RequestMapping("/WINUS/WMSOP630_V2.action")
	public ModelAndView wmsop630(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP630_V2", wmsop030Service.selectItemGrp(model));
	}
	

	@RequestMapping("/WMSOP630/ai-picking.action")
	public ResponseEntity<Map<String, Object>> orderShipmentChangePart(Map<String, Object> model, @RequestBody Map<String, Object> reqBody,
			HttpServletRequest request, HttpServletResponse response){
		String uuid = generatePickingCode();
		
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			wmsop630Service.saveAIPicking(model, reqBody, uuid);
			responseMap = wmsop630Service.requestAiPicking(uuid);
			responseMap.put("errCnt", 0);
			responseMap.put("MSG", MessageResolver.getMessage("list.success"));
			responseMap.put("UUID", uuid);
		} catch (IOException e) {
			log.error(e);
			responseMap.put("errCnt", 1);
			responseMap.put("MSG", MessageResolver.getMessage("list.error"));
		}
        
		
		return new ResponseEntity<>(responseMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSOP630AI_pop.action")
	public ModelAndView WMSOP630pop(Map<String, Object> model) throws Exception {
	    ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("winus/wmsop/WMSOP630AI_pop");
        try
        {
            Map<String, Object> map = wmsop030Service.insertPickingListLog(model);
            if(map.get("O_MSG_NAME").equals("SUCCESS"))
            {
                modelAndView.addObject("O_SYSDATE", map.get("O_SYSDATE"));
            }
            System.out.println(map.get("O_SYSDATE"));
        }catch(Exception ex){
            return modelAndView;
        }   
        
        return modelAndView;
	}
	
	/*
	 * 요청코드 및 피킹로그 난수생성
	 * PICKING_yyyyMMddHHmmssSSS_6자리무작위숫자
	 * */
	private String generatePickingCode() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String timestamp = dateFormat.format(new Date());

        Random random = new Random();
        int randomNumber = random.nextInt(1000000); // 0 ~ 999999

        String formattedRandomNumber = String.format("%06d", randomNumber);

        // 결과 조합
        return "PICKING_" + timestamp + "_" + formattedRandomNumber;
    }
}
