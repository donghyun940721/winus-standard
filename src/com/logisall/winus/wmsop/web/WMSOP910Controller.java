package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP910Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;

@Controller
public class WMSOP910Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP910Service")
	private WMSOP910Service service;
	
	@Resource(name = "WMSIF000Service")
    private WMSIF000Service WMSIF000Service;
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;
	
	/*-
	 * Method ID    : wmsop910
	 * Method 설명      : 입/출고(통합)화면
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP910.action")
	public ModelAndView wmsop910(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP910",  service.selectData(model));
	}
	
	/*-
	 * Method ID    : listE1
     * Method 설명      : 입고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP910/listE1.action")
	public ModelAndView listE1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE2
     * Method 설명      : 출고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP910/listE2.action")
	public ModelAndView listNew(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE3
	 * Method 설명      : 화주별 출고관리 조회(OM)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE3.action")
	public ModelAndView listE3(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE4Header
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE4Header.action")
	public ModelAndView listE4Header(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4Header(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE4Detail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE4Detail.action")
	public ModelAndView listE4Detail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4Detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID    : listE5Header
     * Method 설명      : 출고B2C Header 조회
     * 작성자                 : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP910/listE5Header.action")
    public ModelAndView listE5Header(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jsonView", service.listE5Header(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    
    /*-
     * Method ID    : listE5Detail
     * Method 설명      : 출고 B2C Detail 조회
     * 작성자                 : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP910/listE5Detail.action")
    public ModelAndView listE5Detail(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jsonView", service.listE5Detail(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get list :", e);
            }
        }
        return mav;
    }
    
    /*-
	 * Method ID    : listE6
     * Method 설명      : 반품입고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP910/listE6.action")
	public ModelAndView listE6(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE6(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE1SummaryCount
	 * Method 설명      : 입출고통합관리 -> 입고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE1SummaryCount.action")
	public ModelAndView listE1SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE1SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE2SummaryCount
	 * Method 설명      : 입출고통합관리 -> B2B 출고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE2SummaryCount.action")
	public ModelAndView listE2SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE2SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE3SummaryCount
	 * Method 설명      : 입출고통합관리 -> B2C 출고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE3SummaryCount.action")
	public ModelAndView listE3SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE3SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE6SummaryCount
	 * Method 설명      : 입출고통합관리 -> 반품입고관리 count summary
	 * 작성자           : 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE6SummaryCount.action")
	public ModelAndView listE6SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE6SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	* Method ID    		: sendMfcOrder
	* Method 설명       : 설비출고(창원)
	* 작성자            : KSJ
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP910/sendMfcOrder.action")
	public ModelAndView sendMfcOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			
			/* EAI WCS 설비 상태 최신화 */
            Map<String, Object> eaiMap = new HashMap<String, Object>();
            String cMethod = "POST";
            String cUrl = "WCS_MFC/MFC/SYNC_DEVICE_STATUS_U";

            eaiMap.put("cMethod", cMethod);
            eaiMap.put("cUrl", cUrl);
            eaiMap.put("data", "");

            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
            
			/* EAI WCS 설비 작업결과 최신화 */
            cMethod = "POST";
            cUrl = "WCS_MFC/MFC/SYNC_JOB_RET";

            eaiMap.put("cMethod", cMethod);
            eaiMap.put("cUrl", cUrl);
            eaiMap.put("data", "");

            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
            
            m = service.sendMfcOrder(model);
            
            /* EAI WCS 주문 작업 전송 */
            cMethod = "POST";
            cUrl = "WCS_MFC/MFC/SYNC_JOB_ORDER";

            eaiMap.put("cMethod", cMethod);
            eaiMap.put("cUrl", cUrl);
            eaiMap.put("data", "");

            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID  : MAKE DAS DEGREE WHIT KOTECH
	* Method 설명     : B2B 주문 DAS 사용에 따른 차수 생성
	* 작성자                     : KSG
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP910/dasMake.action")
	public ModelAndView dasMake(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dasMake(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID  	: createDasDegree
	* Method 설명   : 출고B2C 주문 기준 최대 셀 단위 배치 생성
	* 작성자        : dhkim
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP910/createDasDegreeFromOrder.action")
	public ModelAndView createDasDegreeFromOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
        String concurcSeq;          
  
		try {
			String serviceNm	= "DAS 차수 생성";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrOrdDt").toString();
        	String ccSeparator3 = StringUtils.isEmpty(model.get("vrOrdDegree").toString()) ? "All" : model.get("vrOrdDegree").toString();
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";  
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	// 전체 차수 대상인 경우, 개별 차수작업 제한
           	if(concurcInfo.size() == 0 && !StringUtils.equals("All", ccSeparator3)){
        		concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, "All", ccSeparator4, ccSeparator5);	
        	}        	
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        		
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();  
        	
			m = service.createDasDegreeFromOrder(model);
			
			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			// m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", 1);
			m.put("MSG", e.getMessage());     
		}
		mav.addAllObjects(m);
		return mav;
	}	
	
	/*-
	* Method ID  	: createDasDegree
	* Method 설명   : 출고B2C 주문 기준 최대 셀 단위 배치 생성
	* 작성자        : dhkim
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP910/sendDasData.action")
	public ModelAndView sendDasData(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.sendDasData(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}	
	
	/*-
	* Method ID  : MAKE DAS DEGREE WHIT KOTECH
	* Method 설명     : B2C 주문 DAS 사용에 따른 차수 생성(주문일별차수)
	* 작성자                     : 
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP910/dasMakeOrderDegree.action")
	public ModelAndView dasMakeOrderDegree(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dasMakeOrderDegree(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP910/dasDelete.action")
	public ModelAndView dasDelete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dasDelete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP910/dasIfSend.action")
	public ModelAndView dasIfSend(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		
		try {
			m = service.dasIfSend(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID  : 코텍DAS B2C 전송
	* Method 설명     : 코텍DAS B2C 전송
	* 작성자                     : 
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP910/dasIfSendB2C.action")
	public ModelAndView dasIfSendB2C(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
		String concurcSeq; 
		
		try {
        	String serviceNm	= "DAS 차수 전송 (택배)";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrSrchOrdDegreeDate").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";         	
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
			m = service.dasIfSendB2C(model);
			
			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			
			m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID  : 코텍DAS B2C 전송
	* Method 설명     : 코텍DAS B2C 전송
	* 작성자                     : 
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP910/dasIfSendNoInvcB2C.action")
	public ModelAndView dasIfSendNoInvcB2C(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
		String concurcSeq; 
		
		try {
			String serviceNm	= "DAS 차수 전송 (일반)";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrSrchOrdDegreeDate").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = ""; 
        	        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();       	
        	        	
			m = service.dasIfSendNoInvcB2C(model);               	
            
			/** 동시성 제어 (작업해제) */
            WMSYS400Service.complete(model, concurcSeq);
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP910/getDevOrdDegree.action")
	public ModelAndView getDevOrdDegree(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getDevOrdDegree(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP910/makeDegreeB2C.action")
	public ModelAndView makeDegreeB2C(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.makeDegreeB2C(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP910/deleteDegree.action")
	public ModelAndView deleteDegree(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteDegree(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	@RequestMapping("/WMSOP910/excelByTurkiye.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			model.put("pageSize", "1000");
			map = service.listE1ExcelByTurkiye(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("")    		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("KOS")    		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("QTY") 		, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("PRICE")   	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("bos")   	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("bos")   	, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("bos")   	, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("bos")   	, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("bos")   	, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("bos")   	, "9", "9", "0", "0", "100"},
				            		{MessageResolver.getText("L/NO DATA")   , "10", "10", "0", "0", "100"},
				            		{MessageResolver.getText("R-B/NO")    	, "11", "11", "0", "0", "100"}
//				            		{MessageResolver.getText("판매가합계")    	, "5", "5", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"RNUM"	, "S"},
				            		{"RITEM_CD"	, "S"},
				            		{"ORD_QTY"		, "S"},
				            		{"PRICE"		, "S"},
				            		{"BOS1"		, "S"},
				            		{"BOS2"		, "S"},
				            		{"BOS3"		, "S"},
				            		{"BOS4"		, "S"},
				            		{"BOS5"		, "S"},
				            		{"BOS6"		, "S"},
				            		{"LNODATA"	, "S"},
				            		{"UNIT_NO"		, "S"}
//				            		{"SALES_PRICE"		, "S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("STOCK_FORMAT");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}