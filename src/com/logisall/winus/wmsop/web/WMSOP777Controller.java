package com.logisall.winus.wmsop.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsop.service.WMSOP777Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP777Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSOP777Service")
	private WMSOP777Service service;

	/*-
	 * Method ID    : wmsop777
	 * Method 설명      : 주문할당수량조회 화면
	 * 작성자                 : atomyoun
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP777.action")
	public ModelAndView wmsop777(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP777");
	}
	
	/*-
	 * Method ID    : listE1
     * Method 설명      : 주문할당수량 조회
     * 작성자                 : atomyoun
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP777/listE1.action")
	public ModelAndView listE1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE2
     * Method 설명      : 주문할당수량(상품) 조회
     * 작성자                 : atomyoun
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP777/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE2Count
	 * Method 설명      : 주문건수 & 주문seq수 조회
	 * 작성자           : atomyoun
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP777/listE2OrdCount.action")
	public ModelAndView listE2Count(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE2OrdCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
}
