package com.logisall.winus.main.service;

import java.util.Map;


public interface MainDashBoardService {
	/**interfacebody*/
	public Map<String, Object> getMultiCenterLatLngInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> getMultiCenterStockInfo(Map<String, Object> model) throws Exception;
}
