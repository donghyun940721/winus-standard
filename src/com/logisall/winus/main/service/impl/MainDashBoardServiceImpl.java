package com.logisall.winus.main.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.main.service.MainDashBoardService;
import com.logisall.winus.main.service.MainService;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;


@Service("MainDashBoardService")
public class MainDashBoardServiceImpl extends AbstractServiceImpl implements MainDashBoardService {
	
	protected Log loger = LogFactory.getLog(this.getClass());

	@Resource(name = "MainDashBoardDao")
	private MainDashBoardDao dao;

    @Override
    public Map<String, Object> getMultiCenterLatLngInfo(Map<String, Object> model) throws Exception {
        Map<String,Object> m = new HashMap<String,Object>();
        try{
            m.put("LIST", dao.getMultiCenterLatLngInfo(model));
        }catch(Exception e){
            e.printStackTrace();
            
            m.put("CODE", "-1");
            m.put("MSG", e.toString());
        }
        return m;
    }
    
    @Override
    public Map<String, Object> getMultiCenterStockInfo(Map<String, Object> model) throws Exception {
        Map<String,Object> m = new HashMap<String,Object>();
        try{
            m.put("LIST", dao.getMultiCenterStockInfo(model));
        }catch(Exception e){
            e.printStackTrace();
            
            m.put("CODE", "-1");
            m.put("MSG", e.toString());
        }
        return m;
    }
    
    

}
