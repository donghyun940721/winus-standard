package com.logisall.winus.main.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;


@Repository("mainDao")
public class MainDao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("fta.sample.list", model);
	}

	public Object detail(Map<String, Object> model) {
		return executeView("fta.sample.detail", model);
	}

	public Object insert(Map<String, Object> model) {
		return executeInsert("fta.sample.insert", model);
	}

	public int update(Map<String, Object> model) {
		return executeUpdate("fta.sample.update", model);
	}

	public int delete(Map<String, Object> model) {
		return executeDelete("fta.sample.delete", model);
	}
}
