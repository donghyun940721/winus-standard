package com.logisall.winus.main.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.main.service.MainService;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;


@Service("mainService")
public class MainServiceImpl extends AbstractServiceImpl implements MainService {
	
	protected Log loger = LogFactory.getLog(this.getClass());

	@Resource(name = "mainDao")
	private MainDao dao;

	/**
	 * 대체 Method ID   : list
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		if(model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if(model.get("rows") == null) {
			model.put("pageSize", "100");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.list(model));
		return map;
	}

	/**
	 * 대체 Method ID   : detail
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> detail(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DETAIL", dao.detail(model));
		return map;
	}

	/**
	 * 대체 Method ID   : insert
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> insert(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("CREATE_ID", "TEST");
		dao.insert(model);
		map.put("DETAIL", dao.detail(model));
		map.put("MSG", "등록되었습니다.");
		return map;
	}

	/**
	 * 대체 Method ID   : update
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> update(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		/* 반환값에 대한 별도 로직 없음 */
		int cnt = dao.update(model);
		
		map.put("DETAIL", dao.detail(model));
		map.put("MSG", "저장되었습니다.");
		return map;
	}

	/**
	 * 대체 Method ID   : delete
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> delete(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		/* 반환값에 대한 별도 로직 없음 */
		int cnt = dao.delete(model);
		map.put("MSG", "삭제되었습니다.");
		return map;
	}
}
