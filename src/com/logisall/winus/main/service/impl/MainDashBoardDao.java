package com.logisall.winus.main.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;


@Repository("MainDashBoardDao")
public class MainDashBoardDao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());


	public GenericResultSet getMultiCenterLatLngInfo(Map<String, Object> model) {
		return executeQueryWq("wmsdh200.multiCenterLatLngInfoList", model);
	}
	
   public GenericResultSet getMultiCenterStockInfo(Map<String, Object> model) {
        return executeQueryWq("wmsst010.multiCenterStockInfoList", model);
    }
}
