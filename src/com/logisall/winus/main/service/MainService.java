package com.logisall.winus.main.service;

import java.util.Map;


public interface MainService {
	/**interfacebody*/
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> detail(Map<String, Object> model) throws Exception;
	public Map<String, Object> insert(Map<String, Object> model) throws Exception;
	public Map<String, Object> update(Map<String, Object> model) throws Exception;
	public Map<String, Object> delete(Map<String, Object> model) throws Exception;
}
