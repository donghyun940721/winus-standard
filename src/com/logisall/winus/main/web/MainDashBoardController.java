package com.logisall.winus.main.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.google.gson.Gson;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.LoginInfo;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.frm.common.util.SessionMn;
import com.logisall.winus.main.service.MainDashBoardService;
import com.logisall.winus.meta.service.MetaUserRoleService;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.client.service.JDFWClientInfo;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.SessionUtil;

@Controller
public class MainDashBoardController {
	protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSIF000Service")
    private WMSIF000Service wmsif000service;
    
    @Resource(name = "MainDashBoardService")
    private MainDashBoardService service;

    /**
     * Method ID    : mqDashBoard
     * Method 설명    : 
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mqDashBoard.action")
    public ModelAndView dhDashBoard(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/dashboard/mqDashBoard");
    }
    
    /**
     * Method ID    : multiCenterMapDashBoard
     * Method 설명    : 
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/multiCenterMapDashBoard.action")
    public ModelAndView multiCenterMapDashBoard(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/dashboard/multiCenterMapDashBoard");
    }
    
    /**
     * Method ID    : ediyaDashBoard
     * Method 설명    : 
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/ediyaDashBoard.action")
    public ModelAndView ediyaDashBoard(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/dashboard/ediyaDashBoard");
    }
    
    @RequestMapping(value = "/MQ/crossDomainHttpWs.action", method = RequestMethod.POST)
    public ModelAndView crossDomainHttpWs(@RequestBody JSONObject obj , HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> model = new HashMap<String, Object>();
        Map<String, Object> m = new HashMap<String, Object>();

        try {
            String cMethod      = "POST";
            String cUrl ="DASHBOARD/TODAY_JOB_STATUS_MQ";
            String hostUrl = request.getServerName();
            
            String dataJson = obj.toJSONString(); 
            //String dataJson = request.getParameter("jsonData");
            //dataJson =  HtmlUtils.htmlUnescape(dataJson);
            if(dataJson != null && dataJson !=""){
                model.put("data"        , dataJson);
                model.put("cMethod"     , cMethod);
                model.put("cUrl"        , cUrl);
                model.put("hostUrl"     , hostUrl);
                m = wmsif000service.crossDomainHttpWs5200DH(model);
            }
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    @RequestMapping("/getMultiCenterLatLngInfo.action")
    public ModelAndView getMultiCenterLatLngInfo(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            m = service.getMultiCenterLatLngInfo(model);
        }catch (Exception e){
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
        }
        mav.addAllObjects(m);
        
        return mav;
    }
    
    @RequestMapping("/getMultiCenterStockInfo.action")
    public ModelAndView getMultiCenterStockInfo(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            m = service.getMultiCenterStockInfo(model);
        }catch (Exception e){
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
        }
        mav.addAllObjects(m);
        
        return mav;
    }
    
    @RequestMapping(value = "/EDIYA/getDashBoardDataEdiya.action", method = RequestMethod.POST)
    public ModelAndView getDashBoardDataEdiya(@RequestBody JSONObject obj , HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> model = new HashMap<String, Object>();
        Map<String, Object> m = new HashMap<String, Object>();

        try {
            String cMethod      = "POST";
            String cUrl ="EDIYAWMS/DEVICE/COMPLETE/STATUS";
            String hostUrl = request.getServerName();
            
            String dataJson = obj.toJSONString(); 
            //String dataJson = request.getParameter("jsonData");
            //dataJson =  HtmlUtils.htmlUnescape(dataJson);
            model.put("data"        , dataJson);
            model.put("cMethod"     , cMethod);
            model.put("cUrl"        , cUrl);
            model.put("hostUrl"     , hostUrl);
            m = wmsif000service.crossDomainHttpWs5400New2(model);
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
        }
        mav.addAllObjects(m);
        return mav;
    }
	
}
