package com.logisall.winus.wmspk.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmspk.service.WMSPK200Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSPK200Service")
public class WMSPK200ServiceImpl extends AbstractServiceImpl implements WMSPK200Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPK200Dao")
    private WMSPK200Dao dao;

   /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
	/**
     * Method ID : saveExcelInfo
     * Method 설명 : 엑셀읽기저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	int insertCnt = (list != null)?list.size():0;
            if(insertCnt > 0){
            	String[] HK 				= new String[insertCnt];
            	String[] PART_NO 			= new String[insertCnt];
            	String[] PART_NAME 			= new String[insertCnt];
            	String[] SUPPILER_PART_NO 	= new String[insertCnt];
            	String[] SUPPLIER 			= new String[insertCnt];
            	
            	String[] PURCHASER 			= new String[insertCnt];
            	String[] P_PDC 				= new String[insertCnt];
            	String[] D_PDC 				= new String[insertCnt];
            	String[] R_PDC 				= new String[insertCnt];
            	String[] R_DLR 				= new String[insertCnt];
            	
            	String[] PO_NO 				= new String[insertCnt];
            	String[] PO_LINE 			= new String[insertCnt];
            	String[] POI_NO 			= new String[insertCnt];
            	String[] PO_CODE 			= new String[insertCnt];
            	String[] PO_RAW 			= new String[insertCnt];
            	
            	String[] PROCESS 			= new String[insertCnt];
            	String[] CLASS 				= new String[insertCnt];
            	String[] CAR 				= new String[insertCnt];
            	String[] PO_DATE 			= new String[insertCnt];
            	String[] DUE_DATE 			= new String[insertCnt];
            	
            	String[] BO_QTY 			= new String[insertCnt];
            	String[] DELAY 				= new String[insertCnt];
            	String[] REQ_DATE 			= new String[insertCnt];
            	String[] SHIP_MODE 			= new String[insertCnt];
            	String[] COMMON 			= new String[insertCnt];
            	
            	String[] PO_QTY 			= new String[insertCnt];
            	String[] CXL_QTY 			= new String[insertCnt];
            	String[] P_RCY 				= new String[insertCnt];
            	String[] M_RCY 				= new String[insertCnt];
            	String[] ASN_QTY 			= new String[insertCnt];
            	
            	String[] DI_QTY 			= new String[insertCnt];
            	String[] CUR 				= new String[insertCnt];
            	String[] ATP_DATE 			= new String[insertCnt];
            	String[] ATP_QTY 			= new String[insertCnt];
            	String[] ATP_LN 			= new String[insertCnt];
            	
            	String[] ACCEPETANCE 		= new String[insertCnt];
            	String[] PA_FLAG 			= new String[insertCnt];
                
                Map<String, Object> paramMap = null;
                for (int i=0;i<list.size();i++) {
            		paramMap = (Map)list.get(i);
            		HK[i] 					= (String)paramMap.get("O_HK");
            		PART_NO[i] 				= (String)paramMap.get("O_PART_NO");
            		PART_NAME[i] 			= (String)paramMap.get("O_PART_NAME");
            		SUPPILER_PART_NO[i] 	= (String)paramMap.get("O_SUPPILER_PART_NO");
            		SUPPLIER[i] 			= (String)paramMap.get("O_SUPPLIER");
            		
            		PURCHASER[i] 			= (String)paramMap.get("O_PURCHASER");
            		P_PDC[i] 				= (String)paramMap.get("O_P_PDC");
            		D_PDC[i] 				= (String)paramMap.get("O_D_PDC");
            		R_PDC[i] 				= (String)paramMap.get("O_R_PDC");
            		R_DLR[i] 				= (String)paramMap.get("O_R_DLR");
            		
            		PO_NO[i] 				= (String)paramMap.get("O_PO_NO");
            		PO_LINE[i] 				= (String)paramMap.get("O_PO_LINE");
            		POI_NO[i] 				= (String)paramMap.get("O_POI_NO");
            		PO_CODE[i] 				= (String)paramMap.get("O_PO_CODE");
            		PO_RAW[i] 				= (String)paramMap.get("O_PO_RAW");
            		
            		PROCESS[i] 				= (String)paramMap.get("O_PROCESS");
            		CLASS[i] 				= (String)paramMap.get("O_CLASS");
            		CAR[i] 					= (String)paramMap.get("O_CAR");
            		PO_DATE[i] 				= (String)paramMap.get("O_PO_DATE");
            		DUE_DATE[i] 			= (String)paramMap.get("O_DUE_DATE");
            		
            		BO_QTY[i] 				= (String)paramMap.get("O_BO_QTY");
            		DELAY[i] 				= (String)paramMap.get("O_DELAY");
            		REQ_DATE[i] 			= (String)paramMap.get("O_REQ_DATE");
            		SHIP_MODE[i] 			= (String)paramMap.get("O_SHIP_MODE");
            		COMMON[i] 				= (String)paramMap.get("O_COMMON");
            		
            		PO_QTY[i] 				= (String)paramMap.get("O_PO_QTY");
            		CXL_QTY[i] 				= (String)paramMap.get("O_CXL_QTY");
            		P_RCY[i] 				= (String)paramMap.get("O_P_RCY");
            		M_RCY[i] 				= (String)paramMap.get("O_M_RCY");
            		ASN_QTY[i] 				= (String)paramMap.get("O_ASN_QTY");
            		
            		DI_QTY[i] 				= (String)paramMap.get("O_DI_QTY");
            		CUR[i] 					= (String)paramMap.get("O_CUR");
            		ATP_DATE[i] 			= (String)paramMap.get("O_ATP_DATE");
            		ATP_QTY[i] 				= (String)paramMap.get("O_ATP_QTY");
            		ATP_LN[i] 				= (String)paramMap.get("O_ATP_LN");
            		
            		ACCEPETANCE[i] 			= (String)paramMap.get("O_ACCEPETANCE");
            		PA_FLAG[i] 				= (String)paramMap.get("O_PA_FLAG");
            	}

                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("I_HK"					, HK);
                modelIns.put("I_PART_NO"			, PART_NO);
                modelIns.put("I_PART_NAME"			, PART_NAME);
                modelIns.put("I_SUPPILER_PART_NO"	, SUPPILER_PART_NO);
                modelIns.put("I_SUPPLIER"			, SUPPLIER);
                
                modelIns.put("I_PURCHASER"			, PURCHASER);
                modelIns.put("I_P_PDC"				, P_PDC);
                modelIns.put("I_D_PDC"				, D_PDC);
                modelIns.put("I_R_PDC"				, R_PDC);
                modelIns.put("I_R_DLR"				, R_DLR);
                
                modelIns.put("I_PO_NO"				, PO_NO);
                modelIns.put("I_PO_LINE"			, PO_LINE);
                modelIns.put("I_POI_NO"				, POI_NO);
                modelIns.put("I_PO_CODE"			, PO_CODE);
                modelIns.put("I_PO_RAW"				, PO_RAW);
                
                modelIns.put("I_PROCESS"			, PROCESS);
                modelIns.put("I_CLASS"				, CLASS);
                modelIns.put("I_CAR"				, CAR);
                modelIns.put("I_PO_DATE"			, PO_DATE);
                modelIns.put("I_DUE_DATE"			, DUE_DATE);
                
                modelIns.put("I_BO_QTY"				, BO_QTY);
                modelIns.put("I_DELAY"				, DELAY);
                modelIns.put("I_REQ_DATE"			, REQ_DATE);
                modelIns.put("I_SHIP_MODE"			, SHIP_MODE);
                modelIns.put("I_COMMON"				, COMMON);
                
                modelIns.put("I_PO_QTY"				, PO_QTY);
                modelIns.put("I_CXL_QTY"			, CXL_QTY);
                modelIns.put("I_P_RCY"				, P_RCY);
                modelIns.put("I_M_RCY"				, M_RCY);
                modelIns.put("I_ASN_QTY"			, ASN_QTY);
                
                modelIns.put("I_DI_QTY"				, DI_QTY);
                modelIns.put("I_CUR"				, CUR);
                modelIns.put("I_ATP_DATE"			, ATP_DATE);
                modelIns.put("I_ATP_QTY"			, ATP_QTY);
                modelIns.put("I_ATP_LN"				, ATP_LN);
                
                modelIns.put("I_ACCEPETANCE"		, ACCEPETANCE);
                modelIns.put("I_PA_FLAG"			, PA_FLAG);
                
                modelIns.put("I_LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("I_WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));
                
                //dao
                modelIns = (Map<String, Object>)dao.saveOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSPK200", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
        	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
			model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
			
            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
            
        } catch(Exception e){
            m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID  : excelDown
     * Method 설명    : 엑셀다운로드
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     @Override
     public Map<String, Object> excelDown(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         model.put("pageIndex", "1");
         model.put("pageSize", "60000");
         map.put("LIST", dao.list(model));
         return map;
     }
}
