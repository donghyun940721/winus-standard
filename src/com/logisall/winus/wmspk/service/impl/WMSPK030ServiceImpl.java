package com.logisall.winus.wmspk.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


import com.logisall.winus.wmspk.service.WMSPK030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;


@Service("WMSPK030Service")
public class WMSPK030ServiceImpl extends AbstractServiceImpl implements WMSPK030Service {
    
    @Resource(name = "WMSPK030Dao")
    private WMSPK030Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 라벨송장발행 라벨 조회
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", e.getMessage());
        }
        return map;
    }
    
    /**
     * Method ID : workInvcNo
     * Method 설명 : 라벨송장발행 송장 조회
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> workInvcNo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.workInvcNo(model));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get result :", e);
            } 
            map.put("MSG", e.getMessage());
        }
        return map;
    }
    
    


}
