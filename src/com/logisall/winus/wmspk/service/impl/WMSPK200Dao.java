package com.logisall.winus.wmspk.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Repository("WMSPK200Dao")
public class WMSPK200Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspk200.list", model);
    }
    
    /**
     * Method ID	: saveOrder
     * Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     */
    public Object saveOrder(Map<String, Object> model){
        executeUpdate("wmspk200.pk_wmspk020t.sp_insert_ph161_template", model);
        return model;
    }
}
