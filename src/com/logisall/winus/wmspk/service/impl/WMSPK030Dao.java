package com.logisall.winus.wmspk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.PDF.model.Wmsst070HeardlVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070InDetailVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070OutDetailVO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPK030Dao")
public class WMSPK030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID : list
     * Method 설명 : 라벨송장발행 라벨 조회
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmspk030.list", model);
    }
    
    /**
     * Method ID : workInvcNo
     * Method 설명 : 라벨송장발행 송장조회
     * 작성자 : schan
     * @param model
     * @return
     */
    public List workInvcNo(Map<String, Object> model) {
        return executeQueryForList("wmspk030.workInvcNo", model);
    }
    
    
    
}
