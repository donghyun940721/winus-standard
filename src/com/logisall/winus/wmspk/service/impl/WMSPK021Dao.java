package com.logisall.winus.wmspk.service.impl;

import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPK021Dao")
public class WMSPK021Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 임가공 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKit", model);
    }
  
    /**
     * Method ID : listSub
     * Method 설명 : 임가공 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitDetail", model);
    }
    
    /**
     * Method ID : listSub2
     * Method 설명 : 임가공 상세 조회2
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listSub2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitDetail2", model);
    }
    
    /**
     * Method ID : popdetail
     * Method 설명 : 임가공조립 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet popdetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitWorkDetail", model);
    }
    
    /**
     * Method ID  : save
     * Method 설명  : 임가공조립 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_gen_kit_work", model);
        return model;
    }
    
    /**
     * Method ID  : save2
     * Method 설명  : 임가공해체 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object save2(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_gen_unkit_work", model);
        return model;
    }
    
    /**
     * Method ID  : delete
     * Method 설명  : 임가공조립 삭제
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object delete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_del_work", model);
        return model;
    }

    /**
     * Method ID  : updateKitComplete
     * Method 설명  : 임가공조립확정
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object updateKitComplete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_kit_complete", model);
        return model;
    }
    /**
     * Method ID  : updateUnkitComplete
     * Method 설명  : 임가공해체확정
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object updateUnkitComplete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_unkit_complete", model);
        return model;
    }

     
    
    /**
     * Method ID    : saveSimpleOut
     * Method 설명      : 번들보충
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOrderKit(Map<String, Object> model){
        executeUpdate("wmspk021.pk_wmsst070.sp_order_kit", model);
        return model;
    }
    
    /**
     * Method ID  : workList
     * Method 설명  : workList
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object workList(Map<String, Object> model){
        return executeQueryForList("wmspk021.workList", model);
    }
}
