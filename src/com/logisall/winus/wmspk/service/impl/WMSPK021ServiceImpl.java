package com.logisall.winus.wmspk.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmspk.service.WMSPK021Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service("WMSPK021Service")
public class WMSPK021ServiceImpl extends AbstractServiceImpl implements WMSPK021Service {
    
    @Resource(name = "WMSPK021Dao")
    private WMSPK021Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 임가공 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : updateComplete
     * Method 설명 : 임가공 조립/해체 확정
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelSP = new HashMap<String, Object>();
        
        try{
            // 그리드 저장 시작
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            if(iuCnt > 0){              
                modelSP.put("I_WORK_ID",     model.get("vrWorkId"));
                modelSP.put("I_WORK_IP",     model.get("SS_CLIENT_IP"));
                modelSP.put("I_USER_NO",     model.get("SS_USER_NO"));
           
                if(model.get("gubun").equals("kit")){
                    // 임가공 조립
                	modelSP = (Map<String, Object>) dao.updateKitComplete(modelSP);
                	ServiceUtil.isValidReturnCode("WMSPK021", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));

                }else if(model.get("gubun").equals("unkit")){
                    // 임가공 해체
                	modelSP = (Map<String, Object>)dao.updateUnkitComplete(modelSP);
                	ServiceUtil.isValidReturnCode("WMSPK021", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
                }
            }              
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );
        
        }catch (Exception e) {
            throw e;
        }
        return m;
        
    }
    /**
     * Method ID : save
     * Method 설명 : 임가공 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(iuCnt > 0){
                for(int i=0; i<iuCnt; i++){
                    main.put("ST_GUBUN", model.get("ST_GUBUN"+i));
                    main.put("vrInWhId", model.get("vrInWhId"+i));
                    main.put("vrOutWhId", model.get("vrOutWhId"+i));
                    main.put("custId", model.get("custId"+i));
                    main.put("ritemId", model.get("ritemId"+i));
                    main.put("uomId", model.get("uomId"+i));
                    main.put("qty", model.get("qty"+i));
                    main.put("vrWorkId", model.get("vrWorkId"+i));                        
                    main.put("type", model.get("type"+i));               
                    
                    if(main.get("ST_GUBUN").equals("DELETE")){
                        Map<String, Object> modelSPD = new HashMap<String, Object>();
                        modelSPD.put("I_WORK_ID",     model.get("vrWorkId"+i));
                        modelSPD.put("I_WORK_IP",     model.get("SS_CLIENT_IP"));
                        modelSPD.put("I_USER_NO",     model.get("SS_USER_NO"));                        
                        modelSPD = (Map<String, Object>)dao.delete(modelSPD);
                        ServiceUtil.isValidReturnCode("WMSPK021", String.valueOf(modelSPD.get("O_MSG_CODE")), (String)modelSPD.get("O_MSG_NAME"));
                    }
                }
                if(!main.get("ST_GUBUN").equals("DELETE")){
                    Map<String, Object> modelSP = new HashMap<String, Object>();
                    modelSP.put("I_IN_WH_ID",     main.get("vrInWhId"));
                    modelSP.put("I_OUT_WH_ID",    main.get("vrOutWhId"));
                    modelSP.put("I_CUST_ID",      main.get("custId"));
                    modelSP.put("I_RITEM_ID",     main.get("ritemId"));
                    modelSP.put("I_UOM_ID",       main.get("uomId"));
                    modelSP.put("I_WORK_QTY",     main.get("qty"));
                    modelSP.put("I_MAKE_CUST_CD", "");
                    
                    modelSP.put("I_LC_ID",        model.get("SS_SVC_NO"));
                    modelSP.put("I_WORK_IP",      model.get("SS_CLIENT_IP"));
                    modelSP.put("I_USER_NO",      model.get("SS_USER_NO"));
                    
                    if(main.get("type").equals("1")){
                        // 임가공 조립
                    	modelSP = (Map<String, Object>) dao.save(modelSP);
                    	ServiceUtil.isValidReturnCode("WMSPK021", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
                       
                    }else if(main.get("type").equals("2")){
                        // 임가공 해체
                    	modelSP = (Map<String, Object>)dao.save2(modelSP);
                    	ServiceUtil.isValidReturnCode("WMSPK021", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));

                    }
                }
            }              
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );                

        }catch (Exception e) {
            throw e;
        }
        return m;
    }


    /**
     * Method ID : listExcel
     * Method 설명 : 임가공 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : listSub
     * Method 설명 : 임가공 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.listSub(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listSub2
     * Method 설명 : 임가공 상세 조회2
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.listSub2(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : detailPop
     * Method 설명 : 임가공조립 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.popdetail(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    
    /**
     * 
     * Method ID   : saveOrderKit
     * Method 설명    : 번들보충
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOrderKit(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                
                //session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOrderKit(modelIns);
                ServiceUtil.isValidReturnCode("WMSPK021", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );                

        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
	 * Method ID   : workList
	 * Method 설명 : 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> workList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<String> workIdArr = new ArrayList();
        String[] spVrWorkIdStr = model.get("vrWorkIdStr").toString().split(",");
        for (String keyword : spVrWorkIdStr ){
        	workIdArr.add(keyword);
        }
        model.put("workIdArr", workIdArr);
        
		map.put("WORKLIST", dao.workList(model));
		return map;
	}
}
