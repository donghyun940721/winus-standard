package com.logisall.winus.wmspk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.PDF.model.Wmsst070HeardlVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070InDetailVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070OutDetailVO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPK207Dao")
public class WMSPK207Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 임가공 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspk207.searchKit", model);
    }
    
    /**
     * Method ID : poplist
     * Method 설명 : 임가공조립 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet poplist(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitWork", model);
    }
    
    /**
     * Method ID : popdetail
     * Method 설명 : 임가공조립 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet popdetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitWorkDetail", model);
    }

    /**
     * Method ID  : updateKitComplete
     * Method 설명  : 임가공조립확정
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object updateKitComplete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_kit_complete", model);
        return model;
    }
    /**
     * Method ID  : updateUnkitComplete
     * Method 설명  : 임가공해체확정
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object updateUnkitComplete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_unkit_complete", model);
        return model;
    }

     
    
    /**
     * Method ID    : saveSimpleOut
     * Method 설명      : 번들보충
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOrderKit(Map<String, Object> model){
        executeUpdate("wmspk207.pk_wmsst070.sp_order_kit", model);
        return model;
    }
    
    /**
     * Method ID    : selectHeader
     * Method 설명      : 임가공 작업지시서발행 헤더(조립)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Map<String, Object> selectHeaderMap(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeaderMap", model);
    }
    
    public Wmsst070HeardlVO selectHeader(Map<String, Object> model) throws Exception{
        return (Wmsst070HeardlVO)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeader", model);
    }
    
    /**
     * Method ID    : selectInDeatil
     * Method 설명      : 임가공 작업지시서발행 입고 디테일(조립)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectInDeatilList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("wmsst500.selectDetailList", model);
    }
    
    public Wmsst070InDetailVO[] selectInDeatil(Map<String, Object> model) throws Exception{
        return (Wmsst070InDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.selectDetail", model).toArray(new Wmsst070InDetailVO[0]);
    }    
    /**
     * Method ID    : selectOutDeatil
     * Method 설명      : 임가공 작업지시서발행 출고 디테일(조립)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectOutDeatilList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2List", model);
    }
    
    public Wmsst070OutDetailVO[] selectOutDeatil(Map<String, Object> model) throws Exception{
        return (Wmsst070OutDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2", model).toArray(new Wmsst070OutDetailVO[0]);
    }    
    
    /**
     * Method ID    : selectHeader2
     * Method 설명      : 임가공 작업지시서발행 헤더(해체)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Map<String, Object> selectHeader2Map(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeader2Map", model);
    }
    
    public Wmsst070HeardlVO selectHeader2(Map<String, Object> model) throws Exception{
        return (Wmsst070HeardlVO)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeader2", model);
    }    
    /**
     * Method ID    : selectInDeatil2
     * Method 설명      : 임가공 작업지시서발행 입고 디테일(해체)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectInDeatil2List(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsst500.select2DetailList", model);
    }
    
    public Wmsst070InDetailVO[] selectInDeatil2(Map<String, Object> model) throws Exception{
        return (Wmsst070InDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.select2Detail", model).toArray(new Wmsst070InDetailVO[0]);
    }    
    
    /**
     * Method ID    : selectOutDeatil2
     * Method 설명      : 임가공 작업지시서발행 출고 디테일(해체)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectOutDeatil2List(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2List", model);
    }
    
    public Wmsst070OutDetailVO[] selectOutDeatil2(Map<String, Object> model) throws Exception{
        return (Wmsst070OutDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2", model).toArray(new Wmsst070OutDetailVO[0]);
    }    
    
    /**
     * Method ID  : workList
     * Method 설명  : workList
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object workList(Map<String, Object> model){
        return executeQueryForList("wmspk207.workList", model);
    }
    
    /**
     * Method ID	: lableDataSearchHeaderMap
     * Method 설명	: 포장작업관리 라벨 해더
     * 작성자			: wdy
     * @param model
     * @return
     */
    public Map<String, Object> lableDataSearchHeaderMap(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmspk207.lableDataSearchHeaderMap", model);
    }
    /**
     * Method ID	: lableDataSearchDetailMap
     * Method 설명	: 포장작업관리 라벨 디테일
     * 작성자			: wdy
     * @param model
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > lableDataSearchDetailMap(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmspk207.lableDataSearchDetailMap", model);
    }
    
    /**
     * Method ID	: workListDataSearchDetailMap
     * Method 설명	: 포장작업관리 작업지시서 디테일
     * 작성자			: wdy
     * @param model
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > workListDataSearchDetailMap(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmspk207.workListDataSearchDetailMap", model);
    }
    
    /**
     * Method ID  : getWorkData
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getWorkData(Map<String, Object> model){
        return executeQueryForList("wmspk207.getWorkData", model);
    }
}
