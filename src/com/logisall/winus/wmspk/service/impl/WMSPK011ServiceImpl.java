package com.logisall.winus.wmspk.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmspk.service.WMSPK011Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.logisall.winus.frm.exception.BizException;

@Service("WMSPK011Service")
public class WMSPK011ServiceImpl implements WMSPK011Service{
    protected Log log = LogFactory.getLog(this.getClass());

    private final static String[] CHECK_VALIDATE_WMSPK011 = {"SET_RITEM_ID", "PART_RITEM_ID"};
    
    @Resource(name = "WMSPK011Dao")
    private WMSPK011Dao dao;
    
    /**
     * 
     * 대체 Method ID    : list
     * 대체 Method 설명      : 세트상품 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * Method ID : genKitWork
     * Method 설명 : 임가공 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> genKitWork(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
            int iuCnt   = Integer.parseInt(model.get("selectIds").toString());
            
            if(iuCnt > 0){
                for(int i=0; i<iuCnt; i++){
                	int trunCnt = Integer.parseInt(model.get("TURN_CNT"+i).toString());
                	
                	for(int k=0; k<trunCnt; k++){
                		Map<String, Object> modelSP = new HashMap<String, Object>();
                        modelSP.put("I_LC_ID",        model.get("SS_SVC_NO"));
                        
                        modelSP.put("I_IN_WH_ID"		,model.get("IN_WH_ID"+i));
                        modelSP.put("I_OUT_WH_ID"		,model.get("OUT_WH_ID"+i));
                        modelSP.put("I_CUST_ID"			,model.get("CUST_ID"+i));
                        modelSP.put("I_RITEM_ID"		,model.get("SET_RITEM"+i));
                        modelSP.put("I_UOM_ID"			,model.get("UOM_ID"+i));
                        
                        modelSP.put("I_WORK_QTY"		,model.get("WORK_QTY"+i));
                        modelSP.put("I_MAKE_RUN_TIME"	,model.get("MAKE_RUN_TIME"+i));
                        modelSP.put("I_MAKE_CUST_CD"	,model.get("MAKE_CUST_CD"+i));
                        modelSP.put("I_CUST_LOT_NO"		,model.get("CUST_LOT_NO"+i));
                        
                        modelSP.put("I_WORK_IP",      model.get("SS_CLIENT_IP"));
                        modelSP.put("I_USER_NO",      model.get("SS_USER_NO"));
                        
                        // 임가공 조립
                    	modelSP = (Map<String, Object>) dao.genKitWork(modelSP);
                        ServiceUtil.isValidReturnCode("WMSST070", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
                	}
                }
            }    
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", 0);
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );                

        }catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   : search
     * Method 설명    : 템플릿 정보에 따른 컬럼
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("vrTemplateId"     , model.get("vrTemplateId")  );
            modelIns.put("vrCustSeq"        , model.get("vrCustSeq"));

            // dao
            modelIns = (Map<String, Object>)dao.searchTpCol(modelIns);
            ServiceUtil.isValidReturnCode("PK_WMSCM210", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            //우선 여기서 다국어처리해놓자
            //.vm에서 다국어적용안되길래 여기서 했음
            List<Map<String, Object>> dsTpcolList = (List<Map<String, Object>>)modelIns.get("DS_TPCOL");
            for(int i = 0 ; i < dsTpcolList.size() ; i++){
               dsTpcolList.get(i).put("FORMAT_COL_NM", MessageResolver.getText((String)dsTpcolList.get(i).get("FORMAT_COL_NM")));
            }
            m.put("DS_TPCOL", modelIns.get("DS_TPCOL"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJava
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExcelOrderJava(Map<String, Object> model,  Map<String, Object> model2) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		List<Map<String, Object>> listHeader = (List<Map<String, Object>>)model2.get("DS_TPCOL");
		
		int listHeaderCnt = listHeader.size();
		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
                String[] no             = new String[listBodyCnt];     
                
                String[] outReqDt       = new String[listBodyCnt];         
                String[] inReqDt        = new String[listBodyCnt];     
                String[] custOrdNo      = new String[listBodyCnt];     
                String[] custOrdSeq     = new String[listBodyCnt];    
                String[] trustCustCd    = new String[listBodyCnt];     
                
                String[] transCustCd    = new String[listBodyCnt];                     
                String[] transCustTel   = new String[listBodyCnt];         
                String[] transReqDt     = new String[listBodyCnt];     
                String[] custCd         = new String[listBodyCnt];     
                String[] ordQty         = new String[listBodyCnt];     
                
                String[] uomCd          = new String[listBodyCnt];                
                String[] sdeptCd        = new String[listBodyCnt];         
                String[] salePerCd      = new String[listBodyCnt];     
                String[] carCd          = new String[listBodyCnt];     
                String[] drvNm          = new String[listBodyCnt];     
                
                String[] dlvSeq         = new String[listBodyCnt];                
                String[] drvTel         = new String[listBodyCnt];         
                String[] custLotNo      = new String[listBodyCnt];     
                String[] blNo           = new String[listBodyCnt];     
                String[] recDt          = new String[listBodyCnt];     
                
                String[] outWhCd        = new String[listBodyCnt];                
                String[] inWhCd         = new String[listBodyCnt];         
                String[] makeDt         = new String[listBodyCnt];     
                String[] timePeriodDay  = new String[listBodyCnt];     
                String[] workYn         = new String[listBodyCnt];     
                
                String[] rjType         = new String[listBodyCnt];                
                String[] locYn          = new String[listBodyCnt];         
                String[] confYn         = new String[listBodyCnt];     
                String[] eaCapa         = new String[listBodyCnt];     
                String[] inOrdWeight    = new String[listBodyCnt];     
                
                String[] itemCd         = new String[listBodyCnt];                
                String[] itemNm         = new String[listBodyCnt];         
                String[] transCustNm    = new String[listBodyCnt];     
                String[] transCustAddr  = new String[listBodyCnt];     
                String[] transEmpNm     = new String[listBodyCnt];     
                
                String[] remark         = new String[listBodyCnt];                
                String[] transZipNo     = new String[listBodyCnt];         
                String[] etc2           = new String[listBodyCnt];     
                String[] unitAmt        = new String[listBodyCnt];     
                String[] transBizNo     = new String[listBodyCnt];     
                
                String[] inCustAddr     = new String[listBodyCnt];                
                String[] inCustCd       = new String[listBodyCnt];         
                String[] inCustNm       = new String[listBodyCnt];     
                String[] inCustTel      = new String[listBodyCnt];     
                String[] inCustEmpNm    = new String[listBodyCnt];     
                
                String[] expiryDate     = new String[listBodyCnt];
                String[] salesCustNm    = new String[listBodyCnt];
                String[] zip     		= new String[listBodyCnt];
                String[] addr     		= new String[listBodyCnt];
                String[] phone1    	 	= new String[listBodyCnt];
                
                String[] etc1    		= new String[listBodyCnt];
                String[] unitNo    		= new String[listBodyCnt];
                
                String[] deliveryNo    	= new String[listBodyCnt]; //I_DELIVERY_NO
                String[] shipmentNo    	= new String[listBodyCnt]; //I_SHIPMENT_NO
                String[] poiNo    		= new String[listBodyCnt]; //I_POI_NO
                String[] invoiceNo    	= new String[listBodyCnt]; //I_INVOICE_NO

                String[] timeDate       = new String[listBodyCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[listBodyCnt];   //상품유효기간만료일
                String[] timeUseEnd     = new String[listBodyCnt];   //소비가한만료일
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	String OUT_REQ_DT      = "";
        			String IN_REQ_DT       = "";
        			String CUST_ORD_NO     = "";
        			String CUST_ORD_SEQ    = "";
        			String TRUST_CUST_CD   = "";
        			String TRANS_CUST_CD   = "";
        			String TRANS_CUST_TEL  = "";
        			String TRANS_REQ_DT    = "";
        			String CUST_CD         = "";
        			String ORD_QTY         = "";
        			String UOM_CD          = "";
        			String SDEPT_CD        = "";
        			String SALE_PER_CD     = "";
        			String CAR_CD          = "";
        			String DRV_NM          = "";
        			String DLV_SEQ         = "";
        			String DRV_TEL         = "";
        			String CUST_LOT_NO     = "";
        			String BL_NO           = "";
        			String REC_DT          = "";
        			String OUT_WH_CD       = "";
        			String IN_WH_CD        = "";
        			String MAKE_DT         = "";
        			String TIME_PERIOD_DAY = "";
        			String WORK_YN         = "";
        			String RJ_TYPE         = "";
        			String LOC_YN          = "";
        			String CONF_YN         = "";
        			String EA_CAPA         = "";
        			String IN_ORD_WEIGHT   = "";
        			String ITEM_CD         = "";
        			String ITEM_NM         = "";
        			String TRANS_CUST_NM   = "";
        			String TRANS_CUST_ADDR = "";
        			String TRANS_EMP_NM    = "";
        			String REMARK          = "";
        			String TRANS_ZIP_NO    = "";
        			String ETC2            = "";
        			String UNIT_AMT        = "";
        			String TRANS_BIZ_NO    = "";
        			String IN_CUST_ADDR    = "";
        			String IN_CUST_CD      = "";
        			String IN_CUST_NM      = "";
        			String IN_CUST_TEL     = "";
        			String IN_CUST_EMP_NM  = "";
        			String EXPIRY_DATE     = "";
        			String SALES_CUST_NM   = "";
        			String ZIP             = "";
        			String ADDR            = "";
        			String PHONE_1         = "";
        			String ETC1            = "";
        			String UNIT_NO         = "";
        			String DELIVERY_NO     = "";
        			String SHIPMENT_NO     = "";
        			String POI_NO          = "";
        			String INVOICE_NO      = "";
        			String TIME_DATE       = "";
        			String TIME_DATE_END   = "";
        			String TIME_USE_END    = "";
        			
        			for(int k = 0 ; k < listHeaderCnt ; k++){
        				JsonObject object = (JsonObject) listBody.get(i);
        				
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_REQ_DT")){OUT_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_REQ_DT")){IN_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_NO")){CUST_ORD_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_ORD_SEQ")){CUST_ORD_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRUST_CUST_CD")){TRUST_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_CD")){TRANS_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_TEL")){TRANS_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_REQ_DT")){TRANS_REQ_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_CD")){CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ORD_QTY")){ORD_QTY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UOM_CD")){UOM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SDEPT_CD")){SDEPT_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALE_PER_CD")){SALE_PER_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CAR_CD")){CAR_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_NM")){DRV_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DLV_SEQ")){DLV_SEQ = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DRV_TEL")){DRV_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CUST_LOT_NO")){CUST_LOT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("BL_NO")){BL_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REC_DT")){REC_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("OUT_WH_CD")){OUT_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_WH_CD")){IN_WH_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("MAKE_DT")){MAKE_DT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_PERIOD_DAY")){TIME_PERIOD_DAY = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("WORK_YN")){WORK_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("RJ_TYPE")){RJ_TYPE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("LOC_YN")){LOC_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("CONF_YN")){CONF_YN = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EA_CAPA")){EA_CAPA = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_ORD_WEIGHT")){IN_ORD_WEIGHT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_CD")){ITEM_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ITEM_NM")){ITEM_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_NM")){TRANS_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_CUST_ADDR")){TRANS_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_EMP_NM")){TRANS_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("REMARK")){REMARK = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_ZIP_NO")){TRANS_ZIP_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC2")){ETC2 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_AMT")){UNIT_AMT = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TRANS_BIZ_NO")){TRANS_BIZ_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_ADDR")){IN_CUST_ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_CD")){IN_CUST_CD = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_NM")){IN_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_TEL")){IN_CUST_TEL = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("IN_CUST_EMP_NM")){IN_CUST_EMP_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("EXPIRY_DATE")){EXPIRY_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SALES_CUST_NM")){SALES_CUST_NM = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ZIP")){ZIP = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ADDR")){ADDR = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("PHONE_1")){PHONE_1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("ETC1")){ETC1 = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("UNIT_NO")){UNIT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("DELIVERY_NO")){DELIVERY_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("SHIPMENT_NO")){SHIPMENT_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("POI_NO")){POI_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("INVOICE_NO")){INVOICE_NO = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE")){TIME_DATE = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_DATE_END")){TIME_DATE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        				if(listHeader.get(k).get("FORMAT_BIND_COL").equals("TIME_USE_END")){TIME_USE_END = object.get("S_"+k).toString().replaceAll("\"", "");};
        			}
     				
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         = OUT_REQ_DT;    
                    inReqDt[i]          = IN_REQ_DT;    
                    custOrdNo[i]        = CUST_ORD_NO;    
                    custOrdSeq[i]       = CUST_ORD_SEQ;    
                    trustCustCd[i]      = TRUST_CUST_CD;    
                    
                    transCustCd[i]      = TRANS_CUST_CD;    
                    transCustTel[i]     = TRANS_CUST_TEL;    
                    transReqDt[i]       = TRANS_REQ_DT;    
                    custCd[i]           = CUST_CD;    
                    ordQty[i]           = ORD_QTY;    
                    
                    uomCd[i]            = UOM_CD;    
                    sdeptCd[i]          = SDEPT_CD;    
                    salePerCd[i]        = SALE_PER_CD;    
                    carCd[i]            = CAR_CD;
                    drvNm[i]            = DRV_NM;
                    
                    dlvSeq[i]           = DLV_SEQ;    
                    drvTel[i]           = DRV_TEL;    
                    custLotNo[i]        = CUST_LOT_NO;    
                    blNo[i]             = BL_NO;    
                    recDt[i]            = REC_DT;    
                    
                    outWhCd[i]          = OUT_WH_CD;
                    inWhCd[i]           = IN_WH_CD;
                    makeDt[i]           = MAKE_DT;
                    timePeriodDay[i]    = TIME_PERIOD_DAY;
                    workYn[i]           = WORK_YN;
                    
                    rjType[i]           = RJ_TYPE;    
                    locYn[i]            = LOC_YN;    
                    confYn[i]           = CONF_YN;    
                    eaCapa[i]           = EA_CAPA;    
                    inOrdWeight[i]      = IN_ORD_WEIGHT;   
                    
                    itemCd[i]           = ITEM_CD;    
                    itemNm[i]           = ITEM_NM;    
                    transCustNm[i]      = TRANS_CUST_NM;    
                    transCustAddr[i]    = TRANS_CUST_ADDR;    
                    transEmpNm[i]       = TRANS_EMP_NM;    

                    remark[i]           = REMARK;    
                    transZipNo[i]       = TRANS_ZIP_NO;    
                    etc2[i]             = ETC2;    
                    unitAmt[i]          = UNIT_AMT;    
                    transBizNo[i]       = TRANS_BIZ_NO;    
                    
                    inCustAddr[i]       = IN_CUST_ADDR;   
                    inCustCd[i]         = IN_CUST_CD;    
                    inCustNm[i]         = IN_CUST_NM;    
                    inCustTel[i]        = IN_CUST_TEL;    
                    inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                    
                    expiryDate[i]       = EXPIRY_DATE;
                    salesCustNm[i]      = SALES_CUST_NM;
                    zip[i]       		= ZIP;
                    addr[i]       		= ADDR;
                    phone1[i]       	= PHONE_1;
                    
                    etc1[i]      		= ETC1;
                    unitNo[i]      		= UNIT_NO;
                    
                    deliveryNo[i]      	= DELIVERY_NO;
                    shipmentNo[i]      	= SHIPMENT_NO;
                    poiNo[i]      		= POI_NO;
                    invoiceNo[i]      	= INVOICE_NO;
                    
                    timeDate[i]         = TIME_DATE;      
                    timeDateEnd[i]      = TIME_DATE_END;      
                    timeUseEnd[i]       = TIME_USE_END;  
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("no"  , no);
                if(model.get("vrOrdType").equals("I")){
                    modelIns.put("reqDt"     	, inReqDt);
                    modelIns.put("whCd"      	, inWhCd);
                }else{
                    modelIns.put("reqDt"     	, outReqDt);
                    modelIns.put("whCd"      	, outWhCd);
                }
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
               
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
               
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);      
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("phone1"       	, phone1);
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                
                modelIns.put("deliveryNo"     	, deliveryNo);
                modelIns.put("shipmentNo"     	, shipmentNo);
                modelIns.put("poiNo"     	 	, poiNo);
                modelIns.put("invoiceNo"     	, invoiceNo);
                
                modelIns.put("time_date"        , timeDate);
                modelIns.put("time_date_end"    , timeDateEnd);                
                modelIns.put("time_use_end"     , timeUseEnd);  
                modelIns.put("vrOrdType"		, model.get("vrOrdType"));
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

                //dao                
                modelIns = (Map<String, Object>)dao.saveExcelOrder(modelIns);
                
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                m.put("O_CUR", modelIns.get("O_CUR"));                
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
