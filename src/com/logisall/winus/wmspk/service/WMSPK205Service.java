package com.logisall.winus.wmspk.service;

import java.util.Map;

public interface WMSPK205Service {
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
}
