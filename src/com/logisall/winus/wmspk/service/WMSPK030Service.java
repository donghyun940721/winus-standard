package com.logisall.winus.wmspk.service;

import java.util.Map;


public interface WMSPK030Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> workInvcNo(Map<String, Object> model) throws Exception;
   
}
