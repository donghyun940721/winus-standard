package com.logisall.winus.wmspk.service;

import java.util.Map;

public interface WMSPK021Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub2(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveOrderKit(Map<String, Object> model) throws Exception;
    public Map<String, Object> workList(Map<String, Object> model) throws Exception;
}
