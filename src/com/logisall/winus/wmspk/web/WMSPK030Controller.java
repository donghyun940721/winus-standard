package com.logisall.winus.wmspk.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.logisall.winus.wmspk.service.WMSPK030Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSPK030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPK030Service")
	private WMSPK030Service service;
	
	@Resource(name = "WMSOP642Service")
	private WMSOP642Service WMSOP642_service;
	
	/**
	 * Method ID : mn
	 * Method 설명 : 라벨송장발행 
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSPK030.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmspk/WMSPK030");
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}


	/**
	 * Method ID : list
	 * Method 설명 : 라벨송장발행 라벨 조회
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK030/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jqGridJsonView");
		Map<String,Object> m = new HashMap<String, Object>();
		try {
			m = service.list(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID     : getPrintInvcNO
     * Method 설명         : 송장 발행 및 송장출력
     * 작성자                 : schan
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSPK030/getPrintInvcNo.action")
    public ModelAndView getPrintInvcNO(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        String errLog = "";
        try {
            /* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
            if(model.get("PARCEL_COM_TY").equals("04") || model.get("PARCEL_COM_TY").equals("06")){
                int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
                
                //주문번호 박스구분을 기준으로, 송장 발행여부 선행 확인후 수행한다. 
                Map<String, Object> modelIns = new HashMap<String,Object>();
                errLog = "[송장 발행 존재 확인]";
                modelIns = new HashMap<String,Object>();
                modelIns.put("LC_ID", (String)model.get("LC_ID"));
                modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
                List<String> gubun = new ArrayList<String>();
                for(int i = 0 ; i < tmpCnt; i++){
                    gubun.add((String)model.get("BOX_NO"+i));
                }
                modelIns.put("GUBUN", gubun.toArray(new String[gubun.size()]));
                modelIns.put("ORD_ID", (String)model.get("ORD_ID"));
                
                m = service.workInvcNo(modelIns);
                List list0 = (List)m.get("LIST");
                if(list0.size() > 0){
                    m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : LIST=" + list0);
                    mav.addAllObjects(m);
                    return mav;
                }
                
                // time set
                //long beforeTime = System.currentTimeMillis();                
                errLog = "택배 송장 발행";
                //택배 송장 발행
                Map<String, Object> WMSOP642Model = new HashMap<String, Object>();
                for(int i = 0 ; i < tmpCnt; i++){
                    WMSOP642Model.put("LC_ID"+i, (String)model.get("LC_ID"));
                    WMSOP642Model.put("CUST_ID"+i, (String)model.get("CUST_ID"));
                    WMSOP642Model.put("TRACKING_NO"+i, (String)model.get("BOX_NO"+i));
                    WMSOP642Model.put("DENSE_FLAG"+i, (String)model.get("DENSE_FLAG"+i));
                    WMSOP642Model.put("ORD_QTY"+i, (String)model.get("CHK_QTY"+i));
                    WMSOP642Model.put("ORD_ID"+i, (String)model.get("ORD_ID"));
                    WMSOP642Model.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
                    WMSOP642Model.put("INVC_NO"+i, (String)model.get("INVC_NO"+i));
                }
                WMSOP642Model.put("selectIds", (String)model.get("selectIds"));
                WMSOP642Model.put("PARCEL_SEQ_YN", (String)model.get("PARCEL_SEQ_YN"));
                WMSOP642Model.put("PARCEL_COM_TY", (String)model.get("PARCEL_COM_TY"));
                WMSOP642Model.put("PARCEL_COM_TY_SEQ", (String)model.get("PARCEL_COM_TY_SEQ"));
                WMSOP642Model.put("PARCEL_ORD_TY", (String)model.get("PARCEL_ORD_TY"));
                WMSOP642Model.put("PARCEL_PAY_TY", (String)model.get("PARCEL_PAY_TY"));
                WMSOP642Model.put("PARCEL_BOX_TY", (String)model.get("PARCEL_BOX_TY"));
                WMSOP642Model.put("PARCEL_ETC_TY", (String)model.get("PARCEL_ETC_TY"));
                WMSOP642Model.put("PARCEL_PAY_PRICE", (String)model.get("PARCEL_PAY_PRICE"));
                WMSOP642Model.put("ADD_FLAG", (String)model.get("ADD_FLAG"));
                WMSOP642Model.put("DIV_FLAG", (String)model.get("DIV_FLAG"));
                WMSOP642Model.put("WORK_TYPE", (String)model.get("WORK_TYPE"));
                
                WMSOP642Model.put("SS_CLIENT_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                WMSOP642Model.put("SS_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                String hostUrl = request.getServerName();
                WMSOP642Model.put("hostUrl", hostUrl);
                
                m = WMSOP642_service.DlvInvcNoOrderCommTotal(WMSOP642Model);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                //beforeTime = System.currentTimeMillis();
                if(!m.get("header").toString().equals("Y")){
                    m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(1) : " + m.get("message"));
                    mav.addAllObjects(m);
                    return mav;
                }
                
                errLog = "택배 송장 조회";

                m = service.workInvcNo(modelIns);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                //beforeTime = System.currentTimeMillis();
                List list = (List)m.get("LIST");
                if(list == null || list.isEmpty() == true || list.size() < 1){
                    m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(2) : LIST=" + list);
                    mav.addAllObjects(m);
                    return mav;
                }
                                
                errLog = "택배 송장 출력";
                modelIns = new HashMap<String,Object>();
                for(int i = 0 ; i < list.size() ; i++){
                    Map<String,Object> invcInfo = (Map<String,Object>)list.get(i);
                    modelIns.put("ORD_ID"+i, (String)invcInfo.get("ORD_ID"));
                    modelIns.put("ORD_SEQ"+i, String.valueOf(invcInfo.get("ORD_SEQ")));
                    modelIns.put("INVC_NO"+i, (String)invcInfo.get("INVC_NO"));
                }
                modelIns.put("PARCEL_COM_TY", (String)model.get("PARCEL_COM_TY"));
                modelIns.put("ORDER_FLAG", "01");
                modelIns.put("selectIds", list.size());
                
                modelIns.put(ConstantIF.SS_SVC_NO, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_USER_NO));
                m = WMSOP642_service.dlvPrintPoiNoUpdate(modelIns);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                if(!m.get("POI_NO_YN").equals("Y") || m.get("POI_NO") == null){
                    m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(5) : " + m.get("MSG"));
                    mav.addAllObjects(m);
                    return mav;
                }else{
                    //택배 설정별 운송장 상품명 명칭 설정 정보
                    if(model.get("PARCEL_COM_TY_SEQ") != null && !model.get("PARCEL_COM_TY_SEQ").equals("")){
                        m.put("INVC_ITEMNM_INFO", WMSOP642_service.dlvInvcItemNmSetInfo(model));
                    }
                }
            }else{
                m.put("header", "E");
                m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get picking info :", e);
            }
            m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
        }
        mav.addAllObjects(m);
        return mav;
    }


}