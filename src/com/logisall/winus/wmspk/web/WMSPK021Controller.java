package com.logisall.winus.wmspk.web;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmspk.service.WMSPK021Service;
import com.m2m.jdfw5x.document.DocumentView;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSPK021Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPK021Service")
	private WMSPK021Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 임가공 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSPK021.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmspk/WMSPK021");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : popmn
	 * Method 설명 : 임가공조립 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK021pop.action")
	public ModelAndView popmn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmspk/WMSPK021E2");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get pop info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : popmn
	 * Method 설명 : 임가공해체 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK021pop2.action")
	public ModelAndView popmn2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmspk/WMSPK021E3");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get pop info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : popdetail
	 * Method 설명 : 임가공조립 상세 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK021/popdetail.action")
	public ModelAndView popdetail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detailPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get pop detail :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 임가공 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK021/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : sublist
	 * Method 설명 : 임가공 상세 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK021/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list(sub) :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : sublist2
	 * Method 설명 : 임가공 상세 조회2
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK021/sublist2.action")
	public ModelAndView sublist2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list(sub) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 임가공 조립 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSPK021/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : complete
	 * Method 설명 : 임가공 조립 확정
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSPK021/complete.action")
	public ModelAndView complete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.updateComplete(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to complete :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 임가공 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPK021/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업구분"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일자"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업시간"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("완성품"), "3", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량"), "5", "5", "0", "0", "200"},
                                   {"UOM", "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고"), "7", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "10", "11", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_STAT_EXCEL"          , "S"},
                                    {"WORK_DT"           , "S"},
                                    {"WORK_TM"           , "S"},
                                    {"RITEM_CD"          , "S"},
                                    {"RITEM_NM"          , "S"},
                                    
                                    {"WORK_QTY"            , "N"},
                                    {"UOM_NM"            , "S"},
                                    {"WH_CD"            , "S"},
                                    {"WH_NM"            , "S"},
                                    {"LOC_CD"            , "S"},
                                    
                                    {"CUST_CD"            , "S"},
                                    {"CUST_NM"            , "S"},
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("임가공");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : saveOrderKit
	 * Method 설명      : 번들보충
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPK021/saveOrderKit.action")
	public ModelAndView saveOrderKit(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOrderKit(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : preView
	 * Method 설명 : PDF 오픈
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 */
	@RequestMapping("/WMSPK021/preViewOpen.action")
	public void preViewOpen(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			// log.info("[ model ] :" + model);

			String pathDirectory = request.getParameter("pdfFilePath");
			String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
			String pdfFileName = request.getParameter("pdfFileName");

			// log.info("[ pdfFilePath ] :" + pdfFilePath);
			// log.info("[ pdfFileName ] :" + pdfFileName);

			DocumentView.getPDFView(request, response, new File(pdfFilePath, pdfFileName));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to preview :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}
	
	/*-
	 * Method ID   : getTypicalCust
	 * Method 설명 : 물류센터 별 대표화주값 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSPK021/workList.action")
	public ModelAndView workList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.workList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}