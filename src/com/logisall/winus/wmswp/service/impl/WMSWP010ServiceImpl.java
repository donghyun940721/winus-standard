package com.logisall.winus.wmswp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmswp.service.WMSWP010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSWP010Service")
public class WMSWP010ServiceImpl extends AbstractServiceImpl implements WMSWP010Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

   @Resource(name = "WMSWP010Dao")
    private WMSWP010Dao dao;

   /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);

        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }

        map.put("LIST", dao.list(model));
        return map;
    }
    
	/**
	 * Method ID : selectReqPerson 
	 * Method 설명 : 셀렉트 박스 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> detail(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			String boardId = (String) model.get("vrBoardId");
			String wBoardType = (String) model.get("wBoardType");	
			
			model.put("BOARD_TYPE", wBoardType);
			model.put("BOARD_IDX", boardId);
			
			int cnt = dao.updateCount(model);	
			if ( cnt > 0) {
				map.put("DETAIL", dao.selectBoardInfo(model));	
				map.put("CLIENT", dao.selectClientList(model));
				map.put("FILE", dao.selectFileInfoList(model));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}		
		return map;
	}
    
	/**
	 * Method ID : save Method 설명 : 통합 HelpDesk 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			String flag = (String) model.get("FLAG");
			
			// log.info("############ flag :" + flag);

			if ("".equals(flag)) { // 등록
				List<String> fileMngNoList = null;
				if ( model.get("D_ATCH_FILE_NAME") != null ) {	
					Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
					Object attachFileState = model.get("FILE_STATE");
					fileMngNoList = new ArrayList<String>();
										
					if (attachFileInfo instanceof String[]) {
						for (int i=0; i< ((String[])attachFileInfo).length; i++) {							
							String str = ((String[])attachFileInfo)[i];
							String state = ((String[])attachFileState)[i];
							
							// log.info("############ state :" + state);
							
							if ( str != null && StringUtils.isNotEmpty(str)
									&& state != null && "I".equals(state) ) {
								
								Map<String, Object> modelDt = new HashMap<String, Object>();
								String fileRoute = ((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
								String fileSize = ((String[])model.get("FILE_SIZE"))[i];								
								String ext = "." + str.substring((str.lastIndexOf('.') + 1));							
								String fileId = CommonUtil.getLocalDateTime() + i + ext;
								modelDt.put("FILE_ID", fileId);
								modelDt.put("ATTACH_GB", "BOARD");
								modelDt.put("FILE_VALUE", "tmsba230");
								modelDt.put("FILE_PATH", fileRoute);
								modelDt.put("ORG_FILENAME", str);
								modelDt.put("FILE_EXT", ext);
								modelDt.put("FILE_SIZE", fileSize);
								
								String fileMngNo = (String) dao.fileUpload(modelDt);
								
								// log.info("############ fileMngNo :" + fileMngNo);
								fileMngNoList.add(fileMngNo);
							}
						}
					} else if (attachFileInfo instanceof String) {
						Map<String, Object> modelDt = new HashMap<String, Object>();
						String str = (String) model.get("D_ATCH_FILE_NAME");
						String ext = "." + str.substring((str.lastIndexOf('.') + 1));
						
						int fileSeq = 1;
						String fileId = "";
						fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
						modelDt.put("FILE_ID", fileId);
						modelDt.put("ATTACH_GB", "BOARD");
						modelDt.put("FILE_VALUE", "tmsba230");
						modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE"));
						modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME"));
						modelDt.put("FILE_EXT", ext);
						modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));
						
						String fileMngNo = (String) dao.fileUpload(modelDt);			
						// log.info("############ fileMngNo :" + fileMngNo);
						
						fileMngNoList.add(fileMngNo);
					}
				}
				
				
				
				
				String boardStep = (String)model.get("D_BOARD_STEP");
				if ( StringUtils.isEmpty(boardStep)) {
					boardStep = "0";
				}
				String boardLevel = (String)model.get("D_BOARD_LEVEL");
				if ( StringUtils.isEmpty(boardLevel)) {
					boardLevel = "0";
				}
				
				String startDt = (String)model.get("D_START_DT");
				if ( startDt != null ) {
					startDt = startDt.replaceAll("-", "");
				}
				
				String endDt = (String)model.get("D_END_DT");
				if ( endDt != null ) {
					endDt = endDt.replaceAll("-", "");
				}
				
				//model.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
				model.put("BOARD_REF", model.get("D_BOARD_REF"));
				model.put("BOARD_STEP", boardStep);
				model.put("BOARD_LEVEL", boardLevel);
				model.put("BOARD_TITLE", model.get("D_BOARD_TITLE"));
				model.put("BOARD_CONTENT", model.get("D_CONTENTS"));				
				model.put("ORG_BOARD_ID", model.get("D_ORG_BOARD_ID"));
				model.put("BOARD_NAME", model.get("D_BOARD_NAME"));
				model.put("BOARD_PWD", model.get("D_BOARD_PWD"));
				model.put("POP_YN", model.get("D_POP_YN"));				
				model.put("DIS_GBN", model.get("D_DIS_GBN"));
				
				model.put("START_DATE", startDt);
				model.put("END_DATE", endDt);
				
				model.put("DEL_YN", "N");	
				model.put("READNUM", 0);				
				
				model.put("BOARD_ID", model.get("SS_USER_NO"));
				
				model.put("BOARD_TYPE", model.get("WBD_TYPE"));
				model.put("WSTATUS", model.get("WSTATUS"));
				
				if (fileMngNoList != null) {
					int fileSize = fileMngNoList.size();
					switch (fileSize){
						case 1:
							model.put("ATTACH_FILE_NO", fileMngNoList.get(0)); 
							break;						
						case 2:
							model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
							model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
							break;
						case 3:
							model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
							model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
							model.put("ATTACH_FILE_NO_2", fileMngNoList.get(2));
							break;
						default:							
							break;
					}
				}
				
				// log.info("############ model :" + model);
				String boardIdx = (String) dao.insert(model);
				
				// log.info("############ boardIdx :" + boardIdx);
				if ( boardIdx != null ) {
					
					List<Map<String, Object> > custInfoList = null;
					String disType = (String)model.get("DIS_GBN");
					if (disType != null) {
						switch (disType){
							case "P":
								custInfoList = dao.selectCustInfoTypeP(model);
								if ( custInfoList != null && !custInfoList.isEmpty() ) {
									for( Map<String, Object> custInfo : custInfoList) {
										custInfo.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
										custInfo.put("BOARD_IDX", boardIdx);
										custInfo.put("CLIENT_CD", custInfo.get("ID"));
										
										dao.insertCustInfo(custInfo);
									}
								}
								break;						
							case "C":
								custInfoList = dao.selectCustInfoTypeC(model);
								if ( custInfoList != null && !custInfoList.isEmpty() ) {
									for( Map<String, Object> custInfo : custInfoList) {
										custInfo.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
										custInfo.put("BOARD_IDX", boardIdx);
										custInfo.put("CLIENT_CD", custInfo.get("ID"));
										
										dao.insertCustInfo(custInfo);
									}
								}							
								break;
							case "E":
								if ( model.get("CUST_INFO") != null ) {	
									Object custInfoObj = model.get("CUST_INFO");
									Map<String, Object> custInfo = null;
									if (custInfoObj instanceof String[]) {
										for (int i=0; i< ((String[])custInfoObj).length; i++) {
											custInfo = new HashMap<String, Object>();
											custInfo.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
											custInfo.put("BOARD_IDX", boardIdx);
											custInfo.put("CLIENT_CD", ((String[])custInfoObj)[i]);
											
											dao.insertCustInfo(custInfo);										
										}									
									} else if (custInfoObj instanceof String) {
										custInfo = new HashMap<String, Object>();
										custInfo.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
										custInfo.put("BOARD_IDX", boardIdx);
										custInfo.put("CLIENT_CD", custInfoObj);
										
										dao.insertCustInfo(custInfo);									
									}
								}
								break;
								
							default:							
								break;
						}
					}
				}

				map.put("MSG", MessageResolver.getMessage("insert.success"));
				
			} else if ("U".equals(flag)) { // 수정				
				String boardIdx = (String)model.get("D_BOARD_IDX");
				String boardType = (String)model.get("D_BOARD_TYPE");
				String wBoardType = (String)model.get("WBD_TYPE");
				
				System.out.println("boardType::::::"+ boardType);
				System.out.println("wBoardType::::::"+ wBoardType);
				
				if ( boardIdx != null && StringUtils.isNotEmpty(boardIdx)
						&& boardType != null && StringUtils.isNotEmpty(boardType)) {
				
					List<String> fileMngNoList = null;
					if ( model.get("D_ATCH_FILE_NAME") != null ) {	
						Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
						Object attachFileId = model.get("FILE_ID");
						fileMngNoList = new ArrayList<String>();
											
						if (attachFileInfo instanceof String[]) {
							for (int i=0; i< ((String[])attachFileInfo).length; i++) {							
								String str = ((String[])attachFileInfo)[i];
								String fileId = ((String[])attachFileId)[i];
								
								// log.info("############ fileId :" + fileId);
								
								if ( str != null && StringUtils.isNotEmpty(str) ) {
									Map<String, Object> modelDt = new HashMap<String, Object>();
									String fileRoute = ((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
									String fileSize = ((String[])model.get("FILE_SIZE"))[i];								
									String ext = "." + str.substring((str.lastIndexOf('.') + 1));
									
									if (fileId == null || "".equals(fileId)) {			
										fileId = CommonUtil.getLocalDateTime() + i + ext;
										
										modelDt.put("FILE_ID", fileId);
										modelDt.put("ATTACH_GB", "BOARD");
										modelDt.put("FILE_VALUE", "tmsba230");
										modelDt.put("FILE_PATH", fileRoute);
										modelDt.put("ORG_FILENAME", str);
										modelDt.put("FILE_EXT", ext);
										modelDt.put("FILE_SIZE", fileSize);
										
										String fileMngNo = (String) dao.fileUpload(modelDt);									
										// log.info("############ fileMngNo :" + fileMngNo);
										
										fileMngNoList.add(fileMngNo);
									} else {
										model.put("D_ATCH_FILE_NAME", fileId);									
										String mngNo = dao.selectMngno(model);
										// log.info("############ mngNo :" + mngNo);
	
										modelDt.put("FILE_PATH", fileRoute);
										modelDt.put("ORG_FILENAME", str);
										modelDt.put("FILE_EXT", ext);
										modelDt.put("FILE_SIZE", fileSize);
										modelDt.put("MNG_NO", mngNo);
										modelDt.put("FILE_ID", fileId);
										
										dao.fileUpdate(modelDt);
										
										fileMngNoList.add(mngNo);
									}
								}
							}
						}
					}
					
					String startDt = (String)model.get("D_START_DT");
					if ( startDt != null ) {
						startDt = startDt.replaceAll("-", "");
					}
					
					String endDt = (String)model.get("D_END_DT");
					if ( endDt != null ) {
						endDt = endDt.replaceAll("-", "");
					}				
					
					model.put("BOARD_TYPE", boardType);
					model.put("WBOARD_TYPE", wBoardType);
					model.put("BOARD_IDX", boardIdx);
					model.put("BOARD_REF", model.get("D_BOARD_REF"));
					model.put("BOARD_STEP", model.get("D_BOARD_STEP"));
					model.put("BOARD_LEVEL", model.get("D_BOARD_LEVEL"));
					model.put("BOARD_TITLE", model.get("D_BOARD_TITLE"));
					model.put("BOARD_CONTENT", model.get("D_CONTENTS"));
					model.put("BOARD_ID", model.get("D_BOARD_ID"));
					model.put("ORG_BOARD_ID", model.get("D_ORG_BOARD_ID"));
					model.put("BOARD_NAME", model.get("D_BOARD_NAME"));
					model.put("BOARD_PWD", model.get("D_BOARD_PWD"));
					model.put("POP_YN", model.get("D_POP_YN"));				
					model.put("DIS_GBN", model.get("D_DIS_GBN"));
					model.put("START_DATE", startDt);
					model.put("END_DATE", endDt);				
					model.put("WSTATUS", model.get("WSTATUS"));
					
					if (fileMngNoList != null) {
						int fileSize = fileMngNoList.size();
						switch (fileSize){
							case 1:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0)); 
								break;						
							case 2:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
								model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
								break;
							case 3:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
								model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
								model.put("ATTACH_FILE_NO_2", fileMngNoList.get(2));
								break;
							default:							
								break;
						}
					}
	
					dao.update(model);
					
					List<Map<String, Object> > custInfoList = null;
								
					String disType = (String)model.get("DIS_GBN");
					String preDisType = (String)model.get("D_PRE_DIS_GBN");
					
					if ( disType != null ) {
						if ( !disType.equals(preDisType)) {
							Map<String, Object> custInfo = new HashMap<String, Object>();
							custInfo.put("BOARD_TYPE", boardType);
							custInfo.put("BOARD_IDX", boardIdx);
							
							dao.deleteCustInfo(custInfo);
							
							switch (disType){
								case "P":
									custInfoList = dao.selectCustInfoTypeP(model);
									if ( custInfoList != null && !custInfoList.isEmpty() ) {
										for( Map<String, Object> cust : custInfoList) {
											custInfo.put("BOARD_TYPE", boardType);
											custInfo.put("BOARD_IDX", boardIdx);
											custInfo.put("CLIENT_CD", cust.get("ID"));
											
											dao.insertCustInfo(custInfo);
										}
									}
									break;						
								case "C":
									custInfoList = dao.selectCustInfoTypeC(model);
									if ( custInfoList != null && !custInfoList.isEmpty() ) {
										for( Map<String, Object> cust : custInfoList) {
											custInfo.put("BOARD_TYPE", boardType);
											custInfo.put("BOARD_IDX", boardIdx);
											custInfo.put("CLIENT_CD", cust.get("ID"));
											
											dao.insertCustInfo(custInfo);
										}
									}							
									break;
								case "E":
									if ( model.get("CUST_INFO") != null ) {	
										Object custInfoObj = model.get("CUST_INFO");									
										if (custInfoObj instanceof String[]) {
											Map<String, Object>  custInfoTemp = null;
											for (int i=0; i< ((String[])custInfoObj).length; i++) {
												custInfoTemp = new HashMap<String, Object>();
												custInfoTemp.put("BOARD_TYPE", boardType);
												custInfoTemp.put("BOARD_IDX", boardIdx);
												custInfoTemp.put("CLIENT_CD", ((String[])custInfoObj)[i]);
												
												dao.insertCustInfo(custInfoTemp);										
											}									
										} else if (custInfoObj instanceof String) {
											custInfo.put("BOARD_TYPE", boardType);
											custInfo.put("BOARD_IDX", boardIdx);
											custInfo.put("CLIENT_CD", custInfoObj);
											
											dao.insertCustInfo(custInfo);									
										}
									}
									break;
									
								default:							
									break;
							}
							
						} else if ("E".equals(disType)) {
							Map<String, Object> custInfo = new HashMap<String, Object>();
							custInfo.put("BOARD_TYPE", boardType);
							custInfo.put("BOARD_IDX", boardIdx);
							
							dao.deleteCustInfo(custInfo);
							
							if ( model.get("CUST_INFO") != null ) {	
								Object custInfoObj = model.get("CUST_INFO");									
								if (custInfoObj instanceof String[]) {
									Map<String, Object>  custInfoTemp = null;
									for (int i=0; i< ((String[])custInfoObj).length; i++) {
										custInfoTemp = new HashMap<String, Object>();
										custInfoTemp.put("BOARD_TYPE", boardType);
										custInfoTemp.put("BOARD_IDX", boardIdx);
										custInfoTemp.put("CLIENT_CD", ((String[])custInfoObj)[i]);
										
										dao.insertCustInfo(custInfoTemp);										
									}									
								} else if (custInfoObj instanceof String) {
									custInfo.put("BOARD_TYPE", boardType);
									custInfo.put("BOARD_IDX", boardIdx);
									custInfo.put("CLIENT_CD", custInfoObj);
									
									dao.insertCustInfo(custInfo);									
								}
							}							
						}
					}
	
					map.put("MSG", MessageResolver.getMessage("update.success"));
				}

			} else if ("D".equals(flag)) {
								
				if ( model.get("D_ATCH_FILE_NAME") != null ) {	
					Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
					Object attachFileId = model.get("FILE_ID");
										
					if (attachFileInfo instanceof String[]) {
						for (int i=0; i< ((String[])attachFileInfo).length; i++) {							
							String str = ((String[])attachFileInfo)[i];
							String fileId = ((String[])attachFileId)[i];
							
							if ( str != null && StringUtils.isNotEmpty(str) && fileId != null && "".equals(fileId) ) {
								Map<String, Object> modelDt = new HashMap<String, Object>();
								
								modelDt.put("D_ATCH_FILE_NAME", fileId);									
								String mngNo = dao.selectMngno(modelDt);
								
								modelDt.put("MNG_NO", mngNo);
								modelDt.put("FILE_ID", fileId);									
								dao.fileDelete(modelDt);
							}
						}
					}
				}
				model.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
				model.put("BOARD_IDX", model.get("D_BOARD_IDX"));				
				dao.delete(model);
				
				map.put("MSG", MessageResolver.getMessage("delete.success"));
			}
		} catch (Exception e) {
			throw e;
		}
		return map;
	}    
    
    
	/**
	 * Method ID : saveReply 
	 * Method 설명 : 공지사항 답변 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveReply(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			String flag = (String) model.get("FLAG");
			
			// log.info("############ flag :" + flag);

			if ("".equals(flag)) { // 등록
				
				if ( model.get("D_BOARD_STEP") == null || model.get("D_BOARD_LEVEL") == null ) {
					map.put("MSG", MessageResolver.getMessage("insert.error"));
					
				} else {
				
					List<String> fileMngNoList = null;
					if ( model.get("D_ATCH_FILE_NAME") != null ) {	
						Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
						Object attachFileState = model.get("FILE_STATE");
						fileMngNoList = new ArrayList<String>();
											
						if (attachFileInfo instanceof String[]) {
							for (int i=0; i< ((String[])attachFileInfo).length; i++) {							
								String str = ((String[])attachFileInfo)[i];
								String state = ((String[])attachFileState)[i];
								
								// log.info("############ state :" + state);
								
								if ( str != null && StringUtils.isNotEmpty(str)
										&& state != null && "I".equals(state) ) {
									
									Map<String, Object> modelDt = new HashMap<String, Object>();
									String fileRoute = ((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
									String fileSize = ((String[])model.get("FILE_SIZE"))[i];								
									String ext = "." + str.substring((str.lastIndexOf('.') + 1));							
									String fileId = CommonUtil.getLocalDateTime() + i + ext;
									modelDt.put("FILE_ID", fileId);
									modelDt.put("ATTACH_GB", "BOARD");
									modelDt.put("FILE_VALUE", "tmsba230");
									modelDt.put("FILE_PATH", fileRoute);
									modelDt.put("ORG_FILENAME", str);
									modelDt.put("FILE_EXT", ext);
									modelDt.put("FILE_SIZE", fileSize);
									
									String fileMngNo = (String) dao.fileUpload(modelDt);
									
									// log.info("############ fileMngNo :" + fileMngNo);
									fileMngNoList.add(fileMngNo);
								}
							}
						} else if (attachFileInfo instanceof String) {
							Map<String, Object> modelDt = new HashMap<String, Object>();
							String str = (String) model.get("D_ATCH_FILE_NAME");
							String ext = "." + str.substring((str.lastIndexOf('.') + 1));
							
							int fileSeq = 1;
							String fileId = "";
							fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
							modelDt.put("FILE_ID", fileId);
							modelDt.put("ATTACH_GB", "BOARD");
							modelDt.put("FILE_VALUE", "tmsba230");
							modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE"));
							modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME"));
							modelDt.put("FILE_EXT", ext);
							modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));
							
							String fileMngNo = (String) dao.fileUpload(modelDt);			
							// log.info("############ fileMngNo :" + fileMngNo);
							
							fileMngNoList.add(fileMngNo);
						}
					}
					
					
					System.out.println("BOARD_TYPE::::" + model.get("D_BOARD_TYPE"));
					System.out.println("D_BOARD_IDX::::" + model.get("D_BOARD_IDX"));
					System.out.println("D_BOARD_ID::::" + model.get("D_BOARD_ID"));
					System.out.println("STATUS::::" + model.get("status"));
					
					model.put("BOARD_STEP", Integer.parseInt((String)model.get("D_BOARD_STEP")) + 1);
					model.put("BOARD_LEVEL", Integer.parseInt((String)model.get("D_BOARD_LEVEL")) + 1);
					model.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
					model.put("BOARD_REF", model.get("D_BOARD_REF"));					
					model.put("BOARD_TITLE", model.get("D_BOARD_TITLE"));
					model.put("BOARD_CONTENT", model.get("D_CONTENTS"));				
					model.put("ORG_BOARD_ID", model.get("D_ORG_BOARD_ID"));
					model.put("BOARD_NAME", model.get("D_BOARD_NAME"));
					model.put("BOARD_PWD", model.get("D_BOARD_PWD"));
					model.put("POP_YN", "N");	
					model.put("DEL_YN", "N");	
					model.put("READNUM", 0);					
					model.put("BOARD_ID", model.get("SS_USER_NO"));
					model.put("WSTATUS", model.get("status"));
					
					if (fileMngNoList != null) {
						int fileSize = fileMngNoList.size();
						switch (fileSize){
							case 1:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0)); 
								break;						
							case 2:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
								model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
								break;
							case 3:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
								model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
								model.put("ATTACH_FILE_NO_2", fileMngNoList.get(2));
								break;
							default:							
								break;
						}
					}
					
					// log.info("############ model :" + model);
					
					dao.updateReply(model);
					
					dao.insert(model);
	
					map.put("MSG", MessageResolver.getMessage("insert.success"));
				}
				
			} else if ("U".equals(flag)) { // 수정				
				String boardIdx = (String)model.get("D_BOARD_IDX");
				String boardType = (String)model.get("D_BOARD_TYPE");
				
				if ( boardIdx != null && StringUtils.isNotEmpty(boardIdx)
						&& boardType != null && StringUtils.isNotEmpty(boardType)) {
				
					List<String> fileMngNoList = null;
					if ( model.get("D_ATCH_FILE_NAME") != null ) {	
						Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
						Object attachFileId = model.get("FILE_ID");
						fileMngNoList = new ArrayList<String>();
											
						if (attachFileInfo instanceof String[]) {
							for (int i=0; i< ((String[])attachFileInfo).length; i++) {							
								String str = ((String[])attachFileInfo)[i];
								String fileId = ((String[])attachFileId)[i];
								
								// log.info("############ fileId :" + fileId);
								
								if ( str != null && StringUtils.isNotEmpty(str) ) {
									Map<String, Object> modelDt = new HashMap<String, Object>();
									String fileRoute = ((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
									String fileSize = ((String[])model.get("FILE_SIZE"))[i];								
									String ext = "." + str.substring((str.lastIndexOf('.') + 1));
									
									if (fileId == null || "".equals(fileId)) {			
										fileId = CommonUtil.getLocalDateTime() + i + ext;
										
										modelDt.put("FILE_ID", fileId);
										modelDt.put("ATTACH_GB", "BOARD");
										modelDt.put("FILE_VALUE", "tmsba230");
										modelDt.put("FILE_PATH", fileRoute);
										modelDt.put("ORG_FILENAME", str);
										modelDt.put("FILE_EXT", ext);
										modelDt.put("FILE_SIZE", fileSize);
										
										String fileMngNo = (String) dao.fileUpload(modelDt);									
										// log.info("############ fileMngNo :" + fileMngNo);
										
										fileMngNoList.add(fileMngNo);
									} else {
										model.put("D_ATCH_FILE_NAME", fileId);									
										String mngNo = dao.selectMngno(model);
										// log.info("############ mngNo :" + mngNo);
	
										modelDt.put("FILE_PATH", fileRoute);
										modelDt.put("ORG_FILENAME", str);
										modelDt.put("FILE_EXT", ext);
										modelDt.put("FILE_SIZE", fileSize);
										modelDt.put("MNG_NO", mngNo);
										modelDt.put("FILE_ID", fileId);
										
										dao.fileUpdate(modelDt);
										
										fileMngNoList.add(mngNo);
									}
								}
							}
						}
					}			
					
					model.put("BOARD_TYPE", boardType);
					model.put("WBOARD_TYPE", boardType);
					model.put("BOARD_IDX", boardIdx);
					model.put("BOARD_REF", model.get("D_BOARD_REF"));
					model.put("BOARD_STEP", model.get("D_BOARD_STEP"));
					model.put("BOARD_LEVEL", model.get("D_BOARD_LEVEL"));
					model.put("BOARD_TITLE", model.get("D_BOARD_TITLE"));
					model.put("BOARD_CONTENT", model.get("D_CONTENTS"));
					model.put("BOARD_ID", model.get("D_BOARD_ID"));
					model.put("ORG_BOARD_ID", model.get("D_ORG_BOARD_ID"));
					model.put("BOARD_NAME", model.get("D_BOARD_NAME"));
					model.put("BOARD_PWD", model.get("D_BOARD_PWD"));
					model.put("POP_YN", model.get("D_POP_YN"));				
					model.put("DIS_GBN", model.get("D_DIS_GBN"));			
					model.put("WSTATUS", model.get("status"));
					
					if (fileMngNoList != null) {
						int fileSize = fileMngNoList.size();
						switch (fileSize){
							case 1:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0)); 
								break;						
							case 2:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
								model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
								break;
							case 3:
								model.put("ATTACH_FILE_NO", fileMngNoList.get(0));
								model.put("ATTACH_FILE_NO_1", fileMngNoList.get(1));
								model.put("ATTACH_FILE_NO_2", fileMngNoList.get(2));
								break;
							default:							
								break;
						}
					}
	
					dao.update(model);
	
					map.put("MSG", MessageResolver.getMessage("update.success"));
				}

			} else if ("D".equals(flag)) {
								
				if ( model.get("D_ATCH_FILE_NAME") != null ) {	
					Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
					Object attachFileId = model.get("FILE_ID");
										
					if (attachFileInfo instanceof String[]) {
						for (int i=0; i< ((String[])attachFileInfo).length; i++) {							
							String str = ((String[])attachFileInfo)[i];
							String fileId = ((String[])attachFileId)[i];
							
							if ( str != null && StringUtils.isNotEmpty(str) && fileId != null && "".equals(fileId) ) {
								Map<String, Object> modelDt = new HashMap<String, Object>();
								
								modelDt.put("D_ATCH_FILE_NAME", fileId);									
								String mngNo = dao.selectMngno(modelDt);
								
								modelDt.put("MNG_NO", mngNo);
								modelDt.put("FILE_ID", fileId);									
								dao.fileDelete(modelDt);
							}
						}
					}
				}
				model.put("BOARD_TYPE", model.get("D_BOARD_TYPE"));
				model.put("BOARD_IDX", model.get("D_BOARD_IDX"));				
				dao.delete(model);
				
				map.put("MSG", MessageResolver.getMessage("delete.success"));
			}
		} catch (Exception e) {
			throw e;
		}
		return map;
	}   	
 
}
