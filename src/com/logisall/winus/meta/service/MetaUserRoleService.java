package com.logisall.winus.meta.service;

import java.util.Map;

public interface MetaUserRoleService {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> detail(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveDetailList(Map<String, Object> model) throws Exception;
}
