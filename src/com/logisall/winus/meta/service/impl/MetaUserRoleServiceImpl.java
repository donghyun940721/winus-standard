package com.logisall.winus.meta.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.meta.service.MetaUserRoleService;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;


@Service("metaUserRoleService")
public class MetaUserRoleServiceImpl extends AbstractServiceImpl implements MetaUserRoleService {
	
	protected Log loger = LogFactory.getLog(this.getClass());

	@Resource(name = "metaUserRoleDao")
	private MetaUserRoleDao dao;

	/**
	 * 대체 Method ID   : list
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> list(final Map<String, Object> model) throws Exception {
		final Map<String, Object> map = new HashMap<String, Object>();
		loger.info(model);
		if(model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if(model.get("rows") == null) {
			model.put("pageSize", "100");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		
		final String userType = (String)model.get("SS_USER_TYPE");
		if(userType.equals("M")){ // 서비스 관리자인 경우 서비스 신청번호를 화면에서 불러온 것을 사용해야 하므로...
			model.put("SS_SVC_NO", ""); 
		}
		map.put("LIST", dao.list(model));
		return map;
	}

	/**
	 * 대체 Method ID   : detail
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> detail(final Map<String, Object> model) throws Exception {
		final Map<String, Object> map = new HashMap<String, Object>();

		if(model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if(model.get("rows") == null) {
			model.put("pageSize", "1000000");
		} else {
			model.put("pageSize", model.get("rows"));
		}

		map.put("LIST_DETAIL", dao.listDetail(model));
		return map;
	}

	/**
	 * 대체 Method ID   : saveDetailList
	 * 대체 Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
	 * 작성자      : minjae
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveDetailList(final Map<String, Object> model) throws Exception {
		final Map<String, Object> map = new HashMap<String, Object>();
		final String svcNo = (String)model.get("SVC_NO");
			
		Map<String, Object> modelDt = null;
		for(int i=0; i<Integer.parseInt(model.get("selectIds").toString()); i++){			
			modelDt = new HashMap<String, Object>();
			
			modelDt.put("ST_GUBUN"        , model.get("ST_GUBUN"        + i));
			modelDt.put("SVC_NO"          , model.get("SVC_NO"          + i));
			modelDt.put("SVC_TYPE"        , model.get("SVC_TYPE"        + i));
			modelDt.put("MENU_ID"         , model.get("MENU_ID"         + i));
			modelDt.put("WINDOW_ID"       , model.get("WINDOW_ID"       + i));
			modelDt.put("USER_ID"         , model.get("USER_ID"         + i));
			modelDt.put("READ_ROLE"       , model.get("READ_ROLE"       + i));
			modelDt.put("WRITE_ROLE"      , model.get("WRITE_ROLE"      + i));

			if("UPDATE".equals(modelDt.get("ST_GUBUN"))){
				dao.updateDetail(modelDt);
			}else if("INSERT".equals(modelDt.get("ST_GUBUN"))){
				dao.insertDetail(modelDt);
			}else if("DELETE".equals(modelDt.get("ST_GUBUN"))){
				dao.deleteDetail(modelDt);
			}
		}

		if(model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if(model.get("rows") == null) {
			model.put("pageSize", "100");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		
		model.put("S_SVC_NO", svcNo);   // 초기의 화면정보 셋팅
		model.put("S_USER_ID", (String)model.get("USER_ID")); // 초기의 화면정보 셋팅
		map.put("LIST_DETAIL", dao.listDetail(model));
		map.put("MSG", "저장되었습니다.");
		return map;
	}	
}
