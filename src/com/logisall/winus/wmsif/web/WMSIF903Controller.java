package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF903Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF903Controller {
  protected Log log = LogFactory.getLog(this.getClass());
  @Autowired
  private WMSIF903Service service;
  @Autowired
  private WMSIF000Service WMSIF000Service;
  
  /**
   * Method ID    : WMSIF903
   * Method 설명    : 샵링커 인터페이스 화면
   * 작성자          : schan
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping("/WINUS/WMSIF903.action")
  public ModelAndView WMSIF903(Map<String, Object> model) throws Exception {
      return new ModelAndView("winus/wmsif/WMSIF903");
  }
  
  
  /**
   * Method ID    : listE1
   * Method 설명    : 샵링커 주문정보 조회
   * 작성자          : schan
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping("/WMSIF903/listE1.action")
  public ModelAndView listE1(Map<String, Object> model) throws Exception{
      ModelAndView mav = null;
      mav = new ModelAndView("jsonView", service.listE01(model));
      
      return mav;
  }
  
  /**
   * Method ID    : getOutOrder
   * Method 설명    : 샵링커 주문수집
   * 작성자          : schan
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping("/WMSIF903/getOutOrder.action")
  public ModelAndView getOutOrder (Map<String, Object> model, HttpServletRequest request) throws Exception {
      
      ModelAndView mav = new ModelAndView("jsonView");
      Map<String, Object> m = new HashMap<String, Object>();
      Map<String, Object> eaiArguments = new HashMap<String, Object>();
      
      try {                   
          
          JSONObject jsonObject   = new JSONObject();
          JSONObject data1        = new JSONObject();         
          
          // INPUT
          data1.put("LC_ID"       ,   (String)model.get(ConstantIF.SS_SVC_NO));
          data1.put("CUST_ID" ,   model.get("CUST_ID").toString());
          data1.put("USER_NO" ,   (String)model.get(ConstantIF.SS_USER_NO));
          data1.put("WORK_IP" ,   model.get("SS_CLIENT_IP").toString());          
          
          data1.put("shoplinker_id"     ,   model.get("shoplinker_id").toString());
          data1.put("st_date"       ,   model.get("st_date").toString());
          data1.put("ed_date"     ,   model.get("ed_date").toString());
          
          jsonObject.put("request", data1);
          
          
          String dataJson = jsonObject.toString();
                              
          System.out.println("dataJson : " + dataJson);
          
          eaiArguments.put("data"     , dataJson);
          eaiArguments.put("cMethod"  , "POST");
          eaiArguments.put("cUrl"     , "SHOPLINKER/GET/ORDER");
          eaiArguments.put("hostUrl"  , request.getServerName());
          
          m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);             
          
      } catch (Exception e) {
          e.printStackTrace();
      }
      
      mav.addAllObjects(m);
      return mav;
  }
  
  /**
   * Method ID    : syncOutOrder
   * Method 설명    : 출고주문정보 동기화 (ShopLinker > Winus)
   * 작성자          : schan
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping(value="/WMSIF903/syncOutOrder.action", method = RequestMethod.POST)
  public ModelAndView syncOutOrder(@RequestBody HashMap<String, Object> model, HttpServletRequest request) throws Exception {
      ModelAndView mav = new ModelAndView("jsonView");
      Map<String, Object> m = new HashMap<String, Object>();
      try {
          model.put("SS_SVC_NO",request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
          model.put("SS_USER_NO",request.getSession().getAttribute(ConstantIF.SS_USER_NO));
          model.put("SS_CLIENT_IP",request.getSession().getAttribute(ConstantIF.SS_CLIENT_IP));
          
          m = service.syncOutOrder(model);
      } catch (Exception e) {
          e.printStackTrace();
          if (log.isErrorEnabled()) {
              log.error("Fail to save :", e);
          }
          m.put("errCnt", e.getMessage());
          m.put("MSG", MessageResolver.getMessage("save.error"));
      }
      
      mav.addAllObjects(m);
      return mav;
  }
  
  /**
   * Method ID    : listE2
   * Method 설명    : 샵링커 주문정보 조회
   * 작성자          : schan
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping("/WMSIF903/listE2.action")
  public ModelAndView listE2(Map<String, Object> model) throws Exception{
      ModelAndView mav = null;
      mav = new ModelAndView("jsonView", service.listE02(model));
      
      return mav;
  }
  
  /**
   * Method ID    : addInvoiceNo
   * Method 설명    : 샵링커 송장전송
   * 작성자          : schan
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping(value="/WMSIF903/addInvoiceNo.action", method = RequestMethod.POST)
  public ModelAndView addInvoiceNo (@RequestBody HashMap<String, Object> model, HttpServletRequest request) throws Exception {
      
      ModelAndView mav = new ModelAndView("jsonView");
      Map<String, Object> m = new HashMap<String, Object>();
      Map<String, Object> eaiArguments = new HashMap<String, Object>();
      
      try {                   
          
          JSONObject jsonObject   = new JSONObject();
          JSONObject data1        = new JSONObject();         
          
          // INPUT
          data1.put("LC_ID"   ,   request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
          data1.put("CUST_ID" ,   model.get("custId").toString());
          data1.put("USER_NO" ,   request.getSession().getAttribute(ConstantIF.SS_USER_NO));
          data1.put("WORK_IP" ,   request.getSession().getAttribute(ConstantIF.SS_CLIENT_IP));          
          
          data1.put("shoplinker_id" , model.get("shoplinker_id").toString());
          data1.put("invoice"       ,   model.get("list"));
          
          jsonObject.put("request", data1);
          
          
          String dataJson = jsonObject.toString();
                              
          System.out.println("dataJson : " + dataJson);
          
          eaiArguments.put("data"     , dataJson);
          eaiArguments.put("cMethod"  , "POST");
          eaiArguments.put("cUrl"     , "SHOPLINKER/POST/INVOICE");
          eaiArguments.put("hostUrl"  , request.getServerName());
          
          m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);             
          
      } catch (Exception e) {
          e.printStackTrace();
      }
      
      mav.addAllObjects(m);
      return mav;
  }
  
  /**
   * Method ID    : sendShoppingMall
   * Method 설명    : 샵링커 쇼핑몰 송장 전송
   * 작성자          : schan
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping(value="/WMSIF903/sendShoppingMall.action", method = RequestMethod.POST)
  public ModelAndView sendShoppingMall (@RequestBody HashMap<String, Object> model, HttpServletRequest request) throws Exception {
      
      ModelAndView mav = new ModelAndView("jsonView");
      Map<String, Object> m = new HashMap<String, Object>();
      Map<String, Object> eaiArguments = new HashMap<String, Object>();
      
      try {                   
          
          JSONObject jsonObject   = new JSONObject();
          JSONObject data1        = new JSONObject();         
          
          // INPUT
          data1.put("LC_ID"   ,   request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
          data1.put("CUST_ID" ,   model.get("custId").toString());
          data1.put("USER_NO" ,   request.getSession().getAttribute(ConstantIF.SS_USER_NO));
          data1.put("WORK_IP" ,   request.getSession().getAttribute(ConstantIF.SS_CLIENT_IP));          
          
          data1.put("shoplinker_id" , model.get("shoplinker_id").toString());
          data1.put("order"       ,   model.get("list"));
          
          jsonObject.put("request", data1);
          
          
          String dataJson = jsonObject.toString();
                              
          System.out.println("dataJson : " + dataJson);
          
          eaiArguments.put("data"     , dataJson);
          eaiArguments.put("cMethod"  , "POST");
          eaiArguments.put("cUrl"     , "SHOPLINKER/POST/SHOPPINGMALL");
          eaiArguments.put("hostUrl"  , request.getServerName());
          
          m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);             
          
      } catch (Exception e) {
          e.printStackTrace();
      }
      
      mav.addAllObjects(m);
      return mav;
  }
  

}
