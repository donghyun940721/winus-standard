package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF820Service;
import com.logisall.winus.wmsop.service.WMSOP910Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
//@EnableAsync
public class WMSIF820Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF820Service")
	private WMSIF820Service service;
	
	//EAI 통신용
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;

	@Resource(name = "WMSOP910Service")
	private WMSOP910Service WMSOP910Service;
	
	/**
	 * Method ID : WMSIF820
	 * Method 설명 : 자연드림 쿱 인터페이스 관리 페이지 연결
	 * 작성자 : 
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WINUS/WMSIF820.action")
	public ModelAndView wmsif820(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF820");
	}

	/**
	 * Method ID : listE01
	 * Method 설명 : 입고 주문 수신
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE01.action")
	public ModelAndView listE01(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listE01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : listE02
	 * Method 설명 : 입고 주문 송신
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE02.action")
	public ModelAndView listE02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listE02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : listE03
	 * Method 설명 : 출고 B2C 수신 주문내역
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE03.action")
	public ModelAndView listE03(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listE03(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : listE04
	 * Method 설명 : 출고 결과
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE04.action")
	public ModelAndView listE04(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE04(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID 	: listE05
	 * Method 설명 	: 택배실적
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE05.action")
	public ModelAndView listE05(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE05(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID 	: listE05
	 * Method 설명 	: 택배실적 통계
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE05Summary.action")
	public ModelAndView listE05Summary(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listE05Summary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID 	: listE05
	 * Method 설명 	: 통합작업 조회
	 * 작성자 		: dhkim
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE06.action")
	public ModelAndView listE06(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE06(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID 	: listE06Summary
	 * Method 설명 	: 통합작업 통계
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listE06Summary.action")
	public ModelAndView listE06Summary(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listE06Summary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID 	: listBottomE06
	 * Method 설명 	: 통합작업 조회 (하단 작업내역)
	 * 작성자 		: dhkim
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF820/listBottomE06.action")
	public ModelAndView listBottomE06(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listBottomE06(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	//E03
	/**
	 * Method ID	: getOrderInfoIF
	 * Method 설명	: 자연드림 쿱 IF 주문 정보 수집
	 * 작성자			: 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF820/getOrderInfoIF.action")
	public ModelAndView getOrderInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "ITOPLEVEL/POST/OUT_ORDER_INFO";
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("sendFromDate", (String)model.get("sendFromDate"));
			data1.put("sendToDate", (String)model.get("sendToDate"));
			
			jsonObject.put("request", data1);
			
			String dataJson = jsonObject.toString();
	        System.out.println("dataJson : " + dataJson);
		        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);

			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건의 주문을 수집하였습니다.");
			}else{
				m.put("message", resultMsg);
			}
			
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	 * Method ID	: deleteOrderInfoIF
	 * Method 설명	: 자연드림 쿱 IF 주문 수집 내역 삭제
	 * 작성자			: 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF820/deleteOrderInfoIF.action")
	public ModelAndView deleteOrderInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			//String cUrl 	= "ITOPLEVEL/POST/OUT";
			String cUrl 	= "";
			String hostUrl  = request.getServerName();
			
			int cnt = Integer.parseInt((String) model.get("tmpCnt"));
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			JSONArray json_array = new JSONArray();
			for(int i = 0; i<cnt; i++){
				JSONObject dataSub = new JSONObject();
				dataSub.put("ordId",(String) model.get("ORD_ID"+i));
				dataSub.put("ordSeq",(String) model.get("ORD_SEQ"+i));
				json_array.add(dataSub);
			}
			
			data1.put("ordInfo", json_array);
			data1.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("custId", (String)model.get("CUST_ID"));
			
			data1.put("sendDate", (String)model.get("sendDate"));

			jsonObject.put("request", data1);
		        
			String dataJson = jsonObject.toString();
		     
			System.out.println(dataJson);
			
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			
			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건"+MessageResolver.getMessage("save.success"));
			}else{
				m.put("message", resultMsg);
			}

		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	* Method ID 	: syncIF
	* Method 설명 	: 주문내역동기화
	* 작성자 			: 
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF820/syncIF.action")
	public ModelAndView syncIF(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			//System.out.println(model);
			m = service.syncIF(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m = new HashMap<String, Object>();
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());

		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	* Method ID 	: createInvoiceData
	* Method 설명 	: 택배실적 생성
	* 작성자 		: dhkim 
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF820/createInvoiceData.action")
	public ModelAndView createInvoiceData(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
        String concurcSeq;
		
		try {
			String serviceNm	= "택배 실적 생성";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrOrdDt").toString();
        	String ccSeparator3 = StringUtils.isEmpty(model.get("devOrdDegreePopE5").toString()) ? "All" : model.get("devOrdDegreePopE5").toString();
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";  
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	// 전체 차수 대상인 경우, 개별 차수작업 제한
           	if(concurcInfo.size() == 0 && !StringUtils.equals("All", ccSeparator3)){
        		concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, "All", ccSeparator4, ccSeparator5);	
        	}        	
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
			m = service.createInvoiceData(model);
			
			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			
			m.put("errCnt", 1);
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID	: sendInvocieResult
     * Method 설명	: COOP ERP 택배실적 전송
     * 작성자		: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF820/sendInvocieResult.action")
    public ModelAndView CollectOrder (Map<String, Object> model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>(); 
    	List<Object> concurcInfo;
        String concurcSeq;
    	
        try {					
			String serviceNm	= "택배 실적 전송";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrOrdDt").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";  
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);        
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
			JSONObject jsonObject 	= new JSONObject();						
			
			// INPUT
			jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID"	, 	model.get("vrCustId").toString());			
			jsonObject.put("OUT_REQ_DT"	, 	model.get("vrOrdDt").toString().replaceAll("-", ""));
			jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());
			
			String dataJson = jsonObject.toString();							
			
	        eaiArguments.put("data"	    , dataJson);
			eaiArguments.put("cMethod"	, "POST");
//			eaiArguments.put("cUrl"		, "COOP/POST/DLV/RESULT/SEND/TEST");		// 테스트 환경 (EAI)
			eaiArguments.put("cUrl"		, "COOP/POST/DLV/RESULT/SEND");				// 운영환경
			eaiArguments.put("hostUrl"	, request.getServerName());			
			
	        m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
	        
	        m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
            /** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
        } catch (Exception e) {
            e.printStackTrace();
            
            m.put("errCnt", 1);
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage());
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
	/**
     * Method ID	: deleteInvoiceResult
     * Method 설명	: COOP 택배실적 삭제
     * 작성자		: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF820/deleteInvoiceResult.action")
    public ModelAndView DeleteInvoiceResult (Map<String, Object> model, HttpServletRequest request) throws Exception {    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();    	
    	
        try {
        	
        	m = service.deleteInvoiceResult(model);
			
        } catch (Exception e) {
            e.printStackTrace();
            
            m.put("errCnt", 1);
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage());
        }
        
        mav.addAllObjects(m);
        return mav;
    }

    
	/*-
	* Method ID  	: 코텍DAS B2C 전송
	* Method 설명   : 코텍DAS B2C 전송
	* 작성자        : dhkim 
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSIF820/dasIfSendB2C.action")
	public ModelAndView dasIfSendB2C(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
		String concurcSeq; 
		
		try {
        	String serviceNm	= "DAS 차수 전송 (택배)";
        	String ccSeparator1 = model.get("CUST_ID0").toString();
        	String ccSeparator2 = model.get("DEV_ORD_DEGREE0").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";         	
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
			m = WMSOP910Service.dasIfSendB2C(model);
			
			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			
			m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}

    /*
	* Method ID  	: 코텍DAS B2C 전송
	* Method 설명   : 코텍DAS B2C 전송
	* 작성자        : dhkim 
	* @param   model
	* @return  
	*/	
	@RequestMapping("/WMSIF820/dasIfSendNoInvcB2C.action")	
	public ModelAndView dasIfSendNoInvcB2C(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
		String concurcSeq; 
		
		try {
			String serviceNm	= "DAS 차수 전송 (일반)";
        	String ccSeparator1 = model.get("CUST_ID0").toString();
        	String ccSeparator2 = model.get("DEV_ORD_DEGREE0").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = ""; 
        	        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
        	m = WMSOP910Service.dasIfSendNoInvcB2C(model); 		
        	
    		/** 동시성 제어 (작업해제) */
            WMSYS400Service.complete(model, concurcSeq);        
        	
//        	mav.addObject("errCnt", "0");
//        	mav.addObject("DEV_ORD_DEGREE", ccSeparator2);
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	 * Method ID 	: dasIfSend
	 * Method 설명  : 코텍 DAS 전송 (택배/일반)
	 * 작성자       : dhkim
	 * 	
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIF820/dasIfSend.action")	
	public ModelAndView dasIfSend(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
		String concurcSeq; 
		
		try {
			String serviceNm	= "DAS 차수 전송";
        	String ccSeparator1 = model.get("CUST_ID").toString();
        	String ccSeparator2 = model.get("DEV_ORD_DEGREE").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = ""; 
        	        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();        	
        	 		
        	m = service.dasIfSend(model);
        	
    		/** 동시성 제어 (작업해제) */
            WMSYS400Service.complete(model, concurcSeq);       
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save menu :", e);
			}
			m.put("errCnt", "1");
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * DAS 차수 삭재
	 * Method ID  	: dasDelete
	 * Method 설명  : 코텍DAS 차수 삭제
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF820/dasDelete.action")
	public ModelAndView dasDelete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dasDelete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
    /*
	* Method ID  	: 코텍DAS B2C 전송
	* Method 설명   : 코텍DAS B2C 전송
	* 작성자        : dhkim 
	* @param   model
	* @return  
	*/	
//	@RequestMapping("/WMSIF820/dasSend.action")
//	@ResponseBody
//	@Async
//	public Future<String> dasSendTest(Map<String, Object> model, String concurcSeq) throws Exception {
//		System.out.println("비동기 사전 처리 부");
//		
//		Thread.sleep(5000);
//		
//		System.out.println("비동기 처리완료");
//		
//		return new AsyncResult<>("result");		 		
//	}	
	
	
    /*
	* Method ID  	: 코텍DAS B2C 전송
	* Method 설명   : 코텍DAS B2C 전송
	* 작성자        : dhkim 
	* @param   model
	* @return  
	*/	
//	@RequestMapping("/WMSIF820/dasSend.action")
//	@ResponseBody
//	@Async
//	public Callable<String> dasSendTest(Map<String, Object> model, String concurcSeq) throws Exception {
//		System.out.println("비동기 사전 처리 부");
//		
//		Thread.sleep(2000);
//		
//		System.out.println("비동기 처리완료");
//		
//		return new Callable<String>() {
//			@Override
//			public String call() throws Exception {
//				try {
//					Thread.sleep(2000);
//					System.out.println("비동기 처리완료");
//					//DB처리 프로세스 생략 (DB처리 대신 sleep을 걸어 비동기 확인을 위해 호출 시간차이를 둠)
//					return "result";
//				} catch (Exception e) {
//	                // 예외 처리 로직
//	                e.printStackTrace();
//	                return "error";
//	            }
//			}
//		};		
//	}	
	
    /**
	* Method ID 	: createAllOutResult
	* Method 설명 	: 출고 실적 생성
	* 작성자 		:  
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF820/createAllOutResult.action")
	public ModelAndView createAllOutResult (Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>(); 
		List<Object> concurcInfo;
        String concurcSeq;
		
		try {
			String serviceNm	= "출고 실적 생성";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrOrdDt").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);        
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
			JSONObject jsonObject 	= new JSONObject();						
			JSONObject data1 = new JSONObject();
			
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", model.get("vrCustId").toString());
			data1.put("OUT_REQ_DT", model.get("vrOrdDt").toString().replaceAll("-", ""));
			data1.put("WORK_DT", model.get("vrWorkDt").toString().replaceAll("-", ""));
			
			jsonObject.put("request", data1);
			
			String dataJson = jsonObject.toString();
	        System.out.println("dataJson : " + dataJson);
		        
			if(dataJson != null && dataJson !=""){
				// INPUT
				jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
				jsonObject.put("CUST_ID"	, 	model.get("vrCustId").toString());			
				jsonObject.put("OUT_REQ_DT"	, 	model.get("vrOrdDt").toString().replaceAll("-", ""));
				jsonObject.put("WORK_DT", 	model.get("vrWorkDt").toString().replaceAll("-", ""));
				//jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
				//jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());

		        eaiArguments.put("data"	    , dataJson);
				eaiArguments.put("cMethod"	, "POST");
				eaiArguments.put("cUrl"		, "COOP/POST/OUT_ORDER_BUTTON");
				eaiArguments.put("hostUrl"	, request.getServerName());		
	
				m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);
			}

	        //m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
	        
	        m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
            /** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
        } catch (Exception e) {
            e.printStackTrace();
            
            m.put("errCnt", 1);
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage());
        }
        
        mav.addAllObjects(m);
        return mav;
	} 
	
    /**
	* Method ID 	: CreateResultData
	* Method 설명 	: 출고 실적 생성
	* 작성자 		: dhkim
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF820/CreateResultData.action")
	public ModelAndView CreateResultData (Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>(); 
		List<Object> concurcInfo;
        String concurcSeq;
		
		try {
			String serviceNm	= "출고 실적 생성";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrOrdDt").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);        
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
			JSONObject jsonObject 	= new JSONObject();						
			JSONObject data1 = new JSONObject();
			
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", model.get("vrCustId").toString());
			data1.put("OUT_REQ_DT", model.get("vrOrdDt").toString().replaceAll("-", ""));
			data1.put("WORK_DT", model.get("vrWorkDt").toString().replaceAll("-", ""));
			
			jsonObject.put("request", data1);
			
			String dataJson = jsonObject.toString();
	        System.out.println("dataJson : " + dataJson);
		        
			if(dataJson != null && dataJson !=""){
				// INPUT
				jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
				jsonObject.put("CUST_ID"	, 	model.get("vrCustId").toString());			
				jsonObject.put("OUT_REQ_DT"	, 	model.get("vrOrdDt").toString().replaceAll("-", ""));
				jsonObject.put("WORK_DT"	, 	model.get("vrWorkDt").toString().replaceAll("-", ""));

		        eaiArguments.put("data"	    , dataJson);
				eaiArguments.put("cMethod"	, "POST");
				eaiArguments.put("cUrl"		, "COOP/POST/CREATE_OUT_RESULT");
				eaiArguments.put("hostUrl"	, request.getServerName());		
	
				m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);
			}				
	        
	        m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
            /** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
        } catch (Exception e) {
            e.printStackTrace();
            
            m.put("errCnt", 1);
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage());
        }
        
        mav.addAllObjects(m);
        return mav;
	} 
	
	/**
	* Method ID 	: sendAllOutResult
	* Method 설명 	: 출고 실적 전송
	* 작성자 		:  
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF820/sendAllOutResult.action")
	public ModelAndView sendAllOutResult (Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>(); 
		List<Object> concurcInfo;
        String concurcSeq;
		
		try {
			String serviceNm	= "출고 실적 전송";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrOrdDt").toString();
        	String ccSeparator3 = "";
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);        
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();
        	
			JSONObject jsonObject 	= new JSONObject();						
			JSONObject data1 = new JSONObject();
			
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", model.get("vrCustId").toString());
			data1.put("OUT_REQ_DT", model.get("vrOrdDt").toString().replaceAll("-", ""));
			data1.put("WORK_DT", model.get("vrWorkDt").toString().replaceAll("-", ""));
			
			jsonObject.put("request", data1);
			
			String dataJson = jsonObject.toString();
	        System.out.println("dataJson : " + dataJson);
		        
			if(dataJson != null && dataJson !=""){
				// INPUT
				jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
				jsonObject.put("CUST_ID"	, 	model.get("vrCustId").toString());			
				jsonObject.put("OUT_REQ_DT"	, 	model.get("vrOrdDt").toString().replaceAll("-", ""));
				jsonObject.put("WORK_DT", 	model.get("vrWorkDt").toString().replaceAll("-", ""));
				//jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
				//jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());

		        eaiArguments.put("data"	    , dataJson);
				eaiArguments.put("cMethod"	, "POST");
				eaiArguments.put("cUrl"		, "COOP/POST/SEND_OUT_ORDER_BUTTON");
				eaiArguments.put("hostUrl"	, request.getServerName());		
	
				m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);
			}

	        //m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
	        
	        m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
            /** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
        } catch (Exception e) {
            e.printStackTrace();
            
            m.put("errCnt", 1);
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage());
        }
        
        mav.addAllObjects(m);
        return mav;
	} 
}
