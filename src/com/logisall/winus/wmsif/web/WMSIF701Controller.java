package com.logisall.winus.wmsif.web;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsif.service.WMSIF701Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSIF701Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF701Service")
	private WMSIF701Service service;
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	@Resource(name = "WMSOP030Service")
	private WMSOP030Service WMSOP030service;
	/*-
	 * Method ID    : WMSIF701
	 * Method 설명      : 오픈몰 주문관리 (플레이오토)
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSIF701.action")
	public ModelAndView WMSIF701(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF701");
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 오픈몰 주문관리 내역조회 
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	
	@RequestMapping("/WMSIF701/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			System.out.println(model);
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	@RequestMapping("/WMSIF701/list2.action")
	public ModelAndView list2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID     : saveListOrderJava
	 * Method 설명      	 : 오픈몰 주문 등록
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF701/saveListOrderJava.action")
	public ModelAndView saveListOrderJavaVX(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveListOrderJava(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	
	/*-
	 * Method ID : playautoListDelete
	 * Method 설명 : 플레이오토 주문 리스트 삭제
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF701/playautoListDelete.action")
	public ModelAndView playautoListDelete(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.playautoListDelete(model);
			mav.addAllObjects(m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID	: playautoOrdIFInsert
	 * Method 설명	: 
	 * 작성자			: yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF701/playautoOrdIFInsert.action")
	public ModelAndView playautoOrdIFInsert(Map<String, Object> model, HttpServletRequest request ) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "PLAYAUTO/ORDER/COLLECT";
			String hostUrl  = request.getServerName();
			
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("MASTER_SEQ", (String)model.get("masterSeq"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("COMMINPUT", data1);
			
			JSONObject data2 = new JSONObject();
			data2.put("date_type", (String)model.get("dateType")); //플레이오토측 날짜구분
			data2.put("sdate", (String)model.get("ordStDate"));
			data2.put("edate", (String)model.get("ordEdDate"));
				JSONArray jsonData2_array = new JSONArray();
				jsonData2_array.add((String)model.get("orderStatus"));
				data2.put("status", jsonData2_array);
			data2.put("depot", (String)model.get("depotCd"));
			data2.put("api_read_status", (String)model.get("apiReadStatus"));
			jsonObject.put("ORDERINPUT", data2);
	        String dataJson = jsonObject.toString();
			
	        System.out.println("dataJson : " + dataJson);
	        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: getOpenMallApiAuthMaster
	 * Method 설명	: 오픈몰 API AUTH 마스터 리스트
	 * 작성자                 : yhku
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSIF701/getOpenMallApiAuthMaster.action")
	public ModelAndView getOpenMallApiAuthMaster(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getOpenMallApiAuthMaster(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	
	
	
	/*-
	* Method ID    : wmsOrderDeleteJava
	* Method 설명      : 주문삭제
	* 작성자                 : yhku
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSIF701/wmsOrderDeleteJava.action")
	public ModelAndView deleteOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteOrder(model);	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	 * Method ID	: wmsInvcNoSend
	 * Method 설명	: 
	 * 작성자			: yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF701/wmsInvcNoSend.action")
	public ModelAndView wmsInvcNoSend(Map<String, Object> model, HttpServletRequest request ) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "PLAYAUTO/ORDER/INVCNO/SEND";
			String hostUrl  = request.getServerName();
			
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("MASTER_SEQ", (String)model.get("masterSeq"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("COMMINPUT", data1);
			
			JSONObject data2 = new JSONObject();
			data2.put("ord_type", (String)model.get("ordType")); // 입고:01 , 출고:02 
			data2.put("date_type", (String)model.get("dateType")); //wms날짜구분
			data2.put("sdate", (String)model.get("ordStDate"));
			data2.put("edate", (String)model.get("ordEdDate"));
			jsonObject.put("INVCNO_INPUT", data2);
	        String dataJson = jsonObject.toString();
			
	        System.out.println("dataJson : " + dataJson);
	        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	

	/*-
	 * Method ID	: playautoOrdHold
	 * Method 설명	: 
	 * 작성자			: yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF701/playautoOrdHold.action")
	public ModelAndView playautoOrdHold(Map<String, Object> model, HttpServletRequest request ) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "PLAYAUTO/ORDER/HOLD/SEND";
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("MASTER_SEQ", (String)model.get("masterSeq"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("COMMINPUT", data1);
			
			ArrayList<String> bundleList = new ArrayList<String>();
			JSONArray bundle_array = new JSONArray();
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	Boolean checkRun = true;
            	
            	String bundleNo = model.get("BUNDLE_NO"+i).toString();
     			for (String bundleCompare : bundleList) { //중복건 제외 
     		        if (bundleCompare.equals(bundleNo)) {//중복이 없으면, 
     		        	checkRun = false; //중복 있으면 수행X
     		        	break;
     		        }
     		    }
     			if(bundleNo != null && !bundleNo.equals("") && checkRun ){
     				bundleList.add(bundleNo);
     			}
     			
     			JSONObject data = new JSONObject();
				data.put("bundle_no", (String)model.get("BUNDLE_NO"+i));
				data.put("uniq", (String)model.get("UNIQ"+i));
				data.put("seq", (String)model.get("SEQ"+i));
				bundle_array.add(data);
            }
			JSONObject data2 = new JSONObject();
			data2.put("bundle_codes", bundleList);
			data2.put("holdReason", (String)model.get("holdReason"));
			data2.put("BUNDLE_INFO", bundle_array);
			jsonObject.put("ORDHOLD_INPUT", data2);
	        String dataJson = jsonObject.toString();
			
	        System.out.println("dataJson : " + dataJson);
	        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
