package com.logisall.winus.wmsif.web;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.WMSIF301Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF301Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF301Service")
    private WMSIF301Service service;
    
    @Resource(name = "WMSIF000Service")
    private WMSIF000Service WMSIF000Service;
    
    /**
     * Method ID	: wmsif301
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF301.action")
    public ModelAndView wmsif301(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF301");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF301/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF301/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : listE3
     * Method 설명    : 
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF301/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("jsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    /*-
	 * Method ID    : crossDomainHttp
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF301/crossDomainHttp.action")
	public ModelAndView crossDomainHttp(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
		    model.put("hostUrl", request.getServerName());
			m = service.crossDomainHttps(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : crossDomainHttpWs
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF301/crossDomainHttpWs.action")
	public ModelAndView crossDomainHttpWs(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
		    model.put("hostUrl", request.getServerName());
			m = service.crossDomainHttpWs(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
     * Method ID    : syncStock
     * Method 설명      : 
     * 작성자                 : 
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSIF301/syncStock.action")
    public ModelAndView syncStock (Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {                   
            
            JSONObject jsonObject   = new JSONObject();
            JSONObject data1        = new JSONObject();         
            JSONParser jsonParser   = new JSONParser();         
            
            // INPUT
            //data1.put("LC_ID"       ,   (String)model.get(ConstantIF.SS_SVC_NO));
            //data1.put("CUST_ID" ,   model.get("CUST_ID").toString());
                        
            //jsonObject.put("input", data1);
            
            
            //String dataJson = jsonObject.toString();
                                
            //System.out.println("dataJson : " + dataJson);
            
            eaiArguments.put("data"     , "");
            eaiArguments.put("cMethod"  , "POST");
            eaiArguments.put("cUrl"     , "SAEROPNLWMS/ERP/STOCK");
            eaiArguments.put("hostUrl"  , request.getServerName());
            
            m = WMSIF000Service.crossDomainHttpWs5100New(eaiArguments);             
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID    : updateStockMemo
     * Method 설명      : 
     * 작성자                 : 
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSIF301/updateStockMemo.action")
    public ModelAndView updateStockMemo (Map<String, Object> model, HttpServletRequest request) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String,Object> m = new HashMap<String, Object>();
        try{
            
            StringBuffer sb = new StringBuffer();
            String line = null;
            
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                sb.append(line);
            
            JSONObject jsonObj = new JSONObject(sb.toString());            
            JSONArray jsonArr = jsonObj.getJSONArray("jsonData");
            
            model.put("jsonData", jsonArr.toString());

            m = service.updateStockMemo(model);
        }catch(Exception e){
            m.clear();
            m.put("MSG_CODE", "-1");
            m.put("MSG_NAME", e.getMessage());
            e.printStackTrace();
            log.error(e.getStackTrace());
        }
        mav.addAllObjects(m);
        return mav;
    }
    
}
