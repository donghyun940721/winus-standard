package com.logisall.winus.wmsif.web;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.WMSIF203Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF203Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF203Service")
    private WMSIF203Service service;
    
    /**
     * Method ID	: wmsif203
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF203.action")
    public ModelAndView wmsif202(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF203");
    }
	
	/**
     * Method ID	: listExtra
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF203/listExtra.action")
    public ModelAndView listExtra(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listExtra(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : dashControll
     * Method 설명    : 
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF203/dashControll.action")
    public ModelAndView dashControll(Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String,Object> m = new HashMap<String,Object>();
        String sUrl ="http://193.123.252.222:3000/api/unstable/";
        try {
            
            sUrl += ((String)model.get("stat")).equals("start") ? "start-scenario" : "stop-scenario";
            sUrl += "/"+(String)model.get("dashName");
            sUrl += "?access_token="+(String)model.get("access_token");
            
            URL url = null; 
            url = new URL(sUrl);
            
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod("POST");
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Content-Length", "0");
            con.setRequestProperty("Accept", "*/*");
            con.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
            
            
            con.connect();
            
            int resCode = 0;
            resCode = con.getResponseCode();
            System.out.println("resCode: " + resCode);
            
            if(resCode == 200){
                m.put("errCnt", 0);
                m.put("MSG", "처리되었습니다.");
            }else{
                String resultStr = con.getResponseMessage();
                m.put("errCnt", -1);
                m.put("MSG", "I/F 처리오류. 코드:"+resCode+"("+resultStr+")(관리자문의)");
            }
            
            con.disconnect();
            
        } catch (Exception e) {
            e.printStackTrace();
            m.put("errCnt", -1);
            m.put("MSG", "I/F 처리오류. "+e.getMessage());
        }
        mav.addAllObjects(m);
        return mav;
    }
    
}
