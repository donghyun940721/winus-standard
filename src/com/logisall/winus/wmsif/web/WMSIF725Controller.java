package com.logisall.winus.wmsif.web;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF725Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF725Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF725Service")
    private WMSIF725Service service;
    
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;	
    
    /**
     * Method ID	: WMSIF725
     * Method 설명	: EBIZWAY 주문 관리 페이지 접속
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF725.action")
    public ModelAndView WMSIF725(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF725");
    }
	
    
    /**
	 * Method ID 	: list
	 * Method 설명 	:  EBIZWAY 주문 관리 입출고 내역조회 
	 * 작성자 			: SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF725/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			//System.out.println(model);
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
  
    /**
	 * Method ID 	: list2
	 * Method 설명 	:  EBIZWAY 주문 관리 입출고 내역조회 
	 * 작성자 			: SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF725/list2.action")
	public ModelAndView list2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			//System.out.println(model);
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
    /**
	 * Method ID 	: getCustOrdDegree
	 * Method 설명 	:  주문차수검색
	 * 작성자 			: SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF725/getCustOrdDegree.action")
	public ModelAndView getCustOrdDegree(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustOrdDegree(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : crossDomainHttp
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF725/crossDomainHttp.action")
	public ModelAndView crossDomainHttp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			
			//DATA 설정 
			String cMethod	= "POST";
			String cUrl 	= String.valueOf(model.get("cUrl"));
			String custId 	= String.valueOf(model.get("custId"));
			
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", custId);
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			data1.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			
			jsonObject.put("ORD_REG_REQ", data1);
			
			String dataJson = jsonObject.toString();
			//System.out.println("dataJson ===>"+dataJson);
			
			//호출 
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				
				m = service.crossDomainHttps(model);
			}
			
			//결과 
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			JSONObject jsonData2 =  new JSONObject(jsonData.get("ORD_REG_RES").toString());
			String msg = jsonData2.get("MSG").toString();
			m.put("MSG", msg);
			//m.put("MSG", MessageResolver.getMessage("save.success"));
			//System.out.println(Arrays.asList(m));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	 
/*
    *//**
	 * Method ID	: bflowOrdIFInsert
	 * Method 설명	: 비플로우 주문수집
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 *//*
	@RequestMapping("/WMSIF715/bflowOrdIFInsert.action")
	public ModelAndView bflowOrdIFInsert(Map<String, Object> model, HttpServletRequest request ) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			
			String cMethod	= "POST";
			String cUrl 	= "BFLOW/ORDER/COLLECT";
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("OPENMALL_CD", "BFLOW");
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			
			jsonObject.put("COMM_INPUT", data1);
			
			JSONObject data2 = new JSONObject();
			
		//	System.out.println((String)model.get("ordStDate"));
		//	System.out.println((String)model.get("ordEdDate"));
		//	System.out.println((String)model.get("type"));
			
			data2.put("start_day", (String)model.get("ordStDate"));
			data2.put("end_day", (String)model.get("ordEdDate"));
			data2.put("type", (String)model.get("type"));
			
			jsonObject.put("API_CALL_INPUT", data2);
	        
			String dataJson = jsonObject.toString();
			
	//        System.out.println("dataJson : " + dataJson);
	        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
			
			String result_cnt = "0";
			
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			result_cnt = jsonData.get("total_2").toString();
			String api_msg = jsonData.get("API_RTN_MSG").toString();
			
			String err_ord_list = jsonData.get("ERR_LIST_STRING").toString();
			
			m.put("totalCnt", result_cnt);
			m.put("api_msg", api_msg);
			m.put("err_ord_list", err_ord_list);
			
			m.put("message", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}*/
	
	
}

