package com.logisall.winus.wmsif.web;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF726Service;
import com.logisall.winus.wmsif.service.WMSIF725Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF726Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF726Service")
    private WMSIF726Service service;
    
    @Resource(name = "WMSIF725Service")
    private WMSIF725Service service765;
	
    @Resource(name = "WMSYS400Service")
    private WMSYS400Service WMSYS400Service;
    
    /**
     * Method ID	: WMSIF726
     * Method 설명	: EBIZWAY 인터페이스관리 접속
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF726.action")
    public ModelAndView WMSIF726(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF726");
    }
    
    /**
   	 * Method ID 	: listE01
   	 * Method 설명 	:  EBIZWAY 호출 메인 조회
   	 * 작성자 			: SUMMER HYUN
   	 * @param model
   	 * @return
   	 */
   	@RequestMapping("/WMSIF726/list01.action")
   	public ModelAndView list01(Map<String, Object> model) {
   		ModelAndView mav = null;
   		try {
   			//System.out.println(model);
   			mav = new ModelAndView("jqGridJsonView", service.list01(model));
   		} catch (Exception e) {
   			if (log.isErrorEnabled()) {
   				log.error("Fail to get list :", e);
   			}
   		}
   		return mav;
   	}
	
    /**
	 * Method ID 	: listE02
	 * Method 설명 	:  EBIZWAY 호출이력 조회
	 * 작성자 			: SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF726/listE02.action")
	public ModelAndView list02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			//System.out.println(model);
			mav = new ModelAndView("jqGridJsonView", service.list02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	  /**
		 * Method ID 	: listE03
		 * Method 설명 	:  EBIZWAY 상품마스터
		 * 작성자 			: SUMMER HYUN
		 * @param model
		 * @return
		 */
		@RequestMapping("/WMSIF726/listE03.action")
		public ModelAndView listE03(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				//System.out.println(model);
				mav = new ModelAndView("jqGridJsonView", service.list03(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}

	 /**
		* Method ID 	: listE04
		* Method 설명 	:  EBIZWAY 입고처마스터
		* 작성자 			: SUMMER HYUN
		* @param model
		* @return
	*/
		@RequestMapping("/WMSIF726/listE04.action")
		public ModelAndView listE04(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				//System.out.println(model);
				mav = new ModelAndView("jqGridJsonView", service.list04(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}		
		
		
		 /**
			* Method ID 	: listE05
			* Method 설명 	:  EBIZWAY 매장마스터
			* 작성자 			: SUMMER HYUN
			* @param model
			* @return
		*/
	@RequestMapping("/WMSIF726/listE05.action")
	public ModelAndView listE05(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			//System.out.println(model);
			mav = new ModelAndView("jqGridJsonView", service.list05(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/**
	* Method ID 	: listE06
	* Method 설명 	:  EBIZWAY 창고이동정보
	* 작성자 			: SUMMER HYUN
	* @param model
	* @return
		*/
	@RequestMapping("/WMSIF726/listE06.action")
	public ModelAndView listE06(Map<String, Object> model) {
	ModelAndView mav = null;
	try {
		//System.out.println(model);
		mav = new ModelAndView("jqGridJsonView", service.list06(model));
	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to get list :", e);
		}
	}
	return mav;
	}
	
	/**
	* Method ID 	: listE07
	* Method 설명 	:  EBIZWAY 전송재고
	* 작성자 			: SUMMER HYUN
	* @param model
	* @return
		*/
	@RequestMapping("/WMSIF726/listE07.action")
	public ModelAndView listE07(Map<String, Object> model) {
	ModelAndView mav = null;
	try {
		//System.out.println(model);
		mav = new ModelAndView("jqGridJsonView", service.list07(model));
	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to get list :", e);
		}
	}
	return mav;
	}
    
	/*-
	 * Method ID    : crossDomainHttp
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF726/crossDomainHttp.action")
	public ModelAndView crossDomainHttp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		
		List<Object> concurcInfo;
    	String concurcSeq;    	
    	
		String serviceNm	= "/WMSIF726/crossDomainHttp.action";
    	String ccSeparator1 = String.valueOf(model.get("custId"));		// 화주
    	String ccSeparator2 = String.valueOf(model.get("cUrl"));
    	String ccSeparator3 = "";
    	String ccSeparator4 = "";
    	String ccSeparator5 = ""; 
		
		try {
			
			/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5); 
        	
        	//작업 중인 내역 존재 -> 완료 후 작업 가능 
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception]\n";        		
        		        		
        		excecptionMsg += "호출자 : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "호출내역 : " + ((Map<String, Object>)concurcInfo.get(0)).get("SEPARATOR4");
        		excecptionMsg += "작업 진행 중 입니다. 동시 작업이 제한 됩니다.";
        		
        		throw new Exception(excecptionMsg);
        	}
        	
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString(); 
			
			//DATA 설정 
			String cMethod	= "POST";
			String cUrl 	= String.valueOf(model.get("cUrl"));
			String custId 	= String.valueOf(model.get("custId"));
			
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			data1.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			
			jsonObject.put("MASTER_REQ", data1);
			
			JSONObject data2 = new JSONObject();
			
			data2.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data2.put("CUST_ID", custId);
			data2.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			data2.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			
			jsonObject.put("ORD_REG_REQ", data2);
			
			String dataJson = jsonObject.toString();
			//System.out.println("dataJson ===>"+dataJson);
			
			//호출 
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				
				System.out.println(Arrays.asList(model));;
				m = service765.crossDomainHttps(model);
			}
				
			
			//결과 
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			JSONObject jsonData2 =  new JSONObject(jsonData.get("ORD_REG_RES").toString());
			String msg = jsonData2.get("MSG").toString();
			m.put("MSG", msg);
			//m.put("MSG", MessageResolver.getMessage("save.success"));
			//System.out.println(Arrays.asList(m));
			

			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);	
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}	
	
}

