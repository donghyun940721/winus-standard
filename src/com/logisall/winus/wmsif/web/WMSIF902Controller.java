package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF902Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF902Controller {
  protected Log log = LogFactory.getLog(this.getClass());
  @Autowired
  private WMSIF902Service service;
  
  /**
   * Method ID    : WMSIF902
   * Method 설명    : 롯데웰푸드 인터페이스 관리 화면 진입
   * 작성자          : kih
   * @param   model
   * @return  
   * @throws Exception 
   */
  @RequestMapping("/WINUS/WMSIF902.action")
  public ModelAndView WMSIF902(Map<String, Object> model) throws Exception {
      return new ModelAndView("winus/wmsif/wellfood/WMSIF902");
  }
  
  
  @RequestMapping("/WMSIF902/listE01.action")
  public ModelAndView selectE01List(Map<String, Object> model) throws Exception{
      ModelAndView mav = null;
      mav = new ModelAndView("jqGridJsonView", service.listE01(model));
      
      return mav;
  }
  
  @RequestMapping("/WMSIF902/selectCommonCodeList.action")
  public ResponseEntity<Map<String, Object>> selectWmsOrderList(Map<String, Object> model) {
	
      Map<String, Object> resultMap = service.selectCommonCodeList(model);
	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
  }
  
  @RequestMapping("/WMSIF902/selectZsCustList.action")
  public ResponseEntity<Map<String, Object>> selectZsCustList(Map<String, Object> model) {
	
      Map<String, Object> resultMap = service.selectZsCustList(model);
	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
  }
  
  @RequestMapping("/WMSIF902/selectWarehouseList.action")
  public ResponseEntity<Map<String, Object>> selectWarehouseList(Map<String, Object> model) {
      Map<String, Object> resultMap = service.selectWarehouseList(model);
	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
  }

  @RequestMapping("/WMSIF902/selectProducts.action")
  public ResponseEntity<Map<String, Object>> selectProducts(Map<String, Object> model) {
	
      Map<String, Object> resultMap = service.selectProducts(model);
	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
  }
  
  @RequestMapping("/WMSIF902/historyListE06.action")
  public ModelAndView selectInboundList(Map<String, Object> model){
      ModelAndView mav = null;
      mav = new ModelAndView("jqGridJsonView", service.selectInboundList(model));
      
      return mav;
  }
  
  @RequestMapping("/WMSIF902/selectOutOrderAsnList.action")
  public ResponseEntity<Map<String, Object>> selectOutOrderAsnList(Map<String, Object> model) throws Exception{
      return new ResponseEntity<Map<String,Object>>(service.selectOutOrderAsnList(model), HttpStatus.OK);
  }
  @RequestMapping("/WMSIF902/historyListE08.action")
  public ModelAndView selectInResultList(Map<String, Object> model){
      ModelAndView mav = new ModelAndView("jsonView");
      Map<String, Object> m = new HashMap<String, Object>();
      try {
          m = service.selectInResultList(model);
      } catch (Exception e) {
          if (log.isErrorEnabled()) {
              log.error("Fail to save approve :", e);
          }
          m.put("errCnt",1);
          m.put("MSG", e.getMessage());
      }
      mav.addAllObjects(m);
      return mav;
  }
  
  
  @RequestMapping("/WMSIF902/selectDayProductDivList.action")
  public ResponseEntity<Map<String, Object>> selectDayProductDivList(Map<String, Object> model) {
	
    Map<String, Object> resultMap = service.selectDayProductDivList(model);
	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
  }
  
  @RequestMapping("/WMSIF902/saveCommonCodes.action")
  @ResponseBody
  public ModelAndView saveCommonCodes(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveCommonCodesByQuery(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
        m.put("MSG", MessageResolver.getMessage("save.error"));
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/syncCommonCode.action")
  @ResponseBody
  public ModelAndView syncCommonCode(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveCommonCodesByCheckedList(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/saveZsCusts.action")
  @ResponseBody
  public ModelAndView saveZsCusts(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveZsCusts(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
        m.put("MSG", MessageResolver.getMessage("save.error"));
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/syncZsCusts.action")
  @ResponseBody
  public ModelAndView syncZsCusts(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveZsCusts(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/saveWarehouses.action")
  @ResponseBody
  public ModelAndView saveWarehouses(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveWarehouses(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/saveProducts.action")
  @ResponseBody
  public ModelAndView saveProducts(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveProducts(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/syncProducts.action")
  @ResponseBody
  public ModelAndView syncProducts(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveCheckedProducts(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  
  @RequestMapping("/WMSIF902/saveInboundOrders.action")
  @ResponseBody
  public ModelAndView saveInboundOrders(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveInOrders(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/saveOutOrders.action")
  @ResponseBody
  public ModelAndView saveOutOrders(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveOutOrders(model, reqMap);
    } catch (BizException | RuntimeException be) {
		m.put("errCnt", "1");
		m.put("MSG", be.getMessage());
	} catch (Exception e) {
		log.error(e);
		m.put("errCnt", "1");
		m.put("MSG", MessageResolver.getMessage("save.error"));
	}
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/makeInResult.action")
  @ResponseBody
  public ModelAndView makeInResult(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.makeInResult(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
        m.put("errCnt", "1");
        m.put("MSG", e.getMessage());
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/selectWinusItemList.action")
  @ResponseBody
  public ModelAndView selectWinusItemList(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.selectWinusItemList(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
        m.put("errCnt", "1");
        m.put("MSG", e.getMessage());
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  @RequestMapping("/WMSIF902/sendAPI.action")
  @ResponseBody
  public ModelAndView sendAPI(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.sendAPI(reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
        m.put("errCnt", "1");
        m.put("MSG", e.getMessage());
    }
    mav.addAllObjects(m);
    return mav;
  }
  
  
  
  @RequestMapping("/WMSIF902/saveItemRegionDiv.action")
  @ResponseBody
  public ModelAndView saveItemRegionDiv(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.saveItemRegionDiv(model, reqMap);
    } catch (Exception e) {
		log.error(e);
		m.put("errCnt", "1");
		m.put("MSG", MessageResolver.getMessage("save.error"));
	}
    mav.addAllObjects(m);
    return mav;
  }
}
