package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsif.service.WMSIF613Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF613Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF613Service")
    private WMSIF613Service service;
    
    /**
     * Method ID	: WMSIF613
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF613.action")
    public ModelAndView WMSIF613(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF613");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 자사몰 주문조회
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF613/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listE2
     * Method 설명	: 자사몰 출고실적 주문조회
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF613/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * Method ID     : saveJasaOrderListVX
     * Method 설명         : 자사몰 주문 등록
     * 작성자                 : sing09
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSIF613/saveJasaOrderListVX.action")
    public ModelAndView saveListOrderJavaVX(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();

        try {
            m = service.saveJasaOrderListVX(model);
            mav.addAllObjects(m);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to get picking info :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("MSG_ORA", e.getMessage());
            m.put("errCnt", "1");
            mav.addAllObjects(m);
        }
        return mav;
    }
    

    /**
     * Method ID : jasamallDelete
     * Method 설명 : 자사몰 주문,실적 삭제
     * 작성자 : sing09
     * @param model
     * @return
     */
    @RequestMapping("/WMSIF613/jasamallDelete.action")
    public ModelAndView jasaListDelete(Map<String, Object> model) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = null;
        try {
            m = service.jasamallDelete(model);
            mav.addAllObjects(m);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m = new HashMap<String, Object>();
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("MSG_ORA", e.getMessage());
            m.put("errCnt", "1");
            mav.addAllObjects(m);
        }
        return mav;
    }
}
