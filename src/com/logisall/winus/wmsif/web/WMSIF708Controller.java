package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF708Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF708Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF708Service")
    private WMSIF708Service service;
    
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;		
    
    /**
     * Method ID	: wmsif301
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF708.action")
    public ModelAndView WMSIF708(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF708");
    }
	
    /**
     * Method ID	: listE1
     * Method 설명	: 상품정보 조회
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF708/listE1.action")
    public ModelAndView listE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {        			
            mav = new ModelAndView("jqGridJsonView", service.listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }    
    
    
    /**
     * Method ID	: listE2
     * Method 설명	: 매입처 조회
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF708/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {        			
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }    
    
    
    /**
     * Method ID	: listE3
     * Method 설명	: 매출처 조회
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF708/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {        			
            mav = new ModelAndView("jqGridJsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }    
    
    
    /**
     * Method ID	: getToken
     * Method 설명	: 스카이젯 인증서 생성
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF708/getToken.action")
    public ModelAndView getToken (Map<String, Object> model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {					
            
			JSONObject jsonObject 	= new JSONObject();
			JSONObject data1 		= new JSONObject();			
			JSONParser jsonParser 	= new JSONParser();			
			
			// INPUT
			jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID"	, 	model.get("CUST_ID").toString());
			jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());
	            
			// docRequest
			data1.put("LoginID"       ,    model.get("LoginID").toString());
			data1.put("LoginPW"       ,    model.get("LoginPW").toString());			
						
			jsonObject.put("Request", data1);
			
			
			String dataJson = jsonObject.toString();
								
	        System.out.println("dataJson : " + dataJson);
			
	        eaiArguments.put("data"	    , dataJson);
			eaiArguments.put("cMethod"	, "POST");
			eaiArguments.put("cUrl"		, "ISKYZ/GET/TOKEN");
			eaiArguments.put("hostUrl"	, request.getServerName());
			
	        m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }


    /**
     * Method ID	: CollectItem
     * Method 설명	: 스카이젯 상품정보 수집
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF708/CollectItem.action")
    public ModelAndView CollectItem (Map<String, Object> model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {					
            
			JSONObject jsonObject 	= new JSONObject();
			JSONObject data1 		= new JSONObject();			
			JSONParser jsonParser 	= new JSONParser();			
			
			// INPUT
			jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID"	, 	model.get("CUST_ID").toString());
			jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());			
			
			jsonObject.put("Z_TOKEN"	,	model.get("Z_TOKEN").toString());
			jsonObject.put("Z_FROM"		,	model.get("Z_FROM").toString());
			jsonObject.put("Z_TO"		,	model.get("Z_TO").toString());
	            
			// docRequest
			data1.put("ProductCode"     ,    model.get("ProductCode").toString());
			data1.put("ProductName"     ,    model.get("ProductName").toString());
			data1.put("MakerName"     	,    model.get("MakerName").toString());
			data1.put("KDcode"   		,    model.get("KDcode").toString());			
			data1.put("StockCode"       ,    model.get("StockCode").toString().replaceAll("-", ""));
			
			if(!model.containsKey("vrAllDayYn"))
			{
				data1.put("WriteDate"        ,    model.get("WriteDate").toString().replaceAll("-", ""));
				data1.put("EditDate"         ,    model.get("EditDate").toString().replaceAll("-", ""));
			}	
						
			jsonObject.put("docRequest", data1);
			
			
			String dataJson = jsonObject.toString();
								
	        System.out.println("dataJson : " + dataJson);
			
	        eaiArguments.put("data"	    , dataJson);
			eaiArguments.put("cMethod"	, "POST");
			eaiArguments.put("cUrl"		, "ISKYZ/GET/ITEM");
			eaiArguments.put("hostUrl"	, request.getServerName());
			
	        m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }

    
    /**
     * Method ID	: syncItem
     * Method 설명	: 상품정보 동기화 (이지어드민 > Winus)
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF708/syncItem.action")
    public ModelAndView syncItem(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.syncItem(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    
    /**
     * Method ID	: CollectStore
     * Method 설명	: 스카이젯 거래처 수집
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF708/collectStore.action")
    public ModelAndView CollectStore (Map<String, Object> model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {					
            
			JSONObject jsonObject 	= new JSONObject();
			JSONObject data1 		= new JSONObject();			
			JSONParser jsonParser 	= new JSONParser();			
			
			// INPUT
			jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID"	, 	model.get("CUST_ID").toString());
			jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());
			
			jsonObject.put("CATEGORY"	, 	model.get("CATEGORY").toString());
			
			jsonObject.put("Z_TOKEN"	,	model.get("Z_TOKEN").toString());
			jsonObject.put("Z_FROM"		,	model.get("Z_FROM").toString());
			jsonObject.put("Z_TO"		,	model.get("Z_TO").toString());
	            
			// docRequest
			data1.put("VenderCode"       ,    model.get("VenderCode").toString());
			data1.put("VenderName"       ,    model.get("VenderName").toString());
			data1.put("RegistrationNo"   ,    model.get("RegistrationNo").toString());
			if(!model.containsKey("vrAllDayYn"))
			{
				data1.put("WriteDate"        ,    model.get("WriteDate").toString().replaceAll("-", ""));
				data1.put("EditDate"         ,    model.get("EditDate").toString().replaceAll("-", ""));
			}								
						
			jsonObject.put("docRequest", data1);
			
			
			String dataJson = jsonObject.toString();
								
	        System.out.println("dataJson : " + dataJson);
			
	        eaiArguments.put("data"	    , dataJson);
			eaiArguments.put("cMethod"	, "POST");
			eaiArguments.put("cUrl"		, "ISKYZ/GET/STORE");
			eaiArguments.put("hostUrl"	, request.getServerName());
			
	        m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }


}

