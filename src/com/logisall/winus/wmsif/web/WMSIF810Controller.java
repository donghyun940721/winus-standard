package com.logisall.winus.wmsif.web;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF810Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF810Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF810Service")
	private WMSIF810Service service;
	
	//EAI 통신용
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	/**
	 * Method ID : WMSIF810
	 * Method 설명 : 대화물류 인터페이스 관리 페이지 연결
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WINUS/WMSIF810.action")
	public ModelAndView wmsif402(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF810");
	}
	
	/**
	 * Method ID : listE01
	 * Method 설명 : 거래처 조회 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF810/listE01.action")
	public ModelAndView listE01(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : listE02
	 * Method 설명 : 상품
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF810/listE02.action")
	public ModelAndView listE02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : listE03
	 * Method 설명 : 입고
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF810/listE03.action")
	public ModelAndView listE03(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE03(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : listE04
	 * Method 설명 : 재고
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF810/listE04.action")
	public ModelAndView listE04(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE04(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/**
	 * Method ID : listE05
	 * Method 설명 : 주문
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF810/listE05.action")
	public ModelAndView listE05(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listE05(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/**
	 * Method ID : listE06
	 * Method 설명 : 출고
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF810/listE06.action")
	public ModelAndView listE06(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE06(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	//-----------E01--------------------//
	/**
	 * Method ID	: sendTrustCustInfoIF
	 * Method 설명	: 대화물류 탑벨 IF 거래처 정보 전송
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF810/sendTrustCustInfoIF.action")
	public ModelAndView sendTrustCustInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "ITOPLEVEL/POST/TRANS_CUST";
			String hostUrl  = request.getServerName();
			
			int cnt = Integer.parseInt((String) model.get("tmpCnt"));
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("trustCustId", (String)model.get("CUST_ID"));
			
			JSONArray json_array = new JSONArray();
				for(int i = 0; i<cnt; i++){
					json_array.add((String)model.get("TRUST_CUST_ID"+i));
				}
			data1.put("custId", json_array);
			data1.put("sendDate", (String)model.get("sendDate"));

			jsonObject.put("request", data1);
		        
			String dataJson = jsonObject.toString();
		        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건"+MessageResolver.getMessage("save.success"));
			}else{
				m.put("message", resultMsg);
			}
			
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	//E02
	/**
	 * Method ID	: sendProductInfoIF
	 * Method 설명	: 대화물류 탑벨 IF 상품 정보 전송
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF810/sendProductInfoIF.action")
	public ModelAndView sendProductInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "ITOPLEVEL/POST/ITEM";
			String hostUrl  = request.getServerName();
			
			int cnt = Integer.parseInt((String) model.get("tmpCnt"));
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("custId", (String)model.get("CUST_ID"));
			
			JSONArray json_array = new JSONArray();
				for(int i = 0; i<cnt; i++){
					json_array.add((String)model.get("RITEM_ID"+i));
				}
			data1.put("rItemId", json_array);
			data1.put("sendDate", (String)model.get("sendDate"));

			jsonObject.put("request", data1);
		        
			String dataJson = jsonObject.toString();
		    
			System.out.println(dataJson);
			
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건"+MessageResolver.getMessage("save.success"));
			}else{
				m.put("message", resultMsg);
			}
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	
	//E03
	/**
	 * Method ID	: sendIncomingInfoIF
	 * Method 설명	: 대화물류 탑벨 IF 입고 정보 전송
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF810/sendIncomingInfoIF.action")
	public ModelAndView sendIncomingInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "ITOPLEVEL/POST/IN";
			String hostUrl  = request.getServerName();
			
			int cnt = Integer.parseInt((String) model.get("tmpCnt"));
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			JSONArray json_array = new JSONArray();
			
			for(int i = 0; i<cnt; i++){
				JSONObject dataSub = new JSONObject();
				dataSub.put("ordId",(String) model.get("ORD_ID"+i));
				dataSub.put("ordSeq",(String) model.get("ORD_SEQ"+i));
				json_array.add(dataSub);
			}
			
			data1.put("ordInfo", json_array);
			data1.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("custId", (String)model.get("CUST_ID"));
			
			data1.put("sendDate", (String)model.get("sendDate"));
			data1.put("inDate", (String)model.get("sendDate"));

			jsonObject.put("request", data1);
		        
			String dataJson = jsonObject.toString();
		   
			System.out.println(dataJson);
			
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			
			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건"+MessageResolver.getMessage("save.success"));
			}else{
				m.put("message", resultMsg);
			}

		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	//E04
	/**
	 * Method ID	: sendStockInfoIF
	 * Method 설명	: 대화물류 탑벨 IF 재고 정보 전송
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF810/sendStockInfoIF.action")
	public ModelAndView sendStockInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "ITOPLEVEL/POST/STOCK";
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("custId", (String)model.get("CUST_ID"));
			
			jsonObject.put("request", data1);
		        
			String dataJson = jsonObject.toString();
			
			System.out.println(dataJson);
			
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", MessageResolver.getMessage("save.success"));
			}else{
				m.put("message", resultMsg);
			}
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	//E05
	/**
	 * Method ID	: getOrderInfoIF
	 * Method 설명	: 대화물류 탑벨 IF 주문 정보 수집
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF810/getOrderInfoIF.action")
	public ModelAndView getOrderInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "ITOPLEVEL/POST/OUT_ORDER_INFO";
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("sendFromDate", (String)model.get("sendFromDate"));
			data1.put("sendToDate", (String)model.get("sendToDate"));
			
			jsonObject.put("request", data1);
			
			String dataJson = jsonObject.toString();
	        System.out.println("dataJson : " + dataJson);
		        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);

			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건의 주문을 수집하였습니다.");
			}else{
				m.put("message", resultMsg);
			}
			
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	 * Method ID	: deleteOrderInfoIF
	 * Method 설명	: 대화물류 탑벨 IF 주문 수집 내역 삭제
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF810/deleteOrderInfoIF.action")
	public ModelAndView deleteOrderInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			//String cUrl 	= "ITOPLEVEL/POST/OUT";
			String cUrl 	= "";
			String hostUrl  = request.getServerName();
			
			int cnt = Integer.parseInt((String) model.get("tmpCnt"));
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			JSONArray json_array = new JSONArray();
			for(int i = 0; i<cnt; i++){
				JSONObject dataSub = new JSONObject();
				dataSub.put("ordId",(String) model.get("ORD_ID"+i));
				dataSub.put("ordSeq",(String) model.get("ORD_SEQ"+i));
				json_array.add(dataSub);
			}
			
			data1.put("ordInfo", json_array);
			data1.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("custId", (String)model.get("CUST_ID"));
			
			data1.put("sendDate", (String)model.get("sendDate"));

			jsonObject.put("request", data1);
		        
			String dataJson = jsonObject.toString();
		     
			System.out.println(dataJson);
			
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			
			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건"+MessageResolver.getMessage("save.success"));
			}else{
				m.put("message", resultMsg);
			}

		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	* Method ID 	: syncIF
	* Method 설명 	: 주문내역동기화
	* 작성자 			: SUMMER HYUN
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF810/syncIF.action")
	public ModelAndView syncIF(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			//System.out.println(model);
			m = service.syncIF(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m = new HashMap<String, Object>();
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());

		}
		mav.addAllObjects(m);
		return mav;
	}
	
	//E06
	/**
	 * Method ID	: sendOutgoingInfoIF
	 * Method 설명	: 대화물류 탑벨 IF 출고주문 전송
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF810/sendOutgoingInfoIF.action")
	public ModelAndView sendOutgoingInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "ITOPLEVEL/POST/OUT";
			String hostUrl  = request.getServerName();
			
			int cnt = Integer.parseInt((String) model.get("tmpCnt"));
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			JSONArray json_array = new JSONArray();
			for(int i = 0; i<cnt; i++){
				JSONObject dataSub = new JSONObject();
				dataSub.put("ordId",(String) model.get("ORD_ID"+i));
				//dataSub.put("ordSeq",(String) model.get("ORD_SEQ"+i));
				json_array.add(dataSub);
			}
			
			data1.put("ordInfo", json_array);
			data1.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("custId", (String)model.get("CUST_ID"));
			
			data1.put("sendDate", (String)model.get("sendDate"));
			data1.put("outDate", (String)model.get("sendDate"));

			jsonObject.put("request", data1);
		        
			String dataJson = jsonObject.toString();
		     
			System.out.println(dataJson);
			
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
					
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
				
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			
			String resultCode = jsonData.get("resultCode").toString();
			String resultMsg  = jsonData.get("resultMsg").toString();
			String Cnt  = jsonData.get("Cnt").toString();
			
			if(resultCode == "0" || resultCode.equals("0")){
				m.put("message", Cnt+"건"+MessageResolver.getMessage("save.success"));
			}else{
				m.put("message", resultMsg);
			}

		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
}
