package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.WMSIF203Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF206Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID	: WMSIF206
     * Method 설명	: DENSO dashboard
     * 작성자		: KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF206.action")
    public ModelAndView wmsif202(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF206");
    }
}
