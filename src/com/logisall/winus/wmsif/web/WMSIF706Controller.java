package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.WMSIF706Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF706Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF706Service")
    private WMSIF706Service service;
    
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
    
    /**
     * Method ID	: wmsif301
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF706.action")
    public ModelAndView wmsif706(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF706");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF706/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    /**
     * Method ID	: itemCollect
     * Method 설명	: 이지어드민 상품정보 수집
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF706/itemCollect.action")
    public ModelAndView ItemCollect(Map<String, Object> model , HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
        
        try {
        	String cMethod	= "POST";
			String cUrl 	= "EZADMIN/ITEM/COLLECT/ALL";
			String hostUrl  = request.getServerName();
			
//            mav = new ModelAndView("jqGridJsonView", service.itemCollect(model));
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			jsonObject.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID", model.get("CUST_ID").toString());
			jsonObject.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP", model.get("SS_CLIENT_IP").toString());
	            
			data1.put("partner_key"     ,    model.get("partner_key").toString());
			data1.put("domain_key"      ,    model.get("domain_key").toString());
			data1.put("action"          ,    model.get("action").toString());
			data1.put("date_type"       ,    model.get("date_type").toString());
			data1.put("start_date"      ,    model.get("start_date").toString());
			data1.put("end_date"        ,    model.get("end_date").toString());
			data1.put("barcode"         ,    model.get("barcode").toString());
			data1.put("new_link_id"     ,    model.get("new_link_id").toString());
			data1.put("limit"           ,    model.get("limit").toString());
			data1.put("page"            ,    model.get("page").toString());
			
			jsonObject.put("getReqItem", data1);
						
			String dataJson = jsonObject.toString();
			
					
	        System.out.println("dataJson : " + dataJson);
			
			model.put("data"	    , dataJson);
			model.put("cMethod"		, cMethod);
			model.put("cUrl"		, cUrl);
			model.put("hostUrl"		, hostUrl);
			
	        m = WMSIF000Service.crossDomainHttpWs5200(model);				
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }

    
    /**
     * Method ID	: syncItem
     * Method 설명	: 상품정보 동기화 (이지어드민 > Winus)
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF706/syncItem.action")
    public ModelAndView syncItem(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.syncItem(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    
    /**
     * Method ID	: getOpenMallInfo
     * Method 설명	: 오픈몰 권한정보 획득
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF706/getOpenMallInfo.action")
    public ModelAndView getOpenMallInfo(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.getOpenMallInfo(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }

    /**
    * Method ID	: itemInfo
    * Method 설명	: 
    * 작성자			: dhkim
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSIF706/itemInfo.action")
   public ModelAndView itemInfo(Map<String, Object> model) throws Exception {
	   	ModelAndView mav = null;
	    
	    try {
	        mav = new ModelAndView("jqGridJsonView", service.itemInfo(model));
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return mav;
    }   
    
}
