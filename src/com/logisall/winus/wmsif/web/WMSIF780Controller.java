package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF780Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF780Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF780Service")
    private WMSIF780Service service;
    
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
    
    /**
     * Method ID	: wmsif780
     * Method 설명	: GoodMD ERP 인터페이스 화면
     * 작성자			: schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF780.action")
    public ModelAndView WMSIF780(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF780");
    }
	
    /**
     * Method ID	: listE1
     * Method 설명	: 상품정보 조회
     * 작성자			: schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/listE1.action")
    public ModelAndView listE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {        			
            mav = new ModelAndView("jsonView", service.listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }    
    /**
     * Method ID    : listE2
     * Method 설명    : 입고정보 조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {                   
            mav = new ModelAndView("jsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }  
    /**
     * Method ID    : listE3
     * Method 설명    : 출고정보 조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {                   
            mav = new ModelAndView("jsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }  
    
    /**
     * Method ID    : listE4
     * Method 설명    : 입고결과 조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/listE4.action")
    public ModelAndView listE4(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {                   
            mav = new ModelAndView("jsonView", service.listE4(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }  
    
    /**
     * Method ID    : listE5
     * Method 설명    : 출고결과 조회
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/listE5.action")
    public ModelAndView listE5(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {                   
            mav = new ModelAndView("jsonView", service.listE5(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }  
    

    /**
     * Method ID	: CollectItem
     * Method 설명	: GoodMd 상품정보 수집
     * 작성자			: schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/CollectItem.action")
    public ModelAndView CollectItem (Map<String, Object> model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {					
            
			JSONObject jsonObject 	= new JSONObject();
			JSONObject data1 		= new JSONObject();			
			JSONParser jsonParser 	= new JSONParser();			
			
			// INPUT
			data1.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID"	, 	model.get("CUST_ID").toString());
			data1.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
			data1.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());			
			
			data1.put("OPENMALL_CD"	,	model.get("OPENMALL_CD").toString());
			data1.put("DEV_YN"  ,   "N");
			data1.put("MOD_FROM_DATE"		,	model.get("MOD_FROM_DATE").toString());
			data1.put("MOD_TO_DATE"		,	model.get("MOD_TO_DATE").toString());
	        
			jsonObject.put("input", data1);
			
			
			String dataJson = jsonObject.toString();
								
	        System.out.println("dataJson : " + dataJson);
			
	        eaiArguments.put("data"	    , dataJson);
			eaiArguments.put("cMethod"	, "POST");
			eaiArguments.put("cUrl"		, "GOODMD/GET/ITEM");
			eaiArguments.put("hostUrl"	, request.getServerName());
			
	        m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : CollectInOrder
     * Method 설명    : GoodMd 입고주문정보 수집
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/CollectInOrder.action")
    public ModelAndView CollectInOrder (Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {                   
            
            JSONObject jsonObject   = new JSONObject();
            JSONObject data1        = new JSONObject();         
            JSONParser jsonParser   = new JSONParser();         
            
            // INPUT
            data1.put("LC_ID"       ,   (String)model.get(ConstantIF.SS_SVC_NO));
            data1.put("CUST_ID" ,   model.get("CUST_ID").toString());
            data1.put("USER_NO" ,   (String)model.get(ConstantIF.SS_USER_NO));
            data1.put("WORK_IP" ,   model.get("SS_CLIENT_IP").toString());          
            
            data1.put("OPENMALL_CD" ,   model.get("OPENMALL_CD").toString());
            data1.put("DEV_YN"  ,   "N");
            data1.put("MOD_FROM_DATE"       ,   model.get("MOD_FROM_DATE").toString());
            data1.put("MOD_TO_DATE"     ,   model.get("MOD_TO_DATE").toString());
            
            jsonObject.put("input", data1);
            
            
            String dataJson = jsonObject.toString();
                                
            System.out.println("dataJson : " + dataJson);
            
            eaiArguments.put("data"     , dataJson);
            eaiArguments.put("cMethod"  , "POST");
            eaiArguments.put("cUrl"     , "GOODMD/GET/INORDER/INFO");
            eaiArguments.put("hostUrl"  , request.getServerName());
            
            m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);                
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : CollectOutOrder
     * Method 설명    : GoodMd 출고주문정보 수집
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/CollectOutOrder.action")
    public ModelAndView CollectOutOrder (Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {                   
            
            JSONObject jsonObject   = new JSONObject();
            JSONObject data1        = new JSONObject();         
            JSONParser jsonParser   = new JSONParser();         
            
            // INPUT
            data1.put("LC_ID"       ,   (String)model.get(ConstantIF.SS_SVC_NO));
            data1.put("CUST_ID" ,   model.get("CUST_ID").toString());
            data1.put("USER_NO" ,   (String)model.get(ConstantIF.SS_USER_NO));
            data1.put("WORK_IP" ,   model.get("SS_CLIENT_IP").toString());          
            
            data1.put("OPENMALL_CD" ,   model.get("OPENMALL_CD").toString());
            data1.put("DEV_YN"  ,   "N");
            data1.put("MOD_FROM_DATE"       ,   model.get("MOD_FROM_DATE").toString());
            data1.put("MOD_TO_DATE"     ,   model.get("MOD_TO_DATE").toString());
            
            jsonObject.put("input", data1);
            
            
            String dataJson = jsonObject.toString();
                                
            System.out.println("dataJson : " + dataJson);
            
            eaiArguments.put("data"     , dataJson);
            eaiArguments.put("cMethod"  , "POST");
            eaiArguments.put("cUrl"     , "GOODMD/GET/OUTORDER/INFO");
            eaiArguments.put("hostUrl"  , request.getServerName());
            
            m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);                
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : syncItem
     * Method 설명    : 상품정보 동기화 (Good-MD > Winus)
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/syncItem.action")
    public ModelAndView syncItem(Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");;
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.syncItem(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("errCnt", e.getMessage());
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : syncInOrder
     * Method 설명    : 입고주문정보 동기화 (Good-MD > Winus)
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/syncInOrder.action")
    public ModelAndView syncInOrder(Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");;
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.syncInOrder(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("errCnt", e.getMessage());
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }

    /**
     * Method ID    : syncOutOrder
     * Method 설명    : 출고주문정보 동기화 (Good-MD > Winus)
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/syncOutOrder.action")
    public ModelAndView syncOutOrder(Map<String, Object> model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");;
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.syncOutOrder(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("errCnt", e.getMessage());
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : sendInResult
     * Method 설명    : GoodMd 입고결과 송신
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/sendInResult.action")
    public ModelAndView sendInResult (Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {
            
            JSONObject jsonObject   = new JSONObject();
            JSONObject data1        = new JSONObject();         
            JSONParser jsonParser   = new JSONParser();  
            

            // INPUT
            data1.put("LC_ID"       ,   (String)model.get(ConstantIF.SS_SVC_NO));
            data1.put("CUST_ID" ,   model.get("CUST_ID").toString());
            data1.put("USER_NO" ,   (String)model.get(ConstantIF.SS_USER_NO));
            data1.put("WORK_IP" ,   model.get("SS_CLIENT_IP").toString());          
            
            data1.put("OPENMALL_CD" ,   model.get("OPENMALL_CD").toString());
            data1.put("DEV_YN"  ,   "N");
            
            int selectIds = Integer.parseInt(model.get("ROW_COUNT").toString());
            
            JSONArray ordInfo = new JSONArray();
            
            for(int i = 0; i < selectIds ; i++){
                JSONObject data = new JSONObject();
                data.put("ORD_ID", model.get("I_ORD_ID"+i).toString());
                data.put("ORD_SEQ", model.get("I_ORD_SEQ"+i).toString());
                ordInfo.add(data);
            }
            data1.put("ORD_INFO", ordInfo);
            
            
            jsonObject.put("input", data1);
            
            
            String dataJson = jsonObject.toString();
                                
            System.out.println("dataJson : " + dataJson);
            
            eaiArguments.put("data"     , dataJson);
            eaiArguments.put("cMethod"  , "POST");
            eaiArguments.put("cUrl"     , "GOODMD/POST/INORDER/RESULT");
            eaiArguments.put("hostUrl"  , request.getServerName());
            
            m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);                
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    /**
     * Method ID    : sendOutResult
     * Method 설명    : GoodMd 출고결과 송신
     * 작성자          : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF780/sendOutResult.action")
    public ModelAndView sendOutResult (Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {                   
            
            JSONObject jsonObject   = new JSONObject();
            JSONObject data1        = new JSONObject();         
            JSONParser jsonParser   = new JSONParser();         
            
            // INPUT
            data1.put("LC_ID"       ,   (String)model.get(ConstantIF.SS_SVC_NO));
            data1.put("CUST_ID" ,   model.get("CUST_ID").toString());
            data1.put("USER_NO" ,   (String)model.get(ConstantIF.SS_USER_NO));
            data1.put("WORK_IP" ,   model.get("SS_CLIENT_IP").toString());          
            
            data1.put("OPENMALL_CD" ,   model.get("OPENMALL_CD").toString());
            data1.put("DEV_YN"  ,   "N");
            
            int selectIds = Integer.parseInt(model.get("ROW_COUNT").toString());
            
            JSONArray ordInfo = new JSONArray();
            
            for(int i = 0; i < selectIds ; i++){
                JSONObject data = new JSONObject();
                data.put("ORD_ID", model.get("I_ORD_ID"+i).toString());
                data.put("ORD_SEQ", model.get("I_ORD_SEQ"+i).toString());
                ordInfo.add(data);
            }
            data1.put("ORD_INFO", ordInfo);
            
            jsonObject.put("input", data1);
            
            String dataJson = jsonObject.toString();
                                
            System.out.println("dataJson : " + dataJson);
            
            eaiArguments.put("data"     , dataJson);
            eaiArguments.put("cMethod"  , "POST");
            eaiArguments.put("cUrl"     , "GOODMD/POST/OUTORDER/RESULT");
            eaiArguments.put("hostUrl"  , request.getServerName());
            
            m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);                
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }

}

