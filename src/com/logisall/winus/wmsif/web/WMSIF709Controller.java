package com.logisall.winus.wmsif.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF709Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF709Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF709Service")
    private WMSIF709Service service;
    
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;		
    
    /**
     * Method ID	: wmsif301
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF709.action")
    public ModelAndView WMSIF709(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF709");
    }         
    
    /**
     * Method ID	: listE1
     * Method 설명	: 스카이젯 주문관리 입고 조회 (OM)
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF709/listE1.action")
    public ModelAndView listE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {        			
            mav = new ModelAndView("jqGridJsonView", service.listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }    
         
    
    /**
     * Method ID	: saveE1
     * Method 설명	: 매입주문 저장 (스카이젯 → Winus)
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF709/saveE1.action")
    public ModelAndView saveE1(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.saveE1(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }    
    
    
    /**
     * Method ID	: saveE1_OM
     * Method 설명	: 매입주문 저장 (스카이젯 → Winus)
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF709/saveE1_OM.action")
    public ModelAndView saveE1_OM(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.saveE1_OM(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }    
    
        
    /**
     * Method ID	: saveE2
     * Method 설명	: 매출주문 저장 (스카이젯 → Winus)
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF709/saveE2.action")
    public ModelAndView saveE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.saveE2(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
        }
        
        mav.addAllObjects(m);
        return mav;
    }    
    
    
    /**
     * Method ID	: deleteE1
     * Method 설명	: 인터페이스 주문삭제
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF709/deleteE1.action")
    public ModelAndView deleteE1(Map<String, Object> model , HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");    	
    	Map<String, Object> m = new HashMap<String, Object>();  	
	    	
        try {  	        
        	m = service.deleteE1(model);
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
        	m.put("errCnt", 1);      
			m.put("MSG", e.getMessage());            
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    
    
    /**
     * Method ID	: CollectOrder
     * Method 설명	: 스카이젯 주문 수집
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF709/collectOrder.action")
    public ModelAndView CollectOrder (Map<String, Object> model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>();
        String dateType = new String();
    	
        try {					
            
			JSONObject jsonObject 	= new JSONObject();
			JSONObject data1 		= new JSONObject();			
			JSONParser jsonParser 	= new JSONParser();			
			
			// INPUT
			jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID"	, 	model.get("CUST_ID").toString());
			jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());
			
			jsonObject.put("ORDER_TYPE"	, 	model.get("ORDER_TYPE").toString());
			
			jsonObject.put("Z_TOKEN"	,	model.get("Z_TOKEN").toString());
			jsonObject.put("Z_FROM"		,	model.get("Z_FROM").toString());
			jsonObject.put("Z_TO"		,	model.get("Z_TO").toString());

			// docRequest
			data1.put("TRANS_DATE1"     ,    model.get("TRANS_DATE1").toString().replaceAll("-", ""));
			data1.put("TRANS_DATE2"     ,    model.get("TRANS_DATE2").toString().replaceAll("-", ""));
			data1.put("VenderCode"   	,    model.get("VenderCode").toString());
			
			if(model.containsKey("iSWORK_DAY")){
				dateType = model.get("iSWORK_DAY").toString();
				
				if(!dateType.equals("R")){
					dateType = "";
				}
			}
				
			data1.put("iSWORK_DAY"   	,    dateType);
			jsonObject.put("docRequest", data1);			
			
			String dataJson = jsonObject.toString();
								
//	        System.out.println("dataJson : " + dataJson);
			
	        eaiArguments.put("data"	    , dataJson);
			eaiArguments.put("cMethod"	, "POST");
			eaiArguments.put("cUrl"		, "ISKYZ/GET/ORDER");
			eaiArguments.put("hostUrl"	, request.getServerName());
			
	        m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
}

