package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.wmsif.service.WMSIF902ResultService;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF902ResultController {
  protected Log log = LogFactory.getLog(this.getClass());
  @Autowired
  private WMSIF902ResultService service;
  
  @RequestMapping("/WMSIF902/selectOutOrderResultList.action")
  @ResponseBody
  public ResponseEntity<Map<String, Object>> selectE01List(Map<String, Object> model) throws Exception{
      return new ResponseEntity<Map<String,Object>>(service.selectOutOrderResultList(model), HttpStatus.OK);
  }
  
  
  
  @RequestMapping("WMSIF902/makeOutResult")
  @ResponseBody
  public ModelAndView makeInResult(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.makeOutResult(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
        m.put("errCnt", "1");
        m.put("MSG", e.getMessage());
    }
    mav.addAllObjects(m);
    return mav;
  }
}
