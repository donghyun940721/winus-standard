package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF715Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF715Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF715Service")
    private WMSIF715Service service;
    
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;	
    
    /**
     * Method ID	: WMSIF715
     * Method 설명	: 비플로우 주문 관리 페이지 접속
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF715.action")
    public ModelAndView WMSIF715(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF715");
    }
	
    
    /**
	 * Method ID 	: list
	 * Method 설명 	: 비플로우 주문 내역조회 
	 * 작성자 			: SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF715/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			//System.out.println(model);
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
  
	 /**
		 * Method ID 	: list2
		 * Method 설명 	: 비플로우 인터페이스 이력 조회 
		 * 작성자 			: SUMMER HYUN
		 * @param model
		 * @return
	*/
	@RequestMapping("/WMSIF715/list2.action")
	public ModelAndView list2(Map<String, Object> model) {
			ModelAndView mav = null;
			try {
				//System.out.println(model);
				mav = new ModelAndView("jqGridJsonView", service.list2(model));
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get list :", e);
				}
			}
			return mav;
		}

    /**
	 * Method ID	: bflowOrdIFInsert
	 * Method 설명	: 비플로우 주문수집
	 * 작성자			: SUMMER H
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF715/bflowOrdIFInsert.action")
	public ModelAndView bflowOrdIFInsert(Map<String, Object> model, HttpServletRequest request ) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			
			String cMethod	= "POST";
			String cUrl 	= "BFLOW/ORDER/COLLECT";
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("OPENMALL_CD", "BFLOW");
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			
			jsonObject.put("COMM_INPUT", data1);
			
			JSONObject data2 = new JSONObject();
			
		//	System.out.println((String)model.get("ordStDate"));
		//	System.out.println((String)model.get("ordEdDate"));
		//	System.out.println((String)model.get("type"));
			
			data2.put("start_day", (String)model.get("ordStDate"));
			data2.put("end_day", (String)model.get("ordEdDate"));
			data2.put("type", (String)model.get("type"));
			
			jsonObject.put("API_CALL_INPUT", data2);
	        
			String dataJson = jsonObject.toString();
			
	//        System.out.println("dataJson : " + dataJson);
	        
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
			
			String result_cnt = "0";
			
			String jsonString = m.get("RESULT").toString();
			JSONObject jsonData = new JSONObject(jsonString);
			
			result_cnt = jsonData.get("total_2").toString();
			String api_msg = jsonData.get("API_RTN_MSG").toString();
			
			String err_ord_list = jsonData.get("ERR_LIST_STRING").toString();
			
			m.put("totalCnt", result_cnt);
			m.put("api_msg", api_msg);
			m.put("err_ord_list", err_ord_list);
			
			m.put("message", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	* Method ID 	: wmsOrdRegister
	* Method 설명 	: WMS 주문내역 등록
	* 작성자 			: SUMMER HYUN
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF715/wmsOrdRegister.action")
	public ModelAndView wmsOrdRegister(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
		//	System.out.println("CONT wmsOrdRegister");
		//	System.out.println(model);

			m = service.wmsOrdRegister(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	* Method ID 	: invcSendToBflow
	* Method 설명 	: 비플로우에 송장송신
	* 작성자 			: SUMMER HYUN
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF715/invcSendToBflow.action")
	public ModelAndView invcSendToBflow(Map<String, Object> model, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= "BFLOW/ORDER/INVCNO/SEND";
			String hostUrl  = request.getServerName();
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("CUST_ID"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			data1.put("OPENMALL_CD", "BFLOW");
			jsonObject.put("COMM_INPUT", data1);
			
			JSONObject data2 = new JSONObject();
			
			int tmpCnt = Integer.parseInt((String) model.get("tmpCnt"));
			int total_successCnt = 0;
	        int total_failCnt = 0;
	        int invc_duple = 0;
	          
	        //토큰 정보 수집 후 전달 
	        String token = null;
	        
	        token = service.getBflowToken(model);
	        if(token != null){
				//한 건씩 EAI 호출
				for(int i = 0; i<tmpCnt; i++){
					
					data2.put("BF_ORI_ORD_CD",(String)model.get("BF_ORI_ORD_CD"+i)); //ori_ord_id
					data2.put("BF_ORD_NO",(String)model.get("BF_ORD_NO"+i)); 
					data2.put("PRD_ORD_NO",(String)model.get("PRD_ORD_NO"+i)); 
					data2.put("DELIVERY_COMPANY",(String)model.get("DELIVERY_COMPANY"+i)); 
					data2.put("INVC_NO",(String)model.get("INVC_NO"+i)); 
					data2.put("TOKEN",token); 
					data2.put("ORD_ID",(String)model.get("ORD_ID"+i)); //ord_id
					data2.put("ORD_SEQ",(String)model.get("ORD_SEQ"+i)); //ord_seq
					
					jsonObject.put("INVCNO_INPUT", data2);
			        String dataJson = jsonObject.toString();
					
			     //   System.out.println("송장송신 dataJson : " + dataJson);
			        
					model.put("data"	    , dataJson);
					model.put("cMethod"		, cMethod);
					model.put("cUrl"		, cUrl);
					model.put("hostUrl"		, hostUrl);
						
					result = WMSIF000Service.crossDomainHttpWs5200New(model);
				//	System.out.println(result.get("RESULT"));
					
					String jsonString = result.get("RESULT").toString();
					JSONObject jsonData = new JSONObject(jsonString);
					
					String result_cd = jsonData.get("RTN_CD").toString();
					
					if(result_cd.equals("0")|| result_cd == "0"){
						total_successCnt += 1; 
					}else if(result_cd.equals("D")|| result_cd == "D"){
						invc_duple += 1;
					}else{
						total_failCnt += 1;
					}
				}
				
		         m.put("MSG", "");
	        }else{
	        	 m.put("MSG", "접속권한(토큰)이 올바르지 않습니다.");
	        }
	       
			//송장 발신 건 
			 m.put("SUCCESS_CNT", total_successCnt);
	         m.put("FAIL_CNT", total_failCnt);
	         m.put("DUPLE", invc_duple);
	         
	         //송장 발신 내역 저장 
	         Map<String, Object> resultTotal = new HashMap<String, Object>();
	         resultTotal.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	         resultTotal.put("CUST_ID", (String)model.get("CUST_ID"));
	         resultTotal.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	         resultTotal.put("RESULT", "송장 전송 성공:"+total_successCnt+"("+invc_duple+")"+"  /  송장 전송 실패:"+total_failCnt);
	         service.InvcSendCntSave(resultTotal);
	         
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	* Method ID 	: wmsOrderDelete
	* Method 설명 	: WMS 주문내역 삭제 
	* 작성자 			: SUMMER HYUN
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF715/wmsOrderDelete.action")
	public ModelAndView wmsOrderDelete(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			m = service.wmsOrderDelete(model);
		} catch (Exception e) {
		//	System.out.println("controller catch");
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	* Method ID 	: orderDelete
	* Method 설명 	: IF 주문수집 내역 삭제 
	* 작성자 			: SUMMER HYUN
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF715/IFOrderDelete.action")
	public ModelAndView IFOrderDelete(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		m = new HashMap<String, Object>();
		try {
			m = service.IFOrderDelete(model);
			
			m.put("errCnt", m.get("errCnt"));
			m.put("MSG", m.get("MSG"));
			
		} catch (Exception e) {
		//	System.out.println("controller catch");
			if (log.isErrorEnabled()) {
				log.error("Fail to delete:", e);
			}
		}
		mav.addAllObjects(m);
		return mav;
	}

}

