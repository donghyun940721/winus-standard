package com.logisall.winus.wmsif.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsif.service.WMSIF603Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF603Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF603Service")
	private WMSIF603Service service;

	@RequestMapping("/WINUS/WMSIF603.action")
	public ModelAndView wmsif603(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF603");
	}
	
    @RequestMapping("/WMSIF603/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF603/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
}
