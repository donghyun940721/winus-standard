package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.wmsif.service.WMSIF911Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF911Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	@Autowired
	private WMSIF911Service service;
  
	/**
	 * Method ID : WMSIF911 Method 설명 : WEVERSE 인터페이스 관리 화면 진입 작성자 : kih
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSIF911.action")
	public ModelAndView WMSIF911(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/weverse/WMSIF911");
	}
	
	@RequestMapping("/WMSIF911/selectIfOrders.action")
	public ResponseEntity<Map<String, Object>> selectIfOrders(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> resultMap = service.selectIfOrders(model);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
  
  
  @RequestMapping("/WMSIF911/sendWeverseOrderToWmsom010.action")
  @ResponseBody
  public ModelAndView sendAPI(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response){
    ModelAndView mav = new ModelAndView("jsonView");
    Map<String, Object> m = new HashMap<String, Object>();
    try {
        m = service.sendWeverseOrderToWmsom010(model, reqMap);
    } catch (Exception e) {
        if (log.isErrorEnabled()) {
            log.error("Fail to save approve :", e);
        }
        m.put("errCnt", "1");
        m.put("MSG", e.getMessage());
    }
    mav.addAllObjects(m);
    return mav;
  }
  
}
