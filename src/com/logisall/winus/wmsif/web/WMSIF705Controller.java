package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.WMSIF705Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF705Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF705Service")
    private WMSIF705Service service;
    
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
    
    /**
     * Method ID	: wmsif301
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF705.action")
    public ModelAndView wmsif301(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF705");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF705/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF705/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
	/*-
	 * Method ID    : crossDomainHttpWs
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF705/playautoApiDepotsSelect.action")
	public ModelAndView playautoApiDepotsSelect(Map<String, Object> model, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= (String)model.get("URL");
			
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("MASTER_SEQ", (String)model.get("masterSeq"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("COMMINPUT", data1);
			
	        String dataJson = jsonObject.toString();
			
	        //System.out.println("dataJson : " + dataJson);
	        
	        if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	

	/*-
	 * Method ID    : crossDomainHttpWs SKU insert
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF705/playautoSKUItemInsert.action")
	public ModelAndView playautoSKUItemInsert(Map<String, Object> model, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= (String)model.get("URL");
			
			System.out.println("______________cUrl  : " + cUrl );
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("MASTER_SEQ", (String)model.get("masterSeq"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("COMMINPUT", data1);
			
			JSONObject data2 = new JSONObject();
			data2.put("fromDt", (String)model.get("fromDt"));
			data2.put("toDt", (String)model.get("toDt"));
			data2.put("depots", (String)model.get("depots"));
			jsonObject.put("SKUITEM_INST_INPUT", data2);
	        String dataJson = jsonObject.toString();
			
	        System.out.println("dataJson : " + dataJson);
	        
	        if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : crossDomainHttpWs SKU update
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF705/playautoSKUItemUpdate.action")
	public ModelAndView playautoSKUItemUpdate(Map<String, Object> model, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= (String)model.get("URL");
			
			System.out.println("______________cUrl  : " + cUrl );
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("MASTER_SEQ", (String)model.get("masterSeq"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("COMMINPUT", data1);
			
			JSONObject data2 = new JSONObject();
			data2.put("fromDt", (String)model.get("fromDt"));
			data2.put("toDt", (String)model.get("toDt"));
			data2.put("depots", (String)model.get("depots"));
			jsonObject.put("SKUITEM_UPDT_INPUT", data2);
	        String dataJson = jsonObject.toString();
			
	        System.out.println("dataJson : " + dataJson);
	        
	        if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : crossDomainHttpWs SKU update
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF705/playautoStockUpdate.action")
	public ModelAndView playautoStockUpdate(Map<String, Object> model, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String cMethod	= "POST";
			String cUrl 	= (String)model.get("URL");
			
			System.out.println("______________cUrl  : " + cUrl );
			String hostUrl  = request.getServerName();
			
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			data1.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			data1.put("CUST_ID", (String)model.get("custId"));
			data1.put("MASTER_SEQ", (String)model.get("masterSeq"));
			data1.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("COMMINPUT", data1);
			
			JSONObject data2 = new JSONObject();
			data2.put("ordType", (String)model.get("ordType"));
			data2.put("fromDt", (String)model.get("fromDt"));
			data2.put("toDt", (String)model.get("toDt"));
			data2.put("depots", (String)model.get("depots"));
			jsonObject.put("STOCK_UPD_INPUT", data2);
	        String dataJson = jsonObject.toString();
			
	        System.out.println("dataJson : " + dataJson);
	        
	        if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = WMSIF000Service.crossDomainHttpWs5200New(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	 /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF705/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
}
