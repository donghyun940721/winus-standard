package com.logisall.winus.wmsif.web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibatis.common.jdbc.exception.NestedSQLException;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.wmsif.service.impl.WMSIF904Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

/**
 * CAFE24 인터페이스
 */
@Controller
public class WMSIF904Controller {
	
	private final Log log = LogFactory.getLog(this.getClass());
	
	private final WMSIF904Service service;
	private final WMSIF000Service WMSIF000Service;
	
	@Autowired
	public WMSIF904Controller(WMSIF904Service service, WMSIF000Service WMSIF000Service){
		this.service= service;
		this.WMSIF000Service = WMSIF000Service;
	}

	@RequestMapping("/WINUS/WMSIF904.action")
	public ModelAndView WMSIF904(Map<String, Object> model) {
		Map<String, Object> codeMap = new HashMap<String, Object>();
		codeMap.put("MALL_LIST", service.selectMallList(model));
		return new ModelAndView("winus/wmsif/cafe24/WMSIF904", codeMap);
	}

	@RequestMapping("/WMSIF904/getOrderList.action")
	public ModelAndView selectOrderList(Map<String, Object> model) {
		return new ModelAndView("jqGridJsonView", service.selectOrderList(model));
	}

	@RequestMapping("/WMSIF904/getOrderShipmentList.action")
	public ModelAndView selectOrderShipmentList(Map<String, Object> model) {
		return new ModelAndView("jqGridJsonView", service.selectOrderShipmentList(model));
	}

	@RequestMapping("/WMSIF904/saveOutboundOrders.action")
	@ResponseBody
	public ModelAndView saveOutboundOrders(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutboundOrders(model, reqMap);
		} catch (SQLException e) {
			m.put("errCnt", "-1");
			m.put("MSG", e.getMessage());
		}catch (Exception e) {
			log.info(e.getCause());
			if (e.getCause() instanceof NestedSQLException) {
				m.put("errCnt", "-1");
				m.put("MSG", e.getMessage());
				mav.addAllObjects(m);
				return mav;
			}
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "-1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSIF904/selectOrderCount.action")
	@ResponseBody
	public ModelAndView selectOrderCount(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			Map<String, Object> eaiArguments = new HashMap<String, Object>();
			Map<String, Object> jsonObject = new HashMap<String, Object>();

			String mall_id = reqMap.get("vrSrchMallId").toString();
			String startDate = reqMap.get("vrSrchReqDtFrom").toString();
			String endDate = reqMap.get("vrSrchReqDtTo").toString();
			String orderStatus = reqMap.get("vrSrchOrderStatus").toString();
			String url = "CAFE24/orders/count?";
			jsonObject.put("mall_id", mall_id);
			jsonObject.put("start_date", startDate);
			jsonObject.put("end_date", endDate);
			jsonObject.put("order_status", orderStatus);

			String queryString = CommonUtil.mapToQueryString(jsonObject);

			eaiArguments.put("data", jsonObject.toString());
			eaiArguments.put("cMethod", HttpMethod.GET.toString());
			eaiArguments.put("cUrl", url + queryString);
			eaiArguments.put("hostUrl", request.getServerName());
			m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);
			if (m.containsKey("header") && m.get("header").equals("-1")) {
				m.put("errCnt", "-1");
				m.put("MSG", m.get("message"));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "-1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSIF904/saveCafe24Orders.action")
	@ResponseBody
	public ModelAndView saveCafe24Orders(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			Map<String, Object> eaiArguments = new HashMap<String, Object>();
			JSONObject jsonObject = new JSONObject();

			String mall_id = reqMap.get("vrSrchMallId").toString();
			String startDate = reqMap.get("vrSrchReqDtFrom").toString();
			String endDate = reqMap.get("vrSrchReqDtTo").toString();
			String orderStatus = reqMap.get("vrSrchOrderStatus").toString();
			String url = "CAFE24/orders";
			jsonObject.put("mall_id", mall_id);
			jsonObject.put("start_date", startDate);
			jsonObject.put("end_date", endDate);
			jsonObject.put("order_status", orderStatus);
			jsonObject.put("embed", "items,buyer,receivers");

			String jsonString = jsonObject.toString();

			eaiArguments.put("data", jsonString);
			eaiArguments.put("cMethod", HttpMethod.POST.toString());
			eaiArguments.put("cUrl", url);
			eaiArguments.put("hostUrl", request.getServerName());
			m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);
			if (m.containsKey("header") && m.get("header").equals("-1")) {
				m.put("errCnt", "-1");
				m.put("MSG", m.get("message"));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "-1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSIF904/deleteOrderAsn.action")
	@ResponseBody
	public ModelAndView deleteOrderAsn(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.deleteOrderAsn(request, reqMap);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "-1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSIF904/saveCafe24Shipment.action")
	@ResponseBody
	public ModelAndView saveCafe24Shipment(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveCafe24Shipment(request, reqMap);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "-1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
