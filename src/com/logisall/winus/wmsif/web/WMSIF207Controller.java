package com.logisall.winus.wmsif.web;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Controller; 
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.SessionUtil;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;


@Controller
public class WMSIF207Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : WMSIF207 Method 설명 : inosub dashboard 작성자 : brad07
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSIF207.action")
	public ModelAndView wmsif202(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF207");
	}

	/**
	 * Method ID : WMSIF208 Method 설명 : wcs 창원공동물류센터 dashboard 작성자 : brad07
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSIF208.action")
	public ModelAndView wmsif208(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF208");
	}

	/**
	 * Method ID : WMSIF209 Method 설명 : LG_엔솔 dashboard 1층 작성자 : brad07
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSIF210.action")
	public ModelAndView wmsif210(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF210");
	}

	/**
	 * Method ID : WMSIF211 Method 설명 : KPP 광양자동화창고 대시보드 작성자 : brad07
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSIF211.action")
	public ModelAndView wmsif211(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF211");
	}

	/**
	 * Method ID : WMSIF212 Method 설명 : 삼성SDI 진천물류센터 대시보드 작성자 : brad07
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSIF212.action")
	public ModelAndView wmsif212(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF212");
	}

	/**
	 * Method ID : WMSIF213 Method 설명 : PNS 네트웍스 대시보드 작성자 : brad07
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSIF213.action")
	public ModelAndView wmsif213(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF213");
	}

	/*-
	 * Method ID : wmsop642e3
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt, YSI
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIF213pop.action")
	public ModelAndView WMSIF213pop(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF213pop");
	}
	
	/*-
	 * Method ID : wmsop642e3
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt, YSI
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIF213pop2.action")
	public ModelAndView WMSIF213pop2(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF213pop2");
	}
	
	/*-
	 * Method ID : wmsop642e3
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt, YSI
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIF213pop3.action")
	public ModelAndView WMSIF213pop3(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF213pop3");
	}
	

}