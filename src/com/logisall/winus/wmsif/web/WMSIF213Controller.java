package com.logisall.winus.wmsif.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.WMSIF213Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSIF213Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF213Service")
	private WMSIF213Service service;
	
	static final String[] COLUMN_NAME_WMSIF213 = { "CUSTOMER_R", "WH_R",
		"MTR_CODE", "MTR_DESC", "UNIT_CODE", "LOT_NUMBER", "QTY", "SKU",
		"GROSS_WEIGHT", "NET_WEIGHT", "WH_IN_DT", "WH_OUT_DT", "AGING",
		"LOCATION" };
	
	static final String[] COLUMN_NAME_WMSIF213_IN = { "WH_INFO", "SUPPLIER", "CUSTOMER", "ORDER_NO", "INVOICE", "BL_NO", "CONTAINER_NO", "CONTAINER_TYPE",
		"VESSEL_NAME", "RECEIPT_DATE", "MATERIAL_CODE", "MATERIAL_DESTINATION", "BATCH", "UNIT", "QTY_PLAN", "PLT_PLAN", "QTY_ACTUAL", "PLT_ACTUAL", "QTY_DAMAGE_FND",
		"PLT_DAMAGE_FND", "QTY_DAMAGE_OCCURS", "PLT_DAMAGE_OCCURS", "UNLOADING_PLAN", "UNLOADING_ACTUAL", "PUT_IN_STOCK_PLAN", "PUT_IN_STOCK_ACTUAL",
		"EMPTY_CNT_PLAN", "EMPTY_CNT_ACTUAL"};

	static final String[] COLUMN_NAME_WMSIF213_OUT = { "WH_INFO", "CUSTOMER_CODE", "CUSTOMER_NAME", "FINAL_CUSTOMER", "FINAL_DESTINATION", "ORDER_NO",
		"INVOICE", "TRUCK_TYPE", "TRUCK_NO", "TRUCK_DRIVER", "MATERIAL_CODE", "MATERIAL_DESTINATION", "BATCH", "UNIT", "QTY_PLAN", "PLT_PLAN", "QTY_ACTUAL",
		"PLT_ACTUAL", "LOADING_PLAN", "LOADING_ACTUAL", "ARRIVAL_PLAN", "ARRIVAL_ACTUAL", "REMARK"};

	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF213/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request,
			HttpServletResponse response, Map<String, Object> model,
			@RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName,
					destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(
					destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(),
			// destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			// List<Map> list = ExcelReader.excelLimitRowRead(destination,
			// COLUMN_NAME_WMSOP642, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(
					destination, COLUMN_NAME_WMSIF213, 0, startRow, 10000, 0);

			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i + 2;

				String fieldOrdId = (String) hashMap
						.get(COLUMN_NAME_WMSIF213[0]);
				if (fieldOrdId == null || fieldOrdId.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213[0]
							+ "(Customer reference)Required value Line : " + rowNum);
				}

				String fieldOrdSeq = (String) hashMap
						.get(COLUMN_NAME_WMSIF213[1]);
				if (fieldOrdSeq == null || fieldOrdSeq.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213[1]
							+ "(WH reference)Required value Line :  " + rowNum);
				}

				String fieldDlvCompCd = (String) hashMap
						.get(COLUMN_NAME_WMSIF213[2]);
				if (fieldDlvCompCd == null || fieldDlvCompCd.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213[2]
							+ "(Material code)Required value Line :  " + rowNum);
				}

				String fieldInvcNo = (String) hashMap
						.get(COLUMN_NAME_WMSIF213[5]);
				if (fieldInvcNo == null || fieldInvcNo.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213[5]
							+ "(LotNumber)Required value Line :  " + rowNum);
				}

			}

			m = service.saveCsv2(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF213/inExcelFileInboundUpload.action")
	public ModelAndView inExcelFileInboundUpload(HttpServletRequest request,
			HttpServletResponse response, Map<String, Object> model,
			@RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName,
					destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(
					destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(),
			// destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			// List<Map> list = ExcelReader.excelLimitRowRead(destination,
			// COLUMN_NAME_WMSOP642, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(
					destination, COLUMN_NAME_WMSIF213_IN, 0, startRow, 10000, 0);

			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i + 2;


				String fieldInvoice = (String) hashMap
						.get(COLUMN_NAME_WMSIF213_IN[4]);
				if (fieldInvoice == null || fieldInvoice.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213_IN[4]
							+ "(INVOICE)Required value Line :  " + rowNum);
				}

				String fieldBlNo = (String) hashMap
						.get(COLUMN_NAME_WMSIF213_IN[5]);
				if (fieldBlNo == null || fieldBlNo.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213_IN[5]
							+ "(BL_NO)Required value Line :  " + rowNum);
				}

				String fieldCntNo = (String) hashMap
						.get(COLUMN_NAME_WMSIF213_IN[6]);
				if (fieldCntNo == null || fieldCntNo.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213_IN[6]
							+ "(CONTAINER_NO)Required value Line :  " + rowNum);
				}
				
				String fieldMtrCode = (String) hashMap
						.get(COLUMN_NAME_WMSIF213_IN[9]);
				if (fieldMtrCode == null || fieldMtrCode.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213_IN[9]
							+ "(MATERIAL_CODE)Required value Line :  " + rowNum);
				}
				

			}

			m = service.InbounSaveCsv(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF213/inExcelFileOutboundUpload.action")
	public ModelAndView inExcelFileOutboundUpload(HttpServletRequest request,
			HttpServletResponse response, Map<String, Object> model,
			@RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName,
					destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(
					destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(),
			// destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			// List<Map> list = ExcelReader.excelLimitRowRead(destination,
			// COLUMN_NAME_WMSOP642, 0, startRow, 10000, 0);
			List<Map> list = ExcelReader.excelLimitRowReadByHandler(
					destination, COLUMN_NAME_WMSIF213_OUT, 0, startRow, 10000, 0);

			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i + 2;



				String fieldInvoice = (String) hashMap
						.get(COLUMN_NAME_WMSIF213_OUT[6]);
				if (fieldInvoice == null || fieldInvoice.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213_OUT[6]
							+ "(INVOICE)Required value Line :  " + rowNum);
				}

				String fieldTukNo= (String) hashMap
						.get(COLUMN_NAME_WMSIF213_OUT[8]);
				if (fieldTukNo == null || fieldTukNo.equals("")) {
					throw new Exception("Exception:" + COLUMN_NAME_WMSIF213_OUT[8]
							+ "(TRUCK_NO)Required value Line :  " + rowNum);
				}

			}

			m = service.OutboundSaveCsv(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
    /**
     * Method ID	: WMSIF213.listE2
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF213/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE2(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /**
     * Method ID	: WMSIF213.listE3
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF213/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE3(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /**
     * Method ID	: WMSIF213.listE4
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF213/listE4.action")
    public ModelAndView listE4(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE4(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
	
	/*-
	 * Method ID : WMSIF213pop4
	 * Method 설명 : oz화면출력
	 * 작성자 :  YSI
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIF213pop4.action")
	public ModelAndView WMSIF213pop4(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF213pop4");
	}
	
	/*-
	 * Method ID : WMSIF213pop5
	 * Method 설명 : oz화면출력
	 * 작성자 :  YSI
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIF213pop5.action")
	public ModelAndView WMSIF213pop5(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF213pop5");
	}
}
