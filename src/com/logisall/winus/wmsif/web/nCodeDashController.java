package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.nCodeDashService;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class nCodeDashController {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "nCodeDashService")
    private nCodeDashService service;
    
    /**
     * Method ID	: wmsif203
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF500.action")
    public ModelAndView oliveYoungDash(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/dashboard/ncodeDashBoard");
    }
    
    /**
     * Method ID	: wmsif203
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/ncode_TV.action")
    public ModelAndView oliveYoungDashTv(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/dashboard/ncode-tv");
    }
//    @RequestMapping("/OLVYDASH_TV.action")
//    public ModelAndView oliveYoungDashTv(Map<String, Object> model) throws Exception {
//    	return new ModelAndView("winus/dashboard/oliveyoung-tv");
//    }
//    
    
//    
    /*-
	 * Method ID    : crossDomainHttpWs
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/NCODEDASH/crossDomainHttpWs.action")
	public ModelAndView crossDomainHttpWs(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.crossDomainHttpWs(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/NCODEDASH/crossDomainHttpWs2.action")
	public ModelAndView crossDomainHttpWs2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.crossDomainHttpWs2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
    
}
