package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF707Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF707Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF707Service")
    private WMSIF707Service service;
    
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;		
    
    /**
     * Method ID	: wmsif301
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF707.action")
    public ModelAndView WMSIF707(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF707");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {        			
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    /**
     * Method ID	: listE2
     * Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    /**
     * Method ID	: OrderCollect
     * Method 설명	: 주문수집
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/OrderCollect.action")
    public ModelAndView OrderCollect(Map<String, Object> model , HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");    	
    	Map<String, Object> m = new HashMap<String, Object>();
    	List<Object> concurcInfo;
    	String concurcSeq;    	
    	
    	String serviceNm	= "/WMSIF707/OrderCollect.action";
    	String ccSeparator1 = model.get("CUST_ID").toString();		// 화주
    	String ccSeparator2 = StringUtils.isEmpty(model.get("shop_id").toString())
    								? "All"
    								: model.get("shop_id").toString();		// 판매처
    	String ccSeparator3 = "";
    	String ccSeparator4 = "";
    	String ccSeparator5 = "";    	
    	
        try {     	

        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);        	
        	
        	if(concurcInfo.size() == 0 && !StringUtils.equals("All", ccSeparator2)){
        		// 특정 판매처 수집 시, 동작수행 중인 전체 범위작업과 교차되는 범위 탐색
        		concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, "All", ccSeparator3, ccSeparator4, ccSeparator5);	
        	}        	
        	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception]\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        		
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();        	
        	
        	String cMethod	= "POST";
			String cUrl 	= "EZADMIN/ORD/COLLECT/ALL";
			String hostUrl  = request.getServerName();			
            
			JSONObject jsonObject = new JSONObject();
			JSONObject data1 = new JSONObject();
			
			jsonObject.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID", model.get("CUST_ID").toString());
			jsonObject.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP", model.get("SS_CLIENT_IP").toString());
	            
			data1.put("partner_key"       ,    model.get("partner_key").toString());
			data1.put("domain_key"        ,    model.get("domain_key").toString());
			data1.put("action"            ,    model.get("action").toString());
			data1.put("date_type"         ,    model.get("vrCollectType").toString());
			data1.put("start_date"        ,    model.get("vrColStDateFrom").toString());
			data1.put("end_date"          ,    model.get("vrColStDateTo").toString());
//			data1.put("sub_domain_seq"    ,    "");
			data1.put("shop_id"           ,    model.get("shop_id").toString());
//			data1.put("group_id"          ,    "");
			
			if(model.containsKey("vrPackSeqYn") && !StringUtils.isEmpty(model.get("vrPackSeqYn").toString())){
				data1.put("seq_pack",    model.get("vrOrderSeq").toString());		// 관리번호 & 합포장 검색
			}
			else{
				data1.put("seq",    model.get("vrOrderSeq").toString());			// 관리번호 검색 (단 건)
			}		
			
			
			data1.put("order_id"          ,    model.get("vrOrderId").toString());
//			data1.put("trans_no"          ,    "");
			data1.put("status"            ,    model.get("vrOrdType").toString());
			data1.put("order_cs"          ,    model.get("vrOrdStatus").toString());
			data1.put("trans_delay_date"  ,    model.get("vrDelayDt").toString());
			data1.put("limit"             ,    model.get("limit").toString());
			data1.put("page"              ,    model.get("page").toString());
			data1.put("is_cancel"         ,    model.get("is_cancel").toString());		
			
			jsonObject.put("getReqOrd", data1);
						
			String dataJson = jsonObject.toString();			
					
	        System.out.println("dataJson : " + dataJson);
			
			model.put("data"	    , dataJson);
			model.put("cMethod"		, cMethod);
			model.put("cUrl"		, cUrl);
			model.put("hostUrl"		, hostUrl);
			
	        m = WMSIF000Service.crossDomainHttpWs5200(model);
			
			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);		
			
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
        	m.put("errCnt", 1);      
			m.put("MSG", e.getMessage());            
        }
        
        mav.addAllObjects(m);
        return mav;
    }

    
    /**
     * Method ID	: MissOrderCollect
     * Method 설명	: 미수집 주문 재수행
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/MissOrderCollect.action")
    public ModelAndView MissOrderCollect(Map<String, Object> model , HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> resultMessage 	= new HashMap<String, Object>();
    	
        try {
        	String cMethod	= "POST";
			String cUrl 	= "EZADMIN/ORD/COLLECT";
			String hostUrl  = request.getServerName();			
            String[] ordIdList = (model.get("orderIdList").toString()).split(",");            
            int total_successCnt = 0;
            int total_failCnt = 0;
            
            for(String ordId : ordIdList){
            	
            	Map<String, Object> resultData 		= new HashMap<String, Object>();
            	
            	JSONObject jsonObject 				= new JSONObject();
    			JSONObject data1 					= new JSONObject();
    			
    			jsonObject.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    			jsonObject.put("CUST_ID", model.get("CUST_ID").toString());
    			jsonObject.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			jsonObject.put("WORK_IP", model.get("SS_CLIENT_IP").toString());
    	            
    			data1.put("partner_key"       ,    model.get("partner_key").toString());
    			data1.put("domain_key"        ,    model.get("domain_key").toString());
    			data1.put("action"            ,    model.get("action").toString());
    			data1.put("date_type"         ,    model.get("date_type").toString());
//    			data1.put("start_date"        ,    "");
//    			data1.put("end_date"          ,    "");
//    			data1.put("sub_domain_seq"    ,    "");
//    			data1.put("shop_id"           ,    "");
//    			data1.put("group_id"          ,    ""); 
//    			data1.put("seq"               ,    model.get("vrOrderSeq").toString());
    			data1.put("order_id"          ,    ordId);
//    			data1.put("trans_no"          ,    "");
    			data1.put("status"            ,    model.get("status").toString());
//    			data1.put("order_cs"          ,    model.get("vrOrdStatus").toString());
//    			data1.put("trans_delay_date"  ,    model.get("vrDelayDt").toString());
    			data1.put("limit"             ,    model.get("limit").toString());
    			data1.put("page"              ,    model.get("page").toString());
//    			data1.put("is_cancel"         ,    model.get("is_cancel").toString());		
    			
    			jsonObject.put("getReqOrd", data1);
    						
    			String dataJson = jsonObject.toString();			
    					
    	        System.out.println("dataJson : " + dataJson);
    			
    			model.put("data"	    , dataJson);
    			model.put("cMethod"		, cMethod);
    			model.put("cUrl"		, cUrl);
    			model.put("hostUrl"		, hostUrl);
    			
    			resultData = WMSIF000Service.crossDomainHttpWs5200(model);
    			
    			JSONObject jsonData = new JSONObject(resultData.get("RESULT").toString());
    			Iterator iterator = jsonData.keys();
    			
    			while(iterator.hasNext()){
    				String key = (String)iterator.next();
    				
    				if(key.equals("SUCCESS_CNT")){
    					total_successCnt += Integer.parseInt(jsonData.get("SUCCESS_CNT").toString());
    				}
    				
    				if(key.equals("FAIL_CNT")){
    					total_failCnt += Integer.parseInt(jsonData.get("FAIL_CNT").toString());
    				}   				
    			}    			
            }
            
            resultMessage.put("SUCCESS_CNT", total_successCnt);
            resultMessage.put("FAIL_CNT", total_failCnt);            

        } catch (Exception e) {
            e.printStackTrace();
            resultMessage.put("ERROR", e);
        }
       
        mav.addAllObjects(resultMessage);
        return mav;
    }
    
    
    /**
     * Method ID	: syncOrder
     * Method 설명	: Winus 주문등록
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/syncOrder.action")
    public ModelAndView syncOrder(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.syncOrder(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    
    /**
     * Method ID	: registInvoice
     * Method 설명	: 택배송장등록
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/registInvoice.action")
    public ModelAndView registInvoice (Map<String, Object> model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> eaiArguments = new HashMap<String, Object>();
        
        try {					
            
			JSONObject jsonObject 	= new JSONObject();
			JSONObject data1 		= new JSONObject();			
			JSONParser jsonParser 	= new JSONParser();
			Object gridData 		= jsonParser.parse(model.get("gridData").toString());
			
			// INPUT
			jsonObject.put("LC_ID"		, 	(String)model.get(ConstantIF.SS_SVC_NO));
			jsonObject.put("CUST_ID"	, 	model.get("CUST_ID").toString());
			jsonObject.put("USER_NO"	, 	(String)model.get(ConstantIF.SS_USER_NO));
			jsonObject.put("WORK_IP"	,	model.get("SS_CLIENT_IP").toString());
	            
			// docRequest
			data1.put("partner_key"       ,    model.get("partner_key").toString());
			data1.put("domain_key"        ,    model.get("domain_key").toString());
			data1.put("action"            ,    model.get("action").toString());
			data1.put("trans_pos"         ,    model.get("vrTransType").toString());		// 송장상태처리 (배송처리)
			data1.put("type"        	  ,    model.get("vrDlvType").toString());			// 배송처리 (타입)
			data1.put("orderData"         ,    gridData);		// Client로 부터받은 Grid 데이터
						
			jsonObject.put("docRequest", data1);
			
			
			String dataJson = jsonObject.toString();
								
	        System.out.println("dataJson : " + dataJson);
			
	        eaiArguments.put("data"	    , dataJson);
			eaiArguments.put("cMethod"	, "POST");
			eaiArguments.put("cUrl"		, "EZADMIN/INVC/SEND/ALL");
			eaiArguments.put("hostUrl"	, request.getServerName());
			
	        m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);				
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        mav.addAllObjects(m);
        return mav;
    }


    /**
     * Method ID	: getTransCorpInfo
     * Method 설명	: 택배사 맵핑정보 획득
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/getTransCorpInfo.action")
    public ModelAndView getTransCorpInfo(Map<String, Object> model) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");;
    	Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.getTransCorpInfo(model);
        } catch (Exception e) {
            e.printStackTrace();
            if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        
        mav.addAllObjects(m);
        return mav;
    }
    
    
    /**
     * Method ID	: deleteE1
     * Method 설명	: 인터페이스 주문삭제
     * 작성자			: dhkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF707/deleteE1.action")
    public ModelAndView deleteE1(Map<String, Object> model , HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("jsonView");    	
    	Map<String, Object> m = new HashMap<String, Object>();  	
	
    	
        try {    	
        
        	m = service.deleteE1(model);
			
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
        	m.put("errCnt", 1);      
			m.put("MSG", e.getMessage());            
        }
        
        mav.addAllObjects(m);
        return mav;
    }


}

