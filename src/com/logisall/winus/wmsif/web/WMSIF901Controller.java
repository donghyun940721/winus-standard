package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.logisall.winus.wmsif.service.WMSIF901Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
/**
 * MOBIS AL 인터페이스 관리 컨트롤러
 */
@Controller
public class WMSIF901Controller {
    protected Log log = LogFactory.getLog(this.getClass());
    @Autowired
    private WMSIF901Service service;
    
    /**
     * Method ID	: WMSIF901
     * Method 설명	: 
     * 작성자			: kih
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF901.action")
    public ModelAndView WMSIF901(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF901");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: kih
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF901/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: historyList
     * Method 설명	: 
     * 작성자			: kih
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF901/historyListE02.action")
    public ModelAndView historyListE02(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF901/historyListE03.action")
    public ModelAndView historyListE03(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF901/historyListE04.action")
    public ModelAndView historyListE04(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE4(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF901/historyListE05.action")
    public ModelAndView historyListE05(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE5(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
	/*-
	 * Method ID    : runMobisProcedure
	 * Method 설명      : 
	 * 작성자                 : kih
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF901/runMobisProcedure.action")
	@ResponseBody
	public ModelAndView runMobisProcedure(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.runMobisProcedure(model, reqMap);
			// 에러메세지 save.error 로 통일할경우 
			// if(Integer.parseInt(m.get("errCnt").toString()) != 0){
			// throw new Exception();
			// }
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*
	 * 입고 ASN IF 리스트 조회 후 체크된 것만 실행
	 * */
	@RequestMapping("/WMSIF901/saveInboundIf.action")
	@ResponseBody
    public ModelAndView saveInboundIf(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.saveInboundIf(model, reqMap);
            // 에러메세지 save.error 로 통일할경우 
            // if(Integer.parseInt(m.get("errCnt").toString()) != 0){
            // throw new Exception();
            // }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save approve :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
	
	/*
     * 출고확정 IF 리스트 조회 후 체크된 것만 삭제
     * */
    @RequestMapping("/WMSIF901/deleteOutboundIf.action")
    @ResponseBody
    public ModelAndView deleteOutboundIf(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.deleteOutboundIf(model, reqMap);
            // 에러메세지 save.error 로 통일할경우 
            // if(Integer.parseInt(m.get("errCnt").toString()) != 0){
            // throw new Exception();
            // }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save approve :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    
	/*
     * 출고주문 IF 리스트 조회 후 체크된 것만 삭제(DEL_YN)
     * */
    @RequestMapping("/WMSIF901/deleteOutboundOrderIf.action")
    @ResponseBody
    public ModelAndView deleteOutboundOrderIf(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.deleteOutboundOrderIf(model, reqMap);
            // 에러메세지 save.error 로 통일할경우 
            // if(Integer.parseInt(m.get("errCnt").toString()) != 0){
            // throw new Exception();
            // }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save approve :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
	
    /*
     * 출고실적 IF 리스트 조회 후 체크된 것만 삭제
     * */
    @RequestMapping("/WMSIF901/deleteOutboundIf2.action")
    @ResponseBody
    public ModelAndView deleteOutboundIf2(Map<String, Object> model, @RequestBody Map<String, Object> reqMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            m = service.deleteOutboundIf2(model, reqMap);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save approve :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
}
