package com.logisall.winus.wmsif.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsif.service.WMSIF602Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF602Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF602Service")
	private WMSIF602Service service;

	@RequestMapping("/WINUS/WMSIF602.action")
	public ModelAndView wmsif601(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF602");
	}
	
    @RequestMapping("/WMSIF602/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF602/takeDegree.action")
    public ModelAndView takeDegree(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.takeDegree(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF602/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    @RequestMapping("/WMSIF602/orderIns.action")
    public ModelAndView orderIns(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.orderIns(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF602/orderIns_return.action")
    public ModelAndView orderIns_return(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.orderIns_return(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
}
