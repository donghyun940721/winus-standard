package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF604Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF604Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF604Service")
	private WMSIF604Service service;

	@Resource(name = "WMSOP642Service")
	private WMSOP642Service WMSOP642_service;
	
	@Resource(name = "WMSYS400Service")
	private WMSYS400Service WMSYS400Service;
	
	@RequestMapping("/WINUS/WMSIF604.action")
	public ModelAndView WMSIF604(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF604");
	}
	
    @RequestMapping("/WMSIF604/listE1.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE1(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF604/listE2.action")
    public ModelAndView listE2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    @RequestMapping("/WMSIF604/listE3.action")
    public ModelAndView listE3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listE3(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
	/**
	 * Method ID 	: listE3
	 * Method 설명 	: 택배 추가송장 통계
	 * 작성자 :
	 * @param model
	 * @return
	 **/
	@RequestMapping("/WMSIF604/listE3Summary.action")
	public ModelAndView listE3Summary(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listE3Summary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
    /**
	* Method ID 	: dasResultCoop
	* Method 설명 	: 다스결과내역 검수확정
	* 작성자 			: HDY
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF604/dasResultCoop.action")
	public ModelAndView dasResultCoop(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			//System.out.println(model);
			m = service.dasResultCoop(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m = new HashMap<String, Object>();
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());

		}
		mav.addAllObjects(m);
		return mav;
	}
	
    /**
	* Method ID 	: createDasResultData
	* Method 설명 	: 다스결과내역 검수확정
	* 작성자 		: dhkim
	* @param model
	* @return
	*/	
	@RequestMapping("/WMSIF604/createDasResultData.action")
	public ModelAndView createDasResultData(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
        String concurcSeq;     
    	
		try {
			String serviceNm	= "DAS 검수결과 생성";
	    	String ccSeparator1 = model.get("CUST_ID").toString();
	    	String ccSeparator2 = model.get("ORDER_WAVE").toString();
	    	String ccSeparator3 = "";
	    	String ccSeparator4 = "";
	    	String ccSeparator5 = "";  
	    	
	    	/** 동시성 제어 (유효성 검사) */
	    	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
	       	
	    	if(concurcInfo.size() > 0){
	    		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
	    		        		
	    		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
	    		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
	    		
	    		throw new Exception(excecptionMsg);
	    	}
	    		
	    	/** 동시성 제어 (작업 생성) */
	    	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
	    								.get("SEQ")
	    								.toString();
	    	
			m = service.createDasResultData(model);
			
			/** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m = new HashMap<String, Object>();
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());

		}
		mav.addAllObjects(m);
		return mav;
	}	
	
	/**
	 * Method ID	: sendProductInfoIF
	 * Method 설명	: 다스 코텍전자  송장 추가 접수 결과 전송
	 * 작성자		: 
	 * @param   model
	 * @return  
	 */	
	@RequestMapping("/WMSIF604/sendProductInfoIF.action")
	public ModelAndView sendProductInfoIF(Map<String, Object> model, HttpServletRequest request ) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		List<Object> concurcInfo;
        String concurcSeq;     
        
		try {			
			Map<String, Object> params = new HashMap<String, Object>();
			int tmpCnt = Integer.parseInt(model.get("selectIds").toString());			
			
			String serviceNm	= "DAS 추가송장 접수";
        	String ccSeparator1 = model.get("vrCustId").toString();
        	String ccSeparator2 = model.get("vrOrdDt").toString();
        	String ccSeparator3 = StringUtils.isEmpty(model.get("vrDevOrdDegree").toString()) ? "All" : model.get("vrDevOrdDegree").toString();
        	String ccSeparator4 = "";
        	String ccSeparator5 = "";  
        	
        	/** 동시성 제어 (유효성 검사) */
        	concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5);
        	
        	// 전체 차수 대상인 경우, 개별 차수작업 제한
           	if(concurcInfo.size() == 0 && !StringUtils.equals("All", ccSeparator3)){
        		concurcInfo = WMSYS400Service.list(model, serviceNm, ccSeparator1, ccSeparator2, "All", ccSeparator4, ccSeparator5);	
        	}        	
           	
        	if(concurcInfo.size() > 0){
        		String excecptionMsg = "\n[Concurrency Exception] 작업 범위가 같은 작업자가 존재합니다.\n";        		
        		        		
        		excecptionMsg += "User : " + ((Map<String, Object>)concurcInfo.get(0)).get("USER_NM") + "\n";        		
        		excecptionMsg += "IP : " + ((Map<String, Object>)concurcInfo.get(0)).get("WORK_IP");
        		
        		throw new Exception(excecptionMsg);
        	}
        		
        	/** 동시성 제어 (작업 생성) */
        	concurcSeq = WMSYS400Service.insert(model, serviceNm, ccSeparator1, ccSeparator2, ccSeparator3, ccSeparator4, ccSeparator5)
        								.get("SEQ")
        								.toString();  
			//택배 송장 발행
			for(int i = 0 ; i < tmpCnt; i++){
				params.put("LC_ID"+i, (String)request.getSession().getAttribute(ConstantIF.SS_SVC_NO));
				params.put("CUST_ID"+i, (String)model.get("vrCustId"));
				params.put("TRACKING_NO"+i, (String)model.get("BOX_NO"+i));//박스번호
				params.put("DENSE_FLAG"+i, (String)model.get("DENSE_FLAG"+i)); //송장단위로 그룹 넘버링
				params.put("ORD_QTY"+i, (String)model.get("ITEM_QTY"+i)); //박스에 담기는 수량
				params.put("ORD_ID"+i, (String)model.get("ORDER_CD"+i));
				params.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
				params.put("SET_INVC_NO"+i, (String)model.get("INVO_NO"+i)); //송장
				params.put("PARCEL_BOX_TY"+i, (String)model.get("PARCEL_BOX_TY"+i)); //박스코드 (기준코드 : PARCEL_BOX      택배박스구분(CJ) 참조)
		    }			
			
			params.put("selectIds", (String)model.get("selectIds"));
			params.put("PARCEL_SEQ_YN", "Y"); //'Y' : 고정
			params.put("PARCEL_COM_TY", "04"); //대한통운 '04'
			params.put("PARCEL_COM_TY_SEQ", "1"); // wmsdf001의 대한통운의 순번
			params.put("PARCEL_ORD_TY", "01"); // '01'
			params.put("PARCEL_PAY_TY", "03"); //신용 : 03
			params.put("PARCEL_ETC_TY", "01"); //''
			params.put("PARCEL_PAY_PRICE", "");
			params.put("ADD_FLAG", "N"); //'N' : 고정
			params.put("DIV_FLAG", "DIV"); //'DIV' : 고정
		   
			params.put("SS_CLIENT_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			params.put("SS_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		   
		    String hostUrl = request.getServerName();
		    params.put("hostUrl", hostUrl);		   
		    
		    m = WMSOP642_service.DlvInvcNoOrderCommTotal(params);
		    
		    /** 동시성 제어 (작업해제) */
			WMSYS400Service.complete(model, concurcSeq);
			
		} catch (Exception e) {			
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			
			m.put("errCnt", 1);
			m.put("message", MessageResolver.getMessage("save.error"));
			m.put("MSG", e.getMessage()); 
		}
		
		mav.addAllObjects(m);
		return mav;
	}
}
