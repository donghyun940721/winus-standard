package com.logisall.winus.wmsif.web;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.m2m.jdfw5x.egov.servlet.ModelAndView;
//import com.logisall.winus.wmsif.service.kccDashService;

@Controller
public class JvDashController {
    protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID	: JvEurope
     * Method 설명	: JvEurope 대시보드
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF510.action")
    public ModelAndView JvEurope(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/dashboard/jv");
    }
}
