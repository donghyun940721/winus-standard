package com.logisall.winus.wmsif.model;

public enum Wellfood {
	WH_SUWON("0000004460", "K9767701"), CUST_SUWON("0000553456", "K9767701"), EA_SUWON("0000018502", "EA"),

	WH_BUSAN("0000004461", "K9787701"), CUST_BUSAN("0000553457", "K9787701"), EA_BUSAN("0000018513", "EA");

	private final String winusCode;
	private final String wellfoodCode;

	Wellfood(String winusCode, String wellfoodCode) {
		this.winusCode = winusCode;
		this.wellfoodCode = wellfoodCode;
	}

	public String getWinusCode() {
		return winusCode;
	}

	public String getWellfoodCode() {
		return wellfoodCode;
	}
	
	public static String getWellfoodCodeByWinusCode(String winusCode) {
        for (Wellfood wf : Wellfood.values()) {
            if (wf.getWinusCode().equals(winusCode)) {
                return wf.getWellfoodCode();
            }
        }
        throw new NullPointerException("매핑되는 코드가 없습니다");
    }

    public static String getWinusCodeByWellfoodCode(String wellfoodCode) {
        for (Wellfood wf : Wellfood.values()) {
            if (wf.getWellfoodCode().equals(wellfoodCode)) {
                return wf.getWinusCode();
            }
        }
        throw new NullPointerException("매핑되는 코드가 없습니다");
    }
	
}
