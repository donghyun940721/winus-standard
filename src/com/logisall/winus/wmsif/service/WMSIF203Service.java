package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF203Service {
	public Map<String, Object> listExtra(Map<String, Object> model) throws Exception;
	public Map<String, Object> listExtra() throws Exception;
}
