package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF726Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list01(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list02(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list03(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list04(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list05(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list06(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list07(Map<String, Object> model) throws Exception;    
    public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
   // public Map<String, Object> wmsOrderDelete(Map<String, Object> model) throws Exception;    
}
