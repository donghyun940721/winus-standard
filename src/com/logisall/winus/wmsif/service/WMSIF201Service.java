package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF201Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
}
