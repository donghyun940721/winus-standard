package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF715Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;    
    public void InvcSendCntSave(Map<String, Object> model) throws Exception;    
    public Map<String, Object> wmsOrdRegister(Map<String, Object> model) throws Exception;    
    public Map<String, Object> wmsOrderDelete(Map<String, Object> model) throws Exception;    
    public Map<String, Object> IFOrderDelete(Map<String, Object> model) throws Exception;    
    public String getBflowToken(Map<String, Object> model) throws Exception;    
}
