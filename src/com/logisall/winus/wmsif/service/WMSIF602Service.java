package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF602Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> takeDegree(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
	public Map<String, Object> orderIns(Map<String, Object> model) throws Exception;
	public Map<String, Object> orderIns_return(Map<String, Object> model) throws Exception;
}
