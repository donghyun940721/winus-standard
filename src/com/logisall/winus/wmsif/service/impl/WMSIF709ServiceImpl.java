package com.logisall.winus.wmsif.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsif.service.WMSIF709Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF709Service")
public class WMSIF709ServiceImpl implements WMSIF709Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF709Dao")
    private WMSIF709Dao dao;
    
	@Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
    
    /**
     * 
     * 대체 Method ID		: listE1
     * 대체 Method 설명		: 스카이젯 주문관리 입고 조회 (OM)
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) { 
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }      

        
        map.put("LIST", dao.listE1(model));
        return map;
    }    

    
	/**
     * 
     * 대체 Method ID		: saveE1
     * 대체 Method 설명	: 스카아젯 매입주문 입력 (스카이젯 → Winus)
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

    	try{
		 	int tmpCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
		 	
	 		String[] no             = new String[tmpCnt];      	//no(처리)
			
			String[] outReqDt       = new String[tmpCnt];   
			String[] inReqDt        = new String[tmpCnt];     
			String[] custOrdNo      = new String[tmpCnt];     	//원주문번호(처리)
			String[] custOrdSeq     = new String[tmpCnt];    	//원주문seq(처리)
			String[] trustCustCd    = new String[tmpCnt];     
			
			String[] transCustCd    = new String[tmpCnt];                     
			String[] transCustTel   = new String[tmpCnt];         
			String[] transReqDt     = new String[tmpCnt];     
			String[] custCd         = new String[tmpCnt];      	//화주코드(처리)
			String[] ordQty         = new String[tmpCnt];     	//수량(처리) 
			
			String[] uomCd          = new String[tmpCnt];                
			String[] sdeptCd        = new String[tmpCnt];         
			String[] salePerCd      = new String[tmpCnt];     
			String[] carCd          = new String[tmpCnt];     
			String[] drvNm          = new String[tmpCnt];     
			
			String[] dlvSeq         = new String[tmpCnt];                
			String[] drvTel         = new String[tmpCnt];         
			String[] custLotNo      = new String[tmpCnt];     
			String[] blNo           = new String[tmpCnt];     
			String[] recDt          = new String[tmpCnt];     
			
			String[] outWhCd        = new String[tmpCnt];                
			String[] inWhCd         = new String[tmpCnt];         
			String[] makeDt         = new String[tmpCnt];     
			String[] timePeriodDay  = new String[tmpCnt];     
			String[] workYn         = new String[tmpCnt];     
			
			String[] rjType         = new String[tmpCnt];        //반품사유         
			String[] locYn          = new String[tmpCnt];         
			String[] confYn         = new String[tmpCnt];     
			String[] eaCapa         = new String[tmpCnt];     
			String[] inOrdWeight    = new String[tmpCnt];     
			
			String[] itemCd         = new String[tmpCnt];        //상품코드(처리)        
			String[] itemNm         = new String[tmpCnt];        //상품명 (처리) 
			String[] transCustNm    = new String[tmpCnt];     
			String[] transCustAddr  = new String[tmpCnt];     
			String[] transEmpNm     = new String[tmpCnt];     
			
			String[] remark         = new String[tmpCnt];                
			String[] transZipNo     = new String[tmpCnt];         
			String[] etc2           = new String[tmpCnt];     
			String[] unitAmt        = new String[tmpCnt];     
			String[] transBizNo     = new String[tmpCnt];     
			
			String[] inCustAddr     = new String[tmpCnt];                
			String[] inCustCd       = new String[tmpCnt];         
			String[] inCustNm       = new String[tmpCnt];     
			String[] inCustTel      = new String[tmpCnt];     
			String[] inCustEmpNm    = new String[tmpCnt];     
			
			String[] expiryDate     = new String[tmpCnt];
			String[] salesCustNm    = new String[tmpCnt]; //수령인명(처리)
			String[] zip     		= new String[tmpCnt]; //수령인우편번호(처리)
			String[] addr     		= new String[tmpCnt]; //수령인주소(처리)
			String[] phone1    	 	= new String[tmpCnt];//수령인핸드폰번호(처리)
			
			String[] etc1    		= new String[tmpCnt];
			String[] unitNo    		= new String[tmpCnt];                
			String[] salesCompanyNm	 	= new String[tmpCnt];
			String[] timeDate       = new String[tmpCnt];   //상품유효기간     
			String[] timeDateEnd    = new String[tmpCnt];   //상품유효기간만료일
			
			String[] timeUseEnd     = new String[tmpCnt];   //소비가한만료일
			String[] locCd     		= new String[tmpCnt];   //로케이션코드
			
			String[] epType     	= new String[tmpCnt];   //납품유형
				
			String[] lotType     	= new String[tmpCnt];   //LOT속성 	2022-03-04
			String[] ownerCd     	= new String[tmpCnt];   //소유자		2022-03-04
			
			String[] custLegacyItemCd   = new String[tmpCnt]; 
			String[] itemBarcode     	= new String[tmpCnt]; 
			String[] itemSize     		= new String[tmpCnt]; 
			String[] color     			= new String[tmpCnt]; 
			
			for(int i = 0 ; i < tmpCnt ; i ++){
			 	String NO              = Integer.toString(i+1);//no(처리)
				String OUT_REQ_DT      = "";
				String IN_REQ_DT       = "";  //sysdate
				String CUST_ORD_NO     = (String)model.get("I_CUST_ORD_NO" + i); 
				String CUST_ORD_SEQ    = (String)model.get("I_CUST_ORD_SEQ" + i);	//원주문seq(처리)
				String TRUST_CUST_CD   = "";
				String TRANS_CUST_CD   = (String)model.get("I_TRANS_CUST_CD" + i);	//
				String TRANS_CUST_TEL  = "";
				String TRANS_REQ_DT    = "";
				String CUST_CD         = (String)model.get("I_CUST_CD" + i);//화주코드(처리)
				String ORD_QTY         = (String)model.get("I_ORD_QTY" + i);//출고수량(처리)
				String UOM_CD          = (String)model.get("I_UOM_CD" + i);
				String SDEPT_CD        = "";
				String SALE_PER_CD     = "";
				String CAR_CD          = "";
				String DRV_NM          = "";
				String DLV_SEQ         = "";
				String DRV_TEL         = "";
				String CUST_LOT_NO     = (String)model.get("I_CUST_LOT_NO" + i);
				String BL_NO           = "";
				String REC_DT          = "";
				String OUT_WH_CD       = "";
				String IN_WH_CD        = "";
				String MAKE_DT         = (String)model.get("I_MAKE_DT" + i);//상품코드(처리);
				String TIME_PERIOD_DAY = "";
				String WORK_YN         = "";
				String RJ_TYPE         = "";
				String LOC_YN          = "";
				String CONF_YN         = "";
				String EA_CAPA         = "";
				String IN_ORD_WEIGHT   = "";
				String ITEM_CD         = (String)model.get("I_ITEM_CD" + i);//상품코드(처리)
				String ITEM_NM         = (String)model.get("I_ITEM_NM" + i);//상품명(처리)
				String TRANS_CUST_NM   = "";
				String TRANS_CUST_ADDR = "";
				String TRANS_EMP_NM    = "";
				String REMARK          = (String)model.get("I_REMARK" + i);	//
				String TRANS_ZIP_NO    = "";
				String ETC2            = (String)model.get("I_ETC2" + i);	//
				String UNIT_AMT        = (String)model.get("I_UNIT_AMT" + i);	//
				String TRANS_BIZ_NO    = "";
				String IN_CUST_ADDR    = "";
				String IN_CUST_CD      = (String)model.get("I_IN_CUST_CD" + i);
				String IN_CUST_NM      = (String)model.get("I_IN_CUST_NM" + i);;
				String IN_CUST_TEL     = "";
				String IN_CUST_EMP_NM  = "";
				String EXPIRY_DATE     = (String)model.get("I_EXPIRY_DATE" + i); // 유효기간 만료일
				String SALES_CUST_NM   = "";		//수령인명(처리)
				String ZIP             = "";		//수령인우편번호(처리)
				String ADDR            = "";		//수령인주소(처리)
				String PHONE_1         = "";		//수령인핸드폰번호(처리)
				String ETC1            = (String)model.get("I_ETC1" + i);
				String UNIT_NO         = "";
				String SALES_COMPANY_NM   = "";
				String TIME_DATE       = "";
				String TIME_DATE_END   = "";
				String TIME_USE_END    = "";
				String LOC_CD    	   = "";
				String EP_TYPE    	   = "";
				
				String LOT_TYPE         = ""; // 템플릿 추가 sing09  2022-03-04
				String OWNER_CD			= ""; // 템플릿 추가 sing09  2022-03-04
				
				String CUST_LEGACY_ITEM_CD 	= "";
				String ITEM_BARCODE			= "";
				String ITEM_SIZE			= "";
				String COLOR				= "";
				
			 	//변수에 담음. 
				no[i]               = NO;
				outReqDt[i]         = OUT_REQ_DT.replaceAll("[^\\d]", "");    
				inReqDt[i]          = IN_REQ_DT.replaceAll("[^\\d]", "");    
				custOrdNo[i]        = CUST_ORD_NO;    
				custOrdSeq[i]       = CUST_ORD_SEQ;    
				trustCustCd[i]      = TRUST_CUST_CD;    
				
				transCustCd[i]      = TRANS_CUST_CD;    
				transCustTel[i]     = TRANS_CUST_TEL;    
				transReqDt[i]       = TRANS_REQ_DT;    
				custCd[i]           = CUST_CD;    
				ordQty[i]           = ORD_QTY;    
				
				uomCd[i]            = UOM_CD;    
				sdeptCd[i]          = SDEPT_CD;    
				salePerCd[i]        = SALE_PER_CD;    
				carCd[i]            = CAR_CD;
				drvNm[i]            = DRV_NM;
				
				dlvSeq[i]           = DLV_SEQ;    
				drvTel[i]           = DRV_TEL;    
				custLotNo[i]        = CUST_LOT_NO;    
				blNo[i]             = BL_NO;    
				recDt[i]            = REC_DT;    
				
				outWhCd[i]          = OUT_WH_CD;
				inWhCd[i]           = IN_WH_CD;
				makeDt[i]           = MAKE_DT;
				timePeriodDay[i]    = TIME_PERIOD_DAY;
				workYn[i]           = WORK_YN;
				
				rjType[i]           = RJ_TYPE;    
				locYn[i]            = LOC_YN;    
				confYn[i]           = CONF_YN;    
				eaCapa[i]           = EA_CAPA;    
				inOrdWeight[i]      = IN_ORD_WEIGHT;   
				
				itemCd[i]           = ITEM_CD;    
				itemNm[i]           = ITEM_NM;    
				transCustNm[i]      = TRANS_CUST_NM;    
				transCustAddr[i]    = TRANS_CUST_ADDR;    
				transEmpNm[i]       = TRANS_EMP_NM;    
				
				remark[i]           = REMARK;    
				transZipNo[i]       = TRANS_ZIP_NO;    
				etc2[i]             = ETC2;    
				unitAmt[i]          = UNIT_AMT.replaceAll("[^\\d]", "");    
				transBizNo[i]       = TRANS_BIZ_NO;    
				
				inCustAddr[i]       = IN_CUST_ADDR;   
				inCustCd[i]         = IN_CUST_CD;    
				inCustNm[i]         = IN_CUST_NM;    
				inCustTel[i]        = IN_CUST_TEL;    
				inCustEmpNm[i]      = IN_CUST_EMP_NM;    
				
				expiryDate[i]       = EXPIRY_DATE;
				salesCustNm[i]      = SALES_CUST_NM;
				zip[i]       		= ZIP;
				addr[i]       		= ADDR;
				phone1[i]       	= PHONE_1;
				
				etc1[i]      		= ETC1;
				unitNo[i]      		= UNIT_NO;
				salesCompanyNm[i]   = SALES_COMPANY_NM;
				timeDate[i]         = TIME_DATE;      
				timeDateEnd[i]      = TIME_DATE_END;     
				
				timeUseEnd[i]       = TIME_USE_END;
				locCd[i]       		= LOC_CD;
				epType[i]       	= EP_TYPE;//납품유형 신규컬럼 추가
				
				lotType[i]       	= LOT_TYPE;		//LOT속성 신규컬럼 추가
				ownerCd[i]       	= OWNER_CD;		//소유자 신규컬럼 추가
				
				custLegacyItemCd[i] = CUST_LEGACY_ITEM_CD; 
				itemBarcode[i]     	= ITEM_BARCODE; 
				itemSize[i]     	= ITEM_SIZE; 
				color[i]     		= COLOR;				
				
			}
			
			//프로시져에 보낼것들 다담는다
			Map<String, Object> modelIns = new HashMap<String, Object>();
			
			modelIns.put("vrOrdType"		, model.get("I_ORD_TYPE"));
			modelIns.put("vrOrdSubType"		, model.get("I_ORD_SUBTYPE"));
			
			modelIns.put("no"  , no);

			modelIns.put("reqDt"     	, inReqDt);			
			modelIns.put("custOrdNo"    	, custOrdNo);
			modelIns.put("custOrdSeq"   	, custOrdSeq);
			modelIns.put("trustCustCd"  	, trustCustCd);			
			modelIns.put("transCustCd"  	, transCustCd);		// 5
			
			modelIns.put("transCustTel" 	, transCustTel);
			modelIns.put("transReqDt"   	, transReqDt);
			modelIns.put("custCd"       	, custCd);
			modelIns.put("ordQty"       	, ordQty);      			
			modelIns.put("uomCd"        	, uomCd);			//10
			
			modelIns.put("sdeptCd"      	, sdeptCd);
			modelIns.put("salePerCd"    	, salePerCd);
			modelIns.put("carCd"        	, carCd);
			modelIns.put("drvNm"        	, drvNm);       		
			modelIns.put("dlvSeq"       	, dlvSeq);			//15
					
			modelIns.put("drvTel"       	, drvTel);
			modelIns.put("custLotNo"    	, custLotNo);
			modelIns.put("blNo"         	, blNo);
			modelIns.put("recDt"        	, recDt);       
			modelIns.put("whCd"      		, inWhCd);			//20
			
			modelIns.put("makeDt"       	, makeDt);
			modelIns.put("timePeriodDay"	, timePeriodDay);
			modelIns.put("workYn"       	, workYn);                
			modelIns.put("rjType"       	, rjType);
			modelIns.put("locYn"        	, locYn);       //25
			
			modelIns.put("confYn"       	, confYn);     
			modelIns.put("eaCapa"       	, eaCapa);
			modelIns.put("inOrdWeight"  	, inOrdWeight); //28
			
			modelIns.put("itemCd"           , itemCd);
			modelIns.put("itemNm"           , itemNm);			
			modelIns.put("transCustNm"      , transCustNm);
			modelIns.put("transCustAddr"    , transCustAddr);
			modelIns.put("transEmpNm"       , transEmpNm);
			
			modelIns.put("remark"           , remark);
			modelIns.put("transZipNo"       , transZipNo);			
			modelIns.put("etc2"             , etc2);
			modelIns.put("unitAmt"          , unitAmt);
			modelIns.put("transBizNo"       , transBizNo);
			
			modelIns.put("inCustAddr"       , inCustAddr);
			modelIns.put("inCustCd"         , inCustCd);			
			modelIns.put("inCustNm"         , inCustNm);                 
			modelIns.put("inCustTel"        , inCustTel);
			modelIns.put("inCustEmpNm"      , inCustEmpNm);
			
			modelIns.put("expiryDate"       , expiryDate);
			
			modelIns.put("salesCustNm"      , salesCustNm);			
			modelIns.put("zip"       		, zip);
			modelIns.put("addr"       		, addr);
			modelIns.put("phone1"       	, phone1);
			modelIns.put("etc1"     	 	, etc1);
			
			modelIns.put("unitNo"     	 	, unitNo);			
			modelIns.put("salesCompanyNm"   , salesCompanyNm);
			modelIns.put("locCd"     		, locCd);
			
			modelIns.put("time_date"        , timeDate);
			modelIns.put("time_date_end"    , timeDateEnd);                
			modelIns.put("time_use_end"     , timeUseEnd); 
		  
			modelIns.put("epType"     		, epType);  
			
			modelIns.put("lotType"     		, lotType);  
			modelIns.put("ownerCd"     		, ownerCd);  
			
			modelIns.put("custLegacyItemCd" , custLegacyItemCd);  
			modelIns.put("itemBarcode"     	, itemBarcode);  
			modelIns.put("itemSize"     	, itemSize);  
			modelIns.put("color"     		, color);  		
						
			modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
			modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
			modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
			
			//dao                
			modelIns = (Map<String, Object>)dao030.saveExcelOrder_AS(modelIns);		// WMSOP
//			modelIns = (Map<String, Object>)dao030.saveExcelOrderB2O(modelIns);		// WMSOM
			ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
			
			map.put("MSG", MessageResolver.getMessage("save.success"));
			map.put("MSG_ORA", "");
			map.put("errCnt", 0);          
			
			
		} catch(Exception e){
			if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
			map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
			throw e;
		}
        
        return map;        
    }
    
    
    /**
     * 
     * 대체 Method ID		: saveE1
     * 대체 Method 설명	: 스카아젯 매입주문 입력 (스카이젯 → Winus)
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE1_OM(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

    	try{
		 	int tmpCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
		 	
	 		String[] no             = new String[tmpCnt];      	//no(처리)
			
			String[] outReqDt       = new String[tmpCnt];   
			String[] inReqDt        = new String[tmpCnt];     
			String[] custOrdNo      = new String[tmpCnt];     	//원주문번호(처리)
			String[] custOrdSeq     = new String[tmpCnt];    	//원주문seq(처리)
			String[] trustCustCd    = new String[tmpCnt];     
			
			String[] transCustCd    = new String[tmpCnt];                     
			String[] transCustTel   = new String[tmpCnt];         
			String[] transReqDt     = new String[tmpCnt];     
			String[] custCd         = new String[tmpCnt];      	//화주코드(처리)
			String[] ordQty         = new String[tmpCnt];     	//수량(처리) 
			
			String[] uomCd          = new String[tmpCnt];                
			String[] sdeptCd        = new String[tmpCnt];         
			String[] salePerCd      = new String[tmpCnt];     
			String[] carCd          = new String[tmpCnt];     
			String[] drvNm          = new String[tmpCnt];     
			
			String[] dlvSeq         = new String[tmpCnt];                
			String[] drvTel         = new String[tmpCnt];         
			String[] custLotNo      = new String[tmpCnt];     
			String[] blNo           = new String[tmpCnt];     
			String[] recDt          = new String[tmpCnt];     
			
			String[] outWhCd        = new String[tmpCnt];                
			String[] inWhCd         = new String[tmpCnt];         
			String[] makeDt         = new String[tmpCnt];     
			String[] timePeriodDay  = new String[tmpCnt];     
			String[] workYn         = new String[tmpCnt];     
			
			String[] rjType         = new String[tmpCnt];        //반품사유         
			String[] locYn          = new String[tmpCnt];         
			String[] confYn         = new String[tmpCnt];     
			String[] eaCapa         = new String[tmpCnt];     
			String[] inOrdWeight    = new String[tmpCnt];     
			
			String[] itemCd         = new String[tmpCnt];        //상품코드(처리)        
			String[] itemNm         = new String[tmpCnt];        //상품명 (처리) 
			String[] transCustNm    = new String[tmpCnt];     
			String[] transCustAddr  = new String[tmpCnt];     
			String[] transEmpNm     = new String[tmpCnt];     
			
			String[] remark         = new String[tmpCnt];                
			String[] transZipNo     = new String[tmpCnt];         
			String[] etc2           = new String[tmpCnt];     
			String[] unitAmt        = new String[tmpCnt];     
			String[] transBizNo     = new String[tmpCnt];     
			
			String[] inCustAddr     = new String[tmpCnt];                
			String[] inCustCd       = new String[tmpCnt];         
			String[] inCustNm       = new String[tmpCnt];     
			String[] inCustTel      = new String[tmpCnt];     
			String[] inCustEmpNm    = new String[tmpCnt];     
			
			String[] expiryDate     = new String[tmpCnt];
			String[] salesCustNm    = new String[tmpCnt]; //수령인명(처리)
			String[] zip     		= new String[tmpCnt]; //수령인우편번호(처리)
			String[] addr     		= new String[tmpCnt]; //수령인주소(처리)
			String[] addr2     		= new String[tmpCnt];
			
			String[] phone1    	 	= new String[tmpCnt];//수령인핸드폰번호(처리)			
			String[] etc1    		= new String[tmpCnt];
			String[] unitNo    		= new String[tmpCnt];               			
			String[] timeDate       = new String[tmpCnt];   //상품유효기간     
			String[] timeDateEnd    = new String[tmpCnt];   //상품유효기간만료일
			
			
			String[] timeUseEnd     = new String[tmpCnt];   //소비가한만료일
			String[] phone2			= new String[tmpCnt];   //고객전화번호2
			String[] buyCustNm		= new String[tmpCnt];   //주문자명
            String[] buyPhone1		= new String[tmpCnt];   //주문자전화번호1            
			String[] salesCompanyNm	= new String[tmpCnt];	
			
            String[] ordDegree		= new String[tmpCnt];   //주문등록차수                
            String[] bizCond		= new String[tmpCnt];   //업태
            String[] bizType		= new String[tmpCnt];   //업종
            String[] bizNo			= new String[tmpCnt];   //사업자등록번호
            String[] custType		= new String[tmpCnt];   //화주타입     
            
            String[] dataSenderNm	= new String[tmpCnt];   //쇼핑몰                
            String[] legacyOrgOrdNo	= new String[tmpCnt];   //사방넷주문번호                
            String[] custSeq 		= new String[tmpCnt];  //템플릿구분                                               
            String[] lotType		= new String[tmpCnt];	 // 재고lot속성                
            String[] ownerCd		= new String[tmpCnt];	 // 재고소유자코드
            
            String[] etd			= new String[tmpCnt];	 // ETD
            String[] eta			= new String[tmpCnt];	 // ETA
            String[] exchangerate	= new String[tmpCnt];	 // 환율
            String[] ordWeight		= new String[tmpCnt];	 // 주문중량                
            String[] cntrNo			= new String[tmpCnt];	 // 컨테이너번호
            
            String[] refNo			= new String[tmpCnt];	 // 참조번호
            String[] itemBarcode	= new String[tmpCnt];	 // 상품바코드 (다중바코드 혹은 소분상품바코드)
            String[] fromTimeZone	= new String[tmpCnt];	 // 입항일자
            String[] ordSubType		= new String[tmpCnt];	 // 주문상세타입                
            String[] ccCd			= new String[tmpCnt];	 // 화물관리번호
            
            String[] um				= new String[tmpCnt];	 // 단가
            String[] cntrType		= new String[tmpCnt];	 // 컨테이너구분
            String[] locCd			= new String[tmpCnt];	 // 로케이션
           
			
			for(int i = 0 ; i < tmpCnt ; i ++){
			 	String NO              = Integer.toString(i+1);//no(처리)
			 	
				String OUT_REQ_DT      = "";
				String IN_REQ_DT       = "";  //sysdate
				String CUST_ORD_NO     = (String)model.get("I_CUST_ORD_NO" + i); 
				String CUST_ORD_SEQ    = (String)model.get("I_CUST_ORD_SEQ" + i);	//원주문seq(처리)
				String TRUST_CUST_CD   = "";
				
				String TRANS_CUST_CD   = (String)model.get("I_TRANS_CUST_CD" + i);	//
				String TRANS_CUST_TEL  = "";
				String TRANS_REQ_DT    = "";
				String CUST_CD         = (String)model.get("I_CUST_CD" + i);//화주코드(처리)
				String ORD_QTY         = (String)model.get("I_ORD_QTY" + i);//출고수량(처리)
				
				String UOM_CD          = (String)model.get("I_UOM_CD" + i);
				String SDEPT_CD        = "";
				String SALE_PER_CD     = "";
				String CAR_CD          = "";
				String DRV_NM          = "";
				
				String DLV_SEQ         = "";
				String DRV_TEL         = "";
				String CUST_LOT_NO     = (String)model.get("I_CUST_LOT_NO" + i);
				String BL_NO           = "";
				String REC_DT          = "";
				
				String OUT_WH_CD       = "";
				String IN_WH_CD        = "";
				String MAKE_DT         = (String)model.get("I_MAKE_DT" + i);//상품코드(처리);
				String TIME_PERIOD_DAY = "";
				String WORK_YN         = "";
				
				String RJ_TYPE         = "";
				String LOC_YN          = "";
				String CONF_YN         = "";
				String EA_CAPA         = "";
				String IN_ORD_WEIGHT   = "";
				
				String ITEM_CD         = (String)model.get("I_ITEM_CD" + i);//상품코드(처리)
				String ITEM_NM         = (String)model.get("I_ITEM_NM" + i);//상품명(처리)
				String TRANS_CUST_NM   = "";
				String TRANS_CUST_ADDR = "";
				String TRANS_EMP_NM    = "";
				
				String REMARK          = (String)model.get("I_REMARK" + i);	//
				String TRANS_ZIP_NO    = "";
				String ETC2            = (String)model.get("I_ETC2" + i);	//
				String UNIT_AMT        = (String)model.get("I_UNIT_AMT" + i);	//
				String TRANS_BIZ_NO    = "";
				
				String IN_CUST_ADDR    = "";
				String IN_CUST_CD      = (String)model.get("I_IN_CUST_CD" + i);
				String IN_CUST_NM      = (String)model.get("I_IN_CUST_NM" + i);;
				String IN_CUST_TEL     = "";
				String IN_CUST_EMP_NM  = "";
				
				String EXPIRY_DATE     = (String)model.get("I_EXPIRY_DATE" + i); // 유효기간 만료일
				String SALES_CUST_NM   = "";		//수령인명(처리)
				String ZIP             = "";		//수령인우편번호(처리)
				String ADDR            = "";		//수령인주소(처리)
				String ADDR2			= "";
				
				String PHONE_1         = "";		//수령인핸드폰번호(처리)								
				String ETC1            = (String)model.get("I_ETC1" + i);
				String UNIT_NO         = "";
				String TIME_DATE       = "";
				String TIME_DATE_END   = "";
				
				String TIME_USE_END     = "";
				String PHONE_2			= "";				
				String BUY_CUST_NM		= "";
    			String BUY_PHONE_1		= "";    			
    			String SALES_COMPANY_NM	= "";
				
    			String ORD_DEGREE		= "";
    			String BIZ_COND			= "";
    			String BIZ_TYPE			= "";
    			String BIZ_NO			= "";
    			String CUST_TYPE		= "";
    			
    			String DATA_SENDER_NM 		= "";
    			String LEGACY_ORG_ORD_NO 	= "";
    			String CUST_SEQ 			= "";        		
    			String LOT_TYPE		    	= "";        			
    			String OWNER_CD		    	= "";
    			
    			String ETD			    = "";
    			String ETA			    = "";
    			String EXCHANGE_RATE    = "";
    			String ORD_WEIGHT	    = "";        			
    			String CNTR_NO		    = "";
    			
    			String REF_NO		    = "";
    			String ITEM_BARCODE		= "";
    			String FROM_TIME_ZONE 	= "";
    			String ORD_SUBTYP		= "";
    			String CC_CD			= "";
    			
    			String UM				= (String)model.get("I_UM" + i);	//  
    			String CNTR_TYPE		= ""; 
    			String LOC_CD			= "";
				
				
			 	//변수에 담음. 
				no[i]               = NO;
				outReqDt[i]         = OUT_REQ_DT.replaceAll("[^\\d]", "");    
				inReqDt[i]          = IN_REQ_DT.replaceAll("[^\\d]", "");    
				custOrdNo[i]        = CUST_ORD_NO;    
				custOrdSeq[i]       = CUST_ORD_SEQ;    
				trustCustCd[i]      = TRUST_CUST_CD;    
				
				transCustCd[i]      = TRANS_CUST_CD;    
				transCustTel[i]     = TRANS_CUST_TEL;    
				transReqDt[i]       = TRANS_REQ_DT;    
				custCd[i]           = CUST_CD;    
				ordQty[i]           = ORD_QTY;    
				
				uomCd[i]            = UOM_CD;    
				sdeptCd[i]          = SDEPT_CD;    
				salePerCd[i]        = SALE_PER_CD;    
				carCd[i]            = CAR_CD;
				drvNm[i]            = DRV_NM;
				
				dlvSeq[i]           = DLV_SEQ;    
				drvTel[i]           = DRV_TEL;    
				custLotNo[i]        = CUST_LOT_NO;    
				blNo[i]             = BL_NO;    
				recDt[i]            = REC_DT;    
				
				outWhCd[i]          = OUT_WH_CD;
				inWhCd[i]           = IN_WH_CD;
				makeDt[i]           = MAKE_DT;
				timePeriodDay[i]    = TIME_PERIOD_DAY;
				workYn[i]           = WORK_YN;
				
				rjType[i]           = RJ_TYPE;    
				locYn[i]            = LOC_YN;    
				confYn[i]           = CONF_YN;    
				eaCapa[i]           = EA_CAPA;    
				inOrdWeight[i]      = IN_ORD_WEIGHT;   
				
				itemCd[i]           = ITEM_CD;    
				itemNm[i]           = ITEM_NM;    
				transCustNm[i]      = TRANS_CUST_NM;    
				transCustAddr[i]    = TRANS_CUST_ADDR;    
				transEmpNm[i]       = TRANS_EMP_NM;    
				
				remark[i]           = REMARK;    
				transZipNo[i]       = TRANS_ZIP_NO;    
				etc2[i]             = ETC2;    
				//unitAmt[i]          = UNIT_AMT.replaceAll("[^\\d]", "");    
				unitAmt[i]          = UNIT_AMT; 
				transBizNo[i]       = TRANS_BIZ_NO;    
				
				inCustAddr[i]       = IN_CUST_ADDR;   
				inCustCd[i]         = IN_CUST_CD;    
				inCustNm[i]         = IN_CUST_NM;    
				inCustTel[i]        = IN_CUST_TEL;    
				inCustEmpNm[i]      = IN_CUST_EMP_NM;    
				
				expiryDate[i]       = EXPIRY_DATE;
				salesCustNm[i]      = SALES_CUST_NM;
				zip[i]       		= ZIP;
				addr[i]       		= ADDR;
				addr2[i]       		= ADDR2;				
				
				phone1[i]       	= PHONE_1;				
				etc1[i]      		= ETC1;
				unitNo[i]      		= UNIT_NO;				
				timeDate[i]         = TIME_DATE;      
                timeDateEnd[i]      = TIME_DATE_END;
                
                timeUseEnd[i]       = TIME_USE_END;                      
                phone2[i]       	= PHONE_2;     
                buyCustNm[i]       	= BUY_CUST_NM;     
                buyPhone1[i]       	= BUY_PHONE_1;
                salesCompanyNm[i]   = SALES_COMPANY_NM;
				
                ordDegree[i]       	= ORD_DEGREE;                    
                bizCond[i]       	= BIZ_COND;                    
                bizType[i]       	= BIZ_TYPE;
                bizNo[i]       		= BIZ_NO;
                custType[i]       	= CUST_TYPE;
				    
                dataSenderNm[i]     = DATA_SENDER_NM;
                legacyOrgOrdNo[i] 	= LEGACY_ORG_ORD_NO;                             
                custSeq[i]			= CUST_SEQ;                    
                lotType[i]          = LOT_TYPE;                    
                ownerCd[i]          = OWNER_CD;          
				
                etd[i]            	= ETD;
                eta[i]            	= ETA;                    
                exchangerate[i]   	= EXCHANGE_RATE;                    
                ordWeight[i]      	= ORD_WEIGHT;                    
                cntrNo[i]         	= CNTR_NO;
                
                refNo[i]          	= REF_NO;                    
                itemBarcode[i]      = ITEM_BARCODE;
                fromTimeZone[i]     = FROM_TIME_ZONE;
                ordSubType[i]       = ORD_SUBTYP;
                ccCd[i]             = CC_CD;
                
                um[i]				= UM;		
                cntrType[i] 		= CNTR_TYPE;
                locCd[i]			= LOC_CD;									
			}
			
			//프로시져에 보낼것들 다담는다
			Map<String, Object> modelIns = new HashMap<String, Object>();
			
			modelIns.put("vrOrdType"		, model.get("I_ORD_TYPE"));
			modelIns.put("vrOrdSubType"		, model.get("I_ORD_SUBTYPE"));
			
			modelIns.put("no"  , no);

			modelIns.put("reqDt"     	, inReqDt);			
			modelIns.put("custOrdNo"    	, custOrdNo);
			modelIns.put("custOrdSeq"   	, custOrdSeq);
			modelIns.put("trustCustCd"  	, trustCustCd);			
			modelIns.put("transCustCd"  	, transCustCd);		// 5
			
			modelIns.put("transCustTel" 	, transCustTel);
			modelIns.put("transReqDt"   	, transReqDt);
			modelIns.put("custCd"       	, custCd);
			modelIns.put("ordQty"       	, ordQty);      			
			modelIns.put("uomCd"        	, uomCd);			//10
			
			modelIns.put("sdeptCd"      	, sdeptCd);
			modelIns.put("salePerCd"    	, salePerCd);
			modelIns.put("carCd"        	, carCd);
			modelIns.put("drvNm"        	, drvNm);       		
			modelIns.put("dlvSeq"       	, dlvSeq);			//15
					
			modelIns.put("drvTel"       	, drvTel);
			modelIns.put("custLotNo"    	, custLotNo);
			modelIns.put("blNo"         	, blNo);
			modelIns.put("recDt"        	, recDt);       
			modelIns.put("whCd"      		, inWhCd);			//20
			
			modelIns.put("makeDt"       	, makeDt);
			modelIns.put("timePeriodDay"	, timePeriodDay);
			modelIns.put("workYn"       	, workYn);                
			modelIns.put("rjType"       	, rjType);
			modelIns.put("locYn"        	, locYn);       //25
			
			modelIns.put("confYn"       	, confYn);     
			modelIns.put("eaCapa"       	, eaCapa);
			modelIns.put("inOrdWeight"  	, inOrdWeight); //28
			
			modelIns.put("itemCd"           , itemCd);
			modelIns.put("itemNm"           , itemNm);			
			modelIns.put("transCustNm"      , transCustNm);
			modelIns.put("transCustAddr"    , transCustAddr);
			modelIns.put("transEmpNm"       , transEmpNm);
			
			modelIns.put("remark"           , remark);
			modelIns.put("transZipNo"       , transZipNo);			
			modelIns.put("etc2"             , etc2);
			modelIns.put("unitAmt"          , unitAmt);
			modelIns.put("transBizNo"       , transBizNo);
			
			modelIns.put("inCustAddr"       , inCustAddr);
			modelIns.put("inCustCd"         , inCustCd);			
			modelIns.put("inCustNm"         , inCustNm);                 
			modelIns.put("inCustTel"        , inCustTel);
			modelIns.put("inCustEmpNm"      , inCustEmpNm);
			
            modelIns.put("expiryDate"       , expiryDate);                
            modelIns.put("salesCustNm"      , salesCustNm);
            modelIns.put("zip"       		, zip);
            modelIns.put("addr"       		, addr);
            modelIns.put("addr2"       		, addr2);     		// 50
            
            modelIns.put("phone1"       	, phone1);                
            modelIns.put("etc1"     	 	, etc1);
            modelIns.put("unitNo"     	 	, unitNo);
            modelIns.put("phone2"			, phone2);  
            modelIns.put("buyCustNm"		, buyCustNm);		// 55
            
            modelIns.put("buyPhone1"		, buyPhone1);                
            modelIns.put("salesCompanyNm"	, salesCompanyNm);
            modelIns.put("ordDegree"		, ordDegree);
            modelIns.put("bizCond"			, bizCond);
            modelIns.put("bizType"			, bizType);			// 60
            
            modelIns.put("bizNo"			, bizNo);                
            modelIns.put("custType"			, custType);                
            modelIns.put("dataSenderNm"		, dataSenderNm);
            modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
            modelIns.put("custSeq"       	, custSeq);			// 65
            
            modelIns.put("lotType"      	, lotType);                
            modelIns.put("ownerCd"      	, ownerCd);
            modelIns.put("etd"       		, etd);
            modelIns.put("eta"       		, eta);
            modelIns.put("exchangerate" 	, exchangerate);	// 70
            
            modelIns.put("ordWeight"    	, ordWeight);                
            modelIns.put("cntrNo"       	, cntrNo);
            modelIns.put("refNo"       		, refNo);
            modelIns.put("itemBarCode"  	, itemBarcode);       	                                                               
            modelIns.put("fromTimeZone"  	, fromTimeZone);	// 75
            
            modelIns.put("ordSubType"  		, ordSubType);
            modelIns.put("ccCd"  		    , ccCd);			
            modelIns.put("um"  		    	, um);			      
       	  	modelIns.put("cntrType"  		, cntrType);			            
       	  	modelIns.put("locCd"  			, locCd);		    // 80        

						
			modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));   
			modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));  
			modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));
			
			//dao               			
			modelIns = (Map<String, Object>)dao030.saveExcelOrderB2O(modelIns);		// WMSOM
			ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
			
			map.put("MSG", MessageResolver.getMessage("save.success"));
			map.put("MSG_ORA", "");
			map.put("errCnt", 0);          
			
			
		} catch(Exception e){
			if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
			map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
			throw e;
		}
        
        return map;        
    }
    
    
	/**
     * 
     * 대체 Method ID		: saveE2
     * 대체 Method 설명	: 스카아젯 매출주문 입력 (스카이젯 → Winus)
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        
        try{
        	
        	int rowCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
        	String vrOrdType = "";
        	String vrOrdSubtype = "";
        	
        	
            // 기본프로시저 
            String[] no                 = new String[rowCnt]; //no(처리)
            String[] reqDt              = new String[rowCnt]; //출고요청일자(처리)
            String[] custOrdNo          = new String[rowCnt]; //원주문번호(처리)
            String[] custOrdSeq         = new String[rowCnt]; //원주문seq(처리)

            String[] trustCustCd        = new String[rowCnt];
            String[] transCustCd        = new String[rowCnt];
            String[] transCustTel       = new String[rowCnt];
            String[] transReqDt         = new String[rowCnt];
            String[] custCd             = new String[rowCnt]; //화주코드(처리)

            String[] ordQty             = new String[rowCnt]; //출고수량(처리) 
            String[] uomCd              = new String[rowCnt];
            String[] sdeptCd            = new String[rowCnt];
            String[] salePerCd          = new String[rowCnt];
            String[] carCd              = new String[rowCnt];

            String[] drvNm              = new String[rowCnt];
            String[] dlvSeq             = new String[rowCnt];
            String[] drvTel             = new String[rowCnt];
            String[] custLotNo          = new String[rowCnt];
            String[] blNo               = new String[rowCnt];

            String[] recDt              = new String[rowCnt];
            String[] whCd               = new String[rowCnt];
            String[] makeDt             = new String[rowCnt];
            String[] timePeriodDay      = new String[rowCnt];
            String[] workYn             = new String[rowCnt];

            String[] rjType             = new String[rowCnt];
            String[] locYn              = new String[rowCnt];
            String[] confYn             = new String[rowCnt];
            String[] eaCapa             = new String[rowCnt];
            String[] inOrdWeight        = new String[rowCnt];

            String[] itemCd             = new String[rowCnt]; //상품코드(처리)
            String[] itemNm             = new String[rowCnt]; //상품명 (처리)
            String[] transCustNm        = new String[rowCnt];
            String[] transCustAddr      = new String[rowCnt];
            String[] transEmpNm         = new String[rowCnt];

            String[] remark             = new String[rowCnt];
            String[] transZipNo         = new String[rowCnt];
            String[] etc2               = new String[rowCnt];
            String[] unitAmt            = new String[rowCnt];
            String[] transBizNo         = new String[rowCnt];

            String[] inCustAddr         = new String[rowCnt];
            String[] inCustCd           = new String[rowCnt];
            String[] inCustNm           = new String[rowCnt];
            String[] inCustTel          = new String[rowCnt];
            String[] inCustEmpNm        = new String[rowCnt];

            String[] expiryDate         = new String[rowCnt];
            String[] salesCustNm        = new String[rowCnt]; //수령인명(처리)
            String[] zip                = new String[rowCnt]; //수령인우편번호(처리)
            String[] addr               = new String[rowCnt]; //수령인주소(처리)
            String[] addr2              = new String[rowCnt];

            String[] phone1             = new String[rowCnt]; //수령인핸드폰번호(처리)
            String[] etc1               = new String[rowCnt]; 
            String[] unitNo             = new String[rowCnt]; // : B2C(처리)
            String[] phone2             = new String[rowCnt]; //수령인 번호2 (처리)
            String[] buyCustNm          = new String[rowCnt]; 

            String[] buyPhone1          = new String[rowCnt]; 
            String[] salesCompanyNm     = new String[rowCnt]; //eQtipTp ???
            String[] ordDegree          = new String[rowCnt]; //등록차수(처리)
            String[] bizCond            = new String[rowCnt]; //업태
            String[] bizType            = new String[rowCnt]; //업종

            String[] bizNo              = new String[rowCnt]; //사업자등록번호
            String[] custType           = new String[rowCnt]; //화주타입
            String[] dataSenderNm       = new String[rowCnt]; //쇼핑몰(처리)
            String[] legacyOrgOrdNo     = new String[rowCnt]; //쇼핑몰주문번호(처리)
            String[] custSeq            = new String[rowCnt];

            String[] ordDesc            = new String[rowCnt]; //ord_desc
            String[] dlvMsg1            = new String[rowCnt]; //배송메세지(처리)
            String[] dlvMsg2            = new String[rowCnt];
            String[] workTypeCd			= new String[rowCnt];
            
//            String[] ordType				= new String[rowCnt];
//            String[] ordSubType			= new String[rowCnt];
            
            String[] MSG_COCD            = new String[rowCnt]; //callback용
            String[] MSG_NAME            = new String[rowCnt]; //callback용
        	 
        	for(int i=0; i < rowCnt; i++){
        		
//        		ordType[i]			   = model.get(String.format("I_ORD_TYPE_%d", i)).toString();
//              ordSubType[i]	   	   = model.get(String.format("I_ORD_SUBTYPE_%d", i)).toString();
        		
        		no[i]              	   = Integer.toString(i+1); 				//no(처리)
        		reqDt[i]      		   = model.get("ordOutDt").toString();		// Winus 출고예정일 		        		
        		custOrdNo[i]      	   = model.get(String.format("I_CUST_ORD_NO_%d", i)).toString();	// 원주문번호
        		custOrdSeq[i]      	   = model.get(String.format("I_CUST_ORD_SEQ_%d", i)).toString();	// 원주문seq

        		trustCustCd[i]     	   = "";
        		transCustCd[i]     	   = model.get(String.format("I_TRANS_CUST_CD_%d", i)).toString();	// 배송 거래처 코드
        		transCustTel[i]        = "";        		
        		transReqDt[i]      	   = "";
        		custCd[i]      		   = model.get(String.format("I_CUST_CD_%d", i)).toString();			// 화주 ID

        		ordQty[i]			   = model.get(String.format("I_ORD_QTY_%d", i)).toString();
        		uomCd[i]               = model.get(String.format("I_UOM_CD_%d", i)).toString();
        		sdeptCd[i]             = "";
        		salePerCd[i]           = "";
        		carCd[i]               = "";

        		drvNm[i]               = "";
        		dlvSeq[i]              = "";
        		drvTel[i]              = "";
        		custLotNo[i]           = model.get(String.format("I_CUST_LOT_NO_%d", i)).toString();
        		blNo[i]                = "";
        		                    
        		recDt[i]               = "";
        		whCd[i]                = "";
        		makeDt[i]              = "";
        		timePeriodDay[i]       = "";
        		workYn[i]              = "";
        		                    
        		rjType[i]              = "";
        		locYn[i]               = "";
        		confYn[i]              = "";
        		eaCapa[i]              = "";
        		inOrdWeight[i]         = "";
        		                    
        		itemCd[i]              = model.get(String.format("I_ITEM_CD_%d", i)).toString();
        		itemNm[i]              = model.get(String.format("I_ITEM_NM_%d", i)).toString();
        		transCustNm[i]         = "";
        		transCustAddr[i]       = "";
        		transEmpNm[i]          = "";
        		                    
        		remark[i]              = model.get(String.format("I_REMARK_%d", i)).toString();
        		transZipNo[i]          = "";
        		etc2[i]                = "";
        		unitAmt[i]             = model.get(String.format("I_UNIT_AMT_%d", i)).toString();
        		transBizNo[i]          = "";
        		                    
        		inCustAddr[i]          = "";
        		inCustCd[i]            = "";
        		inCustNm[i]            = "";
        		inCustTel[i]           = "";
        		inCustEmpNm[i]         = "";
        		                    
        		expiryDate[i]          = model.get(String.format("I_EXPIRY_DATE_%d", i)).toString();	// 유효기간 만료일 (유통기한)
        		salesCustNm[i]         = model.get(String.format("I_SALES_CUST_NM_%d", i)).toString();
        		zip[i]                 = model.get(String.format("I_ZIP_%d", i)).toString();
        		addr[i]                = model.get(String.format("I_ADDR_%d", i)).toString();
        		addr2[i]               = "";
        		                    
        		phone1[i]              = model.get(String.format("I_PHONE_1_%d", i)).toString();
        		etc1[i]                = model.get(String.format("I_ETC1_%d", i)).toString();;
        		unitNo[i]              = "";
        		phone2[i]              = model.get(String.format("I_PHONE_2_%d", i)).toString();
        		buyCustNm[i]           = "";
        		                    
        		buyPhone1[i]           = "";
        		salesCompanyNm[i]      = "ISKYZ";
        		ordDegree[i]           = model.get("ordDegree").toString();
        		bizCond[i]             = "";
        		bizType[i]             = "";
        		                    
        		bizNo[i]               = "";
        		custType[i]            = "";
        		dataSenderNm[i]        = "";
        		legacyOrgOrdNo[i]      = "";
        		custSeq[i]             = model.get(String.format("I_CUST_ORD_SEQ_%d", i)).toString();
        		                    
        		ordDesc[i]             = "";
        		dlvMsg1[i]             = model.get(String.format("I_DLV_MSG1_%d", i)).toString();
        		dlvMsg2[i]             = "";
        		workTypeCd[i]		   = model.get(String.format("I_WORK_TYPE_CD_%d", i)).toString();
        		
        		if(workTypeCd[i].equals("508")){//반품매출
        			vrOrdType = "01";            
        			vrOrdSubtype = "20";   
        		}else{
        			vrOrdType = "02";            
        			vrOrdSubtype = "30";   
        		}
        	}        	
        	
        	// Procedure Parameter Set
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("vrOrdType"		  , vrOrdType);            
            modelIns.put("vrOrdSubtype"		  , vrOrdSubtype);         
            
            modelIns.put("no"                 , no               );//no(처리)                                                  
            modelIns.put("reqDt"              , reqDt            );//출고요청일자(처리)                                        
            modelIns.put("custOrdNo"          , custOrdNo        );//원주문번호(처리)                                          
            modelIns.put("custOrdSeq"         , custOrdSeq       );//원주문seq(처리)                                           
                                                                       
            modelIns.put("trustCustCd"        , trustCustCd      );                                                            
            modelIns.put("transCustCd"        , transCustCd      );                                                            
            modelIns.put("transCustTel"       , transCustTel     );                                                            
            modelIns.put("transReqDt"         , transReqDt       );                                                            
            modelIns.put("custCd"             , custCd           );//화주코드(처리)                                            
                                                                       
            modelIns.put("ordQty"             , ordQty           );//출고수량(처리)                                            
            modelIns.put("uomCd"              , uomCd            );                                                            
            modelIns.put("sdeptCd"            , sdeptCd          );                                                            
            modelIns.put("salePerCd"          , salePerCd        );                                                            
            modelIns.put("carCd"              , carCd            );                                                            
                                                                       
            modelIns.put("drvNm"              , drvNm            );                                                            
            modelIns.put("dlvSeq"             , dlvSeq           );                                                            
            modelIns.put("drvTel"             , drvTel           );                                                            
            modelIns.put("custLotNo"          , custLotNo        );                                                            
            modelIns.put("blNo"               , blNo             );                                                            
                                                                       
            modelIns.put("recDt"              , recDt            );                                                            
            modelIns.put("whCd"               , whCd             );                                                            
            modelIns.put("makeDt"             , makeDt           );                                                            
            modelIns.put("timePeriodDay"      , timePeriodDay    );                                                            
            modelIns.put("workYn"             , workYn           );                                                            
                                                                       
            modelIns.put("rjType"             , rjType           );                                                            
            modelIns.put("locYn"              , locYn            );                                                            
            modelIns.put("confYn"             , confYn           );                                                            
            modelIns.put("eaCapa"             , eaCapa           );                                                            
            modelIns.put("inOrdWeight"        , inOrdWeight      );                                                            
                                                                       
            modelIns.put("itemCd"             , itemCd           );//상품코드(처리)                                            
            modelIns.put("itemNm"             , itemNm           );//상품명(처리)                                              
            modelIns.put("transCustNm"        , transCustNm      );                                                            
            modelIns.put("transCustAddr"      , transCustAddr    );                                                            
            modelIns.put("transEmpNm"         , transEmpNm       );                                                            
                                                                       
            modelIns.put("remark"             , remark           );                                                            
            modelIns.put("transZipNo"         , transZipNo       );                                                            
            modelIns.put("etc2"               , etc2             );                                                            
            modelIns.put("unitAmt"            , unitAmt          );                                                            
            modelIns.put("transBizNo"         , transBizNo       );                                                            
                                                                       
            modelIns.put("inCustAddr"         , inCustAddr       );                                                            
            modelIns.put("inCustCd"           , inCustCd         );                                                            
            modelIns.put("inCustNm"           , inCustNm         );                                                            
            modelIns.put("inCustTel"          , inCustTel        );                                                            
            modelIns.put("inCustEmpNm"        , inCustEmpNm      );                                                            
                                                                       
            modelIns.put("expiryDate"         , expiryDate       );//유통기한 or 유효기간 or 유효기간 만료일                                              
            modelIns.put("salesCustNm"        , salesCustNm      );//수령인명(처리)                                            
            modelIns.put("zip"                , zip              );//수령인우편번호(처리)                                      
            modelIns.put("addr"               , addr             );//수령인주소(처리)                                          
            modelIns.put("addr2"              , addr2            );                                                            
                                                                       
            modelIns.put("phone1"             , phone1           );//수령인핸드폰번호(처리)                                    
            modelIns.put("etc1"               , etc1             );                                                            
            modelIns.put("unitNo"             , unitNo           );//:B2C(처리)                                                
            modelIns.put("phone2"             , phone2           );//수령인번호2(처리)                                         
            modelIns.put("buyCustNm"          , buyCustNm        );                                                            
                                                                       
            modelIns.put("buyPhone1"          , buyPhone1        );                                                            
            modelIns.put("salesCompanyNm"     , salesCompanyNm   );//eQtipTp???                                                
            modelIns.put("ordDegree"          , ordDegree        );//등록차수(처리)                                            
            modelIns.put("bizCond"            , bizCond          );//업태                                                      
            modelIns.put("bizType"            , bizType          );//업종                                                      
                                                                       
            modelIns.put("bizNo"              , bizNo            );//사업자등록번호                                            
            modelIns.put("custType"           , custType         );//화주타입                                                  
            modelIns.put("dataSenderNm"       , dataSenderNm     );//쇼핑몰(처리)                                              
            modelIns.put("legacyOrgOrdNo"     , legacyOrgOrdNo   );//쇼핑몰주문번호(처리)                                      
            modelIns.put("custSeq"            , custSeq          );                                                            
                                                                       
            modelIns.put("ordDesc"            , ordDesc          );//ord_desc                                                  
            modelIns.put("dlvMsg1"            , dlvMsg1          );//배송메세지(처리)                                          
            modelIns.put("dlvMsg2"            , dlvMsg2          );                                                            
            
            modelIns.put("LC_ID"		, model.get("SS_SVC_NO"));            
            modelIns.put("WORK_IP"		, model.get("SS_CLIENT_IP"));
            modelIns.put("USER_NO"		, model.get("SS_USER_NO"));                          

            System.out.println(modelIns);
            
            modelIns = (Map<String, Object>)dao030.saveExcelOrderB2TS(modelIns);            
        	
        	map.put("O_MSG_CODE", modelIns.get("O_MSG_CODE").toString());
        	map.put("O_MSG_NAME", modelIns.get("O_MSG_NAME").toString());
        	
        }
        catch(Exception e){
        	/*
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("save.error") );*/
        	throw e;
        }
        
        
        return map;        
    }

    
    /**
	 * Method ID	: deleteE1
	 * Method 설명	: 인터페이스 주문삭제
	 * @param 
	 * @return
	 */
    public Map<String, Object> deleteE1(Map<String, Object> model) throws Exception {
    	
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> rowData = null;
		JSONParser jsonParser 		= new JSONParser();
    	ArrayList<Object> gridData 	= (ArrayList<Object>) jsonParser.parse(model.get("gridData").toString());
    	
    	String userNo = (String)model.get(ConstantIF.SS_USER_NO);
    	
    	try{
    		
        	for(Object dt : gridData){
        		rowData = new ObjectMapper().convertValue(dt, Map.class);
        		rowData.put("MAS_SEQ", rowData.get("MAS_SEQ"));
        		rowData.put("DET_SEQ", rowData.get("DET_SEQ"));        		
        		rowData.put("USER_NO", userNo);
        		        		
        		// 1. Delete Update Detail
        		dao.deleteDet(rowData);        		
        		
        		Map<String, Object> detInfos = (Map<String, Object>) dao.getDetList(rowData).get(0);
        		
        		// 2. Delete Update Master (When All Detail Deleted)
        		if(Integer.parseInt(detInfos.get("DET_INFO").toString()) == 0){
        			dao.deleteMas(rowData);
        		}       		   		
        	}
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
            map.put("MSG_ORA", "");
            map.put("errCnt", 0);                
            
    	}catch(Exception e){
    		map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
    	}
    	
 
		return map;
    }
}