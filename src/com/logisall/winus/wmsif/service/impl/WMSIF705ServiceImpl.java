package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsif.service.WMSIF705Service;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF705Service")
public class WMSIF705ServiceImpl implements WMSIF705Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF705Dao")
    private WMSIF705Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE2
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE2(model));
        return map;
    }

	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	
    /**
     * 
     * 대체 Method ID	: listE3
     * 대체 Method 설명	: 
     * 작성자			: yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE3(model));
        return map;
    }
}
