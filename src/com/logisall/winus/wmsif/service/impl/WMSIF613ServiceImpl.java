package com.logisall.winus.wmsif.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF613Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF613Service")
public class WMSIF613ServiceImpl implements WMSIF613Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF613Dao")
    private WMSIF613Dao dao;

    @Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 자사몰 주문조회
     * 작성자			: sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE2
     * 대체 Method 설명	: 자사몰 주문조회
     * 작성자			: sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE2(model));
        return map;
    }


    /**
     *  Method ID  :  saveJasaOrderListVX
     *  Method 설명    : 자사몰 주문확정
     *  작성자              : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveJasaOrderListVX(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());

            // === 실제 들어가는 데이터 변수
            String[] no             = new String[tmpCnt]; // 자체SEQ?
            String[] eSapShopMallName = new String[tmpCnt]; // 쇼핌몰명 (SAP연동)
            String[] eStorageCode   = new String[tmpCnt]; // SAP (저장위치)
            String[] eOrgOrdNo      = new String[tmpCnt]; // 원주문번호
            String[] eOrgDetailNo   = new String[tmpCnt]; // 원주문순번
            String[] eItemCd        = new String[tmpCnt]; // 상품코드
            String[] eOrdQty        = new String[tmpCnt]; // 주문수량
            String[] ePostCost      = new String[tmpCnt]; // 수하인 우편번호
            String[] eAddress       = new String[tmpCnt]; // 수하인주소
            String[] eCustNm        = new String[tmpCnt]; // 수하인명
            String[] eTel           = new String[tmpCnt]; // 수하인번호1
            String[] eCel           = new String[tmpCnt]; // 수하인번호2
            String[] eMessage       = new String[tmpCnt]; // 배송메세지
            String[] eOutWhCd       = new String[tmpCnt]; // 출고창고
            String[] eOutReqDt      = new String[tmpCnt]; // 주문일자
            String[] eOrdDegree     = new String[tmpCnt]; // 주문차수
            String[] eCustCd        = new String[tmpCnt]; // 화주코드
            String[] eUnitNo        = new String[tmpCnt]; // UNIT NO ( B2C )
            String[] eQtipTp        = new String[tmpCnt]; // 자사몰

            //== NULL 처리를 위한 데이터 변수
            String[] trustCustCd    = new String[tmpCnt];     
            String[] transCustCd    = new String[tmpCnt];                     
            String[] transCustTel   = new String[tmpCnt];         
            String[] transReqDt     = new String[tmpCnt];     
            
            String[] uomCd          = new String[tmpCnt];                
            String[] sdeptCd        = new String[tmpCnt];         
            String[] salePerCd      = new String[tmpCnt];     
            String[] carCd          = new String[tmpCnt];     
            String[] drvNm          = new String[tmpCnt];     
            
            String[] dlvSeq         = new String[tmpCnt];                
            String[] drvTel         = new String[tmpCnt];         
            String[] custLotNo      = new String[tmpCnt];     
            String[] blNo           = new String[tmpCnt];     
            String[] recDt          = new String[tmpCnt];     
            
            String[] makeDt         = new String[tmpCnt];     
            String[] timePeriodDay  = new String[tmpCnt];     
            String[] workYn         = new String[tmpCnt];     
            
            String[] rjType         = new String[tmpCnt];                
            String[] locYn          = new String[tmpCnt];         
            String[] confYn         = new String[tmpCnt];     
            String[] eaCapa         = new String[tmpCnt];     
            String[] inOrdWeight    = new String[tmpCnt];     
            
            String[] itemNm         = new String[tmpCnt];         
            String[] transCustNm    = new String[tmpCnt];     
            String[] transCustAddr  = new String[tmpCnt];     
            String[] transEmpNm     = new String[tmpCnt];     
            
            String[] remark         = new String[tmpCnt];                
            String[] transZipNo     = new String[tmpCnt];         
            String[] unitAmt        = new String[tmpCnt];     
            String[] transBizNo     = new String[tmpCnt];     
            
            String[] inCustAddr     = new String[tmpCnt];                
            String[] inCustCd       = new String[tmpCnt];         
            String[] inCustNm       = new String[tmpCnt];     
            String[] inCustTel      = new String[tmpCnt];     
            String[] inCustEmpNm    = new String[tmpCnt];     
            
            String[] expiryDate     = new String[tmpCnt];
            String[] addr2          = new String[tmpCnt];
            
            String[] etc1           = new String[tmpCnt];
            String[] timeDate       = new String[tmpCnt];   //상품유효기간     
            String[] timeDateEnd    = new String[tmpCnt];   //상품유효기간만료일
            String[] timeUseEnd     = new String[tmpCnt];   //소비가한만료일
            
            String[] bizCond        = new String[tmpCnt];   //업태
            String[] bizType        = new String[tmpCnt];   //업종
            String[] bizNo          = new String[tmpCnt];   //사업자등록번호
            String[] custType       = new String[tmpCnt];   //화주타입
            
            String[] legacyOrgOrdNo     = new String[tmpCnt];  
            String[] custSeq            =  new String[tmpCnt];  
            String[] ordDesc            =  new String[tmpCnt];   //ord_desc
            String[] dlvMsg2            =  new String[tmpCnt];   //배송메세지2
            String[] locCd            =  new String[tmpCnt];   //로케이션코드
            
            // I_ 로 시작하는 변수는 안담김
            for (int i = 0; i < tmpCnt; i++) {
                 // === NULL 처리를 위한 데이터 변수
                String TRUST_CUST_CD    = "";
                String TRANS_CUST_CD    = "";
                String TRANS_CUST_TEL   = "";
                String TRANS_REQ_DT     = "";
                String UOM_CD           = "";
                String SDEPT_CD         = "";
                String SALE_PER_CD      = "";
                String CAR_CD           = "";
                String DRV_NM           = "";
                String DLV_SEQ          = "";
                String DRV_TEL          = "";
                String CUST_LOT_NO      = "";
                String BL_NO            = "";
                String REC_DT           = "";
                String MAKE_DT          = "";
                String TIME_PERIOD_DAY  = "";
                String WORK_YN          = "";
                String RJ_TYPE          = "";
                String LOC_YN           = "";
                String CONF_YN          = "";
                String EA_CAPA          = "";
                String IN_ORD_WEIGHT    = "";
                String ITEM_NM          = "";
                String TRANS_CUST_NM    = "";
                String TRANS_CUST_ADDR  = "";
                String TRANS_EMP_NM     = "";
                String REMARK           = "";
                String TRANS_ZIP_NO     = "";
                String UNIT_AMT         = "";
                String TRANS_BIZ_NO     = "";
                String IN_CUST_ADDR     = "";
                String IN_CUST_CD       = "";
                String IN_CUST_NM       = "";
                String IN_CUST_TEL      = "";
                String IN_CUST_EMP_NM   = "";
                String EXPIRY_DATE      = "";
                String ADDR2            = "";
                String ETC1             = "";
                String TIME_DATE        = "";
                String TIME_DATE_END    = "";
                String TIME_USE_END     = "";
                String BIZ_COND         = "";
                String BIZ_TYPE         = "";
                String BIZ_NO           = "";
                String CUST_TYPE        = "";
                String LEGACY_ORG_ORD_NO ="";
                String CUST_SEQ         = "";
                String ORD_DESC = "";
                String DLV_MSG2 = "";
                String LOC_CD = "";
                
                //==== 실제 들어가는 데이터
                String NO = Integer.toString(i+1);
                no[i]                   = NO;
                eSapShopMallName[i]     = HtmlUtils.htmlUnescape((String)model.get("SAP_SHOP_MALL_NAME_" + i)); // 특수문자 Unescape 변환처리
                eStorageCode[i]         = (String)model.get("STORAGE_CODE_" + i);
                eOrgOrdNo[i]            = (String)model.get("ORG_ORD_NO_" + i);
                eOrgDetailNo[i]         = (String)model.get("ORG_DETAIL_NO" + i);
                eItemCd[i]              = (String)model.get("ITEM_CD_" + i);
                eOrdQty[i]              = (String)model.get("ORD_QTY_" + i);
                ePostCost[i]            = StringUtils.isEmpty((String)model.get("D_POST_COST_" + i))
                        ? (String)model.get("D_POST_COST_" + i)
                        : ((String)model.get("D_POST_COST_" + i)).replaceAll("-", "");
                eAddress[i]             = (String)model.get("D_ADDRESS_" + i);
                eCustNm[i]              = (String)model.get("D_CUST_NM_" + i);
                eTel[i]                 = (String)model.get("D_TEL_" + i);
                eCel[i]                 = (String)model.get("D_CEL_" + i);
                eMessage[i]             = (String)model.get("MESSAGE_" + i);
                eOutWhCd[i]             = (String)model.get("OUT_WH_CD_" + i);

                eOrdDegree[i]           = (String)model.get("colORD_DEGREE");
                eOutReqDt[i]            = (String)model.get("colORD_DT");
                eUnitNo[i]              = (String)model.get("colUNIT_NO");
                eCustCd[i]              = (String)model.get("colCUST_CD");
                eQtipTp[i]              = (String)model.get("colQTIO_TP");
                
                //==== NULL 처리 데이터
                trustCustCd[i]      = TRUST_CUST_CD;    
                transCustCd[i]      = TRANS_CUST_CD;    
                transCustTel[i]     = TRANS_CUST_TEL;    
                transReqDt[i]       = TRANS_REQ_DT;    
                
                uomCd[i]            = UOM_CD;    
                sdeptCd[i]          = SDEPT_CD;    
                salePerCd[i]        = SALE_PER_CD;    
                carCd[i]            = CAR_CD;
                drvNm[i]            = DRV_NM;
                
                dlvSeq[i]           = DLV_SEQ;    
                drvTel[i]           = DRV_TEL;    
                custLotNo[i]        = CUST_LOT_NO;    
                blNo[i]             = BL_NO;    
                recDt[i]            = REC_DT;    
                
                makeDt[i]           = MAKE_DT;
                timePeriodDay[i]    = TIME_PERIOD_DAY;
                workYn[i]           = WORK_YN;
                
                rjType[i]           = RJ_TYPE;    
                locYn[i]            = LOC_YN;    
                confYn[i]           = CONF_YN;    
                eaCapa[i]           = EA_CAPA;    
                inOrdWeight[i]      = IN_ORD_WEIGHT;   
                
                itemNm[i]           = ITEM_NM;    
                transCustNm[i]      = TRANS_CUST_NM;    
                transCustAddr[i]    = TRANS_CUST_ADDR;    
                transEmpNm[i]       = TRANS_EMP_NM;    

                remark[i]           = REMARK;    
                transZipNo[i]       = TRANS_ZIP_NO;    
                unitAmt[i]          = UNIT_AMT;    
                transBizNo[i]       = TRANS_BIZ_NO;    
                
                inCustAddr[i]       = IN_CUST_ADDR;   
                inCustCd[i]         = IN_CUST_CD;    
                inCustNm[i]         = IN_CUST_NM;    
                inCustTel[i]        = IN_CUST_TEL;    
                inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                
                expiryDate[i]       = EXPIRY_DATE;
                addr2[i]            = ADDR2;
                
                etc1[i]             = ETC1;
                timeDate[i]         = TIME_DATE;      
                timeDateEnd[i]      = TIME_DATE_END;      
                timeUseEnd[i]       = TIME_USE_END;  
                
                bizCond[i]          = BIZ_COND;
                bizType[i]          = BIZ_TYPE;
                bizNo[i]            = BIZ_NO;
                custType[i]         = CUST_TYPE;
                
                legacyOrgOrdNo[i]   = LEGACY_ORG_ORD_NO;
                custSeq[i]          = CUST_SEQ;
                ordDesc[i]          = ORD_DESC;
                dlvMsg2[i]          = DLV_MSG2;
                locCd[i]          = LOC_CD;
                
            }
          
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("vrOrdType"        , (String)model.get("colORD_TYPE")); // 주문타입 
            modelIns.put("no"               , no);
            modelIns.put("reqDt"            , eOutReqDt);
            modelIns.put("whCd"             , eOutWhCd);

            modelIns.put("custOrdNo"        , eOrgOrdNo); 
            modelIns.put("custOrdSeq"       , eOrgDetailNo);
            modelIns.put("trustCustCd"      , trustCustCd); 
            modelIns.put("transCustCd"      , transCustCd);
            modelIns.put("transCustTel"     , transCustTel);
            modelIns.put("transReqDt"       , transReqDt);

            modelIns.put("custCd"           , eCustCd);
            modelIns.put("ordQty"           , eOrdQty);
            modelIns.put("uomCd"            , uomCd);
            modelIns.put("sdeptCd"          , sdeptCd);
            modelIns.put("salePerCd"        , salePerCd);

            modelIns.put("carCd"            , carCd);
            modelIns.put("drvNm"            , drvNm);       
            modelIns.put("dlvSeq"           , dlvSeq);
            modelIns.put("drvTel"           , drvTel);
            modelIns.put("custLotNo"        , custLotNo);

            modelIns.put("blNo"             , blNo);
            modelIns.put("recDt"            , recDt);      
            modelIns.put("makeDt"           , makeDt);
            modelIns.put("timePeriodDay"    , timePeriodDay);
            modelIns.put("workYn"           , workYn);

            modelIns.put("rjType"           , rjType);
            modelIns.put("locYn"            , locYn);    
            modelIns.put("confYn"           , confYn);     
            modelIns.put("eaCapa"           , eaCapa);
            modelIns.put("inOrdWeight"      , inOrdWeight); 

            modelIns.put("itemCd"           , eItemCd);
            modelIns.put("itemNm"           , itemNm);
            modelIns.put("transCustNm"      , transCustNm);
            modelIns.put("transCustAddr"    , transCustAddr);
            modelIns.put("transEmpNm"       , transEmpNm);

            modelIns.put("remark"           , remark);
            modelIns.put("transZipNo"       , transZipNo);
            modelIns.put("etc2"             , eStorageCode);
            modelIns.put("unitAmt"          , unitAmt);
            modelIns.put("transBizNo"       , transBizNo);

            modelIns.put("inCustAddr"       , inCustAddr);
            modelIns.put("inCustCd"         , inCustCd);
            modelIns.put("inCustNm"         , inCustNm);                 
            modelIns.put("inCustTel"        , inCustTel);
            modelIns.put("inCustEmpNm"      , inCustEmpNm);

            modelIns.put("expiryDate"       , expiryDate);
            modelIns.put("salesCustNm"      , eCustNm);
            modelIns.put("zip"              , ePostCost);
            modelIns.put("addr"             , eAddress);
            modelIns.put("addr2"            , addr2);

            modelIns.put("phone1"           , eTel);
            modelIns.put("etc1"             , etc1);
            modelIns.put("unitNo"           , eUnitNo);
            modelIns.put("phone2"           , eCel);
            modelIns.put("buyCustNm"        , eCustNm);  

            modelIns.put("buyPhone1"        , eTel);
            modelIns.put("salesCompanyNm"   , eQtipTp);
            modelIns.put("ordDegree"        , eOrdDegree);
            modelIns.put("bizCond"          , bizCond);
            modelIns.put("bizType"          , bizType);

            modelIns.put("bizNo"            , bizNo);
            modelIns.put("custType"         , custType);
            modelIns.put("dataSenderNm"     , eSapShopMallName);
            modelIns.put("legacyOrgOrdNo"   , legacyOrgOrdNo);
            modelIns.put("custSeq"          , custSeq);
            
            modelIns.put("ordDesc"          , ordDesc);
            modelIns.put("dlvMsg1"          , eMessage);  // 배송메세지 etc1-> dlvMsg1 로 변경 
            modelIns.put("dlvMsg2"          , dlvMsg2);
            modelIns.put("locCd"          , locCd);
            
            modelIns.put("LC_ID",    (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("WORK_IP",  (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO",  (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao030.saveExcelOrderB2T(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            // CALLBACK 등록여부
            if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
                 Map<String, Object> modelCallback = new HashMap<String, Object>();
                 modelCallback.put("ORG_ORD_NO"     , eOrgOrdNo);
                 modelCallback.put("ORG_DETAIL_NO"  , eOrgDetailNo);
                 modelCallback.put("WORK_IP"        , (String)model.get(ConstantIF.SS_CLIENT_IP));
                 modelCallback.put("USER_NO"        , (String)model.get(ConstantIF.SS_USER_NO));
                 dao.saveJasamallCallback(modelCallback);
            }
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG_ORA", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : jasamallDelete
     * Method 설명 : 자사몰 주문/실적 Delete
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> jasamallDelete(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            int errCnt = 0;
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());

            String[] orgOrdNo       = new String[tmpCnt];     
            String[] orgDetailNo    = new String[tmpCnt];       
            
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                orgOrdNo[i]     = (String) model.get("ORG_ORD_NO_" + i);
                orgDetailNo[i]  = (String) model.get("ORG_DETAIL_NO_" + i);
            }
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("SP_TYPE"          ,(String) model.get("SP_TYPE"));  
            modelIns.put("CUST_ID"          ,(String) model.get("CUST_ID"));  
            modelIns.put("ORG_ORD_NO"       ,orgOrdNo);  
            modelIns.put("ORG_DETAIL_NO"    ,orgDetailNo);

            modelIns.put("LC_ID"            ,(String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("WORK_IP"          ,(String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO"          ,(String)model.get(ConstantIF.SS_USER_NO));
            modelIns = (Map<String, Object>)dao.jasamallDelete(modelIns);
            ServiceUtil.isValidReturnCode("JASAMALL", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            map.put("errCnt", errCnt);
            map.put("MSG_ORA", "");
            map.put("MSG", MessageResolver.getMessage("delete.success"));
        } catch (Exception e) {
            map.put("errCnt", "1");
            map.put("MSG_ORA", "");
            map.put("MSG", e.getMessage());
            throw e;
        }
        return map;
    }
}
