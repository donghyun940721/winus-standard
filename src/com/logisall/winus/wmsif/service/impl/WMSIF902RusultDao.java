package com.logisall.winus.wmsif.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
@SuppressWarnings("unchecked")
public class WMSIF902RusultDao extends SqlMapAbstractDAO {
	private final Log log = LogFactory.getLog(this.getClass());
	// WINUSUSR
	@Autowired
	private SqlMapClient sqlMapClient;

	// WINUSUSR_IF
	@Autowired
	private SqlMapClient sqlMapClientWinusIf;

	public List<Object> selectOutOrderResultList(Map<String, Object> model) {
		return executeQueryForList("wmsif902R.selectOutOrderResultList", model);
	}
	
	public String selectOutOrderResultKey(Map<String, Object> model) {
		return (String) executeQueryForObject("wmsif902R.selectOutOrderResultKey", model);
	}
	
	public void insertOutOrderResult(Map<String, Object> model) throws SQLException {
		sqlMapClientWinusIf.insert("wmsif902R.insertOutOrderResult", model);
	}

	public void updateItfComYn(Map<String, Object> model) {
		executeUpdate("wmsif902.updateItfComYn", model);
	}


	public List<Object> selectOutOrderSendList(Map<String, Object> model) throws SQLException {
		return sqlMapClientWinusIf.queryForList("wmsif902R.selectOutOrderSendList", model);
	}

}
