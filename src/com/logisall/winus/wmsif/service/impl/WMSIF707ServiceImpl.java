package com.logisall.winus.wmsif.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.wmsif.service.WMSIF707Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF707Service")
public class WMSIF707ServiceImpl implements WMSIF707Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF707Dao")
    private WMSIF707Dao dao;
    
	@Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
	
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) { 
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }        
        
        String csState = model.get("vrSrchCSState").toString();			// [검색조건] CS상태
        String holdState = model.get("vrSrchHoldState").toString();		// [검색조건] 보류상태
  
        model.put("vrSrchCSState"	, StringUtils.equals(csState, "A") ? csState : csState.split(","));
        model.put("vrSrchHoldState"	, StringUtils.equals(holdState, "A") ? holdState : holdState.split(","));
        
        
        map.put("LIST", dao.list(model));
        return map;
    }    
    
    
    /**
     * 
     * 대체 Method ID	: listE2
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) { 
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listE2(model));
        return map;
    }   
        
	
	/**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> syncOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        try{
        	
        	int rowCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
        	
            // 기본프로시저 
            String[] no                 = new String[rowCnt]; //no(처리)
            String[] reqDt              = new String[rowCnt]; //출고요청일자(처리)
            String[] custOrdNo          = new String[rowCnt]; //원주문번호(처리)
            String[] custOrdSeq         = new String[rowCnt]; //원주문seq(처리)

            String[] trustCustCd        = new String[rowCnt];
            String[] transCustCd        = new String[rowCnt];
            String[] transCustTel       = new String[rowCnt];
            String[] transReqDt         = new String[rowCnt];
            String[] custCd             = new String[rowCnt]; //화주코드(처리)

            String[] ordQty             = new String[rowCnt]; //출고수량(처리) 
            String[] uomCd              = new String[rowCnt];
            String[] sdeptCd            = new String[rowCnt];
            String[] salePerCd          = new String[rowCnt];
            String[] carCd              = new String[rowCnt];

            String[] drvNm              = new String[rowCnt];
            String[] dlvSeq             = new String[rowCnt];
            String[] drvTel             = new String[rowCnt];
            String[] custLotNo          = new String[rowCnt];
            String[] blNo               = new String[rowCnt];

            String[] recDt              = new String[rowCnt];
            String[] whCd               = new String[rowCnt];
            String[] makeDt             = new String[rowCnt];
            String[] timePeriodDay      = new String[rowCnt];
            String[] workYn             = new String[rowCnt];

            String[] rjType             = new String[rowCnt];
            String[] locYn              = new String[rowCnt];
            String[] confYn             = new String[rowCnt];
            String[] eaCapa             = new String[rowCnt];
            String[] inOrdWeight        = new String[rowCnt];

            String[] itemCd             = new String[rowCnt]; //상품코드(처리)
            String[] itemNm             = new String[rowCnt]; //상품명 (처리)
            String[] transCustNm        = new String[rowCnt];
            String[] transCustAddr      = new String[rowCnt];
            String[] transEmpNm         = new String[rowCnt];

            String[] remark             = new String[rowCnt];
            String[] transZipNo         = new String[rowCnt];
            String[] etc2               = new String[rowCnt];
            String[] unitAmt            = new String[rowCnt];
            String[] transBizNo         = new String[rowCnt];

            String[] inCustAddr         = new String[rowCnt];
            String[] inCustCd           = new String[rowCnt];
            String[] inCustNm           = new String[rowCnt];
            String[] inCustTel          = new String[rowCnt];
            String[] inCustEmpNm        = new String[rowCnt];

            String[] expiryDate         = new String[rowCnt];
            String[] salesCustNm        = new String[rowCnt]; //수령인명(처리)
            String[] zip                = new String[rowCnt]; //수령인우편번호(처리)
            String[] addr               = new String[rowCnt]; //수령인주소(처리)
            String[] addr2              = new String[rowCnt];

            String[] phone1             = new String[rowCnt]; //수령인핸드폰번호(처리)
            String[] etc1               = new String[rowCnt]; 
            String[] unitNo             = new String[rowCnt]; // : B2C(처리)
            String[] phone2             = new String[rowCnt]; //수령인 번호2 (처리)
            String[] buyCustNm          = new String[rowCnt]; 

            String[] buyPhone1          = new String[rowCnt]; 
            String[] salesCompanyNm     = new String[rowCnt]; //eQtipTp ???
            String[] ordDegree          = new String[rowCnt]; //등록차수(처리)
            String[] bizCond            = new String[rowCnt]; //업태
            String[] bizType            = new String[rowCnt]; //업종

            String[] bizNo              = new String[rowCnt]; //사업자등록번호
            String[] custType           = new String[rowCnt]; //화주타입
            String[] dataSenderNm       = new String[rowCnt]; //쇼핑몰(처리)
            String[] legacyOrgOrdNo     = new String[rowCnt]; //쇼핑몰주문번호(처리)
            String[] custSeq            = new String[rowCnt];

            String[] ordDesc            = new String[rowCnt]; //ord_desc
            String[] dlvMsg1            = new String[rowCnt]; //배송메세지(처리)
            String[] dlvMsg2            = new String[rowCnt];

//            String[] ordType			= new String[rowCnt];
//            String[] ordSubType			= new String[rowCnt];
            
            String[] MSG_COCD            = new String[rowCnt]; //callback용
            String[] MSG_NAME            = new String[rowCnt]; //callback용
        	        	
        	for(int i=0; i < rowCnt; i++){
        		
//        		ordType[i]			   = model.get(String.format("I_ORD_TYPE_%d", i)).toString();
//                ordSubType[i]	   	   = model.get(String.format("I_ORD_SUBTYPE_%d", i)).toString();
        		
        		no[i]              	   = Integer.toString(i+1); //no(처리)
//        		reqDt[i]      		   = model.get(String.format("I_ORDER_DATE_%d", i)).toString();		// Winus 출고예정일
        		reqDt[i]      		   = model.get("ordOutDt").toString();		// Winus 출고예정일
//        		custOrdNo[i]      	   = model.get(String.format("I_ORDER_ID_%d", i)).toString();		// 원주문번호
//        		custOrdSeq[i]      	   = model.get(String.format("I_PRD_SEQ_%d", i)).toString();		// 원주문seq
        		
        		custOrdNo[i]      	   = StringUtils.equals(model.get(String.format("I_PACK_%d", i)).toString(), "0")
        										? model.get(String.format("I_SEQ_%d", i)).toString()
        										: model.get(String.format("I_PACK_%d", i)).toString();	// 원주문번호
        		custOrdSeq[i]      	   = model.get(String.format("I_DET_SEQ_%d", i)).toString();		// 원주문seq

        		trustCustCd[i]     	   = "";
        		transCustCd[i]     	   = "";
        		transCustTel[i]        = "";        		
        		transReqDt[i]      	   = "";
        		custCd[i]      		   = model.get("vrSrchCustCdE1").toString();

        		ordQty[i]			   = model.get(String.format("I_DET_QTY_%d", i)).toString();
        		uomCd[i]               = "";
        		sdeptCd[i]             = "";
        		salePerCd[i]           = "";
        		carCd[i]               = "";

        		drvNm[i]               = "";
        		dlvSeq[i]              = "";
        		drvTel[i]              = "";
        		custLotNo[i]           = "";
        		blNo[i]                = "";
        		                    
        		recDt[i]               = "";
        		whCd[i]                = "";
        		makeDt[i]              = "";
        		timePeriodDay[i]       = "";
        		workYn[i]              = "";
        		                    
        		rjType[i]              = "";
        		locYn[i]               = "";
        		confYn[i]              = "";
        		eaCapa[i]              = "";
        		inOrdWeight[i]         = "";
        		                    
        		itemCd[i]              = model.get(String.format("I_PRODUCT_ID_%d", i)).toString();
        		itemNm[i]              = model.get(String.format("I_ITEM_NAME_%d", i)).toString();
        		transCustNm[i]         = "";
        		transCustAddr[i]       = "";
        		transEmpNm[i]          = "";
        		                    
        		remark[i]              = "";
        		transZipNo[i]          = "";
        		etc2[i]                = "";
        		unitAmt[i]             = "";
        		transBizNo[i]          = "";
        		                    
        		inCustAddr[i]          = "";
        		inCustCd[i]            = "";
        		inCustNm[i]            = "";
        		inCustTel[i]           = "";
        		inCustEmpNm[i]         = "";
        		                    
        		expiryDate[i]          = "";
        		salesCustNm[i]         = model.get(String.format("I_RECV_NAME_%d", i)).toString();
        		zip[i]                 = model.get(String.format("I_RECV_ZIP_%d", i)).toString();
        		addr[i]                = model.get(String.format("I_RECV_ADDRESS_%d", i)).toString();
        		addr2[i]               = "";
        		                    
//        		phone1[i]              = model.get(String.format("I_RECV_MOBILE_%d", i)).toString().replaceAll("\\)", "");
        		phone1[i]              = model.get(String.format("I_RECV_MOBILE_%d", i)).toString();
        		etc1[i]                = "";
        		unitNo[i]              = "";
//        		phone2[i]              = model.get(String.format("I_RECV_TEL_%d", i)).toString().replaceAll("\\)", "");
        		phone2[i]              = model.get(String.format("I_RECV_TEL_%d", i)).toString();
        		buyCustNm[i]           = "";
        		                    
        		buyPhone1[i]           = "";
        		salesCompanyNm[i]      = "EZADMIN";
        		ordDegree[i]           = model.get("ordDegree").toString();
        		bizCond[i]             = "";
        		bizType[i]             = "";
        		                    
        		bizNo[i]               = "";
        		custType[i]            = "";
        		dataSenderNm[i]        = "";
        		legacyOrgOrdNo[i]      = model.get(String.format("I_ORDER_ID_%d", i)).toString();
        		custSeq[i]             = model.get(String.format("I_DET_SEQ_%d", i)).toString();
        		                    
        		ordDesc[i]             = "";
        		dlvMsg1[i]             = model.get(String.format("I_MEMO_%d", i)).toString();
        		dlvMsg2[i]             = "";             		
        	}        	
        	
        	// Procedure Parameter Set
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("vrOrdType"		  , model.get("I_ORD_TYPE").toString());            
            modelIns.put("vrOrdSubtype"		  , model.get("I_ORD_SUBTYPE").toString());
            
            modelIns.put("no"                 , no               );//no(처리)                                                  
            modelIns.put("reqDt"              , reqDt            );//출고요청일자(처리)                                        
            modelIns.put("custOrdNo"          , custOrdNo        );//원주문번호(처리)                                          
            modelIns.put("custOrdSeq"         , custOrdSeq       );//원주문seq(처리)                                           
                                                                       
            modelIns.put("trustCustCd"        , trustCustCd      );                                                            
            modelIns.put("transCustCd"        , transCustCd      );                                                            
            modelIns.put("transCustTel"       , transCustTel     );                                                            
            modelIns.put("transReqDt"         , transReqDt       );                                                            
            modelIns.put("custCd"             , custCd           );//화주코드(처리)                                            
                                                                       
            modelIns.put("ordQty"             , ordQty           );//출고수량(처리)                                            
            modelIns.put("uomCd"              , uomCd            );                                                            
            modelIns.put("sdeptCd"            , sdeptCd          );                                                            
            modelIns.put("salePerCd"          , salePerCd        );                                                            
            modelIns.put("carCd"              , carCd            );                                                            
                                                                       
            modelIns.put("drvNm"              , drvNm            );                                                            
            modelIns.put("dlvSeq"             , dlvSeq           );                                                            
            modelIns.put("drvTel"             , drvTel           );                                                            
            modelIns.put("custLotNo"          , custLotNo        );                                                            
            modelIns.put("blNo"               , blNo             );                                                            
                                                                       
            modelIns.put("recDt"              , recDt            );                                                            
            modelIns.put("whCd"               , whCd             );                                                            
            modelIns.put("makeDt"             , makeDt           );                                                            
            modelIns.put("timePeriodDay"      , timePeriodDay    );                                                            
            modelIns.put("workYn"             , workYn           );                                                            
                                                                       
            modelIns.put("rjType"             , rjType           );                                                            
            modelIns.put("locYn"              , locYn            );                                                            
            modelIns.put("confYn"             , confYn           );                                                            
            modelIns.put("eaCapa"             , eaCapa           );                                                            
            modelIns.put("inOrdWeight"        , inOrdWeight      );                                                            
                                                                       
            modelIns.put("itemCd"             , itemCd           );//상품코드(처리)                                            
            modelIns.put("itemNm"             , itemNm           );//상품명(처리)                                              
            modelIns.put("transCustNm"        , transCustNm      );                                                            
            modelIns.put("transCustAddr"      , transCustAddr    );                                                            
            modelIns.put("transEmpNm"         , transEmpNm       );                                                            
                                                                       
            modelIns.put("remark"             , remark           );                                                            
            modelIns.put("transZipNo"         , transZipNo       );                                                            
            modelIns.put("etc2"               , etc2             );                                                            
            modelIns.put("unitAmt"            , unitAmt          );                                                            
            modelIns.put("transBizNo"         , transBizNo       );                                                            
                                                                       
            modelIns.put("inCustAddr"         , inCustAddr       );                                                            
            modelIns.put("inCustCd"           , inCustCd         );                                                            
            modelIns.put("inCustNm"           , inCustNm         );                                                            
            modelIns.put("inCustTel"          , inCustTel        );                                                            
            modelIns.put("inCustEmpNm"        , inCustEmpNm      );                                                            
                                                                       
            modelIns.put("expiryDate"         , expiryDate       );                                                            
            modelIns.put("salesCustNm"        , salesCustNm      );//수령인명(처리)                                            
            modelIns.put("zip"                , zip              );//수령인우편번호(처리)                                      
            modelIns.put("addr"               , addr             );//수령인주소(처리)                                          
            modelIns.put("addr2"              , addr2            );                                                            
                                                                       
            modelIns.put("phone1"             , phone1           );//수령인핸드폰번호(처리)                                    
            modelIns.put("etc1"               , etc1             );                                                            
            modelIns.put("unitNo"             , unitNo           );//:B2C(처리)                                                
            modelIns.put("phone2"             , phone2           );//수령인번호2(처리)                                         
            modelIns.put("buyCustNm"          , buyCustNm        );                                                            
                                                                       
            modelIns.put("buyPhone1"          , buyPhone1        );                                                            
            modelIns.put("salesCompanyNm"     , salesCompanyNm   );//eQtipTp???                                                
            modelIns.put("ordDegree"          , ordDegree        );//등록차수(처리)                                            
            modelIns.put("bizCond"            , bizCond          );//업태                                                      
            modelIns.put("bizType"            , bizType          );//업종                                                      
                                                                       
            modelIns.put("bizNo"              , bizNo            );//사업자등록번호                                            
            modelIns.put("custType"           , custType         );//화주타입                                                  
            modelIns.put("dataSenderNm"       , dataSenderNm     );//쇼핑몰(처리)                                              
            modelIns.put("legacyOrgOrdNo"     , legacyOrgOrdNo   );//쇼핑몰주문번호(처리)                                      
            modelIns.put("custSeq"            , custSeq          );                                                            
                                                                       
            modelIns.put("ordDesc"            , ordDesc          );//ord_desc                                                  
            modelIns.put("dlvMsg1"            , dlvMsg1          );//배송메세지(처리)                                          
            modelIns.put("dlvMsg2"            , dlvMsg2          );                                                            
            
            modelIns.put("LC_ID"		, model.get("SS_SVC_NO"));            
            modelIns.put("WORK_IP"		, model.get("SS_CLIENT_IP"));
            modelIns.put("USER_NO"		, model.get("SS_USER_NO"));                          

//            System.out.println(modelIns);

            modelIns = (Map<String, Object>)dao030.saveExcelOrderB2TS(modelIns);
            
//        	modelIns = (Map<String, Object>) dao.syncOrder(modelIns);
        	
        	map.put("O_MSG_CODE", modelIns.get("O_MSG_CODE").toString());
        	map.put("O_MSG_NAME", modelIns.get("O_MSG_NAME").toString());
        	
        }
        catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("save.error") );
        }
        
        
        return map;
    }


    /**
	 * Method ID	: getTransCorpInfo
	 * Method 설명	: 오픈몰 API 접속정보조회 
	 * @param 
	 * @return
	 */
    public Map<String, Object> getTransCorpInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RESULT", dao.getTransCorpInfo(model));
		return map;
	}
    
    
    /**
	 * Method ID	: deleteE1
	 * Method 설명	: 인터페이스 주문삭제
	 * @param 
	 * @return
	 */
    public Map<String, Object> deleteE1(Map<String, Object> model) throws Exception {
    	
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> rowData = null;
		JSONParser jsonParser 		= new JSONParser();
    	ArrayList<Object> gridData 	= (ArrayList<Object>) jsonParser.parse(model.get("gridData").toString());
		
    	
    	try{
    		
        	for(Object dt : gridData){
        		rowData = new ObjectMapper().convertValue(dt, Map.class);
        		rowData.put("MAS_SEQ", rowData.get("MAS_SEQ"));
        		rowData.put("DET_SEQ", rowData.get("DET_SEQ"));    		
        		        		
        		// 1. Delete Update Detail
        		dao.deleteDet(rowData);
        		
        		
        		Map<String, Object> detInfos = (Map<String, Object>) dao.getDetList(rowData).get(0);
        		
        		// 2. Delete Update Master (When All Detail Deleted)
        		if(Integer.parseInt(detInfos.get("DET_INFO").toString()) == 0){
        			dao.deleteMas(rowData);
        		}       		   		
        	}
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
            map.put("MSG_ORA", "");
            map.put("errCnt", 0);                
            
    	}catch(Exception e){
    		map.put("errCnt", 1);
	        map.put("MSG", MessageResolver.getMessage("save.error") );
    	}
    	
 
		return map;
	}

}