package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF715Dao")
public class WMSIF715Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list 
     * Method 설명  : 비플로우 주문관리 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif715.list", model);
    }   
    
    /**
     * Method ID  : list2 
     * Method 설명  : 비플로우 인터페이스 이력 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
        return executeQueryPageWq("wmsif715.list2", model);
    }
    
    /**
     * Method ID  : InvcSendCntSave 
     * Method 설명  : 비플로우 송장송신 내역 저장
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public Object InvcSendCntSave(Map<String, Object> model) {
       return executeInsert("wmsif715.InvcSendCntSave", model);
       
    }  
    
    /**
     * Method ID  : IFOrderDelete 
     * Method 설명  : 비플로우 주문관리 - IF 주문등록 DEL_YN -> Y
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int IFOrderDelete(Map<String, Object> model) {
       return executeUpdate("wmsif715.updateBflowOrdDelYn", model);
       
    }   
    
    /**
     * Method ID  : IFOrderDelete 
     * Method 설명  : 비플로우 주문관리 - IF WMS_REG_YN -> Y (원복)
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int wmsOrdUpdate(Map<String, Object> model) {
       return executeUpdate("wmsif715.wmsOrdUpdate", model);
       
    } 
    
    /**
     * Method ID  : IFOrderDelete 
     * Method 설명  : 비플로우 주문관리 - WMS 출고주문 삭제 && IF WMS_REG_YN -> N (원복)
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int wmsOrdUpdateRe(Map<String, Object> model) {
       return executeUpdate("wmsif715.wmsOrdUpdateRe", model);
       
    } 
   
    /**
     * Method ID  : bflowIfInvcDelete 
     * Method 설명  : 송장정보삭제 시, 비플로우 송장 등록 내역 변경
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public int bflowIfInvcDelete(Map<String, Object> model) {
       return executeUpdate("wmsif715.bflowIfInvcDelete", model);
       
    }
    
    /**
     * Method ID  : getBflowtoken 
     * Method 설명  : 비플로우 토큰 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public String getBflowtoken(Map<String, Object> model) {
    	
       String token = (String)executeView("wmsif715.getBflowtoken", model);
       return token;
       
    } 
    
}


