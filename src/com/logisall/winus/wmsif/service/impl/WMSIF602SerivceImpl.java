package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF602Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF602Service")
public class WMSIF602SerivceImpl extends AbstractServiceImpl implements WMSIF602Service{

	@Resource(name = "WMSIF602Dao")
    private WMSIF602Dao dao;
	
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        String rfidType = (String) model.get("vrRfidType");
        String workDtFrom = ((String) model.get("vrWorkDtFrom")).replace("-", "");
        String workDtTo = ((String) model.get("vrWorkDtTo")).replace("-", "");
        
        model.put("vrSrchReqDtFrom", workDtFrom);
        model.put("vrSrchReqDtTo", workDtTo);
        
        if(rfidType.equals("Y")) {
        	map.put("LIST", dao.listEnter(model));
        }else if(rfidType.equals("N")){
        	map.put("LIST", dao.listEntreturn(model));
        }
        return map;
    }
    
    @Override
    public Map<String, Object> takeDegree(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        String rfidType = (String) model.get("vrRfidTypeE2");
        String vrInOrdDt = ((String) model.get("vrInOrdDtE2")).replace("-", "");
        
        model.put("vrInOrdDt", vrInOrdDt);
        
        if(rfidType.equals("Y")) {
        	map.put("LIST", dao.degreeListEnter(model));
        	map.put("LISTLOC", dao.locListEnter(model));
//        	map.put("UNITNO_LIST", dao.unitnoListEnter(model));
        }else if(rfidType.equals("N")){
        	map.put("LIST", dao.degreeListEntreturn(model));
//        	map.put("UNITNO_LIST", dao.unitnoListEntreturn(model));
        }
        return map;
    }
    
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        String rfidType = (String) model.get("vrRfidTypeE2");
        String vrWorkDt = ((String) model.get("vrWorkDtE2")).replace("-", "");
        String vrInOrdDt = ((String) model.get("vrInOrdDtE2")).replace("-", "");
        
        model.put("vrWorkDt", vrWorkDt);
        model.put("vrInOrdDt", vrInOrdDt);
        
        if(rfidType.equals("Y")) {
        	map.put("LIST", dao.listE2Enter(model));
        }else if(rfidType.equals("N")){
        	map.put("LIST", dao.listE2Entreturn(model));
        }
        return map;
    }
    
    @Override
    public Map<String, Object> orderIns(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("CUST_ID", (String)model.get("modal_vrCustId"));
            modelIns.put("CUST_CD", "");
            modelIns.put("ORD_TYPE", "");
            modelIns.put("ORD_SUBTYPE", "");
            
            modelIns.put("WORK_DEGREE", (String)model.get("modal_workDegree"));
            modelIns.put("DLV_DT", (String)model.get("modal_dlvDt"));
            modelIns.put("LOC_CD", (String)model.get("vrLocIn"));
            
            modelIns.put("RFID_DT", ((String)model.get("modal_ordWorkDt")).replace("-", ""));
            modelIns.put("TRANS_CUST_ID", (String)model.get("modal_transCustCd"));
            modelIns.put("STYLE", (String)model.get("modal_style"));

            //session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            //dao                
            modelIns = (Map<String, Object>)dao.orderIns(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSIF602", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> orderIns_return(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("CUST_ID", (String)model.get("modal_vrCustId"));
            modelIns.put("CUST_CD", "");
            modelIns.put("ORD_TYPE", "");
            modelIns.put("ORD_SUBTYPE", "");
            
            modelIns.put("WORK_DEGREE", (String)model.get("modal_workDegree"));
            modelIns.put("DLV_DT", (String)model.get("modal_dlvDt"));
            modelIns.put("LOC_CD", (String)model.get("vrLocIn"));
            
            modelIns.put("RFID_DT", ((String)model.get("modal_ordWorkDt")).replace("-", ""));
            modelIns.put("TRANS_CUST_ID", (String)model.get("modal_transCustCd"));
            modelIns.put("STYLE", (String)model.get("modal_style"));

            //session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            //dao                
            modelIns = (Map<String, Object>)dao.orderIns_return(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSIF602", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
