package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface WMSIF904Service {
    List<Map<String, Object>> selectMallList(Map<String, Object> model) ;
    Map selectOrderList(Map<String, Object> model) ;
    Map<String, Object> saveOutboundOrders(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    Map selectOrderShipmentList(Map<String, Object> model);
    Map<String, Object> saveCafe24Shipment( HttpServletRequest request, Map<String, Object> reqMap)throws Exception;
	Map<String, Object> deleteOrderAsn(HttpServletRequest request, Map<String, Object> reqMap);
}
