package com.logisall.winus.wmsif.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF780Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF780Service")
public class WMSIF780ServiceImpl implements WMSIF780Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF780Dao")
    private WMSIF780Dao dao;
    
    
    /**
     * 
     * 대체 Method ID		: listE1
     * 대체 Method 설명	: 상품정보 조회 
     * 작성자				: schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

//        String csState = model.get("vrSrchCSState").toString();			// [검색조건] CS상태
//        String holdState = model.get("vrSrchHoldState").toString();		// [검색조건] 보류상태
//  
//        model.put("vrSrchCSState"	, StringUtils.equals(csState, "A") ? csState : csState.split(","));
//        model.put("vrSrchHoldState"	, StringUtils.equals(holdState, "A") ? holdState : holdState.split(","));
        
        map.put("rows", dao.listE1(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID     : listE2
     * 대체 Method 설명 : 입고정보 조회 
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        if(model.get("vrSrchItemGrpCnt") != null && !model.get("vrSrchItemGrpCnt").equals("")){
            int vrSrchItemGrpCnt = Integer.parseInt(model.get("vrSrchItemGrpCnt").toString());
            ArrayList<String> vrSrchItemGrp = new ArrayList<String>();
            
            if (vrSrchItemGrpCnt > 0){
            
                if (vrSrchItemGrpCnt == 1) {
                    vrSrchItemGrp.add(String.valueOf(model.get("vrSrchItemGrpId")));
                }else{
                    vrSrchItemGrp = new ArrayList<String>(Arrays.asList((String[]) model.get("vrSrchItemGrpId")));
                }
                model.put("vrSrchItemGrpList", vrSrchItemGrp);
            }
        }
        
        map.put("rows", dao.listE2(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID     : listE3
     * 대체 Method 설명 : 출고정보 조회 
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("vrSrchItemGrpCnt") != null && !model.get("vrSrchItemGrpCnt").equals("")){
            int vrSrchItemGrpCnt = Integer.parseInt(model.get("vrSrchItemGrpCnt").toString());
            ArrayList<String> vrSrchItemGrp = new ArrayList<String>();
            
            if (vrSrchItemGrpCnt > 0){
            
                if (vrSrchItemGrpCnt == 1) {
                    vrSrchItemGrp.add(String.valueOf(model.get("vrSrchItemGrpId")));
                }else{
                    vrSrchItemGrp = new ArrayList<String>(Arrays.asList((String[]) model.get("vrSrchItemGrpId")));
                }
                model.put("vrSrchItemGrpList", vrSrchItemGrp);
            }
        }

//        String csState = model.get("vrSrchCSState").toString();           // [검색조건] CS상태
//        String holdState = model.get("vrSrchHoldState").toString();       // [검색조건] 보류상태
//  
//        model.put("vrSrchCSState" , StringUtils.equals(csState, "A") ? csState : csState.split(","));
//        model.put("vrSrchHoldState"   , StringUtils.equals(holdState, "A") ? holdState : holdState.split(","));
        
        map.put("rows", dao.listE3(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID     : listE4
     * 대체 Method 설명 : 입고결과 조회 
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        if(model.get("vrSrchItemGrpCnt") != null && !model.get("vrSrchItemGrpCnt").equals("")){
            int vrSrchItemGrpCnt = Integer.parseInt(model.get("vrSrchItemGrpCnt").toString());
            ArrayList<String> vrSrchItemGrp = new ArrayList<String>();
            
            if (vrSrchItemGrpCnt > 0){
            
                if (vrSrchItemGrpCnt == 1) {
                    vrSrchItemGrp.add(String.valueOf(model.get("vrSrchItemGrpId")));
                }else{
                    vrSrchItemGrp = new ArrayList<String>(Arrays.asList((String[]) model.get("vrSrchItemGrpId")));
                }
                model.put("vrSrchItemGrpList", vrSrchItemGrp);
            }
        }
        
        map.put("rows", dao.listE4(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID     : listE5
     * 대체 Method 설명 : 출고결과 조회 
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        if(model.get("vrSrchItemGrpCnt") != null && !model.get("vrSrchItemGrpCnt").equals("")){
            int vrSrchItemGrpCnt = Integer.parseInt(model.get("vrSrchItemGrpCnt").toString());
            ArrayList<String> vrSrchItemGrp = new ArrayList<String>();
            
            if (vrSrchItemGrpCnt > 0){
            
                if (vrSrchItemGrpCnt == 1) {
                    vrSrchItemGrp.add(String.valueOf(model.get("vrSrchItemGrpId")));
                }else{
                    vrSrchItemGrp = new ArrayList<String>(Arrays.asList((String[]) model.get("vrSrchItemGrpId")));
                }
                model.put("vrSrchItemGrpList", vrSrchItemGrp);
            }
        }
        
        map.put("rows", dao.listE5(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID     : syncItem
     * 대체 Method 설명 : 상품정보 동기화
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> syncItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        
        try{
            
            int selectIds = Integer.parseInt(model.get("ROW_COUNT").toString());
            
            String[] itemCode = new String[selectIds];
            String[] itemNm = new String[selectIds];
            String[] itemBarCd = new String[selectIds];
            String[] itemGrpCd = new String[selectIds];
            String[] itemGrpNm = new String[selectIds];
            String[] itemGrp2ndCd = new String[selectIds];
            String[] itemGrp2ndNm = new String[selectIds];
            String[] itemGrp3rdCd = new String[selectIds];
            String[] itemGrp3rdNm = new String[selectIds];
            String[] uomCd = new String[selectIds];
            String[] uomNm = new String[selectIds];
            String[] unitQty = new String[selectIds];
            String[] statGbn = new String[selectIds];
            String[] vatYn = new String[selectIds];
            String[] standards = new String[selectIds];
            String[] vendorCd = new String[selectIds];
            String[] caseQty = new String[selectIds];
            String[] boxQty = new String[selectIds];
            String[] mipTypeCd = new String[selectIds];
            String[] curPrd = new String[selectIds];
            String[] suppPrd = new String[selectIds];
            String[] mipUnitPrc = new String[selectIds];
            String[] vatPrc = new String[selectIds];
            String[] marginRt = new String[selectIds];
            String[] modDate = new String[selectIds];
            String[] ritemId = new String[selectIds];
            
            for(int i = 0; i < selectIds ; i++){
                itemCode[i] = model.get("I_ITEM_CODE_"+i) == null ? "" : (String)model.get("I_ITEM_CODE_"+i).toString();
                itemNm[i] = model.get("I_ITEM_NM_"+i) == null ? "" : (String)model.get("I_ITEM_NM_"+i).toString();
                itemBarCd[i] = model.get("I_ITEM_BAR_CD_"+i) == null ? "" : (String)model.get("I_ITEM_BAR_CD_"+i).toString();
                itemGrpCd[i] = model.get("I_ITEM_GRP_CD_"+i) == null ? "" : (String)model.get("I_ITEM_GRP_CD_"+i).toString(); 
                itemGrpNm[i] = model.get("I_ITEM_GRP_NM_"+i) == null ? "" : (String)model.get("I_ITEM_GRP_NM_"+i).toString(); 
                itemGrp2ndCd[i] = model.get("I_ITEM_GRP_2ND_CD_"+i) == null ? "" : (String)model.get("I_ITEM_GRP_2ND_CD_"+i).toString();
                itemGrp2ndNm[i] = model.get("I_ITEM_GRP_2ND_NM_"+i) == null ? "" : (String)model.get("I_ITEM_GRP_2ND_NM_"+i).toString();
                itemGrp3rdCd[i] = model.get("I_ITEM_GRP_3RD_CD_"+i) == null ? "" : (String)model.get("I_ITEM_GRP_3RD_CD_"+i).toString();
                itemGrp3rdNm[i] = model.get("I_ITEM_GRP_3RD_NM_"+i) == null ? "" : (String)model.get("I_ITEM_GRP_3RD_NM_"+i).toString();
                uomCd[i] = model.get("I_UOM_CD_"+i) == null ? "" : (String)model.get("I_UOM_CD_"+i).toString(); 
                uomNm[i] = model.get("I_UOM_NM_"+i) == null ? "" : (String)model.get("I_UOM_NM_"+i).toString(); 
                unitQty[i] = model.get("I_UNIT_QTY_"+i) == null ? "" : (String)model.get("I_UNIT_QTY_"+i).toString();
                statGbn[i] = model.get("I_STAT_GBN_"+i) == null ? "" : (String)model.get("I_STAT_GBN_"+i).toString();
                vatYn[i] = model.get("I_VAT_YN_"+i) == null ? "" : (String)model.get("I_VAT_YN_"+i).toString(); 
                standards[i] = model.get("I_STANDARDS_"+i) == null ? "" : (String)model.get("I_STANDARDS_"+i).toString(); 
                vendorCd[i] = model.get("I_VENDOR_CD_"+i) == null ? "" : (String)model.get("I_VENDOR_CD_"+i).toString();
                caseQty[i] = model.get("I_CASE_QTY_"+i) == null ? "" : (String)model.get("I_CASE_QTY_"+i).toString();
                boxQty[i] = model.get("I_BOX_QTY_"+i) == null ? "" : (String)model.get("I_BOX_QTY_"+i).toString();
                mipTypeCd[i] = model.get("I_MIP_TYPE_CD_"+i) == null ? "" : (String)model.get("I_MIP_TYPE_CD_"+i).toString(); 
                curPrd[i] = model.get("I_CUR_PRC_"+i) == null ? "" : (String)model.get("I_CUR_PRC_"+i).toString();
                suppPrd[i] = model.get("I_SUPP_PRC_"+i) == null ? "" : (String)model.get("I_SUPP_PRC_"+i).toString();
                mipUnitPrc[i] = model.get("I_MIP_UNIT_PRC_"+i) == null ? "" : (String)model.get("I_MIP_UNIT_PRC_"+i).toString();
                vatPrc[i] = model.get("I_VAT_PRC_"+i) == null ? "" : (String)model.get("I_VAT_PRC_"+i).toString();
                marginRt[i] = model.get("I_MARGIN_RT_"+i) == null ? "" : (String)model.get("I_MARGIN_RT_"+i).toString();
                modDate[i] = model.get("I_MOD_DATE_"+i) == null ? "" : (String)model.get("I_MOD_DATE_"+i).toString();
                ritemId[i] = model.get("I_RITEM_ID_"+i) == null ? "" : (String)model.get("I_RITEM_ID_"+i).toString();
            }
            
            modelIns.put("I_ITEM_CODE", itemCode);
            modelIns.put("I_ITEM_NM", itemNm);
            modelIns.put("I_ITEM_BAR_CD", itemBarCd); 
            modelIns.put("I_ITEM_GRP_CD", itemGrpCd); 
            modelIns.put("I_ITEM_GRP_NM", itemGrpNm); 
            modelIns.put("I_ITEM_GRP_2ND_CD", itemGrp2ndCd);
            modelIns.put("I_ITEM_GRP_2ND_NM", itemGrp2ndNm);
            modelIns.put("I_ITEM_GRP_3RD_CD", itemGrp3rdCd);
            modelIns.put("I_ITEM_GRP_3RD_NM", itemGrp3rdNm);
            modelIns.put("I_UOM_CD", uomCd); 
            modelIns.put("I_UOM_NM", uomNm); 
            modelIns.put("I_UNIT_QTY", unitQty);
            modelIns.put("I_STAT_GBN", statGbn);
            modelIns.put("I_VAT_YN", vatYn); 
            modelIns.put("I_STANDARDS", standards); 
            modelIns.put("I_VENDOR_CD", vendorCd);
            modelIns.put("I_CASE_QTY", caseQty);
            modelIns.put("I_BOX_QTY", boxQty);
            modelIns.put("I_MIP_TYPE_CD", mipTypeCd); 
            modelIns.put("I_CUR_PRC", curPrd);
            modelIns.put("I_SUPP_PRC", suppPrd);
            modelIns.put("I_MIP_UNIT_PRC", mipUnitPrc);
            modelIns.put("I_VAT_PRC", vatPrc);
            modelIns.put("I_MARGIN_RT", marginRt);
            modelIns.put("I_MOD_DATE", modDate);
            modelIns.put("I_RITEM_ID", ritemId);
            
            modelIns.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_CUST_ID", (String)model.get("vrSrchCustIdE1"));
            modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            
            map = (Map<String,Object>)dao.syncItem(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSIF780", String.valueOf(map.get("O_MSG_CODE")), (String)map.get("O_MSG_NAME"));
            
            map.put("errCnt", 0);
            map.put("MSG", MessageResolver.getMessage("save.sucess"));
            
        }catch(BizException be){
            map.put("errCnt", -1);
            map.put("MSG", be.getMessage() );
        }
        catch(Exception e){
            throw e;
        }
        
        return map;
    }
    /**
     * 
     * 대체 Method ID     : syncInOrder
     * 대체 Method 설명 : 입고주문정보 동기화
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> syncInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        
        try{
            
            int selectIds = Integer.parseInt(model.get("ROW_COUNT").toString());
            
            String[] orderDate      = new String[selectIds];
            String[] orderNo        = new String[selectIds];
            String[] orderSeqNo     = new String[selectIds];
            String[] ipgoEptDate    = new String[selectIds];
            String[] gdsCd          = new String[selectIds];
            String[] orderCnt       = new String[selectIds];
            String[] vendorCd       = new String[selectIds];
            String[] baseCd         = new String[selectIds];
            String[] orderGbn       = new String[selectIds];
            String[] orderPrc       = new String[selectIds];
            String[] orderAmt       = new String[selectIds];
            String[] boxGdsCnt      = new String[selectIds];
            String[] note           = new String[selectIds];
            String[] dNote          = new String[selectIds];
            String[] wmsNo          = new String[selectIds];
            String[] ordId          = new String[selectIds];
            String[] ordSeq         = new String[selectIds];
            String[] ritemId        = new String[selectIds];
            
            for(int i = 0; i < selectIds ; i++){
                orderDate[i]      = model.get("I_ORDER_DATE_"   +i) == null ? "" : (String)model.get("I_ORDER_DATE_"   +i).toString();
                orderNo[i]        = model.get("I_ORDER_NO_"     +i) == null ? "" : (String)model.get("I_ORDER_NO_"     +i).toString();
                orderSeqNo[i]     = model.get("I_ORDER_SEQ_NO_" +i) == null ? "" : (String)model.get("I_ORDER_SEQ_NO_" +i).toString();
                ipgoEptDate[i]    = model.get("I_IPGO_EPT_DATE_"+i) == null ? "" : (String)model.get("I_IPGO_EPT_DATE_"+i).toString();
                gdsCd[i]          = model.get("I_GDS_CD_"       +i) == null ? "" : (String)model.get("I_GDS_CD_"       +i).toString();
                orderCnt[i]       = model.get("I_ORDER_CNT_"    +i) == null ? "" : (String)model.get("I_ORDER_CNT_"    +i).toString();
                vendorCd[i]       = model.get("I_VENDOR_CD_"    +i) == null ? "" : (String)model.get("I_VENDOR_CD_"    +i).toString();
                baseCd[i]         = model.get("I_BASE_CD_"      +i) == null ? "" : (String)model.get("I_BASE_CD_"      +i).toString();
                orderGbn[i]       = model.get("I_ORDER_GBN_"    +i) == null ? "" : (String)model.get("I_ORDER_GBN_"    +i).toString();
                orderPrc[i]       = model.get("I_ORDER_PRC_"    +i) == null ? "" : (String)model.get("I_ORDER_PRC_"    +i).toString();
                orderAmt[i]       = model.get("I_ORDER_AMT_"    +i) == null ? "" : (String)model.get("I_ORDER_AMT_"    +i).toString();
                boxGdsCnt[i]      = model.get("I_BOX_GDS_CNT_"  +i) == null ? "" : (String)model.get("I_BOX_GDS_CNT_"  +i).toString();
                note[i]           = model.get("I_NOTE_"         +i) == null ? "" : (String)model.get("I_NOTE_"         +i).toString();
                dNote[i]          = model.get("I_D_NOTE_"       +i) == null ? "" : (String)model.get("I_D_NOTE_"       +i).toString();
                wmsNo[i]          = model.get("I_WMS_NO_"       +i) == null ? "" : (String)model.get("I_WMS_NO_"       +i).toString();
                ordId[i]          = model.get("I_ORD_ID_"       +i) == null ? "" : (String)model.get("I_ORD_ID_"       +i).toString();
                ordSeq[i]         = model.get("I_ORD_SEQ_"      +i) == null ? "" : (String)model.get("I_ORD_SEQ_"      +i).toString();
                ritemId[i]        = model.get("I_RITEM_ID_"     +i) == null ? "" : (String)model.get("I_RITEM_ID_"     +i).toString();
            }
            modelIns.put("I_ORDER_DATE"   , orderDate  );
            modelIns.put("I_ORDER_NO"     , orderNo    );
            modelIns.put("I_ORDER_SEQ_NO" , orderSeqNo );
            modelIns.put("I_IPGO_EPT_DATE", ipgoEptDate);
            modelIns.put("I_GDS_CD"       , gdsCd      );
            modelIns.put("I_ORDER_CNT"    , orderCnt   );
            modelIns.put("I_VENDOR_CD"    , vendorCd   );
            modelIns.put("I_BASE_CD"      , baseCd     );
            modelIns.put("I_ORDER_GBN"    , orderGbn   );
            modelIns.put("I_ORDER_PRC"    , orderPrc   );
            modelIns.put("I_ORDER_AMT"    , orderAmt   );
            modelIns.put("I_BOX_GDS_CNT"  , boxGdsCnt  );
            modelIns.put("I_NOTE"         , note       );
            modelIns.put("I_D_NOTE"       , dNote      );
            modelIns.put("I_WMS_NO"       , wmsNo      );
            modelIns.put("I_ORD_ID"       , ordId      );
            modelIns.put("I_ORD_SEQ"      , ordSeq     );
            modelIns.put("I_RITEM_ID"     , ritemId    );
            
            modelIns.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_CUST_ID", (String)model.get("vrSrchCustIdE2"));
            modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            
            map = (Map<String,Object>)dao.syncInOrder(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSIF780", String.valueOf(map.get("O_MSG_CODE")), (String)map.get("O_MSG_NAME"));
            
            map.put("errCnt", 0);
            map.put("MSG", MessageResolver.getMessage("save.sucess"));
            
        }catch(BizException be){
            map.put("errCnt", -1);
            map.put("MSG", be.getMessage() );
        }
        catch(Exception e){
            throw e;
        }
        
        return map;
    }
    
    /**
     * 
     * 대체 Method ID     : syncOutOrder
     * 대체 Method 설명 : 출고주문정보 동기화
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> syncOutOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        
        try{
            
            int selectIds = Integer.parseInt(model.get("ROW_COUNT").toString());
            
            String[] sujuDate       = new String[selectIds];
            String[] sujuNo         = new String[selectIds];
            String[] sujuSeqNo      = new String[selectIds];
            String[] chulEptDate    = new String[selectIds];
            String[] storeCd        = new String[selectIds];
            String[] baseCd         = new String[selectIds];
            String[] sujuGbn        = new String[selectIds];
            String[] gdsCd          = new String[selectIds];
            String[] sujuCnt        = new String[selectIds];
            String[] suppPrc        = new String[selectIds];
            String[] suppAmt        = new String[selectIds];
            String[] vatPrc         = new String[selectIds];
            String[] salePrc        = new String[selectIds];
            String[] saleAmt        = new String[selectIds];
            String[] boxGdsCnt      = new String[selectIds];
            String[] notaxYn        = new String[selectIds];
            String[] note           = new String[selectIds];
            String[] dNote          = new String[selectIds];
            String[] wmsNo          = new String[selectIds];
            String[] ordId          = new String[selectIds];
            String[] ordSeq         = new String[selectIds];
            String[] ritemId        = new String[selectIds];
            
            for(int i = 0; i < selectIds ; i++){
                sujuDate[i]       = model.get("I_SUJU_DATE_"    +i) == null ? "" : (String)model.get("I_SUJU_DATE_"    +i).toString();
                sujuNo[i]         = model.get("I_SUJU_NO_"      +i) == null ? "" : (String)model.get("I_SUJU_NO_"      +i).toString();
                sujuSeqNo[i]      = model.get("I_SUJU_SEQ_NO_"  +i) == null ? "" : (String)model.get("I_SUJU_SEQ_NO_"  +i).toString();
                chulEptDate[i]    = model.get("I_CHUL_EPT_DATE_"+i) == null ? "" : (String)model.get("I_CHUL_EPT_DATE_"+i).toString();
                storeCd[i]        = model.get("I_STORE_CD_"     +i) == null ? "" : (String)model.get("I_STORE_CD_"     +i).toString();
                baseCd[i]         = model.get("I_BASE_CD_"      +i) == null ? "" : (String)model.get("I_BASE_CD_"      +i).toString();
                sujuGbn[i]        = model.get("I_SUJU_GBN_"     +i) == null ? "" : (String)model.get("I_SUJU_GBN_"     +i).toString();
                gdsCd[i]          = model.get("I_GDS_CD_"       +i) == null ? "" : (String)model.get("I_GDS_CD_"       +i).toString();
                sujuCnt[i]        = model.get("I_SUJU_CNT_"     +i) == null ? "" : (String)model.get("I_SUJU_CNT_"     +i).toString();
                suppPrc[i]        = model.get("I_SUPP_PRC_"     +i) == null ? "" : (String)model.get("I_SUPP_PRC_"     +i).toString();
                suppAmt[i]        = model.get("I_SUPP_AMT_"     +i) == null ? "" : (String)model.get("I_SUPP_AMT_"     +i).toString();
                vatPrc[i]         = model.get("I_VAT_PRC_"      +i) == null ? "" : (String)model.get("I_VAT_PRC_"      +i).toString();
                salePrc[i]        = model.get("I_SALE_PRC_"     +i) == null ? "" : (String)model.get("I_SALE_PRC_"     +i).toString();
                saleAmt[i]        = model.get("I_SALE_AMT_"     +i) == null ? "" : (String)model.get("I_SALE_AMT_"     +i).toString();
                boxGdsCnt[i]      = model.get("I_BOX_GDS_CNT_"  +i) == null ? "" : (String)model.get("I_BOX_GDS_CNT_"  +i).toString();
                notaxYn[i]        = model.get("I_NOTAX_YN_"     +i) == null ? "" : (String)model.get("I_NOTAX_YN_"     +i).toString();
                note[i]           = model.get("I_NOTE_"         +i) == null ? "" : (String)model.get("I_NOTE_"         +i).toString();
                dNote[i]          = model.get("I_D_NOTE_"       +i) == null ? "" : (String)model.get("I_D_NOTE_"       +i).toString();
                wmsNo[i]          = model.get("I_WMS_NO_"       +i) == null ? "" : (String)model.get("I_WMS_NO_"       +i).toString();
                ordId[i]          = model.get("I_ORD_ID_"       +i) == null ? "" : (String)model.get("I_ORD_ID_"       +i).toString();
                ordSeq[i]         = model.get("I_ORD_SEQ_"      +i) == null ? "" : (String)model.get("I_ORD_SEQ_"      +i).toString();
                ritemId[i]        = model.get("I_RITEM_ID_"     +i) == null ? "" : (String)model.get("I_RITEM_ID_"     +i).toString();
              
            }
            
            modelIns.put("I_SUJU_DATE"    , sujuDate   );
            modelIns.put("I_SUJU_NO"      , sujuNo     );
            modelIns.put("I_SUJU_SEQ_NO"  , sujuSeqNo  );
            modelIns.put("I_CHUL_EPT_DATE", chulEptDate);
            modelIns.put("I_STORE_CD"     , storeCd    );
            modelIns.put("I_BASE_CD"      , baseCd     );
            modelIns.put("I_SUJU_GBN"     , sujuGbn    );
            modelIns.put("I_GDS_CD"       , gdsCd      );
            modelIns.put("I_SUJU_CNT"     , sujuCnt    );
            modelIns.put("I_SUPP_PRC"     , suppPrc    );
            modelIns.put("I_SUPP_AMT"     , suppAmt    );
            modelIns.put("I_VAT_PRC"      , vatPrc     );
            modelIns.put("I_SALE_PRC"     , salePrc    );
            modelIns.put("I_SALE_AMT"     , saleAmt    );
            modelIns.put("I_BOX_GDS_CNT"  , boxGdsCnt  );
            modelIns.put("I_NOTAX_YN"     , notaxYn    );
            modelIns.put("I_NOTE"         , note       );
            modelIns.put("I_D_NOTE"       , dNote      );
            modelIns.put("I_WMS_NO"       , wmsNo      );
            modelIns.put("I_ORD_ID"       , ordId      );
            modelIns.put("I_ORD_SEQ"      , ordSeq     );
            modelIns.put("I_RITEM_ID"     , ritemId    );
            
            modelIns.put("I_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_CUST_ID", (String)model.get("vrSrchCustIdE3"));
            modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            
            map = (Map<String,Object>)dao.syncOutOrder(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSIF780", String.valueOf(map.get("O_MSG_CODE")), (String)map.get("O_MSG_NAME"));
            
            map.put("errCnt", 0);
            map.put("MSG", MessageResolver.getMessage("save.sucess"));
            
        }catch(BizException be){
            map.put("errCnt", -1);
            map.put("MSG", be.getMessage() );
        }
        catch(Exception e){
            throw e;
        }
        
        return map;
    }
    
}