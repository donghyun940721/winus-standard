package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF726Dao")
public class WMSIF726Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list 
     * Method 설명  : EBIZ 주문관리 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list", model);
    }   
    
    /**
     * Method ID  : list01
     * Method 설명  :  EBIZWAY 호출이력 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list01(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list01", model);
    }
    
    /**
     * Method ID  : list02
     * Method 설명  :  EBIZWAY 호출이력 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list02(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list02", model);
    }   
    
    /**
     * Method ID  : list03
     * Method 설명  :  EBIZWAY 상품마스터 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list03(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list03", model);
    } 
    
    /**
     * Method ID  : list04
     * Method 설명  :  EBIZWAY 입고처마스터 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list04(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list04", model);
    } 
    
    /**
     * Method ID  : list05
     * Method 설명  :  EBIZWAY 매장마스터 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list05(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list05", model);
    } 

    /**
     * Method ID  : list06
     * Method 설명  :  EBIZWAY 창고이동정보 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list06(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list06", model);
    } 
    
    /**
     * Method ID  : list07
     * Method 설명  :  EBIZWAY 전송재고 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list07(Map<String, Object> model) {
        return executeQueryPageWq("wmsif726.list07", model);
    } 
    
    /**
     * Method ID  : getCustOrdDegree
     * Method 설명  : 주문차수 검색 
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public Object getCustOrdDegree(Map<String, Object> model){
        return executeQueryForList("wmsif726.getCustOrdDegree", model);
    }
}


