package com.logisall.winus.wmsif.service.impl;

import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF901Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
@Primary
public class WMSIF901ServiceImpl implements WMSIF901Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    private static final String MENU_ID = "WMSIF901";
    private static final String MOBIS_LCID = "0000002060"; 			//LOGISALL USA 제2물류센터
    private static final String MOBIS_MULTIMEDIA = "0000549557";	//모비스 멀티미디어
    private static final String MOBIS_MODULE = "0000549558";		//모비스 모듈
    
    @Autowired
    private WMSIF901Dao dao;
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: kih
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
//      if(1==1) {
//        throw new Exception("에러");
//      }
        Map<String, Object> map = new HashMap<String, Object>();
        String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
//        String testCustId = "0000020164";		//LOGISALL > 0000020164 테스트 동안만...
        model.put("LC_ID", lcId);
        model.put("CUST_ID", MOBIS_MULTIMEDIA);
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE2
     * 대체 Method 설명	: 
     * 작성자			: kih
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE2(model));
        return map;
    }
    

	/*멀티미디어 출고주문 수신 리스트*/
	@Override
	public Map<String, Object> listE3(Map<String, Object> model) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE3(model));
        return map;
	}
	
	/**/
	@Override
	public Map<String, Object> listE4(Map<String, Object> model) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE4(model));
        return map;
	}

	
	/**/
	@Override
	public Map<String, Object> listE5(Map<String, Object> model) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE5(model));
        return map;
	}
	
	@Override
	public Map<String, Object> runMobisProcedure(Map<String, Object> sessionModel, Map<String, Object> reqMap) throws Exception{
		Map<String, Object> m = new HashMap<String, Object>();

		String serviceName = (String) reqMap.get("SRC_NAME");
		switch (serviceName != null ? serviceName : "") {
		case "PK_WMSIF111.SP_MOBIS_AL_ASN_INSERT_ORDER"://모비스 ASN 입고
			m = insertMobisInbAsn(sessionModel, reqMap);
			break;
		case "PK_WMSIF111.SP_MOBIS_AL_SHIP_INSERT_ORDER"://모비스 출고주문
			m = insertMobisOutbOrder(sessionModel, reqMap);		
			break;
		case "PK_WMSIF111.SP_MOBIS_AL_SHIP_COMPLETE"://모비스 출고확정
			m = insertMobisOutbOrderConfirm(sessionModel, reqMap);
			break;
		case "PK_WMSIF111.SP_MOBIS_AL_SHIP_HU_COMPLETE"://모비스 출고실적
			m = insertMobisModuleOutbOrderResults(sessionModel, reqMap);
			break;
		default:
			throw new InvalidParameterException("서비스 명을 확인해 주세요, "+serviceName);
		}

		return m;
	}

	/**
     * 
     * 대체 Method ID   : insertMobisInbAsn 
     * call PK_WMSIF111.SP_MOBIS_AL_ASN_INSERT_ORDER
     * 대체 Method 설명    : 모비스 ASN 입고 주문 줄력
     * 작성자                      : kih
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> insertMobisInbAsn(Map<String, Object> sessionModel, Map<String, Object> reqMap) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			validationCheck(sessionModel);
			Map<String, Object> modelDt = setMobisOrdDatas(sessionModel, reqMap, "01", MOBIS_MULTIMEDIA);
			// dao
			dao.insertMobisInbAsn(modelDt);
			ServiceUtil.isValidReturnCode(
					MENU_ID, 
					String.valueOf(modelDt.get("O_MSG_CODE")),
					(String) modelDt.get("O_MSG_NAME")
					);
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		
		return m;
	}

	
	/**
     * 
     * 대체 Method ID   : insertMobisOutbOrder 
     * call PK_WMSIF111.SP_MOBIS_AL_SHIP_INSERT_ORDER
     * 대체 Method 설명    : 모비스 멀티미디어 출고 주문입력
     * 작성자                      : kih
     * @param model @requestBody reqMap
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> insertMobisOutbOrder(Map<String, Object> sessionModel, Map<String, Object> reqMap) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			validationCheck(sessionModel);
			Map<String, Object> modelDt = setMobisOrdDatas(sessionModel, reqMap, "02", MOBIS_MULTIMEDIA);
			// dao
			dao.insertMobisOutbOrder(modelDt);
			ServiceUtil.isValidReturnCode(
					MENU_ID, 
					String.valueOf(modelDt.get("O_MSG_CODE")),
					(String) modelDt.get("O_MSG_NAME")
					);
			
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		
		return m;
	}
	
	/**
     * 
     * 대체 Method ID   : insertMobisOutbOrderConfirm 
     * call PK_WMSIF111.SP_MOBIS_AL_SHIP_COMPLETE
     * 대체 Method 설명    : 모비스 멀티미디어 출고 확정 입력
     * 작성자                      : kih
     * @param model @requestBody reqMap
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> insertMobisOutbOrderConfirm(Map<String, Object> sessionModel, Map<String, Object> reqMap) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			validationCheck(sessionModel);
			Map<String, Object> modelDt = setMobisOrdDatas(sessionModel, reqMap, "02", MOBIS_MULTIMEDIA);
			// dao
			dao.insertMobisOutbOrderConfirm(modelDt);
			ServiceUtil.isValidReturnCode(
					MENU_ID, 
					String.valueOf(modelDt.get("O_MSG_CODE")),
					(String) modelDt.get("O_MSG_NAME")
					);
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		
		return m;
	}
	
	/**
     * 
     * 대체 Method ID   : insertMobisModuleOutbOrderResults 
     * call PK_WMSIF111.PK_WMSIF111.SP_MOBIS_AL_SHIP_HU_COMPLETE
     * 대체 Method 설명    : 모비스 모듈 출고 실적 입력
     * 작성자                      : kih
     * @param model @requestBody reqMap
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> insertMobisModuleOutbOrderResults(Map<String, Object> sessionModel, Map<String, Object> reqMap) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			validationCheck(sessionModel);
			Map<String, Object> modelDt = setMobisOrdDatas(sessionModel, reqMap, "02", MOBIS_MODULE);
			// dao
			dao.insertMobisModuleOutbOrderResults(modelDt);
			ServiceUtil.isValidReturnCode(
					MENU_ID, 
					String.valueOf(modelDt.get("O_MSG_CODE")),
					(String) modelDt.get("O_MSG_NAME")
					);
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		
		return m;
	}
	
	private void validationCheck(Map<String, Object> sessionModel) throws Exception{
		String myLcId = (String) sessionModel.get(ConstantIF.SS_SVC_NO);
		if(!MOBIS_LCID.equals(myLcId)) {
			throw new BizException("센터코드가 다릅니다.");
		}
	}
	
	private Map<String, Object> setMobisOrdDatas(Map<String, Object> sessionModel, Map<String, Object> reqMap, String ordType, String custId) throws ParseException{
		Map<String, Object> modelDt = new HashMap<String, Object>();
//		String mobisLcId = "0000002060"; 	//LOGISALL USA 제2물류센터
//		String mobisCustId = "0000021696"; 	//HYUNDAISTEEL(AL) 고객사(화주)
		
//		String testLcId = "0000001262";		//PR_WAREHOUSE 0000001262 테스트 동안만...
//		String testCustId = "0000020164";		//LOGISALL > 0000020164 테스트 동안만...
		
//		String mobisModule = "0000549558";	//모비스 모듈
//		String mobisMultimedia = "0000549557";	//모비스 멀티미디어
		
		
		//ordType(주문유형): 02(출고) 01(입고)
		//ordSubType(주문단계): 30(피킹리스트발행) 20(로케이션지정)
		String ordSubType = "01".equals(ordType) ? "20" : "30";			
		String workIp = (String) sessionModel.get(ConstantIF.SS_CLIENT_IP);
		String userNo = (String) sessionModel.get(ConstantIF.SS_USER_NO);
		
		SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String fromDtString = (String)reqMap.get("FROM_DT");
        if(fromDtString == null) {
          fromDtString = new DateUtil().getLocaleDate(new Date());
        }
		
		Date fromDate = inputDateFormat.parse(fromDtString);
		String fromDateToString = DateUtil.getDateFormat(fromDate, "yyyyMMdd");
		
//		modelDt.put("LC_ID", testLcId);
//        modelDt.put("CUST_ID", testCustId);
		
		modelDt.put("LC_ID", MOBIS_LCID);
		modelDt.put("CUST_ID", custId);
		modelDt.put("ORD_TYPE", ordType);
		modelDt.put("ORD_SUBTYPE", ordSubType);
		modelDt.put("DLV_DT", fromDateToString);
		modelDt.put("USER_NO", userNo);
        modelDt.put("WORK_IP", workIp);
        
        return modelDt;
	}
	
  @Override
  public Map<String, Object> saveInboundIf(Map<String, Object> sessionModel, Map<String, Object> reqMap) throws Exception{
    Map<String, Object> m = new HashMap<String, Object>();
    int errCnt = 0;
    try {
        validationCheck(sessionModel);
        Map<String, Object> modelDt = setMobisOrdDatas(sessionModel, reqMap, "01", MOBIS_MULTIMEDIA);
        List<Map<String, Object>> inboundAsnList = (List<Map<String, Object>>) reqMap.get("gridData");
        Set<String> vbelnList = new HashSet<String>(); 
        for (Map<String, Object> inboundData : inboundAsnList) {
          String vbeln = (String)inboundData.get("VBELN");
          String huNo = (String)inboundData.get("VENUM");
          String wmsIfYn = (String)inboundData.get("WMS_IF_YN");
          if("Y".equalsIgnoreCase(wmsIfYn)) {
            throw new BizException(vbeln+"[huNo:"+huNo+"]는 이미 인터페이스 완료되었습니다. 입고정보를 확인해 주세요.");
          }
          vbelnList.add(vbeln);
          
        }
        String[] stringArrayVBELN = vbelnList.toArray(new String[vbelnList.size()]);
        modelDt.put("VBELN_LIST", stringArrayVBELN);

        dao.insertMobisInbAsnMulti(modelDt);
        
        
        
        ServiceUtil.isValidReturnCode(
              MENU_ID, 
              String.valueOf(modelDt.get("O_MSG_CODE")),
              (String) modelDt.get("O_MSG_NAME")
              );
        m.put("errCnt", errCnt);
        m.put("MSG", MessageResolver.getMessage("save.success"));

    } catch (BizException be) {
        if (log.isInfoEnabled()) {
            log.info(be.getMessage());
        }
        m.put("errCnt", 1);
        m.put("MSG", be.getMessage());

    } catch (Exception e) {
        throw e;
    }
    
    return m;
  }

  @Override
  public Map<String, Object> deleteOutboundIf(Map<String, Object> sessionModel,
      Map<String, Object> reqMap) throws Exception{
    Map<String, Object> m = new HashMap<String, Object>();
    int errCnt = 0;
    try {
        validationCheck(sessionModel);
        Map<String, Object> modelDt = setMobisOrdDatas(sessionModel, reqMap, "01", MOBIS_MULTIMEDIA);
        List<Map<String, Object>> outboundAsnList = (List<Map<String, Object>>) reqMap.get("gridData");
        Set<String> asnList = new HashSet<String>(); 
        for (Map<String, Object> outbound : outboundAsnList) {
          String asnNo = (String)outbound.get("ASNNO");
//          String comYn = (String)outbound.get("COM_YN");
//          if("Y".equalsIgnoreCase(comYn)) {
//            throw new BizException("[asnNo:"+asnNo+"]는 이미 인터페이스 전송 완료되었습니다.");
//          }
          asnList.add(asnNo);
          
        }
        modelDt.put("ASNNO_LIST",  new ArrayList<String>(asnList));
        dao.deleteOutboundIf(modelDt);

        m.put("errCnt", errCnt);
        m.put("MSG", MessageResolver.getMessage("save.success"));

    } catch (BizException be) {
        if (log.isInfoEnabled()) {
            log.info(be.getMessage());
        }
        m.put("errCnt", 1);
        m.put("MSG", be.getMessage());

    } catch (Exception e) {
        throw e;
    }
    
    return m;
  }

  @Override
  public Map<String, Object> deleteOutboundIf2(Map<String, Object> sessionModel,
      Map<String, Object> reqMap) throws Exception {
    Map<String, Object> m = new HashMap<String, Object>();
    int errCnt = 0;
    try {
        validationCheck(sessionModel);
        List<Map<String, Object>> outboundAsnList = (List<Map<String, Object>>) reqMap.get("gridData");
        Set<String> dcNumList = new HashSet<String>(); 
        for (Map<String, Object> outbound : outboundAsnList) {
          String asnNo = (String)outbound.get("DCNUM");
          dcNumList.add(asnNo);
        }
        reqMap.put("DCNUM_LIST",  new ArrayList<String>(dcNumList));
        dao.deleteMobisHuComtoMobisDetail(reqMap);
        dao.deleteMobisHuComtoMobisHeader(reqMap);
        
        m.put("errCnt", errCnt);
        m.put("MSG", MessageResolver.getMessage("save.success"));
    } catch (BizException be) {
        if (log.isInfoEnabled()) {
            log.info(be.getMessage());
        }
        m.put("errCnt", 1);
        m.put("MSG", be.getMessage());

    } catch (Exception e) {
        throw e;
    }
    
    return m;
  }

    @Override
    public Map<String, Object> deleteOutboundOrderIf(Map<String, Object> sessionModel, Map<String, Object> reqMap)
    	throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try {
            validationCheck(sessionModel);
            Map<String, Object> modelDt = setMobisOrdDatas(sessionModel, reqMap, "01", MOBIS_MULTIMEDIA);
            List<Map<String, Object>> outboundOrderList = (List<Map<String, Object>>) reqMap.get("gridData");
            Set<String> exidvList = new HashSet<String>(); 
            Set<Map<String, Object>> dupOutBound = new HashSet<Map<String,Object>>();
            for (Map<String, Object> outbound : outboundOrderList) {
        	String kunnr = (String) outbound.get("KUNNR");
        	String plstdt = (String) outbound.get("PLSTDT");
        	String rqtdepdt = (String) outbound.get("RQTDEPDT");
        	String rqtdeptm = (String) outbound.get("RQTDEPTM");
        	
        	modelDt.put("KUNNR", kunnr);
        	modelDt.put("PLSTDT", plstdt);
        	modelDt.put("RQTDEPDT", rqtdepdt);
        	modelDt.put("RQTDEPTM", rqtdeptm);
        	
        	String wmsIfYn = (String)outbound.get("WMS_IF_YN");
              
        	if("Y".equalsIgnoreCase(wmsIfYn)) {
        	    throw new BizException("["+rqtdepdt+"_"+rqtdeptm+"]는 이미 인터페이스 완료되었습니다.");
        	}
//              exidvList.add(exidv);
        	dupOutBound.add(modelDt);
//              dao.deleteOutboundOrderIf(modelDt);
            }
            
            for (Map<String, Object> map : dupOutBound) {
        	 dao.deleteOutboundOrderIf(map);
	    }
            modelDt.put("EXIDV_LIST",  new ArrayList<String>(exidvList));
           
    
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
    
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage());
    
        } catch (Exception e) {
            throw e;
        }
        
        return m;
    }
}
