package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF613Dao")
public class WMSIF613Dao extends SqlMapAbstractDAO{

    
    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif613.list", model);
    }

    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmsif613.listE2", model);
    }

    /**
     * Method ID : saveJasamallCallback 
     * Method 설명 : 자사몰 주문확정 Callback
     * 작성자 : sing09
     * @param model
     * @return
     */
    public Object saveJasamallCallback(Map<String, Object> model){
        executeUpdate("wmsif613.wmsif108.saveJasamallCallback", model);
        return model;
    }
    
    /**
     * Method ID : jasamallDelete 
     * Method 설명 : 자사몰 주문/실적 삭제
     * 작성자 : sing09
     * @param model
     * @return
     */
    public Object jasamallDelete(Map<String, Object> model){
        executeUpdate("wmsif613.wmsif108.deleteJasamallOrder", model);
        return model;
    }
}


