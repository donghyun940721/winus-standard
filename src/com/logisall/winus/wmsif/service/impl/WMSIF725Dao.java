package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF725Dao")
public class WMSIF725Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list 
     * Method 설명  : EBIZ 주문관리 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif725.list", model);
    }   
    
    /**
     * Method ID  : list2
     * Method 설명  : EBIZ 주문관리 조회
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
        return executeQueryPageWq("wmsif725.list2", model);
    }   
    
    /**
     * Method ID  : getCustOrdDegree
     * Method 설명  : 주문차수 검색 
     * 작성자            : SUMMER H
     * @param   model
     * @return
     */
    public Object getCustOrdDegree(Map<String, Object> model){
        return executeQueryForList("wmsif725.getCustOrdDegree", model);
    }
}


