package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF709Dao")
public class WMSIF709Dao extends SqlMapAbstractDAO{
    
    /**
     * Method ID  	: listE1
     * Method 설명   	: 
     * 작성자               	: dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
        return executeQueryPageWq("wmsif709.listE1", model);
    }  
    
    
    /**
     * Method ID   	: getDetList
     * Method 설명    	: 주문상세조회
     * 작성자              	: dhkim
     * @param   model
     * @return
     */
    public List getDetList(Map<String, Object> model) {        
        return executeQueryForList("wmsif709.getDetList", model);
    }
    
    
    /**
     * Method ID 	: deleteE1
     * Method 설명 	: 인터페이스 주문삭제 (MAS)
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public Object deleteMas(Object model) {
		return executeUpdate("wmsif709.deleteMas", model);
	}
	
	
    /**
     * Method ID 	: deleteE1
     * Method 설명 	: 인터페이스 주문삭제 (DET)
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public Object deleteDet(Object model) {
		return executeUpdate("wmsif709.deleteDet", model);
	}
}


