package com.logisall.winus.wmsif.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
@SuppressWarnings("unchecked")
public class WMSIF902Dao extends SqlMapAbstractDAO {
	private final Log log = LogFactory.getLog(this.getClass());
	// WINUSUSR
	@Autowired
	private SqlMapClient sqlMapClient;

	// WINUSUSR_IF
	@Autowired
	private SqlMapClient sqlMapClientWinusIf;

	public List<Object> listE01(Map<String, Object> model) {
		List<Object> ifList = new ArrayList<Object>();
		try {
			ifList = sqlMapClientWinusIf.queryForList("wmsif902.listE01", model);
		} catch (SQLException e) {
			log.error("SQL 오류", e);
			throw new RuntimeSQLException(e);
		}
		return ifList;
	}

	public List<Map<String, Object>> selectCommonCodeHeaderList(Map<String, Object> model) throws SQLException {
		return sqlMapClientWinusIf.queryForList("wmsif902.selectCommonCodeHeaderList", model);
	}

	public void updateCommonCodeHeader(Map<String, Object> param) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateCommonCodeHeader", param);
	}

	public List<Map<String, Object>> selectCommonCodeDetailList(Map<String, Object> param) throws SQLException {
		return sqlMapClientWinusIf.queryForList("wmsif902.selectCommonCodeDetailList", param);
	}

	public void updateCommonCodeDetail(Map<String, Object> param) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateCommonCodeDetail", param);
	}

	public List<Map<String, Object>> selectZsCustList(Map<String, Object> param) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmsif902.selectZsCustList", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
	}

	public Object selectWarehouseList(Map<String, Object> param) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmsif902.selectWarehouseList", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
	}

	public String selectCustId(Map<String, Object> param) throws SQLException {
		return (String) sqlMapClient.queryForObject("wmsif902.selectCustId", param);
	}

	public void updateZsCustIfYn(Map<String, Object> param) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateZsCustIfYn", param);
	}

	public void updateWarehouseIfYn(Map<String, Object> param) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateWarehouseIfYn", param);
	}

	public List<Map<String, Object>> selectProductList(Map<String, Object> param) {
		try {
			return sqlMapClientWinusIf.queryForList("wmsif902.selectProductList", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}

	public List<Map<String, Object>> selectProductPkgngList(Map<String, Object> param) throws SQLException {
		return sqlMapClientWinusIf.queryForList("wmsif902.selectProductPkgngList", param);
	}

	public void updateProductIfYn(Map<String, Object> param) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateProductIfYn", param);
	}

	public void insertBulkOrder(List<Map<String, Object>> outOrderList) throws SQLException {
		int batchSize = 200;
		int updateCnt = 0;

		try {
			sqlMapClient.startTransaction();
			sqlMapClient.startBatch();

			for (int i = 0; i < outOrderList.size(); i++) {
				updateCnt++;
				processOrder(outOrderList.get(i));

				if (i > 0 && i % batchSize == 0) {
					executeBatchWithLogging(outOrderList, i - batchSize + 1, i + 1, "batch index " + (i / batchSize));
					sqlMapClient.startBatch(); // Reset batch to avoid ORA-01000
				}
			}

			executeBatchWithLogging(outOrderList, (outOrderList.size() / batchSize) * batchSize, outOrderList.size(),
					"final batch");
			sqlMapClient.commitTransaction();
			log.info("전체 처리된 update 수: " + updateCnt);

		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeSQLException(e);
		} finally {
			endTransaction();
		}
	}

	private void processOrder(Map<String, Object> order) throws SQLException {
		sqlMapClient.insert("outborder.insertHeader", order);
		List<Map<String, Object>> orderDetailList = (List<Map<String, Object>>) order.get("children");

		for (Map<String, Object> orderDetail : orderDetailList) {
			String ordId = order.get("ORD_ID").toString();
			orderDetail.put("ORD_ID", ordId);
			sqlMapClient.insert("outborder.insertDetail", orderDetail);
		}
	}

	private void executeBatchWithLogging(List<Map<String, Object>> outOrderList, int start, int end,
			String batchDescription) throws SQLException {
		try {
			sqlMapClient.executeBatch();
		} catch (SQLException e) {
			log.error("Error executing " + batchDescription, e);
			logFailedQueries(outOrderList.subList(start, end));
			throw e;
		}
	}

	private void logFailedQueries(List<Map<String, Object>> failedOrders) {
		for (Map<String, Object> order : failedOrders) {
			log.error("Failed order: " + order);
			List<Map<String, Object>> orderDetailList = (List<Map<String, Object>>) order.get("children");
			for (Map<String, Object> orderDetail : orderDetailList) {
				log.error("Failed order detail: " + orderDetail);
			}
		}
	}

	private void endTransaction() {
		try {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		} catch (SQLException e) {
			log.error("Error ending transaction", e);
		}
	}

	public void updateProductListIfSuccess(List<Map<String, Object>> productList) throws SQLException {
		try {
			sqlMapClientWinusIf.startTransaction();
			sqlMapClientWinusIf.startBatch();

			int batchSize = 300;
			int updateCnt = 0;
			for (int i = 0; i < productList.size(); i++) {
				updateCnt++;
				Map<String, Object> product = productList.get(i);
				sqlMapClientWinusIf.update("wmsif902.updateProductIfYn_V2", product);
				sqlMapClientWinusIf.update("wmsif902.updateProductPkgngIfYn_V2", product);

				if (i > 0 && i % batchSize == 0) {
					sqlMapClientWinusIf.executeBatch();
					sqlMapClientWinusIf.startBatch(); // Reset batch to avoid ORA-01000
				}
			}

			sqlMapClientWinusIf.executeBatch();
			sqlMapClientWinusIf.commitTransaction();
			log.info("전체 처리된 update 수: " + updateCnt);

		} catch (SQLException e) {
			sqlMapClientWinusIf.endTransaction();
			log.error(e);
			throw new RuntimeSQLException();
		} finally {
			if (sqlMapClientWinusIf != null) {
				sqlMapClientWinusIf.endTransaction();
			}
		}
	}

	public void updateProductPkgngIfYn(Map<String, Object> param) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateProductPkgngIfYn", param);
	}

	public List<Map<String, Object>> selectInboundHeaderList(Map<String, Object> model) {
		try {
			return sqlMapClientWinusIf.queryForList("wmsif902.selectInboundHeaderList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}

	public String selectWinusCustCd(Map<String, Object> model) {
		try {
			return (String) sqlMapClient.queryForObject("wmsif902.selectWinusCustCd", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}

	public List<Object> selectInboundDetailList(Map<String, Object> model) {
		List<Object> resultList = new ArrayList<Object>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmsif902.selectInboundDetailList", model);
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeSQLException(e.getMessage(), e);
		}
		return resultList;
	}

	public List<Object> selectOutOrderAsnList(Map<String, Object> model) {
		List<Object> resultList = new ArrayList<Object>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmsif902.selectOutOrderAsnList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
	}

	public List<Object> selectInResultList(Map<String, Object> model) {
		List<Object> resultList = new ArrayList<Object>();
		try {
			resultList = sqlMapClient.queryForList("wmsif902.selectInResultList", model);
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeSQLException(e.getMessage(), e);
		}
		return resultList;
	}

	public List<Object> selectInSendList(Map<String, Object> model) {
		List<Object> sendList = new ArrayList<Object>();
		try {
			sendList = sqlMapClientWinusIf.queryForList("wmsif902.selectInSendList", model);
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeSQLException(e.getMessage(), e);
		}
		return sendList;
	}

	public List<Object> selectWinusItemList(Map<String, Object> model) {
		List<Object> resultList = new ArrayList<Object>();
		try {
			resultList = sqlMapClient.queryForList("wmsif902.selectWinusItemList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
	}

	public List<Map<String, Object>> selectCommonCodeList(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmsif902.selectCommonCodeList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
	}

	public void insertOrderHeader(Map<String, Object> model) throws SQLException {
		sqlMapClient.insert("outborder.insertHeader", model);
	}

	public void insertOrderDetail(Map<String, Object> model) throws SQLException {
		sqlMapClient.insert("outborder.insertDetail", model);
	}

	public void updateWellfoodInboundHeader(Map<String, Object> model) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateWellfoodInboundHeader", model);
	}

	public void updateWellfoodInboundDetail(Map<String, Object> model) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.updateWellfoodInboundDetail", model);
	}

	public void updateWellfoodOutboundHeader(Map<String, Object> model) {
		try {
			sqlMapClientWinusIf.update("wmsif902.updateWellfoodOutboundHeader", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}

	public void updateWellfoodOutboundDetail(Map<String, Object> model) {
		try {
			sqlMapClientWinusIf.update("wmsif902.updateWellfoodOutboundDetail", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}

	public void insertOrderDetailList(List list) throws SQLException {
		executeInsertBatch("outborder.insertDetail", list, 5000);
	}

	public List<Map<String, Object>> selectProducts(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmsif902.selectProducts", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
	}

	public Object checkItfComYn(Map<String, Object> model) throws Exception {
		return sqlMapClient.queryForObject("wmsif902.checkItfComYn", model);
	}

	public Object checkCountOrder(Map<String, Object> model) throws Exception {
		return sqlMapClient.queryForObject("wmsif902.checkCountOrder", model);
	}

	public void insertInResult(Map<String, Object> model) throws Exception {
		sqlMapClientWinusIf.insert("wmsif902.insertInResult", model);
	}

	public void updateItfComYn(Map<String, Object> model) throws Exception {
		sqlMapClient.update("wmsif902.updateItfComYn", model);
	}

	public void mergeDayProductDiv(Map<String, Object> model) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.mergeDayProductDiv", model);
	}

	public void deleteDayProductDiv(Map<String, Object> model) throws SQLException {
		sqlMapClientWinusIf.update("wmsif902.deleteDayProductDiv", model);
	}

	public List<Object> selectDayProductDivList(Map<String, Object> model) {
		try {
			return sqlMapClientWinusIf.queryForList("wmsif902.selectDayProductDivList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}
}
