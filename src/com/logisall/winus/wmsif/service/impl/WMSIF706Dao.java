package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF706Dao")
public class WMSIF706Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif706.list", model);
    }
    
    
    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet itemCollect(Map<String, Object> model) {
        return executeQueryPageWq("wmsif706.list1", model);
    }
    
    
    /**
     * Method ID   : list
     * Method 설명    : 이지어드민 상품정보 동기화 (EZAdmin -> Winus) 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public Object getItemId(Map<String, Object> model) {
    	return executeView("wmsif706.getItemId", model);        
    }
    
    
    /**
     * Method ID   : list
     * Method 설명    : 이지어드민 상품정보 동기화 (EZAdmin -> Winus) 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public Object getUomId(Map<String, Object> model) {
    	return executeView("wmsif706.getUomId", model);        
    }
    
    
    /**
     * Method ID   : list
     * Method 설명    : 이지어드민 상품정보 동기화 (EZAdmin -> Winus) 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public Object syncItem(Map<String, Object> model) {
        executeUpdate("wmsif202.pk_wmsif202.sp_ezadmin_sav_item", model);
        return model;
    }

    /**
    * Method ID   : list
    * Method 설명    : API 계정정보 획득
    * 
    * ToDo :: 공퉁부 처리
    * 작성자                : dhkim
    * @param   model
    * @return
    */
   public Object getOpenMallInfo(Map<String, Object> model) {
	   return executeQueryForList("wmsif706.getOpenMallInfo", model);       
   }
    
   
   /**
    * Method ID  : itemInfo
    * Method 설명   : 
    * 작성자                : dhkim
    * @param   model
    * @return
    */
   public GenericResultSet itemInfo(Map<String, Object> model) {
       return executeQueryPageWq("wmsif706.itemInfo", model);
   }   

}


