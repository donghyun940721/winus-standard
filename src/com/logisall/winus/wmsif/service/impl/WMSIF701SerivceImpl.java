package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF701Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF701Service")
public class WMSIF701SerivceImpl extends AbstractServiceImpl implements WMSIF701Service{

	@Resource(name = "WMSIF701Dao")
    private WMSIF701Dao dao;
	
	@Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
	
	/**
     * Method ID : list
     * Method 설명 : 오픈몰 주문 리스트 
     * 작성자 : yhku
     * @param model
     * @return
     */
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}

	 public Map<String, Object> list2(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("LIST", dao.list2(model));
		} catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
		return map;
	}
	/**
     *  Method ID 		 :  
     *  Method 설명  	 : 오픈몰 주문 등록
     *  작성자            	 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveListOrderJava(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // IF테이블 PK관련 항목 
            String[] masterSeq	   = new String[tmpCnt]; //묶음번호
            String[] bundleNo      = new String[tmpCnt]; //묶음번호
            String[] uniq          = new String[tmpCnt]; //주문번호
            String[] seq           = new String[tmpCnt]; //버전seq
            String[] skuCd         = new String[tmpCnt]; //상품코드
            String[] prodSeq       = new String[tmpCnt]; //원주문SEQ
            
            
            // 기본프로시저 
            String[] no                 = new String[tmpCnt]; //no(처리)
            String[] reqDt              = new String[tmpCnt]; //출고요청일자(처리)
            String[] custOrdNo          = new String[tmpCnt]; //원주문번호(처리)
            String[] custOrdSeq         = new String[tmpCnt]; //원주문seq(처리)

            String[] trustCustCd        = new String[tmpCnt];
            String[] transCustCd        = new String[tmpCnt];
            String[] transCustTel       = new String[tmpCnt];
            String[] transReqDt         = new String[tmpCnt];
            String[] custCd             = new String[tmpCnt]; //화주코드(처리)

            String[] ordQty             = new String[tmpCnt]; //출고수량(처리) 
            String[] uomCd              = new String[tmpCnt];
            String[] sdeptCd            = new String[tmpCnt];
            String[] salePerCd          = new String[tmpCnt];
            String[] carCd              = new String[tmpCnt];

            String[] drvNm              = new String[tmpCnt];
            String[] dlvSeq             = new String[tmpCnt];
            String[] drvTel             = new String[tmpCnt];
            String[] custLotNo          = new String[tmpCnt];
            String[] blNo               = new String[tmpCnt];

            String[] recDt              = new String[tmpCnt];
            String[] whCd               = new String[tmpCnt];
            String[] makeDt             = new String[tmpCnt];
            String[] timePeriodDay      = new String[tmpCnt];
            String[] workYn             = new String[tmpCnt];

            String[] rjType             = new String[tmpCnt];
            String[] locYn              = new String[tmpCnt];
            String[] confYn             = new String[tmpCnt];
            String[] eaCapa             = new String[tmpCnt];
            String[] inOrdWeight        = new String[tmpCnt];

            String[] itemCd             = new String[tmpCnt]; //상품코드(처리)
            String[] itemNm             = new String[tmpCnt]; //상품명 (처리)
            String[] transCustNm        = new String[tmpCnt];
            String[] transCustAddr      = new String[tmpCnt];
            String[] transEmpNm         = new String[tmpCnt];

            String[] remark             = new String[tmpCnt];
            String[] transZipNo         = new String[tmpCnt];
            String[] etc2               = new String[tmpCnt];
            String[] unitAmt            = new String[tmpCnt];
            String[] transBizNo         = new String[tmpCnt];

            String[] inCustAddr         = new String[tmpCnt];
            String[] inCustCd           = new String[tmpCnt];
            String[] inCustNm           = new String[tmpCnt];
            String[] inCustTel          = new String[tmpCnt];
            String[] inCustEmpNm        = new String[tmpCnt];

            String[] expiryDate         = new String[tmpCnt];
            String[] salesCustNm        = new String[tmpCnt]; //수령인명(처리)
            String[] zip                = new String[tmpCnt]; //수령인우편번호(처리)
            String[] addr               = new String[tmpCnt]; //수령인주소(처리)
            String[] addr2              = new String[tmpCnt];

            String[] phone1             = new String[tmpCnt]; //수령인핸드폰번호(처리)
            String[] etc1               = new String[tmpCnt]; 
            String[] unitNo             = new String[tmpCnt]; // : B2C(처리)
            String[] phone2             = new String[tmpCnt]; //수령인 번호2 (처리)
            String[] buyCustNm          = new String[tmpCnt]; 

            String[] buyPhone1          = new String[tmpCnt]; 
            String[] salesCompanyNm     = new String[tmpCnt]; //eQtipTp ???
            String[] ordDegree          = new String[tmpCnt]; //등록차수(처리)
            String[] bizCond            = new String[tmpCnt]; //업태
            String[] bizType            = new String[tmpCnt]; //업종

            String[] bizNo              = new String[tmpCnt]; //사업자등록번호
            String[] custType           = new String[tmpCnt]; //화주타입
            String[] dataSenderNm       = new String[tmpCnt]; //쇼핑몰(처리)
            String[] legacyOrgOrdNo     = new String[tmpCnt]; //쇼핑몰주문번호(처리)
            String[] custSeq            = new String[tmpCnt];

            String[] ordDesc            = new String[tmpCnt]; //ord_desc
            String[] dlvMsg1            = new String[tmpCnt]; //배송메세지(처리)
            String[] dlvMsg2            = new String[tmpCnt];

            String[] LC_ID              = new String[tmpCnt];
            String[] USER_NO            = new String[tmpCnt];
            String[] WORK_IP            = new String[tmpCnt];
            

            String[] MSG_COCD            = new String[tmpCnt]; //callback용
            String[] MSG_NAME            = new String[tmpCnt]; //callback용

            for (int i = 0; i < tmpCnt; i++) {
            	String NO                  = Integer.toString(i+1);//no(처리)
            	String REQ_DT              = (String)model.get("REQ_DT" + i);//입/출고요청일자(처리)
            	String CUST_ORD_NO         = (String)model.get("BUNDLE_NO" + i);//원주문번호(처리)
            	String CUST_ORD_SEQ        = (String)model.get("PROD_SEQ" + i);//원주문seq(처리)
            	                                
            	String TRUST_CUST_CD       = "";
            	String TRANS_CUST_CD       = "";
            	String TRANS_CUST_TEL      = "";
            	String TRANS_REQ_DT        = "";
            	String CUST_CD             = (String)model.get("colCUST_CD" + i);//화주코드(처리)
            	                                
            	String ORD_QTY             = (String)model.get("ORD_QTY" + i);//출고수량(처리)
            	String UOM_CD              = "";
            	String SDEPT_CD            = "";// OP030.SDEPT_CD 사업부코드
            	String SALE_PER_CD         = "";// OP030.SALE_PER_CD 담당영업사원번호
            	String CAR_CD              = "";
            	                                
            	String DRV_NM              = "";
            	String DLV_SEQ             = "";
            	String DRV_TEL             = "";
            	String CUST_LOT_NO         = "";
            	String BL_NO               = "";
            	                                
            	String REC_DT              = "";
            	String WH_CD               = "";
            	String MAKE_DT             = "";
            	String TIME_PERIOD_DAY     = "";
            	String WORK_YN             = "";
            	                                
            	String RJ_TYPE             = "";
            	String LOC_YN              = "";
            	String CONF_YN             = "";
            	String EA_CAPA             = "";//OP011.EA_CAPA 개당용적
            	String IN_ORD_WEIGHT       = "";
            	                                
            	String ITEM_CD             = (String)model.get("SKU_CD" + i);//상품코드(처리)
            	String ITEM_NM             = (String)model.get("PROD_NAME" + i);//상품명(처리)
            	String TRANS_CUST_NM       = "";
            	String TRANS_CUST_ADDR     = "";
            	String TRANS_EMP_NM        = "";
            	                                
            	String REMARK              = (String)model.get("SHOP_ID" + i); // op11 : ord_desc : 비고1 => 쇼핑몰계정으로 사용 
            	String TRANS_ZIP_NO        = "";
            	String ETC2                = ""; // OM.ORD_DESC, OP10.ORD_DESC , OP11.ETC2 
            	String UNIT_AMT            = "";
            	String TRANS_BIZ_NO        = "";
            	                                
            	String IN_CUST_ADDR        = ""; //OP030.IN_CUST_ADDR 입하처주소
            	String IN_CUST_CD          = ""; //OP030.IN_CUST_CD 입하처코드
            	String IN_CUST_NM          = "";
            	String IN_CUST_TEL         = "";
            	String IN_CUST_EMP_NM      = ""; //OP030.IN_CUST_ADDR 입하처담당자
            	                                
            	String EXPIRY_DATE         = "";
            	String SALES_CUST_NM       = (String)model.get("TO_NAME" + i);//수령인명(처리)
            	String ZIP                 = (String)model.get("TO_ZIPCD" + i);//수령인우편번호(처리)
            	String ADDR                = (String)model.get("TO_ADDR1" + i);//수령인주소(처리)
            	String ADDR2               = "";
            	                                
            	String PHONE1              = (String)model.get("TO_HTEL" + i);//수령인핸드폰번호(처리)
            	String ETC1                = (String)model.get("SHOP_ORD_NO" + i);//임시 :쇼핑몰주문번호(처리) 넣음  om: etc2
            	String UNIT_NO             = (String)model.get("colUNIT_NO" + i); //:B2C(처리)
            	String PHONE2              = (String)model.get("TO_TEL" + i);//수령인번호2(처리)
            	String BUY_CUST_NM         = "";
            	                                
            	String BUY_PHONE1          = "";
            	String SALES_COMPANY_NM    = "PLAYAUTO";//om : QTIO_TP???
            	String ORD_DEGREE          = (String)model.get("colORD_DEGREE" + i);//등록차수(처리)
            	String BIZ_COND            = "";//업태
            	String BIZ_TYPE            = "";//업종
            	                                
            	String BIZ_NO              = "";//사업자등록번호
            	String CUST_TYPE           = "";//화주타입
            	String DATA_SENDER_NM      = (String)model.get("SHOP_NAME" + i);//쇼핑몰(처리)
            	String LEGACY_ORG_ORD_NO   = (String)model.get("UNIQ" + i)+"-"+(String)model.get("SEQ" + i); //unit-seq(version)
            	String CUST_SEQ            = "";// OP010  :  BL_NO
            	                                
            	String ORD_DESC            = "";//ord_desc (X)
            	String DLV_MSG1            = (String)model.get("SHIP_MSG" + i);//배송메세지(처리)
            	String DLV_MSG2            = "";
            	
            	
            	//변수에 닮음. 
            	no[i]                      = NO;                 //no(처리)
            	reqDt[i]                   = REQ_DT;             //출고요청일자(처리)
            	custOrdNo[i]               = CUST_ORD_NO;        //원주문번호(처리)
            	custOrdSeq[i]              = CUST_ORD_SEQ;       //원주문seq(처리)
            	                             
            	trustCustCd[i]             = TRUST_CUST_CD;
            	transCustCd[i]             = TRANS_CUST_CD;
            	transCustTel[i]            = TRANS_CUST_TEL;
            	transReqDt[i]              = TRANS_REQ_DT;
            	custCd[i]                  = CUST_CD;            //화주코드(처리)
            	                             
            	ordQty[i]                  = ORD_QTY;            //출고수량(처리)
            	uomCd[i]                   = UOM_CD;
            	sdeptCd[i]                 = SDEPT_CD;
            	salePerCd[i]               = SALE_PER_CD;
            	carCd[i]                   = CAR_CD;
            	                             
            	drvNm[i]                   = DRV_NM;
            	dlvSeq[i]                  = DLV_SEQ;
            	drvTel[i]                  = DRV_TEL;
            	custLotNo[i]               = CUST_LOT_NO;        // 고객로뜨번호 
            	blNo[i]                    = BL_NO;
            	                             
            	recDt[i]                   = REC_DT;
            	whCd[i]                    = WH_CD;
            	makeDt[i]                  = MAKE_DT;
            	timePeriodDay[i]           = TIME_PERIOD_DAY;
            	workYn[i]                  = WORK_YN;
            	                             
            	rjType[i]                  = RJ_TYPE;
            	locYn[i]                   = LOC_YN;
            	confYn[i]                  = CONF_YN;
            	eaCapa[i]                  = EA_CAPA;
            	inOrdWeight[i]             = IN_ORD_WEIGHT;
            	                             
            	itemCd[i]                  = ITEM_CD;            //상품코드(처리)
            	itemNm[i]                  = ITEM_NM;            //상품명(처리)
            	transCustNm[i]             = TRANS_CUST_NM;
            	transCustAddr[i]           = TRANS_CUST_ADDR;
            	transEmpNm[i]              = TRANS_EMP_NM;
            	                             
            	remark[i]                  = REMARK;
            	transZipNo[i]              = TRANS_ZIP_NO;
            	etc2[i]                    = ETC2;
            	unitAmt[i]                 = UNIT_AMT;
            	transBizNo[i]              = TRANS_BIZ_NO;
            	                             
            	inCustAddr[i]              = IN_CUST_ADDR;
            	inCustCd[i]                = IN_CUST_CD;
            	inCustNm[i]                = IN_CUST_NM;
            	inCustTel[i]               = IN_CUST_TEL;
            	inCustEmpNm[i]             = IN_CUST_EMP_NM;
            	                             
            	expiryDate[i]              = EXPIRY_DATE;
            	salesCustNm[i]             = SALES_CUST_NM;      //수령인명(처리)
            	zip[i]                     = ZIP;                //수령인우편번호(처리)
            	addr[i]                    = ADDR;               //수령인주소(처리)
            	addr2[i]                   = ADDR2;
            	                             
            	phone1[i]                  = PHONE1;             //수령인핸드폰번호(처리)
            	etc1[i]                    = ETC1;
            	unitNo[i]                  = UNIT_NO;            //:B2C(처리)
            	phone2[i]                  = PHONE2;             //수령인번호2(처리)
            	buyCustNm[i]               = BUY_CUST_NM;
            	                             
            	buyPhone1[i]               = BUY_PHONE1;
            	salesCompanyNm[i]          = SALES_COMPANY_NM;   //eQtipTp???
            	ordDegree[i]               = ORD_DEGREE;         //등록차수(처리)
            	bizCond[i]                 = BIZ_COND;           //업태
            	bizType[i]                 = BIZ_TYPE;           //업종
            	                             
            	bizNo[i]                   = BIZ_NO;             //사업자등록번호
            	custType[i]                = CUST_TYPE;          //화주타입
            	dataSenderNm[i]            = DATA_SENDER_NM;     //쇼핑몰(처리)
            	legacyOrgOrdNo[i]          = LEGACY_ORG_ORD_NO;  //쇼핑몰주문번호(처리)
            	custSeq[i]                 = CUST_SEQ;
            	                             
            	ordDesc[i]                 = ORD_DESC;           //ord_desc
            	dlvMsg1[i]                 = DLV_MSG1;           //배송메세지(처리)
            	dlvMsg2[i]                 = DLV_MSG2;
            	
            	
            	// IF테이블 PK관련 항목 
            	masterSeq[i]      			= (String)model.get("MASTER_SEQ" + i); //묶음번호
                bundleNo[i]      			= (String)model.get("BUNDLE_NO" + i); //묶음번호
                uniq[i]          			= (String)model.get("UNIQ" + i); //주문번호
                seq[i]           			= (String)model.get("SEQ" + i); //버전seq
                skuCd[i]         			= (String)model.get("SKU_CD" + i); //상품코드
                prodSeq[i]       			= (String)model.get("PROD_SEQ" + i); //원주문SEQ
            	                                                     
            }
          
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("vrOrdType"		  , (String)model.get("colI_ORD_TYPE")); //VARCHAR 주문타입 
            modelIns.put("vrOrdSubtype"       , (String)model.get("colI_ORD_SUBTYPE"));//VARCHAR 출고유형(처리)                                            
            modelIns.put("no"                 , no               );//no(처리)                                                  
            modelIns.put("reqDt"              , reqDt            );//출고요청일자(처리)                                        
            modelIns.put("custOrdNo"          , custOrdNo        );//원주문번호(처리)                                          
            modelIns.put("custOrdSeq"         , custOrdSeq       );//원주문seq(처리)                                           
                                                                       
            modelIns.put("trustCustCd"        , trustCustCd      );                                                            
            modelIns.put("transCustCd"        , transCustCd      );                                                            
            modelIns.put("transCustTel"       , transCustTel     );                                                            
            modelIns.put("transReqDt"         , transReqDt       );                                                            
            modelIns.put("custCd"             , custCd           );//화주코드(처리)                                            
                                                                       
            modelIns.put("ordQty"             , ordQty           );//출고수량(처리)                                            
            modelIns.put("uomCd"              , uomCd            );                                                            
            modelIns.put("sdeptCd"            , sdeptCd          );                                                            
            modelIns.put("salePerCd"          , salePerCd        );                                                            
            modelIns.put("carCd"              , carCd            );                                                            
                                                                       
            modelIns.put("drvNm"              , drvNm            );                                                            
            modelIns.put("dlvSeq"             , dlvSeq           );                                                            
            modelIns.put("drvTel"             , drvTel           );                                                            
            modelIns.put("custLotNo"          , custLotNo        );                                                            
            modelIns.put("blNo"               , blNo             );                                                            
                                                                       
            modelIns.put("recDt"              , recDt            );                                                            
            modelIns.put("whCd"               , whCd             );                                                            
            modelIns.put("makeDt"             , makeDt           );                                                            
            modelIns.put("timePeriodDay"      , timePeriodDay    );                                                            
            modelIns.put("workYn"             , workYn           );                                                            
                                                                       
            modelIns.put("rjType"             , rjType           );                                                            
            modelIns.put("locYn"              , locYn            );                                                            
            modelIns.put("confYn"             , confYn           );                                                            
            modelIns.put("eaCapa"             , eaCapa           );                                                            
            modelIns.put("inOrdWeight"        , inOrdWeight      );                                                            
                                                                       
            modelIns.put("itemCd"             , itemCd           );//상품코드(처리)                                            
            modelIns.put("itemNm"             , itemNm           );//상품명(처리)                                              
            modelIns.put("transCustNm"        , transCustNm      );                                                            
            modelIns.put("transCustAddr"      , transCustAddr    );                                                            
            modelIns.put("transEmpNm"         , transEmpNm       );                                                            
                                                                       
            modelIns.put("remark"             , remark           );                                                            
            modelIns.put("transZipNo"         , transZipNo       );                                                            
            modelIns.put("etc2"               , etc2             );                                                            
            modelIns.put("unitAmt"            , unitAmt          );                                                            
            modelIns.put("transBizNo"         , transBizNo       );                                                            
                                                                       
            modelIns.put("inCustAddr"         , inCustAddr       );                                                            
            modelIns.put("inCustCd"           , inCustCd         );                                                            
            modelIns.put("inCustNm"           , inCustNm         );                                                            
            modelIns.put("inCustTel"          , inCustTel        );                                                            
            modelIns.put("inCustEmpNm"        , inCustEmpNm      );                                                            
                                                                       
            modelIns.put("expiryDate"         , expiryDate       );                                                            
            modelIns.put("salesCustNm"        , salesCustNm      );//수령인명(처리)                                            
            modelIns.put("zip"                , zip              );//수령인우편번호(처리)                                      
            modelIns.put("addr"               , addr             );//수령인주소(처리)                                          
            modelIns.put("addr2"              , addr2            );                                                            
                                                                       
            modelIns.put("phone1"             , phone1           );//수령인핸드폰번호(처리)                                    
            modelIns.put("etc1"               , etc1             );                                                            
            modelIns.put("unitNo"             , unitNo           );//:B2C(처리)                                                
            modelIns.put("phone2"             , phone2           );//수령인번호2(처리)                                         
            modelIns.put("buyCustNm"          , buyCustNm        );                                                            
                                                                       
            modelIns.put("buyPhone1"          , buyPhone1        );                                                            
            modelIns.put("salesCompanyNm"     , salesCompanyNm   );//eQtipTp???                                                
            modelIns.put("ordDegree"          , ordDegree        );//등록차수(처리)                                            
            modelIns.put("bizCond"            , bizCond          );//업태                                                      
            modelIns.put("bizType"            , bizType          );//업종                                                      
                                                                       
            modelIns.put("bizNo"              , bizNo            );//사업자등록번호                                            
            modelIns.put("custType"           , custType         );//화주타입                                                  
            modelIns.put("dataSenderNm"       , dataSenderNm     );//쇼핑몰(처리)                                              
            modelIns.put("legacyOrgOrdNo"     , legacyOrgOrdNo   );//쇼핑몰주문번호(처리)                                      
            modelIns.put("custSeq"            , custSeq          );                                                            
                                                                       
            modelIns.put("ordDesc"            , ordDesc          );//ord_desc                                                  
            modelIns.put("dlvMsg1"            , dlvMsg1          );//배송메세지(처리)                                          
            modelIns.put("dlvMsg2"            , dlvMsg2          );                                                            
                                                                       
            modelIns.put("LC_ID",  	 (String)model.get("colI_LC_ID"));  //VARCHAR 
            modelIns.put("WORK_IP",  (String)model.get("colI_WORK_IP")); //VARCHAR 
            modelIns.put("USER_NO",  (String)model.get("colI_USER_NO")); //VARCHAR 
            
            System.out.println(modelIns);
            
            modelIns = (Map<String, Object>)dao030.saveExcelOrderB2TS(modelIns);
            ServiceUtil.isValidReturnCode("WMSIF701", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

           if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
            	//프로시져에 보낼것들 다담는다
                Map<String, Object> modelCallback = new HashMap<String, Object>();
                modelCallback.put("I_MASTER_SEQ"	, masterSeq);
                modelCallback.put("I_BUNDLE_NO"		, bundleNo);
                modelCallback.put("I_UNIQ"			, uniq);
                modelCallback.put("I_SEQ"			, seq);
                modelCallback.put("I_SKU_CD"		, skuCd);
                modelCallback.put("I_PROD_SEQ"		, prodSeq);
                modelCallback.put("I_WORK_IP"		, (String)model.get("colI_WORK_IP"));
                modelCallback.put("I_USER_NO"		, (String)model.get("colI_USER_NO"));
                
            	//System.out.println("CallBack");
            	dao.saveOrdPlayAutoCallback(modelCallback);
           }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    
    public static String subStrByte(String str, int cutlen) {
        if(!str.isEmpty()) {
            str = str.trim();
            if(str.getBytes().length <= cutlen) {
                return str;
            } else {
                StringBuffer sbStr = new StringBuffer(cutlen);
                int nCnt = 0;
                for(char ch: str.toCharArray())
                {
                    nCnt += String.valueOf(ch).getBytes().length;
                    if(nCnt > cutlen)  break;
                    sbStr.append(ch);
                }
                return sbStr.toString() + "...";
            }
        }
        else {
            return "";
        }
    }
    
    
    /**
     * Method ID : sapbp
     * Method 설명 : sapbp 마스터
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> sapbp(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.sapbp(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID : crudSap
     * Method 설명 : sap 마스터 CRUD
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> crudSap(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("USER_ID", 			model.get("USER_ID" + i));
	             modelDt.put("MALL_ID", 			model.get("MALL_ID" + i));
	             modelDt.put("PARTNER_ID", 			model.get("PARTNER_ID" + i));
	             modelDt.put("CUST_CD", 			model.get("CUST_CD" + i));
	             modelDt.put("PLANT_CD", 			model.get("PLANT_CD" + i));
	             modelDt.put("SAP_BP_CODE", 		model.get("SAP_BP_CODE" + i));
	             modelDt.put("SAP_BP_NAME", 		model.get("SAP_BP_NAME" + i));
	             modelDt.put("STORAGE_LOCATION", 	model.get("STORAGE_LOCATION" + i));
	             modelDt.put("ETC", 				model.get("ETC" + i));
	             modelDt.put("WMS_ORD_FLAG", 		model.get("WMS_ORD_FLAG" + i));
	             
	             modelDt.put("SYS_REG_USER", 		model.get("SYS_REG_USER"));
	             
	             String stGubun = (String)model.get("ST_GUBUN" + i);
	             if("UPDATE".equals(stGubun)){
	            	 dao.saveSap(modelDt);
	             }else if ("INSERT".equals(stGubun)){
	            	 dao.insertSap(modelDt);
	             }else if ("DELETE".equals(stGubun)){
	            	 dao.deleteSap(modelDt);
	             }
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : playautoListDelete
     * Method 설명 : playautoListDelete
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> playautoListDelete(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		int errCnt = 0;
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
				
				 Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("MASTER_SEQ", 			model.get("MASTER_SEQ" + i));
	             modelDt.put("BUNDLE_NO", 			model.get("BUNDLE_NO" + i));
	             modelDt.put("UNIQ", 				model.get("UNIQ" + i));
	             modelDt.put("SEQ", 				model.get("SEQ" + i));
	             modelDt.put("PROD_SEQ", 			model.get("PROD_SEQ" + i));
	             modelDt.put("LC_ID", 				(String)model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("CUST_ID", 			model.get("colCUST_ID"));
	             modelDt.put("USER_NO", 			(String)model.get(ConstantIF.SS_USER_NO));

	             dao.playautoListDelete(modelDt);
    		}
    		
    		map.put("errCnt", errCnt);
    		map.put("MSG_ORA", "");
    		map.put("MSG", MessageResolver.getMessage("delete.success"));
    		
    	} catch (Exception e) {
    		map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("MSG_ORA", e.getMessage());
    		map.put("errCnt", "1");
    		throw e;
    		
    	}
    	
    	return map;
    }
    
  
	
	/*-
	 * Method ID	: getOpenMallApiAuthMaster
	 * Method 설명	: 오픈몰 API AUTH 마스터 리스트
	 * @param 
	 * @return
	 */
    public Map<String, Object> getOpenMallApiAuthMaster(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_APIAUTH", dao.getOpenMallApiAuthMaster(model));
		return map;
	}
    
    /**
     * Method ID   : deleteOrder
     * Method 설명    : 출고주문삭제
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //int errCnt = 1;
        String errMsg = MessageResolver.getMessage("delete.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId 		= new String[tmpCnt];                
                String[] ordSeq	 	= new String[tmpCnt];
                String[] masterSeq 	= new String[tmpCnt];  
                String[] bundleNo 	= new String[tmpCnt];  
                String[] uniq		= new String[tmpCnt];  
                String[] seq	 	= new String[tmpCnt];  
                String[] skuCd	 	= new String[tmpCnt];  
                String[] prodSeq 	= new String[tmpCnt];  
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    			= (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   			= (String)model.get("ORD_SEQ"+i);
                    masterSeq[i]   			= (String)model.get("MASTER_SEQ"+i);
                    bundleNo[i]   			= (String)model.get("BUNDLE_NO"+i);
                    uniq[i]   				= (String)model.get("UNIQ"+i);
                    seq[i]   				= (String)model.get("SEQ"+i);
                    skuCd[i]   				= (String)model.get("SKU_CD"+i);
                    prodSeq[i]   			= (String)model.get("PROD_SEQ"+i);
                    
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                
                //session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

				//dao
                modelIns = (Map<String, Object>)dao030.deleteOrder(modelIns);	
                ServiceUtil.isValidReturnCode("WMSIF701", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                
                System.out.println("String.valueOf(modelIns.get(O_MSG_CODE))_____ : " + String.valueOf(modelIns.get("O_MSG_CODE")));
                if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
                	//프로시져에 보낼것들 다담는다
                    Map<String, Object> modelCallback = new HashMap<String, Object>();
                    modelCallback.put("I_MASTER_SEQ"	, masterSeq);
                    modelCallback.put("I_BUNDLE_NO"		, bundleNo);
                    modelCallback.put("I_UNIQ"			, uniq);
                    modelCallback.put("I_SEQ"			, seq);
                    modelCallback.put("I_SKU_CD"		, skuCd);
                    modelCallback.put("I_PROD_SEQ"		, prodSeq);
                    modelCallback.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelCallback.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                    
                	dao.deleteOrdPlayAutoCallback(modelCallback);
               }
                
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
}
