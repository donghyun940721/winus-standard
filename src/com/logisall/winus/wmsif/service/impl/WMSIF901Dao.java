package com.logisall.winus.wmsif.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@SuppressWarnings("unchecked")
@Repository
public class WMSIF901Dao extends SqlMapAbstractDAO {
  
  @Autowired
  private SqlMapClient sqlMapClientWinusIf2;

  /**
   * Method ID : list Method 설명 : 작성자 : kih
   * 
   * @param model
   * @return
   * @throws SQLException
   */
  
  public GenericResultSet list(Map<String, Object> model) throws SQLException {
//    GenericResultSet grs = new GenericResultSet();
//    List<Object> resultList= sqlMapClientWinusIf2.queryForList("wmsif901.list", model);
//    grs.setTotCnt(resultList.size());
//    grs.setList(resultList);
    return executeQueryWq("wmsif901.list", model);
  }

  /**
   * Method ID : listE2 Method 설명 : 작성자 : kih
   * 
   * @param model
   * @return
   * @throws SQLException 
   */
  public GenericResultSet listE2(Map<String, Object> model) throws SQLException {
    GenericResultSet grs = new GenericResultSet();
    List<Object> resultList= sqlMapClientWinusIf2.queryForList("wmsif901.listE2", model);
    grs.setTotCnt(resultList.size());
    grs.setList(resultList);
    return grs;
  }

  public Object listE3(Map<String, Object> model) throws SQLException {
    GenericResultSet grs = new GenericResultSet();
    List<Object> resultList= sqlMapClientWinusIf2.queryForList("wmsif901.listE3", model);
    grs.setTotCnt(resultList.size());
    grs.setList(resultList);
    return grs;
  }

  public Object listE4(Map<String, Object> model) throws SQLException {
    GenericResultSet grs = new GenericResultSet();
    List<Object> resultList= sqlMapClientWinusIf2.queryForList("wmsif901.listE4", model);
    grs.setTotCnt(resultList.size());
    grs.setList(resultList);
    return grs;
  }

  public Object listE5(Map<String, Object> model) throws SQLException {
    GenericResultSet grs = new GenericResultSet();
    List<Object> resultList= sqlMapClientWinusIf2.queryForList("wmsif901.listE5", model);
    grs.setTotCnt(resultList.size());
    grs.setList(resultList);
    return grs;
  }

  /**
   * Method ID : insertMobisInbAsn Method 설명 : 모비스 AL 입고 ASN 작성자 : kih
   * 
   * @param model
   * @return
   */
  public Map<String, Object> insertMobisInbAsn(Map<String, Object> model) {
    executeUpdate("wmsif901.pk_wmsif111.sp_mobis_al_asn_insert_order", model);
    return model;
  }

  /**
   * Method ID : insertMobisOutbOrder Method 설명 : 모비스 AL 입고 ASN 작성자 : kih
   * 
   * @param model
   * @return
   */
  public Map<String, Object> insertMobisOutbOrder(Map<String, Object> model) {
    executeUpdate("wmsif901.pk_wmsif111.sp_mobis_al_ship_insert_order", model);
    return model;
  }

  public Map<String, Object> insertMobisOutbOrderConfirm(Map<String, Object> model) {
    executeUpdate("wmsif901.pk_wmsif111.sp_mobis_al_ship_complete", model);
    return model;
  }

  public Map<String, Object> insertMobisModuleOutbOrderResults(Map<String, Object> model) {
    executeUpdate("wmsif901.pk_wmsif111.sp_mobis_al_ship_hu_complete", model);
    return model;
  }

  public Map<String, Object> insertMobisInbAsnMulti(Map<String, Object> model) {
    executeUpdate("wmsif901.pk_wmsif111.sp_mobis_al_asn_insert_order_multi", model);
    return model;
  }
  public void deleteOutboundIf(Map<String, Object> model) throws SQLException {
    sqlMapClientWinusIf2.delete("wmsif901.deleteOutboundIf", model);
  }
  public void deleteOutboundOrderIf(Map<String, Object> model) throws SQLException {
      sqlMapClientWinusIf2.update("wmsif901.deleteOutboundOrderIf", model);
    }

  public void deleteMobisHuComtoMobisHeader(Map<String, Object> model) throws SQLException {
    sqlMapClientWinusIf2.delete("wmsif901.deleteMobisHuComtoMobisHeader", model);
  }

  public void deleteMobisHuComtoMobisDetail(Map<String, Object> model) throws SQLException {
    sqlMapClientWinusIf2.delete("wmsif901.deleteMobisHuComtoMobisDetail", model);
  }
}


