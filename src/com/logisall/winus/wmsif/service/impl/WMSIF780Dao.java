package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF780Dao")
public class WMSIF780Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  	: listE1
     * Method 설명   	: 상품정보 조회 
     * 작성자               : schan
     * @param   model
     * @return
     */
    public List listE1(Map<String, Object> model) {
        return executeQueryForList("wmsif780.listE1", model);
    }
    
    /**
     * Method ID    : listE2
     * Method 설명    : 입고정보 조회 
     * 작성자               : schan
     * @param   model
     * @return
     */
    public List listE2(Map<String, Object> model) {
        return executeQueryForList("wmsif780.listE2", model);
    }
    
    /**
     * Method ID    : listE3
     * Method 설명    : 출고정보 조회 
     * 작성자               : schan
     * @param   model
     * @return
     */
    public List listE3(Map<String, Object> model) {
        return executeQueryForList("wmsif780.listE3", model);
    }
    
    /**
     * Method ID    : listE4
     * Method 설명    : 입고결과 조회 
     * 작성자               : schan
     * @param   model
     * @return
     */
    public List listE4(Map<String, Object> model) {
        return executeQueryForList("wmsif780.listE4", model);
    }
    
    /**
     * Method ID    : listE5
     * Method 설명    : 출고결과 조회 
     * 작성자               : schan
     * @param   model
     * @return
     */
    public List listE5(Map<String, Object> model) {
        return executeQueryForList("wmsif780.listE5", model);
    }
    
    /**
     * Method ID    : syncItem
     * Method 설명    : 상품정보 동기화
     * 작성자               : schan
     * @param   model
     * @return
     */
    public Object syncItem(Map<String, Object> model) {
        executeUpdate("wmsif780.pk_wmsif203.sp_goodmd_sav_item", model);
        return model;
    }
    
    /**
     * Method ID    : syncInOrder
     * Method 설명    : 입고주문정보 동기화
     * 작성자               : schan
     * @param   model
     * @return
     */
    public Object syncInOrder(Map<String, Object> model) {
        executeUpdate("wmsif780.pk_wmsif203.sp_goodmd_sav_in_order", model);
        return model;
    }
    
    /**
     * Method ID    : syncOutOrder
     * Method 설명    : 출고주문정보 동기화
     * 작성자               : schan
     * @param   model
     * @return
     */
    public Object syncOutOrder(Map<String, Object> model) {
        executeUpdate("wmsif780.pk_wmsif203.sp_goodmd_sav_out_order", model);
        return model;
    }
    
    
    
}


