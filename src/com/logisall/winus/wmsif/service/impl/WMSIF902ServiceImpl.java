package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.common.service.impl.CmBeanDao;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.impl.TMSYS010Dao;
import com.logisall.winus.wmsif.model.Wellfood;
import com.logisall.winus.wmsif.service.WMSIF902Service;
import com.logisall.winus.wmsms.service.WMSMS090Service;
import com.logisall.winus.wmsms.service.impl.WMSMS011Dao;
import com.logisall.winus.wmsms.service.impl.WMSMS100Dao;
import com.logisall.winus.wmsom.service.impl.OutOrderRegDao;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsop.service.WMSOP999Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
public class WMSIF902ServiceImpl implements WMSIF902Service {
	private final Log log = LogFactory.getLog(this.getClass());

	// 0000001262는 테스트용
	private static final String[] WELLFOOD_CENTERS = { "0000004460", "0000004461", "0000001262" };

	// 0000553745는 테스트용
	private static final String[] WELLFOOD_CUSTS = { "0000553456", "0000553457", "0000553745" };

	// 대표UOM 일단 EA 고정.
	private static final String[] WELLFOOD_REP_UOM_ID = { "0000018502", "0000018513", "0000001219" };

	private static final String WELLFOOD_BUSAN = "WFD"; // 웰푸드 화주코드
	private static final String WELLFOOD_NAME = "롯데웰푸드";
//  private static final String WELLFOOD_CODE = "0000553456";

	@Autowired
	private CmBeanDao cmBeanDao;

	@Autowired 
	private OutOrderRegDao outOrderRegDao;
	
	@Autowired
	private WMSIF902Dao wmsif902Dao;

	@Autowired
	private TMSYS010Dao tmsys010Dao;

	@Autowired
	private WMSMS011Dao wmsms011Dao;

	@Autowired
	private WMSMS100Dao wmsms100Dao;
	
	@Autowired
    private WMSOP030Dao wmsop030Dao;
	
	@Autowired
    private WMSOP030Service wmsop030Service;

	@Autowired
	private WMSMS090Service wmsms090Service;
	@Autowired
	private WMSOP999Service wmsop999Service;

//  @Autowired
//  private WMSMS090Dao wmsms090Dao;

	@Override
	public Map listE01(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();

//    List listWqrs = getSqlMapClientTemplate().queryForList(statementName, obj);

//    wqrs.setCpage(1);
//    wqrs.setTpage(1);

		Map<String, Object> map = new HashMap<String, Object>();
		String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
		// String testCustId = "0000020164"; //LOGISALL > 0000020164 테스트 동안만...
		model.put("LC_ID", lcId);
		model.put("CUST_ID", WELLFOOD_BUSAN);
		List<Object> resultList = wmsif902Dao.listE01(model);
		wqrs.setTotCnt(resultList.size());
		wqrs.setList(resultList);

		map.put("LIST", wqrs);
		return map;
	}

	@Override
	public Map<String, Object> saveCommonCodesByQuery(Map<String, Object> model, Map<String, Object> reqMap)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		List<Map<String, Object>> headerList = wmsif902Dao.selectCommonCodeHeaderList(reqMap);
		try {
			for (Map<String, Object> header : headerList) {
				header.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
				header.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
				setCommonCodeHeaderParam(header);

				int checkCnt = tmsys010Dao.checkForInsertCode(header);
				if (checkCnt == 0) {
					tmsys010Dao.insertCode(header);
				} else {
					tmsys010Dao.updateCode(header);
				}
				header.put("IF_YN", "Y");
				wmsif902Dao.updateCommonCodeHeader(header);

				saveCommonCodeAllDetails(header);
			}
		} catch (BizException be) {
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());
		} catch (Exception e) {
			m.put("errCnt", 1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		m.put("errCnt", 0);
		m.put("MSG", MessageResolver.getMessage("save.success"));
		return m;
	}

	@Override
//    @Transactional("chainedTransactionManager")
	public Map<String, Object> saveCommonCodesByCheckedList(Map<String, Object> model, Map<String, Object> reqMap)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		List<Map<String, Object>> headerList = (List<Map<String, Object>>) reqMap.get("codeHeaderList");
		List<Map<String, Object>> detailList = (List<Map<String, Object>>) reqMap.get("codeDetailList");
		saveCommonCodeHeader(model, headerList);
		updateCommonCodeStats(model, headerList);
		saveCommonCodeDetails(model, detailList);
		m.put("MSG", MessageResolver.getMessage("save.success"));
		m.put("errCnt", "0");
		return m;
	}

	public void saveCommonCodeHeader(Map<String, Object> model, List<Map<String, Object>> headers) throws SQLException {
		for (Map<String, Object> header : headers) {
			header.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
			header.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
			setCommonCodeHeaderParam(header);
			int checkCnt = tmsys010Dao.checkForInsertCode(header);
			if (checkCnt == 0) {
				tmsys010Dao.insertCode(header);
			} else {
				tmsys010Dao.updateCode(header);
			}
		}
	}

	public void saveCommonCodeDetails(Map<String, Object> model, List<Map<String, Object>> details)
			throws SQLException {
		for (int i = 0; i < details.size(); i++) {
			Map<String, Object> detail = details.get(i);
			detail.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
			detail.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
			detail.put("VIEW_SEQ", i);
			setCommonCodeDetailParam(detail);

			int checkCnt = tmsys010Dao.checkForInsertDetailCode(detail);
			if (checkCnt == 0) {
				tmsys010Dao.insertDetailCode(detail);
			} else {
				tmsys010Dao.updateDetailCode(detail);
			}
		}
	}

	public void updateCommonCodeStats(Map<String, Object> model, List<Map<String, Object>> headers)
			throws SQLException {
		for (Map<String, Object> header : headers) {
			header.put("IF_YN", "Y");
			wmsif902Dao.updateCommonCodeHeader(header);
			wmsif902Dao.updateCommonCodeDetail(header);
		}
	}

	public void saveCommonCodeAllDetails(Map<String, Object> header) throws Exception {
		header.put("IF_YN", "N");
		List<Map<String, Object>> detailList = wmsif902Dao.selectCommonCodeDetailList(header);
		for (int i = 0; i < detailList.size(); i++) {
			Map<String, Object> detail = detailList.get(i);
			detail.put("WORK_IP", header.get("WORK_IP"));
			detail.put("SS_USER_NO", header.get("SS_USER_NO"));
			detail.put("VIEW_SEQ", i);
			setCommonCodeDetailParam(detail);
			int checkCnt = tmsys010Dao.checkForInsertDetailCode(detail);
			if (checkCnt == 0) {
				tmsys010Dao.insertDetailCode(detail);
			} else {
				tmsys010Dao.updateDetailCode(detail);
			}
		}

		header.put("IF_YN", "Y");
		wmsif902Dao.updateCommonCodeDetail(header);
	}

	private void setCommonCodeHeaderParam(Map<String, Object> comCodeHeader) {
		String codeType = (String) comCodeHeader.get("CD_GRP");
		String codeTypeNm = (String) comCodeHeader.get("CD_GRP_NM");
		String userType = "100"; // 사용자구분 롯데웰푸드
		String codeLevel = "0"; // 0:삭제가능, 1:마스터삭제가능, 2:삭제불가

		comCodeHeader.put("ORG_CODE_TYPE_CD", codeType);
		comCodeHeader.put("CODE_TYPE_CD", codeType);
		comCodeHeader.put("CODE_TYPE_NM", codeTypeNm);
		comCodeHeader.put("CODE_TYPE", "B");
		comCodeHeader.put("USER_GB", userType);
		comCodeHeader.put("CODE_LEVEL", codeLevel);
		comCodeHeader.put("DEL_YN", "N");
	}

	private void setCommonCodeDetailParam(Map<String, Object> comCodeDetail) {
		String codeType = (String) comCodeDetail.get("CD_GRP");
		String codeCd = (String) comCodeDetail.get("CD_NO");
		String codeNm = (String) comCodeDetail.get("CD_NM");

//    comCodeHeader.put("ORG_CODE_TYPE_CD", codeType);
		comCodeDetail.put("CODE_TYPE_CD", codeType);
		comCodeDetail.put("BA_CODE_CD", codeCd);
		comCodeDetail.put("BA_CODE_CD_O", codeCd);
		comCodeDetail.put("BA_CODE_NM", codeNm);
		comCodeDetail.put("ST_CODE_CD", codeType);
		comCodeDetail.put("CODE_VALUE", codeCd);
		comCodeDetail.put("DEL_YN", "N");
	}

	/**
	 * @category 거래처정보 IF
	 */
	@Override
	public Map<String, Object> saveZsCusts(Map<String, Object> model, Map<String, Object> reqMap) throws BizException {
		String ifYn = "N";
		String outType = "2"; // 거래처 타입 출고처
		Map<String, Object> m = new HashMap<String, Object>();
		reqMap.put("IF_YN", ifYn);
		List<Map<String, Object>> zsCustList = setCustList(reqMap, "zsCustList");
		try {
			for (Map<String, Object> zscust : zsCustList) {
				for (int i = 0; i < WELLFOOD_CENTERS.length; i++) {
					String lcId = new String(WELLFOOD_CENTERS[i]);
					String custId = new String(WELLFOOD_CUSTS[i]);
					zscust.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
					zscust.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
//	          zscust.put("TRUST_CUST_CD", WELLFOOD_BUSAN);
					zscust.put("TRUST_CUST_ID", custId);
					zscust.put("TRUST_CUST_NM", WELLFOOD_NAME);
					zscust.put("CUST_CD", zscust.get("ZSCUST_CD"));
					zscust.put("CUST_NM", zscust.get("NAME1"));
					zscust.put("CUST_ID", zscust.get("ZSCUST_CD"));
					zscust.put("REP_NM", zscust.get("BOSS_NM"));
					zscust.put("TEL", zscust.get("TEL_NO"));
					zscust.put("ZIP", zscust.get("ZIP_CD1"));
					zscust.put("ADDR", zscust.get("ORT01") + " (" + zscust.get("STRAS") + ")");
					zscust.put("CUST_INOUT_TYPE", outType);

					zscust.put("DUTY_NM", zscust.get("EMP_NM") + " (" + zscust.get("ZPERNR") + ")");
					zscust.put("BEST_DATE_YN", "N");
					zscust.put("ASN_YN", "N");
					zscust.put("LC_ID", lcId);
					zscust.put("REG_NO", model.get("SS_USER_NO"));
					zscust.put("UPD_NO", model.get("SS_USER_NO"));
//	          int checkCnt = wmsms011Dao.checkTxtCustId(zscust);
					String winusCustId = wmsif902Dao.selectCustId(zscust);
					if (winusCustId == null) {
						String wmsms010CustId = (String) wmsms011Dao.insertWmsms010(zscust);
						wmsms011Dao.insertWmsms011(zscust);
						// ** 거래처ZONE정보관리 연동 저장 1:1
						Map<String, Object> modelDtMs085Header = new HashMap<String, Object>();
						modelDtMs085Header.put("SS_SVC_NO", lcId);
						modelDtMs085Header.put("CUST_ZONE_NM", zscust.get("CUST_NM"));
						modelDtMs085Header.put("SS_USER_NO", zscust.get("SS_USER_NO"));

						String checkLcClientOrdYn = wmsms011Dao.checkLcClientOrdYn(modelDtMs085Header);
						if (checkLcClientOrdYn.equals("Y")) {
							// ** 거래처ZONE정보관리 연동 저장 1:1 wmsms085 header insert
							String custZoneId = (String) wmsms011Dao.insertMs085Header(modelDtMs085Header);
							// System.out.println("custZoneId : " + custZoneId);
							Map<String, Object> modelDtMs085Detail = new HashMap<String, Object>();
							modelDtMs085Detail.put("WORK_IP", zscust.get("WORK_IP"));
							modelDtMs085Detail.put("CUST_ID", wmsms010CustId);
							modelDtMs085Detail.put("CUST_ZONE_ID", custZoneId);
							modelDtMs085Detail.put("SS_USER_NO", zscust.get("SS_USER_NO"));
							modelDtMs085Detail.put("WORK_SEQ", "1");

							// ** 거래처ZONE정보관리 연동 저장 1:1 wmsms085 detail insert
							wmsms011Dao.insertMs085Detail(modelDtMs085Detail);
						}

					} else {
						zscust.put("CUST_ID", winusCustId);
						wmsms011Dao.update(zscust);
						wmsms011Dao.updateWmsms011(zscust);
					}
				}
				/* IF_YN Y로 업데이트 */
				wmsif902Dao.updateZsCustIfYn(zscust);
			}
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			log.error(e);
			m.put("errCnt", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		return m;
	}

	/**
	 * @category 창고정보 거래처 저장
	 */
	@Override
	public Map<String, Object> saveWarehouses(Map<String, Object> model, Map<String, Object> reqMap) {
		String ifYn = "N";
		String outType = "2"; // 거래처 타입 출고처
		Map<String, Object> m = new HashMap<String, Object>();
		reqMap.put("IF_YN", ifYn);
		List<Map<String, Object>> warehouseList = setCustList(reqMap, "warehouseList");
		try {
			for (Map<String, Object> warehouse : warehouseList) {
				for (int i = 0; i < WELLFOOD_CENTERS.length; i++) {
					String lcId = new String(WELLFOOD_CENTERS[i]);
					String custId = new String(WELLFOOD_CUSTS[i]);
					warehouse.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
					warehouse.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
					warehouse.put("TRUST_CUST_ID", custId);
					warehouse.put("TRUST_CUST_NM", WELLFOOD_NAME);
					warehouse.put("CUST_CD", warehouse.get("CT_CD"));
					warehouse.put("CUST_NM", warehouse.get("CT_NM"));
					warehouse.put("CUST_ID", warehouse.get("CT_CD"));
					warehouse.put("REP_NM", warehouse.get("OWN_NM"));
					warehouse.put("TEL", warehouse.get("TEL_NO"));
					warehouse.put("ZIP", warehouse.get("ZIP_CD"));
					warehouse.put("ADDR", warehouse.get("ADDR1") + " (" + warehouse.get("ADDR2") + ")");
					warehouse.put("CUST_INOUT_TYPE", outType);

//					warehouse.put("DUTY_NM", zscust.get("EMP_NM") + " (" + zscust.get("ZPERNR") + ")");
					warehouse.put("BEST_DATE_YN", "N");
					warehouse.put("ASN_YN", "N");
					warehouse.put("LC_ID", lcId);
					warehouse.put("REG_NO", model.get("SS_USER_NO"));
					warehouse.put("UPD_NO", model.get("SS_USER_NO"));
//	          int checkCnt = wmsms011Dao.checkTxtCustId(zscust);
					String winusCustId = wmsif902Dao.selectCustId(warehouse);
					if (winusCustId == null) {
						String wmsms010CustId = (String) wmsms011Dao.insertWmsms010(warehouse);
						wmsms011Dao.insertWmsms011(warehouse);
						// ** 거래처ZONE정보관리 연동 저장 1:1
						Map<String, Object> modelDtMs085Header = new HashMap<String, Object>();
						modelDtMs085Header.put("SS_SVC_NO", lcId);
						modelDtMs085Header.put("CUST_ZONE_NM", warehouse.get("CUST_NM"));
						modelDtMs085Header.put("SS_USER_NO", warehouse.get("SS_USER_NO"));

						String checkLcClientOrdYn = wmsms011Dao.checkLcClientOrdYn(modelDtMs085Header);
						if (checkLcClientOrdYn.equals("Y")) {
							// ** 거래처ZONE정보관리 연동 저장 1:1 wmsms085 header insert
							String custZoneId = (String) wmsms011Dao.insertMs085Header(modelDtMs085Header);
							// System.out.println("custZoneId : " + custZoneId);
							Map<String, Object> modelDtMs085Detail = new HashMap<String, Object>();
							modelDtMs085Detail.put("WORK_IP", warehouse.get("WORK_IP"));
							modelDtMs085Detail.put("CUST_ID", wmsms010CustId);
							modelDtMs085Detail.put("CUST_ZONE_ID", custZoneId);
							modelDtMs085Detail.put("SS_USER_NO", warehouse.get("SS_USER_NO"));
							modelDtMs085Detail.put("WORK_SEQ", "1");

							// ** 거래처ZONE정보관리 연동 저장 1:1 wmsms085 detail insert
							wmsms011Dao.insertMs085Detail(modelDtMs085Detail);
						}

					} else {
						warehouse.put("CUST_ID", winusCustId);
						wmsms011Dao.update(warehouse);
						wmsms011Dao.updateWmsms011(warehouse);
					}
				}
				/* IF_YN Y로 업데이트 */
				wmsif902Dao.updateWarehouseIfYn(warehouse);
			}
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			log.error(e);
			m.put("errCnt", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		return m;
	}

	private List<Map<String, Object>> setCustList(Map<String, Object> reqMap, String listkey) {
		List<Map<String, Object>> zsCustList = new ArrayList<Map<String, Object>>();
		if (reqMap.containsKey(listkey)) {
			zsCustList = (List<Map<String, Object>>) reqMap.get(listkey);
		} else {
			zsCustList = wmsif902Dao.selectZsCustList(reqMap);
		}

		return zsCustList;
	}

	/**
	 * @category 품목정보 및 포장정보 IF
	 */
	@Override
	public Map<String, Object> saveProducts(Map<String, Object> model, Map<String, Object> reqMap) throws Exception {
		String ifYn = "N";
		Map<String, Object> m = new HashMap<String, Object>();
		reqMap.put("IF_YN", ifYn);

		List<Map<String, Object>> updateProductList = new ArrayList<Map<String, Object>>();

		for (Map<String, Object> wellfoodItem : wmsif902Dao.selectProductList(reqMap)) {
			for (int i = 0; i < WELLFOOD_CENTERS.length; i++) {
				Map<String, Object> winusItem = new HashMap<String, Object>();
				String ItemCode = (String) wellfoodItem.get("MATNR");
				String itemName = (String) wellfoodItem.get("MAKTX");
				String itemUomNm = (String) wellfoodItem.get("MEINS");

				winusItem.put("LC_ID", WELLFOOD_CENTERS[i]);
				winusItem.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
				winusItem.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));

				winusItem.put("CUST_ID", WELLFOOD_CUSTS[i]);
				winusItem.put("MATNR", ItemCode);
				winusItem.put("ITEM_CODE", ItemCode);
				winusItem.put("ITEM_ENG_NM", itemName);
				winusItem.put("ITEM_KOR_NM", itemName);
				winusItem.put("ITEM_SHORT_NM", wellfoodItem.get("ZCUSTXT"));

				winusItem.put("UOM_CD", wellfoodItem.get("MEINS"));
				winusItem.put("ITEM_BAR_CD", wellfoodItem.get("ITM_BCD"));
				winusItem.put("BEST_DATE_TYPE", "1");
				winusItem.put("OUT_LOC_REC", "03"); // 출고추천타입 유통기한
				winusItem.put("BEST_DATE_NUM", wellfoodItem.get("MHDHB"));
				winusItem.put("BEST_DATE_UNIT", wellfoodItem.get("IPRKZ"));

				winusItem.put("SET_ITEM_YN", "N");// 임가공상품여부
				winusItem.put("LOT_USE_YN", "N"); // 입고 LOT 사용여부
				winusItem.put("AUTO_OUT_ORD_YN", "N"); // 출고자동주문사용여부
				winusItem.put("DEL_YN", "N");
				winusItem.put("IF_YN", "N");
				List<Map<String, Object>> winusItemPkgngList = new ArrayList<Map<String, Object>>();
				for (Map<String, Object> productPkgng : wmsif902Dao.selectProductPkgngList(wellfoodItem)) {
					String uomNm = (String) productPkgng.get("MEINH");
					String weightUnit = (String) productPkgng.get("GEWEI");
					BigDecimal weight = new BigDecimal(productPkgng.get("BRGEW").toString());
					int comparisonResult = weight.compareTo(BigDecimal.ZERO);
					if (itemUomNm.equalsIgnoreCase(uomNm)) {
						if ("G".equalsIgnoreCase(weightUnit) && comparisonResult > 0) {
							winusItem.put("ITEM_WGT", weight.divide(weight, 1000));
						}
						winusItem.put("SIZE_W", productPkgng.get("BREIT"));
						winusItem.put("SIZE_H", productPkgng.get("HOEHE"));
						winusItem.put("SIZE_L", productPkgng.get("LAENG"));
						winusItem.put("UNIT_PRICE", Integer.parseInt(productPkgng.get("KBETR1").toString()));
						winusItem.put("PURCHASE_PRICE", Integer.parseInt(productPkgng.get("KBETR2").toString()));
						winusItem.put("SALES_PRICE", Integer.parseInt(productPkgng.get("KBETR3").toString()));
					}
					productPkgng.put("LC_ID", WELLFOOD_CENTERS[i]);
					productPkgng.put("UOM1_CD", productPkgng.get("MEINS"));
					productPkgng.put("UOM2_CD", productPkgng.get("MEINH"));
					productPkgng.put("UOM2_CD_NM", productPkgng.get("MEINH"));
					productPkgng.put("QTY", Integer.parseInt(productPkgng.get("UMREZ").toString()));
					winusItem.put("DEL_YN", "N");
					winusItemPkgngList.add(productPkgng);
				}
				winusItem.put("itemPkgngs", winusItemPkgngList);
				updateProductList.add(winusItem);
			}

			model.put("items", updateProductList);
		}

		wmsms090Service.saveItemMasterV2(model);
		wmsif902Dao.updateProductIfYn(model);
		wmsif902Dao.updateProductPkgngIfYn(model);

		m.put("MSG", MessageResolver.getMessage("save.success"));
		return m;
	}

	/**
	 * @category 품목정보 및 포장정보 IF
	 */
	@Override
	public Map<String, Object> saveCheckedProducts(Map<String, Object> model, Map<String, Object> reqMap) {
		String ifYn = "N";
		Map<String, Object> m = new HashMap<String, Object>();
		reqMap.put("IF_YN", ifYn);

		try {
			List<Map<String, Object>> productList = setProductListParams(model, reqMap);
			model.put("items", productList);
			wmsms090Service.saveItemMasterV2(model);
			wmsif902Dao.updateProductListIfSuccess(productList);

			m.put("errCnt", "0");
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			log.error("e", e);
			m.put("errCnt", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		return m;
	}

	private List<Map<String, Object>> setProductListParams(Map<String, Object> model, Map<String, Object> reqMap) {
		List<Map<String, Object>> productList = (List<Map<String, Object>>) reqMap.get("productList");

		// 웰푸드 창고 품온타입 조회 후 매핑
		reqMap.put("CODE_TYPE_CD", "10006");

		List<Map<String, Object>> updateProductList = new ArrayList<Map<String, Object>>();
		/* WELLFOOD_CENTERS로 선언된 센터와 화주 모두에 품목정보 추가(부산, 수원) */
		for (int i = 0; i < WELLFOOD_CENTERS.length; i++) {
			for (Map<String, Object> product : productList) {
				Map<String, Object> productCopy = new HashMap<String, Object>(product);
				String ItemCode = (String) product.get("MATNR");
				String itemName = (String) product.get("MAKTX");
				String itemUomCd = (String) product.get("H_MEINS");

				String wellfoodItemTempCd = (String) product.get("TMP_DVSN_CD");
				// 웰푸드 창고 온도Type 0:해당없음, 1:실온,2:상온,4:냉장, 5냉동
				String winusTempType = "3";// WINUS 1:냉동, 2:냉장, 3:상온(기본)
				if ("4".equals(wellfoodItemTempCd)) {
					winusTempType = "2";
				}
				if ("5".equals(wellfoodItemTempCd)) {
					winusTempType = "1";
				}
				productCopy.put("LC_ID", WELLFOOD_CENTERS[i]);
				productCopy.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
				productCopy.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));

				productCopy.put("CUST_ID", WELLFOOD_CUSTS[i]);
				productCopy.put("ITEM_CODE", ItemCode);
				productCopy.put("ITEM_ENG_NM", itemName);
				productCopy.put("ITEM_KOR_NM", itemName);
				productCopy.put("ITEM_SHORT_NM", product.get("ZCUSTXT"));

				productCopy.put("REP_UOM_ID", WELLFOOD_REP_UOM_ID[i]);
				productCopy.put("UOM_CD", itemUomCd);
				productCopy.put("ITEM_BAR_CD", product.get("ITM_BCD"));
				productCopy.put("BEST_DATE_TYPE", "1");
				productCopy.put("OUT_LOC_REC", "03"); // 출고추천타입 유통기한
				productCopy.put("BEST_DATE_NUM", product.get("MHDHB"));
				productCopy.put("BEST_DATE_UNIT", product.get("IPRKZ"));

				productCopy.put("SET_ITEM_YN", "N");// 임가공상품여부
				productCopy.put("LOT_USE_YN", "N"); // 입고 LOT 사용여부
				productCopy.put("AUTO_OUT_ORD_YN", "N"); // 출고자동주문사용여부
				productCopy.put("TEMP_TYPE", winusTempType); // 품온구분
				productCopy.put("DEL_YN", "N");
				productCopy.put("IF_YN", "N");

				/* 상품 포장정보 파라미터 세팅 */
				setProductPackageParam(productCopy);

				/* 센터 수 만큼 업데이트할 배열에 객체 저장 */
				updateProductList.add(productCopy);
			}
		}

		return updateProductList;
	}

	private void setProductPackageParam(Map<String, Object> product) throws RuntimeException {
		List<Map<String, Object>> updatePackageList = new ArrayList<Map<String, Object>>();
		String productLcId = product.get("LC_ID").toString();
		String itemUomNm = (String) product.get("H_MEINS");
		List<Map<String, Object>> productPkgngs = (List<Map<String, Object>>) product.get("itemPkgngs");

		for (Map<String, Object> productPkgng : productPkgngs) {
			Map<String, Object> packageCopy = new HashMap<String, Object>(productPkgng);
			String uomNm = (String) productPkgng.get("MEINH");
			String weightUnit = (String) productPkgng.get("GEWEI");
			BigDecimal weight = new BigDecimal(productPkgng.get("BRGEW").toString());
			int comparisonResult = weight.compareTo(BigDecimal.ZERO);
			if (itemUomNm.equalsIgnoreCase(uomNm)) {
				if ("G".equalsIgnoreCase(weightUnit) && comparisonResult > 0) {
					packageCopy.put("ITEM_WGT", weight.divide(weight, 1000));
				}
				packageCopy.put("SIZE_W", productPkgng.get("BREIT"));
				packageCopy.put("SIZE_H", productPkgng.get("HOEHE"));
				packageCopy.put("SIZE_L", productPkgng.get("LAENG"));
				packageCopy.put("UNIT_PRICE", Integer.parseInt(productPkgng.get("KBETR1").toString()));
				packageCopy.put("PURCHASE_PRICE", Integer.parseInt(productPkgng.get("KBETR2").toString()));
				packageCopy.put("SALES_PRICE", Integer.parseInt(productPkgng.get("KBETR3").toString()));
			}
			packageCopy.put("LC_ID", productLcId);
			packageCopy.put("UOM1_CD", productPkgng.get("D_MEINS"));
			packageCopy.put("UOM2_CD", productPkgng.get("MEINH"));
			packageCopy.put("UOM2_CD_NM", productPkgng.get("MEINH"));
			packageCopy.put("QTY", Integer.parseInt(productPkgng.get("UMREZ").toString()));
			packageCopy.put("DEL_YN", "N");

			packageCopy.put("WORK_IP", product.get(ConstantIF.SS_CLIENT_IP));
			packageCopy.put("SS_USER_NO", product.get(ConstantIF.SS_USER_NO));
			updatePackageList.add(packageCopy);
		}

		/* 간편입고 하려면 PLT UOM도 있어야 하므로 강제로 추가... */
		Map<String, Object> pltObj = new HashMap<String, Object>();
		pltObj.put("LC_ID", productLcId);
		pltObj.put("UOM1_CD", "EA");
		pltObj.put("UOM2_CD", "PLT");
		pltObj.put("UOM2_CD_NM", "PLT");
		pltObj.put("D_MEINS", "EA");
		pltObj.put("UOM2_CD_NM", "PLT");
		pltObj.put("QTY", "1000");
		pltObj.put("BRGEW", "1");
		pltObj.put("UMREZ", "1000");
		pltObj.put("DEL_YN", "N");
		pltObj.put("WORK_IP", product.get(ConstantIF.SS_CLIENT_IP));
		pltObj.put("SS_USER_NO", product.get(ConstantIF.SS_USER_NO));
//	pltObj.put("SIZE_W", productPkgng.get("BREIT"));
//	pltObj.put("SIZE_H", productPkgng.get("HOEHE"));
//	pltObj.put("SIZE_L", productPkgng.get("LAENG"));
//	pltObj.put("UNIT_PRICE", Integer.parseInt(productPkgng.get("KBETR1").toString()));
//	pltObj.put("PURCHASE_PRICE", Integer.parseInt(productPkgng.get("KBETR2").toString()));
//	pltObj.put("SALES_PRICE", Integer.parseInt(productPkgng.get("KBETR3").toString()));
		updatePackageList.add(pltObj);
		product.put("itemPkgngs", updatePackageList);
	}

	@Override
	public Map<String, Object> saveInboundOrders(Map<String, Object> model, Map<String, Object> reqMap)
			throws Exception {

		String ifYn = "N";
		Map<String, Object> m = new HashMap<String, Object>();
		reqMap.put("IF_YN", ifYn);
		Map<String, Object> custMap = ((List<Map<String, Object>>) cmBeanDao.selectMngCodeCUST(model)).get(0);
		String custId = custMap.get("ID").toString();
		String custCd = wmsif902Dao.selectWinusCustCd(model);

		model.put("REPPART_CD", custCd);
		List<Map<String, Object>> inOrderList = wmsif902Dao.selectInboundHeaderList(model);
		for (Map<String, Object> inOrderHeader : inOrderList) {
			inOrderHeader.put("vrSrchCustId", custId); // 입고화주
			inOrderHeader.put("vrInCustId", ""); // 입고화주
			setInOrderHeaderParams(inOrderHeader);
			List<Object> inOrderDetailList = wmsif902Dao.selectInboundDetailList(inOrderHeader);
			int insCnt = inOrderDetailList.size();
			try {
				for (int i = 0; i < insCnt; i++) {
					Map<String, Object> inOrderDetail = (Map<String, Object>) inOrderDetailList.get(i);
					if (inOrderDetail == null) {
						throw new BizException(inOrderDetail.get("ITEM_CD") + "는 미등록 상품입니다.");
					}
					model.put("vrSrchItemCd", inOrderDetail.get("ITEM_CD"));
					Map<String, Object> itemMap = selectItemMapByCd(model);
					inOrderDetail.put("UOM_ID", itemMap.get("UOM_ID"));
					inOrderDetail.put("I_RITEM_ID", itemMap.get("ID"));
					setInOrderDetailParams(inOrderDetail, i);
//          inOrderDetail.put("I_ORD_SEQ", Integer.toString(i+1));

					inOrderHeader.putAll(inOrderDetail);
				}
				inOrderHeader.put("orderList", inOrderDetailList);
				model.putAll(inOrderHeader);
				m = wmsop999Service.saveInOrderV4(model);
				log.info(m);

				updateWellfoodInboundIfStatus(model);
			} catch (BizException be) {
				m.put("errCnt", "1");
				m.put("MSG", be.getMessage());
				throw be;
			}
		}
		return m;
	}

	@SuppressWarnings("unchecked")
    @Override
	public Map<String, Object> saveInOrders(Map<String, Object> model, Map<String, Object> reqMap) throws Exception {

		String ifYn = "N";
		Map<String, Object> m = new HashMap<String, Object>();
		reqMap.put("IF_YN", ifYn);

		List<Map<String, Object>> inOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
		
		try {
		    Map<String,Object> modelIns = new HashMap<String, Object>();
		    
		    modelIns.put("selectIds",inOrderList.size());
		    modelIns.put("vrOrdType", "I");
		    modelIns.put(ConstantIF.SS_SVC_NO, (String)model.get(ConstantIF.SS_SVC_NO));
		    modelIns.put(ConstantIF.SS_CLIENT_IP, (String)model.get(ConstantIF.SS_CLIENT_IP));
		    modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_USER_NO));
		    String sysdate = DateUtil.getSysDate();

		    for (int i = 0 ; i < inOrderList.size(); i++) {
		        Map<String, Object> inOrder = inOrderList.get(i);
		        
		        String inReqDate = inOrder.get("ISS_DATE").toString();
		        String calInDt = DateUtil.isCorrectDate(inReqDate) ? inReqDate : sysdate; // 날짜유형 안맞으면 입고일 오늘날짜로
		        
		        modelIns.put("custOrdNo"+i,inOrder.get("ISS_NO"));
		        modelIns.put("custOrdSeq"+i,inOrder.get("ISS_SEQ"));
		        modelIns.put("custCd"+i,(String)reqMap.get("custCd"));
		        modelIns.put("inReqDt"+i,calInDt);
		        modelIns.put("itemCd"+i,inOrder.get("ITEM_CD"));
		        modelIns.put("ordQty"+i,inOrder.get("ISSCNV_QTY").toString());
		        modelIns.put("unitAmt"+i,inOrder.get("PA_UNIT_PRICE").toString());
		        modelIns.put("uomCd"+i,inOrder.get("USE_UNIT"));
		        modelIns.put("inCustCd"+i,inOrder.get("REPPART_CD"));
		        modelIns.put("remark"+i,inOrder.get("RMK") == null ? "" : inOrder.get("RMK"));
		        modelIns.put("etc2"+i,inOrder.get("RMK AS DETAIL_RMK") == null ? "": inOrder.get("RMK AS DETAIL_RMK"));
		    }
		    
		    m = (Map<String,Object>)wmsop030Service.saveExcelOrder(modelIns);
		    
		    if(m.get("errCnt").equals(1)){
		        throw new BizException ((String)m.get("MSG"));
		    }
		    		    
		    for (Map<String, Object> inOrder : inOrderList) {
		        inOrder.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		        inOrder.put("SS_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		        updateWellfoodInboundIfStatus(inOrder);
		    }
		    
		} catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage());
            
        } catch (Exception e) {
            e.printStackTrace();
            m.put("errCnt", "1");
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
		return m;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveOutOrders(Map<String, Object> model, Map<String, Object> reqMap)
			throws Exception {
//		String instructorWarehouse = reqMap.get("warehouse").toString();
		String orderRegist = "100"; /* 주문등록 */
		String outboundStat = "02"; /* 출고 */
		String normalOrder = "30"; /* 정상출고 */
		String moveOrder = "134"; /* 이동출고 */
		String ordTypeB2B = "B2B"; /* 주문유형 B2B */
		String ifYn = "N";
		String oudSubType = normalOrder;
//		String oudSubType = "SUWON".equals(instructorWarehouse) ? moveOrder : normalOrder;

		Map<String, Object> m = new HashMap<String, Object>();
		reqMap.put("IF_YN", ifYn);
		
		String custId = (String)reqMap.get("vrSrchCustId");
		if ("".equals(custId)) {
			throw new NullPointerException("WFD 화주코드 미등록.");
		}
		model.put("CUST_ID", custId);


		List<Map<String, Object>> outOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
		try {
			String remark = "";
			//수원이고이면 수원입고 잡고 부산출고처리.
//			if("SUWON".equals(instructorWarehouse)) {
//				remark = "수원이고";
//				Map<String, Object> suwonInorders = createOutOrderToInOrderMap(outOrderList);
//				Map<String, Object> inOrderModelMap = new HashMap<String, Object>(model);
//				inOrderModelMap.put("SS_SVC_NO", "0000004460");
//				saveInOrders(inOrderModelMap, suwonInorders);
//			}
			String winusCustCd = (String )reqMap.get("vrSrchCustCd");
			for (Map<String, Object> outOrder : outOrderList) {
				outOrder.putAll(model);
				
//				String kunnr = outOrder.get("KUNNR").toString(); /* 납품처 */
//				String reppartCd = outOrder.get("REPPART_CD").toString();
				String orgOrdId = outOrder.get("INDI_NO").toString();
//				String transCustId =  outOrder.get("TRANS_CUST_ID").toString()
				List<Map<String, Object>> children = (List<Map<String, Object>>) outOrder.get("children");

				
				/*transCustId 외부거래처, transWarehouseId 내부센터
				 * transCustId가 있으면 transWarehouseId는 빈값(반대도 동일)
				 * */
				model.put("pageSize", "10000");
				model.put("vrSrchCustCd", winusCustCd);

				outOrder.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
				outOrder.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
				outOrder.put("ORG_ORD_ID", orgOrdId);
				outOrder.put("CUST_ID", custId);
				outOrder.put("WORK_STAT", orderRegist);
				outOrder.put("ORD_TYPE", outboundStat);
				outOrder.put("ORD_SUBTYPE", oudSubType);
				outOrder.put("OUT_REQ_DT", outOrder.get("INDI_DATE").toString());
				outOrder.put("ORD_BIZ_TYPE", ordTypeB2B);
				for (Map<String, Object> orderDetail : children) {
					orderDetail.putAll(model);

					String detailOrdId = orderDetail.get("INDI_NO").toString(); /* 원주문번호 */
					String ordSeq = orderDetail.get("INDI_SEQ").toString(); /* 원주문번호상세 */
					String itemCd = (String) orderDetail.get("ITEM_CD");
					String rItemId = (String) orderDetail.get("RITEM_ID");		/* WINUS 상품고유코드 */
					
					Integer unitAmt = (Integer)orderDetail.get("ADJ_UCOST");
					Integer amt = (Integer)orderDetail.get("INDI_AMT");
					Integer addAmt = (Integer) orderDetail.get("ADD_AMT");
					Integer indiQty = /* 박스수량 */
							("PVC".equals(itemCd) || "PVCL".equals(itemCd)) ? 0 : (Integer) orderDetail.get("INDI_QTY");
					Integer indicnvQty = (Integer) orderDetail.get("INDICNV_QTY");/* 환산수량(EA) */
					String registOrderStatus = "100"; // 주문등록
					String repUomId = (String) orderDetail.get("REP_UOM_ID");
					String orderUomId = (String) orderDetail.get("UOM_ID");
					Integer datTo = (Integer)orderDetail.get("DAT_TO"); /* 유통기한범위 */
					
					if("".equals(rItemId)) {
						throw new BizException("상품번호: "+ itemCd+ "는 미등록 상품입니다.");
					}
					if (detailOrdId.equals(orgOrdId)) {
						orderDetail.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
						orderDetail.put("CUST_ID", custId);
						
						orderDetail.put("ORD_SEQ", ordSeq);
						orderDetail.put("REAL_BOX_QTY", indiQty);
						orderDetail.put("WORK_STAT", registOrderStatus);
						orderDetail.put("UNIT_AMT", unitAmt);
						orderDetail.put("AMT", amt);
						orderDetail.put("ADD_AMT", addAmt);
						orderDetail.put("OUT_ORD_UOM_ID", repUomId);
						orderDetail.put("OUT_WORK_UOM_ID", orderUomId);
						orderDetail.put("OUT_ORD_QTY", indicnvQty);
						orderDetail.put("OUT_WORK_ORD_QTY", indicnvQty);
						orderDetail.put("ITEM_BEST_DATE", datTo);
						orderDetail.put("ORDER_DESC", remark);

					}
				}
				
				
			}
			wmsif902Dao.insertBulkOrder(outOrderList);
			
			List<List<Map<String, Object>>> orderSplitList = CommonUtil.splitList(outOrderList, 1000);
			for (List<Map<String, Object>> updateList : orderSplitList) {
				reqMap.put("updateIfList", updateList);
				updateWellfoodOubundIfStatus(reqMap);
			}
			m.put("errCnt", "0");
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			throw e;
		}

		return m;
	}

	private String selectWinusDeliveryCustCode(Map<String, Object> model, String kunnr, String reppartCd)  throws BizException{
		List<Object> deliveryCustList = wmsms011Dao.list(model).getList();
		
		String transCustId = selectWinusDeliveryCustId(deliveryCustList, kunnr);
		String transWarehouseId = selectWinusDeliveryCustId(deliveryCustList, reppartCd);
		
		if ("".equals(transCustId) && "".equals(transWarehouseId)) {
			throw new BizException("거래처코드가 없습니다. "+kunnr+", 또는"+ reppartCd+"가 등록된 코드인지 확인하세요.");
		}
		
		return "".equals(transWarehouseId) ? transCustId : transWarehouseId;
	}
	

	private void checkExistOrderItem(String sCustCd, String reppartCd) throws BizException{
	}

	/*
	 * 수원이고의 경우
	 * 부산출고처리된 주문을 수원으로 입고하기 위해 출고주문을 입고주문으로 새로 담아서 리턴.
	 * */
	private Map<String, Object> createOutOrderToInOrderMap(List<Map<String, Object>> outOrders){
		Map<String, Object> inorderMap = new HashMap<String, Object>();
		List<Map<String, Object>> inorderList = new ArrayList<Map<String,Object>>();
		for (Map<String, Object> outOrder : outOrders) {
			List<Map<String, Object>> outOrderDetails = (List<Map<String, Object>>) outOrder.get("children");
			List<Map<String, Object>> inorderDetailList = new ArrayList<Map<String,Object>>();
			Map<String, Object> inOrder = new HashMap<String, Object>();
			String whCd = (String) outOrder.get("INDIPART_CD");
			String orderDate = (String) outOrder.get("INDI_DATE");
			String orgOrdId = (String) outOrder.get("INDI_NO");
			String orderType = "IM32"; //정상이고(WMS)
			String remark = "부산 -> 수원이고 ";
			
			inOrder.put("REPPART_CD", whCd);
			inOrder.put("ISSPART_CD", whCd);
			inOrder.put("ISS_DATE", orderDate);
			inOrder.put("ISS_NO", orgOrdId);
			
			inOrder.put("PA_TYP", orderType);
			inOrder.put("RMK", remark);

			for (Map<String, Object> outOrderDetail : outOrderDetails) {
				Map<String, Object> inOrderDetail = new HashMap<String, Object>();

				String orgOrdItemSeq = (String) outOrderDetail.get("INDI_SEQ");
				String itemCode = (String) outOrderDetail.get("ITEM_CD");
				String itemQty = outOrderDetail.get("INDI_QTY").toString();
				String itemUomQty = outOrderDetail.get("INDICNV_QTY").toString();
				String itemUom = (String)outOrderDetail.get("USE_UNIT");
				
				inOrderDetail.put("REPPART_CD", whCd);
				inOrderDetail.put("ISSPART_CD", whCd);
				inOrderDetail.put("ISS_DATE", orderDate);
				inOrderDetail.put("ISS_NO", orgOrdId);
				inOrderDetail.put("ISS_SEQ", orgOrdItemSeq);
				inOrderDetail.put("ITEM_CD", itemCode);
				inOrderDetail.put("ISS_QTY", itemQty);
				inOrderDetail.put("ISSCNV_QTY", itemUomQty);
				inOrderDetail.put("USE_UNIT", itemUom);
				inOrderDetail.put("PA_UNIT_PRICE", 0);
				inOrderDetail.put("PA_TYP", orderType);
				inOrderDetail.put("RMK", remark);
				
				inorderDetailList.add(inOrderDetail);
			}
			inOrder.put("children", inorderDetailList);
			
			inorderList.add(inOrder);
		}
		
		inorderMap.put("orderList", inorderList);
		return inorderMap;
	}
	
	
	private void updateWellfoodInboundIfStatus(Map<String, Object> param) throws SQLException {
		param.put("IF_YN", "Y");
		wmsif902Dao.updateWellfoodInboundHeader(param);
		wmsif902Dao.updateWellfoodInboundDetail(param);
	}

	private Map<String, Object> selectItemMapByCd(Map<String, Object> model) throws BizException {
		List<Map<String, Object>> itemList = ((List<Map<String, Object>>) cmBeanDao.selectMngCodeITEM(model));
		if (itemList.size() == 0) {
			throw new BizException(model.get("vrSrchItemCd") + "의 상품정보가 등록되지 않았습니다.");
		}
//	Map<String, Object> itemMap = itemList.get(0);
		return itemList.get(0);
	}

	private void setInOrderHeaderParams(Map<String, Object> inOrderHeader) {
		String vrOrgOrdId = inOrderHeader.get("ISS_NO").toString();
		String inReqDate = inOrderHeader.get("ISS_DATE").toString();
		String sysdate = DateUtil.getSysDate();
		String calInDt = DateUtil.isCorrectDate(inReqDate) ? inReqDate : sysdate; // 날짜유형 안맞으면 입고일 오늘날짜로
		inOrderHeader.put("dsMain_rowStatus", "INSERT");
		inOrderHeader.put("vrOrdId", "");
		inOrderHeader.put("calInDt", calInDt);
		inOrderHeader.put("inDt", "");
		inOrderHeader.put("custPoid", "");

		inOrderHeader.put("vrOrgOrdId", vrOrgOrdId); // 원입고번호
		inOrderHeader.put("vrSrchOrderPhase", "20");// 정상입고

		inOrderHeader.put("custPoseq", "");
		inOrderHeader.put("orgOrdId", "");
		inOrderHeader.put("orgOrdSeq", "");
		inOrderHeader.put("vrWhId", "");
		inOrderHeader.put("outWhId", "");

		inOrderHeader.put("transCustId", "");
		inOrderHeader.put("vrCustId", "");
		inOrderHeader.put("pdaFinishYn", "");
		inOrderHeader.put("blNo", "");
		inOrderHeader.put("workStat", "");

		inOrderHeader.put("ordType", "");
		inOrderHeader.put("outReqDt", "");
		inOrderHeader.put("outDt", "");
		inOrderHeader.put("pdaStat", "");

		inOrderHeader.put("workSeq", "");
		inOrderHeader.put("capaTot", "");
		inOrderHeader.put("kinOutYn", "");
		inOrderHeader.put("carConfYn", "");

		inOrderHeader.put("tplOrdId", "");
		inOrderHeader.put("approveYn", "");
		inOrderHeader.put("payYn", "");
		inOrderHeader.put("inCustId", "");
		inOrderHeader.put("inCustAddr", "");

		inOrderHeader.put("inCustEmpNm", "");
		inOrderHeader.put("inCustTel", "");
	}

	private void setInOrderDetailParams(Map<String, Object> inOrderDetail, int i) {
		inOrderDetail.put("I_ST_GUBUN", "INSERT");
		inOrderDetail.put("I_ORD_SEQ", inOrderDetail.get("ISS_SEQ").toString());

		inOrderDetail.put("I_CUST_LOT_NO", "");
		inOrderDetail.put("I_REAL_IN_QTY", "0");
		inOrderDetail.put("I_REAL_OUT_QTY", "0");
		inOrderDetail.put("I_REAL_OUT_QTY", "0");
		inOrderDetail.put("I_MAKE_DT", null);
		inOrderDetail.put("I_TIME_PERIOD_DAY", null);
		inOrderDetail.put("I_LOC_YN", null);
		inOrderDetail.put("I_PDA_CD", null);
		inOrderDetail.put("I_WORK_YN", null);
		inOrderDetail.put("I_RJ_TYPE", null);
		inOrderDetail.put("I_REAL_PLT_QTY", "0");
		inOrderDetail.put("I_CONF_YN", null);
		inOrderDetail.put("I_AMT", "0");
		inOrderDetail.put("I_EA_CAPA", null);
		inOrderDetail.put("I_BOX_BARCODE", null);
		inOrderDetail.put("I_OUT_ORD_UOM_ID", null);
		inOrderDetail.put("I_OUT_WORK_UOM_ID", null);
		inOrderDetail.put("I_IN_WORK_ORD_QTY", "0");
		inOrderDetail.put("I_OUT_ORD_QTY", "0");
		inOrderDetail.put("I_OUT_WORK_ORD_QTY", "0");
		inOrderDetail.put("I_REF_SUB_LOT_ID", null);
		inOrderDetail.put("I_DSP_ID", null);
		inOrderDetail.put("I_CAR_ID", null);
		inOrderDetail.put("I_CNTR_ID", null);
		inOrderDetail.put("I_CNTR_NO", null);
		inOrderDetail.put("I_CNTR_TYPE", null);
		inOrderDetail.put("I_BAD_QTY", null);
		inOrderDetail.put("I_UOM_NM", null);
		inOrderDetail.put("I_UNIT_PRICE", null);
		inOrderDetail.put("I_WH_NM", null);
		inOrderDetail.put("I_ITEM_GRP_ID", null);
		inOrderDetail.put("I_EXPIRY_DATE", null);
		inOrderDetail.put("I_IN_ORD_WEIGHT", "0");
		inOrderDetail.put("I_UNIT_NO", null);
		inOrderDetail.put("I_ORD_DESC", inOrderDetail.get("RMK"));
		inOrderDetail.put("I_VALID_DT", null);
		inOrderDetail.put("I_ETC2", null);
		inOrderDetail.put("I_ITEM_BEST_DATE", null);
		inOrderDetail.put("I_ITEM_BEST_DATE_END", null);
		inOrderDetail.put("I_RTI_NM", null);
		inOrderDetail.put("I_ITEM_KOR_NM", null);
		inOrderDetail.put("I_ITEM_ENG_NM", null);

		inOrderDetail.put("I_REAL_BOX_QTY", inOrderDetail.get("ISS_QTY").toString()); // 박스수량
		inOrderDetail.put("I_IN_ORD_QTY", inOrderDetail.get("ISSCNV_QTY").toString()); // 지시수량(EA)

		inOrderDetail.put("I_UNIT_AMT", inOrderDetail.get("PA_UNIT_PRICE"));

		inOrderDetail.put("I_IN_WORK_UOM_ID", inOrderDetail.get("UOM_ID")); // 입고작업UOM
		inOrderDetail.put("I_IN_ORD_UOM_ID", inOrderDetail.get("UOM_ID")); // 대표UOM
		inOrderDetail.put("I_IN_WORK_ORD_QTY", inOrderDetail.get("ISSCNV_QTY").toString()); // 주문수량
	}


	private void updateWellfoodOubundIfStatus(Map<String, Object> param) {
		param.put("IF_YN", "Y");
		wmsif902Dao.updateWellfoodOutboundHeader(param);
		wmsif902Dao.updateWellfoodOutboundDetail(param);
	}

	@Override
	public Map selectInboundList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		Map<String, Object> map = new HashMap<String, Object>();
		String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
		// String testCustId = "0000020164"; //LOGISALL > 0000020164 테스트 동안만...
		model.put("LC_ID", lcId);

		model.put("WELLFOOD_CUST_ID", WELLFOOD_BUSAN);
		String custId = (String)model.get("vrSrchCustId");
		
		if ("".equals(custId)) {
			throw new NullPointerException("WFD 화주코드 미등록.");
		}

		model.put("CUST_ID", custId);

		List<Object> inbList = wmsif902Dao.selectInboundDetailList(model);
		if (inbList.size() > 0) {
			mappingWinusItem(model, inbList);
		}
		wqrs.setTotCnt(inbList.size());
		wqrs.setList(inbList);
		map.put("LIST", wqrs);
		return map;
	}

	@Override
	public Map selectOutOrderAsnList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
		// String testCustId = "0000020164"; //LOGISALL > 0000020164 테스트 동안만...
		model.put("LC_ID", lcId);
		
//		model.put("WELLFOOD_CUST_ID", WELLFOOD_BUSAN);
		try {
			String custId = (String)model.get("vrSrchCustId");
			if ("".equals(custId)) {
				throw new BizException("WFD 화주코드 미등록.");
			}
			model.put("CUST_ID", custId);

			List<Object> outbList = wmsif902Dao.selectOutOrderAsnList(model);
			List<List<String>> splitWfdCusts = CommonUtil
					.splitList(CommonUtil.deduplicationColumn(outbList, "KUNNR"), 1000);
			for (List<String> wfdCusts : splitWfdCusts) {
				List<Map<String, Object>> mappedList = (List<Map<String, Object>>) (List<?>) outbList;
				mappingCustId(model, wfdCusts, mappedList);
			}
			
			if (outbList.size() > 0) {
				mappingWinusItem(model, outbList);
			}

//			wqrs.setTotCnt(outbList.size());
//			wqrs.setList(outbList);
			
			
//			List<Object> outbListTest = new ArrayList<Object>();
//			for (Object object : outbList) {
//				Map<String, Object> aa = (Map<String, Object>) object;
//				if(aa.get("CUST_MAP_YN").equals("Y")){
//					outbListTest.add(object);
//				}
//			}
			
			map.put("LIST", outbList);
//			map.put("LIST", outbListTest);
		} catch (BizException e) {
			log.error(e);
			map.put("errCnt", "1");
			map.put("MSG", e.getMessage());
		}
		
		return map;
	}
	
	@Override
    public Map<String,Object> selectInResultList(Map<String, Object> model)throws Exception{
        Map<String, Object> map = new HashMap<String, Object>();
        String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
        model.put("LC_ID", lcId);

        model.put("WELLFOOD_CUST_ID", "K9787702");
        List<Map<String, Object>> custList = ((List<Map<String, Object>>) cmBeanDao.selectMngCodeCUST(model));
        String custId = "";
        for (Map<String, Object> custMap : custList) {
            String custCode = custMap.get("CODE").toString();
            if ("K9787702".equals(custCode)) {
                custId = custMap.get("ID").toString();
                break;
            }
        }
        if ("".equals(custId)) {
            throw new NullPointerException("WFD 화주코드 미등록.");
        }
        model.put("CUST_ID", custId);

        List<Object> inOrder = wmsif902Dao.selectInResultList(model);
        List<Object> inResult = wmsif902Dao.selectInSendList(model);
        
        map.put("inOrder", inOrder);
        map.put("inResult", inResult);
        return map;
    }
	
	
	private void mappingWinusItem(Map<String, Object> model, List<Object> ifList) {
	    List<Object> winusItemList = wmsif902Dao.selectWinusItemList(model);
	    List<Object> winusUomList = wmsms100Dao.list(model).getList();

	    // 웰푸드 아이템 코드를 기반으로 위너스 아이템 정보를 담은 Map
	    Map<String, Map<String, Object>> itemMap = new HashMap<>();
	    for (Object itemObj : winusItemList) {
	        Map<String, Object> item = (Map<String, Object>) itemObj;
	        String itemCode = (String) item.get("ITEM_CODE");
	        itemMap.put(itemCode, item);
	    }

	    // 웰푸드 UOM 코드를 기반으로 위너스 UOM 정보를 담은 Map
	    Map<String, Map<String, Object>> uomMap = new HashMap<>();
	    for (Object uomObj : winusUomList) {
	        Map<String, Object> uom = (Map<String, Object>) uomObj;
	        String uomCd = (String) uom.get("UOM_CD");
	        uomMap.put(uomCd, uom);
	    }

	    for (Object ifObj : ifList) {
	        Map<String, Object> ifMap = (Map<String, Object>) ifObj;
	        String orderItemCode = (String) ifMap.get("ITEM_CD");
	        String useUnit = (String) ifMap.get("USE_UNIT");

	        // 초기화
	        ifMap.put("RITEM_ID", "");
	        ifMap.put("REP_UOM_ID", "");
	        ifMap.put("ITEM_NM", "");
	        ifMap.put("UOM_ID", "");

	        // 웰푸드 아이템 매핑
	        Map<String, Object> item = itemMap.get(orderItemCode);
	        if (item != null) {
	            ifMap.put("RITEM_ID", item.get("RITEM_ID"));
	            ifMap.put("ITEM_NM", item.get("ITEM_NM"));
	            ifMap.put("REP_UOM_ID", item.get("REP_UOM_ID"));
	        }

	        // 웰푸드 UOM 매핑
	        Map<String, Object> uom = uomMap.get(useUnit);
	        if (uom != null) {
	            ifMap.put("UOM_ID", uom.get("UOM_ID"));
	        }
	    }
	}

	private String selectWinusDeliveryCustId(List<Object> deliveryCustList, String wellfoodCustCd) {
		String winusCustId = "";
		for (Object object : deliveryCustList) {
			Map<String, Object> custMap = (Map<String, Object>) object;
			String winusCustCd = custMap.get("CUST_CD").toString();
			if (wellfoodCustCd.equals(winusCustCd)) {
				winusCustId = custMap.get("CUST_ID").toString();
				break;
			}
		}
		return winusCustId;
	}

	@Override
	public Map<String, Object> selectCommonCodeList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> commonCodeList = wmsif902Dao.selectCommonCodeList(model);
		resultMap.put("LIST", commonCodeList);
		return resultMap;
	}

	@Override
	public Map<String, Object> selectZsCustList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> zsCustList = wmsif902Dao.selectZsCustList(model);
		resultMap.put("LIST", zsCustList);
		return resultMap;
	}

	@Override
	public Map<String, Object> selectWarehouseList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("LIST", wmsif902Dao.selectWarehouseList(model));
		return resultMap;
	}

	@Override
	public Map<String, Object> selectProducts(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> products = wmsif902Dao.selectProducts(model);
		resultMap.put("LIST", products);
		return resultMap;
	}
	
	@Override
	public Map<String, Object> makeInResult(Map<String, Object> model, Map<String, Object> reqMap) throws Exception{
	    Map<String, Object> resultMap = new HashMap<String, Object>();
	    try{
	        List<Map<String, Object>> inOrderList = (List<Map<String, Object>>) reqMap.get("inOrderList");
	        
	        for(int i = 0 ; i < inOrderList.size() ; i++){
	            Map<String,Object> inOrder = inOrderList.get(i);
	            inOrder.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
	            inOrder.put("USER_NO", model.get(ConstantIF.SS_USER_NO));
	            inOrder.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
	            //ITF_COM_YN 확인
	            if(wmsif902Dao.checkItfComYn(inOrder).equals("Y")){
	                throw new Exception("이미 접수된 건이 있습니다.");
	            }
	            //WMSOP011 COUNT 확인
                if(!(wmsif902Dao.checkCountOrder(inOrder)).equals(0)){
                    throw new Exception("완료되지 않은 주문이 있습니다.");
                }
                //INSERT WELLFOOD_INBOUND_RESULT
                wmsif902Dao.insertInResult(inOrder);
                
                
	        }
	        
	        //UPDATE ITF_COM_YN
	        for (int i = 0 ; i < inOrderList.size() ; i++){
	            Map<String,Object> inOrder = inOrderList.get(i);
                inOrder.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
                inOrder.put("USER_NO", model.get(ConstantIF.SS_USER_NO));
                inOrder.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
              
                wmsif902Dao.updateItfComYn(inOrder);
	        }
	           
	        resultMap.put("errCnt", "0");
	        resultMap.put("MSG", MessageResolver.getMessage("save.success"));

	    }catch(Exception e){
	        e.printStackTrace();
	        resultMap.put("errCnt", "1");
	        resultMap.put("MSG", MessageResolver.getMessage("save.error"));
	    }
	    return resultMap;
	}
	
	
	/*-
     * Method ID    : sendAPI
     * Method 설명    : 
     * 작성자      
     */
    @Override
    public Map<String, Object> sendAPI(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String method  = (String)model.get("method");
        String apiUrl     = (String)model.get("apiUrl");
        String strUrl   = "http://10.30.11.25:8088/api/wellfood/v1"+apiUrl;
        
        try{            
            String data = new ObjectMapper().writeValueAsString(model.get("jsonData"));

            URL url = null; 
            url = new URL(strUrl);
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod(method);
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Content-Type"   , "application/json");
            con.setRequestProperty("Accept"         , "application/json");
            
            String jsonInputString = data;
    
            String[] requestBodyMethods = {
                HttpMethod.POST.name(), 
                HttpMethod.PUT.name(), 
                HttpMethod.PATCH.name()
                };
            if (Arrays.asList(requestBodyMethods).contains(method)) {
                try(OutputStream os = con.getOutputStream()){
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
                }
            }
            
            //Response data 받는 부분
            int responceCode = con.getResponseCode();
            if (responceCode == HttpURLConnection.HTTP_OK) {
           
               try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    m.put("MSG"  , response.toString());
                    m.put("errCnt", "0");
                }
           }
           con.disconnect();
           
        } catch(Exception e){
            throw e;
        }
        return m;
    }

	@Override
	public Map<String, Object> selectWinusItemList(Map<String, Object> model, Map<String, Object> reqMap) {
		Map<String, Object> map = new HashMap<String, Object>();
		String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
		String custId = reqMap.get("vrSrchCustId").toString();
		model.put("LC_ID", lcId);
		model.put("CUST_ID", custId);
		List<Object> itemList = (List<Object>) reqMap.get("itemList");
		mappingWinusItem(model, itemList);
		map.put("LIST", itemList);
		return map;
	}

	/*
	 * 일별 수원분류상품 저장
	 * 삭제한 리스트를 저장하고 새로 등록한 것
	 * */
	@Override
	public Map<String, Object> saveItemRegionDiv(Map<String, Object> model, Map<String, Object> reqMap) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> updateItems = (List<Map<String, Object>>) reqMap.get("updateItems");
		List<Map<String, Object>> deleteItems = (List<Map<String, Object>>) reqMap.get("deleteItems");
		String lcId = (String)model.get(ConstantIF.SS_SVC_NO);
		String fromCtCd = Wellfood.WH_BUSAN.getWellfoodCode();
		String toCtCd = Wellfood.WH_SUWON.getWellfoodCode();
		if(lcId.equals(Wellfood.WH_SUWON.getWinusCode())) {
			fromCtCd = Wellfood.WH_SUWON.getWellfoodCode();
			toCtCd = Wellfood.WH_BUSAN.getWellfoodCode();
		}
//		try {
			String asnDate = ( String )reqMap.get("ASN_DT");
			SessionListener.setSessionParams(reqMap);
			if(deleteItems.size() > 0) {
				wmsif902Dao.deleteDayProductDiv(reqMap);
			}
			for (Map<String, Object> item : updateItems) {
				item.put("ASN_DT", asnDate);
				item.put("FROM_CT_CD", fromCtCd);
				item.put("TO_CT_CD", toCtCd);
				SessionListener.setSessionParams(item);
				wmsif902Dao.mergeDayProductDiv(item);
			}
			
			resultMap.put("errCnt", 0);
			resultMap.put("MSG", MessageResolver.getMessage("save.success"));
//		} catch (Exception e) {
//			throw e;
//		}
		
		return resultMap;
	}

	@Override
	public Map<String, Object> selectDayProductDivList(Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> itemList = wmsif902Dao.selectDayProductDivList(model);
		if (itemList.size() > 0) {
			String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
			model.put("LC_ID", lcId);
			mappingWinusItem(model, itemList);
		}

		map.put("LIST", itemList);
		return map;
	}
	
//	private void mappingCustId(List<Map<String, Object>> orderList, List<Map<String, Object>> chkCustList) {
//	    // chkCustList를 맵으로 변환 (CUST_CD -> chkcust 맵)
//	    Map<String, Map<String, Object>> chkCustMap = new HashMap<>();
//	    for (Map<String, Object> chkcust : chkCustList) {
//	        String custCd = (String) chkcust.get("CUST_CD");
//	        chkCustMap.put(custCd, chkcust);
//	    }
//
//	    for (Map<String, Object> order : orderList) {
//	        String transCustCode = (String) order.get("KUNNR");
//	        Map<String, Object> chkcust = chkCustMap.get(transCustCode);
//	        if (chkcust != null) {
//	            order.put("CUST_ID", chkcust.get("TRUST_CUST_ID"));
//	            order.put("TRANS_CUST_ID", chkcust.get("CUST_ID"));
//	        }
//	    }
//	}

	
	/*
	 * 거래처정보 등록되어 있는지 체크한다.
	 */
	private void mappingCustId(Map<String, Object> model, List<String> custList, List<Map<String, Object>> orderList) throws Exception{
		String warehouseId = (String)model.get("SS_SVC_NO");
		String user_no = model.get("SS_USER_NO").toString();
		//주문별 중복상품 코드 제거.
//		List<String> custList = deduplicationColumn(orderList, "TRANS_CUST_CD");
		Map<String, Object> custMap = new HashMap<String, Object>();


		custMap.put("SS_SVC_NO", warehouseId);
		custMap.put("SS_USER_NO", user_no);
    	
    	model.put("S_LC_ALL", "LC_ALL");
		model.put("S_LC_ID", warehouseId);
		model.put("S_CUST_CD_LIST", custList);
		model.put("S_PAGE_NUM", 1);
		model.put("S_PAGE_LEN", 5000);
		List<Map<String, Object>> chkCustList = outOrderRegDao.selectAllCustList(model);
		
		// chkCustList를 맵으로 변환 (CUST_CD -> chkcust 맵)
	    Map<String, Map<String, Object>> chkCustMap = new HashMap<>();
	    for (Map<String, Object> chkcust : chkCustList) {
	        String custCd = (String) chkcust.get("CUST_CD");
	        chkCustMap.put(custCd, chkcust);
	    }

	    for (Map<String, Object> order : orderList) {
	        String transCustCode = (String) order.get("KUNNR");
	        Map<String, Object> chkcust = chkCustMap.get(transCustCode);
	        String custId = "";
	        String transCustId = "";
	        String isMappingCust = "N";
	        if (chkcust != null) {
	            custId = (String )chkcust.get("TRUST_CUST_ID");
	            transCustId = (String )chkcust.get("CUST_ID");
	            isMappingCust = "Y";
	        }
	        order.put("CUST_ID", custId);
            order.put("TRANS_CUST_ID", transCustId);
            order.put("CUST_MAP_YN", isMappingCust);
	    }
	}
	
//	private boolean isEqualsMapValue(String codeValue, String mapKey, List<Map<String, Object>> checkitemList) {
//		boolean isItem = false;
//		for (Map<String, Object> checkItemMap : checkitemList) {
//			if(!checkItemMap.containsKey(mapKey)) {
//				throw new IllegalArgumentException("mapKey가 없음. >"+mapKey);
//			}
//			String mapValue = (String) checkItemMap.get(mapKey);
//			if(codeValue.equals(mapValue)) {
//				isItem = true;
//				break;
//			}
//		}
//		
//		return isItem;
//	}

}
