package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF725Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF725Service")
public class WMSIF725ServiceImpl implements WMSIF725Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF725Dao")
    private WMSIF725Dao dao;
    
	@Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
	
    
    /**
     * Method ID	: list
     * Method 설명	:  EBIZWAY 주문 관리 입출고 내역조회 - B2B
     * 작성자			: SUMMER H
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("model: " + model);
		if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
		map.put("LIST", dao.list(model));
        return map;
    }    

    /**
     * Method ID	: list2
     * Method 설명	:  EBIZWAY 주문 관리 입출고 내역조회 -B2C
     * 작성자			: SUMMER H
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
		if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
		map.put("LIST", dao.list2(model));
        return map;
    } 
    
    /**
     * Method ID	: getCustOrdDegree
     * Method 설명	: 주문차수 검색 
     * 작성자			: SUMMER H
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		//System.out.println(model);
		map.put("DS_ORD_DEGREE", dao.getCustOrdDegree(model));
		return map;
	}
    
    /**
     * 
     * 대체 Method ID   : crossDomainHttps
     * 대체 Method 설명    :
     * 작성자                      : 
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        String sUrl = "";      
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        String apiUrl = "";
        
        
        //신규 EAI 서버 - https://10.30.1.14:5201 http://10.30.1.14:5200
        sUrl = "http://10.30.1.14:5200/";  //
        apiUrl= sUrl+cUrl;
        
        
        try{
  
        	//ssl disable
        	//System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2"); //신규 서버 호출 프로토콜 추가 (https 연결 시 )
        	disableSslVerification();
        	
	        //System.out.println("apiUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        /*HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();*/
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod); //cMethod
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
        	con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			//String jsonInputString = "{\"input\":\""+data+"\"}";
			String jsonInputString = data;
			//System.out.println("param jsonInputString : " + jsonInputString);
	
		String[] requestBodyMethods = {
			HttpMethod.POST.name(), 
			HttpMethod.PUT.name(), 
			HttpMethod.PATCH.name()
			};
		if (Arrays.asList(requestBodyMethods).contains(cMethod)) {
		    try(OutputStream os = con.getOutputStream()){
			byte[] input = jsonInputString.getBytes("utf-8");
			os.write(input, 0, input.length);
		    }    
		}
	        
	        //Response data 받는 부분
	        int responceCode = con.getResponseCode();
	        //System.out.println("responceCode =========> "+responceCode);
	        
	        if (responceCode == HttpURLConnection.HTTP_OK) {
	        	
	        	//System.out.println("OK");
	        	
	    	   try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
		            StringBuilder response = new StringBuilder();
		            String responseLine = null;
		            while ((responseLine = br.readLine()) != null) {
		                response.append(responseLine.trim());
		            }
		            
		            //System.out.println("jsonOutputString : "+response.toString());
		            m.put("RESULT"	, response.toString()); 
		            
		        }
	       }
	       con.disconnect();
	       
        } catch(Exception e){
            throw e;
        }
        return m;
	}

	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}

}