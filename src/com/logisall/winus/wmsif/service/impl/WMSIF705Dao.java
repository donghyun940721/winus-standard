package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF705Dao")
public class WMSIF705Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif705.list", model);
    }
    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryWq("wmsif705.listE2", model);
    }
    
    /**
     * Method ID  : listE3
     * Method 설명   : 
     * 작성자                : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryWq("wmsif705.listE3", model);
    }
    
}


