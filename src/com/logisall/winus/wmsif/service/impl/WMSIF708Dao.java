package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF708Dao")
public class WMSIF708Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  	: listE1
     * Method 설명   	: 상품정보 조회 
     * 작성자               : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
        return executeQueryPageWq("wmsif708.listE1", model);
    }   
 
    
    /**
     * Method ID  	: listE2
     * Method 설명   	: 매입처 조회
     * 작성자               : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmsif708.listE2", model);
    }   
    
    
    /**
     * Method ID  	: listE3
     * Method 설명   	: 매출처 조회 
     * 작성자               : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryPageWq("wmsif708.listE3", model);
    }
    
    
    /**
     * Method ID   	: syncItem
     * Method 설명    	: 스카이젯 상품정보 동기화 (스카이젯 -> Winus) 
     * 작성자               : dhkim
     * @param   model
     * @return
     */
    public Object syncItem(Map<String, Object> model) {
        executeUpdate("wmsif202.pk_wmsif202.sp_iskyz_sav_item", model);
        return model;
    }
    
    
    /**
     * Method ID   	: getUomId
     * Method 설명    	: 스카이젯 상품정보 동기화 (스카이젯 -> Winus) 
     * 작성자              	: dhkim
     * @param   model
     * @return
     */
    public Object getUomId(Map<String, Object> model) {
    	return executeView("wmsif708.getUomId", model);        
    }
    
}


