package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsif.service.WMSIF203Service;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF203Service")
public class WMSIF203ServiceImpl implements WMSIF203Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF203Dao")
    private WMSIF203Dao dao;
	
	/**
     * 
     * 대체 Method ID	: listExtra
     * 대체 Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExtra(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listExtra(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listExtra
     * 대체 Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExtra() throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listExtra());
        return map;
    }
    
}
