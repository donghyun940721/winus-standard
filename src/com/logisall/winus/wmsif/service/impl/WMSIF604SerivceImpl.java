package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF604Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF604Service")
public class WMSIF604SerivceImpl extends AbstractServiceImpl implements WMSIF604Service{

	@Resource(name = "WMSIF604Dao")
    private WMSIF604Dao dao;
	
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }        
                
        model.put("OUT_REQ_DT", model.get("vrSrchOrdDegreeDateE1").toString().replaceAll("-", ""));
        
        map.put("LIST", dao.listE1(model));

        return map;
    }
    
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        model.put("OUT_REQ_DT", model.get("vrSrchOrdDegreeDateE2").toString().replaceAll("-", ""));
        
        map.put("LIST", dao.listE2(model));
        return map;
    }
    
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        model.put("OUT_REQ_DT", model.get("vrSrchOrdDegreeDateE3").toString().replaceAll("-", ""));
        
        map.put("LIST", dao.listE3(model));
        return map;
    }
    
	/**
     * Method ID 	: listE3Summary
     * Method 설명 	: 택배 추가송장 통계
     * 작성자 		: dhkim
     * @param model
     * @return
     */
	public Map<String, Object> listE3Summary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {        	

        	model.put("OUT_REQ_DT", model.get("vrSrchOrdDegreeDateE3").toString().replaceAll("-", ""));
        	map.put("LIST", dao.listE3Summary(model));
        	
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
    /**
     * Method ID   		: dasResultCoop
     * Method 설명   	: 다스결과내역 검수확정
     * 작성자           : HDY
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> dasResultCoop(Map<String, Object> model) throws Exception {
    	Map<String, Object> modelIns = new HashMap<String, Object>();
    	Map<String, Object> result = new HashMap<String, Object>();  
    	
    	try{

        	int tmpCnt = Integer.parseInt(model.get("tmpCnt").toString());
        	
        	
        	if(tmpCnt > 0){
              String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
              String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
              String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
              
              //session 및 등록정보
              modelIns.put("LC_ID", LC_ID);
              modelIns.put("WORK_IP", WORK_IP);
              modelIns.put("USER_NO", USER_NO);
              
              modelIns.put("SEND_DATE", (String)model.get("sendDate"));
              modelIns.put("CUST_ID", (String)model.get("srchCustId"));
              modelIns.put("ORDER_WAVE", (String)model.get("degreeE2"));         
              
              result = dao.dasResultCoop(modelIns);
        	}
        	
        	if(Integer.parseInt(result.get("O_MSG_CODE").toString()) < 0) {
        		modelIns.put("errCnt", 1);
        		modelIns.put("MSG", result.get("O_MSG_NAME").toString());            	
            }
            else{
            	modelIns.put("errCnt", 0);
            	modelIns.put("MSG", MessageResolver.getMessage("save.success"));	
            }
        
      } catch(Exception e){
          throw e;
      }
    	
      return modelIns;
      
    }
    
    /**
     * Method ID   		: createDasResultData
     * Method 설명   	: 다스결과내역 검수확정
     * 작성자           : dhkim
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> createDasResultData(Map<String, Object> model) throws Exception {
		Map<String, Object> modelIns = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();  
		boolean isExistConfData = false;
    	
    	try{
    		
			String LC_ID   = (String)model.get(ConstantIF.SS_SVC_NO);
			String WORK_IP = (String)model.get(ConstantIF.SS_CLIENT_IP);
			String USER_NO = (String)model.get(ConstantIF.SS_USER_NO);
			  
			//session 및 등록정보
			modelIns.put("LC_ID", LC_ID);
			modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
			modelIns.put("SEND_DATE", model.get("SEND_DATE").toString().replaceAll("-", ""));    	// 전송일    
			modelIns.put("ORDER_WAVE", (String)model.get("ORDER_WAVE"));	// 물류차수			
			
			modelIns.put("WORK_IP", WORK_IP);
			modelIns.put("USER_NO", USER_NO);              
			
			isExistConfData = (dao.existConfData(modelIns) > 0);
			
			if(isExistConfData){
				throw new BizException("검수확정이 진행된 차수 입니다.");
			}
			
			result = dao.createDasResultData(modelIns);				
				
			if(Integer.parseInt(result.get("O_MSG_CODE").toString()) < 0) {				          
				throw new BizException(result.get("O_MSG_NAME").toString());
			}
			
			modelIns.put("errCnt", 0);
			modelIns.put("MSG", MessageResolver.getMessage("save.success"));
			        
      } catch (BizException be) {
    	  modelIns.put("errCnt", 1);
    	  modelIns.put("MSG", be.getMessage());    	  
      } catch(Exception e){
          throw e;
      }
    	
      return modelIns;      
      
    }
     
}
