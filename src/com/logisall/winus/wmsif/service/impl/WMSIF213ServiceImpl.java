package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsif.service.WMSIF213Service;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF213Service")
public class WMSIF213ServiceImpl implements WMSIF213Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF213Dao")
    private WMSIF213Dao dao;
	

    public Map<String, Object> saveCsv2(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;

        if (list == null) {
            throw new Exception("List cannot be null.");
        }

        int insertCnt = list.size();

        try {
            log.info("Inserting CSV data with model: " + model + " and list size: " + insertCnt);
            dao.insertCsv2(model, list);

            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}));
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
        } catch (Exception e) {
            throw new Exception("Error while saving CSV: " + e.getMessage(), e);
        }

        return m;
    }
    
    public Map<String, Object> InbounSaveCsv(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;

        if (list == null) {
            throw new Exception("List cannot be null.");
        }

        int insertCnt = list.size();

        try {
            log.info("Inserting CSV data with model: " + model + " and list size: " + insertCnt);
            dao.InboundInsertCsv(model, list);

            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}));
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
        } catch (Exception e) {
            throw new Exception("Error while saving CSV: " + e.getMessage(), e);
        }

        return m;
    }
    
    
    public Map<String, Object> OutboundSaveCsv(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;

        if (list == null) {
            throw new Exception("List cannot be null.");
        }

        int insertCnt = list.size();

        try {
            log.info("Inserting CSV data with model: " + model + " and list size: " + insertCnt);
            dao.OutboundInsertCsv(model, list);

            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}));
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
        } catch (Exception e) {
            throw new Exception("Error while saving CSV: " + e.getMessage(), e);
        }

        return m;
    }
    
    /**
     * Method ID	: WMSIF213.listE2
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
    **/ 
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("LIST", dao.listE2(model));
    	return map;
    }
    
    /**
     * Method ID	: WMSIF213.listE3
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
    **/ 
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("LIST", dao.listE3(model));
    	return map;
    }
    
    /**
     * Method ID	: WMSIF213.listE4
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
    **/ 
    @Override
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("LIST", dao.listE4(model));
    	return map;
    }
    
    
}
