package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF213Dao")
public class WMSIF213Dao extends SqlMapAbstractDAO{

	/**
	 * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void insertCsv2(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				if (paramMap.get("CUSTOMER_R") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("CUSTOMER_R"))
    			 && paramMap.get("WH_R") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("WH_R"))
    			 && paramMap.get("MTR_CODE") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("MTR_CODE"))
    	    	 && paramMap.get("LOT_NUMBER") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("LOT_NUMBER"))
    			){

					sqlMapClient.insert("wmsif213.insertCsv", paramMap);
					
    			}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	public void InboundInsertCsv(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				if (paramMap.get("INVOICE") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("INVOICE"))
    			 && paramMap.get("BL_NO") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("BL_NO"))
    	    	 && paramMap.get("CONTAINER_NO") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("CONTAINER_NO"))
    	    	 && paramMap.get("MATERIAL_CODE") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("MATERIAL_CODE"))
    			){

					sqlMapClient.insert("wmsif213.InboundInsertCsv", paramMap);
					
    			}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	public void OutboundInsertCsv(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				if (paramMap.get("INVOICE") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("INVOICE"))
    	    	 && paramMap.get("TRUCK_NO") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("TRUCK_NO"))
    			){

					sqlMapClient.insert("wmsif213.OutboundInsertCsv", paramMap);
					
    			}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
    /**
     * Method ID	: WMSIF213.listE2
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif213.listE2", model);
    }
    
    /**
     * Method ID	: WMSIF213.listE3
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif213.listE3", model);
    }
    
    /**
     * Method ID	: WMSIF213.listE4
     * Method 설명	: 
     * 작성자			: YSI
     * @param   model
     * @return  
     * @throws Exception 
     */
    public GenericResultSet listE4(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif213.listE4", model);
    }
}


