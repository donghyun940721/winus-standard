package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF603Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF603Service")
public class WMSIF603SerivceImpl extends AbstractServiceImpl implements WMSIF603Service{

	@Resource(name = "WMSIF603Dao")
    private WMSIF603Dao dao;
	
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        String vrSendYNAll = (String) model.get("vrSendYNAll");
        String vrDegreeDt = ((String) model.get("vrDegreeDt")).replace("-", "");
        
        model.put("vrDegreeDt", vrDegreeDt);
        
        if(vrSendYNAll.equals("Y")) {
        	map.put("LIST", dao.listSend(model));
        }else if(vrSendYNAll.equals("N")){
        	map.put("LIST", dao.list(model));
        }
        return map;
    }
    
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.listE2(model));
        return map;
    }
}
