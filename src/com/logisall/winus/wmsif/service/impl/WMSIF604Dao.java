package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF604Dao")
public class WMSIF604Dao extends SqlMapAbstractDAO{
    
    public GenericResultSet listE1(Map<String, Object> model) {
        return executeQueryPageWq("WMSIF604.listE1", model);
    }
    
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("WMSIF604.listE2", model);
    }
    
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryPageWq("WMSIF604.listE3", model);
    }

   public GenericResultSet listE3Summary(Map<String, Object> model) {
       return executeQueryWq("WMSIF604.listE3Summary", model);
   }  

	public Map<String, Object> dasResultCoop(Map<String, Object> modelIns) {
		executeUpdate("WMSIF604.sp_das_result_coop", modelIns);
		return modelIns;
	}
	
	public Map<String, Object> createDasResultData(Map<String, Object> modelIns) {
		executeUpdate("wmsif604.wmsif113.sp_das_result_coop", modelIns);
		return modelIns;
	}
	
	public int existConfData(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("WMSIF604.existConfData", model);
	}
}
