package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsif.service.WMSIF301Service;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF301Service")
public class WMSIF301ServiceImpl implements WMSIF301Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF301Dao")
    private WMSIF301Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE2
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE2(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID : listE3
     * 대체 Method 설명 : 
     * 작성자          : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.listE3(model));
        map.put("HEADER", dao.listE3Summary(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : crossDomainHttps
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //String hostUrl     = (String)model.get("hostUrl");
        try{
        	String eaiUrl  = "https://10.30.1.14:5101/"; //실운영(local에서 접근)
        	String sUrl = eaiUrl+"invoke/" + (String)model.get("URL");
        	System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
	        System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        //https ssl
	        disableSslVerification();
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "eaiuser01" + ":" + "eaiuser01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	
        	int resCode = 0;
        	resCode = con.getResponseCode();
        	System.out.println("resCode: " + resCode);
        	
        	if(resCode < 400){
        		m.put("errCnt", 0);
                m.put("MSG", "처리되었습니다.");
        	}else{
        		//result = con.getResponseMessage();
        		m.put("errCnt", -1);
	            m.put("MSG", "I/F 처리오류. 코드:"+resCode+" (관리자문의)");
        	}
        	
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID	: crossDomainHttpWs
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String sUrl = "";
        //String hostUrl     = (String)model.get("hostUrl");
        String eaiUrl  = "https://10.30.1.14:5101/";
        String cUrl = (String)model.get("URL");
        if("WINUS.SAEROPNL.IF_ORDER.SRC_CUST_IN_ORDER:TASK_SRC_CUST_IN_ORDER".equals(cUrl)){
        	sUrl = eaiUrl+"SAEROPNLWMS/ERP/IN/ORDER";
        }else if("WINUS.SAEROPNL.IF_ORDER.SRC_CUST_OUT_ORDER:TASK_SRC_CUST_OUT_ORDER".equals(cUrl)){
        	sUrl = eaiUrl+"SAEROPNLWMS/ERP/OUT/ORDER";
        }
        
        try{
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
        	//ssl disable
        	disableSslVerification();
        	

	        System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "eaiuser01" + ":" + "eaiuser01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			//Json Data
			String jsonInputString = "{\"orderResult\":  {\"fromDt\":\""+(String) model.get("fromDt")+"\""
									+					",\"toDt\":\""+(String) model.get("toDt")+"\"}}";
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()) {
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            JSONObject jsonData = new JSONObject(response.toString());
	            m.put("header"	, jsonData.get("header"));
                m.put("message"	, jsonData.get("message"));
                //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
                m.put("rst"	, response.toString());
	        }catch(Exception e){
	        	m.put("rst"	, "{\"header\":\"-99\",\"message\":\"EXC\"}");
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
    /**
     * 
     * 대체 Method ID : updateStockMemo
     * 대체 Method 설명 : 
     * 작성자          : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> updateStockMemo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        JSONArray data = new JSONArray((String)model.get("jsonData"));
        
        Map<String,Object> modelIns;
        
        for(int i = 0 ; i < data.length() ; i++){
            modelIns = new HashMap<String, Object>();
            JSONObject element = data.getJSONObject(i);
            
            modelIns.put("STOCK_MEMO",element.isNull("STOCK_MEMO") ? "" : element.get("STOCK_MEMO"));
            modelIns.put("ITEM_CODE",element.get("ITEM_CODE"));
            
            dao.updateStockMemo(modelIns);
            
        }
        
        map.put("MSG_CODE","0");
        map.put("MSG_NAME","SUCESS");
        
        return map;
    }
	
}
