package com.logisall.winus.wmsif.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Repository("WMSIF701Dao")
public class WMSIF701Dao extends SqlMapAbstractDAO{
	
	/**
     * Method ID : list
     * Method 설명 : 오픈몰 주문 리스트 
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmsif701.list", model);
    }
    public Object list2(Map<String, Object> model){
        return executeQueryForList("wmsif701.list", model);
    }
    
    
    /**
     * Method ID : sapbp
     * Method 설명 : sapbp
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet sapbp(Map<String, Object> model) {
        return executeQueryPageWq("wmsif701.sapbp", model);
    }
    
    /**
	 * Method ID : saveSap
	 * Method 설명 : sap 마스터 수정 저장
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void saveSap(Map<String, Object> model) {
		executeUpdate("wmsif701.saveSap", model);
		
	}
	/**
	 * Method ID : insertSap
	 * Method 설명 : sap 마스터 신규 저장
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void insertSap(Map<String, Object> model) {
		executeInsert("wmsif701.insertSap", model);
		
	}
	/**
	 * Method ID : deleteSap
	 * Method 설명 : sap 마스터 삭제
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void deleteSap(Map<String, Object> model) {
		executeInsert("wmsif701.deleteSap", model);
		
	}
	
	/**
	 * Method ID : playautoListDelete
	 * Method 설명 : playautoListDelete
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	public void playautoListDelete(Map<String, Object> model) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			String orgOrdId = (String) model.get("BUNDLE_NO");
			// existRsSet = WMSOP010 원주문번호로 조회 DEL_YN 체크
			String existRsSet = (String)sqlMapClient.queryForObject("wmsif701.selectPlayautoDelInfo", model);
			
			// existRsSet NULL 일 경우 OP010에 등록이 안된 상태
			if ((String)existRsSet == null){
				sqlMapClient.update("wmsif701.updatePlayautoHDelYn"	, model);//H
				sqlMapClient.update("wmsif701.updatePlayautoDDelYn"	, model);//D: prod
			}else{
				// existRsSet 사용중인게 1건이라도 있으면 삭제 불가
				if(!existRsSet.equals("0")){
					throw new Exception("묶음번호 : "+orgOrdId+" 의 출고 주문을 먼저 삭제해주세요.");
				}else{
					sqlMapClient.update("wmsif701.updatePlayautoHDelYn"	, model);//H
					sqlMapClient.update("wmsif701.updatePlayautoDDelYn"	, model);//D: prod
				}
			}
			
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			sqlMapClient.endTransaction();
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	
	
	/**
	 * Method ID : sapListDelete
	 * Method 설명 : sap 주문 삭제
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void sapListDelete(Map<String, Object> model) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			String orgOrdId = (String) model.get("IDX");
			// existRsSet = WMSOP010 원주문번호로 조회 DEL_YN 체크
			String existRsSet = (String)sqlMapClient.queryForObject("wmsif701.selectDelYn", orgOrdId);
			
			// existRsSet NULL 일 경우 OP010에 등록이 안된 상태
			if ((String)existRsSet == null){
//				throw new Exception("주문번호(사방넷) : "+orgOrdId+" 은/는 등록되지 않은 주문입니다. ");
				sqlMapClient.delete("wmsif701.deleteDelYn"	, orgOrdId);
			}else{
				// existRsSet Y 일 경우 삭제 
				if(existRsSet.equals("N")){
					// existRsSet Y 아닐 경우 삭제 X 
					throw new Exception("주문번호(사방넷) : "+orgOrdId+" 의 출고 주문을 먼저 삭제해주세요.");
				}else{
					sqlMapClient.delete("wmsif701.deleteDelYn"	, orgOrdId);
				}
			}
			
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			sqlMapClient.endTransaction();
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/**
	 * Method ID : saveOrdPlayAutoCallback 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
    public Object saveOrdPlayAutoCallback(Map<String, Object> model){
        executeUpdate("wmsif701.wmsif201.saveOrdPlayAutoCallback", model);
        return model;
    }
    
    /**
     * Method ID	: getOpenMallApiAuthMaster
     * Method 설명 	: 오픈몰 API AUTH 마스터 리스트
     * @param model
     * @return
     */
    public Object getOpenMallApiAuthMaster(Map<String, Object> model){
        return executeQueryForList("wmsif701.getOpenMallApiAuthMaster", model);
    }
    
	/**
	 * Method ID : saveOrdPlayAutoCallback 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
    public Object deleteOrdPlayAutoCallback(Map<String, Object> model){
        executeUpdate("wmsif701.wmsif201.deleteOrdPlayAutoCallback", model);
        return model;
    }
}
