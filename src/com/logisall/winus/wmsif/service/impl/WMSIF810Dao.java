package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF810Dao")
public class WMSIF810Dao extends SqlMapAbstractDAO{
	
	/**
	 * Method ID : listE01
	 * Method 설명 : 대화물류IF 거래처 조회 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE01(Map<String, Object> model) {
        return executeQueryWq("wmsif810.listE01", model);
    }   
    
    /**
	 * Method ID : listE02
	 * Method 설명 : 대화물류IF 상품 조회 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE02(Map<String, Object> model) {
        return executeQueryWq("wmsif810.listE02", model);
    }   
    
    /**
	 * Method ID : listE03
	 * Method 설명 : 대화물류IF 입고 조회 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE03(Map<String, Object> model) {
        return executeQueryWq("wmsif810.listE03", model);
    }   
    
    /**
	 * Method ID : listE04
	 * Method 설명 : 대화물류IF 재고 조회 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE04(Map<String, Object> model) {
        return executeQueryWq("wmsif810.listE04", model);
    }
    
    /**
	 * Method ID : listE05
	 * Method 설명 : 대화물류IF 주문 조회 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE05(Map<String, Object> model) {
        return executeQueryWq("wmsif810.listE05", model);
    }  
    
    /**
     * Method ID : listE05
     * Method 설명 : 대화물류IF 주문 동기화 수량 조회 
     * 작성자 : schan
     * @param model
     * @return
     **/
    public GenericResultSet listE05SyncCount(Map<String, Object> model) {
        return executeQueryWq("wmsif810.listE05SyncCount", model);
    } 
    
    
    /**
	 * Method ID : listE06
	 * Method 설명 : 대화물류IF 출고 조회 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE06(Map<String, Object> model) {
        return executeQueryWq("wmsif810.listE06", model);
    }  
    

    //E01
	
	
  	//E02
  	
  	
  	//E03
  	
  	
  	//E04
  	
  	
  	//E05
    /**
     * Method ID    : syncIF
     * Method 설명      : 출고주문동기화 PK_WMSIF110.SP_HDD_SAVE_OUT_ORDER 호
     * 작성자                 : Summer Hyun
     * @param   model
     * @return
     */
    public Object syncIF(Map<String, Object> model){
        executeUpdate("wmsif810.pk_wmsif110.sp_hdd_save_out_order", model);
        return model;
    }
  	
  	
  	//E06
    
}
