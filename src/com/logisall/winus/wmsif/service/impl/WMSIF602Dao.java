package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF602Dao")
public class WMSIF602Dao extends SqlMapAbstractDAO{
    public GenericResultSet listEnter(Map<String, Object> model) {
        return executeQueryPageWq("wmsif602.listEnter", model);
    }
    
    public GenericResultSet listEntreturn(Map<String, Object> model) {
        return executeQueryPageWq("wmsif602.listEntreturn", model);
    }
    
    public Object degreeListEnter(Map<String, Object> model) {
        return executeQueryForList("wmsif602.degreeListEnter", model);
    }
    
    public Object degreeListEntreturn(Map<String, Object> model) {
        return executeQueryForList("wmsif602.degreeListEntreturn", model);
    }
    
    public Object unitnoListEnter(Map<String, Object> model) {
        return executeQueryForList("wmsif602.unitnoListEnter", model);
    }
    
    public Object unitnoListEntreturn(Map<String, Object> model) {
        return executeQueryForList("wmsif602.unitnoListEntreturn", model);
    }
    
    public Object locListEnter(Map<String, Object> model) {
        return executeQueryForList("wmsif602.locListEnter", model);
    }
    
    public GenericResultSet listE2Enter(Map<String, Object> model) {
        return executeQueryPageWq("wmsif602.listE2Enter", model);
    }
    
    public GenericResultSet listE2Entreturn(Map<String, Object> model) {
        return executeQueryPageWq("wmsif602.listE2Entreturn", model);
    }
    
    public Object orderIns(Map<String, Object> model){
    	executeUpdate("pk_wmsif112.sp_ussoft_in_insert_order_complete_ver_update", model);
    	return model;
    }
    
    public Object orderIns_return(Map<String, Object> model){
    	executeUpdate("pk_wmsif112.sp_ussoft_out_return_complete_ver_update", model);
    	return model;
    }
}
