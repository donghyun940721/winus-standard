package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF301Dao")
public class WMSIF301Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif301.list", model);
    }
    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : schan
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryWq("wmsif301.listE2", model);
    }
    /**
     * Method ID  : listE3
     * Method 설명   : 
     * 작성자                : schan
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
        return executeQueryWq("wmsif301.listE3", model);
    }
    /**
     * Method ID  : listE3Summary
     * Method 설명   : 
     * 작성자                : schan
     * @param   model
     * @return
     */
    public GenericResultSet listE3Summary(Map<String, Object> model) {
        return executeQueryWq("wmsif301.listE3Summary", model);
    }

    /**
     * Method ID : updateStockMemo
     * Method 설명 : 
     * 변경) 작성자 : schan
     * 
     * @param model
     * @return
     */
    public Object updateStockMemo(Map<String, Object> model) {
        return executeUpdate("wmsif301.udpatememo", model);
    }
}


