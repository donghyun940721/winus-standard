package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logisall.api.weverse.dao.WeverseDao;
import com.logisall.api.weverse.service.WeverseService;
import com.logisall.winus.wmsif.service.WMSIF911Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
public class WMSIF911ServiceImpl implements WMSIF911Service{
	private final Log log = LogFactory.getLog(this.getClass());
	@Autowired
	private WeverseDao weverseDao;
	
	@Autowired
	private WeverseService weverseService;
	
	@Override
	public Map<String, Object> selectIfOrders(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> ifOrders = weverseDao.selectIfOrders(model);
		resultMap.put("LIST", ifOrders);
		return resultMap;
	}

	@Override
	@Transactional
	public Map<String, Object> sendWeverseOrderToWmsom010(Map<String, Object> model, Map<String, Object> reqMap) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> orderParam = new HashMap<String, Object>();
		orderParam.put("LC_ID", model.get("SS_SVC_NO"));
		orderParam.put("CUST_ID", reqMap.get("CUST_ID"));
		List<Map<String, Object>> ifOderList = (List<Map<String, Object>>) reqMap.get("list");
		orderParam.put("list", ifOderList);
		try {
			weverseService.processOrders(model, ifOderList);
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (Exception e) {
			log.error(e);
			m.put("errCnt", 1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		return m;
	}

}
