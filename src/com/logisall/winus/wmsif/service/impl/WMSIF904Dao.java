package com.logisall.winus.wmsif.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.i18n.Exception;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
@SuppressWarnings("unchecked")
public class WMSIF904Dao extends SqlMapAbstractDAO{
    private final Log log = LogFactory.getLog(this.getClass());
    // WINUSUSR_IF
    @Autowired
    private SqlMapClient sqlMapClientWinusIf;
    
    public List<Map<String, Object>> selectMallList(Map<String , Object> model) {
	try {
	    return sqlMapClientWinusIf.queryForList("wmsif904.selectMallList", model);
	} catch (SQLException e) {
	    throw new RuntimeSQLException("SQL 오류", e);
	}
    }

    public List<Object> selectOrderList(Map<String, Object> model) {
	try {
	    return sqlMapClientWinusIf.queryForList("wmsif904.selectOrderList", model);
	} catch (SQLException e) {
	    throw new RuntimeSQLException("SQL 오류", e);
	}
    }

    public List<Object> selectOrderShipmentList(Map<String, Object> model) {
	return executeQueryForList("wmsif904.selectOrderShipmentList", model);
    }

    public void updateCafe24OrderStatus(Map<String, Object> outbOrder) {
	try {
	    sqlMapClientWinusIf.update("wmsif904.updateCafe24OrderStatus", outbOrder);
	} catch (SQLException e) {
	    throw new RuntimeException("SQL 오류", e);
	}
    }

    public List<Object> selectCafe24ShipmentList(Map<String, Object> model) {
	try {
	    return sqlMapClientWinusIf.queryForList("wmsif904.selectCafe24ShipmentList", model);
	} catch (SQLException e) {
	    throw new RuntimeException("SQL 오류", e);
	}
    }
}
