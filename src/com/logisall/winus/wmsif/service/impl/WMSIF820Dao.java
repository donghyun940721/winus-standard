package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF820Dao")
public class WMSIF820Dao extends SqlMapAbstractDAO{
	
	
	/**
	 * Method ID : listE01
	 * Method 설명 : 자연드림 쿱 IF 입고 주문 수신내역 조회 
	 * 작성자 : 
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE01(Map<String, Object> model) {
        return executeQueryWq("wmsif820.listE01", model);
    }  
	
    /**
	 * Method ID : listE02
	 * Method 설명 : 자연드림 쿱 IF 입고 주문 수신내역 조회 
	 * 작성자 : 
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE02(Map<String, Object> model) {
        return executeQueryWq("wmsif820.listE02", model);
    }  
    
    /**
	 * Method ID : listE03
	 * Method 설명 : 자연드림 쿱 IF 출고 주문 수내역 조회 
	 * 작성자 : 
	 * @param model
	 * @return
	 **/
    public GenericResultSet listE03(Map<String, Object> model) {
        return executeQueryWq("wmsif820.listE03", model);
    }  
    
    /**
   	 * Method ID : listE04
   	 * Method 설명 : 자연드림 쿱 IF 출고 결과 조회 
   	 * 작성자 : 
   	 * @param model
   	 * @return
   	 **/
	   public GenericResultSet listE04(Map<String, Object> model) {
	       return executeQueryPageWq("wmsif820.listE04", model);
	   }  

	    /**
	   	 * Method ID 	: listE05
	   	 * Method 설명 	: 자연드림 쿱 IF 택배실적 조회 
	   	 * 작성자 		: dhkim 
	   	 * @param model
	   	 * @return
	   	 **/
	   public GenericResultSet listE05(Map<String, Object> model) {
	       return executeQueryPageWq("wmsif820.listE05", model);
	   }  
	   
	   /**
	   	 * Method ID 	: listE05
	   	 * Method 설명 	: 자연드림 쿱 IF 통합작업 조회
	   	 * 작성자 		: dhkim 
	   	 * @param model
	   	 * @return
	   	 **/
	   public GenericResultSet listE06(Map<String, Object> model) {
	       return executeQueryPageWq("wmsif820.listE06", model);
	   }  
	   
	   /**
	   	 * Method ID 	: listBottomE06
	   	 * Method 설명 	: 자연드림 쿱 IF 통합작업 조회
	   	 * 작성자 		: dhkim 
	   	 * @param model
	   	 * @return
	   	 **/
	   public GenericResultSet listBottomE06(Map<String, Object> model) {
	       return executeQueryPageWq("wmsif820.listBottomE06", model);
	   }  
	   
	    /**
	   	 * Method ID 	: listE05
	   	 * Method 설명 	: 자연드림 쿱 IF 택배실적 조회 
	   	 * 작성자 		: dhkim 
	   	 * @param model
	   	 * @return
	   	 **/
	   public GenericResultSet listE05Summary(Map<String, Object> model) {
	       return executeQueryWq("wmsif820.listE05Summary", model);
	   }  
	   
	    /**
	   	 * Method ID 	: listE06Summary
	   	 * Method 설명 	: 자연드림 쿱 IF 통합작업 통계
	   	 * 작성자 		: dhkim 
	   	 * @param model
	   	 * @return
	   	 **/
	   public GenericResultSet listE06Summary(Map<String, Object> model) {
	       return executeQueryWq("wmsif820.listE06Summary", model);
	   }  
	       
    /**
     * Method ID : listE03
     * Method 설명 : 자연드림 쿱 IF 출고 주문 동기화 수량 조회 
     * 작성자 : 
     * @param model
     * @return
     **/
    public GenericResultSet listE03SyncCount(Map<String, Object> model) {
        return executeQueryWq("wmsif820.listE03SyncCount", model);
    } 
    
  	//E05
    /**
     * Method ID    : syncIF
     * Method 설명      : 출고주문동기화 PK_WMSIF110.SP_HDD_SAVE_OUT_ORDER 호
     * 작성자                 : 
     * @param   model
     * @return
     */
    public Object syncIF(Map<String, Object> model){
        executeUpdate("wmsif820.pk_wmsif110.sp_hdd_save_out_order", model);
        return model;
    }

	public Map<String, Object> dasIfSend(Map<String, Object> modelIns) {
		executeUpdate("wmsif820.pk_wmsif113.sp_insert_das_coop_order", modelIns);
		return modelIns;
	}
	
    /**
     * Method ID    	: createInvoiceData
     * Method 설명     	: 출고주문동기화 PK_WMSIF110.SP_HDD_SAVE_OUT_ORDER 호
     * 작성자           : dhkim 
     * @param   model
     * @return
     */
    public Object createInvoiceData(Map<String, Object> model){
        executeUpdate("wmsif820.pk_wmsif115.sp_icoop_dlv_result", model);
        return model;
    }
    
    /**
     * Method ID    	: deleteInvoiceResult
     * Method 설명     	: 택배실적(IRW360) 삭제
     * 작성자           : dhkim 
     * @param   model
     * @return
     */
    public Object deleteInvoiceResult(Map<String, Object> model){
        executeUpdate("wmsif820.deleteInvoiceResult", model);
        return model;
    }
    
    /**
     * Method ID    	: deleteInvoiceResult
     * Method 설명     	: 택배주문(ISW360) 플래그 변경
     * 작성자           : dhkim 
     * @param   model
     * @return
     */
    public Object changeFlagInvoiceOrder(Map<String, Object> model){
        executeUpdate("wmsif820.changeFlagInvoiceOrder", model);
        return model;
    }
    
    public Object dasDelete(Map<String, Object> model){
        executeUpdate("wmsif820.dasDelete", model);
        return model;
    }   
    
}
