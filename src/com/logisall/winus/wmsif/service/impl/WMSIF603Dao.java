package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF603Dao")
public class WMSIF603Dao extends SqlMapAbstractDAO{
    public GenericResultSet listSend(Map<String, Object> model) {
        return executeQueryPageWq("wmsif603.listSend", model);
    }
    
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif603.list", model);
    }
    
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmsif603.listE2", model);
    }
}
