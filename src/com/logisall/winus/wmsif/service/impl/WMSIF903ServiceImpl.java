package com.logisall.winus.wmsif.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF903Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;


@Service
public class WMSIF903ServiceImpl implements WMSIF903Service {
    private final Log log = LogFactory.getLog(this.getClass());

    @Autowired
    private WMSIF903Dao dao;
    
    @Resource(name = "WMSOP030Dao")
    private WMSOP030Dao WMSOP030Dao;

    
    @Override
    public Map<String, Object> listE01(Map<String, Object> model){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE1(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID     : syncOutOrder
     * 대체 Method 설명 : 출고주문정보 동기화
     * 작성자              : schan
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> syncOutOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        
        try{
            
            int cnt = (int)model.get("count");
            
            if (cnt < 1){
                map.put("errCnt", -1);
                map.put("MSG", MessageResolver.getMessage("save.nodata"));
                return map;
            }

            String[] emptyValue = new String[cnt];
            String[] custCd = new String[cnt];
            String[] outReqDt = new String[cnt];
            String[] ordDegree = new String[cnt];
            Arrays.fill(emptyValue,"");
            Arrays.fill(custCd,(String)model.get("custCd"));
            Arrays.fill(outReqDt,(String)model.get("outReqDt"));
            Arrays.fill(ordDegree,(String)model.get("ordDegree"));
            
            String flag = "";
            String custOrdNo = "";
            
            ArrayList<String> orgOrdNo = new ArrayList<String>();
            ArrayList<String> orgOrdSeq = new ArrayList<String>();
            
            List<Integer> densFlagList = (List<Integer>) model.get("DENSE_FLAG");
            List<String> syncOrgOrdNoList = (List<String>) model.get("SYNC_ORG_ORD_NO");
            List<String> shopLinkerOrderId = (List<String>) model.get("SHOPLINKER_ORDER_ID");
            List<String> orderProductId = (List<String>) model.get("ORDER_PRODUCT_ID");
            
            List<Integer> densSeqList = (List<Integer>) model.get("DENSE_SEQ");
            
            Map<String, Object> m = new HashMap<String, Object>();
            Map<String, Object> validation = new HashMap<String, Object>();
            for(int i = 0 ; i < cnt; i++){
                String shopOrderId = shopLinkerOrderId.get(i);
                String shopproductId = orderProductId.get(i);
                
                validation.clear();
                validation.put("LC_ID", (String)model.get("SS_SVC_NO"));
                validation.put("CUST_ID", (String)model.get("custId"));
                validation.put("SHOPLINKER_ORDER_ID", shopOrderId);
                validation.put("ORDER_PRODUCT_ID", shopproductId);
                if(((Map<String,String>)dao.selectShopLinkerIfYnValidation(validation)).get("IF_YN").equals("Y")){
                    throw new BizException("이미 동기화된 주문이 있습니다.");
                }
                
                String denseFlag = densFlagList.get(i).toString();
                String syncOrgOrdNo = syncOrgOrdNoList.get(i);
                
                if(flag.equals("") || !flag.equals(denseFlag)){
                    m.clear();
                    m.put("SYNC_ORG_ORD_NO", syncOrgOrdNo);
                    flag = denseFlag;
                    custOrdNo = (String)dao.getShopLinkerSyncOrgOrdNo(m);
                }
                
                orgOrdNo.add(custOrdNo);
                orgOrdSeq.add(densSeqList.get(i).toString());
            }

            modelIns.put("vrOrdType"        , (String)model.get("ordType")); // 주문타입 
            modelIns.put("no"               , emptyValue);
            modelIns.put("reqDt"            , outReqDt); //출고예정일
            modelIns.put("custOrdNo"        , orgOrdNo.toArray(new String[cnt])); //원주문번호 = DENSE_FLAG 별 SHOPLINKER_SYNC_ORG_ORD_NO@WINUSUSR_IF 시퀀스
            modelIns.put("custOrdSeq"       , orgOrdSeq.toArray(new String[cnt]));
            
            modelIns.put("trustCustCd"      , emptyValue); 
            modelIns.put("transCustCd"      , ((List<String>) model.get("MALL_ID")).toArray(new String[cnt])); //배송처코드 = 쇼핑몰코드
            modelIns.put("transCustTel"     , emptyValue);
            modelIns.put("transReqDt"       , emptyValue);
            modelIns.put("custCd"           , custCd);          //화주코드
            
            modelIns.put("ordQty"           , ((List<String>) model.get("QUANTITY")).toArray(new String[cnt])); //주문수량
            modelIns.put("uomCd"            , emptyValue);
            modelIns.put("sdeptCd"          , emptyValue);
            modelIns.put("salePerCd"        , emptyValue);
            modelIns.put("carCd"            , emptyValue);
            
            modelIns.put("drvNm"            , emptyValue);       
            modelIns.put("dlvSeq"           , emptyValue);
            modelIns.put("drvTel"           , emptyValue);
            modelIns.put("custLotNo"        , emptyValue);
            modelIns.put("blNo"             , emptyValue);
            
            modelIns.put("recDt"            , emptyValue);
            modelIns.put("whCd"             , emptyValue);
            modelIns.put("makeDt"           , emptyValue);
            modelIns.put("timePeriodDay"    , emptyValue);
            modelIns.put("workYn"           , emptyValue);

            modelIns.put("rjType"           , emptyValue);
            modelIns.put("locYn"            , emptyValue);    
            modelIns.put("confYn"           , emptyValue);     
            modelIns.put("eaCapa"           , emptyValue);
            modelIns.put("inOrdWeight"      , emptyValue); 

            modelIns.put("itemCd"           , ((List<String>) model.get("PARTNER_PRODUCT_ID")).toArray(new String[cnt])); //상품코드 = 자사상품코드
            modelIns.put("itemNm"           , ((List<String>) model.get("PRODUCT_NAME")).toArray(new String[cnt])); //상품명 = 쇼핑몰상품명
            modelIns.put("transCustNm"      , ((List<String>) model.get("MALL_NAME")).toArray(new String[cnt])); //배송처명 = 쇼핑몰명
            modelIns.put("transCustAddr"    , emptyValue);
            modelIns.put("transEmpNm"       , emptyValue);

            modelIns.put("remark"           , emptyValue);
            modelIns.put("transZipNo"       , emptyValue);
            modelIns.put("etc2"             , emptyValue); // WMSOM010.ORD_DESC 와 매핑
            modelIns.put("unitAmt"          , emptyValue); //단가
            modelIns.put("transBizNo"       , emptyValue); 

            modelIns.put("inCustAddr"       , emptyValue);
            modelIns.put("inCustCd"         , emptyValue);
            modelIns.put("inCustNm"         , emptyValue);                 
            modelIns.put("inCustTel"        , emptyValue);
            modelIns.put("inCustEmpNm"      , emptyValue);

            modelIns.put("expiryDate"       , emptyValue);
            modelIns.put("salesCustNm"      , ((List<String>) model.get("RECEIVE")).toArray(new String[cnt])); //받는사람
            modelIns.put("zip"              , ((List<String>) model.get("RECEIVE_ZIPCODE")).toArray(new String[cnt])); //우편번호
            modelIns.put("addr"             , ((List<String>) model.get("RECEIVE_ADDR")).toArray(new String[cnt])); //받는 주소
            modelIns.put("addr2"            , emptyValue);

            modelIns.put("phone1"           , ((List<String>) model.get("RECEIVE_CEL")).toArray(new String[cnt])); //전화번호
            modelIns.put("etc1"             , ((List<String>) model.get("SKU")).toArray(new String[cnt])); //옵션명
            modelIns.put("unitNo"           , emptyValue);
            modelIns.put("phone2"           , ((List<String>) model.get("RECEIVE_TEL")).toArray(new String[cnt])); //전화번호
            modelIns.put("buyCustNm"        , emptyValue);  

            modelIns.put("buyPhone1"        , emptyValue);
            modelIns.put("salesCompanyNm"   , emptyValue);
            modelIns.put("ordDegree"        , ordDegree);
            modelIns.put("bizCond"          , emptyValue);
            modelIns.put("bizType"          , emptyValue);

            modelIns.put("bizNo"            , emptyValue);
            modelIns.put("custType"         , emptyValue);
            modelIns.put("dataSenderNm"     , ((List<String>) model.get("MALL_NAME")).toArray(new String[cnt])); //채널 = 쇼핑몰명
            modelIns.put("legacyOrgOrdNo"   , ((List<String>) model.get("MALL_ORDER_ID")).toArray(new String[cnt])); //주문번호(쇼핑몰) = 쇼핑몰주문번호
            modelIns.put("custSeq"          , emptyValue);
            
            modelIns.put("ordDesc"          , emptyValue);
            modelIns.put("dlvMsg1"          , ((List<String>) model.get("DELIVERY_MSG")).toArray(new String[cnt])); //배송메세지 
            modelIns.put("dlvMsg2"          , emptyValue);
            modelIns.put("locCd"            , emptyValue);
            
            modelIns.put("LC_ID",    (String)model.get("SS_SVC_NO"));
            modelIns.put("WORK_IP",  (String)model.get("SS_CLIENT_IP"));
            modelIns.put("USER_NO",  (String)model.get("SS_USER_NO"));
            
            
            map = (Map<String,Object>)WMSOP030Dao.saveExcelOrderB2T(modelIns);
            
            ServiceUtil.isValidReturnCode("WMSIF903", String.valueOf(map.get("O_MSG_CODE")), (String)map.get("O_MSG_NAME"));
            
            for(int i = 0 ; i < cnt ; i++){
                modelIns.clear();
                modelIns.put("SYNC_ORG_ORD_NO",orgOrdNo.get(i));
                modelIns.put("SYNC_ORG_ORD_SEQ",orgOrdSeq.get(i));
                modelIns.put("SHOPLINKER_ORDER_ID",shopLinkerOrderId.get(i));
                modelIns.put("ORDER_PRODUCT_ID",orderProductId.get(i));
                modelIns.put("LC_ID",    (String)model.get("SS_SVC_NO"));
                modelIns.put("CUST_ID",    (String)model.get("custId"));
                dao.updateShopLinkerOrder(modelIns);
            }
            
            map.put("errCnt", 0);
            map.put("MSG", MessageResolver.getMessage("save.sucess"));
            
        }catch(BizException be){
            map.put("errCnt", -1);
            map.put("MSG", be.getMessage() );
        }
        catch(Exception e){
            throw e;
        }
        
        return map;
    }
    
    @Override
    public Map<String, Object> listE02(Map<String, Object> model){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", dao.listE2(model));
        return map;
    }
}
