package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF707Dao")
public class WMSIF707Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif707.list", model);
    }
    
    
    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
        return executeQueryPageWq("wmsif707.listE2", model);
    }
        
        
    /**
     * Method ID   : list
     * Method 설명    : 이지어드민 상품정보 동기화 (EZAdmin -> Winus) 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public Object getItemId(Map<String, Object> model) {
    	return executeView("wmsif707.getItemId", model);        
    }
    
    
    /**
     * Method ID   : list
     * Method 설명    : 이지어드민 상품정보 동기화 (EZAdmin -> Winus) 
     * 작성자                : dhkim
     * @param   model
     * @return
     */
    public Object syncOrder(Map<String, Object> model) {
        executeUpdate("wmsif202.pk_wmsif202.sp_ezadmin_sav_item", model);
        return model;
    }
    
    
    /**
     * Method ID   : getTransCorpInfo
     * Method 설명    : 택배사 맵핑정보 획득 
     * 작성자              : dhkim
     * @param   model
     * @return
     */
    public Object getTransCorpInfo(Map<String, Object> model) {        
        return executeQueryForList("wmsif707.getTransCorpInfo", model);
    }
    
    
    /**
     * Method ID   : getDetList
     * Method 설명    : 주문상세조회
     * 작성자              : dhkim
     * @param   model
     * @return
     */
    public List getDetList(Map<String, Object> model) {        
        return executeQueryForList("wmsif707.getDetList", model);
    }
    
    
    /*-
     * Method ID 	: deleteE1
     * Method 설명 	: 인터페이스 주문삭제
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public Object deleteMas(Object model) {
		return executeUpdate("wmsif707.deleteMas", model);
	}
	
    /*-
     * Method ID 	: deleteE1
     * Method 설명 	: 인터페이스 주문삭제
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public Object deleteDet(Object model) {
		return executeUpdate("wmsif707.deleteDet", model);
	}
}


