package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF715Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF715Service")
public class WMSIF715ServiceImpl implements WMSIF715Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF715Dao")
    private WMSIF715Dao dao;
    
	@Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
	
    
    /**
     * Method ID	: list
     * Method 설명	: 비플로우 주문관리 조회 
     * 작성자			: SUMMER H
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.list(model));
        return map;
    }    
    
    /**
     * Method ID	: list2
     * Method 설명	: 비플로우 인터페이스 이력 조회 
     * 작성자			: SUMMER H
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.list2(model));
        return map;
    }
    
    /**
     * Method ID	: InvcSendCntSave
     * Method 설명	: 비플로우 송장송신 내역 저장
     * 작성자			: SUMMER H
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public void InvcSendCntSave(Map<String, Object> model) throws Exception {
		dao.InvcSendCntSave(model);
    }  
    
    /**
     *  Method ID 	 : wmsOrdRegister
     *  Method 설명  	 : BFLOW 오픈몰 주문 등록
     *  작성자            	 : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> wmsOrdRegister(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // IF테이블 PK관련 항목 
            String[] masterSeq	   		= new String[tmpCnt]; 
            String[] bfOriOrdNo      	= new String[tmpCnt]; //묶음번호_원주문번호
            String[] prdOrdNo          	= new String[tmpCnt]; //상품주문번호_레거시
            String[] packageNo          = new String[tmpCnt]; //배송묶음번호
            String[] CustomCode         = new String[tmpCnt]; //상품코드
            
            
            // 기본프로시저 
            String[] no                 = new String[tmpCnt]; //no(처리)
            String[] reqDt              = new String[tmpCnt]; //출고요청일자(처리)
            String[] custOrdNo          = new String[tmpCnt]; //원주문번호(처리)
            String[] custOrdSeq         = new String[tmpCnt]; //원주문seq(처리)

            String[] trustCustCd        = new String[tmpCnt];
            String[] transCustCd        = new String[tmpCnt];
            String[] transCustTel       = new String[tmpCnt];
            String[] transReqDt         = new String[tmpCnt];
            String[] custCd             = new String[tmpCnt]; //화주코드(처리)

            String[] ordQty             = new String[tmpCnt]; //출고수량(처리) 
            String[] uomCd              = new String[tmpCnt];
            String[] sdeptCd            = new String[tmpCnt];
            String[] salePerCd          = new String[tmpCnt];
            String[] carCd              = new String[tmpCnt];

            String[] drvNm              = new String[tmpCnt];
            String[] dlvSeq             = new String[tmpCnt];
            String[] drvTel             = new String[tmpCnt];
            String[] custLotNo          = new String[tmpCnt];
            String[] blNo               = new String[tmpCnt];

            String[] recDt              = new String[tmpCnt];
            String[] whCd               = new String[tmpCnt];
            String[] makeDt             = new String[tmpCnt];
            String[] timePeriodDay      = new String[tmpCnt];
            String[] workYn             = new String[tmpCnt];

            String[] rjType             = new String[tmpCnt];
            String[] locYn              = new String[tmpCnt];
            String[] confYn             = new String[tmpCnt];
            String[] eaCapa             = new String[tmpCnt];
            String[] inOrdWeight        = new String[tmpCnt];

            String[] itemCd             = new String[tmpCnt]; //상품코드(처리)
            String[] itemNm             = new String[tmpCnt]; //상품명 (처리)
            String[] transCustNm        = new String[tmpCnt];
            String[] transCustAddr      = new String[tmpCnt];
            String[] transEmpNm         = new String[tmpCnt];

            String[] remark             = new String[tmpCnt];
            String[] transZipNo         = new String[tmpCnt];
            String[] etc2               = new String[tmpCnt];
            String[] unitAmt            = new String[tmpCnt];
            String[] transBizNo         = new String[tmpCnt];

            String[] inCustAddr         = new String[tmpCnt];
            String[] inCustCd           = new String[tmpCnt];
            String[] inCustNm           = new String[tmpCnt];
            String[] inCustTel          = new String[tmpCnt];
            String[] inCustEmpNm        = new String[tmpCnt];

            String[] expiryDate         = new String[tmpCnt];
            String[] salesCustNm        = new String[tmpCnt]; //수령인명(처리)
            String[] zip                = new String[tmpCnt]; //수령인우편번호(처리)
            String[] addr               = new String[tmpCnt]; //수령인주소(처리)
            String[] addr2              = new String[tmpCnt];

            String[] phone1             = new String[tmpCnt]; //수령인핸드폰번호(처리)
            String[] etc1               = new String[tmpCnt]; 
            String[] unitNo             = new String[tmpCnt]; // : B2C(처리)
            String[] phone2             = new String[tmpCnt]; //수령인 번호2 (처리)
            String[] buyCustNm          = new String[tmpCnt]; 

            String[] buyPhone1          = new String[tmpCnt]; 
            String[] salesCompanyNm     = new String[tmpCnt]; //eQtipTp ???
            String[] ordDegree          = new String[tmpCnt]; //등록차수(처리)
            String[] bizCond            = new String[tmpCnt]; //업태
            String[] bizType            = new String[tmpCnt]; //업종

            String[] bizNo              = new String[tmpCnt]; //사업자등록번호
            String[] custType           = new String[tmpCnt]; //화주타입
            String[] dataSenderNm       = new String[tmpCnt]; //쇼핑몰(처리)
            String[] legacyOrgOrdNo     = new String[tmpCnt]; //쇼핑몰주문번호(처리)
            String[] custSeq            = new String[tmpCnt];

            String[] ordDesc            = new String[tmpCnt]; //ord_desc
            String[] dlvMsg1            = new String[tmpCnt]; //배송메세지(처리)
            String[] dlvMsg2            = new String[tmpCnt];

            String[] LC_ID              = new String[tmpCnt];
            String[] USER_NO            = new String[tmpCnt];
            String[] WORK_IP            = new String[tmpCnt];
            

            String[] MSG_COCD            = new String[tmpCnt]; //callback용
            String[] MSG_NAME            = new String[tmpCnt]; //callback용

            for (int i = 0; i < tmpCnt; i++) {
            	String NO                  = Integer.toString(i+1);//no(처리)
            	String REQ_DT              = (String)model.get("REQ_DT" + i);//입/출고요청일자(처리)
            	String CUST_ORD_NO         = (String)model.get("BF_ORI_ORD_CD" + i);//원주문번호(처리)
            	String CUST_ORD_SEQ        = "";// 주문은 한 번만 받음
            	                                
            	String TRUST_CUST_CD       = "";
            	String TRANS_CUST_CD       = "";
            	String TRANS_CUST_TEL      = "";
            	String TRANS_REQ_DT        = "";
            	String CUST_CD             = (String)model.get("colCUST_CD" + i);//화주코드(처리)
            	                                
            	String ORD_QTY             = (String)model.get("QUANTITY" + i);//출고수량(처리)
            	String UOM_CD              = "";
            	String SDEPT_CD            = "";// OP030.SDEPT_CD 사업부코드
            	String SALE_PER_CD         = "";// OP030.SALE_PER_CD 담당영업사원번호
            	String CAR_CD              = "";
            	                                
            	String DRV_NM              = "";
            	String DLV_SEQ             = "";
            	String DRV_TEL             = "";
            	String CUST_LOT_NO         = "";
            	String BL_NO               = "";
            	                                
            	String REC_DT              = "";
            	String WH_CD               = "";
            	String MAKE_DT             = "";
            	String TIME_PERIOD_DAY     = "";
            	String WORK_YN             = "";
            	                                
            	String RJ_TYPE             = "";
            	String LOC_YN              = "";
            	String CONF_YN             = "";
            	String EA_CAPA             = "";//OP011.EA_CAPA 개당용적
            	String IN_ORD_WEIGHT       = "";
            	                                
            	String ITEM_CD             = (String)model.get("CUSTOM_CODE" + i);//상품코드(처리)
            	String ITEM_NM             = (String)model.get("PRODUCT_NAME" + i);//상품명(처리)
            	String TRANS_CUST_NM       = "";
            	String TRANS_CUST_ADDR     = "";
            	String TRANS_EMP_NM        = "";
            	                                
            	String REMARK              = ""; 
            	String TRANS_ZIP_NO        = "";
            	String ETC2                = ""; // OM.ORD_DESC, OP10.ORD_DESC , OP11.ETC2 
            	String UNIT_AMT            = "";
            	String TRANS_BIZ_NO        = "";
            	                                
            	String IN_CUST_ADDR        = ""; //OP030.IN_CUST_ADDR 입하처주소
            	String IN_CUST_CD          = ""; //OP030.IN_CUST_CD 입하처코드
            	String IN_CUST_NM          = "";
            	String IN_CUST_TEL         = "";
            	String IN_CUST_EMP_NM      = ""; //OP030.IN_CUST_ADDR 입하처담당자
            	                                
            	String EXPIRY_DATE         = "";
            	String SALES_CUST_NM       = (String)model.get("RECIPIENT_NAME" + i);//수령인명(처리)
            	String ZIP                 = (String)model.get("RECIPIENT_ZIPCODE" + i);//수령인우편번호(처리)
            	String ADDR                = (String)model.get("RECIPIENT_ADDRESS" + i);//수령인주소(처리)
            	String ADDR2               = "";
            	                                
            	String PHONE1              = (String)model.get("RECIPIENT_CELLPHONE" + i);//수령인핸드폰번호(처리)
            	String ETC1                = "";
            	String UNIT_NO             = ""; 
            	String PHONE2              = (String)model.get("RECIPIENT_TELEPHONE" + i);//수령인번호2(처리)
            	String BUY_CUST_NM         = "";
            	                                
            	String BUY_PHONE1          = "";
            	String SALES_COMPANY_NM    = "BFLOW";
            	String ORD_DEGREE          = (String)model.get("colORD_DEGREE" + i);//등록차수(처리)
            	String BIZ_COND            = "";//업태
            	String BIZ_TYPE            = "";//업종
            	                                
            	String BIZ_NO              = "";//사업자등록번호
            	String CUST_TYPE           = "";//화주타입
            	String DATA_SENDER_NM      = (String)model.get("DISTRIBUTION_CHANNEL" + i);//쇼핑몰(처리)
            	String LEGACY_ORG_ORD_NO   = (String)model.get("PRD_ORD_NO" + i); //unit-seq(version)
            	String CUST_SEQ            = "";
            	                                
            	String ORD_DESC            = "";
            	String DLV_MSG1            = (String)model.get("ORDERER_MEMO" + i);//배송메세지(처리)
            	String DLV_MSG2            = "";
            	
            	
            	//변수 
            	no[i]                      = NO;                 //no(처리)
            	reqDt[i]                   = REQ_DT;             //출고요청일자(처리)
            	custOrdNo[i]               = CUST_ORD_NO;        //원주문번호(처리)
            	custOrdSeq[i]              = CUST_ORD_SEQ;       //원주문seq(처리)
            	                             
            	trustCustCd[i]             = TRUST_CUST_CD;
            	transCustCd[i]             = TRANS_CUST_CD;
            	transCustTel[i]            = TRANS_CUST_TEL;
            	transReqDt[i]              = TRANS_REQ_DT;
            	custCd[i]                  = CUST_CD;            //화주코드(처리)
            	                             
            	ordQty[i]                  = ORD_QTY;            //출고수량(처리)
            	uomCd[i]                   = UOM_CD;
            	sdeptCd[i]                 = SDEPT_CD;
            	salePerCd[i]               = SALE_PER_CD;
            	carCd[i]                   = CAR_CD;
            	                             
            	drvNm[i]                   = DRV_NM;
            	dlvSeq[i]                  = DLV_SEQ;
            	drvTel[i]                  = DRV_TEL;
            	custLotNo[i]               = CUST_LOT_NO;        // 고객로뜨번호 
            	blNo[i]                    = BL_NO;
            	                             
            	recDt[i]                   = REC_DT;
            	whCd[i]                    = WH_CD;
            	makeDt[i]                  = MAKE_DT;
            	timePeriodDay[i]           = TIME_PERIOD_DAY;
            	workYn[i]                  = WORK_YN;
            	                             
            	rjType[i]                  = RJ_TYPE;
            	locYn[i]                   = LOC_YN;
            	confYn[i]                  = CONF_YN;
            	eaCapa[i]                  = EA_CAPA;
            	inOrdWeight[i]             = IN_ORD_WEIGHT;
            	                             
            	itemCd[i]                  = ITEM_CD;            //상품코드(처리)
            	itemNm[i]                  = ITEM_NM;            //상품명(처리)
            	transCustNm[i]             = TRANS_CUST_NM;
            	transCustAddr[i]           = TRANS_CUST_ADDR;
            	transEmpNm[i]              = TRANS_EMP_NM;
            	                             
            	remark[i]                  = REMARK;
            	transZipNo[i]              = TRANS_ZIP_NO;
            	etc2[i]                    = ETC2;
            	unitAmt[i]                 = UNIT_AMT;
            	transBizNo[i]              = TRANS_BIZ_NO;
            	                             
            	inCustAddr[i]              = IN_CUST_ADDR;
            	inCustCd[i]                = IN_CUST_CD;
            	inCustNm[i]                = IN_CUST_NM;
            	inCustTel[i]               = IN_CUST_TEL;
            	inCustEmpNm[i]             = IN_CUST_EMP_NM;
            	                             
            	expiryDate[i]              = EXPIRY_DATE;
            	salesCustNm[i]             = SALES_CUST_NM;      //수령인명(처리)
            	zip[i]                     = ZIP;                //수령인우편번호(처리)
            	addr[i]                    = ADDR;               //수령인주소(처리)
            	addr2[i]                   = ADDR2;
            	                             
            	phone1[i]                  = PHONE1;             //수령인핸드폰번호(처리)
            	etc1[i]                    = ETC1;
            	unitNo[i]                  = UNIT_NO;            //:B2C(처리)
            	phone2[i]                  = PHONE2;             //수령인번호2(처리)
            	buyCustNm[i]               = BUY_CUST_NM;
            	                             
            	buyPhone1[i]               = BUY_PHONE1;
            	salesCompanyNm[i]          = SALES_COMPANY_NM;   //eQtipTp???
            	ordDegree[i]               = ORD_DEGREE;         //등록차수(처리)
            	bizCond[i]                 = BIZ_COND;           //업태
            	bizType[i]                 = BIZ_TYPE;           //업종
            	                             
            	bizNo[i]                   = BIZ_NO;             //사업자등록번호
            	custType[i]                = CUST_TYPE;          //화주타입
            	dataSenderNm[i]            = DATA_SENDER_NM;     //쇼핑몰(처리)
            	legacyOrgOrdNo[i]          = LEGACY_ORG_ORD_NO;  //쇼핑몰주문번호(처리)
            	custSeq[i]                 = CUST_SEQ;
            	                             
            	ordDesc[i]                 = ORD_DESC;           //ord_desc
            	dlvMsg1[i]                 = DLV_MSG1;           //배송메세지(처리)
            	dlvMsg2[i]                 = DLV_MSG2;
            	
            	
            	// IF테이블 PK관련 항목 
            	masterSeq[i]      				= (String)model.get("MASTER_SEQ" + i); //묶음번호
            	bfOriOrdNo[i]      				= (String)model.get("BF_ORI_ORD_CD" + i); //묶음번호
            	prdOrdNo[i]          			= (String)model.get("PRD_ORD_NO" + i); //주문번호
            	CustomCode[i]         			= (String)model.get("CUSTOM_CODE" + i); //상품코드
             //   prodSeq[i]       				= (String)model.get("PROD_SEQ" + i); //원주문SEQ
            	                                                     
            }
            
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("vrOrdType"		  , (String)model.get("colI_ORD_TYPE")); //VARCHAR 주문타입 - 출고 
            modelIns.put("vrOrdSubtype"       , (String)model.get("colI_ORD_SUBTYPE"));//VARCHAR WORK01(작업코드) - 정상출고                                           
            modelIns.put("no"                 , no               );//no(처리)                                                  
            modelIns.put("reqDt"              , reqDt            );//출고요청일자(처리)                                        
            modelIns.put("custOrdNo"          , custOrdNo        );//원주문번호(처리)                                          
            modelIns.put("custOrdSeq"         , custOrdSeq       );//원주문seq(처리)                                           
                                                                       
            modelIns.put("trustCustCd"        , trustCustCd      );                                                            
            modelIns.put("transCustCd"        , transCustCd      );                                                            
            modelIns.put("transCustTel"       , transCustTel     );                                                            
            modelIns.put("transReqDt"         , transReqDt       );                                                            
            modelIns.put("custCd"             , custCd           );//화주코드(처리)                                            
                                                                       
            modelIns.put("ordQty"             , ordQty           );//출고수량(처리)                                            
            modelIns.put("uomCd"              , uomCd            );                                                            
            modelIns.put("sdeptCd"            , sdeptCd          );                                                            
            modelIns.put("salePerCd"          , salePerCd        );                                                            
            modelIns.put("carCd"              , carCd            );                                                            
                                                                       
            modelIns.put("drvNm"              , drvNm            );                                                            
            modelIns.put("dlvSeq"             , dlvSeq           );                                                            
            modelIns.put("drvTel"             , drvTel           );                                                            
            modelIns.put("custLotNo"          , custLotNo        );                                                            
            modelIns.put("blNo"               , blNo             );                                                            
                                                                       
            modelIns.put("recDt"              , recDt            );                                                            
            modelIns.put("whCd"               , whCd             );                                                            
            modelIns.put("makeDt"             , makeDt           );                                                            
            modelIns.put("timePeriodDay"      , timePeriodDay    );                                                            
            modelIns.put("workYn"             , workYn           );                                                            
                                                                       
            modelIns.put("rjType"             , rjType           );                                                            
            modelIns.put("locYn"              , locYn            );                                                            
            modelIns.put("confYn"             , confYn           );                                                            
            modelIns.put("eaCapa"             , eaCapa           );                                                            
            modelIns.put("inOrdWeight"        , inOrdWeight      );                                                            
                                                                       
            modelIns.put("itemCd"             , itemCd           );//상품코드(처리)                                            
            modelIns.put("itemNm"             , itemNm           );//상품명(처리)                                              
            modelIns.put("transCustNm"        , transCustNm      );                                                            
            modelIns.put("transCustAddr"      , transCustAddr    );                                                            
            modelIns.put("transEmpNm"         , transEmpNm       );                                                            
                                                                       
            modelIns.put("remark"             , remark           );                                                            
            modelIns.put("transZipNo"         , transZipNo       );                                                            
            modelIns.put("etc2"               , etc2             );                                                            
            modelIns.put("unitAmt"            , unitAmt          );                                                            
            modelIns.put("transBizNo"         , transBizNo       );                                                            
                                                                       
            modelIns.put("inCustAddr"         , inCustAddr       );                                                            
            modelIns.put("inCustCd"           , inCustCd         );                                                            
            modelIns.put("inCustNm"           , inCustNm         );                                                            
            modelIns.put("inCustTel"          , inCustTel        );                                                            
            modelIns.put("inCustEmpNm"        , inCustEmpNm      );                                                            
                                                                       
            modelIns.put("expiryDate"         , expiryDate       );                                                            
            modelIns.put("salesCustNm"        , salesCustNm      );//수령인명(처리)                                            
            modelIns.put("zip"                , zip              );//수령인우편번호(처리)                                      
            modelIns.put("addr"               , addr             );//수령인주소(처리)                                          
            modelIns.put("addr2"              , addr2            );                                                            
                                                                       
            modelIns.put("phone1"             , phone1           );//수령인핸드폰번호(처리)                                    
            modelIns.put("etc1"               , etc1             );                                                            
            modelIns.put("unitNo"             , unitNo           );//:B2C(처리)                                                
            modelIns.put("phone2"             , phone2           );//수령인번호2(처리)                                         
            modelIns.put("buyCustNm"          , buyCustNm        );                                                            
                                                                       
            modelIns.put("buyPhone1"          , buyPhone1        );                                                            
            modelIns.put("salesCompanyNm"     , salesCompanyNm   );//eQtipTp???                                                
            modelIns.put("ordDegree"          , ordDegree        );//등록차수(처리)                                            
            modelIns.put("bizCond"            , bizCond          );//업태                                                      
            modelIns.put("bizType"            , bizType          );//업종                                                      
                                                                       
            modelIns.put("bizNo"              , bizNo            );//사업자등록번호                                            
            modelIns.put("custType"           , custType         );//화주타입                                                  
            modelIns.put("dataSenderNm"       , dataSenderNm     );//쇼핑몰(처리)                                              
            modelIns.put("legacyOrgOrdNo"     , legacyOrgOrdNo   );//쇼핑몰주문번호(처리)                                      
            modelIns.put("custSeq"            , custSeq          );                                                            
                                                                       
            modelIns.put("ordDesc"            , ordDesc          );//ord_desc                                                  
            modelIns.put("dlvMsg1"            , dlvMsg1          );//배송메세지(처리)                                          
            modelIns.put("dlvMsg2"            , dlvMsg2          );                                                            
                                                                       
            modelIns.put("LC_ID",  	 (String)model.get("colI_LC_ID"));  //VARCHAR 
            modelIns.put("WORK_IP",  (String)model.get("colI_WORK_IP")); //VARCHAR 
            modelIns.put("USER_NO",  (String)model.get("colI_USER_NO")); //VARCHAR 
            
         //   System.out.println("SERVIMPL saveExcelOrderB2TS modelIns");
         //   System.out.println(modelIns);
            
            modelIns = (Map<String, Object>)dao030.saveExcelOrderB2TS(modelIns);
            ServiceUtil.isValidReturnCode("WMSIF715", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
            	for(int i = 0; i < tmpCnt; i++){
            		Map<String, Object> modelCallback = new HashMap<String, Object>();
                    modelCallback.put("MASTER_SEQ"		, masterSeq[i]);
                    modelCallback.put("BF_ORI_ORD_CD"	, bfOriOrdNo[i]);
                    modelCallback.put("PRD_ORD_NO"		, prdOrdNo[i]);
                    modelCallback.put("LC_ID"			, (String)model.get("colI_LC_ID"));
                    modelCallback.put("CUST_ID"			, (String)model.get("colI_CUST_ID"));
                    modelCallback.put("WORK_IP"			, (String)model.get("colI_WORK_IP"));
                    modelCallback.put("USER_NO"			, (String)model.get("colI_USER_NO"));
                  	dao.wmsOrdUpdate(modelCallback);
            	}
              
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     * Method ID   : wmsOrderDelete
     * Method 설명    : 출고주문삭제   PK_WMSOP030TS.SP_DELETE_ORDER && IF 테이블 WMS_ORD_REG_FLAG N
     * 작성자               : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> wmsOrderDelete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId 		= new String[tmpCnt];                
                String[] ordSeq	 	= new String[tmpCnt];
                String[] masterSeq	 	= new String[tmpCnt];
                String[] bfOriOrdNo	 	= new String[tmpCnt];
                String[] prdOrdNo	 	= new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    			= (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   			= (String)model.get("ORD_SEQ"+i);
                    masterSeq[i]   			= (String)model.get("MASTER_SEQ"+i);
                    bfOriOrdNo[i]   			= (String)model.get("BF_ORI_ORD_CD"+i);
                    prdOrdNo[i]   			= (String)model.get("PRD_ORD_NO"+i);
                    
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                
                //session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

				//dao - 출고주문삭제   PK_WMSOP030TS.SP_DELETE_ORDER
                modelIns = (Map<String, Object>)dao030.deleteOrder(modelIns);	
                ServiceUtil.isValidReturnCode("WMSIF715", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                
                if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
                	for(int i = 0; i < tmpCnt; i++){
                		Map<String, Object> modelCallback = new HashMap<String, Object>();
                        modelCallback.put("MASTER_SEQ"		, masterSeq[i]);
                        modelCallback.put("BF_ORI_ORD_CD"	, bfOriOrdNo[i]);
                        modelCallback.put("PRD_ORD_NO"		, prdOrdNo[i]);
                        modelCallback.put("LC_ID"			, (String)model.get("colI_LC_ID"));
                        modelCallback.put("CUST_ID"			, (String)model.get("colI_CUST_ID"));
                        modelCallback.put("WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));
                        modelCallback.put("USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
                        
                        //IF 테이블 WMS_REG_FLAG INFO 업데이트
                      	//dao.wmsOrdUpdateRe(modelCallback);
                	}
                  
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
            
        } catch(BizException be) {
        //	System.out.println("serviceImpl - err catch    "+be.getMessage()  );
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    /**
     * Method ID : IFOrderDelete
     * Method 설명 : 비플로우 IF DELYN -> Y wmsif715.updateBflowOrdDelYn
     * 작성자 : SUMMER H
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> IFOrderDelete(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	
    	try {
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
				 Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("MASTER_SEQ", 				model.get("MASTER_SEQ" + i));
	             modelDt.put("BF_ORI_ORD_CD", 			model.get("BF_ORI_ORD_CD" + i));
	             modelDt.put("PRD_ORD_NO", 				model.get("PRD_ORD_NO" + i));
	             modelDt.put("LC_ID", 					(String)model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("CUST_ID", 				model.get("CUST_ID"));
	             modelDt.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
	             dao.IFOrderDelete(modelDt);
    		}
    		
    		map.put("MSG", MessageResolver.getMessage("delete.success"));
    		map.put("errCnt", 0);
    		
    	} catch (Exception e) {
    		map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("errCnt", "1");
    	}
    	
    	return map;
    }
    
    public static String subStrByte(String str, int cutlen) {
        if(!str.isEmpty()) {
            str = str.trim();
            if(str.getBytes().length <= cutlen) {
                return str;
            } else {
                StringBuffer sbStr = new StringBuffer(cutlen);
                int nCnt = 0;
                for(char ch: str.toCharArray())
                {
                    nCnt += String.valueOf(ch).getBytes().length;
                    if(nCnt > cutlen)  break;
                    sbStr.append(ch);
                }
                return sbStr.toString() + "...";
            }
        }
        else {
            return "";
        }
    }
    
    /**
     * Method ID	: getBflowToken
     * Method 설명	: 비플로우 토큰 조회
     * 작성자			: SUMMER H
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public String getBflowToken(Map<String, Object> model) throws Exception {
       
        return (String) dao.getBflowtoken(model);
    }
}