package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmsif.service.WMSIF706Service;

@Service("WMSIF706Service")
public class WMSIF706ServiceImpl implements WMSIF706Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF706Dao")
    private WMSIF706Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }    
  
    
	/**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> syncItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        try{
        	
        	int rowCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
        	
        	String[] nullValue     = new String[rowCnt];        	
        	String[] itemWeight    = new String[rowCnt];
        	String[] itemBarcode   = new String[rowCnt];
        	String[] itemCode      = new String[rowCnt];
        	String[] itemEngNm     = new String[rowCnt];
        	String[] itemKorNm     = new String[rowCnt];
        	String[] itemNm        = new String[rowCnt];
        	String[] makerNm       = new String[rowCnt];
        	String[] unitPrice     = new String[rowCnt];
        	String[] salesPrice    = new String[rowCnt];
        	String[] custId		   = new String[rowCnt];
        	String[] uomId		   = new String[rowCnt];
        	        	
        	model.put("UOM_CD", "EA");
        	String convertUomId = dao.getUomId(model).toString();         	
        	
        	for(int i=0; i < rowCnt; i++){        		        		        		
        		// 공백인 경우 신규채번
//        		itemId[i]		   = StringUtils.isEmpty(vItemId) ? 
//        								dao.getItemId(new HashMap<String, Object>()).toString() : vItemId;
        								
        		nullValue[i]	   = "";
        		itemWeight[i]      = model.get(String.format("I_ITEM_WGT_%d", i)).toString();
        		itemBarcode[i]     = model.get(String.format("I_ITEM_BAR_CD_%d", i)).toString();
        		itemCode[i]        = model.get(String.format("I_ITEM_CODE_%d", i)).toString();
        		itemEngNm[i]       = model.get(String.format("I_ITEM_ENG_NM_%d", i)).toString();
        		itemKorNm[i]       = model.get(String.format("I_ITEM_KOR_NM_%d", i)).toString();
        		itemNm[i]          = model.get(String.format("I_ITEM_NM_%d", i)).toString();
        		makerNm[i]         = model.get(String.format("I_MAKER_NM_%d", i)).toString();
        		unitPrice[i]       = model.get(String.format("I_UNIT_PRICE_%d", i)).toString();
        		salesPrice[i]      = model.get(String.format("I_SALES_PRICE_%d", i)).toString();
        		custId[i]		   = model.get("vrSrchCustIdE1").toString();
        		uomId[i]		   = convertUomId;
        				
        	}        	
        	
        	// Procedure Parameter Set
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("nullValue"	, nullValue);
            modelIns.put("itemWeight"	, itemWeight);
            modelIns.put("itemBarcode"	, itemBarcode);
            modelIns.put("itemCode"		, itemCode);
            modelIns.put("itemEngNm"	, itemEngNm);
            modelIns.put("itemKorNm"	, itemKorNm);			// 5
            
            modelIns.put("itemNm"		, itemNm);
            modelIns.put("makerNm"		, makerNm);
            modelIns.put("unitPrice"	, unitPrice);
            modelIns.put("salesPrice"	, salesPrice);
            modelIns.put("uomId"		, uomId);
            
            modelIns.put("LC_ID"		, model.get("SS_SVC_NO"));
            modelIns.put("CUST_ID"		, custId);
            modelIns.put("WORK_IP"		, model.get("SS_CLIENT_IP"));
            modelIns.put("USER_NO"		, model.get("SS_USER_NO"));                          
            
        	modelIns = (Map<String, Object>) dao.syncItem(modelIns);
        	
        	map.put("O_MSG_CODE", modelIns.get("O_MSG_CODE").toString());
        	map.put("O_MSG_NAME", modelIns.get("O_MSG_NAME").toString());
        	
        }
        catch(Exception e){
        	throw e;
        }
        
        
        return map;
    }


	/**
	 * Method ID	: getOpenMallInfo
	 * Method 설명	: 오픈몰 API 접속정보조회 
	 * @param 
	 * @return
	 */
    public Map<String, Object> getOpenMallInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_APIAUTH", dao.getOpenMallInfo(model));
		return map;
	}
    
    
    /**
     * 
     * 대체 Method ID	: itemInfo
     * 대체 Method 설명	: 
     * 작성자			: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> itemInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.itemInfo(model));
        return map;
    }      

}
