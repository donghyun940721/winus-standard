package com.logisall.winus.wmsif.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.ibatis.common.jdbc.exception.NestedSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.common.service.impl.CmBeanDao;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.model.Wellfood;
import com.logisall.winus.wmsif.service.WMSIF902ResultService;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

import egovframework.rte.psl.orm.ibatis.SqlMapClientTemplate;

@Service
public class WMSIF902ResultServiceImpl implements WMSIF902ResultService {
	private final Log log = LogFactory.getLog(this.getClass());

	// 0000001262는 테스트용
	private static final String[] WELLFOOD_CENTERS = { "0000004460", "0000004461", "0000001262" };

	// 0000553745는 테스트용
	private static final String[] WELLFOOD_CUSTS = { "0000553456", "0000553457", "0000553745" };

	// 대표UOM 일단 EA 고정.
	private static final String[] WELLFOOD_REP_UOM_ID = { "0000018502", "0000018513", "0000001219" };

	private static final String WELLFOOD_BUSAN = "WFD"; // 웰푸드 화주코드
	private static final String WELLFOOD_NAME = "롯데웰푸드";
//  private static final String WELLFOOD_CODE = "0000553456";

	@Autowired
	private WMSIF902RusultDao wmsif902RusultDao;
	
	@Autowired
	private WMSIF902Dao wmsif902Dao;
	
	@Autowired
	private CmBeanDao cmBeanDao;
	
//	@Autowired
//	private SqlMapClient sqlMapClient;
//
//	@Autowired
//	private SqlMapClient sqlMapClientWinusIf;
	@Autowired
	private PlatformTransactionManager txManager;
	@Autowired
	private PlatformTransactionManager txManagerif;

	private SqlMapClientTemplate sqlMapClientTemplate;
	private SqlMapClientTemplate sqlMapClientTemplateIf;

	@Autowired
	public void setSqlMapClientTemplate(SqlMapClient sqlMapClient) {
		this.sqlMapClientTemplate = new SqlMapClientTemplate(sqlMapClient);
	}

	@Autowired
	public void setSqlMapClientTemplateIf(SqlMapClient sqlMapClientWinusIf) {
		this.sqlMapClientTemplateIf = new SqlMapClientTemplate(sqlMapClientWinusIf);
	}
	
//  @Autowired
//  private WMSMS090Dao wmsms090Dao;

	
	public Map<String,Object> selectInResultList(Map<String, Object> model)throws Exception{
        Map<String, Object> map = new HashMap<String, Object>();
        String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
        String wellfoodCustCode = Wellfood.getWellfoodCodeByWinusCode(lcId);
        model.put("LC_ID", lcId);
        model.put("WELLFOOD_CUST_ID", wellfoodCustCode);
        List<Map<String, Object>> custList = ((List<Map<String, Object>>) cmBeanDao.selectMngCodeCUST(model));
        String custId = "";
        for (Map<String, Object> custMap : custList) {
            String custCode = custMap.get("CODE").toString();
            if (wellfoodCustCode.equals(custCode)) {
                custId = custMap.get("ID").toString();
                break;
            }
        }
        if ("".equals(custId)) {
            throw new NullPointerException("WFD 화주코드 미등록.");
        }
        model.put("CUST_ID", custId);

        List<Object> inOrder = wmsif902RusultDao.selectOutOrderResultList(model);
        List<Object> inResult = wmsif902RusultDao.selectOutOrderSendList(model);
        
        map.put("outOrder", inOrder);
        map.put("outOrderResult", inResult);
        return map;
    }
	
	@Override
	public Map selectOutOrderResultList(Map<String, Object> model) throws SQLException {
		Map<String, Object> map = new HashMap<String, Object>();
		String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
		model.put("LC_ID", lcId);
		model.put("CUST_ID", WELLFOOD_BUSAN);
		List<Object> resultList = wmsif902RusultDao.selectOutOrderResultList(model);
		List<Object> outSendResultList = wmsif902RusultDao.selectOutOrderSendList(model);

		map.put("errCnt", "0");
		map.put("outOrderList", resultList);
		map.put("outSendResultList", outSendResultList);
		return map;
	}

	@Override
	public Map<String, Object> makeOutResult(Map<String, Object> model, Map<String, Object> reqMap) throws Exception{
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status1 = txManager.getTransaction(def);
        TransactionStatus status2 = txManagerif.getTransaction(def);
        Map<String, Object> resultMap = new HashMap<String, Object>();
	    try{
	        List<Map<String, Object>> outOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
	        String sendSysId = wmsif902RusultDao.selectOutOrderResultKey(model);
	        for (Map<String, Object> outOrder : outOrderList) {
	        	outOrder.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
	        	outOrder.put("USER_NO", model.get(ConstantIF.SS_USER_NO));
	        	outOrder.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
	        	outOrder.put("IF_SYSID", sendSysId);
	        	 //ITF_COM_YN 확인
//	            if(wmsif902Dao.checkItfComYn(outOrder).equals("Y")){
//	                throw new BizException("이미 접수된 건이 있습니다.");
//	            }
	            //WMSOP011 COUNT 확인
//                if(!(wmsif902Dao.checkCountOrder(outOrder)).equals(0)){
//                    throw new BizException("완료되지 않은 주문이 있습니다.");
//                }
                //INSERT WELLFOOD_INBOUND_RESULT
	            sqlMapClientTemplateIf.insert("wmsif902R.insertOutOrderResult", outOrder);
	            sqlMapClientTemplate.update("wmsif902.updateItfComYn", outOrder);
			}
	        for (Map<String, Object> outOrder : outOrderList) {
	        	   wmsif902Dao.updateItfComYn(outOrder);
	        }
	       
	        txManager.commit(status1);
	        txManagerif.commit(status2);
	        resultMap.put("errCnt", "0");
	        resultMap.put("MSG", MessageResolver.getMessage("save.success"));

	    }catch(BizException e){
	    	txManager.rollback(status1);
	    	txManagerif.rollback(status2);
	    	resultMap.put("errCnt", "1");
	    	resultMap.put("MSG", e.getMessage());
	    }catch(Exception e){
	    	txManager.rollback(status1);
	    	txManagerif.rollback(status2);
	    	throw e;
	    }
	    return resultMap;
    }
	
}
