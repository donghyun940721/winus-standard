package com.logisall.winus.wmsif.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
public class WMSIF904ServiceImpl implements WMSIF904Service {
	private final org.apache.commons.logging.Log log = LogFactory.getLog(this.getClass());
	@Autowired
	private WMSIF000Service WMSIF000Service;
	@Autowired
	private WMSOP030Dao WMSOP030Dao;
	@Autowired
	private WMSIF904Dao dao;

	@Override
	public List<Map<String, Object>> selectMallList(Map<String, Object> model) {
		return dao.selectMallList(model);
	}

	@Override
	public Map selectOrderList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();

		Map<String, Object> map = new HashMap<String, Object>();
		String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
		// String testCustId = "0000020164"; //LOGISALL > 0000020164 테스트 동안만...
//	model.put("LC_ID", lcId);
//	model.put("CUST_ID", WELLFOOD_BUSAN);
		List<Object> resultList = dao.selectOrderList(model);
		wqrs.setTotCnt(resultList.size());
		wqrs.setList(resultList);
		map.put("LIST", wqrs);
		return map;
	}

	@Override
	@Transactional
	public Map<String, Object> saveOutboundOrders(Map<String, Object> model, Map<String, Object> reqMap)
			throws Exception {
		List<Map<String, Object>> outOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
		outOrderList = addOrderSeq(outOrderList);
		Map<String, Object> resultMap = new HashMap<String, Object>();

		Map<String, Object> modelIns = new HashMap<String, Object>();
		String lcId = (String) model.get("SS_SVC_NO");
		Map<String, String[]> orderParamMap = CommonUtil.convertMapStringArrayFromListMap(outOrderList,
				new String[] { "CUST_ID", "CUST_CD", "CUST_NM", "ORD_DEGREE", "MALL_ID", "SHOP_NO", "SHOP_NAME",
						"ORDER_ID", "ORDER_SEQ", "PRODUCT_CODE", "PRODUCT_NAME", "PRODUCT_NAME_DEFAULT",
						"PRODUCT_PRICE", "QUANTITY", "RECEIVER_ADDRESS_FULL", "RECEIVER_PHONE", "RECEIVER_CELLPHONE",
						"RECEIVER_NAME", "RECEIVER_ZIPCODE", "SHIPPING_CODE", "SHIPPING_MESSAGE" });
		String sysdate = DateUtil.getSysDate();
		int ordSize = outOrderList.size();
//	String calInDt = DateUtil.isCorrectDate(inReqDate) ? inReqDate : sysdate; // 날짜유형 안맞으면 입고일 오늘날짜로
		String[] emptyValue = new String[ordSize];
		String[] outReqDt = new String[ordSize];
		Arrays.fill(emptyValue, "");
		Arrays.fill(outReqDt, sysdate);

		modelIns.put("vrOrdType", "O"); // 출고주문이기에 고정값 추후 수정해야할수 있음
//	modelIns.put("vrOrdType"        , (String)model.get("ordType")); // 주문타입 
		modelIns.put("no", emptyValue);
		modelIns.put("reqDt", outReqDt); // 출고예정일
		modelIns.put("custOrdNo", orderParamMap.get("ORDER_ID")); // 원주문번호 = CAFE24주문번호
		modelIns.put("custOrdSeq", orderParamMap.get("ORDER_SEQ"));

		modelIns.put("trustCustCd", emptyValue);
		modelIns.put("transCustCd", orderParamMap.get("SHOP_NO")); // 배송처코드 = 쇼핑몰의 판매처코드
		modelIns.put("transCustTel", emptyValue);
		modelIns.put("transReqDt", emptyValue);
		modelIns.put("custCd", orderParamMap.get("CUST_CD")); // 화주코드

		modelIns.put("ordQty", orderParamMap.get("QUANTITY")); // 주문수량
		modelIns.put("uomCd", emptyValue);
		modelIns.put("sdeptCd", emptyValue);
		modelIns.put("salePerCd", emptyValue);
		modelIns.put("carCd", emptyValue);

		modelIns.put("drvNm", emptyValue);
		modelIns.put("dlvSeq", emptyValue);
		modelIns.put("drvTel", emptyValue);
		modelIns.put("custLotNo", emptyValue);
		modelIns.put("blNo", emptyValue);

		modelIns.put("recDt", emptyValue);
		modelIns.put("whCd", emptyValue);
		modelIns.put("makeDt", emptyValue);
		modelIns.put("timePeriodDay", emptyValue);
		modelIns.put("workYn", emptyValue);

		modelIns.put("rjType", emptyValue);
		modelIns.put("locYn", emptyValue);
		modelIns.put("confYn", emptyValue);
		modelIns.put("eaCapa", emptyValue);
		modelIns.put("inOrdWeight", emptyValue);

		modelIns.put("itemCd", orderParamMap.get("PRODUCT_CODE")); // 상품코드 = 쇼핑몰상품코드
		modelIns.put("itemNm", orderParamMap.get("PRODUCT_NAME"));
		modelIns.put("transCustNm", orderParamMap.get("SHOP_NAME")); // 배송처명 = 판매처명
		modelIns.put("transCustAddr", emptyValue);
		modelIns.put("transEmpNm", emptyValue);

		modelIns.put("remark", emptyValue);
		modelIns.put("transZipNo", emptyValue);
		modelIns.put("etc2", emptyValue); // WMSOM010.ORD_DESC 와 매핑
		modelIns.put("unitAmt", emptyValue); // 단가
		modelIns.put("transBizNo", emptyValue);

		modelIns.put("inCustAddr", emptyValue);
		modelIns.put("inCustCd", emptyValue);
		modelIns.put("inCustNm", emptyValue);
		modelIns.put("inCustTel", emptyValue);
		modelIns.put("inCustEmpNm", emptyValue);

		modelIns.put("expiryDate", emptyValue);
		modelIns.put("salesCustNm", orderParamMap.get("RECEIVER_NAME")); // 받는사람
		modelIns.put("zip", orderParamMap.get("RECEIVER_ZIPCODE")); // 우편번호
		modelIns.put("addr", orderParamMap.get("RECEIVER_ADDRESS_FULL")); // 받는 주소
		modelIns.put("addr2", emptyValue);

		modelIns.put("phone1", orderParamMap.get("RECEIVER_CELLPHONE")); // 전화번호
		modelIns.put("etc1", emptyValue);
		modelIns.put("unitNo", emptyValue);
		modelIns.put("phone2", orderParamMap.get("RECEIVER_PHONE")); // 전화번호
		modelIns.put("buyCustNm", emptyValue);

		modelIns.put("buyPhone1", emptyValue);
		modelIns.put("salesCompanyNm", emptyValue);
		modelIns.put("ordDegree", orderParamMap.get("ORD_DEGREE")); // 주문차수
		modelIns.put("bizCond", emptyValue);
		modelIns.put("bizType", emptyValue);

		modelIns.put("bizNo", emptyValue);
		modelIns.put("custType", emptyValue);
		modelIns.put("dataSenderNm", orderParamMap.get("SHOP_NAME")); // 채널 = 쇼핑몰명
		modelIns.put("legacyOrgOrdNo", orderParamMap.get("ORDER_ID")); // 주문번호(쇼핑몰) = 쇼핑몰주문번호
		modelIns.put("custSeq", emptyValue);

		modelIns.put("ordDesc", emptyValue);
		modelIns.put("dlvMsg1", orderParamMap.get("SHIPPING_MESSAGE")); // 배송메세지
		modelIns.put("dlvMsg2", emptyValue);
		modelIns.put("locCd", emptyValue);

		modelIns.put("LC_ID", lcId);
		modelIns.put("WORK_IP", (String) model.get("SS_CLIENT_IP"));
		modelIns.put("USER_NO", (String) model.get("SS_USER_NO"));

		modelIns.put("reqDt", outReqDt); // 출고예정일

		try {
			resultMap = (Map<String, Object>) WMSOP030Dao.saveExcelOrderB2T(modelIns);
			ServiceUtil.isValidReturnCode("WMSIF904", String.valueOf(resultMap.get("O_MSG_CODE")),
					(String) resultMap.get("O_MSG_NAME"));
			for (Map<String, Object> outbOrder : outOrderList) {
				outbOrder.put("LC_ID", lcId);
				dao.updateCafe24OrderStatus(outbOrder);
			}
			resultMap.put("errCnt", 0);
			resultMap.put("MSG", MessageResolver.getMessage("save.sucess"));
		} catch (BizException be) {
			resultMap.put("errCnt", -1);
			resultMap.put("MSG", be.getMessage());
		}
//	catch (UncategorizedSQLException se) {
//	    resultMap.put("errCnt", -1);
//	    resultMap.put("MSG", se.getMessage());
//	} 
		catch (Exception e) {
			throw e;
		}
		return resultMap;
	}

	/**
	 * CAFE24 I/F 운송장전송 할 리스트
	 */
	@Override
	public Map selectOrderShipmentList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Object> winusOrdList = dao.selectOrderShipmentList(model);

		List<Object> cafe24ShipmentList = dao.selectCafe24ShipmentList(model);
		List<Object> joinList = new ArrayList<Object>();
		/**
		 * 위너스 주문테이블과 CAFE24 주문테이블 매핑
		 */
		for (int i = 0; i < cafe24ShipmentList.size(); i++) {
			Map<String, Object> cafeOrderMap = (Map<String, Object>) cafe24ShipmentList.get(i);
			String cafeOrderId = (String) cafeOrderMap.get("ORDER_ID");
			String cafeIfYn = (String) cafeOrderMap.get("IF_SEND_YN");
			String cafeSendTiemstamp = (String) cafeOrderMap.get("WMS_SEND_TIMESTAMP");
			for (int j = 0; j < winusOrdList.size(); j++) {
				Map<String, Object> winusOrderMap = (Map<String, Object>) winusOrdList.get(j);
				String OrgOrdId = (String) winusOrderMap.get("ORG_ORD_ID");
				if (cafeOrderId.equals(OrgOrdId)) {
					winusOrderMap.put("IF_SEND_YN", cafeIfYn);
					winusOrderMap.put("WMS_SEND_TIMESTAMP", cafeSendTiemstamp);
					joinList.add(winusOrderMap);
					break;
				}
			}
		}

		wqrs.setTotCnt(joinList.size());
		wqrs.setList(joinList);
		resultMap.put("LIST", wqrs);
		return resultMap;
	}

	@Override
	public Map<String, Object> saveCafe24Shipment(HttpServletRequest request, Map<String, Object> reqMap)
			throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> eaiArguments = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> orderList = (List<Map<String, Object>>) reqMap.get("orderList");
			if (orderList.size() == 0) {
				throw new BizException("전송할 주문이 없습니다.");
			}
			for (Map<String, Object> order : orderList) {
				String CJLogisticsCode = "0006";
				String shippingStatus = "shipping";
				order.put("shop_no", "1");
				order.put("order_id", order.get("ORG_ORD_ID").toString());
				order.put("tracking_no", order.get("INVC_NO").toString());
				order.put("shipping_company_code", CJLogisticsCode);
				order.put("status", shippingStatus);
			}
			JSONObject jsonObject = new JSONObject();
			String mall_id = reqMap.get("vrSrchMallId").toString();
			String url = "CAFE24/orders/shipment";
			jsonObject.put("mall_id", mall_id);
			jsonObject.put("request", orderList);
			String jsonString = jsonObject.toString();

			eaiArguments.put("data", jsonString);
			eaiArguments.put("cMethod", HttpMethod.POST.toString());
			eaiArguments.put("cUrl", url);
			eaiArguments.put("hostUrl", request.getServerName());

			m = WMSIF000Service.crossDomainHttpWs5200New(eaiArguments);
			if (m.containsKey("header") && m.get("header").equals("-1")) {// 웹메서드 에러시
				String errMsg = (String) m.get("message");
				throw new BizException(errMsg);
			}
		} catch (BizException be) {
			m.put("errCnt", "1");
			m.put("MSG", be.getMessage());
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	private List<Map<String, Object>> addOrderSeq(List<Map<String, Object>> orders) {
		Map<String, Integer> orderSeqMap = new HashMap<>();

		for (Map<String, Object> order : orders) {
			String orderId = order.get("ORDER_ID").toString();
			int orderSeq = orderSeqMap.containsKey(orderId) ? orderSeqMap.get(orderId) + 1 : 1;
			orderSeqMap.put(orderId, orderSeq);
			order.put("ORDER_SEQ", orderSeq);
		}

		return orders;
	}

	@Override
	public Map<String, Object> deleteOrderAsn(HttpServletRequest request, Map<String, Object> reqMap) {
		return null;
	}

}
