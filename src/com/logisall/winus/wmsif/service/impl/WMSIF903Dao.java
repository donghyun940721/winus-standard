package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
public class WMSIF903Dao extends SqlMapAbstractDAO {
    private final Log log = LogFactory.getLog(this.getClass());
    // WINUSUSR
    @Autowired
    private SqlMapClient sqlMapClient;

    /**
     * Method ID    : listE1
     * Method 설명    : 샵링커 주문연동 조회
     * 작성자               : schan
     * @param   model
     * @return
     */
    public List listE1(Map<String, Object> model) {
        return executeQueryForList("wmsif903.listE1", model);
    }
    
    /**
     * Method ID    : updateShopLinkerOrder
     * Method 설명    : 출고주문 동기화 완료처리
     * 작성자               : schan
     * @param   model
     * @return
     */
    public Object updateShopLinkerOrder(Map<String, Object> model) {
        executeUpdate("wmsif903.updateShopLinkerOrder", model);
        return model;
    }
    
    /**
     * Method ID    : listE2
     * Method 설명    : 샵링커 주문연동 조회
     * 작성자               : schan
     * @param   model
     * @return
     */
    public List listE2(Map<String, Object> model) {
        return executeQueryForList("wmsif903.listE2", model);
    }
    
    public Object getShopLinkerSyncOrgOrdNo(Map<String, Object> model) {
        return executeQueryForObject("wmsif903.getShopLinkerSyncOrgOrdNo", model);
    }
    
    public Object selectShopLinkerIfYnValidation(Map<String, Object> model) {
        return executeQueryForObject("wmsif903.selectShopLinkerIfYnValidation", model);
    }
    
    
}
