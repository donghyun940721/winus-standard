package com.logisall.winus.wmsif.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Repository("WMSIF600Dao")
public class WMSIF600Dao extends SqlMapAbstractDAO{
	
	/**
     * Method ID : list
     * Method 설명 : 오픈몰 주문 리스트 (사방넷)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif600.list", model);
    }
    
    /**
     * Method ID : sapbp
     * Method 설명 : sapbp
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet sapbp(Map<String, Object> model) {
        return executeQueryPageWq("wmsif600.sapbp", model);
    }
    
    /**
	 * Method ID : saveSap
	 * Method 설명 : sap 마스터 수정 저장
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void saveSap(Map<String, Object> model) {
		executeUpdate("wmsif600.saveSap", model);
		
	}
	/**
	 * Method ID : insertSap
	 * Method 설명 : sap 마스터 신규 저장
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void insertSap(Map<String, Object> model) {
		executeInsert("wmsif600.insertSap", model);
		
	}
	/**
	 * Method ID : deleteSap
	 * Method 설명 : sap 마스터 삭제
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void deleteSap(Map<String, Object> model) {
		executeInsert("wmsif600.deleteSap", model);
		
	}
	
	/**
	 * Method ID : sapListDelete
	 * Method 설명 : sap 주문 삭제
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void sapListDelete(Map<String, Object> model) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			String orgOrdId = (String) model.get("IDX");
			// existRsSet = WMSOP010 원주문번호로 조회 DEL_YN 체크
			String existRsSet = (String)sqlMapClient.queryForObject("wmsif600.selectDelYn", orgOrdId);
			
			// existRsSet NULL 일 경우 OP010에 등록이 안된 상태
			if ((String)existRsSet == null){
//				throw new Exception("주문번호(사방넷) : "+orgOrdId+" 은/는 등록되지 않은 주문입니다. ");
				sqlMapClient.delete("wmsif600.deleteDelYn"	, orgOrdId);
			}else{
				// existRsSet Y 일 경우 삭제 
				if(existRsSet.equals("N")){
					// existRsSet Y 아닐 경우 삭제 X 
					throw new Exception("주문번호(사방넷) : "+orgOrdId+" 의 출고 주문을 먼저 삭제해주세요.");
				}else{
					sqlMapClient.delete("wmsif600.deleteDelYn"	, orgOrdId);
				}
			}
			
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			sqlMapClient.endTransaction();
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}

	/**
	 * Method ID : saveSabangnetCallback 
	 * Method 설명 : 
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object saveSabangnetCallback(Map<String, Object> model){
		executeUpdate("wmsif600.wmsif108.saveSabangnetCallback", model);
		return model;
	}
    
    /**
	 * Method ID : sabangnetResetInvoice 
	 * Method 설명 : 사방넷 송장송신여부 Flag 초기화
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
    public Object sabangnetResetInvoice(Map<String, Object> model){
        executeUpdate("wmsif600.wmsif108.sabangnetResetInvoice", model);
        return model;
    }
    
	/**
	 * Method ID : saveSabangnetCallback_step2 
	 * Method 설명 : 박스추천 CALLBACK
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
    public Object saveSabangnetCallback_step2(Map<String, Object> model){
        executeUpdate("wmsif600.wmsif108.saveSabangnetCallback_step2", model);
        return model;
    }
    
	/**
	 * Method ID : spSabangnetDeleteOrder 
	 * Method 설명 : 사방넷 주문삭제
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
    public Object spSabangnetDeleteOrder(Map<String, Object> model){
        executeUpdate("wmsif600.wmsif108.spSabangnetDeleteOrder", model);
        return model;
    }
}
