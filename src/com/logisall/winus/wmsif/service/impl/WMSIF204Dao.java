package com.logisall.winus.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF204Dao")
public class WMSIF204Dao extends SqlMapAbstractDAO{
	/**
     * Method ID  : getStatus
     * Method 설명  : 
     * 작성자             : 
     * @param model
     * @return
     */
    public Object getStatus(Map<String, Object> model){
        return executeQueryForList("wmsif204.getStatus", model);
    }
}


