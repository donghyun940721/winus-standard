package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmsif.service.WMSIF708Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;

@Service("WMSIF708Service")
public class WMSIF708ServiceImpl implements WMSIF708Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF708Dao")
    private WMSIF708Dao dao;
    
	@Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao030;
	
    
    /**
     * 
     * 대체 Method ID		: listE1
     * 대체 Method 설명	: 상품정보 조회 
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) { 
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }          
        
        
        map.put("LIST", dao.listE1(model));
        return map;
    }    
    
    
    /**
     * 
     * 대체 Method ID		: listE2
     * 대체 Method 설명	: 매입처 조회 
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) { 
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }          
               
        
        map.put("LIST", dao.listE2(model));
        return map;
    }    
    
    
    /**
     * 
     * 대체 Method ID		: listE3
     * 대체 Method 설명	: 매출처 조회
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) { 
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }          
               
        
        map.put("LIST", dao.listE3(model));
        return map;
    }    
    
    
    /**
     * 
     * 대체 Method ID		: syncItem
     * 대체 Method 설명	: 상품정보 등록 (스카이젯 → Winus) 
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> syncItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        try{
        	
        	int rowCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
        	
        	String[] nullValue     	= new String[rowCnt];
        	String[] nullValue2048 = new String[rowCnt];
        	String[] unitPrice    	= new String[rowCnt];
        	String[] repUomId   	= new String[rowCnt];
        	String[] itemCode     	= new String[rowCnt];
        	String[] itemBarCd      = new String[rowCnt];
        	String[] itemGrpCd      = new String[rowCnt];
        	
        	String[] itemGrp2ndCd   = new String[rowCnt];
        	String[] itemGrp3rdCd   = new String[rowCnt];
        	String[] itemEngNm     	= new String[rowCnt];
        	String[] itemKorNm     	= new String[rowCnt];
        	String[] itemShortNm    = new String[rowCnt];      
        	
        	String[] itemNm        	= new String[rowCnt];        	
        	String[] makerNm       	= new String[rowCnt];    	
        	String[] syncYn       	= new String[rowCnt];
        	String[] makeNoFlag		= new String[rowCnt];
        	String[] dateFlag		= new String[rowCnt];
        	String[] serialFlag		= new String[rowCnt];
        	String[] makeDateFlag	= new String[rowCnt];
        	String[] custId		   	= new String[rowCnt];        	
        	        	
        	//model.put("UOM_CD", "EA"); // TODO :: 제거
        	//String convertUomId = dao.getUomId(model).toString(); // TODO :: 제거   	
        	
        	for(int i=0; i < rowCnt; i++){       		        		        		
        								
        		nullValue[i]	   = "";
        		nullValue2048[i] = "";
        		unitPrice[i]       = model.get(String.format("I_UNIT_PRICE_%d", i)).toString();
        		//repUomId[i]        = convertUomId;		// TODO :: 제거
        		repUomId[i]        = model.get(String.format("I_REP_UOM_ID_%d", i)).toString(); // TODO :: 주석해제 - UOM코드 (프로시저에서 ID변환)
        		itemCode[i]        = model.get(String.format("I_ITEM_CODE_%d", i)).toString();
        		itemBarCd[i]       = model.get(String.format("I_ITEM_BAR_CD_%d", i)).toString();
        		itemGrpCd[i]       = model.get(String.format("I_ITEM_GRP_CD_%d", i)).toString();

        		itemGrp2ndCd[i]    = model.get(String.format("I_ITEM_GRP_2ND_CD_%d", i)).toString();
        		itemGrp3rdCd[i]    = model.get(String.format("I_ITEM_GRP_3RD_CD_%d", i)).toString();
        		itemEngNm[i]       = model.get(String.format("I_ITEM_ENG_NM_%d", i)).toString();
        		itemKorNm[i]       = model.get(String.format("I_ITEM_KOR_NM_%d", i)).toString();
        		itemShortNm[i]     = model.get(String.format("I_ITEM_SHORT_NM_%d", i)).toString();
        		
        		itemNm[i]          = model.get(String.format("I_ITEM_NM_%d", i)).toString();
        		makerNm[i]         = model.get(String.format("I_MAKER_NM_%d", i)).toString();
        		
        		syncYn[i]          = model.get(String.format("I_SYNC_YN_%d", i)).toString();
        	    makeNoFlag[i]	   = model.get(String.format("I_MAKENO_FLAG_%d", i)).toString();
        		dateFlag[i]	       = model.get(String.format("I_DATE_FLAG_%d", i)).toString();
        		serialFlag[i]	   = model.get(String.format("I_SERIAL_FLAG_%d", i)).toString();
        		makeDateFlag[i]	   = model.get(String.format("I_MAKEDATE_FLAG_%d", i)).toString();
        		custId[i]		   = model.get("vrSrchCustIdE1").toString();
        		
        	}        	
        	
        	// Procedure Parameter Set
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("nullValue"	        , nullValue);
            modelIns.put("nullValue2048", nullValue2048);
            modelIns.put("I_UNIT_PRICE"	        , unitPrice);
            modelIns.put("I_REP_UOM_ID"	        , repUomId);
            modelIns.put("I_ITEM_CODE"		    , itemCode);
            modelIns.put("I_ITEM_BAR_CD"	    , itemBarCd);
            modelIns.put("I_ITEM_GRP_CD"	    , itemGrpCd);

            modelIns.put("I_ITEM_GRP_2ND_CD"	, itemGrp2ndCd);
            modelIns.put("I_ITEM_GRP_3RD_CD"	, itemGrp3rdCd);
            modelIns.put("I_ITEM_ENG_NM"		, itemEngNm);
            modelIns.put("I_ITEM_KOR_NM"	    , itemKorNm);
            modelIns.put("I_ITEM_SHORT_NM"	    , itemShortNm);
            
            modelIns.put("I_ITEM_NM"		    , itemNm);
            modelIns.put("I_MAKER_NM"		    , makerNm);
            
            modelIns.put("I_SYNC_YN"		    , syncYn);
            modelIns.put("I_MAKENO_FLAG"		, makeNoFlag);
            modelIns.put("I_DATE_FLAG"		    , dateFlag);
            modelIns.put("I_SERIAL_FLAG"		, serialFlag);
            modelIns.put("I_MAKEDATE_FLAG"		, makeDateFlag);
            
            modelIns.put("LC_ID"		        , model.get("SS_SVC_NO"));
            modelIns.put("CUST_ID"		        , custId);
            modelIns.put("WORK_IP"		        , model.get("SS_CLIENT_IP"));
            modelIns.put("USER_NO"		        , model.get("SS_USER_NO"));                
            
        	modelIns = (Map<String, Object>) dao.syncItem(modelIns);
        	
        	map.put("O_MSG_CODE", modelIns.get("O_MSG_CODE").toString());
        	map.put("O_MSG_NAME", modelIns.get("O_MSG_NAME").toString());
        	
        }
        catch(Exception e){
        	throw e;
        }
        
        
        return map;
    }

}