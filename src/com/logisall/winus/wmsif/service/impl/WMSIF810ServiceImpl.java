package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF810Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF810Service")
public class WMSIF810ServiceImpl extends AbstractServiceImpl implements WMSIF810Service{

	@Resource(name = "WMSIF810Dao")
    private WMSIF810Dao dao;
	
	/**
     * Method ID : listE01
     * Method 설명 : 대화물류 IF 조회 - 거래처
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
	public Map<String, Object> listE01(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE01(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE02
     * Method 설명 : 대화물류 IF 조회 - 상품
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
	public Map<String, Object> listE02(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE02(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE03
     * Method 설명 : 대화물류 IF 조회 - 입고
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
	public Map<String, Object> listE03(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE03(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE02
     * Method 설명 : 대화물류 IF 조회 - 재고
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
	public Map<String, Object> listE04(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE04(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE05
     * Method 설명 : 대화물류 IF 조회 - 주문
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
	public Map<String, Object> listE05(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE05(model));
            map.put("TOTAL_CNT", dao.listE05SyncCount(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE06
     * Method 설명 : 대화물류 IF 조회 - 출고
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
	public Map<String, Object> listE06(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE06(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	//E01
	
	
	//E02
		
		
	//E03
		
		
	//E04
		
		
	//E05
	/**
    * Method ID   : syncIF
    * Method 설명    : 출고주문동기화 
    * 작성자               : SUMMER H
    * @param model
    * @return
    * @throws Exception
    */
   @Override
   public Map<String, Object> syncIF(Map<String, Object> model) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       try{

    	   int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
           // 기본프로시저 
           String[] keyNo           = new String[tmpCnt]; 
           
           String[] outD            = new String[tmpCnt]; 
           String[] orgOrdId        = new String[tmpCnt]; 
           String[] orgOrdSeq       = new String[tmpCnt];
           String[] outCompleteDay  = new String[tmpCnt];
           String[] agentId         = new String[tmpCnt];
           
           String[] goodsId        	= new String[tmpCnt];
           String[] goodsNm        	= new String[tmpCnt];
           String[] itemBarCd       = new String[tmpCnt];
           String[] goodsGbn        = new String[tmpCnt];
           String[] blNo         	= new String[tmpCnt];
           
           String[] lotNo         	= new String[tmpCnt];

           String[] expD         	= new String[tmpCnt];
           String[] ordDesc         = new String[tmpCnt];
           String[] inMngNo         = new String[tmpCnt];
           String[] orderGbn        = new String[tmpCnt];
           String[] rjYn         	= new String[tmpCnt];
           String[] ordQty         	= new String[tmpCnt];
    
           String[] saleAmt         = new String[tmpCnt];
           String[] saleVat         = new String[tmpCnt];
           String[] dlvMsg         	= new String[tmpCnt];
           String[] salesCustNm     = new String[tmpCnt];
           String[] zip         	= new String[tmpCnt];
 
           String[] addr         	= new String[tmpCnt];
           String[] phone         	= new String[tmpCnt];
           String[] custCd          = new String[tmpCnt];
           String[] uomCd         	= new String[tmpCnt];

           for (int i = 0; i < tmpCnt; i++) {
           	
        	String KEY_NO               = Integer.toString(i);;

        	String OUT_D               	= (String)model.get("OUT_D" + i);
        	String ORG_ORD_ID          	= (String)model.get("ORG_ORD_ID" + i);
           	String ORG_ORD_SEQ			= (String)model.get("ORG_ORD_SEQ" + i);
           	String OUT_COMPLETE_D		= (String)model.get("OUT_COMPLETE_D" + i);
           	String AGENT_ID				= (String)model.get("AGENT_ID" + i);
           	
           	String GOODS_ID				= (String)model.get("GOODS_ID" + i);
           	String GOODS_NM				= (String)model.get("GOODS_NM" + i);
           	String ITEM_BAR_CD			= (String)model.get("ITEM_BAR_CD" + i);
           	String GOODS_GBN			= (String)model.get("GOODS_GBN" + i);
           	String BL_NO				= (String)model.get("BL_NO" + i);
           	
           	String LOT_NO 				= (String)model.get("LOT_NO" + i);
           	
           	String EXP_D				= (String)model.get("EXP_D" + i);
         	String ORD_DESC				= String.valueOf(model.get("ORD_DESC" + i));
           	String IN_MNG_NO			= String.valueOf(model.get("IN_MNG_NO" + i));
           	String ORDER_GBN                = (String)model.get("ORDER_GBN" + i);
           	String RJ_YN                = (String)model.get("RJ_YN" + i);
           	String ORDER_QTY			= (String)model.get("ORDER_QTY" + i);

           	String SALE_AMT				= (String)model.get("SALE_AMT" + i);
           	String SALE_VAT				= (String)model.get("SALE_VAT" + i);
           	String DLV_MSG1				= (String)model.get("DLV_MSG1" + i);
           	String SALES_CUST_NM		= (String)model.get("SALES_CUST_NM" + i);
           	String ZIP					= (String)model.get("ZIP" + i);

           	String ADDR					= (String)model.get("ADDR" + i);
           	String PHONE_1 				= (String)model.get("PHONE_1" + i);
           	String CUST_CD		        = (String)model.get("CUST_CD" + i);
           	String UOM_CD 				= (String)model.get("UOM_CD" + i);
           	
           	//변수 
           	keyNo[i]                     = KEY_NO;      

           	outD[i]                      = OUT_D;      
           	orgOrdId[i]                  = ORG_ORD_ID;    
           	orgOrdSeq[i]                 = ORG_ORD_SEQ;     
           	outCompleteDay[i]            = OUT_COMPLETE_D;          
           	agentId[i]                   = AGENT_ID;   
           	                             
           	goodsId[i]                   = GOODS_ID;   
           	goodsNm[i]                   = GOODS_NM;  
           	itemBarCd[i]                 = ITEM_BAR_CD;     
           	goodsGbn[i]                  = GOODS_GBN;    
           	blNo[i]                      = BL_NO;
           	                             
           	lotNo[i]                     = LOT_NO; 
           	                             
           	expD[i]                      = EXP_D;                     
           	ordDesc[i]                   = ORD_DESC;   
           	inMngNo[i]                   = IN_MNG_NO;
           	orderGbn[i]                  = ORDER_GBN;    
           	rjYn[i]                      = RJ_YN;
           	ordQty[i]                    = ORDER_QTY;  
           	                             
           	saleAmt[i]                   = SALE_AMT;   
           	saleVat[i]                   = SALE_VAT;   
           	dlvMsg[i]                    = DLV_MSG1;  
           	salesCustNm[i]               = SALES_CUST_NM;          
           	zip[i]                       = ZIP;        	
           	                             
           	addr[i]                      = ADDR;         	
           	phone[i]                     = PHONE_1;          
           	custCd[i]                    = CUST_CD;          
           	uomCd[i]                     = UOM_CD;   
           	                                                     
           }
           
           // 프로시져에 보낼것들 다담는다
           Map<String, Object> modelIns = new HashMap<String, Object>();

           modelIns.put("I_KEY_NO"           ,   keyNo);                                             

           modelIns.put("I_OUT_D"            ,   outD);                                             
           modelIns.put("I_ORG_ORD_ID"       ,   orgOrdId);                                             
           modelIns.put("I_ORG_ORD_SEQ"      ,   orgOrdSeq);                                             
           modelIns.put("I_OUT_COMPLETE_D"   ,   outCompleteDay);                                             
           modelIns.put("I_AGENT_ID"         ,   agentId);                                             

           modelIns.put("I_GOODS_ID"         ,   goodsId);                                             
           modelIns.put("I_GOODS_NM"         ,   goodsNm);                                             
           modelIns.put("I_ITEM_BAR_CD"      ,   itemBarCd);                                             
           modelIns.put("I_GOODS_GBN"        ,   goodsGbn);                                             
           modelIns.put("I_BL_NO"            ,   blNo);                                             

           modelIns.put("I_LOT_NO"           ,   lotNo);                                             

           modelIns.put("I_EXP_D"            ,   expD);                                             
           modelIns.put("I_ORD_DESC"         ,   ordDesc); 
           modelIns.put("I_IN_MNG_NO"        ,   inMngNo);   
           modelIns.put("I_ORDER_GBN"        ,   orderGbn);                                             
           modelIns.put("I_RJ_YN"            ,   rjYn);                                             
           modelIns.put("I_ORDER_QTY"        ,   ordQty);                                             

           modelIns.put("I_SALE_AMT"         ,   saleAmt);                                             
           modelIns.put("I_SALE_VAT"         ,   saleVat);                                             
           modelIns.put("I_DLV_MSG1"         ,   dlvMsg);                                             
           modelIns.put("I_SALES_CUST_NM"    ,   salesCustNm);                                             
           modelIns.put("I_ZIP"            	 ,   zip);                                             

           modelIns.put("I_ADDR"             ,   addr);                                             
           modelIns.put("I_PHONE_1"          ,   phone);                                             
           modelIns.put("I_CUST_CD"          ,   custCd);                                             
           modelIns.put("I_UOM_CD"           ,   uomCd);                                             
           
           modelIns.put("I_LC_ID"			,(String)model.get("LC_ID"));  
           modelIns.put("I_USER_NO"			,(String)model.get("USER_NO")); 
           modelIns.put("I_WORK_IP"			,(String)model.get("WORK_IP")); 
           
          modelIns = (Map<String, Object>)dao.syncIF(modelIns);
          ServiceUtil.isValidReturnCode("WMSIF810", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
          //m.put("O_CUR", modelIns.get("O_CUR"));   
           
           m.put("errCnt", 0);
           m.put("MSG", MessageResolver.getMessage("save.success"));
           
       } catch(BizException be) {
           m.put("errCnt", 1);
           m.put("MSG", be.getMessage() );
           
       } catch(Exception e){
           throw e;
       }
       return m;
   }  
		
	//E06
	
}
