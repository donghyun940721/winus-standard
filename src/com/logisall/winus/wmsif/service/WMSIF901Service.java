package com.logisall.winus.wmsif.service;

import java.sql.SQLException;
import java.util.Map;

public interface WMSIF901Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
    
    public Object insertMobisInbAsn(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    public Object insertMobisOutbOrder(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    public Object insertMobisOutbOrderConfirm(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    public Object insertMobisModuleOutbOrderResults(Map<String, Object> sessionModel, Map<String, Object> reqMap) throws Exception;
    public Map<String, Object> runMobisProcedure(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    public Map<String, Object> saveInboundIf(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    public Map<String, Object> deleteOutboundIf(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    public Map<String, Object> deleteOutboundIf2(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
    public Map<String, Object> deleteOutboundOrderIf(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
	
}
