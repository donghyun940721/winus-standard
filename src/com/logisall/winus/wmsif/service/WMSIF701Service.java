package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF701Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> list2(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveListOrderJava(Map<String, Object> model) throws Exception;	
	public Map<String, Object> sapbp(Map<String, Object> model) throws Exception;    
	public Map<String, Object> crudSap(Map<String, Object> model) throws Exception;    
	public Map<String, Object> playautoListDelete(Map<String, Object> model) throws Exception;
	public Map<String, Object> getOpenMallApiAuthMaster(Map<String, Object> model) throws Exception;
	public Map<String, Object> deleteOrder(Map<String, Object> model) throws Exception;
}
