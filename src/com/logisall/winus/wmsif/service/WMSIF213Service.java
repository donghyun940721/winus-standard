package com.logisall.winus.wmsif.service;

import java.util.List;
import java.util.Map;

public interface WMSIF213Service {
	 public Map<String, Object> saveCsv2(Map<String, Object> model, List list) throws Exception;
	 
	 public Map<String, Object> InbounSaveCsv(Map<String, Object> model, List list) throws Exception;
	 
	 public Map<String, Object> OutboundSaveCsv(Map<String, Object> model, List list) throws Exception;

	 public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
	 
	 public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
	 
	 public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
}
