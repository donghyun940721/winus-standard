package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF202Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
    public Map<String, Object> deviceInvalidView(Map<String, Object> model) throws Exception;
}
