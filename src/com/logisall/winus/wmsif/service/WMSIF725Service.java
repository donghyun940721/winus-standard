package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF725Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;    
    public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception;  
    public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
   // public Map<String, Object> wmsOrderDelete(Map<String, Object> model) throws Exception;    
}
