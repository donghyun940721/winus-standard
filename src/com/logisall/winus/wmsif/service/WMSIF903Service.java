package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF903Service {

  Map<String, Object> listE01(Map<String, Object> model) throws Exception;
  Map<String, Object> syncOutOrder(Map<String, Object> model) throws Exception;
  Map<String, Object> listE02(Map<String, Object> model) throws Exception;

}
