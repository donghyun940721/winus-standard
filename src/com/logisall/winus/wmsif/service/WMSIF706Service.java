package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF706Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> syncItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> getOpenMallInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> itemInfo(Map<String, Object> model) throws Exception;
}
