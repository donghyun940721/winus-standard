package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF601Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
	public Map<String, Object> parcelList(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveListOrderJavaVX(Map<String, Object> model) throws Exception;	
	public Map<String, Object> sapbp(Map<String, Object> model) throws Exception;    
	public Map<String, Object> crudSap(Map<String, Object> model) throws Exception;    
	public Map<String, Object> sapListDelete(Map<String, Object> model) throws Exception;
	public Map<String, Object> sabangnetListDelete(Map<String, Object> model) throws Exception;
	public Map<String, Object> sabangnetResetInvoice(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs_V2(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs_PARCEL(Map<String, Object> model) throws Exception;
	public Map<String, Object> getOpenMallApiAuthMaster(Map<String, Object> model) throws Exception;
}
