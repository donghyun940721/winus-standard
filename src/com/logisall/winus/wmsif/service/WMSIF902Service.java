package com.logisall.winus.wmsif.service;

import java.util.Map;

import com.logisall.winus.frm.exception.BizException;

public interface WMSIF902Service {

    Map listE01(Map<String, Object> model) throws Exception;

    Map<String, Object> saveCommonCodesByQuery(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

    Map<String, Object> saveZsCusts(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

    Map<String, Object> saveProducts(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

    Map<String, Object> saveInboundOrders(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

//    Map<String, Object> saveOutboundOrders(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

    Map selectOutOrderAsnList(Map<String, Object> model) throws Exception;

    Map selectInboundList(Map<String, Object> model);
    
    Map<String,Object> selectInResultList(Map<String, Object> model) throws Exception;

    Map<String, Object> selectCommonCodeList(Map<String, Object> model);

    Map<String, Object> saveCommonCodesByCheckedList(Map<String, Object> model, Map<String, Object> reqMap)
	    throws Exception;

    Map<String, Object> selectZsCustList(Map<String, Object> model);
    
    Map<String, Object> selectWarehouseList(Map<String, Object> model);

    Map<String, Object> selectProducts(Map<String, Object> model);

    Map<String, Object> saveCheckedProducts(Map<String, Object> model, Map<String, Object> reqMap);

    Map<String, Object> saveInOrders(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

	Map<String, Object> saveWarehouses(Map<String, Object> model, Map<String, Object> reqMap);

	Map<String, Object> saveOutOrders(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
	
	Map<String, Object> makeInResult(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
	
	Map<String, Object> sendAPI(Map<String, Object> model) throws Exception;

	Map<String, Object> selectWinusItemList(Map<String, Object> model, Map<String, Object> reqMap);

	Map<String, Object> saveItemRegionDiv(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;

	Map<String, Object> selectDayProductDivList(Map<String, Object> model);
	
}
