package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF001Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> crudOpenMallMaster(Map<String, Object> model) throws Exception;    
}
