package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF604Service {
	public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE3Summary(Map<String, Object> model) throws Exception;
	public Map<String, Object> dasResultCoop(Map<String, Object> model) throws Exception;
	public Map<String, Object> createDasResultData(Map<String, Object> model) throws Exception;
}
