package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF708Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> syncItem(Map<String, Object> model) throws Exception;
}
