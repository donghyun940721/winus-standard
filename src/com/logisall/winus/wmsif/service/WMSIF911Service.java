package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF911Service {

	Map<String, Object> selectIfOrders(Map<String, Object> model);

	Map<String, Object> sendWeverseOrderToWmsom010(Map<String, Object> model, Map<String, Object> reqMap);

}
