package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF709Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception;    
    public Map<String, Object> deleteE1(Map<String, Object> model) throws Exception;
	Map<String, Object> saveE1_OM(Map<String, Object> model) throws Exception;
}

