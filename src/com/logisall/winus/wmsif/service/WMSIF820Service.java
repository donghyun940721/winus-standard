package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF820Service {
	public Map<String, Object> listE01(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE02(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE03(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE04(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE05(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE06(Map<String, Object> model) throws Exception;
	public Map<String, Object> listBottomE06(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE05Summary(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE06Summary(Map<String, Object> model) throws Exception;
	public Map<String, Object> syncIF(Map<String, Object> model) throws Exception;
	public Map<String, Object> dasIfSend(Map<String, Object> model) throws Exception;
	public Map<String, Object> createInvoiceData(Map<String, Object> model) throws Exception;
	public Map<String, Object> deleteInvoiceResult(Map<String, Object> model) throws Exception;
	public Map<String, Object> dasDelete(Map<String, Object> model) throws Exception;
}
