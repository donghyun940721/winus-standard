package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF902ResultService {

	Map selectOutOrderResultList(Map<String, Object> model) throws Exception;

	Map<String, Object> makeOutResult(Map<String, Object> model, Map<String, Object> reqMap) throws Exception;
}
