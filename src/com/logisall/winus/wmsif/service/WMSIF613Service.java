package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF613Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveJasaOrderListVX(Map<String, Object> model) throws Exception;
    public Map<String, Object> jasamallDelete(Map<String, Object> model) throws Exception;
}
