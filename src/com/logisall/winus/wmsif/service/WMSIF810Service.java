package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF810Service {
	public Map<String, Object> listE01(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE02(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE03(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE04(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE05(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE06(Map<String, Object> model) throws Exception;
	public Map<String, Object> syncIF(Map<String, Object> model) throws Exception;
}
