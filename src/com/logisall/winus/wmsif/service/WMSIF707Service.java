package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF707Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> syncOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> getTransCorpInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteE1(Map<String, Object> model) throws Exception;
}
