package com.logisall.winus.wmswcs.service;

import java.util.Map;

public interface WMSWC002Service {
	Map<String, Object> orderClassificationsList(Map<String, Object> model);
	Map<String, Object> orderStatusChangeList(Map<String, Object> model);
	Map<String, Object> selectOrdList(Map<String, Object> model);
	Map<String, Object> selectOrdListE2(Map<String, Object> model);
	Map<String, Object> selectOrdListE3(Map<String, Object> model);
	Map<String, Object> orderShipmentList(Map<String, Object> model);
	Map<String, Object> orderShipmentChangeList(Map<String, Object> model);
	Map<String, Object> getOliveSrtCode(Map<String, Object> model);
 	Map<String, Object> printOzInvoice(Map<String, Object> model, Map<String, Object> reqMap);
}
