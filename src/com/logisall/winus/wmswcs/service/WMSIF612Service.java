package com.logisall.winus.wmswcs.service;

import java.util.Map;

public interface WMSIF612Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
}
