package com.logisall.winus.wmswcs.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.HttpUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.wmswcs.service.WMSWC002Service;

@Service
@SuppressWarnings("unchecked")
public class WMSWC002ServiceImpl implements WMSWC002Service {
	private final Log log = LogFactory.getLog(this.getClass());
	
    @Autowired
    private WMSWC002Dao dao;
    
    @Value("${winus.api.auth}")
    private String winusApiAuth;

    @Value("${winus.api.url}")
    private String winusApiUrl;
    
	@Override
	public Map<String, Object> orderClassificationsList(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	List<Map<String, Object>> orderClassificationsList = dao.orderClassificationsList(model);
        	resultMap.put("LIST", orderClassificationsList);
	    } catch(Exception e) {
	    	log.error(e);
	    	resultMap.put("errCnt", 1);
	    	resultMap.put("MSG", e.getMessage());
	    }
        return resultMap;
	}

	@Override
	public Map<String, Object> orderStatusChangeList(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	List<Map<String, Object>> orderStatusChangeList = dao.orderStatusChangeList(model);
        	resultMap.put("LIST", orderStatusChangeList);
	    } catch(Exception e) {
	    	log.error(e);
	    	resultMap.put("errCnt", 1);
	    	resultMap.put("MSG", e.getMessage());
	    }
        return resultMap;
	}
	
	@Override
	public Map<String, Object> selectOrdList(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	model = putOrgOrdIdListInModel(model);
        	List<Map<String, Object>> ordList = dao.selectOrdList(model);
            resultMap.put("LIST", ordList);
        } catch(Exception e) {
        	log.error(e);
        	resultMap.put("errCnt", 1);
        	resultMap.put("MSG", e.getMessage());
        }
        return resultMap;
	}
	
	@Override
	public Map<String, Object> selectOrdListE2(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	model = putOrgOrdIdListInModel(model);
            List<Map<String, Object>> ordList = dao.selectOrdListE2(model);
            resultMap.put("LIST", ordList);
	    } catch(Exception e) {
	    	log.error(e);
	    	resultMap.put("errCnt", 1);
	    	resultMap.put("MSG", e.getMessage());
	    }
        return resultMap;
	}
	
	@Override
	public Map<String, Object> selectOrdListE3(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	model = putOrgOrdIdListInModel(model);
	        List<Map<String, Object>> ordList = dao.selectOrdListE3(model);
	        resultMap.put("LIST", ordList);
	    } catch(Exception e) {
	    	log.error(e);
	    	resultMap.put("errCnt", 1);
	    	resultMap.put("MSG", e.getMessage());
	    }
        return resultMap;
	}
	
	@Override
	public Map<String, Object> orderShipmentList(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	List<Map<String, Object>> orserShipmentList = dao.orderShipmentList(model);
        	resultMap.put("LIST", orserShipmentList);
	    } catch(Exception e) {
	    	log.error(e);
	    	resultMap.put("errCnt", 1);
	    	resultMap.put("MSG", e.getMessage());
	    }
        return resultMap;
	}
	
	@Override
	public Map<String, Object> orderShipmentChangeList(Map<String, Object> model) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
        	List<Map<String, Object>> orderShipmentChangeList = dao.orderShipmentChangeList(model);
        	resultMap.put("LIST", orderShipmentChangeList);
	    } catch(Exception e) {
	    	log.error(e);
	    	resultMap.put("errCnt", 1);
	    	resultMap.put("MSG", e.getMessage());
	    }
        return resultMap;
	}

	@Override
	public Map<String, Object> getOliveSrtCode(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> srtCdList = dao.getOliveSrtCode();
	        resultMap.put("SRT_CD_LIST", srtCdList);
	    } catch(Exception e) {
	    	log.error(e);
	    	resultMap.put("errCnt", 1);
	    	resultMap.put("MSG", e.getMessage());
	    }
        return resultMap;
	}

	@Override
	public Map<String, Object> printOzInvoice(Map<String, Object> model, Map<String, Object> reqMap) {
		String srtCd = (String) reqMap.get("srtCd");
		reqMap.put("lc_id", model.get("SS_SVC_NO"));
		
		Map<String, Object> responseMap = new HashMap<>();
		try {
			if ("10".equals(srtCd)) { // EMS
				Map<String, Object> emsReqMap = getEmsParamForPrintInvoice(reqMap);
				responseMap = sendOzPrintInvoice(emsReqMap);
			} else { // LXP
				responseMap = sendOzPrintInvoice((Map<String, Object>)reqMap.get("request"));	
			}
		} catch(Exception e) {
			log.error(e);
			responseMap.put("errCnt", 1);
			responseMap.put("MSG", e.getMessage());
		}
		return responseMap; 
	}
	
	private Map<String, Object> getEmsParamForPrintInvoice(Map<String, Object> reqMap) {
	    Map<String, Object> sqlResultMap = dao.getEmsOzInvoiceParam(reqMap);
	    return createEmsOzReqMap(reqMap, sqlResultMap);
	}

	private Map<String, Object> createEmsOzReqMap(Map<String, Object> reqMap, Map<String, Object> sqlResultMap) {
	    Map<String, Object> emsOzReqMap = new HashMap<>();
	    Map<String, Object> requset = (Map<String, Object>) reqMap.get("request");
	    
	    emsOzReqMap.put("exportFileName", requset.get("exportFileName"));
	    emsOzReqMap.put("ozrFilePath", requset.get("ozrFilePath"));
	    emsOzReqMap.put("uploadOrdId", requset.get("uploadOrdId"));
	    
	    Map<String, Object> params = new HashMap<>();
	    params.put("vrSrchOrdId", requset.get("uploadOrdId"));
	    try {
	    	params.put("dAddress", OliveAes256.decrpyt((String) sqlResultMap.get("D_ADDRESS")));
		    params.put("dTel", OliveAes256.decrpyt((String) sqlResultMap.get("D_TEL")));
		    params.put("dPost", OliveAes256.decrpyt((String) sqlResultMap.get("D_POST")));
		    params.put("jAddress", OliveAes256.decrpyt((String) sqlResultMap.get("J_ADDRESS")));
		    params.put("jTel", OliveAes256.decrpyt((String) sqlResultMap.get("J_TEL")));
		    params.put("jPost", OliveAes256.decrpyt((String) sqlResultMap.get("J_POST")));
	    } catch (Exception e) {
	    	log.error(e);
			throw new RuntimeException(e);
	    }
	    emsOzReqMap.put("params", params);
	    
	    return emsOzReqMap;
	}
	
	private Map<String, Object> sendOzPrintInvoice(Map<String, Object> reqMap) {
		String apiUrl = "/api/oz/create-file.do";
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> responseMap = new HashMap<>();
		try {
			String jsonBody = objectMapper.writeValueAsString(reqMap);
			String responseJson = HttpUtil.sendJsonHttpRequest(winusApiUrl + apiUrl, HttpMethod.POST, jsonBody, winusApiAuth);
			responseMap = objectMapper.readValue(responseJson, new TypeReference<HashMap<String, Object>>() {});
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		
		return responseMap;
	}
	
	private Map<String, Object> putOrgOrdIdListInModel(Map<String, Object> model) {
		String vrSrchOrgOrdIdList = model.get("vrSrchOrgOrdIdList").toString();
		if (!vrSrchOrgOrdIdList.isEmpty()) {
			String[] orgOrdIdList = vrSrchOrgOrdIdList.trim().split("\r");
			model.put("orgOrdIdList", orgOrdIdList);
		}
		return model;
	}
}

