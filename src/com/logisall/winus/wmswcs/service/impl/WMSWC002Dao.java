package com.logisall.winus.wmswcs.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSWC002Dao")
public class WMSWC002Dao extends SqlMapAbstractDAO{
	// WINUSUSR
	@Autowired
	private SqlMapClient sqlMapClient;

	// WINUSUSR_IF
	@Autowired
	private SqlMapClient sqlMapClientWinusIf;

    public List<Map<String, Object>> selectOrdList(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClient.queryForList("wmswc002.selectOrdList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }
    
	public List<Map<String, Object>> selectOrdListE2(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClient.queryForList("wmswc002.selectOrdListE2", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }
    
    public List<Map<String, Object>> selectOrdListE3(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClient.queryForList("wmswc002.selectOrdListE3", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }

    public List<Map<String, Object>> orderClassificationsList(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmswc002.orderClassificationsList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }
    
    public List<Map<String, Object>> orderStatusChangeList(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmswc002.orderStatusChangeList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }
    
    public List<Map<String, Object>> orderShipmentList(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmswc002.orderShipmentList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }
    
    public List<Map<String, Object>> orderShipmentChangeList(Map<String, Object> model) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmswc002.orderShipmentChangeList", model);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }
    
    
    public List<Map<String, Object>> getOliveSrtCode() {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClient.queryForList("wmswc002.getOliveSrtCode");
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
		return resultList;
    }
    
    public Map<String, Object> getEmsOzInvoiceParam(Map<String, Object> model) {
    	Map<String, Object> resultMap = new HashMap<>();
    	try {
    		resultMap = (Map<String, Object>) sqlMapClient.queryForObject("wmswc002.getEmsOzInvoiceParam", model);
    	} catch (SQLException e) {
    		throw new RuntimeSQLException(e);
    	}
    	
    	return resultMap;
    }
}


