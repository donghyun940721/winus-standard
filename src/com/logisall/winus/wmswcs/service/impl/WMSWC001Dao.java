package com.logisall.winus.wmswcs.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
public class WMSWC001Dao extends SqlMapAbstractDAO {
	private Log log = LogFactory.getLog(this.getClass());
	private static final int BATCH_SIZE = 1000;

	// WINUSUSR_IF
	@Autowired
	private SqlMapClient sqlMapClientWinusIf;

	public List selectOlsWcsNotLinkList(Map<String, Object> reqMap) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		try {
			resultList = sqlMapClientWinusIf.queryForList("wmswc001.selectOlsWcsNotLinkList", reqMap);
		} catch (SQLException e) {
			e.printStackTrace();
			log.error(e);
		} catch (Exception e) {
			log.error(e);
		}

		return resultList;
	}

	public List selectWmsOrderList(Map<String, Object> reqMap) {
		return executeQueryForList("wmswc001.selectWmsOrderList", reqMap);
	}

	public void insertOlsWcsIf(Object order) {
		try {
			sqlMapClientWinusIf.insert("wmswc001.insertOttogiApi", order);
		} catch (SQLException e) {
			log.error(e);
		}
	}

	public void updateOrderSendComplete(Object order) {
		executeUpdate("wmswc001.updateOrderSendComplete", order);
	}

//    public void insertOlsWcsDASIfList(List wmsOrderList) {
//	int totalEntities = wmsOrderList.size();
//	int startIndex = 0;
//	while (startIndex < totalEntities) {
//	    int endIndex = Math.min(startIndex + BATCH_SIZE, totalEntities);
//	    List batchEntities = wmsOrderList.subList(startIndex, endIndex);
//
//	    try {
//		executeBatch(batchEntities);
//	    } catch (SQLException e) {
//		log.error(e);
//	    } // BATCH_SIZE 만큼의 데이터를 처리
//
//	    startIndex += BATCH_SIZE;
//	}
////	List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
//
//    }

	public void insertOlsWcsDPSIfList(List wmsOrderList) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

	}

	public Integer selectWcsOrdDegree(Map<String, Object> model) {
		Integer result = 0;
		try {
			result = (Integer) sqlMapClientWinusIf.queryForObject("wmswc001.selectWcsOrdDegree", model);
		} catch (SQLException e) {
			log.error(e);
		}
		return result;
	}

	public Map selectWcsUUIDAndLinkYn(Map<String, Object> reqMap) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			resultMap = (Map<String, Object>) sqlMapClientWinusIf.queryForObject("wmswc001.selectWcsUUIDAndLinkYn",
					reqMap);
		} catch (SQLException e) {
			log.error(e);
		}

		return resultMap;
	}
	public void updateOttogiWcsOrderSendComplete(Map<String, Object> reqMap) {
		try {
			sqlMapClientWinusIf.insert("wmswc001.updateOttogiWcsOrderSendComplete", reqMap);
		} catch (SQLException e) {
			log.error(e);
		}
	}

	public void deleteOttogiWcsOrder(Map<String, Object> reqMap) {
		try {
			sqlMapClientWinusIf.insert("wmswc001.deleteOttogiWcsOrder", reqMap);
		} catch (SQLException e) {
			log.error(e);
		}
	}

	public void rollbackOrderIftYn(Map<String, Object> reqMap) {
		executeUpdate("wmswc001.rollbackOrderIftYn", reqMap);
	}

	public List<Map<String, Object>> selectItemWeightList(Map<String, Object> reqMap) {
		return executeQueryForList("wmswc001.selectItemWeightList", reqMap);
	}

}
