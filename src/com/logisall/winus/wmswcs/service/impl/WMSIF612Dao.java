package com.logisall.winus.wmswcs.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF612Dao")
public class WMSIF612Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif612.list", model);
    }
    
    /**
     * Method ID  : listE2
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif612.listE2", model);
    }
    
    /**
     * Method ID  : listE3
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif612.listE3", model);
    }
    
    /**
     * Method ID  : listE4
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet listE4(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif612.listE4", model);
    }
  /**
     * Method ID  : listE5
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet listE5(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif612.listE5", model);
    }
  /**
     * Method ID  : listE6
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet listE6(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif612.listE6", model);
    }
    
    /**
	 * Method ID : insert 
	 * Method 설명 : 인터페이스 이력 등록
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
    public Object insert(Map<String, Object> model) {
		return executeInsert("wmsif612.insert", model);
	}
    
}


