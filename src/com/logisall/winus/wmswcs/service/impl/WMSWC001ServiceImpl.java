package com.logisall.winus.wmswcs.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.HttpUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmswcs.service.WMSWC001Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
@SuppressWarnings("unchecked")
public class WMSWC001ServiceImpl implements WMSWC001Service {
	private final Log log = LogFactory.getLog(this.getClass());
	private final String WCS_CANCEL_URL = "http://121.183.166.166:14035/AWCS.svc/IF_LI_04";
	@Autowired
	private WMSWC001Dao wmswc001Dao;

	/**
	 * 오뚜기 설비 IF 테이블 전송
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Map sendWmsOrderToWcsIf(Map<String, Object> model, Map<String, Object> reqMap) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		setSessionParam(model, reqMap);
		try {
			List wmsOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
			checkDuplicateOrders(reqMap);
			checkOrderItemWeight(wmsOrderList);
			checkWcsLink(reqMap);
			updateOrderSendCompleteList(wmsOrderList);
			insertOlsWcsIfList(wmsOrderList);
			resultMap.put("errCnt", 0);
			resultMap.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (BizException e) {
			resultMap.put("errCnt", 1);
			resultMap.put("MSG", e.getMessage());
		} catch (Exception e) {
			throw e;
		}
		return resultMap;
	}

	/*
	 * 상품의 중량정보가 등록되어 있는지 체크한다.
	 */
	private void checkOrderItemWeight(List wmsOrderList) throws BizException{
		//주문별 중복상품 코드 제거.
		Set<String> items = new HashSet<String>();
		for (Object order : wmsOrderList) {
			Map<String, Object> orderMap = (Map<String, Object>) order;
			items.add((String) orderMap.get("RITEM_ID"));
		}
		List<String> itemList = new ArrayList<String>();
		for (String item : items) {
			itemList.add(item);
		}
		List<List<String>> orderItemChunks = CommonUtil.splitList(itemList, 1000);
		//아이템 1000개씩 끊어서 중량정보 등록되어 있는지 체크
        for (List<String> orderItemChunk : orderItemChunks) {
        	Map<String, Object> itemMap = new HashMap<String, Object>();
        	itemMap.put("itemList", orderItemChunk);
        	List<Map<String, Object>> itemWeightList = wmswc001Dao.selectItemWeightList(itemMap);
        	for (Map<String, Object> item : itemWeightList) {
        		String itemName = (String) item.get("ITEM_NM");
        		String itemCode = (String) item.get("ITEM_CODE");
        		BigDecimal itemWeight = (BigDecimal) item.get("ITEM_WGT");
				if(itemWeight.compareTo(BigDecimal.ZERO) == 0) {
					throw new BizException(itemName+"("+itemCode+") 의 중량정보가 등록되지 않았습니다.");
				}
			}
        }
	}

	public void insertOlsWcsIfList(List wmsOrderList) {
		for (Object order : wmsOrderList) {
			wmswc001Dao.insertOlsWcsIf(order);
		}
	}

	public void updateOrderSendCompleteList(List wmsOrderList) {
		for (Object order : wmsOrderList) {
			wmswc001Dao.updateOrderSendComplete(order);
		}
	}

	private void checkWcsLink(Map<String, Object> reqMap) throws BizException {
		String wcsOrdDegree = reqMap.get("vrOrdDegreeE1").toString();
		String machineType = reqMap.get("vrMachineType2").toString();
		String groupDate = reqMap.get("workDate").toString();
		String equipmentTypeString = machineType.equals("06") ? "das" : "dps"; //06 DAS, 05 DPS
		String equipmentType = equipmentTypeString.equals("das") ? "1" : "2"; //06 DAS, 05 DPS
		String autolabelerType = reqMap.get("autolabelerType").toString();

		Map wcsUUIDAndLinkYn = wmswc001Dao.selectWcsUUIDAndLinkYn(reqMap);
		String uuid = equipmentTypeString+"-"+UUID.randomUUID().toString();
		String linkYn = "N";
		if (wcsUUIDAndLinkYn != null) {
			uuid = (String) wcsUUIDAndLinkYn.get("UUID");
			linkYn = (String) wcsUUIDAndLinkYn.get("LINK_YN");
		}

		if ("Y".equals(linkYn)) {
			throw new BizException("WCS 주문 전송된 작업입니다.");
		}
		List wmsOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
		for (Object wmsOrderObj : wmsOrderList) {
			Map<String, Object> wmsOrder = (Map<String, Object>) wmsOrderObj;

			wmsOrder.put("UUID", uuid);
			wmsOrder.put("ORD_DETAIL_NO", wmsOrder.get("ORD_SEQ"));
			wmsOrder.put("ORD_QTY", wmsOrder.get("OUT_ORD_QTY"));
			wmsOrder.put("INVOICE_GROUP_NUMBER", wcsOrdDegree);
			wmsOrder.put("EQUIPMENT_TYPE", equipmentType);
			wmsOrder.put("AUTOLABEL_TYPE", autolabelerType);
			wmsOrder.put("GROUP_DATE", groupDate);
			wmsOrder.put("SS_USER_NO", reqMap.get("SS_USER_NO"));
		}
	}

	/*
	 * 전송된 주문인지 체크한다.
	 */
	private void checkDuplicateOrders(Map<String, Object> reqMap) throws BizException {
		Set<String> olsWcsOrderIds = new HashSet<String>();
		List wmsOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
		List olsWcsNotLinkList = wmswc001Dao.selectOlsWcsNotLinkList(reqMap);

		for (Object olsWcsOrderObj : olsWcsNotLinkList) {
			Map<String, Object> wcsIfOrder = (Map<String, Object>) olsWcsOrderObj;
			String orderId = wcsIfOrder.get("ORD_ID").toString();
			olsWcsOrderIds.add(orderId);
		}
		for (Object wmsOrderObj : wmsOrderList) {
			Map<String, Object> wmsOrder = (Map<String, Object>) wmsOrderObj;
			String orderId = wmsOrder.get("ORD_ID").toString();
			if (olsWcsOrderIds.contains(orderId)) {
				throw new BizException(orderId + "는 이미 전송완료된 주문번호 입니다.");
			}
		}
	}

	private void setSessionParam(Map<String, Object> model, Map<String, Object> reqMap) {
		String lc_id = model.get("SS_SVC_NO").toString();
		reqMap.put("SS_SVC_NO", lc_id);
		String user_no = model.get("SS_USER_NO").toString();
		reqMap.put("SS_USER_NO", user_no);
	}

	@Override
	public Map selectWmsOrderList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			List olsWcsNotLinkList = wmswc001Dao.selectWmsOrderList(model);
			resultMap.put("errCnt", "0");
			resultMap.put("LIST", olsWcsNotLinkList);
		} catch (Exception e) {
			log.error(e);
			resultMap.put("errCnt", "1");
			resultMap.put("MSG", MessageResolver.getMessage("save.error"));
		} finally {
			return resultMap;
		}
	}

	@Override
	public Map<String, Object> selectWcsOrdDegree(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			Integer maxOrdDegree = wmswc001Dao.selectWcsOrdDegree(model);
			resultMap.put("errCnt", "0");
			resultMap.put("ORD_DEGREE", maxOrdDegree);
		} catch (Exception e) {
			log.error(e);
			resultMap.put("errCnt", "1");
			resultMap.put("MSG", MessageResolver.getMessage("save.error"));
		} finally {
			return resultMap;
		}
	}

	@Override
	public Map<String, Object> selectOlsWcsNotLinkList(Map<String, Object> model) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			List olsWcsNotLinkList = wmswc001Dao.selectOlsWcsNotLinkList(model);
			resultMap.put("errCnt", "0");
			resultMap.put("LIST", olsWcsNotLinkList);
		} catch (Exception e) {
			log.error(e);
			resultMap.put("errCnt", "1");
			resultMap.put("MSG", MessageResolver.getMessage("save.error"));
		} finally {
			return resultMap;
		}
	}
   
	/*
	 * wcs 전송취소
	 * wcs에서 작업시작하면 취소 불가
	 * */
	@Override
	public Map<String, Object> cancelWcsIf(Map<String, Object> model, Map<String, Object> reqMap) throws IOException {
		String uuid = (String) reqMap.get("UUID");
		Map<String, Object> httpReqMap = new HashMap<String, Object>();
		httpReqMap.put("invoiceGroupId", uuid);
		Gson gson = new Gson();
		String response = HttpUtil.sendJsonHttpRequest(
				WCS_CANCEL_URL, 
				HttpMethod.POST, 
				gson.toJson(httpReqMap)
				);
		HashMap<String, Object> responseMap = gson.fromJson(response, new TypeToken<HashMap<String, Object>>(){}.getType());
		String responsResult = (String)responseMap.get("result");
		if("FAIL".equalsIgnoreCase(responsResult)) {
			return responseMap;
		}
		
		setSessionParam(model, reqMap);
		reqMap.put("LINK_YN", "N");
		wmswc001Dao.updateOttogiWcsOrderSendComplete(reqMap);
		return responseMap;
	}
	
	/*
	 * WCS로 바로전송(개발진행중)
	 * 중복 uuid로 전송 불가
	 * */
	@Override
	public Map<String, Object> createWcsIf(Map<String, Object> model, Map<String, Object> reqMap) throws IOException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		setSessionParam(model, reqMap);
		try {
			List<Map<String, Object>> wmsOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
			checkDuplicateOrders(reqMap);
			checkOrderItemWeight(wmsOrderList);
//			checkWcsLink(reqMap);
			
//			OlsWcsDto olsWcsDto = createWcsDto(reqMap);
//			updateOrderSendCompleteList(wmsOrderList);
//			insertOlsWcsIfList(wmsOrderList);
			resultMap.put("errCnt", 0);
			resultMap.put("MSG", MessageResolver.getMessage("save.success"));
		} catch (BizException e) {
			resultMap.put("errCnt", 1);
			resultMap.put("MSG", e.getMessage());
		} catch (Exception e) {
			throw e;
		}
		return resultMap;
		
//		String uuid = (String) reqMap.get("UUID");
//		Map<String, Object> httpReqMap = new HashMap<String, Object>();
//		httpReqMap.put("invoiceGroupId", uuid);
//		Gson gson = new Gson();
//		String response = HttpUtil.sendHttpRequest(
//				WCS_CANCEL_URL, 
//				HttpMethod.POST, 
//				gson.toJson(httpReqMap)
//				);
//		HashMap<String, Object> responseMap = gson.fromJson(response, new TypeToken<HashMap<String, Object>>(){}.getType());
//		String responsResult = (String)responseMap.get("result");
//		if("FAIL".equalsIgnoreCase(responsResult)) {
//			return responseMap;
//		}
//		
//		setSessionParam(model, reqMap);
//		reqMap.put("LINK_YN", "N");
//		wmswc001Dao.updateOttogiWcsOrderSendComplete(reqMap);
//		return responseMap;
	}
	
	/*
	private OlsWcsDto createWcsDto(Map<String, Object> reqMap) {
		OlsWcsDto wcsDto = new OlsWcsDto();
		
		List<Map<String, Object>> wmsOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
		String wcsOrdDegree = reqMap.get("vrOrdDegreeE1").toString();
		String machineType = reqMap.get("vrMachineType2").toString();
		String groupDate = reqMap.get("workDate").toString();
		String equipmentTypeString = machineType.equals("06") ? "das" : "dps"; //06 DAS, 05 DPS
		String equipmentType = equipmentTypeString.equals("das") ? "1" : "2"; //06 DAS, 05 DPS
		String autolabelerType = reqMap.get("autolabelerType").toString();
		String uuid = equipmentTypeString+"-"+UUID.randomUUID().toString();
		
		wcsDto.setInvoiceGroupId(uuid);
		wcsDto.setInvoiceGroupDate(groupDate);
		wcsDto.setInvoiceGroupNumber(wcsOrdDegree);
		wcsDto.setEquipmentType(equipmentType);
		List<Invoice> invoices = new ArrayList<OlsWcsDto.Invoice>();
		for (Map<String, Object> order : wmsOrderList) {
			String invoiceNumber = (String) order.get("INVC_NO");
			Invoice invoice = new Invoice();
			invoice.setInvoiceNumber(invoiceNumber);
			
		}
		return wcsDto;
	}
	*/

	/**
	 * 오뚜기 설비 IF 차수 삭제
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Map deleteIfOrder(Map<String, Object> model, Map<String, Object> reqMap) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			String linkYn = (String) reqMap.get("LINK_YN");
			if("Y".equalsIgnoreCase(linkYn)) {
				throw new BizException("전송완료 건은 삭제 불가합니다.");
			}
			List<Map<String, Object>> wmsOrderList = (List<Map<String, Object>>) reqMap.get("orderList");
			List<List<Map<String, Object>>> orderChunks = CommonUtil.splitList(wmsOrderList, 1000);
			
			//차수 삭제
			setSessionParam(model, reqMap);
			wmswc001Dao.deleteOttogiWcsOrder(reqMap);
			
			//주문번호 1000개씩 끊어서 wmsop010 ITF_COM_YN 컬럼 롤백
	        for (List<Map<String, Object>> orderChunk : orderChunks) {
	        	Map<String, Object> updateOrder = new HashMap<String, Object>();
	        	updateOrder.put("LC_ID", model.get("SS_SVC_NO"));
	        	updateOrder.put("CUST_ID", reqMap.get("vrSrchCustId"));
	        	setSessionParam(model, updateOrder);
	        	updateOrder.put("orderList", orderChunk);
	            log.info("Chunk size: " + orderChunk.size());
	            wmswc001Dao.rollbackOrderIftYn(updateOrder);
	        }
	        resultMap.put("errCnt", "0");
			resultMap.put("MSG", MessageResolver.getMessage("save.success"));
		}catch(BizException e) {
			resultMap.put("errCnt", "1");
			resultMap.put("MSG", e.getMessage());
		}catch (Exception e) {
			throw new IllegalStateException(e);
		}
		
		return resultMap;
	}
	
}
