package com.logisall.winus.wmswcs.service;

import java.io.IOException;
import java.util.Map;

public interface WMSWC001Service {

    Map sendWmsOrderToWcsIf(Map<String, Object> model, Map<String, Object> reqMap);
    Map selectWmsOrderList(Map<String, Object> model);
    Map<String, Object> selectWcsOrdDegree(Map<String, Object> model);
    Map<String, Object> selectOlsWcsNotLinkList(Map<String, Object> model);
    Map<String, Object> cancelWcsIf(Map<String, Object> model, Map<String, Object> reqMap) throws IOException;
	Map deleteIfOrder(Map<String, Object> model, Map<String, Object> reqMap);
	Map<String, Object> createWcsIf(Map<String, Object> model, Map<String, Object> reqMap) throws IOException;
}
