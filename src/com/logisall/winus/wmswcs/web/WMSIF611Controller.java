package com.logisall.winus.wmswcs.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmswcs.service.WMSIF611Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF611Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF611Service")
    private WMSIF611Service service;
    
    /**
     * Method ID	: WMSIF611
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF611.action")
    public ModelAndView WMSIF611(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmswcs/WMSIF611");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF611/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listE2
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF611/listE2.action")
    public ModelAndView list2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE2(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /**
     * Method ID	: listE3
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF611/listE3.action")
    public ModelAndView list3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE3(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    /**
     * Method ID	: listE4
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF611/listE4.action")
    public ModelAndView list4(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE4(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }

     /**
     * Method ID	: listE5
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF611/listE5.action")
    public ModelAndView list5(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE5(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }

     /**
     * Method ID	: listE6
     * Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSIF611/listE6.action")
    public ModelAndView list6(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE6(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /*-
	 * Method ID    : crossDomainHttp
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF611/crossDomainHttp.action")
	public ModelAndView crossDomainHttp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.crossDomainHttps(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
