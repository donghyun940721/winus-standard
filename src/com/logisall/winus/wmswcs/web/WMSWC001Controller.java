package com.logisall.winus.wmswcs.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.HTTP;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.impl.WMSIF904Service;
import com.logisall.winus.wmswcs.service.WMSWC001Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

/**
 * OLS WCS 전송관리
 */
@Controller
public class WMSWC001Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	@Autowired
	private WMSIF904Service service;
	@Autowired
	private WMSIF000Service WMSIF000Service;
	@Autowired
	private WMSWC001Service wmswc001Service;

	/**
	 * OLS WCS 전송관리 화면진입
	 */
	@RequestMapping("/WINUS/WMSWC001.action")
	public ModelAndView WMSWC001(Map<String, Object> model) {
		Map<String, Object> codeMap = new HashMap<String, Object>();
		codeMap.put("MALL_LIST", service.selectMallList(model));
		return new ModelAndView("winus/wmswcs/WMSWC001", codeMap);
	}

	@RequestMapping("/WMSWC001/selectWmsOrderList.action")
	public ResponseEntity<Map<String, Object>> selectWmsOrderList(Map<String, Object> model) {
		Map<String, Object> resultMap = wmswc001Service.selectWmsOrderList(model);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}

	@RequestMapping("/WMSWC001/selectOlsWcsNotLinkList.action")
	public ResponseEntity<Map<String, Object>> selectOlsWcsNotLinkList(Map<String, Object> model) {
		Map<String, Object> resultMap = wmswc001Service.selectOlsWcsNotLinkList(model);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}

	@RequestMapping("/WMSWC001/selectWcsOrdDegree.action")
	public ResponseEntity<Map<String, Object>> selectWcsOrdDegree(Map<String, Object> model) {
		Map<String, Object> resultMap = wmswc001Service.selectWcsOrdDegree(model);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}

	/**
	 * 오뚜기 WCS I/F 전용 테이블로 전송
	 */
	@RequestMapping("/WMSWC001/sendWmsOrderToWcsIf.action")
	@ResponseBody
	public ModelAndView sendWmsOrderToWcs(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = wmswc001Service.sendWmsOrderToWcsIf(model, reqMap);

		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSWC001/sendWcsIf.action")
	@ResponseBody
	public ModelAndView sendWcsIf(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			
			Map<String, Object> eaiArguments = new HashMap<String, Object>();
			Map<String, Object> jsonObject = new HashMap<String, Object>();

			String uuid = reqMap.get("UUID").toString();
			String equipmentType = reqMap.get("EQUIPMENT_TYPE").toString();
			String url = "OTTOGIWCS/createData?";
			jsonObject.put("uuid", uuid);
			jsonObject.put("equipmentType", equipmentType);

			String queryString = CommonUtil.mapToQueryString(jsonObject);

			eaiArguments.put("data", "");
			eaiArguments.put("cMethod", HttpMethod.GET.toString());
			eaiArguments.put("cUrl", url + queryString);
			eaiArguments.put("hostUrl", request.getServerName());
			m = WMSIF000Service.crossDomainHttpWs5100New(eaiArguments);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSWC001/cancelWcsIf.action")
	@ResponseBody
	public ModelAndView cancelWcsIf(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = wmswc001Service.cancelWcsIf(model, reqMap);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	@RequestMapping("/WMSWC001/deleteIfOrder.action")
	@ResponseBody
	public ModelAndView deleteIfOrder(Map<String, Object> model, @RequestBody Map<String, Object> reqMap,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = wmswc001Service.deleteIfOrder(model, reqMap);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save approve :", e);
			}
			m.put("errCnt", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
