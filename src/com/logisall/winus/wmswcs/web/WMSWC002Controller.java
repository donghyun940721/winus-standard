package com.logisall.winus.wmswcs.web;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.api.oywcs.dto.OrdrListWrapper;
import com.logisall.api.oywcs.service.OliveWcsApiService;
import com.logisall.winus.wmswcs.service.WMSWC002Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;


@Controller
public class WMSWC002Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private WMSWC002Service WMSWC002Service;
	
	@Autowired
	private OliveWcsApiService OliveWcsApiService;

	@RequestMapping("/WINUS/WMSWC002.action")
	public ModelAndView WMSWC002(Map<String, Object> model) {
		return new ModelAndView("winus/wmswcs/WMSWC002");
	}

	/**
	 * WCS전송관리(올리브영) > 분류코드정보
	 */
	@RequestMapping("/WMSWC002/selectOrdList.action")
	public ResponseEntity<Map<String, Object>> selectOrdList(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.selectOrdList(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}

	@RequestMapping("/WMSWC002/orderClassificationsList.action")
	public ResponseEntity<Map<String, Object>> orderClassificationsList(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.orderClassificationsList(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC002/sendOrderClassifications.action")
	public ModelAndView sendOrdersClassifications(Map<String, Object> model, @RequestBody Map<String, Object> reqMap) {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = new HashMap<String, Object>();
	    try {
	    	OliveWcsApiService.sendOrderClassifications(reqMap);
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to save approve :", e);
	        }
	        m.put("errCnt", 1);
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	    }
	    mav.addAllObjects(m);
	    return mav;
	}

	/**
	 * WCS전송관리(올리브영) > 주문작업상태변경
	 */
	@RequestMapping("/WMSWC002/selectOrdListE3.action")
	public ResponseEntity<Map<String, Object>> selectOrdListE3(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.selectOrdListE3(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC002/orderStatusChangeList.action")
	public ResponseEntity<Map<String, Object>> orderStatusChangeList(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.orderStatusChangeList(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC002/sendOrderStatusChange.action")
	public ModelAndView sendOrderStatusChange(Map<String, Object> model, @RequestBody Map<String, Object> reqMap) {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = new HashMap<String, Object>();
	    try {
	    	OliveWcsApiService.sendOrderStatusChange(reqMap);
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to save approve :", e);
	        }
	        m.put("errCnt", 1);
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	    }
	    mav.addAllObjects(m);
	    return mav;
	}
	
	/**
	 * WCS전송관리(올리브영) > 운송장출력정보
	 */
	@RequestMapping("/WMSWC002/selectOrdListE2.action")
	public ResponseEntity<Map<String, Object>> selectOrdListE2(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.selectOrdListE2(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC002/orderShipmentList.action")
	public ResponseEntity<Map<String, Object>> orderShipmentList(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.orderShipmentList(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC002/orderShipmentChangeList.action")
	public ResponseEntity<Map<String, Object>> orderShipmentChangeList(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.orderShipmentChangeList(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC002/getSrtCdList.action")
	public ResponseEntity<Map<String, Object>> getOliveSrtCdList(Map<String, Object> model) {
		Map<String, Object> resultMap = WMSWC002Service.getOliveSrtCode(model);
	  	return new ResponseEntity<Map<String,Object>>(resultMap, HttpStatus.OK);
	}
	
	@RequestMapping("/WMSWC002/sendOrderShipment.action")
	public ModelAndView sendOrderShipment(Map<String, Object> model, @RequestBody OrdrListWrapper reqMap) {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = new HashMap<String, Object>();
	    try {
	    	OliveWcsApiService.sendOrderShipments(reqMap);
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to save approve :", e);
	        }
	        m.put("errCnt", 1);
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	    }
	    mav.addAllObjects(m);
	    return mav;
	}
	
	@RequestMapping("/WMSWC002/sendOrderShipmentChange.action")
	public ModelAndView sendOrderShipmentChange(Map<String, Object> model, @RequestBody OrdrListWrapper reqMap) {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = new HashMap<String, Object>();
	    try { 
	    	OliveWcsApiService.orderShipmentChange(reqMap);
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to save approve :", e);
	        }
	        m.put("errCnt", 1);
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	    }
	    mav.addAllObjects(m);
	    return mav;
	}
	
	@RequestMapping("/WMSWC002/printOzInvoice.action")
	public ResponseEntity<Map<String, Object>> printOzInvoice(Map<String, Object> model, @RequestBody Map<String, Object> reqMap) {
		Map<String, Object> responseMap = WMSWC002Service.printOzInvoice(model, reqMap);
	  	return new ResponseEntity<Map<String,Object>>(responseMap, HttpStatus.OK);
	}
}
