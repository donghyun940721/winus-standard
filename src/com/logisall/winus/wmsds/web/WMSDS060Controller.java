package com.logisall.winus.wmsds.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSSP010Service;
import com.logisall.winus.wmsst.service.WMSST230Service;
import com.logisall.winus.wmsds.service.WMSDS060Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSDS060Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSDS060Service")
	private WMSDS060Service service;
	
	/*-
	 * Method ID : 
	 * Method 설명 : 배차통계
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSDS060.action")
	public ModelAndView wmsds060(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsds/WMSDS060");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : STG710E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS060/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : STG710E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS060/detail.action")
	public ModelAndView detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : STG710E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS060/detailItemlist.action")
	public ModelAndView detailItemlist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detailItemlist(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : STG710E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS060/familyList.action")
	public ModelAndView familyList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.familyList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : STG710E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS060/familyDetailList.action")
	public ModelAndView familyDetailList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.familyDetailList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 임가공 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST060/listExcel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getList().size() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("NO"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("날짜"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("S/N"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("고객명"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량판정(회수내역)"), "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("접수 및 판정 사유"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("플앳홈 판정"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("부품"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("종류"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("파트넘버"), "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("자재넘버"), "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("리퍼완료여부"), "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업결과"), "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("비고"), "14", "14", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_STAT_EXCEL"          , "S"},
                                    {"WORK_DT"           , "S"},
                                    {"WORK_TM"           , "S"},
                                    {"RITEM_CD"          , "S"},
                                    {"RITEM_NM"          , "S"},
                                    
                                    {"WORK_QTY"            , "N"},
                                    {"UOM_NM"            , "S"},
                                    {"WH_CD"            , "S"},
                                    {"WH_NM"            , "S"},
                                    {"LOC_CD"            , "S"},
                                    
                                    {"CUST_CD"            , "S"},
                                    {"CUST_NM"            , "S"},
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("임가공");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}