package com.logisall.winus.wmsds.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsct.service.impl.WMSSP010Dao;
import com.logisall.winus.wmsst.service.impl.WMSST230Dao;
import com.logisall.winus.wmsds.service.WMSDS050Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
	
@Service("WMSDS050Service")
public class WMSDS050ServiceImpl implements WMSDS050Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSDS050Dao")
    private WMSDS050Dao dao;
    
    @Resource(name = "WMSSP010Dao")
    private WMSSP010Dao wmssp011Dao;
    
    @Resource(name = "WMSST230Dao")
    private WMSST230Dao wmsst230Dao;
    
    /**
     * Method ID : list
     * Method 설명 : RDC 배송통계 리스트 
     * 작성자 : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<List<Map<String, Object>>> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
        try {
        	for(int i = 1; i <= 10; i++){
        		Map<String, Object> subModel = new HashMap<String, Object>();
        		String LC_ID = (String)model.get("LC_NAME_VAL" + String.format("%02d", i));
        		if("NDF".equals(LC_ID)){
        			continue;
        		}
        		builder.append(String.format("'%s'", LC_ID));
        		if(i < 10){
        			builder.append(",");
        		}
        		subModel.put("LC_ID", LC_ID);
        		subModel.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom"));
        		subModel.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo"));
        		subModel.put("vrSrchDtType", model.get("vrSrchDtType"));
        		subModel.put("dtLength", model.get("dtLength"));
        		subModel.put("vrSrchCustId", model.get("vrSrchCustId"));
        		subModel.put("vrSrchCustCd", model.get("vrSrchCustCd"));
        		list.add(dao.list(subModel));
        		
        	}
        	Map<String, Object> sumModel = new HashMap<String, Object>();
        	sumModel.put("LC_ID", builder.toString());
        	sumModel.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom"));
        	sumModel.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo"));
        	sumModel.put("vrSrchDtType", model.get("vrSrchDtType"));
        	sumModel.put("dtLength", model.get("dtLength"));
        	sumModel.put("vrSrchCustId", model.get("vrSrchCustId"));
        	sumModel.put("vrSrchCustCd", model.get("vrSrchCustCd"));
        	list.add(dao.listSum(sumModel));
        	
        	map.put("list", list);
        	
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	/**
	 * Method ID : driverList
	 * Method 설명 : RDC 기사통계 리스트 
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> driverList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<List<Map<String, Object>>> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		try {
			
			for(int i = 1; i <= 10; i++){
				Map<String, Object> subModel = new HashMap<String, Object>();
				String LC_ID = (String)model.get("LC_NAME_VAL" + String.format("%02d", i));
				if("NDF".equals(LC_ID)){
					continue;
				}
				builder.append(String.format("'%s'", LC_ID));
				if(i < 10){
					builder.append(",");
				}
				subModel.put("LC_ID", LC_ID);
				subModel.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom"));
				subModel.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo"));
				subModel.put("vrSrchDtType", model.get("vrSrchDtType"));
				subModel.put("dtLength", model.get("dtLength"));
				subModel.put("vrSrchCustId", model.get("vrSrchCustId"));
				subModel.put("vrSrchCustCd", model.get("vrSrchCustCd"));
				subModel.put("vrSrchDrvId", model.get("vrSrchDrvId"));
				list.add(dao.driverList(subModel));
				
			}
			model.put("LC_ID", builder.toString());
			list.add(dao.driverListSum(model));
			map.put("list", list);
			map.put("driverNmList", dao.driverNmList(model));
			map.put("dateList", dao.dateList(model));
			
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}
	
	/**
	 * Method ID : averageList
	 * Method 설명 : 평균 배송일 통계
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> averageList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<List<Map<String, Object>>> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		List<Map<String, Object>> rowList = new ArrayList<>();
		try {
			
			for(int i = 1; i <= 10; i++){
				Map<String, Object> subModel = new HashMap<String, Object>();
				String LC_ID = (String)model.get("LC_NAME_VAL" + String.format("%02d", i));
				String CUST_NM = (String)model.get("CUST_NAME_VAL" + String.format("%02d", i));
				if("NDF".equals(LC_ID)){
					continue;
				}
				builder.append(String.format("'%s'", LC_ID));
				if(i < 10){
					builder.append(",");
				}
				subModel.put("LC_ID", LC_ID);
				subModel.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom"));
				subModel.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo"));
				subModel.put("vrSrchDtType", model.get("vrSrchDtType"));
				subModel.put("dtLength", model.get("dtLength"));
				subModel.put("vrSrchCustId", model.get("vrSrchCustId"));
				subModel.put("vrSrchCustCd", model.get("vrSrchCustCd"));
				subModel.put("vrSrchDrvId", model.get("vrSrchDrvId"));
				list.add(dao.averageList(subModel));
				
			}
			model.put("LC_ID", builder.toString());
			list.add(dao.averageListSum(model));
			map.put("list", list);
			map.put("rowList", dao.custList(model));
			map.put("dateList", dao.dateList(model));
			
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}
	
	
	/**
	 * Method ID : selectBox
	 * Method 설명 : RDC selectBox list 
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectBox(Map<String, Object> model){
		
		Map<String, Object> map = new HashMap<String, Object>();
		try{
	    	model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	        map.put("LCLIST", wmsst230Dao.selectLc(model));
	    } catch (Exception e) {
	        log.error(e.toString());
	        map.put("MSG", MessageResolver.getMessage("list.error"));
	    }
	    return map;
	}	
	/**
	 * Method ID : selectBox
	 * Method 설명 : RDC selectBox list 
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectDriverList(Map<String, Object> model){
		
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			map.put("DRIVERS", dao.selectDriverList(model));
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}	
}