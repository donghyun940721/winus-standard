package com.logisall.winus.wmsds.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDS020Dao")
public class WMSDS020Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID  : selectLc
	 * Method 설명  : selectLc
	 * 작성자             : chsong
	 * @param model
	 * @return
	 */
	public Object selectLc(Map<String, Object> model){
		return executeQueryForList("wmsds020.selectLc", model);
	}
	
	/**
	 * Method ID : list 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsds020.list", model);
	}
}
