package com.logisall.winus.wmsds.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDS260Dao")
public class WMSDS260Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID  : myLcList
     * Method 설명  : 
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object myLcList(Map<String, Object> model){
     	return executeQueryForList("wmsds260.myLcList", model);
     }
     
	/**
     * Method ID : list
     * Method 설명 : 입출고미완료현황
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsds260.list", model);
    }
}
