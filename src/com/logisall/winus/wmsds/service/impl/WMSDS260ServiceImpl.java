package com.logisall.winus.wmsds.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsds.service.WMSDS260Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSDS260Service")
public class WMSDS260ServiceImpl extends AbstractServiceImpl implements WMSDS260Service {
    
    @Resource(name = "WMSDS260Dao")
    private WMSDS260Dao dao;
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("MYLC", dao.myLcList(model));
        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 입출고미완료현황 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
             
            List<String> myLcArr = new ArrayList();
            String[] spMyLcListVal = model.get("myLcListVal").toString().split(",");
            for (String keyword : spMyLcListVal ){
            	myLcArr.add(keyword);
            }
            model.put("myLcArr", myLcArr);
            
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : listExcel
     * Method 설명 : 입출고미완료현황 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();           
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        List<String> myLcArr = new ArrayList();
        String[] spMyLcListVal = model.get("myLcListVal").toString().split(",");
        for (String keyword : spMyLcListVal ){
        	myLcArr.add(keyword);
        }
        model.put("myLcArr", myLcArr);
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
}
