package com.logisall.winus.wmsds.service;

import java.util.Map;

public interface WMSDS050Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> driverList(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectDriverList(Map<String, Object> model) throws Exception;
	public Map<String, Object> averageList(Map<String, Object> model) throws Exception;
}
