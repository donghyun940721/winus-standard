package com.logisall.ws.interfaces.common;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ISODateTypeAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {

	private final DateFormat dateFormat;

	public ISODateTypeAdapter() {
		
		dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX") {
			
			@Override
			public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
				StringBuffer rfcFormat = super.format(date, toAppendTo, pos);
				return rfcFormat.insert(rfcFormat.length() - 2, ":");
			}

			@Override
			public Date parse(String text, ParsePosition pos) {
				if (text.length() > 3) {
					text = text.substring(0, text.length() - 3) + text.substring(text.length() - 2);
				}
				return super.parse(text, pos);
			}
		};

	}

	@Override
	public synchronized JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
		return new JsonPrimitive(dateFormat.format(date));
	}

	@Override
	public synchronized Date deserialize(JsonElement jsonElement, Type type,
			JsonDeserializationContext jsonDeserializationContext) {
		try {
			return dateFormat.parse(jsonElement.getAsString());
		} catch (ParseException e) {
			throw new JsonParseException(e);
		}
	}
}
