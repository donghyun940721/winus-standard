package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RtiSerialInfoVOKRDate implements Serializable {

	private static final long serialVersionUID = -3844727177593095135L;

	private String epcCd;	

	private List<ItemSerialInfoVOKRDate> itemSerialList;

	public String getEpcCd() {
		return epcCd;
	}

	public void setEpcCd(String epcCd) {
		this.epcCd = epcCd;
	}
	
	public List<ItemSerialInfoVOKRDate> getItemSerialList() {
		return itemSerialList;
	}

	public void setItemSerialList(List<ItemSerialInfoVOKRDate> itemSerialList) {
		this.itemSerialList = itemSerialList;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("RtiSerialInfoVOKRDate [epcCd=");
		builder.append(epcCd);
		builder.append(", itemSerialList=");
		builder.append(itemSerialList);
		builder.append("]");
		return builder.toString();
	}
	
}
