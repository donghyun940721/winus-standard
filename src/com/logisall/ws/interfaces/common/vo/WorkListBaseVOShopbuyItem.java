package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVOShopbuyItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String  accumulationRate;
	private String  accumulationUseYn;
	private String  addOptionImageYn;
	private String  additionalDiscountYn;
	private String  adminNo;
	private String  applyStatusType;
	private String  brandNo;
	private String  cartOffEndYmdt;
	private String  cartOffPeriodYn;
	private String  cartOffStartYmdt;
	private String  cartUseYn;
	private String  categoryNo;
	private String  certificationJson;
	private String  certificationType;
	private String  classType;
	private String  commissionRate;
	private String  commissionRateType;
	private String  comparingPriceSite;
	private String  content;
	private String  contentFooter;
	private String  contentHeader;
	private String  couponYn;
	private String  deleteYn;
	private String  deliveryCombinationYn;
	private String  deliveryCustomerInfo;
	private String  deliveryInternationalYn;
	private String  deliveryTemplateNo;
	private String  deliveryYn;
	private String  displayBrandNo;
	private String  dutyInfo;
	private String  eanCode;
	private String  expirationYmdt;
	private String  extraJson;
	private String  frontDisplayYn;
	private String  groupType;
	private String  hsCode;
	private String  immediateDiscountApplyPrice;
	private String  immediateDiscountEndYmdt;
	private String  immediateDiscountPeriodYn;
	private String  immediateDiscountStartYmdt;
	private String  immediateDiscountUnitType;
	private String  immediateDiscountValue;
	private String  isOptionUsed;
	private String  itemYn;
	private String  keyword;
	private String  mallNo;
	private String  mallProductNo;
	private String  manufactureYmdt;
	private String  mappingKey;
	private String  masterYn;
	private String  maxBuyDays;
	private String  maxBuyPeriodCnt;
	private String  maxBuyPersonCnt;
	private String  maxBuyTimeCnt;
	private String  memberGradeDisplayInfo;
	private String  memberGroupDisplayInfo;
	private String  minBuyCnt;
	private String  minorPurchaseYn;
	private String  nonmemberPurchaseYn;
	private String  parentMallProductNo;
	private String  partnerChargeAmt;
	private String  partnerName;
	private String  partnerNo;
	private String  paymentMeans;
	private String  paymentMeansControlYn;
	private String  placeOrigin;
	private String  placeOriginSeq;
	private String  placeOriginsYn;
	private String  platformDisplayMobileWebYn;
	private String  platformDisplayMobileYn;
	private String  platformDisplayPcYn;
	private String  platformDisplayYn;
	private String  pointRate;
	private String  productManagementCd;
	private String  productName;
	private String  productNameEn;
	private String  productNo;
	private String  productType;
	private String  promotionText;
	private String  promotionTextEndYmdt;
	private String  promotionTextStartYmdt;
	private String  promotionTextYn;
	private String  promotionYn;
	private String  refundableYn;
	private String  registerAdminNo;
	private String  registerSiteType;
	private String  registerYmdt;
	private String  saleEndYmd;
	private String  saleEndYmdt;
	private String  saleMethodType;
	private String  salePeriodType;
	private String  salePrice;
	private String  saleStartYmd;
	private String  saleStartYmdt;
	private String  saleStatusType;
	private String  searchengineDisplayYn;
	private String  shippingAreaPartnerNo;
	private String  shippingAreaType;
	private String  syncWmsYn;
	private String  tempSave;
	private String  unitName;
	private String  unitNameType;
	private String  unitPrice;
	private String  updateAdminNo;
	private String  updateYmdt;
	private String  urlDirectDisplayYn;
	private String  valueAddedTaxType;

	private List<DeliveryGroupsVO> deliveryGroups;
	
	public String getAccumulationRate(){
	 return accumulationRate; 
	} 
	   
	public void setAccumulationRate(String accumulationRate) 
	{
	 this.accumulationRate=accumulationRate;
	} 
	  
	public String getAccumulationUseYn(){
	 return accumulationUseYn; 
	} 
	   
	public void setAccumulationUseYn(String accumulationUseYn) 
	{
	 this.accumulationUseYn=accumulationUseYn;
	} 
	  
	public String getAddOptionImageYn(){
	 return addOptionImageYn; 
	} 
	   
	public void setAddOptionImageYn(String addOptionImageYn) 
	{
	 this.addOptionImageYn=addOptionImageYn;
	} 
	  
	public String getAdditionalDiscountYn(){
	 return additionalDiscountYn; 
	} 
	   
	public void setAdditionalDiscountYn(String additionalDiscountYn) 
	{
	 this.additionalDiscountYn=additionalDiscountYn;
	} 
	  
	public String getAdminNo(){
	 return adminNo; 
	} 
	   
	public void setAdminNo(String adminNo) 
	{
	 this.adminNo=adminNo;
	} 
	  
	public String getApplyStatusType(){
	 return applyStatusType; 
	} 
	   
	public void setApplyStatusType(String applyStatusType) 
	{
	 this.applyStatusType=applyStatusType;
	} 
	  
	public String getBrandNo(){
	 return brandNo; 
	} 
	   
	public void setBrandNo(String brandNo) 
	{
	 this.brandNo=brandNo;
	} 
	  
	public String getCartOffEndYmdt(){
	 return cartOffEndYmdt; 
	} 
	   
	public void setCartOffEndYmdt(String cartOffEndYmdt) 
	{
	 this.cartOffEndYmdt=cartOffEndYmdt;
	} 
	  
	public String getCartOffPeriodYn(){
	 return cartOffPeriodYn; 
	} 
	   
	public void setCartOffPeriodYn(String cartOffPeriodYn) 
	{
	 this.cartOffPeriodYn=cartOffPeriodYn;
	} 
	  
	public String getCartOffStartYmdt(){
	 return cartOffStartYmdt; 
	} 
	   
	public void setCartOffStartYmdt(String cartOffStartYmdt) 
	{
	 this.cartOffStartYmdt=cartOffStartYmdt;
	} 
	  
	public String getCartUseYn(){
	 return cartUseYn; 
	} 
	   
	public void setCartUseYn(String cartUseYn) 
	{
	 this.cartUseYn=cartUseYn;
	} 
	  
	public String getCategoryNo(){
	 return categoryNo; 
	} 
	   
	public void setCategoryNo(String categoryNo) 
	{
	 this.categoryNo=categoryNo;
	} 
	  
	public String getCertificationJson(){
	 return certificationJson; 
	} 
	   
	public void setCertificationJson(String certificationJson) 
	{
	 this.certificationJson=certificationJson;
	} 
	  
	public String getCertificationType(){
	 return certificationType; 
	} 
	   
	public void setCertificationType(String certificationType) 
	{
	 this.certificationType=certificationType;
	} 
	  
	public String getClassType(){
	 return classType; 
	} 
	   
	public void setClassType(String classType) 
	{
	 this.classType=classType;
	} 
	  
	public String getCommissionRate(){
	 return commissionRate; 
	} 
	   
	public void setCommissionRate(String commissionRate) 
	{
	 this.commissionRate=commissionRate;
	} 
	  
	public String getCommissionRateType(){
	 return commissionRateType; 
	} 
	   
	public void setCommissionRateType(String commissionRateType) 
	{
	 this.commissionRateType=commissionRateType;
	} 
	  
	public String getComparingPriceSite(){
	 return comparingPriceSite; 
	} 
	   
	public void setComparingPriceSite(String comparingPriceSite) 
	{
	 this.comparingPriceSite=comparingPriceSite;
	} 
	  
	public String getContent(){
	 return content; 
	} 
	   
	public void setContent(String content) 
	{
	 this.content=content;
	} 
	  
	public String getContentFooter(){
	 return contentFooter; 
	} 
	   
	public void setContentFooter(String contentFooter) 
	{
	 this.contentFooter=contentFooter;
	} 
	  
	public String getContentHeader(){
	 return contentHeader; 
	} 
	   
	public void setContentHeader(String contentHeader) 
	{
	 this.contentHeader=contentHeader;
	} 
	  
	public String getCouponYn(){
	 return couponYn; 
	} 
	   
	public void setCouponYn(String couponYn) 
	{
	 this.couponYn=couponYn;
	} 
	  
	public String getDeleteYn(){
	 return deleteYn; 
	} 
	   
	public void setDeleteYn(String deleteYn) 
	{
	 this.deleteYn=deleteYn;
	} 
	  
	public String getDeliveryCombinationYn(){
	 return deliveryCombinationYn; 
	} 
	   
	public void setDeliveryCombinationYn(String deliveryCombinationYn) 
	{
	 this.deliveryCombinationYn=deliveryCombinationYn;
	} 
	  
	public String getDeliveryCustomerInfo(){
	 return deliveryCustomerInfo; 
	} 
	   
	public void setDeliveryCustomerInfo(String deliveryCustomerInfo) 
	{
	 this.deliveryCustomerInfo=deliveryCustomerInfo;
	} 
	  
	public String getDeliveryInternationalYn(){
	 return deliveryInternationalYn; 
	} 
	   
	public void setDeliveryInternationalYn(String deliveryInternationalYn) 
	{
	 this.deliveryInternationalYn=deliveryInternationalYn;
	} 
	  
	public String getDeliveryTemplateNo(){
	 return deliveryTemplateNo; 
	} 
	   
	public void setDeliveryTemplateNo(String deliveryTemplateNo) 
	{
	 this.deliveryTemplateNo=deliveryTemplateNo;
	} 
	  
	public String getDeliveryYn(){
	 return deliveryYn; 
	} 
	   
	public void setDeliveryYn(String deliveryYn) 
	{
	 this.deliveryYn=deliveryYn;
	} 
	  
	public String getDisplayBrandNo(){
	 return displayBrandNo; 
	} 
	   
	public void setDisplayBrandNo(String displayBrandNo) 
	{
	 this.displayBrandNo=displayBrandNo;
	} 
	  
	public String getDutyInfo(){
	 return dutyInfo; 
	} 
	   
	public void setDutyInfo(String dutyInfo) 
	{
	 this.dutyInfo=dutyInfo;
	} 
	  
	public String getEanCode(){
	 return eanCode; 
	} 
	   
	public void setEanCode(String eanCode) 
	{
	 this.eanCode=eanCode;
	} 
	  
	public String getExpirationYmdt(){
	 return expirationYmdt; 
	} 
	   
	public void setExpirationYmdt(String expirationYmdt) 
	{
	 this.expirationYmdt=expirationYmdt;
	} 
	  
	public String getExtraJson(){
	 return extraJson; 
	} 
	   
	public void setExtraJson(String extraJson) 
	{
	 this.extraJson=extraJson;
	} 
	  
	public String getFrontDisplayYn(){
	 return frontDisplayYn; 
	} 
	   
	public void setFrontDisplayYn(String frontDisplayYn) 
	{
	 this.frontDisplayYn=frontDisplayYn;
	} 
	  
	public String getGroupType(){
	 return groupType; 
	} 
	   
	public void setGroupType(String groupType) 
	{
	 this.groupType=groupType;
	} 
	  
	public String getHsCode(){
	 return hsCode; 
	} 
	   
	public void setHsCode(String hsCode) 
	{
	 this.hsCode=hsCode;
	} 
	  
	public String getImmediateDiscountApplyPrice(){
	 return immediateDiscountApplyPrice; 
	} 
	   
	public void setImmediateDiscountApplyPrice(String immediateDiscountApplyPrice) 
	{
	 this.immediateDiscountApplyPrice=immediateDiscountApplyPrice;
	} 
	  
	public String getImmediateDiscountEndYmdt(){
	 return immediateDiscountEndYmdt; 
	} 
	   
	public void setImmediateDiscountEndYmdt(String immediateDiscountEndYmdt) 
	{
	 this.immediateDiscountEndYmdt=immediateDiscountEndYmdt;
	} 
	  
	public String getImmediateDiscountPeriodYn(){
	 return immediateDiscountPeriodYn; 
	} 
	   
	public void setImmediateDiscountPeriodYn(String immediateDiscountPeriodYn) 
	{
	 this.immediateDiscountPeriodYn=immediateDiscountPeriodYn;
	} 
	  
	public String getImmediateDiscountStartYmdt(){
	 return immediateDiscountStartYmdt; 
	} 
	   
	public void setImmediateDiscountStartYmdt(String immediateDiscountStartYmdt) 
	{
	 this.immediateDiscountStartYmdt=immediateDiscountStartYmdt;
	} 
	  
	public String getImmediateDiscountUnitType(){
	 return immediateDiscountUnitType; 
	} 
	   
	public void setImmediateDiscountUnitType(String immediateDiscountUnitType) 
	{
	 this.immediateDiscountUnitType=immediateDiscountUnitType;
	} 
	  
	public String getImmediateDiscountValue(){
	 return immediateDiscountValue; 
	} 
	   
	public void setImmediateDiscountValue(String immediateDiscountValue) 
	{
	 this.immediateDiscountValue=immediateDiscountValue;
	} 
	  
	public String getIsOptionUsed(){
	 return isOptionUsed; 
	} 
	   
	public void setIsOptionUsed(String isOptionUsed) 
	{
	 this.isOptionUsed=isOptionUsed;
	} 
	  
	public String getItemYn(){
	 return itemYn; 
	} 
	   
	public void setItemYn(String itemYn) 
	{
	 this.itemYn=itemYn;
	} 
	  
	public String getKeyword(){
	 return keyword; 
	} 
	   
	public void setKeyword(String keyword) 
	{
	 this.keyword=keyword;
	} 
	  
	public String getMallNo(){
	 return mallNo; 
	} 
	   
	public void setMallNo(String mallNo) 
	{
	 this.mallNo=mallNo;
	} 
	  
	public String getMallProductNo(){
	 return mallProductNo; 
	} 
	   
	public void setMallProductNo(String mallProductNo) 
	{
	 this.mallProductNo=mallProductNo;
	} 
	  
	public String getManufactureYmdt(){
	 return manufactureYmdt; 
	} 
	   
	public void setManufactureYmdt(String manufactureYmdt) 
	{
	 this.manufactureYmdt=manufactureYmdt;
	} 
	  
	public String getMappingKey(){
	 return mappingKey; 
	} 
	   
	public void setMappingKey(String mappingKey) 
	{
	 this.mappingKey=mappingKey;
	} 
	  
	public String getMasterYn(){
	 return masterYn; 
	} 
	   
	public void setMasterYn(String masterYn) 
	{
	 this.masterYn=masterYn;
	} 
	  
	public String getMaxBuyDays(){
	 return maxBuyDays; 
	} 
	   
	public void setMaxBuyDays(String maxBuyDays) 
	{
	 this.maxBuyDays=maxBuyDays;
	} 
	  
	public String getMaxBuyPeriodCnt(){
	 return maxBuyPeriodCnt; 
	} 
	   
	public void setMaxBuyPeriodCnt(String maxBuyPeriodCnt) 
	{
	 this.maxBuyPeriodCnt=maxBuyPeriodCnt;
	} 
	  
	public String getMaxBuyPersonCnt(){
	 return maxBuyPersonCnt; 
	} 
	   
	public void setMaxBuyPersonCnt(String maxBuyPersonCnt) 
	{
	 this.maxBuyPersonCnt=maxBuyPersonCnt;
	} 
	  
	public String getMaxBuyTimeCnt(){
	 return maxBuyTimeCnt; 
	} 
	   
	public void setMaxBuyTimeCnt(String maxBuyTimeCnt) 
	{
	 this.maxBuyTimeCnt=maxBuyTimeCnt;
	} 
	  
	public String getMemberGradeDisplayInfo(){
	 return memberGradeDisplayInfo; 
	} 
	   
	public void setMemberGradeDisplayInfo(String memberGradeDisplayInfo) 
	{
	 this.memberGradeDisplayInfo=memberGradeDisplayInfo;
	} 
	  
	public String getMemberGroupDisplayInfo(){
	 return memberGroupDisplayInfo; 
	} 
	   
	public void setMemberGroupDisplayInfo(String memberGroupDisplayInfo) 
	{
	 this.memberGroupDisplayInfo=memberGroupDisplayInfo;
	} 
	  
	public String getMinBuyCnt(){
	 return minBuyCnt; 
	} 
	   
	public void setMinBuyCnt(String minBuyCnt) 
	{
	 this.minBuyCnt=minBuyCnt;
	} 
	  
	public String getMinorPurchaseYn(){
	 return minorPurchaseYn; 
	} 
	   
	public void setMinorPurchaseYn(String minorPurchaseYn) 
	{
	 this.minorPurchaseYn=minorPurchaseYn;
	} 
	  
	public String getNonmemberPurchaseYn(){
	 return nonmemberPurchaseYn; 
	} 
	   
	public void setNonmemberPurchaseYn(String nonmemberPurchaseYn) 
	{
	 this.nonmemberPurchaseYn=nonmemberPurchaseYn;
	} 
	  
	public String getParentMallProductNo(){
	 return parentMallProductNo; 
	} 
	   
	public void setParentMallProductNo(String parentMallProductNo) 
	{
	 this.parentMallProductNo=parentMallProductNo;
	} 
	  
	public String getPartnerChargeAmt(){
	 return partnerChargeAmt; 
	} 
	   
	public void setPartnerChargeAmt(String partnerChargeAmt) 
	{
	 this.partnerChargeAmt=partnerChargeAmt;
	} 
	  
	public String getPartnerName(){
	 return partnerName; 
	} 
	   
	public void setPartnerName(String partnerName) 
	{
	 this.partnerName=partnerName;
	} 
	  
	public String getPartnerNo(){
	 return partnerNo; 
	} 
	   
	public void setPartnerNo(String partnerNo) 
	{
	 this.partnerNo=partnerNo;
	} 
	  
	public String getPaymentMeans(){
	 return paymentMeans; 
	} 
	   
	public void setPaymentMeans(String paymentMeans) 
	{
	 this.paymentMeans=paymentMeans;
	} 
	  
	public String getPaymentMeansControlYn(){
	 return paymentMeansControlYn; 
	} 
	   
	public void setPaymentMeansControlYn(String paymentMeansControlYn) 
	{
	 this.paymentMeansControlYn=paymentMeansControlYn;
	} 
	  
	public String getPlaceOrigin(){
	 return placeOrigin; 
	} 
	   
	public void setPlaceOrigin(String placeOrigin) 
	{
	 this.placeOrigin=placeOrigin;
	} 
	  
	public String getPlaceOriginSeq(){
	 return placeOriginSeq; 
	} 
	   
	public void setPlaceOriginSeq(String placeOriginSeq) 
	{
	 this.placeOriginSeq=placeOriginSeq;
	} 
	  
	public String getPlaceOriginsYn(){
	 return placeOriginsYn; 
	} 
	   
	public void setPlaceOriginsYn(String placeOriginsYn) 
	{
	 this.placeOriginsYn=placeOriginsYn;
	} 
	  
	public String getPlatformDisplayMobileWebYn(){
	 return platformDisplayMobileWebYn; 
	} 
	   
	public void setPlatformDisplayMobileWebYn(String platformDisplayMobileWebYn) 
	{
	 this.platformDisplayMobileWebYn=platformDisplayMobileWebYn;
	} 
	  
	public String getPlatformDisplayMobileYn(){
	 return platformDisplayMobileYn; 
	} 
	   
	public void setPlatformDisplayMobileYn(String platformDisplayMobileYn) 
	{
	 this.platformDisplayMobileYn=platformDisplayMobileYn;
	} 
	  
	public String getPlatformDisplayPcYn(){
	 return platformDisplayPcYn; 
	} 
	   
	public void setPlatformDisplayPcYn(String platformDisplayPcYn) 
	{
	 this.platformDisplayPcYn=platformDisplayPcYn;
	} 
	  
	public String getPlatformDisplayYn(){
	 return platformDisplayYn; 
	} 
	   
	public void setPlatformDisplayYn(String platformDisplayYn) 
	{
	 this.platformDisplayYn=platformDisplayYn;
	} 
	  
	public String getPointRate(){
	 return pointRate; 
	} 
	   
	public void setPointRate(String pointRate) 
	{
	 this.pointRate=pointRate;
	} 
	  
	public String getProductManagementCd(){
	 return productManagementCd; 
	} 
	   
	public void setProductManagementCd(String productManagementCd) 
	{
	 this.productManagementCd=productManagementCd;
	} 
	  
	public String getProductName(){
	 return productName; 
	} 
	   
	public void setProductName(String productName) 
	{
	 this.productName=productName;
	} 
	  
	public String getProductNameEn(){
	 return productNameEn; 
	} 
	   
	public void setProductNameEn(String productNameEn) 
	{
	 this.productNameEn=productNameEn;
	} 
	  
	public String getProductNo(){
	 return productNo; 
	} 
	   
	public void setProductNo(String productNo) 
	{
	 this.productNo=productNo;
	} 
	  
	public String getProductType(){
	 return productType; 
	} 
	   
	public void setProductType(String productType) 
	{
	 this.productType=productType;
	} 
	  
	public String getPromotionText(){
	 return promotionText; 
	} 
	   
	public void setPromotionText(String promotionText) 
	{
	 this.promotionText=promotionText;
	} 
	  
	public String getPromotionTextEndYmdt(){
	 return promotionTextEndYmdt; 
	} 
	   
	public void setPromotionTextEndYmdt(String promotionTextEndYmdt) 
	{
	 this.promotionTextEndYmdt=promotionTextEndYmdt;
	} 
	  
	public String getPromotionTextStartYmdt(){
	 return promotionTextStartYmdt; 
	} 
	   
	public void setPromotionTextStartYmdt(String promotionTextStartYmdt) 
	{
	 this.promotionTextStartYmdt=promotionTextStartYmdt;
	} 
	  
	public String getPromotionTextYn(){
	 return promotionTextYn; 
	} 
	   
	public void setPromotionTextYn(String promotionTextYn) 
	{
	 this.promotionTextYn=promotionTextYn;
	} 
	  
	public String getPromotionYn(){
	 return promotionYn; 
	} 
	   
	public void setPromotionYn(String promotionYn) 
	{
	 this.promotionYn=promotionYn;
	} 
	  
	public String getRefundableYn(){
	 return refundableYn; 
	} 
	   
	public void setRefundableYn(String refundableYn) 
	{
	 this.refundableYn=refundableYn;
	} 
	  
	public String getRegisterAdminNo(){
	 return registerAdminNo; 
	} 
	   
	public void setRegisterAdminNo(String registerAdminNo) 
	{
	 this.registerAdminNo=registerAdminNo;
	} 
	  
	public String getRegisterSiteType(){
	 return registerSiteType; 
	} 
	   
	public void setRegisterSiteType(String registerSiteType) 
	{
	 this.registerSiteType=registerSiteType;
	} 
	  
	public String getRegisterYmdt(){
	 return registerYmdt; 
	} 
	   
	public void setRegisterYmdt(String registerYmdt) 
	{
	 this.registerYmdt=registerYmdt;
	} 
	  
	public String getSaleEndYmd(){
	 return saleEndYmd; 
	} 
	   
	public void setSaleEndYmd(String saleEndYmd) 
	{
	 this.saleEndYmd=saleEndYmd;
	} 
	  
	public String getSaleEndYmdt(){
	 return saleEndYmdt; 
	} 
	   
	public void setSaleEndYmdt(String saleEndYmdt) 
	{
	 this.saleEndYmdt=saleEndYmdt;
	} 
	  
	public String getSaleMethodType(){
	 return saleMethodType; 
	} 
	   
	public void setSaleMethodType(String saleMethodType) 
	{
	 this.saleMethodType=saleMethodType;
	} 
	  
	public String getSalePeriodType(){
	 return salePeriodType; 
	} 
	   
	public void setSalePeriodType(String salePeriodType) 
	{
	 this.salePeriodType=salePeriodType;
	} 
	  
	public String getSalePrice(){
	 return salePrice; 
	} 
	   
	public void setSalePrice(String salePrice) 
	{
	 this.salePrice=salePrice;
	} 
	  
	public String getSaleStartYmd(){
	 return saleStartYmd; 
	} 
	   
	public void setSaleStartYmd(String saleStartYmd) 
	{
	 this.saleStartYmd=saleStartYmd;
	} 
	  
	public String getSaleStartYmdt(){
	 return saleStartYmdt; 
	} 
	   
	public void setSaleStartYmdt(String saleStartYmdt) 
	{
	 this.saleStartYmdt=saleStartYmdt;
	} 
	  
	public String getSaleStatusType(){
	 return saleStatusType; 
	} 
	   
	public void setSaleStatusType(String saleStatusType) 
	{
	 this.saleStatusType=saleStatusType;
	} 
	  
	public String getSearchengineDisplayYn(){
	 return searchengineDisplayYn; 
	} 
	   
	public void setSearchengineDisplayYn(String searchengineDisplayYn) 
	{
	 this.searchengineDisplayYn=searchengineDisplayYn;
	} 
	  
	public String getShippingAreaPartnerNo(){
	 return shippingAreaPartnerNo; 
	} 
	   
	public void setShippingAreaPartnerNo(String shippingAreaPartnerNo) 
	{
	 this.shippingAreaPartnerNo=shippingAreaPartnerNo;
	} 
	  
	public String getShippingAreaType(){
	 return shippingAreaType; 
	} 
	   
	public void setShippingAreaType(String shippingAreaType) 
	{
	 this.shippingAreaType=shippingAreaType;
	} 
	  
	public String getSyncWmsYn(){
	 return syncWmsYn; 
	} 
	   
	public void setSyncWmsYn(String syncWmsYn) 
	{
	 this.syncWmsYn=syncWmsYn;
	} 
	  
	public String getTempSave(){
	 return tempSave; 
	} 
	   
	public void setTempSave(String tempSave) 
	{
	 this.tempSave=tempSave;
	} 
	  
	public String getUnitName(){
	 return unitName; 
	} 
	   
	public void setUnitName(String unitName) 
	{
	 this.unitName=unitName;
	} 
	  
	public String getUnitNameType(){
	 return unitNameType; 
	} 
	   
	public void setUnitNameType(String unitNameType) 
	{
	 this.unitNameType=unitNameType;
	} 
	  
	public String getUnitPrice(){
	 return unitPrice; 
	} 
	   
	public void setUnitPrice(String unitPrice) 
	{
	 this.unitPrice=unitPrice;
	} 
	  
	public String getUpdateAdminNo(){
	 return updateAdminNo; 
	} 
	   
	public void setUpdateAdminNo(String updateAdminNo) 
	{
	 this.updateAdminNo=updateAdminNo;
	} 
	  
	public String getUpdateYmdt(){
	 return updateYmdt; 
	} 
	   
	public void setUpdateYmdt(String updateYmdt) 
	{
	 this.updateYmdt=updateYmdt;
	} 
	  
	public String getUrlDirectDisplayYn(){
	 return urlDirectDisplayYn; 
	} 
	   
	public void setUrlDirectDisplayYn(String urlDirectDisplayYn) 
	{
	 this.urlDirectDisplayYn=urlDirectDisplayYn;
	} 
	  
	public String getValueAddedTaxType(){
	 return valueAddedTaxType; 
	} 
	   
	public void setValueAddedTaxType(String valueAddedTaxType) 
	{
	 this.valueAddedTaxType=valueAddedTaxType;
	} 

	public List<DeliveryGroupsVO> getDeliveryGroups() {
		return deliveryGroups;
	}

	public void setDeliveryGroups(List<DeliveryGroupsVO> deliveryGroups) {
		this.deliveryGroups = deliveryGroups;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOShopbuyItem [accumulationRate=");
		builder.append(accumulationRate);
		builder.append(",accumulationRate=");
		builder.append(accumulationRate);
		builder.append(",accumulationUseYn=");
		builder.append(accumulationUseYn);
		builder.append(",addOptionImageYn=");
		builder.append(addOptionImageYn);
		builder.append(",additionalDiscountYn=");
		builder.append(additionalDiscountYn);
		builder.append(",adminNo=");
		builder.append(adminNo);
		builder.append(",applyStatusType=");
		builder.append(applyStatusType);
		builder.append(",brandNo=");
		builder.append(brandNo);
		builder.append(",cartOffEndYmdt=");
		builder.append(cartOffEndYmdt);
		builder.append(",cartOffPeriodYn=");
		builder.append(cartOffPeriodYn);
		builder.append(",cartOffStartYmdt=");
		builder.append(cartOffStartYmdt);
		builder.append(",cartUseYn=");
		builder.append(cartUseYn);
		builder.append(",categoryNo=");
		builder.append(categoryNo);
		builder.append(",certificationJson=");
		builder.append(certificationJson);
		builder.append(",certificationType=");
		builder.append(certificationType);
		builder.append(",classType=");
		builder.append(classType);
		builder.append(",commissionRate=");
		builder.append(commissionRate);
		builder.append(",commissionRateType=");
		builder.append(commissionRateType);
		builder.append(",comparingPriceSite=");
		builder.append(comparingPriceSite);
		builder.append(",content=");
		builder.append(content);
		builder.append(",contentFooter=");
		builder.append(contentFooter);
		builder.append(",contentHeader=");
		builder.append(contentHeader);
		builder.append(",couponYn=");
		builder.append(couponYn);
		builder.append(",deleteYn=");
		builder.append(deleteYn);
		builder.append(",deliveryCombinationYn=");
		builder.append(deliveryCombinationYn);
		builder.append(",deliveryCustomerInfo=");
		builder.append(deliveryCustomerInfo);
		builder.append(",deliveryInternationalYn=");
		builder.append(deliveryInternationalYn);
		builder.append(",deliveryTemplateNo=");
		builder.append(deliveryTemplateNo);
		builder.append(",deliveryYn=");
		builder.append(deliveryYn);
		builder.append(",displayBrandNo=");
		builder.append(displayBrandNo);
		builder.append(",dutyInfo=");
		builder.append(dutyInfo);
		builder.append(",eanCode=");
		builder.append(eanCode);
		builder.append(",expirationYmdt=");
		builder.append(expirationYmdt);
		builder.append(",extraJson=");
		builder.append(extraJson);
		builder.append(",frontDisplayYn=");
		builder.append(frontDisplayYn);
		builder.append(",groupType=");
		builder.append(groupType);
		builder.append(",hsCode=");
		builder.append(hsCode);
		builder.append(",immediateDiscountApplyPrice=");
		builder.append(immediateDiscountApplyPrice);
		builder.append(",immediateDiscountEndYmdt=");
		builder.append(immediateDiscountEndYmdt);
		builder.append(",immediateDiscountPeriodYn=");
		builder.append(immediateDiscountPeriodYn);
		builder.append(",immediateDiscountStartYmdt=");
		builder.append(immediateDiscountStartYmdt);
		builder.append(",immediateDiscountUnitType=");
		builder.append(immediateDiscountUnitType);
		builder.append(",immediateDiscountValue=");
		builder.append(immediateDiscountValue);
		builder.append(",isOptionUsed=");
		builder.append(isOptionUsed);
		builder.append(",itemYn=");
		builder.append(itemYn);
		builder.append(",keyword=");
		builder.append(keyword);
		builder.append(",mallNo=");
		builder.append(mallNo);
		builder.append(",mallProductNo=");
		builder.append(mallProductNo);
		builder.append(",manufactureYmdt=");
		builder.append(manufactureYmdt);
		builder.append(",mappingKey=");
		builder.append(mappingKey);
		builder.append(",masterYn=");
		builder.append(masterYn);
		builder.append(",maxBuyDays=");
		builder.append(maxBuyDays);
		builder.append(",maxBuyPeriodCnt=");
		builder.append(maxBuyPeriodCnt);
		builder.append(",maxBuyPersonCnt=");
		builder.append(maxBuyPersonCnt);
		builder.append(",maxBuyTimeCnt=");
		builder.append(maxBuyTimeCnt);
		builder.append(",memberGradeDisplayInfo=");
		builder.append(memberGradeDisplayInfo);
		builder.append(",memberGroupDisplayInfo=");
		builder.append(memberGroupDisplayInfo);
		builder.append(",minBuyCnt=");
		builder.append(minBuyCnt);
		builder.append(",minorPurchaseYn=");
		builder.append(minorPurchaseYn);
		builder.append(",nonmemberPurchaseYn=");
		builder.append(nonmemberPurchaseYn);
		builder.append(",parentMallProductNo=");
		builder.append(parentMallProductNo);
		builder.append(",partnerChargeAmt=");
		builder.append(partnerChargeAmt);
		builder.append(",partnerName=");
		builder.append(partnerName);
		builder.append(",partnerNo=");
		builder.append(partnerNo);
		builder.append(",paymentMeans=");
		builder.append(paymentMeans);
		builder.append(",paymentMeansControlYn=");
		builder.append(paymentMeansControlYn);
		builder.append(",placeOrigin=");
		builder.append(placeOrigin);
		builder.append(",placeOriginSeq=");
		builder.append(placeOriginSeq);
		builder.append(",placeOriginsYn=");
		builder.append(placeOriginsYn);
		builder.append(",platformDisplayMobileWebYn=");
		builder.append(platformDisplayMobileWebYn);
		builder.append(",platformDisplayMobileYn=");
		builder.append(platformDisplayMobileYn);
		builder.append(",platformDisplayPcYn=");
		builder.append(platformDisplayPcYn);
		builder.append(",platformDisplayYn=");
		builder.append(platformDisplayYn);
		builder.append(",pointRate=");
		builder.append(pointRate);
		builder.append(",productManagementCd=");
		builder.append(productManagementCd);
		builder.append(",productName=");
		builder.append(productName);
		builder.append(",productNameEn=");
		builder.append(productNameEn);
		builder.append(",productNo=");
		builder.append(productNo);
		builder.append(",productType=");
		builder.append(productType);
		builder.append(",promotionText=");
		builder.append(promotionText);
		builder.append(",promotionTextEndYmdt=");
		builder.append(promotionTextEndYmdt);
		builder.append(",promotionTextStartYmdt=");
		builder.append(promotionTextStartYmdt);
		builder.append(",promotionTextYn=");
		builder.append(promotionTextYn);
		builder.append(",promotionYn=");
		builder.append(promotionYn);
		builder.append(",refundableYn=");
		builder.append(refundableYn);
		builder.append(",registerAdminNo=");
		builder.append(registerAdminNo);
		builder.append(",registerSiteType=");
		builder.append(registerSiteType);
		builder.append(",registerYmdt=");
		builder.append(registerYmdt);
		builder.append(",saleEndYmd=");
		builder.append(saleEndYmd);
		builder.append(",saleEndYmdt=");
		builder.append(saleEndYmdt);
		builder.append(",saleMethodType=");
		builder.append(saleMethodType);
		builder.append(",salePeriodType=");
		builder.append(salePeriodType);
		builder.append(",salePrice=");
		builder.append(salePrice);
		builder.append(",saleStartYmd=");
		builder.append(saleStartYmd);
		builder.append(",saleStartYmdt=");
		builder.append(saleStartYmdt);
		builder.append(",saleStatusType=");
		builder.append(saleStatusType);
		builder.append(",searchengineDisplayYn=");
		builder.append(searchengineDisplayYn);
		builder.append(",shippingAreaPartnerNo=");
		builder.append(shippingAreaPartnerNo);
		builder.append(",shippingAreaType=");
		builder.append(shippingAreaType);
		builder.append(",syncWmsYn=");
		builder.append(syncWmsYn);
		builder.append(",tempSave=");
		builder.append(tempSave);
		builder.append(",unitName=");
		builder.append(unitName);
		builder.append(",unitNameType=");
		builder.append(unitNameType);
		builder.append(",unitPrice=");
		builder.append(unitPrice);
		builder.append(",updateAdminNo=");
		builder.append(updateAdminNo);
		builder.append(",updateYmdt=");
		builder.append(updateYmdt);
		builder.append(",urlDirectDisplayYn=");
		builder.append(urlDirectDisplayYn);
		builder.append(",valueAddedTaxType=");
		builder.append(valueAddedTaxType);

		builder.append(", deliveryGroups=");
		builder.append(deliveryGroups);
		builder.append("]");
		return builder.toString();
	}

}