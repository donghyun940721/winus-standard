package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVOGodomallItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String goodsNo;
	private String goodsNm;
	private String goodsNmFl;
	private String goodsNmMain;
	private String goodsNmList;
	private String goodsNmDetail;
	private String goodsNmPartner;
	private String purchaseGoodsNm;
	private String goodsDisplayFl;
	private String goodsDisplayMobileFl;
	private String goodsSellFl;
	private String goodsSellMobilFl;
	private String scmNo;
	private String applyType;
	private String applyMsg;
	private String applyDt;
	private String commission;
	private String goodsCd;
	private String catCd;
	private String allCateCd;
	private String goodsSearchWord;
	private String goodsOpenDt;
	private String goodsState;
	private String goodsColor;
	private String brandCd;
	private String makerNm;
	private String originNm;
	private String goodsModelNo;
	private String makeYmd;
	private String launchYmd;
	private String effectiveStartYmd;
	private String effectiveEndYmd;
	private String goodsPermission;
	private String goodsPermissionGroup;
	private String onlyAdultFl;
	private String imageStorage;
	private String taxFreeFl;
	private String taxPercent;
	private String goodsWeight;
	private String totalStock;
	private String stockFl;
	private String soldOutFl;
	private String salesUnit;
	private String minOrderCnt;
	private String maxOrderCnt;
	private String salesStartYmd;
	private String salesEndYmd;
	private String restockFl;
	private String mileageFl;
	private String mileageGoods;
	private String mileageGoodsUnit;
	private String goodsDiscountFl;
	private String goodsDiscount;
	private String goodsDiscountUnit;
	private String payLimitFl;
	private String payLimit;
	private String goodsPriceString;
	private String goodsPrice;
	private String fixedPrice;
	private String costPrice;
	private String optionFl;
	private String optionDisplayFl;
	private String optionName;
	private String optionTextFl;
	private String addGoodsFl;
	private String shortDescription;
	private String goodsDescription;
	private String goodsDescriptionMobile;
	private String deliverySno;
	private String relationFl;
	private String relationSameFl;
	private String relationGoodsNo;
	private String goodsIconStartYmd;
	private String goodsIconEndYmd;
	private String goodsIconCdPeriod;
	private String goodsIconCd;
	private String imgDetailViewFl;
	private String externalVideoFl;
	private String externalVideoUrl;
	private String externalVideoWidth;
	private String externalVideoHeight;
	private String detailInfoDelivery;
	private String detailInfoAS;
	private String detailInfoRefund;
	private String detailInfoExchange;
	private String memo;
	private String orderCnt;
	private String hitCnt;
	private String reviewCnt;
	private String excelFl;
	private String regDt;
	private String modDt;
	private String addGoodsData;
	private String purchaseNm;
	private String purchaseCatagory;
	private String purchasePhone;
	private String purchaseAddress;
	private String purchaseAddressSub;
	private String purchaseMemo;
	private String cultureBenefitFl;
	private String eventDescription;
	
	private MagnifyImageDataVO magnifyImageData;	
	private MainImageDataVO mainImageData;	
	private ListImageDataVO listImageData;	
	private DetailImageDataVO detailImageData;	

	private List<OptionDataVO> optionData;
	private List<TextOptionDataVO> textOptionData;
	private List<GoodsMustInfoDataVO> goodsMustInfoData;
	
	public String getGoodsNo() { 
		return   goodsNo;
	}
	
	public void setGoodsNo(String goodsNo) {
		this.goodsNo=goodsNo; 
	} 
	
	public String getGoodsNm() { 
		return   goodsNm;
	}
	
	public void setGoodsNm(String goodsNm) {
		this.goodsNm=goodsNm; 
	} 
	
	public String getGoodsNmFl() { 
		return   goodsNmFl;
	}
	
	public void setGoodsNmFl(String goodsNmFl) {
		this.goodsNmFl=goodsNmFl; 
	}
	
	public String getGoodsNmMain() { 
		return   goodsNmMain;
	}
	
	public void setGoodsNmMain(String goodsNmMain) {
		this.goodsNmMain=goodsNmMain; 
	}
	
	public String getGoodsNmList() { 
		return   goodsNmList;
	}
	
	public void setGoodsNmList(String goodsNmList) {
		this.goodsNmList=goodsNmList; 
	}
	
	public String getGoodsNmDetail() { 
		return   goodsNmDetail;
	}
	
	public void setGoodsNmDetail(String goodsNmDetail) {
		this.goodsNmDetail=goodsNmDetail; 
	}
	
	public String getGoodsNmPartner() {
		return   goodsNmPartner;
	}
	
	public void setGoodsNmPartner(String goodsNmPartner) {
		this.goodsNmPartner=goodsNmPartner; 
	}
	
	public String getPurchaseGoodsNm() { 
		return   purchaseGoodsNm;
	}
	
	public void setPurchaseGoodsNm(String purchaseGoodsNm) {
		this.purchaseGoodsNm=purchaseGoodsNm; 
	}
	
	public String getGoodsDisplayFl() { 
		return   goodsDisplayFl;
	}
	
	public void setGoodsDisplayFl(String goodsDisplayFl) {
		this.goodsDisplayFl=goodsDisplayFl; 
	}
	
	public String getGoodsDisplayMobileFl() { 
		return   goodsDisplayMobileFl;
	}
	
	public void setGoodsDisplayMobileFl(String goodsDisplayMobileFl) {
		this.goodsDisplayMobileFl=goodsDisplayMobileFl; 
	}
	
	public String getGoodsSellFl() { 
		return   goodsSellFl;
	}
	
	public void setGoodsSellFl(String goodsSellFl) {
		this.goodsSellFl=goodsSellFl; 
	}
	
	public String getGoodsSellMobilFl() { 
		return   goodsSellMobilFl;
	}
	
	public void setGoodsSellMobilFl(String goodsSellMobilFl) {
		this.goodsSellMobilFl=goodsSellMobilFl; 
	}
	
	public String getScmNo() { 
		return   scmNo;
	}
	
	public void setScmNo(String scmNo) {
		this.scmNo=scmNo; 
	}
	
	public String getApplyType() { 
		return   applyType;
	}
	
	public void setApplyType(String applyType) {
		this.applyType=applyType; 
	}
	
	public String getApplyMsg() { 
		return   applyMsg;
	}
	
	public void setApplyMsg(String applyMsg) {
		this.applyMsg=applyMsg; 
	}
	
	public String getApplyDt() { 
		return   applyDt;
	}
	
	public void setApplyDt(String applyDt) {
		this.applyDt=applyDt; 
	}
	
	public String getCommission() { 
		return   commission;
	}
	
	public void setCommission(String commission) {
		this.commission=commission; 
	}
	
	public String getGoodsCd() { 
		return   goodsCd;
	}
	
	public void setGoodsCd(String goodsCd) {
		this.goodsCd=goodsCd; 
	}
	
	public String getCatCd() { 
		return   catCd;
	}
	
	public void setCatCd(String catCd) {
		this.catCd=catCd; 
	}
	
	public String getAllCateCd() { 
		return   allCateCd;
	}
	
	public void setAllCateCd(String allCateCd) {
		this.allCateCd=allCateCd; 
	}
	
	public String getGoodsSearchWord() { 
		return   goodsSearchWord;
	}
	
	public void setGoodsSearchWord(String goodsSearchWord) {
		this.goodsSearchWord=goodsSearchWord; 
	}
	
	public String getGoodsOpenDt() { 
		return   goodsOpenDt;
	}
	
	public void setGoodsOpenDt(String goodsOpenDt) {
		this.goodsOpenDt=goodsOpenDt; 
	}
	
	public String getGoodsState() { 
		return   goodsState;
	}
	
	public void setGoodsState(String goodsState) {
		this.goodsState=goodsState; 
	}
	
	public String getGoodsColor() { 
		return   goodsColor;
		}
	
	public void setGoodsColor(String goodsColor) {
		this.goodsColor=goodsColor; 
		}
	
	public String getBrandCd() { 
		return   brandCd;
		}
	
	public void setBrandCd(String brandCd) {
		this.brandCd=brandCd; 
		}
	
	public String getMakerNm() { 
		return   makerNm;
		}
	
	public void setMakerNm(String makerNm) {
		this.makerNm=makerNm; 
		}
	
	public String getOriginNm() { 
		return   originNm;
		}
	
	public void setOriginNm(String originNm) {
		this.originNm=originNm; 
		}
	
	public String getGoodsModelNo() { 
		return   goodsModelNo;
		}
	
	public void setGoodsModelNo(String goodsModelNo) {
		this.goodsModelNo=goodsModelNo; 
		}
	
	public String getMakeYmd() { 
		return   makeYmd;
		}
	
	public void setMakeYmd(String makeYmd) {
		this.makeYmd=makeYmd; 
		}
	
	public String getLaunchYmd() { 
		return   launchYmd;
		}
	
	public void setLaunchYmd(String launchYmd) {
		this.launchYmd=launchYmd; 
		}
	
	public String getEffectiveStartYmd() { 
		return   effectiveStartYmd;
		}
	
	public void setEffectiveStartYmd(String effectiveStartYmd) {
		this.effectiveStartYmd=effectiveStartYmd; 
		}
	
	public String getEffectiveEndYmd() { 
		return   effectiveEndYmd;
		}
	
	public void setEffectiveEndYmd(String effectiveEndYmd) {
		this.effectiveEndYmd=effectiveEndYmd; 
		}
	
	public String getGoodsPermission() { 
		return   goodsPermission;
		}
	
	public void setGoodsPermission(String goodsPermission) {
		this.goodsPermission=goodsPermission; 
		}
	
	public String getGoodsPermissionGroup() { 
		return   goodsPermissionGroup;
		}
	
	public void setGoodsPermissionGroup(String goodsPermissionGroup) {
		this.goodsPermissionGroup=goodsPermissionGroup; 
		}
	
	public String getOnlyAdultFl() { 
		return   onlyAdultFl;
		}
	
	public void setOnlyAdultFl(String onlyAdultFl) {
		this.onlyAdultFl=onlyAdultFl; 
		}
	
	public String getImageStorage() { 
		return   imageStorage;
		}
	
	public void setImageStorage(String imageStorage) {
		this.imageStorage=imageStorage; 
		}
	
	public String getTaxFreeFl() { 
		return   taxFreeFl;
		}
	
	public void setTaxFreeFl(String taxFreeFl) {
		this.taxFreeFl=taxFreeFl; 
		}
	
	public String getTaxPercent() { 
		return   taxPercent;
		}
	
	public void setTaxPercent(String taxPercent) {
		this.taxPercent=taxPercent; 
		}
	
	public String getGoodsWeight() { 
		return   goodsWeight;
		}
	
	public void setGoodsWeight(String goodsWeight) {
		this.goodsWeight=goodsWeight; 
		}
	
	public String getTotalStock() { 
		return   totalStock;
		}
	
	public void setTotalStock(String totalStock) {
		this.totalStock=totalStock; 
		}
	
	public String getStockFl() { 
		return   stockFl;
		}
	
	public void setStockFl(String stockFl) {
		this.stockFl=stockFl; 
		}
	
	public String getSoldOutFl() { 
		return   soldOutFl;
		}
	
	public void setSoldOutFl(String soldOutFl) {
		this.soldOutFl=soldOutFl; 
		}
	
	public String getSalesUnit() { 
		return   salesUnit;
		}
	
	public void setSalesUnit(String salesUnit) {
		this.salesUnit=salesUnit; 
		}
	
	public String getMinOrderCnt() { 
		return   minOrderCnt;
		}
	
	public void setMinOrderCnt(String minOrderCnt) {
		this.minOrderCnt=minOrderCnt; 
		}
	
	public String getNaxOrderCnt() { 
		return   maxOrderCnt;
		}
	
	public void setNaxOrderCnt(String maxOrderCnt) {
		this.maxOrderCnt=maxOrderCnt; 
		}
	
	public String getSalesStartYmd() { 
		return   salesStartYmd;
		}
	
	public void setSalesStartYmd(String salesStartYmd) {
		this.salesStartYmd=salesStartYmd;
		}
	
	public String getSalesEndYmd() { 
		return   salesEndYmd;
		}
	
	public void setSalesEndYmd(String salesEndYmd) {
		this.salesEndYmd=salesEndYmd; 
		}
	
	public String getRestockFl() { 
		return   restockFl;
		}
	
	public void setRestockFl(String restockFl) {
		this.restockFl=restockFl;
		}
	
	public String getMileageFl() { 
		return   mileageFl;
		}
	
	public void setMileageFl(String mileageFl) {
		this.mileageFl=mileageFl; 
		}
	
	public String getMileageGoods() { 
		return   mileageGoods;
		}
	
	public void setMileageGoods(String mileageGoods) {
		this.mileageGoods=mileageGoods; 
		}
	
	public String getMileageGoodsUnit() { 
		return   mileageGoodsUnit;
		}
	
	public void setMileageGoodsUnit(String mileageGoodsUnit) {
		this.mileageGoodsUnit=mileageGoodsUnit;
		}
	
	public String getGoodsDiscountFl() { 
		return   goodsDiscountFl;
		}
	
	public void setGoodsDiscountFl(String goodsDiscountFl) {
		this.goodsDiscountFl=goodsDiscountFl; 
		}
	
	public String getGoodsDiscount() { 
		return   goodsDiscount;
		}
	
	public void setGoodsDiscount(String goodsDiscount) {
		this.goodsDiscount=goodsDiscount; 
		}
	
	public String getGoodsDiscountUnit() { 
		return   goodsDiscountUnit;
		}
	
	public void setGoodsDiscountUnit(String goodsDiscountUnit) {
		this.goodsDiscountUnit=goodsDiscountUnit; 
		}
	
	public String getPayLimitFl() { 
		return   payLimitFl;
		}
	
	public void setPayLimitFl(String payLimitFl) {
		this.payLimitFl=payLimitFl; 
		}
	
	public String getPayLimit() { 
		return   payLimit;
		}
	
	public void setPayLimit(String payLimit) {
		this.payLimit=payLimit; 
		}
	
	public String getGoodsPriceString() { 
		return   goodsPriceString;
		}
	
	public void setGoodsPriceString(String goodsPriceString) {
		this.goodsPriceString=goodsPriceString; 
		}
	
	public String getGoodsPrice() { 
		return   goodsPrice;
		}
	
	public void setGoodsPrice(String goodsPrice) {
		this.goodsPrice=goodsPrice; 
		}
	
	public String getFixedPrice() { 
		return   fixedPrice;
		}
	
	public void setFixedPrice(String fixedPrice) {
		this.fixedPrice=fixedPrice; 
		}
	
	public String getCostPrice() { 
		return   costPrice;
		}
	
	public void setCostPrice(String costPrice) {
		this.costPrice=costPrice; 
		}
	
	public String getOptionFl() { 
		return   optionFl;
		}
	
	public void setOptionFl(String optionFl) {
		this.optionFl=optionFl; 
		}
	
	public String getOptionDisplayFl() { 
		return   optionDisplayFl;
		}
	
	public void setOptionDisplayFl(String optionDisplayFl) {
		this.optionDisplayFl=optionDisplayFl; 
		}
	
	public String getOptionName() { 
		return   optionName;
		}
	
	public void setOptionName(String optionName) {
		this.optionName=optionName; 
		}
	
	public String getOptionTextFl() { 
		return   optionTextFl;
		}
	
	public void setOptionTextFl(String optionTextFl) {
		this.optionTextFl=optionTextFl; 
		}
	
	public String getAddGoodsFl() { 
		return   addGoodsFl;
		}
	
	public void setAddGoodsFl(String addGoodsFl) {
		this.addGoodsFl=addGoodsFl; 
		}
	
	public String getShortDescription() { 
		return   shortDescription;
		}
	
	public void setShortDescription(String shortDescription) {
		this.shortDescription=shortDescription; 
		}
	
	public String getGoodsDescription() { 
		return   goodsDescription;
		}
	
	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription=goodsDescription;
		}
	
	public String getGoodsDescriptionMobile() { 
		return   goodsDescriptionMobile;
		}
	
	public void setGoodsDescriptionMobile(String goodsDescriptionMobile) {
		this.goodsDescriptionMobile=goodsDescriptionMobile; 
		}
	
	public String getDeliverySno() { 
		return   deliverySno;
		}
	
	public void setDeliverySno(String deliverySno) {
		this.deliverySno=deliverySno; 
		}
	
	public String getRelationFl() { 
		return   relationFl;
		}
	
	public void setRelationFl(String relationFl) {
		this.relationFl=relationFl; 
		}
	
	public String getRelationSameFl() { 
		return   relationSameFl;
		}
	
	public void setRelationSameFl(String relationSameFl) {
		this.relationSameFl=relationSameFl; 
		}
	
	public String getRelationGoodsNo() { 
		return   relationGoodsNo;
		}
	
	public void setRelationGoodsNo(String relationGoodsNo) {
		this.relationGoodsNo=relationGoodsNo; 
		}
	
	public String getGoodsIconStartYmd() { 
		return   goodsIconStartYmd;
		}
	
	public void setGoodsIconStartYmd(String goodsIconStartYmd) {
		this.goodsIconStartYmd=goodsIconStartYmd; 
		}
	
	public String getGoodsIconEndYmd() { 
		return   goodsIconEndYmd;
		}
	
	public void setGoodsIconEndYmd(String goodsIconEndYmd) {
		this.goodsIconEndYmd=goodsIconEndYmd; 
		}
	
	public String getGoodsIconCdPeriod() { 
		return   goodsIconCdPeriod;
		}
	
	public void setGoodsIconCdPeriod(String goodsIconCdPeriod) {
		this.goodsIconCdPeriod=goodsIconCdPeriod; 
		}
	
	public String getGoodsIconCd() { 
		return   goodsIconCd;
		}
	
	public void setGoodsIconCd(String goodsIconCd) {
		this.goodsIconCd=goodsIconCd; 
		}
	
	public String getImgDetailViewFl() { 
		return   imgDetailViewFl;
		}
	
	public void setImgDetailViewFl(String imgDetailViewFl) {
		this.imgDetailViewFl=imgDetailViewFl; 
		}
	
	public String getExternalVideoFl() { 
		return   externalVideoFl;
		}
	
	public void setExternalVideoFl(String externalVideoFl) {
		this.externalVideoFl=externalVideoFl; 
		}
	
	public String getExternalVideoUrl() { 
		return   externalVideoUrl;
		}
	
	public void setExternalVideoUrl(String externalVideoUrl) {
		this.externalVideoUrl=externalVideoUrl; 
		}
	
	public String getExternalVideoWidth() { 
		return   externalVideoWidth;
		}
	
	public void setExternalVideoWidth(String externalVideoWidth) {
		this.externalVideoWidth=externalVideoWidth; 
		}
	
	public String getExternalVideoHeight() { 
		return   externalVideoHeight;
		}
	
	public void setExternalVideoHeight(String externalVideoHeight) {
		this.externalVideoHeight=externalVideoHeight; 
		}
	
	public String getDetailInfoDelivery() { 
		return   detailInfoDelivery;
		}
	
	public void setDetailInfoDelivery(String detailInfoDelivery) {
		
		this.detailInfoDelivery=detailInfoDelivery; 
		}
	
	public String getDetailInfoAS() { 
		return   detailInfoAS;
		}
	
	public void setDetailInfoAS(String detailInfoAS) {
		this.detailInfoAS=detailInfoAS; 
		}
	
	public String getDetailInfoRefund() { 
		return   detailInfoRefund;
		}
	
	public void setDetailInfoRefund(String detailInfoRefund) {
		this.detailInfoRefund=detailInfoRefund; 
		}
	
	public String getDetailInfoExchange() { 
		return   detailInfoExchange;
		}
	
	public void setDetailInfoExchange(String detailInfoExchange) {
		this.detailInfoExchange=detailInfoExchange; 
		}
	
	public String getMemo() { 
		return   memo;
		}
	
	public void setMemo(String memo) {
		this.memo=memo; 
		}
	
	public String getOrderCnt() { 
		return   orderCnt;
		}
	
	public void setOrderCnt(String orderCnt) {
		this.orderCnt=orderCnt; 
		}
	
	public String getHitCnt() { 
		return   hitCnt;
		}
	
	public void setHitCnt(String hitCnt) {
		this.hitCnt=hitCnt; 
		}
	
	public String getReviewCnt() { 
		return   reviewCnt;
		}
	
	public void setReviewCnt(String reviewCnt) {
		this.reviewCnt=reviewCnt; 
		}
	
	public String getExcelFl() { 
		return   excelFl;
		}
	
	public void setExcelFl(String excelFl) {
		this.excelFl=excelFl; 
		}
	
	public String getRegDt() { 
		return   regDt;
		}
	
	public void setRegDt(String regDt) {
		this.regDt=regDt; 
		}
	
	public String getModDt() { 
		return   modDt;
		}
	
	public void setModDt(String modDt) {
		this.modDt=modDt; 
		}
	
	public String getAddGoodsData() { 
		return   addGoodsData;
		}
	
	public void setAddGoodsData(String addGoodsData) {
		this.addGoodsData=addGoodsData; 
		}

	public String getPurchaseNm() { 
		return   purchaseNm;
	}
	public void setPurchaseNm(String purchaseNm) {
		this.purchaseNm=purchaseNm; 
	}
	public String getPurchaseCatagory() { 
		return   purchaseCatagory;
	}
	public void setPurchaseCatagory(String purchaseCatagory) {
		this.purchaseCatagory=purchaseCatagory; 
	}
	public String getPurchasePhone() { 
		return   purchasePhone;
	}
	public void setPurchasePhone(String purchasePhone) {
		this.purchasePhone=purchasePhone; 
	}
	public String getPurchaseAddress() { 
		return   purchaseAddress;
	}
	public void setPurchaseAddress(String purchaseAddress) {
		this.purchaseAddress=purchaseAddress; 
	}
	public String getPurchaseAddressSub() { 
		return   purchaseAddressSub;
	}
	public void setPurchaseAddressSub(String purchaseAddressSub) {
		this.purchaseAddressSub=purchaseAddressSub; 
	}
	public String getPurchaseMemo() { 
		return   purchaseMemo;
	}
	public void setPurchaseMemo(String purchaseMemo) {
		this.purchaseMemo=purchaseMemo; 
	}
	public String getCultureBenefitFl() { 
		return   cultureBenefitFl;
	}
	public void setCultureBenefitFl(String cultureBenefitFl) {
		this.cultureBenefitFl=cultureBenefitFl; 
	}
	public String getEventDescription() { 
		return   eventDescription;
	}
	public void setEventDescription(String eventDescription) {
		this.eventDescription=eventDescription; 
	}

	public MagnifyImageDataVO getMagnifyImageData() {
		return magnifyImageData;
	}

	public void setMagnifyImageData(MagnifyImageDataVO magnifyImageData) {
		this.magnifyImageData = magnifyImageData;
	}

	public MainImageDataVO getMainImageData() {
		return mainImageData;
	}

	public void setMainImageData(MainImageDataVO mainImageData) {
		this.mainImageData = mainImageData;
	}

	public ListImageDataVO getListImageData() {
		return listImageData;
	}

	public void setListImageData(ListImageDataVO listImageData) {
		this.listImageData = listImageData;
	}

	public DetailImageDataVO getDetailImageData() {
		return detailImageData;
	}

	public void setDetailImageData(DetailImageDataVO detailImageData) {
		this.detailImageData = detailImageData;
	}

	public List<TextOptionDataVO> getTextOptionData() {
		return textOptionData;
	}

	public void setTextOptionData(List<TextOptionDataVO> textOptionData) {
		this.textOptionData = textOptionData;
	}

	public List<OptionDataVO> getOptionData() {
		return optionData;
	}

	public void setOptionData(List<OptionDataVO> optionData) {
		this.optionData = optionData;
	}

	public List<GoodsMustInfoDataVO> getGoodsMustInfoData() {
		return goodsMustInfoData;
	}

	public void setGoodsMustInfoData(List<GoodsMustInfoDataVO> goodsMustInfoData) {
		this.goodsMustInfoData = goodsMustInfoData;
	}
				
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOGodomall [goodsNo=");
		builder.append(goodsNo);
		builder.append(", goodsNm=");
		builder.append(goodsNm);
		builder.append(", goodsNmFl=");
		builder.append(goodsNmFl);
		builder.append(", goodsNmMain=");
		builder.append(goodsNmMain);
		builder.append(", goodsNmList=");
		builder.append(goodsNmList);
		builder.append(", goodsNmDetail=");
		builder.append(goodsNmDetail);
		builder.append(", goodsNmPartner=");
		builder.append(goodsNmPartner);
		builder.append(", purchaseGoodsNm=");
		builder.append(purchaseGoodsNm);
		builder.append(", goodsDisplayFl=");
		builder.append(goodsDisplayFl);
		builder.append(", goodsDisplayMobileFl=");
		builder.append(goodsDisplayMobileFl);
		builder.append(", goodsSellFl=");
		builder.append(goodsSellFl);
		builder.append(", goodsSellMobilFl=");
		builder.append(goodsSellMobilFl);
		builder.append(", scmNo=");
		builder.append(scmNo);
		builder.append(", applyType=");
		builder.append(applyType);
		builder.append(", applyMsg=");
		builder.append(applyMsg);
		builder.append(", applyDt=");
		builder.append(applyDt);
		builder.append(", commission=");
		builder.append(commission);
		builder.append(", goodsCd=");
		builder.append(goodsCd);
		builder.append(", catCd=");
		builder.append(catCd);
		builder.append(", allCateCd=");
		builder.append(allCateCd);
		builder.append(", goodsSearchWord=");
		builder.append(goodsSearchWord);
		builder.append(", goodsOpenDt=");
		builder.append(goodsOpenDt);
		builder.append(", goodsState=");
		builder.append(goodsState);
		builder.append(", goodsColor=");
		builder.append(goodsColor);
		builder.append(", brandCd=");
		builder.append(brandCd);
		builder.append(", makerNm=");
		builder.append(makerNm);
		builder.append(", originNm=");
		builder.append(originNm);
		builder.append(", goodsModelNo=");
		builder.append(goodsModelNo);
		builder.append(", makeYmd=");
		builder.append(makeYmd);
		builder.append(", launchYmd=");
		builder.append(launchYmd);
		builder.append(", effectiveStartYmd=");
		builder.append(effectiveStartYmd);
		builder.append(", effectiveEndYmd=");
		builder.append(effectiveEndYmd);
		builder.append(", goodsPermission=");
		builder.append(goodsPermission);
		builder.append(", goodsPermissionGroup=");
		builder.append(goodsPermissionGroup);
		builder.append(", onlyAdultFl=");
		builder.append(onlyAdultFl);
		builder.append(", imageStorage=");
		builder.append(imageStorage);
		builder.append(", taxFreeFl=");
		builder.append(taxFreeFl);
		builder.append(", taxPercent=");
		builder.append(taxPercent);
		builder.append(", goodsWeight=");
		builder.append(goodsWeight);
		builder.append(", totalStock=");
		builder.append(totalStock);
		builder.append(", stockFl=");
		builder.append(stockFl);
		builder.append(", soldOutFl=");
		builder.append(soldOutFl);
		builder.append(", salesUnit=");
		builder.append(salesUnit);
		builder.append(", minOrderCnt=");
		builder.append(minOrderCnt);
		builder.append(", maxOrderCnt=");
		builder.append(maxOrderCnt);
		builder.append(", salesStartYmd=");
		builder.append(salesStartYmd);
		builder.append(", salesEndYmd=");
		builder.append(salesEndYmd);
		builder.append(", restockFl=");
		builder.append(restockFl);
		builder.append(", mileageFl=");
		builder.append(mileageFl);
		builder.append(", mileageGoods=");
		builder.append(mileageGoods);
		builder.append(", mileageGoodsUnit=");
		builder.append(mileageGoodsUnit);
		builder.append(", goodsDiscountFl=");
		builder.append(goodsDiscountFl);
		builder.append(", goodsDiscount=");
		builder.append(goodsDiscount);
		builder.append(", goodsDiscountUnit=");
		builder.append(goodsDiscountUnit);
		builder.append(", payLimitFl=");
		builder.append(payLimitFl);
		builder.append(", payLimit=");
		builder.append(payLimit);
		builder.append(", goodsPriceString=");
		builder.append(goodsPriceString);
		builder.append(", goodsPrice=");
		builder.append(goodsPrice);
		builder.append(", fixedPrice=");
		builder.append(fixedPrice);
		builder.append(", costPrice=");
		builder.append(costPrice);
		builder.append(", optionFl=");
		builder.append(optionFl);
		builder.append(", optionDisplayFl=");
		builder.append(optionDisplayFl);
		builder.append(", optionName=");
		builder.append(optionName);
		builder.append(", optionTextFl=");
		builder.append(optionTextFl);
		builder.append(", addGoodsFl=");
		builder.append(addGoodsFl);
		builder.append(", shortDescription=");
		builder.append(shortDescription);
		builder.append(", goodsDescription=");
		builder.append(goodsDescription);
		builder.append(", goodsDescriptionMobile=");
		builder.append(goodsDescriptionMobile);
		builder.append(", deliverySno=");
		builder.append(deliverySno);
		builder.append(", relationFl=");
		builder.append(relationFl);
		builder.append(", relationSameFl=");
		builder.append(relationSameFl);
		builder.append(", relationGoodsNo=");
		builder.append(relationGoodsNo);
		builder.append(", goodsIconStartYmd=");
		builder.append(goodsIconStartYmd);
		builder.append(", goodsIconEndYmd=");
		builder.append(goodsIconEndYmd);
		builder.append(", goodsIconCdPeriod=");
		builder.append(goodsIconCdPeriod);
		builder.append(", goodsIconCd=");
		builder.append(goodsIconCd);
		builder.append(", imgDetailViewFl=");
		builder.append(imgDetailViewFl);
		builder.append(", externalVideoFl=");
		builder.append(externalVideoFl);
		builder.append(", externalVideoUrl=");
		builder.append(externalVideoUrl);
		builder.append(", externalVideoWidth=");
		builder.append(externalVideoWidth);
		builder.append(", externalVideoHeight=");
		builder.append(externalVideoHeight);
		builder.append(", detailInfoDelivery=");
		builder.append(detailInfoDelivery);
		builder.append(", detailInfoAS=");
		builder.append(detailInfoAS);
		builder.append(", detailInfoRefund=");
		builder.append(detailInfoRefund);
		builder.append(", detailInfoExchange=");
		builder.append(detailInfoExchange);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", orderCnt=");
		builder.append(orderCnt);
		builder.append(", hitCnt=");
		builder.append(hitCnt);
		builder.append(", reviewCnt=");
		builder.append(reviewCnt);
		builder.append(", excelFl=");
		builder.append(excelFl);
		builder.append(", regDt=");
		builder.append(regDt);
		builder.append(", modDt=");
		builder.append(modDt);
		builder.append(", addGoodsData=");
		builder.append(addGoodsData);		
		builder.append(", purchaseNm=");
		builder.append(purchaseNm);
		builder.append(", purchaseCatagory=");
		builder.append(purchaseCatagory);
		builder.append(", purchasePhone=");
		builder.append(purchasePhone);
		builder.append(", purchaseAddress=");
		builder.append(purchaseAddress);
		builder.append(", purchaseAddressSub=");
		builder.append(purchaseAddressSub);
		builder.append(", purchaseMemo=");
		builder.append(purchaseMemo);
		builder.append(", cultureBenefitFl=");
		builder.append(cultureBenefitFl);
		builder.append(", eventDescription=");
		builder.append(eventDescription);
		builder.append(", optionData=");
		builder.append(optionData);	
		builder.append(", magnifyImageData=");
		builder.append(magnifyImageData);
		builder.append(", detailImageData=");
		builder.append(detailImageData);
		builder.append(", listImageData=");
		builder.append(listImageData);
		builder.append(", mainImageData=");
		builder.append(mainImageData);
		builder.append(", textOptionData=");
		builder.append(textOptionData);
		builder.append(", goodsMustInfoData=");
		builder.append(goodsMustInfoData);
		builder.append("]");
		return builder.toString();
	}

}