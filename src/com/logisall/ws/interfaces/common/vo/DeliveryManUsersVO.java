package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;

public class DeliveryManUsersVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;
	
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("DeliveryManUsersVO [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}
}
