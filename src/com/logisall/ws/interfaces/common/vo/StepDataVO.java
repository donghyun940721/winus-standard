package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;

public class StepDataVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;
	
	private String idx;
	private String infoValue;
	private String infoTitle;
	
	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public String getInfoValue() {
		return infoValue;
	}

	public void setInfoValue(String infoValue) {
		this.infoValue = infoValue;
	}

	public String getInfoTitle() {
		return infoTitle;
	}

	public void setInfoTitle(String infoTitle) {
		this.infoTitle = infoTitle;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("StepDataVO [idx=");
		builder.append(idx);
		builder.append(", infoValue=");
		builder.append(infoValue);
		builder.append(", infoTitle=");
		builder.append(infoTitle);
		builder.append("]");
		return builder.toString();
	}
}
