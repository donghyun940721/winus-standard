package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;

public class OnsiteImagesVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;
	
	private String onsiteImages;

	public String getOnsiteImages() {
		return onsiteImages;
	}

	public void setOnsiteImages(String onsiteImages) {
		this.onsiteImages = onsiteImages;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("OnsiteImagesVO [onsiteImages=");
		builder.append(onsiteImages);
		builder.append("]");
		return builder.toString();
	}
}
