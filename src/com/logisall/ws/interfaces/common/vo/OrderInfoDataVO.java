package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;

public class OrderInfoDataVO implements Serializable {

	private static final long serialVersionUID = -3844727177593095135L;

	private String orderInfoCd;
	private String orderName;
	private String orderEmail;
	private String orderPhonePrefixCode;
	private String orderPhonePrefix;
	private String orderPhone;
	private String orderCellPhonePrefixCode;
	private String orderCellPhonePrefix;
	private String orderCellPhone;
	private String orderZipcode;
	private String orderZonecode;
	private String orderState;
	private String orderCity;
	private String orderAddress;
	private String orderAddressSub;
	private String receiverName;
	private String receiverCountryCode;
	private String receiverPhonePrefixCode;
	private String receiverPhonePrefix;
	private String receiverPhone;
	private String receiverCellPhonePrefixCode;
	private String receiverCellPhonePrefix;
	private String receiverCellPhone;
	private String receiverUseSafeNumberFl;
	private String receiverSafeNumber;
	private String receiverSafeNumberDt;
	private String receiverZipcode;
	private String receiverZonecode;
	private String receiverCountry;
	private String receiverState;
	private String receiverCity;
	private String receiverAddress;
	private String receiverAddressSub;
	private String deliveryVisit;
	private String visitAddress;
	private String visitName;
	private String visitPhone;
	private String visitMemo;
	private String customIdNumber;
	private String orderMemo;
	private String packetCode;

	public String getOrderInfoCd() {
		return orderInfoCd;
	}

	public void setOrderInfoCd(String orderInfoCd) {
		this.orderInfoCd = orderInfoCd;
	}
	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}
	
	public String getOrderEmail() {
		return orderEmail;
	}

	public void setOrderEmail(String orderEmail) {
		this.orderEmail = orderEmail;
	}

	public String getOrderPhonePrefixCode() {
		return orderPhonePrefixCode;
	}

	public void setOrderPhonePrefixCode(String orderPhonePrefixCode) {
		this.orderPhonePrefixCode = orderPhonePrefixCode;
	}

	public String getOrderPhonePrefix() {
		return orderPhonePrefix;
	}

	public void setOrderPhonePrefix(String orderPhonePrefix) {
		this.orderPhonePrefix = orderPhonePrefix;
	}

	public String getOrderPhone() {
		return orderPhone;
	}

	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}

	public String getOrderCellPhonePrefixCode() {
		return orderCellPhonePrefixCode;
	}

	public void setOrderCellPhonePrefixCode(String orderCellPhonePrefixCode) {
		this.orderCellPhonePrefixCode = orderCellPhonePrefixCode;
	}

	public String getOrderCellPhonePrefix() {
		return orderCellPhonePrefix;
	}

	public void setOrderCellPhonePrefix(String orderCellPhonePrefix) {
		this.orderCellPhonePrefix = orderCellPhonePrefix;
	}

	public String getOrderCellPhone() {
		return orderCellPhone;
	}

	public void setOrderCellPhone(String orderCellPhone) {
		this.orderCellPhone = orderCellPhone;
	}

	public String getOrderZipcode() {
		return orderZipcode;
	}

	public void setOrderZipcode(String orderZipcode) {
		this.orderZipcode = orderZipcode;
	}

	public String getOrderZonecode() {
		return orderZonecode;
	}

	public void setOrderZonecode(String orderZonecode) {
		this.orderZonecode = orderZonecode;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getOrderCity() {
		return orderCity;
	}

	public void setOrderCity(String orderCity) {
		this.orderCity = orderCity;
	}

	public String getOrderAddress() {
		return orderAddress;
	}

	public void setOrderAddress(String orderAddress) {
		this.orderAddress = orderAddress;
	}

	public String getOrderAddressSub() {
		return orderAddressSub;
	}

	public void setOrderAddressSub(String orderAddressSub) {
		this.orderAddressSub = orderAddressSub;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverCountryCode() {
		return receiverCountryCode;
	}

	public void setReceiverCountryCode(String receiverCountryCode) {
		this.receiverCountryCode = receiverCountryCode;
	}

	public String getReceiverPhonePrefixCode() {
		return receiverPhonePrefixCode;
	}

	public void setReceiverPhonePrefixCode(String receiverPhonePrefixCode) {
		this.receiverPhonePrefixCode = receiverPhonePrefixCode;
	}

	public String getReceiverPhonePrefix() {
		return receiverPhonePrefix;
	}

	public void setReceiverPhonePrefix(String receiverPhonePrefix) {
		this.receiverPhonePrefix = receiverPhonePrefix;
	}

	public String getReceiverPhone() {
		return receiverPhone;
	}

	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}

	public String getReceiverCellPhonePrefixCode() {
		return receiverCellPhonePrefixCode;
	}

	public void setReceiverCellPhonePrefixCode(String receiverCellPhonePrefixCode) {
		this.receiverCellPhonePrefixCode = receiverCellPhonePrefixCode;
	}

	public String getReceiverCellPhonePrefix() {
		return receiverCellPhonePrefix;
	}

	public void setReceiverCellPhonePrefix(String receiverCellPhonePrefix) {
		this.receiverCellPhonePrefix = receiverCellPhonePrefix;
	}

	public String getReceiverCellPhone() {
		return receiverCellPhone;
	}

	public void setReceiverCellPhone(String receiverCellPhone) {
		this.receiverCellPhone = receiverCellPhone;
	}

	public String getReceiverUseSafeNumberFl() {
		return receiverUseSafeNumberFl;
	}

	public void setReceiverUseSafeNumberFl(String receiverUseSafeNumberFl) {
		this.receiverUseSafeNumberFl = receiverUseSafeNumberFl;
	}

	public String getReceiverSafeNumber() {
		return receiverSafeNumber;
	}

	public void setReceiverSafeNumber(String receiverSafeNumber) {
		this.receiverSafeNumber = receiverSafeNumber;
	}

	public String getReceiverSafeNumberDt() {
		return receiverSafeNumberDt;
	}

	public void setReceiverSafeNumberDt(String receiverSafeNumberDt) {
		this.receiverSafeNumberDt = receiverSafeNumberDt;
	}

	public String getReceiverZipcode() {
		return receiverZipcode;
	}

	public void setReceiverZipcode(String receiverZipcode) {
		this.receiverZipcode = receiverZipcode;
	}

	public String getReceiverZonecode() {
		return receiverZonecode;
	}

	public void setReceiverZonecode(String receiverZonecode) {
		this.receiverZonecode = receiverZonecode;
	}

	public String getReceiverCountry() {
		return receiverCountry;
	}

	public void setReceiverCountry(String receiverCountry) {
		this.receiverCountry = receiverCountry;
	}

	public String getReceiverState() {
		return receiverState;
	}

	public void setReceiverState(String receiverState) {
		this.receiverState = receiverState;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverAddressSub() {
		return receiverAddressSub;
	}

	public void setReceiverAddressSub(String receiverAddressSub) {
		this.receiverAddressSub = receiverAddressSub;
	}

	public String getDeliveryVisit() {
		return deliveryVisit;
	}

	public void setDeliveryVisit(String deliveryVisit) {
		this.deliveryVisit = deliveryVisit;
	}

	public String getVisitAddress() {
		return visitAddress;
	}

	public void setVisitAddress(String visitAddress) {
		this.visitAddress = visitAddress;
	}

	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public String getVisitPhone() {
		return visitPhone;
	}

	public void setVisitPhone(String visitPhone) {
		this.visitPhone = visitPhone;
	}

	public String getVisitMemo() {
		return visitMemo;
	}

	public void setVisitMemo(String visitMemo) {
		this.visitMemo = visitMemo;
	}

	public String getCustomIdNumber() {
		return customIdNumber;
	}

	public void setCustomIdNumber(String customIdNumber) {
		this.customIdNumber = customIdNumber;
	}

	public String getOrderMemo() {
		return orderMemo;
	}

	public void setOrderMemo(String orderMemo) {
		this.orderMemo = orderMemo;
	}

	public String getPacketCode() {
		return packetCode;
	}

	public void setPacketCode(String packetCode) {
		this.packetCode = packetCode;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("OrderInfoDataVO [orderInfoCd=");
		builder.append(orderInfoCd);
		builder.append(", orderName=");
		builder.append(orderName);
		builder.append(", orderEmail=");
		builder.append(orderEmail);
		builder.append(", orderPhonePrefixCode=");
		builder.append(orderPhonePrefixCode);
		builder.append(", orderPhonePrefix=");
		builder.append(orderPhonePrefix);
		builder.append(", orderPhone=");
		builder.append(orderPhone);
		builder.append(", orderCellPhonePrefixCode=");
		builder.append(orderCellPhonePrefixCode);
		builder.append(", orderCellPhonePrefix=");
		builder.append(orderCellPhonePrefix);
		builder.append(", orderCellPhone=");
		builder.append(orderCellPhone);
		builder.append(", orderZipcode=");
		builder.append(orderZipcode);
		builder.append(", orderZonecode=");
		builder.append(orderZonecode);
		builder.append(", orderState=");
		builder.append(orderState);
		builder.append(", orderCity=");
		builder.append(orderCity);
		builder.append(", orderAddress=");
		builder.append(orderAddress);
		builder.append(", orderAddressSub=");
		builder.append(orderAddressSub);
		builder.append(", receiverName=");
		builder.append(receiverName);
		builder.append(", receiverCountryCode=");
		builder.append(receiverCountryCode);
		builder.append(", receiverPhonePrefixCode=");
		builder.append(receiverPhonePrefixCode);
		builder.append(", receiverPhonePrefix=");
		builder.append(receiverPhonePrefix);
		builder.append(", receiverPhone=");
		builder.append(receiverPhone);
		builder.append(", receiverCellPhonePrefixCode=");
		builder.append(receiverCellPhonePrefixCode);
		builder.append(", receiverCellPhonePrefix=");
		builder.append(receiverCellPhonePrefix);
		builder.append(", receiverCellPhone=");
		builder.append(receiverCellPhone);
		builder.append(", receiverUseSafeNumberFl=");
		builder.append(receiverUseSafeNumberFl);
		builder.append(", receiverSafeNumber=");
		builder.append(receiverSafeNumber);
		builder.append(", receiverSafeNumberDt=");
		builder.append(receiverSafeNumberDt);
		builder.append(", receiverZipcode=");
		builder.append(receiverZipcode);
		builder.append(", receiverZonecode=");
		builder.append(receiverZonecode);
		builder.append(", receiverCountry=");
		builder.append(receiverCountry);
		builder.append(", receiverState=");
		builder.append(receiverState);
		builder.append(", receiverCity=");
		builder.append(receiverCity);
		builder.append(", receiverAddress=");
		builder.append(receiverAddress);
		builder.append(", receiverAddressSub=");
		builder.append(receiverAddressSub);
		builder.append(", deliveryVisit=");
		builder.append(deliveryVisit);
		builder.append(", visitAddress=");
		builder.append(visitAddress);
		builder.append(", visitName=");
		builder.append(visitName);
		builder.append(", visitPhone=");
		builder.append(visitPhone);
		builder.append(", visitMemo=");
		builder.append(visitMemo);
		builder.append(", customIdNumber=");
		builder.append(customIdNumber);
		builder.append(", orderMemo=");
		builder.append(orderMemo);
		builder.append(", packetCode=");
		builder.append(packetCode);
		builder.append("]");
		return builder.toString();
	}
	
}
