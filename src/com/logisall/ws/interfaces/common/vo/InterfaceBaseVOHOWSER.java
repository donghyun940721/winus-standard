package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;

public class InterfaceBaseVOHOWSER  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6445511060732512635L;
	
	private String orderSrl;
	private String agencyMgrCode;
	private String agencyCustomerCode;
	private String status;
	private String svcType;
	private String receiverName;
	private String arriveAddress;	
	private String receiverPhone2nd;
	private String receiverPhone;
	private String arrivalDate;
	private String arrivalTime;	
	//private String onsiteImages;

	//private DeliveryInfoVO deliveryInfo;

	private List<GoodsResultVO> goodsResult;
	private List<DeliveryManUsersVO> deliveryManUsers;
	private List onsiteImages;

	public String getOrderSrl() {
		return orderSrl;
	}

	public void setOrderSrl(String orderSrl) {
		this.orderSrl = orderSrl;
	}
	
	public String getAgencyMgrCode() {
		return agencyMgrCode;
	}

	public void setAgencyMgrCode(String agencyMgrCode) {
		this.agencyMgrCode = agencyMgrCode;
	}
	
	public String getAgencyCustomerCode() {
		return agencyCustomerCode;
	}

	public void setAgencyCustomerCode(String agencyCustomerCode) {
		this.agencyCustomerCode = agencyCustomerCode;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String ostatusrdSeq) {
		this.status = status;
	}
	
	public String getSvcType() {
		return svcType;
	}

	public void setSvcType(String svcType) {
		this.svcType = svcType;
	}
	
	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	
	public String getArriveAddress() {
		return arriveAddress;
	}

	public void setArriveAddress(String arriveAddress) {
		this.arriveAddress = arriveAddress;
	}
	
	public String getReceiverPhone() {
		return receiverPhone;
	}

	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}
	
	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}	
	
	public String getReceiverPhone2nd() {
		return receiverPhone2nd;
	}

	public void setReceiverPhone2nd(String receiverPhone2nd) {
		this.receiverPhone2nd = receiverPhone2nd;
	}
	
//	public String getOnsiteImages() {
//		return onsiteImages;
//	}
//
//	public void setOnsiteImages(String onsiteImages) {
//		this.onsiteImages = onsiteImages;
//	}
	
//	public DeliveryInfoVO getDeliveryInfo() {
//		return deliveryInfo;
//	}
//
//	public void setDeliveryInfo(DeliveryInfoVO deliveryInfo) {
//		this.deliveryInfo = deliveryInfo;
//	}

	public List<GoodsResultVO> getGoodsResult() {
		return goodsResult;
	}

	public void setGoodsResult(List<GoodsResultVO> goodsResult) {
		this.goodsResult = goodsResult;
	}

	public List<DeliveryManUsersVO> getDeliveryManUsers() {
		return deliveryManUsers;
	}

	public void setDeliveryManUsers(List<DeliveryManUsersVO> deliveryManUsers) {
		this.deliveryManUsers = deliveryManUsers;
	}

	public List getOnsiteImages() {
		return onsiteImages;
	}

	public void setOnsiteImages(List onsiteImages) {
		this.onsiteImages = onsiteImages;
	}

	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOKR [orderSrl=");
		builder.append(orderSrl);
		builder.append(", agencyMgrCode=");
		builder.append(agencyMgrCode);
		builder.append(", agencyCustomerCode=");
		builder.append(agencyCustomerCode);
		builder.append(", status=");
		builder.append(status);
		builder.append(", svcType=");
		builder.append(svcType);
		builder.append(", receiverName=");
		builder.append(receiverName);
		builder.append(", arriveAddress=");
		builder.append(arriveAddress);
		builder.append(", receiverPhone=");
		builder.append(receiverPhone);
		builder.append(", arrivalDate=");
		builder.append(arrivalDate);
		builder.append(", arrivalTime=");
		builder.append(arrivalTime);
		builder.append(", receiverPhone2ns=");
		builder.append(receiverPhone2nd);
//		builder.append(", onsiteImages=");
//		builder.append(onsiteImages);
//		builder.append(", deliveryInfo=");
//		builder.append(deliveryInfo);

		builder.append(", [goodsResult=");
		builder.append(goodsResult);
		builder.append(", deliveryManUsers=");
		builder.append(deliveryManUsers);
		builder.append(", onsiteImages=");
		builder.append(onsiteImages);
		builder.append("]");
		return builder.toString();
	}
	
}
