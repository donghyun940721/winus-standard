package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;

public class ItemSerialInfoVO implements Serializable {

	private static final long serialVersionUID = 7620021732504671435L;

	private String itemSerial;
	private String alertReasonCd;
	private Date rfidReadDt;

	public String getItemSerial() {
		return itemSerial;
	}

	public void setItemSerial(String itemSerial) {
		this.itemSerial = itemSerial;
	}

	public String getAlertReasonCd() {
		return alertReasonCd;
	}

	public void setAlertReasonCd(String alertReasonCd) {
		this.alertReasonCd = alertReasonCd;
	}

	public Date getRfidReadDt() {
		return rfidReadDt;
	}

	public void setRfidReadDt(Date rfidReadDt) {
		this.rfidReadDt = rfidReadDt;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("ItemSerialInfoVO [itemSerial=");
		builder.append(itemSerial);
		builder.append(", alertReasonCd=");
		builder.append(alertReasonCd);
		builder.append(", rfidReadDt=");
		builder.append(rfidReadDt);
		builder.append("]");
		return builder.toString();
	}



}
