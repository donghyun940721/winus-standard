package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;

public class ItemSerialInfoVOKR implements Serializable {

	private static final long serialVersionUID = 7620021732504671435L;

	private String itemSerial;
	private Date rfidReadDt;
	private String childCustLotNo;
	private String mapItemBarCd;
	private String mapItemQty;

	public String getItemSerial() {
		return itemSerial;
	}

	public void setItemSerial(String itemSerial) {
		this.itemSerial = itemSerial;
	}

	public Date getRfidReadDt() {
		return rfidReadDt;
	}

	public void setRfidReadDt(Date rfidReadDt) {
		this.rfidReadDt = rfidReadDt;
	}

	public String getChildCustLotNo() {
		return childCustLotNo;
	}

	public void setChildCustLotNo(String childCustLotNo) {
		this.childCustLotNo = childCustLotNo;
	}
	
	public String getMapItemBarCd() {
		return mapItemBarCd;
	}

	public void setMapItemBarCd(String mapItemBarCd) {
		this.mapItemBarCd = mapItemBarCd;
	}
	
	public String getMapItemQty() {
		return mapItemQty;
	}

	public void setMapItemQty(String mapItemQty) {
		this.mapItemQty = mapItemQty;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("ItemSerialInfoVOKR [itemSerial=");
		builder.append(itemSerial);
		builder.append(", rfidReadDt=");
		builder.append(rfidReadDt);
		builder.append(", childCustLotNo=");
		builder.append(childCustLotNo);
		builder.append(", mapItemBarCd=");
		builder.append(mapItemBarCd);
		builder.append(", mapItemQty=");
		builder.append(mapItemQty);
		builder.append("]");
		return builder.toString();
	}

}
