package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVOForklift implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;

	private String deviceId;
	private String epcCd;
	private String positionX;
	private String positionY;
	private String positionZ;
	private String wgt;
	
	//private List<WorkListBaseVOKR> workList;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getEpcCd() {
		return epcCd;
	}

	public void setEpcCd(String epcCd) {
		this.epcCd = epcCd;
	}

	public String getPositionX() {
		return positionX;
	}

	public void setPositionX(String positionX) {
		this.positionX = positionX;
	}

	public String getPositionY() {
		return positionY;
	}

	public void setPositionY(String positionY) {
		this.positionY = positionY;
	}

	public String getPositionZ() {
		return positionZ;
	}

	public void setPositionZ(String positionZ) {
		this.positionZ = positionZ;
	}

	public String getWgt() {
		return wgt;
	}

	public void setWgt(String wgt) {
		this.wgt = wgt;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOKR [deviceId=");
		builder.append(deviceId);
		builder.append(", epcCd=");
		builder.append(epcCd);
		builder.append(", positionX=");
		builder.append(positionX);
		builder.append(", positionY=");
		builder.append(positionY);
		builder.append(", positionZ=");
		builder.append(positionZ);
		builder.append(", wgt=");
		builder.append(wgt);
//		builder.append(", workList=");
//		builder.append(workList);
		builder.append("]");
		return builder.toString();
	}

}