package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InterfaceBaseVOGodomallItem implements Serializable {

	private static final long serialVersionUID = -6445511060732512635L;

	private List<WorkListBaseVOGodomallItem> goodsData;

	public List<WorkListBaseVOGodomallItem> getGoodsData() {
		return goodsData;
	}

	public void setGoodsData(List<WorkListBaseVOGodomallItem> goodsData) {
		this.goodsData = goodsData;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOGodomallItem [goodsData=");
		builder.append(goodsData);
		builder.append("]");
		return builder.toString();
	}
	

}
