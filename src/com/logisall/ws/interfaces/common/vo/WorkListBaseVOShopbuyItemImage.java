package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVOShopbuyItemImage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String  displayOrder;
	private String  imageId;
	private String  imageUrl;
	private String  mainImage;
	private String  mainYn;
	private String  mallImageNo;
	private String  mallProductNo;
	private String  originImageUrl;
	private String  productImageNo;
	
	public String getDisplayOrder(){
	 return displayOrder; 
	} 
	   
	public void setDisplayOrder(String displayOrder) 
	{
	 this.displayOrder=displayOrder;
	} 
	  
	public String getImageId(){
	 return imageId; 
	} 
	   
	public void setImageId(String imageId) 
	{
	 this.imageId=imageId;
	} 
	  
	public String getImageUrl(){
	 return imageUrl; 
	} 
	   
	public void setImageUrl(String imageUrl) 
	{
	 this.imageUrl=imageUrl;
	} 
	  
	public String getMainImage(){
	 return mainImage; 
	} 
	   
	public void setMainImage(String mainImage) 
	{
	 this.mainImage=mainImage;
	} 
	  
	public String getMainYn(){
	 return mainYn; 
	} 
	   
	public void setMainYn(String mainYn) 
	{
	 this.mainYn=mainYn;
	} 
	  
	public String getMallImageNo(){
	 return mallImageNo; 
	} 
	   
	public void setMallImageNo(String mallImageNo) 
	{
	 this.mallImageNo=mallImageNo;
	} 
	  
	public String getMallProductNo(){
	 return mallProductNo; 
	} 
	   
	public void setMallProductNo(String mallProductNo) 
	{
	 this.mallProductNo=mallProductNo;
	} 
	  
	public String getOriginImageUrl(){
	 return originImageUrl; 
	} 
	   
	public void setOriginImageUrl(String originImageUrl) 
	{
	 this.originImageUrl=originImageUrl;
	} 
	  
	public String getProductImageNo(){
	 return productImageNo; 
	} 
	   
	public void setProductImageNo(String productImageNo) 
	{
	 this.productImageNo=productImageNo;
	} 
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOShopbuyItemImage [displayOrder=");
		builder.append(displayOrder);
		builder.append(",imageId=");
		builder.append(imageId);
		builder.append(",imageUrl=");
		builder.append(imageUrl);
		builder.append(",mainImage=");
		builder.append(mainImage);
		builder.append(",mainYn=");
		builder.append(mainYn);
		builder.append(",mallImageNo=");
		builder.append(mallImageNo);
		builder.append(",mallProductNo=");
		builder.append(mallProductNo);
		builder.append(",originImageUrl=");
		builder.append(originImageUrl);
		builder.append(",productImageNo=");
		builder.append(productImageNo);
		builder.append("]");
		return builder.toString();
	}

}