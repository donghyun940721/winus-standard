package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;

	private String eventCd;
	private String stateCd;
	private String registSeq;
	private String workSeq;
	private String localSlipNo;
	private String customerSlipNo;
	private String otherSlipNo;
	private String baseLocCd;	
	private String otherPartyLocCd;
	private String customerLocCd;
	private String warehouseId;
	private String warehouseNm;
	private String customerId;
	private String customerNm;
	private Date shippingDt;
	private Date receivingDt;
	private Date inOutDt;
	private Date eventDt;
	private String forwarderCd;
	private String forwarderNm;
	private String carId;

	private String reasonCd;
	private String unitWeight;
	private String lockStateCd;

	private List<PackingInfoVO> packingList;
	private List<ItemInfoVO> itemList;

	public String getEventCd() {
		return eventCd;
	}

	public void setEventCd(String eventCd) {
		this.eventCd = eventCd;
	}

	public String getStateCd() {
		return stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public String getRegistSeq() {
		return registSeq;
	}

	public void setRegistSeq(String registSeq) {
		this.registSeq = registSeq;
	}

	public String getWorkSeq() {
		return workSeq;
	}

	public void setWorkSeq(String workSeq) {
		this.workSeq = workSeq;
	}

	public String getLocalSlipNo() {
		return localSlipNo;
	}

	public void setLocalSlipNo(String localSlipNo) {
		this.localSlipNo = localSlipNo;
	}

	public String getCustomerSlipNo() {
		return customerSlipNo;
	}

	public void setCustomerSlipNo(String customerSlipNo) {
		this.customerSlipNo = customerSlipNo;
	}

	public String getOtherSlipNo() {
		return otherSlipNo;
	}

	public void setOtherSlipNo(String otherSlipNo) {
		this.otherSlipNo = otherSlipNo;
	}

/*
	public String getLocalLocCd() {
		return localLocCd;
	}

	public void setLocalLocCd(String localLocCd) {
		this.localLocCd = localLocCd;
	}
*/

	public String getBaseLocCd() {
		return baseLocCd;
	}

	public void setBaseLocCd(String baseLocCd) {
		this.baseLocCd = baseLocCd;
	}

	public String getOtherPartyLocCd() {
		return otherPartyLocCd;
	}

	public void setOtherPartyLocCd(String otherPartyLocCd) {
		this.otherPartyLocCd = otherPartyLocCd;
	}

	public String getCustomerLocCd() {
		return customerLocCd;
	}

	public void setCustomerLocCd(String customerLocCd) {
		this.customerLocCd = customerLocCd;
	}

	public String getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouseNm() {
		return warehouseNm;
	}

	public void setWarehouseNm(String warehouseNm) {
		this.warehouseNm = warehouseNm;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNm() {
		return customerNm;
	}

	public void setCustomerNm(String customerNm) {
		this.customerNm = customerNm;
	}

	public Date getShippingDt() {
		return shippingDt;
	}

	public void setShippingDt(Date shippingDt) {
		this.shippingDt = shippingDt;
	}

	public Date getReceivingDt() {
		return receivingDt;
	}

	public void setReceivingDt(Date receivingDt) {
		this.receivingDt = receivingDt;
	}

	public Date getInOutDt() {
		return inOutDt;
	}

	public void setInOutDt(Date inOutDt) {
		this.inOutDt = inOutDt;
	}

	public Date getEventDt() {
		return eventDt;
	}

	public void setEventDt(Date eventDt) {
		this.eventDt = eventDt;
	}

	public String getForwarderCd() {
		return forwarderCd;
	}

	public void setForwarderCd(String forwarderCd) {
		this.forwarderCd = forwarderCd;
	}

	public String getForwarderNm() {
		return forwarderNm;
	}

	public void setForwarderNm(String forwarderNm) {
		this.forwarderNm = forwarderNm;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getReasonCd() {
		return reasonCd;
	}

	public void setReasonCd(String reasonCd) {
		this.reasonCd = reasonCd;
	}

	public String getUnitWeight() {
		return unitWeight;
	}

	public void setUnitWeight(String unitWeight) {
		this.unitWeight = unitWeight;
	}
/*
	public String getLockReasonCd() {
		return lockReasonCd;
	}

	public void setLockReasonCd(String lockReasonCd) {
		this.lockReasonCd = lockReasonCd;
	}
*/
	public String getLockStateCd() {
		return lockStateCd;
	}

	public void setLockStateCd(String lockStateCd) {
		this.lockStateCd = lockStateCd;
	}

	public List<ItemInfoVO> getItemList() {
		return itemList;
	}

	public void setItemList(List<ItemInfoVO> itemList) {
		this.itemList = itemList;
	}

	public List<PackingInfoVO> getPackingList() {
		return packingList;
	}

	public void setPackingList(List<PackingInfoVO> packingList) {
		this.packingList = packingList;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVO [eventCd=");
		builder.append(eventCd);
		builder.append(", stateCd=");
		builder.append(stateCd);
		builder.append(", registSeq=");
		builder.append(registSeq);
		builder.append(", workSeq=");
		builder.append(workSeq);
		builder.append(", localSlipNo=");
		builder.append(localSlipNo);
		builder.append(", customerSlipNo=");
		builder.append(customerSlipNo);
		builder.append(", otherSlipNo=");
		builder.append(otherSlipNo);
		builder.append(", baseLocCd=");
		builder.append(baseLocCd);
		builder.append(", otherPartyLocCd=");
		builder.append(otherPartyLocCd);
		builder.append(", customerLocCd=");
		builder.append(customerLocCd);
		builder.append(", warehouseId=");
		builder.append(warehouseId);
		builder.append(", warehouseNm=");
		builder.append(warehouseNm);
		builder.append(", customerId=");
		builder.append(customerId);
		builder.append(", customerNm=");
		builder.append(customerNm);
		builder.append(", shippingDt=");
		builder.append(shippingDt);
		builder.append(", receivingDt=");
		builder.append(receivingDt);
		builder.append(", inOutDt=");
		builder.append(inOutDt);
		builder.append(", eventDt=");
		builder.append(eventDt);
		builder.append(", forwarderCd=");
		builder.append(forwarderCd);
		builder.append(", forwarderNm=");
		builder.append(forwarderNm);
		builder.append(", carId=");
		builder.append(carId);
		builder.append(", reasonCd=");
		builder.append(reasonCd);
		builder.append(", unitWeight=");
		builder.append(unitWeight);
		builder.append(", lockStateCd=");
		builder.append(lockStateCd);
		builder.append(", packingList=");
		builder.append(packingList);
		builder.append(", itemList=");
		builder.append(itemList);
		builder.append("]");
		return builder.toString();
	}


	
}
