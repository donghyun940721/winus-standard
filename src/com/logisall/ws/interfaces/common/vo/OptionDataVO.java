package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;

public class OptionDataVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;

	private String idx;
	private String sno;
	private String goodsNo;
	private String optionNo;
	private String optionCode;
	private String optionValue1;
	private String optionValue2;
	private String optionValue3;
	private String optionValue4;
	private String optionValue5;
	private String optionSellFl;
	private String optionPrice;
	private String optionDeliveryCode;
	private String confirmRequestStock;
	private String optionDeliveryFl;
	private String reqDt;
	private String modDt;
	private String optionCostPrice;
	private String optionSellCode;
	private String deliverySmsSent;
	private String optionViewFl;
	private String optionImage;
	private String sellStopStock;
	private String stockCnt;
	private String sellStopFl;
	private String optionMemo;
	
	public String getIdx() {return idx;}public void setIdx(String idx) {this.idx = idx;}
	public String getSno() {return sno;}public void setSno(String sno) {this.sno = sno;}
	public String getGoodsNo() {return goodsNo;}public void setGoodsNo(String goodsNo) {this.goodsNo = goodsNo;}
	public String getOptionNo() {return optionNo;}public void setOptionNo(String optionNo) {this.optionNo = optionNo;}
	public String getOptionCode() {return optionCode;}public void setOptionCode(String optionCode) {this.optionCode = optionCode;}
	public String getOptionValue1() {return optionValue1;}public void setOptionValue1(String optionValue1) {this.optionValue1 = optionValue1;}
	public String getOptionValue2() {return optionValue2;}public void setOptionValue2(String optionValue2) {this.optionValue2 = optionValue2;}
	public String getOptionValue3() {return optionValue3;}public void setOptionValue3(String optionValue3) {this.optionValue3 = optionValue3;}
	public String getOptionValue4() {return optionValue4;}public void setOptionValue4(String optionValue4) {this.optionValue4 = optionValue4;}
	public String getOptionValue5() {return optionValue5;}public void setOptionValue5(String optionValue5) {this.optionValue5 = optionValue5;}
	public String getOptionSellFl() {return optionSellFl;}public void setOptionSellFl(String optionSellFl) {this.optionSellFl = optionSellFl;}
	public String getOptionPrice() {return optionPrice;}public void setOptionPrice(String optionPrice) {this.optionPrice = optionPrice;}
	public String getOptionDeliveryCode() {return optionDeliveryCode;}public void setOptionDeliveryCode(String optionDeliveryCode) {this.optionDeliveryCode = optionDeliveryCode;}
	public String getConfirmRequestStock() {return confirmRequestStock;}public void setConfirmRequestStock(String confirmRequestStock) {this.confirmRequestStock = confirmRequestStock;}
	public String getOptionDeliveryFl() {return optionDeliveryFl;}public void setOptionDeliveryFl(String optionDeliveryFl) {this.optionDeliveryFl = optionDeliveryFl;}
	public String getReqDt() {return reqDt;}public void setReqDt(String reqDt) {this.reqDt = reqDt;}
	public String getModDt() {return modDt;}public void setModDt(String modDt) {this.modDt = modDt;}
	public String getOptionCostPrice() {return optionCostPrice;}public void setOptionCostPrice(String optionCostPrice) {this.optionCostPrice = optionCostPrice;}
	public String getOptionSellCode() {return optionSellCode;}public void setOptionSellCode(String optionSellCode) {this.optionSellCode = optionSellCode;}
	public String getDeliverySmsSent() {return deliverySmsSent;}public void setDeliverySmsSent(String deliverySmsSent) {this.deliverySmsSent = deliverySmsSent;}
	public String getOptionViewFl() {return optionViewFl;}public void setOptionViewFl(String optionViewFl) {this.optionViewFl = optionViewFl;}
	public String getOptionImage() {return optionImage;}public void setOptionImage(String optionImage) {this.optionImage = optionImage;}
	public String getSellStopStock() {return sellStopStock;}public void setSellStopStock(String sellStopStock) {this.sellStopStock = sellStopStock;}
	public String getStockCnt() {return stockCnt;}public void setStockCnt(String stockCnt) {this.stockCnt = stockCnt;}
	public String getSellStopFl() {return sellStopFl;}public void setSellStopFl(String sellStopFl) {this.sellStopFl = sellStopFl;}
	public String getOptionMemo() {return optionMemo;}public void setOptionMemo(String optionMemo) {this.optionMemo = optionMemo;}


	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("OptionDataVO [idx=");
		builder.append(idx);
		builder.append(", sno=");
		builder.append(sno);
		builder.append(", goodsNo=");
		builder.append(goodsNo);
		builder.append(", optionNo=");
		builder.append(optionNo);
		builder.append(", optionCode=");
		builder.append(optionCode);
		builder.append(", optionValue1=");
		builder.append(optionValue1);
		builder.append(", optionValue2=");
		builder.append(optionValue2);
		builder.append(", optionValue3=");
		builder.append(optionValue3);
		builder.append(", optionValue4=");
		builder.append(optionValue4);
		builder.append(", optionValue5=");
		builder.append(optionValue5);
		builder.append(", optionSellFl=");
		builder.append(optionSellFl);
		builder.append(", optionPrice=");
		builder.append(optionPrice);
		builder.append(", optionDeliveryCode=");
		builder.append(optionDeliveryCode);
		builder.append(", confirmRequestStock=");
		builder.append(confirmRequestStock);
		builder.append(", optionDeliveryFl=");
		builder.append(optionDeliveryFl);
		builder.append(", reqDt=");
		builder.append(reqDt);
		builder.append(", modDt=");
		builder.append(modDt);
		builder.append(", optionCostPrice=");
		builder.append(optionCostPrice);
		builder.append(", optionSellCode=");
		builder.append(optionSellCode);
		builder.append(", deliverySmsSent=");
		builder.append(deliverySmsSent);
		builder.append(", optionViewFl=");
		builder.append(optionViewFl);
		builder.append(", optionImage=");
		builder.append(optionImage);
		builder.append(", sellStopStock=");
		builder.append(sellStopStock);
		builder.append(", stockCnt=");
		builder.append(stockCnt);
		builder.append(", sellStopFl=");
		builder.append(sellStopFl);
		builder.append(", optionMemo=");
		builder.append(optionMemo);
		builder.append("]");
		return builder.toString();
	}
}
