package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;

public class OrderGoodsDataVO implements Serializable {

	private static final long serialVersionUID = -3844727177593095135L;

	private String idx;
	private String sno;
	private String orderNo;
	private String mallSno;
	private String apiOrderGoodsNo;
	private String orderCd;
	private String orderGroupCd;
	private String eventSno;
	private String orderStatus;
	private String orderDeliverySno;
	private String invoiceCompanySno;
	private String invoiceNo;
	private String scmNo;
	private String purchaseNo;
	private String commission;
	private String scmAdjustAfterNo;
	private String goodsType;
	private String timeSaleFl;
	private String parentMustFl;
	private String parentGoodsNo;
	private String goodsNo;
	private String listImageData;
	private String goodsCd;
	private String goodsModelNo;
	private String goodsNm;
	private String goodsNmStandard;
	private String goodsCnt;
	private String goodsPrice;
	private String divisionUseDeposit;
	private String divisionUseMileage;
	private String divisionGoodsDeliveryUseDeposit;
	private String divisionGoodsDeliveryUseMileage;
	private String divisionCouponOrderDcPrice;
	private String divisionCouponOrderMileage;
	private String addGoodsPrice;
	private String optionPrice;
	private String optionCostPrice;
	private String optionTextPrice;
	private String fixedPrice;
	private String costPrice;
	private String goodsDcPrice;
	private String memberDcPrice;
	private String memberOverlapDcPrice;
	private String couponGoodsDcPrice;
	private String timeSalePrice;
	private String brandBankSalePrice;
	private String myappDcPrice;
	private String goodsDeliveryCollectPrice;
	private String goodsMileage;
	private String memberMileage;
	private String couponGoodsMileage;
	private String goodsDeliveryCollectFl;
	private String minusDepositFl;
	private String minusRestoreDepositFl;
	private String minusMileageFl;
	private String minusRestoreMileageFl;
	private String plusMileageFl;
	private String plusRestoreMileageFl;
	private String minusStockFl;
	private String minusRestoreStockFl;
	private String optionSno;
	private String optionInfo;
	private String optionTextInfo;
	private String cateAllCd;
	private String hscode;
	private String cancelDt;
	private String paymentDt;
	private String invoiceDt;
	private String deliveryDt;
	private String deliveryCompleteDt;
	private String finishDt;
	private String mileageGiveDt;
	private String checkoutData;
	private String statisticsOrderFl;
	private String statisticsGoodsFl;
	private String sendSmsFl;
	private String deliveryMethodFl;
	private String enuri;
	private String goodsDiscountInfo;
	private String goodsMileageAddInfo;
	private String inflow;
	private String linkMainTheme;
	private String visitAddress;	

	private ClaimDataVO claimData;


	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}
	
	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getMallSno() {
		return mallSno;
	}

	public void setMallSno(String mallSno) {
		this.mallSno = mallSno;
	}

	public String getApiOrderGoodsNo() {
		return apiOrderGoodsNo;
	}

	public void setApiOrderGoodsNo(String apiOrderGoodsNo) {
		this.apiOrderGoodsNo = apiOrderGoodsNo;
	}

	public String getOrderCd() {
		return orderCd;
	}

	public void setOrderCd(String orderCd) {
		this.orderCd = orderCd;
	}

	public String getOrderGroupCd() {
		return orderGroupCd;
	}

	public void setOrderGroupCd(String orderGroupCd) {
		this.orderGroupCd = orderGroupCd;
	}

	public String getEventSno() {
		return eventSno;
	}

	public void setEventSno(String eventSno) {
		this.eventSno = eventSno;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderDeliverySno() {
		return orderDeliverySno;
	}

	public void setOrderDeliverySno(String orderDeliverySno) {
		this.orderDeliverySno = orderDeliverySno;
	}

	public String getInvoiceCompanySno() {
		return invoiceCompanySno;
	}

	public void setInvoiceCompanySno(String invoiceCompanySno) {
		this.invoiceCompanySno = invoiceCompanySno;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getScmNo() {
		return scmNo;
	}

	public void setScmNo(String scmNo) {
		this.scmNo = scmNo;
	}

	public String getPurchaseNo() {
		return purchaseNo;
	}

	public void setPurchaseNo(String purchaseNo) {
		this.purchaseNo = purchaseNo;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getScmAdjustAfterNo() {
		return scmAdjustAfterNo;
	}

	public void setScmAdjustAfterNo(String scmAdjustAfterNo) {
		this.scmAdjustAfterNo = scmAdjustAfterNo;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public String getTimeSaleFl() {
		return timeSaleFl;
	}

	public void setTimeSaleFl(String timeSaleFl) {
		this.timeSaleFl = timeSaleFl;
	}

	public String getParentMustFl() {
		return parentMustFl;
	}

	public void setParentMustFl(String parentMustFl) {
		this.parentMustFl = parentMustFl;
	}

	public String getParentGoodsNo() {
		return parentGoodsNo;
	}

	public void setParentGoodsNo(String parentGoodsNo) {
		this.parentGoodsNo = parentGoodsNo;
	}

	public String getGoodsNo() {
		return goodsNo;
	}

	public void setGoodsNo(String goodsNo) {
		this.goodsNo = goodsNo;
	}

	public String getListImageData() {
		return listImageData;
	}

	public void setListImageData(String listImageData) {
		this.listImageData = listImageData;
	}

	public String getGoodsCd() {
		return goodsCd;
	}

	public void setGoodsCd(String goodsCd) {
		this.goodsCd = goodsCd;
	}

	public String getGoodsModelNo() {
		return goodsModelNo;
	}

	public void setGoodsModelNo(String goodsModelNo) {
		this.goodsModelNo = goodsModelNo;
	}

	public String getGoodsNm() {
		return goodsNm;
	}

	public void setGoodsNm(String goodsNm) {
		this.goodsNm = goodsNm;
	}

	public String getGoodsNmStandard() {
		return goodsNmStandard;
	}

	public void setGoodsNmStandard(String goodsNmStandard) {
		this.goodsNmStandard = goodsNmStandard;
	}

	public String getGoodsCnt() {
		return goodsCnt;
	}

	public void setGoodsCnt(String goodsCnt) {
		this.goodsCnt = goodsCnt;
	}

	public String getGoodsPrice() {
		return goodsPrice;
	}

	public void setGoodsPrice(String goodsPrice) {
		this.goodsPrice = goodsPrice;
	}

	public String getDivisionUseDeposit() {
		return divisionUseDeposit;
	}

	public void setDivisionUseDeposit(String divisionUseDeposit) {
		this.divisionUseDeposit = divisionUseDeposit;
	}

	public String getDivisionUseMileage() {
		return divisionUseMileage;
	}

	public void setDivisionUseMileage(String divisionUseMileage) {
		this.divisionUseMileage = divisionUseMileage;
	}

	public String getDivisionGoodsDeliveryUseDeposit() {
		return divisionGoodsDeliveryUseDeposit;
	}

	public void setDivisionGoodsDeliveryUseDeposit(String divisionGoodsDeliveryUseDeposit) {
		this.divisionGoodsDeliveryUseDeposit = divisionGoodsDeliveryUseDeposit;
	}

	public String getDivisionGoodsDeliveryUseMileage() {
		return divisionGoodsDeliveryUseMileage;
	}

	public void setDivisionGoodsDeliveryUseMileage(String divisionGoodsDeliveryUseMileage) {
		this.divisionGoodsDeliveryUseMileage = divisionGoodsDeliveryUseMileage;
	}

	public String getDivisionCouponOrderDcPrice() {
		return divisionCouponOrderDcPrice;
	}

	public void setDivisionCouponOrderDcPrice(String divisionCouponOrderDcPrice) {
		this.divisionCouponOrderDcPrice = divisionCouponOrderDcPrice;
	}

	public String getDivisionCouponOrderMileage() {
		return divisionCouponOrderMileage;
	}

	public void setDivisionCouponOrderMileage(String divisionCouponOrderMileage) {
		this.divisionCouponOrderMileage = divisionCouponOrderMileage;
	}

	public String getAddGoodsPrice() {
		return addGoodsPrice;
	}

	public void setAddGoodsPrice(String addGoodsPrice) {
		this.addGoodsPrice = addGoodsPrice;
	}

	public String getOptionPrice() {
		return optionPrice;
	}

	public void setOptionPrice(String optionPrice) {
		this.optionPrice = optionPrice;
	}

	public String getOptionCostPrice() {
		return optionCostPrice;
	}

	public void setOptionCostPrice(String optionCostPrice) {
		this.optionCostPrice = optionCostPrice;
	}

	public String getOptionTextPrice() {
		return optionTextPrice;
	}

	public void setOptionTextPrice(String optionTextPrice) {
		this.optionTextPrice = optionTextPrice;
	}

	public String getFixedPrice() {
		return fixedPrice;
	}

	public void setFixedPrice(String fixedPrice) {
		this.fixedPrice = fixedPrice;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public String getGoodsDcPrice() {
		return goodsDcPrice;
	}

	public void setGoodsDcPrice(String goodsDcPrice) {
		this.goodsDcPrice = goodsDcPrice;
	}

	public String getMemberDcPrice() {
		return memberDcPrice;
	}

	public void setMemberDcPrice(String memberDcPrice) {
		this.memberDcPrice = memberDcPrice;
	}

	public String getMemberOverlapDcPrice() {
		return memberOverlapDcPrice;
	}

	public void setMemberOverlapDcPrice(String memberOverlapDcPrice) {
		this.memberOverlapDcPrice = memberOverlapDcPrice;
	}

	public String getCouponGoodsDcPrice() {
		return couponGoodsDcPrice;
	}

	public void setCouponGoodsDcPrice(String couponGoodsDcPrice) {
		this.couponGoodsDcPrice = couponGoodsDcPrice;
	}

	public String getTimeSalePrice() {
		return timeSalePrice;
	}

	public void setTimeSalePrice(String timeSalePrice) {
		this.timeSalePrice = timeSalePrice;
	}

	public String getBrandBankSalePrice() {
		return brandBankSalePrice;
	}

	public void setBrandBankSalePrice(String brandBankSalePrice) {
		this.brandBankSalePrice = brandBankSalePrice;
	}

	public String getMyappDcPrice() {
		return myappDcPrice;
	}

	public void setMyappDcPrice(String myappDcPrice) {
		this.myappDcPrice = myappDcPrice;
	}

	public String getGoodsDeliveryCollectPrice() {
		return goodsDeliveryCollectPrice;
	}

	public void setGoodsDeliveryCollectPrice(String goodsDeliveryCollectPrice) {
		this.goodsDeliveryCollectPrice = goodsDeliveryCollectPrice;
	}

	public String getGoodsMileage() {
		return goodsMileage;
	}

	public void setGoodsMileage(String goodsMileage) {
		this.goodsMileage = goodsMileage;
	}

	public String getMemberMileage() {
		return memberMileage;
	}

	public void setMemberMileage(String memberMileage) {
		this.memberMileage = memberMileage;
	}

	public String getCouponGoodsMileage() {
		return couponGoodsMileage;
	}

	public void setCouponGoodsMileage(String couponGoodsMileage) {
		this.couponGoodsMileage = couponGoodsMileage;
	}

	public String getGoodsDeliveryCollectFl() {
		return goodsDeliveryCollectFl;
	}

	public void setGoodsDeliveryCollectFl(String goodsDeliveryCollectFl) {
		this.goodsDeliveryCollectFl = goodsDeliveryCollectFl;
	}

	public String getMinusDepositFl() {
		return minusDepositFl;
	}

	public void setMinusDepositFl(String minusDepositFl) {
		this.minusDepositFl = minusDepositFl;
	}

	public String getMinusRestoreDepositFl() {
		return minusRestoreDepositFl;
	}

	public void setMinusRestoreDepositFl(String minusRestoreDepositFl) {
		this.minusRestoreDepositFl = minusRestoreDepositFl;
	}

	public String getMinusMileageFl() {
		return minusMileageFl;
	}

	public void setMinusMileageFl(String minusMileageFl) {
		this.minusMileageFl = minusMileageFl;
	}

	public String getMinusRestoreMileageFl() {
		return minusRestoreMileageFl;
	}

	public void setMinusRestoreMileageFl(String minusRestoreMileageFl) {
		this.minusRestoreMileageFl = minusRestoreMileageFl;
	}

	public String getPlusMileageFl() {
		return plusMileageFl;
	}

	public void setPlusMileageFl(String plusMileageFl) {
		this.plusMileageFl = plusMileageFl;
	}

	public String getPlusRestoreMileageFl() {
		return plusRestoreMileageFl;
	}

	public void setPlusRestoreMileageFl(String plusRestoreMileageFl) {
		this.plusRestoreMileageFl = plusRestoreMileageFl;
	}

	public String getMinusStockFl() {
		return minusStockFl;
	}

	public void setMinusStockFl(String minusStockFl) {
		this.minusStockFl = minusStockFl;
	}

	public String getMinusRestoreStockFl() {
		return minusRestoreStockFl;
	}

	public void setMinusRestoreStockFl(String minusRestoreStockFl) {
		this.minusRestoreStockFl = minusRestoreStockFl;
	}

	public String getOptionSno() {
		return optionSno;
	}

	public void setOptionSno(String optionSno) {
		this.optionSno = optionSno;
	}

	public String getOptionInfo() {
		return optionInfo;
	}

	public void setOptionInfo(String optionInfo) {
		this.optionInfo = optionInfo;
	}

	public String getOptionTextInfo() {
		return optionTextInfo;
	}

	public void setOptionTextInfo(String optionTextInfo) {
		this.optionTextInfo = optionTextInfo;
	}

	public String getCateAllCd() {
		return cateAllCd;
	}

	public void setCateAllCd(String cateAllCd) {
		this.cateAllCd = cateAllCd;
	}

	public String getHscode() {
		return hscode;
	}

	public void setHscode(String hscode) {
		this.hscode = hscode;
	}

	public String getCancelDt() {
		return cancelDt;
	}

	public void setCancelDt(String cancelDt) {
		this.cancelDt = cancelDt;
	}

	public String getPaymentDt() {
		return paymentDt;
	}

	public void setPaymentDt(String paymentDt) {
		this.paymentDt = paymentDt;
	}

	public String getInvoiceDt() {
		return invoiceDt;
	}

	public void setInvoiceDt(String invoiceDt) {
		this.invoiceDt = invoiceDt;
	}

	public String getDeliveryDt() {
		return deliveryDt;
	}

	public void setDeliveryDt(String deliveryDt) {
		this.deliveryDt = deliveryDt;
	}

	public String getDeliveryCompleteDt() {
		return deliveryCompleteDt;
	}

	public void setDeliveryCompleteDt(String deliveryCompleteDt) {
		this.deliveryCompleteDt = deliveryCompleteDt;
	}

	public String getFinishDt() {
		return finishDt;
	}

	public void setFinishDt(String finishDt) {
		this.finishDt = finishDt;
	}

	public String getMileageGiveDt() {
		return mileageGiveDt;
	}

	public void setMileageGiveDt(String mileageGiveDt) {
		this.mileageGiveDt = mileageGiveDt;
	}

	public String getCheckoutData() {
		return checkoutData;
	}

	public void setCheckoutData(String checkoutData) {
		this.checkoutData = checkoutData;
	}

	public String getStatisticsOrderFl() {
		return statisticsOrderFl;
	}

	public void setStatisticsOrderFl(String statisticsOrderFl) {
		this.statisticsOrderFl = statisticsOrderFl;
	}

	public String getStatisticsGoodsFl() {
		return statisticsGoodsFl;
	}

	public void setStatisticsGoodsFl(String statisticsGoodsFl) {
		this.statisticsGoodsFl = statisticsGoodsFl;
	}

	public String getSendSmsFl() {
		return sendSmsFl;
	}

	public void setSendSmsFl(String sendSmsFl) {
		this.sendSmsFl = sendSmsFl;
	}

	public String getDeliveryMethodFl() {
		return deliveryMethodFl;
	}

	public void setDeliveryMethodFl(String deliveryMethodFl) {
		this.deliveryMethodFl = deliveryMethodFl;
	}

	public String getEnuri() {
		return enuri;
	}

	public void setEnuri(String enuri) {
		this.enuri = enuri;
	}

	public String getGoodsDiscountInfo() {
		return goodsDiscountInfo;
	}

	public void setGoodsDiscountInfo(String goodsDiscountInfo) {
		this.goodsDiscountInfo = goodsDiscountInfo;
	}

	public String getGoodsMileageAddInfo() {
		return goodsMileageAddInfo;
	}

	public void setGoodsMileageAddInfo(String goodsMileageAddInfo) {
		this.goodsMileageAddInfo = goodsMileageAddInfo;
	}

	public String getInflow() {
		return inflow;
	}

	public void setInflow(String inflow) {
		this.inflow = inflow;
	}

	public String getLinkMainTheme() {
		return linkMainTheme;
	}

	public void setLinkMainTheme(String linkMainTheme) {
		this.linkMainTheme = linkMainTheme;
	}

	public String getVisitAddress() {
		return visitAddress;
	}

	public void setVisitAddress(String visitAddress) {
		this.visitAddress = visitAddress;
	}

	public ClaimDataVO getClaimData() {
		return claimData;
	}

	public void setClaimData(ClaimDataVO claimData) {
		this.claimData = claimData;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("OrderGoodsDataVO [idx=");
		builder.append(idx);
		builder.append(", sno=");
		builder.append(sno);
		builder.append(", orderNo=");
		builder.append(orderNo);
		builder.append(", mallSno=");
		builder.append(mallSno);
		builder.append(", apiOrderGoodsNo=");
		builder.append(apiOrderGoodsNo);
		builder.append(", orderCd=");
		builder.append(orderCd);
		builder.append(", orderGroupCd=");
		builder.append(orderGroupCd);
		builder.append(", eventSno=");
		builder.append(eventSno);
		builder.append(", orderStatus=");
		builder.append(orderStatus);
		builder.append(", orderDeliverySno=");
		builder.append(orderDeliverySno);
		builder.append(", invoiceCompanySno=");
		builder.append(invoiceCompanySno);
		builder.append(", invoiceNo=");
		builder.append(invoiceNo);
		builder.append(", scmNo=");
		builder.append(scmNo);
		builder.append(", purchaseNo=");
		builder.append(purchaseNo);
		builder.append(", commission=");
		builder.append(commission);
		builder.append(", scmAdjustAfterNo=");
		builder.append(scmAdjustAfterNo);
		builder.append(", goodsType=");
		builder.append(goodsType);
		builder.append(", timeSaleFl=");
		builder.append(timeSaleFl);
		builder.append(", parentMustFl=");
		builder.append(parentMustFl);
		builder.append(", parentGoodsNo=");
		builder.append(parentGoodsNo);
		builder.append(", goodsNo=");
		builder.append(goodsNo);
		builder.append(", listImageData=");
		builder.append(listImageData);
		builder.append(", goodsCd=");
		builder.append(goodsCd);
		builder.append(", goodsModelNo=");
		builder.append(goodsModelNo);
		builder.append(", goodsNm=");
		builder.append(goodsNm);
		builder.append(", goodsNmStandard=");
		builder.append(goodsNmStandard);
		builder.append(", goodsCnt=");
		builder.append(goodsCnt);
		builder.append(", goodsPrice=");
		builder.append(goodsPrice);
		builder.append(", divisionUseDeposit=");
		builder.append(divisionUseDeposit);
		builder.append(", divisionUseMileage=");
		builder.append(divisionUseMileage);
		builder.append(", divisionGoodsDeliveryUseDeposit=");
		builder.append(divisionGoodsDeliveryUseDeposit);
		builder.append(", divisionGoodsDeliveryUseMileage=");
		builder.append(divisionGoodsDeliveryUseMileage);
		builder.append(", divisionCouponOrderDcPrice=");
		builder.append(divisionCouponOrderDcPrice);
		builder.append(", divisionCouponOrderMileage=");
		builder.append(divisionCouponOrderMileage);
		builder.append(", addGoodsPrice=");
		builder.append(addGoodsPrice);
		builder.append(", optionPrice=");
		builder.append(optionPrice);
		builder.append(", optionCostPrice=");
		builder.append(optionCostPrice);
		builder.append(", optionTextPrice=");
		builder.append(optionTextPrice);
		builder.append(", fixedPrice=");
		builder.append(fixedPrice);
		builder.append(", costPrice=");
		builder.append(costPrice);
		builder.append(", goodsDcPrice=");
		builder.append(goodsDcPrice);
		builder.append(", memberDcPrice=");
		builder.append(memberDcPrice);
		builder.append(", memberOverlapDcPrice=");
		builder.append(memberOverlapDcPrice);
		builder.append(", couponGoodsDcPrice=");
		builder.append(couponGoodsDcPrice);
		builder.append(", timeSalePrice=");
		builder.append(timeSalePrice);
		builder.append(", brandBankSalePrice=");
		builder.append(brandBankSalePrice);
		builder.append(", myappDcPrice=");
		builder.append(myappDcPrice);
		builder.append(", goodsDeliveryCollectPrice=");
		builder.append(goodsDeliveryCollectPrice);
		builder.append(", goodsMileage=");
		builder.append(goodsMileage);
		builder.append(", memberMileage=");
		builder.append(memberMileage);
		builder.append(", couponGoodsMileage=");
		builder.append(couponGoodsMileage);
		builder.append(", goodsDeliveryCollectFl=");
		builder.append(goodsDeliveryCollectFl);
		builder.append(", minusDepositFl=");
		builder.append(minusDepositFl);
		builder.append(", minusRestoreDepositFl=");
		builder.append(minusRestoreDepositFl);
		builder.append(", minusMileageFl=");
		builder.append(minusMileageFl);
		builder.append(", minusRestoreMileageFl=");
		builder.append(minusRestoreMileageFl);
		builder.append(", plusMileageFl=");
		builder.append(plusMileageFl);
		builder.append(", plusRestoreMileageFl=");
		builder.append(plusRestoreMileageFl);
		builder.append(", minusStockFl=");
		builder.append(minusStockFl);
		builder.append(", minusRestoreStockFl=");
		builder.append(minusRestoreStockFl);
		builder.append(", optionSno=");
		builder.append(optionSno);
		builder.append(", optionInfo=");
		builder.append(optionInfo);
		builder.append(", optionTextInfo=");
		builder.append(optionTextInfo);
		builder.append(", cateAllCd=");
		builder.append(cateAllCd);
		builder.append(", hscode=");
		builder.append(hscode);
		builder.append(", cancelDt=");
		builder.append(cancelDt);
		builder.append(", paymentDt=");
		builder.append(paymentDt);
		builder.append(", invoiceDt=");
		builder.append(invoiceDt);
		builder.append(", deliveryDt=");
		builder.append(deliveryDt);
		builder.append(", deliveryCompleteDt=");
		builder.append(deliveryCompleteDt);
		builder.append(", finishDt=");
		builder.append(finishDt);
		builder.append(", mileageGiveDt=");
		builder.append(mileageGiveDt);
		builder.append(", checkoutData=");
		builder.append(checkoutData);
		builder.append(", statisticsOrderFl=");
		builder.append(statisticsOrderFl);
		builder.append(", statisticsGoodsFl=");
		builder.append(statisticsGoodsFl);
		builder.append(", sendSmsFl=");
		builder.append(sendSmsFl);
		builder.append(", deliveryMethodFl=");
		builder.append(deliveryMethodFl);
		builder.append(", enuri=");
		builder.append(enuri);
		builder.append(", goodsDiscountInfo=");
		builder.append(goodsDiscountInfo);
		builder.append(", goodsMileageAddInfo=");
		builder.append(goodsMileageAddInfo);
		builder.append(", inflow=");
		builder.append(inflow);
		builder.append(", linkMainTheme=");
		builder.append(linkMainTheme);
		builder.append(", visitAddress=");
		builder.append(visitAddress);
		builder.append(", claimData=");
		builder.append(claimData);
		builder.append("]");
		return builder.toString();
	}
	
}
