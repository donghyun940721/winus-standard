package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrderProductsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String orderProductNo;
	private String mallProductNo;
	private String productName;
	private String productNameEn;
	private String productManagementCd;
	private String imageUrl;
	private String hsCode;
	private String firstProductCouponDiscountAmt;
	private String lastProductCouponDiscountAmt;

	private List<OrderProductOptionsVO> orderProductOptions;
	
	public String getOrderProductNo() {
		return orderProductNo;
	}

	public void setOrderProductNo(String orderProductNo) {
		this.orderProductNo = orderProductNo;
	}

	public String getMallProductNo() {
		return mallProductNo;
	}

	public void setMallProductNo(String mallProductNo) {
		this.mallProductNo = mallProductNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductNameEn() {
		return productNameEn;
	}

	public void setProductNameEn(String productNameEn) {
		this.productNameEn = productNameEn;
	}

	public String getProductManagementCd() {
		return productManagementCd;
	}

	public void setProductManagementCd(String productManagementCd) {
		this.productManagementCd = productManagementCd;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getHsCode() {
		return hsCode;
	}

	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}

	public String getFirstProductCouponDiscountAmt() {
		return firstProductCouponDiscountAmt;
	}

	public void setFirstProductCouponDiscountAmt(String firstProductCouponDiscountAmt) {
		this.firstProductCouponDiscountAmt = firstProductCouponDiscountAmt;
	}

	public String getLastProductCouponDiscountAmt() {
		return lastProductCouponDiscountAmt;
	}

	public void setLastProductCouponDiscountAmt(String lastProductCouponDiscountAmt) {
		this.lastProductCouponDiscountAmt = lastProductCouponDiscountAmt;
	}
	
	public List<OrderProductOptionsVO> getOrderProductOptions() {
		return orderProductOptions;
	}

	public void setOrderProductOptions(List<OrderProductOptionsVO> orderProductOptions) {
		this.orderProductOptions = orderProductOptions;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("OrderProductsVO [orderProductNo=");
		builder.append(orderProductNo);
		builder.append(", mallProductNo=");
		builder.append(mallProductNo);
		builder.append(", productName=");
		builder.append(productName);
		builder.append(", productNameEn=");
		builder.append(productNameEn);
		builder.append(", productManagementCd=");
		builder.append(productManagementCd);
		builder.append(", imageUrl=");
		builder.append(imageUrl);
		builder.append(", hsCode=");
		builder.append(hsCode);
		builder.append(", firstProductCouponDiscountAmt=");
		builder.append(firstProductCouponDiscountAmt);
		builder.append(", lastProductCouponDiscountAmt=");
		builder.append(lastProductCouponDiscountAmt);
		builder.append(", orderProductOptions=");
		builder.append(orderProductOptions);
		builder.append("]");
		return builder.toString();
	}

}