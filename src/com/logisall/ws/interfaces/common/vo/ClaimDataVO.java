package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;

public class ClaimDataVO implements Serializable {

	private static final long serialVersionUID = -3844727177593095135L;

	private String refundUseMileageCommission;
	private String refundUseMileage;
	private String handleMode;
	private String handleReason;
	private String refundPrice;
	private String refundUseDepositCommission;
	private String refundDeliveryCharge;
	private String handleDt;
	private String refundDeliveryCoupon;
	private String refundDeliveryInsuranceFee;
	private String refundDeliveryUseMileage;
	private String beforeStatus;
	private String refundCharge;
	private String handleCompleteFl;
	private String handleGroupCd;
	private String handleDetailReason;
	private String handleDetailReasonShowFl;
	private String refundDeliveryUseDeposit;
	private String regDt;
	private String refundUseDeposit;

	public String getRefundUseMileageCommission() {
		return refundUseMileageCommission;
	}

	public void setRefundUseMileageCommission(String refundUseMileageCommission) {
		this.refundUseMileageCommission = refundUseMileageCommission;
	}
	public String getRefundUseMileage() {
		return refundUseMileage;
	}

	public void setRefundUseMileage(String refundUseMileage) {
		this.refundUseMileage = refundUseMileage;
	}
	public String getHandleMode() {
		return handleMode;
	}

	public void setHandleMode(String handleMode) {
		this.handleMode = handleMode;
	}
	public String getHandleReason() {
		return handleReason;
	}

	public void setHandleReason(String handleReason) {
		this.handleReason = handleReason;
	}
	public String getRefundPrice() {
		return refundPrice;
	}

	public void setRefundPrice(String refundPrice) {
		this.refundPrice = refundPrice;
	}
	public String getRefundUseDepositCommission() {
		return refundUseDepositCommission;
	}

	public void setRefundUseDepositCommission(String refundUseDepositCommission) {
		this.refundUseDepositCommission = refundUseDepositCommission;
	}
	public String getRefundDeliveryCharge() {
		return refundDeliveryCharge;
	}

	public void setHandleDt(String handleDt) {
		this.handleDt = handleDt;
	}
	public String getHandleDt() {
		return handleDt;
	}

	public void setRefundDeliveryCoupon(String refundDeliveryCoupon) {
		this.refundDeliveryCoupon = refundDeliveryCoupon;
	}
	public String getRefundDeliveryCoupon() {
		return refundDeliveryCoupon;
	}

	public void setRefundDeliveryInsuranceFee(String refundDeliveryInsuranceFee) {
		this.refundDeliveryInsuranceFee = refundDeliveryInsuranceFee;
	}
	public String getRefundDeliveryInsuranceFee() {
		return refundDeliveryInsuranceFee;
	}

	public void setRefundDeliveryUseMileage(String refundDeliveryUseMileage) {
		this.refundDeliveryUseMileage = refundDeliveryUseMileage;
	}
	public String getRefundDeliveryUseMileage() {
		return refundDeliveryUseMileage;
	}

	public void setBeforeStatus(String beforeStatus) {
		this.beforeStatus = beforeStatus;
	}
	public String getBeforeStatus() {
		return beforeStatus;
	}

	public void setRefundCharge(String refundCharge) {
		this.refundCharge = refundCharge;
	}
	public String getRefundCharge() {
		return refundCharge;
	}

	public void setHandleCompleteFl(String handleCompleteFl) {
		this.handleCompleteFl = handleCompleteFl;
	}
	public String getHandleCompleteFl() {
		return handleCompleteFl;
	}

	public void setHandleGroupCd(String handleGroupCd) {
		this.handleGroupCd = handleGroupCd;
	}
	public String getHandleGroupCd() {
		return handleGroupCd;
	}

	public void setHandleDetailReason(String handleDetailReason) {
		this.handleDetailReason = handleDetailReason;
	}
	public String getHandleDetailReason() {
		return handleDetailReason;
	}

	public void setHandleDetailReasonShowFl(String handleDetailReasonShowFl) {
		this.handleDetailReasonShowFl = handleDetailReasonShowFl;
	}
	public String getHandleDetailReasonShowFl() {
		return handleDetailReasonShowFl;
	}

	public void setRefundDeliveryUseDeposit(String refundDeliveryUseDeposit) {
		this.refundDeliveryUseDeposit = refundDeliveryUseDeposit;
	}
	public String getRefundDeliveryUseDeposit() {
		return refundDeliveryUseDeposit;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getRegDt() {
		return regDt;
	}

	public void setRefundUseDeposit(String refundUseDeposit) {
		this.refundUseDeposit = refundUseDeposit;
	}
	public String getRefundUseDeposit() {
		return refundUseDeposit;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("ClaimDataVO [refundUseMileageCommission=");
		builder.append(refundUseMileageCommission);
		builder.append(", refundUseMileage=");
		builder.append(refundUseMileage);
		builder.append(", handleMode=");
		builder.append(handleMode);
		builder.append(", handleReason=");
		builder.append(handleReason);
		builder.append(", refundPrice=");
		builder.append(refundPrice);
		builder.append(", refundUseDepositCommission=");
		builder.append(refundUseDepositCommission);
		builder.append(", refundDeliveryCharge=");
		builder.append(refundDeliveryCharge);
		builder.append(", handleDt=");
		builder.append(handleDt);
		builder.append(", refundDeliveryCoupon=");
		builder.append(refundDeliveryCoupon);
		builder.append(", refundDeliveryInsuranceFee=");
		builder.append(refundDeliveryInsuranceFee);
		builder.append(", refundDeliveryUseMileage=");
		builder.append(refundDeliveryUseMileage);
		builder.append(", beforeStatus=");
		builder.append(beforeStatus);
		builder.append(", refundCharge=");
		builder.append(refundCharge);
		builder.append(", handleCompleteFl=");
		builder.append(handleCompleteFl);
		builder.append(", handleGroupCd=");
		builder.append(handleGroupCd);
		builder.append(", handleDetailReason=");
		builder.append(handleDetailReason);
		builder.append(", handleDetailReasonShowFl=");
		builder.append(handleDetailReasonShowFl);
		builder.append(", refundDeliveryUseDeposit=");
		builder.append(refundDeliveryUseDeposit);
		builder.append(", regDt=");
		builder.append(regDt);
		builder.append(", refundUseDeposit=");
		builder.append(refundUseDeposit);
		builder.append("]");
		return builder.toString();
	}
	
}
