package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InterfaceBaseVOShopbuyItem implements Serializable {

	private static final long serialVersionUID = -6445511060732512635L;

	private List<WorkListBaseVOShopbuyItem> mallProduct;
	private List<WorkListBaseVOShopbuyItemImage> mallProductImages;

	public List<WorkListBaseVOShopbuyItem> getMallProduct() {
		return mallProduct;
	}

	public void setMallProduct(List<WorkListBaseVOShopbuyItem> mallProduct) {
		this.mallProduct = mallProduct;
	}

	public List<WorkListBaseVOShopbuyItemImage> getMallProductImages() {
		return mallProductImages;
	}

	public void setMallProductImages(List<WorkListBaseVOShopbuyItemImage> mallProductImages) {
		this.mallProductImages = mallProductImages;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("InterfaceBaseVOShopbuyItem [mallProduct=");
		builder.append(mallProduct);
		builder.append(", mallProductImages=");
		builder.append(mallProductImages);
		builder.append("]");
		return builder.toString();
	}
	
	

}
