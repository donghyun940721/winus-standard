package com.logisall.ws.interfaces.common.exception;

import com.m2m.jdfw5x.message.Msg;

public class InterfaceException extends Exception {

	private static final long serialVersionUID = 606632553491930768L;

	private String msgCode;
	private String message;
	private Object obj;

	public String getMsgCode() {
		return msgCode;
	}

	public Object getObj() {
		return obj;
	}

	public InterfaceException() {
	}

	public InterfaceException(String msgCode, String message) {
		super(message);
		this.msgCode = msgCode;
		this.message = message;
	}

	public InterfaceException(String msgCode, String message, Throwable cause) {
		super(message, cause);
		this.msgCode = msgCode;
		this.message = message;
	}

	public InterfaceException(Throwable cause) {
		super(cause);
	}

	public InterfaceException(String message, Object obj) {
		super(message);
		this.obj = obj;
		msgCode = message;
	}

	public String toString() {
		if (this.message == null) {
			return super.toString();
		} else {
			return this.message;
		}
	}

}