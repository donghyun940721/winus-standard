package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF010Service;
import com.lowagie.text.List;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF010Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF010Service")
	private WMSIF010Service service;
	
//	/*-
//	 * Method ID : getAcceptingList 
//	 * Method 설명 : 입하지시 JSON 송신
//	 * 작성자 : KWT
//	 * 
//	 * @param inputJSON
//	 * 
//	 */
//	@RequestMapping("/IF_LOGIN_QRY.if")	
//	public void getLoginList(HttpServletRequest request, HttpServletResponse response) {
//	Map<String, Object> m = new HashMap<String, Object>();
//	Map<String, Object> model = new HashMap<String, Object>();
//	try {
//		model.put("SAVE_JSON_FILE", "Y");
//		model.put("SAVE_DOC_INFO", "Y");
//
//		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
//		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
//		model.put("D_DOC_CODE", "WMSIF010");
//
//		log.info(" ********** request.getParameterMap()"+ request.getParameterMap().size());
//		
//		String a = request.getParameter("login_id");
//		
//		log.info(" ********** request.a()"+ a);
//		log.info(" ********** request.getParameterMap()"+ request.getParameter("login_id") );
//		
//		if (request.getParameterMap() != null) {
//			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
//				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
//			}
//
//			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
//				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
//			}
//			m = service.selectLoginList(model);
//		}
//	} catch (InterfaceException ife) {
//		if (log.isErrorEnabled()) {
//			log.error("Fail to create Work Info :", ife);
//		}
//		m.put("ERRCODE", ife.getMsgCode());
//		m.put("MSG", ife.getMessage());
//
//	} catch (Exception e) {
//		if (log.isErrorEnabled()) {
//			log.error("Fail to create Work Info :", e);
//		}
//		m.put("ERRCODE", -1);
//		m.put("MSG", MessageResolver.getMessage("save.error"));
//
//	}
//
//	try {
//		//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
//		service.sendJSON(response, m, model);
//
//	} catch (Exception e) {
//		if (log.isErrorEnabled()) {
//			log.error("Fail to send JSON :", e);
//		}
//	}
//}	
	
//	public void getLoginList(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
//		Map<String, Object> m = new HashMap<String, Object>();
//		Map<String, Object> model = new HashMap<String, Object>();
//
//		try {
//			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
//
//			model.put("SAVE_JSON_FILE", "Y");
//			model.put("SAVE_DOC_INFO", "Y");
//
//			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
//			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
//			model.put("D_DOC_CODE", "WMSIF010");
//
//			service.receiveJSON(model, inputJSON);
//			
//			m = service.selectLoginList(model);
//						
//			
//		} catch (InterfaceException ife) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to create Work Info :", ife);
//			}
//			m.put("ERRCODE", ife.getMsgCode());
//			m.put("MSG", ife.getMessage());
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to create Work Info :", e);
//			}
//			m.put("ERRCODE", -1);
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//
//		}
//
//		try {
//			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
//			service.sendJSON(response, m, model);
//			log.info("[ data ==========================] :" + model);
//			log.info("[ data ==========================] :" + response);
//			log.info("[ data ==========================] :" + m);
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to send JSON :", e);
//			}
//		}
//	}	

	/*-
	 * Method ID : getAcceptingList 
	 * Method 설명 : 입하지시 JSON 송신
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_RECEIVING_LIST_QRY.if")
	public void getAcceptingList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF010");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}
				m = service.selectAcceptingList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : receiveOutQry
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping("/IF_OUT_RECEIVE_QRY.if")
	public void receiveOutQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVE_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVE_QRY);
			model.put("D_DOC_CODE", "WMSIF010");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_WORK_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
					model.put(ConstantWSIF.IF_KEY_REGIST_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
				}

				/*
				 * if
				 * (request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ)
				 * != null) { model.put(ConstantWSIF.IF_KEY_ORD_SEQ,
				 * request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ)[0]
				 * ); }
				 */

				m = service.selectAcceptingQry(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}


	/*-
	 * Method ID : receiveComplete
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_IN_RECEIVE_COMPLETE.if")
	public void receiveComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.updateAcceptingComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			// service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}
	
	/*-
	 * Method ID : getReceivingList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping("/IF_OUT_RECEIVING_IN_LIST_QRY.if")
	public void getReceivingList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_IN_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_IN_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF010");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}
				m = service.selectReceivingList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : receiveInQry
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping("/IF_OUT_RECEIVEING_IN_QRY.if")
	public void receiveInQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVEING_IN_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVEING_IN_QRY);
			model.put("D_DOC_CODE", "WMSIF010");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_WORK_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
					model.put(ConstantWSIF.IF_KEY_REGIST_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
				}
				/*
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ)[0]);
				}
				*/
				m = service.selectInList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}

	/*-
	 * Method ID : receiveInComplete
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_IN_RECEIVEING_IN_COMPLETE.if")
	public void receiveInComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVEING_IN_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVEING_IN_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.updateInComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}
	

	/*-
	 * Method ID : mappingInComplete
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_IN_MAPPING_IN_COMPLETE.if")
	public void mappingInComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_MAPPING_IN_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_MAPPING_IN_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.updateInComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
			if (log.isInfoEnabled()) {
				log.info("m :" + m);
			}
		*/
	}

}
