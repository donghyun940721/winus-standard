package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.util.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF102Service;
import com.logisall.ws.interfaces.common.RestApiUtil;

import com.lowagie.text.List;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;


@Controller
public class WMSIF102Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF102Service")
	private WMSIF102Service service;
	
	/*-
	 * Method ID : getGodomallOrderInQry 
	 * Method 설명 : 고도몰 주문 정보 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_GODOMALL_ORDER_IN_QRY.if")	
	public void getGodomallOrderInQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF102");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			m = service.selectGodomallOrderInQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : receiveInOrderComplete
	 * Method 설명 : 고도몰 주문 정보 조회
	 * 작성자 : MonkeySeok
	 */
	@RequestMapping("/IF_GODOMALL_IN_ORDER_COMPLETE.if")
	public void GodomallOrderSearchComplete(String inputJSON, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m     = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		
		try {
			/*restApi 실행 부분 **********************************************************************************/
			Map<String, Object> modelParam = new HashMap<String, Object>();
			modelParam.put("partner_key"	, request.getParameterMap().get("partner_key")[0]);
			modelParam.put("key"				, request.getParameterMap().get("key")[0]);
			modelParam.put("startDate"		, request.getParameterMap().get("startDate")[0]);
			modelParam.put("endDate"			, request.getParameterMap().get("endDate")[0]);
			/*실제운영 JSON문*/
			inputJSON = RestApiUtil.nameCamelcaseToUnderscore(RestApiUtil.xmlToJsonStrOrderInfo(RestApiUtil.postGetXmltoJson(request.getParameterMap().get("url")[0], modelParam)));
			/*테스트 JSON문*/
			//inputJSON = RestApiUtil.nameCamelcaseToUnderscore(RestApiUtil.xmlToJsonStrOrderInfo(request.getParameterMap().get("xmlSample")[0]));
			/*************************************************************************************************/
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF050");			
			
			m.put(ConstantWSIF.IF_KEY_LOGIN_ID, "m208019");
		//}

		//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
			m.put(ConstantWSIF.IF_KEY_PASSWORD, "m1234");
			
			//log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			//LC_ID, USER_NO, CUST_ID model Add
			model.put("LC_ID"			, request.getParameterMap().get("strLcId")[0]);
			model.put("USER_NO"	, request.getParameterMap().get("strUserNo")[0]);
			model.put("CUST_ID"		, request.getParameterMap().get("strCustId")[0]);
			service.updateGodomallOrderSearchComplete(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	

	/*-
	 * Method ID : GodomallItemInfoComplete
	 * Method 설명 : 고도몰 상품 정보 조회
	 * 작성자 : MonkeySeok
	 */
	@RequestMapping("/IF_GODOMALL_ITEM_INFO_COMPLETE.if")
	public void GodomallItemInfoComplete(String inputJSON, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m     = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		
		try {
			/*restApi 실행 부분 **********************************************************************************/
			Map<String, Object> modelParam = new HashMap<String, Object>();
			modelParam.put("partner_key"	, request.getParameterMap().get("partner_key")[0]);
			modelParam.put("key"				, request.getParameterMap().get("key")[0]);
			/*실제운영 JSON문*/
			inputJSON = RestApiUtil.nameCamelcaseToUnderscore(RestApiUtil.xmlToJsonStrItemInfo(RestApiUtil.postGetXmltoJson(request.getParameterMap().get("url")[0], modelParam)));
			/*테스트 JSON문*/
			//inputJSON = RestApiUtil.nameCamelcaseToUnderscore(RestApiUtil.xmlToJsonStrItemInfo(request.getParameterMap().get("xmlSample")[0]));
			/*************************************************************************************************/
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF050");			
			
			m.put(ConstantWSIF.IF_KEY_LOGIN_ID, "m208019");
		//}

		//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
			m.put(ConstantWSIF.IF_KEY_PASSWORD, "m1234");
			
			//log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			//LC_ID, USER_NO, CUST_ID model Add
			model.put("LC_ID"			, request.getParameterMap().get("strLcId")[0]);
			model.put("USER_NO"	, request.getParameterMap().get("strUserNo")[0]);
			model.put("CUST_ID"		, request.getParameterMap().get("strCustId")[0]);
			service.updateGodomallItemInfoComplete(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : receiveInOrderComplete
	 * Method 설명 : 고도몰 주문 정보 조회
	 * 작성자 : MonkeySeok
	 */
	@RequestMapping("/IF_SHOPBY_IN_ORDER_COMPLETE.if")
	public void ShopByOrderSearchComplete(String inputJSON, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m     = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		
		try {
			/*restApi 실행 부분 **********************************************************************************/
			Map<String, Object> modelHeaderParam = new HashMap<String, Object>();
			modelHeaderParam.put("systemKey"	, request.getParameterMap().get("systemKey")[0]);
			modelHeaderParam.put("partnerId"	, request.getParameterMap().get("partnerId")[0]);
			modelHeaderParam.put("mallKey"		, request.getParameterMap().get("mallKey")[0]);
			modelHeaderParam.put("Version"		, request.getParameterMap().get("Version")[0]);
			Map<String, Object> modelBodyParam = new HashMap<String, Object>();
			modelBodyParam.put("startYmd"		, request.getParameterMap().get("startYmd")[0]);
			modelBodyParam.put("endYmd"			, request.getParameterMap().get("endYmd")[0]);
			
			System.out.println("[modelHeaderParam]");
			System.out.println("url : " +request.getParameterMap().get("url")[0]);
			System.out.println("systemKey : " +request.getParameterMap().get("systemKey")[0]);
			System.out.println("partnerId : " +request.getParameterMap().get("partnerId")[0]);
			System.out.println("mallKey : " +request.getParameterMap().get("mallKey")[0]);
			System.out.println("Version : " +request.getParameterMap().get("Version")[0]);
			System.out.println("[modelBodyParam]");
			System.out.println("startYmd : " +request.getParameterMap().get("startYmd")[0]);
			System.out.println("endYmd : " +request.getParameterMap().get("endYmd")[0]);
			System.out.println("");
			
			/*실제운영 JSON문*/
			inputJSON = RestApiUtil.nameCamelcaseToUnderscore(RestApiUtil.getGetJson(request.getParameterMap().get("url")[0], modelHeaderParam, modelBodyParam));
			//log.info("inputJSON==============="+inputJSON);
			/*테스트 JSON문*/
			//inputJSON = RestApiUtil.nameCamelcaseToUnderscore(RestApiUtil.xmlToJsonStrOrderInfo(request.getParameterMap().get("xmlSample")[0]));
			/*************************************************************************************************/
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF050");			
			
			m.put(ConstantWSIF.IF_KEY_LOGIN_ID, "m208019");
			
		//}

		//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
			m.put(ConstantWSIF.IF_KEY_PASSWORD, "m1234");
			
			//log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			//LC_ID, USER_NO, CUST_ID model Add
			model.put("LC_ID"			, request.getParameterMap().get("strLcId")[0]);
			model.put("USER_NO"	, request.getParameterMap().get("strUserNo")[0]);
			model.put("CUST_ID"		, request.getParameterMap().get("strCustId")[0]);
			service.updateShopbuyOrderSearchComplete(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
}
