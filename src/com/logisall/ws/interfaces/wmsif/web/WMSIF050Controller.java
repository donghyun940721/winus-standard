package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.util.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF050Service;
import com.lowagie.text.List;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import java.util.ArrayList;

@Controller
public class WMSIF050Controller {

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF050Service")
    private WMSIF050Service service;

    @Resource(name = "WMSIF000Service")
    private WMSIF000Service WMSIF000Service;

    /*-
     * Method ID : getLoginList 
     * Method 설명 : 로그인
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_LOGIN_QRY.if")
    public void getLoginList(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectLoginList(model);
                service.fcmNaviSender(m);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getAuthQry 
     * Method 설명 : 권한획득
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_AUTH_QRY.if")
    public void getAuthQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                m = service.selectAuthQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getAuthQry 
     * Method 설명 : 권한획득
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_VERSION_QRY.if")
    public void getVersionQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            System.out.println("[DEBUG] GET CLIENT REQUEST...");
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectVersionQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderQry 
     * Method 설명 : 입고주문조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_QRY.if")
    public void getInOrderQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectInOrderQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInUsaOrderQry 
     * Method 설명 : 입고주문조회(LOGIALLUSA)
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_USA_ORDER_QRY.if")
    public void getInUsaOrderQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                m = service.selectInUsaOrderQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderDetailQry 
     * Method 설명 : 입고주문상세정보조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_DETAIL_QRY.if")
    public void getInOrderDetailQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                m = service.selectInOrderDetailQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderMappingQry 
     * Method 설명 : 입고매핑정보조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_MAPPING_QRY.if")
    public void getInOrderMappingQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectInOrderMappingQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderRemainQry 
     * Method 설명 : 입고잔여작업정보조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_REMAIN_QRY.if")
    public void getInOrderRemainQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectInOrderRemainQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getPltConfirmQry 
     * Method 설명 : 입고잔여작업정보조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_PLT_CONFIRM_QRY.if")
    public void getPltConfirmQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectPltConfirmQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderConfirmQry
     * Method 설명 : 입고주문상태조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_CONFIRM_QRY.if")
    public void getInOrderConfirmQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        log.info("IF_IN_ORDER_CONFIRM_QRY================");

        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectInOrderConfirmQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getPltConfirmQry 
     * Method 설명 : 물류용기 상태 정보 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_PLT_MAPPING_CONFIRM_QRY.if")
    public void getPltMappingConfirmQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectPltMappingConfirmQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : receiveInComplete
     * Method 설명 : 
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_RECEIVE_COMPLETE_KR.if")
    public void receiveInComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceiveInComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : receiveInPltComplete
     * Method 설명 : 물류용기입고왁정
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_RECEIVE_PLT_COMPLETE.if")
    public void receiveInPltComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceiveInPltComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getLcRitemQry 
     * Method 설명 : 물류센터별 상품조회 
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_LC_RITEM_QRY.if")
    public void getLcRitemQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectLcRitemQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getMapPltConfirmQry 
     * Method 설명 : 입고잔여작업시 매핑 상태조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_MAP_PLT_CONFIRM_QRY.if")
    public void getMapPltConfirmQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                m = service.selectMapPltConfirmQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getEpcConfirmQry 
     * Method 설명 : 입고잔여작업시 매핑 상태조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_EPC_CONFIRM_QRY.if")
    public void getEpcConfirmQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                m = service.selectEpcConfirmQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getEpcHexConfirmQry 
     * Method 설명 : 이재매핑시 HEX 값으로 plt 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_EPC_HEX_CONFIRM_QRY.if")
    public void getEpcHexConfirmQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                m = service.selectEpcHexConfirmQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getEpcHexConfirmQry 
     * Method 설명 : 이재매핑시 HEX 값으로 plt 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_CUST_ID_QRY.if")
    public void getCustIdQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectCustIdQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getReceivingOrderStatQry 
     * Method 설명 : 당일 입고주문 상태 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_RECEIVING_ORDER_STAT_QRY.if")
    public void getReceivingOrderStatQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF060");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectReceivingOrderStatQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getReceivingOrderStatQry_DH 
     * Method 설명 : (대화물류) 입고주문 조회
     * 작성자 : kimzero
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_RECEIVING_ORDER_STAT_QRY_DH.if")
    public void getReceivingOrderStatQry_DH(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF060");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_CUST_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ITEM_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ITEM_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ITEM_CD)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SUBTYPE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_SUBTYPE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SUBTYPE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_IN_DT) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_IN_DT,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_IN_DT)[0]);
                }

                m = service.selectReceivingOrderStatQry_DH(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inAutoOrderInsert
     * Method 설명 : 입고주문자동입력
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_AUTO_ORDER_INSERT.if")
    public void inAutoOrderInsert(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateInAutoOrderInsert(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderLotDetailQry 
     * Method 설명 : 롯트번호로 주문정보 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_LOT_DETAIL_QRY.if")
    public void getInOrderLotDetailQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF060");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }
                
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                m = service.selectInOrderLotDetailQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : receiveChangePasswd
     * Method 설명 : 비밀번호변경
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_CHANGE_PASSWD_COMPLETE.if")
    public void receiveChangePasswd(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        ModelAndView mav = null;

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            try {
                mav = new ModelAndView("jsonView", service.save(model));
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Fail to change pwd :", e);
                }
                m = new HashMap<String, Object>();
                m.put("ERROR", "1");
                m.put("MSG", MessageResolver.getMessage("save.error"));
            }

            // service.updateReceiveInPltComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : setPartComplete
     * Method 설명 : 임가공 파트 입고 로직
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_SET_PART_COMPLETE.if")
    public void setPartComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF070");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateSetPartComplete(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getSetPartInfoQry 
     * Method 설명 : 임가공 파트 식별표 정보 분석
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_SET_PART_INFO_QRY.if")
    public void getSetPartInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                m = service.selectPartInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getSetPartInfoQry 
     * Method 설명 : 임가공 파트 식별표 정보 분석
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_LOT_STAT_INFO_QRY.if")
    public void getInLotStatInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectInLotStatInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : setFcmPhoneComplete
     * Method 설명 : 기기 FCM, 전화번호 전송 로직
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_FCM_PHONE_COMPLETE.if")
    public void setFcmPhoneComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF070");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateFcmPhoneComplete(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inBoxAutoOrderInsert
     * Method 설명 : 입고주문자동입력(box 수량 입력)
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_BOX_AUTO_ORDER_INSERT.if")
    public void inBoxAutoOrderInsert(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateInBoxAutoOrderInsert(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderLotMultiQry 
     * Method 설명 : 입고주문조회시 동일 lot가 있을 경우 주문번호, seq 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_LOT_MULTI_QRY.if")
    public void getInOrderLotMultiQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectInOrderLotMultiQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inAutoOrderSapInsert
     * Method 설명 : 입고주문자동입력
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_AUTO_ORDER_SAP_INSERT.if")
    public void inAutoOrderSapInsert(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateInAutoOrderSapInsert(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : receiveJSONWithFiles
     * Method 설명 : 파일을 포함한 Json request 의 저장함수
     * 작성자 : kdalma
     *
     * @param model : mulitpart form request 객체
     * @param inputJSON : file 이외의 json 데이터
     * @param files : 첨부된 파일 array
     * @throws Exception
     */
    public void saveImageFromBase64(String key, String fileName, String base64Data, String ord_id) throws Exception {

        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model2 = new HashMap<String, Object>();

        try {
            if (base64Data == null)
                return;

            String path = ConstantWSIF.WMS_IMG_PATH + DateUtil.getDateByPattern2() + "/" + key;
            String file_id = key + fileName;
            InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));

            model2.put("ORD_ID", ord_id);
            model2.put("FILE_ID", file_id);
            model2.put("ATTACH_GB", "WMS");
            model2.put("FILE_VALUE", "WMSOP");
            model2.put("FILE_PATH", path);
            model2.put("FILE_NAME", fileName);
            model2.put("FILE_EXT", ".jpg");

            try {
                m = service.WmsImgDataInsert(model2);
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Fail to save confirm :", e);
                }
                m.put("MSG", MessageResolver.getMessage("save.error"));
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Return Map :", e);
            }
        }
    }

    /*-
     * Method ID : receivingSingleComplete
     * Method 설명 : 모바일 입고 주문 입력 후 입고확정(이미지 처리)
     * DATE : 2019-02-13
     * 작성자 : SMICS
     *
     * @param request : 
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RECEIVING_SINGLE_COMPLETE.if")
    public void receivingSingleComplete(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.receiveJSON(model, inputJSON);

            service.updateReceiveInComplete(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inOrdLocCompleteMobile
     * Method 설명 : 주문입력, 로케이션지정, 배차(옵션), 입고 완료 진행
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_LOC_COMPLETE_MOBILE.if")
    public void inOrdLocCompleteMobile(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.receiveJSON(model, inputJSON);

            service.updateInOrdLocCompleteMobile(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getLcCarInfoQry 
     * Method 설명 : 물류센터별 차량조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_LC_CAR_INFO_QRY.if")
    public void getLcCarInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                m = service.selectLcCarInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inTakingPicturesMobile
     * Method 설명 : 입고사진촬영
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_TAKING_PICTURES_MOBILE.if")
    public void inTakingPicturesMobile(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[9];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");
            files[6] = request.getParameter("file6");
            files[7] = request.getParameter("file7");
            files[8] = request.getParameter("file8");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInTakingPicturesMobile(model);

            // Save photo files  
            for (int index = 0; index < 9; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inTakingPicturesMobile
     * Method 설명 : 입고사진촬영 - 기존 내역에 추가
     * 작성자 : SUMMER
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_TAKING_PICTURES_MOBILE2.if")
    public void inTakingPicturesMobile2(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[21];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");
            files[6] = request.getParameter("file6");
            files[7] = request.getParameter("file7");
            files[8] = request.getParameter("file8");
            files[9] = request.getParameter("file9");
            files[10] = request.getParameter("file10");
            files[11] = request.getParameter("file11");
            files[12] = request.getParameter("file12");
            files[13] = request.getParameter("file13");
            files[14] = request.getParameter("file14");
            files[15] = request.getParameter("file15");
            files[16] = request.getParameter("file16");
            files[17] = request.getParameter("file17");
            files[18] = request.getParameter("file18");
            files[19] = request.getParameter("file19");
            files[20] = request.getParameter("file20");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String picCntString = request.getParameter("picCnt");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInTakingPicturesMobile(model);

            int picCnt = Integer.parseInt(picCntString);

            // Save photo files - 최대 사진 갯수 21개로 조정 
            for (int index = 0; index < 21; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", picCnt), files[index], ord_id);
                picCnt++;
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getInfoQry 
     * Method 설명 : 물류센터별 기준 정보조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_INFO_QRY.if")
    public void getInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_FROM_LOC_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_FROM_LOC_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_FROM_LOC_BARCODE_CD)[0]);
                }

                m = service.selectInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderInsertQry 
     * Method 설명 : 입고주문입력을 위한 상품코드 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_INSERT_QRY.if")
    public void getInOrderInsertQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                m = service.selectInOrderInsertQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : receiveTransferAllComplete
     * Method 설명 : 주문입력된 상태에서 모바일 로케이션 지정
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RECEIVING_LOC_WORKING_COMPLETE.if")
    public void receiveLocWorkingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateLocWorkingComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getInCustInfoQry 
     * Method 설명 : 입출고거래처조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_TRANS_CUST_INFO_QRY.if")
    public void getTransCustInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectTransCustInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getTransItemInfoQry 
     * Method 설명 : 거래처별 상품조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_TRANS_ITEM_INFO_QRY.if")
    public void getTransItemInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectTransItemInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getInOrderDateQry 
     * Method 설명 : 화주별 입고 주문 조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_DATE_QRY.if")
    public void getInOrderDateQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                m = service.selectInOrderDateQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : receiveInComplete
     * Method 설명 : 
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_INS_SIMPLE_IN_.if")
    public void receiveInOrdInsSimpleIn(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.receiveJSON(model, inputJSON);

            service.updateReceiveInOrdInsSimpleIn(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : receiveAfterGrn
     * Method 설명 : 입하완료 후 입고확정의 재고이동 처리
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RECEIVING_AFTER_GRN.if")
    public void receiveAfterGrn(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceiveAfterGrn(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : packingInspection
     * Method 설명 : 포장 부품 검수 결과 
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_PACKING_PART_INSPECTION.if")
    public void packingPartInspection(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.receiveJSON(model, inputJSON);

            service.updatepackingPartInspection(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : receiveOrderInsertConfirmGrn
     * Method 설명 : 입고 주문 입력 후 입하확정
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_ORDER_INSERT_CONFIRM_GRN.if")
    public void receiveOrderInsertConfirmGrn(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceiveOrderInsertConfirmGrn(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : receiveReceivingCompleteGrn
     * Method 설명 : 입하확정 후 입고확정
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RECEIVING_COMPLETE_CONFIRM_GRN.if")
    public void receiveReceivingCompleteGrn(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceiveReceivingCompleteGrn(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

//	
//	public void receiveReceivingCompleteGrn(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
//		Map<String, Object> m = new HashMap<String, Object>();
//		Map<String, Object> model = new HashMap<String, Object>();	
//		
//		try {
//			String[] files = new String[6];
//			files[0] = request.getParameter("file0");
//			files[1] = request.getParameter("file1");
//			files[2] = request.getParameter("file2");
//			files[3] = request.getParameter("file3");
//			files[4] = request.getParameter("file4");
//			files[5] = request.getParameter("file5");
//			
//			String key = request.getParameter("key");
//			String sign = request.getParameter("sign");			
//			String ord_id = request.getParameter("img_ord_id");
//			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
//			
//			//System.out.println(inputJSON); // print log
//			//log.info(" ********** inputJSON ********** : " + inputJSON);
//			
//			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
//
//			model.put("SAVE_JSON_FILE", "Y");
//			model.put("SAVE_DOC_INFO", "Y");
//
//			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
//			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
//			model.put("D_DOC_CODE", "WMSIF090");
//
//			service.receiveJSON(model, inputJSON);
//
//			service.updateReceiveReceivingCompleteGrn(model);
//			
//			// Save photo files
//			for(int index=0;index<6;index++) {
//				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
//			}
//			// Save Signature file
//			saveImageFromBase64(key, "sign.jpg", sign, ord_id);
//
//			// m.put("MSG", MessageResolver.getMessage("complete"));
//
//		} catch (InterfaceException ife) {
//			m = new HashMap<String, Object>();
//			m.put("ERRCODE", ife.getMsgCode());
//			m.put("MSG", ife.getMessage());
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to create Work Info :", e);
//			}
//			m = new HashMap<String, Object>();
//			m.put("ERRCODE", -1);
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//
//		try {
//			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
//			service.sendResponse(response, model, m);
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to sendResponse :", e);
//			}
//		}
//	}

    /*-
     * Method ID : receiveReceivingOrdLocDayComplete
     * Method 설명 : 입고주문, 로케이션, 유통기한 입력 후 입고 확정
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RECEIVING_ORD_LOC_DAY_COMPLETE.if")
    public void receiveReceivingOrdLocDayComplete(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceivingOrdLocDayComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : CancelGrnBadComplete
     * Method 설명 : 입하확정 후 입하 취소 및 불량 입고확정
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_CANCEL_GRN_BAD_COMPLETE.if")
    public void receiveCancelGrnBadComplete(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.receiveJSON(model, inputJSON);

            service.updateReceiveCancelGrnBadComplete(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : ItemImgSenderMobile
     * Method 설명 : 모바일 상품 사진 등록
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_ITEM_IMG_SENDER_MOBILE.if")
    public void ItemImgSenderMobile(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.receiveJSON(model, inputJSON);

            // service.updateItemImgSenderMobile(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveItemImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveItemImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            model.put("RETURN_CD", "0");
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : saveItemImageFromBase64
     * Method 설명 : 파일을 포함한 Json request 의 저장함수
     * 작성자 : monkeyseok
     * date : 20191111
     *
     * @param model : mulitpart form request 객체
     * @param inputJSON : file 이외의 json 데이터
     * @param files : 첨부된 파일 array
     * @throws Exception
     */
    public void saveItemImageFromBase64(String key, String fileName, String base64Data, String ord_id) throws Exception {

        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model2 = new HashMap<String, Object>();

        try {
            if (base64Data == null)
                return;

            String path = ConstantWSIF.ITEM_IMG_PATH + key;
            log.info("path:" + path);
            String file_id = key + fileName;
            InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));

            model2.put("ORD_ID", ord_id);
            model2.put("FILE_ID", file_id);
            model2.put("ATTACH_GB", "ITEM");
            model2.put("FILE_VALUE", "wmsms090");
            model2.put("FILE_PATH", path);
            model2.put("FILE_NAME", fileName);
            model2.put("FILE_EXT", ".jpg");

            try {
                m = service.ItemImgDataInsert(model2);
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Fail to save confirm :", e);
                }
                m.put("MSG", MessageResolver.getMessage("save.error"));
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Return Map :", e);
            }
        }
    }

    /*-
     * Method ID : locDayInMobileComplete
     * Method 설명 : 로케이션, 유통기한 입력 후 입고 확정
     * 작성자 : SMICS
     * 날 짜   : 2020.02.25
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_LOC_DAY_IN_MOBILE_COMPLETE.if")
    public void locDayInMobileComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateLocDayInMobileComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getInCustInfoQry 
     * Method 설명 : 입출고거래처조회
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_CUST_CAL_PLT_ID_QRY.if")
    public void getCustCalPltIdQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                m = service.selectCustCalPltIdQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inIssueLabel
     * Method 설명 : 레이블 발행 정보 입력
     * 작성자 : SMICS
     * 날 짜 : 2020.06.01
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ISSUE_LABEL.if")
    public void inIssueLabel(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInIssueLabel(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inIssueOrdLabel
     * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트
     * 작성자 : SMICS
     * 날 짜 : 2021.05.16
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ISSUE_ORD_LABEL.if")
    public void inIssueOrdLabel(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInIssueOrdLabel(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getIssueLabelInfoQry 
     * Method 설명 : 레이블 바코드 정보 조회
     * 작성자 : smics
     * 날 짜 : 2020.06.01
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_ISSUE_LABEL_INFO_QRY.if")
    public void getIssueLabelInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                m = service.selectIssueLabelInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : receiveInOrdInsDaySimpleIn
     * Method 설명 : 자동 주문 입력 후 간편 입고(제조일자, 유통기한 추가)
     * 작성자 : MonkeySeok
     * 날 짜 : 2020-06-18
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_INS_DAY_SIMPLE_IN.if")
    public void receiveInOrdInsDaySimpleIn(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

//            service.receiveJSON(model, inputJSON);

            service.updateReceiveInOrdInsDaySimpleIn(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getInUsaOrderQry 
     * Method 설명 : 입고주문조회(LOGIALLUSA)
     * 작성자 : smics
     * 
     * @param sendJSON
     * 
     */
    @RequestMapping("/IF_IN_ORD_INS_DAY_SIMPLE_IN_SATORI.if")
    public void receiveInOrdInsDaySimpleInSatori(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                service.updateReceiveInOrdInsDaySimpleIn(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inOrdDiviedCompleteMobile
     * Method 설명 : 주문을 파렛트 수량으로 나눠서 주문 입력
     * 작성자 : SMICS
     * 날 짜 : 2020-09-25
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_DIVIDED_COMPLETE_MOBILE.if")
    public void inOrdDiviedCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateOrdDiviedCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : OrdCngQtySimpleIn
     * Method 설명 : 주문입력 후 SIMPLE IN(수량변경가능)
     * 작성자 : SMICS
     * 날 짜 : 2021-04-21
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_ORD_CHANGE_QTY_SIMPLE_IN.if")
    public void OrdCngQtySimpleIn(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateOrdCngQtySimpleIn(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getFixLocConfirmQry 
     * Method 설명 : 픽스 로케이션 지정
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_FIX_LOC_CONFIRM_QRY.if")
    public void getFixLocConfirmQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }

                m = service.selectFixLocConfirmQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inOrdDiviedCompleteMobile2048
     * Method 설명 : 주문을 파렛트 수량으로 나눠서 주문 입력
     * 작성자 : SMICS
     * 날 짜 : 2021-07-06
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_DIVIDED_COMPLETE_MOBILE_2048.if")
    public void inOrdDiviedCompleteMobile2048(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateOrdDiviedCompleteMobile2048(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inIssueOrdLbLabel
     * Method 설명 : 레이블 발행 정보 입력
     * 작성자 : SMICS
     * 날 짜 : 2021.07.08
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ISSUE_LB_LABEL.if")
    public void inIssueLbLabel(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInIssueLbLabel(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inOrd2048LocCompleteMobile
     * Method 설명 : 발행된 레이블 정보를 기반으로 주문입력, 로케이션 지정, 입고 확정
     * 작성자 : SMICS
     * 날 짜 : 2021-07-18
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORDER_2048_LOC_COMPLETE_MOBILE.if")
    public void inOrd2048LocCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateOrd2048LocCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inOrd2048LocCompleteMobile
     * Method 설명 : 발행된 레이블 정보를 기반으로 주문입력, 로케이션 지정, 입고 확정
     * 작성자 : SMICS
     * 날 짜 : 2021-07-18
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORDER_2048_LOC_COMPLETE_MOBILE_DH.if")
    public void inOrd2048LocCompleteMobileDH(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateOrd2048LocCompleteMobileDH(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inIssueOrdLabel
     * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트
     * 작성자 : SMICS
     * 날 짜 : 2021.05.16
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ISSUE_LABEL_LB_ORD_COMPLETE.if")
    public void inIssueLabelLbOrdComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);
            service.updateInIssueLabelLbOrdComplete(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : OmsOpMobileInComplete
     * Method 설명 : oms기준 주문입력 및 간편입고
     * 작성자 : SMICS
     * 날 짜   : 2021.10.25
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_OMS_OP_MOBILE_IN_COMPLETE.if")
    public void OmsOpMobileInComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateOmsOpMobileInComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getSetPartInfoQry 
     * Method 설명 : 임가공 파트 식별표 정보 분석
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_ITEM_QRY.if")
    public void getInOrdItemInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectInOrdItemInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inOrd2048LocCompleteMobileDHSub
     * Method 설명 : 주문입력(ord_subtype 변수), 로케이션 지정, 입고 확정
     * 작성자 : SMICS
     * 날 짜 : 2022-04-27
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORDER_2048_LOC_COMPLETE_MOBILE_DH_SUB.if")
    public void inOrd2048LocCompleteMobileDHSub(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateOrd2048LocCompleteMobileDHSub(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : LcMultiBarInfoQry 
     * Method 설명 : 상품 다중 바코드 조회 
     * 날 짜 : 2022-05-17
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_LC_MULTI_BAR_INFO_QRY.if")
    public void getLcMultiBarInfoQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectLcMultiBarInfoQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : receiveInOrdSimpleIn
     * Method 설명 : 간편입고
     * 날 짜 : 2022-07-20
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_SIMPLE_IN.if")
    public void receiveInOrdSimpleIn(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            files[0] = request.getParameter("file0");
            files[1] = request.getParameter("file1");
            files[2] = request.getParameter("file2");
            files[3] = request.getParameter("file3");
            files[4] = request.getParameter("file4");
            files[5] = request.getParameter("file5");

            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            // System.out.println(inputJSON); // print log
            // log.info(" ********** inputJSON ********** : " + inputJSON);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.receiveJSON(model, inputJSON);

            service.updateReceiveInOrdSimpleIn(model);

            // Save photo files
            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            // Save Signature file
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getWcsInOrderLotDetailQry 
     * Method 설명 : 창원 WCS 롯트번호로 주문정보 조회
     * 날 짜 : 2022-08-19
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_WCS_IN_ORDER_LOT_DETAIL_QRY.if")
    public void getWcsInOrderLotDetailQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {

            /* EAI WCS 설비 상태 최신화 */
            Map<String, Object> eaiMap = new HashMap<String, Object>();
            String cMethod = "POST";
            String cUrl = "WCS_MFC/MFC/SYNC_DEVICE_STATUS_U";

            eaiMap.put("cMethod", cMethod);
            eaiMap.put("cUrl", cUrl);
            eaiMap.put("data", "");

            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF060");

            if (request.getParameterMap() != null) {
            	if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
    				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
    			}
    			
    			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
    				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
    			}
    			
    			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
    				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
    			}
    			
    			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
    				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
    			}
    			
    			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
    				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
    			}

                m = service.selectWcsInOrderLotDetailQry(model);

                /* EAI WCS 주문 작업 전송 */
                cMethod = "POST";
                cUrl = "WCS_MFC/MFC/SYNC_JOB_ORDER";

                eaiMap.put("cMethod", cMethod);
                eaiMap.put("cUrl", cUrl);
                eaiMap.put("data", "");

                WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : getWcsInOrderLotDetailQry 
     * Method 설명 : 창원 WCS 롯트번호로 주문정보 조회
     * 날 짜 : 2022-08-19
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_WCS_IN_ORDER_RECOMMEND_QRY.if")
    public void getWcsInOrderRecommendQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {

            /* EAI WCS 설비 상태 최신화 */
            Map<String, Object> eaiMap = new HashMap<String, Object>();
            String cMethod = "POST";
            String cUrl = "WCS_MFC/MFC/SYNC_DEVICE_STATUS_U";

            eaiMap.put("cMethod", cMethod);
            eaiMap.put("cUrl", cUrl);
            eaiMap.put("data", "");

            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF060");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.MAP_KEY_WORK_SET_TYPE) != null) {
                    model.put(ConstantWSIF.MAP_KEY_WORK_SET_TYPE,
                            request.getParameterMap().get(ConstantWSIF.MAP_KEY_WORK_SET_TYPE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectWcsInOrderLotDetailQry(model);

                /* EAI WCS 주문 작업 전송 */
                cMethod = "POST";
                cUrl = "WCS_MFC/MFC/SYNC_JOB_ORDER";

                eaiMap.put("cMethod", cMethod);
                eaiMap.put("cUrl", cUrl);
                eaiMap.put("data", "");

                WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
        	if(m.get("ERRCODE").equals("-1")){
        		model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
        		service.sendResponse(response, model, m);
        	}else{
        		service.sendJSON(response, m, model);
        	}

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : WcsCwPltInComplete	 
     * Method 설명 : 창원 wcs 입고 적치 지시 생성
     * 작성자 : SMICS
     * 날 짜   : 2022.08.22
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_WCS_CW_PLT_IN_COMPLETE.if")
    public void WcsCwPltInComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            /* EAI WCS 설비 상태 최신화 */
            Map<String, Object> eaiMap = new HashMap<String, Object>();
            String cMethod = "POST";
            String cUrl = "WCS_MFC/MFC/SYNC_DEVICE_STATUS_U";

            eaiMap.put("cMethod", cMethod);
            eaiMap.put("cUrl", cUrl);
            eaiMap.put("data", "");

            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateWcsCwPltInComplete(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

            /* EAI WCS 주문 작업 전송 */
            cMethod = "POST";
            cUrl = "WCS_MFC/MFC/SYNC_JOB_ORDER";

            eaiMap.put("cMethod", cMethod);
            eaiMap.put("cUrl", cUrl);
            eaiMap.put("data", "");

            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getParcelInvcQry 
     * Method 설명 : 반품입고 송장 검수 조회
     * 날 짜 : 2022-12-06
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */

    @RequestMapping("/IF_IN_PARCEL_INVC_QRY.if")
    public void getParcelInvcQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectParcelInvcQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : inIssueLabelOrdLoc
     * Method 설명 : 다임러 주문입력 , 로케이션지정, 레이블 발행
     * 작성자 : SMICS
     * 날 짜 : 2023-01-18
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ISSUE_LABEL_ORD_LOC.if")
    public void inIssueLabelOrdLoc(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInIssueLabelOrdLoc(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : getLotUnitLocQry 
     * Method 설명 : LOT번호와  UNIT번호로 로케이션 조회
     * 날 짜 : 2023-01-18
     * 작성자 : smics
     * 
     * @param inputJSON
     * 
     */

    @RequestMapping("/IF_LOT_UNIT_LOC_QRY.if")
    public void getLotUnitLocQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                m = service.selectLotUnitLocQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : updateInOrdLocCompleteMobileUnit
     * Method 설명 : 주문입력(ord_subtype 변수), 로케이션 지정, 입고 확정, unit_no 포함
     * 작성자 : SMICS
     * 날 짜 : 2023-05-15
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_LOC_COMPLETE_MOBILE_UNIT.if")
    public void updateInOrdLocCompleteMobileUnit(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInOrdLocCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    
    /*-
     * Method ID : 
     * Method 설명 : 주문입력(ord_subtype 변수)
     * 작성자 : 
     * 날 짜 : 2024-01-12
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORD_LOC_CHECK_MOBILE.if")
    public void updateInOrdLocCheckMobileUnit(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateInOrdLocCheckMobileUnit(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }

    /*-
     * Method ID : inOrd2048LocTypeCompleteMobile
     * Method 설명 : 발행된 레이블 정보를 기반으로 주문입력, 로케이션 지정, 입고처, 입고타입 후 입고 확정
     * 작성자 : SMICS
     * 날 짜 : 2023-06-29
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ORDER_2048_LOC_TYPE_COMPLETE_MOBILE.if")
    public void inOrd2048LocTypeCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateOrd2048LocTypeCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /*-
     * Method ID : receiveInCheckCompleteMobile
     * 날 짜 : 2023-07-11
	 * Method 설명 : 입고 검수 후 입고 완료
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_CHECK_COMPLETE_MOBILE.if")
    public void receiveInCheckCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response)

            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);

            service.updateReceiveInCheckCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /*-
     * Method ID : ifRitemSearchQry
     * Method 설명 : 모바일 item select 호출
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RITEM_SEARCH_QRY.if")
    public ModelAndView ritemSearchMobile(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        ModelAndView mav = new ModelAndView("jsonView");
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_CUST_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID)[0]);
                }
                
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_CD)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_NM) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_NM,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_NM)[0]);
                }

                m = service.selectRitemQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }
        mav.addAllObjects(m);
        return mav;
        /*
        try {
            
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
        */
    }
    
    /*-
     * Method ID : receiveReceivingOrdLocDayComplete
     * Method 설명 : 입고주문, 로케이션, 유통기한, 제조일자 입력 후 입고 확정
     * 작성자 : 
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RECEIVING_ORD_LOC_DAY_COMPLETE2.if")
    public void receiveReceivingOrdLocDayComplete2(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceivingOrdLocDayComplete2(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /*-
     * Method ID : receivingChkCompleteMobile
     * Method 설명 : 카카오VX 입고 검수 완료
     * 작성자 : 
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_RECEIVING_CHK_COMPLETE_MOBILE.if")
    public void receivingChkCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceivingChkCompleteMobile(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /*-
     * Method ID : ifInOmSelectQry 
     * Method 설명 : 입고주문조회(om)
     * 작성자 : 
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_OM_SELECT_QRY.if")
    public void inorderOmSelectQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_CUST_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }

                m = service.inorderOmSelectQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : ifInOmSelectQry 
     * Method 설명 : 입고주문조회(om)
     * 작성자 : 
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_IN_ORDER_OM_SELECT_QRY_OV.if")
    public void inorderOmSelectOvQry(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_CUST_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_DATE,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
                }
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
                }

                m = service.inorderOmSelectOvQry(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : inOrderPartCompleteMobile 
     * Method 설명 : 입고주문 부분확정
     * 작성자 : 
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IN_ORDER_PART_COMPLETE_MOBILE.if")
    public void inOrderPartCompleteMobile(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {

            if (request.getParameterMap() != null) {
                String[] parameter = {
                        ConstantWSIF.IF_KEY_LOGIN_ID
                        , "user_no"
                        , ConstantWSIF.IF_KEY_PASSWORD
                        , ConstantWSIF.IF_KEY_ROW_LC_ID
                        , ConstantWSIF.IF_KEY_ROW_CUST_ID
                        , ConstantWSIF.IF_KEY_ROW_RITEM_ID
                        , "org_ord_id"
                        , "org_ord_seq"
                        , "ord_dt"
                        , "in_ord_qty"
                        , "in_req_dt"
                        , "cust_lot_no"
                        , "make_dt"
                        , "item_best_date_end"
                        , "loc_cd"
                        , ConstantWSIF.MAP_KEY_WORK_IP
                };
                for(String param : parameter){
                    if (request.getParameterMap().get(param) != null) {
                        model.put(param,request.getParameterMap().get(param)[0]);
                    }
                }
                m = service.inOrderPartCompleteMobile(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : inOrderPartCompleteMobile 
     * Method 설명 : 입고주문 부분확정
     * 작성자 : 
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IN_ORDER_PART_COMPLETE_MOBILE_OLIV.if")
    public void inOrderPartCompleteMobileOv(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {

            if (request.getParameterMap() != null) {
                String[] parameter = {
                        ConstantWSIF.IF_KEY_LOGIN_ID
                        , "user_no"
                        , ConstantWSIF.IF_KEY_PASSWORD
                        , ConstantWSIF.IF_KEY_ROW_LC_ID
                        , ConstantWSIF.IF_KEY_ROW_CUST_ID
                        , ConstantWSIF.IF_KEY_ROW_RITEM_ID
                        , "org_ord_id"
                        , "org_ord_seq"
                        , "ord_dt"
                        , "in_ord_qty"
                        , "in_req_dt"
                        , "cust_lot_no"
                        , "make_dt"
                        , "item_best_date_end"
                        , "loc_cd"
                        , "ord_desc"
                        , ConstantWSIF.MAP_KEY_WORK_IP
                };
                for(String param : parameter){
                    if (request.getParameterMap().get(param) != null) {
                        model.put(param,request.getParameterMap().get(param)[0]);
                    }
                }
                m = service.inOrderPartCompleteMobileOv(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            // model.put("D_TR_STAT",
            // ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    /*-
     * Method ID : receivingSetPartCompleteMobile
     * Method 설명 : SKON 임가공 구성품 입고(총량)
     * 작성자 : MonkeySeok
     * 날   짜 : 2024-01-29
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_SET_PART_COMPLETE_MOBILE.if")
    public void receivingSetPartCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceivingSetPartCompleteMobile(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
        
    /*-
     * Method ID : receivingSetPartSerialCompleteMobile
     * Method 설명 : SKON 임가공 구성품 입고(시리얼 구조 포함)
     * 작성자 : MonkeySeok
     * 날   짜 : 2024-01-29
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_SET_PART_SERIAL_COMPLETE_MOBILE.if")
    public void receivingSetPartSerialCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            // String data = getInputStream(inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            log.info("inputJSON===============" + inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateReceivingSetPartSerialCompleteMobile(model);

            // m.put("MSG", MessageResolver.getMessage("complete"));

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    @RequestMapping("/IF_TAKE_ITEM_VAL.if")
    public void getTakeItemGroup(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get("spcd") != null) {
                    model.put("spcd",
                            request.getParameterMap().get("spcd")[0]);
                }
                if (request.getParameterMap().get("item_group") != null) {
                    model.put("item_group",
                            request.getParameterMap().get("item_group")[0]);
                }
                if (request.getParameterMap().get("item_first") != null) {
                    model.put("item_first",
                            request.getParameterMap().get("item_first")[0]);
                }
                if (request.getParameterMap().get("item_second") != null) {
                    model.put("item_second",
                            request.getParameterMap().get("item_second")[0]);
                }
                if (request.getParameterMap().get("item_last") != null) {
                    model.put("item_last",
                            request.getParameterMap().get("item_last")[0]);
                }
                if (request.getParameterMap().get("item_code") != null) {
                    model.put("item_code",
                            request.getParameterMap().get("item_code")[0]);
                }
                if (request.getParameterMap().get("label_code") != null) {
                    model.put("label_code",
                            request.getParameterMap().get("label_code")[0]);
                }
                if (request.getParameterMap().get("make_line_no") != null) {
                    model.put("make_line_no",
                            request.getParameterMap().get("make_line_no")[0]);
                }
                if (request.getParameterMap().get("make_date") != null) {
                    model.put("make_date",
                            request.getParameterMap().get("make_date")[0]);
                }
                if (request.getParameterMap().get("item_bar_cd") != null) {
                    model.put("item_bar_cd",
                            request.getParameterMap().get("item_bar_cd")[0]);
                }
                m = service.selectItemVar(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : inIssueLabelLbOrdSubComplete
     * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트, ord_subtype, sap_barcode, work_memo 추가
     * 작성자 : SMICS
     * 날 짜 : 2024.02.02
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_ISSUE_LABEL_LB_ORD_SUB_COMPLETE.if")
    public void inIssueLabelLbOrdSubComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);
            service.updateInIssueLabelLbOrdSubComplete(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /*-
     * Method ID : InSerialPltCompleteMobile
     * Method 설명 : 개체 시리얼 인식 후 입고 처리
     * 작성자 : SMICS
     * 날 짜 : 2024.02.26
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_IN_SERIAL_PLT_COMPLETE_MOBILE.if")
    public void InSerialPltCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);
            service.updateInSerialCompleteMobile(model);
            
        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    @RequestMapping("/IF_IN_ORD_INS_DAY_SIMPLE_IN_STLOGIS.if")
    public void receiveInOrdInsDaySimpleInStlogis(MultipartHttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            String[] files = new String[6];
            String key = request.getParameter("key");
            String sign = request.getParameter("sign");
            String ord_id = request.getParameter("img_ord_id");
            String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
            model.put("D_DOC_CODE", "WMSIF090");

            service.updateReceiveInOrdInsDaySimpleInStlogis(model);

            for (int index = 0; index < 6; index++) {
                saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
            }
            saveImageFromBase64(key, "sign.jpg", sign, ord_id);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
}
