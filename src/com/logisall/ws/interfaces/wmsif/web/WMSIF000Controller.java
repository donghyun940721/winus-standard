package com.logisall.ws.interfaces.wmsif.web;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.lowagie.text.List;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.FileUtil;

@Controller
public class WMSIF000Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF000Service")
	private WMSIF000Service service;

	/*-
	 * Method ID	: crossDomainHttpWsMobile
	 * Method 설명	: 웹서비스 전송 공통모듈
	 * 작성자			: W
	 */
	@RequestMapping("/crossDomainHttpWsMobile.if")
	public ModelAndView crossDomainHttpWs(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String pOrdId		= request.getParameter("pOrdId");
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("pOrdId"		, pOrdId);
		
		try {
			m = service.crossDomainHttpWsMobile(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: crossDomainKakaoSms
	 * Method 설명	: 웹서비스 전송 공통모듈
	 * 작성자			: W
	 */
	@RequestMapping("/crossDomainKakaoSms.if")
	public ModelAndView crossDomainKakaoSms(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String data		= request.getParameter("data");
		String flag		= request.getParameter("flag");
		model.put("data", data);
		if(flag == null) {
			flag = "";
		}
		try {
			if(flag.equals("DLV_REQ")) {
				m = service.crossDomainKakaoSms2(model);
			} else {
				m = service.crossDomainKakaoSms(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}

	
	
	
	
	/*-
	 * Method ID	: crossDomain
	 * Method 설명	: 웹서비스 전송 공통모듈 (KCC)
	 * 작성자			: W
	 */
	@RequestMapping("/crossDomainHttpWsKcc.if")
	public ModelAndView crossDomainHttpWsKcc(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String jsonString	= request.getParameter("formData");
		String data		= request.getParameter("data");
		
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("jsonString"	, jsonString);
		model.put("data", data);
		
		try {
			m = service.crossDomainHttpWsKcc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID	: crossDomain
	 * Method 설명	: 웹서비스 전송 공통모듈 5200port
	 * 작성자			: yhku
	 */
	
	@RequestMapping("/crossDomainHttpWs5200.if")
	public ModelAndView crossDomainHttpWs5200(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String jsonString	= request.getParameter("formData");
		String data		= request.getParameter("data");
		
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("jsonString"	, jsonString);
		model.put("data", data);
		
		try {
			m = service.crossDomainHttpWs5200(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	 * Method ID	: ifNcodeSendKakaoMessage
	 * Method 설명	: 엔코드 메세지 전송 
	 * 작성자			: yhku
	 */
	
	@RequestMapping("/IF_NCODE_SEND_KAKAO_MESSAGE.if")
	public ModelAndView ifNcodeSendKakaoMessage(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			
			String cMethod		= "POST";
			String cUrl ="NCODE/SEND/KAKAO_SMS";
			String hostUrl = request.getServerName();
			
			String dataJson = request.getParameter("jsonData");
			model.put("data", dataJson);
			model.put("cMethod"		, cMethod);
			model.put("cUrl"		, cUrl);
			model.put("hostUrl"		, hostUrl);
			
			m = service.crossDomainHttpWs5200(model);
			
			String retunJson = (String)m.get("RESULT");
			
			if(retunJson != null){
				JSONParser parser = new JSONParser();
				Map json = (Map)parser.parse(retunJson);
				Iterator iter = json.entrySet().iterator(); 
				String entry = (String)json.get("R_CODE");
				
				if(entry != null && entry.equals("SUCCESS")){ //{"R_CODE":"SUCCESS"}
					m.put("errCnt", 0);
			        m.put("MSG", MessageResolver.getMessage("save.success"));
			        
				}else{ //{"R_CODE":"ERROR"
					m.put("errCnt", 1);
					m.put("MSG", MessageResolver.getMessage("save.error"));
				}
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("errCnt", 1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID	: crossDomain
	 * Method 설명	: 웹서비스 전송 공통모듈 5200port_UCPS
	 * 작성자			: ykim
	 */
	@RequestMapping("/crossDomainHttpWs5200_UCPS.if")
	public ModelAndView crossDomainHttpWs5200_UCPS(Map<String, Object> model, @RequestBody String request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			JSONParser parser = new JSONParser();
			Map<String, Object> json = (Map)parser.parse(request);
			
			String cMethod		= "POST";
			String cUrl			= "UCPS_SEND/UCPS_INTERFACE";
			String hostUrl = "localhost";
			
			model.put("data"		, json.get("jsonData"));
			model.put("cMethod"		, cMethod);
			model.put("cUrl"		, cUrl);
			model.put("hostUrl"		, hostUrl);
			m = service.crossDomainHttpWs5200_UCPS(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID	: ifProductivityList
	 * Method 설명	: 새로피엔엘 생산성입력/조회 모바일 리스트 
	 * 작성자			: schan
	 */
	@RequestMapping("/IF_PRODUCTIVITY_LIST.if")	
	public void ifProductivityList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
					model.put("LC_ID", request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
				}
				if (request.getParameterMap().get("WORK_DT") != null) {
					model.put("WORK_DT", request.getParameterMap().get("WORK_DT")[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO) != null) {
					model.put("USER_NO", request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO)[0]);
				}
				if (request.getParameterMap().get("COMPANY_CD") != null) {
					model.put("COMPANY_CD", request.getParameterMap().get("COMPANY_CD")[0]);
				}
				m = service.ifProductivityList(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	/*-
	 * Method ID	: ifProductivityListMonth
	 * Method 설명	: 새로피엔엘 생산성 조회(월간) 모바일 리스트 
	 * 작성자			: schan
	 */
	@RequestMapping("/IF_PRODUCTIVITY_LIST_MONTH.if")	
	public void ifProductivityListMonth(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
					model.put("LC_ID", request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
				}
				if (request.getParameterMap().get("WORK_DT_MONTH") != null) {
					model.put("WORK_DT_MONTH", request.getParameterMap().get("WORK_DT_MONTH")[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO) != null) {
					model.put("USER_NO", request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO)[0]);
				}
				if (request.getParameterMap().get("COMPANY_CD") != null) {
					model.put("COMPANY_CD", request.getParameterMap().get("COMPANY_CD")[0]);
				}
				m = service.ifProductivityListMonth(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	/*-
	 * Method ID	: ifProductivityList
	 * Method 설명	: 새로피엔엘 생산성입력/조회 모바일 사용자 이름 검색 
	 * 작성자			: schan
	 */
	@RequestMapping("/IF_PRODUCTIVITY_USER_NM.if")	
	public void ifProductivityUserNm(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
					model.put("SS_SVC_NO", request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO) != null) {
					model.put("vrSrchUserNo", request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO)[0]);
				}
				
				m = service.ifProductivityUserNm(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	/*-
	 * Method ID	: ifProductivityListUpdate
	 * Method 설명	: 새로피엔엘 생산성입력/조회 모바일 데이터 저장 
	 * 작성자			: schan
	 */
	@RequestMapping("/IF_PRODUCTIVITY_LIST_UPDATE.if")	
	public void ifProductivityListUpdate(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO) != null) {
					model.put(ConstantWSIF.IF_KEY_ROW_USER_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_USER_NO)[0]);
				}
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
					model.put("LC_ID", request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
				}
				if (request.getParameterMap().get("WORK_DT") != null) {
					model.put("WORK_DT", request.getParameterMap().get("WORK_DT")[0]);
				}
				if (request.getParameterMap().get("init_edit_flag") != null) {
					model.put("init_edit_flag", request.getParameterMap().get("init_edit_flag")[0]);
				}
				if (request.getParameterMap().get("T_ETC_BYEONGYEONG") != null) {
					model.put("T_ETC_BYEONGYEONG", request.getParameterMap().get("T_ETC_BYEONGYEONG")[0]);
				}
				if (request.getParameterMap().get("T_ETC_JAEGO") != null) {
					model.put("T_ETC_JAEGO", request.getParameterMap().get("T_ETC_JAEGO")[0]);
				}
				if (request.getParameterMap().get("T_ETC_JEONSAN") != null) {
					model.put("T_ETC_JEONSAN", request.getParameterMap().get("T_ETC_JEONSAN")[0]);
				}
				if (request.getParameterMap().get("T_ETC_SOBUN") != null) {
					model.put("T_ETC_SOBUN", request.getParameterMap().get("T_ETC_SOBUN")[0]);
				}
				if (request.getParameterMap().get("T_IN_BANPUM") != null) {
					model.put("T_IN_BANPUM", request.getParameterMap().get("T_IN_BANPUM")[0]);
				}
				if (request.getParameterMap().get("T_IN_BOCHUNG") != null) {
					model.put("T_IN_BOCHUNG", request.getParameterMap().get("T_IN_BOCHUNG")[0]);
				}
				if (request.getParameterMap().get("T_IN_IBHA") != null) {
					model.put("T_IN_IBHA", request.getParameterMap().get("T_IN_IBHA")[0]);
				}
				if (request.getParameterMap().get("T_IN_JEOGCHI") != null) {
					model.put("T_IN_JEOGCHI", request.getParameterMap().get("T_IN_JEOGCHI")[0]);
				}
				if (request.getParameterMap().get("T_OUT_CHULHA") != null) {
					model.put("T_OUT_CHULHA", request.getParameterMap().get("T_OUT_CHULHA")[0]);
				}
				if (request.getParameterMap().get("T_OUT_GEOMSU") != null) {
					model.put("T_OUT_GEOMSU", request.getParameterMap().get("T_OUT_GEOMSU")[0]);
				}
				if (request.getParameterMap().get("T_OUT_PIKING") != null) {
					model.put("T_OUT_PIKING", request.getParameterMap().get("T_OUT_PIKING")[0]);
				}
				if (request.getParameterMap().get("T_OUT_POJANG") != null) {
					model.put("T_OUT_POJANG", request.getParameterMap().get("T_OUT_POJANG")[0]);
				}
				m = service.ifProductivityListUpdate(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	
	/*-
	 * Method ID	: crossDomain
	 * Method 설명	: 웹서비스 전송 공통모듈 5200por - WCSt
	 * 작성자		: KSJ
	 */
	
	@RequestMapping("/crossDomainHttpWs5200WCS.if")
	public ModelAndView crossDomainHttpWs5200WCS(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String jsonString	= request.getParameter("formData");
		String data			= request.getParameter("data");
		
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("jsonString"	, jsonString);
		model.put("data", data);
		
		try {
			m = service.crossDomainHttpWs5200WCS(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	 * Method ID	: crossDomain
	 * Method 설명	: 웹서비스 전송 공통모듈 5200por - WCSt
	 * 작성자		: KSJ
	 */
	
	@RequestMapping("/crossDomainHttpWs5200WCSNew.if")
	public ModelAndView crossDomainHttpWs5200WCSNew(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String jsonString	= request.getParameter("formData");
		String data			= request.getParameter("data");
		
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("jsonString"	, jsonString);
		model.put("data", data);
		
		try {
			m = service.crossDomainHttpWs5200WCSNew(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
     * Method ID    : crossDomainHttpWsMobile
     * Method 설명    : 웹서비스 전송 공통모듈
     * 작성자          : W
     */
    @RequestMapping("/crossDomainHttpWsMobileNew5400.if")
    public ModelAndView crossDomainHttpWsNew5400(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        
        String cMethod      = request.getParameter("cMethod");
        String cUrl         = request.getParameter("cUrl");
        String pOrdId       = request.getParameter("pOrdId");
        model.put("cMethod"     , cMethod);
        model.put("cUrl"        , cUrl);
        model.put("pOrdId"      , pOrdId);
        
        try {
            m = service.crossDomainHttpWs5400New(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
	/*-
     * Method ID    : crossDomainHttpWsMobile
     * Method 설명    : 웹서비스 전송 공통모듈
     * 작성자          : W
     */
    @RequestMapping("/crossDomainHttpWcsMobileNew5400.if")
    public ModelAndView crossDomainHttpWcsNew5400(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        
        String cMethod      = request.getParameter("cMethod");
        String cUrl         = request.getParameter("cUrl");
        String pOrdId       = request.getParameter("pOrdId");
        String pOrdType     = request.getParameter("pOrdType");
        String pLocType     = request.getParameter("pLocType");
        String pPltType     = request.getParameter("pPltType");
        String pLotNo 		= request.getParameter("pLotNo");
        String pLocation 		= request.getParameter("pLocation");
        
        model.put("cMethod"     , cMethod);
        model.put("cUrl"        , cUrl);
        model.put("pOrdId"      , pOrdId);
        model.put("pOrdType"      , pOrdType);
        model.put("pLocType"      , pLocType);
        model.put("pPltType"      , pPltType);
        model.put("pLotNo"		,pLotNo);
        model.put("pLocation"		,pLocation); 
        
        try {
            m = service.crossDomainHttpWcs5400New(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    
    @RequestMapping("/crossDomainHttpWsSwagger.if")
    public void crossDomainHttpWsSwagger(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        BufferedOutputStream bos = null;
        HttpURLConnection con = null;
        Map<String, Object> m = new HashMap<String, Object>();
        
        String resultJson = "";
        
        String port       = request.getParameter("port");
        String uri       = request.getParameter("uri");

        try {
            
            String getUrl   = "http://10.30.1.14:"+port+"/"+uri+"?swagger.json";
            
            URL url = null; 
            url = new URL(getUrl);
            
            
            con = (HttpURLConnection) url.openConnection();
            
            //con.setRequestProperty("Authorization", basicAuth);
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod("GET");
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Content-Type"   , "application/json;charset=UTF-8");
            con.setRequestProperty("Accept"         , "application/json");
                        
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json");
                        
            bos = new BufferedOutputStream(response.getOutputStream());
            
            try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "euc-kr"))) {
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    bos.write(responseLine.getBytes("utf-8"));
                }
            }
            bos.flush();
                     
        } catch (Exception e) {
            e.printStackTrace();
            
        }finally{
            if (bos != null) {
                try {
                    bos.close();
                } catch (Exception ie) {
                }
            }
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception ise) {
                }
            }
        }

    }
    
    
	/*-
     * Method ID    : crossDomainHttpWsMobile
     * Method 설명    : 웹서비스 전송 공통모듈
     * 작성자          : W
     */
    @RequestMapping("/crossDomainHttpWsMobileNew5100.if")
    public ModelAndView crossDomainHttpWsNew5100(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = new HashMap<String, Object>();
        
        String cMethod       = request.getParameter("cMethod");
        String cUrl          = request.getParameter("cUrl");
        String InReqDt       = request.getParameter("InReqDt");
        String ItemCd        = request.getParameter("ItemCd");
        String OrdQty        = request.getParameter("OrdQty");
        String DlvyCd        = request.getParameter("DlvyCd");
        String ItemNm        = request.getParameter("ItemNm");
        
        model.put("cMethod"     , cMethod);
        model.put("cUrl"        , cUrl);
        model.put("InReqDt"      , InReqDt);
        model.put("ItemCd"      , ItemCd);
        model.put("OrdQty"      , OrdQty);
        model.put("DlvyCd"      , DlvyCd);
        model.put("ItemNm"      , ItemNm);
        
        try {
            m = service.crossDomainHttpWs5100NewPOP(model);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        mav.addAllObjects(m);
        return mav;
    }
	
}