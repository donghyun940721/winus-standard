package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF020Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF020Service")
	private WMSIF020Service service;
	
	/*-
	 * Method ID : pickingOrderQry 
	 * Method 설명 : 피킹지시 JSON 송신
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_PICKING_LIST_QRY.if")
	public void getPickingList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_PICKING_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_PICKING_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF020");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}
				m = service.selectPickingList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : pickingOrderQry 
	 * Method 설명 : 피킹지시 JSON 송신
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_PICKING_QRY.if")
	public void pickingOrderQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_PICKING_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_PICKING_QRY);
			model.put("D_DOC_CODE", "WMSIF020");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_WORK_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
					model.put(ConstantWSIF.IF_KEY_REGIST_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
				}

				//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
					//model.put(ConstantWSIF.IF_KEY_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ)[0]);
				//}

				m = service.selectPickingQry(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}

	/*-
	 * Method ID : pickingOrderComplete 
	 * Method 설명 : JSON데이터 수신, 피킹확정 처리 진행
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_PICKING_COMPLETE.if")
	public void pickingOrderComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_PICKING_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_PICKING_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF020");

			service.receiveJSON(model, inputJSON);

			service.updatePickingOrderComplete(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}

	
	@RequestMapping("/IF_OUT_SHIPPING_LIST_QRY.if")
	public void getShippingList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_SHIPPING_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_SHIPPING_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF020");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}
				m = service.selectShippingingList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : shippingQry 
	 * Method 설명 : 출하지시 JSON 송신
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_SHIPPING_QRY.if")
	public void shippingQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_SHIPPING_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_SHIPPING_QRY);
			model.put("D_DOC_CODE", "WMSIF020");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_WORK_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
					model.put(ConstantWSIF.IF_KEY_REGIST_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
				}

				m = service.selectSearchList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}

	/*-
	 * Method ID : outComplete 
	 * Method 설명 : JSON데이터 수신, 출하처리 진행  
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_SHIPPING_COMPLETE.if")
	public void outComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_SHIPPING_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_SHIPPING_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF020");

			service.receiveJSON(model, inputJSON);

			service.updateOutComplete(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}

	/*-
	 * Method ID : mappingComplete 
	 * Method 설명 : JSON데이터 수신, 매핑확정처리 진행  
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_MAPPING_COMPLETE.if")
	public void mappingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_MAPPING_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_MAPPING_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF020");

			service.receiveJSON(model, inputJSON);

			service.updateMappingComplete(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/

	}

	/*-
	 * Method ID : deleteMappingComplete 
	 * Method 설명 : JSON데이터 수신, 매핑취소처리 진행 
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_MAPPING_DELETE_COMPLETE.if")
	public void deleteMappingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_MAPPING_DELETE_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_MAPPING_DELETE_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF020");

			service.receiveJSON(model, inputJSON);

			service.updateMappingDeleteComplete(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}

}
