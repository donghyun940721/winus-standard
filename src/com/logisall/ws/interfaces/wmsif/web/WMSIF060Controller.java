package com.logisall.ws.interfaces.wmsif.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.util.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF060Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF060Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF060Service")
	private WMSIF060Service service;
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service WMSIF000Service;
	
	/*-
	 * Method ID : getOutOrderQry 
	 * Method 설명 : 출고주문조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_QRY.if")	
	public void getOutOrderQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_QTY) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_QTY, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_QTY)[0]);
			}
			
			m = service.selectOutOrderQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getCwErrOrderQry 
	 * Method 설명 : 에러 주문 조회
	 * 작성자 : 
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_ERR_ORDER_QRY.if")	
	public void getCwErrOrderQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		
		/* EAI WCS 설비 상태 최신화 */
		Map<String, Object> eaiMap = new HashMap<String, Object>();
		String cMethod   = "POST";
        String cUrl      = "WCS_MFC/MFC/SYNC_DEVICE_STATUS_U";
           
        eaiMap.put("cMethod"   , cMethod);
        eaiMap.put("cUrl"      , cUrl);
        eaiMap.put("data"      , "");
        
        WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
		
		/* EAI WCS 에러 상태 최신화 */
        cUrl      = "WCS_MFC/MFC/SYNC_ERR_JOB_ORDER";
           
        eaiMap.put("cMethod"   , cMethod);
        eaiMap.put("cUrl"      , cUrl);
        eaiMap.put("data"      , "");
        
        WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
        
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}	
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.cwSelectErrOrderQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getOutOrderDetailQry 
	 * Method 설명 : 출고주문상세조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_DETAIL_QRY.if")	
	public void getOutOrderDetailQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
						
			m = service.selectOutOrderDetailQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getOutOrderMappingQry 
	 * Method 설명 : 출고주문매핑조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_MAPPING_QRY.if")	
	public void getOutOrderMappingQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectOutOrderMappingQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getOutOrderConfirmQry 
	 * Method 설명 : 출고 주문 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_CONFIRM_QRY.if")	
	public void getOutOrderConfirmQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			m = service.selectOutOrderConfirmQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getShippingOrderStatQry 
	 * Method 설명 : 당일 출고주문 상태 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_SHIPPING_ORDER_STAT_QRY.if")	
	public void getShippingOrderStatQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			m = service.selectShippingOrderStatQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : shippingOutPltComplete
	 * Method 설명 : 물류용기출고확정
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_IN_SHIPPING_PLT_COMPLETE.if")
	public void shippingInPltComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateShippingInPltComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : shippingOutComplete
	 * Method 설명 : 상품출고확정
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_IN_SHIPPING_COMPLETE_KR.if")
	public void shippingInComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateShippingInComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getOutOrderQry 
	 * Method 설명 : 출고주문조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ASN_ORDER_QRY.if")	
	public void getOutAsnOrderQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectOutAsnOrderQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getOutOrderDetailQry 
	 * Method 설명 : 출고주문상세조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ASN_ORDER_DETAIL_QRY.if")	
	public void getOutAsnOrderDetailQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
						
			m = service.selectOutAsnOrderDetailQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : shippingOutComplete
	 * Method 설명 : 상품출고확정
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_SHIPPING_COMPLETE_ASN.if")
	public void outShippingCompleteAsn(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutShippingCompleteAsn(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getOutOrderQry 
	 * Method 설명 : 출고주문조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_LOT_ORDER_QRY.if")	
	public void getOutLotOrderQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectOutLotOrderQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : shippingOutComplete
	 * Method 설명 : 상품출고확정
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_SHIPPING_COMPLETE_LOT.if")
	public void outShippingCompleteLot(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutShippingCompleteLot(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getOutAutoOrderStatQry 
	 * Method 설명 : 자동출고주문상태조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_AUTO_ORDER_STAT.if")	
	public void getOutAutoOrderStatQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}		
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectOutAutoOrderStatQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : getOutAutoOrderChangeQry 
	 * Method 설명 : 자동출고주문상태변경
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_AUTO_ORDER_CHANGE.if")	
	public void getOutAutoOrderChangeQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectOutAutoOrderChangeQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : getOutOrderInsertQry 
	 * Method 설명 : 출고주문입력을 위한 상품코드 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_INSERT_QRY.if")	
	public void getOutOrderInsertQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectOutOrderInsertQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : outAutoOrderInsert
	 * Method 설명 : 출고주문자동입력
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_AUTO_ORDER_INSERT.if")
	public void outAutoOrderInsert(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutAutoOrderInsert(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getOutOrderFeedingQry 
	 * Method 설명 : 출고 feeding 주문 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_FEEDING_QRY.if")	
	public void getOutOrderFeedingQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectOutOrderFeedingQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : outFeedingComplete
	 * Method 설명 : 출고확정(FEEDING)
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_FEEDING_COMPLETE.if")
	public void outFeedingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutFeedingComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : outFeedingCompleteForce
	 * Method 설명 : 강제로 주문등록부터 출고처리까지 수행
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_FEEDING_COMPLETE_FORCE.if")
	public void outFeedingCompleteForce(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutFeedingCompleteForce(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			
			}
		}
	}
	
	/*-
	 * Method ID : getOutOrderFeedingFifoQry 
	 * Method 설명 : 출고 feeding LOT 선입선출 주문 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_FEEDING_FIFO_QRY.if")	
	public void getOutOrderFeedingFifoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectOutOrderFeedingFifoQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : outFeedingCompleteFifo
	 * Method 설명 : 출고확정(FEEDING_FIFO)
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_FEEDING_COMPLETE_FIFO.if")
	public void outFeedingCompleteFifo(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutFeedingCompleteFifo(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getOutEpcMapQry 
	 * Method 설명 : 임가공 물류용기 매핑 정보 확인
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_EPC_MAP_QRY.if")	
	public void getOutEpcMapQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			
			m = service.selectOutEpcMapQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : outCompleteSetForce
	 * Method 설명 : 강제로 주문등록부터 출고처리까지 수행(임가공)
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_COMPLETE_SET_FORCE.if")
	public void outCompleteSetForce(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutCompleteSetForce(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			
			}
		}
	}
	
	/*-
	 * Method ID : getOutFroceLotOrdQry 
	 * Method 설명 : 출고주문기준 재고이동 정보 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_FORCE_LOT_ORD_QRY.if")	
	public void getOutFroceLotOrdQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
						
			
			m = service.selectOutFroceLotOrdQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : outInordComplete
	 * Method 설명 : 주문이 등록되어 있는 상태에서의 출고처리(로케이션지정, 피킹리스트발행, 출고확정)
	 * DATE : 2018-06-25
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_INORD_COMPLETE.if")
	public void outInordComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateoutInordComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}		
		
	/*-
	 *  Method ID : getOutInordProcessQry 
	 *  Method 설명 : 출고주문기준 로케이션지정 정보 조회
	 *  Date : 2018-06-26
	 *  작성자 : smics
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_INORD_PROCESS_QRY.if")	
	public void getOutInordProcessQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_QTY) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_QTY, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_QTY)[0]);
			}
			
			
			m = service.selectOutProcessQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 *  Method ID : getOutLocRemainQry 
	 *  Method 설명 : 로케이션 지정 안된 주문 조회
	 *  Date : 2018-06-26
	 *  작성자 : smics
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_LOC_REMAIN_QRY.if")	
	public void getOutLocRemainQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
						
			m = service.selectOutLocRemainQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 *  Method ID : getOutOrdRemainQry 
	 *  Method 설명 : 출고주문기준 츌고 미처리 정보 조회
	 *  Date : 2018-06-26
	 *  작성자 : smics
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORD_REMAIN_QRY.if")	
	public void getOutOrdRemainQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			m = service.selectOutOrdRemainQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : outInordProcessComplete
	 * Method 설명 : 주문이 등록되어 있는 상태에서의 로케이션 지정처리(로케이션지정, 피킹리스트발행)
	 * DATE : 2018-06-25
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_INORD_PROCESS_COMPLETE.if")
	public void outInordProcessComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateoutInordProcessComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	


	/*-
	 *  Method ID : getOutOrderLotQry 
	 *  Method 설명 : 매핑이 되어 있지 않는 주문에 대한 LOT 정보 조회
	 *  Date : 2018-06-27
	 *  작성자 : smics
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORDER_LOT_QRY.if")	
	public void getOutOrderLotQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}

			m = service.selectOutOrderLotQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 *  Method ID : getLotOverlapConfirmQry 
	 *  Method 설명 : LOT 중복 조회
	 *  Date : 2018-07-28
	 *  작성자 : smics
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LOT_OVERLAP_CONFIRM_QRY.if")	
	public void getLotOverlapConfirmQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectLotOverlapConfirmQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	

	/*-
	 * Method ID : outCompleteBoxForce
	 * Method 설명 : 강제로 주문등록부터 출고처리까지 수행(BOX주문)
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_COMPLETE_BOX_FORCE.if")
	public void outCompleteBoxForce(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutCompleteBoxForce(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			
			}
		}
	}

	/*-
	 * Method ID : getOutOrdIdOrderQry 
	 * Method 설명 : 출고주문조회(주문번호, seq)
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ORD_ID_ORDER_QRY.if")	
	public void getOutOrdIdOrderQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectOutOrdIdOrderQry(model);
			
//			if((request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]).equals("ANYWARE")){
//				service.fcmNaviSender(m);
//			}
		}
		
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("1.Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("2.Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			//log.info(" ********** (111)response ====================================: " + response);
			//log.info(" ********** (122)m ====================================: " + m);
			//log.info(" ********** (133)paramMap ====================================: " + model);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getTotalPickingList 
	 * Method 설명 : 모바일 1227.피킹리스트 조회(피킹지기준) 리스트 조회
	 * 작성자 : wl2258
	 * 
	 * @param request, response
	 * 
	 */
	@RequestMapping("/IF_TOTAL_PICKING_LIST_QRY.if")	
	public void getTotalPickingList(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectTotalPickingList(model);
		}
		
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("1.Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("2.Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : outPickingComplete
	 * Method 설명 : 피킹확정
	 * DATE : 2019-01-04
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_PICKING_COMPLETE.if")
	public void outPickingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updatepickingComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	

	/*-
	 * Method ID : receiveJSONWithFiles
	 * Method 설명 : 파일을 포함한 Json request 의 저장함수
	 * 작성자 : kdalma
	 *
	 * @param model : mulitpart form request 객체
	 * @param inputJSON : file 이외의 json 데이터
	 * @param files : 첨부된 파일 array
	 * @throws Exception
	 */
	public void saveImageFromBase64(String key, String fileName, String base64Data, String ord_id) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model2 = new HashMap<String, Object>();
		
		try {
			if(base64Data == null) return;
			
			String path = ConstantWSIF.WMS_IMG_PATH + DateUtil.getDateByPattern2() +"/"+ key;
			String file_id = key+fileName;
			InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));
			
			model2.put("ORD_ID", ord_id);
			model2.put("FILE_ID", file_id);
			model2.put("ATTACH_GB", "WMS");
			model2.put("FILE_VALUE", "WMSOP");
			model2.put("FILE_PATH", path);
			model2.put("FILE_NAME", fileName);
			model2.put("FILE_EXT", ".jpg");
			
			try {
				m = service.WmsImgDataInsert(model2);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save confirm :", e);
				}
				m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}
	
	/*-
	 * Method ID : shippingPickedComplete
	 * Method 설명 : 피킹 확정 후 출고확정(이미지 처리)
	 * DATE : 2019-01-04
	 * 작성자 : SMICS
	 *
	 * @param request : 
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SHIPPING_PICKED_COMPLETE.if")
	public void shippingPickedComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[9];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF090");

			service.receiveJSON(model, inputJSON);

			service.updateShippingPickedComplete(model);
			
			// Save photo files
			for(int index=0;index<9;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	
	/*-
	 * Method ID : pickingCheckedComplete
	 * Method 설명 : 피킹 확정 (이미지 처리)
	 * DATE : 2019-07-30
	 * 작성자 : SMICS
	 *
	 * @param request : 
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_PICKING_CHECKED_COMPLETE.if")
	public void pickingCheckedComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[8];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF090");

			service.receiveJSON(model, inputJSON);

			service.updatePickingCheckedComplete(model);
			
			// Save photo files
			for(int index=0;index<8;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : getOutLotOrderIdQry 
	 * Method 설명 : LOT번호로 출고 주문 정보 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_LOT_ORDER_ID_QRY.if")	
	public void getOutLotOrderIdQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			m = service.selectOutLotOrderIdQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		

	/*-
	 * Method ID : getOutLotUnitOrderIdQry 
	 * Method 설명 : LOT번호로 출고 주문 정보 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_LOT_UNIT_ORDER_ID_QRY.if")	
	public void getOutLotUnitOrderIdQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ)[0]);
			}
			
			m = service.selectOutLotUnitOrderIdQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : shippingPickedUnitComplete
	 * Method 설명 : 피킹 확정 후 출고확정(이미지 처리)
	 * DATE : 2019-01-04
	 * 작성자 : SMICS
	 *
	 * @param request : 
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SHIPPING_PICKED_UNIT_COMPLETE.if")
	public void shippingPickedUnitComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[6];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF090");

			service.receiveJSON(model, inputJSON);

			service.updateShippingPickedUnitComplete(model);
			
			// Save photo files
			for(int index=0;index<6;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : PickingTransCustMobile
	 * Method 설명 : 피킹확정(거래처별)
	 * DATE : 2019-01-04
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_PICKING_TRANS_CUST_MOBILE.if")
	public void PickingTransCustMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updatepickingTransCustMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : outMultiShippingComplete
	 * Method 설명 : 출고확정(다중 ord, seq)
	 * DATE : 2019-02-22
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_MULTI_SHIPPING_COMPLETE_MOBILE.if")
	public void outMultiShippingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateMultiShippingComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : getOutLotConfirmQry 
	 * Method 설명 : LOT일치 여부확인 및 로케이션 변경 로직
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_LOT_CONFIRM_QRY.if")	
	public void getOutLotConfirmQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectOutLotConfirmQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : outShippingCompleteMobile
	 * Method 설명 : 출고확정
	 * DATE : 2019-06-06
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SHIPPING_COMPLETE_MOBILE.if")
	public void outShippingCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			//log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateShippingCompleteMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : outCancelPickingMobile
	 * Method 설명 : 피킹취소모바일
	 * DATE : 2019-08-05
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_CANCEL_PICKING_MOBILE.if")
	public void outCancelPickingMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			//log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateCancelPickingMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			log.info("model==============="+model);
			service.deleteImgFolder(model);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : outSetLocMappingOutMobile
	 * Method 설명 : 출고주문이 있을 때 LOT정보로 로케이션 지정, 피킹리스트 발행, 매핑, 출고 처리
	 * DATE : 2019-08-12
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SET_LOC_MAPPING_OUT_MOBILE.if")
	public void outSetLocMappingOutMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			//log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateSetLocMappingOutMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			log.info("model==============="+model);
			service.deleteImgFolder(model);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : simpleOutMobile
	 * Method 설명 : 모바일 간편 출고
	 * DATE : 2019-11-06
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SIMPLE_OUT_MOBILE.if")
	public void simpleOutMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updatesimpleOutMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

		/*-
	 * Method ID : outLotLocCompleteMobile
	 * Method 설명 : 모바일 - lot 번호 지정 출고
	 * DATE : 2023-09-19
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_LOT_LOC_COMPLETE_MOBILE.if")
	public void outLotLocCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOutLotLocCompleteMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : OrdersimpleOutMobile
	 * Method 설명 : 주문입력 후 모바일 간편 출고
	 * DATE : 2019-11-14
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_ORDER_SIMPLE_OUT_MOBILE.if")
	public void OrdersimpleOutMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOrdersimpleOutMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : getOutPickingListQry 
	 * Method 설명 : 피킹리스트 조회 쿼리
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_PICKING_LIST_INFO_QRY.if")	
	public void getOutPickingListInfoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectOutPickingListInfoQry(model);
			if(!m.isEmpty()){
				//m => {DATA=[{...}]  형태로 담긴것에서 item_bar_cd 를 key로 갖는 Scan 검증용 맵을 생성해서 전달. - 210929 ykim
				List<Map<String, Object>> castMap = new ArrayList<Map<String, Object>>();  
				castMap.addAll((ArrayList)m.get("DATA"));
				
				Map<String, Object> invoiceMap = new HashMap<String, Object>();
				
				//STEP1. 전체 피킹리스트 루핑
				for(int i = 0; i < castMap.size(); i++){
					
					HashMap<String, Object> barcdMap = new HashMap<String, Object>();
					//STEP2. 송장번호가 같은 제품이 있는지 조회.
					if(invoiceMap.containsKey(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_REMARK)))){
						
						//STEP3. 같은송장번호로 이미 등록되어있는게 두건 이상인지(이미 ArrayList로 셋팅되어있는지 체크)
						if(!(invoiceMap.get(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_REMARK))) instanceof ArrayList<?>)){
							//STEP3-1. ArrayList형태가 아니라면 들어있는 Map을 List<Map>에 add 하여 다시 put.
							List<Map<String, Object>> innerBarcodeMap = new ArrayList<Map<String, Object>>();  
							innerBarcodeMap.add((Map)invoiceMap.get(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_REMARK))));
							
							barcdMap.put(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD)), castMap.get(i));
							innerBarcodeMap.add(barcdMap);
							
							invoiceMap.put(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_REMARK)), innerBarcodeMap);
						}else{
							//STEP3-2. 이미 ArrayList형태로 되어있다면(두건 이상이라면) 해당 List<Map>에 add하여 다시 put.
							
							List<Map<String, Object>> innerBarcodeMap = new ArrayList<Map<String, Object>>();
							innerBarcodeMap.addAll((List<Map<String, Object>>)invoiceMap.get(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_REMARK))));

							barcdMap.put(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD)), castMap.get(i));
							innerBarcodeMap.add(barcdMap);
							
							invoiceMap.put(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_REMARK)), innerBarcodeMap);
						};
					}else{
						barcdMap.put(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD)), castMap.get(i));
						invoiceMap.put(String.valueOf(castMap.get(i).get(ConstantWSIF.IF_KEY_ROW_REMARK)), barcdMap);
					}
				}
				m.put("KEYMAP", invoiceMap);
			}
			
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		

	/*-
	 * Method ID : MappingPickingComplete
	 * Method 설명 : 주문이 입력되어 있는 상태에서 로케이션 지정, 매핑, 피킹리스트 발행, 피킹확정 처리
	 * DATE : 2020-02-20
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_LOC_MAPPING_PICKING_COMPLETE.if")
	public void LocMappingPickingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateLocMappingPickingComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : ordInsSimpleOut
	 * Method 설명 : 출고 주문 입력 후 간편 출고
	 * DATE : 2020-06-21
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_ORD_INS_SIMPLE_OUT.if")
	public void ordInsSimpleOut(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateOrdInsSimpleOut(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
     * Method ID : ordInsSimpleOut
     * Method 설명 : 출고 주문 입력 후 간편 출고
     * DATE : 2020-06-21
     * 작성자 : SMICS
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_ORD_INS_SIMPLE_OUT_SATORI.if")
    public void ordInsSimpleOutSatori(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
            
            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF060");
            
            log.info("inputJSON==============="+inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateOrdInsSimpleOutSatori(model);
        
        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            if(Integer.parseInt(model.get("RETURN_CD").toString()) == 105) {
            	response.setCharacterEncoding("UTF-8");
                PrintWriter printWriter = null;
                printWriter = response.getWriter();
                
                String sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S).append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_START).append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
                        .append("\"").append("LOTerror").append("\"")
                        .append(",\"").append("RETURN_MSG").append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
                        .append("\"").append(model.get("RETURN_MSG").toString()).append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E)
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString(); // sendData = "{\"SEND_DATA\":\"SUCCESS\",\"RETURN_MSG\":\"변수\"}";
                
                printWriter.println(sendData);
                printWriter.close();
            }else{
            	service.sendResponse(response, model, m);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    @RequestMapping("/IF_TRANS_CUST_ID.if")
    public void getTakeTransCustId(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                m = service.selectTransCustId(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

	/*-
	 * Method ID : LotLocShippingComplete
	 * Method 설명 : 수신된 LOT번호로 로케이션 지정 후 출고확정
	 * DATE : 2020-06-30
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_LOT_LOC_SHIPPING_COMPLETE.if")
	public void LotLocShippingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateLotLocShippingComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : shippingEdiyaNotDepart
	 * Method 설명 : 이디야 미배송 차량 데이터 전송(이미지 포함)
	 * 작성자 :  MonkeySeok
	 * 날 짜 : 2020-09-09
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SHIPPING_EDIYA_NOT_DEPART.if")
	public void shippingEdiyaNotDepart(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[9];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			
			String key = request.getParameter("key");
//			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF050");

			service.receiveJSON(model, inputJSON);

			service.updateshippingEdiyaNotDepart(model);
			
			// Save photo files
			for(int index=0;index<9;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
//			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : getOutLotUnitOrderIdQry
	 * Method 설명 : 토탈 피킹리스트 조회 쿼리
	 * 작성자 : smics 
	 * 날  짜 : 2020-10-03
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_TOTAL_PICKING_QRY.if")	
	public void getOutTotalPickingQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LOC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LOC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LOC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectOutTotalPickingQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		

	/*-
	 * Method ID : pickingTotalComplete
	 * Method 설명 : 토탈피킹리스트 조회 후 피킹 확정
	 * DATE : 2020-10-05
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_PICKING_TOTAL_COMPLETE.if")
	public void PickingTotalComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updatePickingTotalComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : shippingSerialComplete
	 * Method 설명 : 시리얼 출고확정(모바일)
	 * 작성자 : SMICS
	 * 날   짜 : 202-12-22
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SHIPPING_SERIAL_COMPLETE.if")
	public void shippingSerialComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateShippingSerialComplete(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}


	/*-
	 * Method ID : crossDomainHttpWsMobile
	 * Method 설명 : CJ올리브영 DHL 전송로직
	 * 작성자 : SMICS
	 * 날   짜 : 2021-04-25
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_DHL_DATA_SENDING.if")
	public void crossDomainHttpWsMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateCrossDomainHttpWsMobile(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
//
//	/*-
//	 * Method ID : LocCancelAlloComplete
//	 * Method 설명 : 토탈피킹리스트 발행 후 로케이션 삭제 및 할당
//	 * DATE : 2020-11-03
//	 * 작성자 : SMICS
//	 *
//	 * @param inputJSON
//	 * @param response
//	 * @throws Exception
//	 */
//	@RequestMapping("/IF_LOC_CANCEL_ALLO_COMPLETE.if")
//	public void LocCancelAlloComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
//		Map<String, Object> m = new HashMap<String, Object>();
//		Map<String, Object> model = new HashMap<String, Object>();
//
//		try {
//			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
//			
//			m.put("SAVE_JSON_FILE", "Y");
//			m.put("SAVE_DOC_INFO", "Y");
//
//			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
//			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
//			m.put("D_DOC_CODE", "WMSIF060");
//			
//			log.info("inputJSON==============="+inputJSON);
//
//			service.receiveJSON(model, inputJSON);
//
//			service.updateLocCancelAlloComplete(model);
//		
//		} catch (InterfaceException ife) {
//			m = new HashMap<String, Object>();
//			m.put("ERRCODE", ife.getMsgCode());
//			m.put("MSG", ife.getMessage());
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to create Work Info :", e);
//			}
//			m = new HashMap<String, Object>();
//			m.put("ERRCODE", -1);
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//
//		try {
//			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
//			service.sendResponse(response, model, m);
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to sendResponse :", e);
//			}
//		}
//	}	
	
	/*-
	 * Method ID : getOutOrderDetailQry 
	 * Method 설명 : 출고주문상세조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_PARCEL_INVC_QRY.if")	
	public void getOutParcelInvcQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectOutParcelInvcQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : PickingCheckComplete
	 * Method 설명 : 피킹작업완료 
	 * DATE : 2021-08-23
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_PICKING_CHECK_COMPLETE.if")
	public void PickingCheckComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updatePickingCheckComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : TempCheckComplete
	 * Method 설명 : 임시 데이터 전송용
	 * DATE : 2021-08-24
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_TEMP_CHECK_COMPLETE.if")
	public void TempCheckComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateTempCheckComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : shippingInvcNoComplete
	 * Method 설명 : 송장 출고 확정
	 * DATE : 2021-08-25
	 * 작성자 : SMICS
	 *
	 * @param request : 
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SHIPPING_INVC_NO_COMPLETE.if")
	public void shippingInvcNoComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[9];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF060");

			service.receiveJSON(model, inputJSON);

			service.updateShippingCompleteMobile(model);
			
			// Save photo files
			for(int index=0;index<9;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : getParcelPickingCntQry 
	 * Method 설명 : 합포장 여부 확인
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_PARCEL_PICKING_CNT_QRY.if")	
	public void getParcelPickingCntQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
						
			m = service.selectParcelPickingCntQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : getParcelDashboardQry 
	 * Method 설명 : 송장피킹검수(수량수정)-메뉴 상단 수량조회쿼리
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_PARCEL_DASHBOARD_QRY.if")	
	public void getParcelDashboardQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			if (request.getParameterMap().get("ord_degree") != null) {
				model.put("ord_degree", request.getParameterMap().get("ord_degree")[0]);
			}
						
			m = service.selectParcelDashboardQry2(model);
			Map<String, Object> temp = service.selectOrdDegree(model);
			m.put("ORD_DEGREE", temp.get("LIST"));
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	/*-
	 * Method ID : getParcelDashboardDetailQry 
	 * Method 설명 : 송장피킹검수(수량수정)-미작업 상세팝업 쿼리.
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_PARCEL_DASHBOARD_DETAIL_QRY.if")	
	public void getParcelDashboardDetailQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			if (request.getParameterMap().get("ord_degree") != null) {
				model.put("ord_degree", request.getParameterMap().get("ord_degree")[0]);
			}
			m = service.selectParcelDashboardQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	/*-
	 * Method ID : TempCheckComplete
	 * Method 설명 : 주문번호 단위별 송장 분리 작업
	 * DATE : 2021-10-27
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_INVC_DIVIDED_COMPLETE.if")
	public void InvcDividedComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateInvcDividedComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : PickingLotCheckComplete
	 * Method 설명 : 피킹작업완료(LOT 검수)
	 * DATE : 2021-08-23
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_PICKING_LOT_CHECK_COMPLETE.if")
	public void PickingLotCheckComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updatePickingLotCheckComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : locSetPickingCompleteMobile
	 * Method 설명 : 주문등록 상태에서 로케이션 추천 후 피킹확정
	 * DATE : 2022-06-21
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_LOC_SET_PICKING_COMPLETE_MOBILE.if")
	public void locSetPickingCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateLocSetPickingCompleteMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}		

	/*-
	 * Method ID : runSpMultiLocSetPickingComplete
	 * Method 설명 : 주문등록상태에서 수신된 정보로 로케이션지정, 피킹리스트 발행
	 * DATE : 2022-07-05
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_MULTI_LOC_SET_PICKING_COMPLETE_MOBILE.if")
	public void multiLocSetPickingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateMultiLocSetPickingComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : WcsShippingProcess
	 * Method 설명 : 창원 WCS 출고 작업 지시
	 * DATE : 2022-08-22
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_WCS_SHIPPING_PROCESS.if")
	public void WcsShippingProcess(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			
			/* EAI WCS 설비 상태 최신화 */
			Map<String, Object> eaiMap = new HashMap<String, Object>();
			String cMethod   = "POST";
	        String cUrl      = "WCS_MFC/MFC/SYNC_DEVICE_STATUS_U";
	           
	        eaiMap.put("cMethod"   , cMethod);
	        eaiMap.put("cUrl"      , cUrl);
	        eaiMap.put("data"      , "");
            
            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
            
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateWcsShippingProcess(model);
			
			/* EAI WCS 주문 작업 전송 */
			cMethod   = "POST";
	        cUrl      = "WCS_MFC/MFC/SYNC_JOB_ORDER";
	           
	        eaiMap.put("cMethod"   , cMethod);
	        eaiMap.put("cUrl"      , cUrl);
	        eaiMap.put("data"      , "");
            
            WMSIF000Service.crossDomainHttpWs5200WCS(eaiMap);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : ShippingCheckCompleteMobile
	 * Method 설명 : 출고 검수 처리
	 * DATE : 2023-05-30
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_SHIPPING_CHECKED_COMPLETE_MOBILE.if")
	public void ShippingCheckCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateShippingCheckCompleteMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : outCompleteMobile
	 * Method 설명 : 출고 확정 처리
	 * DATE : 2024-08-30
	 * 작성자 : 
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_OUT_COMPLETE_MOBILE.if")
	public void outCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.outCompleteMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
     * Method ID : pickingListKeySearch 
     * Method 설명 : 피킹리스트 key log 조회
     * 작성자 : schan
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/PICKINGLIST_KEY_SEARCH.if")  
    public void pickingListKeySearch(HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            if (request.getParameterMap().get("pickingList_key") != null) {
                model.put("pickingList_key" , request.getParameterMap().get("pickingList_key")[0]);
            }
            
            m = service.pickingListKeySearch(model);
            
        }
        
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("1.Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("2.Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            //log.info(" ********** (111)response ====================================: " + response);
            //log.info(" ********** (122)m ====================================: " + m);
            //log.info(" ********** (133)paramMap ====================================: " + model);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }   
    
    
    /*-
     * Method ID : parcelInvcSearchMobile 
     * Method 설명 : 모바일 송장검수 리스트 조회
     * 작성자 : schan
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("/IF_PARCEL_INVC_SEARCH.if")   
    public void parcelInvcSearchMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
              
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
            }
            
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("invc_no") != null) {
                model.put("invc_no", request.getParameterMap().get("invc_no")[0]);
            }
            
            m = service.parcelInvcSearchMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : parcelInvcSearchMobile 
     * Method 설명 : 모바일 입고검수 내역 조회
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_SEARCH_RCV_INSPECTION.if")   
    public void searchRcvInspectionMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
              
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
            }
            
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("ord_id") != null) {
                model.put("ord_id", request.getParameterMap().get("ord_id")[0]);
            }
            
            if (request.getParameterMap().get("box_no") != null) {
                model.put("box_no", request.getParameterMap().get("box_no")[0]);
            }
            
            m = service.searchRcvInspectionMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : searchRcvInspectioListMobile 
     * Method 설명 : 모바일 입고검수 리스트 조회
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_SEARCH_RCV_INSPECTION_LIST.if")   
    public void searchRcvInspectionListMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
              
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
            }
            
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("org_ord_id") != null) {
                model.put("org_ord_id", request.getParameterMap().get("org_ord_id")[0]);
            }
            
            if (request.getParameterMap().get("from_date") != null) {
                model.put("from_date", request.getParameterMap().get("from_date")[0]);
            }
            
            if (request.getParameterMap().get("to_date") != null) {
                model.put("to_date", request.getParameterMap().get("to_date")[0]);
            }
            

            System.out.println(model.toString());
            m = service.searchRcvInspectionListMobile(model);

        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : itemBarcodeSearchMobile 
     * Method 설명 : 아이템 바코드를 통한 상품 정보 조회 
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_SEARCH_ITEM_INFO_BARCODE.if")   
    public void itemBarcodeSearchMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
                
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("item_barcode_cd") != null) {
                model.put("item_barcode_cd", request.getParameterMap().get("item_barcode_cd")[0]);
            }
              
            m = service.itemBarcodeSearchMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    

    /*-
     * Method ID : tempOrdInspectionSearchMobile 
     * Method 설명 : WMSCK050 임시원주문 내역 조회(입고)
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_SEARCH_TEMP_ORD_INSPECTION.if")   
    public void tempOrdInspectionSearchMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
                
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("date") != null) {
                model.put("date", request.getParameterMap().get("date")[0]);
            }
              
            m = service.tempOrdInspectionSearchMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : searchStockByLocMobile 
     * Method 설명 : WMS010 기준 재고 내역 조회 (CFC 재고이동 1284)
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_SEARCH_STOCK_BY_LOC.if")   
    public void searchStockByLocMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
                
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("ritem_id") != null) {
                model.put("ritem_id", request.getParameterMap().get("ritem_id")[0]);
            }
              
            m = service.searchStockByLocMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    
    /*-
     * Method ID : searchStockInfoByLotMobile 
     * Method 설명 : CUST_LOT_NO 기준 재고 찾기  
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_GET_STOCK_INFO_FROM_LOT.if")   
    public void searchStockInfoByLotMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
                
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("search_lot_no") != null) {
                model.put("search_lot_no", request.getParameterMap().get("search_lot_no")[0]);
            }
              
            m = service.searchStockInfoByLotMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
	/*-
	 * Method ID : itemLotLocShippingComplete
	 * Method 설명 : LOT번호, 로케이션정보 기반 주문입력 및 출고확정
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_ITEM_LOT_LOC_SHIPPING_COMPLETE.if")
	public void itemLotLocShippingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateItemLotLocShippingComplete(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
    
	/*-
	 * Method ID : multiLotShippingComplete
	 * Method 설명 : 동일한 LOT번호 다중 LOT를 가지고 주문입력부터 출고확정까지 진행
	 * 작성자 : SMICS
	 * 날   짜 : 2024-01-29
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_MULTI_LOT_SHIPPING_COMPLETE.if")
	public void multiLotShippingComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateMultiLotShippingComplete(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
    /*-
     * Method ID : outOrdSearchMobile 
     * Method 설명 : 출고주문리스트 조회 
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_SEARCH_OUT_ORD_INFO.if")   
    public void outOrdSearchMobile (HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
                
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("ord_id") != null) {
                model.put("ord_id", request.getParameterMap().get("ord_id")[0]);
            }
            if (request.getParameterMap().get("date") != null) {
                model.put("date", request.getParameterMap().get("date")[0]);
            }
              
            m = service.outOrdSearchMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    
    /* Method ID : itemSetPartSearchMobile 
    * Method 설명 : 임가공정보 조회 
    * 작성자 : summer
    * 
    * @param inputJSON
    * 
    */
   @RequestMapping("IF_SEARCH_ITEM_SET_PART_INFO.if")   
   public void itemSetPartSearchMobile (HttpServletRequest request, HttpServletResponse response) {
   Map<String, Object> m = new HashMap<String, Object>();
   Map<String, Object> model = new HashMap<String, Object>();
   try {
       model.put("SAVE_JSON_FILE", "Y");
       model.put("SAVE_DOC_INFO", "Y");

       model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
       model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
       model.put("D_DOC_CODE", "WMSIF060");
       
       if (request.getParameterMap() != null) {
           if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
               model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
           }

           if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
               model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
           }
               
           if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
               model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
           }
           
           if (request.getParameterMap().get("ritem_id") != null) {
               model.put("ritem_id", request.getParameterMap().get("ritem_id")[0]);
           }
             
           m = service.itemSetPartSearchMobile(model);
       }
       } catch (InterfaceException ife) {
           if (log.isErrorEnabled()) {
               log.error("Fail to create Work Info :", ife);
           }
           m.put("ERRCODE", ife.getMsgCode());
           m.put("MSG", ife.getMessage());
   
       } catch (Exception e) {
           if (log.isErrorEnabled()) {
               log.error("Fail to create Work Info :", e);
           }
           m.put("ERRCODE", -1);
           m.put("MSG", MessageResolver.getMessage("save.error"));
   
       }
   
       try {
           service.sendJSON(response, m, model);
   
       } catch (Exception e) {
           if (log.isErrorEnabled()) {
               log.error("Fail to send JSON :", e);
           }
       }
   }
    
    /*-
     * Method ID : OutSerialPltCompleteMobile
     * Method 설명 : 개체 시리얼 인식 후 출고 처리
     * 작성자 : SMICS
     * 날 짜 : 2024.02.26
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_OUT_SERIAL_PLT_COMPLETE_MOBILE.if")
    public void OutSerialPltCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);
           
            service.updateOutSerialPltCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /*-
     * Method ID : OutSerialTraceCompleteMobile
     * Method 설명 : 개체 시리얼 인식 후 출고 처리
     * 작성자 : SMICS
     * 날 짜 : 2024.02.26
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_OUT_SERIAL_TRACE_COMPLETE_MOBILE.if")
    public void OutSerialTraceCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);
           
            service.updateOutSerialTraceCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /* Method ID : pltSerialSearchMobile 
     * Method 설명 : PLT 시리얼 정보 조회 (WMSTS010)
     * 작성자 : summer
     * 
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_SEARCH_PLT_SERIAL_MOBILE.if")   
    public void pltSerialSearchMobile(HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
                
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("plt_serial_cd") != null) {
                model.put("plt_serial_cd", request.getParameterMap().get("plt_serial_cd")[0]);
            }
              
            m = service.pltSerialSearchMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
	/*-
     * Method ID : ordInsSimpleOut
     * Method 설명 : 출고 주문을 여러 lot 간편 출고
     * DATE : 2024-04-25
     * 작성자 : ksg
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_ORD_INS_SIMPLE_OUT_STLOGIS.if")
    public void ordInsSimpleOutStlogis(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
            
            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF060");
            
            log.info("inputJSON==============="+inputJSON);

            service.receiveJSON(model, inputJSON);

            service.updateOrdInsSimpleOutStlogis(model);
        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            if(Integer.parseInt(model.get("RETURN_CD").toString()) == 105) {
            	response.setCharacterEncoding("UTF-8");
                PrintWriter printWriter = null;
                printWriter = response.getWriter();
                
                String sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S).append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_START).append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
                        .append("\"").append("LOTerror").append("\"")
                        .append(",\"").append("RETURN_MSG").append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
                        .append("\"").append(model.get("RETURN_MSG").toString()).append("\"")
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E)
                        .append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString(); // sendData = "{\"SEND_DATA\":\"SUCCESS\",\"RETURN_MSG\":\"변수\"}";
                
                printWriter.println(sendData);
                printWriter.close();
            }else{
            	service.sendResponse(response, model, m);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /* Method ID : itemStockByUnitNoSearchMobile 
     * Method 설명 : 재고 속성 조회(STI)
     * 작성자 : summer
     * @param inputJSON
     * 
     */
    @RequestMapping("IF_ITEM_STOCK_BY_UNIT_NO_SEARCH.if")   
    public void itemStockByUnitNoSearchMobile(HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
                
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("PLT_NO") != null) {
                model.put("PLT_NO", request.getParameterMap().get("PLT_NO")[0]);
            }
              
            m = service.itemStockByUnitNoSearchMobile(model);
        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }

    /*-
     * Method ID : OutOrdSerialTraceCompleteMobile
     * Method 설명 : 개체 시리얼 인식 후 출고 처리
     * 작성자 : SMICS
     * 날 짜 : 2024.07.26
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/IF_ORD_OUT_SERIAL_TRACE_COMPLETE_MOBILE.if")
    public void OutOrdSerialTraceCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            service.receiveJSON(model, inputJSON);
           
            service.updateOrdOutSerialTraceCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
            service.sendResponse(response, model, m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /**
     * Method ID : OutOrderSerialInfoMobile
     * Method 설명 : 1435.출고 주문 리스트(시리얼) 조회 
     * 작성자 : 
     * 날 짜 : 
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/OUT_ORDER_SERIAL_INFO_MOBILE.if")
    public void OutOrderSerialInfoMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            m = service.OutOrderSerialInfoMobile(model);
        
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /**
     * Method ID : SerialLotStockInfoMobile
     * Method 설명 : 1435.출고 주문 리스트(시리얼) lot 조회 
     * 작성자 : 
     * 날 짜 : 
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/SERIAL_LOT_STOCK_INFO_MOBILE.if")
    public void SerialLotStockInfoMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");

            m = service.SerialLotStockInfoMobile(model);
        
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /**
     * Method ID : subLotOrdCompleteMobile
     * Method 설명 : 1435.출고 주문 리스트(시리얼) 출고확정
     * 작성자 : 
     * 날 짜 : 
     *
     * @param inputJSON
     * @param response
     * @throws Exception
     */
    @RequestMapping("/SUB_LOT_ORD_COMPLETE_MOBILE.if")
    public void subLotOrdCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            m.put("SAVE_JSON_FILE", "Y");
            m.put("SAVE_DOC_INFO", "Y");

            m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            m.put("D_DOC_CODE", "WMSIF050");
            
            service.subLotOrdCompleteMobile(model);
        
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, model, m);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : searchInOrderListMobile 
     * Method 설명 : 모바일 입고 주문 내역 검색 
     * 작성자 : summer
     * @param inputJSON ORG_ORD_ID
     */
    @RequestMapping("IF_SEARCH_IN_ORDER_LIST.if")   
    public void searchInOrderListMobile(HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
              
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
            }
            
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("org_ord_id") != null) {
                model.put("org_ord_id", request.getParameterMap().get("org_ord_id")[0]);
            }
            
            if (request.getParameterMap().get("from_date") != null) {
                model.put("from_date", request.getParameterMap().get("from_date")[0]);
            }
            

            System.out.println(model.toString());
            m = service.searchInOrderListMobile(model);

        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /*-
     * Method ID : searchInOrderDetailMobile 
     * Method 설명 : 모바일 입고 주문 내역 검색 (디테일)
     * 작성자 : summer
     * @param inputJSON ORD_ID
     */
    @RequestMapping("IF_SEARCH_IN_ORDER_DETAIL.if")   
    public void searchInOrderDetailMobile(HttpServletRequest request, HttpServletResponse response) {
    Map<String, Object> m = new HashMap<String, Object>();
    Map<String, Object> model = new HashMap<String, Object>();
    try {
        model.put("SAVE_JSON_FILE", "Y");
        model.put("SAVE_DOC_INFO", "Y");

        model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
        model.put("D_DOC_CODE", "WMSIF060");
        
        if (request.getParameterMap() != null) {
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
            }

            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
            }
              
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
            }
            
            if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
            }
            
            if (request.getParameterMap().get("ord_id") != null) {
                model.put("ord_id", request.getParameterMap().get("ord_id")[0]);
            }

            System.out.println(model.toString());
            m = service.searchInOrderDetailMobile(model);

        }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
    
        }
    
        try {
            //model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
            service.sendJSON(response, m, model);
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
    }
    
    /**
     * Method ID : pickingLabelOrderSearchMobile 
     * Method 설명 : 모바일 피킹 라벨 주문 조회
     * 작성자 : 
     * @param inputJSON LABEL_NO
     */
    @RequestMapping("IF_PICKING_LABEL_ORDER_SEARCH.if")   
    public void pickingLabelOrderSearchMobile(@RequestBody String inputJSON, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            model.put("D_DOC_CODE", "WMSIF060");
           
            m = service.pickingLabelOrderSearchMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    /**
     * Method ID : labelOrderCheckCompleteMobile 
     * Method 설명 : 모바일 피킹 라벨 주문 검수확정
     * 작성자 : 
     * @param inputJSON LABEL_NO
     */
    @RequestMapping("IF_LABEL_ORDER_CHECK_COMPLETE.if")   
    public void labelOrderCheckCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
            model.put("D_DOC_CODE", "WMSIF060");
           
            m = service.labelOrderCheckCompleteMobile(model);

        } catch (InterfaceException ife) {
            m = new HashMap<String, Object>();
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }

        try {
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to sendResponse :", e);
            }
        }
    }
    
    


}