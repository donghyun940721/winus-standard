package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF030Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF030Service")
	private WMSIF030Service service;

	/*-
	 * Method ID : pickingOrderComplete 
	 * Method 설명 : JSON데이터 수신, 피킹확정 처리 긴행
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_ALERT.if")
	public void inAlert(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_ALERT);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_ALERT);
			model.put("D_DOC_CODE", "WMSIF030");

			service.receiveJSON(model, inputJSON);

			service.updateRtiAlert(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}

	
	/*-
	 * Method ID : getIsolationList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping("/IF_IN_ISOLATION_LIST_QRY.if")
	public void getIsolationList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_ISOLATION_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_ISOLATION_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF030");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}
				m = service.selectIsolationList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : isolationQry 
	 * Method 설명 : 격리지시 JSON 송신
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_ISOLATION_QRY.if")
	public void isolationQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_ISOLATION_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_ISOLATION_QRY);
			model.put("D_DOC_CODE", "WMSIF030");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_WORK_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
					model.put(ConstantWSIF.IF_KEY_REGIST_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ)[0]);
				}

				m = service.selectIsolationList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}

	/*-
	 * Method ID : isolationComplete 
	 * Method 설명 : JSON데이터 수신, 격리확정 처리 긴행
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_ISOLATION_COMPLETE.if")
	public void isolationComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_ISOLATION_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_ISOLATION_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF030");

			service.receiveJSON(model, inputJSON);

			service.updateIsolationComplete(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}

	/*-
	 * Method ID : getMoveList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping("/IF_IN_MOVE_LIST_QRY.if")
	public void getMoveList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_MOVE_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_MOVE_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF020");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}
				m = service.selectMoveList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : moveQry 
	 * Method 설명 : 재고이동지시 JSON 송신
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_MOVE_QRY.if")
	public void moveQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_MOVE_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_MOVE_QRY);
			model.put("D_DOC_CODE", "WMSIF030");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_WORK_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
					model.put(ConstantWSIF.IF_KEY_REGIST_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ)[0]);
				}

				m = service.selectMoveList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}

	/*-
	 * Method ID : moveComplete 
	 * Method 설명 : JSON데이터 수신, 재고이동 처리 긴행
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_MOVE_COMPLETE.if")
	public void moveComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_MOVE_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_MOVE_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF030");

			service.receiveJSON(model, inputJSON);

			service.updateMoveComplete(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}
	
	/*-
	 * Method ID : getStockList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 */
	@RequestMapping("/IF_IN_STOCK_LIST_QRY.if")
	public void getStockList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_STOCK_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_STOCK_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF020");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}
				m = service.selectStockList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		

	/*-
	 * Method ID : stockQry 
	 * Method 설명 : 재고조사지시 JSON 송신
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_IN_STOCK_SEARCH.if")
	public void stockQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_STOCK_SEARCH);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_STOCK_SEARCH);
			model.put("D_DOC_CODE", "WMSIF030");

			if (request.getParameterMap() != null) {
				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
					model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID) != null) {
					model.put(ConstantWSIF.IF_KEY_TERMINAL_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_TERMINAL_ID)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_WORK_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
					model.put(ConstantWSIF.IF_KEY_REGIST_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_WORK_SEQ)[0]);
				}

				if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
					model.put(ConstantWSIF.IF_KEY_ORD_SEQ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ORD_SEQ)[0]);
				}

				m = service.selectStockList(model);
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}

		try {
			model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}

	/*-
	 * Method ID : stockComplete 
	 * Method 설명 : JSON데이터 수신, 재고조사 확정처리 긴행
	 * 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_OUT_STOCK_COMPLETE.if")
	public void stockComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_STOCK_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_STOCK_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF030");

			service.receiveJSON(model, inputJSON);

			service.updateStockComplete(model);

		} catch (InterfaceException ife) {
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
		if (log.isInfoEnabled()) {
			log.info("m :" + m);
		}
		*/
	}

}
