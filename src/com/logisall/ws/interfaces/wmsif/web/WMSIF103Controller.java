package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF103Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF103Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF103Service")
	private WMSIF103Service service;
	
	@RequestMapping("/IF_GET_STOCK_LIST.if")
	public void get_stock_list(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
                }

                if (request.getParameterMap().get("loc_barcode") != null) {
                    model.put("loc_barcode",
                            request.getParameterMap().get("loc_barcode")[0]);
                }
                
                if (request.getParameterMap().get("item_barcode") != null) {
                    model.put("item_barcode",
                            request.getParameterMap().get("item_barcode")[0]);
                }
                
                if (request.getParameterMap().get("make_dt") != null) {
                    model.put("make_dt",
                            request.getParameterMap().get("make_dt")[0]);
                }
                
                if (request.getParameterMap().get("use_dt") != null) {
                    model.put("use_dt",
                            request.getParameterMap().get("use_dt")[0]);
                }
                
                m = service.get_stock_list(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
	}
	
	@RequestMapping("/IF_TRANSFER_STOCKID_COMPLETE.if")
	public void if_transfer_stockid_complete(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<String, Object>();
        try {
            model.put("SAVE_JSON_FILE", "Y");
            model.put("SAVE_DOC_INFO", "Y");

            model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
            model.put("D_DOC_CODE", "WMSIF050");

            if (request.getParameterMap() != null) {
                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_LOGIN_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
                    model.put(ConstantWSIF.IF_KEY_PASSWORD,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
                }

                if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
                    model.put(ConstantWSIF.IF_KEY_ROW_LC_ID,
                            request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
                }

                if (request.getParameterMap().get("to_loc_barcode") != null) {
                    model.put("to_loc_barcode",
                            request.getParameterMap().get("to_loc_barcode")[0]);
                }
                
                if (request.getParameterMap().get("stock_id") != null) {
                    model.put("stock_id",
                            request.getParameterMap().get("stock_id")[0]);
                }
                
                if (request.getParameterMap().get("stock_qty") != null) {
                    model.put("stock_qty",
                            request.getParameterMap().get("stock_qty")[0]);
                }
                
                if (request.getParameterMap().get("work_ip") != null) {
                    model.put("work_ip",
                            request.getParameterMap().get("work_ip")[0]);
                }
                
                if (request.getParameterMap().get("user_no") != null) {
                    model.put("user_no",
                            request.getParameterMap().get("user_no")[0]);
                }
                
                m = service.if_transfer_stockid_complete(model);
            }
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", ife);
            }
            m.put("ERRCODE", ife.getMsgCode());
            m.put("MSG", ife.getMessage());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to create Work Info :", e);
            }
            m.put("ERRCODE", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));

        }

        try {
            service.sendJSON(response, m, model);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to send JSON :", e);
            }
        }
	}
}
