package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF030Service extends AbstractInterfaceService {

    public void updateRtiAlert(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectIsolationList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectIsolationQry(Map<String, Object> model) throws Exception;
    public void updateIsolationComplete(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectMoveList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectMoveQry(Map<String, Object> model) throws Exception;
    public void updateMoveComplete(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectStockList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectStockQry(Map<String, Object> model) throws Exception;
    public void updateStockComplete(Map<String, Object> model) throws Exception;
    
}
