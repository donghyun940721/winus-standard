package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.impl.WMSST040Dao;
import com.logisall.winus.wmsst.service.impl.WMSST076Dao;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKR;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKRDate;
import com.logisall.ws.interfaces.wmsif.service.WMSIF070Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF070Service")
public class WMSIF070ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF070Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF070Dao")
	private WMSIF070Dao dao;
	
	@Resource(name = "WMSST040Dao")
	private WMSST040Dao WMSST040Dao;
	
	@Resource(name = "WMSST076Dao")
    private WMSST076Dao WMSST076Dao;

	@Autowired
	WMSIF070ServiceImpl(WMSIF070Dao dao) {
		super(dao);
	}
		
	
	public Map<String, Object> selectMappingDetailAsan(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMappingDetailAsan(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMappingRemainQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMappingRemainQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMoveFromLocQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveFromLocQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMoveToLocQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveToLocQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMappingLotConfirm(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMappingLotConfirm(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectLotRitemQtyQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotRitemQtyQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMovePriorityQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMovePriorityQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMovePriorityEpcQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMovePriorityEpcQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectStocktakingIdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStocktakingIdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectLocIdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLocIdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectStocktakingTotalQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStocktakingTotalQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateTransferAllComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferAllComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateDemappingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpDemappingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	} 

	public void updateMappingCompleteAsan(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMappingCompleteAsan(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	
	public void updateLotMappingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLotMappingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectLotEpcQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotEpcQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectStockInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateStocktakingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpStocktakingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public void updateMappingCompleteMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMappingCompleteMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		

	
	public Map<String, Object> selectIfMoveFromLocQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpIfMoveFromLocQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateRtiTransferComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpRtiTransferComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateKitGenMakeComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitGenMakeComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateKitGenMakeCompleteInplt(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitGenMakeCompleteInplt(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateKitDeleteComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitDeleteComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectDemappingDetailQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpDemappingDetailQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMoveTransQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveTransQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMakeKitFifoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMakeKitFifoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateMovestockCmtComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMovestockCmtComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	

	public Map<String, Object> selectLotInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectItemZoneFifoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpItemZoneFifoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
		
	public Map<String, Object> selectLotItemConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotItemConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updaterepackingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpRepackingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectMoveFromRefurbishedQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveFromRefurbishedQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}


	public Map<String, Object> selectSerialAsInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSerialAsInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updaterefurbishedTransferComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSprefurbishedTransferComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updaterefurbishedPartOutComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSprefurbishedPartOutComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updatekitMakeSerialComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpkitMakeSerialComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectMoveFromLocUnitQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveFromLocUnitQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updatekitGenWorkseqComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpkitGenWorkseqComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectKitGenListInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpKitGenListInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}


	public Map<String, Object> selectLotDuplicationInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotDuplicationInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateTransferAllMultiPart(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferAllMultiPart(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		

	  /**
	  * 
	  * Method ID   : WmsImgDataInsert
	  * Method �ㅻ�    : ��怨�����
	  * ���깆��                      : smics
	  * @param model
	  * @return
	  * @throws Exception
	  */
	 @Override
	 public Map<String, Object> WmsImgDataInsert(Map<String, Object> model) throws Exception {
	     Map<String, Object> m = new HashMap<String, Object>();
	     
	     try{

	         //log.info(" ********** model DeliveryImgDataInsert model: " + model);
	         int tmpCnt = 1;
	         
	         if(tmpCnt > 0){
	             String[] ordId  = new String[tmpCnt];                
	             String[] fileId = new String[tmpCnt];         
	             String[] attachGb  = new String[tmpCnt];
	             String[] fileValue = new String[tmpCnt];
	             String[] filePath = new String[tmpCnt];
	             String[] fileName = new String[tmpCnt];
	             String[] fileExt = new String[tmpCnt];
	             
	             for(int i = 0 ; i < tmpCnt ; i ++){
	                 ordId[i]    	= (String)model.get("ORD_ID");               
	                 fileId[i]   	= (String)model.get("FILE_ID");         
	                 attachGb[i]    	= (String)model.get("ATTACH_GB");
	                 fileValue[i]    = (String)model.get("FILE_VALUE");
	                 filePath[i]    	= (String)model.get("FILE_PATH");
	                 fileName[i]    	= (String)model.get("FILE_NAME");
	                 fileExt[i]    	= (String)model.get("FILE_EXT");		
	             }
	             
	             Map<String, Object> modelIns = new HashMap<String, Object>();
	             
	             modelIns.put("ordId", ordId);
	             modelIns.put("fileId", fileId);
	             modelIns.put("attachGb", attachGb);
	             modelIns.put("fileValue", fileValue);
	             modelIns.put("filePath", filePath);
	             modelIns.put("fileName", fileName);
	             modelIns.put("fileExt", fileExt);                
	             
	             //dao                
	             modelIns = (Map<String, Object>)dao.WmsImgDataInsert(modelIns);
	             
	             ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	             // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
	             // errMsg = modelIns.get("O_MSG_NAME").toString();
	             
	         }            
	         m.put("errCnt", 0);
	         m.put("MSG", MessageResolver.getMessage("save.success"));
	         
	     } catch(BizException be) {
	         m.put("errCnt", 1);
	         m.put("MSG", be.getMessage() );
	         
	     } catch(Exception e){
	         throw e;
	     }
	     return m;
	 }
	 
	 public Map<String, Object> selectMoveItemFromDataQry(Map<String, Object> model) throws Exception {

			Map<String, Object> map = new HashMap<String, Object>();
			try {

				checkParamMapValidationForList(model);

				checkUserInfoValidation(model);

				Map<String, Object> modelIns = createSPParamMapKR(model);
				
				//log.info(" ********** modelIns ====================================: " + modelIns);

				// //log.info(" ********** outputList 1 : " + modelIns);
				if(modelIns.get("lcId").equals("0000003920")){
					modelIns = (Map<String, Object>) dao.runSpMoveItemFromDataQry2(modelIns);
				}else{
					modelIns = (Map<String, Object>) dao.runSpMoveItemFromDataQry(modelIns);
				}
				

				// //log.info(" ********** outputList 2 : " + modelIns);
				InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
				//log.info(" ********** outputList : " + outputList);

				map = createReturnMapKr(outputList, model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
				}
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to select list :", e);
				}
				throw e;
			}

			return map;

		}
	 
	public void updateItemTransferAllComplete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpItemTransferAllComplete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}


	public void updateItemTransferAll2048Complete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpItemTransferAll2048Complete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}
	
	public void updateItemTransferAllMultiComplete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpItemTransferAllMultiComplete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}
	
	public void updateItemTransferAllStockidComplete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpItemTransferAllStockidComplete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}
	
	public Map<String, Object> selectLocInoutDataQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLocInoutDataQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	 
	public void updateInoutCheckComplete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpInoutCheckComplete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}

	 
public void updateLotLocResettingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLotLocResettingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

public Map<String, Object> selectTransferReadyInfoQry(Map<String, Object> model) throws Exception {

	Map<String, Object> map = new HashMap<String, Object>();
	try {

		checkParamMapValidationForList(model);

		checkUserInfoValidation(model);

		Map<String, Object> modelIns = createSPParamMapKR(model);
		
		//log.info(" ********** modelIns ====================================: " + modelIns);

		// //log.info(" ********** outputList 1 : " + modelIns);
		modelIns = (Map<String, Object>) dao.runSpTransferReadyInfoQry(modelIns);

		// //log.info(" ********** outputList 2 : " + modelIns);
		InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

		List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
		//log.info(" ********** outputList : " + outputList);

		map = createReturnMapKr(outputList, model);

	} catch (InterfaceException ife) {
		if (log.isErrorEnabled()) {
			log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
		}
		throw ife;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to select list :", e);
		}
		throw e;
	}

	return map;

}

	public Map<String, Object> selectMoveItemPriorityQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveItemPriorityQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updatekitMakeSerialWork(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpkitMakeSerialWork(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updatemodifyStockComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpmodifyStockComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateTransferQtyMulti(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferQtyMulti(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateKitGenMakeComplete2048(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitGenMakeComplete2048(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateKitMakeMultiCompleteMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitMakeMultiCompleteMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	 
	public void updateItemTransferAllLotCngComplete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpItemTransferAllLotCngComplete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}
	
	public void updateKitMakeBagCompleteMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitMakeBagCompleteMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateStocktakingModifyComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpStocktakingModifyComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void stocktakingCompleteMobile(Map<String, Object> model) throws Exception {

        String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

        InterfaceBaseVOKRDate data = null;
        List<Map<String, Object>> workList = null;
        try {
            if (log.isInfoEnabled()) {
                //log.info("[ inputJSON ] :" + inputJSON);
            }

            data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

            if (log.isInfoEnabled()) {
                //log.info("[ data ] :" + data);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to excecute complete :", e);
            }
            throw new InterfaceException("5005", "JSON Data Convert Error");

        }

        if (data != null) {
            try {

                model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
                model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

                checkUserInfoValidation(model);

            } catch (InterfaceException ife) {
                if (log.isErrorEnabled()) {
                    log.error(ife.getMsgCode());
                }
                throw ife;

            }

            try {
                //log.info("[ data########################### ] :" + data);
                workList = getDataRowFromInterfaceVOKRDate(data);               
                if (log.isInfoEnabled()) {
                    //log.info("===========================================================================");
                    //log.info("[ workList ] :" + workList);
                }
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Fail to excecute complete :", e);
                }
                throw new InterfaceException("5006", "Data ROW Convert Error");

            }
            
            try {
                if (workList != null && !workList.isEmpty()) {
                    Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
                    if (log.isInfoEnabled()) {
                        //log.info("===========================================================================");
                        //log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
                    }

                    //for(int i=0; i<modelIns.size(); i++){
                        ////log.info("iLcId:" + modelIns.get("iLcId"));
                        //String[] test = modelIns.
                        
                    //}
                    
                    log.info(" ********** outputList 1 : " + modelIns);
                    modelIns = (Map<String, Object>) dao.stocktakingCompleteMobile(modelIns);

                    model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
                    model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
                    InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
                    
                }
            } catch (InterfaceException ife) {
                throw ife;

            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Fail to excecute complete :", e);
                }
                throw e;

            }
        }

    }
	
	public void updatekitGenDelNewComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpkitGenDelNewComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateTransferWcsComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferWcsComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateWcsCwFirstMoveUseAgv(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpWcsCwFirstMoveUseAgv(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateWcsCwMoveDev(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpWcsCwMoveDev(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateWcsCwCancelWork(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpWcsCwCancelWork(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateWcsCwFacilityCancelWork(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpWcsCwFacilityCancelWork(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateWcsRereceivingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpWcsRereceivingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateWcsCwShippingMoveUseAgv(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpWcsCwShippingMoveUseAgv(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updaterepackingBadComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpRepackingBadComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectTransferWorkListQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpTransferWorkListQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateTransferLotMulti(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferLotMulti(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	/**
     * Method ID : updateSerialKitMappingCompleteMobile
     * Method 설명 : 시리얼 기반 임가공 및 매핑 진행
     * 작성자 : 
     * 날   짜 : 
     *
     * @param model
     * @throws Exception
     */
	public Map<String, Object> updateSerialKitMappingCompleteMobile(Map<String, Object> model) throws Exception {
	    Map<String, Object> map = new HashMap<String,Object>();
		try{
		    JSONObject request = new JSONObject((String)model.get(ConstantWSIF.KEY_JSON_STRING));
            
            model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getString(ConstantWSIF.IF_KEY_LOGIN_ID));
            model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getString(ConstantWSIF.IF_KEY_PASSWORD));
            model.put("work_ip", request.getString("work_ip"));
            model.put("user_no" , request.getString("user_no"));

            checkUserInfoValidation(model);
            
            Map<String, Object> modelIns = new HashMap<String,Object>();
            JSONArray partItemList = request.getJSONArray("part_item_list");
            
            //0.자식상품 시리얼 validation 체크
            for(int i = 0 ; i < partItemList.length() ; i++){
                modelIns.clear();
                
                JSONObject partItemInfo = partItemList.getJSONObject(i);
                modelIns.put("LC_ID", request.getString("lc_id"));
                modelIns.put("PART_SERIAL_NO", partItemInfo.get("part_serial_no"));
                modelIns.put("PART_RITEM_ID", partItemInfo.get("part_ritem_id"));
                if((Integer)dao.checkKitPartSerialValidation(modelIns) > 0){
                    map.put("RETURN_CD", "-1");
                    map.put("RETURN_MSG", MessageResolver.getMessage("save.error"));
                    return map;
                }
            }
            
            modelIns.clear();
            
            //1. 세트아이템 임가공 진행 
            String[] lc_id = {request.getString("lc_id")};
            String[] epc_cd = {""};
            String[] ritem_id = {request.getString("set_ritem_id")};
            String[] cust_lot_no = {request.getString("set_serial_no")};
            String[] work_qty = {"1"};

            modelIns.put("lc_id", lc_id);
            modelIns.put("epc_cd", epc_cd);
            modelIns.put("ritem_id", ritem_id);
            modelIns.put("cust_lot_no", cust_lot_no);
            modelIns.put("work_qty", work_qty);
            
            modelIns.put("work_ip", request.getString("work_ip"));
            modelIns.put("user_no", request.getString("user_no"));
            
            modelIns = (Map<String, Object>)dao.spKitCompleteMobileInplt2048(modelIns);
            
            map.put("RETURN_CD", String.valueOf(modelIns.get("O_MSG_CODE")));
            map.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
            
            InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
            
            //2. 세트/파트아이템 시리얼번호 데이터 확인 후 최초 정보 등록
            
            //세트 아이템 시리얼번호 데이터 최초 정보 등록
            modelIns.clear();
            
            modelIns.put("SERIAL_NO", request.getString("set_serial_no"));
            modelIns.put("RITEM_ID", request.getString("set_ritem_id"));
            modelIns.put("LC_ID", request.getString("lc_id"));
            modelIns.put("CUST_ID", request.getString("cust_id"));
            modelIns.put("USER_NO", request.getString("user_no"));
            modelIns.put("SET_YN","Y");
            
            dao.setSerialItemInfoFirst(modelIns);
            
            //파트 아이템 시리얼 번호 데이터 최초 정보 등록
            
            //set 시리얼 정보 jobNo 검색
            String jobNo = (String)dao.selectSerialItemMappingJobNo(modelIns);
            
            for(int i = 0 ; i < partItemList.length() ; i++){
                modelIns.clear();
                
                JSONObject partItemInfo = partItemList.getJSONObject(i);
                modelIns.put("SERIAL_NO", partItemInfo.get("part_serial_no"));
                modelIns.put("RITEM_ID", partItemInfo.get("part_ritem_id"));
                modelIns.put("LC_ID", request.getString("lc_id"));
                modelIns.put("CUST_ID", request.getString("cust_id"));
                modelIns.put("USER_NO", request.getString("user_no"));
                modelIns.put("SET_YN","N");
                
                dao.setSerialItemInfoFirst(modelIns);
                
                //3. 세트-파트아이템 시리얼번호 매핑 정보 등록
                modelIns.put("SET_SERIAL_NO", request.getString("set_serial_no"));
                modelIns.put("SET_RITEM_ID", request.getString("set_ritem_id"));
                modelIns.put("PART_SERIAL_NO", partItemInfo.get("part_serial_no"));
                modelIns.put("PART_RITEM_ID", partItemInfo.get("part_ritem_id"));
                modelIns.put("CUR_JOB_NO", jobNo);
                
                dao.setSerialItemMappingInfo(modelIns);
                dao.setSerialWorkInfo(modelIns);
            }
            
		} catch (InterfaceException ife) {
			throw ife;
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw e;
		}
		return map;
	}
	
	public Map<String, Object> getItemMoveDataByIdLoc(Map<String, Object> model) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	checkParamMapValidation(model);
            checkUserInfoValidation(model);          
            List<Map<String, Object>> modelIns;           
            modelIns = (List<Map<String, Object>>) dao.getItemMoveDataByIdLoc(model);          
            map.put("DATA", modelIns);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to select list :", e);
            }
            throw e;
        }
        return map;
    }
	
	public Map<String, Object> selectSerialMappingInfoQry(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
	        JSONObject request = new JSONObject((String)model.get(ConstantWSIF.KEY_JSON_STRING));
            
            model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getString(ConstantWSIF.IF_KEY_LOGIN_ID));
            model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getString(ConstantWSIF.IF_KEY_PASSWORD));

            checkUserInfoValidation(model);
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("lc_id",request.getString("lc_id"));
            modelIns.put("set_serial_no",request.getString("set_serial_no"));
            
            map = createReturnMapKr((List<Map<String, Object>>)dao.selectSerialMappingInfoQry(modelIns), model);
            
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> getTransCustList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            JSONObject request = new JSONObject((String)model.get(ConstantWSIF.KEY_JSON_STRING));
            
            model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getString(ConstantWSIF.IF_KEY_LOGIN_ID));
            model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getString(ConstantWSIF.IF_KEY_PASSWORD));

            checkUserInfoValidation(model);
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("lc_id",request.getString("lc_id"));
            if(!request.isNull("cust_id")) modelIns.put("cust_id",request.getString("cust_id"));
            if(!request.isNull("trans_cust_bar_cd")) modelIns.put("trans_cust_bar_cd",request.getString("trans_cust_bar_cd"));
            
            map = createReturnMapKr((List<Map<String, Object>>)dao.getTransCustList(modelIns), model);
            
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
            }
            throw ife;

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to select list :", e);
            }
            throw e;
        }

        return map;

    }
	
	public Map<String, Object> getCustIdList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            JSONObject request = new JSONObject((String)model.get(ConstantWSIF.KEY_JSON_STRING));
            
            model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getString(ConstantWSIF.IF_KEY_LOGIN_ID));
            model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getString(ConstantWSIF.IF_KEY_PASSWORD));

            checkUserInfoValidation(model);
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("lc_id",request.getString("lc_id"));
            
            map = createReturnMapKr((List<Map<String, Object>>)dao.getCustIdList(modelIns), model);
            
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
            }
            throw ife;

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to select list :", e);
            }
            throw e;
        }

        return map;

    }
	
	/**
     * Method ID : InCheckItemTransferSKON
     * Method 설명 : 입고 검수 재고이동 진행(SKON)
     * 작성자 : 
     * 날   짜 : 
     *
     * @param model
     * @throws Exception
     */
	@Transactional(rollbackFor=Exception.class)
	public void InCheckItemTransferSKON(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            JSONObject request = new JSONObject((String)model.get(ConstantWSIF.KEY_JSON_STRING));
            
            model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getString(ConstantWSIF.IF_KEY_LOGIN_ID));
            model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getString(ConstantWSIF.IF_KEY_PASSWORD));
    
            checkUserInfoValidation(model);
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            /*
             * 
             * 1. 수리로케이션 정보, 교체상품 재고 찾기
             * -- 수리재고 --
             * 2. 재고이동 (clean_loc > repair_loc)
             * 3. 재고속성변경 (cust_lot_no = null)
             * 
             * -- 교체재고--
             * 4. 재고속성변경 (cust_lot_no = cust_lot_no)
             * --1차검수이면
             * 5. 재고이동 (out_loc > clean_loc)
             * --2차검수이면
             * 5. 재고이동 (clean_loc > out_loc)
             * */

            map.put("LC_ID",request.getString("lc_id"));
            map.put("LOC_TYPE", "130");
            Map<String,Object> repairLocInfo = (Map<String,Object>)dao.selectTypeLocInfo(map);
            map.clear();
            
            map.put("LC_ID",request.getString("lc_id"));
            map.put("LOC_TYPE", "140");
            Map<String,Object> cleanLocInfo = (Map<String,Object>)dao.selectTypeLocInfo(map);
            map.clear();
            
            map.put("LC_ID",request.getString("lc_id"));
            map.put("LOC_TYPE", "10");
            Map<String,Object> outLocInfo = (Map<String,Object>)dao.selectTypeLocInfo(map);
            
            if (repairLocInfo == null || !repairLocInfo.containsKey("LOC_CD") || repairLocInfo.get("LOC_CD").equals("")){
                throw new InterfaceException("-1","there's not Repair Location");
            }
            
            ArrayList<Map<String,String>> moveStockInfo = new ArrayList<Map<String,String>>();
            
            //재고이동 param
            ArrayList<String> workSeq = new ArrayList<String>();
            ArrayList<String> fromLocCd = new ArrayList<String>();
            ArrayList<String> toLocCd = new ArrayList<String>();
            ArrayList<String> ritemId = new ArrayList<String>();
            ArrayList<String> subLotId = new ArrayList<String>();
            ArrayList<String> uomId = new ArrayList<String>();
            ArrayList<String> moveQty = new ArrayList<String>();
            ArrayList<String> pltQty = new ArrayList<String>();
            ArrayList<String> workStat = new ArrayList<String>();
            ArrayList<String> itemWorkDt = new ArrayList<String>();
            ArrayList<String> itemWorkTm = new ArrayList<String>();
            ArrayList<String> makeDt = new ArrayList<String>();
            ArrayList<String> custId = new ArrayList<String>();
            ArrayList<String> custLotNo = new ArrayList<String>();
            ArrayList<String> blNo = new ArrayList<String>();
            ArrayList<String> stockId = new ArrayList<String>();
            ArrayList<String> reasonCd = new ArrayList<String>();
            ArrayList<String> workDesc = new ArrayList<String>();
            ArrayList<String> lotType = new ArrayList<String>();
            ArrayList<String> ownerCd = new ArrayList<String>();
            
            //LOT 속성변경 추가 param
            ArrayList<String> toLocId = new ArrayList<String>();
            ArrayList<String> itemBestDateEnd = new ArrayList<String>();

            JSONArray workList = request.getJSONArray("work_list");
            
            if(workList.length() == 0 && request.getString("checkStep").equals("1")){
                throw new InterfaceException("-1","there's not Sendding Data");
            }
            
            
            for(int i = 0 ; i < workList.length() ; i++){
                JSONObject workInfo = workList.getJSONObject(i);
                
                Map<String,Object> stockInfoSearchModel = new HashMap<String,Object>();
                
                stockInfoSearchModel.put("LC_ID", request.getString("lc_id"));
                stockInfoSearchModel.put("RITEM_ID", workInfo.getString("ritem_id"));
                stockInfoSearchModel.put("CUST_LOT_NO", request.getString("cust_lot_no"));
                stockInfoSearchModel.put("LOC_TYPE","140");
                //세탁로케이션 상품 조회
                List<Map<String,Object>>cleanLocStockInfoList = (List)dao.selectLocStockInfo(stockInfoSearchModel);
                
                int workQty = Integer.parseInt(workInfo.getString("work_qty"));
                
                for (Map<String,Object> stockInfo: cleanLocStockInfoList){
                    //재고이동 param
                    workSeq.add(null);
                    fromLocCd.add((String)stockInfo.get("LOC_CD"));
                    toLocCd.add((String)repairLocInfo.get("LOC_CD"));
                    ritemId.add((String)stockInfo.get("RITEM_ID"));
                    subLotId.add((String)stockInfo.get("SUB_LOT_ID"));
                    uomId.add((String)stockInfo.get("UOM_ID"));
                    pltQty.add("0");
                    workStat.add("FF");
                    itemWorkDt.add((String)stockInfo.get("WORK_DT"));
                    itemWorkTm.add(null);
                    makeDt.add((String)stockInfo.get("MAKE_DT"));
                    custId.add((String)stockInfo.get("CUST_ID"));
                    custLotNo.add(null);
                    blNo.add(null);
                    stockId.add(null);
                    reasonCd.add("24");
                    workDesc.add(request.getString("work_desc"));
                    lotType.add((String)stockInfo.get("SUB_LOT_TYPE"));
                    ownerCd.add((String)stockInfo.get("OWNER_CD"));
                    
                    //속성변경 추가 param
                    toLocId.add((String)repairLocInfo.get("LOC_ID"));
                    itemBestDateEnd.add((String)stockInfo.get("ITEM_BEST_DATE_END"));
                    
                    if(Integer.parseInt((String)stockInfo.get("WORK_ABLE_QTY").toString()) >= workQty){
                        moveQty.add(String.valueOf(workQty));
                        workQty = 0;
                        break;
                    }
                    else {
                        moveQty.add((String)stockInfo.get("WORK_ABLE_QTY").toString());
                        workQty -= Integer.parseInt((String)stockInfo.get("WORK_ABLE_QTY").toString());
                    }
                }
                if (workQty > 0){
                    throw new InterfaceException("-1","ERROR : not much stock");
                }
            }
            
            int cnt = workSeq.size();

            modelIns.put("vrWorkId","");
            modelIns.put("vrWorkCd","");
            modelIns.put("vrWorkDt","");
            
            modelIns.put("workSeq", workSeq.toArray(new String[cnt]));
            modelIns.put("fromLocCd", fromLocCd.toArray(new String[cnt]));
            modelIns.put("toLocCd", toLocCd.toArray(new String[cnt]));
            modelIns.put("ritemId", ritemId.toArray(new String[cnt]));
            modelIns.put("subLotId", subLotId.toArray(new String[cnt]));
            modelIns.put("uomId", uomId.toArray(new String[cnt]));
            modelIns.put("moveQty", moveQty.toArray(new String[cnt]));
            modelIns.put("pltQty", pltQty.toArray(new String[cnt]));
            modelIns.put("workStat", workStat.toArray(new String[cnt]));
            modelIns.put("itemWorkDt", itemWorkDt.toArray(new String[cnt]));
            modelIns.put("itemWorkTm", itemWorkTm.toArray(new String[cnt]));
            modelIns.put("makeDt", makeDt.toArray(new String[cnt]));
            modelIns.put("custId", custId.toArray(new String[cnt]));
            modelIns.put("custLotNo", custLotNo.toArray(new String[cnt]));
            modelIns.put("blNo", blNo.toArray(new String[cnt]));
            modelIns.put("stockId", stockId.toArray(new String[cnt]));
            modelIns.put("reasonCd", reasonCd.toArray(new String[cnt]));
            modelIns.put("workDesc", workDesc.toArray(new String[cnt]));
            modelIns.put("lotType", lotType.toArray(new String[cnt]));
            modelIns.put("ownerCd", ownerCd.toArray(new String[cnt]));
            
            
            modelIns.put("vrLcId",request.getString("lc_id"));
            modelIns.put("gvUserIP",request.getString("work_ip"));
            modelIns.put("gvUserNo",request.getString("user_no"));
            
            if(workList.length() != 0){
                modelIns = (Map<String, Object>)WMSST040Dao.save(modelIns);
                InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
                //O_MSG_NAME = MOVE_WORK_ID
                model.put("MOVE_WORK_ID", (String) modelIns.get("O_MSG_NAME"));
            }
            
            //재고속성 변경
            Map<String, Object> modelIns2 = new HashMap<String, Object>();
            
            modelIns2.put("I_LC_ID", request.getString("lc_id"));
            modelIns2.put("I_CUST_ID", custId.toArray(new String[cnt]));
            modelIns2.put("I_RITEM_ID", ritemId.toArray(new String[cnt]));
            modelIns2.put("I_UOM_ID", uomId.toArray(new String[cnt]));
            modelIns2.put("I_SUB_LOT_ID", subLotId.toArray(new String[cnt]));
            modelIns2.put("I_LOC_ID", toLocId.toArray(new String[cnt]));
            modelIns2.put("I_CUST_LOT_NO", custLotNo.toArray(new String[cnt]));
            modelIns2.put("I_WORK_QTY", moveQty.toArray(new String[cnt]));
            modelIns2.put("I_OLD_LOT_TYPE", lotType.toArray(new String[cnt]));
            modelIns2.put("I_OLD_OWNER_CD", ownerCd.toArray(new String[cnt]));
            modelIns2.put("I_NEW_LOT_TYPE", lotType.toArray(new String[cnt]));
            modelIns2.put("I_NEW_OWNER_CD", ownerCd.toArray(new String[cnt]));
            modelIns2.put("I_CHANGE_MEMO", workDesc.toArray(new String[cnt]));
            modelIns2.put("I_ITEM_BEST_DATE_END", itemBestDateEnd.toArray(new String[cnt]));
            modelIns2.put("I_MAKE_DT", makeDt.toArray(new String[cnt]));
            
            modelIns2.put("I_WORK_IP", request.getString("work_ip"));
            modelIns2.put("I_USER_NO", request.getString("user_no"));
            if(workList.length() != 0){
                WMSST076Dao.update(modelIns2);
                InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns2.get("O_MSG_CODE")), (String) modelIns2.get("O_MSG_NAME"));
            }
            //교체 재고 속성변경 param
            ArrayList<String> changeCustId = new ArrayList<String>();
            ArrayList<String> changeRitemId = new ArrayList<String>();
            ArrayList<String> changeUomId = new ArrayList<String>();
            ArrayList<String> changeSubLotId = new ArrayList<String>();
            ArrayList<String> changeLocId = new ArrayList<String>();
            ArrayList<String> changeCustLotNo = new ArrayList<String>();
            ArrayList<String> changeWorkQty = new ArrayList<String>();
            ArrayList<String> changeLotType = new ArrayList<String>();
            ArrayList<String> changeOwnerCd = new ArrayList<String>();
            ArrayList<String> changeWorkDesc = new ArrayList<String>();
            ArrayList<String> changeItemBestDateEnd = new ArrayList<String>();
            ArrayList<String> changeMakeDt = new ArrayList<String>();
            
            //교체재고찾기
            for(int i = 0 ; i < workList.length() ; i++){
                JSONObject workInfo = workList.getJSONObject(i);
                
                Map<String,Object> stockInfoSearchModel = new HashMap<String,Object>();
                
                stockInfoSearchModel.put("LC_ID", request.getString("lc_id"));
                stockInfoSearchModel.put("RITEM_ID", workInfo.getString("ritem_id"));
                stockInfoSearchModel.put("LOC_TYPE","10");
                stockInfoSearchModel.put("LOT_NO_YN","N");
                
                List<Map<String,Object>>itemStockInfoList = (List)dao.selectLocStockInfo(stockInfoSearchModel);
                
                int workQty = Integer.parseInt(workInfo.getString("work_qty"));
                
                for (Map<String,Object> stockInfo: itemStockInfoList){
                    //속성변경 param
                    changeCustId.add((String)stockInfo.get("CUST_ID"));
                    changeRitemId.add((String)stockInfo.get("RITEM_ID"));
                    changeUomId.add((String)stockInfo.get("UOM_ID"));
                    changeSubLotId.add((String)stockInfo.get("SUB_LOT_ID"));
                    changeLocId.add((String)stockInfo.get("LOC_ID"));
                    changeCustLotNo.add(request.getString("cust_lot_no"));
                    
                    changeLotType.add((String)stockInfo.get("SUB_LOT_TYPE"));
                    changeOwnerCd.add((String)stockInfo.get("OWNER_CD"));
                    changeWorkDesc.add(request.getString("work_desc"));
                    changeItemBestDateEnd.add((String)stockInfo.get("ITEM_BEST_DATE_END"));
                    changeMakeDt.add((String)stockInfo.get("MAKE_DT"));
                    
                    if(Integer.parseInt((String)stockInfo.get("WORK_ABLE_QTY").toString()) >= workQty){
                        changeWorkQty.add(String.valueOf(workQty));
                        workQty = 0;
                        break;
                    }
                    else {
                        changeWorkQty.add((String)stockInfo.get("WORK_ABLE_QTY").toString());
                        workQty -= Integer.parseInt((String)stockInfo.get("WORK_ABLE_QTY").toString());
                    }
                }
                if (workQty > 0){
                    //TODO 없는 재고 = 입고 처리
                    throw new InterfaceException("-1","ERROR : not much stock");
                }
            }
            //재고속성 변경
            Map<String, Object> modelIns3 = new HashMap<String, Object>();
            
            modelIns3.put("I_LC_ID", request.getString("lc_id"));
            modelIns3.put("I_CUST_ID", changeCustId.toArray(new String[cnt]));
            modelIns3.put("I_RITEM_ID", changeRitemId.toArray(new String[cnt]));
            modelIns3.put("I_UOM_ID", changeUomId.toArray(new String[cnt]));
            modelIns3.put("I_SUB_LOT_ID", changeSubLotId.toArray(new String[cnt]));
            modelIns3.put("I_LOC_ID", changeLocId.toArray(new String[cnt]));
            modelIns3.put("I_CUST_LOT_NO", changeCustLotNo.toArray(new String[cnt]));
            modelIns3.put("I_WORK_QTY", changeWorkQty.toArray(new String[cnt]));
            modelIns3.put("I_OLD_LOT_TYPE", changeLotType.toArray(new String[cnt]));
            modelIns3.put("I_OLD_OWNER_CD", changeOwnerCd.toArray(new String[cnt]));
            modelIns3.put("I_NEW_LOT_TYPE", changeLotType.toArray(new String[cnt]));
            modelIns3.put("I_NEW_OWNER_CD", changeOwnerCd.toArray(new String[cnt]));
            modelIns3.put("I_CHANGE_MEMO", changeWorkDesc.toArray(new String[cnt]));
            modelIns3.put("I_ITEM_BEST_DATE_END", changeItemBestDateEnd.toArray(new String[cnt]));
            modelIns3.put("I_MAKE_DT", changeMakeDt.toArray(new String[cnt]));
            
            modelIns3.put("I_WORK_IP", request.getString("work_ip"));
            modelIns3.put("I_USER_NO", request.getString("user_no"));
            if(workList.length() != 0){
                WMSST076Dao.update(modelIns3);
                InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns3.get("O_MSG_CODE")), (String) modelIns3.get("O_MSG_NAME"));
            }
            //재고이동 대상 param
            ArrayList<String> finalWorkSeq = new ArrayList<String>();
            ArrayList<String> finalFromLocCd = new ArrayList<String>();
            ArrayList<String> finalToLocCd = new ArrayList<String>();
            ArrayList<String> finalRitemId = new ArrayList<String>();
            ArrayList<String> finalSubLotId = new ArrayList<String>();
            ArrayList<String> finalUomId = new ArrayList<String>();
            ArrayList<String> finalMoveQty = new ArrayList<String>();
            ArrayList<String> finalPltQty = new ArrayList<String>();
            ArrayList<String> finalWorkStat = new ArrayList<String>();
            ArrayList<String> finalItemWorkDt = new ArrayList<String>();
            ArrayList<String> finalItemWorkTm = new ArrayList<String>();
            ArrayList<String> finalMakeDt = new ArrayList<String>();
            ArrayList<String> finalCustId = new ArrayList<String>();
            ArrayList<String> finalCustLotNo = new ArrayList<String>();
            ArrayList<String> finalBlNo = new ArrayList<String>();
            ArrayList<String> finalStockId = new ArrayList<String>();
            ArrayList<String> finalReasonCd = new ArrayList<String>();
            ArrayList<String> finalWorkDesc = new ArrayList<String>();
            ArrayList<String> finalLotType = new ArrayList<String>();
            ArrayList<String> finalOwnerCd = new ArrayList<String>();
            
            
            Map<String,Object> stockInfoSearchModel = new HashMap<String,Object>();
            
            stockInfoSearchModel.put("LC_ID", request.getString("lc_id"));
            stockInfoSearchModel.put("CUST_LOT_NO", request.getString("cust_lot_no"));
            //checkStep = 1/2차 여부
            if (request.getString("checkStep").equals("1")){
                stockInfoSearchModel.put("LOC_TYPE","10");
            }else{
                stockInfoSearchModel.put("LOC_TYPE","140");
            }
            
            //재고이동 대상 select
            List<Map<String,Object>>moveStockInfoList = (List)dao.selectLocStockInfo(stockInfoSearchModel);
            
            for(Map<String,Object> stockInfo : moveStockInfoList){
                finalWorkSeq.add(null);
                finalFromLocCd.add((String)stockInfo.get("LOC_CD"));
                finalRitemId.add((String)stockInfo.get("RITEM_ID"));
                finalSubLotId.add((String)stockInfo.get("SUB_LOT_ID"));
                finalUomId.add((String)stockInfo.get("UOM_ID"));
                finalPltQty.add("0");
                finalWorkStat.add("FF");
                finalItemWorkDt.add((String)stockInfo.get("WORK_DT"));
                finalItemWorkTm.add(null);
                finalMakeDt.add((String)stockInfo.get("MAKE_DT"));
                finalCustId.add((String)stockInfo.get("CUST_ID"));
                finalCustLotNo.add(null);
                finalBlNo.add(null);
                finalStockId.add(null);
                if (request.getString("checkStep").equals("1")){
                    //1차 = 시스템, 세척로케이션 재고이동
                    finalReasonCd.add("07");
                    finalToLocCd.add((String)cleanLocInfo.get("LOC_CD"));
                }else{
                   //2차 = 세척, 출고로케이션 재고이동
                    finalReasonCd.add("23");
                    finalToLocCd.add((String)outLocInfo.get("LOC_CD"));
                }
                finalWorkDesc.add(request.getString("work_desc"));
                finalLotType.add((String)stockInfo.get("SUB_LOT_TYPE"));
                finalOwnerCd.add((String)stockInfo.get("OWNER_CD"));
                finalMoveQty.add((String)stockInfo.get("WORK_ABLE_QTY").toString());
            }
            
            Map<String, Object> modelIns4 = new HashMap<String, Object>();
            modelIns4.put("vrWorkId","");
            modelIns4.put("vrWorkCd","");
            modelIns4.put("vrWorkDt","");
            
            modelIns4.put("workSeq", finalWorkSeq.toArray(new String[cnt]));
            modelIns4.put("fromLocCd", finalFromLocCd.toArray(new String[cnt]));
            modelIns4.put("toLocCd", finalToLocCd.toArray(new String[cnt]));
            modelIns4.put("ritemId", finalRitemId.toArray(new String[cnt]));
            modelIns4.put("subLotId", finalSubLotId.toArray(new String[cnt]));
            modelIns4.put("uomId", finalUomId.toArray(new String[cnt]));
            modelIns4.put("moveQty", finalMoveQty.toArray(new String[cnt]));
            modelIns4.put("pltQty", finalPltQty.toArray(new String[cnt]));
            modelIns4.put("workStat", finalWorkStat.toArray(new String[cnt]));
            modelIns4.put("itemWorkDt", finalItemWorkDt.toArray(new String[cnt]));
            modelIns4.put("itemWorkTm", finalItemWorkTm.toArray(new String[cnt]));
            modelIns4.put("makeDt", finalMakeDt.toArray(new String[cnt]));
            modelIns4.put("custId", finalCustId.toArray(new String[cnt]));
            modelIns4.put("custLotNo", finalCustLotNo.toArray(new String[cnt]));
            modelIns4.put("blNo", finalBlNo.toArray(new String[cnt]));
            modelIns4.put("stockId", finalStockId.toArray(new String[cnt]));
            modelIns4.put("reasonCd", finalReasonCd.toArray(new String[cnt]));
            modelIns4.put("workDesc", finalWorkDesc.toArray(new String[cnt]));
            modelIns4.put("lotType", finalLotType.toArray(new String[cnt]));
            modelIns4.put("ownerCd", finalOwnerCd.toArray(new String[cnt]));
            
            
            modelIns4.put("vrLcId",request.getString("lc_id"));
            modelIns4.put("gvUserIP",request.getString("work_ip"));
            modelIns4.put("gvUserNo",request.getString("user_no"));
            
            modelIns4 = (Map<String, Object>)WMSST040Dao.save(modelIns4);
            InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns4.get("O_MSG_CODE")), (String) modelIns4.get("O_MSG_NAME"));
            
            model.put("RETURN_CD", String.valueOf(modelIns4.get("O_MSG_CODE")));
            model.put("RETURN_MSG", (String) modelIns4.get("O_MSG_NAME"));
            
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
            }
            throw ife;
    
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to select list :", e);
            }
            throw e;
        }

    }
	
	/**
     * Method ID : getStockInfoCleanLocLotNo
     * Method 설명 : 세척로케이션 LOT NO 재고 조회(SKON)
     * 작성자 : 
     * 날   짜 : 
     *
     * @param model
     * @throws Exception
     */
	@Override
	public Map<String, Object> getStockInfoCleanLocLotNo(Map<String, Object> model) throws Exception{
	    Map<String, Object> map = new HashMap<String, Object>();
        try {
            JSONObject request = new JSONObject((String)model.get(ConstantWSIF.KEY_JSON_STRING));
            
            model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getString(ConstantWSIF.IF_KEY_LOGIN_ID));
            model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getString(ConstantWSIF.IF_KEY_PASSWORD));

            checkUserInfoValidation(model);
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("LC_ID",request.getString("lc_id"));
            modelIns.put("CUST_LOT_NO",request.getString("cust_lot_no"));
            
            map = createReturnMapKr((List<Map<String, Object>>)dao.getStockInfoCleanLocLotNo(modelIns), model);
            
        } catch (InterfaceException ife) {
            if (log.isErrorEnabled()) {
                log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
            }
            throw ife;

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to select list :", e);
            }
            throw e;
        }

        return map;
	}
	
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
//		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
//			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
//			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5013", "work_seq is NOT VALID");
//		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
//		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
//			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
//			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
//		}
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

}
