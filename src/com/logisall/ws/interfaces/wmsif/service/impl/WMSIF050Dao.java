package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF050Dao")
public class WMSIF050Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpLoginList
	 * Method 설명 : 로그인
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLoginList(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_login_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpAuthQry
	 * Method 설명 : 권한획득
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpAuthQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_auth_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpVersionQry
	 * Method 설명 : 권한획득
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpVersionQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_version_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpInUsaOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInUsaOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_usa_order_mapping_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpInUsaOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderMappingQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_mapping_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpInUsaOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_detail_qry", model);
		return model;
	}
	
	public Object runSpInOrderDetailQry2(Map<String, Object> model){
		executeUpdate("pk_wmsif050.sp_in_order_detail_qry2", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInOrderRemainQry
	 * Method 설명 : 입고잔여주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderRemainQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_remain_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpPltConfirmQry
	 * Method 설명 : 입고잔여주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPltConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_plt_confirm_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpReceiveInComplete 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_reveive_in_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceiveInComplete 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInPltComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_reveive_in_rti_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInOrderConfirmQry 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInOrderConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_confirm_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpPltMappingConfirmQry
	 * Method 설명 : 매핑시 매핑된 상태인지확인 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPltMappingConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_plt_mapping_confirm_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpPltMappingConfirmQry
	 * Method 설명 : 매핑시 매핑된 상태인지확인 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLcRitemQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_lc_ritem_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpMapPltConfirmQry
	 * Method 설명 : 물류용기입고시 매핑상태 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMapPltConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_map_plt_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpEpcConfirmQry
	 * Method 설명 : 바코드 변환 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEpcConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_epc_confirm_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpEpcHexConfirmQry
	 * Method 설명 : Hex 코드로 EPC코드 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEpcHexConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_epc_hex_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpCustIdQry
	 * Method 설명 : 물류센터별 화주 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpCustIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_cust_id_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceivingOrderStatQry
	 * Method 설명 : 입고 주문 상태 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceivingOrderStatQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_receiving_order_stat_qry", model);
		return model;
	}	
	
	
	/*-
	 * Method ID : selectReceivingOrderStat
	 * Method 설명 : (대화물류)입고 주문 조회
	 * 작성자 : kimzero
	 *
	 * @param model
	 * @return
	 */
	public Object selectReceivingOrderStat(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectReceivingOrderStat", model);
	}	
	
	/*-
	 * Method ID : runSpInAutoOrderInsert
	 * Method 설명 : 출고주문자동입력
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInAutoOrderInsert(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_auto_order_insert", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceivingOrderStatQry
	 * Method 설명 : 롯트번호로 주문정보 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderLotDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_lot_detail_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpSetPartComplete
	 * Method 설명 : 임가공 파트 입고 로직
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSetPartComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_set_part_complete_mobile", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpPartInfoQry
	 * Method 설명 : 임가공 파트 식별표 바코드 정보 획득
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPartInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_set_part_info_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpInLotStatInfoQry
	 * Method 설명 : LOT번호로 정보 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInLotStatInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_lot_stat_info_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpFcmPhoneComplete
	 * Method 설명 : 기기 FCM, 전화번호 전송 로직
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpFcmPhoneComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_fcm_phone_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInBoxAutoOrderInsert
	 * Method 설명 : 입고주문자동입력(box 수량 입력)
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInBoxAutoOrderInsert(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_box_auto_order_insert", model);
		return model;
	}
	
	
	/*-
	 * Method ID : runSpInOrderLotMultiQry
	 * Method 설명 : 입고주문조회시 동일 lot가 있을 경우 주문번호, seq 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderLotMultiQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_lot_multi_qry", model);
		return model;
	}			
	
	/*-
	 * Method ID : runSpReceiveInSapComplete 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInSapComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_auto_order_sap_insert", model);
		return model;
	}	
		
    /**
     * Method ID    : WmsImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object WmsImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif090.sp_delivery_img_data_insert", model);
        return model;
    }
	
	/*-
	 * Method ID : runSpInOrdLocCompleteMobile 
	 * Method 설명 : 주문입력, 로케이션지정, 배차(옵션), 입고 완료 진행
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInOrdLocCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_ord_loc_complete_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInOrdLocCheckMobile 
	 * Method 설명 : 주문입력 입고 / 보그워너
	 * 작성자 : 
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInOrdLocCheckMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_ord_loc_check_mobile", model);
		return model;
	}	
	/*-
	 * Method ID : runSpInLcCarInfoQry 
	 * Method 설명 : 물류센터별 차량조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInLcCarInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_lc_car_info_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpInTakingPicturesMobile 
	 * Method 설명 : 입출고사진저장
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInTakingPicturesMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_taking_pictures_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInInfoQry 
	 * Method 설명 : 물류센터별 기준 정보조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_info_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInOrderInsertQry
	 * Method 설명 : 입고주문등록을 위한 상품코드 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderInsertQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_insert_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpLocWorkingComplete
	 * Method 설명 : 주문입력된 상태에서 모바일 로케이션 지정
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocWorkingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_save_loc_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpTransCustInfoQry
	 * Method 설명 : 입출고거래처조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransCustInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_trans_cust_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpTransItemInfoQry
	 * Method 설명 : 거래처별 상품조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransItemInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_trans_item_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpTransItemInfoQry
	 * Method 설명 : 거래처별 상품조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderDateQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_date_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceiveInOrdInsSimpleIn
	 * Method 설명 : 입고 주문 입력 후 간편 입고 처리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInOrdInsSimpleIn(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_ord_ins_simple_in", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceiveAfterGrn 
	 * Method 설명 : 입하완료 입고확정 프로세스 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveAfterGrn(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_reveive_after_grn", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpPackingPartInspection
	 * Method 설명 : 포장 부품 검수 결과 전송
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPackingPartInspection(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_packing_part_inspection", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceiveOrderInsertConfirmGrn 
	 * Method 설명 : 입고 주문 입력 후 입하확정 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveOrderInsertConfirmGrn(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_order_insert_confirm_grn", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceiveReceivingCompleteGrn 
	 * Method 설명 : 입하확정 후 입고확정
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveReceivingCompleteGrn(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_receiving_complete_confirm_grn", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceivingOrdLocDayComplete 
	 * Method 설명 : 입고주문, 로케이션, 유통기한 입력 후 입고 확정
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceivingOrdLocDayComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_receiving_ord_loc_day_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpCancelGrnBadComplete 
	 * Method 설명 : 입하확정 후 입하 취소 및 불량 입고확정
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpCancelGrnBadComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_cancel_grn_bad_complete", model);
		return model;
	}	

    /**
     * Method ID    : ItemImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object ItemImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif050.sp_item_img_data_insert", model);
        return model;
    }
    
	/*-
	 * Method ID : runSpItemImgSenderMobile 
	 * Method 설명 : 모바일 상품 사진 등록
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpItemImgSenderMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_item_img_path_insert", model);
		return model;
	}	
	
	/*-
	 * Method ID : locDayInMobileComplete
	 * Method 설명 : 로케이션, 유통기한 입력 후 입고 확정
	 * 작성자 : SMICS
	 * 날 짜   : 2020.02.25
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpLocDayInMobileComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_loc_day_in_mobile_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpCustCalPltIdQry
	 * Method 설명 : 물류센터 화주의 정산용 PLT 조회 쿼리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpCustCalPltIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_cust_cal_plt_id_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInIssueLabel
	 * Method 설명 : 레이블 발행 정보 입력
	 * 작성자 : SMICS
	 * 날 짜 : 2020.06.01
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLabel(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_label", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueLabelInfoQry
	 * Method 설명 : 레이블 바코드 정보 조회
	 * 작성자 : smics
	 * 날 짜 : 2020.06.01
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLabelInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_issue_label_info_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceiveInOrdInsDaySimpleIn
	 * Method 설명 : 자동 주문 입력 후 간편 입고(제조일자, 유통기한 추가)
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2020-06-18
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInOrdInsDaySimpleIn(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_ord_ins_day_simple_in", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOrdDiviedCompleteMobile
	 * Method 설명 : 주문을 파렛트 수량으로 나눠서 주문 입력
	 * 작성자 : SMICS
	 * 날 짜 : 2020-09-25
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdDiviedCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_ord_divied_complete_mobile", model);
		return model;
	}
	
	/*-
	 * Method ID : OrdCngQtySimpleIn
	 * Method 설명 : 주문입력 후 SIMPLE IN(수량변경가능)
	 * 작성자 : SMICS
	 * 날 짜 : 2021-04-21
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdCngQtySimpleIn(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_ord_cng_qty_simple_in", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpFixLocConfirmQry
	 * Method 설명 : 픽스 로케이션 추전 상품정보 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpFixLocConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_fix_loc_confirm_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueOrdLabel
	 * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트
	 * 작성자 : SMICS
	 * 날 짜 : 2021.05.16
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueOrdLabel(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_ord_label", model);
		return model;
	}
	
	/*-
	 * Method ID : inOrdDiviedCompleteMobile2048
	 * Method 설명 : 주문을 파렛트 수량으로 나눠서 주문 입력
	 * 작성자 : SMICS
	 * 날 짜 : 2021-07-06
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdDiviedCompleteMobile2048(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_ord_divied_complete_mobile_2048", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueLbLabel
	 * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트
	 * 작성자 : SMICS
	 * 날 짜 : 2021.05.16
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLbLabel(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_lb_label", model);
		return model;
	}
	
	/*-
	 * Method ID : inOrdDiviedCompleteMobile2048
	 * Method 설명 : 발행된 레이블 정보를 기반으로 주문입력, 로케이션 지정, 입고 확정
	 * 작성자 : SMICS
	 * 날 짜 : 2021-07-18
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrd2048LocCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_2048_loc_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : inOrdDiviedCompleteMobile2048DH
	 * Method 설명 : 발행된 레이블 정보를 기반으로 주문입력, 로케이션 지정, 입고 확정
	 * 작성자 : SMICS
	 * 날 짜 : 2022-03-11
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrd2048LocCompleteMobileDH(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_2048_loc_complete_DH", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueLabelLbOrdComplete
	 * Method 설명 : 레이블 발행 정보 입력 후 입고 주문 생성
	 * 작성자 : SMICS
	 * 날 짜 : 2021.08.12
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLabelLbOrdComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_label_lb_ord_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInOmsOpMobileInComplete
	 * Method 설명 :  oms기준 주문입력 및 간편입고
	 * 작성자 : SMICS
	 * 날 짜   : 2021.10.25
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOmsOpMobileInComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_oms_op_mobile_in_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInOrdItemInfoQry
	 * Method 설명 : 주문번호, 상품으로 주문정보 획득
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrdItemInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_ord_item_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : inOrd2048LocCompleteMobileDHSub
	 * Method 설명 : 주문입력(ord_subtype 변수), 로케이션 지정, 입고 확정
	 * 작성자 : SMICS
	 * 날 짜 : 2022-04-27
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrd2048LocCompleteMobileDHSub(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_2048_loc_complete_DH_SUB", model);
		return model;
	}
	
	/*-
	 * Method ID : LcMultiBarInfoQry 
	 * Method 설명 : 상품 다중 바코드 조회 
	 * 날 짜 : 2022-05-17
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInLcMultiBarInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_lc_multi_bar_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceiveInOrdSimpleIn
	 * Method 설명 : 간편 입고
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2022-07-20
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInOrdSimpleIn(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_ord_simple_in", model);
		return model;
	}
	
	/*-
	 * Method ID : getWcsInOrderLotDetailQry 
	 * Method 설명 : 창원 WCS 롯트번호로 주문정보 조회
	 * 날 짜 : 2022-08-19
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpWcsInOrderLotDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_wcs_in_order_lot_detail_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpWcsCwPltInComplete	 
	 * Method 설명 : 창원 wcs 입고 적치 지시 생성
	 * 작성자 : SMICS
	 * 날 짜   : 2022.08.22
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpWcsCwPltInComplete(Map<String, Object> model) {
		executeUpdate("pk_wmswcs010.sp_wcs_receiving_process", model);
		return model;
	}	
	
	/*-
	 * Method ID : getParcelInvcQry 
	 * Method 설명 : 반품입고 송장 검수 조회
	 * 날 짜 : 2022-12-06
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpParcelInvcQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_parcel_invc_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInIssueLabelOrdLoc	
	 * Method 설명 : 다임러 주문입력 , 로케이션지정, 레이블 발행
	 * 작성자 : SMICS
	 * 날 짜 : 2023-01-18
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLabelOrdLoc(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_label_ord_loc", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpLotUnitLocQry 
	 * Method 설명 : LOT번호와  UNIT번호로 로케이션 조회
	 * 날 짜 : 2023-01-18
	 * 작성자 : smics
	 *SP_IN_LOT_LOC_ORDER_QRY
	 * @param model
	 * @return
	 */
	public Object runSpLotUnitLocQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif051.sp_in_lot_loc_order_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpOrd2048LocTypeCompleteMobile
	 * Method 설명 : 주문입력(ord_subtype 변수), 로케이션 지정, 입고 확정, unit_no 포함
	 * 작성자 : SMICS
	 * 날 짜 : 2023-06-29
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrd2048LocTypeCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_2048_loc_type_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpReceiveInCheckCompleteMobile
	 * Method 설명 : 입고 검수 후 입고 완료
	 * 작성자 : SMICS
	 * 날 짜 : 2023-07-11
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInCheckCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_check_complete_mobile", model);
		return model;
	}
	
	/*-
     * Method ID : selectRitemQry
     * Method 설명 : 상품 단순 조회 쿼리
     * DATE : 2023-08-08
     * 작성자 : schan
     *
     * @param model
     * @return
     */
    public Object selectRitemQry(Map<String, Object> model) {
        return executeQueryForList("wmsif050.selectRitemQry", model);
    }   
    
    /*-
	 * Method ID : runSpReceivingOrdLocDayComplete 
	 * Method 설명 : 입고주문, 로케이션, 유통기한, 제조일자 입력 후 입고 확정
	 * 작성자 : 
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceivingOrdLocDayComplete2(Map<String, Object> model) {
		//executeUpdate("pk_wmsif050.sp_receiving_ord_loc_day_complete", model);
		executeUpdate("pk_wmsst001_4.sp_ord_loc_day_complete2", model);
		return model;
	}	
    
    /*-
     * Method ID : receivingChkCompleteMobile
     * Method 설명 : 카카오VX 입고 검수 완료
	 * 작성자 : 
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceivingChkCompleteMobile(Map<String, Object> model) {
		//executeUpdate("pk_wmsif050.sp_receiving_ord_loc_day_complete", model);
		executeUpdate("pk_wmsop021.sp_receiving_chk_complete_mobile", model);
		return model;
	}	
	
	/*-
     * Method ID : inorderOmSelectQry
     * Method 설명 : 입고주문(OM)조회 쿼리
     * DATE : 2023-12-14
     * 작성자 : schan
     *
     * @param model
     * @return
     */
    public Object inorderOmSelectQry(Map<String, Object> model) {
        return executeQueryForList("wmsif050.inorderOmSelectQry", model);
    }
    
	/*-
     * Method ID : inorderOmSelectOvQry
     * Method 설명 : 올리브영 입고주문(OM)조회 쿼리
     * DATE : 2025-01-09
     * 작성자 : yoseop
     *
     * @param model
     * @return
     */
    public Object inorderOmSelectOvQry(Map<String, Object> model) {
        return executeQueryForList("wmsif050.inorderOmSelectOvQry", model);
    }
    
    /*-
    * Method ID : inOrderOmOrdIdSelectQry
    * Method 설명 : 입고주문(OM) 주문번호 등록 
    * DATE : 2023-12-19
    * 작성자 : schan
    *
    * @param model
    * @return
    */
   public Object inOrderOmOrdIdSelectQry(Map<String, Object> model) {
       return executeQueryForList("wmsif050.inOrderOmOrdIdSelectQry", model);
   }
   
   /*-
    * Method ID : inOrderOmOrdIdUpsert
    * Method 설명 : 입고주문(OM) 주문번호 등록 
    * DATE : 2023-12-19
    * 작성자 : schan
    *
    * @param model
    * @return
    */
   public int inOrderOmOrdIdUpsert(Map<String, Object> model) {
       int res = executeUpdate("wmsif050.inOrderOmOrdIdHeaderUpsert", model);
       res = executeUpdate("wmsif050.inOrderOmOrdIdDetailUpsert", model);
       return res;
   }
   /*-
    * Method ID : inOrderOmOrdIdUpsertOv
    * Method 설명 : 입고주문(OM) 주문번호 등록  olive
    * DATE : 2025-01-21
    * 작성자 : yoseop
    *
    * @param model
    * @return
    */
   public int inOrderOmOrdIdUpsertOv(Map<String, Object> model) {
       int res = executeUpdate("wmsif050.inOrderOmOrdIdHeaderUpsertOv", model);
       res = executeUpdate("wmsif050.inOrderOmOrdIdDetailUpsertOv", model);
       return res;
   }
   
   /*-
     * Method ID : runSpReceivingSetPartCompleteMobile
     * Method 설명 : SKON 임가공 구성품 입고(총량)
     * 작성자 : MonkeySeok
     * 날   짜 : 2024-01-29
	 * @param model
	 * @return
	 */
	public Object runSpReceivingSetPartCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_receiving_set_part_complete_mobile", model);
		return model;
	}	
	   
	   /*-
	     * Method ID : runSpReceivingSetPartSerialCompleteMobile
	     * Method 설명 : SKON 임가공 구성품 입고(시리얼 구조 포함)
	     * 작성자 : MonkeySeok
	     * 날   짜 : 2024-01-29
		 * @param model
		 * @return
		 */
		public Object runSpReceivingSetPartSerialCompleteMobile(Map<String, Object> model) {
			executeUpdate("pk_wmsif050.sp_receiving_set_part_serial_complete_mobile", model);
			return model;
		}	
		
		/*-
		 * Method ID : runSpInIssueLabelLbOrdSubComplete
     * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트, ord_subtype, sap_barcode, work_memo 추가
     * 작성자 : SMICS
     * 날 짜 : 2024.02.02
		 *
		 * @param model
		 * @return
		 */
		public Object runSpInIssueLabelLbOrdSubComplete(Map<String, Object> model) {
			executeUpdate("pk_wmsif050.sp_in_issue_label_lb_ord_sub_complete", model);
			return model;
		}
   
	public Object selectItemGroup(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectItemGroup", model);
	}
	public Object selectItemFirst(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectItemFirst", model);
	}
	public Object selectItemSecond(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectItemSecond", model);
	}
	public Object selectItemLast(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectItemLast", model);
	}
	public Object selectItemCode(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectItemCode", model);
	}
	public Object selectLot(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectLot", model);
	}
	public Object checkCust_lot_no(Map<String, Object> model) {
		return executeQueryForList("wmsif050.checkCust_lot_no", model);
	}
	public Object selectLot_2(Map<String, Object> model) {
		return executeQueryForList("wmsif050.selectLot_2", model);
	}
	   
   /*-
     * Method ID : runSpReceivingSetPartCompleteMobile
     * 개체 시리얼 인식 후 입고 처리
     * 작성자 : SMICS
     * 날 짜 : 2024.02.26
	 */
	public Object runSpInSerialPltCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif051.sp_in_serial_plt_complete_mobile", model);
		return model;
	}
	
	public Object runSpReceiveInOrdInsDaySimpleInStlogis(Map<String, Object> model) {
		executeUpdate("pk_wmsop021.sp_rcv_in_order_2048_loc_complete", model);
		return model;
	}
	
	/**
     * Method ID : getLocIdByLocType
     * Method 설명 : 로케이션 조회(로케이션 타입)
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
	public Object getLocIdByLocType(Map<String, Object> model) {
		return executeQueryForObject("wmsif050.getLocIdByLocType", model);
	}
	
	/**
     * Method ID : saveInOrder
     * Method 설명 : 주문 등록
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object saveInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order_2048array", model);
        return model;
    }
    
	/**
     * Method ID : saveLoc
     * Method 설명 : 로케이션 지정
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object saveLoc(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001_sable7.sp_save_loc", model);
        return model;
    }
    
	/**
     * Method ID : saveInComplete
     * Method 설명 : 입고 확정
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object saveInComplete(Map<String, Object> model){
    	return executeUpdate("wmsop020.pk_wmsop020.sp_in_complete", model);
    }
    
	/**
     * Method ID : getUomId
     * Method 설명 : UOM_ID 조회
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object getUomId(Map<String, Object> model) {
    	return executeQueryForObject("wmsif050.getUomId", model);
    }
    
	/**
     * Method ID : checkSetItem
     * Method 설명 : 임가공 상품 여부 조회
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public String getSetItemYn(Map<String, Object> model) {
    	return (String) executeQueryForObject("wmsif050.getSetItemYn", model);
    }
    
	/**
     * Method ID : getPartItemList
     * Method 설명 : 구성품 ritem_id 조회
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object getPartItemList(Map<String, Object> model) {
    	return executeQueryForList("wmsif050.getPartItemList", model);
    }
    
	/**
     * Method ID : getOrdDetail
     * Method 설명 : 주문 상세 정보 조회
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object getOrdDetail(Map<String, Object> model) {
    	return executeQueryForList("wmsif050.getOrdDetail", model);
    }
    
    /**
     * Method ID : getSerialStockLotNoCount
     * Method 설명 : 현재고 LotNo 카운트
     * 작성자 : 
     * @param model
     * @return
     */
    public Object getSerialStockLotNoCount(Map<String, Object> model) {
        return executeQueryForObject("wmsif050.getSerialStockLotNoCount", model);
    }
    
	/**
     * Method ID : initWork
     * Method 설명 : INSERT WMSST500, WMSST501
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object initWork(Map<String, Object> model) {
    	return executeUpdate("pk_wmsop001.sp_init_work", model);
    }
    
	/**
     * Method ID : insertSerialInventory
     * Method 설명 : INSERT WMSTS010 시리얼 거점재고
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object insertSerialInventory(Map<String, Object> model) {
    	return executeInsert("wmsts010.insert", model);
    }
    
    public void insertSerialInventory(List list) {
    	executeInsertBatch("wmsts010.insert", list, 5000);
    }
    
	/**
     * Method ID : deleteSerialInventory
     * Method 설명 : DELETE WMSTS010 시리얼 거점재고
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object deleteSerialInventory(Map<String, Object> model) {
    	return executeInsert("wmsts010.delete", model);
    }
    
	/**
     * Method ID : insertSerialWorkHistory
     * Method 설명 : INSERT WMSTW010 시리얼번호 작업이력추적
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object insertSerialWorkHistory(Map<String, Object> model) {
    	return executeInsert("wmstw010.insert", model);
    }
    
    public void insertSerialWorkHistory(List list) {
    	executeInsertBatch("wmstw010.insert", list, 5000);
    }
    
    /**
     * Method ID : insertWorkHistory
     * Method 설명 : 시리얼 작업 이력 추가
     * 작성자 : 
     * @param model
     * @return
     */
    public Object insertWorkHistory(Map<String, Object> model) {
        return executeInsert("wmstw010.insertWorkHistory", model);
    }
    
    /**
     * Method ID : updateWorkHistory
     * Method 설명 : 시리얼 작업 이력 결과 업데이트
     * 작성자 : 
     * @param model
     * @return
     */
    public Object updateWorkHistory(Map<String, Object> model) {
        return executeUpdate("wmstw010.updateWorkHistory", model);
    }
    
    /**
     * Method ID : getLastWorkId
     * Method 설명 : 시리얼 작업 ID 조회
     * 작성자 : 
     * @param model
     * @return
     */
    public String getLastWorkId(Map<String, Object> model) {
        return (String) executeQueryForObject("wmstw010.getLastWorkId", model);
    }
    
	/**
     * Method ID : insertSerialHistoryTracking
     * Method 설명 : INSERT WMSTR010 시리얼번호 이력추척
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object insertSerialHistoryTracking(Map<String, Object> model) {
    	return executeInsert("wmstr010.insert", model);
    }

    public void insertSerialHistoryTracking(List list) {
    	executeInsertBatch("wmstr010.insert", list, 5000);
    }

	/**
     * Method ID : getPartSerial
     * Method 설명 : PART_SERIAL 정보 조회 (SET_SERIAL_NO)
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object getPartSerial(Map<String, Object> model) {
    	return executeQueryForList("wmsif050.getPartSerial", model);
    }

	/**
     * Method ID : getWorkId
     * Method 설명 : SELECT WORK_ID
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public String getWorkId(Map<String, Object> model) {
    	return (String)executeQueryForObject("wmsif050.getWorkId", model);
    }
    
	/**
     * Method ID : getOrdCount
     * Method 설명 : 금일 등록된 주문 개수 조회
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object getOrdCount(Map<String, Object> model) {
    	return executeQueryForObject("wmsif050.getOrdCount", model);
    }
    
	/**
     * Method ID : getOrdIdTodayLastSave
     * Method 설명 : 금일 등록된 주문 아이디 조회
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object getOrdIdTodayLastSave(Map<String, Object> model) {
    	return executeQueryForObject("wmsif050.getOrdIdTodayLastSave", model);
    }
    
	/**
     * Method ID : getStockHistory
     * Method 설명 : WMSTS010 SERIAL_NO LC_ID 조회
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object getStockHistory(Map<String, Object> model) {
    	return executeQueryForObject("wmsif050.getStockHistory", model);
    }
    
	/**
     * Method ID : getMappingCnt
     * Method 설명 : WMSSI010 매핑 개수 조회 (SET_SERIAL_NO)
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Integer getMappingCnt(Map<String, Object> model) {
    	return (Integer)executeQueryForObject("wmsif050.getMappingCnt", model);
    }
    
	/**
     * Method ID : updateMappingYn
     * Method 설명 : UPDTAE WMSSI010 MAPPING_YN
     * 작성자 : wl2258
	 * @param model
	 * @return
	 */
    public Object updateMappingYn(Map<String, Object> model) {
    	return executeUpdate("wmsif050.updateMappingYn", model);
    }
    
	/**
     * Method ID : getOrdCount
     * Method 설명 : 부분 입고된 수량 조회
     * 작성자 : seoprim12
	 * @param model
	 * @return
	 */
    public Object getSumWorkQty(Map<String, Object> model) {
    	return executeQueryForList("wmsif050.getSumWorkQty", model);
    }
}
