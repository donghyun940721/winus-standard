package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOGodomall;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOGodomallItem;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOShopbuy;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOShopbuyItem;
import com.logisall.ws.interfaces.wmsif.service.WMSIF102Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF102Service")
public class WMSIF102ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF102Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF102Dao")
	private WMSIF102Dao dao;

	@Autowired
	WMSIF102ServiceImpl(WMSIF102Dao dao) {
		super(dao);
	}
		
	
	public Map<String, Object> selectGodomallOrderInQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpGodomallOrderInQry(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF102", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKrDlv(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateGodomallOrderSearchComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOGodomall data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONGodomall(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[=== InterfaceVOGodomall data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
//			try {
//
//				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
//				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());
//
//				checkUserInfoValidation(model);
//
//			} catch (InterfaceException ife) {
//				if (log.isErrorEnabled()) {
//					log.error(ife.getMsgCode());
//				}
//				throw ife;
//
//			}


			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOGodomall(data);				
				if (log.isInfoEnabled()) {
					//log.info("[=== InterfaceVOGodomall workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapGodomall(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						//log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns.put("LC_ID"			, (String) model.get("LC_ID"));
					modelIns.put("USER_NO"	, (String) model.get("USER_NO"));
					modelIns.put("CUST_ID"		, (String) model.get("CUST_ID"));
					modelIns = (Map<String, Object>) dao.runSpGodomallOrderSearchComplete(modelIns);
					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF102", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateGodomallItemInfoComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOGodomallItem data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONGodomallItem(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
//			try {
//
//				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
//				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());
//
//				checkUserInfoValidation(model);
//
//			} catch (InterfaceException ife) {
//				if (log.isErrorEnabled()) {
//					log.error(ife.getMsgCode());
//				}
//				throw ife;
//
//			}


			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOGodomallItem(data);				
				if (log.isInfoEnabled()) {
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapGodomallItem(data, workList);
					if (log.isInfoEnabled()) {
						log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						//log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns.put("LC_ID"			, (String) model.get("LC_ID"));
					modelIns.put("USER_NO"	, (String) model.get("USER_NO"));
					modelIns.put("CUST_ID"		, (String) model.get("CUST_ID"));
					modelIns = (Map<String, Object>) dao.runSpGodomallItemInfoComplete(modelIns);
					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF102", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateShopbuyOrderSearchComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOShopbuy data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONShopbuy(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("[=== InterfaceVOGodomall data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
//			try {
//
//				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
//				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());
//
//				checkUserInfoValidation(model);
//
//			} catch (InterfaceException ife) {
//				if (log.isErrorEnabled()) {
//					log.error(ife.getMsgCode());
//				}
//				throw ife;
//
//			}


			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOShopbuy(data);				
				if (log.isInfoEnabled()) {
					log.info("[=== InterfaceVOGodomall workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapShopbuy(data, workList);
					if (log.isInfoEnabled()) {
						log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						//log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns.put("LC_ID"			, (String) model.get("LC_ID"));
					modelIns.put("USER_NO"	, (String) model.get("USER_NO"));
					modelIns.put("CUST_ID"		, (String) model.get("CUST_ID"));
					modelIns = (Map<String, Object>) dao.runSpShopbuyOrderSearchComplete(modelIns);
					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF102", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateShopbuyItemSearchComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOShopbuyItem data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONShopbuyItem(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[=== InterfaceVOGodomall data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
//			try {
//
//				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
//				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());
//
//				checkUserInfoValidation(model);
//
//			} catch (InterfaceException ife) {
//				if (log.isErrorEnabled()) {
//					log.error(ife.getMsgCode());
//				}
//				throw ife;
//
//			}


			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOShopbuyItem(data);				
				if (log.isInfoEnabled()) {
					//log.info("[=== InterfaceVOGodomall workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapShopbuyItem(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						//log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns.put("LC_ID"			, (String) model.get("LC_ID"));
					modelIns.put("USER_NO"	, (String) model.get("USER_NO"));
					modelIns.put("CUST_ID"		, (String) model.get("CUST_ID"));
					modelIns = (Map<String, Object>) dao.runSpShopbuyItemSearchComplete(modelIns);
					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF102", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
//		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
//			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
//			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5013", "work_seq is NOT VALID");
//		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
//		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
//			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
//			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
//		}
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

}

