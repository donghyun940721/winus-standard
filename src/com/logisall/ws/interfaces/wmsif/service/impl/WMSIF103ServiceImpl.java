package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.wmsif.service.WMSIF103Service;

@Service("WMSIF103Service")
public class WMSIF103ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF103Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF103Dao")
	private WMSIF103Dao dao;
	
	@Autowired
	WMSIF103ServiceImpl(WMSIF103Dao dao) {
		super(dao);
	}
			
	public Map<String, Object> get_stock_list(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Map<String, Object> modelIns = model;
			log.info(" ********** modelIns ====================================: " + modelIns);
			modelIns = (Map<String, Object>) dao.get_stock_list(modelIns);

			InterfaceUtil.isValidReturnCode("WMSIF117", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) log.error("Fail to select list :", e);
			throw e;
		}
		return map;
	}
	
	public Map<String, Object> if_transfer_stockid_complete(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Map<String, Object> modelIns = model;
			log.info(" ********** modelIns ====================================: " + modelIns);
			modelIns = (Map<String, Object>) dao.if_transfer_stockid_complete(modelIns);

			InterfaceUtil.isValidReturnCode("WMSIF117", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");;

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) log.error("Fail to select list :", e);
			throw e;
		}
		return map;
	}
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {
		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

}
