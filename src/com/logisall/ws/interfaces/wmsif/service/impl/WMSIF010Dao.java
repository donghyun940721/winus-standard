package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF010Dao")
public class WMSIF010Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
//	public Object runSpLoginList(Map<String, Object> model) {
//		executeUpdate("pk_wmsif010.sp_login_list", model);
//		return model;
//	}

	
	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	public Object runSpAcceptingList(Map<String, Object> model) {
		executeUpdate("pk_wmsif010.sp_accepting_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpAcceptingQry 
	 * Method 설명 : 입하지사 프로시져 호출, 취득한 정보를 List형태로 반환
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpAcceptingQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif010.sp_accepting_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingComplete 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpAcceptingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif010.sp_accepting_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceivingList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceivingList(Map<String, Object> model) {
		executeUpdate("pk_wmsif010.sp_receiving_list", model);
		return model;
	}	

	/*-
	 * Method ID : runSpInQry 
	 * Method 설명 : 입고지사 프로시져 호출, 취득한 정보를 List형태로 반환
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif010.sp_in_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpInComplete 
	 * Method 설명 : 입고완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif010.sp_in_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInComplete 
	 * Method 설명 : 입고완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpMappingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif010.sp_mapping_complete", model);
		return model;
	}

}
