package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

@Repository("WMSIF000Dao")
public class WMSIF000Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
	/*-
	 * Method ID : ifProductivityList
	 * Method 설명 : 새로피엔엘 생산성 관리 모바일 화면 조회
	 * 작성자 : schan
	 */
	public Object ifProductivityList(Map<String, Object> model) {
		return executeQueryForList("wmsif101.ifProductivityList", model);
	}
	
	/*-
	 * Method ID : ifProductivityListMonth
	 * Method 설명 : 새로피엔엘 생산성 관리 모바일 화면 조회(월간)
	 * 작성자 : schan
	 */
	public Object ifProductivityListMonth(Map<String, Object> model) {
		return executeQueryForList("wmsif101.ifProductivityListMonth", model);
	}
	
	/**
	 * Method ID    :  ifProductivityUserNm 
	 * Method 설명 : 새로피엔엘 생산성입력/조회 모바일 사용자 이름 검색  
	 * 작성자          : KSJ
	 * @param model
	 * @return
	 */
	public Object ifProductivityUserNm(Map<String, Object> model) {
		return executeQueryForList("wmsif101.ifProductivityUserNm", model);
	}
	
	/**
	 * Method ID : ifProductivityListInsert 
	 * Method 설명 : 새로피엔엘 생산성입력/조회 모바일
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object ifProductivityListInsert(Map<String, Object> model) {
		return executeInsert("wmsif101.ifProductivityListInsert", model);
	}

	/**
	 * Method ID : ifProductivityListUpdate
	 * Method 설명 : 새로피엔엘 생산성입력/조회 모바일
	 * 작성자 : schan
	 * 
	 * @param model
	 * @return
	 */
	public Object ifProductivityListUpdate(Map<String, Object> model) {
		return executeUpdate("wmsif101.ifProductivityListUpdate", model);
	}
}