package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF030Dao")
public class WMSIF030Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());

	/*-
	 * Method ID : runSpRtiAlertComplete 
	 * Method 설명 : 용기경고 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpRtiAlertComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_rti_alert", model);
		return model;
	}

	
	/*-
	 * Method ID : runSpIsolationList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	public Object runSpIsolationList(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_isolation_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpIsolationQry 
	 * Method 설명 : 격리지사 프로시져 호출, 취득한 정보를 List형태로 반환
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpIsolationQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_isolation_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpIsolationComplete 
	 * Method 설명 : 격리완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpIsolationComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_isolation_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpMoveList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveList(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_move_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpMoveQry 
	 * Method 설명 : 이동지시 프로시져 호출, 취득한 정보를 List형태로 반환
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpMoveQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_move_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpMoveComplete 
	 * Method 설명 : 이동완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpMoveComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_move_complete", model);
		return model;
	}

	
	public Object runSpStockList(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_stock_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpStockQry 
	 * Method 설명 : 재고조사지사 프로시져 호출, 취득한 정보를 List형태로 반환
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpStockQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_stock_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpStockComplete 
	 * Method 설명 : 재고조사완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpStockComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif030.sp_stock_complete", model);
		return model;
	}

}
