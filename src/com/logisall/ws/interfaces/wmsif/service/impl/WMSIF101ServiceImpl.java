package com.logisall.ws.interfaces.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.sql.PreparedStatement;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOHOWSER;
import com.logisall.ws.interfaces.wmsif.service.WMSIF101Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF101Service")
public class WMSIF101ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF101Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF101Dao")
	private WMSIF101Dao dao;

	@Autowired
	WMSIF101ServiceImpl(WMSIF101Dao dao) {
		super(dao);
	}
		
	
	public void updateifHowereDlvData(Map<String, Object> model) throws Exception {
		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOHOWSER data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONHOWSER(inputJSON);
			log.info("[ data1 ] :" + data);

			if (log.isInfoEnabled()) {
				log.info("[ data2 ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");
		}

		if (data != null) {
			try {
				workList = getDataRowFromInterfaceVOHOWSER(data);
				if (log.isInfoEnabled()) {
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");
			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapHOWSER(data, workList);
					Map<String, Object> map = new HashMap<String, Object>();
					
					/************* iRunner HOWSER 인터페이스 이미지 저장 처리 부분 *************   **시작**/
					List listImg = new ArrayList();
					Map rowMap = null;
					rowMap = new LinkedHashMap();
					rowMap.put("CUST_CD", "KGK");
					rowMap.put("ORG_ORD_ID", data.getAgencyMgrCode());
					
					//이미지 : 사인이미지 유무 체크
					if(data.getGoodsResult() != null){
						rowMap.put("SERIAL_IMG_PATH", data.getGoodsResult().get(0).getRentalSrlImg());
					}
					//이미지 : 제품이미지 유무 체크
					String DLV_IMG_PATH = "";
					if(data.getOnsiteImages() != null){
						for (int k = 0; k < data.getOnsiteImages().size(); k++) {
							if(k == 0){
								DLV_IMG_PATH += (String)data.getOnsiteImages().get(k);
							}else{
								DLV_IMG_PATH += "\n"+(String)data.getOnsiteImages().get(k);
							}
						}						
						System.out.println("DLV_IMG_PATH : " + DLV_IMG_PATH);
						rowMap.put("DLV_IMG_PATH", DLV_IMG_PATH);
					
						listImg.add(rowMap);
						
						//이미지 인터페이스 시작 & 고객사 원주문번호 유무 체크 리턴값
						map = excelUploadRstApiImgGet(listImg);
						log.info("[ map  errCnt ] :" + map.get("errCnt"));
					}
					/**********************************************************   **종료**/
					
					if (log.isInfoEnabled()) {
						log.info("[ modelIns ] :" + modelIns);
					}

					//고객사 원주문번호 유무 체크따른 인터페이스 처리
					if(map.get("errCnt").equals(0)){
						// log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpIfHowereDlvData(modelIns);
						
						model.put("RETURN_CD", 0);
						model.put("RETURN_MSG", 0);
					}else{
						model.put("RETURN_CD", -1);
						model.put("RETURN_MSG", "No agencyMgrCode");
						model.put("ERRCODE", -1);
					}
				}
			} catch (InterfaceException ife) {
				model.put("RETURN_CD", -1);
				model.put("RETURN_MSG", 9);
				model.put("ERRCODE", -1);
				throw ife;

			} catch (Exception e) {
				model.put("RETURN_CD", -1);
				model.put("RETURN_MSG", 9);
				model.put("ERRCODE", -1);
				
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;
			}
		}
	}

	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}
	
	/**
     * Method ID : excelUploadRstApiImgGet
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	public Map<String, Object> excelUploadRstApiImgGet(List list) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
                Map<String, Object> paramMap = null;

    			for (int i = 0; i < list.size(); i++) {
    				paramMap = (Map) list.get(i);
    				Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("CUST_CD"	, paramMap.get("CUST_CD"));
					modelSP.put("ORG_ORD_ID", paramMap.get("ORG_ORD_ID"));
    				
					//고객사원주문번호 유무 체크
					String dlvOrdId  = dao.checkExistData(modelSP);
    				String serialUrl = (String)paramMap.get("SERIAL_IMG_PATH");
    				String[] addDlvImgPath = paramMap.get("DLV_IMG_PATH").toString().split("\n");

    				for (int k = 0; k < addDlvImgPath.length; k++) {
		            	if (  dlvOrdId != null && !StringUtils.isEmpty(dlvOrdId)
		            	   && addDlvImgPath[k] != null && !StringUtils.isEmpty(addDlvImgPath[k])){
		            		
							// 1. 원주문번호 검색 후 이미지URL이 있으면 이미지 파일 저장.
		            		String ext       = ".jpeg";
		            		String fileId    = dlvOrdId + "_" + CommonUtil.getLocalDateTime().substring(0, 8) + "photo"+(k+1) + ext;
		            		String fileRoute = "E:/ATCH_FILE/INTERFACE/"+CommonUtil.getLocalDateTime().substring(2, 8)+"/"+dlvOrdId+"_"+CommonUtil.getLocalDateTime().substring(0, 8);
		            		String fileNewNm = "photo"+(k+1);
		            		String fileSize  = "0";
		            		String workOrdId = dlvOrdId;
		            		
		            		String imgUrl = addDlvImgPath[k];
		            		String dir    = "E:\\ATCH_FILE\\INTERFACE\\"+CommonUtil.getLocalDateTime().substring(2, 8)+"\\"+dlvOrdId+"_"+CommonUtil.getLocalDateTime().substring(0, 8);
		            		String serialFile = "photo0"+ext;
		            		String goodsFile  = "photo"+(k+1)+ext;
		            		
		            		if(k == 0 && serialUrl != null && !StringUtils.isEmpty(serialUrl)){
		            			// 시리얼 이미지가 있으면 저장
		            			binaryImgSave(serialUrl, dir, serialFile);
		            		}
		            		// 상품 이미지 저장
		            		binaryImgSave(imgUrl, dir, goodsFile);
		            		
		            		// 2. 이미지 파일 저장 완료 후 저장정보 DB Insert.	            		
		            		Map<String, Object> modelDt = new HashMap<String, Object>();
		            		modelDt.put("FILE_ID"		, fileId);
							modelDt.put("ATTACH_GB"		, "INTERFACE"); // 통합 HELPDESK 업로드
							modelDt.put("FILE_VALUE"	, "wmssp010"); // 기존코드 "tmsba230" 로
							modelDt.put("FILE_PATH"		, fileRoute); // 서버저장경로
							modelDt.put("ORG_FILENAME"	, fileNewNm + ext); // 원본파일명
							modelDt.put("FILE_EXT"		, ext); // 파일 확장자
							modelDt.put("FILE_SIZE"		, fileSize);// 파일 사이즈
							modelDt.put("WORK_ORD_ID"	, workOrdId); // 원본파일명
							
							String fileMngNo = (String) dao.t2FileInsert(modelDt);
							dao.insertInfo(modelDt);
							
		                    map.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
		                    map.put("MSG_ORA", "");
		                    map.put("errCnt", errCnt);
	                    }else{
		                    map.put("errCnt", 1);
	                    }
		            }
    			}
            } catch(Exception e){
            	map.put("MSG", MessageResolver.getMessage("save.error"));
            	map.put("MSG_ORA", e.getMessage());
            	map.put("errCnt", "1");
                throw e;
            }
        return map;
    }    
    
	public void binaryImgSave(String imgUrl, String dir, String file) throws Exception{
		//하우저 인증키 정보 필수
        //개발서버
		//String key    = "OpenApiKey";
		//String value  = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1ZWVmZGM1My03ZDk4LTQyNzMtOGM5Yi0xMTcyZTE0MmNiNDAiLCJnIjoiZ3JwMTgwODAwMDA2Iiwic3YiOnsiMSI6MX19.pbbcziGaU_oNOSu_NoGXgcMT2kjvshk5isXjLYObsAHshSiWoC6QAxmOqG8fSoO8hwmRHP32Ld8anwnN6uefDDivleocAAZj3S-9RaM_xaKqJUJsbtOEtYaKt4u6ZBBUD8Xy-ZUdLgiID5qw-YfYSY6WeWVmmOuU3v0oc7Gn8e4";
        //운영서버
		String key    = "OpenApiKey";
		String value  = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIzZWY3YWY5OC0yMzYyLTQwY2MtYmNhYS1hYjE0OGFhYWQ2ZjIiLCJnIjoiZ3JwMTgwODAwMDE1Iiwic3YiOnsiMSI6MX19.UP9_Q1N30vUUTt3pr1EjZ3sybrpekqamlY2yHjl2a9oUuXiqss9T2_iBEh86VQXPCCkTuJYSgjMqjD36WLFaP2qJdFSK1ggdZVkYh-5mbcIA8r1P3XudG4gw1Qvsv9kSd84Jd3G-aF5Vt4Fa1icUpvyLmMBrlb9Jwa1_UX7-UHo";
        String rstUrl = imgUrl;
        
		URL url = new URL(rstUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
		con.setConnectTimeout(10000);	//서버에 연결  Timeout 설정
		con.setReadTimeout(10000);		// InputStream 읽기 Timeout 설정
		con.setRequestMethod("GET");
		con.setDoOutput(false); 
		con.setDoInput(true);
		
		//header 파라미터셋팅
		con.addRequestProperty(key, value);
		con.setRequestProperty("Content-Type", "application/json");
		
		if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
        	int len = con.getContentLength();
            byte[] tmpByte = new byte[len];
            
            File chkDir   = new File(dir);
            File savePath = new File(dir +"\\"+ file);

            if(!chkDir.isDirectory()){
            	// 해당 폴더가 없으면 생성
            	chkDir.mkdirs();
			}
            
            InputStream is = con.getInputStream();
            FileOutputStream fos = new FileOutputStream(savePath);

            int read;
            while(true) {
                read = is.read(tmpByte);
                if (read <= 0) {
                    break;
                }
                fos.write(tmpByte, 0, read); //file 생성
            }

            is.close();
            fos.close();
            con.disconnect();
		}
	}
	
	public Map<String, Object> selectSweettrackerDlvTraceQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapDlvTrace(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSweettrackerDlvTraceQry(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF101", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapDlvTrace(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectTwelvecmGoodsinfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapTwelvecmTrace(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);
			//log.info(" ********** outputList 1 : " + modelIns);
			
			map = (Map<String, Object>) dao.runSpTwelvecmGoodsinfoQry(modelIns);
			
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
//		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
//			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
//			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5013", "work_seq is NOT VALID");
//		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
//		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
//			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
//			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
//		}
	}
	
}

