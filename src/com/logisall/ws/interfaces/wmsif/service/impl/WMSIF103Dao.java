package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF103Dao")
public class WMSIF103Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	public Object get_stock_list(Map<String, Object> model) {
		executeUpdate("pk_wmsif117.sp_get_stock_list", model);
		return model;
	}	
	
	public Object if_transfer_stockid_complete(Map<String, Object> model) {
		executeUpdate("pk_wmsif117.sp_transfer_stockid_complete", model);
		return model;
	}
}
