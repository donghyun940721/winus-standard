package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.wmsif.service.WMSIF010Service;

@Service("WMSIF010Service")
public class WMSIF010ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF010Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF010Dao")
	private WMSIF010Dao dao;

	@Autowired
	WMSIF010ServiceImpl(WMSIF010Dao dao) {
		super(dao);
	}
		
	
//	public Map<String, Object> selectLoginList(Map<String, Object> model) throws Exception {
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//
//			checkParamMapValidationForList(model);
//
//			checkUserInfoValidation(model);
//
//			Map<String, Object> modelIns = createSPParamMapKR(model);
//			
//			log.info(" ********** modelIns ====================================: " + modelIns);
//
//			// log.info(" ********** outputList 1 : " + modelIns);
//			modelIns = (Map<String, Object>) dao.runSpLoginList(modelIns);
//
//			// log.info(" ********** outputList 2 : " + modelIns);
//			InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
//
//			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
//			log.info(" ********** outputList : " + outputList);
//
//			map = createReturnMapKr(outputList, model);
//
//		} catch (InterfaceException ife) {
//			if (log.isErrorEnabled()) {
//				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
//			}
//			throw ife;
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to select list :", e);
//			}
//			throw e;
//		}
//
//		return map;
//
//	}
	
//	public Map<String, Object> selectLoginList(Map<String, Object> model) throws Exception {
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);
//		
//		InterfaceBaseVO data = null;
//		
//		try {
//			if (log.isInfoEnabled()) {
//				log.info("[ inputJSON =================] :" + inputJSON);
//			}
//
//			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);
//
//			if (log.isInfoEnabled()) {
//				log.info("[ data ==========================] :" + data);
//			}
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to excecute complete :", e);
//			}
//			throw new InterfaceException("50005", "JSON Data Convert Error");
//
//		}
//		
//		try {
//			
//			model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
//			model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());
//
//			//checkParamMapValidationForList(model);
//			checkUserInfoValidation(model);
//
//			Map<String, Object> modelIns = createSPParamMapKR(model);
//
//			// log.info(" ********** outputList 1 : " + modelIns);
//			modelIns = (Map<String, Object>) dao.runSpLoginList(modelIns);
//
//			// log.info(" ********** outputList 2 : " + modelIns);
//			InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
//
//			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
//			log.info(" ********** outputList : " + outputList);
//
//			map = createReturnMapKr(outputList, model);
//
//		} catch (InterfaceException ife) {
//			if (log.isErrorEnabled()) {
//				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
//			}
//			throw ife;
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to select list :", e);
//			}
//			throw e;
//		}
//
//		return map;
//
//	}
	
	public Map<String, Object> selectAcceptingList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpAcceptingList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectAcceptingQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidation(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			modelIns = (Map<String, Object>) dao.runSpAcceptingQry(modelIns);

			InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateAcceptingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVO data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVO(data);
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMap(data, workList);
					
					if (log.isInfoEnabled()) {
						log.info("===========================================================================");
						log.info("[ modelIns ] :" + modelIns);
					}

					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpAcceptingComplete(modelIns);
					InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectReceivingList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpReceivingList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public Map<String, Object> selectInList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidation(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpInQry(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateInComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVO data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVO(data);
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMap(data, workList);

					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpInComplete(modelIns);
					InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void mappingInComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVO data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVO(data);
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMap(data, workList);

					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMappingComplete(modelIns);
					InterfaceUtil.isValidReturnCode("WMSIF010", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
		}
		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
		}
		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
			throw new InterfaceException("5013", "work_seq is NOT VALID");
		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
		}
		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
		}
		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
		}
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

}
