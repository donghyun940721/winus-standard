package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF020Dao")
public class WMSIF020Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());

	
	/*-
	 * Method ID : runSpPickingList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPickingList(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_picking_list", model);
		return model;
	}
	/*-
	 * Method ID : runSpPickingQry 
	 * Method 설명 : 피킹지사 프로시져 호출, 취득한 정보를 List형태로 반환
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpPickingQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_picking_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpPickingComplete 
	 * Method 설명 : 피킹완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpPickingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_picking_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpShppingList
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShppingList(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_shipping_list", model);
		return model;
	}	

	/*-
	 * Method ID : runSpSearchQry 
	 * Method 설명 : 출하지사 프로시져 호출, 취득한 정보를 List형태로 반환
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpSearchQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_search_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpOutComplete 
	 * Method 설명 : 출하확정 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpOutComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_out_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpMappingComplete 
	 * Method 설명 : 매핑완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpMappingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_mapping_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpMappingDeleteComplete 
	 * Method 설명 : 매핑취소 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpMappingDeleteComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif020.sp_mapping_delete_complete", model);
		return model;
	}

}
