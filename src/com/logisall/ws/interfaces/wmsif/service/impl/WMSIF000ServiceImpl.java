package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;

import org.json.JSONObject;


@Service("WMSIF000Service")
public class WMSIF000ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF000Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF000Dao")
	private WMSIF000Dao dao;

	@Autowired
	WMSIF000ServiceImpl(WMSIF000Dao dao) {
		super(dao);
	}
	/*-
	 * Method ID	: crossDomainHttpWsMobile 
	 * Method 설명	: 웹메소드 웹서비스 전송모듈
	 * 작성자			: W
	 */
	@Override
	public Map<String, Object> crossDomainHttpWsMobile(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String pOrdId 	= (String)model.get("pOrdId");
        m.put("ORDID"	, pOrdId);
        
        String apiUrl 	= "https://52.79.206.98:5521/restv2/" + cUrl;
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("sUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "manage";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = "{\"ordId\":\""+pOrdId+"\"}";
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 대체 Method ID		: disableSslVerification
     * 대체 Method 설명		: SSL 예외처리
     * 작성자				: W
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	@Override
	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void checkParamMapValidation(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Map<String, Object> crossDomainKakaoSms(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        String apiUrl 	= "https://52.79.206.98:5101/restv2/WINUS.SMS:SMS_RS/POOLATHOME/KAKAO_SMS";
        URL url = null;
        HttpsURLConnection con = null; 
        try{
        	//ssl disable
        	disableSslVerification();
	        
	         
	        url = new URL(apiUrl);
	        
	        
	        con = (HttpsURLConnection) url.openConnection();
	        
        	//웹페이지 로그인 권한 적용
        	String userpass		= "winus01" + ":" + "winus01";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = (String)model.get("data");
			
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            System.out.println(response.toString());
	            m.put("RESULT"	, response.toString());
	        }
	        
        	
        } catch(Exception e){
        	e.printStackTrace();
            throw e;
        } finally {
        	con.disconnect();
        }
        return m;
	}
	
	@Override
	public Map<String, Object> crossDomainKakaoSms2(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		String apiUrl 	= "https://52.79.206.98:5101/restv2/WINUS.SMS:SMS_RS/POOLATHOME/KAKAO_SMS_DLV_REQ";
		URL url = null;
		HttpsURLConnection con = null; 
		try{
			//ssl disable
			disableSslVerification();
			
			
			url = new URL(apiUrl);
			
			
			con = (HttpsURLConnection) url.openConnection();
			
			//웹페이지 로그인 권한 적용
			String userpass		= "winus01" + ":" + "winus01";
			String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
			
			con.setRequestProperty("Authorization", basicAuth);
			con.setDoInput(true);
			con.setDoOutput(true);  
			con.setRequestMethod("POST");
			con.setConnectTimeout(0);
			con.setReadTimeout(0);
			con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = (String)model.get("data");
			
			
			//JSON 보내는 Output stream
			try(OutputStream os = con.getOutputStream()){
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}
			
			//Response data 받는 부분
			try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				
				//JSONObject jsonData = new JSONObject(response.toString());
				System.out.println(response.toString());
				m.put("RESULT"	, response.toString());
			}
			
			
		} catch(Exception e){
			e.printStackTrace();
			throw e;
		} finally {
			con.disconnect();
		}
		return m;
	}
	
	@Override
	public String crossDomainSabangOrderHead(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		
		String apiUrl 	= "https://52.79.206.98:5101/restv2/WINUS.SABANGNET:SABANGNET_RR/SABANGNET/ORDER_HEAD";
		apiUrl += "?orderDate=" + (String)model.get("orderDate");
		URL url = null;
		HttpsURLConnection con = null; 
		String resp;
		try{
			//ssl disable
			disableSslVerification();
			
			
			url = new URL(apiUrl);
			
			
			con = (HttpsURLConnection) url.openConnection();
			
			//웹페이지 로그인 권한 적용
			String userpass		= "winus01" + ":" + "winus01";
			String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
			
			con.setRequestProperty("Authorization", basicAuth);
			con.setDoInput(true);
			//con.setDoOutput(true);  
			con.setRequestMethod("GET");
			con.setConnectTimeout(0);
			con.setReadTimeout(0);
			con.setRequestProperty("ContentType", "application/x-www-form-urlencoded");

			//con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "text/xml");
			
			JSONObject param  = new JSONObject();
			/*
			JSONObject sabangOrderList  = new JSONObject();
			param.put("SABANG_ORDER_LIST", sabangOrderList);

			JSONObject header  = new JSONObject();
			sabangOrderList.put("HEADER", header);
			
			String sendCompanyId  = "muriapu";
			header.put("SEND_COMPANY_ID", sendCompanyId);
			String sendAuthKey  = "VPuH8AFCMdR7Ed0889ES4BTWX2rGAyM";
			header.put("SEND_AUTH_KEY", sendAuthKey);
			String sendDate  = "20210629";
			header.put("SEND_DATE", sendDate);
			
			
			JSONObject data  = new JSONObject();
			sabangOrderList.put("DATA", data);
			
			String ordStDate  = "20210629";
			data.put("ORD_ST_DATE", ordStDate);
			String ordEdDate  = "20210629";
			data.put("ORD_ED_DATE", ordEdDate);
			String ordField  = "<![CDATA[" + "ORDER_ID|MALL_USER_ID|ORDER_STATUS|USER_NAME|RECEIVE_NAME" + "]]>";
			data.put("ORD_FIELD", ordField);
			*/
			
			
			
			//Json Data
			String jsonInputString = param.toString();
			
			
			//JSON 보내는 Output stream
			/*
			try(OutputStream os = con.getOutputStream()){
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}
			*/
			
			//Response data 받는 부분
			try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				
				//JSONObject jsonData = new JSONObject(response.toString());
				String filter_word = "\\\"";
				resp = response.toString().replaceAll(filter_word, "'");
				System.out.println(resp);
				//ObjectMapper obj = new ObjectMapper();
				//result = obj.readValue(resp, SabangnetOrderHeaderVO.class);
				
			}
			
			
		} catch(Exception e){
			e.printStackTrace();
			throw e;
		} finally {
			con.disconnect();
		}
		return resp;
	}
	
	
	/*-
	 * Method ID	: crossDomainHttpWsKcc
	 * Method 설명	: 웹메소드 웹서비스 전송모듈 (kcc) _ POST
	 * 작성자			: yhku
	 */
	@Override
	public Map<String, Object> crossDomainHttpWsKcc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        
        //String apiUrl 	= "http://52.79.206.98:5555/restv2/" + cUrl;
        //String apiUrl 	= "http://52.79.206.98:5555/KCCHCC_WMS/" + cUrl; //개발 
        String apiUrl 	= "http://52.79.206.98:5200/KCCHCC_WMS/" + cUrl; //실운영
        
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("apiUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        /*HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();*/
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			//String jsonInputString = "{\"input\":\""+data+"\"}";
			String jsonInputString = data;
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	       
	        
	        
	        //Response data 받는 부분
	        int responceCode = con.getResponseCode();
	        if (responceCode == HttpURLConnection.HTTP_OK) {
	       
	    	   try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
		            StringBuilder response = new StringBuilder();
		            String responseLine = null;
		            while ((responseLine = br.readLine()) != null) {
		                response.append(responseLine.trim());
		            }
		            
		            //JSONObject jsonData = new JSONObject(response.toString());
		            System.out.println("jsonOutputString : "+response.toString());
		            m.put("RESULT"	, response.toString());
		        }
	       }
	       con.disconnect();
	       
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	

	/*-
	 * Method ID	: crossDomainHttpWs5200
	 * Method 설명	: 웹메소드 웹서비스 전송모듈  _ POST
	 * 작성자			: yhku
	 */
	@Override
	public Map<String, Object> crossDomainHttpWs5200(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        String hostUrl		= (String)model.get("hostUrl");
        String apiUrl 	= "";
        
        if(hostUrl != null && hostUrl.equals("localhost")){
        	 apiUrl 	= "http://172.31.10.253:5200/" + cUrl; //실운영(local에서 접근)
        }else{
        	 apiUrl 	= "http://52.79.206.98:5200/" + cUrl; //실운영
        }
        
        try{
        	//ssl disable
        	disableSslVerification();
//	        System.out.println("apiUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        /*HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();*/
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			//String jsonInputString = "{\"input\":\""+data+"\"}";
			String jsonInputString = data;
//			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        int responceCode = con.getResponseCode();
	        if (responceCode == HttpURLConnection.HTTP_OK) {
	       
	    	   try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
		            StringBuilder response = new StringBuilder();
		            String responseLine = null;
		            while ((responseLine = br.readLine()) != null) {
		                response.append(responseLine.trim());
		            }
		            
		            //System.out.println("jsonOutputString : "+response.toString());
		            m.put("RESULT"	, response.toString());
		            
		            JSONObject jsonData = new JSONObject(response.toString());
		            //JSONObject docResponse = new JSONObject(jsonData.get("RESULT").toString());
		            Iterator iterator = jsonData.keys();
		            while(iterator.hasNext()){
		                String key = (String)iterator.next();
		                //System.out.println("iterator .. :  "+key);
		                if(key.equals("RTNCD"))  m.put("header"	, jsonData.get("RTNCD").toString());// 성공 : 0, 실패 :-1 
		                if(key.equals("RTNMSG")) m.put("message", jsonData.get("RTNMSG").toString());//  
		                //------ 추가 ------ 
		                if(key.equals("RTNERRCNT")) m.put("errcnt", jsonData.get("RTNERRCNT").toString());//리턴실패수
		                if(key.equals("RTNWORKTYPE")) m.put("worktype", jsonData.get("RTNWORKTYPE").toString());//리턴작업구분 
		                if(key.equals("RTNNEWINSTCNT")) m.put("workinstcnt", jsonData.get("RTNNEWINSTCNT").toString());//추가등록건 
		            }
		        }
	       }
	       con.disconnect();
	       
        } catch(Exception e){
            throw e;
        }
        return m;
	}

	/*-
	 * Method ID	: crossDomainHttpWs5200New : 신규 EAI 서버로 연결 23.07.12
	 * Method 설명	: 웹메소드 웹서비스 전송모듈  _ POST
	 * 작성자		
	 */
	@Override
	public Map<String, Object> crossDomainHttpWs5200New(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        String hostUrl		= (String)model.get("hostUrl");
        String apiUrl 	= "";
        
        if(hostUrl != null && hostUrl.equals("localhost")){
        //	 apiUrl 	= "http://172.31.10.253:5200/" + cUrl; //실운영(local에서 접근) - OLD
        	 apiUrl 	= "http://10.30.1.14:5200/" + cUrl; //실운영(local에서 접근) 내부망 사용 
        }else{
        //	 apiUrl 	= "http://52.79.206.98:5200/" + cUrl; //실운영 - OLD
        	 apiUrl 	= "http://10.30.1.14:5200/" + cUrl; //실운영 내부망 사용 
        }
        
        try{
        	//ssl disable
        	disableSslVerification();
//	        System.out.println("apiUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        /*HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();*/
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
        	con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			//String jsonInputString = "{\"input\":\""+data+"\"}";
			String jsonInputString = data;
//			System.out.println("param jsonInputString : " + jsonInputString);
	
		String[] requestBodyMethods = {
			HttpMethod.POST.name(), 
			HttpMethod.PUT.name(), 
			HttpMethod.PATCH.name()
			};
		if (Arrays.asList(requestBodyMethods).contains(cMethod)) {
		    try(OutputStream os = con.getOutputStream()){
			byte[] input = jsonInputString.getBytes("utf-8");
			os.write(input, 0, input.length);
		    }    
		}
	        
	        //Response data 받는 부분
	        int responceCode = con.getResponseCode();
	        if (responceCode == HttpURLConnection.HTTP_OK) {
	       
	    	   try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
		            StringBuilder response = new StringBuilder();
		            String responseLine = null;
		            while ((responseLine = br.readLine()) != null) {
		                response.append(responseLine.trim());
		            }
		            
		            //System.out.println("jsonOutputString : "+response.toString());
		            m.put("RESULT"	, response.toString());
		            
		            JSONObject jsonData = new JSONObject(response.toString());
		            //JSONObject docResponse = new JSONObject(jsonData.get("RESULT").toString());
		            Iterator iterator = jsonData.keys();
		            while(iterator.hasNext()){
		                String key = (String)iterator.next();
		                //System.out.println("iterator .. :  "+key);
		                if(key.equals("RTNCD"))  m.put("header"	, jsonData.get("RTNCD").toString());// 성공 : 0, 실패 :-1 
		                if(key.equals("RTNMSG")) m.put("message", jsonData.get("RTNMSG").toString());//  
		                //------ 추가 ------ 
		                if(key.equals("RTNERRCNT")) m.put("errcnt", jsonData.get("RTNERRCNT").toString());//리턴실패수
		                if(key.equals("RTNWORKTYPE")) m.put("worktype", jsonData.get("RTNWORKTYPE").toString());//리턴작업구분 
		                if(key.equals("RTNNEWINSTCNT")) m.put("workinstcnt", jsonData.get("RTNNEWINSTCNT").toString());//추가등록건 
		            }
		        }
	       }
	       con.disconnect();
	       
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/*-
	 * Method ID	: crossDomainHttpWs5200DH
	 * Method 설명	: DH 웹메소드 웹서비스 전송모듈  _ POST
	 * 작성자			: kimzero
	 */
	@Override
	public Map<String, Object> crossDomainHttpWs5200DH(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        String hostUrl		= (String)model.get("hostUrl");
        String apiUrl 	= "";
        
        if(hostUrl != null && (hostUrl.equals("localhost") || hostUrl.equals("127.0.0.1"))){
        	 apiUrl 	= "http://10.30.1.14:5200/" + cUrl; //실운영(local에서 접근)
        }else{
        	 apiUrl 	= "http://10.30.1.14:5200/" + cUrl; //실운영
        }
        
        try{
        	//ssl disable
        	disableSslVerification();
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = data;
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	       
	        //Response data 받는 부분
	        int responceCode = con.getResponseCode();
	        if (responceCode == HttpURLConnection.HTTP_OK) {
	       
	    	   try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
		            StringBuilder response = new StringBuilder();
		            String responseLine = null;
		            while ((responseLine = br.readLine()) != null) {
		                response.append(responseLine.trim());
		            }
		            
		            m.put("RESULT"	, response.toString());
		        }
	       }
	       con.disconnect();
	       
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	
	/*-
	 * Method ID	: crossDomainHttpWs5200
	 * Method 설명	: 웹메소드 웹서비스 전송모듈  _ POST
	 * 작성자			: ykim
	 */
	public Map<String, Object> crossDomainHttpWs5200_UCPS(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        String hostUrl		= (String)model.get("hostUrl");
        String apiUrl 	= "";
        
        if(hostUrl != null && hostUrl.equals("localhost")){
        	 apiUrl 	= "http://172.31.10.253:5200/" + cUrl; //실운영(local에서 접근)
        }else{
        	 apiUrl 	= "http://52.79.206.98:5200/" + cUrl; //실운영
        }
        
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("apiUrl : " + apiUrl);
	        
	        URL url = new URL(apiUrl);
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			//String jsonInputString = "{\"input\":\""+data+"\"}";
			String jsonInputString = data;
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	       
	        
	        
	        //Response data 받는 부분
	        int responceCode = con.getResponseCode();
	        if (responceCode == HttpURLConnection.HTTP_OK) {
	       
	    	   try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
		            StringBuilder response = new StringBuilder();
		            String responseLine = null;
		            while ((responseLine = br.readLine()) != null) {
		                response.append(responseLine.trim());
		            }
		            
		            //JSONObject jsonData = new JSONObject(response.toString());
		            System.out.println("jsonOutputString : "+response.toString());
		            m.put("RESULT"	, response.toString());
		        }
	       }
	       con.disconnect();
	       
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	   
	/*-
	 * Method ID	: crossDomainHttpWsMobile 
	 * Method 설명	: 웹메소드 웹서비스 전송모듈
	 * 작성자			: W
	 */
	@Override
	public Map<String, Object> crossDomainHttpWs5520(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        
        String apiUrl 	= "https://52.79.206.98:5521/restv2/" + cUrl;
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("sUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "manage";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = data;
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	/*-
	 * Method ID	: ifProductivityList 
	 * Method 설명	: 새로피엔엘 생산성 관리 모바일 조회
	 * 작성자			: schan
	 */
	@Override
	public Map<String, Object> ifProductivityList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("LIST", dao.ifProductivityList(model));

		}  catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
		return map;
	}
	/*-
	 * Method ID	: ifProductivityListMonth 
	 * Method 설명	: 새로피엔엘 생산성 관리 모바일 조회(월간)
	 * 작성자			: schan
	 */
	@Override
	public Map<String, Object> ifProductivityListMonth(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("LIST", dao.ifProductivityListMonth(model));

		}  catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
		return map;
	}
	/*-
	 * Method ID	: ifProductivityUserNm 
	 * Method 설명	: 새로피엔엘 생산성입력/조회 모바일 사용자 이름 검색 
	 * 작성자			: schan
	 */
	@Override
	public Map<String, Object> ifProductivityUserNm(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
	        model.put("pageIndex", "1");
	        model.put("pageSize", "1");
			map.put("LIST", dao.ifProductivityUserNm(model));

		}  catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
		return map;
	}
	/*-
	 * Method ID	: ifProductivityListUpdate 
	 * Method 설명	: 새로피엔엘 생산성입력/조회 모바일 사용자 이름 검색 
	 * 작성자			: schan
	 */
	@Override
	public Map<String, Object> ifProductivityListUpdate(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try{
			String flag = (String)model.get("init_edit_flag"); 
			if(flag.equals("A")){
				dao.ifProductivityListInsert(model);
			}
			// update
			else if(flag.equals("E")){
				dao.ifProductivityListUpdate(model);
			}
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("save.success"));
		} catch(Exception e){
			m.put("errCnt", -1);
			m.put("MSG", e.getMessage());
			throw e;
		}
		return m;
	}
	
	/*-
	 * Method ID		: crossDomainHttpWsOlive 
	 * Method 설명		: 웹메소드 -> 올리브영 특송사 변환
	 * 작성자			: KSJ
	 */
	@Override
	public Map<String, Object> crossDomainHttpWsOlive(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod		= (String)model.get("cMethod");
        String cUrl			= (String)model.get("cUrl");
        String pOrdId 		= (String)model.get("pOrdId");
        String pOrgOrdId 	= (String)model.get("pOrgOrdId");
        String pUpdNo 		= (String)model.get("pUpdNo");
        String pShippingCompany 	= (String)model.get("pShippingCompany");
        
        m.put("ORDID"				, pOrdId);
        m.put("ORGORDID"			, pOrgOrdId);
        m.put("SHIPPING_COMPANY"	, pShippingCompany);
        m.put("UPD_NO"	, pUpdNo);
        
        String apiUrl 	= "https://52.79.206.98:5521/restv2/" + cUrl;
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("sUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "manage";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = "{\"Request\":{\"ordId\":\""+pOrdId+"\", \"shippingCompany\":\""+ pShippingCompany + "\", \"orgOrdId\":\""+ pOrgOrdId + "\", \"updNo\":\""+pUpdNo+"\"}}";
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/*-
	 * Method ID		: crossDomainHttpWs5200WCS 
	 * Method 설명		: 웹메소드 웹서비스 전송모듈 5200 포트 WCS
	 * 작성자			: W
	 */
	@Override
	public Map<String, Object> crossDomainHttpWs5200WCS(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        
        String apiUrl 	= "http://10.30.1.14:5200/" + cUrl;
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("sUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = data;
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	
	
	/*-
	 * Method ID		: crossDomainHttpWs5200WCSNew 
	 * Method 설명		: 웹메소드 웹서비스 전송모듈 5200 포트 WCS
	 * 작성자			: W
	 */
	@Override
	public Map<String, Object> crossDomainHttpWs5200WCSNew(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String data		= (String)model.get("data");
        
        //String apiUrl 	= "https://52.79.206.98:5201/" + cUrl;
        String apiUrl 	= "https://10.30.1.14:5201/" + cUrl;
        try{
        	//TLS 버전 설정
        	System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
        	
        	//ssl disable
        	disableSslVerification();
	        System.out.println("sUrl : " + apiUrl);
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = data;
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/*-
     * Method ID    : crossDomainHttpWs5100New : 신규 EAI 서버로 연결 23.07.12
     * Method 설명    : 웹메소드 웹서비스 전송모듈  _ POST
     * 작성자      
     */
    @Override
    public Map<String, Object> crossDomainHttpWs5100New(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod  = (String)model.get("cMethod");
        String cUrl     = (String)model.get("cUrl");
        String data     = (String)model.get("data");
        String hostUrl      = (String)model.get("hostUrl");
        String apiUrl   = "";
        
        if(hostUrl != null && hostUrl.equals("localhost")){
        //   apiUrl     = "http://172.31.10.253:5100/" + cUrl; //실운영(local에서 접근) - OLD
             apiUrl     = "http://10.30.1.14:5100/" + cUrl; //실운영(local에서 접근) 내부망 사용 
        }else{
        //   apiUrl     = "http://52.79.206.98:5100/" + cUrl; //실운영 - OLD
             apiUrl     = "http://10.30.1.14:5100/" + cUrl; //실운영 내부망 사용 
        }
        
        try{
            //ssl disable
            disableSslVerification();
//          System.out.println("apiUrl : " + apiUrl);
            
            URL url = null; 
            url = new URL(apiUrl);
            /*HttpsURLConnection con = null;
            con = (HttpsURLConnection) url.openConnection();*/
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            
            //웹페이지 로그인 권한 적용
            String userpass     = "Administrator" + ":" + "ulndkagh1!";
            String basicAuth    = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

            con.setRequestProperty("Authorization", basicAuth);
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod(cMethod);
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Content-Type"   , "application/json");
            con.setRequestProperty("Accept"         , "application/json");
            
            //Json Data
            //String jsonInputString = "{\"input\":\""+data+"\"}";
            String jsonInputString = data;
//          System.out.println("param jsonInputString : " + jsonInputString);
            
            //JSON 보내는 Output stream
            try(OutputStream os = con.getOutputStream()){
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            
            //Response data 받는 부분
            int responceCode = con.getResponseCode();
            if (responceCode == HttpURLConnection.HTTP_OK) {
           
               try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    
                    //System.out.println("jsonOutputString : "+response.toString());
                    m.put("RESULT"  , response.toString());
                    
                    JSONObject jsonData = new JSONObject(response.toString());
                    //JSONObject docResponse = new JSONObject(jsonData.get("RESULT").toString());
                    Iterator iterator = jsonData.keys();
                    while(iterator.hasNext()){
                        String key = (String)iterator.next();
                        //System.out.println("iterator .. :  "+key);
                        if(key.equals("RTNCD"))  m.put("header" , jsonData.get("RTNCD").toString());// 성공 : 0, 실패 :-1 
                        if(key.equals("RTNMSG")) m.put("message", jsonData.get("RTNMSG").toString());//  
                        //------ 추가 ------ 
                        if(key.equals("RTNERRCNT")) m.put("errcnt", jsonData.get("RTNERRCNT").toString());//리턴실패수
                        if(key.equals("RTNWORKTYPE")) m.put("worktype", jsonData.get("RTNWORKTYPE").toString());//리턴작업구분 
                        if(key.equals("RTNNEWINSTCNT")) m.put("workinstcnt", jsonData.get("RTNNEWINSTCNT").toString());//추가등록건 
                    }
                }
           }
           con.disconnect();
           
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
     * Method ID    : crossDomainHttpWs5400New 
     * Method 설명    : 웹메소드 웹서비스 전송모듈
     * 작성자          : YSI
     */
    @Override
    public Map<String, Object> crossDomainHttpWs5400New(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod  = (String)model.get("cMethod");
        String cUrl     = (String)model.get("cUrl");
        String pOrdId   = (String)model.get("pOrdId");
        m.put("ORDID"   , pOrdId);
        
        String apiUrl   = "http://10.30.1.14:5400/" + cUrl;
        try{
            //ssl disable
            disableSslVerification();
            System.out.println("sUrl : " + apiUrl);
            
            URL url = null; 
            url = new URL(apiUrl);
            
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            
            //웹페이지 로그인 권한 적용
            String userpass     = "Administrator" + ":" + "ulndkagh4$";
            String basicAuth    = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

            con.setRequestProperty("Authorization", basicAuth);
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod(cMethod);
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Content-Type"   , "application/json");
            con.setRequestProperty("Accept"         , "application/json");
            
            //Json Data
            String jsonInputString = "{\"ordId\":\""+pOrdId+"\"}";
            System.out.println("param jsonInputString : " + jsonInputString);
            
            //JSON 보내는 Output stream
            try(OutputStream os = con.getOutputStream()){
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            
            //Response data 받는 부분
            try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
                
                //JSONObject jsonData = new JSONObject(response.toString());
                m.put("RST" , response.toString());
            }
            
            con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
     * Method ID    : crossDomainHttpWs5400New 
     * Method 설명    : 웹메소드 웹서비스 전송모듈
     * 작성자          : YSI
     */
    @Override
    public Map<String, Object> crossDomainHttpWcs5400New(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod  	= (String)model.get("cMethod");
        String cUrl     	= (String)model.get("cUrl");
        String pOrdId   	= (String)model.get("pOrdId");
        String pOrdType   	= (String)model.get("pOrdType");
        String pLocType     = (String)model.get("pLocType");
        String pPltType     = (String)model.get("pPltType");
        String pLotNo 		= (String)model.get("pLotNo");
        String pLocation 	= (String)model.get("pLocation");
        
        
        m.put("ord_id"   , pOrdId);
        m.put("ord_type"   , pOrdType);
        m.put("plt_type"      , pPltType);
        m.put("loc_type"      , pLocType);
        m.put("lot_no"      , pLotNo);
        m.put("acspos"      , pLocation);
        
        String apiUrl   = "http://10.30.1.14:5400/" + cUrl;
        try{
            //ssl disable
            disableSslVerification();
            System.out.println("sUrl : " + apiUrl);
            
            URL url = null; 
            url = new URL(apiUrl);
            
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            
            //웹페이지 로그인 권한 적용
            String userpass     = "Administrator" + ":" + "ulndkagh4$";
            String basicAuth    = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

            con.setRequestProperty("Authorization", basicAuth);
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod(cMethod);
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Content-Type"   , "application/json");
            con.setRequestProperty("Accept"         , "application/json");
            
            String jsonInputString = "{\"ord_id\":\"" + pOrdId + "\", " +
                    "\"ord_type\":\"" + pOrdType + "\", " +
                    "\"plt_type\":\"" + pPltType + "\", " +
                    "\"loc_type\":\"" + pLocType + "\", " +
                    "\"lot_no\":\"" + pLotNo + "\", " +
                    "\"acspos\":\"" + pLocation + "\"}";  // pLocation 추가
            //Json Data
           // String jsonInputString = "{\"ord_id\":\"" + pOrdId + "\", \"ord_type\":\"" + pOrdType + "\", \"plt_type\":\"" + pPltType + "\", \"loc_type\":\"" + pLocType + "\"}";
            System.out.println("param jsonInputString : " + jsonInputString);
            
            //JSON 보내는 Output stream
            try(OutputStream os = con.getOutputStream()){
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            
            //Response data 받는 부분
            try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
                
                //JSONObject jsonData = new JSONObject(response.toString());
                m.put("RST" , response.toString());
            }
            
            con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
     * Method ID    : crossDomainHttpWs5400New 
     * Method 설명    : 웹메소드 웹서비스 전송모듈
     * 작성자          : YSI
     */
    @Override
    public Map<String, Object> crossDomainHttpWs5400New2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod  = (String)model.get("cMethod");
        String cUrl     = (String)model.get("cUrl");
        String data		= (String)model.get("data");
        
        String apiUrl   = "http://10.30.1.14:5400/" + cUrl;
        try{
            //ssl disable
            disableSslVerification();
            System.out.println("sUrl : " + apiUrl);
            
            URL url = null; 
            url = new URL(apiUrl);
            
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            
            //웹페이지 로그인 권한 적용
            String userpass     = "Administrator" + ":" + "ulndkagh4$";
            String basicAuth    = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

            con.setRequestProperty("Authorization", basicAuth);
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod(cMethod);
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Content-Type"   , "application/json");
            con.setRequestProperty("Accept"         , "application/json");
            
          //Json Data
			String jsonInputString = data;
			System.out.println("param jsonInputString : " + jsonInputString);
            
            //JSON 보내는 Output stream
            try(OutputStream os = con.getOutputStream()){
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            
            //Response data 받는 부분
            try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
                
                //JSONObject jsonData = new JSONObject(response.toString());
                m.put("RST" , response.toString());
            }
            
            con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
     * Method ID    : crossDomainHttpWs5400New 
     * Method 설명    : 웹메소드 웹서비스 전송모듈
     * 작성자          : YSI
     */
    @Override
    public Map<String, Object> crossDomainHttpWs5100NewPOP(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod  = (String)model.get("cMethod");
        String cUrl     = (String)model.get("cUrl");
        String InReqDt  = (String)model.get("InReqDt");
        String ItemCd   = (String)model.get("ItemCd");
        String OrdQty   = (String)model.get("OrdQty");
        String DlvyCd   = (String)model.get("DlvyCd");
        String ItemNm   = (String)model.get("ItemNm");
        
        
        m.put("stkinYmd"   , InReqDt);
        m.put("gdsCd"   , ItemCd);
        m.put("stkinQty"   , OrdQty);
        m.put("poDlvTypNm"   , DlvyCd);
        m.put("gdsNm"   , ItemNm);

        String apiUrl   = "http://10.30.1.14:5100/" + cUrl;
        try{
            //ssl disable
            disableSslVerification();
            System.out.println("sUrl : " + apiUrl);
            
            URL url = null; 
            url = new URL(apiUrl);
            
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            
            //웹페이지 로그인 권한 적용
            String userpass     = "Administrator" + ":" + "ulndkagh1!";
            String basicAuth    = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

            con.setRequestProperty("Authorization", basicAuth);
            con.setDoInput(true);
            con.setDoOutput(true);  
            con.setRequestMethod(cMethod);
            con.setConnectTimeout(0);
            con.setReadTimeout(0);
            con.setRequestProperty("Content-Type"   , "application/json");
            con.setRequestProperty("Accept"         , "application/json");
            
            //Json Data
            String jsonInputString = "{"
            	    + "\"order\":["
            	    + "   {"
            	    + "      \"poDlvTypNm\":\"" + DlvyCd + "\","
            	    + "      \"stkinYmd\":\"" + InReqDt + "\","
            	    + "      \"stkinQty\":" + OrdQty + ","
            	    + "      \"gdsCd\":\"" + ItemCd + "\","
            	    + "      \"gdsNm\":\"" + ItemNm + "\""
            	    + "   }"
            	    + "]"
            	    + "}";
            System.out.println("param jsonInputString : " + jsonInputString);
            
            //JSON 보내는 Output stream
            try(OutputStream os = con.getOutputStream()){
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            
            //Response data 받는 부분
            try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
                
                //JSONObject jsonData = new JSONObject(response.toString());
                m.put("RST" , response.toString());
            }
            
            con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}