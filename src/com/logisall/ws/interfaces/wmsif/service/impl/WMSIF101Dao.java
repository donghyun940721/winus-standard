package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF101Dao")
public class WMSIF101Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpIfHowereDlvData
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpIfHowereDlvData(Map<String, Object> model) {
		executeUpdate("pk_wmsif101.sp_if_howser_dlv_complete", model);
		return model;
	}
	
	/*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsys300e7.selectExistData", model);
    }
    
    /**
     * Method ID : wmssp010t2Tmsys900Insert
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object t2FileInsert(Map<String, Object> model) {
        return executeInsert("wmssp010t2.t2FileInsert", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmssp010.itemImgInfoSave", model);
    }
    
	/*-
	 * Method ID : runSpSweettrackerDlvQry
	 * Method 설명 : 스윗트렉커 택배 정보조회 인터페이스
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSweettrackerDlvTraceQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif101.sp_sweettracker_dlv_trace_qry", model);
		return model;
	}
    
	/*-
	 * Method ID : runSpTwelvecmGoodsinfoQry
	 * Method 설명 : 12CM 트레킹 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTwelvecmGoodsinfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif101.sp_twelvecm_goods_info_qry", model);
		return model;
	}
}
