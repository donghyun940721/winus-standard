package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF000Service extends AbstractInterfaceService {
	public Map<String, Object> crossDomainHttpWsMobile(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainKakaoSms(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainKakaoSms2(Map<String, Object> model) throws Exception;
	public String crossDomainSabangOrderHead(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWsKcc(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs5200(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs5200New(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs5200WCS(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs5200WCSNew(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs5200DH(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs5200_UCPS(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWsOlive(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> crossDomainHttpWs5520(Map<String, Object> model) throws Exception;
	public Map<String, Object> ifProductivityList(Map<String, Object> model) throws Exception;
	public Map<String, Object> ifProductivityListMonth(Map<String, Object> model) throws Exception;
	public Map<String, Object> ifProductivityUserNm(Map<String, Object> model) throws Exception;
	public Map<String, Object> ifProductivityListUpdate(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttpWs5100New(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttpWs5400New(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttpWcs5400New(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttpWs5400New2(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> crossDomainHttpWs5100NewPOP(Map<String, Object> model) throws Exception;

}