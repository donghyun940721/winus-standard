package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF103Service extends AbstractInterfaceService {

	public Map<String, Object> get_stock_list(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> if_transfer_stockid_complete(Map<String, Object> model) throws Exception;
}
