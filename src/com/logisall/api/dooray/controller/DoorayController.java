package com.logisall.api.dooray.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.logisall.api.controller.BaseController;
import com.logisall.api.dooray.dto.CreatePostRequest;
import com.logisall.api.dooray.service.DoorayService;

@Controller
@RequestMapping("/dooray")
public class DoorayController extends BaseController {
	
	private final DoorayService doorayService;

	@Autowired
	public DoorayController(DoorayService doorayService) {
		this.doorayService = doorayService;
	}
	
	@RequestMapping(value = "/posts/create.do", method = RequestMethod.POST)
	public ResponseEntity<String> createAsk(@RequestBody CreatePostRequest requestDto) throws Exception {
		
		try {
			
			return doorayService.createAsk(requestDto);
		} catch(Exception e) {
			String errorMessage = "Error occurred: " + e.getMessage();
			 return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);  // HTTP 500, 에러 메시지 문자열
        }

	}
	
	

}
