package com.logisall.api.dooray.service.Impl;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.logisall.api.dooray.dto.CreatePostDoorayRequest;
import com.logisall.api.dooray.dto.CreatePostRequest;
import com.logisall.api.dooray.service.DoorayService;

@Service
public class DoorayServiceImpl implements DoorayService {
	
	private final String BASE_URL = "https://api.dooray.com";
	private final String PROJECT_ID = "3942074625711924665";
	private String authToken = "dooray-api rjkdvtm7uazr:WYnQEDUjSt-cTkTZFHhj2w";
	
	@Override
	public ResponseEntity<String> createAsk(CreatePostRequest requestDto) throws Exception {
		
		// 헤더에 Authorization 추가 > 이건 직접 넣지말고 프로퍼티로 관리해야 함
		String url = BASE_URL + "/project/v1/projects/" + PROJECT_ID + "/posts";
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authToken);
		headers.set("Content-Type", "application/json");
		
		String subject = requestDto.getSubject();
		String bodyContent =requestDto.getContent();
		CreatePostDoorayRequest doorayrequestDto = new CreatePostDoorayRequest(subject, bodyContent);
		
		
		HttpEntity<CreatePostDoorayRequest> httpEntity = new HttpEntity<>(doorayrequestDto, headers);
		
		try {
		    ResponseEntity<String> response = restTemplate.exchange(
		    		url, 
		            HttpMethod.POST, 
		            httpEntity, 
		            String.class
		    );
		    
		    return response;
		} catch (Exception e) {
		    throw new RuntimeException("Dooray API 호출 중 오류 발생", e);
		}		
	}
	
	

}
