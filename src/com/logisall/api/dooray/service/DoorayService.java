package com.logisall.api.dooray.service;


import org.springframework.http.ResponseEntity;

import com.logisall.api.dooray.dto.CreatePostRequest;

public interface DoorayService {
	
	ResponseEntity<String> createAsk(CreatePostRequest requestDto) throws Exception;

}
