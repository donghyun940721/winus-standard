package com.logisall.api.dooray.dto;


import java.util.List;
import java.util.ArrayList;

public class CreatePostDoorayRequest {

    private String parentPostId;
    private Users users;
    private String subject;
    private Body body;
    private String dueDate;
    private Boolean dueDateFlag;
    private String milestoneId;
    private List<String> tagIds;
    private String priority;

    public CreatePostDoorayRequest() {
        this.parentPostId = "";
        this.users = new Users();
        this.body = new Body("text/html", "");
        this.dueDate = "";
        this.dueDateFlag = true;
        this.milestoneId = "";
        this.tagIds = new ArrayList<>();
        this.priority = "none";
    }

    public CreatePostDoorayRequest(String subject, String content) {
        this();
        this.subject = subject;
        this.body.setContent(content);
    }

    public String getParentPostId() {
        return parentPostId;
    }

    public void setParentPostId(String parentPostId) {
        this.parentPostId = parentPostId;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Boolean getDueDateFlag() {
        return dueDateFlag;
    }

    public void setDueDateFlag(Boolean dueDateFlag) {
        this.dueDateFlag = dueDateFlag;
    }

    public String getMilestoneId() {
        return milestoneId;
    }

    public void setMilestoneId(String milestoneId) {
        this.milestoneId = milestoneId;
    }

    public List<String> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<String> tagIds) {
        this.tagIds = tagIds;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public static class Users {
        private List<String> to;
        private List<String> cc;

        public List<String> getTo() {
            return to;
        }

        public void setTo(List<String> to) {
            this.to = to;
        }

        public List<String> getCc() {
            return cc;
        }

        public void setCc(List<String> cc) {
            this.cc = cc;
        }
    }

    public static class Body {
        private String mimeType;
        private String content;
        
        public Body(String mimeType, String content) {
			this.mimeType = mimeType;
			this.content = content;
		}

        public String getMimeType() {
            return mimeType;
        }

        public void setMimeType(String mimeType) {
            this.mimeType = mimeType;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

}
