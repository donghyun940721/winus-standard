package com.logisall.api.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.logisall.api.common.exception.SeedException;
import com.logisall.api.common.exception.UnAuthorizedException;
import com.logisall.api.common.exception.ValidationException;
import com.logisall.api.common.response.factory.ErrorResponseFactory;
import com.logisall.api.common.response.service.ErrorResponseService;
import com.logisall.winus.frm.exception.BizException;

public class BaseController {
	
	private final Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
    private ErrorResponseService errorResponseService;
	
	//권한인증 에러
	@ExceptionHandler(UnAuthorizedException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public @ResponseBody Map<String, Object> handleUnauthorizedException(HttpServletRequest request, UnAuthorizedException ex) {
		ErrorResponseFactory factory = errorResponseService.getFactory(request);
        return factory.createErrorResponse(
            HttpStatus.UNAUTHORIZED.value(), 
            ex.getMessage()
        );
	}
	
	//암복호화 에러
	@ExceptionHandler(SeedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody Map<String, Object> handleSeedException(HttpServletRequest request, SeedException ex) {
		ErrorResponseFactory factory = errorResponseService.getFactory(request);
        return factory.createErrorResponse(
            HttpStatus.BAD_REQUEST.value(), 
            ex.getMessage()
        );

	}
	
	/*
	 * http 통신에러
	 * */
	@ExceptionHandler(IOException.class)
	@ResponseStatus(HttpStatus.BAD_GATEWAY)
	public @ResponseBody Map<String, Object> handleAllIoExceptions(HttpServletRequest request, Exception ex) {
		log.error("An error occurred: ", ex);
		ErrorResponseFactory factory = errorResponseService.getFactory(request);
        return factory.createErrorResponse(
            HttpStatus.BAD_GATEWAY.value(), 
            ex.getMessage()
        );
	}
	
	//발리데이션 에러
	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody Map<String, Object> handleValidationException(HttpServletRequest request, ValidationException ex) {
		log.error("An error occurred: ", ex);
		Map<String, String> errorMap = ex.getErrors();
		Object errObj = errorMap == null ? ex.getMessage() : errorMap;
		ErrorResponseFactory factory = errorResponseService.getFactory(request);
        return factory.createErrorResponse(
            HttpStatus.BAD_REQUEST.value(), 
            "Check the request data",
            errObj
        );
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody Map<String, Object> handleHttpMessageNotReadable(HttpServletRequest request, HttpMessageNotReadableException ex) {
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, String> errors = new HashMap<String, String>();
        log.error("An error occurred: ", ex);
        // 원인 예외 가져오기
        Throwable cause = ex.getCause();
        if (cause instanceof JsonMappingException) {
            JsonMappingException jme = (JsonMappingException) cause;
            if (!jme.getPath().isEmpty()) {
                // 에러가 발생한 필드 경로 가져오기
                String fieldName = "";
                for (JsonMappingException.Reference reference : jme.getPath()) {
                    if (fieldName.length() > 0) {
                        fieldName += ".";
                    }
                    if (reference.getIndex() >= 0) {
                        fieldName += "[" + reference.getIndex() + "]";
                    } else {
                        fieldName += reference.getFieldName();
                    }
                }
                errors.put(fieldName, "잘못된 요청 형식입니다.");
            }
        }

        if (errors.isEmpty()) {
            errors.put("message", "잘못된 요청 형식입니다.");
        }

    	ErrorResponseFactory factory = errorResponseService.getFactory(request);
        return factory.createErrorResponse(
            HttpStatus.BAD_REQUEST.value(), 
            "잘못된 요청 형식입니다.",
            errors
        );
    }
	
	/*
	 * 비즈니스 로직 에러
	 * */
	@ExceptionHandler(BizException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody Map<String, Object> handleBizExceptions(HttpServletRequest request, Exception ex) {
		log.error("An error occurred: ", ex);
		ErrorResponseFactory factory = errorResponseService.getFactory(request);
        return factory.createErrorResponse(
            HttpStatus.INTERNAL_SERVER_ERROR.value(), 
            "내부 로직 오류.. 관리자에게 문의하세요."
        );
	}
	
	/*
	 * 내부 시스템에러
	 * */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody Map<String, Object> handleAllExceptions(HttpServletRequest request, Exception ex) {
		log.error("An error occurred: ", ex);
		ErrorResponseFactory factory = errorResponseService.getFactory(request);
        return factory.createErrorResponse(
            HttpStatus.INTERNAL_SERVER_ERROR.value(), 
            "시스템 오류.. 관리자에게 문의하세요."
        );
	}
	
}
