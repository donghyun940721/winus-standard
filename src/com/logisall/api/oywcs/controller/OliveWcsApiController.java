package com.logisall.api.oywcs.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logisall.api.controller.BaseController;
import com.logisall.api.oywcs.dto.OrdrListWrapper;
import com.logisall.api.oywcs.service.OliveWcsApiService;

@RestController
@RequestMapping("/api/oywcs")
public class OliveWcsApiController extends BaseController{

	@Autowired
	private OliveWcsApiService service;
    
	/*
	 * 특송사별 주문분류 전송
	 * */
	@RequestMapping("/orders/classifications.do")
	public ResponseEntity<Map<String, Object>> orderClassifications(Map<String, Object> model, @RequestBody Map<String, Object> reqBody,
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = service.sendOrderClassifications(reqBody);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/*
	 * 주문별 운송장 전송
	 * */
	@RequestMapping("/orders/shipments.do")
	public ResponseEntity<Map<String, Object>> orderShipments(Map<String, Object> model, @RequestBody OrdrListWrapper reqBody,
			HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> result = service.sendOrderShipments(reqBody);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	

	/*
	 * 주문작업 상태 변경 전송
	 * */
	@RequestMapping("/orders/status-change.do")
	public ResponseEntity<Map<String, Object>> orderStatusChange(Map<String, Object> model, @RequestBody Map<String, Object> reqBody,
			HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> result = service.sendOrderStatusChange(reqBody);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/*
	 * 변경된 운송장 정보 전송
	 * */
	@RequestMapping("/orders/shipment-change.do")
	public ResponseEntity<Map<String, Object>> orderShipmentChange(Map<String, Object> model, @RequestBody OrdrListWrapper reqBody,
			HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> result = service.orderShipmentChange(reqBody);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping("/orders/shipment-change-part.do")
	public ResponseEntity<Map<String, Object>> orderShipmentChangePart(Map<String, Object> model, @RequestBody OrdrListWrapper reqBody,
			HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> result = service.orderShipmentChangePart(reqBody);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/*
	 * WCS 작업 완료 수신
	 * */
	@RequestMapping("/orders/completes.do")
	public ResponseEntity<Map<String, Object>> orderCompletes(Map<String, Object> model, @RequestBody Map<String, Object> reqBody,
			HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> result = service.receiveOrderCompletes(reqBody);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/*
	 * WMS 출고확정 처리
	 * */
	@RequestMapping("/orders/saveOutOrderCompletes.do")
	public ResponseEntity<Map<String, Object>> saveOutOrderCompletes(Map<String, Object> model, @RequestBody Map<String, Object> reqBody,
			HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> result = service.saveOutOrderCompletes(reqBody);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
