package com.logisall.api.oywcs.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.api.oywcs.dto.ShipmentDto;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
@SuppressWarnings("unchecked")
public class OliveWcsApiDao extends SqlMapAbstractDAO {
	private final Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private SqlMapClient sqlMapClientWinusIf;
	

	public void insertParcelWcsOrdrClsf(Map<String, Object> param) {
		try {
			sqlMapClientWinusIf.insert("oywcs.insertParcelWcsOrdrClsf", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}
	
	public void insertBulkOrder(List<Map<String, Object>> outOrderList){
		int batchSize = 200;
		int updateCnt = 0;

		try {
			sqlMapClientWinusIf.startTransaction();
			sqlMapClientWinusIf.startBatch();

			for (int i = 0; i < outOrderList.size(); i++) {
				updateCnt++;
				processOrder(outOrderList.get(i));

				if (i > 0 && i % batchSize == 0) {
					executeBatchWithLogging(outOrderList, i - batchSize + 1, i + 1, "batch index " + (i / batchSize));
					sqlMapClientWinusIf.startBatch(); // Reset batch to avoid ORA-01000
				}
			}

			executeBatchWithLogging(outOrderList, (outOrderList.size() / batchSize) * batchSize, outOrderList.size(),
					"final batch");
			sqlMapClientWinusIf.commitTransaction();
			log.info("전체 처리된 insert 수: " + updateCnt);

		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeSQLException(e);
		} finally {
			endTransaction();
		}
	}
	
	private void processOrder(Map<String, Object> order) throws SQLException {
		sqlMapClientWinusIf.insert("oywcs.insertParcelWcsOrdrClsf", order);
	}

	private void executeBatchWithLogging(List<Map<String, Object>> outOrderList, int start, int end,
			String batchDescription) throws SQLException {
		try {
			sqlMapClientWinusIf.executeBatch();
		} catch (SQLException e) {
			log.error("Error executing " + batchDescription, e);
			logFailedQueries(outOrderList.subList(start, end));
			throw e;
		}
	}

	private void logFailedQueries(List<Map<String, Object>> failedOrders) {
		for (Map<String, Object> order : failedOrders) {
			log.error("Failed order: " + order);
			List<Map<String, Object>> orderDetailList = (List<Map<String, Object>>) order.get("children");
			for (Map<String, Object> orderDetail : orderDetailList) {
				log.error("Failed order detail: " + orderDetail);
			}
		}
	}

	private void endTransaction() {
		try {
			if (sqlMapClientWinusIf != null) {
				sqlMapClientWinusIf.endTransaction();
			}
		} catch (SQLException e) {
			log.error("Error ending transaction", e);
		}
	}
	
	public void insertParcelWcsShipment(Map<String, Object> param) {
		try {
			sqlMapClientWinusIf.insert("oywcs.insertParcelWcsShipment", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}
	
	public void insertParcelWcsStatChange(Map<String, Object> param) {
		try {
			sqlMapClientWinusIf.insert("oywcs.insertParcelWcsStatChange", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}
	
	public void insertParcelWcsOrdrComplete(Map<String, Object> param) {
		try {
			sqlMapClientWinusIf.insert("oywcs.insertParcelWcsOrdrComplete", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}
	
	public List<Map<String, Object>> selectOrdIdList(Map<String, Object> param) {
		return executeQueryForList("oywcs.selectOrdIdList", param);

	}
	
	public List<ShipmentDto> selectBlobPdfShipment(Map<String, Object> param, String company) {
		List<ShipmentDto> pdfShipmentList = new ArrayList<ShipmentDto>();
		try {
			if(company.equals("20")){ //DHL
				pdfShipmentList = sqlMapClientWinusIf.queryForList("oywcs.selectShipmentByDHL", param);
			}
			if(company.equals("30")){ //QXP
				pdfShipmentList = sqlMapClientWinusIf.queryForList("oywcs.selectShipmentByQXP", param);
			}
			if(company.equals("60")){ //FEDEX
				pdfShipmentList = sqlMapClientWinusIf.queryForList("oywcs.selectShipmentByFedex", param);
			}
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}	
		return pdfShipmentList;
	}
	
	public List<Map<String, Object>> selectPdfShipmentPath(Map<String, Object> param) {
		return executeQueryForList("oywcs.selectPdfShipmentPath", param);
	}

	public List<Map<String, Object>> selectPickingConfirmOrderList(Map<String, Object> param) {
		return executeQueryForList("oywcs.selectPickingConfirmOrderList", param);
	}

	public void insertParcelWcsShipmentChange(Map<String, Object> param) {
		try {
			sqlMapClientWinusIf.insert("oywcs.insertParcelWcsShipmentChange", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}

	public void insertFailedRequestData(Map<String, Object> param) {
		try {
			sqlMapClientWinusIf.insert("oywcs.insertParcelWcsFail", param);
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeSQLException(e);
		}
	}
	
	public List<Map<String, Object>> selectWorkList(Map<String, Object> param) {
		try {
			return sqlMapClientWinusIf.queryForList("oywcs.selectWorkList", param);
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeSQLException(e);
		}
	}
}
