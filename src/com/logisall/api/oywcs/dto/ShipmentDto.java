package com.logisall.api.oywcs.dto;

public class ShipmentDto {
	private String ordId;
	private String ordrNo;
	private String wblNo;
	private String wblImgType = "pdf";
	private String srtCd;
	private byte[] wblImg;

	public String getOrdId() {
		return ordId;
	}

	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}

	public String getOrdrNo() {
		return ordrNo;
	}

	public void setOrdrNo(String ordrNo) {
		this.ordrNo = ordrNo;
	}
	
	public byte[] getWblImg() {
		return wblImg;
	}

	public void setWblImg(byte[] wblImg) {
		this.wblImg = wblImg;
	}

	public String getWblNo() {
		return wblNo;
	}

	public void setWblNo(String wblNo) {
		this.wblNo = wblNo;
	}

	public String getWblImgType() {
		return wblImgType;
	}

	public String getSrtCd() {
		return srtCd;
	}

	public void setSrtCd(String srtCd) {
		this.srtCd = srtCd;
	}

}
