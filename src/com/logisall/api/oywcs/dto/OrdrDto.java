package com.logisall.api.oywcs.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdrDto {
	  private String ordrNo;
	    private String srtCd;

	    // Getters and Setters
	    public String getOrdrNo() {
	        return ordrNo;
	    }

	    public void setOrdrNo(String ordrNo) {
	        this.ordrNo = ordrNo;
	    }

	    public String getSrtCd() {
	        return srtCd;
	    }

	    public void setSrtCd(String srtCd) {
	        this.srtCd = srtCd;
	    }
}
