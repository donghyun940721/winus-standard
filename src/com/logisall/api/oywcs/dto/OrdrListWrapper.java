package com.logisall.api.oywcs.dto;

import java.util.List;

public class OrdrListWrapper {
	private List<OrdrDto> ordrList;

	// Getter and Setter
	public List<OrdrDto> getOrdrList() {
		return ordrList;
	}

	public void setOrdrList(List<OrdrDto> ordrList) {
		this.ordrList = ordrList;
	}
}
