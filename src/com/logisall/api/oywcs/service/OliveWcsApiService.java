package com.logisall.api.oywcs.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.api.oywcs.dao.OliveWcsApiDao;
import com.logisall.api.oywcs.dto.OrdrDto;
import com.logisall.api.oywcs.dto.OrdrListWrapper;
import com.logisall.api.oywcs.dto.ShipmentDto;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.HttpUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service
@SuppressWarnings("unchecked")
public class OliveWcsApiService {
	private final Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private OliveWcsApiDao oliveWcsApiDao;
	
	@Autowired
	private WMSOP030Dao wmsop030Dao;
	
	@Value("${oywcs.api.url}")
	private String wcsApiUrl;

    
	 /*
     * 특송사별 주문 분류 전송
     * */
    public Map<String, Object> sendOrderClassifications(Map<String, Object> requestData){
    	String ifApiUrl = "/api/ifs/outb/01_01";
    	log.info(wcsApiUrl+ifApiUrl);
        // 요청 데이터 생성
        Map<String, Object> jsonRequest = new HashMap<>();
         String ifId = createIfId("01_01");
        String ifDt = createIfDt();
        jsonRequest.put("ifId", ifId);
        jsonRequest.put("ifDt", ifDt);
        removeDuplicatesOrders(requestData);
        List<Map<String, Object>> ordrReqList = (List<Map<String, Object>>) requestData.get("ordrList");
        jsonRequest.put("ordrList", ordrReqList); // 주문 리스트 데이터 추가

        Map<String, Object> ordrSrtCdMap = new HashMap<>();
        for (Map<String, Object> ordr : ordrReqList) {
        	ordrSrtCdMap.put((String) ordr.get("ordrNo"), (String)ordr.get("srtCd"));
        }
		try {
			String jsonBody = new ObjectMapper().writeValueAsString(jsonRequest);
			// HTTP POST 요청 보내기
	        String responseJson = HttpUtil.sendJsonHttpRequest(wcsApiUrl+ifApiUrl, HttpMethod.POST, jsonBody);
            responseJson = removeSurroundingQuotes(responseJson);

	        // 응답 데이터 파싱
	        Map<String, Object> responseMap = new ObjectMapper().readValue(responseJson, new TypeReference<HashMap<String, Object>>() {});
	       
	        // 응답 데이터 로깅
	        log.info("Response Data: " + responseMap);
	        // 응답 데이터에서 orderList 처리
	        List<Map<String, Object>> ordrList = (List<Map<String, Object>>) responseMap.get("ordrList");
//	        oliveWcsApiDao.insertBulkOrder(ordrList);
	        for (Map<String, Object> order : ordrList) {
	        	String ordrSrtCd = (String) ordrSrtCdMap.get(order.get("ordrNo"));
	        	order.put("srtCd", ordrSrtCd);
	        	order.put("ifId", ifId);
	        	order.put("ifDt", ifDt);
            	SessionListener.setSessionParams(order);
            	oliveWcsApiDao.insertParcelWcsOrdrClsf(order);
	        }
	        return responseMap;
		} catch (Exception e) {
			log.error(e);
			saveFailedRequestDataToDb(requestData, ifId, ifDt, ifApiUrl , e);
			throw new RuntimeException(e);
		}
    }
    

    //송장정보 매핑
	private List<ShipmentDto> mappingShipmentFile(Map<String, List<OrdrDto>> orderListByShippingCode) {
		List<ShipmentDto> sendOrdrList = new ArrayList<ShipmentDto>();
        
        // 각 srtCd별 리스트를 처리 (여기서 비즈니스 로직 처리 가능)
        for (Map.Entry<String, List<OrdrDto>> entry : orderListByShippingCode.entrySet()) {
        	Map<String, Object> paramMap = new HashMap<String, Object>();
            String srtCd = entry.getKey();
            List<OrdrDto> listBySrtCd = entry.getValue();
            paramMap.put("ordrList", listBySrtCd);
			if("20".equals(srtCd) || "30".equals(srtCd) || "60".equals(srtCd)) {// DHL, QXP FEDEX일때
				List<ShipmentDto> dhlShipmentList = oliveWcsApiDao.selectBlobPdfShipment(paramMap, srtCd);
				sendOrdrList.addAll(dhlShipmentList);
			}

			if("10".equals(srtCd) || "50".equals(srtCd)) {// EMS, 판토스일 때
				List<Map<String, Object>> shipmentPathList = oliveWcsApiDao.selectPdfShipmentPath(paramMap);
				for (Map<String, Object> shipment : shipmentPathList) {
					shipment.put("SHIPPING_COMPANY", srtCd);
				}
				if(shipmentPathList.size() ==0) {
					throw new NullArgumentException("미발급 운송장이 존재합니다.");
				}
				addShipmentFile(sendOrdrList, shipmentPathList);
			}
        }
		return sendOrdrList;
	}
    
	private void addShipmentFile(List<ShipmentDto> sendOrdrList, List<Map<String, Object>> shipmentPathList) {
		for (Map<String, Object> shipment : shipmentPathList) {
			String ordId = (String)shipment.get("ORG_ID");
			String ordrNo = (String)shipment.get("ORG_ORD_ID");
			String wblNo = (String)shipment.get("BL_NO");
			String filePath = (String)shipment.get("FILE_PATH");
			String fileName = (String)shipment.get("ORG_FILENAME");
			String srtCd = (String)shipment.get("SHIPPING_COMPANY");
			String uploadFilePath = filePath + File.separator + fileName;
			File file = new File(uploadFilePath);

			if (file.exists()) {
			    log.info("파일이 존재합니다.");
			    try {
					byte[] wblImg = Files.readAllBytes(file.toPath());
					ShipmentDto shipmentDto = new ShipmentDto();
					shipmentDto.setOrdId(ordId);
					shipmentDto.setOrdrNo(ordrNo);
					shipmentDto.setWblNo(wblNo);
					shipmentDto.setWblImg(wblImg);
					shipmentDto.setSrtCd(srtCd);
					sendOrdrList.add(shipmentDto);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				log.info("파일이 존재하지 않습니다.");
			}
		}
	}
    
    private Map<String, List<OrdrDto>> divideOrderListByShippingCode(OrdrListWrapper ordrListWrapper){
    	 // 받아온 데이터 리스트
        List<OrdrDto> ordrList = ordrListWrapper.getOrdrList();

        // srtCd 별로 리스트를 분리하는 Map
        Map<String, List<OrdrDto>> srtCdMap = new HashMap<>();

        // 각 srtCd의 그룹을 리스트로 나눔
        for (OrdrDto ordr : ordrList) {
            String srtCd = ordr.getSrtCd();

            // Map에 존재하지 않으면 새로 리스트 추가
            if (!srtCdMap.containsKey(srtCd)) {
                srtCdMap.put(srtCd, new ArrayList<OrdrDto>());
            }
            // srtCd에 해당하는 리스트에 추가
            srtCdMap.get(srtCd).add(ordr);
        }

        // 분리된 리스트 반환 (원하는 경우 반환)
        return srtCdMap;
    }
    
    /*
     * 주문작업 상태변경
     * */
	public Map<String, Object> sendOrderStatusChange(Map<String, Object> requestData) {
		String ifApiUrl = "/api/ifs/outb/03_01";
		log.info(wcsApiUrl+ifApiUrl);
        // 요청 데이터 생성
        Map<String, Object> jsonRequest = new HashMap<>();
        List<Map<String, Object>> requestOrdrList = (List<Map<String, Object>>) requestData.get("ordrList");
        String ifId = createIfId("03_01");
        String ifDt = createIfDt();
        jsonRequest.put("ifId", ifId);
        jsonRequest.put("ifDt", ifDt);
        jsonRequest.put("ordrList", requestOrdrList); // 주문 리스트 데이터 추가

		try {
			String jsonBody = new ObjectMapper().writeValueAsString(jsonRequest);
			  // HTTP POST 요청 보내기
	        String responseJson = HttpUtil.sendJsonHttpRequest(wcsApiUrl+ifApiUrl, HttpMethod.POST, jsonBody);
            responseJson = removeSurroundingQuotes(responseJson);
            
	        // 응답 데이터 파싱
	        Map<String, Object> responseMap = new ObjectMapper().readValue(responseJson, new TypeReference<HashMap<String, Object>>() {});
	        // 응답 데이터 로깅
	        log.info("Response Data: " + responseMap);
	        // 응답 데이터에서 orderList 처리
	        List<Map<String, Object>> ordrList = (List<Map<String, Object>>) responseMap.get("ordrList");
	        
	        Map<String, String> ordrStatMap = createOrdrStatMap(requestOrdrList);	        
	        for (Map<String, Object> order : ordrList) {
	        	String ordrNo = (String) order.get("ordrNo");
	            // srtCdMap에서 ordrNo에 해당하는 srtCd 값을 찾아 추가
	            if (ordrStatMap.containsKey(ordrNo)) {
	            	order.put("ordrStat", ordrStatMap.get(ordrNo));
	            }
	        	order.put("ifId", ifId);
	        	order.put("ifDt", ifDt);
	        	

            	SessionListener.setSessionParams(order);
            	oliveWcsApiDao.insertParcelWcsStatChange(order);
	        }
	        return responseMap;
		} catch (Exception e) {
			log.error(e);
			saveFailedRequestDataToDb(requestData, ifId, ifDt, ifApiUrl, e);
			throw new RuntimeException(e);
		}
	}

	// ordrNo와 ordrStat를 매핑하는 메서드 분리
	private Map<String, String> createOrdrStatMap(List<Map<String, Object>> requestOrdrList) {
	    Map<String, String> srtCdMap = new HashMap<>();
	    for (Map<String, Object> requestOrder : requestOrdrList) {
	        srtCdMap.put((String) requestOrder.get("ordrNo"), (String) requestOrder.get("ordrStat"));
	    }
	    return srtCdMap;
	}
	
    /*
     * WCS 작업완료 정보 수신
     * */
	public Map<String, Object> receiveOrderCompletes(Map<String, Object> reqBody) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		List<Map<String, Object>> ordrList = (List<Map<String, Object>>) reqBody.get("ordrList");
		if(ordrList == null) {
			log.info("request ordrList is null");
			return responseMap;
		}
		String ifId = (String) reqBody.get("ifId");
		String ifDt = (String) reqBody.get("ifDt");
		
		log.info("ifId: "+ ifId);
		log.info("ifDt: "+ifDt);
		List<Map<String, Object>> orderResultList = new ArrayList<>();		
        for (Map<String, Object> order : ordrList) {
            String ordrNo = (String) order.get("ordrNo");
            String rsltCd;
            String rsltMsg;

            try {
            	SessionListener.setSessionParams(order);
            	order.put("ifId", ifId);
            	order.put("ifDt", ifDt);
            	oliveWcsApiDao.insertParcelWcsOrdrComplete(order); // iBatis로 insert 처리
                rsltCd = "0000"; // 성공 시 0000 반환
                rsltMsg = "정상 처리되었습니다."; // 성공 시 0000 반환
            } catch (Exception e) {
            	log.error(e);
                // 예외 발생 시 처리
                rsltCd = "9999"; // 실패 시 9999 반환
                rsltMsg = "처리에 실패하였습니다.";
            }

            // 응답을 위한 주문 정보와 결과 코드 설정
            Map<String, Object> orderResultMap = new HashMap<>();
            orderResultMap.put("ordrNo", ordrNo);
            orderResultMap.put("rsltCd", rsltCd);
            orderResultMap.put("rsltMsg", rsltMsg);
            orderResultList.add(orderResultMap);
        }
        
        responseMap.put("ifId", ifId);
        responseMap.put("ifDt", ifDt);
        responseMap.put("ordrList", orderResultList);
		return responseMap;
	}

	
	/* WCS 작업완료 데이터 WCS에서 출고확정 처리 5400 에서 select 후 호출 
	 * EAI 에서 프로시저 직접 콜하도록 변경해야함..
	 * */
	public Map<String, Object> saveOutOrderCompletes(Map<String, Object> reqBody) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> ordrList = (List<Map<String, Object>>) reqBody.get("ordrList");
			int tmpCnt = ordrList.size();
			if (tmpCnt > 0) {
				String[] emptyValue = new String[tmpCnt];
				Map<String, String[]> orderCompleteMap = CommonUtil.convertMapStringArrayFromListMap(
						ordrList,
						new String[] { "ordId", "ordSeq" }
						);

				// 프로시져에 보낼것들 다담는다
				Map<String, Object> modelIns = new HashMap<String, Object>();

				modelIns.put("ordId", orderCompleteMap.get("ordId"));
				modelIns.put("ordSeq", orderCompleteMap.get("ordSeq"));
				modelIns.put("workQty", emptyValue);

				// session 및 등록정보
				modelIns.put("LC_ID", (String) ordrList.get(0).get("lcId"));
				modelIns.put("WORK_IP", SessionListener.getIpAddr());
				modelIns.put("USER_NO", "0000000001");

				// dao
				modelIns = (Map<String, Object>) wmsop030Dao.saveOutComplete(modelIns);
				ServiceUtil.isValidReturnCodeNew("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")),
						(String) modelIns.get("O_MSG_NAME"));
			}
			responseMap.put("errCnt", 0);
			responseMap.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			responseMap.put("errCnt", -1);
			responseMap.put("MSG", be.getMessage());
		} catch (Exception e) {
			throw e;
		}

		return responseMap;
	}
	
	/*
	 * ordrList에서 출고확정된 주문리스트는 삭제 후 확정리스트 리턴
	 * */
	private List<Map<String, Object>> checkCompleteOrders(List<Map<String, Object>> ordrList) {
		List<List<Map<String, Object>>> splitOrdList = CommonUtil.splitList(ordrList, 500);
		Map<String, Object> orderParam = new HashMap<String, Object>();
		List<Map<String, Object>> completeOrders = new ArrayList<Map<String, Object>>();

		for (List<Map<String, Object>> ordList : splitOrdList) {
			orderParam.put("ordrList", ordList);
			List<Map<String, Object>> completeOrderList = oliveWcsApiDao.selectPickingConfirmOrderList(orderParam);
			Set<String> completeOrderNos = new HashSet<String>();
			for (Map<String, Object> completeOrder : completeOrderList) {
			    completeOrderNos.add((String)completeOrder.get("ORG_ORD_ID"));
			}

			// ordrList를 순회하면서 ordrNo가 completeOrderNos에 존재하는 경우 해당 항목을 제거
			Iterator<Map<String, Object>> iterator = ordrList.iterator();
			while (iterator.hasNext()) {
			    Map<String, Object> order = iterator.next();
			    if (completeOrderNos.contains(order.get("ordrNo"))) {
			    	completeOrders.add(order);
			        iterator.remove();
			    }
			}
		}
		
		return completeOrders;
	}

	// if_id 생성 메서드 (WMS3+식별자번호5자리(identifier)+날짜시간+Seq 8자리)
    private String createIfId(String identifier) {
        String wmsCode = "WMS"; // WMS 코드
        String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String seq = String.format("%08d", (int) (Math.random() * 100000000)); // Seq 8자리
        String ifId = wmsCode + identifier + date + seq;
        if (ifId.length() != 30) {
            throw new IllegalArgumentException("ifId는 30자여야 합니다.. ifId: " + ifId+", 자리수: "+ ifId.length());
        }
        return ifId;
    }
    
    
    // if_dt 생성 메서드 (YYYY-MM-DD HH24:MI:SS)
    private String createIfDt() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
    
    //중복 데이터 삭제
    private void removeDuplicatesOrders(Map<String, Object> requestData) {
        // ordrList를 가져와서 List로 캐스팅
        List<Object> ordrList = (List<Object>) requestData.get("ordrList");

        if (ordrList != null) {
            // Set을 사용해 중복 제거
            Set<Object> uniqueSet = new LinkedHashSet<Object>(ordrList);

            // 중복이 제거된 Set을 다시 List로 변환
            List<Object> uniqueList = new ArrayList<Object>(uniqueSet);

            // 중복이 제거된 리스트를 다시 ordrList에 저장
            requestData.put("ordrList", uniqueList);
        }
    }
  
    // 문자열의 맨 앞과 끝에 있는 큰따옴표를 제거하는
    private String removeSurroundingQuotes(String str) {
    	str = StringEscapeUtils.unescapeJava(str);
        if (str != null && str.startsWith("\"") && str.endsWith("\"")) {
            return str.substring(1, str.length() - 1);
        }
        return str;
    }
    
    
    // 공통 메서드로 추출
    private Map<String, Object> sendOrderShipments(String apiIdentifier, OrdrListWrapper ordrListWrapper, boolean includeSrtCd) {
        String ifApiUrl = "/api/ifs/outb/" + apiIdentifier;
        log.info(wcsApiUrl + ifApiUrl);
        
        Map<String, Object> jsonRequest = new HashMap<>();
        String ifId = createIfId(apiIdentifier);
        String ifDt = createIfDt();
        
        jsonRequest.put("ifId", ifId);
        jsonRequest.put("ifDt", ifDt);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
        	 // srtCd에 따른 주문 리스트 분할 및 송장 정보 매핑
            Map<String, List<OrdrDto>> orderListByShippingCode = divideOrderListByShippingCode(ordrListWrapper);
        	List<ShipmentDto> sendOrdrList = mappingShipmentFile(orderListByShippingCode);
        	if(sendOrdrList.size() == 0) {
        		throw new NullArgumentException("sendOrdrList 에 매핑되는 송장정보 없음. ");
        	}
        	
            jsonRequest.put("ordrList", sendOrdrList);
            String jsonBody = objectMapper.writeValueAsString(jsonRequest);
            String responseJson = HttpUtil.sendJsonHttpRequest(wcsApiUrl + ifApiUrl, HttpMethod.POST, jsonBody);
            responseJson = removeSurroundingQuotes(responseJson);

            // 응답 데이터 파싱
            Map<String, Object> responseMap = objectMapper.readValue(responseJson, new TypeReference<HashMap<String, Object>>() {});
            log.info("Response Data: " + responseMap);

            // 응답 데이터에서 ordrList 처리
            List<Map<String, Object>> ordrList = (List<Map<String, Object>>) responseMap.get("ordrList");
            for (Map<String, Object> order : ordrList) {
                order.put("ifId", ifId);
                order.put("ifDt", ifDt);
                SessionListener.setSessionParams(order);
                if(includeSrtCd) {
                	oliveWcsApiDao.insertParcelWcsShipmentChange(order);
                }else {
                	oliveWcsApiDao.insertParcelWcsShipment(order);
                }
            }
            return responseMap;
        } catch (Exception e) {
            log.error(e);
			Map<String, Object> requestData = objectMapper.convertValue(ordrListWrapper, Map.class);
            saveFailedRequestDataToDb(requestData, ifId, ifDt, ifApiUrl, e);
            throw new RuntimeException(e);
        }
    }
    
    public void saveFailedRequestDataToDb(Map<String, Object> requestData, String ifId, String ifDt, String ifApiUrl, Exception exception) {
        try {
        	Map<String, Object> param = new HashMap<String, Object>();
        	// Map -> JSON 문자열로 변환
            String jsonString = new ObjectMapper().writeValueAsString(requestData);
        	param.put("ifId", ifId);
        	param.put("ifDt", ifDt);
        	param.put("ifApiUrl", ifApiUrl);
        	param.put("jsonString", jsonString);
        	param.put("message", getStackTraceAsString(exception));
        	SessionListener.setSessionParams(param);
            oliveWcsApiDao.insertFailedRequestData(param);
            log.info("인터페이스 실패정보 저장.");
        } catch (Exception dbEx) {
            log.error("인터페이스 실패정보 저장 실패: " + dbEx.getMessage());
            throw new RuntimeException(dbEx);
        }
    }
    
    private String getStackTraceAsString(Throwable throwable) {
    	StringBuilder stackTraceBuilder = new StringBuilder();
        while (throwable != null) {
            stackTraceBuilder.append(throwable.toString()).append("\n");
            for (StackTraceElement element : throwable.getStackTrace()) {
                stackTraceBuilder.append("\tat ").append(element.toString()).append("\n");
            }
            // 중첩된 예외로 이동
            throwable = throwable.getCause();
            if (throwable != null) {
                stackTraceBuilder.append("Caused by: ");
            }
        }
        return stackTraceBuilder.toString();
    }

    //송장전송
    public Map<String, Object> sendOrderShipments(OrdrListWrapper ordrListWrapper) {
        // sendOrdersShipments에서는 srtCd를 포함하지 않음
        return sendOrderShipments("02_01", ordrListWrapper, false);
    }

    //변경송장전송
    public Map<String, Object> orderShipmentChange(OrdrListWrapper ordrListWrapper) {
        // orderShipmentChange에서는 srtCd를 포함
        return sendOrderShipments("05_01", ordrListWrapper, true);
    }
    
    public Map<String, Object> orderShipmentChangePart(OrdrListWrapper ordrListWrapper) {
        return sendOrderShipmentsPart("05_01", ordrListWrapper, true);
    }
    
    private Map<String, Object> sendOrderShipmentsPart(String apiIdentifier, OrdrListWrapper ordrListWrapper, boolean includeSrtCd) {
        int limitBytes = 512000; //500kb
        
        String ifApiUrl = "/api/ifs/outb/" + apiIdentifier;
        log.info(wcsApiUrl + ifApiUrl);
        
        Map<String, Object> jsonRequest = new HashMap<>();
        String ifId = createIfId(apiIdentifier);
        String ifDt = createIfDt();
        
        jsonRequest.put("ifId", ifId);
        jsonRequest.put("ifDt", ifDt);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
             // srtCd에 따른 주문 리스트 분할 및 송장 정보 매핑
            Map<String, List<OrdrDto>> orderListByShippingCode = divideOrderListByShippingCode(ordrListWrapper);
            List<ShipmentDto> sendOrdrList = mappingShipmentFile(orderListByShippingCode);
            if(sendOrdrList.size() == 0) {
                throw new NullArgumentException("sendOrdrList 에 매핑되는 송장정보 없음. ");
            }
            int indexRange = getMaxIndexLengthListByte(sendOrdrList, limitBytes);
            
            int loopSize = (int)Math.ceil(sendOrdrList.size()/indexRange);
            
            Map<String, Object> resultMap = new HashMap<String,Object>();
            
            for(int i = 0 ; i < loopSize ; i++){
                int splitIndex = (i+1)*indexRange;
                if ( i == loopSize-1){
                    splitIndex = sendOrdrList.size();
                }
                
                List<ShipmentDto> sendList = sendOrdrList.subList(i*indexRange,splitIndex);
                jsonRequest.put("ordrList", sendList);
                String jsonBody = objectMapper.writeValueAsString(jsonRequest);
                String responseJson = HttpUtil.sendJsonHttpRequest(wcsApiUrl + ifApiUrl, HttpMethod.POST, jsonBody);
                responseJson = removeSurroundingQuotes(responseJson);
                
                // 응답 데이터 파싱
                Map<String, Object> responseMap = objectMapper.readValue(responseJson, new TypeReference<HashMap<String, Object>>() {});
                log.info("Response Data: " + responseMap);
                
                // 응답 데이터에서 ordrList 처리
                List<Map<String, Object>> ordrList = (List<Map<String, Object>>) responseMap.get("ordrList");
                if( i == 0){
                    resultMap.putAll(responseMap);
                }
                for (Map<String, Object> order : ordrList) {
                    order.put("ifId", ifId);
                    order.put("ifDt", ifDt);
                    SessionListener.setSessionParams(order);
                    if(includeSrtCd) {
                        oliveWcsApiDao.insertParcelWcsShipmentChange(order);
                    }else {
                        oliveWcsApiDao.insertParcelWcsShipment(order);
                    }
                    ((List<Map<String, Object>>)resultMap.get("ordrList")).add(order);
                }
            }
            return resultMap;
        } catch (Exception e) {
            log.error(e);
            Map<String, Object> requestData = objectMapper.convertValue(ordrListWrapper, Map.class);
            saveFailedRequestDataToDb(requestData, ifId, ifDt, ifApiUrl, e);
            throw new RuntimeException(e);
        }
    }

    private int getMaxIndexLengthListByte(List list, int bytes) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            
            int minIndex = 0;
            int maxIndex = list.size()-1;
            
            //validation minIndex size , maxIndex size
            
            //최소 index의 byte의 array가 limitByte값보다 큰경우
            if (objectMapper.writeValueAsBytes(list.subList(0, 1)).length > bytes){
                return 1;
            }
            
            //최대 index의 byte의 array가 limitByte값보다 작은경우
            if (objectMapper.writeValueAsBytes(list.subList(0, maxIndex)).length <= bytes){
                return list.size();
            }
            
            int resultIndex = -1;
            int middleIndex;
            //이진탐색 진행
            while(minIndex <= maxIndex){
                middleIndex = (minIndex+maxIndex)/2;
                int byteLength = objectMapper.writeValueAsBytes(list.subList(0, middleIndex)).length;
                if (byteLength > bytes){
                    maxIndex = middleIndex-1;
                }
                else if (byteLength <= bytes){
                    resultIndex = middleIndex;
                    minIndex = middleIndex+1;
                }
            }
            
            return resultIndex+1;
        }catch(Exception e){
            log.error("failed method getMaxIndexInListByte: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }
}
