package com.logisall.api.weverse.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.logisall.api.common.annotation.NotNullOrEmpty;
import com.logisall.api.common.annotation.WNumeric;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderReqDto {
    @JsonProperty("no_info1")
    @NotNullOrEmpty(message = "주문번호는 필수입니다.")
    private String noInfo1;

    @JsonProperty("no_info2")
    private String noInfo2;

    private String country;

    @JsonProperty("type_code")
    private String typeCode;
    @JsonProperty("tax_no")
    private String taxNo;
    private String memo;
    
    private Sender sender;
    private Recipient recipient;
    private List<Parcel> parcels;
    private Extra extra;
    private Payment payment;
    
    private String receiptNo;
    private String regNo = "weverse-api";
    private String regIp;
    private String lcId;
    private String custId;

    //보내는이
    @Getter
    @Setter
    public static class Sender {
    	
    	@NotNullOrEmpty(message = "발송인명은 필수입니다.")
        private String name1;
        private String name2;
        
        @NotNullOrEmpty(message = "핸드폰번호는 필수입니다.")
        private String phone;
        private String mobile;
        private String country;
        private String zipcode;

        @JsonProperty("addr_state")
        private String addrState;
        @JsonProperty("addr_city")
        private String addrCity;
        @JsonProperty("addr_gu")
        private String addrGu;
        
        private String addr2;
        private String email;
        
        @JsonProperty("id_no")
        private String idNo;
    }

    //받는이
    @Getter
    @Setter
    public static class Recipient {
    	
    	@JsonProperty("id_no")
    	@NotNullOrEmpty(message = "신분번호는 필수입니다.")
    	private String idNo;
        private String name1;
        private String name2;
        private String phone;
        private String mobile;
        private String country;
        private String zipcode;

        @JsonProperty("addr_state")
        private String addrState;
        @JsonProperty("addr_city")
        private String addrCity;
        @JsonProperty("addr_gu")
        private String addrGu;

        private String addr2;
        private String email;
    }

    //상품 정보
    @Getter
    @Setter
    public static class Parcel {
    	private int seq;
        private String name;
        
        @NotNullOrEmpty(message = "주문수량은 필수입니다.")
        private int quantity;

        @JsonProperty("box_count")
        private int boxCount;

        @WNumeric(message = "상품무게는 숫자여야 합니다.")
        private double weight = 0.1;
        
        @WNumeric(message = "상품금액은 숫자여야 합니다.")
        private double price;
        private String currency;
        private String sku;
        private String hscode;
        private String brand;
        private String url;
        private String material;
        @JsonProperty("goods_type")
        private String goodsType;
        @JsonProperty("origin_goods_code")
        private String originGoodsCode;
        @JsonProperty("is_freebie")
        private String isFreebie;
        
        private Prices prices; 
        @Getter
        @Setter
        public static class Prices {
        	@WNumeric(message = "금액은 숫자여야 합니다.")
            private double price;
        }
    }

    //추가 정보
    @Getter
    @Setter
    public static class Extra {
        private String data1;
    }

    //결제 정보
    @Getter
    @Setter
    public static class Payment {
        private String delivery;
        @WNumeric(message = "결제금액은 숫자여야 합니다.")
        private double purchaser;
    }
}
