package com.logisall.api.weverse.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeverseResponseDto {
	private String code = "2000";
	private String message = "success";
	private Map<String, Object> result;
}
