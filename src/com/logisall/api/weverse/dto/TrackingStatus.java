package com.logisall.api.weverse.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TrackingStatus {
	 WAITING("해외배송 허브센터 이동", "DF"),
	 INBOUND("해외배송 허브센터 인수", "AF"),
	 OUTBOUND("해외 배송 중", "TR"),
	 INCUSTOMS("통관 중", "IC/CD/UD"),
	 CUSTOMS("통관 완료", "CR"),
	 SHIPPING("배송중", "WC"),
	 DELIVERED("배송 완료", "OK"),
	 CLOSED("배송 종료", "CS"),
	 BOUNCE("반송 진행", "RT"),
	 ERROR("주소 오류", "BA"),
	 REJECTED("수취 거절", "RD"),
	 FAILED("배달 실패", "ND"),
	 DAMAGED("분실/파손", "DD"),
	 UNKNOWN("Unknown", "Unknown"); // 유효하지 않은 값을 처리할 기본 상수

	private String mean;
	private String dhlTrackingStatus;

	TrackingStatus(String mean) {
		this.mean = mean;
	}
	
	TrackingStatus(String mean, String dhlTrackingStatus) {
		this.mean = mean;
		this.dhlTrackingStatus = dhlTrackingStatus;
	}

	public String getMean() {
		return mean;
	}
    public String getDhlTrackingStatus() {
        return dhlTrackingStatus;
    }

    public static String getDhlTrackingStatus(TrackingStatus status) {
        return status.getDhlTrackingStatus();
    }
    
    @JsonCreator
    public static TrackingStatus fromValue(String value) {
        if (value == null) {
            return UNKNOWN;
        }

        // Enum 이름으로 비교
        for (TrackingStatus status : TrackingStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                return status;
            }
        }

        // DHL 코드값으로 비교
        for (TrackingStatus status : TrackingStatus.values()) {
            
        	//ID/CD/UD값 incustoms로 리턴
        	if (status.getDhlTrackingStatus().contains("/")) {
                String[] codes = status.getDhlTrackingStatus().split("/");
                for (String code : codes) {
                    if (code.equalsIgnoreCase(value)) {
                        return status;
                    }
                }
            } else if (status.getDhlTrackingStatus().equalsIgnoreCase(value)) {
                return status;
            }
        }

        return UNKNOWN;
    }

    @JsonValue
    public String toValue() {
        return name();
    }
}
