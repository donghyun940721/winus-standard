package com.logisall.api.weverse.dto;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrackResDto {
	private static final Log log = LogFactory.getLog(TrackingDto.class);  // static final로 선언
	   
	@JsonProperty("receipt_no")
	private String receiptNo;    
	@JsonProperty("no_info1")
	private String noInfo1;
	@JsonProperty("waybill_no")
    private String waybillNo;    
    private TrackingStatus recentStatus;  
    private String url;           
    private String courier;       // "DHL" 고정값
    private int count;           
    private List<TrackingDto> trackings;
    
    @JsonGetter("recentStatus")
    public String getRecentStatusAsString() {
        return recentStatus != null ? recentStatus.name().toLowerCase() : null;
    }
	
	@Getter @Setter
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class TrackingDto {
		@JsonProperty("receipt_no")
		private String receiptNo;
		@JsonProperty("waybill_no")
	    private String waybillNo;  
		@JsonProperty("check_point")
	    private String checkPoint;
	    private String courier;
	    private String nation;
	    @JsonProperty("nation_code")
	    private String nationCode;
	    private TrackingStatus status;
	    private String detail;
	    @JsonProperty("local_time")
	    private String localTime;
	    @JsonProperty("checked_at")
	    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
	    private String checkedAt;
	    private TimezoneDto timezone;

	    @JsonGetter("status")
        public String getStatusAsString() {
            return status != null ? status.name().toLowerCase() : null;
        }
	   
	}

	@Getter @Setter
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class TimezoneDto {
	    private String kst;
	}
	
}
