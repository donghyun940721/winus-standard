package com.logisall.api.weverse.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.api.weverse.dto.OrderReqDto;

import egovframework.rte.psl.orm.ibatis.SqlMapClientTemplate;

@SuppressWarnings(value= {"deprecation","unchecked"})
@Repository
public class WeverseDao{
	private final SqlMapClientTemplate sqlMapClientTemplate;
	private final SqlMapClientTemplate sqlMapClientIfTemplate;

	@Autowired
	public WeverseDao(SqlMapClient sqlMapClient, SqlMapClient sqlMapClientWinusIf) {
		this.sqlMapClientIfTemplate = new SqlMapClientTemplate(sqlMapClientWinusIf);
		this.sqlMapClientTemplate = new SqlMapClientTemplate(sqlMapClient);
	}

	public String selectOrderByNoInfo1(String noInfo1){
		return (String) sqlMapClientIfTemplate.queryForObject("weverse.selectOrderByNoInfo1", noInfo1);
	}
	
	public List<Map<String, Object>> selectNotLinkOrdeList(Map<String, Object> paramMap){
		return sqlMapClientIfTemplate.queryForList("weverse.selectNotLinkOrdeList",paramMap);
	}
	
	public List<Map<String, Object>> selectTrackingListByOrders(String no){
		return sqlMapClientIfTemplate.queryForList("weverse.selectTrackingListByOrders",no);
	}
	

	public void insertBatchOrder(OrderReqDto orderReqDto) {
		sqlMapClientIfTemplate.insert("weverse.insertBatchOrder", orderReqDto);
	}

	public List<String> selectWmsom010(Map<String, Object> param) {
		return (List<String>)sqlMapClientTemplate.queryForList("weverse.selectWmsom010", param);
	}
	public List<Map<String, Object>> selectIfOrders(Map<String, Object> param) {
		return (List<Map<String, Object>>)sqlMapClientIfTemplate.queryForList("weverse.selectIfOrders", param);
	}
	
	public void updateWeverseIfYn(Map<String, Object> param) {
		sqlMapClientIfTemplate.update("weverse.updateWeverseIfYn", param);
	}

	public void insertWmsom010(Map<String, Object> param) {
		sqlMapClientTemplate.insert("weverse.insertWmsom010", param);
	}

}
