package com.logisall.api.weverse.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logisall.api.common.annotation.WValidation;
import com.logisall.api.controller.BaseController;
import com.logisall.api.weverse.dto.OrderReqDto;
import com.logisall.api.weverse.dto.WeverseResponseDto;
import com.logisall.api.weverse.service.WeverseService;

@RestController
@RequestMapping("/api/weverse")
public class WeverseController extends BaseController{
	private final Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private WeverseService weverseService;
	
	/*
	 * 오더입력&확정
	 * */
	@RequestMapping(value = "/order/create.do", method = RequestMethod.POST)
	public ResponseEntity<WeverseResponseDto> createOrder(Map<String, Object> model, @RequestBody @WValidation OrderReqDto orderReqDto,
			HttpServletRequest request, HttpServletResponse response) throws SQLException{

		WeverseResponseDto result = weverseService.createOrder(request, model, orderReqDto);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/order/sendWinusWms.do", method = RequestMethod.POST)
	public ResponseEntity<WeverseResponseDto> sendWinusWms(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response){
		return new ResponseEntity<>(weverseService.saveWmsom010(request, model), HttpStatus.OK);
	}
	
	/*
	 * 트래킹
	 * */
	@RequestMapping(value = "/order/track.do", method = RequestMethod.GET)
	public ResponseEntity<WeverseResponseDto> orderTrack(Map<String, Object> model
			, @RequestParam(required = true) String no, HttpServletRequest request, HttpServletResponse response){

		WeverseResponseDto result = weverseService.selectOrderTrack(model, no);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
}
