package com.logisall.api.weverse.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logisall.api.common.exception.ValidationException;
import com.logisall.api.weverse.dao.WeverseDao;
import com.logisall.api.weverse.dto.OrderReqDto;
import com.logisall.api.weverse.dto.OrderReqDto.Parcel;
import com.logisall.api.weverse.dto.OrderResDto;
import com.logisall.api.weverse.dto.TrackResDto;
import com.logisall.api.weverse.dto.TrackResDto.TimezoneDto;
import com.logisall.api.weverse.dto.TrackResDto.TrackingDto;
import com.logisall.api.weverse.dto.TrackingStatus;
import com.logisall.api.weverse.dto.WeverseResponseDto;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.SessionListener;

@Service
public class WeverseService {
	private final Log log = LogFactory.getLog(this.getClass());
	@Autowired
	private WeverseDao weverseDao;
	@Resource(name = "authKeyMapBean") 
    private Map<String, Map<String, Object>> authKeyMap;
	
	public WeverseResponseDto createOrder(HttpServletRequest request, Map<String, Object> model, OrderReqDto orderReqDto) {
		WeverseResponseDto result = new WeverseResponseDto();
		// 중복 체크
        String existingOrder = weverseDao.selectOrderByNoInfo1(orderReqDto.getNoInfo1());
        if (existingOrder != null) {
            throw new ValidationException("Duplicate order number: " + orderReqDto.getNoInfo1());
        }
        int parcelSeq = 1;
        List<Parcel> parcelList = orderReqDto.getParcels();
        String receiptNo = createReceiptNo();
        orderReqDto.setReceiptNo(receiptNo);
        SessionListener.<OrderReqDto>setSessionParams(orderReqDto);
        //상품시퀀스 생성
        for (Parcel parcel : parcelList) {
			parcel.setSeq(parcelSeq++);
		}
        // 주문 저장
        orderReqDto.setLcId(request.getAttribute("LC_ID").toString());
        orderReqDto.setCustId(request.getAttribute("CUST_ID").toString());
        weverseDao.insertBatchOrder(orderReqDto);
        
        //가송장번호만 리턴
        Map<String, Object> resultmap = new HashMap<String, Object>();
        OrderResDto orderResult = OrderResDto.builder()
        		.noInfo1(orderReqDto.getNoInfo1())
        		.reciptNo(receiptNo).build(); 
        resultmap.put("order", orderResult);
        result.setResult(resultmap);
		return result;
	}
	
	
	//가송장번호(주문접수번호) 생성
    public String createReceiptNo() {
        String wmsCode = "LO"; // WMS 코드
        String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String seq = String.format("%04d", (int) (Math.random() * 1000)); // Seq 4자리
        String ifId = wmsCode + date + seq;
        return ifId;
    }

	public WeverseResponseDto selectOrderTrack(Map<String, Object> model,String no) {
		List<Map<String, Object>> trackingList = weverseDao.selectTrackingListByOrders(no);
		Map<String, List<Map<String, Object>>> groupedByOrderId = new HashMap<>();
//		if(1==1) {
//			throw new RuntimeException("에러로그 테스트");
//		}
		// ORD_ID 기준으로 그룹핑
		for (Map<String, Object> tracking : trackingList) {
			String ordId = (String) tracking.get("ORD_ID");
			if (!groupedByOrderId.containsKey(ordId)) {
				groupedByOrderId.put(ordId, new ArrayList<Map<String, Object>>());
			}
			groupedByOrderId.get(ordId).add(tracking);
		}
		List<TrackResDto> result = new ArrayList<>();
		
		// 각 ORD_ID 그룹별로 TrackResDto 생성
	    for (Map.Entry<String, List<Map<String, Object>>> entry : groupedByOrderId.entrySet()) {
	        List<Map<String, Object>> group = entry.getValue();
	        if (!group.isEmpty()) {
	            
	            Map<String, Object> firstRecord = group.get(0);
	    	    String recentEventCode = (String) group.get(0).get("EV_D_STAT_EVENT_CODE");
	    	    String waybillNo = (String) firstRecord.get("AWB_NUMBER");
	    	    TrackResDto trackRes = TrackResDto.builder()
	    	    		.receiptNo((String) firstRecord.get("ORD_ID"))
	    	    		.noInfo1((String) firstRecord.get("ORG_ORD_ID"))
	    	    		.waybillNo(waybillNo)
	    	    		.courier("DHL")
	    	    		.url("https://www.dhl.com/kr-ko/home/tracking.html?tracking-id="+waybillNo)
	    	    		.recentStatus(TrackingStatus.fromValue(recentEventCode))
	    	    		.build();
	            
	            // trackings 설정
	            List<TrackingDto> trackings = new ArrayList<>();
	            for (Map<String, Object> record : group) {
	                TrackingDto tracking = convertToTrackingDto(record);
	                trackings.add(tracking);
	            }
	            
	            trackRes.setTrackings(trackings);
	            trackRes.setCount(trackings.size());
	            
	            result.add(trackRes);
	        }
	    }
		
		Map<String, Object> trackResultMap = new HashMap<String, Object>();
		trackResultMap.put("tracks", result);
		WeverseResponseDto res = new WeverseResponseDto();
		res.setCode("2000");
		res.setMessage("success");
		res.setResult(trackResultMap);
		return res;
	}
	
	public TrackingDto convertToTrackingDto(Map<String, Object> record) {
		String eventDatetime = (String) record.get("EV_H_DATETIME");
		String gmtOffset = (String) record.get("GMTOFFSET");
		 // KST 시간 포맷팅 및 UTC 변환 로직
	    String kstTime =  DateUtil.convertTimeZone(
	    		eventDatetime,
	            "yyyy-MM-dd HH:mm:ss",
	            "yyyy-MM-dd'T'HH:mm:ssXXX",
	            gmtOffset,
	            TimeZone.getTimeZone("Asia/Seoul")
	    );
	    String utcTime = DateUtil.convertTimeZone(
	            kstTime,
	            "yyyy-MM-dd'T'HH:mm:ssXXX",
	            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
	            "+09:00",
	            TimeZone.getTimeZone("UTC")
	    );
		TimezoneDto timezone = TrackResDto.TimezoneDto.builder()
			        .kst(kstTime)
			        .build();
		
		return TrackResDto.TrackingDto.builder()
		        .receiptNo((String) record.get("ORD_ID"))
		        .waybillNo((String) record.get("AWB_NUMBER"))
		        .checkPoint((String) record.get("EV_D_AREA_DESCRIPTION"))
		        .courier("DHL")
		        .nationCode((String) record.get("COUNTRY_CD"))
		        .status(TrackingStatus.fromValue((String) record.get("EV_D_STAT_EVENT_CODE")))
		        .detail((String) record.get("EV_D_STAT_DESCRIPTION"))
		        .localTime(eventDatetime)
		        .checkedAt(utcTime)
		        .timezone(timezone)
		        .build();
	}
	
	@Transactional
	public WeverseResponseDto saveWmsom010(HttpServletRequest request, Map<String, Object> orderParam) {
		orderParam.put("LC_ID", request.getAttribute("LC_ID").toString());
		orderParam.put("CUST_ID", request.getAttribute("CUST_ID").toString());
		List<Map<String, Object>> notLinkOrderList = weverseDao.selectNotLinkOrdeList(orderParam);
		WeverseResponseDto result = processOrders(orderParam, notLinkOrderList);

		return result;
	}
	
	public WeverseResponseDto processOrders(Map<String, Object> orderParam, List<Map<String, Object>> notLinkOrderList) {
	    List<List<Map<String, Object>>> splitOrderList = CommonUtil.splitList(notLinkOrderList, 1000);
	    
	    int successCount = 0;
	    List<Map<String, Object>> failOrders = new ArrayList<>();

	    for (List<Map<String, Object>> orderList : splitOrderList) {
	        orderParam.put("list", orderList);

	        //주문 있는지 확인
	        List<String> existingOrders = weverseDao.selectWmsom010(orderParam);

	        // 신규 주문과 중복 주문 분류
	        List<Map<String, Object>> newOrders = new ArrayList<>();
	        List<Map<String, Object>> duplicateOrders = new ArrayList<>();

	        for (Map<String, Object> order : orderList) {
	            String orgOrdNo = (String) order.get("NO_INFO1");
	            if (existingOrders != null && existingOrders.contains(orgOrdNo)) {
	                duplicateOrders.add(order);
	            } else {
	                newOrders.add(order);
	            }
	        }

	        // 신규 주문 처리
	        if (!newOrders.isEmpty()) {
	        	orderParam.put("list", newOrders);
	        	orderParam.put("WMS_IF_YN", "Y"); 
	            weverseDao.insertWmsom010(orderParam);
	            weverseDao.updateWeverseIfYn(orderParam);
	            successCount += newOrders.size();
	        }

	        // 중복 주문 처리
	        if (!duplicateOrders.isEmpty()) {
	        	orderParam.put("list", duplicateOrders);
	        	orderParam.put("WMS_IF_YN", "E");
	            weverseDao.updateWeverseIfYn(orderParam);
	            failOrders.addAll(duplicateOrders);
	        }
	    }
	    WeverseResponseDto res = createResponse(successCount, failOrders);

	    return res;
	}

	public WeverseResponseDto createResponse(int successCount, List<Map<String, Object>> failOrders) {
	    WeverseResponseDto result = new WeverseResponseDto();
	    Map<String, Object> resultMap = new HashMap<>();
	    resultMap.put("successCount", successCount);
	    resultMap.put("failCount", failOrders.size());
	    resultMap.put("failOrders", failOrders);
	    result.setResult(resultMap);
	    return result;
	}

}
