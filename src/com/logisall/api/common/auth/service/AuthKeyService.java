package com.logisall.api.common.auth.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.api.common.exception.UnAuthorizedException;

@Service
public class AuthKeyService {
	private static final Log log = LogFactory.getLog(AuthKeyService.class);
	
	@Resource(name = "authKeyMapBean") 
    private Map<String, Map<String, Object>> authKeyMap;

	public String validateAuthKey(String authKey) {
		if (authKey == null || authKey.trim().isEmpty()) {
			return null;
		}

		// 캐시정보
		Map<String, Object> authInfo = authKeyMap.get(authKey);
		if(authInfo == null ) return null;
		String companyCd = (String) authInfo.get("COMPANY_CD");
		// 유효기간 체크
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String currentDate = sdf.format(new Date());

		String validStartDt = (String) authInfo.get("VALID_START_DT");
		String validEndDt = (String) authInfo.get("VALID_END_DT");

		// 시작일 체크
		if (validStartDt != null && currentDate.compareTo(validStartDt) < 0) {
			throw new UnAuthorizedException("인증키 만료.");
		}
		// 종료일 체크
		if (validEndDt != null && currentDate.compareTo(validEndDt) > 0) {
			throw new UnAuthorizedException("인증키 만료.");
		}

		return companyCd;

	}
	public String getCompanyCd(String authKey) {
		return authKeyMap.get(authKey).get("COMPANY_CD").toString();
	}
	
	public String getLcId(String authKey) {
		return authKeyMap.get(authKey).get("LC_ID").toString();
	}
	
	public String getCustId(String authKey) {
		return authKeyMap.get(authKey).get("CUST_ID").toString();
	}
}
