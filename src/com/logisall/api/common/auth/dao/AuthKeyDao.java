package com.logisall.api.common.auth.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;

import egovframework.rte.psl.orm.ibatis.SqlMapClientTemplate;
import lombok.RequiredArgsConstructor;

@Repository
public class AuthKeyDao {
	private final SqlMapClientTemplate sqlMapClientIfTemplate;

	@Autowired
	public AuthKeyDao(SqlMapClient sqlMapClientWinusIf) {
		this.sqlMapClientIfTemplate = new SqlMapClientTemplate(sqlMapClientWinusIf);
	}
    
	public List<Map<String, Object>> selectAllAuthKeyInfo() {
		return sqlMapClientIfTemplate.queryForList("auth.selectAllAuthKeyInfo", null);
	}
}
