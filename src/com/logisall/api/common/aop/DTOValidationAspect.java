package com.logisall.api.common.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.logisall.api.common.annotation.NotNullOrEmpty;
import com.logisall.api.common.annotation.WDateFormat;
import com.logisall.api.common.annotation.WLength;
import com.logisall.api.common.annotation.WNumeric;
import com.logisall.api.common.annotation.WPattern;
import com.logisall.api.common.annotation.WValidation;
import com.logisall.api.common.exception.ValidationException;


@Aspect
@Component
public class DTOValidationAspect {
	private final Log log = LogFactory.getLog(this.getClass());
	private int fieldCounter = 0;
    private Map<String, String> fieldMapping = new HashMap<>();
    private static final int MAX_DEPTH = 10;  // 재귀 깊이 제한

    @Pointcut("within(com.logisall.api..*)")
    public void inApiPackage() {}

    @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void hasRequestMapping() {}

    @Around("inApiPackage() && hasRequestMapping()")
    public Object validateDTO(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Map<String, String> validationErrors = new HashMap<>();
        
        // 메소드의 파라미터 정보 가져오기
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Class<?> targetClass = joinPoint.getTarget().getClass();
        Method method = targetClass.getMethod(signature.getMethod().getName(), 
                                            signature.getMethod().getParameterTypes());
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();

        // 각 파라미터 검사
        for (int i = 0; i < args.length; i++) {
            if (args[i] != null && shouldValidate(parameterAnnotations[i])) {
                validateObjectRecursively(args[i], "", validationErrors, 0, new HashSet<>());
            }
        }

        //개발시
        if (!validationErrors.isEmpty()) {
            throw new ValidationException(validationErrors, "validation errors");
        }

      //운영시 필드명 가리고
//    if (!validationErrors.isEmpty()) {
//        // 에러 메시지 변환
//        Map<String, String> maskedErrors = new HashMap<>();
//        for (Map.Entry<String, String> entry : validationErrors.entrySet()) {
//            String maskedFieldName = getOrCreateMaskedFieldName(entry.getKey());
//            maskedErrors.put(maskedFieldName, entry.getValue());
//        }
//        throw new ValidationException(maskedErrors);
//    }
        return joinPoint.proceed();
    }
    
	private boolean shouldValidate(Annotation[] annotations) {
		boolean hasRequestBody = false;
		boolean hasWValidation = false;

		for (Annotation annotation : annotations) {
			if (annotation instanceof RequestBody) {
				hasRequestBody = true;
			}
			if (annotation instanceof WValidation) {
				hasWValidation = true;
			}
		}

		return hasRequestBody && hasWValidation;
	}
    
	private void validateObjectRecursively(Object obj, String prefix, Map<String, String> errors, int depth,
			Set<Object> validatedObjects) {
		if (obj == null || depth > MAX_DEPTH || validatedObjects.contains(obj)) return;

		validatedObjects.add(obj); // 현재 객체를 검증된 목록에 추가
		Field[] fields = obj.getClass().getDeclaredFields();

		for (Field field : fields) {
			field.setAccessible(true);
			String fieldName = prefix.isEmpty() ? field.getName() : prefix + "." + field.getName();
			try {
				Object value = field.get(obj);

				if (value != null) {
					if (isCustomClass(field.getType())) {
						validateObjectRecursively(value, fieldName, errors, depth + 1, validatedObjects);
					} else if (value instanceof Collection<?>) {
						int index = 0;
						for (Object item : (Collection<?>) value) {
							if (item != null && isCustomClass(item.getClass())) {
								validateObjectRecursively(item, fieldName + "[" + index + "]", errors, depth + 1,
										validatedObjects);
							}
							index++;
						}
					}
				}
				validateAnnotations(field, value, fieldName, errors);
			} catch (IllegalAccessException e) {
				errors.put(fieldName, "필드 접근 오류");
			}
		}
	}

	private boolean isCustomClass(Class<?> clazz) {
        if (clazz.isPrimitive()) return false;
        
        String packageName = clazz.getPackage() != null ? clazz.getPackage().getName() : "";
        
        // 기본 타입들 제외
        if (clazz == String.class || 
            clazz == Integer.class || 
            clazz == Long.class || 
            clazz == Double.class || 
            clazz == Float.class || 
            clazz == Boolean.class || 
            clazz == Byte.class || 
            clazz == Character.class) {
            return false;
        }
        
        // 시스템 패키지 제외
        if (packageName.startsWith("java.") || 
            packageName.startsWith("javax.") ||
            packageName.startsWith("org.springframework.")) {
            return false;
        }
        
        // 내부 클래스 제외
        if (clazz.getName().contains("$")) return false;
        
        // 배열 타입 제외
        if (clazz.isArray()) return false;
        
        // 특정 패키지만 허용
        return packageName.startsWith("com.logisall.api");
    }

	private void validateAnnotations(Field field, Object value, String fieldName, Map<String, String> errors) {
		try {
			// NotNullOrEmpty 체크
			if (field.isAnnotationPresent(NotNullOrEmpty.class)) {
				validateNotNullOrEmpty(field, value, errors);
			}
			// Length 체크
			if (field.isAnnotationPresent(WLength.class)) {
				validateLength(field, value, errors);
			}

			// Pattern 체크
			if (field.isAnnotationPresent(WPattern.class)) {
				validatePattern(field, value, errors);
			}

			// Numeric 체크
			if (field.isAnnotationPresent(WNumeric.class)) {
				validateNumeric(field, value, errors);
			}

			// DateFormat 체크
			if (field.isAnnotationPresent(WDateFormat.class)) {
				validateDateFormat(field, value, errors);
			}
		} catch (IllegalAccessException e) {
			errors.put(field.getName(), "필드 접근 오류");
		}

	}

	private void validateNotNullOrEmpty(Field field, Object value, Map<String, String> errors)
			throws IllegalArgumentException, IllegalAccessException {
		NotNullOrEmpty annotation = field.getAnnotation(NotNullOrEmpty.class);
		if (value == null || (value instanceof String && ((String) value).trim().isEmpty())) {
			errors.put(field.getName(), annotation.message());
		}
	}

	private void validateLength(Field field, Object value, Map<String, String> errors) {
		if (value != null) {
			WLength annotation = field.getAnnotation(WLength.class);
			String strValue = value.toString();
			if (strValue.length() < annotation.min() || strValue.length() > annotation.max()) {
				errors.put(field.getName(), annotation.message());
			}
		}
	}

	private void validatePattern(Field field, Object value, Map<String, String> errors) {
		if (value != null) {
			WPattern annotation = field.getAnnotation(WPattern.class);
			if (!value.toString().matches(annotation.regexp())) {
				errors.put(field.getName(), annotation.message());
			}
		}
	}

	private void validateNumeric(Field field, Object value, Map<String, String> errors) {
		if (value != null) {
			WNumeric annotation = field.getAnnotation(WNumeric.class);
			String numericPattern = "^-?[0-9]+(\\.[0-9]+)?$"; // 음수, 소수점 허용
			try {
				if (!value.toString().matches(numericPattern)) {
					errors.put(field.getName(), annotation.message());
				}
			} catch (NumberFormatException e) {
                errors.put(field.getName(), annotation.message());
            }

			
		}
	}

	private void validateDateFormat(Field field, Object value, Map<String, String> errors) {
		if (value != null) {
			WDateFormat annotation = field.getAnnotation(WDateFormat.class);
			try {
				new SimpleDateFormat(annotation.format()).parse(value.toString());
			} catch (ParseException e) {
				errors.put(field.getName(), annotation.message());
			}
		}
	}
	
    private String getOrCreateMaskedFieldName(String originalFieldName) {
        if (!fieldMapping.containsKey(originalFieldName)) {
            fieldCounter++;
            fieldMapping.put(originalFieldName, "field_" + fieldCounter);
        }
        return fieldMapping.get(originalFieldName);
    }
    
}
