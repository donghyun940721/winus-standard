package com.logisall.api.common.response.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.logisall.api.common.response.factory.DefaultErrorResponseFactory;
import com.logisall.api.common.response.factory.ErrorResponseFactory;
import com.logisall.api.common.response.factory.WeverseErrorResponseFactory;

@Component
public class ErrorResponseService {
    @Autowired
    private DefaultErrorResponseFactory defaultFactory;
    
    @Autowired
    private WeverseErrorResponseFactory weverseFactory;
    
    public ErrorResponseFactory getFactory(HttpServletRequest request) {
        String path = request.getRequestURI();
        if (path.startsWith("/api/weverse")) {
            return weverseFactory;
        }
        return defaultFactory;
    }
}
