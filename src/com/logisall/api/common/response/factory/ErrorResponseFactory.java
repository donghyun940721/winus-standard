package com.logisall.api.common.response.factory;

import java.util.Map;

public interface ErrorResponseFactory {
	 Map<String, Object> createErrorResponse(int status, String message);
	 Map<String, Object> createErrorResponse(int status, String message, Object errors);
}
