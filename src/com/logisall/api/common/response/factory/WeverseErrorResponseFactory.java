package com.logisall.api.common.response.factory;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;


/*
 * 위버스는 응답코드를 code로 줘야함..
 * */
@Component
public class WeverseErrorResponseFactory implements ErrorResponseFactory {
    @Override
    public Map<String, Object> createErrorResponse(int code, String message) {
        Map<String, Object> errorResponse = new HashMap<String, Object>();
        errorResponse.put("code", Integer.toString(code));
        errorResponse.put("message", message);
        return errorResponse;
    }

	@Override
	public Map<String, Object> createErrorResponse(int code, String message, Object errors) {
		Map<String, Object> errorResponse = new HashMap<String, Object>();
        errorResponse.put("code", Integer.toString(code));
        errorResponse.put("message", message);
        errorResponse.put("errors", errors);
        return errorResponse;
	}
    
    
}