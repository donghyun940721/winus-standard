package com.logisall.api.common.response.factory;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class DefaultErrorResponseFactory implements ErrorResponseFactory {
    @Override
    public Map<String, Object> createErrorResponse(int status, String message) {
        Map<String, Object> errorResponse = new HashMap<String, Object>();
        errorResponse.put("status", status);
        errorResponse.put("message", message);
        return errorResponse;
    }
    
    @Override
    public Map<String, Object> createErrorResponse(int status, String message, Object errors) {
        Map<String, Object> errorResponse = new HashMap<String, Object>();
        errorResponse.put("status", status);
        errorResponse.put("message", message);
        errorResponse.put("errors", errors);
        return errorResponse;
    }
}
