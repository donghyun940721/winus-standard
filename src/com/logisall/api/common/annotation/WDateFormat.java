package com.logisall.api.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//5. 날짜 형식 체크 어노테이션
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WDateFormat {
 String format() default "yyyy-MM-dd";
 String message() default "날짜 형식이 올바르지 않습니다.";
}
