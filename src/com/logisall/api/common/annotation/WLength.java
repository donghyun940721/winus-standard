package com.logisall.api.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//2. 문자열 길이 체크 어노테이션
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WLength {
	int min() default 0;
    int max() default Integer.MAX_VALUE;
    String message() default "문자열 길이가 올바르지 않습니다.";
}
