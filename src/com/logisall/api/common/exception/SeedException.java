package com.logisall.api.common.exception;

public class SeedException extends RuntimeException {
	public SeedException(String message) {
		super(message);
	}
}