package com.logisall.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.logisall.api.common.aop.DTOValidationAspect;

//2. Spring AOP 설정 추가
@Configuration
@EnableAspectJAutoProxy
public class AopConfig {
 
	@Bean
	public DTOValidationAspect dtoValidationAspect() {
		return new DTOValidationAspect();
	}
}