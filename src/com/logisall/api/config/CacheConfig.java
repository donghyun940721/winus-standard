package com.logisall.api.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.logisall.api.common.auth.dao.AuthKeyDao;

@Configuration
public class CacheConfig {
	private static final Log log = LogFactory.getLog(CacheConfig.class);
	
	@Autowired
	private AuthKeyDao authKeyDao;

	@Bean(name = "authKeyMapBean")
    public Map<String, Map<String, Object>> authKeyMap() {
        log.info("### 인증키 정보 초기화 시작");
        Map<String, Map<String, Object>> authKeyMap = new HashMap<>();
        
        // 모든 유효한 인증키 정보 조회
        List<Map<String, Object>> authKeys = authKeyDao.selectAllAuthKeyInfo();
        // Map으로 변환 (key: AUTH_KEY, value: 전체 정보)
        for (Map<String, Object> authInfo : authKeys) {
            String authKey = (String) authInfo.get("AUTH_KEY");
            authKeyMap.put(authKey, authInfo);
        }
        
        log.info("### 인증키 " + authKeyMap.size() + "개 로딩 완료");
        return authKeyMap;
    }
}
