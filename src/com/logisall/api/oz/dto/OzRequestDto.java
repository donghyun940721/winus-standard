package com.logisall.api.oz.dto;

import java.util.Map;

public class OzRequestDto {
	private String exportPath;
	private String exportFileName;
	private String ozrFilePath;
	private String uploadOrdId;
	private OzFormatType formatType = OzFormatType.pdf;
    private Map<String, String> params;

    // Getters and Setters
    public String getExportPath() {
        return exportPath;
    }

    public void setExportPath(String exportPath) {
        this.exportPath = exportPath;
    }

    public String getExportFileName() {
        return exportFileName;
    }

    public void setExportFileName(String exportFileName) {
        this.exportFileName = exportFileName;
    }

    public String getOzrFilePath() {
        return ozrFilePath;
    }

    public void setOzrFilePath(String ozrFilePath) {
        this.ozrFilePath = ozrFilePath;
    }

    public String getUploadOrdId() {
		return uploadOrdId;
	}

	public void setUploadOrdId(String uploadOrdId) {
		this.uploadOrdId = uploadOrdId;
	}

	public OzFormatType getFormatType() {
        return formatType;
    }

    public void setFormatType(OzFormatType formatType) {
        this.formatType = formatType;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }
}
