package com.logisall.api.oz.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum OzFormatType {
	csv,xls,html,ozd,pdf,txt,tif,doc,ppt,jpg,svg,unknown;
	
	@JsonCreator
    public static OzFormatType fromValue(String value) {
        // 기존 값들과 비교
        for (OzFormatType type : OzFormatType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                return type;
            }
        }
        
        // 해당하지 않는 값이 들어오면 UNKNOWN 처리
        return unknown;
    }
}
