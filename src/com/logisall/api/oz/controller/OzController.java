package com.logisall.api.oz.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logisall.api.controller.BaseController;
import com.logisall.api.lxpantos.dto.ResponseDto;
import com.logisall.api.oz.dto.OzRequestDto;
import com.logisall.api.oz.dto.OzResponseDto;
import com.logisall.api.oz.service.OzService;

@RestController
@RequestMapping("/api/oz")
public class OzController extends BaseController{
	@Autowired
	private OzService service;
	
	/*
	 * 오즈 파일생성
	 * */
	@RequestMapping("/create-file.do")
	public ResponseEntity<OzResponseDto> createFile(Map<String, Object> model,@Validated @RequestBody OzRequestDto reqBody,
			HttpServletRequest request) throws Exception {
		OzResponseDto resBody = service.createFile(reqBody);

		return new ResponseEntity<>(resBody, HttpStatus.OK);
	}
}
