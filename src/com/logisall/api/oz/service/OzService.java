package com.logisall.api.oz.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.logisall.api.oz.dto.OzFormatType;
import com.logisall.api.oz.dto.OzRequestDto;
import com.logisall.api.oz.dto.OzResponseDto;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.tmsbd.service.impl.TMSBD010Dao;

import oz.framework.api.Scheduler;
import oz.scheduler.DirectExportResult;
import oz.scheduler.SchedulerException;
import oz.scheduler.ServerInfo;
import oz.scheduler.TaskResult;
import oz.util.SortProperties;

@Service
public class OzService {
	private final Log log = LogFactory.getLog(this.getClass());
	private static final long POLLING_INTERVAL = 1000; // 1초
    private static final long TIMEOUT = 50000; // 50초
    @Value("${oz.server.ip}")
	private String serverIp;
	@Value("${oz.server.url}")
	private String ozServerUrl;
	@Value("${oz.server.userid}")
	private String serverUserId;
	@Value("${oz.server.userpw}")
	private String serverUserPw;
	@Value("${oz.server.scheduler.port}")
	private int schedulerPort;
//	@Value("${oz.server.upload.path}")
//	private String ozUploadPath;
	@Autowired
	private TMSBD010Dao tmsbd010Dao;

	private ServerInfo setServerInfo() {
		ServerInfo serverInfo = new ServerInfo();
		serverInfo.setURL(ozServerUrl);
		serverInfo.setID(serverUserId); // 오즈 서버 ID
		serverInfo.setPWD(serverUserPw); // 오즈 서버 Password
		return serverInfo;
	}

	private TaskResult[] waitForTaskCompletion(Scheduler scheduler, ServerInfo serverInfo, String taskID) throws SchedulerException {
        long startTime = System.currentTimeMillis();
        String today = DateUtil.getSysDate();

        while (true) {
            // taskResults 조회
            TaskResult[] taskResults = scheduler.getTaskResult(serverInfo, today, today, taskID);

            // 결과가 있으면 반환
            if (taskResults != null && taskResults.length > 0) {
                return taskResults;
            }

            // 타임아웃 체크
            if (System.currentTimeMillis() - startTime > TIMEOUT) {
                throw new RuntimeException("파일 생성 시간이 초과되었습니다.");
            }

            // 폴링 인터벌만큼 대기
            sleep(POLLING_INTERVAL);
        }
    }
	
	private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException("스레드가 인터럽트되었습니다.", e);
        }
    }


	// 태스크 정보 설정
	private SortProperties setServerProperties(Scheduler scheduler, ServerInfo serverInfo) throws SchedulerException {
		SortProperties config = new SortProperties();
		config.setProperty("task_type", "viewerTag"); // 뷰어의 태그를 직접 사용
		config.setProperty("cfg.type", "new"); // Task 의 타입 new, edit
		// 스케쥴 시간한번실행(즉시:"immediately", 주기적:"periodically")
		config.setProperty("launch_type", "immediately");
//		config.setProperty("RepositoryFileRootPath", Paths.get(uploadPath).toString());
		config.setProperty("ErrorNotifyToSender", "true");
//		scheduler.modifyConfiguration(serverInfo, config);
		return config;
	}
	
	// 익스포트 정보 설정(뷰어 태그)
	private SortProperties setExportProperties(ServerInfo serverInfo, OzRequestDto exprotInfo) {
		String formatType = exprotInfo.getFormatType().toString();
		Map<String, String> params = exprotInfo.getParams();
		SortProperties export = new SortProperties();
		
		export.setProperty("connection.servlet", serverInfo.getURL());
		export.setProperty("connection.reportName", exprotInfo.getOzrFilePath());
		export.setProperty("viewer.mode", "export");
		export.setProperty("viewer.useprogressbar", "false");
		export.setProperty("viewer.allowmultiframe", "true");
		
		if(params != null) {
			int paramSize = params.size();
			//파라미터 수량
			export.setProperty("connection.pcount", Integer.toString(paramSize));//파라미터(args 수량)
			//파라미터 세팅
			int i = 1;
	        for (Map.Entry<String, String> entry : params.entrySet()) {
	            if (i > 10) break; // 최대 10개까지만 처리
	            export.setProperty("connection.args" + i, entry.getKey() + "=" + entry.getValue());
	            i++;
	        }
		}
		String uuid = String.format("%08d", (int) (Math.random() * 1000000)); // uuid 6자리
//		export.setProperty("connection.args1", "vrSrchOrdId=0144048606");
		export.setProperty("export.mode", "silent");
		export.setProperty("export.saveonefile", "true"); // 하나의 파일로 익스포트
		export.setProperty("connection.fetchtype", "BATCH");
		export.setProperty("export.confirmsave", "false");
		export.setProperty("information.debug", "debug");
		
		export.setProperty("viewer.showerrormessage", "true");
		// export.setProperty("export.format","csv/xls/html/ozd/pdf/txt/tif/doc/ppt/jpg/svg");
		export.setProperty("export.format", formatType);
		export.setProperty("export_file", "false");
		export.setProperty(formatType+".filename", exprotInfo.getExportFileName()+uuid+"_"+"_$OZ.#D/yyyyMMdd_HHmmss/#D$."+formatType);
		export.setProperty(formatType+".fontembedding", "true");
		export.setProperty(formatType+".fontembedding_subset", "true");
		return export;
	}
	
	private void showDirectExportResult(DirectExportResult t) {
		log.debug("태스크 아이디 :" + t.taskID);
		log.debug("태스크 이름 :" + t.taskName);
		log.debug("태스크 그룹이름 :" + t.taskGroupName);
		log.debug("완료시간 :" + t.completedTime);
		log.debug("성공여부 : " + t.isSuccessful);
		log.debug("보고서 카테고리 이름 : " + t.formCategoryName);
		log.debug("export 파일리스트 : " + t.exportFileList);
		log.debug("");
	}

	private void showDirectExportResult(TaskResult t) {
		log.debug("태스크 아이디 :" + t.taskID);
		log.debug("태스크 이름 :" + t.taskName);
		log.debug("태스크 그룹이름 :" + t.taskGroupName);
		log.debug("완료시간 :" + t.completedTime);
		log.debug("성공여부 : " + t.isSuccessful);
		log.debug("보고서 카테고리 이름 : " + t.formCategoryName);
		log.debug("export 파일리스트 : " + t.exportFileList);
	}

	/**
	 * 오즈파일 생성 로직
	 */
	public OzResponseDto createFile(OzRequestDto reqBody) {
		// 오즈 스케줄러 정보
		OzResponseDto resBody = new OzResponseDto();
		try {
			validRequest(reqBody);
			
			 // 오즈 서버 정보
			ServerInfo serverInfo = setServerInfo();
//			String exportPath = reqBody.getExportPath();
//			String uploadPath = exportPath == null ? 
//					ozUploadPath : ozUploadPath + File.separator + exportPath;
			Scheduler scheduler = new Scheduler(serverIp, schedulerPort);
			SortProperties config = setServerProperties(scheduler, serverInfo);
			SortProperties export = setExportProperties(serverInfo, reqBody);
			// 태스크 생성
			String taskID = scheduler.createTask(serverInfo, config, export);
//			DirectExportResult taskID2 = scheduler.directExport(serverInfo, config, export);
//			showDirectExportResult(taskID2);
			
			// 파일 생성 완료 대기 후 결과 반환
			TaskResult[] taskResults =  waitForTaskCompletion(scheduler, serverInfo, taskID);
			//taskResult 없으면 실패로 간주
			if(taskResults.length == 0) {
				throw new RuntimeException("파일 생성 실패");
			}
			if(!"Succeeded".equals(taskResults[0].isSuccessful)) {
				throw new RuntimeException(taskResults[0].errorMsg);
			}
			showDirectExportResult(taskResults[0]);
			resBody.setTaskId(taskID);
			resBody.setMessage(taskResults[0].isSuccessful);
			resBody.setIsSuccessful(taskResults[0].isSuccessful);
			
			String exportFile = taskResults[0].exportFileList;
			insetFileUploadResult(reqBody, exportFile);
		} catch (SchedulerException | RuntimeException e) {
			log.error(e);
			resBody.setMessage(e.getMessage());
			resBody.setIsSuccessful("Exporting Failed");
		}
		
		return resBody;
	}


	private void insetFileUploadResult(OzRequestDto reqBody, String exportFile) {
		Map<String, Object> fileUploadMap = new HashMap<String, Object>();
		 // 파일명 추출
        String fileName = exportFile.substring(exportFile.lastIndexOf("\\") + 1);
        // 확장자 추출
        String extension = fileName.substring(fileName.lastIndexOf("."));
        // 파일 경로 추출 (파일명을 제외한 부분)
        String filePath = exportFile.substring(exportFile.indexOf("\\OZSCH"), exportFile.lastIndexOf("\\")).replace("\\", "/");

//        String filePath = exportFile.substring(exportFile.indexOf("\\OZSCH"), exportFile.lastIndexOf("\\"));
		fileUploadMap.put("FILE_ID", fileName);
		fileUploadMap.put("ATTACH_GB", "WMS");
		fileUploadMap.put("FILE_VALUE", "OZSCH");
		fileUploadMap.put("FILE_PATH", "E:/ATCH_FILE"+filePath);
		fileUploadMap.put("ORG_FILENAME", fileName);
		fileUploadMap.put("FILE_EXT", extension);
		fileUploadMap.put("FILE_SIZE", 0);
		fileUploadMap.put("WORK_ORD_ID", reqBody.getUploadOrdId());
		fileUploadMap.put("REG_NO", "OZSCH");
		tmsbd010Dao.fileUpload(fileUploadMap);
	}
	
	 // null 체크 로직
    private void validRequest(OzRequestDto requestDto){
        if (requestDto.getExportFileName() == null) {
            throw new IllegalArgumentException("파일명은 필수입니다.");
        }
        if (requestDto.getOzrFilePath() == null) {
            throw new IllegalArgumentException("오즈파일명은 필수입니다.");
        }
        if (requestDto.getUploadOrdId() == null || requestDto.getUploadOrdId().trim().isEmpty()) {
        	throw new IllegalArgumentException("업로드키는 필수입니다.");
        }
        if (requestDto.getFormatType() == null) {
        	requestDto.setFormatType(OzFormatType.pdf);
        }
        if (requestDto.getFormatType().equals(OzFormatType.unknown)) {
        	throw new IllegalArgumentException("지원하지 않는 포맷입니다.");
        }
    }
    
    
}















