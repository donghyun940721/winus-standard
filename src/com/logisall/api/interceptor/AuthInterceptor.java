package com.logisall.api.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.logisall.api.common.auth.service.AuthKeyService;
import com.logisall.api.common.exception.UnAuthorizedException;

public class AuthInterceptor implements HandlerInterceptor {
	private static final String AUTH_HEADER = "Authorization";
//	private static final String AUTH_KEY = "winus-api"; //임시로.. DB화해서 암호화키로 관리해야함
	private final Log log = LogFactory.getLog(this.getClass());
	@Autowired
    private AuthKeyService authKeyService;
	
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        log.info("Request URI: " + request.getRequestURI());
        
        String authKey = request.getHeader(AUTH_HEADER);
        String companyCd = authKeyService.validateAuthKey(authKey);
        
        if (companyCd != null) {
            request.setAttribute("COMPANY_CD", companyCd);
            request.setAttribute("LC_ID", authKeyService.getLcId(authKey));
            request.setAttribute("CUST_ID", authKeyService.getCustId(authKey));
            return true;
        } else {
            throw new UnAuthorizedException("올바르지 않은 인증키입니다.");
        }
    }
	
//	@Override
//	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
//			throws Exception {
//		log.info("Request URI: " + request.getRequestURI());
//		String authKey = request.getHeader(AUTH_HEADER);
//		if (AUTH_KEY.equals(authKey)) {
//			return true;
//		} else {
//			throw new UnAuthorizedException("올바르지 않은 인증키 입니다.");
//		}
//	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// Do nothing
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// Do nothing
	}
}
