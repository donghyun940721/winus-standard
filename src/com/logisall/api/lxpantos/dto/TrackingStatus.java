package com.logisall.api.lxpantos.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TrackingStatus {
	CRT("주문입력(PKR번호 생성)", "-"),
	DTL("국내 송장 번호 업데이트(관리X)", "-"),
	LWHI("출발지 창고입고", "PU"),
	PKU("픽업", "PU"),
	DEP("출발지 도착", "PL"),
	LWHO("출발지 창고출고", "PL"),
	ARR("목적국도착", "PL"),
	DWHI("도착지 창고입고", "PL"),
	DCCC("통관완료", "CR"),
	DWHO("도착지 창고출고", "CR"),
	FST("배송중", "CR"),
	DLI("배송완료", "OK"),
	IREE("주소불명, 부재, 분실 ,수취거부(반송)" ,"RR"),
	UNKNOWN("Unknown", "Unknown"); // 유효하지 않은 값을 처리할 기본 상수

	private String mean;
	private String oliveTrackingStatus;

	TrackingStatus(String mean) {
		this.mean = mean;
	}
	
	TrackingStatus(String mean, String oliveTrackingStatus) {
		this.mean = mean;
		this.oliveTrackingStatus = oliveTrackingStatus;
	}

	public String getMean() {
		return mean;
	}
    public String getOliveTrackingStatus() {
        return oliveTrackingStatus;
    }

    public static String getOliveTrackingStatus(TrackingStatus status) {
        return status.getOliveTrackingStatus();
    }
    
    @JsonCreator
    public static TrackingStatus fromValue(String value) {
        // 기존 값들과 비교
        for (TrackingStatus status : TrackingStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                return status;
            }
        }
        // 코드 값과도 비교
        for (TrackingStatus status : TrackingStatus.values()) {
            if (status.oliveTrackingStatus.equalsIgnoreCase(value)) {
                return status;
            }
        }
        // 해당하지 않는 값이 들어오면 UNKNOWN 처리
        return UNKNOWN;
    }

    @JsonValue
    public String toValue() {
        return name();
    }
}
