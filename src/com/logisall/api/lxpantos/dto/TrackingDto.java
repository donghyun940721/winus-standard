package com.logisall.api.lxpantos.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackingDto {
	private String hblNo;
	private String carrCd;
	private String senderName;
	private String cneeName;
	private String itemName;
	private String cneeAddr;
	private String zipcode;
	private String dliYn;
	private String trnOrdNo;
	private String polCd;
	private String podCd;
	private TrackingStatus statusCd;
	private String oliveStatusCd;
	private List<TrackingDetailDTO> trackingDetails;
	public TrackingDto() {}
	public String getHblNo() {
		return hblNo;
	}
	public void setHblNo(String hblNo) {
		this.hblNo = hblNo;
	}
	public String getCarrCd() {
		return carrCd;
	}
	public void setCarrCd(String carrCd) {
		this.carrCd = carrCd;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getCneeName() {
		return cneeName;
	}
	public void setCneeName(String cneeName) {
		this.cneeName = cneeName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCneeAddr() {
		return cneeAddr;
	}
	public void setCneeAddr(String cneeAddr) {
		this.cneeAddr = cneeAddr;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getDliYn() {
		return dliYn;
	}
	public void setDliYn(String dliYn) {
		this.dliYn = dliYn;
	}
	public String getTrnOrdNo() {
		return trnOrdNo;
	}
	public void setTrnOrdNo(String trnOrdNo) {
		this.trnOrdNo = trnOrdNo;
	}
	public String getPolCd() {
		return polCd;
	}
	public void setPolCd(String polCd) {
		this.polCd = polCd;
	}
	public String getPodCd() {
		return podCd;
	}
	public void setPodCd(String podCd) {
		this.podCd = podCd;
	}
	public String getOliveStatusCd() {
		return oliveStatusCd;
	}
	public void setOliveStatusCd(TrackingStatus statusCd) {
		this.statusCd = statusCd;
        if (statusCd != null) {
            this.oliveStatusCd = statusCd.getOliveTrackingStatus();
        }
	}
	public TrackingStatus getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(TrackingStatus statusCd) {
		this.statusCd = statusCd;
	}
	public List<TrackingDetailDTO> getTrackingDetails() {
		return trackingDetails;
	}
	public void setTrackingDetails(List<TrackingDetailDTO> trackingDetails) {
		this.trackingDetails = trackingDetails;
	}
	
	@Override
	public String toString() {
		return "TrackingDto [hblNo=" + hblNo + ", carrCd=" + carrCd + ", senderName=" + senderName + ", cneeName="
				+ cneeName + ", itemName=" + itemName + ", cneeAddr=" + cneeAddr + ", zipcode=" + zipcode + ", dliYn="
				+ dliYn + ", trnOrdNo=" + trnOrdNo + ", polCd=" + polCd + ", podCd=" + podCd + ", statusCd=" + statusCd
				+ ", trackingDetails=" + trackingDetails + "]";
	}
	
	

}
