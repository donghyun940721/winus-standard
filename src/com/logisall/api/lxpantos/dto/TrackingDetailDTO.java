package com.logisall.api.lxpantos.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackingDetailDTO {
	private String prosPlcNm;
    private String location;
    private String actoNm;
    private TrackingStatus evntCd;
    private String oliveEventCd;
    private String evntDesc;
    private String prosEmpTelNo;
    private String prosPlcTelNo;
    private String evntYmd;
    private String prosEmpName;

    // 기본 생성자
    public TrackingDetailDTO() {}

    // Getter 및 Setter
    public String getProsPlcNm() {
        return prosPlcNm;
    }

    public void setProsPlcNm(String prosPlcNm) {
        this.prosPlcNm = prosPlcNm;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActoNm() {
        return actoNm;
    }

    public void setActoNm(String actoNm) {
        this.actoNm = actoNm;
    }

    public String getOliveEventCd() {
		return oliveEventCd;
	}

	public void setOliveEventCd(TrackingStatus evntCd) {
		this.evntCd = evntCd;
		if (evntCd != null) {
			this.oliveEventCd = evntCd.getOliveTrackingStatus(evntCd);
        }
	}

	public TrackingStatus getEvntCd() {
        return evntCd;
    }

    public void setEvntCd(TrackingStatus evntCd) {
        this.evntCd = evntCd;
    }

    public String getEvntDesc() {
        return evntDesc;
    }

    public void setEvntDesc(String evntDesc) {
        this.evntDesc = evntDesc;
    }

    public String getProsEmpTelNo() {
        return prosEmpTelNo;
    }

    public void setProsEmpTelNo(String prosEmpTelNo) {
        this.prosEmpTelNo = prosEmpTelNo;
    }

    public String getProsPlcTelNo() {
        return prosPlcTelNo;
    }

    public void setProsPlcTelNo(String prosPlcTelNo) {
        this.prosPlcTelNo = prosPlcTelNo;
    }

    public String getEvntYmd() {
        return evntYmd;
    }

    public void setEvntYmd(String evntYmd) {
        this.evntYmd = evntYmd;
    }

    public String getProsEmpName() {
        return prosEmpName;
    }

    public void setProsEmpName(String prosEmpName) {
        this.prosEmpName = prosEmpName;
    }
}
