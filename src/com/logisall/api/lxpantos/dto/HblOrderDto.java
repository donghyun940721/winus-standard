package com.logisall.api.lxpantos.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HblOrderDto {
	private String coNo          ;
    private String result        ;
    private String soNo          ;
    private String hblNo         ;
    private String domTrnNo      ;
    private String domCarrCd     ;
    private String dlvMainClsfCd ;
    private String dlvSubClsfCd  ;
    private String rstrId        ;
    private String errMsg        ;
    
	public String getCoNo() {
		return coNo;
	}
	public void setCoNo(String coNo) {
		this.coNo = coNo;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getSoNo() {
		return soNo;
	}
	public void setSoNo(String soNo) {
		this.soNo = soNo;
	}
	public String getHblNo() {
		return hblNo;
	}
	public void setHblNo(String hblNo) {
		this.hblNo = hblNo;
	}
	public String getDomTrnNo() {
		return domTrnNo;
	}
	public void setDomTrnNo(String domTrnNo) {
		this.domTrnNo = domTrnNo;
	}
	public String getDomCarrCd() {
		return domCarrCd;
	}
	public void setDomCarrCd(String domCarrCd) {
		this.domCarrCd = domCarrCd;
	}
	public String getDlvMainClsfCd() {
		return dlvMainClsfCd;
	}
	public void setDlvMainClsfCd(String dlvMainClsfCd) {
		this.dlvMainClsfCd = dlvMainClsfCd;
	}
	public String getDlvSubClsfCd() {
		return dlvSubClsfCd;
	}
	public void setDlvSubClsfCd(String dlvSubClsfCd) {
		this.dlvSubClsfCd = dlvSubClsfCd;
	}
	public String getRstrId() {
		return rstrId;
	}
	public void setRstrId(String rstrId) {
		this.rstrId = rstrId;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
}
