package com.logisall.api.lxpantos.dto;

public enum ErrorCode {
	SUCCESS("0", "success")
	, ERR_001("ERR_001", "The interface code is not validate.")
	, ERR_002("ERR_002", "The interface Server is not validate.")		
	, ERR_003("ERR_003", "The token is published.")		//신규토큰발행
	, ERR_004("ERR_004", "The token is not validate.")	//검증에러 -> 신규토큰 발행해야함
	, ERR_005("ERR_005", "Unexpected Exception is occurred.");	//비즈니스로직에러
	private final String code;
	private final String description;

	ErrorCode(String code, String description) {
		this.code = code;
		this.description = description;
	}

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
    
    public static ErrorCode fromCode(String code) {
        for (ErrorCode errorCode : values()) {
            if (errorCode.getCode().equals(code)) {
                return errorCode;
            }
        }
        throw new IllegalArgumentException("Unknown error code: " + code);
    }
}
