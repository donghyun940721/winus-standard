package com.logisall.api.lxpantos.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDto {
	public HeaderDto header;
	private JsonNode body;
	
	public HeaderDto getHeader() {
		return header;
	}
	public void setHeader(HeaderDto header) {
		this.header = header;
	}
	public JsonNode getBody() {
		return body;
	}
	public void setBody(JsonNode body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "ResponseDto [header=" + header.toString() + ", body=" + body + "]";
	}

	public class HeaderDto{
		private String mallCd; 
		private String comCd = "KCP";
		private String userId = "kcp_oliveyoung";
		private String result;
		private ErrorCode code;
		private String type;
		private String message;
		private String token;
		
		public String getMallCd() {
			return mallCd;
		}
		public void setMallCd(String mallCd) {
			this.mallCd = mallCd;
		}
		public String getComCd() {
			return comCd;
		}
		public String getUserId() {
			return userId;
		}
		public String getResult() {
			return result;
		}
		public void setResult(String result) {
			this.result = result;
		}
		public ErrorCode getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = ErrorCode.fromCode(code);
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		
		@Override
		public String toString() {
			return "HeaderDto [comCd=" + comCd + ", userId=" + userId + ", result=" + result + ", code=" + code
					+ ", type=" + type + ", message=" + message + ", token=" + token + "]";
		}
	}
	
	public class BodyDTO<T> {
	    private List<T> resultList;

	    // 기본 생성자
//	    public BodyDTO() {}

	    // Getter 및 Setter
	    public List<T> getResultList() {
	        return resultList;
	    }

	    public void setResultList(List<T> resultList) {
	        this.resultList = resultList;
	    }
	}
}
