package com.logisall.api.lxpantos.dto;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class RequestDto {
	private String comCd = "KCP";
	private String encver = "2.0";
	private String msg;
	
	public String getComCd() {
		return comCd;
	}
	public String getEncver() {
		return encver;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/*
	 * formData 형태로 요청보내야 함 
	 * */
	@Override
	public String toString() {
		StringBuilder formData = new StringBuilder();
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				if (formData.length() > 0) {
					formData.append("&");
				}
				formData.append(URLEncoder.encode(field.getName(), StandardCharsets.UTF_8.toString())).append("=")
						.append(URLEncoder.encode(String.valueOf(field.get(this)), StandardCharsets.UTF_8.toString()));
			} catch (IllegalAccessException | UnsupportedEncodingException e) {
				throw new RuntimeException("Failed to convert to form data", e);
			}
		}
		return formData.toString();
	}
}
