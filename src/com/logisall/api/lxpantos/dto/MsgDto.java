package com.logisall.api.lxpantos.dto;

import com.fasterxml.jackson.databind.JsonNode;

public class MsgDto {
	private HeaderDto header; 
	private JsonNode body; //리스트일수도 있고 단건일수도 있음
	
	public HeaderDto getHeader() {
		return header;
	}

	public void setHeader(HeaderDto header) {
		this.header = header;
	}

	public JsonNode getBody() {
		return body;
	}
	public void setBody(JsonNode body) {
		this.body = body;
	}
	
	public class HeaderDto{
		private String mallCd; 
		private String bizPtrId = "KCP"; //고객사ID
		private String callId; //API 호출할 ID명
		private String ifCd = "kcp_oliveyoung"; //판토스 등록된 사용자ID
		private String encType = "UTF-8"; //인코딩
		private String token; //토큰
		

		public void setMallCd(String mallCd) {
			this.mallCd = mallCd;
		}
		public String getMallCd() {
			return mallCd;
		}
		public String getBizPtrId() {
			return bizPtrId;
		}

		public String getCallId() {
			return callId;
		}

		public void setCallId(String callId) {
			this.callId = callId;
		}

		public String getIfCd() {
			return ifCd;
		}

		public String getEncType() {
			return encType;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		@Override
	    public String toString() {
	        return "MsgDto{" +
	                "header=" + header +
	                ", body=" + body +
	                '}';
	    }
	}
}
