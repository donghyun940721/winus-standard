package com.logisall.api.lxpantos.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.api.lxpantos.dao.LxPantosDao;
import com.logisall.api.lxpantos.dto.ErrorCode;
import com.logisall.api.lxpantos.dto.HblOrderDto;
import com.logisall.api.lxpantos.dto.MsgDto;
import com.logisall.api.lxpantos.dto.MsgDto.HeaderDto;
import com.logisall.api.lxpantos.dto.RequestDto;
import com.logisall.api.lxpantos.dto.ResponseDto;
import com.logisall.api.lxpantos.dto.TrackingDetailDTO;
import com.logisall.api.lxpantos.dto.TrackingDto;
import com.logisall.api.lxpantos.dto.TrackingStatus;
import com.logisall.api.lxpantos.util.CustomSeedUtil;
import com.logisall.winus.frm.common.util.HttpUtil;

@Service
public class LxPantosService {
	private final Log log = LogFactory.getLog(this.getClass());

	@Autowired
    private CustomSeedUtil customSeedUtil;
	@Autowired
	private LxPantosDao lxPantosDao;
	
	@Value("${lxpantos.api.url}")
	private String PANTOS_URL;
	@Value("${lxpantos.openmallcd}")
	private String PANTOS_MALL_CD;
	
	/*
	 * 토큰 재발급
	 * */
	public ResponseDto refreshToken(MsgDto msgDto) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		
		//파라미터 아무것도 못받으면 강제로 dto 생성 후 세팅
		HeaderDto reqHeaderDto = msgDto.new HeaderDto();
		if(msgDto.getHeader() == null ) {
			msgDto.setHeader(reqHeaderDto);
		}
		
		//토큰재발급으로 위해 token값을 공백으로 세팅
		msgDto.getHeader().setToken("");
		ResponseDto responseDto = callPantosApi(msgDto, objectMapper);
		responseDto.getHeader().setMallCd(PANTOS_MALL_CD);
		//토큰정보 DB 업데이트
		updateToken(responseDto.getHeader());
		
		return responseDto; 
	}
	
	private void updateToken(ResponseDto.HeaderDto param) {
		lxPantosDao.updateToken(param);
	}

	/*
	 * 오더입력&확정
	 * */
	public ResponseDto proc0009(MsgDto msgDto) throws Exception {
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		ObjectMapper objectMapper = new ObjectMapper();
		
		checkedToken(msgDto);
		
		msgDto.getHeader().setCallId("proc0009");
		ResponseDto responseDto = callPantosApi(msgDto, objectMapper);
		
		//판토스 결과 버그로 에러리턴 있으면 첫번째 로우 중복으로 2개 리턴해줘서 강제로 1행 제거..
		Map<String, Object> hblOrderResultMap = removeReturnFailOrder(objectMapper, responseDto);
		responseDto.setBody(objectMapper.valueToTree(hblOrderResultMap));
		
//		log.info("response: "+ errCnt); 
		log.info("response: "+ responseDto.toString());
		
		return responseDto;
	}

	private Map<String, Object> removeReturnFailOrder(ObjectMapper objectMapper, ResponseDto responseDto)
			throws IOException, JsonParseException, JsonMappingException {
		JsonNode errCntNode = responseDto.getBody().get("errCnt");
		Integer errCnt = errCntNode == null ? null : errCntNode.asInt(); 
		Map<String, Object> hblOrderResultMap = objectMapper.readValue(responseDto.getBody().toString(), new TypeReference<HashMap<String, Object>>() {});
		List<HblOrderDto> hblOrderList = objectMapper.convertValue(hblOrderResultMap.get("resultList"), new TypeReference<List<HblOrderDto>>() {});
		if(errCnt > 0) {
			hblOrderList.remove(0);
		}
		hblOrderResultMap.put("resultList", hblOrderList);
		return hblOrderResultMap;
	}

	
	/*
	 * 토큰검증
	 * */
	private void checkedToken(MsgDto msgDto) throws Exception {
		HeaderDto headerDto = msgDto.getHeader();
		headerDto.setMallCd(PANTOS_MALL_CD);
		Map<String, Object> tokenMap = lxPantosDao.selectToken(headerDto);
		String expiresAt = (String)tokenMap.get("EXPIRES_AT");
		String accessToken = (String)tokenMap.get("ACCESS_TOKEN");
		
		//시스템시간이 토큰만료일보다 크면 토큰 재발급 후 msgDto에 세팅
		if(!isAfterDateWithSystemDate(expiresAt)) {
			ResponseDto tokenResponse = refreshToken(msgDto);
			headerDto.setToken(tokenResponse.getHeader().getToken());
		}else {
			headerDto.setToken(accessToken);
		}
		msgDto.setHeader(headerDto);
	}
	

	public boolean isAfterDateWithSystemDate(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        boolean returnValue = true;
        try {
            Date date = sdf.parse(dateString);
            Calendar currentDate = Calendar.getInstance();
            Calendar comparisonDate = Calendar.getInstance();
            comparisonDate.setTime(date);

            returnValue = comparisonDate.before(currentDate) ? false : true;
        } catch (ParseException e) {
            log.error(e);
            throw new IllegalArgumentException(e);
        }
		return returnValue;
    }

	public ResponseDto track0001(MsgDto msgDto) throws Exception {
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		ObjectMapper objectMapper = new ObjectMapper();
		checkedToken(msgDto);
		
		msgDto.getHeader().setCallId("track0001");
		ResponseDto responseDto = callPantosApi(msgDto, objectMapper);
		
		Map<String, Object> trackingResultMap = removeNoTrackingList(objectMapper, responseDto);
        responseDto.setBody(objectMapper.valueToTree(trackingResultMap));
		
		return responseDto;
	}
	
	
	// 판토스 API 호출
	private ResponseDto callPantosApi(MsgDto msgDto, ObjectMapper objectMapper) throws Exception {
		log.info("PANTOS_URL: "+PANTOS_URL);
	    final int maxRetries = 3;   // 최대 재시도 횟수 설정
	    int retryCount = 0;         // 현재 재시도 횟수

	    while (retryCount < maxRetries) {
	        ResponseDto responseDto = sendApiRequest(msgDto, objectMapper);
	        
	        if (responseDto.getHeader().getCode() != ErrorCode.ERR_004) {
	            return responseDto; // 성공적인 응답일 경우 바로 반환
	        }

	        // 에러 코드 ERR_004일 경우 토큰을 갱신하고 재시도
	        refreshAndRetry(msgDto);
	        retryCount++;
	    }

	    throw new Exception("Maximum retry attempts exceeded for ERR_004");
	}

	// API 요청을 보내는 메서드
	private ResponseDto sendApiRequest(MsgDto msgDto, ObjectMapper objectMapper) throws Exception {
	    RequestDto requestDto = new RequestDto();
	    String msgString = objectMapper.writeValueAsString(msgDto);
	    String reqEncParam = customSeedUtil.encrypt(msgString);
	    requestDto.setMsg(reqEncParam);

	    String response = HttpUtil.sendFormHttpRequest(
	            PANTOS_URL,
	            HttpMethod.POST,
	            requestDto.toString()
	    );

	    String decrypt = customSeedUtil.decrypt(response);
	 // errMsg에서 이중 따옴표가 포함된 경우 이스케이프 처리
	    return objectMapper.readValue(decrypt, ResponseDto.class);
	}

	
	// 토큰을 갱신하고 재시도 설정을 위한 메서드
	private void refreshAndRetry(MsgDto msgDto) throws Exception {
	    ResponseDto tokenResponse = refreshToken(msgDto);
	    msgDto.getHeader().setToken(tokenResponse.getHeader().getToken());
	}
	
	//트래킹정보가 없거나 조회가 안된 결과는 삭제.
	private Map<String, Object> removeNoTrackingList(ObjectMapper objectMapper, ResponseDto responseDto)
			throws IOException {
		JsonNode responseBody = responseDto.getBody();
		Map<String, Object> trackingResultMap = objectMapper.readValue(responseBody.toString(), new TypeReference<HashMap<String, Object>>() {});
		List<TrackingDto> trackingList = objectMapper.convertValue(trackingResultMap.get("resultList"), new TypeReference<List<TrackingDto>>() {});
		Iterator<TrackingDto> iterator = trackingList.iterator();
        while (iterator.hasNext()) {
            TrackingDto tracking = iterator.next();
            List<TrackingDetailDTO> trackingDetails =  tracking.getTrackingDetails();
            for (Iterator<TrackingDetailDTO> detailIterator = trackingDetails.iterator(); detailIterator.hasNext(); ) {
                TrackingDetailDTO trackingDetailDTO = detailIterator.next();
                if (TrackingStatus.DTL.equals(trackingDetailDTO.getEvntCd())) {
                	detailIterator.remove();
                }
            }
            // trackingDetails 리스트에서 evntCd가 "DTL"인 요소를 삭제
//          trackingDetails.removeIf(trackingDetailDTO -> TrackingStatus.DTL.equals(trackingDetailDTO.getEvntCd()));
            
            if (tracking.getHblNo() == null) {
                iterator.remove();
            }
            mappingOliveTrackingStatus(tracking);
        }
        
        trackingResultMap.put("resultList", trackingList);
		return trackingResultMap;
	}

	//올리브영 트래킹 코드로 매핑
	private void mappingOliveTrackingStatus(TrackingDto tracking) {
		TrackingStatus statusCd = tracking.getStatusCd();
		tracking.setOliveStatusCd(statusCd);
		
		List<TrackingDetailDTO> trackingDetails = tracking.getTrackingDetails();
		for (TrackingDetailDTO trackingDetail : trackingDetails) {
			TrackingStatus eventCd = trackingDetail.getEvntCd();
			trackingDetail.setOliveEventCd(eventCd);
		}
	}
		
}
