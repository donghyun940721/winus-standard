package com.logisall.api.lxpantos.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logisall.api.controller.BaseController;
import com.logisall.api.lxpantos.dto.MsgDto;
import com.logisall.api.lxpantos.dto.ResponseDto;
import com.logisall.api.lxpantos.service.LxPantosService;

@RestController
@RequestMapping("/api/lxpantos")
public class LxPantosApiController extends BaseController{
	
	@Autowired
	private LxPantosService service;
    
	/*
	 * 토큰 리프레시
	 * */
	@RequestMapping("/refresh-token.do")
	public ResponseEntity<ResponseDto> refreshToken(Map<String, Object> model, @RequestBody MsgDto msgDto,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ResponseDto result = service.refreshToken(msgDto);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/*
	 * 오더입력&확정
	 * */
	@RequestMapping("/proc0009.do")
	public ResponseEntity<ResponseDto> proc0009(Map<String, Object> model, @RequestBody MsgDto msgDto,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ResponseDto result = service.proc0009(msgDto);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/*
	 * 트레킹정보 조회
	 * */
	@RequestMapping("/track0001.do")
	public ResponseEntity<ResponseDto> track0001(Map<String, Object> model, @RequestBody MsgDto msgDto,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ResponseDto result = service.track0001(msgDto);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	
}
