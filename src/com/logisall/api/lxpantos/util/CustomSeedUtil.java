package com.logisall.api.lxpantos.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.logisall.api.common.exception.SeedException;

import gsi.cm.app.extif.http.seed.SeedUtil;

@Component
public class CustomSeedUtil {
	
	@Value("${lxpantos.seed.key}")
	private String SEED_KEY;

	 /* LX PANTOS 암호화 모듈 
     * 데이터 송신시 json전문 암호화
     * */
    public String encrypt(String jsonData){
    	try {
    		return new SeedUtil().getSeedEncrypt(jsonData, SEED_KEY);
		} catch (Exception e) {
			throw new SeedException("암호화 오류.. 전문확인");
		}
    }

    /* LX PANTOS 복호화 모듈 
     * 데이터 수신시 암호화 전문 복호화
     * */
	public String decrypt(String encryptData) {
		try {
			return new SeedUtil().getSeedDecrypt(encryptData, SEED_KEY) ;
		} catch (Exception e) {
			throw new SeedException("복호화 오류.. 전문확인");
		}
	}
}
