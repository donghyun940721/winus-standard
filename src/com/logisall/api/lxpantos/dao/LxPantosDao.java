package com.logisall.api.lxpantos.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibatis.common.jdbc.exception.RuntimeSQLException;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.api.lxpantos.dto.MsgDto.HeaderDto;
import com.logisall.api.lxpantos.dto.ResponseDto;

@Repository
public class LxPantosDao {

	@Autowired
	private SqlMapClient sqlMapClientWinusIf;
	
	public Map<String,Object> selectToken(HeaderDto param){
		try {
			return (Map<String, Object>) sqlMapClientWinusIf.queryForObject("lxpantos.selectToken", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}
	
	public void updateToken(ResponseDto.HeaderDto param) {
		try {
			sqlMapClientWinusIf.update("lxpantos.updateToken", param);
		} catch (SQLException e) {
			throw new RuntimeSQLException(e);
		}
	}
	
}
