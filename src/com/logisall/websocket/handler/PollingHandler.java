package com.logisall.websocket.handler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.websocket.service.PollingService;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;



public class PollingHandler extends TextWebSocketHandler{
    private static Logger logger = LoggerFactory.getLogger(PollingHandler.class);
    
    @Autowired
    private PollingService service;
    
    @Autowired
    private WMSYS400Service WMSYS400Service;
    

    private ObjectMapper objectMapper = new ObjectMapper();
    
    private List<SessionSeq> sessionList = new ArrayList<SessionSeq>();
    
    private TimerTask task;
    private Timer timer;
    
    class SessionSeq {
        private WebSocketSession session;
        private String seq;
        
        SessionSeq(WebSocketSession session){
            this.session = session;
            this.seq = "";
        }
        
        public void setSeq(String seq){
            this.seq = seq;
        }
        
        public WebSocketSession getSession(){
            return this.session;
        }
        
        public String getSeq(){
            return this.seq;
        }
    }
    
    
    /**
     * 클라이언트 연결 이후에 실행되는 메소드
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessionList.add(new SessionSeq(session));
        logger.info("{} 연결됨", session.getId());
        System.out.println(session.getId()+" 연결됨");
    }
    /**
     * 클라이언트가 웹소켓서버로 메시지를 전송했을 때 실행되는 메소드
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        logger.info("{}로 부터 {} 받음", session.getId(), message.getPayload());
        System.out.println(session.getId()+":"+message.getPayload());
        JSONObject resultObject = new JSONObject();
        
        try{
            Map<String,Object> map = objectMapper.readValue(message.getPayload(), HashMap.class);
            String functionNm = (String) map.get("functionNm");
            if (functionNm.equals("run")){
                
                String serviceNm = (String)map.get("serviceNm");

                if(checkConcurMethod(map, "N")){
                    resultObject.put("O_MSG_CODE", "-1");
                    resultObject.put("O_MSG_NAME", "Error : 이미 실행중입니다.");
                    session.sendMessage(new TextMessage(resultObject.toString()));
                    session.close();
                    return;
                }
                
                String concurcSeq = service.insertConcurMethod(map);
                
                if(concurcSeq.equals("")){
                    resultObject.put("O_MSG_CODE", "-1");
                    resultObject.put("O_MSG_NAME", "Error");
                    session.sendMessage(new TextMessage(resultObject.toString()));
                    session.close();
                    return;
                }
                
                map.put("vrListSeq", concurcSeq);
                
                for (int i = 0 ; i < sessionList.size() ; i++){
                    if(sessionList.get(i).getSession().equals(session)){
                        sessionList.get(i).setSeq(concurcSeq);
                        break;
                    }
                }
                
                switch (serviceNm){
                    case "/WMSOP030/autoBestLocSaveMultiV3.action" : 
                        service.autoBestLocSaveMultiV3(map);
                }
                
                //server단 polling 진행시
                if(task == null){
                    task = new TimerTask(){
                        @Override
                        public void run(){
                            try{
                                JSONObject resultObject = new JSONObject();
                                if (sessionList.size() > 0){
                                    
                                    List<WebSocketSession> closingSessionList = new ArrayList<WebSocketSession>();
                                    
                                    for(SessionSeq sessionSeq : sessionList){
                                        Map<String,Object> m = new HashMap<String,Object>();
                                        m.put("vrListSeq",sessionSeq.getSeq());
                                        
                                        List<Map<String, Object>> concurcInfo = WMSYS400Service.listSeq(m);
                                        
                                        if(concurcInfo.size() > 0){
                                            if(concurcInfo.get(0).get("COMPLETE_YN").equals("Y")){
                                                resultObject.put("O_MSG_CODE", concurcInfo.get(0).get("USER_DEFINED1"));
                                                resultObject.put("O_MSG_NAME", concurcInfo.get(0).get("USER_MESSAGE"));
                                                sessionSeq.getSession().sendMessage(new TextMessage(resultObject.toString()));
                                                closingSessionList.add(sessionSeq.getSession());
                                                //sessionSeq.getSession().close();
                                            }else{
                                                resultObject.put("O_MSG_CODE", "1");
                                                resultObject.put("O_MSG_NAME", "CONTINUE");
                                                sessionSeq.getSession().sendMessage(new TextMessage(resultObject.toString()));
                                            }
                                        }
                                        else {
                                            resultObject.put("O_MSG_CODE", "-1");
                                            resultObject.put("O_MSG_NAME", MessageResolver.getMessage("save.error"));
                                            sessionSeq.getSession().sendMessage(new TextMessage(resultObject.toString()));
                                            closingSessionList.add(sessionSeq.getSession());
                                            //sessionSeq.getSession().close();
                                        }
                                    }
                                    for(WebSocketSession session : closingSessionList){
                                        session.close();
                                    }
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    };
                    
                    timer = new Timer(true);
                    timer.scheduleAtFixedRate(task, 0, 5000);
                }
                
            }
            // client polling 진행시
            else if (functionNm.equals("check")){
                if (sessionList.size() > 0){
                    for (int i = 0 ; i < sessionList.size() ; i++){
                        if(sessionList.get(i).getSession().equals(session)){
                            Map<String,Object> m = new HashMap<String,Object>();
                            m.put("vrListSeq",sessionList.get(i).getSeq());
                            
                            List<Map<String, Object>> concurcInfo = WMSYS400Service.listSeq(m);
                            if(concurcInfo.size() > 0){
                                if(concurcInfo.get(0).get("COMPLETE_YN").equals("Y")){
                                    resultObject.put("O_MSG_CODE", concurcInfo.get(0).get("USER_DEFINED1"));
                                    resultObject.put("O_MSG_NAME", concurcInfo.get(0).get("USER_MESSAGE"));
                                    sessionList.get(i).getSession().sendMessage(new TextMessage(resultObject.toString()));
                                    //sessionList.get(i).getSession().close();
                                }else{
                                    resultObject.put("O_MSG_CODE", "1");
                                    resultObject.put("O_MSG_NAME", "CONTINUE");
                                    sessionList.get(i).getSession().sendMessage(new TextMessage(resultObject.toString()));
                                }
                            }else {
                                resultObject.put("O_MSG_CODE", "-1");
                                resultObject.put("O_MSG_NAME", MessageResolver.getMessage("save.error"));
                                sessionList.get(i).getSession().sendMessage(new TextMessage(resultObject.toString()));
                                //sessionList.get(i).getSession().close();
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("["+session.getId()+"] error : " + message.getPayload() + "exception : " + e.getMessage());
            resultObject.put("O_MSG_CODE", "-1");
            resultObject.put("O_MSG_NAME", "Error : " + e.getMessage());
            session.sendMessage(new TextMessage(resultObject.toString()));
            session.close();
        }
        
        
    }
    /**
     * 클라이언트가 연결을 끊었을 때 실행되는 메소드
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        for (int i = 0 ; i < sessionList.size() ; i++){
            if(sessionList.get(i).getSession().equals(session)){
                sessionList.remove(i);
                break;
            }
        }
        logger.info("{} 연결해제", session.getId());
        System.out.println(session.getId()+" 연결해제");
        
        if(sessionList.size() < 1 && task != null){
            task.cancel();
            task = null;
            timer.purge();
            timer = null;
        }
        
    }

    private boolean checkConcurMethod(Map<String,Object> model, String completeYn) throws Exception{
        Map<String, Object> concurM = new HashMap<String, Object>();
        List<Map<String, Object>> concurcInfo;
        concurM.put("vrServiceNm"       , (String)model.get("serviceNm"));
        concurM.put("vrCompleteYn"      , completeYn);
        concurM.put("vrSeparator1"      , (String)model.get(ConstantIF.SS_SVC_NO)); 
        concurM.put("vrSeparator2"      , (String)model.get(ConstantIF.SS_USER_NO));
        concurM.put("SS_SVC_NO"         ,(String)model.get(ConstantIF.SS_SVC_NO));
        concurM.put("SS_USER_NO"        ,(String)model.get(ConstantIF.SS_USER_NO));
        concurM.put("SS_CLIENT_IP"      ,(String)model.get(ConstantIF.SS_CLIENT_IP));
        
        if(completeYn.equals("Y")){
            concurM.put("vrListSeq"      ,(String)model.get("vrListSeq"));
        }
        
        concurcInfo = WMSYS400Service.listMap(concurM);
        if(concurcInfo.size() > 0){
            return true;
        }
        return false;
    }
    
    
        
}
