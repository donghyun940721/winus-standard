package com.logisall.websocket.handler;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.websocket.service.impl.EdiyaServiceImpl;


public class MonitorHandler extends TextWebSocketHandler{
    private static Logger logger = LoggerFactory.getLogger(MonitorHandler.class);
    
    @Autowired
    private EdiyaServiceImpl ediyaService;
    
    private ObjectMapper objectMapper = new ObjectMapper();
    
    private TimerTask task;
    private Timer timer;
    
    private List<WebSocketSession> sessionList = new ArrayList<WebSocketSession>();
    
    /**
     * 클라이언트 연결 이후에 실행되는 메소드
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        
        sessionList.add(session);
        logger.info("{} 연결됨", session.getId());
    }
    /**
     * 클라이언트가 웹소켓서버로 메시지를 전송했을 때 실행되는 메소드
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

        //logger.info("{}로 부터 {} 받음", session.getId(), message.getPayload());
        
    	//while(sessionList.size() > 0){

        	//디비서 자료 가져오기
        	//시간단위로 던져주기
    	
    	if(task == null){
    		
    		task = new TimerTask(){
    			@Override
    			public void run(){
    				try{
    					List<Object> list = ediyaService.listExtra();
    		    		
    					String result = objectMapper.writeValueAsString(list);
    					
    					for(WebSocketSession sess : sessionList){
    			        	sess.sendMessage(new TextMessage(result));
    			        }
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    			}
    		};
    		
    		timer = new Timer(true);
    		timer.scheduleAtFixedRate(task, 0, 30000);
    		
    	}
    		
	        //Thread.sleep(5000);
    	//}
                
    }
    /**
     * 클라이언트가 연결을 끊었을 때 실행되는 메소드
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessionList.remove(session);
        logger.info("{} 연결해제", session.getId());
        if(sessionList.size() < 1){
        	task.cancel();
        	task = null;
        	timer.purge();
        	timer = null;
        }
    }
    
    
        
}
