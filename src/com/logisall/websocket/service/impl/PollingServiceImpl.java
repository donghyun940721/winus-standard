package com.logisall.websocket.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.websocket.service.PollingService;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.logisall.winus.wmsys.service.WMSYS400Service;
import com.logisall.winus.wmsys.service.impl.WMSYS400Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("PollingService")
public class PollingServiceImpl implements PollingService{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSOP030Service")
    private WMSOP030Service WMSOP030Service;
    
    @Resource(name = "WMSYS400Service")
    private WMSYS400Service WMSYS400Service;
    
    @Resource(name = "WMSYS400Dao")
    private WMSYS400Dao WMSYS400Dao;
    
    @Resource(name = "WMSOP030Dao")
    private WMSOP030Dao WMSOP030Dao;
    

    @Async
    @Override
    public void autoBestLocSaveMultiV3(Map<String, Object> model) throws Exception{
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            //동시성 제어 https://velog.io/@ieejo716/Spring-%EB%B9%84%EB%8F%99%EA%B8%B0-%EC%B2%98%EB%A6%AC
            //기준 : 센터, 작업자NO
            Map<String, Object> concurM = new HashMap<String, Object>();
            String concurcSeq = (String) model.get("vrListSeq");
            concurM.put("vrServiceNm"       , (String) model.get("serviceNm"));
            concurM.put("vrCompleteYn"      , "N");
            concurM.put("vrSeparator1"      , (String)model.get(ConstantIF.SS_SVC_NO)); 
            concurM.put("vrSeparator2"      , (String)model.get(ConstantIF.SS_USER_NO));
            concurM.put("SS_SVC_NO"         ,(String)model.get(ConstantIF.SS_SVC_NO));
            concurM.put("SS_USER_NO"        ,(String)model.get(ConstantIF.SS_USER_NO));
            concurM.put("SS_CLIENT_IP"      ,(String)model.get(ConstantIF.SS_CLIENT_IP));
            
            try{
                List list = WMSOP030Dao.rawListByCustSummary(model);
                int tmpCnt = list.size();
                if(tmpCnt > 0){
                    String[] ordId   = new String[tmpCnt];
                    
                    for(int i = 0 ; i < tmpCnt ; i ++){
                        
                        ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                    }
                    //프로시져에 보낼것들 다담는다
                    Map<String, Object> modelIns = new HashMap<String, Object>();
                    
                    modelIns.put("ordId" , ordId);

                    //session 및 등록정보
                    modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                    //dao                
                    modelIns = (Map<String, Object>)WMSOP030Dao.autoBestLocSaveMultiV2(modelIns);
                    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                    m.put("MSG", (String)modelIns.get("O_MSG_NAME"));
                    m.put("errCnt", String.valueOf(modelIns.get("O_MSG_CODE")));
                    //Thread.sleep(60000);
                }
                //m = WMSOP030Service.autoBestLocSaveMultiV3(model);
                
                model.put("USER_MESSAGE", "save.success");
                model.put("USER_DEFINED1", "0");
            }catch(BizException be) {
                model.put("USER_MESSAGE", be.getMessage());
                model.put("USER_DEFINED1","-1");
            }catch (Exception e){
                if (log.isErrorEnabled()) {
                    log.error("Fail to save :", e);
                }
                model.put("USER_MESSAGE", "save.error");
                model.put("USER_DEFINED1","-1");
            }
            
            model.put("vrSeq", concurcSeq);
            WMSYS400Dao.complete(model);
            
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to save :", e);
            }
        }
    }
    
    @Override
    public String insertConcurMethod(Map<String,Object> model) throws Exception{
        Map<String, Object> concurM = new HashMap<String, Object>();
        
        String concurcSeq = "";
        concurM.put("vrServiceNm"       , (String)model.get("serviceNm"));
        concurM.put("vrCompleteYn"      , "N");
        concurM.put("vrSeparator1"      , (String)model.get(ConstantIF.SS_SVC_NO)); 
        concurM.put("vrSeparator2"      , (String)model.get(ConstantIF.SS_USER_NO));
        concurM.put("SS_SVC_NO"         ,(String)model.get(ConstantIF.SS_SVC_NO));
        concurM.put("SS_USER_NO"        ,(String)model.get(ConstantIF.SS_USER_NO));
        concurM.put("SS_CLIENT_IP"      ,(String)model.get(ConstantIF.SS_CLIENT_IP));
        
        concurcSeq = ((Map<String, Object>)WMSYS400Dao.getSequence(model)).get("SEQ").toString();
        
        concurM.put("vrConcurcSeq"    , concurcSeq);
        WMSYS400Dao.save(concurM);
        
        return concurcSeq;
    }
    
}
