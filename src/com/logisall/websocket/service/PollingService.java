package com.logisall.websocket.service;

import java.util.List;
import java.util.Map;


public interface PollingService {
    public void autoBestLocSaveMultiV3(Map<String, Object> model) throws Exception;
    public String insertConcurMethod(Map<String,Object> model) throws Exception;
}
