package com.logisall.websocket.service;

import java.util.List;
import java.util.Map;

public interface EdiyaService {
	public Map<String, Object> listExtra(Map<String, Object> model) throws Exception;
	public List<Object> listExtra() throws Exception;
}
