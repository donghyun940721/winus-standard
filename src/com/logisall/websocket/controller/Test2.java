package com.logisall.websocket.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONTokener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.logisall.winus.frm.common.util.OliveAes256;

public class Test2 {
	public static void main(String[] args) {
		try{
			excel2Csv("C:\\Users\\LogisALL\\Downloads\\2021426113637_출고주문데이터_210426통합_EMS.xls");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public static void excel2Csv(String file_path) {

	    // Variables!
	    String jsonInString = "";
	    int active_sheet_index = 0;

	    // Initializing the parent and the children list!

	    // Initializing the excel variables!        
	    Sheet sheet = null;     
	    Workbook workbook = null;
	    BufferedInputStream inp = null;

	    // Initializing the JSON Mapper variables!
	    ObjectMapper mapper = null;

	    try {
	        // Reading the excel input for conversion!
	        inp =  new BufferedInputStream(new FileInputStream(new File(file_path)));
	        
	        //ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP020E8, 0, startRow, 10000, 0);
	        
	        // Creating the excel workbook object!
	        workbook = WorkbookFactory.create(inp);     

	        // Get the first sheet!
	        sheet = workbook.getSheetAt(active_sheet_index);
	        Iterator<Row> iterator = sheet.iterator();
	        iterator.next();
	        List<Order> orders = new ArrayList<>();
	        int i = 0;
	        StringBuilder builder = new StringBuilder();
	        while (iterator.hasNext()) {
	            Order order = new Order();

	            // Iterating through the excel rows!
	            Row row = iterator.next();
	            System.out.println(i++);
	            System.out.println(row.getCell(1).getStringCellValue());
	            // If Cell0 of the active row is blank or null!
            	order = new Order();               
            	order.setOrgOrdId(row.getCell(1).getStringCellValue());
            	order.setOrgOrdSeq(Integer.parseInt(row.getCell(2).getStringCellValue().split("-")[2]) + "");
                order.setOrdQty(((int)row.getCell(7).getNumericCellValue()) + "");
                order.setTot(row.getCell(8).getNumericCellValue() + "");
                order.setdCustNm(row.getCell(9).getStringCellValue());
   //             order.setdPost(OliveAes256.encrypt(row.getCell(11).getStringCellValue())); //암호화  필요
                order.setdPost(OliveAes256.encrypt(row.getCell(11).toString().replace(".0", "").replaceAll("\"", ""))); //암호화  필요
                System.out.println(row.getCell(11).toString().replace(".0", ""));
                System.out.println(OliveAes256.encrypt(row.getCell(11).toString().replace(".0", "")));
                order.setdAddress(OliveAes256.encrypt(row.getCell(12).getStringCellValue())); //암호화  필요
                order.setdTel(OliveAes256.encrypt(row.getCell(14).getStringCellValue())); //암호화  필요
                order.setdCity(row.getCell(17).getStringCellValue()); 
                order.setdCountry(row.getCell(18).getStringCellValue()); 
                order.setdAddress2("");

                order.setjCustNm("OLIVEYOUNG");
                order.setjPost(OliveAes256.encrypt("16800")); //암호화  필요
                order.setjAddress(OliveAes256.encrypt("60, Baekbong-ro 295beon-gil")); //암호화  필요
                order.setjAddress2(OliveAes256.encrypt("Baegam-myeon Cheoin-gu, Yongin-si"));
              //  order.setjAddress2(OliveAes256.encrypt("Baegam-myeon Yongin-si"));
                order.setjCity("Yongin");
                order.setjTel(OliveAes256.encrypt("8215998962")); //암호화  필요
                
                order.setItemNm(row.getCell(6).getStringCellValue());
                
                order.setHsCode(row.getCell(19).toString().trim()); 
                //System.out.println(row.getCell(6).getStringCellValue());
                order.setItemCd(row.getCell(20).toString() + "");
                order.setDlvDt("20210426"); 
                order.setItemType("10");
                order.setCategoryNo("02");
                order.setReSndYn("N");
            	order.setDataSenderNm("OY");
            	
            	order.setShippingCompany("10");   // 10:ems 20:dhl
            	orders.add(order);
            	
            	//builder.append(" UPDATE WMSOM010 SET ORD_DESC='" + row.getCell(5).getStringCellValue().replace("'", "''") + "' WHERE ORG_ORD_NO = '" + row.getCell(1).getStringCellValue() + "' AND ORD_DETAIL_NO = '" + (Integer.parseInt(row.getCell(2).getStringCellValue().split("-")[2]) + "") + "';");
            	
	        }

	        mapper = new ObjectMapper();
	        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

	        jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(orders);
	        System.out.println("Final Json= \n" + jsonInString);

	        // Convert object to JSON string and save into file directly
	        mapper.writeValue(new File("C:\\Users\\LogisALL\\Downloads\\querytest3.txt"), new JSONTokener(jsonInString).nextValue().toString());
	        //mapper.writeValue(new File("C:\\Users\\LogisALL\\Downloads\\query.txt"),  builder.toString());
	    } catch(Exception ex) {
	        ex.printStackTrace();
	    }
	}
	
	public static class Order {
	    private String orgOrdId;
	    private String orgOrdSeq;
	    private String dlvDt;
	    private String dCountry;
	    private String dCustNm;
	    private String dPost;
	    private String dCity;
	    private String dAddress;
	    private String dAddress2;
	    private String dTel;
	    private String jCustNm;
	    private String jTel;
	    private String jPost;
	    private String jAddress;
	    private String jAddress2;
	    private String jCity;
	    private String itemCd;
	    private String tot;
	    private String itemType;
	    private String ordQty;
	    private String shippingCompany;
	    private String hsCode;
	    private String categoryNo;
	    private String reSndYn;
	    private String dataSenderNm;
	    private String itemNm;
		public String getOrgOrdId() {
			return orgOrdId;
		}
		public void setOrgOrdId(String orgOrdId) {
			this.orgOrdId = orgOrdId;
		}
		public String getOrgOrdSeq() {
			return orgOrdSeq;
		}
		public void setOrgOrdSeq(String orgOrdSeq) {
			this.orgOrdSeq = orgOrdSeq;
		}
		public String getDlvDt() {
			return dlvDt;
		}
		public void setDlvDt(String dlvDt) {
			this.dlvDt = dlvDt;
		}
		public String getdCountry() {
			return dCountry;
		}
		public void setdCountry(String dCountry) {
			this.dCountry = dCountry;
		}
		public String getdCustNm() {
			return dCustNm;
		}
		public void setdCustNm(String dCustNm) {
			this.dCustNm = dCustNm;
		}
		public String getdPost() {
			return dPost;
		}
		public void setdPost(String dPost) {
			this.dPost = dPost;
		}
		public String getdCity() {
			return dCity;
		}
		public void setdCity(String dCity) {
			this.dCity = dCity;
		}
		public String getdAddress() {
			return dAddress;
		}
		public void setdAddress(String dAddress) {
			this.dAddress = dAddress;
		}
		public String getdAddress2() {
			return dAddress2;
		}
		public void setdAddress2(String dAddress2) {
			this.dAddress2 = dAddress2;
		}
		public String getdTel() {
			return dTel;
		}
		public void setdTel(String dTel) {
			this.dTel = dTel;
		}
		public String getjCustNm() {
			return jCustNm;
		}
		public void setjCustNm(String jCustNm) {
			this.jCustNm = jCustNm;
		}
		public String getjTel() {
			return jTel;
		}
		public void setjTel(String jTel) {
			this.jTel = jTel;
		}
		public String getjPost() {
			return jPost;
		}
		public void setjPost(String jPost) {
			this.jPost = jPost;
		}
		public String getjAddress() {
			return jAddress;
		}
		public void setjAddress(String jAddress) {
			this.jAddress = jAddress;
		}
		public String getjAddress2() {
			return jAddress2;
		}
		public void setjAddress2(String jAddress2) {
			this.jAddress2 = jAddress2;
		}
		public String getItemCd() {
			return itemCd;
		}
		public void setItemCd(String itemCd) {
			this.itemCd = itemCd;
		}
		public String getTot() {
			return tot;
		}
		public void setTot(String tot) {
			this.tot = tot;
		}
		public String getItemType() {
			return itemType;
		}
		public void setItemType(String itemType) {
			this.itemType = itemType;
		}
		public String getOrdQty() {
			return ordQty;
		}
		public void setOrdQty(String ordQty) {
			this.ordQty = ordQty;
		}
		public String getShippingCompany() {
			return shippingCompany;
		}
		public void setShippingCompany(String shippingCompany) {
			this.shippingCompany = shippingCompany;
		}
		public String getHsCode() {
			return hsCode;
		}
		public void setHsCode(String hsCode) {
			this.hsCode = hsCode;
		}
		public String getCategoryNo() {
			return categoryNo;
		}
		public void setCategoryNo(String categoryNo) {
			this.categoryNo = categoryNo;
		}
		public String getReSndYn() {
			return reSndYn;
		}
		public void setReSndYn(String reSndYn) {
			this.reSndYn = reSndYn;
		}
		public String getDataSenderNm() {
			return dataSenderNm;
		}
		public void setDataSenderNm(String dataSenderNm) {
			this.dataSenderNm = dataSenderNm;
		}
		public String getItemNm() {
			return itemNm;
		}
		public void setItemNm(String itemNm) {
			this.itemNm = itemNm;
		}
		public String getjCity() {
			return jCity;
		}
		public void setjCity(String jCity) {
			this.jCity = jCity;
		}

	    


	}
	
}
