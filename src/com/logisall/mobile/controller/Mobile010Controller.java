package com.logisall.mobile.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.logisall.winus.wmsif.service.WMSIF601Service;
import com.logisall.winus.wmsop.service.WMSOP035Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class Mobile010Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP035Service")
    private WMSOP035Service wmsop035Service;
    
    @Resource(name = "TMSYS030Service")
	private TMSYS030Service service;
    
    @Resource(name = "WMSIF000Service")
    private WMSIF000Service wmsif000Service;
    
    @Resource(name = "WMSIF601Service")
    private WMSIF601Service wmsif601service;
    
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/tracking.mob")
    public ModelAndView wmsif202(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/mobile/index");
    }

    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/equip.mob")
    public ModelAndView equip(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF203A");
    }

    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/join.mob")
    public @ResponseBody Map<String, Object> join(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.insertediya(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		return m;
    }
    
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/test.mob")
    public @ResponseBody Map<String, Object> test(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Random rnd = new Random();
		m.put("ERROR", rnd.nextInt());
		return m;
    }
    
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/decryptOliveData.mob")
    public @ResponseBody Map<String, Object> decryptOliveData(Map<String, Object> model) throws Exception {
		Map<String, Object> m = wmsop035Service.emsAddress(model);
		return m;
    }

    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/sabangnetOrderHeader3.mob")
    public ResponseEntity<org.springframework.core.io.Resource> sabangnetOrderHeader3(Map<String, Object> model) throws Exception {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.valueOf("image/svg+xml"));
    	String xml = wmsif000Service.crossDomainSabangOrderHead(model);
    	org.springframework.core.io.Resource resource = new InputStreamResource(new ByteArrayInputStream(xml.getBytes("UTF-8")));
    	//System.out.println(xml);
    	ResponseEntity<org.springframework.core.io.Resource> svg = new ResponseEntity<>(resource, headers, HttpStatus.OK); 
    	return svg;
    	
    }
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetOrderHeader2.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetOrderHeader2(Map<String, Object> model, HttpServletResponse response) throws Exception {
    	String xml = wmsif000Service.crossDomainSabangOrderHead(model);
    	try(InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"))) {
    		IOUtils.copy(is, response.getOutputStream());
    		response.flushBuffer();
    	}catch(Exception e){
    		log.error(e.getMessage());
    	}
    }
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetOrderHeader.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetOrderHeader(Map<String, Object> model, HttpServletResponse response) throws Exception {
    	//String xml = wmsif000Service.crossDomainSabangOrderHead(model);
    	
    	//주문일자 셋팅
    	String orderDate;
    	String pattern = "yyyyMMdd";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String date = simpleDateFormat.format(new Date());
    	orderDate = date;
    	
    	//사방넷 검색시작일자 ORD_ST_DATE 파라미터 셋팅
    	Object ordStDateObj = model.get("ordStDate");
    	String ordStDate;
    	if(ordStDateObj != null){
    		ordStDate = (String)ordStDateObj;
    	}else{
    		ordStDate = "";
    	}
    	
    	//사방넷 검색마지막일자 ORD_ED_DATE 파라미터 셋팅
    	Object ordEdDateObj = model.get("ordEdDate");
    	String ordEdDate;
    	if(ordEdDateObj != null){
    		ordEdDate = (String)ordEdDateObj;
    	}else{
    		ordEdDate = "";
    	}
    	
    	//사방넷 주문상태 ORDER_STATUS 파라미터 셋팅
    	Object orderStatusObj = model.get("orderStatus");
    	String orderStatus;
    	if(orderStatusObj != null){
    		orderStatus = (String)orderStatusObj;
    	}else{
    		orderStatus = "";
    	}
    	
    	//사방넷 ORDER_STATUS 파라미터 셋팅
    	Object sendCompanyIdObj = model.get("sendCompanyId");
    	String sendCompanyId;
    	if(sendCompanyIdObj != null){
    		sendCompanyId = (String)sendCompanyIdObj;
    	}else{
    		sendCompanyId = "";
    	}
    	
    	//사방넷 인증키 파라미터 셋팅
    	Object sendAuthKeyObj = model.get("sendAuthKey");
    	String sendAuthKey;
    	if(sendAuthKeyObj != null){
    		sendAuthKey = (String)sendAuthKeyObj;
    	}else{
    		sendAuthKey = "";
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	String[] fieldList = {
    			"IDX",
    			"ORDER_ID",
    			"MALL_ID",
    			"MALL_USER_ID",
    			"MALL_USER_ID2",
    			"ORDER_STATUS",
    			"USER_ID",
    			"USER_NAME",
    			"USER_TEL",
    			"USER_CEL",
    			"USER_EMAIL",
    			"RECEIVE_TEL",
    			"RECEIVE_CEL",
    			"RECEIVE_EMAIL",
    			"DELV_MSG",
    			"RECEIVE_NAME",
    			"RECEIVE_ZIPCODE",
    			"RECEIVE_ADDR",
    			"TOTAL_COST",
    			"PAY_COST",
    			"ORDER_DATE",
    			"PARTNER_ID",
    			"DPARTNER_ID",
    			"MALL_PRODUCT_ID",
    			"PRODUCT_ID",
    			"SKU_ID",
    			"P_PRODUCT_NAME",
    			"P_SKU_VALUE",
    			"PRODUCT_NAME",
    			"SALE_COST",
    			"MALL_WON_COST",
    			"WON_COST",
    			"SKU_VALUE",
    			"SALE_CNT",
    			"DELIVERY_METHOD_STR",
    			"DELV_COST",
    			"COMPAYNY_GOODS_CD",
    			"SKU_ALIAS",
    			"BOX_EA",
    			"JUNG_CHK_YN",
    			"MALL_ORDER_SEQ",
    			"MALL_ORDER_ID",
    			"ETC_FIELD3",
    			"ORDER_GUBUN",
    			"P_EA",
    			"REG_DATE",
    			"ORDER_ETC_1",
    			"ORDER_ETC_2",
    			"ORDER_ETC_3",
    			"ORDER_ETC_4",
    			"ORDER_ETC_5",
    			"ORDER_ETC_6",
    			"ORDER_ETC_7",
    			"ORDER_ETC_8",
    			"ORDER_ETC_9",
    			"ORDER_ETC_10",
    			"ORDER_ETC_11",
    			"ORDER_ETC_12",
    			"ORDER_ETC_13",
    			"ORDER_ETC_14",
    			"ord_field2",
    			"copy_idx",
    			"GOODS_NM_PR",
    			"GOODS_KEYWORD",
    			"ORD_CONFIRM_DATE",
    			"RTN_DT",
    			"CHNG_DT",
    			"DELIVERY_CONFIRM_DATE",
    			"CANCEL_DT",
    			"CLASS_CD1",
    			"CLASS_CD2",
    			"CLASS_CD3",
    			"CLASS_CD4",
    			"BRAND_NM",
    			"DELIVERY_ID",
    			"INVOICE_NO",
    			"HOPE_DELV_DATE",
    			"FLD_DSP",
    			"INV_SEND_MSG",
    			"MODEL_NO",
    			"SET_GUBUN",
    			"ETC_MSG",
    			"DELV_MSG1",
    			"MUL_DELV_MSG",
    			"BARCODE",
    			"INV_SEND_DM",
    			"DELIVERY_METHOD_STR2",
    			"FREE_GIFT"
    			};
    	String fieldListString = StringUtils.join(fieldList, "|");
    	
    	sb.append("<?xml version=\"1.0\" encoding=\"EUC-KR\"?>"
    			+ "<SABANG_ORDER_LIST>"
    			+ "<HEADER>"
    			+ "<SEND_COMPAYNY_ID>"
    			+ sendCompanyId
    			+ "</SEND_COMPAYNY_ID>"
    			+ "<SEND_AUTH_KEY>"
    			+ sendAuthKey
    			+ "</SEND_AUTH_KEY>"
    			+ "<SEND_DATE>"
    			+ orderDate
    			+ "</SEND_DATE>"
    			+ "</HEADER>"
    			
    			+ "<DATA>"
    			+ "<ORD_ST_DATE>"
    			+ ordStDate
    			+ "</ORD_ST_DATE>"
    			+ "<ORD_ED_DATE>"
    			+ ordEdDate
    			+ "</ORD_ED_DATE>"
    			+ "<ORD_FIELD>"
    			+ "<![CDATA[" + fieldListString + "]]>"
    			+ "</ORD_FIELD>"
    			+ "<ORDER_STATUS>"
				+ orderStatus
				+ "</ORDER_STATUS>"
				+ "</DATA>"
    			+ "</SABANG_ORDER_LIST>");
    	
    	String xml = sb.toString();
    	BufferedOutputStream bos = null;
    	InputStream fin = null;
		
		try{
			byte[] xmlByte = xml.getBytes("UTF-8");
			fin = new ByteArrayInputStream(xmlByte);
			
			response.reset();
			//response.setHeader("Content-Disposition", "attachment;filename="
			//		+ URLEncoder.encode("ORDER_HEADER.xml", "UTF-8") + ";");
			response.setHeader("Content-Transfer-Encoding","binary;");
			response.setHeader("Pragma","no-cache;");
			response.setHeader("Expires","-1;");
			response.setContentLength(xmlByte.length);
			response.setContentType("application/xml;charset=UTF-8");
			
			bos = new BufferedOutputStream(response.getOutputStream());
			fin.read(xmlByte);
			bos.write(xmlByte,0,xmlByte.length);
			bos.flush();
			
		}catch(Exception e){
			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
				//e.printStackTrace();
				log.error("download fail");
			}
		}finally{
			if (bos != null) {
				try {
					bos.close();
				} catch (Exception ie) {
				}
			}
			if (fin != null) {
				try {
					fin.close();
				} catch (Exception ise) {
				}
			}
		}
    }
    
    
    /**
     * Method ID	: sabangnetOrderHeader_V2
     * Method 설명	: 사방넷 호출 XML .. V2 ( 매입처명 )
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetOrderHeader_V2.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetOrderHeader_V2(Map<String, Object> model, HttpServletResponse response) throws Exception {
    	//주문일자 셋팅
    	String orderDate;
    	String pattern = "yyyyMMdd";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String date = simpleDateFormat.format(new Date());
    	orderDate = date;
    	
    	//사방넷 검색시작일자 ORD_ST_DATE 파라미터 셋팅
    	Object ordStDateObj = model.get("ordStDate");
    	String ordStDate;
    	if(ordStDateObj != null){
    		ordStDate = (String)ordStDateObj;
    	}else{
    		ordStDate = "";
    	}
    	
    	//사방넷 검색마지막일자 ORD_ED_DATE 파라미터 셋팅
    	Object ordEdDateObj = model.get("ordEdDate");
    	String ordEdDate;
    	if(ordEdDateObj != null){
    		ordEdDate = (String)ordEdDateObj;
    	}else{
    		ordEdDate = "";
    	}
    	
    	//사방넷 주문상태 ORDER_STATUS 파라미터 셋팅
    	Object orderStatusObj = model.get("orderStatus");
    	String orderStatus;
    	if(orderStatusObj != null){
    		orderStatus = (String)orderStatusObj;
    	}else{
    		orderStatus = "";
    	}
    	
    	//사방넷 ORDER_STATUS 파라미터 셋팅
    	Object sendCompanyIdObj = model.get("sendCompanyId");
    	String sendCompanyId;
    	if(sendCompanyIdObj != null){
    		sendCompanyId = (String)sendCompanyIdObj;
    	}else{
    		sendCompanyId = "";
    	}
    	
    	//사방넷 인증키 파라미터 셋팅
    	Object sendAuthKeyObj = model.get("sendAuthKey");
    	String sendAuthKey;
    	if(sendAuthKeyObj != null){
    		sendAuthKey = (String)sendAuthKeyObj;
    	}else{
    		sendAuthKey = "";
    	}
    	
    	//사방넷 매입처 ID 파라미터 셋팅
    	Object sendPartnerIdObj = model.get("sendPartnerId");
    	String sendPartnerId;
    	if(sendPartnerIdObj != null){
    		sendPartnerId = (String)sendPartnerIdObj;
    	}else{
    		sendPartnerId = "";
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	String[] fieldList = {
    			"IDX",
    			"ORDER_ID",
    			"MALL_ID",
    			"MALL_USER_ID",
    			"MALL_USER_ID2",
    			"ORDER_STATUS",
    			"USER_ID",
    			"USER_NAME",
    			"USER_TEL",
    			"USER_CEL",
    			"USER_EMAIL",
    			"RECEIVE_TEL",
    			"RECEIVE_CEL",
    			"RECEIVE_EMAIL",
    			"DELV_MSG",
    			"RECEIVE_NAME",
    			"RECEIVE_ZIPCODE",
    			"RECEIVE_ADDR",
    			"TOTAL_COST",
    			"PAY_COST",
    			"ORDER_DATE",
    			"PARTNER_ID",
    			"DPARTNER_ID",
    			"MALL_PRODUCT_ID",
    			"PRODUCT_ID",
    			"SKU_ID",
    			"P_PRODUCT_NAME",
    			"P_SKU_VALUE",
    			"PRODUCT_NAME",
    			"SALE_COST",
    			"MALL_WON_COST",
    			"WON_COST",
    			"SKU_VALUE",
    			"SALE_CNT",
    			"DELIVERY_METHOD_STR",
    			"DELV_COST",
    			"COMPAYNY_GOODS_CD",
    			"SKU_ALIAS",
    			"BOX_EA",
    			"JUNG_CHK_YN",
    			"MALL_ORDER_SEQ",
    			"MALL_ORDER_ID",
    			"ETC_FIELD3",
    			"ORDER_GUBUN",
    			"P_EA",
    			"REG_DATE",
    			"ORDER_ETC_1",
    			"ORDER_ETC_2",
    			"ORDER_ETC_3",
    			"ORDER_ETC_4",
    			"ORDER_ETC_5",
    			"ORDER_ETC_6",
    			"ORDER_ETC_7",
    			"ORDER_ETC_8",
    			"ORDER_ETC_9",
    			"ORDER_ETC_10",
    			"ORDER_ETC_11",
    			"ORDER_ETC_12",
    			"ORDER_ETC_13",
    			"ORDER_ETC_14",
    			"ord_field2",
    			"copy_idx",
    			"GOODS_NM_PR",
    			"GOODS_KEYWORD",
    			"ORD_CONFIRM_DATE",
    			"RTN_DT",
    			"CHNG_DT",
    			"DELIVERY_CONFIRM_DATE",
    			"CANCEL_DT",
    			"CLASS_CD1",
    			"CLASS_CD2",
    			"CLASS_CD3",
    			"CLASS_CD4",
    			"BRAND_NM",
    			"DELIVERY_ID",
    			"INVOICE_NO",
    			"HOPE_DELV_DATE",
    			"FLD_DSP",
    			"INV_SEND_MSG",
    			"MODEL_NO",
    			"SET_GUBUN",
    			"ETC_MSG",
    			"DELV_MSG1",
    			"MUL_DELV_MSG",
    			"BARCODE",
    			"INV_SEND_DM",
    			"DELIVERY_METHOD_STR2",
    			"FREE_GIFT"
    	};
    	String fieldListString = StringUtils.join(fieldList, "|");
    	
    	sb.append("<?xml version=\"1.0\" encoding=\"EUC-KR\"?>"
    			+ "<SABANG_ORDER_LIST>"
    			+ "<HEADER>"
    			+ "<SEND_COMPAYNY_ID>"
    			+ sendCompanyId
    			+ "</SEND_COMPAYNY_ID>"
    			+ "<SEND_AUTH_KEY>"
    			+ sendAuthKey
    			+ "</SEND_AUTH_KEY>"
    			+ "<SEND_DATE>"
    			+ orderDate
    			+ "</SEND_DATE>"
    			+ "</HEADER>"
    			
    			+ "<DATA>"
    			+ "<ORD_ST_DATE>"
    			+ ordStDate
    			+ "</ORD_ST_DATE>"
    			+ "<ORD_ED_DATE>"
    			+ ordEdDate
    			+ "</ORD_ED_DATE>"
    			+ "<ORD_FIELD>"
    			+ "<![CDATA[" + fieldListString + "]]>"
    			+ "</ORD_FIELD>"
    			+ "<ORDER_STATUS>"
    			+ orderStatus
    			+ "</ORDER_STATUS>"
    			+ "<PARTNER_ID>"
    			+ sendPartnerId
    			+ "</PARTNER_ID>"
    			+ "</DATA>"
    			+ "</SABANG_ORDER_LIST>");
    	
    	String xml = sb.toString();
    	BufferedOutputStream bos = null;
    	InputStream fin = null;
    	
    	try{
    		byte[] xmlByte = xml.getBytes("UTF-8");
    		fin = new ByteArrayInputStream(xmlByte);
    		
    		response.reset();
    		//response.setHeader("Content-Disposition", "attachment;filename="
    		//		+ URLEncoder.encode("ORDER_HEADER.xml", "UTF-8") + ";");
    		response.setHeader("Content-Transfer-Encoding","binary;");
    		response.setHeader("Pragma","no-cache;");
    		response.setHeader("Expires","-1;");
    		response.setContentLength(xmlByte.length);
    		response.setContentType("application/xml;charset=UTF-8");
    		
    		bos = new BufferedOutputStream(response.getOutputStream());
    		fin.read(xmlByte);
    		bos.write(xmlByte,0,xmlByte.length);
    		bos.flush();
    		
    	}catch(Exception e){
    		if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
    			//e.printStackTrace();
    			log.error("download fail");
    		}
    	}finally{
    		if (bos != null) {
    			try {
    				bos.close();
    			} catch (Exception ie) {
    			}
    		}
    		if (fin != null) {
    			try {
    				fin.close();
    			} catch (Exception ise) {
    			}
    		}
    	}
    }
    
    
    /**
     * Method ID	: sabangnetOrderHeader_V2_custom
     * Method 설명	: 사방넷 호출 XML .. V2_custom ( 매입처명, 쇼핑몰주문번호 등  )
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetOrderHeader_V2_custom.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetOrderHeader_V2_custom(Map<String, Object> model, HttpServletResponse response) throws Exception {
    	//주문일자 셋팅
    	String orderDate;
    	String pattern = "yyyyMMdd";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String date = simpleDateFormat.format(new Date());
    	orderDate = date;

    	//사방넷 인증키 파라미터 셋팅
    	Object sendAuthKeyObj = model.get("sendAuthKey");
    	String sendAuthKey;
    	if(sendAuthKeyObj != null){
    		sendAuthKey = (String)sendAuthKeyObj;
    	}else{
    		sendAuthKey = "";
    	}
    	
    	//사방넷 SEND_COMPAYNY_ID 파라미터 셋팅
    	Object sendCompanyIdObj = model.get("sendCompanyId");
    	String sendCompanyId;
    	if(sendCompanyIdObj != null){
    		sendCompanyId = (String)sendCompanyIdObj;
    	}else{
    		sendCompanyId = "";
    	}
    	
    	
    	//사방넷 검색시작일자 ORD_ST_DATE 파라미터 셋팅
    	Object ordStDateObj = model.get("ordStDate");
    	String ordStDate;
    	if(ordStDateObj != null){
    		ordStDate = (String)ordStDateObj;
    	}else{
    		ordStDate = "";
    	}
    	
    	//사방넷 검색마지막일자 ORD_ED_DATE 파라미터 셋팅
    	Object ordEdDateObj = model.get("ordEdDate");
    	String ordEdDate;
    	if(ordEdDateObj != null){
    		ordEdDate = (String)ordEdDateObj;
    	}else{
    		ordEdDate = "";
    	}
    	
    	//사방넷 주문상태 ORDER_STATUS 파라미터 셋팅
    	Object orderStatusObj = model.get("orderStatus");
    	String orderStatus = null;
    	if(orderStatusObj != null){
    		orderStatus = (String)orderStatusObj;
    	}
    	
    	//사방넷 매입처 ID 파라미터 셋팅
    	Object sendPartnerIdObj = model.get("sendPartnerId");
    	String sendPartnerId = null;
    	if(sendPartnerIdObj != null){
    		sendPartnerId = (String)sendPartnerIdObj;
    	}
    	
    	//사방넷 주문번호(쇼핑몰) ORDER_ID 파라미터 셋팅
    	Object orderIdObj = model.get("orderId");
    	String orderId = null;
    	if(orderIdObj != null){
    		orderId = (String)orderIdObj;
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	String[] fieldList = {
    			"IDX",
    			"ORDER_ID",
    			"MALL_ID",
    			"MALL_USER_ID",
    			"MALL_USER_ID2",
    			"ORDER_STATUS",
    			"USER_ID",
    			"USER_NAME",
    			"USER_TEL",
    			"USER_CEL",
    			"USER_EMAIL",
    			"RECEIVE_TEL",
    			"RECEIVE_CEL",
    			"RECEIVE_EMAIL",
    			"DELV_MSG",
    			"RECEIVE_NAME",
    			"RECEIVE_ZIPCODE",
    			"RECEIVE_ADDR",
    			"TOTAL_COST",
    			"PAY_COST",
    			"ORDER_DATE",
    			"PARTNER_ID",
    			"DPARTNER_ID",
    			"MALL_PRODUCT_ID",
    			"PRODUCT_ID",
    			"SKU_ID",
    			"P_PRODUCT_NAME",
    			"P_SKU_VALUE",
    			"PRODUCT_NAME",
    			"SALE_COST",
    			"MALL_WON_COST",
    			"WON_COST",
    			"SKU_VALUE",
    			"SALE_CNT",
    			"DELIVERY_METHOD_STR",
    			"DELV_COST",
    			"COMPAYNY_GOODS_CD",
    			"SKU_ALIAS",
    			"BOX_EA",
    			"JUNG_CHK_YN",
    			"MALL_ORDER_SEQ",
    			"MALL_ORDER_ID",
    			"ETC_FIELD3",
    			"ORDER_GUBUN",
    			"P_EA",
    			"REG_DATE",
    			"ORDER_ETC_1",
    			"ORDER_ETC_2",
    			"ORDER_ETC_3",
    			"ORDER_ETC_4",
    			"ORDER_ETC_5",
    			"ORDER_ETC_6",
    			"ORDER_ETC_7",
    			"ORDER_ETC_8",
    			"ORDER_ETC_9",
    			"ORDER_ETC_10",
    			"ORDER_ETC_11",
    			"ORDER_ETC_12",
    			"ORDER_ETC_13",
    			"ORDER_ETC_14",
    			"ord_field2",
    			"copy_idx",
    			"GOODS_NM_PR",
    			"GOODS_KEYWORD",
    			"ORD_CONFIRM_DATE",
    			"RTN_DT",
    			"CHNG_DT",
    			"DELIVERY_CONFIRM_DATE",
    			"CANCEL_DT",
    			"CLASS_CD1",
    			"CLASS_CD2",
    			"CLASS_CD3",
    			"CLASS_CD4",
    			"BRAND_NM",
    			"DELIVERY_ID",
    			"INVOICE_NO",
    			"HOPE_DELV_DATE",
    			"FLD_DSP",
    			"INV_SEND_MSG",
    			"MODEL_NO",
    			"SET_GUBUN",
    			"ETC_MSG",
    			"DELV_MSG1",
    			"MUL_DELV_MSG",
    			"BARCODE",
    			"INV_SEND_DM",
    			"DELIVERY_METHOD_STR2",
    			"FREE_GIFT"
    	};
    	String fieldListString = StringUtils.join(fieldList, "|");
    	
    	String xmlStr = "<?xml version=\"1.0\" encoding=\"EUC-KR\"?>" ;   
    	xmlStr += "<SABANG_ORDER_LIST>";
    	xmlStr += "<HEADER>";
    	xmlStr += "<SEND_COMPAYNY_ID>";
    	xmlStr += sendCompanyId;
    	xmlStr += "</SEND_COMPAYNY_ID>";
    	xmlStr += "<SEND_AUTH_KEY>";
    	xmlStr += sendAuthKey;
    	xmlStr += "</SEND_AUTH_KEY>";
    	xmlStr += "<SEND_DATE>";
    	xmlStr += orderDate;
    	xmlStr += "</SEND_DATE>";
    	xmlStr += "</HEADER>";
    	xmlStr += "<DATA>";
    	xmlStr += "<ORD_ST_DATE>";
    	xmlStr += ordStDate;
    	xmlStr += "</ORD_ST_DATE>";
    	xmlStr += "<ORD_ED_DATE>";
    	xmlStr += ordEdDate;
    	xmlStr += "</ORD_ED_DATE>";
    	xmlStr += "<ORD_FIELD>";
    	xmlStr += "<![CDATA["+fieldListString+"]]>";
    	xmlStr += "</ORD_FIELD>";
    	if(orderStatusObj != null){
	    	xmlStr += "<ORDER_STATUS>";
	    	xmlStr += orderStatus;
	    	xmlStr += "</ORDER_STATUS>";
    	}
    	if(sendPartnerIdObj != null){
	    	xmlStr += "<PARTNER_ID>";
	    	xmlStr += sendPartnerId;
	    	xmlStr += "</PARTNER_ID>";
    	}
    	if(orderIdObj != null){
    		xmlStr += "<ORDER_ID>";
        	xmlStr += orderId;
        	xmlStr += "</ORDER_ID>";
    	}
    	xmlStr += "</DATA>";
    	xmlStr += "</SABANG_ORDER_LIST>";
    	
    	sb.append(xmlStr);
    	
    	String xml = sb.toString();
    	BufferedOutputStream bos = null;
    	InputStream fin = null;
    	
    	try{
    		byte[] xmlByte = xml.getBytes("UTF-8");
    		fin = new ByteArrayInputStream(xmlByte);
    		
    		response.reset();
    		//response.setHeader("Content-Disposition", "attachment;filename="
    		//		+ URLEncoder.encode("ORDER_HEADER.xml", "UTF-8") + ";");
    		response.setHeader("Content-Transfer-Encoding","binary;");
    		response.setHeader("Pragma","no-cache;");
    		response.setHeader("Expires","-1;");
    		response.setContentLength(xmlByte.length);
    		response.setContentType("application/xml;charset=UTF-8");
    		
    		bos = new BufferedOutputStream(response.getOutputStream());
    		fin.read(xmlByte);
    		bos.write(xmlByte,0,xmlByte.length);
    		bos.flush();
    		
    	}catch(Exception e){
    		if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
    			//e.printStackTrace();
    			log.error("download fail");
    		}
    	}finally{
    		if (bos != null) {
    			try {
    				bos.close();
    			} catch (Exception ie) {
    			}
    		}
    		if (fin != null) {
    			try {
    				fin.close();
    			} catch (Exception ise) {
    			}
    		}
    	}
    }
    
    
    
    /**
     * Method ID	: sabangnetInvoiceHeader
     * Method 설명	: 사방넷 송장등록 XML 
     * 작성자			: sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetInvoiceHeader.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetInvoiceHeader(Map<String, Object> model, HttpServletResponse response) throws Exception {
    	
    	//데이터 
    	Map<String, Object> list = wmsif601service.parcelList(model);
		JSONObject jsonObject = new JSONObject(list);
		JSONObject jsonObject_step1 = jsonObject.getJSONObject("LIST");
		JSONArray  jsonObject_step2 = jsonObject_step1.getJSONArray("list");
		int listCnt   = jsonObject_step2.length();
		
    	//송신일자 셋팅
    	String orderDate;
    	String pattern = "yyyyMMdd";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String date = simpleDateFormat.format(new Date());
    	orderDate = date;
    	
    	//사방넷계정
    	Object sendCompanyIdObj = model.get("sendCompanyId");
    	String sendCompanyId;
    	if(sendCompanyIdObj != null){
    		sendCompanyId = (String)sendCompanyIdObj;
    	}else{
    		sendCompanyId = "";
    	}
    	
    	//사방넷 인증키 파라미터 셋팅
    	Object sendAuthKeyObj = model.get("sendAuthKey");
    	String sendAuthKey;
    	if(sendAuthKeyObj != null){
    		sendAuthKey = (String)sendAuthKeyObj;
    	}else{
    		sendAuthKey = "";
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append("<?xml version=\"1.0\" encoding=\"EUC-KR\"?>"
    			+ "<SABANG_INV_REGI>"
    			+ "<HEADER>"
    			+ "<SEND_COMPAYNY_ID>"
    			+ sendCompanyId
    			+ "</SEND_COMPAYNY_ID>"
    			+ "<SEND_AUTH_KEY>"
    			+ sendAuthKey
    			+ "</SEND_AUTH_KEY>"
    			+ "<SEND_DATE>"
    			+ orderDate
    			+ "</SEND_DATE>"
    			+ "<SEND_INV_EDIT_YN>"
    			+ "N"
    			+ "</SEND_INV_EDIT_YN>"
    			+ "<RESULT_TYPE>"
    			+ "XML"
    			+ "</RESULT_TYPE>"
    			+ "</HEADER>");
		for(int i=0; i< listCnt; i++) {
	    	sb.append("<DATA>"
	    			+ "<SABANGNET_IDX> 		<![CDATA["+jsonObject_step2.getJSONObject(i).getString("IDX")+"]]> 		</SABANGNET_IDX>"
	    			+ "<TAK_CODE> 			<![CDATA["+jsonObject_step2.getJSONObject(i).getString("SBNET_DLV_CD")+"]]> 		</TAK_CODE>"
	    			+ "<TAK_INVOICE> 		<![CDATA["+jsonObject_step2.getJSONObject(i).getString("INVC_NO")+"]]> 		</TAK_INVOICE>"
	    			+ "</DATA>");
		}
		sb.append("</SABANG_INV_REGI>");
		
    	String xml = sb.toString();
    	BufferedOutputStream bos = null;
    	InputStream fin = null;
    	
    	try{
    		byte[] xmlByte = xml.getBytes("UTF-8");
    		fin = new ByteArrayInputStream(xmlByte);
    		
    		response.reset();
    		response.setHeader("Content-Transfer-Encoding","binary;");
    		response.setHeader("Pragma","no-cache;");
    		response.setHeader("Expires","-1;");
    		response.setContentLength(xmlByte.length);
    		response.setContentType("application/xml;charset=UTF-8");
    		
    		bos = new BufferedOutputStream(response.getOutputStream());
    		fin.read(xmlByte);
    		bos.write(xmlByte,0,xmlByte.length);
    		bos.flush();
    		
    	}catch(Exception e){
    		if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
    			log.error("download fail");
    		}
    	}finally{
    		if (bos != null) {
    			try {
    				bos.close();
    			} catch (Exception ie) {
    			}
    		}
    		if (fin != null) {
    			try {
    				fin.close();
    			} catch (Exception ise) {
    			}
    		}
    	}
    }
    
    
    /**
     * Method ID	: sabangnetParcelSendXmlUrl_SB
     * Method 설명	: 사방넷 송장등록 XML_ SB
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetParcelSendXmlUrl_SB.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetParcelSendXmlUrl_SB(Map<String, Object> model, HttpServletResponse response) throws Exception {

		String apiUrl =	"http://10.30.1.14:5200/SABANGNET_SB/PARCEL_REQ_XMLURL"; //new
		System.out.println("apiUrl : " + apiUrl);
		
	   try{
		    //ssl disable
		    //disableSslVerification();
	        
	        URL url = null; 
	        url = new URL(apiUrl);
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = "{";
			jsonInputString		  += "	\"SEND_COMPAYNY_ID\":\""+(String) model.get("SEND_COMPAYNY_ID")+"\"";
			jsonInputString		  += "	,\"MINUS_MM\":\""		+(String) model.get("MINUS_MM")+"\"";
			jsonInputString		  += "}";
			System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        
	        //Response data 받는 부분
	        int responceCode = con.getResponseCode();
	        if (responceCode == HttpURLConnection.HTTP_OK) {
	       
	    	   try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
		            StringBuilder responseB = new StringBuilder();
		            String responseLine = null;
		            while ((responseLine = br.readLine()) != null) {
		            	responseB.append(responseLine.trim());
		            }
		            
		            //xml
		        	String xml = responseB.toString();
		        	System.out.println("xml : " + xml);
		        	BufferedOutputStream bos = null;
		        	InputStream fin = null;
		        	
		        	try{
		        		byte[] xmlByte = xml.getBytes("UTF-8");
		        		fin = new ByteArrayInputStream(xmlByte);
		        		
		        		response.reset();
		        		response.setHeader("Content-Transfer-Encoding","binary;");
		        		response.setHeader("Pragma","no-cache;");
		        		response.setHeader("Expires","-1;");
		        		response.setContentLength(xmlByte.length);
		        		response.setContentType("application/xml;charset=UTF-8");
		        		
		        		bos = new BufferedOutputStream(response.getOutputStream());
		        		fin.read(xmlByte);
		        		bos.write(xmlByte,0,xmlByte.length);
		        		bos.flush();
		        		
		        	}catch(Exception e){
		        		if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
		        			log.error("download fail");
		        		}
		        	}finally{
		        		if (bos != null) {
		        			try {
		        				bos.close();
		        			} catch (Exception ie) {
		        			}
		        		}
		        		if (fin != null) {
		        			try {
		        				fin.close();
		        			} catch (Exception ise) {
		        			}
		        		}
		        	}
		        	
		        	
		        }
	       }
	       con.disconnect();
	       
        } catch(Exception e){
            throw e;
        }
    	
    }
    
    
    
    /**
     * Method ID	: sabangnetInvoiceHeader_SB
     * Method 설명	: 사방넷 송장등록 XML_ SB
     * 작성자			: yhku
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetInvoiceHeader_SB.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetInvoiceHeader_SB(Map<String, Object> model, HttpServletResponse response) throws Exception {
		
    	//송신일자 셋팅
    	String orderDate;
    	String pattern = "yyyyMMdd";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String date = simpleDateFormat.format(new Date());
    	orderDate = date;
    	
    	//사방넷계정
    	Object sendCompanyIdObj = model.get("sendCompanyId");
    	String sendCompanyId;
    	if(sendCompanyIdObj != null){
    		sendCompanyId = (String)sendCompanyIdObj;
    	}else{
    		sendCompanyId = "";
    	}
    	
    	//사방넷 인증키 파라미터 셋팅
    	Object sendAuthKeyObj = model.get("sendAuthKey");
    	String sendAuthKey;
    	if(sendAuthKeyObj != null){
    		sendAuthKey = (String)sendAuthKeyObj;
    	}else{
    		sendAuthKey = "";
    	}

    	//송장정보 데이터
    	List<String> vrSabangnetIdxArr	 = new ArrayList();
    	List<String> vrTakCdArr			 = new ArrayList();
    	List<String> vrTakInvoiceArr 	 = new ArrayList();
		String[] sabangnetIdx = model.get("SABANGNET_IDX").toString().split(",");
		String[] takCode = model.get("TAK_CODE").toString().split(",");
		String[] takInvoice = model.get("TAK_INVOICE").toString().split(",");
		for (String keyword : sabangnetIdx ){
			vrSabangnetIdxArr.add(keyword.replaceAll("(\r\n|\r|\n|\n\r|\\p{Z}|\\t)", ""));
		}
		for (String keyword : takCode ){
			vrTakCdArr.add(keyword.replaceAll("(\r\n|\r|\n|\n\r|\\p{Z}|\\t)", ""));
		}
		for (String keyword : takInvoice ){
			vrTakInvoiceArr.add(keyword.replaceAll("(\r\n|\r|\n|\n\r|\\p{Z}|\\t)", ""));
		}
		int listCnt = vrSabangnetIdxArr.size() ; 
		
		
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append("<?xml version=\"1.0\" encoding=\"EUC-KR\"?>"
    			+ "<SABANG_INV_REGI>"
    			+ "<HEADER>"
    			+ "<SEND_COMPAYNY_ID>"
    			+ sendCompanyId
    			+ "</SEND_COMPAYNY_ID>"
    			+ "<SEND_AUTH_KEY>"
    			+ sendAuthKey
    			+ "</SEND_AUTH_KEY>"
    			+ "<SEND_DATE>"
    			+ orderDate
    			+ "</SEND_DATE>"
    			+ "<SEND_INV_EDIT_YN>"
    			+ "N"
    			+ "</SEND_INV_EDIT_YN>"
    			+ "<RESULT_TYPE>"
    			+ "XML"
    			+ "</RESULT_TYPE>"
    			+ "</HEADER>");
		for(int i=0; i< listCnt; i++) {
	    	sb.append("<DATA>"
	    			+ "<SABANGNET_IDX> 		<![CDATA["+vrSabangnetIdxArr.get(i)+"]]> 	</SABANGNET_IDX>"
	    			+ "<TAK_CODE> 			<![CDATA["+vrTakCdArr.get(i)+"]]> 			</TAK_CODE>"
	    			+ "<TAK_INVOICE> 		<![CDATA["+vrTakInvoiceArr.get(i)+"]]> 		</TAK_INVOICE>"
	    			+ "</DATA>");
		}
		sb.append("</SABANG_INV_REGI>");
		
    	String xml = sb.toString();
    	System.out.println("xml : " + xml);
    	BufferedOutputStream bos = null;
    	InputStream fin = null;
    	
    	try{
    		byte[] xmlByte = xml.getBytes("UTF-8");
    		fin = new ByteArrayInputStream(xmlByte);
    		
    		response.reset();
    		response.setHeader("Content-Transfer-Encoding","binary;");
    		response.setHeader("Pragma","no-cache;");
    		response.setHeader("Expires","-1;");
    		response.setContentLength(xmlByte.length);
    		response.setContentType("application/xml;charset=UTF-8");
    		
    		bos = new BufferedOutputStream(response.getOutputStream());
    		fin.read(xmlByte);
    		bos.write(xmlByte,0,xmlByte.length);
    		bos.flush();
    		
    	}catch(Exception e){
    		if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
    			log.error("download fail");
    		}
    	}finally{
    		if (bos != null) {
    			try {
    				bos.close();
    			} catch (Exception ie) {
    			}
    		}
    		if (fin != null) {
    			try {
    				fin.close();
    			} catch (Exception ise) {
    			}
    		}
    	}
    }
    
    
    
    
}