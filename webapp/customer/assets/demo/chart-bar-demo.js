// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart").getContext('2d');
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["02", "04", "06", "08", "10", "12"],
    datasets: [{
      label: "Revenue",
      backgroundColor: "rgba(36, 87, 189, 0.5)",
      //borderColor: "rgba(2,117,216,1)",
      data: [0, 0, 0, 0, 0, 0],
      order: 1
    },{
        label: "Revenue1",
        backgroundColor: "rgba(204, 61, 61, 0.5)",
        //borderColor: "rgba(2,117,216,1)",
        data: [0, 0, 0, 0, 0, 0],
        order: 1
      },{
    	  type: 'line',
    	  label: 'Line Dataset',
          data: [100, 100, 100, 100, 100, 100],
          borderWidth: 2,
          pointBackgroundColor: "rgba(34,116,28,1)",
          borderColor: "rgba(34,116,28,0.3)",
          order: 2,
          fill:false       
      }
    ],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 15000,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
