<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language ="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");%>

<!DOCTYPE html>
<html style="height:100%">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<script src="https://winusreport.logisall.com/oz80/ozhviewer/jquery-3.7.1.min.js"></script>
		<link rel="stylesheet" href="https://winusreport.logisall.com/oz80/ozhviewer/jquery-ui.css" type="text/css"/>
		<script src="https://winusreport.logisall.com/oz80/ozhviewer/jquery-ui.min.js"></script>
		<link rel="stylesheet" href="https://winusreport.logisall.com/oz80/ozhviewer/ui.dynatree.css" type="text/css"/>
		<script type="text/javascript" src="https://winusreport.logisall.com/oz80/ozhviewer/jquery.dynatree.js" charset="utf-8"></script>
		<script type="text/javascript" src="https://winusreport.logisall.com/oz80/ozhviewer/OZJSViewer.js" charset="utf-8"></script>
	</head>

	<body style="width:100%;height:100%;margin:0px;">
		<div id="OZViewer" style="width:100%;height:100%;"></div>
	</body>

	<script type="text/javascript" >
		const FILE_NAME     = '<%= request.getParameter("fileName") %>';
		const REPORT_NAME   = '<%= request.getParameter("reportName") %>';
		const ODI_NAME      = '<%= request.getParameter("odiName") %>';
		const ZOOM         	= '<%= request.getParameter("zoom") %>';
		const VIEWER_MODE   = '<%= request.getParameter("viewerMode") %>';
		const PRINT_ONCE    = '<%= request.getParameter("printOnce") %>';

		const ODI_PARAMS   	= `<%= request.getParameter("odiParams") %>`;
		const OZR_PARAMS   	= `<%= request.getParameter("ozrParams") %>`;


		/**
		 * 뷰어 설정
		 * 
		 * @override
		 * @returns 
		 */
		function SetOZParamters_OZViewer(){

			const fileName     = FILE_NAME;
			const reportName   = REPORT_NAME;
			const odiName      = ODI_NAME;
			const zoom         = ZOOM == "null" ? '100' : ZOOM;
			const viewerMode   = VIEWER_MODE == "null" ? 'preview' : VIEWER_MODE;
			const printOnce	   = PRINT_ONCE == "null" ? 'false' : PRINT_ONCE;

			const oz  = document.getElementById("OZViewer");

			oz.sendToActionScript("connection.servlet", "https://winusreport.logisall.com/oz80/server");
			oz.sendToActionScript("connection.reportname","WINUS/"+reportName);
			oz.sendToActionScript("odi.odinames", odiName);


			/** Viewer Parameter */
			oz.sendToActionScript("viewer.zoom", zoom);
			oz.sendToActionScript("viewer.mode", viewerMode);


			/** Viewer(Export Mode) Paramter */
			oz.sendToActionScript("export.mode","silent");
			oz.sendToActionScript("export.filename", fileName);
			// oz.sendToActionScript("export.format","pdf");
			oz.sendToActionScript("pdf.fontembedding","true");


			/** Viewer(Print Mode) Paramter */
			oz.sendToActionScript("print.mode","silent");
			oz.sendToActionScript("print.once", printOnce);
			// oz.sendToActionScript("print.copies", copiesCnt);
			// oz.sendToActionScript("print.lockopt", 'true');
			// oz.sendToActionScript("print.size", 'A4');
			

			/** Event */
			oz.sendToActionScript("viewer.progresscommand", "true");
			// oz.sendToActionScript("viewer.printcommand", "true");
			// oz.sendToActionScript("viewer.postcommand", "true");
			// oz.sendToActionScript("viewer.useractioncommand", "true");

			/** Debug Option */
			oz.sendToActionScript("information.debug","true");			// [Ctrl + Z] 디버그창 호출


			/** 1. Parameter Set (*.odi)  */
			if(ODI_PARAMS != "null"){
				const odiParams = JSON.parse(ODI_PARAMS);
				let odiKeyList 	= Object.keys(odiParams);

				oz.sendToActionScript('odi.' + odiName + '.pcount', odiKeyList.length);
				console.log('odi.pcount: ' + odiKeyList.length);

				let odiIndex = 1;
				for(const [key, value] of Object.entries(odiParams))
				{
					oz.sendToActionScript('odi.' + odiName + '.args' + odiIndex, key + '=' + value);
					console.log('odi.' + odiName + '.args' + odiIndex, key + '=' + value);

					odiIndex++;
				}
			}


			/** 2. Parameter Set (*.ozr)  */
			if(OZR_PARAMS != "null"){
				const ozrParams = JSON.parse(OZR_PARAMS);
				let ozrKeyList 	= Object.keys(ozrParams);

				oz.sendToActionScript('connection.pcount', ozrKeyList.length);
				console.log('connection.pcount: ' + ozrKeyList.length);

				let ozrIndex = 1;
				for(const [key, value] of Object.entries(ozrParams))
				{
					oz.sendToActionScript('connection.args' + ozrIndex, key + '=' + value);
					console.log('connection.args' + ozrIndex, key + '=' + value);

					ozrIndex++;
				}
			}

			return true;
		}


		/**
		 * 뷰어 실행 이벤트
		 * 
		 * @param {*} step 			: Step of the report creating(0 : Setting Viewer Options, 1 : Download the report file, 2 : Create a report template, 3 : Download the data, 4 : Report binding)
		 * @param {*} state 		: State of the report creating(1 : Start, 2 : Finish)
		 * @param {*} reportname 
		 * @override
		 */
		function OZProgressCommand_OZViewer(step, state, reportname) {
			if (step == 4 && state == 2){
				switch(VIEWER_MODE){
					case 'print' :
					case 'preview' : 
						OZViewer.Script("print");
						break;

					case 'export' : 
						OZViewer.Script("save");
						break;

					case 'open' : 
						OZViewer.Script("open");
						break;
				}
			}
		}


		/**
		 * 뷰어 인쇄 이벤트
		 * 오즈 뷰어에서 보고서를 인쇄한 후 인쇄 결과 이벤트를 자바 스크립트 함수를 이용하여 사용자 프로그램에 알려줍니다.
		 * (viewer.printcommand)
		 * 
		 * @param {*} msg 			: 프린트 결과 메시지
		 * @param {*} code 			: 프린트 성공 여부(0 : 성공, 1 : 실패)
		 * @param {*} reportname 	: 보고서 이름
		 * @param {*} printername 	: 프린터 이름
		 * @param {*} printcopy 	: 인쇄 매수
		 * @param {*} printranges 	: 인쇄된 용지 매수
		 * @param {*} printrange 	: 인쇄 범위(all : 모두, current : 현재 페이지, selected : 선택된 페이지, range : 페이지 지정)
		 * @param {*} username 		: 사용자 이름
		 * @override
		 */
		function OZPrintCommand_OZViewer(msg, code, reportname, printername, printcopy, printranges, printrange, username) {

		}


		/**
		 * 뷰어 실행 완료 이벤트
		 * (viewer.postcommand)
		 * 
		 * @param {*} cmd 
		 * @param {*} msg 
		 * @override
		 */
		function OZPostCommand_OZViewer(cmd, msg) {

		}


		/**
		 * 보고서 파일을 열거나 인쇄, 저장 등 오즈 뷰어에서 발생하는 조작 이벤트
		 * (viewer.useractioncommand)
		 * 
		 * @param {*} type 
		 * @param {*} attr 
		 * @override
		 */
		function OZUserActionCommand_OZViewer(type, attr) {
			
			/* Timer */

		}


		start_ozjs("OZViewer","https://winusreport.logisall.com/oz80/ozhviewer/");

	</script>
</html>