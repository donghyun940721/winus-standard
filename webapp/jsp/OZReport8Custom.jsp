<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language ="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html style="height:100%">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<script src="https://winusreport.logisall.com/oz80/ozhviewer/jquery-3.7.1.min.js"></script>
<link rel="stylesheet" href="https://winusreport.logisall.com/oz80/ozhviewer/jquery-ui.css" type="text/css"/>
<script src="https://winusreport.logisall.com/oz80/ozhviewer/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://winusreport.logisall.com/oz80/ozhviewer/ui.dynatree.css" type="text/css"/>
<script type="text/javascript" src="https://winusreport.logisall.com/oz80/ozhviewer/jquery.dynatree.js" charset="utf-8"></script>
<script type="text/javascript" src="https://winusreport.logisall.com/oz80/ozhviewer/OZJSViewer.js" charset="utf-8"></script>
</head>
<body style="width:100%;height:100%;margin:0px;">
<div id="OZViewer" style="width:100%;height:100%;"></div>

<script type="text/javascript" >
	
	<%
	String[] viewerMode = (request.getParameter("viewerMode")==null?"":(String)request.getParameter("viewerMode")).split(",");
	Boolean PrintFlag = false;
	Boolean ExportFlag = false;
	Boolean OpenFlag = false;
		
	if(viewerMode.length != 0){
		for(int i = 0 ; i < viewerMode.length ; i++){
			if(viewerMode[i].equals("print")|| viewerMode[i].equals("preview")){
				PrintFlag = true;
			}else if(viewerMode[i].equals("open")){
				OpenFlag = true;
			}else if(viewerMode[i].equals("export")){
				ExportFlag = true;
			}
		}
	}
	%>
	//step - Step of the report creating(0 : Setting Viewer Options, 1 : Download the report file, 2 : Create a report template, 3 : Download the data, 4 : Report binding)
	//state - State of the report creating(1 : Start, 2 : Finish)
	function OZProgressCommand_OZViewer(step, state, reportname) {
		if (step == 4 && state == 2){
			<%if(PrintFlag){%>
			OZViewer.Script("print");
			<%}%>
			<%if(OpenFlag){%>
			OZViewer.Script("open");
			<%}%>
			<%if(ExportFlag){%>
			OZViewer.Script("save");
			<%}%>
		}
	}
	
	// "오즈 뷰어에서 보고서를 인쇄한 후 인쇄 결과 이벤트를 자바 스크립트 함수를 이용하여 사용자 프로그램에 알려줍니다.
	/* msg - 프린트 결과 메시지
	code - 프린트 성공 여부(0 : 성공, 1 : 실패)
	reportname - 보고서 이름
	printername - 프린터 이름
	printcopy - 인쇄 매수
	printpages - 인쇄된 용지 매수
	printrange - 인쇄 범위(all : 모두, current : 현재 페이지, selected : 선택된 페이지, range : 페이지 지정)
	username - 사용자 이름
	printerdrivername - 프린터 드라이버 이름
	printpagesrange - 인쇄된 페이지 범위 */
	function OZPrintCommand_OZViewer(msg, code, reportname, printername, printcopy, printranges, printrange, username) {
	}
	
	function OZPostCommand_OZViewer(cmd, msg) {
	}
	
	function OZUserActionCommand_OZViewer(type, attr) {
		if(type == "Print"){
			var timer = setInterval(function(){
				
				//setTimeout("WinClose();", 3000);//종료
				//$(document).trigger(eventEnterKey);
				//clickevent();
				clearInterval(timer);
			}, 3000);
		}
	}


	jQuery(document).ready(function(){	
//	 	 jQuery(document).keypress(function(e) { 
		 jQuery(document).bind("keyup keydown", function(e){
			    if(e.shiftKey && e.ctrlKey && e.keyCode == 80){
			        return false;
			    }
			    if (e.keyCode == 13){
			    	console.log("BIND ENTER_________________");
			    	WinClose();
			    	return false;
			    }
			});
	});
				
// 	$.browser.version 는 웹브라우저 버전.
// 	$.browser.safari 사파리 웹브라우저일때 true, 아니면 undefined.
// 	$.browser.opera 오페라 웹브라우저일때 true, 아니면 undefined.
// 	$.browser.msie MS IE 웹브라우저일때 true, 아니면 undefined.
// 	$.browser.mozilla 파이어폭스 웹브라우저일때 true, 아니면 undefined.
	function WinClose(){
		//console.log($.browser.msie);
		if ( $.browser.msie ) {
	      	window.opener='Self';
	      	window.open('','_parent','');
	      	window.close();
	      	window.open('about:blank','_self').self.close();
	    } else {
	      	window.close(); // 일반적인 현재 창 닫기
	      	window.open('about:blank','_self').self.close();  // IE에서 묻지 않고 창 닫기
	    }
	    return false;
	}
	
	
	var eventEnterKey = jQuery.Event("keypress", {
	    keyCode:13
	});
	function clickevent(){                
	     var e = $.Event("keydown");
	     e.which = 13;
	     e.keyCode = 80;
	     e.ctrlKey = true;
	     e.shiftKey= true;
	     $(document).trigger(e);     
	}
// 	function WinClickEvent(){
// 		let event = new Event("Events");
// 		event.initEvent('keydown', true, true);
// 		event.keyCode = 13;
// 		document.dispatchEvent(event);
// 	    return false;
// 	}

	
	<%
	String[] key = ((String)request.getParameter("arrKey")).split(",");
	int argLen = key.length;
	%>
	
	var fileName = "<%=request.getParameter("fileName")%>";
	var reportName = "<%=request.getParameter("reportName")%>";
	var odiName = "<%=request.getParameter("odiName")%>";
	var zoom = "<%=request.getParameter("zoom")!=null ? request.getParameter("zoom"):"100"%>";
	var viewerMode_1 = "<%=request.getParameter("viewerMode")!=null ? request.getParameter("viewerMode"):"print"%>";
	var printerMode = "<%=request.getParameter("printerMode")!=null ? request.getParameter("printerMode"):"silent"%>";
	var once = "<%=request.getParameter("once")!=null ? request.getParameter("once"):"flase"%>";
	var printername = "<%=request.getParameter("printername")!=null ? request.getParameter("printername"):"DEFAULT_PRINTER"%>";
	var copiesCnt = "<%=request.getParameter("copiesCnt")!=null ? request.getParameter("copiesCnt"):"1"%>";
	//모아찍기 여부(normal/gather)
	var printStyle = "<%=request.getParameter("printStyle")!=null ? request.getParameter("printStyle"):"normal"%>";
	
	printername= "Microsoft XPS Document Writer";
	
	// i?¤i|?e·°i?´ html5 i?¤i?? e¶e¶?
	function SetOZParamters_OZViewer(){
		var date = new Date();
		var oz = document.getElementById("OZViewer");
		
		// [기본 패러미터]
		oz.sendToActionScript("connection.servlet", "https://winusreport.logisall.com/oz80/server"); // 오즈서버 URL
		oz.sendToActionScript("connection.reportname","WINUS/"+reportName); // 보고서 이름
		oz.sendToActionScript("information.debug","true"); // 콘솔정보(Ctrl+Z) 표시 여부
		
		
		// [폼패러미터]
		oz.sendToActionScript("connection.pcount","1"); // ozr로 넘길 폼패러미터 개수
		oz.sendToActionScript("connection.args1","NOW_DATE="+date);

		// [ODI패러미터]
		oz.sendToActionScript("odi.odinames",odiName);
		oz.sendToActionScript("odi."+odiName+".pcount","<%=argLen%>"); //odi로 넘길 사용자패러미터 개수
		<% for(int i = 0; i< argLen; i++){%>
			oz.sendToActionScript("odi."+odiName+".args<%=i+1%>","<%=key[i]%>=<%=request.getParameter(key[i])%>");
		<%}%>
		
		
		// [뷰어]
		oz.sendToActionScript("viewer.zoom",zoom);
		oz.sendToActionScript("viewer.mode", viewerMode_1);//viewer.mode - print: 미리보기 없이 바로 프린트 / export: 미리보기 없이 저장
		// [뷰어 > 페이지 표시]
		//oz.sendToActionScript("viewer.pagedisplay","continuous"); // 페이지 표시 옵션
		//oz.sendToActionScript("viewer.pagenavigate_by_scroll","true"); // 마우스 휠을 위/아래로 움직일 때 화면이 페이지 시작/끝에 위치하면 페이지를 이전/다음 페이지로 이동 여부 설정
		//oz.sendToActionScript("viewer.viewmode","normal"); // 보고서 보기 모드

		
		// [인쇄 옵션]
		oz.sendToActionScript("print.mode", printerMode); //인쇄창 표시 여부 : true(기본값)/showprogress/silent(안나옴)
		oz.sendToActionScript("print.once", once); //보고서 인쇄를 한번만 할지 여부
// 		oz.sendToActionScript("print.printername", printername); //인쇄할 프린터명 -> ★★★  HTML형식 불가. 패러미터는 ActiveX뷰어와 exe뷰어에서만 사용 가능합니다.
		oz.sendToActionScript("print.copies", copiesCnt); // 인쇄매수 설정
		oz.sendToActionScript("print.lockopt", "true"); //옵션lock
		oz.sendToActionScript("print.size", "A4"); // 인쇄 용지 크기
		
		// [인쇄 옵션 > 모아찍기]-> ★★★  HTML형식 불가.
// 		oz.sendToActionScript("print.style", printStyle); //gether : 모아찍기, normal : 일반인쇄 (기본값)
// 		oz.sendToActionScript("print.pagesinone", "2");//한 페이지당 출력되는 페이지 수를 설정
// 		oz.sendToActionScript("print.pageorient", "vertical");//horizontal : 가로 용지에 인쇄 (기본값), vertical : 세로 용지에 인쇄
// 		oz.sendToActionScript("print.pageorder", "horizontal"); // gather(모아찍기)인 경우 페이지 출력 순서를 수평방향/수직방향
		
		// [저장 옵션]
		oz.sendToActionScript("export.mode","silent"); // 설정창 보여줄지 여부
		oz.sendToActionScript("export.format","pdf"); //저장할 파일 형식
		oz.sendToActionScript("export.filename", fileName);// 파일 이름
		oz.sendToActionScript("pdf.fontembedding","true");
		
		// [뷰어 > 이벤트]
		oz.sendToActionScript("viewer.progresscommand", "true"); // 뷰어 실행 이벤트 사용
		//oz.sendToActionScript("viewer.printcommand", "true"); //뷰어 인쇄 이벤트 사용
		//oz.sendToActionScript("viewer.postcommand", "true"); //뷰어 실행 완료
		//oz.sendToActionScript("viewer.useractioncommand", "true"); //보고서 파일을 열거나 인쇄, 저장 등 오즈 뷰어에서 발생하는 조작 이벤트 
		
		
		return true;
	}

	start_ozjs("OZViewer","https://winusreport.logisall.com/oz80/ozhviewer/");
</script>

</body>
</html>