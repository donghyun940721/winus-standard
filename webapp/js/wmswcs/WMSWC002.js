jQuery(document).ready(function(){
	jQuery('#fullpath_screen').html("$!request.getParameter('_fullPath')");

	jQuery('#tab1').tabs({active:0});
	
	jQuery("input:button").button();
	
	/* datepicker */
	jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ "$ss_lang" ] );
	var date = new Date();
	gbChkDataVal = date.getFormattedString("YYYY-MM-DD");
	date.setDate(date.getDate());
	
	jQuery("input[name=vrSrchReqDtFrom]").each(function(index, item){ 
		jQuery(item).get(0).valueAsDate = date;
	});

	jQuery("input[name=vrSrchReqDtTo]").each(function(index, item){ 
		jQuery(item).get(0).valueAsDate = date;
	});

	gfn_forceEventResize();

	vrSrchCustCd_OnStart("E01");
	
	getOliveSrtCodeList();

	fn_setInputEnterEvent("vrSrchOrdrNoE1", "vrSrchOrgOrdIdListE1", fn_classificationOrdSearch);
	fn_setInputEnterEvent("vrSrchOrdrNoE2", "vrSrchOrgOrdIdListE2", fn_shipmentSearch);
	fn_setInputEnterEvent("vrSrchOrdrNoE3", "vrSrchOrgOrdIdListE3", fn_orderStatusChangeOrdSearch);
});

function fn_setInputEnterEvent(ordrNo, ordrList, searchFunc) {
	jQuery(`#${ordrNo}, #${ordrList}`).on("keypress", function(e) {
		if (e.which === 13) { 
			e.preventDefault();
			searchFunc();
		}
	});
}

function fn_custPopup(tab){
	var strParameter = [];
	var param 		 = '';
	var url 		 = '';
	var width 		 = "800";
	var height 		 = "530";

	param = "?func=fn_setWMSWC002&custType=12";
	url = "/WMSCM011.action" + param;

	var asppop = cfn_openPop2(url, "fn_custPopup", width, height);
	asppop.focus();
}

// 페이지 로딩시 화주 선택 
function vrSrchCustCd_OnStart(tab){
	var vrSrchCustCd = jQuery('form[name="frm_list'+tab+'"] input[name="vrSrchCustCd"]').val();
	vrSrchCustCd = vrSrchCustCd == undefined ? "":vrSrchCustCd;
	var srchKey = "CUST";
	var param   = "srchKey=" + srchKey + "&vrSrchCustCd=" + vrSrchCustCd + "&S_CUST_TYPE=12";
	var url     = "/selectMngCode.action"
	jQuery.post(url, param, function(data){
		var data = data.CUST;
		var cnt  = data.size();
		if( cnt > 1 ){
			var strParameter = [];
			strParameter.push(vrSrchCustCd);
			var width  = "800";
			var height = "530";
			var param  = "?func=fn_setWMSWC002&custType=12&strParameter=" + strParameter + "&stBtnYn=N";
			var url    = "/WMSCM011.action" + param;
			var asppop = cfn_openPop2(url, "fn_Popup", width, height);
			asppop.focus();
		}
		else if( cnt == 1 ){
			jQuery('input[name="vrSrchCustCd"]').val(data[0].CODE);
			jQuery('input[name="vrSrchCustNm"]').val(data[0].NAME);
			jQuery('input[name="vrSrchCustId"]').val(data[0].ID);
		}
		else if( cnt == 0 ){
			alert("$!lang.getText('list.nodata')");
			jQuery('input[name="vrSrchCustCd"]').val("").focus();
			jQuery('input[name="vrSrchCustNm"]').val("");
			jQuery('input[name="vrSrchCustId"]').val("");
		}
	}).fail(function(xhr, textStatus){
   		if(xhr.status == 404){
   			location.href=('/index.html');
   		}
   	});
}

/* 시작 화주 세팅 */
function fn_setWMSWC002(arr){
	var info = arr[0];
	jQuery('input[name="vrSrchCustCd"]').val(info.cust_cd);
	jQuery('input[name="vrSrchCustNm"]').val(info.cust_nm);
	jQuery('input[name="vrSrchCustId"]').val(info.cust_id);
}

function fn_changedEventCallBack(srchKey, tab, key){
	var id = ""; 
	var cd = ""; 
	var nm = ""; 
	var popUrl = ""; 
	var srchParamName = ""; 
	var srchValue = ""; 
	var popParam = []; 

	switch(srchKey){
		case "CUST" :
			id = "vrSrchCustId";
			cd = "vrSrchCustCd";
			nm = "vrSrchCustNm";
			
			srchParamName = key == "CODE" ? "vrSrchCustCd" : "vrSrchCustNm";
			srchValue = jQuery('form[name="frm_list'+tab+'"] input[name="'+srchParamName+'"]').val();

			if (key == "CODE"){
				popParam.push(srchValue);
			}else {
				popParam.push('');
				popParam.push(srchValue);
			}
			popUrl = "/WMSCM011.action?func=fn_setWMSWC002&custType=12&stBtnYn=N&strParameter="+popParam;
			
			break;
	}
	
	if( srchValue != "" ){
		var param   = "srchKey=" + srchKey + "&"+srchParamName+"=" + srchValue;
		var url     = "/selectMngCode.action"
		jQuery.post(url, param, function(data){
			var data = data[srchKey];
			var cnt  = data.size();
			if( cnt > 1 ){
				var width  = "800";
				var height = "530"
				var url    =popUrl;
				var asppop = cfn_openPop2(url, "fn_Popup", width, height);
				asppop.focus();
			}
			else if( cnt == 1 ){
				jQuery('form[name="frm_list'+tab+'"] input[name="'+id+'"]').val(data[0].ID);
				jQuery('form[name="frm_list'+tab+'"] input[name="'+cd+'"]').val(data[0].CODE);
				jQuery('form[name="frm_list'+tab+'"] input[name="'+nm+'"]').val(data[0].NAME);
			}
			else if( cnt == 0 ){
				alert("$!lang.getText('list.nodata')");
				jQuery('form[name="frm_list'+tab+'"] input[name="'+id+'"]').val("");
				jQuery('form[name="frm_list'+tab+'"] input[name="'+cd+'"]').val("");
				jQuery('form[name="frm_list'+tab+'"] input[name="'+nm+'"]').val("");
			}
		}).fail(function(xhr, textStatus){
    		if(xhr.status == 404){
    			location.href=('/index.html');
    		}
     	});
	}else{
		jQuery('form[name="frm_list'+tab+'"] input[name="'+id+'"]').val("");
		jQuery('form[name="frm_list'+tab+'"] input[name="'+cd+'"]').val("");
		jQuery('form[name="frm_list'+tab+'"] input[name="'+nm+'"]').val("");
	}
}

function getOliveSrtCodeList() {
    jQuery.get('/WMSWC002/getSrtCdList.action', function(data) {
		if(data?.errCnt == "1"){
			alert(data.MSG);
			return;
		}
        var options = data.SRT_CD_LIST;
        var selected = jQuery('select[name="vrSrchStrCd"]');
    
        selected.find('option:not(:first)').remove();
    
        if (options && options.length > 0) {
            options.forEach(function(item) {
                var option = jQuery('<option></option>')
                    .attr('value', item.SRT_CD)
                    .text(item.SRT_NM);
				selected.append(option);
            });
        } else {
            alert("데이터가 없습니다.");
        }
    }).fail(function(xhr, textStatus) {
        alert("$!lang.getText('처리 중 오류.')"+"($!lang.getText('관리자 문의. (조회 오류)'))");
    });
}

function updateSelectedCount(table, tableId, field) {
    table.on("rowSelectionChanged", function(data, rows) {
        let element = jQuery(`#${tableId} .tabulator-footer [tabulator-field=${field}]`);
        element.html(`선택된 건수: ${data.length}`);
    });
}