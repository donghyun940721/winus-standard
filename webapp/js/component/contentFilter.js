/**
 * UI 공통 Compoent
 * 
 * CODE/ID/NAME 으로 구성된 컴포넌트 셋에 대한 인스턴스 생성
 * Changed 이벤트를 통한 키워드 검색기능 제공 (Service 호출)
 * 
 * !필수 정의요소
 * UI       : 'COMP_CATEGORY', 'COMP_PARAM_MAP'
 * SERVICE  : 'CmBeanServiceImpl.java(selectMngCode)' 외 각 호출 쿼리
 * 
 */
export class Component {

    #code       = null;     // 태그 ID값
    #id         = null;     // 태그 ID값
    #name       = null;     // 태그 ID값
    #category   = null;     // 'COMP_CATEGORY'에 정의된 구분값


    get Code()      { return this.#code;         }
    get Id()        { return this.#id;           }
    get Name()      { return this.#name;         }
    get Category()  { return this.#category;     }


    constructor(args){

        if(args != undefined){

            this.#code          = "code"        in args     ?    args.code         : "";
            this.#id            = "id"          in args     ?    args.id           : "";
            this.#name          = "name"        in args     ?    args.name         : "";
            this.#category      = "category"    in args     ?    args.category     : "";

        }

    }


    Init () {
        
        jQuery(`#${this.#code}`).val("");
        jQuery(`#${this.#id}`).val("");
        jQuery(`#${this.#name}`).val("");

    }


    /**
     * 
     * @param {String} code 
     * @param {String} id 
     * @param {String} name 
     */
    SetValue(code, id, name) {

        jQuery(`#${this.#code}`).val(code);
        jQuery(`#${this.#id}`).val(id);
        jQuery(`#${this.#name}`).val(name);

    }


    /**
     * 컴포넌트의 타입에 맞는 값 획득
     * 
     * @param {*} type : COMP_VALUE_TYPE 상수값
     * @returns 
     */
    GetValue(type) {

        let value = null;

        switch(type)
        {
            case COMP_VALUE_TYPE.CODE : 
                value = jQuery(`#${this.#code}`).val();
                break;

            case COMP_VALUE_TYPE.ID : 
                value = jQuery(`#${this.#id}`).val();
                break;

            case COMP_VALUE_TYPE.NAME : 
                value = jQuery(`#${this.#name}`).val();
                break;
        }

        return value;
    }


    /**
     * 키워드 검색
     * 
     * @param {String} type         : 코드 혹은 명칭 (COMP_VALUE_TYPE)
     * @param {String} refValue     : 입력값
     * @param {Object} param        : 추가 변수
     */
    Search(type=COMP_VALUE_TYPE.CODE, refValue='', param) {

        const vm = this;
        const refKey = COMP_PARAM_MAP.get(this.#category)[type];
        let formData  = new FormData();

        formData.append('srchKey', this.#category);
        formData.append(refKey, refValue);

        if(param != undefined || param != null){
            
            for(const key of Object.keys(param)){
                formData.append(key, param[key]);
            }
            
        }


        return new Promise((resolve, reject)=>{

            jQuery.ajax({
                url : "/selectMngCode.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(result){

                    // TODO :: Service에서 동일 명칭으로 리턴됨. (Service 반환Key 수정 필요)
                    let type = (vm.#category == COMP_CATEGORY.USER2) ? COMP_CATEGORY.USER : vm.#category;

                    result = result[type];

                    // 값 없음.
                    if(result == null || result.length == 0) { 
                        vm.Init();
                        reject(result);
                    }
                    // 단일 검색결과 획득 시 즉시 적용
                    else if(result.length == 1){
                        vm.SetValue(
                            result[0][COMP_VALUE_TYPE.CODE], 
                            result[0][COMP_VALUE_TYPE.ID], 
                            result[0][COMP_VALUE_TYPE.NAME] ?? result[0][COMP_VALUE_TYPE.CODE]
                        )
                    }
    
                    resolve(result);
                },
                error : function(xhr, textStatus, err){
                    reject(err);
                }

            });

        });
    }


    GetCustomProperty(category){

        switch(category){

            case COMP_CATEGORY.CUST : 
                break;

        }

    }

}


export const Utility = (function(){


    const GetData = (category) => {

        const url      = '/selectMngCode.action';
        let param      = `srchKey=${category}`;

        return jQuery.post(url, param);
    }


    return {
        GetData : GetData
    }

})();


/**
 * Component 입/출력 구분
 * 
 */
export const COMP_VALUE_TYPE = {
    'CODE'  : 'CODE',
    'ID'    : 'ID',
    'NAME'  : 'NAME',
}


/**
 * Component 카테고리 (조회항목)
 * 
 */
export const COMP_CATEGORY = {
    'CAR'        : 'CAR',
    'CENTER'     : 'CENTER',
    'CLIENT'     : 'CLIENT',
    'CUST'       : 'CUST',
    'CUST_ZONE'  : 'CUST_ZONE',
    'STORE'      : 'STORE',
    'DEPT'       : 'DEPT',
    'DOCK'       : 'DOCK',
    'EMPLOY'     : 'EMPLOY',
    'ITEM'       : 'ITEM',
    'LOCATION'   : 'LOCATION',
    'UOM'        : 'UOM',
    'USER'       : 'USER',
    'USER2'      : 'USER2',
    'WH'         : 'WH',
    'ZONE'       : 'ZONE',
    'POOL'       : 'POOL',
    'ITEMGRP'    : 'ITEMGRP',
    'ADMIN'      : 'ADMIN',
    'DLVCUST'    : 'DLVCUST',
    'PARTNER'    : 'PARTNER',
    'TEL'        : 'TEL',
    'ITEMIF'     : 'ITEMIF',
    'DRIVER'     : 'DRIVER',
    'COUNTRY'    : 'COUNTRY'
}


/**
 * Component 맵핑정보 
 * Service 호출 시, MyBatis 매개변수명과의 맵핑
 * 
 * (CmBeanController.java 참조)
 * 
 */
export const COMP_PARAM_MAP = new Map([

    ['CUST'         , {'CODE' : 'vrSrchCustCd'  , 'NAME' : 'vrSrchCustNm'}]
    , ['STORE'      , {'CODE' : 'vrSrchCustCd'  , 'NAME' : 'vrSrchCustNm'}]
    , ['WH'         , {'CODE' : 'vrSrchWhCd'    , 'NAME' : 'vrSrchWhNm'}]
    , ['LOCATION'   , {'CODE' : 'vrSrchLocCd'   , 'NAME' : 'vrSrchLocCd'}]     // 로케이션 명칭 X
    , ['ITEM'       , {'CODE' : 'vrSrchItemCd'  , 'NAME' : 'vrSrchItemNm'}]
    , ['USER2'      , {'CODE' : 'vrSrchUserNo'  , 'NAME' : 'vrSrchUserNm'}]
    
]);


export class ItemGrpSelectBox {

	#firstId	= null;		//대분류 jQuery 검색 ID
	#secondId	= null;		//중분류 jQuery 검색 ID
	#thirdId	= null;		//소분류 jQuery 검색 ID

	#firstList = null;		//대분류 List
	#secondList = null;		//중분류 List
	#thirdList = null;		//소분류 List

	#fistIdx = -1;			//선택된 대분류 Index
	#secondIdx = -1;		//선택된 중분류 Index
	#thirdIdx = -1;			//선택된 소분류 Index

    get firstList()      { return this.#firstList;         }
    get secondList()     { return this.#secondList;        }
    get thirdList()      { return this.#thirdList;         }
    
    constructor(args){
        if(args != undefined){
            this.#firstId          = "firstId"        in args     ?    args.firstId         : "";
            this.#secondId         = "secondId"       in args     ?    args.secondId        : "";
            this.#thirdId          = "thirdId"        in args     ?    args.thirdId         : "";
        }
    }

    Init () {
        jQuery(`#${this.#firstId}`).html("<option value=''>전체</option>");
        jQuery(`#${this.#secondId}`).html("<option value=''>전체</option>");
        jQuery(`#${this.#thirdId}`).html("<option value=''>전체</option>");

		this.getGrpId(1);
    }

	/**
     * 
     * @param {Int} level 
	 * @param {String} upItemGrpId 
	 */
	getGrpId (level, upItemGrpId){
		let vm = this;
		let formData  = new FormData();
		formData.append('vrGrpLevel',level);
		formData.append('vrUpItemGrpId',upItemGrpId);
		return new Promise((resolve, reject)=>{
            jQuery.ajax({
                url : "/WMSMS094/selectItemGrpLvl.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(result){
					console.log(result);
                    // 값 없음.
                    if(result == null) { 
                        vm.Init();
                        reject(result);
                    }
                    else{
                        vm.setValue(level,result.rows);
                    }
                    resolve(result);
                },
                error : function(xhr, textStatus, err){
                    reject(err);
                }

            });

        });
	}

    setValue (level, rows){
        let vm = this;
        switch(level){
            case 1 :
                jQuery(`#${this.#firstId}`).html(this.setSelectBox(rows)).off().on("change", (e)=>{
                    vm.getGrpId(2,jQuery(`#${vm.#firstId} option:selected`).val());
                });
                jQuery(`#${this.#secondId}`).html("<option value=''>전체</option>").off();
                jQuery(`#${this.#thirdId}`).html("<option value=''>전체</option>");
                break;
            case 2 :
                jQuery(`#${this.#secondId}`).html(this.setSelectBox(rows)).off().on("change", (e)=>{
                    vm.getGrpId(3,jQuery(`#${vm.#secondId} option:selected`).val());
                });
                jQuery(`#${this.#thirdId}`).html("<option value=''>전체</option>");
                break;
            case 3 :
                jQuery(`#${this.#thirdId}`).html(this.setSelectBox(rows));
                break;
        }
    }

    setSelectBox(rows){
        let htmlStr = `<option value=''>전체</option>`;
        for(var obj of rows){
            htmlStr+=`<option value='${obj?.CODE_CD}'>${obj?.CODE_NM}</option>`
        }
        return htmlStr; 
    } 
} 

/**
 * MultiSelectBox selectList Object용 Class
 * getter, setter 사용 
 */
class selectBoxObject{
    #value      = null;
    #name       = null;

    get value() {return this.#value;}
    get name()  {return this.#name;}

    set value(value) {this.#value = value;}
    set name(name) {this.#name = name;}

    constructor(args){
        if(args != undefined){
            this.#value = "value" in args ? args.value : "";
            this.#name = "name" in args ? args.name : "";
        }
    }
}

/**
 * MultiSelectBox 다중 selectBox 용 export Class
 * 
 */
export class MultiSelectBox {

    #url            = null;
    #form           = null;
    /**
     * @params {SerachedResultData} url 로 검색된 resultData
     * @returns {[{value:String, name:String}]} Array<Object> [{value:String , name:String}]
     */
    #fn_sucess      = null;     

    #selectList  = null;     //Array<selectBoxObject>

    #htmlObjId    =   null;   // html object id (tag : select)

    get selectList()     { return this.#selectList;           }
    get form()           { return this.#form;                 }

    set form(form) { this.#form = form; }
   
    set selectList(selectListArray) { 
        var ListArray = new Array();
        for (var obj of selectListArray){
            ListArray.push(
                new selectBoxObject({
                    value : obj.value,
                    name : obj.name
                })
            );
        }
        this.#selectList = ListArray;

    }

    constructor(args){
        if(args != undefined){
            this.#htmlObjId            = "htmlObjId"       in args     ?    args.htmlObjId        : "";

            this.#url                  = "url"             in args     ?    args.url              : "";
            this.#form                 = "form"            in args     ?    args.form             : null;
            this.#fn_sucess            = "fn_sucess"       in args     ?    args.fn_sucess        : null;

        }
    }

    Init () {
        jQuery(`#${this.#htmlObjId}`).select2().val(null).trigger("change");
	    jQuery(`#${this.#htmlObjId}`).empty();

        if (this.#url != null){
            this.setSelectListSearch();
        }
    }

    /**
     * url, form, fn_sucess select박스에 들어갈 option 검색
     * 
     * @param 
     * @returns 
     */
     setSelectListSearch (){
		let vm = this;
		let formData  = this.#form;
        let url = this.#url;
        let fn_sucess = this.#fn_sucess;
		return new Promise((resolve, reject)=>{
            jQuery.ajax({
                url : url,
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(result){
                    // 값 없음.
                    if(result == null) { 
                        reject(result);
                    }
                    else {
                        vm.selectList = fn_sucess(result);
                        vm.setSelectList();
                    }
                    resolve(result);
                },
                error : function(xhr, textStatus, err){
                    reject(err);
                }
            });
        });
	}

    /**
     * selectList의 값으로 select Box Option 재지정
     * 
     * @param 
     * @returns 
     */
    setSelectList(){
        jQuery(`#${this.#htmlObjId}`).select2().val(null).trigger("change");
	    jQuery(`#${this.#htmlObjId}`).empty();
        let arr = new Array();
        arr.push({id: "" , text: "전체"});

        for (var i = 0; i < this.#selectList.length ; i++){
            arr.push({id:this.#selectList[i].value, text : this.#selectList[i].name});
        }

        jQuery(`#${this.#htmlObjId}`).select2({data : arr});

    }

    /**
     * 선택된 데이터 return.
     * count : 선택된 데이터 수,
     * data : 선택된 데이터 ID 배열
     * 
     * @param 
     * @returns {[{count : int, data : Array<String>}]}
     */
    getSelectedListItem(){
        let result = {
            count : jQuery(`#${this.#htmlObjId} :selected`).length
            , data : jQuery(`#${this.#htmlObjId}`).val()
        };
        return result;
    }






}


// export {Component, Utility, COMP_VALUE_TYPE, COMP_CATEGORY, COMP_PARAM_MAP}