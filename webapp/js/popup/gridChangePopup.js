import { WinusGrid } from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }  from '../utility/userStorageManger.js';


jQuery(document).ready(function(){

    console.log('Start Changed Pop...');

    let view = new ChangeGridView();
});


class ChangeGridView {

    #custId = null;
    #gridTemplate = null;

    #headerMappingInfo = [
        {
            name: '항목순번',
            code: 'SEQ_SORT',
            startColumnKey: 'COLUMN_SEQ',
            endColumnKey: 'DOWN'
        }
    ]

    #grid = new WinusGrid({
        spdListDiv      : "spdList",
        gridTemplate    : "ChangeGridInfos",
        rowHeight       : 27,
        headerHeight    : 22,
        frozenColIdx    : 10,
        setColor        : false,             // Cell 단위 설정 배경색 적용
        headerMappingInfo : this.#headerMappingInfo
    });


    constructor(args) {

        this.Init();
        this.AddEvent();

        this.#grid.Init();
        this.#grid.SetGrid();

        if(this.#grid.created){
            this.Search();
        }
    }


    Init(){
        let vm = this;

        let title = `${LocalStorageUtil.GetValue('TAB_NM')} (${LocalStorageUtil.GetValue('LC_NM')})`;

        document.querySelector('#gridName').innerHTML = title;
        this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        this.#gridTemplate = LocalStorageUtil.GetValue('GRID_TEMPLATE');

        
        if(this.#custId == null || this.#gridTemplate == null){
            alert(`화주, Grid설정값 누락 (관리자 문의)`);
            return ;
        }
    }


    AddEvent(){

        let vm = this;

        document.getElementById("btnSave").addEventListener('click', (e)=>{
            this.SaveClickEventCallback(e);
        });


        this.#grid.CellEditedCustomEvent = function(sender, args){
            vm.#grid.SetCheckedUnitRow(args.row, args.col, true);
        }

        this.#grid.ButtonClickCustomEvent = function(sender, args){

            let rowIdx = args.row;
            let colIdx = args.col;

            switch(colIdx){
                case vm.#grid.GetColumnIdx('UP') : 
                    if(rowIdx > 0 ){
                        vm.RowUp(rowIdx);
                    }
                    break;

                case vm.#grid.GetColumnIdx('DOWN') : 
                    if(rowIdx < (vm.#grid.GetRowCount() -1)){
                        vm.RowDown(rowIdx);
                    }
                    break;
            }
        }
    }


    Search(){

        let vm = this;

        var param	= `vrSrchCustCd=${this.#custId}&vrTemplateType=${this.#gridTemplate}`;
        var url 	= "/WMSCM300/list.action?"+param;

        jQuery.ajax({
            url : url,
            type : 'GET',
            contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
            dataType : 'html',
            beforeSend : function() {
                cfn_viewProgress();
            },
            complete : function() {
                cfn_closeProgress();
            },
            error : function(_, textStatus, errorThrown) {
                alert("$!lang.getText('처리 중 오류.')" + "($!lang.getText('관리자 문의. (조회 오류)'))");
            },
            success : function(data) {
                vm.#grid.SetData(JSON.parse(data));
            }
        });
    }

    
    SaveClickEventCallback(e) {
        
        let vm = this;

        if(confirm(DICTIONARY['confirm_save'])){

            let gridData = this.#grid.GetSelectedRowData();
            let formData = new FormData();
    
            formData.append('selectIds', gridData.length);
    
            for(let i=0; i < gridData.length; i++)
            {
                formData.append(`vrSrchCustCd${i}`          , this.#custId);
                formData.append(`id${i}`                    , gridData[i]['ID']);
                formData.append(`templateId${i}`            , gridData[i]['TEMPLATE_ID']);
                formData.append(`columnCode${i}`            , gridData[i]['COLUMN_CODE']);
                formData.append(`columnName${i}`            , gridData[i]['COLUMN_NAME']);
                formData.append(`columnType${i}`            , gridData[i]['COLUMN_TYPE']);
                formData.append(`columnSeq${i}`             , gridData[i]['COLUMN_SEQ']);
                formData.append(`columnSize${i}`            , gridData[i]['COLUMN_SIZE']);
                formData.append(`columnColor${i}`           , gridData[i]['COLUMN_COLOR']);
                formData.append(`columnReadOnly${i}`        , gridData[i]['COLUMN_READ_ONLY']);
                formData.append(`columnVisible${i}`         , gridData[i]['COLUMN_VISIBLE']);
                formData.append(`columnAutoLineChange${i}`  , gridData[i]['COLUMN_AUTO_LINE_CHANGE']);
                formData.append(`columnAlign${i}`           , gridData[i]['COLUMN_ALIGN']);
            }
            
            console.log('Processing Save...');
    
            jQuery.ajax({
                url : '/WMSCM300/save.action',
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                    vm.Search();
                },
                success : function(data){
                    alert(DICTIONARY['save_success']);
                    LocalStorageUtil.Input('REFRESH', 'Y');
                }
            });

        }
    }


    RowUp(row){

        let spdList         = this.#grid.spdList;
        let seqColumnIdx    = this.#grid.GetColumnIdx('COLUMN_SEQ');
        let upColumnIdx     = this.#grid.GetColumnIdx('UP');

        //seq와 바꿔주고
        //오름차순정렬
        var fromSeq = spdList.getValue(row, seqColumnIdx);
        var toSeq   = spdList.getValue(row - 1, seqColumnIdx);
        spdList.setValue(row, seqColumnIdx, toSeq);
        spdList.setValue(row - 1, seqColumnIdx, fromSeq);
        
        var sortInfo = [ { index: seqColumnIdx, ascending: true } ] ;
        spdList.sortRange(row - 1, 0, 2, spdList.getColumnCount(), true, sortInfo);
        spdList.setActiveCell(row - 1, upColumnIdx);

        this.#grid.SetCheckedUnitRow(row, null, true);
        this.#grid.SetCheckedUnitRow(row - 1, null, true);
        this.#grid.targetRow = row - 1;
        this.#grid.FocusedRowHightLight();
    }
    
    
    RowDown(row){

        let spdList         = this.#grid.spdList;
        let seqColumnIdx    = this.#grid.GetColumnIdx('COLUMN_SEQ');
        let downColumnIdx   = this.#grid.GetColumnIdx('DOWN');

        var fromSeq = spdList.getValue(row, seqColumnIdx);
        var toSeq   = spdList.getValue(row + 1, seqColumnIdx);
        spdList.setValue(row, seqColumnIdx, toSeq);
        spdList.setValue(row + 1, seqColumnIdx, fromSeq);
        
        var sortInfo = [ { index: seqColumnIdx, ascending: true } ];
        spdList.sortRange(row, 0, 2, spdList.getColumnCount(), true, sortInfo);
        spdList.setActiveCell(row + 1, downColumnIdx);

        this.#grid.SetCheckedUnitRow(row, null, true);
        this.#grid.SetCheckedUnitRow(row + 1, null, true);
        this.#grid.targetRow = row + 1;
        this.#grid.FocusedRowHightLight();
    }
}