import * as InterfaceUtil from '../utility/interfaceUtil.js';

const OUT_ORDER_COLLECT_POP = "popOutOrderCollect";
const OUT_ORDER_REGIST_POP = "popOutOrderRegist";


/**
 * 주문정보 수집
 */
export const OutOrderCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        jQuery("#vrColStDateFromE2").val(localStorage.LocalTime);
        jQuery("#vrColStDateToE2").val(localStorage.LocalTime);

        AddEvent();
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE2").off().on("click", (e)=>{
            OrderCollect();
        });
    }


    /**
     * 매입처정보 수집 I/F 실행 
     * 
     */
    function OrderCollect(){
        
        let formData        = new FormData(document.getElementById(`form709E2_1pop`));
        // const collectType   = jQuery("#vrCollectTypeE2").val();

        formData.append('CUST_ID'           , custId     );
        formData.append('ORDER_TYPE'        , 2);       // 매입처(입고: 1), 매출처(출고: 2)

        formData.append('Z_TOKEN'           , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'TOKEN1'));
        formData.append('Z_FROM'            , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID2'));
        formData.append('Z_TO'              , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID3'));
        
        formData.append("TRANS_DATE1"       , jQuery("#vrColStDateFromE2").val());
        formData.append("TRANS_DATE2"       , jQuery("#vrColStDateToE2").val());
        formData.append("VenderCode"        , jQuery("#vrtxtSeller").val());


        jQuery.ajax({
            url : "/WMSIF709/collectOrder.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${OUT_ORDER_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){

                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                
                alert(`저장되었습니다.`);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });

    }
    

    return {
        Init : Init
    }

})();


/**
 * 주문등록
 * 
 */
export const OutOrderRegist = (function(){

    let opener = null;
    let custId = null;
    let masterSeq = null;
    let sendData = new FormData();

    
    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        jQuery("#vrOrdOutDtE2_1").val(localStorage.LocalTime);
        opener.GetCustOrdDegree(true, "vrPopOrdDegreeE2_1", jQuery("#vrOrdOutDtE2_1").val());

        AddEvent();
    }


    let SetData = function(data){
        sendData = data;
    }


    let AddEvent = function(){
        
        // "출고예정일" 날짜변경 이벤트
        jQuery("#vrOrdOutDtE2_1").off().on("change", (e)=>{
            opener.GetCustOrdDegree(true, "vrPopOrdDegreeE2_1", jQuery("#vrOrdOutDtE2_1").val());
        });

        // "등록" 버튼 클릭 이벤트
        jQuery("#btnSaveE2_1").off().on("click", (e)=>{
            SaveData();
        });
    }


    const SaveData = function(){

        try{

            const ordOutDt    = jQuery("#vrOrdOutDtE2_1").val()       ?? null;
            const ordDegree   = jQuery("#vrPopOrdDegreeE2_1").val()   ?? null;

            sendData.append('ordOutDt', ordOutDt );
            sendData.append('ordDegree', ordDegree );

            jQuery.ajax({
                url : "/WMSIF709/saveE2.action",
                type : 'post',
                data : sendData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    
                    jQuery(`#${OUT_ORDER_REGIST_POP}`).bPopup().close();
    
                    // 호출객체 CallBack 함수 수행 (차수갱신, 재조회)
                    if(opener == null) return ;

                    jQuery.when(
                            opener.GetCustOrdDegree(false), 
                            opener.SearchClickEventCallback(null, false, false))
                            .done(function(rtn1, rtn2){
                                cfn_closeProgress();
                            }
                    );
                },
                success : function(data){
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(data["MSG"] + "\n"+data["MSG_ORA"].split("Exception:")[1]);
                    }
                    else{
                        alert(DICTIONARY['save_success'] + "\n(주문차수가 갱신됩니다.)"  + "\n(주문 목록이 재조회됩니다.)");
                    }
                },
                error: function(request, status, error){
                    alert(`관리자에게 문의바랍니다. (${request.status})`);
                    console.log(request);
                    console.log(status);
                    console.log(error);
                }
            });

        } catch(e) {
            alert(e);
            cfn_closeProgress();
            return;
        }
        
    }


    return {
        Init : Init,
        SetData : SetData
    }

})();
