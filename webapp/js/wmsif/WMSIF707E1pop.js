import * as InterfaceUtil from '../utility/interfaceUtil.js';


const ORD_COLLECT_POP   = "popOrdCollect";
const DLV_SEND_POP      = "popDlvSend";
const ORD_REGISTER_POP  = "popRegOrd";


/**
 * 주문정보 수집
 */
export let OrderCollect = (function(){

    let opener = null;              // 호출자 객체
    let custId = null;
    let masterSeq = null;
    let salesCompList = new Array();
    let selectedSalesCom = null;

    let Init = function(args){

        opener              = "OPENER" in args ? args['OPENER'] : "";
        custId              = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq           = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";
        salesCompList       = "SALES_COMPANY" in args ? args['SALES_COMPANY'] : [];
        selectedSalesCom    = "SELECTED_SALES_COM" in args ? args['SELECTED_SALES_COM'] : [];

        jQuery("#vrColStDateFrom").val(localStorage.LocalTime);
        jQuery("#vrColStDateTo").val(localStorage.LocalTime);

        jQuery('#vrSalesComp option').remove();
        jQuery('#vrSalesComp').append(new Option('전체', 'A', true, true));
        for(let salesInfo of salesCompList){

            let option = new Option();

            option.value    = salesInfo.CUST_CD;
            option.text     = salesInfo.CUST_NM;
            
            if(selectedSalesCom == salesInfo.CUST_CD){
                option.selected = true;
            }

            jQuery('#vrSalesComp').append(option);
        }


        AddEvent();
    }


    let AddEvent = function(){
        jQuery("#btnCollect").off().on("click", (e)=>{
            fn_OrdCollect();
        });
    }


    /**
     * [이지어드민] 주문정보 수집 I/F 실행 
     * 
     */
    function fn_OrdCollect(){

        try {

            let formData    = new FormData(document.getElementById(`form707E1_1`));

            let partnerKey  = InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_KEY1');
            let domainKey   = InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_KEY2');
            
            let action      = 'get_order_info';
            let limit       = '500';            // Max Fix
            let page        = '1';              // Fix
            let isCancel    = jQuery('#vrDeleteOrdYn').prop("checked") ? "1" : "0";
            let shopId      = (jQuery('#vrSalesComp').val() == 'A') ? '' : jQuery('#vrSalesComp').val();

            formData.append('CUST_ID'      ,   custId     );
            formData.append('partner_key'  ,   partnerKey );
            formData.append('domain_key'   ,   domainKey  );
            formData.append('action'       ,   action     );
            formData.append('shop_id'      ,   shopId     );
            formData.append('is_cancel'    ,   isCancel   );
            formData.append('limit'        ,   limit      );
            formData.append('page'         ,   page       );
            

            let status = formData.get('vrOrdStatus');


            /** 커스텀 옵션 */
            switch(status)
            {
                // 전체
                case "A" : 
                    formData.delete('vrOrdStatus')
                    formData.append('vrOrdStatus', '0,1,2,3,4,5,6,7,8');

                    break;

                // '정상+교환' : CS 정상 건과 배송 유무에 관계없는 모든 교환 건
                case '9' : 
                    formData.delete('vrOrdStatus')
                    formData.append('vrOrdStatus', '0,5,6,7,8');
                    
                    break;

                // 취소 (전체)
                case '10' :
                    formData.delete('vrOrdStatus')
                    formData.append('vrOrdStatus', '1,2,3,4');

                    break;
            }

            console.log(`(Start) Order Collect ... ${new Date()}`);

            jQuery.ajax({
                url : "/WMSIF707/OrderCollect.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                    jQuery(`#${ORD_COLLECT_POP}`).bPopup().close();     // 팝업닫기

                    // 호출객체 CallBack 함수 수행
                    if(!(opener == null)){
                        opener.SearchClickEventCallback();
                    }
                },
                success : function(data){

                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(`관리자에게 문의바랍니다. (${data["MSG"]})`);
                    }
                    // Success ...
                    else{
                        let successCnt  = JSON.parse(data.message)['success'] ?? 0;
                        let failCnt     = JSON.parse(data.message)['fail'] ?? 0;

                        alert(DICTIONARY['save_success'] + ` (성공: ${successCnt}건, 실패: ${failCnt}건)` + `\n수집주문을 재조회합니다.`);
                        console.log(`(End) Order Collect ... ${new Date()}`);
                    }

                },
                error: function(request, status, error){
                    alert(`관리자에게 문의바랍니다. (${request.status})`);
                }
            });
            
        } catch (error) {
            alert(`관리자에게 문의바랍니다. (${error})`);
        }
    }


    return {
        Init :Init
    }

})();


/**
 * 택배송장 등록
 */
export let DlvSend = (function(){

    let opener = null;              // 호출자 객체
    let custId = null;              // 화주 ID
    let masterSeq = null;           // 오픈몰계정 ID
    let sendData = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();
    }


    let AddEvent = function(){
        jQuery("#btnSend").off().on("click", (e)=>{
            DlvSendEventCallBack();
        });
    }


    /**
     * 송장등록을 진행할 대상 데이터셋 바인딩
     * 
     * @param {FormData} data : FormData 기반에 Grid의 선택행 데이터가 병합된 인자
     */
    let SetData = function(data){
        sendData = data;
    }


    let DlvSendEventCallBack = function(){

        if(sendData == null){
            return ;
        }

        const errorInfo   = {
            'invalid trans_no'                     : '송장번호 없음.',
            'trans_no already exists'              : '송장번호가 같은 송장/배송 상태 주문이 있는 경우',
            // 'prd_seq is invalid variable'          : 'prd_seq 파라미터가 있는 경우',
            'invalid status'                       : '주문이 발주/배송 상태인 경우, 합포 건 중 배송된 건이 있는 경우',
            'invalid trans_corp'                   : '택배사가 없는 경우',
            'update error'                         : '주문 데이터 업데이트 중 오류 발생한 경우',
            // 'trans_pos'                            : '파라미터 1로 입력된 경우 송장입력 완료 후 아래 메시지 나올 수 있습니다.',
            '배송처리가능한 상품이 없습니다'         : '같은 송장번호인 주문들에 취소되지 않은 상품이 없는 경우',
            'hold status'                          : '주문 보류 상태인 경우',
            'canceled order'                       : '같은 송장번호인 주문들 전체 취소 주문인 경우',
        }
        let formData    = new FormData(document.getElementById(`form707E1_2`));

        let partnerKey  = InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_KEY1');
        let domainKey   = InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_KEY2');
        let action      = 'set_trans_no';

        formData.append('CUST_ID'      ,   custId     );
        formData.append('partner_key'  ,   partnerKey );
        formData.append('domain_key'   ,   domainKey  );
        formData.append('action'       ,   action     );
        formData.append('gridData'     ,   JSON.stringify(sendData));

        jQuery.ajax({
            url : "/WMSIF707/registInvoice.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(args){
                cfn_closeProgress();
                jQuery(`#${DLV_SEND_POP}`).bPopup().close();     // 팝업닫기

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){

                // Fail
                if(!cfn_isEmpty(data) && ('header' in data) && (data['header'] == 'ERROR')){
                    let message         = null;
                    let errorMessage    = data.message.split('/')[0];

                    if(errorMessage in errorInfo){
                        message = errorInfo[errorMessage];
                    }

                    alert(`택배송장등록 실패 \n(EzAdmin : ${data['message']})\n(내용 : ${message})`);
                }

                // Success ...
                else{
                    alert(DICTIONARY['save_success']);
                }
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status})`);
                console.log(request);
                console.log(status);
                console.log(error);
            }
        });
    }


    return {
        Init : Init,
        SetData : SetData
    }
})();


/** 
 * 주문등록
 */
export let OrderRegister = (function(){

    let opener = null;              // 호출자 객체
    let custId = null;              // 화주 ID
    let masterSeq = null;           // 오픈몰계정 ID
    let dataSet = new Array();


    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        jQuery("#vrOrdOutDt").val(localStorage.LocalTime);
        opener.GetCustOrdDegree(true, "vrPopOrdDegree", jQuery("#vrOrdOutDt").val());

        AddEvent();
    }


    /**
     * Grid 및 Filter 조건 FormData 전달
     * 
     * @param {FormData} data : FormData 기반에 Grid의 선택행 데이터가 병합된 인자
     */
    let SetData = function(data){
        dataSet = data;
    }


    let AddEvent = function(){
        
        // "출고예정일" 날짜변경 이벤트
        jQuery("#vrOrdOutDt").off().on("change", (e)=>{
            opener.GetCustOrdDegree(true, "vrPopOrdDegree", jQuery("#vrOrdOutDt").val());
        });

        // "등록" 버튼 클릭 이벤트
        jQuery("#btnSave").off().on("click", (e)=>{
            SaveData();
        });
    }


    /**
     * 주문등록
     * 
     * @param {*} sendData : 주문등록 대상 데이터
     * @returns 
     */
    let OrderRegister = function(sendData){

        let ordOutDt    = jQuery("#vrOrdOutDt").val()       ?? null;
        let ordDegree   = jQuery("#vrPopOrdDegree").val()   ?? null;

        sendData.append('ordOutDt', ordOutDt);
        sendData.append('ordDegree', ordDegree);

        return new Promise((resolve, reject)=>{
            jQuery.ajax({
                url : "/WMSIF707/syncOrder.action",
                type : 'POST',
                data : sendData,
                contentType : false,
                processData : false,
                success : function(data){
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        reject(data["MSG"]);
                    }
                    // Success ...
                    else{
                        resolve(DICTIONARY['save_success'] + "\n(주문차수가 갱신됩니다.)"  + "\n(주문 목록이 재조회됩니다.)");
                    }
                },
                error: function(request, status, error){
                    
                    alert(`관리자에게 문의바랍니다. (${request.status})`);
                    console.log(request);
                    console.log(status);
                    console.log(error);

                    reject(`${request.status}`);
                }
            });
        }) ;
    }


    /**
     * 주문등록 작업단위 수행
     * 
     * @returns 
     */
    const SaveData = async function(){

        function* Worker(fnc, iter){
            for(const i of iter){
                yield i instanceof Promise ? i.then(fnc) : fnc(i);
            }
        }

        try{
            const workCnt = dataSet.length;       // 작업단위수
            let successCnt = 0;

            cfn_viewProgress();

            console.log('Start : Save Order Data ...');
            

            for await (const a of ( Worker(OrderRegister, dataSet) )){
                successCnt++;

                console.log(`Running : Save Order Data ... ${successCnt}`);

                if(workCnt == successCnt){
                    alert(a);

                    console.log('Finish : Save Order Data ...');
                    
                    // 차수, 목록 재조회
                    jQuery.when(opener.GetCustOrdDegree(false), opener.SearchClickEventCallback(null, false, false)).done(function(rtn1, rtn2){
                        console.log('Task All Finished ....');
                        console.log(`${opener.GetCustOrdDegree.name} : ${rtn1}`);
                        console.log(`${opener.SearchClickEventCallback.name} : ${rtn2}`);

                        cfn_closeProgress();
                        jQuery(`#${ORD_REGISTER_POP}`).bPopup().close();     // 팝업닫기

                        return;
                    });
                }
            }

        }catch(e){
            alert(e);
            cfn_closeProgress();
            return;
        }
    }


    return {
        Init : Init,
        SetData : SetData
    }

})();