import { WMSIF820E4 }  from './WMSIF820E4.js';
import { WMSIF820E5 }  from './WMSIF820E5.js';
import { WMSIF820E6 }  from './WMSIF820E6.js';


let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '5';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const IN_ORDER_RECEIVE	= '0';
const IN_ORDER_SEND 	= '1';
const OUT_ORDER_RECEIVE	= '2';
const OUT_ORDER_SEND	= '3';
const INVO_SEND	        = '4';
const WORK_BOARD	    = '5';


jQuery(document).ready(function() {

    let view01 = null;
    let view02 = null;
    let view03 = null;
    let view04 = new WMSIF820E4();
    let view05 = new WMSIF820E5();
    let view06 = new WMSIF820E6();

    const ViewContainer = [view01,view02, view03, view04, view05, view06];

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
     //ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    // ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){

        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;
        ViewContainer[activeTabIdx]?.ActiveViewEventCallBack();
        
        switch(activeTabIdx){
            case IN_ORDER_RECEIVE : break;
            case IN_ORDER_SEND : break;
            case OUT_ORDER_RECEIVE : break;
            case OUT_ORDER_SEND : break;
            case INVO_SEND : break;
            case WORK_BOARD : break;
        }

    });
});