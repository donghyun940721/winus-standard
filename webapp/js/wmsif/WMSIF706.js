import { WMSIF706E1 }   from './WMSIF706E1.js';

let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const ITEM_MASTER               = '0';

jQuery(document).ready(function() {

    let view01 = new WMSIF706E1();

    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });

    const ViewContainer = [view01];

    
    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
    ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();


    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){
        
        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        ViewContainer[activeTabIdx].ActiveViewEventCallBack();

        switch(activeTabIdx){
            case ITEM_MASTER : 
                break;
        }
    });

});