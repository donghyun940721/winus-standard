import * as InterfaceUtil from '../utility/interfaceUtil.js';


const ITEM_COLLECT_POP = "popItemCollect";
const IN_ORDER_COLLECT_POP = "popInOrderCollect";
const OUT_ORDER_COLLECT_POP = "popOutOrderCollect";

/**
 * 상품정보 수집
 */
export const ItemCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();    

        jQuery("#vrColStDateE1").val(localStorage.LocalTime);
        //jQuery("#vrColStDateFromE1").val(localStorage.LocalTime);
        //jQuery("#vrColStDateToE1").val(localStorage.LocalTime);
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE1").off().on("click", (e)=>{
            ItemCollect();
        });

        jQuery("#vrAllDayYnE1").off().on("click", (e)=>{
            document.getElementById('collectDatePickerE1').classList.toggle('act');
        });
    }


    /**
     * [Good-Md] 상품정보 수집 I/F 실행 
     * 
     */
    function ItemCollect(){

        let formData        = new FormData(document.getElementById(`form780E1_1pop`));
        //const collectType   = jQuery("#vrCollectTypeE1").val();

        formData.append('CUST_ID'           , custId     );
        formData.append("OPENMALL_CD"         , "GOODMD");
        formData.append("MOD_FROM_DATE"         , jQuery("#vrColStDateE1").val().replaceAll('-', ''));
        formData.append("MOD_TO_DATE"          , jQuery("#vrColStDateE1").val().replaceAll('-', ''));
        //formData.append("MOD_FROM_DATE"         , jQuery("#vrColStDateFromE1").val().replaceAll('-', ''));
        //formData.append("MOD_TO_DATE"          , jQuery("#vrColStDateToE1").val().replaceAll('-', ''));

        jQuery.ajax({
            url : "/WMSIF780/CollectItem.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${ITEM_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                console.log(data.RESULT);
                data = JSON.parse(data.RESULT);
                if(data.ErrCode != "0"){
                    alert(data.OutputMessage);
                }
                else{
                    alert(`저장되었습니다.`);
                }
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    return {
        Init : Init
    }
})()

export const InOrderCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();    

        jQuery("#vrColStDateE2").val(localStorage.LocalTime);
        //jQuery("#vrColStDateFromE2").val(localStorage.LocalTime);
        //jQuery("#vrColStDateToE2").val(localStorage.LocalTime);
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE2").off().on("click", (e)=>{
            InOrderCollect();
        });

        jQuery("#vrAllDayYnE2").off().on("click", (e)=>{
            document.getElementById('collectDatePickerE2').classList.toggle('act');
        });
    }


    /**
     * [Good-Md] 상품정보 수집 I/F 실행 
     * 
     */
    function InOrderCollect(){

        let formData        = new FormData(document.getElementById(`form780E2_1pop`));
        //const collectType   = jQuery("#vrCollectTypeE1").val();

        formData.append('CUST_ID'           , custId     );
        formData.append("OPENMALL_CD"         , "GOODMD");
        
        formData.append("MOD_FROM_DATE"         , jQuery("#vrColStDateE2").val().replaceAll('-', ''));
        formData.append("MOD_TO_DATE"          , jQuery("#vrColStDateE2").val().replaceAll('-', ''));
        //formData.append("MOD_FROM_DATE"         , jQuery("#vrColStDateFromE2").val().replaceAll('-', ''));
        //formData.append("MOD_TO_DATE"          , jQuery("#vrColStDateToE2").val().replaceAll('-', ''));

        jQuery.ajax({
            url : "/WMSIF780/CollectInOrder.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${IN_ORDER_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                console.log(data.RESULT);
                data = JSON.parse(data.RESULT);
                if(data.ErrCode != "0"){
                    alert(data.OutputMessage);
                }
                else{
                    alert(`저장되었습니다.`);
                }
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    return {
        Init : Init
    }
})();

export const OutOrderCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();    

        jQuery("#vrColStDateE3").val(localStorage.LocalTime);
        //jQuery("#vrColStDateFromE3").val(localStorage.LocalTime);
        //jQuery("#vrColStDateToE3").val(localStorage.LocalTime);
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE3").off().on("click", (e)=>{
            OutOrderCollect();
        });

        jQuery("#vrAllDayYnE3").off().on("click", (e)=>{
            document.getElementById('collectDatePickerE3').classList.toggle('act');
        });
    }


    /**
     * [Good-Md] 상품정보 수집 I/F 실행 
     * 
     */
    function OutOrderCollect(){

        let formData        = new FormData(document.getElementById(`form780E3_1pop`));
        //const collectType   = jQuery("#vrCollectTypeE1").val();

        formData.append('CUST_ID'           , custId     );
        formData.append("OPENMALL_CD"         , "GOODMD");

        formData.append("MOD_FROM_DATE"         , jQuery("#vrColStDateE3").val().replaceAll('-', ''));
        formData.append("MOD_TO_DATE"          , jQuery("#vrColStDateE3").val().replaceAll('-', ''));
        //formData.append("MOD_FROM_DATE"         , jQuery("#vrColStDateFromE3").val().replaceAll('-', ''));
        //formData.append("MOD_TO_DATE"          , jQuery("#vrColStDateToE3").val().replaceAll('-', ''));

        jQuery.ajax({
            url : "/WMSIF780/CollectOutOrder.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${OUT_ORDER_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                console.log(data.RESULT);
                data = JSON.parse(data.RESULT);
                if(data.ErrCode != "0"){
                    alert(data.OutputMessage);
                }
                else{
                    alert(`저장되었습니다.`);
                }
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    return {
        Init : Init
    }
})();