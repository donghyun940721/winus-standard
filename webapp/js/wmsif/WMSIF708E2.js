import * as CustomPopUp             from './WMSIF708E2pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSIF708E2 {

    #custId         = null;
    #formId         = 'frm_listE2';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE2',
        id          : 'vrSrchCustIdE2',
        name        : 'vrSrchCustNmE2',
        category    : COMP_CATEGORY.CUST
    });

    #compBuyer = new Component({
        code        : 'vrSrchBuyerCdE2',
        id          : 'vrSrchBuyerIdE2',
        name        : 'vrSrchBuyerNmE2',
        category    : COMP_CATEGORY.STORE
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: 'WINUS',
            code: 'WINUS_DT',
            startColumnKey: 'REG_DT',
            endColumnKey: 'UPD_NO'
        }
    ];
    
    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE2',
        gridTemplate        :   'WMSIF708E2',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        headerMappingInfo   :   this.#headerMappingInfo,
        setColor            :   false
    });

    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE2").datepicker("setDate", date);                         // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE2").datepicker("setDate", date);                           // 수집등록일자 (to)
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE2").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 거래처정보 (매입처)
        jQuery("#vrSrchBuyerCdE2").off().on("change", (e)=>{
            this.BuyerChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcBuyerImgE2").off().on("click", (e)=>{
            let param = {
                'LC_ID'             : jQuery("select[name='SVC_INFO']").val(),
                'TRANS_CUST_ID'     : this.#custId,
                'CUST_TYPE'         : '1'
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.STORE, param);
        });

        jQuery("#vrSrchBuyerNmE2").off().on("change", (e)=>{
            this.BuyerChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // *매입처정보수집 버튼 이벤트
        jQuery("#btnCollectBuyer").off().on("click", (e)=>{
            
            if(!cfn_isEmpty(this.#custId)){
                this.BuyerCollectClickEventCallBack(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // * "매입처등록" 버튼 클릭 이벤트
        jQuery("#btnRegBuyerE2").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.BuyerRegistClickEventCallBack();
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }

        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE2").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE2").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }

        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE2").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE2").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        /* 오픈몰 정보 설정*/
        this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE2", this.#formId, this.#custId);

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


    /**
     * 컨텍스트 메뉴 이벤트 콜백
     * 
     * (컨텍스트메뉴 사용 시 정의) 
     * @param {*} menuNm : 컨텍스트 메뉴명
     */
    ContextMenuEventCallBack(menuNm){

        alert( menuNm + ' Call ...');

    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE2", this.#formId, this.#custId);

                break;

            //* 거래처 (매입처 - 입고처)
            case COMP_CATEGORY.STORE :
                vm.#compBuyer.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];

                        vm.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE2", this.#formId, this.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async BuyerChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compBuyer.Init();
            return;
        }

        await this.#compBuyer.Search(type, refValue, {'vrOrdType' : '1'})
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
                        popParam['TRANS_CUST_ID'] = vm.#custId;
                        popParam['CUST_TYPE'] = '1';

                        StandardPopUp.call(vm, COMP_CATEGORY.STORE, popParam);
                    }
                })
                .catch((error)=>{
                    // alert(error);
                });
    }


    BuyerCollectClickEventCallBack(e){

        jQuery('#popBuyerCollect').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });
        
        CustomPopUp.BuyerCollect.Init({
            'OPENER' : this,
            'CUST_ID' : this.#custId,
            'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE2").val()
        });
    }


    /**
     * 매입처등록 클릭 이벤트 콜백함수
     * 
     * @param {*} e 
     */
    BuyerRegistClickEventCallBack(e){

        const vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let selectedData = new Array();
        let rowCnt = 0;

        selectedData = this.#grid.GetSelectedRowData();
        rowCnt = selectedData.length;

        if(confirm(DICTIONARY['confirm_save'])){

            formData.append('selectIds', rowCnt);
        
            for(let i = 0; i < rowCnt; i++){
    
                formData.append(`ST_GUBUN${i}`         , selectedData[i].SYNC_YN == 'Y' ? 'UPDATE' : 'INSERT'); // 작업형태(INSERT/UPDATE/DELETE)
    
                formData.append(`TRUST_CUST_ID${i}`    , selectedData[i].CUST_ID);         // 화주ID
                formData.append(`CUST_CD${i}`          , selectedData[i].VENDER_CODE);     // 거래처코드
                formData.append(`CUST_NM${i}`          , selectedData[i].VENDER_NAME);     // 거래처 이름
                formData.append(`REP_NM${i}`           , selectedData[i].VENDER_OWNER);    // 대표자이름
                formData.append(`BIZ_NO${i}`           , selectedData[i].VENDER_NUMBER);   // 사업자번호
                formData.append(`ZIP${i}`              , selectedData[i].VENDER_ZIP);      // 우편번호
                formData.append(`ADDR${i}`             , selectedData[i].VENDER_ADDR);     // 주소
                formData.append(`BIZ_COND${i}`         , selectedData[i].VENDER_KIND);     // 업태
                formData.append(`BIZ_TYPE${i}`         , selectedData[i].VENDER_TYPE);     // 업종
                formData.append(`TEL${i}`              , selectedData[i].VENDER_TEL);      // 전화번호
                formData.append(`FAX${i}`              , selectedData[i].VENDER_FAX);      // 팩스번호
    
                formData.append(`CUST_INOUT_TYPE${i}`  , '1');     // 1: 매입처 (입고처)

                if(selectedData[i].SYNC_YN == 'Y'){
                    formData.append(`CUST_ID${i}`      , selectedData[i].TRANS_CUST_ID); // 화주거래처ID
                }
            }
            
            jQuery.ajax({
                url : "/WMSMS011/save.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                    vm.SearchClickEventCallback(e);
                },
                success : function(data){
    
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        throw data["MSG"];
                    }
                    // Success ...
                    else{
                        alert(data["MSG"]);
                    }
                }
            });

        }

    }



    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('매입처정보', true);
    }
    

    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        formData.append('vrCategoryE2', '1');

        return jQuery.ajax({
            url : "/WMSIF708/listE2.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });
        
    }



    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '매입처');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}