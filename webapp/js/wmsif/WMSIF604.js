import { WMSIF604E1 }  from './WMSIF604E1.js';
import { WMSIF604E2 }  from './WMSIF604E2.js';
import { WMSIF604E3 }  from './WMSIF604E3.js';

const DAS_SEND_INFO 	    = '0';
const DAS_RESULT_INFO 	    = '1';
const DAS_DLV_RESULT 	    = '2';

let   ACTIVE_TAB_INDEX          = null;             // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = DAS_DLV_RESULT;   // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

jQuery(document).ready(function() {

    let view01 = new WMSIF604E1();
    let view02 = new WMSIF604E2();
    let view03 = new WMSIF604E3();

    const ViewContainer = [view01, view02, view03];

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
     //ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    // ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){

        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;
        ViewContainer[activeTabIdx]?.ActiveViewEventCallBack();
        
        switch(activeTabIdx){
            case DAS_SEND_INFO: 
                break;
            case DAS_RESULT_INFO: 
                break;
            case DAS_DLV_RESULT: 
                break;
        }

    });
});