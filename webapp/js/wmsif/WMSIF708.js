import * as InterfaceUtil from '../utility/interfaceUtil.js';
import { WMSIF708E1 } from './WMSIF708E1.js';
import { WMSIF708E2 } from './WMSIF708E2.js';
import { WMSIF708E3 } from './WMSIF708E3.js';

let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const PRODUCT_INFO  = '0';
const SELLER_INFO   = '1';
const BUYER_INFO    = '2';

const store = (function(){

    /**
     * 오픈몰 정보 조회 및 컴포넌트 바인딩
     * 
     * ! 스카이젯 토큰 날짜 확인.: 당일 사용가능 토큰이 아닌 경우, 신규 생성
     */
    async function SetOpenMallComboBox(componentId, formId, custId){

        let openMallInfos = new Array();

        if(cfn_isEmpty(custId)){
            jQuery(`#${componentId} option`).remove();
            return ;
        }

        openMallInfos = InterfaceUtil.OpenMallInfo.GetInfo(custId, 'ISKYZ');   // 세션조회: 화주별 계정 1개 전제

        /* 1. 토큰 취득 (세션 or 기준정보) */
        if(cfn_isEmpty(openMallInfos) || (openMallInfos.length <= 0)){
            await InterfaceUtil.OpenMallInfo.GetAuthInfo(custId, 'ISKYZ', function(){
                openMallInfos = InterfaceUtil.OpenMallInfo.GetInfo(custId, 'ISKYZ'); 

                if(cfn_isEmpty(openMallInfos) || (openMallInfos.length <= 0)){
                    alert(`인터페이스 사용 권한이 없습니다. (관리자 문의)`);
                    return;
                } 
            });
        }

        /* 2. 유효성 검사 및 갱신 */
        if(!ValidationOfToken(openMallInfos[0])){
            await GetToken(formId, custId, openMallInfos[0])
                .then((dt)=>{
                    openMallInfos = InterfaceUtil.OpenMallInfo.GetInfo(custId, 'ISKYZ'); 
                })
                .catch((message)=>{
                    alert(message);
                    return;
                });
        }

        /* 3. 컴포넌트 바인딩 */
        jQuery(`#${componentId} option`).remove();
            
        for(let mallInfo of openMallInfos){

            let option = new Option();

            option.value = mallInfo['MASTER_SEQ'];
            option.text = mallInfo['OPENMALL_CD_DESC'];
            
            jQuery(`#${componentId}`).append(option);
        }


        function ValidationOfToken(mallInfos){
            let valid = true;
            const DateHandler = new Intl.DateTimeFormat();

            /* 1. 토큰 발급일과 작업일이 다른 경우 토큰 갱신요청. */
            if(DateHandler.format(new Date(mallInfos['TOKEN1_REG_DT'])) != DateHandler.format(new Date())){
                valid = false;
            }

            return valid;
        }

    }

    
    /**
     * 스카이젯 인증토큰 생성
     * 
     * @interface 
     * @param {*} info 
     * @returns 
     */
    function GetToken(formId, custId, info){

        const vm = this;
        let formData = new FormData(document.getElementById(formId));

        formData.append('CUST_ID', custId);
        formData.append('LoginID', info['OPENMALL_API_ID1']);
        formData.append('LoginPW', info['OPENMALL_API_PW1']);
        
        return new Promise((resolve, reject)=>{
            jQuery.ajax({
                url : "/WMSIF708/getToken.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
    
                    let result = JSON.parse(data.message);
                    
                    if((data != null) && ('message' in data)){
    
                        result = JSON.parse(data.message);
    
                        if(result.successYn == "N"){
                            reject(`인증서 생성 오류 (관리자 문의)\n${result.message}`);
                        }
                    }
                    else{
                        reject(`인증서 생성 오류 (관리자 문의)\n${result.message}`);
                    }

                    /* 세션정보 갱신 */
                    InterfaceUtil.OpenMallInfo.SetValueOfKey(custId, null, info['MASTER_SEQ'], 'TOKEN1', result.token);
                    InterfaceUtil.OpenMallInfo.SetValueOfKey(custId, null, info['MASTER_SEQ'], 'TOKEN1_REG_DT', new Date());

                    resolve(JSON.parse(data.message));
                }
            });
        })

    }


    return {
        SetOpenMallComboBox : SetOpenMallComboBox,
        GetToken : GetToken
    }
})()


jQuery(document).ready(function() {

    let view01 = new WMSIF708E1({ store });      // 1. 상품관리
    let view02 = new WMSIF708E2({ store });      // 2. 매입처관리
    let view03 = new WMSIF708E3({ store });      // 3. 매출처관리

    const ViewContainer = [view01, view02, view03];


    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
    ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){
        
        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        ViewContainer[activeTabIdx].ActiveViewEventCallBack();

        switch(activeTabIdx){
            case PRODUCT_INFO : 
                break;
            case SELLER_INFO : 
                break;
            case BUYER_INFO : 
                break;
        }
    });

});