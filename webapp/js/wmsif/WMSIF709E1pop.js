import * as InterfaceUtil from '../utility/interfaceUtil.js';

const IN_ORDER_COLLECT_POP = "popInOrderCollect";
const IN_ORDER_REGIST_POP = "popInOrderRegist";

/**
 * 주문정보 수집
 */
export const InOrderCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        jQuery("#vrColStDateFromE1").val(localStorage.LocalTime);
        jQuery("#vrColStDateToE1").val(localStorage.LocalTime);

        AddEvent();
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE1").off().on("click", (e)=>{
            OrderCollect();
        });
    }


    /**
     * 매입처정보 수집 I/F 실행 
     * 
     */
    function OrderCollect(){
        
        let formData        = new FormData(document.getElementById(`form709E1_1pop`));
        let dateType        = Array.from(document.getElementsByName("dateType")).find(radio => radio.checked).value;

        formData.append('CUST_ID'           , custId     );
        formData.append('ORDER_TYPE'        , 1);       // 매입처(입고: 1), 매출처(출고: 2)

        formData.append('Z_TOKEN'           , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'TOKEN1'));
        formData.append('Z_FROM'            , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID2'));
        formData.append('Z_TO'              , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID3'));
        
        formData.append("TRANS_DATE1"       , jQuery("#vrColStDateFromE1").val());
        formData.append("TRANS_DATE2"       , jQuery("#vrColStDateToE1").val());
        formData.append("VenderCode"        , jQuery("#vrtxtBuyer").val());
        formData.append("iSWORK_DAY"        , dateType ?? "N");


        jQuery.ajax({
            url : "/WMSIF709/collectOrder.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${IN_ORDER_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){

                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                
                alert(`저장되었습니다.`);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });

    }
    

    return {
        Init : Init
    }

})();


/**
 * 주문등록
 * 
 */
export const InOrderRegist = (function(){

    let opener = null;
    let custId = null;
    let masterSeq = null;
    let sendData = new FormData();

    
    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        jQuery("#vrOrdInDtE1_1").val(localStorage.LocalTime);
        opener.GetCustOrdDegree(true, "vrPopOrdDegreeE1_1", jQuery("#vrOrdInDtE1_1").val());

        AddEvent();
    }


    let SetData = function(data){
        sendData = data;
    }


    let AddEvent = function(){
        
        // "출고예정일" 날짜변경 이벤트
        jQuery("#vrOrdInDtE1_1").off().on("change", (e)=>{
            opener.GetCustOrdDegree(true, "vrPopOrdDegreeE1_1", jQuery("#vrOrdInDtE1_1").val());
        });

        // "등록" 버튼 클릭 이벤트
        jQuery("#btnSaveE1_1").off().on("click", (e)=>{
            SaveData();
        });
    }


    const SaveData = function(){

        try{

            const ordOutDt    = jQuery("#vrOrdInDtE1_1").val()       ?? null;
            const ordDegree   = jQuery("#vrPopOrdDegreeE1_1").val()   ?? null;

            sendData.append('ordOutDt', ordOutDt );
            sendData.append('ordDegree', ordDegree );

            jQuery.ajax({
                url : "/WMSIF709/saveE1_OM.action",
                type : 'post',
                data : sendData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    
                    jQuery(`#${IN_ORDER_REGIST_POP}`).bPopup().close();
    
                    // 호출객체 CallBack 함수 수행 (차수갱신, 재조회)
                    if(opener == null) return ;

                    jQuery.when(
                            opener.GetCustOrdDegree(false), 
                            opener.SearchClickEventCallback(null, false, false))
                            .done(function(rtn1, rtn2){
                                cfn_closeProgress();
                            }
                    );
                },
                success : function(data){
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(data["MSG"]);
                    }
                    else{
                        alert(DICTIONARY['save_success'] + "\n(주문차수가 갱신됩니다.)"  + "\n(주문 목록이 재조회됩니다.)");
                    }
                },
                error: function(request, status, error){
                    alert(`관리자에게 문의바랍니다. (${request.status})`);
                    console.log(request);
                    console.log(status);
                    console.log(error);
                }
            });

        } catch(e) {
            alert(e);
            cfn_closeProgress();
            return;
        }
        
    }


    return {
        Init : Init,
        SetData : SetData
    }

})();
