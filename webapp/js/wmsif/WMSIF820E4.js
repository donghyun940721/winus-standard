import { WinusGrid }                from '../utility/WinusGrid.js';

export class WMSIF820E4{                // !(필수) 화면 명 수정

    #custId         = '0000575640';
    #formId         = 'frm_listE4';

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE4',           // !(필수)  Grid가 그려질 DIV 태그 ID값
        gridTemplate        :   'WMSIF820E4',          // !(필수) '신규그리드관리' 메뉴에 지정된 Grid 컬럼정보의 템플릿 구분코드
        headerHeight        :   '25',                  // 헤더 높이
        rowHeight           :   '27',                  // 데이터 행 높이
        setColor            :   false,
        useFilter           :   true,
        // rowSpanColIdx       :   1,                    // 행 병합
        // rowSpanColArr       :   [1],
    });


    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성


        /** Grid 생성대기 */
        let settingTimer = setInterval(()=>{

            if (vm.#grid.created) {
                /** Grid 생성 후 처리 내용 기재
                 * 
                 * ex) Column Format Custom, 로딩 시 기본조회 ... 
                 */
                // 횡 스크롤 새로고침
                this.#grid.ShowHorizontalScrollbar(true);
                clearInterval(settingTimer);
            }
        }, 100);
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        date.setDate(date.getDate() + 1); // 공급일 (다음날)
        jQuery("#vrSrchReqDtFromE4").datepicker("setDate", date);

        jQuery('#devOrdDegreePopE4').append("<option value=''>전체</option>");

        this.GetDevOrdDegree();
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        /** 주문예정차수 (날짜) */
        jQuery("#vrSrchReqDtFromE4").off().on("change", (e)=>{
            this.ReqDtChangedCallBack(e);
        });

        jQuery("#btn_createE4").off().on("click", (e)=>{
            this.CreateInvoiceData();
        });

        jQuery("#btn_sendE4").off().on("click", (e)=>{
            this.SendInvoiceData();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE4").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
            // this.GetSummaryData(e);
        });

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE4").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


    /**
     * 컨텍스트 메뉴 이벤트 콜백
     * 
     * (컨텍스트메뉴 사용 시 정의) 
     * @param {*} menuNm : 컨텍스트 메뉴명
     */
    ContextMenuEventCallBack(menuNm){

        alert( menuNm + ' Call ...');

    }


//#region   :: Component Set 

//#endregion


//#region   :: CallBack Event

    SearchClickEventCallback (e){
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        formData.append('CUST_ID'     , this.#custId);

        return jQuery.ajax({
            url : "/WMSIF820/listE04.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    document.getElementById("totalLossQtyE4").innerHTML = data.rows.reduce((acc, curr) => { return Number(acc) + Number(curr.QTYNPK) }, 0);
                }

            }
        });
    }

    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad(`출고실적`, true);
    }

    GetSummaryData (e){
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        formData.append('vrCustId'     , this.#custId);

        return jQuery.ajax({
            url : "/WMSIF820/listE05Summary.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    document.getElementById("totalInvoCntE4").innerHTML = data.LIST.list[0].DLV_CNT;
                    document.getElementById("addInvoCntE4").innerHTML = Number(data.LIST.list[0].DLV_CNT) - Number(data.LIST.list[0].ORDER_CNT);
                    document.getElementById("ordCntE4").innerHTML = data.LIST.list[0].ORDER_CNT;
                    document.getElementById("erpOrdCntE4").innerHTML = data.LIST.list[0].ERP_CNT;
                }

            }
        });
    }

    ReqDtChangedCallBack (e) {
        this.GetDevOrdDegree();
    }

    CreateInvoiceData () {
        const vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        if(confirm(`선택한 공급일의 택배 실적을 생성 하시겠습니까?\n(공급일 : ${jQuery("#vrSrchReqDtFromE4").val()})`)){

            formData.append('vrCustId', this.#custId);
            formData.append('vrOrdDt', jQuery("#vrSrchReqDtFromE4").val());
            
            jQuery.ajax({
                url : "/WMSIF820/createInvoiceData.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
                    alert(data["MSG"]);
                    vm.SearchClickEventCallback();
                }
            });
        }
    }

    SendInvoiceData () {
        const vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        if(confirm(`선택한 공급일의 택배 실적을 전송 하시겠습니까?\n(공급일 : ${jQuery("#vrSrchReqDtFromE4").val()})\n(대상: 선택 공급일 기준 미전송 항목)`)){

            formData.append('vrCustId', this.#custId);
            formData.append('vrOrdDt', jQuery("#vrSrchReqDtFromE4").val());
            
            jQuery.ajax({
                url : "/WMSIF820/sendInvocieResult.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
                    const message = `${data["MSG"]}\n${data.message}`;
                    alert(message);
                    vm.SearchClickEventCallback();
                }
            });

        }
    }

    GetDevOrdDegree () {
        let formData = new FormData();

        formData.append('vrCustId', this.#custId);
        formData.append('vrOrdDt', jQuery("#vrSrchReqDtFromE4").val());
        formData.append('ordDegree', "ALL");
        formData.append('vrSrchOrdBizType', "B2C");

        // Init
        jQuery(`#devOrdDegreePopE4 option`).remove();

        return jQuery.ajax({
            url : "/WMSOP910/getDevOrdDegree.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){
                // Error ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{
                    var data = data.DS_DEV_ORD_DEGREE;
                    var cnt  = data.size();

                    jQuery('#devOrdDegreePopE4').append("<option value=''>전체</option>");
                    for (var i = 0; i < cnt; i++){
                        jQuery('#devOrdDegreePopE4').append("<option value='"+data[i].CODE+"'>"+ data[i].CODE_NM + "</option>");
                    }
                }

                // GetOrderSummery();
            }
        });
    }

//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}