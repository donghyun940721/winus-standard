import * as InterfaceUtil   from '../utility/interfaceUtil.js';
import * as CustomPopUp     from './WMSIF706E1pop.js';
import { WinusGrid }        from '../utility/WinusGrid.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';
import { SessionKey, SessionUtil, LocalStorageUtil }  from '../utility/userStorageManger.js';

export class WMSIF706E1{

    #CODE = 'CODE';
    #NAME = 'NAME';

    #formId = 'frm_listE1';
    #custId = null;

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE1',
        id          : 'vrSrchRitemIdE1',
        name        : 'vrSrchRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });

    #grid = new WinusGrid({ 
        spdListDiv      : "spdListE1",
        gridTemplate    : "WMSIF706E1_1",
        rowHeight       : 27,
        headerHeight    : 22,
        frozenColIdx    : 4,
        setColor        : false,             // Cell 단위 설정 배경색 적용
        exFileExportArr : [0]
    });


    constructor(args){

        if(args != undefined){
            
        }

        this.Init();
        this.AddEvent();

        this.#grid.Init();
        this.#grid.SetGrid();
    }


    Init(){

        let date = new Date();

        // Tabs
        jQuery("#tab1").tabs({show: function(event, ui) {resizeGrid();}});

        /* button */
        jQuery("input:button").button();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrWorkDtE1").datepicker("setDate", date);
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * 1. 화주 
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;

            this.BindSendCompany(custId);
        }
    }


    AddEvent(args){

        let vm = this;

        // 화주
        jQuery("#vrSrchCustCdE1").off().on("change", (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        jQuery("#vrSrchCustNmE1").off().on("change", (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        // 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // 상품정보수집 버튼 이벤트
        jQuery("#btnCollectItem").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.ItemCollectClickEventCallBack(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // '상품등록' 버튼 클릭 이벤트
        jQuery("#btnRegItem").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SyncClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_write").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SearchClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // '그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGrid").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

        /** Custom Grid Event */
        this.#grid.CellDoubleClickEvent = function(sender, args){
            vm.GridDoubleClickEvent(sender, args);
        }
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // Set Cust Info
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), SessionUtil.GetValue(SessionKey.CUST_ID), SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


    BindSendCompany(custId){

        InterfaceUtil.OpenMallInfo.GetAuthInfo(custId, 'EZADMIN', function(){

            let mallInfos = InterfaceUtil.OpenMallInfo.GetInfoByCustId(custId);
        
            jQuery("#vrSrchSendCompanyIdE1 option").remove();
        
            // 오픈몰 정보 바인딩
            for(let mallInfo of mallInfos){
        
                let option = new Option();
        
                option.value = mallInfo['MASTER_SEQ'];
                option.text = mallInfo['OPENMALL_CD_DESC'];
                
                jQuery("#vrSrchSendCompanyIdE1").append(option);
            }
        
        });

    }


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                vm.BindSendCompany(vm.#custId);

                break;

            //* 상품
            case COMP_CATEGORY.ITEM :
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);
                
                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        vm.BindSendCompany(vm.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    ItemCollectClickEventCallBack(e){

        jQuery('#popItemCollect').bPopup({
            zIndex: 999,
            escClose: 0,
            // modalClose: true,
            opacity: 0.6,
            positionStyle: 'fixed', //'fixed' or 'absolute'
            transition: 'slideDown',
            speed: 400
        });
        
        CustomPopUp.ItemCollect.Init({
            'OPENER' : this,
            'CUST_ID' : jQuery("#vrSrchCustIdE1").val(),
            'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE1").val()
        });
    }


    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad('상품정보');
    }


    SearchClickEventCallback(e){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let lcId = jQuery(`[name="SVC_INFO"]`).val();

        formData.append('LC_ID', lcId);

        jQuery.ajax({
            url : "/WMSIF706/list.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{

                    vm.#grid.SetData(data);

                }

            }
        });

    }


    SyncClickEventCallback(e){

        try {

            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = null;

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 상품을 선택해야 합니다.`;
            }

            saveMessage = gridData.some(dt => dt['SYNC_YN'] == 'Y') 
                                ? '이미 동기화된 상품이 존재합니다.\n' + DICTIONARY['confirm_save']
                                : DICTIONARY['confirm_save']


            if(confirm(saveMessage)){

                let formData = new FormData(document.getElementById(`${this.#formId}`));

                formData.append('ROW_COUNT', selectedIdx.length);

                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let rowIdx = selectedIdx[i];

                    let param = {

                        ITEM_WGT          :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('WEIGHT'))
                        , ITEM_BAR_CD     :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('BARCODE'))
                        , ITEM_CODE       :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_ID'))
                        , ITEM_ENG_NM     :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('NAME'))
                        , ITEM_KOR_NM     :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('NAME'))
                        , ITEM_NM         :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('NAME'))
                        , MAKER_NM        :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MAKER'))
                        , UNIT_PRICE      :  Number(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORG_PRICE')))
                                                        + Number(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('EXTRA_PRICE')))
                        , SALES_PRICE     :  Number(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SHOP_PRICE')))
                                                        + Number(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('EXTRA_SHOP_PRICE')))
                        , ITEM_ID         :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ITEM_ID'))

                    }

                    formData.append(`I_ITEM_WGT`      +   `_${i}`, param['ITEM_WGT']);
                    formData.append(`I_ITEM_BAR_CD`   +   `_${i}`, param['ITEM_BAR_CD']);
                    formData.append(`I_ITEM_CODE`     +   `_${i}`, param['ITEM_CODE']);
                    formData.append(`I_ITEM_ENG_NM`   +   `_${i}`, param['ITEM_ENG_NM']);
                    formData.append(`I_ITEM_KOR_NM`   +   `_${i}`, param['ITEM_KOR_NM']);
                    formData.append(`I_ITEM_NM`       +   `_${i}`, param['ITEM_NM']);
                    formData.append(`I_MAKER_NM`      +   `_${i}`, param['MAKER_NM']);
                    formData.append(`I_UNIT_PRICE`    +   `_${i}`, param['UNIT_PRICE']);
                    formData.append(`I_SALES_PRICE`   +   `_${i}`, param['SALES_PRICE']);
                    formData.append(`I_ITEM_ID`       +   `_${i}`, param['ITEM_ID']);
                }


                jQuery.ajax({
                    url : "/WMSIF706/syncItem.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(data["MSG"]);
                        }
                        // Success ...
                        else{
                            alert(`${DICTIONARY['save_success']} (${selectedIdx.length} 건)`);
                        }
            
                    },
                    error: function(request, status, error){
                        alert(`관리자에게 문의바랍니다. (${request.status})`);
                        console.log(request);
                        console.log(status);
                        console.log(error);
                    }
                });
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }
        
        console.log('Call Sync click ..');

    }


    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '상품정보관리');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


    GridDoubleClickEvent(sender, args){

        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let rowIdx = args.row;
        let colIdx = args.col;

        let rowData = this.#grid.GetRowData(rowIdx);

        if(rowData['SYNC_YN'] == 'N') return ;


        formData.append('ITEM_CODE', rowData['PRODUCT_ID']);

        jQuery.ajax({
            url : "/WMSIF706/itemInfo.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                // cfn_viewProgress();
            },
            complete: function(){
                // cfn_closeProgress();
            },
            success : function(data){
                OpenItemComparePopUp(data['rows'][0]);
            }
        });


        function OpenItemComparePopUp(winusData){
            jQuery('#popItemCompare').bPopup({
                zIndex: 999,
                escClose: 0,
                opacity: 0.6,
                positionStyle: 'fixed',
                transition: 'slideDown',
                speed: 400
            });
            
            CustomPopUp.ItemCompare.Init({
                'OPENER'        : this,
                'CUST_ID'       : jQuery("#vrSrchCustIdE1").val(),
                'WMS_ITEM_NM'   : winusData['ITEM_NM'],
                'WMS_ITEM_CD'   : winusData['ITEM_CODE'] ,
                'WMS_ITEM_BAR'  : winusData['ITEM_BAR_CD'] ,
                'WMS_REG_DT'    : winusData['REG_DT'],
                'WMS_UPD_DT'    : winusData['UPD_DT'],
                'EZM_ITEM_NM'   : rowData['NAME'],
                'EZM_OPT_NM'    : rowData['OPTIONS'],
                'EZM_ITEM_BAR'  : rowData['BARCODE'],
                'EZM_REG_DT'    : rowData['REG_DATE'],
                'EZM_UPD_DT'    : rowData['LAST_UPDATE_DATE']
            });
        }
    }


//#endregion


//#region   :: Validation


    /**
     * 상품정보 등록을 위한 유효성 검사
     * 
     * @param {*} colKey 
     * @param {*} value 
     * @returns 
     */
    Validation(colKey, value){
        let valid   = true;
        let msg     = null;
        let level   = this.CONST_VALID_LEVEL.WARNING;

        switch(colKey){

            case 'SYNC_YN' :
                if(value == 'Y'){
                    level = this.CONST_VALID_LEVEL.WARNING;
                    valid = false;
                    msg = '이미 등록된 상품입니다. 업데이트 하시겠습니까?';
                }
                
                break;
        }


        return {
            'MSG' : msg,
            'VALID' : valid,
            'LEVEL' : level
        }
    }


//#endregion

    CONST_VALID_LEVEL = {
        'WARNING'   : 1,
        'ERROR'     : 2
    }

}