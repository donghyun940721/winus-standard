import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSIF604E3{                // !(필수) 화면 명 수정

    #custId         = null;
    #formId         = 'frm_listE3';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE3',
        id          : 'vrSrchCustIdE3',
        name        : 'vrSrchCustNmE3',
        category    : COMP_CATEGORY.CUST
    });

    #grid = new WinusGrid({ 
        spdListDiv          : 'spdListE3',
        gridTemplate        : 'WMSIF604E3',
        headerHeight        : '25',
        rowHeight           : '27',
        setColor            : false,
        useFilter           : true,
        checkFirstIdx       : 0,
        multiCheckRange     : [0, 1],
        rowSpanColIdx       : 2,
        rowSpanColArr       : [0, 2, 3],
    });


    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성


        /** Grid 생성대기 */
        let settingTimer = setInterval(()=>{

            if (vm.#grid.created) {
                clearInterval(settingTimer);

                /** Grid 생성 후 처리 내용 기재
                 * 
                 * ex) Column Format Custom, 로딩 시 기본조회 ... 
                 */
            }
        }, 100);
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        date.setDate(date.getDate() + 1); 
        jQuery("#vrSrchOrdDegreeDateE3").datepicker("setDate", date); // 주문예정차수 (다음날 : COOP)

        jQuery('#devOrdDegreePopE3').append("<option value=''>전체</option>");
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){
        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE3").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE3").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE3").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }
    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){
        // * 화주
        document.getElementById("vrSrchCustCdE3").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE3").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE3").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        /** 주문예정차수 (날짜) */
        jQuery("#vrSrchOrdDegreeDateE3").off().on("change", (e)=>{
            this.GetCustOrdDegree();
        });

        /** 주문차수 변경 */
        document.getElementById('vrSrchOrdDegreeE3').addEventListener('change', (e)=>{
            this.GetDevOrdDegree();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE3").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
            this.GetSummaryData(e);
        });

        // * "송장접수" 버튼 클릭 이벤트
        jQuery("#btn_sendE3").off().on("click", (e)=>{
            this.SaveInvoInfo(e);
        });

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE3").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){
        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);

            this.GetCustOrdDegree();
        }

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


    /**
     * 컨텍스트 메뉴 이벤트 콜백
     * 
     * (컨텍스트메뉴 사용 시 정의) 
     * @param {*} menuNm : 컨텍스트 메뉴명
     */
    ContextMenuEventCallBack(menuNm){

        alert( menuNm + ' Call ...');

    }


//#region   :: Component Set 

    /**
     * 주문예정차수 셋팅
     * 
     * @param {*} id        : 컴포넌트 ID
     * @param {Array} data  : 바인딩 Data (/WMSOP642/getCustOrdDegree.action)
     */
    SetCustOrdDegree(id, data){
        let cnt = data.size();

        // Init
        jQuery(`#${id} option`).remove();

        jQuery(`#${id}`).append("<option value=''>전체</option>");
        
        if(cnt > 0){
            for (var i = 0; i < cnt; i++){
                jQuery(`#${id}`).append("<option value='"+data[i].CODE+"'>"+data[i].CODE+"</option>");
            }
        }

        this.GetDevOrdDegree();
    }

//#endregion


//#region   :: CallBack Event

    SearchClickEventCallback (e){
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        formData.append('vrCustId'     , this.#custId);

        return jQuery.ajax({
            url : "/WMSIF604/listE3.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });
    }

    GetSummaryData (e){
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return jQuery.ajax({
            url : "/WMSIF604/listE3Summary.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    const summaryData = data.LIST.list[0];
                    // data.LIST.list
                    document.getElementById("ordCntE3").innerHTML = data.LIST.list[0].WMS_ORD_CNT;
                    document.getElementById("totalInvoCntE3").innerHTML = data.LIST.list[0].WMS_INVC_CNT;
                    document.getElementById("addInvoCntE3").innerHTML = data.LIST.list[0].ADD_INVC_CNT;
                    // document.getElementById("erpOrdCntE5").innerHTML = data.LIST.list[0].ERP_CNT;
                }

            }
        });
    }

    /**
     * 주문예정차수 데이터 조회
     * 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     * @param {string} componentId      : 조회값을 바인딩할 컴포넌트 ID
     * @param {string} ordDt            : 출고예정일
     */
    GetCustOrdDegree(componentId="vrSrchOrdDegreeE3", ordDt){
    
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.append('vrCustId', jQuery("#vrSrchCustIdE3").val());
        formData.append('vrOrdDt', ordDt ?? jQuery("#vrSrchOrdDegreeDateE3").val());

        return jQuery.ajax({
            url : "/WMSOP642/getCustOrdDegree.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                // Error ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{

                    if('DS_ORD_DEGREE' in data){
                        vm.SetCustOrdDegree(componentId, data.DS_ORD_DEGREE);
                    }
                }
            }
        });
    }

    GetDevOrdDegree () {
        const ordDegree = jQuery("#vrSrchOrdDegreeE3").val();
        let formData = new FormData();

        formData.append('vrCustId', this.#custId);
        formData.append('vrOrdDt', jQuery("#vrSrchOrdDegreeDateE3").val());
        formData.append('ordDegree', ordDegree === '' ? "ALL" : ordDegree);
        formData.append('vrSrchOrdBizType', "B2C");

        // Init
        jQuery(`#devOrdDegreeE3 option`).remove();

        return jQuery.ajax({
            url : "/WMSOP910/getDevOrdDegree.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){
                // Error ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{
                    var data = data.DS_DEV_ORD_DEGREE;
                    var cnt  = data.size();

                    jQuery('#devOrdDegreePopE2').append("<option value=''>전체</option>");
                    for (var i = 0; i < cnt; i++){
                        jQuery('#devOrdDegreeE3').append("<option value='"+data[i].CODE+"'>"+ data[i].CODE_NM + "</option>");
                    }
                }
            }
        });
    }

    /**
     * 송장접수
     * - 선택 행 추가접수 (분할: DIV타입)
     * 
     * @param {*} e 
     */
    SaveInvoInfo(e) {
        const vm = this;
        const selectedRows = this.#grid.GetSelectedRowData(1);
        let formData = new FormData();
        let isConfirm = false;

        isConfirm = confirm(`선택한 행의 추가송장 접수를 진행하시겠습니까?`);

        if(!isConfirm) return;

        if(selectedRows.length == 0){
            alert('선택된 행이 없습니다.');
            return ;
        }

        formData.append('selectIds', selectedRows.length);
        formData.append('vrCustId', this.#custId);
        formData.append('vrOrdDt', jQuery("#vrSrchOrdDegreeDateE3").val());
        formData.append('vrDevOrdDegree', jQuery("#devOrdDegreeE3").val());

        for (let i=0; i<selectedRows.length; i++) {
            formData.append(`DAS_INVO_YN${i}`, selectedRows[i].DAS_INVO_YN);
            formData.append(`DENSE_FLAG${i}`, selectedRows[i].DENSE_FLAG);
            formData.append(`SEND_DATE${i}`, selectedRows[i].SEND_DATE);
            formData.append(`ORDER_WAVE${i}`, selectedRows[i].ORDER_WAVE);
            formData.append(`ORDER_TYPE${i}`, selectedRows[i].ORDER_TYPE);
            formData.append(`ORDER_CD${i}`, selectedRows[i].ORDER_CD);
            formData.append(`ORD_SEQ${i}`, selectedRows[i].ORD_SEQ);
            formData.append(`STORE_CD${i}`, selectedRows[i].STORE_CD);
            formData.append(`BOX_NO${i}`, selectedRows[i].BOX_NO);
            formData.append(`STORE_NM${i}`, selectedRows[i].STORE_NM);
            formData.append(`PARCEL_BOX_TY${i}`, selectedRows[i].PARCEL_BOX_TY);
            formData.append(`INVO_NO${i}`, selectedRows[i].INVO_NO);
            formData.append(`ITEM_CD${i}`, selectedRows[i].ITEM_CD);
            formData.append(`ITEM_NM${i}`, selectedRows[i].ITEM_NM);
            formData.append(`ITEM_BOX${i}`, selectedRows[i].ITEM_BOX);
            formData.append(`ITEM_BARCODE${i}`, selectedRows[i].ITEM_BARCODE);
            formData.append(`ITEM_QTY${i}`, selectedRows[i].ITEM_QTY);
            formData.append(`ITEM_PICK_QTY${i}`, selectedRows[i].ITEM_PICK_QTY);
            formData.append(`IF_DT${i}`, selectedRows[i].IF_DT);
        }

        jQuery.ajax({
            url : "/WMSIF604/sendProductInfoIF.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){
                alert("[안내]" + "\n"  + data.message);
                vm.SearchClickEventCallback();
            }
        });

    }

    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad('택배추가송장', true);
    }

    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                vm.GetCustOrdDegree();

                break;
        }
    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];

                        vm.GetCustOrdDegree();
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }
    
//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}