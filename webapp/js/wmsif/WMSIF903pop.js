import * as InterfaceUtil from '../utility/interfaceUtil.js';

const OUT_ORDER_COLLECT_POP = "popOutOrderCollect";
const OUT_ORDER_SYNC_POP = "popOutOrderSync";

export const OutOrderCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();    

        jQuery("#vrColStDateFromE1").val(localStorage.LocalTime);
        jQuery("#vrColStDateToE1").val(localStorage.LocalTime);
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE1").off().on("click", (e)=>{
            OutOrderCollect();
        });
    }


    /**
     * [SHOPLINKER] 출고주문 수집 I/F 실행 
     * 
     */
    function OutOrderCollect(){

        let formData        = new FormData(document.getElementById(`form903E1_1pop`));

        let fromDate = jQuery("#vrColStDateFromE1").val().replaceAll('-', '');
        let toDate = jQuery("#vrColStDateToE1").val().replaceAll('-', '');

        formData.append('CUST_ID'           , custId);
        formData.append('shoplinker_id', masterSeq);

        formData.append("st_date"         , fromDate);
        formData.append("ed_date"          , toDate);

        jQuery.ajax({
            url : "/WMSIF903/getOutOrder.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${OUT_ORDER_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                console.log(data.RESULT);
                data = JSON.parse(data.RESULT);
                alert(data.resultMessage);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    return {
        Init : Init
    }
})();


export const OutOrderSync = (function(){

    let custId = null;
    let masterSeq = null;
    let jsonData = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";
        jsonData    = "PARAM" in args ? args['PARAM'] : "";

        AddEvent();

        jQuery("#vrOutReqDtE1").val(localStorage.LocalTime);
        setOutOrderDegree();
    }
    
    
    let AddEvent = function(){
        jQuery("#btnSyncE1").off().on("click", (e)=>{
            OutOrderSync();
        });
        jQuery("#vrOutReqDtE1").off().on("change",(e)=>{
            setOutOrderDegree();
        })
    }


    /**
     * [SHOPLINKER] 출고주문 등록 실행 
     * 
     */
    function OutOrderSync(){

        jsonData.ordType = jQuery('#vrOrdTypeE1').val();
        jsonData.outReqDt = jQuery('#vrOutReqDtE1').val();
        jsonData.ordDegree = jQuery('#vrOrdDegreeE1').val();
        
        jQuery.ajax({
            url : "/WMSIF903/syncOutOrder.action",
            type : 'post',
            data : JSON.stringify(jsonData),
            contentType : "application/json; charset=utf-8",
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${OUT_ORDER_SYNC_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    alert(data["MSG"]);
                }
                // Success ...
                else{
                    alert(data["MSG"]);
                }
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    /**
     *  주문차수 불러오기
     */
    function setOutOrderDegree(){
        var param			= "vrCustId="+custId+"&vrOrdDt="+jQuery("#vrOutReqDtE1").val();
        var url				= "/WMSOP640/getCustOrdDegree.action";
        jQuery.post(url, param, function(data){
            var data = data.DS_ORD_DEGREE;
            var cnt  = data.size();
            
            jQuery("#vrOrdDegreeE1 option").remove();
            if(cnt > 0){
                for (var i = 0; i < cnt+1; i++){
                    if(i == cnt){
                        jQuery('#vrOrdDegreeE1').append("<option value='"+(Number(data[i-1].CODE)+1)+"'>"+(Number(data[i-1].CODE)+1)+" (신규예정)</option>");
                        jQuery('#vrOrdDegreeE1').val(Number(data[i-1].CODE)+1);
                    }else{
                        jQuery('#vrOrdDegreeE1').append("<option value='"+data[i].CODE+"'>"+data[i].CODE+"</option>");
                    }
                }
            }else{
                jQuery('#vrOrdDegreeE1').append("<option value='1'>1 (신규예정)</option>");
            }
        }).fail(function(xhr, textStatus){
            if(xhr.status == 404){	
                location.href=('/index.html');
            }
        });
    }

    return {
        Init : Init
    }
})();