import * as InterfaceUtil from '../utility/interfaceUtil.js';


const ITEM_COLLECT_POP = "popItemCollect";

/**
 * 상품정보 수집
 */
export const ItemCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();    
        
        jQuery("#vrColStDateFromE1").val(localStorage.LocalTime);
        // jQuery("#vrColStDateToE1").val(localStorage.LocalTime);
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE1").off().on("click", (e)=>{
            ItemCollect();
        });

        jQuery("#vrAllDayYnE1").off().on("click", (e)=>{
            document.getElementById('collectDatePickerE1').classList.toggle('act');
        });
    }


    /**
     * [스카이젯] 상품정보 수집 I/F 실행 
     * 
     */
    function ItemCollect(){

        let formData        = new FormData(document.getElementById(`form708E1_1pop`));
        const collectType   = jQuery("#vrCollectTypeE1").val();

        formData.append('CUST_ID'           , custId     );
        formData.append('CATEGORY'          , 1);       // 매입처(입고: 1), 매출처(출고: 2)

        formData.append('Z_TOKEN'           , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'TOKEN1'));
        formData.append('Z_FROM'            , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID2'));
        formData.append('Z_TO'              , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID3'));

        formData.append("ProductCode"       , "");
        // formData.append("ProductName"       , "");
        // formData.append("MakerName"         , "");
        // formData.append("KDcode"            , "");
        // formData.append("StockCode"         , "");
        formData.append("WriteDate"         , (collectType == 'reg_date') ? jQuery("#vrColStDateFromE1").val(): "");
        formData.append("EditDate"          , (collectType == 'upd_date') ? jQuery("#vrColStDateFromE1").val(): "");

        jQuery.ajax({
            url : "/WMSIF708/CollectItem.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${ITEM_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                
                alert(`저장되었습니다.`);
                
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    return {
        Init : Init
    }
})()


/**
 * 상품정보 비교 (Winus <> EzAdmin)
 * 
 * 동기화된 상품을 대상으로 현 Winus 등록정보와 비교한다.
 * 
 */
export let ItemCompare = (function(){


    let Init = function(args){

        jQuery("#txtWinusItemNm").text(args['WMS_ITEM_NM'] ?? '');
        jQuery("#txtWinusItemcode").text(args['WMS_ITEM_CD'] ?? '');
        jQuery("#txtWinusItemBarcode").text(args['WMS_ITEM_BAR'] ?? '');
        jQuery("#txtWinusRegDt").text(args['WMS_REG_DT'] ?? '');
        jQuery("#txtWinusUpdDt").text(args['WMS_UPD_DT'] ?? '');

        jQuery("#txtEzadminItemNm").text(args['EZM_ITEM_NM'] ?? '');
        jQuery("#txtEzadminOptNm").text(args['EZM_OPT_NM'] ?? '');
        jQuery("#txtEzadminItemBarcode").text(args['EZM_ITEM_BAR'] ?? '');
        jQuery("#txtEzadminRegDt").text(args['EZM_REG_DT'] ?? '');
        jQuery("#txtEzadminUpdDt").text(args['EZM_UPD_DT'] ?? '');
        
    }


    return {
        Init : Init
    }

})();