import * as CustomPopUp             from './WMSIF903pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY, MultiSelectBox}     from '../component/contentFilter.js';

class mWinusGrid extends WinusGrid{
    SetSheetStyle(){
        super.SetSheetStyle();
        
    }

}

export class WMSIF903E1 {

    #custId         = null;
    #formId         = 'frm_listE1';
    #apiCustId      = null;

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE1',
        id          : 'vrSrchRitemIdE1',
        name        : 'vrSrchRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: 'Shoplinker',                     // 그룹헤더에 출력될 명칭
            code: 'IF_SYS',                 // 그룹헤더 Key값 (아직 사용저 없음)
            startColumnKey: 'SHOPLINKER_ORDER_ID',       // 그룹지정 시작 열의 Key값
            endColumnKey: 'CREATE_DATE'           // 그룹지정 마지막 열의 Key값
        },
        {
            name: 'WMS',                     // 그룹헤더에 출력될 명칭
            code: 'WMS_SYS',                 // 그룹헤더 Key값 (아직 사용저 없음)
            startColumnKey: 'ITEM_CODE',       // 그룹지정 시작 열의 Key값
            endColumnKey: 'RITEM_ID'           // 그룹지정 마지막 열의 Key값
        }
    ];


    #grid = new mWinusGrid({ 
        spdListDiv          :   'spdListE1',
        gridTemplate        :   'WMSIF903E1',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        useFilter           :   true,
        useSort             :   true,
        headerMappingInfo   :   this.#headerMappingInfo,
        usedFooter          :   true,
        frozenFooter        :   true,
    });

    #select2 = new MultiSelectBox({
        htmlObjId : this.#formId+' select[name="vrSrchItemGrpId"]',
        url : '/WMSMS094/selectItemGrpLvl.action',
        fn_sucess : this.SelctItemGpr1StEventCallBack,
    });


    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

        let selectFormData = new FormData();
        selectFormData.append('vrGrpLevel',1);

        this.#select2.form = selectFormData;

        this.#select2.Init();
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE1").datepicker("setDate", date);                         // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE1").datepicker("setDate", date);                           // 수집등록일자 (to)
        
        //this.#apiCustId = DICTIONARY['ss_svc_no'] == '0000004080' ? "0000488488" : null;
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // 출고정보수집 버튼 이벤트
        jQuery("#btnCollectOutOrderE1").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.OutOrderCollectClickEventCallBack(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });

        // 출고등록 버튼 이벤트
        jQuery("#btnRegOutOrder").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SyncClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE1").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE1").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                fn_Popup('CUST');
            }

        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }
        
        /* 오픈몰 정보 설정*/
        this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#apiCustId??this.#custId);
        
        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }

    SelctItemGpr1StEventCallBack(result){
        let data = result.rows;
        let ListArray = new Array();
        for (var obj of data){
            ListArray.push({
                value : obj.CODE_CD,
                name : obj.CODE_NM
            })
        };
        return ListArray;
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#apiCustId??this.#custId);

                break;

            //* 상품
            case COMP_CATEGORY.ITEM :
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);

                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        
                        vm.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#apiCustId??this.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    OutOrderCollectClickEventCallBack(e){

        jQuery('#popOutOrderCollect').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });
        
        CustomPopUp.OutOrderCollect.Init({
            'OPENER' : this,
            'CUST_ID' : this.#apiCustId??this.#custId,
            'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE1").val()
        });
    }

    SyncClickEventCallback(e){
        try {
            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = null;

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 주문을 선택해야 합니다.`;
            }

            if(cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                throw `화주를 선택해야 합니다.`;
            }

            
            saveMessage = gridData.some(dt => dt['IF_YN'] == 'Y') 
                                ? '이미 동기화된 주문이 존재합니다.\n' + DICTIONARY['confirm_save']
                                : DICTIONARY['confirm_save']
            

            if(confirm(saveMessage)){

                let jsonData = new Object();

                jsonData['SHOPLINKER_ORDER_ID'] = new Array();
                jsonData['MALL_ORDER_ID'] = new Array();
                jsonData['MALL_NAME'] = new Array();
                jsonData['ORDER_NAME'] = new Array();
                jsonData['ORDER_TEL'] = new Array();
                jsonData['ORDER_CEL'] = new Array();
                jsonData['ORDER_EMAIL'] = new Array();
                jsonData['RECEIVE'] = new Array();
                jsonData['RECEIVE_TEL'] = new Array();
                jsonData['RECEIVE_CEL'] = new Array();
                jsonData['RECEIVE_ZIPCODE'] = new Array();
                jsonData['RECEIVE_ADDR'] = new Array();
                jsonData['BAESONG_TYPE'] = new Array();
                jsonData['BAESONG_BI'] = new Array();
                jsonData['DELIVERY_MSG'] = new Array();
                jsonData['ORDER_PRODUCT_ID'] = new Array();
                jsonData['SHOPLINKER_PRODUCT_ID'] = new Array();
                jsonData['PARTNER_PRODUCT_ID'] = new Array();
                jsonData['PRODUCT_NAME'] = new Array();
                jsonData['QUANTITY'] = new Array();
                jsonData['ORDER_PRICE'] = new Array();
                jsonData['SALE_PRICE'] = new Array();
                jsonData['SUPPLY_PRICE'] = new Array();
                jsonData['SKU'] = new Array();
                jsonData['ORDERDATE'] = new Array();
                jsonData['MALL_ID'] = new Array();
                jsonData['DENSE_FLAG'] = new Array();
                jsonData['SYNC_ORG_ORD_NO'] = new Array();
                jsonData['DENSE_SEQ'] = new Array();
                
                var skipProductIdCnt = 0;
                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let rowIdx = selectedIdx[i];

                    var productId = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SKU_MATCH_CODE')) == "" ? this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PARTNER_PRODUCT_ID')) : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SKU_MATCH_CODE'));

                    if (productId == ""){
                        skipProductIdCnt++;
                        continue;
                    }

                    jsonData['SHOPLINKER_ORDER_ID'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SHOPLINKER_ORDER_ID')));
                    jsonData['MALL_ORDER_ID'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MALL_ORDER_ID')));
                    jsonData['MALL_NAME'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MALL_NAME')));
                    jsonData['ORDER_NAME'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORDER_NAME')));
                    jsonData['ORDER_TEL'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORDER_TEL')));
                    jsonData['ORDER_CEL'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORDER_CEL')));
                    jsonData['ORDER_EMAIL'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORDER_EMAIL')));
                    jsonData['RECEIVE'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('RECEIVE')));
                    jsonData['RECEIVE_TEL'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('RECEIVE_TEL')));
                    jsonData['RECEIVE_CEL'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('RECEIVE_CEL')));
                    jsonData['RECEIVE_ZIPCODE'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('RECEIVE_ZIPCODE')));
                    jsonData['RECEIVE_ADDR'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('RECEIVE_ADDR')));
                    jsonData['BAESONG_TYPE'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('BAESONG_TYPE')));
                    jsonData['BAESONG_BI'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('BAESONG_BI')));
                    jsonData['DELIVERY_MSG'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('DELIVERY_MSG')));
                    jsonData['ORDER_PRODUCT_ID'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORDER_PRODUCT_ID')));
                    jsonData['SHOPLINKER_PRODUCT_ID'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SHOPLINKER_PRODUCT_ID')));
                    jsonData['PARTNER_PRODUCT_ID'].push(productId);
                    jsonData['PRODUCT_NAME'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_NAME')));
                    jsonData['QUANTITY'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('QUANTITY')));
                    jsonData['ORDER_PRICE'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORDER_PRICE')));
                    jsonData['SALE_PRICE'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SALE_PRICE')));
                    jsonData['SUPPLY_PRICE'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SUPPLY_PRICE')));
                    jsonData['SKU'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SKU')));
                    jsonData['ORDERDATE'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORDERDATE')));
                    jsonData['MALL_ID'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MALL_ID')));
                    jsonData['DENSE_FLAG'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('DENSE_FLAG')));
                    jsonData['SYNC_ORG_ORD_NO'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SYNC_ORG_ORD_NO')));
                    jsonData['DENSE_SEQ'].push(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('DENSE_SEQ')));
                }

                jsonData.count = jsonData['SHOPLINKER_ORDER_ID'].size();
                jsonData.custId = jQuery("#vrSrchCustIdE1").val();
                jsonData.custCd = jQuery("#vrSrchCustCdE1").val();

                if(skipProductIdCnt > 0){
                    alert("주문등록 "+skipProductIdCnt+"건 상품코드 누락으로 생략됩니다.");
                }

                jQuery('#popOutOrderSync').bPopup({
                    zIndex: 999,
                    escClose: 0,
                    opacity: 0.6,
                    positionStyle: 'fixed',
                    transition: 'slideDown',
                    speed: 400
                });
                
                CustomPopUp.OutOrderSync.Init({
                    'OPENER' : this,
                    'CUST_ID' : this.#apiCustId??this.#custId,
                    'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE1").val(),
                    'PARAM' : jsonData
                });
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }

    }

    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('출고주문정보', true);
    }

    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
     SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        return jQuery.ajax({
            url : "/WMSIF903/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                    let rowCnt = vm.#grid.GetRowCount()-1;
                    vm.#grid.setFormula(rowCnt,vm.#grid.GetColumnIdx('QUANTITY'),
                    `SUBTOTAL(109,R[-${rowCnt}]C:R[-1]C)`
                    );
                    vm.#grid.setFormula(rowCnt,vm.#grid.GetColumnIdx('OUT_ORD_QTY'),
                    `SUBTOTAL(109,R[-${rowCnt}]C:R[-1]C)`
                    );

                    //let packCnt = data.rows.map(dt=> (dt.PACK == 0) ? dt.SEQ : dt.PACK).filter((value, index, self)=>{return self.indexOf(value) === index;}).length;

                    //jQuery("#totalDlvCnt").text(packCnt);                                               // 합포단위수량
                    //jQuery("#totalOrdCnt").text(vm.#grid.GetRowCountByDistinctKey('SEQ'));              // 관리번호
                    //jQuery("#totalProcCnt").text(vm.#grid.GetRowCountByDistinctKey('PRD_SEQ'));         // 상품관리번호
                }

            }
        });

    }

    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '출고');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}