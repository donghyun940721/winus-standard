import * as CustomPopUp             from './WMSIF708E1pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSIF708E1 {

    #custId         = null;
    #formId         = 'frm_listE1';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE1',
        id          : 'vrSrchRitemIdE1',
        name        : 'vrSrchRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: '제품',
            code: 'PRODUCT_INFO',
            startColumnKey: 'PRODUCT_CD',
            endColumnKey: 'KD_CODE'
        },
        {
            name: '분류',
            code: 'CATEGORY_INFO',
            startColumnKey: 'KIND_CD',
            endColumnKey: 'GROUP2_NM'
        },
        {
            name: 'WINUS',
            code: 'WINUS_DT',
            startColumnKey: 'REG_DT',
            endColumnKey: 'UPD_NO'
        }
    ];


    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE1',
        gridTemplate        :   'WMSIF708E1',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        headerMappingInfo   :   this.#headerMappingInfo,
        setColor            :   false
    });


    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE1").datepicker("setDate", date);                         // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE1").datepicker("setDate", date);                           // 수집등록일자 (to)

    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보수집 버튼 이벤트
        jQuery("#btnCollectItemE1").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.ItemCollectClickEventCallBack(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }

        });


        // '상품등록' 버튼 클릭 이벤트
        jQuery("#btnRegItem").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SyncClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });
        

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE1").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE1").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }
        
        /* 오픈몰 정보 설정*/
        this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#custId);
        
        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#custId);

                break;

            //* 상품
            case COMP_CATEGORY.ITEM :
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);

                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        
                        vm.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    ItemCollectClickEventCallBack(e){

        jQuery('#popItemCollect').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });
        
        CustomPopUp.ItemCollect.Init({
            'OPENER' : this,
            'CUST_ID' : this.#custId,
            'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE1").val()
        });
    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));


        return jQuery.ajax({
            url : "/WMSIF708/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });
    }


    SyncClickEventCallback(e){

        try {

            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = null;

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 상품을 선택해야 합니다.`;
            }

            saveMessage = gridData.some(dt => dt['SYNC_YN'] == 'Y') 
                                ? '이미 동기화된 상품이 존재합니다.\n' + DICTIONARY['confirm_save']
                                : DICTIONARY['confirm_save']


            if(confirm(saveMessage)){

                let formData = new FormData(document.getElementById(`${this.#formId}`));

                formData.append('ROW_COUNT', selectedIdx.length);

                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let rowIdx = selectedIdx[i];

                    let param = {

                        I_UNIT_PRICE           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_PRICE'))
                        , I_REP_UOM_ID         : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_UNIT'))
                        //, I_REP_UOM_ID         : '' // TODO :: 주석해제 - 스카이젯 단위 획득
                        , I_ITEM_CODE          : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_CD'))
                        , I_ITEM_BAR_CD        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('KD_CODE'))
                        , I_ITEM_GRP_CD        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('KIND_CD'))
                        , I_ITEM_GRP_2ND_CD    : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SPECIAL_CD'))
                        , I_ITEM_GRP_3RD_CD    : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('GROUP2_CD'))
                        , I_ITEM_ENG_NM        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_NM'))
                        , I_ITEM_KOR_NM        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_NM'))
                        , I_ITEM_SHORT_NM      : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_SHORTEN'))
                        , I_ITEM_NM            : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PRODUCT_NM'))
                        , I_MAKER_NM           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MAKER_NM'))
                        , I_SYNC_YN            : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SYNC_YN'))
                        , I_MAKENO_FLAG        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MAKENO_FLAG'))
                        , I_DATE_FLAG          : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('DATE_FLAG'))
                        , I_SERIAL_FLAG        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SERIAL_FLAG'))
                        , I_MAKEDATE_FLAG      : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MAKEDATE_FLAG'))
                    } 
                                                                                                        
                    formData.append(`I_UNIT_PRICE`         +   `_${i}`, param['I_UNIT_PRICE']);
                    formData.append(`I_REP_UOM_ID`         +   `_${i}`, param['I_REP_UOM_ID']);
                    formData.append(`I_ITEM_CODE`          +   `_${i}`, param['I_ITEM_CODE']);
                    formData.append(`I_ITEM_BAR_CD`        +   `_${i}`, param['I_ITEM_BAR_CD']);
                    formData.append(`I_ITEM_GRP_CD`        +   `_${i}`, param['I_ITEM_GRP_CD']);
                    formData.append(`I_ITEM_GRP_2ND_CD`    +   `_${i}`, param['I_ITEM_GRP_2ND_CD']);
                    formData.append(`I_ITEM_GRP_3RD_CD`    +   `_${i}`, param['I_ITEM_GRP_3RD_CD']);
                    formData.append(`I_ITEM_ENG_NM`        +   `_${i}`, param['I_ITEM_ENG_NM']);
                    formData.append(`I_ITEM_KOR_NM`        +   `_${i}`, param['I_ITEM_KOR_NM']);
                    formData.append(`I_ITEM_SHORT_NM`      +   `_${i}`, param['I_ITEM_SHORT_NM']);
                    formData.append(`I_ITEM_NM`            +   `_${i}`, param['I_ITEM_NM']);
                    formData.append(`I_MAKER_NM`           +   `_${i}`, param['I_MAKER_NM']);
                    formData.append(`I_SYNC_YN`            +   `_${i}`, param['I_SYNC_YN']);
                    formData.append(`I_MAKENO_FLAG`        +   `_${i}`, param['I_MAKENO_FLAG']);
                    formData.append(`I_DATE_FLAG`          +   `_${i}`, param['I_DATE_FLAG']);
                    formData.append(`I_SERIAL_FLAG`        +   `_${i}`, param['I_SERIAL_FLAG']);
                    formData.append(`I_MAKEDATE_FLAG`      +   `_${i}`, param['I_MAKEDATE_FLAG']);

                }


                jQuery.ajax({
                    url : "/WMSIF708/syncItem.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
                        // Fail ...
                        if(!cfn_isEmpty(data) && ('O_MSG_CODE' in data) && (Number(data.O_MSG_CODE) < 0)){
                            alert(data.O_MSG_NAME);
                        }
                        // Success ...
                        else{
                            alert(`${DICTIONARY['save_success']} (${selectedIdx.length} 건)`);
                        }
            
                    },
                    error: function(request, status, error){
                        alert(`관리자에게 문의바랍니다. (${request.status})`);
                        console.log(request);
                        console.log(status);
                        console.log(error);
                    }
                });
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }

    }


    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('상품정보', true);
    }

    
    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '상품');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}