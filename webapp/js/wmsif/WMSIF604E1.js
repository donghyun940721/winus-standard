import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSIF604E1{                // !(필수) 화면 명 수정

    #custId         = null;
    #formId         = 'frm_listE1';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE1',
        id          : 'vrSrchRitemIdE1',
        name        : 'vrSrchRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });

    #headerMappingInfo = [
        {
            name: '주문정보',
            code: 'ORDER_INFO',
            startColumnKey: 'ORDER_DATE',
            endColumnKey: 'ORDER_SEQ'
        }
        , {
            name: '택배',
            code: 'DLV_INFO',
            startColumnKey: 'RCV_NM',
            endColumnKey: 'INVO_NO'
        }
        , {
            name: 'ERP',
            code: 'ERP_INFO',
            startColumnKey: 'RQSHPD',
            endColumnKey: 'PRIOCD'
        }
    ];

    #grid = new WinusGrid({ 
        spdListDiv          : 'spdListE1',
        gridTemplate        : 'WMSIF604E1',
        headerHeight        : '25',
        rowHeight           : '27',
        setColor            : false,
        useFilter           : true,
        checkFirstIdx       : 0,
        frozenColIdx        : '6',
        rowSpanColIdx       : 5,
        rowSpanColArr       : [5, 7, 8],
        headerMappingInfo   : this.#headerMappingInfo,
    });


    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성


        /** Grid 생성대기 */
        let settingTimer = setInterval(()=>{

            if (vm.#grid.created) {
                clearInterval(settingTimer);

                /** Grid 생성 후 처리 내용 기재
                 * 
                 * ex) Column Format Custom, 로딩 시 기본조회 ... 
                 */
            }
        }, 100);
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery('#vrSrchOrdDegreeE1').append("<option value=''>전체</option>");
        jQuery('#devOrdDegreePopE1').append("<option value=''>전체</option>");

        date.setDate(date.getDate() + 1); 
        jQuery("#vrSrchOrdDegreeDateE1").datepicker("setDate", date);   // 공급일 (다음날)
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){
        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }
    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){
        
        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        // * 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        /** 주문예정차수 (날짜) */
        jQuery("#vrSrchOrdDegreeDateE1").off().on("change", (e)=>{
            this.GetCustOrdDegree();
        });

        /** 주문차수 변경 */
        document.getElementById('vrSrchOrdDegreeE1').addEventListener('change', (e)=>{
            this.GetDevOrdDegree();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
            // this.GetSummaryData(e);
        });

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE1").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){
        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);

            this.GetCustOrdDegree();
        }

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 

    /**
     * 주문예정차수 셋팅
     * 
     * @param {*} id        : 컴포넌트 ID
     * @param {Array} data  : 바인딩 Data (/WMSOP642/getCustOrdDegree.action)
     */
    SetCustOrdDegree(id, data){
        let cnt = data.size();

        // Init
        jQuery(`#${id} option`).remove();

        jQuery(`#${id}`).append("<option value=''>전체</option>");
        
        if(cnt > 0){
            for (var i = 0; i < cnt; i++){
                jQuery(`#${id}`).append("<option value='"+data[i].CODE+"'>"+data[i].CODE+"</option>");
            }
        }

        this.GetDevOrdDegree();
    }

//#endregion


//#region   :: CallBack Event

    SearchClickEventCallback (e){
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        formData.append('vrCustId'     , this.#custId);

        return jQuery.ajax({
            url : "/WMSIF604/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });
    }

    /**
     * 주문예정차수 데이터 조회
     * 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     * @param {string} componentId      : 조회값을 바인딩할 컴포넌트 ID
     * @param {string} ordDt            : 출고예정일
     */
    GetCustOrdDegree(componentId="vrSrchOrdDegreeE1", ordDt){
    
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.append('vrCustId', jQuery("#vrSrchCustIdE1").val());
        formData.append('vrOrdDt', ordDt ?? jQuery("#vrSrchOrdDegreeDateE1").val());

        return jQuery.ajax({
            url : "/WMSOP642/getCustOrdDegree.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                // Error ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{

                    if('DS_ORD_DEGREE' in data){
                        vm.SetCustOrdDegree(componentId, data.DS_ORD_DEGREE);
                    }
                }
            }
        });
    }

    GetDevOrdDegree () {
        const ordDegree = jQuery("#vrSrchOrdDegreeE1").val();
        let formData = new FormData();

        formData.append('vrCustId', this.#custId);
        formData.append('vrOrdDt', jQuery("#vrSrchOrdDegreeDateE1").val());
        formData.append('ordDegree', ordDegree === '' ? "ALL" : ordDegree);
        formData.append('vrSrchOrdBizType', "B2C");

        // Init
        jQuery(`#devOrdDegreeE1 option`).remove();

        return jQuery.ajax({
            url : "/WMSOP910/getDevOrdDegree.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){
                // Error ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{
                    var data = data.DS_DEV_ORD_DEGREE;
                    var cnt  = data.size();

                    jQuery('#devOrdDegreeE1').append("<option value=''>전체</option>");
                    for (var i = 0; i < cnt; i++){
                        jQuery('#devOrdDegreeE1').append("<option value='"+data[i].CODE+"'>"+ data[i].CODE_NM + "</option>");
                    }
                }
            }
        });
    }

    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad('DAS 전송이력', false);
    }

    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                vm.GetCustOrdDegree();

                break;

            //* 상품
            case COMP_CATEGORY.ITEM : 
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);
                break;
        }
    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];

                        vm.GetCustOrdDegree();
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }
    
    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }
//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}