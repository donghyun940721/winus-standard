import * as CustomPopUp             from './WMSIF780E1pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY, MultiSelectBox}     from '../component/contentFilter.js';

export class WMSIF780E4 {

    #custId         = null;
    #formId         = 'frm_listE4';
    #apiCustId      = null;

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE4',
        id          : 'vrSrchCustIdE4',
        name        : 'vrSrchCustNmE4',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE4',
        id          : 'vrSrchRitemIdE4',
        name        : 'vrSrchRitemNmE4',
        category    : COMP_CATEGORY.ITEM
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        /*
        {
            name: 'ERP',                     // 그룹헤더에 출력될 명칭
            code: 'ERP_SYS',                 // 그룹헤더 Key값 (아직 사용저 없음)
            startColumnKey: 'CHUL_EPT_DATE',       // 그룹지정 시작 열의 Key값
            endColumnKey: 'WMS_NO'           // 그룹지정 마지막 열의 Key값
        },
        {
            name: 'WMS',                     // 그룹헤더에 출력될 명칭
            code: 'WMS_SYS',                 // 그룹헤더 Key값 (아직 사용저 없음)
            startColumnKey: 'ORD_ID',       // 그룹지정 시작 열의 Key값
            endColumnKey: 'RITEM_ID'           // 그룹지정 마지막 열의 Key값
        }*/
    ];


    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE4',
        gridTemplate        :   'WMSIF780E4',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        useFilter           :   true,
        useSort             :   true,
        headerMappingInfo   :   this.#headerMappingInfo,
    });

    #select2 = new MultiSelectBox({
        htmlObjId : this.#formId+' select[name="vrSrchItemGrpId"]',
        url : '/WMSMS094/selectItemGrpLvl.action',
        fn_sucess : this.SelctItemGpr1StEventCallBack,
    });

    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

        let selectFormData = new FormData();
        selectFormData.append('vrGrpLevel',1);

        this.#select2.form = selectFormData;

        this.#select2.Init();
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE4").datepicker("setDate", date);                         // 입고일자 (from)
        jQuery("#vrSrchReqDtToE4").datepicker("setDate", date);                           // 입고일자 (to)
        
        //this.#apiCustId = DICTIONARY['ss_svc_no'] == '0000004080' ? "0000488488" : null;
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE4").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE4").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE4").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE4").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE4").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE4").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE4").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE4").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE4").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        // 입고결과송신 버튼 이벤트
        jQuery("#btnSendInResult").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SendClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE4").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE4").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE4").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE4").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                fn_Popup('CUST');
            }

        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }
        
        /* 오픈몰 정보 설정*/
        this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE4", this.#formId, this.#apiCustId??this.#custId);
        
        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }

    SelctItemGpr1StEventCallBack(result){
        let data = result.rows;
        let ListArray = new Array();
        for (var obj of data){
            ListArray.push({
                value : obj.CODE_CD,
                name : obj.CODE_NM
            })
        };
        return ListArray;
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE4", this.#formId, this.#apiCustId??this.#custId);

                break;

            //* 상품
            case COMP_CATEGORY.ITEM :
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);

                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        
                        vm.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE4", this.#formId, this.#apiCustId??this.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    SendClickEventCallback(e){
        try {

            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = null;

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 주문을 선택해야 합니다.`;
            }

            
            saveMessage = gridData.some(dt => dt['SYNC_YN'] == 'Y') 
                                ? '이미 동기화된 주문이 존재합니다.\n' + DICTIONARY['confirm_save']
                                : DICTIONARY['confirm_save']
            

            if(confirm(saveMessage)){

                let formData = new FormData(document.getElementById(`${this.#formId}`));

                formData.append('ROW_COUNT', selectedIdx.length);
                formData.append('CUST_ID', vm.#custId);
                formData.append('OPENMALL_CD', "GOODMD");

                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let rowIdx = selectedIdx[i];

                    let workStat = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('WORK_STAT'));
                    let ritemId = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('RITEM_ID'));

                    if (ritemId == null || ritemId == ''){
                        throw `상품등록이 안된 주문이 있습니다.`;
                    }
                    else if (!(workStat == '990' || workStat == '')){
                        throw ` 작업 완료가 아닌 주문이 있습니다.`;
                    }

                    let param = {
                          I_ORD_ID        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORD_ID'))
                        , I_ORD_SEQ          : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ORD_SEQ'))
                    };

                    formData.append(`I_ORD_ID`        + `${i}`, param['I_ORD_ID']);
                    formData.append(`I_ORD_SEQ`          + `${i}`, param['I_ORD_SEQ']);
                }

                jQuery.ajax({
                    url : "/WMSIF780/sendInResult.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
                        data = JSON.parse(data.RESULT);
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("ErrCode" in data) && (Number(data["ErrCode"]) < 0)){
                            alert(data["OutputMessage"]);
                        }
                        // Success ...
                        else{
                            alert(data["OutputMessage"]+ `(${selectedIdx.length} 건)`);
                        }
            
                    },
                    error: function(request, status, error){
                        alert(`관리자에게 문의바랍니다. (${request.status})`);
                        console.log(request);
                        console.log(status);
                        console.log(error);
                    }
                });
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }

    }

    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('출고주문정보', true);
    }

    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
     SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let selectList = this.#select2.getSelectedListItem();
        formData.append('vrSrchItemGrpCnt', selectList.count);
        return jQuery.ajax({
            url : "/WMSIF780/listE4.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    //let packCnt = data.rows.map(dt=> (dt.PACK == 0) ? dt.SEQ : dt.PACK).filter((value, index, self)=>{return self.indexOf(value) === index;}).length;

                    //jQuery("#totalDlvCnt").text(packCnt);                                               // 합포단위수량
                    //jQuery("#totalOrdCnt").text(vm.#grid.GetRowCountByDistinctKey('SEQ'));              // 관리번호
                    //jQuery("#totalProcCnt").text(vm.#grid.GetRowCountByDistinctKey('PRD_SEQ'));         // 상품관리번호
                }

            }
        });

    }

    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '출고');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}