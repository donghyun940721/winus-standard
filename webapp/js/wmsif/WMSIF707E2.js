import * as InterfaceUtil           from '../utility/interfaceUtil.js';
import * as Description             from '../component/description.js'
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }   from '../utility/userStorageManger.js';


export class WMSIF707E2{

    #CODE           = 'CODE';
    #NAME           = 'NAME';
    
    #custId         = null;
    #formId         = 'frm_listE2';

    #grid = new WinusGrid({ 
        spdListDiv          : "spdListE2",
        gridTemplate        : "WMSIF707E2",     // Grid 구성요소 기준정보 코드
        multiCheckRange     : [0, 1],           // [Select Column]
        rowHeight           : '22',             // 행 높이
        headerHeight        : '27',             // 헤더 높이
        frozenColIdx        : 4,                // 틀 고정
        useFilter           : true,             // 필터
        // exRowSpanColIVal    : '0',
        rowSpanColIdx       : 4,                // 행 병합
        rowSpanColArr       : [0, 4],       // TODO :: 기준정보 옵션으로 추가되야함.
        setColor            : false,
        exFileExportArr     : [0]
    });

    constructor(args){

        this.Init();
        this.AddEvent();
        this.InitialEventCallBack();

        this.#grid.Init();
        this.#grid.SetGrid();
    }


    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });
                
        jQuery("#vrSrchReqDtFromE2").datepicker("setDate", date);     // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE2").datepicker("setDate", date);       // 수집등록일자 (to)


        /**  설명란 입력 */
        let text1_1 = '[재수집조건]';
        let text2_1 = '주문상태 : '     , text2_2 = '접수상태';
        let text3_1 = '날짜기준 : '     , text3_2 = '발주일';
        let text4_1 = '날짜 : '         , text4_2 = '금일 기준 1개월전';
        let text5_1 = '수집건제한 : '   , text5_2 = '500건';

        Description.Add('INFO_MAIN_CELL_01', null, text1_1, null, Description.HIGHLIGHT.BOLD_BLUE, null);
        Description.Add('INFO_MAIN_CELL_01', Description.ROUND_NUMBER[1], text2_1, text2_2);
        Description.Add('INFO_MAIN_CELL_01', Description.ROUND_NUMBER[2], text3_1, text3_2);
        Description.Add('INFO_MAIN_CELL_01', Description.ROUND_NUMBER[3], text4_1, text4_2);
        Description.Add('INFO_MAIN_CELL_01', Description.ROUND_NUMBER[4], text5_1, text5_2);

        Description.BindHoldingEvent('desc_infoE2', 'INFO_MAIN_CELL_01');
    }


    /**
     * 전역변수 Level의 이벤트 콜백 바인딩
     * 
     */
    InitialEventCallBack(){
        CustPopUpEventCallBack = this.SelectedCustCallBack;
    }


    AddEvent(args){

        // # 화주
        document.getElementById("vrSrchCustCdE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(this.#CODE, e.target.value, 'vrSrchCustCdE2');
        });

        jQuery("#vrSrchCustImgE2").off().on("click", (e)=>{
            fn_Popup("CUST");
        });
        
        document.getElementById("vrSrchCustNmE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(this.#NAME, e.target.value, 'vrSrchCustNmE2');
        });

        // 화주 팝업 선택 후처리 이벤트
        jQuery("#vrSrchCustCdE2").off().on("custom", (e, customParam)=>{
            this.CustCustomEventCallBack();
        });
        
        // # '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE2").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE2").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                fn_Popup('CUST');
            }
        });

        // # '주문수집' 버튼 클릭 이벤트
        jQuery("#btnCollectOrdE2").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustCdE2").val())){
                this.OrdCollectClickEventCallBack();
            }
            else{
                fn_Popup('CUST');
            }
        });

        // # "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE2").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // '그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE2").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // Init Common CallBack
        this.InitialEventCallBack();

        // Set Cust Info
        if(cfn_isEmpty(jQuery('#vrSrchCustCdE2').val())){
            jQuery('#vrSrchCustCdE2').val(SessionUtil.GetValue(SessionKey.CUST_CD));
            jQuery('#vrSrchCustNmE2').val(SessionUtil.GetValue(SessionKey.CUST_NM));
            jQuery("#vrSrchCustIdE2").val(SessionUtil.GetValue(SessionKey.CUST_ID));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        // 오픈몰 정보 조회
        let mallInfos = InterfaceUtil.OpenMallInfo.GetInfoByCustId(this.#custId);

        jQuery("#vrSrchSendCompanyIdE2 option").remove();

        if(!cfn_isEmpty(mallInfos)){
            // 오픈몰 정보 바인딩
            for(let mallInfo of mallInfos){

                let option = new Option();

                option.value = mallInfo['MASTER_SEQ'];
                option.text = mallInfo['OPENMALL_CD_DESC'];
                
                jQuery("#vrSrchSendCompanyIdE2").append(option);
            }
        }

        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: CallBack Event


    CustChangedEventCallBack(type, refValue, id){

        let that = this;
        let paramKey = type == this.#CODE ? 'vrSrchCustCd' : 'vrSrchCustNm';
        let callback = "CustPopUpEventCallBack";
        
        if(!cfn_isEmpty(refValue)){
            
            var param   = `srchKey=CUST&${paramKey}=${refValue}&S_CUST_TYPE=12`;
            var url     = "/selectMngCode.action"
            
            jQuery.post(url, param, function(output){

                let data = output.CUST;

                if(cfn_isEmpty(data)){
                    jQuery('#vrSrchCustCdE2').val("").focus();
                    jQuery('#vrSrchCustNmE2').val("");
                    jQuery("#vrSrchCustIdE2").val("");
                    alert(DICTIONARY['list.nodata']);
                }
                else{
                    var strParameter = [];
                    strParameter.push(type == that.#CODE ? refValue : '');
                    strParameter.push(type == that.#NAME ? refValue : '');
                    var width  = "800";
                    var height = "530";
                    var param  = `?func=${callback}&custType=12&strParameter=${strParameter}&stBtnYn=N`;
                    var url    = "/WMSCM011.action" + param;
                    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
                    asppop.focus();
                }

            }).fail(function(xhr, textStatus, err){

            });
        }else{
            jQuery('#vrSrchCustCdE2').val("");
            jQuery('#vrSrchCustNmE2').val("");
            jQuery("#vrSrchCustIdE2").val("");
        }
    }


    SelectedCustCallBack(arr){
        var info = arr[0];	
        jQuery('#vrSrchCustCdE2').val(info.cust_cd);
        jQuery('#vrSrchCustNmE2').val(info.cust_nm);
        jQuery("#vrSrchCustIdE2").val(info.cust_id);


        jQuery('#vrSrchCustCdE2').trigger('custom', ['customEvent'])
    }


    /**
     * 화주 팝업 선택 이후 수행되는 커스텀 이벤트 콜백함수.
     */
    CustCustomEventCallBack(){

        let vm = this;
        this.#custId = jQuery('#vrSrchCustIdE2').val();

        // 오픈몰 정보 조회
        InterfaceUtil.OpenMallInfo.GetAuthInfo(this.#custId, 'EZADMIN', function(){

            let mallInfos = InterfaceUtil.OpenMallInfo.GetInfoByCustId(vm.#custId);

            jQuery("#vrSrchSendCompanyIdE2 option").remove();

            if(!cfn_isEmpty(mallInfos)){
                // 오픈몰 정보 바인딩
                for(let mallInfo of mallInfos){

                    let option = new Option();

                    option.value = mallInfo['MASTER_SEQ'];
                    option.text = mallInfo['OPENMALL_CD_DESC'];
                    
                    jQuery("#vrSrchSendCompanyIdE2").append(option);
                }
            }
        });

        // 세션 기록
        SessionUtil.Insert(SessionKey.CUST_CD, jQuery('#vrSrchCustCdE1').val());
        SessionUtil.Insert(SessionKey.CUST_NM, jQuery('#vrSrchCustNmE1').val());
        SessionUtil.Insert(SessionKey.CUST_ID, this.#custId);
    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     * @param {boolean} isSrchOrdDegree : 주문예정차수 조건 검색 여부
     */
    SearchClickEventCallback(e, isProgressBar=true, isSrchOrdDegree=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        jQuery.ajax({
            url : "/WMSIF707/listE2.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    // jQuery("#totalSpdList").text(vm.#grid.GetRowCountByDistinctKey('SEQ'));
                }

            }
        });
    }


    /**
     * 주문수집 클릭 이벤트 콜백함수
     * - 주문상태   : 제한없음
     * - CS상태     : 제한없음
     * 
     * @param {*} e 
     */
    OrdCollectClickEventCallBack(e){

        try{
            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
    
            if(cfn_isEmpty(jQuery("#vrSrchSendCompanyIdE2").val())){
                alert(`오픈몰계정을 선택해야 합니다.`);
                return;
            }
    
            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 주문을 선택해야 합니다.`;
            }
    
            if(confirm(DICTIONARY['confirm_save'])){

                let formData        = new FormData(document.getElementById(`${this.#formId}`));
                let orderIdList     = new Array();
                let selectedData    = this.#grid.GetSelectedRowData();
                let openMallId      = jQuery("#vrSrchSendCompanyIdE2").val();

                /** API Property */
                let partnerKey  = InterfaceUtil.OpenMallInfo.GetValueOfKey(this.#custId, openMallId, 'OPENMALL_API_KEY1');
                let domainKey   = InterfaceUtil.OpenMallInfo.GetValueOfKey(this.#custId, openMallId, 'OPENMALL_API_KEY2');
                let action      = 'get_order_info';
                let limit       = '500';            // Max Fix
                let page        = '1';              // Fix
                let dateType    = 'collect_date';   // 발주일, 주문일(order_date)
                let status      = '1'               // 접수상태

                orderIdList = selectedData.map(dt=> dt['ORDER_ID']);

                formData.append('CUST_ID'       ,   this.#custId    );
                formData.append('partner_key'   ,   partnerKey      );
                formData.append('domain_key'    ,   domainKey       );
                formData.append('action'        ,   action          );
                formData.append('limit'         ,   limit           );
                formData.append('page'          ,   page            );
                formData.append('date_type'     ,   dateType        );
                formData.append('orderIdList'   ,   orderIdList     );
                formData.append('status'        ,   status          );

                jQuery.ajax({
                    url : "/WMSIF707/MissOrderCollect.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();  // 호출객체 CallBack 함수 수행
                    },
                    success : function(data){
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("ERROR" in data)){
    
                            throw data["ERROR"];
                        }
                        // Success ...
                        else{
                            let successCnt  = data['SUCCESS_CNT'] ?? 0;
                            let failCnt     = data['FAIL_CNT'] ?? 0;
    
                            alert(DICTIONARY['save_success'] + ` (성공: ${successCnt}건)` + `\n조회내역을 갱신합니다.`);
                        }
    
                    },
                    error: function(request, status, error){
                        throw request.status;
                    }
                });
            }


            console.log('[DEBUG] Finish...');
            
        }catch(error){

        }
    }


    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '미수집주문조회');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('미수집주문정보');
    }


//#endregion

}