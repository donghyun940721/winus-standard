import * as InterfaceUtil from '../utility/interfaceUtil.js';


const ITEM_COLLECT_POP = "popItemCollect";


/**
 * 상품정보 수집
 */
export let ItemCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();    
        
        jQuery("#vrColStDateFrom").val(localStorage.LocalTime);
        jQuery("#vrColStDateTo").val(localStorage.LocalTime);
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollect").off().on("click", (e)=>{
            fn_itemCollect();
        });
    }


    /**
     * [이지어드민] 상품정보 수집 I/F 실행 
     * 
     */
    function fn_itemCollect(){

        let formData    = new FormData();

        let partnerKey  = InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_KEY1');
        let domainKey   = InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_KEY2');

        let action      = 'get_product_info';
        let fromDt      = jQuery("#vrColStDateFrom").val();
        let toDt        = jQuery("#vrColStDateTo").val();
        let dateType    = jQuery("#vrCollectType").val();
        let barcode     = '';
        let new_link_id = '';
        let limit       = '500';
        let page        = "1";
    
        formData.append('CUST_ID'      ,   custId     );
        formData.append('partner_key'  ,   partnerKey );
        formData.append('domain_key'   ,   domainKey  );
        formData.append('action'       ,   action     );
        formData.append('date_type'    ,   dateType   );
        formData.append('start_date'   ,   fromDt     );
        formData.append('end_date'     ,   toDt       );
        formData.append('barcode'      ,   barcode    );
        formData.append('new_link_id'  ,   new_link_id);
        formData.append('limit'        ,   limit      );
        formData.append('page'         ,   page       );
    
        jQuery.ajax({
            url : "/WMSIF706/itemCollect.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${ITEM_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
    
                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
    
                    throw data["MSG"];
                }
                // Success ...
                else{
                    alert('저장되었습니다.');
                }
    
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    return {
        Init : Init
    }
})()


/**
 * 상품정보 비교 (Winus <> EzAdmin)
 * 
 * 동기화된 상품을 대상으로 현 Winus 등록정보와 비교한다.
 * 
 */
export let ItemCompare = (function(){


    let Init = function(args){

        jQuery("#txtWinusItemNm").text(args['WMS_ITEM_NM'] ?? '');
        jQuery("#txtWinusItemcode").text(args['WMS_ITEM_CD'] ?? '');
        jQuery("#txtWinusItemBarcode").text(args['WMS_ITEM_BAR'] ?? '');
        jQuery("#txtWinusRegDt").text(args['WMS_REG_DT'] ?? '');
        jQuery("#txtWinusUpdDt").text(args['WMS_UPD_DT'] ?? '');

        jQuery("#txtEzadminItemNm").text(args['EZM_ITEM_NM'] ?? '');
        jQuery("#txtEzadminOptNm").text(args['EZM_OPT_NM'] ?? '');
        jQuery("#txtEzadminItemBarcode").text(args['EZM_ITEM_BAR'] ?? '');
        jQuery("#txtEzadminRegDt").text(args['EZM_REG_DT'] ?? '');
        jQuery("#txtEzadminUpdDt").text(args['EZM_UPD_DT'] ?? '');
        
    }


    return {
        Init : Init
    }

})();