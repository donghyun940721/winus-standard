import * as InterfaceUtil           from '../utility/interfaceUtil.js';
import * as CustomPopUp             from './WMSIF707E1pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }   from '../utility/userStorageManger.js';

export class WMSIF707E1{

    #CODE           = 'CODE';
    #NAME           = 'NAME';
    #formId         = 'frm_listE1';    

    #custId         = null;                     // 화주 ID
    #salesCompList  = new Array();              // 판매처 목록
    #transCorpInfo  = new Array();              // 화주별 택배사 맵핑정보 (Winus-EzAdmin)

    #csState    = ['00', '01', '02', '03', '04', '05', '06', '07', '08']; // CS상태값 (In 이지어드민 API 스펙)
    #holdState  = ['00', '01', '02', '03', '04', '05', '06'];           // 보류 상태값 (In 이지어드민 API 스펙)

    #headerMappingInfo = [
        {
            name: '주문자',
            code: 'SEND_GROUP',
            startColumnKey: 'ORDER_NAME',
            endColumnKey: 'ORDER_TEL'
        },
        {
            name: '수령자',
            code: 'RECEIVE_GROUP',
            startColumnKey: 'RECV_NAME',
            endColumnKey: 'RECV_ZIP'
        },
        {
            name: '상품정보',
            code: 'ITEM_GROUP',
            startColumnKey: 'PRD_SEQ',
            endColumnKey: 'DET_OPTIONS'
        }
    ];

    #grid = new WinusGrid({ 
        spdListDiv          : "spdListE1",
        gridTemplate        : "WMSIF707E1",         // Grid 구성요소 기준정보 코드
        checkFirstIdx       : 0,
        multiCheckRange     : [0, 1],               // [Select Column]
        rowHeight           : 22,                   // 행 높이
        headerHeight        : 27,                   // 헤더 높이
        frozenColIdx        : 6,                    // 틀 고정
        useFilter           : true,                 // 필터
        // exRowSpanColIVal    : '0',
        rowSpanColIdx       : 68,                    // 행 병합
        rowSpanColArr       : [0, 3, 4, 14],          // TODO :: 기준정보 옵션으로 추가되야함.
        setColor            : false,                // Cell 단위 설정 배경색 적용
        headerMappingInfo   : this.#headerMappingInfo,
        exFileExportArr     : [0]
    });

    get grid() {  return this.#grid;  }


    constructor(args){

        if(args != undefined){
            
        }

        this.Init();
        this.AddEvent();
        this.InitialEventCallBack();

        this.#grid.Init();
        this.#grid.SetGrid();
    }


    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        
        jQuery("#vrSrchReqDtFrom").datepicker("setDate", date);                         // 수집등록일자 (from)
        jQuery("#vrSrchReqDtTo").datepicker("setDate", date);                           // 수집등록일자 (to)
        jQuery("#vrSrchOrdDegreeDate").datepicker("setDate", date);                     // 주문예정차수 (날짜)
        jQuery("#vrSrchTypeDtFromE1").datepicker("setDate", date.addDate(0, -1));       // 날짜타입별 일자 (from) : 한 달 단위 조회
        jQuery("#vrSrchTypeDtToE1").datepicker("setDate", date);                        // 날짜타입별 일자 (to)
    }


    /**
     * 전역변수 Level의 이벤트 콜백 바인딩
     * 
     */
    InitialEventCallBack(){
        CustPopUpEventCallBack      = this.SelectedCustCallBack;
        ItemPopUpEventCallBack      = this.SelectedItemCallBack;
        CustShopPopUpEventCallBack  = this.SelectedCustShopCallBack;
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * 1. 화주 
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            fn_Popup("CUST");
        }
        else{
            jQuery("#vrSrchCustIdE1").val(custId);
            jQuery('#vrSrchCustNmE1').val(custNm);
            jQuery('#vrSrchCustCdE1').val(custCd);
            this.CustCustomEventCallBack();
        }
    }


    AddEvent(args){

        // TODO :: Event Remove

        // * 주문예정차수 (날짜)
        jQuery("#vrSrchOrdDegreeDate").off().on("change", (e)=>{
            this.OrdDegreeDtChangedCallBack(e);
        });

        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(this.#CODE, e.target.value, 'vrSrchCustCdE1');
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            fn_Popup("CUST");
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(this.#NAME, e.target.value, 'vrSrchCustNmE1');
        });


        // * 화주 팝업 선택 후처리 이벤트
        jQuery("#vrSrchCustCdE1").off().on("custom", (e, customParam)=>{
            this.CustCustomEventCallBack();
        });

        
        // * 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(this.#CODE, e.target.value, 'vrSrchRitemCdE1');
        });

        jQuery("#vrSrcRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : jQuery("#vrSrchCustIdE1").val()
            }

            fn_Popup("ITEM", param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(this.#NAME, e.target.value, 'vrSrchRitemNmE1');
        });


        //* 거래처정보 (판매처 : 3)
        jQuery("#vrSalesCompCdE1").off().on("change", (e)=>{
            this.ShopChangedEventCallBack(this.#CODE, e.target.value, 'vrSalesCompCdE1');
        });

        jQuery("#vrSalesCompImgE1").off().on("click", (e)=>{
            let param = {
                'LC_ID'             : jQuery("select[name='SVC_INFO']").val(),
                'TRANS_CUST_ID'     : this.#custId
            }

            fn_Popup('CUST_SHOP', param);
        });

        jQuery("#vrSalesCompNmE1").off().on("change", (e)=>{
            this.ShopChangedEventCallBack(this.#NAME, e.target.value, 'vrSalesCompNmE1');
        });


        // * "주문수집" 버튼 클릭 이벤트
        jQuery("#btnCollectOrd").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.OrdCollectClickEventCallBack();
            }
            else{
                fn_Popup('CUST');
            }

        });


        // * "주문등록" 버튼 클릭 이벤트
        jQuery("#btnOrdRegist").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.OrdRegistClickEventCallBack();
            }
            else{
                fn_Popup('CUST');
            }

        });


        // * "택배송장등록" 버튼 클릭 이벤트
        jQuery("#btnDlvSend").off().on("click", (e)=>{
            this.DlvSendClickEventCallBack(e);
        });


        // * "삭제" 버튼 클릭 이벤트
        jQuery("#btnDelete").off().on("click", (e)=>{
            this.DeleteClickEventCallBack(e);
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_write").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                fn_Popup('CUST');
            }

        });


        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE1").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // Init Common CallBack
        this.InitialEventCallBack();

        // Set Cust Info
        if(cfn_isEmpty(jQuery('#vrSrchCustCdE1').val())){
            jQuery('#vrSrchCustCdE1').val(SessionUtil.GetValue(SessionKey.CUST_CD));
            jQuery('#vrSrchCustNmE1').val(SessionUtil.GetValue(SessionKey.CUST_NM));
            jQuery("#vrSrchCustIdE1").val(SessionUtil.GetValue(SessionKey.CUST_ID));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set


    /**
     * 주문예정차수 데이터 조회
     * 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     * @param {string} componentId      : 조회값을 바인딩할 컴포넌트 ID
     * @param {string} ordDt            : 출고예정일
     */
    GetCustOrdDegree(isProgressBar=true, componentId="vrSrchOrdDegree", ordDt){
        
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.append('vrCustId', jQuery("#vrSrchCustIdE1").val());
        formData.append('vrOrdDt', ordDt ?? jQuery("#vrSrchOrdDegreeDate").val());

        return jQuery.ajax({
            url : "/WMSOP642/getCustOrdDegree.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Error ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{

                    if('DS_ORD_DEGREE' in data){
                        vm.SetCustOrdDegree(componentId, data.DS_ORD_DEGREE);
                    }
                }
            }
        });
    }


    /**
     * 주문예정차수 셋팅
     * 
     * @param {*} id        : 컴포넌트 ID
     * @param {Array} data  : 바인딩 Data (/WMSOP642/getCustOrdDegree.action)
     */
    SetCustOrdDegree(id, data){

        let cnt = data.size();

        // Init
        jQuery(`#${id} option`).remove();
        
		if(cnt > 0){
			for (var i = 0; i < cnt+1; i++){
				if(i == cnt){
					jQuery(`#${id}`).append("<option value='"+(Number(data[i-1].CODE)+1)+"'>"+(Number(data[i-1].CODE)+1)+" (신규예정)</option>");
					jQuery(`#${id}`).val(Number(data[i-1].CODE)+1);
				}else{
					jQuery(`#${id}`).append("<option value='"+data[i].CODE+"'>"+data[i].CODE+"</option>");
				}
			}
		}else{
			jQuery(`#${id}`).append("<option value='1'>1 (신규예정)</option>");
		}
    }


    /**
     * 택배사 코드 조회
     * 
     */
    GetTransCorpInfo(){
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.append('CUST_ID', jQuery("#vrSrchCustIdE1").val());

        jQuery.ajax({
            url : "/WMSIF707/getTransCorpInfo.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){
                if('RESULT' in data){
                    vm.#transCorpInfo = data['RESULT'];
                }
                else{
                    alert('택배사 기준정보 등록되지 않은 화주입니다.');
                }
            }
        });
    }


    /**
     * 판매처 조회 및 컴포넌트 셋팅
     * [기준관리] > "거래처정보관리" : 판매처 타입 거래처 조회
     * 
     */
    GetSalesCompany(){

        let vm          = this;
        let formData    = new FormData(document.getElementById(this.#formId));

        formData.append('S_LC_ID', jQuery("select[name='SVC_INFO']").val());
        formData.append('S_CUST_ID', this.#custId);
        formData.append('S_CUST_TYPE', 3);                      // 거래처 타입 (판매처 : 3)
        formData.append('rows', 1000);                          // 최대수량 조회
        
        jQuery.ajax({
            url : "/WMSCM011/list.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){
                if(('rows' in data) && data['rows'].length > 0 ){
                    vm.SetSalesCompaney(data.rows);
                }
            }
        });

    }


    SetSalesCompaney(salesInfos){
        this.#salesCompList = salesInfos.map((dt)=>{
            return { 'CUST_CD' : dt.CUST_CD, 'CUST_NM' : dt.CUST_NM };
        });
    }


//#endregion


//#region   :: CallBack Event


    OrdDegreeDtChangedCallBack(e){
        if(cfn_isEmpty(this.#custId)){
            alert("$!lang.getText('화주를 선택해 주십시오.')");
            fn_Popup('CUST');
        }
        else{
            this.GetCustOrdDegree();
        }
    }


    CustChangedEventCallBack(type, refValue, id){

        let vm = this;
        let paramKey = type == this.#CODE ? 'vrSrchCustCd' : 'vrSrchCustNm';

        if(!cfn_isEmpty(refValue)){
            
            var param   = `srchKey=CUST&${paramKey}=${refValue}&S_CUST_TYPE=12`;
            var url     = "/selectMngCode.action"
            
            jQuery.post(url, param, function(output){

                let data = output.CUST;

                if(cfn_isEmpty(data)){
                    jQuery('#vrSrchCustCdE1').val("").focus();
                    jQuery('#vrSrchCustNmE1').val("");
                    jQuery("#vrSrchCustIdE1").val("");
                    alert(DICTIONARY['list.nodata']);
                }
                else{

                    let popParam = {};
                    popParam[type == vm.#CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
        
                    fn_Popup("CUST", popParam);
                }

            }).fail(function(xhr, textStatus, err){

            });
        }else{
            jQuery('#vrSrchCustCdE1').val("");
            jQuery('#vrSrchCustNmE1').val("");
            jQuery("#vrSrchCustIdE1").val("");
        }
    }

    
    /**
     * 화주선택 팝업 후처리 콜백함수
     * 
     * @param {*} arr 
     */
    SelectedCustCallBack(arr){

        var info = arr[0];	
        jQuery('#vrSrchCustCdE1').val(info.cust_cd);
        jQuery('#vrSrchCustNmE1').val(info.cust_nm);
        jQuery("#vrSrchCustIdE1").val(info.cust_id);

        jQuery('#vrSrchCustCdE1').trigger('custom', ['customEvent']);
    }


    /**
     * 화주 팝업 선택 이후 수행되는 커스텀 이벤트 콜백함수.
     */
    CustCustomEventCallBack(){

        let vm = this;
        this.#custId = jQuery('#vrSrchCustIdE1').val();


        // 오픈몰 정보 조회
        InterfaceUtil.OpenMallInfo.GetAuthInfo(this.#custId, 'EZADMIN', function(){

            let mallInfos = InterfaceUtil.OpenMallInfo.GetInfoByCustId(vm.#custId);

            jQuery("#vrSrchSendCompanyIdE1 option").remove();

            if(!cfn_isEmpty(mallInfos)){

                // 오픈몰 정보 바인딩
                for(let mallInfo of mallInfos){

                    let option = new Option();

                    option.value = mallInfo['MASTER_SEQ'];
                    option.text = mallInfo['OPENMALL_CD_DESC'];
                    
                    jQuery("#vrSrchSendCompanyIdE1").append(option);
                }
            }
        });

        //* 주문예정차수 조회 
        this.GetCustOrdDegree();

        //* 택배사 코드조회
        this.GetTransCorpInfo();

        //* 판매처 조회
        this.GetSalesCompany();

        // 세션 기록
        SessionUtil.Insert(SessionKey.CUST_CD, jQuery('#vrSrchCustCdE1').val());
        SessionUtil.Insert(SessionKey.CUST_NM, jQuery('#vrSrchCustNmE1').val());
        SessionUtil.Insert(SessionKey.CUST_ID, this.#custId);
    }


    ItemChangedEventCallBack(type, refValue, id){

        let vm = this;
        let paramKey = type == this.#CODE ? 'vrSrchItemCd' : 'vrSrchRitemNm';

        if(!cfn_isEmpty(refValue)){
            
            var param   = `srchKey=ITEM&${paramKey}=${refValue}`;
            var url     = "/selectMngCode.action"

            jQuery.post(url, param, function(output){
                var data = output.ITEM;

                if(cfn_isEmpty(data)){
                    jQuery('#vrSrchRitemCdE1').val("").focus();
                    jQuery('#vrSrchRitemIdE1').val("");
                    jQuery('#vrSrchRitemNmE1').val("");
                    alert(DICTIONARY['list.nodata']);
                }
                else{

                    let popParam = {};
                    popParam[type == vm.#CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                    popParam['CUST_ID'] = vm.#custId;
        
                    fn_Popup("ITEM", popParam);
                }

            }).fail(function(xhr, textStatus){
                
            });
        }else{
            jQuery('#vrSrchRitemCdE1').val("");
            jQuery('#vrSrchRitemIdE1').val("");
            jQuery('#vrSrchRitemNmE1').val("");
        }

    }

    /**
     * 상품선택 팝업 후처리 콜백함수
     * 
     * @param {*} arr 
     */
    SelectedItemCallBack(arr){

        var info = arr[0];	
        jQuery('#vrSrchRitemCdE1').val(info.item_code);
        jQuery('#vrSrchRitemIdE1').val(info.ritem_id);
        jQuery("#vrSrchRitemNmE1").val(info.item_kor_nm);

    }


    ShopChangedEventCallBack(type, refValue, id){

        if(cfn_isEmpty(refValue)){ return ; }

        let vm = this;
        let paramKey = type == this.#CODE ? 'vrSrchCustCd' : 'vrSrchCustNm';
        let formData = new FormData(document.getElementById(this.#formId));

        formData.append('srchKey', 'CUST');
        formData.append('custId', 'OK');
        formData.append(paramKey, refValue);
        
        jQuery.ajax({
            url : "/selectMngCode.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){
                if(cfn_isEmpty(data)){
                    jQuery('#vrSalesCompCdE1').val("").focus();
                    jQuery('#vrSalesCompIdE1').val("");
                    jQuery('#vrSalesCompNmE1').val("");
                    alert(DICTIONARY['list.nodata']);
                }
                
                if('CUST' in data){
                    if(data.CUST.length == 1){
                        jQuery('#vrSalesCompCdE1').val(data.CUST.CUST_CD).focus();
                        jQuery('#vrSalesCompIdE1').val(data.CUST.CUST_ID);
                        jQuery('#vrSalesCompNmE1').val(data.CUST.CUST_NM);
                    }
                    else{
                        let popParam = {};
                        popParam[type == vm.#CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
                        popParam['LC_ID'] = jQuery("select[name='SVC_INFO']").val();
                        popParam['TRANS_CUST_ID'] = vm.#custId;
            
                        fn_Popup("CUST_SHOP", popParam);
                    }
                }
                else{
                    jQuery('#vrSalesCompCdE1').val("");
                    jQuery('#vrSalesCompIdE1').val("");
                    jQuery('#vrSalesCompNmE1').val("");
                }
            }
        });

    }


    /**
     * 거래처선택 팝업 후처리 함수
     * 
     * @param {*} arr 
     */
    SelectedCustShopCallBack(arr){
        var info = arr[0];	
        jQuery('#vrSalesCompCdE1').val(info.cust_cd);
        jQuery('#vrSalesCompIdE1').val(info.cust_id);
        jQuery("#vrSalesCompNmE1").val(info.cust_nm);
    }


    /**
     * 주문수집 클릭 이벤트 콜백함수
     * 
     * @param {*} e 
     */
    OrdCollectClickEventCallBack(e){

        if(cfn_isEmpty(jQuery("#vrSrchSendCompanyIdE1").val())){
            alert(`오픈몰계정을 선택해야 합니다.`);
            return;
        }

        jQuery('#popOrdCollect').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });

        CustomPopUp.OrderCollect.Init({
            'OPENER'               : this,
            'CUST_ID'              : jQuery("#vrSrchCustIdE1").val(),
            'MASTER_SEQ'           : jQuery("#vrSrchSendCompanyIdE1").val(),
            'SALES_COMPANY'        : this.#salesCompList,
            'SELECTED_SALES_COM'   : jQuery("#vrSalesCompCdE1").val()
        });
    }


    /**
     * 주문등록 클릭 이벤트 콜백함수
     * 
     * @param {*} e 
     */
    OrdRegistClickEventCallBack(e){

        let selectedData = this.#grid.GetSelectedRowData(1);

        let jobs = new Array();
        let dataSet = new Array();
        const JOB_MAX_COUNT = 4000;     // 4000건 단위 전송

        try{

            /** 작업 분할 */
            for(let i=0; i < Math.ceil(selectedData.length / JOB_MAX_COUNT); i++){
                jobs.push(selectedData.slice( (i * JOB_MAX_COUNT), ((i+1) * JOB_MAX_COUNT) ));
            }
    
            /** 작업 데이터 생성 */
            for(let job of jobs){
                dataSet.push(CreateFormData.call(this, job));
            }

            jQuery('#popRegOrd').bPopup({
                zIndex: 999,
                escClose: 0,
                opacity: 0.6,
                positionStyle: 'fixed',
                transition: 'slideDown',
                speed: 400
            });
    
            CustomPopUp.OrderRegister.Init({
                'OPENER' : this,
                'CUST_ID' : jQuery("#vrSrchCustIdE1").val(),
                'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE1").val()
            });

            CustomPopUp.OrderRegister.SetData(dataSet);

        }catch(e){
            alert(e);
        }


        /**
         * FormData 생성 (Grid 데이터)
         * 내장함수
         * 
         * @param {*} data 
         * @returns 
         */
        function CreateFormData(data) {

            let formData = new FormData(document.getElementById(`${this.#formId}`));
            let invalidInfos = new Array();
            let invalidWarnings = new Array();
            let validColumns = ['SYNC_YN', 'SEQ', 'PRODUCT_ID', 'DET_QTY', 'HOLD', 'DET_ORDER_CS'];
            let formCnt = 0;

            formData.append('ROW_COUNT', data.length);
            formData.append(`I_ORD_TYPE`,       '02');
            formData.append(`I_ORD_SUBTYPE`,    '30');

            for(const rowData of data){

                const rowIdx = rowData['ROW_IDX'];

                /** Validation (유효성 검사) */
                for (const colKey of validColumns){

                    let validInfo = this.ValidationForOrdSync(colKey, rowData[colKey]);

                    switch(validInfo.VALID){
                        case VALID_STATUS.ERROR : 
                            invalidInfos.push({
                                'NAME' : this.#grid.GetColumnNameByCode(colKey),
                                'KEY'  : colKey,
                                'MSG'  : validInfo.MSG
                            });

                            break;

                        case VALID_STATUS.WARNING :
                            invalidWarnings.push({
                                'NAME' : this.#grid.GetColumnNameByCode(colKey),
                                'KEY'  : colKey,
                                'MSG'  : validInfo.MSG,
                                'ROW'  : rowIdx
                            });

                            break;
                    }

                    // TODO :: thorw 양식 정의 필요
                    if(invalidInfos.length > 0){
                    
                        let msg = '';
    
                        for(let validInfo of invalidInfos){
                            msg += `${rowIdx}행의 [${validInfo['NAME']}] : ${validInfo['MSG']}` + '\n';
                        }
    
                        throw(msg);
                    }
                    if(invalidWarnings.length > 0){
                    
                        let msg = '';
    
                        for(let validInfo of invalidInfos){
                            msg += `${rowIdx}행의 [${validInfo['NAME']}] : ${validInfo['MSG']}` + '\n';
                        }
    
                        throw(msg);
                    }
                }

                const ordDate = new Date(rowData['ORDER_DATE']);
                let recvInfoByPack = new Array();
    
                // 합포기준 대표 주문정보 추출
                recvInfoByPack = this.#grid.DataSource.filter(dt => {return dt.SEQ ==  rowData['TARGET_SEQ'] });
                if(recvInfoByPack.length == 0){
                    recvInfoByPack = this.#grid.DataSource.filter(dt => {return dt.PACK == rowData['TARGET_SEQ'] });
                }    
    
                // let param = {
                //     PACK                :  rowData['PACK']
                //     , SEQ               :  rowData['SEQ']
                //     , ORDER_ID          :  rowData['ORDER_ID']
                    
                //     , RECV_NAME         :  recvInfoByPack[0]['RECV_NAME']
                //     , RECV_MOBILE       :  recvInfoByPack[0]['RECV_MOBILE']
                //     , RECV_TEL          :  recvInfoByPack[0]['RECV_TEL']
                //     , RECV_ADDRESS      :  recvInfoByPack[0]['RECV_ADDRESS']
                //     , RECV_ZIP          :  recvInfoByPack[0]['RECV_ZIP']
    
                //     , MEMO              :  rowData['MEMO']
                //     , ORDER_CS          :  rowData['ORDER_CS']
                //     , ORDER_DATE        :  ordDate.getFormattedString()
                //     , TRANS_DATE_POS    :  rowData['TRANS_DATE_POS']
                //     , SHOPSTAT_DATE     :  rowData['SHOPSTAT_DATE']
                //     , PRODUCT_ID        :  rowData['PRODUCT_ID']
                //     , ITEM_NAME         :  rowData['ITEM_NAME']
                //     , DET_QTY           :  rowData['DET_QTY']
                //     , SHOP_PRICE        :  rowData['SHOP_PRICE']
                //     , PRD_SEQ           :  rowData['PRD_SEQ']
                //     , MAS_SEQ           :  rowData['MAS_SEQ']
                //     , DET_SEQ           :  rowData['DET_SEQ']
                // }
    
                formData.append(`I_PACK`            + `_${formCnt}`, rowData['PACK']                      );
                formData.append(`I_SEQ`             + `_${formCnt}`, rowData['SEQ']                       );
                formData.append(`I_ORDER_ID`        + `_${formCnt}`, rowData['ORDER_ID']                  );
                formData.append(`I_RECV_NAME`       + `_${formCnt}`, recvInfoByPack[0]['RECV_NAME']       );
                formData.append(`I_RECV_MOBILE`     + `_${formCnt}`, (recvInfoByPack[0]['RECV_MOBILE']).toString().removeRegExpChar(')-')     );
                formData.append(`I_RECV_TEL`        + `_${formCnt}`, (recvInfoByPack[0]['RECV_TEL']).toString().removeRegExpChar(')-')        );
                formData.append(`I_RECV_ADDRESS`    + `_${formCnt}`, recvInfoByPack[0]['RECV_ADDRESS']    );
                formData.append(`I_RECV_ZIP`        + `_${formCnt}`, recvInfoByPack[0]['RECV_ZIP']        );
                formData.append(`I_MEMO`            + `_${formCnt}`, rowData['MEMO']                      );
                formData.append(`I_ORDER_CS`        + `_${formCnt}`, rowData['ORDER_CS']                  );
                formData.append(`I_ORDER_DATE`      + `_${formCnt}`, ordDate.getFormattedString()         );
                formData.append(`I_TRANS_DATE_POS`  + `_${formCnt}`, rowData['TRANS_DATE_POS']            );
                formData.append(`I_SHOPSTAT_DATE`   + `_${formCnt}`, rowData['SHOPSTAT_DATE']             );
                formData.append(`I_PRODUCT_ID`      + `_${formCnt}`, rowData['PRODUCT_ID']                );
                formData.append(`I_ITEM_NAME`       + `_${formCnt}`, rowData['ITEM_NAME']                 );
                formData.append(`I_DET_QTY`         + `_${formCnt}`, rowData['DET_QTY']                   );
                formData.append(`I_SHOP_PRICE`      + `_${formCnt}`, rowData['SHOP_PRICE']                );
                formData.append(`I_PRD_SEQ`         + `_${formCnt}`, rowData['PRD_SEQ']                   );
                formData.append(`I_MAS_SEQ`         + `_${formCnt}`, rowData['MAS_SEQ']                   );
                formData.append(`I_DET_SEQ`         + `_${formCnt}`, rowData['DET_SEQ']                   );

                formCnt++;

            }

            return formData;
        }

    }


    /**
     * 택배송장등록 클릭 이벤트 콜백함수
     * 
     * 
     * @param {*} e 
     */
    DlvSendClickEventCallBack(e){

        try {

            let vm = this;
            let formData = new FormData(document.getElementById(`${this.#formId}`));
            let selectedIdx = this.#grid.GetCheckedList(1);
            let sendData = new Array();

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 주문을 선택해야 합니다.`;
            }


            if(confirm(`선택된 주문의 송장번호를 전송합니다. (${selectedIdx.length} 건)(Winus -> EzAdmin)`)){

                formData.append('ROW_COUNT', selectedIdx.length);

                /* [Loop] Rows */
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let rowIdx = selectedIdx[i];
                    let validInfos = new Array();
                    let valid = true;

                    /* [Loop] Columns */
                    for(let colIdx=0; colIdx < this.#grid.GetColumnCount(); colIdx++)
                    {
                        let colKey = this.#grid.GetColumnCodeByIdx(colIdx);
                        let value = cfn_trim(this.#grid.getValue(rowIdx, colIdx));

                        /* 유효성 검사 */
                        let validData = this.ValidationForDlvSend(rowIdx, colKey, value);

                        if(!validData.VALID){
                            valid = false;
                            validInfos.push({
                                'NAME' : this.#grid.GetColumnNameByCode(colKey),
                                'KEY'  : colKey,
                                'MSG'  : validData.MSG
                            });
                        }
                    }

                    // 유효성 검사 결과 출력
                    if(!valid){
                        let msg = '';

                        for(let validInfo of validInfos){
                            msg += `${rowIdx+1}행의 [${validInfo['NAME']}] : ${validInfo['MSG']}` + '\n';
                        }

                        throw(msg);
                    }

                    let isWinusInvc     = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('WMS_INVC_YN')) == "Y";
                    let transCorpValue  =  isWinusInvc 
                                                ? this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PARCEL_COM_TY'))
                                                : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('TRANS_CORP'));

                    let packSeq = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PACK'));
                    let seq = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SEQ'));

                    // Grid Data Set
                    sendData.push({
                        SEQ                 :  packSeq == 0 ? seq : packSeq                                             // 관리번호
                        , TRANS_CORP        :  this.GetTransCorpCode(isWinusInvc, transCorpValue)                       // 택배사코드 (Winus or Ez)
                        , TRANS_NO          :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MERGE_INVC_NO'))    // 송장번호
                        , WMS_INVC_YN       :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('WMS_INVC_YN'))      // WINUS 송장 (Y/N)
                    });
                    
                }

                // 중복제거
                const map = new Map();
                for(const character of sendData){
                    map.set(JSON.stringify(character), character);
                }
                let arrUnique = [...map.values()];


                console.log(arrUnique);
                
                
                // 택배송장등록 팝업 호출
                jQuery('#popDlvSend').bPopup({
                    zIndex: 999,
                    escClose: 0,
                    // modalClose: true,
                    opacity: 0.6,
                    positionStyle: 'fixed', //'fixed' or 'absolute'
                    transition: 'slideDown',
                    speed: 400
                });

                // 팝업 기준정보 전달
                CustomPopUp.DlvSend.Init({
                    'OPENER' : this,
                    'CUST_ID' : jQuery("#vrSrchCustIdE1").val(),
                    'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE1").val()
                });

                // 팝업 수행데이터 셋팅
                CustomPopUp.DlvSend.SetData(arrUnique);

            }
            else{
                return ;
            }

            
        } catch (error) {
            alert(error);
        }

    }


    DeleteClickEventCallBack(e){

        const vm = this;
        let formData = new FormData(document.getElementById(this.#formId));
        let gridData = this.grid.GetSelectedRowData(1);

        if(confirm(DICTIONARY['delete.confirm'])){
            let validData = this.ValidationForDelete(gridData);

            if(!validData.VALID){
                alert(validData.MESSAGE);
                return;
            }
    
            gridData = gridData.map((dt) => { return { 'MAS_SEQ': dt.MAS_SEQ,  'DET_SEQ': dt.DET_SEQ } } );
    
            formData.append('gridData', JSON.stringify(gridData));
    
            jQuery.ajax({
                url : "/WMSIF707/deleteE1.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                    vm.SearchClickEventCallback(e);
                },
                success : function(data){
    
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(`관리자에게 문의바랍니다. (${data["MSG"]})`);
                    }
                    else{
                        alert(`${data["MSG"]}`);
                    }
                }
            });
        }

    }


    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('주문정보', true);
        // this.#grid.ExcelDownLoad('주문정보', false);
    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     * @param {boolean} isSrchOrdDegree : 주문예정차수 조건 검색 여부
     */
    SearchClickEventCallback(e, isProgressBar=true, isSrchOrdDegree=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let isNewDegree = jQuery("#vrSrchOrdDegree option:selected").nextAll().size() == 0; // 신규차수인지 판별

        // TODO :: 하드코딩 개선 필요
        let csStateFileter = new Array();
        let holdStateFileter = new Array();

        // 필터란 CS상태 체크리스트 조회 
        for(const state of this.#csState){

            let value = formData.get(`csState_${state}`);

            if(value != null){
                csStateFileter.push(value);
            }
        }

        // 필터란 보류상태 체크리스트 조회 
        for(const state of this.#holdState){

            let value = formData.get(`holdState_${state}`);

            if(value != null){
                holdStateFileter.push(value);
            }
        }
        
        formData.append('vrSrchCSState'     , csStateFileter.length == 0 ? 'A' : csStateFileter);       // CS상태
        formData.append('vrSrchHoldState'   , csStateFileter.length == 0 ? 'A' : holdStateFileter);     // 보류상태
        formData.append('vrsrchOrdDegree'   , (isSrchOrdDegree && !isNewDegree) ? 'Y': 'N');            // 신규차수 이거나, 주문예정차수 조건으로 검색요청한 경우 (true, false)

        return jQuery.ajax({
            url : "/WMSIF707/list.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    let packCnt = data.rows.map(dt=> (dt.PACK == 0) ? dt.SEQ : dt.PACK).filter((value, index, self)=>{return self.indexOf(value) === index;}).length;

                    jQuery("#totalDlvCnt").text(packCnt);                                               // 합포단위수량
                    jQuery("#totalOrdCnt").text(vm.#grid.GetRowCountByDistinctKey('SEQ'));              // 관리번호
                    jQuery("#totalProcCnt").text(vm.#grid.GetRowCountByDistinctKey('PRD_SEQ'));         // 상품관리번호
                }

            }
        });

    }


    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '주문정보관리');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


//#endregion


//#region   :: Validation


    /**
     * Winus 주문동기화를 위한 유효성검사
     * 
     * 
     * @param {String} colKey   : Grid 컬럼 키
     * @param {*} value         : Grid 컬럼 값
     * @returns 
     */
    ValidationForOrdSync(colKey, value){

        let valid   = VALID_STATUS.VALID;
        let msg     = null;

        switch(colKey){

            case 'SYNC_YN' :
                if(value == 'Y'){
                    valid = VALID_STATUS.ERROR;
                    msg = '이미 등록된 주문입니다.'
                }
                break;

            case 'SEQ' : 
                if(cfn_isEmpty(value)){
                    valid = VALID_STATUS.ERROR;
                    msg = `관리번호는 공백일 수 없습니다.`
                }
                break;

            case 'PRODUCT_ID':
                if(cfn_isEmpty(value)){
                    valid = VALID_STATUS.ERROR;
                    msg = `상품정보는 공백일 수 없습니다.`
                }
                break;

            case 'DET_QTY':
                if(cfn_isEmpty(value) || Number(value) == 0){
                    valid = VALID_STATUS.ERROR;
                    msg = `주문 수량은 1 이상이어야 합니다. (${value})`;
                }
                break;

            case 'HOLD' : 
                // 보류상태 : 미설정(0) 제외 주문등록 제한
                if(value != '미설정'){
                    valid = VALID_STATUS.ERROR;
                    msg = `보류상태(${value}) 인 주문 건이 있습니다.`
                }
                break;

            case 'DET_ORDER_CS':
                // const cancleStates = [1,2,3,4];     // 이지어드민 CS상태 (취소)의 모든 종류

                // 텍스트 구분 제거 필요
                if(value.include('취소')){
                    valid = VALID_STATUS.WARNING;
                    msg = value;
                }

                break;
        }


        return {
            'MSG' : msg,
            'VALID' : valid
        }

    }


    /**
     * 택배송장등록를 위한 유효성검사
     * 
     * 
     * @param {String} colKey   : Grid 컬럼 키
     * @param {*} value         : Grid 컬럼 값
     * @returns 
     */
    ValidationForDlvSend(rowIdx, colKey, value){

        let valid       = true;
        let msg         = null;
        // let isWinusInvc = null;

        switch(colKey){

            case 'SYNC_YN' :
                if(value == 'N'){
                    valid = false;
                    msg = 'Winus에 등록되지 않은 주문입니다. (주문등록)'
                }

                break;

            case 'MERGE_TRANS_CD' : 
                if(cfn_isEmpty(value)){
                    valid = false;
                    msg = `택배송장이 발급되지 않은 주문 (공백).`;
                }

                break;


            case 'MERGE_INVC_NO' :
                if(cfn_isEmpty(value)){
                    valid = false;
                    msg = `택배송장이 발급되지 않은 주문 (공백).`;
                }

                break;


            case 'PARCEL_COM_TY':
                // Winus 등록정보 확인
                if(!cfn_isEmpty(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MERGE_INVC_NO'))) 
                        && !cfn_isEmpty(value)
                        && cfn_isEmpty(this.GetTransCorpCode(true, value)))
                {
                    valid = false;
                    msg = `등록되지 않은 택배사입니다. (code:${value}))`;
                };

                break;


            case 'TRANS_CORP':
                // 이지어드민 등록정보 확인
                if(!cfn_isEmpty(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MERGE_INVC_NO'))) 
                        && !cfn_isEmpty(value)
                        && cfn_isEmpty(this.GetTransCorpCode(false, value)))
                {
                    valid = false;
                    msg = `등록되지 않은 택배사입니다. (code:${value}))`;
                };

                break;


            // case 'TRANS_NO' : 
            //     if(cfn_isEmpty(value)){
            //         valid = false;
            //         msg = `택배송장이 발급되지 않은 주문 (공백).`;
            //     }

            //     break;


            // case 'TRANS_CORP' :
            //     if(cfn_isEmpty(value)){
            //         valid = false;
            //         msg = `공백일 수 없음.`;
            //     }
        }


        return {
            'MSG' : msg,
            'VALID' : valid
        }

    }


    ValidationForDelete(gridData){

        let valid = true;
        let validSeq = '00000';
        let invalidMessage 	= null;	
        let invalidData 	= new Map();
        let rtnMessage 		= '';

        const INVALID_SEQ = {
            'SYNC' : '00001'
        }

        for(const rowData of gridData){

            const syncYn = rowData["SYNC_YN"];
            let targetData = new Object();

            targetData["ROW_IDX"] = rowData["ROW_IDX"];
            targetData["SYNC_YN"] = syncYn;

            if(syncYn == "Y") { validSeq = INVALID_SEQ.SYNC; }

            switch(validSeq){

                case INVALID_SEQ.SYNC : 
                    valid = false;
                    invalidMessage = `Winus 등록된 주문이 존재합니다.\nWinus 주문삭제 후에 삭제 가능합니다.`;

                    break;

                default : 
                    valid = true;
            }

            if(!valid){
                if(invalidData.has(validSeq)){
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: [...invalidData.get(validSeq).DATA, ...[targetData]] });
                }
                else{
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: new Array(targetData) });
                }
            }

        }

        // Print
        if(invalidData.size > 0){
        
            invalidData.forEach((value, key, map)=>{

                rtnMessage += `[${key}] ${value.MESSAGE}\n`;

                for(const rowData of value['DATA']){
                    rtnMessage += `${(rowData.ROW_IDX + 1)} 행\n`;
                }

                rtnMessage += `\n`;
            });
        }

        return {
            'VALID' 	: (invalidData.size == 0), 
            'DATA'		: invalidData,
            'MESSAGE' 	: rtnMessage
        }

    }


//#endregion


//#region   :: Utility


    #CodeType = {
        'WINUS' : 'WINUS',
        'EZADM' : 'EZADM'
    }


    /**
     * 주문 건에 할당된 송장 타입에 따라 택배사 코드값을 가져온다.
     * 
     * @param {boolean} isWinusInvc : true (Winsu 컬럼탐색) , false (Ezadmin 컬럼탐색)
     * @param {*} transCorp         : 택배사코드값
     * @param {*} rtnType           : 반환타입 (Winus 코드기준 or EzAdmin 코드기준) / #CodeType
     * @returns 
     */
    GetTransCorpCode(isWinusInvc, transCorpCd, rtnType = this.#CodeType.EZADM){

        let rtn = null;
        let codeKey = isWinusInvc ? 'WMS_DLV_CD' : 'EZ_DLV_CD';     // 조회정보 Object Key 값
        let nameKey = isWinusInvc ? 'WMS_DLV_NM' : 'EZ_DLV_NM';     // 조회정보 Object Key 값

        let info = this.#transCorpInfo.filter((dt) => { return dt[codeKey] == transCorpCd });

        if(info.length > 0 ){
            rtn = (rtnType == this.#CodeType.WINUS) ? info[0]['WMS_DLV_CD'] : info[0]['EZ_DLV_CD'];
        }


        return rtn;
    }


//#endregion

}


/**
 * 유효성 검사 결과 구분
 * 
 */
const VALID_STATUS = {
    'WARNING'   : 0,
    'ERROR'     : 1,
    'INFO'      : 2,
    'VALID'     : 3
}
