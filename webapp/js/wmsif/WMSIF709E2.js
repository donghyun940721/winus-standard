import * as CustomPopUp             from './WMSIF709E2pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';


export class WMSIF709E2{

    #custId         = null;
    #formId         = 'frm_listE2';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE2',
        id          : 'vrSrchCustIdE2',
        name        : 'vrSrchCustNmE2',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE2',
        id          : 'vrSrchRitemIdE2',
        name        : 'vrSrchRitemNmE2',
        category    : COMP_CATEGORY.ITEM
    });

    #compVender = new Component({
        code        : 'vrSrchVenderCdE2',
        id          : 'vrSrchVenderIdE2',
        name        : 'vrSrchVenderNmE2',
        category    : COMP_CATEGORY.STORE
    });

    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        { name: '매출처'             , code: 'VENDER_INFO'       , startColumnKey: 'VENDER_CODE'        , endColumnKey: 'VENDER_NAME'       },
        { name: '실납처'             , code: 'REAL_VEN_INFO'     , startColumnKey: 'REAL_VEN_CD'        , endColumnKey: 'REAL_VEN_NM'       },
        { name: '명세서 출력'        , code: 'PRINT_INFO'        , startColumnKey: 'PRINT_COUNT'        , endColumnKey: 'PRINT_TIME'        },
        { name: '배송정보'           , code: 'DLV_INFO'          , startColumnKey: 'DELIVERY_WAY_NM'    , endColumnKey: 'DELIVERY_NOTICE'   },
        { name: '제품정보'           , code: 'PRODUCT_INFO'      , startColumnKey: 'ITEM_SORT'          , endColumnKey: 'MANUF_DATE'        },
        { name: '일련번호 정보'      , code: 'SERIAL_INFO'       , startColumnKey: 'PK_SERIAL'          , endColumnKey: 'SERAIL_GROUP'      },
        { name: 'ERP 작업이력'       , code: 'ERP_WORK_HIS'      , startColumnKey: 'WRITE_DATE'         , endColumnKey: 'EDITOR_NM'         },
        { name: 'WMS 작업이력'       , code: 'WMS_WORK_HIS'      , startColumnKey: 'REG_DT'             , endColumnKey: 'UPD_NO'            }
    ];

    #grid = new WinusGrid({ 
        spdListDiv          : 'spdListE2',
        gridTemplate        : 'WMSIF709E2',
        headerHeight        : '27',
        rowHeight           : '22',
        setColor            : false,                // Cell 단위 설정 배경색 적용
        useFilter           : true,                 // 필터
        frozenColIdx        : '5',
        checkFirstIdx       : 0,
        multiCheckRange     : [0, 1],               // [Select Column]
        rowSpanColIdx       : 5,                   // 행 병합
        rowSpanColArr       : [0,4,5,6],
        headerMappingInfo   : this.#headerMappingInfo
    });

    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();
        this.AddEvent();

        this.#grid.Init();
        this.#grid.SetGrid();
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE2").datepicker("setDate", date);                       // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE2").datepicker("setDate", date);                         // 수집등록일자 (to)
//        jQuery("#vrSrchOrdDegreeDateE2").datepicker("setDate", date);                   // 주문예정차수 (날짜)
        jQuery("#vrSrchTypeDtFromE2").datepicker("setDate", date.addDate(0, -1));       // 날짜타입별 일자 (from) : 한 달 단위 조회
        jQuery("#vrSrchTypeDtToE2").datepicker("setDate", date);                        // 날짜타입별 일자 (to)

    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE2").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE2").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE2").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE2").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE2").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE2").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE2").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 거래처정보 (매출처)
        jQuery("#vrSrchVenderCdE2").off().on("change", (e)=>{
            this.VenderChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcVenderImgE2").off().on("click", (e)=>{
            let param = {
                'LC_ID'             : jQuery("select[name='SVC_INFO']").val(),
                'TRANS_CUST_ID'     : this.#custId,
                'CUST_TYPE'         : '2'
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.STORE, param);
        });

        jQuery("#vrSrchVenderNmE2").off().on("change", (e)=>{
            this.VenderChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // *출고주문수집 버튼 이벤트
        jQuery("#btnCollectOrdE2").off().on("click", (e)=>{

            if(!cfn_isEmpty(this.#custId)){
                this.OrderCollectClickEventCallBack(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // * "주문등록" 버튼 클릭 이벤트
        jQuery("#btnOrdRegistE2").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.OrdRegistClickEventCallBack();
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }

        });


        // * "삭제" 버튼 클릭 이벤트
        jQuery("#btnDeleteE2").off().on("click", (e)=>{
            this.DeleteClickEventCallBack(e);
        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE2").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SearchClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }

        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE2").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
}


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }
        
        /* 오픈몰 정보 설정*/
        this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE2", this.#formId, this.#custId);

        /* 주문예정차수 조회 */
//        this.GetCustOrdDegree();

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


    /**
     * 주문예정차수 데이터 조회
     * 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     * @param {string} componentId      : 조회값을 바인딩할 컴포넌트 ID
     * @param {string} ordDt            : 출고예정일
     */
    GetCustOrdDegree(isProgressBar=true, componentId="vrSrchOrdDegreeE2", ordDt){
        
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.append('vrCustId', jQuery("#vrSrchCustIdE2").val());
        formData.append('vrOrdDt', ordDt ?? jQuery("#vrSrchOrdDegreeDateE2").val());

        return jQuery.ajax({
            url : "/WMSOP642/getCustOrdDegree.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Error ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                    throw data["MSG"];
                }
                // Success ...
                else{

                    if('DS_ORD_DEGREE' in data){
                        vm.SetCustOrdDegree(componentId, data.DS_ORD_DEGREE);
                    }
                }
            }
        });
    }


    /**
     * 주문예정차수 셋팅
     * 
     * @param {*} id        : 컴포넌트 ID
     * @param {Array} data  : 바인딩 Data (/WMSOP642/getCustOrdDegree.action)
     */
    SetCustOrdDegree(id, data){

        let cnt = data.size();

        // Init
        jQuery(`#${id} option`).remove();
        
        if(cnt > 0){
            for (var i = 0; i < cnt+1; i++){
                if(i == cnt){
                    jQuery(`#${id}`).append("<option value='"+(Number(data[i-1].CODE)+1)+"'>"+(Number(data[i-1].CODE)+1)+" (신규예정)</option>");
                    jQuery(`#${id}`).val(Number(data[i-1].CODE)+1);
                }else{
                    jQuery(`#${id}`).append("<option value='"+data[i].CODE+"'>"+data[i].CODE+"</option>");
                }
            }
        }else{
            jQuery(`#${id}`).append("<option value='1'>1 (신규예정)</option>");
        }
    }


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE2", this.#formId, this.#custId);

//                this.GetCustOrdDegree();

                break;

            //* 상품
            case COMP_CATEGORY.ITEM : 
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);

                break;

            
            //* 거래처
            case COMP_CATEGORY.STORE :
                vm.#compVender.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                break;

        }

    }

    
    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async VenderChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compVender.Init();
            return;
        }

        await this.#compVender.Search(type, refValue, {'vrOrdType' : '2'})
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
                        popParam['TRANS_CUST_ID'] = vm.#custId;
                        popParam['CUST_TYPE'] = '1';

                        StandardPopUp.call(vm, COMP_CATEGORY.STORE, popParam);
                    }
                })
                .catch((error)=>{
                    // alert(error);
                });
    }


    /**
     * 주문수집 버튼 클릭 이벤트
     * 
     * @param {*} e 
     */
    OrderCollectClickEventCallBack(e){

        jQuery('#popOutOrderCollect').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });
        
        CustomPopUp.OutOrderCollect.Init({
            'OPENER' : this,
            'CUST_ID' : this.#custId,
            'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE2").val()
        });
    }


    /**
     * 매출 주문등록 버튼 클릭 이벤트
     * 
     */
    OrdRegistClickEventCallBack(){

        try{

            let formCnt         = 0;
            let formData        = new FormData(document.getElementById(`${this.#formId}`));

            const selectedData  = this.#grid.GetSelectedRowData(1);
            
            formData.append('ROW_COUNT', selectedData.length);

            const validInfo = this.ValidForSaveOrder(selectedData);
            const NullishDate = new Date('9000-01-01');
            const IsNullishData = (dt) => !cfn_isEmpty(dt) && ((new Date(dt.replace(/^(\d{4})(\d{2})(\d{2})$/, `$1-$2-$3`))) > NullishDate);

            if(!validInfo.VALID){
                alert(validInfo.MESSAGE);
                return;
            }

            for(let rowData of selectedData){

                const rowIdx = rowData['ROW_IDX'];

                formData.append(`I_CUST_ORD_NO`       + `_${formCnt}`, rowData['ORG_ORD_ID']                              );
                formData.append(`I_CUST_ORD_SEQ`      + `_${formCnt}`, rowData['ORG_ORD_SEQ']                             );
                formData.append(`I_TRANS_CUST_CD`     + `_${formCnt}`, rowData['VENDER_CODE']                             );
                formData.append(`I_CUST_CD`           + `_${formCnt}`, this.#compCust.GetValue(COMP_VALUE_TYPE.CODE)      );
                formData.append(`I_ORD_QTY`           + `_${formCnt}`, rowData['PROD_QTY']                                );
                formData.append(`I_UOM_CD`            + `_${formCnt}`, 'EA'                                               );
                formData.append(`I_CUST_LOT_NO`       + `_${formCnt}`, rowData['PRODUCT_NO']                              ); // Lot No
                formData.append(`I_UNIT_NO`           + `_${formCnt}`, rowData['SERIAL_NO']                               ); // Serial No
                formData.append(`I_ITEM_CD`           + `_${formCnt}`, rowData['PROD_CD']                                 );
                formData.append(`I_ITEM_NM`           + `_${formCnt}`, rowData['PROD_NM']                                 );
                formData.append(`I_REMARK`            + `_${formCnt}`, ''                                                 );
                formData.append(`I_ETC2`              + `_${formCnt}`, ''                                                 );
                formData.append(`I_UNIT_AMT`          + `_${formCnt}`, rowData['UNIT_PRICE']                              );
                formData.append(`I_EXPIRY_DATE`       + `_${formCnt}`, IsNullishData(rowData['EFFECT_DATE']) ? '' : rowData['EFFECT_DATE']); // 유효기간
                formData.append(`I_SALES_CUST_NM`     + `_${formCnt}`, rowData['DELIVERY_NAME']                           );
                formData.append(`I_ZIP`               + `_${formCnt}`, rowData['DELIVERY_ZIP']                            );
                formData.append(`I_ADDR`              + `_${formCnt}`, rowData['DELIVERY_ADDRESS']                        );
                formData.append(`I_PHONE_1`           + `_${formCnt}`, rowData['DELIVERY_HANDPHONE']                      );
                formData.append(`I_ETC1`              + `_${formCnt}`, ''                                                 );
                formData.append(`I_PHONE_2`           + `_${formCnt}`, rowData['DELIVERY_TEL']                            );
                formData.append(`I_ORD_DESC`          + `_${formCnt}`, ''                                                 );
                formData.append(`I_DLV_MSG1`          + `_${formCnt}`, rowData['DELIVERY_NOTICE']                         );
                formData.append(`I_WORK_TYPE_CD`      + `_${formCnt}`, rowData['WORK_TYPE_CD']                            );

                formCnt++;
            }


            jQuery('#popOutOrderRegist').bPopup({
                zIndex: 999,
                escClose: 0,
                opacity: 0.6,
                positionStyle: 'fixed',
                transition: 'slideDown',
                speed: 400
            });
    
            CustomPopUp.OutOrderRegist.Init({
                'OPENER' : this,
                'CUST_ID' : this.#custId,
                'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE2").val()
            });

            CustomPopUp.OutOrderRegist.SetData(formData);


        }catch(e) {
            alert(e);
        }

    }


    DeleteClickEventCallBack(e){

        try{

            const vm = this;
            let formData  = new FormData(document.getElementById(`${this.#formId}`));
            let gridData  = this.#grid.GetSelectedRowData(1);

            formData.append('ROW_COUNT', gridData.length);
            formData.append('ORD_TYPE', '1');

            if(confirm(DICTIONARY['delete.confirm'])){

                const validInfo = this.ValidForDelete(gridData);

                if(!validInfo.VALID){
                    alert(validInfo.MESSAGE);
                    return;
                }

                gridData = gridData.map((dt) => { return { 'MAS_SEQ': dt.MAS_SEQ,  'DET_SEQ': dt.DET_SEQ } } );

                formData.append('gridData', JSON.stringify(gridData));

                jQuery.ajax({
                    url : "/WMSIF709/deleteE1.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback(e);
                    },
                    success : function(data){
        
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(`관리자에게 문의바랍니다. (${data["MSG"]})`);
                        }
                        else{
                            alert(`${data["MSG"]}`);
                        }
                    }
                });

            }

        } catch(e) {
            alert(e);
        }

    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     * @param {boolean} isSrchOrdDegree : 주문예정차수 조건 검색 여부
     */
    SearchClickEventCallback(e, isProgressBar=true, isSrchOrdDegree=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let isNewDegree = jQuery("#vrSrchOrdDegree option:selected").nextAll().size() == 0; // 신규차수인지 판별

        formData.append('vrsrchOrdDegree'   , (isSrchOrdDegree && !isNewDegree) ? 'Y': 'N');    // 신규차수 이거나, 주문예정차수 조건으로 검색요청한 경우 (true, false)
        formData.append('ORD_TYPE'          , '2');                                             // 주문타입 (1: 입고, 2: 출고)

        return jQuery.ajax({
            url : "/WMSIF709/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    jQuery("#totalOrdCntE2").text(vm.#grid.GetRowCountByDistinctKey('ORG_ORD_ID'));          // 주문수량
                }

            }
        });
    }


    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad('출고주문정보', true);
    }


//#endregion


//#region   :: Validation


    ValidForSaveOrder = (gridData) => {
        
        let valid = true;
        let invalidMessage 	= null;	
        let invalidData 	= new Map();
        let rtnMessage 		= '';
        let workTypeCd      = '';

        if(gridData.length > 0){
            workTypeCd = gridData[0].WORK_TYPE_CD;
        }

        const INVALID_SEQ       = {
            'NOT_INVALID_FORMAT'           :  '00001',        // 날짜 포맷 오류
            'NOT_INVALID_WORK_TYPE_CD'     :  '00002',        // 매출 구분 오류
        };

        const NOT_INVALID_FORMAT = (dt) => !cfn_isEmpty(dt) && String(new Date(dt.replace(/^(\d{4})(\d{2})(\d{2})$/, `$1-$2-$3`))).includes('Invalid');


        for(const rowData of gridData){

            let columnNm = null;
            let validSeq = null;

            // Get Value
            const targetInfo = {
                'ROW_IDX'       : rowData['ROW_IDX'],
                'MANUF_DATE'    : rowData['MANUF_DATE'],
                'EFFECT_DATE'   : rowData['EFFECT_DATE'],
            }

            // 유효기간
            if(NOT_INVALID_FORMAT(targetInfo.EFFECT_DATE)){ 
                columnNm = this.#grid.GetColumnNameByCode('EFFECT_DATE');  
                validSeq = INVALID_SEQ['NOT_INVALID_FORMAT'];
            }

            // 제조일자
            if(NOT_INVALID_FORMAT(targetInfo.MANUF_DATE)){ 
                columnNm = this.#grid.GetColumnNameByCode('MANUF_DATE');  
                validSeq = INVALID_SEQ['NOT_INVALID_FORMAT'];
            }

            // 매출구분
            if(workTypeCd !=  rowData['WORK_TYPE_CD']){
                columnNm = this.#grid.GetColumnNameByCode('WORK_TYPE_CD');  
                validSeq = INVALID_SEQ['NOT_INVALID_WORK_TYPE_CD'];
            }
            workTypeCd = rowData['WORK_TYPE_CD'];


            // Print Message
            switch(validSeq){

                case INVALID_SEQ.NOT_INVALID_FORMAT :
                    valid = false;
                    invalidMessage = `올바른 날짜형식이 아닙니다. (${columnNm})`;

                    break;

                case INVALID_SEQ.NOT_INVALID_WORK_TYPE_CD :
                        valid = false;
                        invalidMessage = `같은 매출구분이 아닙니다. (${columnNm})`;
    
                        break;

                default : 
                    valid = true;

            }

            if(!valid){
                /** Insert & Update */
                if(invalidData.has(validSeq)){
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: [...invalidData.get(validSeq).DATA, ...[targetInfo]] });
                }
                else{
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: new Array(targetInfo) });
                }
            }

        }
        
        // Gathering Message
        if(invalidData.size > 0){

            invalidData.forEach((value, key, map)=>{

                rtnMessage += `[${key}] ${value.MESSAGE}\n`;

                for(const rowData of value['DATA']){
                    rtnMessage += `${(rowData.ROW_IDX + 1)} 행\n`;
                }

                rtnMessage += `\n`;
            });
        }


        return {
            'VALID' 	: (invalidData.size == 0), 
            'DATA'		: invalidData,
            'MESSAGE' 	: rtnMessage
        }

    }


    ValidForDelete = (gridData) => {
            
        let valid = true;
        let invalidMessage 	= null;	
        let invalidData 	= new Map();
        let rtnMessage 		= '';

        const INVALID_SEQ       = {
            'ALREADY_SYNC'     :  '00001',  // 동기화 여부
        };

        const ALREADY_SYNC = (dt) => dt == 'Y';


        for(const rowData of gridData){

            let columnNm = null;
            let validSeq = null;

            // Get Value
            const targetInfo = {
                'ROW_IDX'       : rowData['ROW_IDX'],
                'SYNC_YN'       : rowData['SYNC_YN']
            }
            
            if(ALREADY_SYNC(targetInfo.SYNC_YN)){ 
                columnNm = this.#grid.GetColumnNameByCode('SYNC_YN');  
                validSeq = INVALID_SEQ.ALREADY_SYNC;
            }

            // Print Message
            switch(validSeq){

                case INVALID_SEQ.ALREADY_SYNC :
                    valid = false;
                    invalidMessage = `Winus 등록된 주문이 존재합니다.\nWinus 주문삭제 후에 삭제 가능합니다. (${columnNm})`;

                    break;

                default : 
                    valid = true;

            }

            if(!valid){
                /** Insert & Update */
                if(invalidData.has(validSeq)){
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: [...invalidData.get(validSeq).DATA, ...[targetInfo]] });
                }
                else{
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: new Array(targetInfo) });
                }
            }
        }
        
        // Gathering Message
        if(invalidData.size > 0){

            invalidData.forEach((value, key, map)=>{

                rtnMessage += `[${key}] ${value.MESSAGE}\n`;

                for(const rowData of value['DATA']){
                    rtnMessage += `${(rowData.ROW_IDX + 1)} 행\n`;
                }

                rtnMessage += `\n`;
            });
        }


        return {
            'VALID' 	: (invalidData.size == 0), 
            'DATA'		: invalidData,
            'MESSAGE' 	: rtnMessage
        }

    }

    
//#endregion


//#region   :: Utility


//#endregion

}