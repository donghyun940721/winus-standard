import { WinusGrid }                from '../utility/WinusGrid.js';

export class WMSIF820E6 {

    // #custId         = '0000020164';
    #custId         = '0000575640';
    #formId         = 'frm_listE6';
    #invcWorkStatus = true;             // 택배송장 작업 상태 (Y: 완료 / N: 미완료)

    #headerMappingInfo = [
        {
            name: 'DAS 전송',
            code: 'DAS_SEND',
            startColumnKey: 'DAS_QTY',
            endColumnKey: 'DAS_BUTTON'
        }
        , {
            name: 'DAS 결과',
            code: 'DAS_RESULT',
            startColumnKey: 'DAS_RESULT_QTY',
            endColumnKey: 'DAS_RESULT_STATE'
        }
        , {
            name: 'WMS 검수',
            code: 'WMS_RESULT',
            startColumnKey: 'CONF_QTY',
            endColumnKey: 'CONF_BUTTON'
        }
        , {
            name: '출고 실적',
            code: 'SEND_GROUP',
            startColumnKey: 'RESULT_QTY',
            endColumnKey: 'SEND_BUTTON'
        }
    ];

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE6',
        gridTemplate        :   'WMSIF820E6',
        headerHeight        :   '25',
        rowHeight           :   '27',
        setColor            :   false,
        frozenColIdx        :   4,
        headerMappingInfo   :   this.#headerMappingInfo,
    });

    #bottomGrid = new WinusGrid({ 
        spdListDiv          :   'spdListE6_bottom',
        gridTemplate        :   'WMSIF820E6_BT',
        headerHeight        :   '25',
        rowHeight           :   '27',
        setColor            :   false,
    });

    #searchInterval;

    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

        this.#bottomGrid.Init();                  // !(필수) 그리드 초기화
        this.#bottomGrid.SetGrid();               // !(필수) 그리드 생성

        /** Grid 생성대기 */
        let settingTimer = setInterval(()=>{

            if (vm.#grid.created) {
                /** Grid 생성 후 처리 내용 기재
                 * 
                 * ex) Column Format Custom, 로딩 시 기본조회 ... 
                 */
                // 횡 스크롤 새로고침
                this.#grid.ShowHorizontalScrollbar(true);
                this.#bottomGrid.ShowHorizontalScrollbar(true);
                clearInterval(settingTimer);
            }
        }, 100);
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchWorkDtE6").datepicker("setDate", date); 

        date.setDate(date.getDate() + 1); // 공급일 (다음날)
        jQuery("#vrSrchReqDtFromE6").datepicker("setDate", date); 
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        const vm = this;

        /** DAS 차수 생성 */
        jQuery("#btn_createE6").off().on("click", (e)=>{
            this.CreateDasDegree();
        });
        
        /** DAS 차수 삭제 */
        jQuery("#btn_deleteE6").off().on("click", (e)=>{
            this.DeleteDasDegree();
        });

        /** 출고실적 일괄생성 */
        jQuery("#btn_createAllOutResultE6").off().on("click", (e)=>{
            this.createAllOutResult();
        });

        /** 출고실적 일괄전송 */
        jQuery("#btn_sendAllOutResultE6").off().on("click", (e)=>{
            this.sendAllOutResult();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE6").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
            this.SearchWorkHistory();
            this.GetSummaryData(e);
        });

        document.getElementById('OPT_01').addEventListener('change', function() {
            const intervalTime = jQuery("#intervaTime").val() * 1000;

            if(intervalTime < 30000) {
                alert('검색 간격은 30초 이상으로 설정해주세요.');
                return ;
            }

            if(this.checked) {
                vm.#searchInterval = setInterval(()=> {
                    vm.SearchClickEventCallback();
                    vm.SearchWorkHistory();
                }, intervalTime);
            } else {
                clearInterval(vm.#searchInterval);
            }
        });

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE6").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });

        /** 그리드 버튼 클릭 이벤트 */
        this.#grid.ButtonClickCustomEvent = function(sender, args){
            let rowIdx = args.row;
            let colIdx = args.col;
            const rowData = vm.#grid.GetRowData(rowIdx);

            switch(colIdx){
                /** DAS전송 */
                case vm.#grid.GetColumnIdx('DAS_BUTTON') : 
                    vm.SendDasData(rowData);
                    break;

                /** 검수확정 */
                case vm.#grid.GetColumnIdx('CONF_BUTTON') : 
                    vm.CreateInspectData(rowData);
                    break;

                /** 실적생성 */
                case vm.#grid.GetColumnIdx('RESULT_BUTTON') :
                    alert('준비 중인 기능입니다.');
                    // vm.CreateResultData(rowData);
                    break;

                /** 실적전송 */
                case vm.#grid.GetColumnIdx('SEND_BUTTON') : 
                    alert('준비 중인 기능입니다.');
                    // vm.SendOutResultData(rowData);
                    break;
            }
        };
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
        this.#bottomGrid.ShowHorizontalScrollbar(true);
    }

//#region   :: Component Set 

//#endregion


//#region   :: CallBack Event

    SearchClickEventCallback (e){
        let vm = this;
        let formData = new FormData(document.getElementById(`${vm.#formId}`));
        
        formData.append('vrCustId'     , vm.#custId);

        return jQuery.ajax({
            url : "/WMSIF820/listE06.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });
    }

    SearchWorkHistory () {
        let vm = this;
        let formData = new FormData(document.getElementById(`${vm.#formId}`));
        
        formData.append('vrCustId'     , vm.#custId);

        return jQuery.ajax({
            url : "/WMSIF820/listBottomE06.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#bottomGrid.SetData(data);

                    const invcWork = data.rows.filter( dt =>  { if (dt.ORG_SERVICE_NM == '\/WMSOP642\/DlvInvcNoOrderTotal.action') return dt; });
                    vm.#invcWorkStatus = invcWork.every( dt => { return (dt.COMPLETE_YN == 'Y') });     // 작업 미완료인 건이 있는 경우 (false)
                }
            }
        });
    }

    createAllOutResult (){
        // const url = "/WMSIF820/createAllOutResult.action";       // [롤백]
        const url = "/WMSIF820/CreateResultData.action";
        const vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let gridCnt = vm.#grid.GetAllRowData();

        for(let i = 0; i < gridCnt.length; i++){
            if(gridCnt[i].DAS_QTY != gridCnt[i].CONF_QTY){
                alert('DAS 결과내역이 생성중이거나 DAS전송 내역 수량과 일치하지 않습니다.');
                return ;
            }
        }

        if(confirm(`출고 실적을 일괄 생성 하시겠습니까?\n(공급일 : ${jQuery("#vrSrchReqDtFromE6").val()})`)){
            formData.append('vrCustId', this.#custId);
            formData.append('vrOrdDt', jQuery("#vrSrchReqDtFromE6").val());
            formData.append('vrWorkDt', jQuery("#vrSrchWorkDtE6").val());

            jQuery.ajax({
                url : url,
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
                    alert(data["MSG"]);
                    vm.SearchClickEventCallback();
                }
            });
        }
    }

    sendAllOutResult (){
        const vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        let gridCnt = vm.#grid.GetAllRowData();

        for(let i = 0; i < gridCnt.length; i++){
            if(gridCnt[i].RESULT_QTY != gridCnt[i].CONF_QTY){
                alert('출고 실적이 생성중이거나 DAS 결과 수량과 일치하지 않습니다.');
                return ;
            }
        }

        if(confirm(`출고 실적을 일괄 전송 하시겠습니까?\n(공급일 : ${jQuery("#vrSrchReqDtFromE6").val()})`)){
            formData.append('vrCustId', this.#custId);
            formData.append('vrOrdDt', jQuery("#vrSrchReqDtFromE6").val());
            formData.append('vrWorkDt', jQuery("#vrSrchWorkDtE6").val());

            jQuery.ajax({
                url : "/WMSIF820/sendAllOutResult.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
                    alert(data["MSG"]);
                    vm.SearchClickEventCallback();
                }
            });
        }
    }

    CreateDasDegree (e){
        jQuery('#popDasCreateViewE3').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });
    
        CreatedDasDegreePop.SetParam({
            'CONTEXT'           : this,
            'CUST_ID' 			: this.#custId,
            'SEARCH_CALLBACK' 	: this.SearchClickEventCallback,
        });
    
        CreatedDasDegreePop.OpenEvent();
    }

    DeleteDasDegree (e){
        jQuery('#popDasDeleteViewE3').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });
    
        DeleteDasDegreePop.SetParam({
            'CONTEXT'           : this,
            'CUST_ID' 			: this.#custId,
            'SEARCH_CALLBACK' 	: this.SearchClickEventCallback,
        });
    
        DeleteDasDegreePop.OpenEvent();
    }

    ExcelDownLoadCallBack(e){        
        this.#grid.ExcelDownLoad('통합작업', true);
    }

    GetSummaryData (e){
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        
        formData.append('vrCustId'     , this.#custId);

        return jQuery.ajax({
            url : "/WMSIF820/listE06Summary.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){

                document.getElementById(`VDLV`).innerHTML = 0;
                document.getElementById(`D190`).innerHTML = 0;
                document.getElementById(`CRD0`).innerHTML = 0;
                document.getElementById(`D120`).innerHTML = 0;

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    for(const summary of data.LIST.list){
                        document.getElementById(`${summary.WAREKY}`).innerHTML = summary.ORD_CNT;
                    }
                }
            }
        });
    }

    /**
     * DAS 전송
     * 
     * @param {*} rowData 
     */
    SendDasData  = async (rowData) =>{
        let task = new Array();
        
        if(rowData.DAS_STATE === 'Y') {
            alert('이미 DAS 전송이 완료된 주문입니다.');
            return ;
        }

        // 택배주문 유효성 검사
        if(rowData.INVC_ORD_CNT > 0){
            if(rowData.INVC_CNT == 0){
                alert(`택배주문이 포함된 설비차수 입니다.\n택배 송장 접수 후 전송 해주세요.\n\n\n메뉴 : [택배관리] → 택배접수관리`);
                return ;
            }
            else if(this.#invcWorkStatus != true){
                alert(`택배 송장 접수 작업이 진행 중입니다.\n잠시 후 다시 시도 해주세요.`);
                return ;
            }
        }

        try{
            const confirmMessage = `DAS 전송하시겠습니까?\n(설비차수: ${rowData.DEV_ORD_DEGREE})`
            let isConfirm = false;

            // WINUS 주문차수 1 : 택배주문
            if(rowData.INVC_ORD_CNT > 0 && rowData.INVC_CNT != rowData.INVC_ORD_CNT){
                isConfirm = confirm(`${confirmMessage}\n\n\n[경고] 송장접수가 되지 않은 택배 주문이 있습니다. 송장접수 상태를 확인해주세요.\n(택배 주문 수: ${rowData.INVC_ORD_CNT})\n(송장 수량: ${rowData.INVC_CNT})`);
            }
            else{
                isConfirm = confirm(`${confirmMessage}`);
            }

            if(!isConfirm){
                return ;
            }

            // 택배주문
            if(rowData.INVC_CNT > 0){
                task.push(this.SendOrderData(rowData));
                task.push(this.SendSupplyData(rowData));
            }
            // 일반주문
            else{
                task.push(this.SendOrderData(rowData));
            }

            await Promise.all(task)
                .then((results)=>{
                    alert(results.join('\n'))
                })
                .catch((error)=> {
                    alert(`관리자 문의 바랍니다.\n${error}`)
                });
        }
        catch(e){
            alert(e);
        }
    }

    /**
     * WMS 검수확정
     * : DAS 결과 기반 CK030 데이터를 생성
     * 
     * @param {*} rowData 
     */
    CreateInspectData (rowData) {
        const vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        if(rowData.DAS_RESULT_STATE !== 'Y') {
            alert('DAS 수신상태 확인 후 검수확정을 진행해주세요.');
            return ;
        }

        if(rowData.INVC_ORD_CNT > 0){
            if(this.#invcWorkStatus != true){
                alert(`택배 송장 접수 작업이 진행 중입니다.\n잠시 후 다시 시도 해주세요.`);
                return ;
            }
        }

        formData.append('CUST_ID', this.#custId);
        formData.append('SEND_DATE', formData.get("vrSrchWorkDtE6"));
        formData.append('ORDER_WAVE', rowData.DEV_ORD_DEGREE);

        if(confirm(`검수 데이터를 생성 합니다.\n(설비차수 : ${rowData.DEV_ORD_DEGREE})`)){

            return jQuery.ajax({
                url : "/WMSIF604/createDasResultData.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(data){

                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(data['MSG']);
                    }
                    // Success ...
                    else{
                        // alert(data["MSG"]);
                        // vm.SearchClickEventCallback();
                    }

                }
            });
        }
    }

    /**
     *ERP 출고 실적 생성
     * 
     * @param {*} rowData 
     */
    CreateResultData (rowData) {

    }

    /**
     * ERP 출고 실적 전송
     * 
     * @param {*} rowData 
     */
    SendOutResultData (rowData) {

        debugger;
        console.log('ERP 전송');
        const isConfirm = confirm(`ERP 실적 전송하시겠습니까?\n(설비차수: ${rowData.DEV_ORD_DEGREE})`);

        if(!isConfirm) return ;

        if(rowData.SEND_STATE === 'Y') {
            alert('이미 ERP 전송이 완료된 주문입니다.');
            return ;
        }

        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.append('OUT_REQ_DT', formData.get('vrSrchReqDtFromE6').replaceAll('-', ''));
        formData.append('ORD_DEGREE', rowData.ORD_DEGREE);
        formData.append('DEV_ORD_DEGREE', rowData.DEV_ORD_DEGREE);

        jQuery.ajax({
            url : "",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){
        
            }
        });
    }

    // CreateInvoiceData () {
    //     const vm = this;
    //     let formData = new FormData(document.getElementById(`${this.#formId}`));

    //     if(confirm(`선택한 공급일의 택배 실적을 생성 하시겠습니까?\n(공급일 : ${jQuery("#vrSrchReqDtFromE6").val()})`)){

    //         formData.append('vrCustId', this.#custId);
    //         formData.append('vrOrdDt', jQuery("#vrSrchReqDtFromE6").val());
            
    //         jQuery.ajax({
    //             url : "/WMSIF820/createInvoiceData.action",
    //             type : 'post',
    //             data : formData,
    //             contentType : false,
    //             processData : false,
    //             beforeSend: function(xhr){
    //                 cfn_viewProgress();
    //             },
    //             complete: function(){
    //                 cfn_closeProgress();
    //             },
    //             success : function(data){
    //                 alert(data["MSG"]);
    //                 vm.SearchClickEventCallback();
    //             }
    //         });
    //     }
    // }

    // SendInvoiceData () {
    //     const vm = this;
    //     let formData = new FormData(document.getElementById(`${this.#formId}`));
        
    //     if(confirm(`선택한 공급일의 택배 실적을 전송 하시겠습니까?\n(공급일 : ${jQuery("#vrSrchReqDtFromE6").val()})\n(대상: 선택 공급일 기준 미전송 항목)`)){

    //         formData.append('vrCustId', this.#custId);
    //         formData.append('vrOrdDt', jQuery("#vrSrchReqDtFromE6").val());
            
    //         jQuery.ajax({
    //             url : "/WMSIF820/sendInvocieResult.action",
    //             type : 'post',
    //             data : formData,
    //             contentType : false,
    //             processData : false,
    //             beforeSend: function(xhr){
    //                 cfn_viewProgress();
    //             },
    //             complete: function(){
    //                 cfn_closeProgress();
    //             },
    //             success : function(data){
    //                 const message = `${data["MSG"]}\n${data.message}`;
    //                 alert(message);
    //                 vm.SearchClickEventCallback();
    //             }
    //         });

    //     }
    // }

//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility

    /**
     * [WMS → DAS] 주문 정보 전송 (차수별)
     * 
     * @returns 
     */
    SendOrderData (rowData) {
        // const url = rowData.INVC_CNT > 0 ? "/WMSIF820/dasIfSendB2C.action" : "/WMSIF820/dasIfSendNoInvcB2C.action";
        // const url = "/WMSIF820/dasIfSendNoInvcB2C.action";
        const url = "/WMSIF820/dasIfSend.action";           // 일괄(택배/일반) 전송 
        const devOrdDegree = rowData.DEV_ORD_DEGREE;
        let custGubn = null;
        let formData = new FormData();

        switch(jQuery("select[name='SVC_INFO']").val()){
            case '0000003540' : custGubn = 'COOP'; break;
            case '0000001262' : custGubn = 'TEST'; break;
        }

        WinusLog.InsertForInfo(new LogData({
            CUST_ID         : this.#custId
            , FUNCTION      : url
            , LOG_TYPE      : 'HISTORY'
            , LOG_DESC      : `[DAS차수전송] 설비차수/기타/택배유무`
            , USER_DEFINED1 : `${devOrdDegree}`
            , USER_DEFINED2 : custGubn
            , USER_DEFINED3 : rowData.INVC_CNT > 0 ? "택배" : "일반"
        }));

        // formData.append('selectIds', 1);
        // formData.append('CUST_ID0', this.#custId);
        // formData.append('DEV_ORD_DEGREE0', devOrdDegree);
        // formData.append('custGubn', custGubn);

        // Params : 일괄전송
        formData.append('CUST_ID', this.#custId);
        formData.append('OUT_REQ_DT', document.getElementById('vrSrchReqDtFromE6').value);
        formData.append('DEV_ORD_DEGREE', devOrdDegree);

        return new Promise((resolve, reject) => {
            jQuery.ajax({
                url : url,
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(data){
                    // Error ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        const errMsg = data["MSG"];
                        reject(`차수 전송 오류. (관리자 문의)\n${errMsg}`);
                    }
                    // Success ...
                    else{
                        // Error ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            const errMsg = data["MSG"];
                            reject(`차수 전송 오류. (관리자 문의)\n${errMsg}`);
                        }
                        // Success ...
                        else{
                            resolve('작업지시 정보 생성 완료');
                        }
                    }
                }
            });
        }) 
    }

    /**
     * [WMS → DAS] 공급명세서 정보 전송 (COOP)
     * 
     * @returns 
     */
    SendSupplyData (rowData) {
        let formData = new FormData();

        formData.append('CUST_ID', this.#custId);
        formData.append('OUT_REQ_DT', jQuery("#vrSrchReqDtFromE6").val());
        formData.append('ORD_DEGREE', rowData.ORD_DEGREE);
        formData.append('DEV_ORD_DEGREE', rowData.DEV_ORD_DEGREE);
        
        return new Promise((resolve, reject) => {
            jQuery.ajax({
                url : "/WMSOP910/sendDasData.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(data){
                    // Error ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        const errMsg = data["MSG"];
                        reject(`명세서 생성 오류. (관리자 문의)\n${errMsg}`);
                    }
                    // Success ...
                    else{
                        // Error ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            const errMsg = data["MSG"];
                            reject(`명세서 생성 오류. (관리자 문의)\n${errMsg}`);
                        }
                        // Success ...
                        else{
                            resolve('명세서 정보 생성 완료');
                        }
                    }
                }
            });
        });
    }

//#endregion

}