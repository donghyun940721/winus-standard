import { WMSIF707E1 } from './WMSIF707E1.js';
import { WMSIF707E2 } from './WMSIF707E2.js';

let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const ORDER_MANAGEMENT          = '0';      // Tab Index : 0
const MISS_ORDER_MANAGEMENT     = '1';      // Tab Index : 1

jQuery(document).ready(function() {

    let view01 = new WMSIF707E1();      // 1. 주문정보관리
    let view02 = new WMSIF707E2();      // 2. 미수신주문조회

    const ViewContainer = [view01, view02];


    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
    ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){
        
        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        ViewContainer[activeTabIdx].ActiveViewEventCallBack();

        switch(activeTabIdx){
            case ORDER_MANAGEMENT : 
                break;

            case MISS_ORDER_MANAGEMENT : 
                break;
        }
    });
});