import * as InterfaceUtil from '../utility/interfaceUtil.js';
import { WMSIF780E1 } from './WMSIF780E1.js';
import { WMSIF780E2 } from './WMSIF780E2.js';
import { WMSIF780E3 } from './WMSIF780E3.js';
import { WMSIF780E4 } from './WMSIF780E4.js';
import { WMSIF780E5 } from './WMSIF780E5.js';

let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const PRODUCT_INFO  = '0';
const IN_ORDER_INFO   = '1';
const OUT_ORDER_INFO    = '2';
const IN_ORDER_RESULT    = '3';
const OUT_ORDER_RESULT    = '4';

const store = (function(){

    /**
     * 오픈몰 정보 조회 및 컴포넌트 바인딩
     * 
     */
    async function SetOpenMallComboBox(componentId, formId, custId){

        let openMallInfos = new Array();

        if(cfn_isEmpty(custId)){
            jQuery(`#${componentId} option`).remove();
            return ;
        }

        openMallInfos = InterfaceUtil.OpenMallInfo.GetInfo(custId, 'GOODMD');   // 세션조회: 화주별 계정 1개 전제

        /* 1. 토큰 취득 (세션 or 기준정보) */
        if(cfn_isEmpty(openMallInfos) || (openMallInfos.length <= 0)){
            await InterfaceUtil.OpenMallInfo.GetAuthInfo(custId, 'GOODMD', function(){
                openMallInfos = InterfaceUtil.OpenMallInfo.GetInfo(custId, 'GOODMD'); 

                if(cfn_isEmpty(openMallInfos) || (openMallInfos.length <= 0)){
                    alert(`인터페이스 사용 권한이 없습니다. (관리자 문의)`);
                    return;
                } 
            });
        }

        /* 2. 유효성 검사 및 갱신 
        if(!ValidationOfToken(openMallInfos[0])){
            await GetToken(formId, custId, openMallInfos[0])
                .then((dt)=>{
                    openMallInfos = InterfaceUtil.OpenMallInfo.GetInfo(custId, 'ISKYZ'); 
                })
                .catch((message)=>{
                    alert(message);
                    return;
                });
        }*/

        /* 3. 컴포넌트 바인딩 */
        jQuery(`#${componentId} option`).remove();
            
        for(let mallInfo of openMallInfos){

            let option = new Option();

            option.value = mallInfo['OPENMALL_API_ID1'];
            option.text = mallInfo['OPENMALL_CD_DESC'];
            
            jQuery(`#${componentId}`).append(option);
        }

        /*
        function ValidationOfToken(mallInfos){
            let valid = true;
            const DateHandler = new Intl.DateTimeFormat();

            //1. 토큰 발급일과 작업일이 다른 경우 토큰 갱신요청.
            if(DateHandler.format(new Date(mallInfos['TOKEN1_REG_DT'])) != DateHandler.format(new Date())){
                valid = false;
            }

            return valid;
        }
        */

    }

    return {
        SetOpenMallComboBox : SetOpenMallComboBox
    }
})()


jQuery(document).ready(function() {

    let view01 = new WMSIF780E1({ store });      // 1. 상품관리
    let view02 = new WMSIF780E2({ store });      // 2. 입고주문관리
    let view03 = new WMSIF780E3({ store });      // 3. 출고주문관리
    let view04 = new WMSIF780E4({ store });      // 3. 입고결과관리
    let view05 = new WMSIF780E5({ store });      // 3. 출고결과관리

    const ViewContainer = [view01, view02, view03, view04, view05];


    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
    ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){
        
        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        ViewContainer[activeTabIdx].ActiveViewEventCallBack();

        switch(activeTabIdx){
            case PRODUCT_INFO : 
                break;
            case IN_ORDER_INFO : 
                break;
            case OUT_ORDER_INFO : 
                break;
            case IN_ORDER_RESULT : 
                break;
            case OUT_ORDER_RESULT : 
                break;
        }
    });

});