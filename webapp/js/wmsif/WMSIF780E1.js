import * as CustomPopUp             from './WMSIF780E1pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSIF780E1 {

    #custId         = null;
    #formId         = 'frm_listE1';
    #apiCustId      = null;

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE1',
        id          : 'vrSrchRitemIdE1',
        name        : 'vrSrchRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: 'ERP',                     // 그룹헤더에 출력될 명칭
            code: 'ERP_SYS',                 // 그룹헤더 Key값 (아직 사용저 없음)
            startColumnKey: 'GDS_NM',       // 그룹지정 시작 열의 Key값
            endColumnKey: 'MOD_DATE'           // 그룹지정 마지막 열의 Key값
        },
        {
            name: 'WMS',                     // 그룹헤더에 출력될 명칭
            code: 'WMS_SYS',                 // 그룹헤더 Key값 (아직 사용저 없음)
            startColumnKey: 'ITEM_NM',       // 그룹지정 시작 열의 Key값
            endColumnKey: 'MOD_DT'           // 그룹지정 마지막 열의 Key값
        }
    ];


    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE1',
        gridTemplate        :   'WMSIF780E1',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        useFilter           :   true,
        useSort             :   true,
        headerMappingInfo   :   this.#headerMappingInfo,
    });


    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE1").datepicker("setDate", date);                         // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE1").datepicker("setDate", date);                           // 수집등록일자 (to)
        
        //this.#apiCustId = DICTIONARY['ss_svc_no'] == '0000004080' ? "0000488488" : null;
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // 상품정보수집 버튼 이벤트
        jQuery("#btnCollectItemE1").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.ItemCollectClickEventCallBack(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });

        // 상품등록 버튼 이벤트
        jQuery("#btnRegItem").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SyncClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE1").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });


        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE1").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                fn_Popup('CUST');
            }

        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }
        
        /* 오픈몰 정보 설정*/
        this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#apiCustId??this.#custId);
        
        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                this.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#apiCustId??this.#custId);

                break;

            //* 상품
            case COMP_CATEGORY.ITEM :
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);

                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        
                        vm.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#apiCustId??this.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    ItemCollectClickEventCallBack(e){

        jQuery('#popItemCollect').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400
        });
        
        CustomPopUp.ItemCollect.Init({
            'OPENER' : this,
            'CUST_ID' : this.#apiCustId??this.#custId,
            'MASTER_SEQ' : jQuery("#vrSrchSendCompanyIdE1").val()
        });
    }

    SyncClickEventCallback(e){
        try {

            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = null;

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 상품을 선택해야 합니다.`;
            }

            
            saveMessage = gridData.some(dt => dt['SYNC_YN'] == 'Y') 
                                ? '이미 동기화된 상품이 존재합니다.\n' + DICTIONARY['confirm_save']
                                : DICTIONARY['confirm_save']
            

            if(confirm(saveMessage)){

                let formData = new FormData(document.getElementById(`${this.#formId}`));

                formData.append('ROW_COUNT', selectedIdx.length);

                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let rowIdx = selectedIdx[i];

                    let param = {
                        
                        I_ITEM_CODE            : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('GDS_CD'))
                        , I_ITEM_NM            : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('GDS_NM'))
                        , I_ITEM_BAR_CD        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('BAR_CODE'))
                        , I_ITEM_GRP_CD        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('CLSS1_CD'))
                        , I_ITEM_GRP_NM        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('CLSS1_CD_NM'))
                        , I_ITEM_GRP_2ND_CD    : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('CLSS2_CD'))
                        , I_ITEM_GRP_2ND_NM    : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('CLSS2_CD_NM'))
                        , I_ITEM_GRP_3RD_CD    : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('CLSS3_CD'))
                        , I_ITEM_GRP_3RD_NM    : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('CLSS3_CD_NM'))
                        , I_UOM_CD             : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('GDS_UNIT_CD'))
                        , I_UOM_NM             : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('GDS_UNIT_CD_NM'))
                        , I_UNIT_QTY           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('GDS_UNIT_CNT'))
                        , I_STAT_GBN           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('STAT_GBN'))
                        , I_VAT_YN             : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('NO_TAX_YN'))
                        , I_STANDARDS          : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('STANDARDS'))
                        , I_VENDOR_CD          : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('VENDOR_CD'))
                        , I_CASE_QTY           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PACK_CNT'))
                        , I_BOX_QTY            : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('BOX_GDS_CNT'))
                        , I_MIP_TYPE_CD        : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MIP_TYPE_CD'))
                        , I_CUR_PRC            : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('CUR_PRC'))
                        , I_SUPP_PRC           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('SUPP_PRC'))
                        , I_MIP_UNIT_PRC       : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MIP_UNIT_PRC'))
                        , I_VAT_PRC            : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('VAT_PRC'))
                        , I_MARGIN_RT          : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MARGIN_RT'))
                        , I_MOD_DATE           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('MOD_DATE'))
                        , I_RITEM_ID           : this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('RITEM_ID'))
                    }

                    formData.append(`I_ITEM_CODE`        +   `_${i}`, param['I_ITEM_CODE']);
                    formData.append(`I_ITEM_NM`          +   `_${i}`, param['I_ITEM_NM']);
                    formData.append(`I_ITEM_BAR_CD`      +   `_${i}`, param['I_ITEM_BAR_CD']);
                    formData.append(`I_ITEM_GRP_CD`      +   `_${i}`, param['I_ITEM_GRP_CD']);
                    formData.append(`I_ITEM_GRP_NM`      +   `_${i}`, param['I_ITEM_GRP_NM']);
                    formData.append(`I_ITEM_GRP_2ND_CD`  +   `_${i}`, param['I_ITEM_GRP_2ND_CD']);
                    formData.append(`I_ITEM_GRP_2ND_NM`  +   `_${i}`, param['I_ITEM_GRP_2ND_NM']);
                    formData.append(`I_ITEM_GRP_3RD_CD`  +   `_${i}`, param['I_ITEM_GRP_3RD_CD']);
                    formData.append(`I_ITEM_GRP_3RD_NM`  +   `_${i}`, param['I_ITEM_GRP_3RD_NM']);
                    formData.append(`I_UOM_CD`           +   `_${i}`, param['I_UOM_CD']);
                    formData.append(`I_UOM_NM`           +   `_${i}`, param['I_UOM_NM']);
                    formData.append(`I_UNIT_QTY`         +   `_${i}`, param['I_UNIT_QTY']);
                    formData.append(`I_STAT_GBN`         +   `_${i}`, param['I_STAT_GBN']);
                    formData.append(`I_VAT_YN`           +   `_${i}`, param['I_VAT_YN']);
                    formData.append(`I_STANDARDS`        +   `_${i}`, param['I_STANDARDS']);
                    formData.append(`I_VENDOR_CD`        +   `_${i}`, param['I_VENDOR_CD']);
                    formData.append(`I_CASE_QTY`         +   `_${i}`, param['I_CASE_QTY']);
                    formData.append(`I_BOX_QTY`          +   `_${i}`, param['I_BOX_QTY']);
                    formData.append(`I_MIP_TYPE_CD`      +   `_${i}`, param['I_MIP_TYPE_CD']);
                    formData.append(`I_CUR_PRC`          +   `_${i}`, param['I_CUR_PRC']);
                    formData.append(`I_SUPP_PRC`         +   `_${i}`, param['I_SUPP_PRC']);
                    formData.append(`I_MIP_UNIT_PRC`     +   `_${i}`, param['I_MIP_UNIT_PRC']);
                    formData.append(`I_VAT_PRC`          +   `_${i}`, param['I_VAT_PRC']);
                    formData.append(`I_MARGIN_RT`        +   `_${i}`, param['I_MARGIN_RT']);
                    formData.append(`I_MOD_DATE`         +   `_${i}`, param['I_MOD_DATE']);
                    formData.append(`I_RITEM_ID`         +   `_${i}`, param['I_RITEM_ID']);
                }


                jQuery.ajax({
                    url : "/WMSIF780/syncItem.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(data["MSG"]);
                        }
                        // Success ...
                        else{
                            alert(`${DICTIONARY['save_success']} (${selectedIdx.length} 건)`);
                        }
            
                    },
                    error: function(request, status, error){
                        alert(`관리자에게 문의바랍니다. (${request.status})`);
                        console.log(request);
                        console.log(status);
                        console.log(error);
                    }
                });
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }

    }

    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('상품정보', true);
    }

    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
     SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return jQuery.ajax({
            url : "/WMSIF780/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    //let packCnt = data.rows.map(dt=> (dt.PACK == 0) ? dt.SEQ : dt.PACK).filter((value, index, self)=>{return self.indexOf(value) === index;}).length;

                    //jQuery("#totalDlvCnt").text(packCnt);                                               // 합포단위수량
                    //jQuery("#totalOrdCnt").text(vm.#grid.GetRowCountByDistinctKey('SEQ'));              // 관리번호
                    //jQuery("#totalProcCnt").text(vm.#grid.GetRowCountByDistinctKey('PRD_SEQ'));         // 상품관리번호
                }

            }
        });

    }

    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '상품');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}