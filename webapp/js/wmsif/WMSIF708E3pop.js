import * as InterfaceUtil from '../utility/interfaceUtil.js';

const SELLER_COLLECT_POP = "popSellerCollect";


/**
 * 매출처정보 수집
 */
export const SellerCollect = (function(){

    let custId = null;
    let masterSeq = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";
        masterSeq   = "MASTER_SEQ" in args ? args['MASTER_SEQ'] : "";

        AddEvent();    
        
        jQuery("#vrColStDateFromE3").val(localStorage.LocalTime);
        // jQuery("#vrColStDateToE3").val(localStorage.LocalTime);
    }
    
    
    let AddEvent = function(){
        jQuery("#btnCollectE3").off().on("click", (e)=>{
            SellerCollect();
        });


        jQuery("#vrAllDayYnE3").off().on("click", (e)=>{
            document.getElementById('collectDatePickerE3').classList.toggle('act');
        });
    }


    /**
     * 매출처정보 수집 I/F 실행 
     * 
     */
    function SellerCollect(){
        let formData        = new FormData(document.getElementById(`form708E3_1pop`));
        const collectType   = jQuery("#vrCollectTypeE3").val();

        formData.append('CUST_ID'           , custId     );
        formData.append('CATEGORY'          , 2);       // 매입처(입고: 1), 매출처(출고: 2)

        formData.append('Z_TOKEN'           , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'TOKEN1'));
        formData.append('Z_FROM'            , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID2'));
        formData.append('Z_TO'              , InterfaceUtil.OpenMallInfo.GetValueOfKey(custId, masterSeq, 'OPENMALL_API_ID3'));
        
        formData.append("VenderCode"        , "");
        formData.append("VenderName"        , "");
        formData.append("RegistrationNo"    , "");
        formData.append("WriteDate"         , (collectType == 'reg_date') ? jQuery("#vrColStDateFromE3").val(): "");
        formData.append("EditDate"          , (collectType == 'upd_date') ? jQuery("#vrColStDateFromE3").val(): "");


        jQuery.ajax({
            url : "/WMSIF708/collectStore.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${SELLER_COLLECT_POP}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){

                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }

                if(!cfn_isEmpty(data) && data.header == 'N'){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                
                alert(`저장되었습니다.`);
                
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    }

    return {
        Init : Init
    }

})();