import * as CustomPopUp             from './WMSAC270pop.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSAC270E4 {

    #custId         = null;
    #formId         = 'frm_listE4';
    #apiCustId      = null;

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE4',
        id          : 'vrSrchCustIdE4',
        name        : 'vrSrchCustNmE4',
        category    : COMP_CATEGORY.CUST
    });

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE4',
        gridTemplate        :   'WMSAC270E4',
        headerHeight        :   '27',
        rowHeight           :   '22',
        useFilter           :   true,
        useSort             :   true,
    });


    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성
        this.AddGridEvent();

    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        jQuery("#vrSrchReqDtFromE4").val(date.getFormattedString('YYYY-MM'));        
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE4").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE4").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE4").val();

        if(DICTIONARY.ss_user_gb == "20"){
            custId = DICTIONARY.ss_cmpy_id
            custNm = DICTIONARY.ss_cmpy_name
            custCd = DICTIONARY.ss_cmpy_code
        }

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE4").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE4").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE4").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        // * "신규" 버튼 클릭 이벤트
        jQuery("#btnNewE4").off().on("click", (e)=>{
            this.AddRowCallBack(e);
        });

        // * "저장" 버튼 클릭 이벤트
        jQuery("#btnSaveE4").off().on("click", (e)=>{
            this.SaveClickCallBack(e);
        });

        // * "삭제" 버튼 클릭 이벤트
        jQuery("#btnDeleteE4").off().on("click", (e)=>{
            this.DeleteClickCallBack(e);
        });

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE4").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });

        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE4").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE4").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE4").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                fn_Popup('CUST');
            }

        });

    }

    /**
     * 그리드 이벤트 설정
     * 
     * @param {*} args 
     */
    AddGridEvent(){
        //Cell 선택 이벤트 콜백
        this.#grid.CellClickCustomEvent = (sender, args)=>{
            var spd = args.sheet, sheetArea = args.sheetArea, row = args.row, col = args.col;
            const columnCode = this.#grid.columnInfo.filter((dt)=> {return dt.COLUMN_SEQ == (args.col + 1)})[0].COLUMN_CODE;
            jQuery('#spdDatePannel').datepicker('hide');
            //청구일자 선택
            if(this.#grid.GetCheckedList().indexOf(row) != -1){
                switch(columnCode){
                    case 'WORK_DT' : 
                        this.showSpdDatepicker(row, col, spd);
                        break;
                }
            }
        };
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        if(DICTIONARY.ss_user_gb == "20"){
            jQuery("input[name='vrSrchCustCdE4']").attr("class", "readOnly").attr("readonly", "readonly");
            jQuery("input[name='vrSrchCustNmE4']").attr("class", "readOnly").attr("readonly", "readonly");
            jQuery("#vrSrchCustImgE4").off();
            this.#compCust.SetValue(DICTIONARY.ss_cmpy_code,DICTIONARY.ss_cmpy_id,DICTIONARY.ss_cmpy_name);
        }
                
        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;
                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        
                        vm.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE4", this.#formId, this.#apiCustId??this.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    AddRowCallBack(e){
        let writeIdx = new Array();
        this.#grid.columnInfo.forEach((dt) => {
            if (dt.COLUMN_READ_ONLY == "N"){
                writeIdx.push(dt.COLUMN_SEQ - 1)
            }
        });
        this.#grid.RowAddEventCallBack(false,writeIdx,true);
    }

    SaveClickCallBack(e){
        try {
            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = DICTIONARY['confirm_save'];

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 선택해야 합니다.`;
            }

            if(cfn_isEmpty(jQuery("#vrSrchCustIdE4").val())){
                throw `화주를 선택해야 합니다.`;
            }

            if(confirm(saveMessage)){

                let jsonData = new Object();

                jsonData.list = new Array();
                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let dataObject = new Object();
                    let rowIdx = selectedIdx[i];

                    if(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('WORK_DT')) == null || this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('WORK_DT')) ==''){
                        throw `청구일자 입력은 필수입니다.`;
                    }

                    if(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('TEMP_QTY_1')) == null || this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('TEMP_QTY_1')) === ''){
                        throw `수량 입력은 필수입니다.`;
                    }

                    if(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('TEMP_QTY_3')) == null || this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('TEMP_QTY_3')) === ''){
                        throw `수량 입력은 필수입니다.`;
                    }

                    dataObject['ACC_WORK_ID'] = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ACC_WORK_ID'));
                    dataObject['WORK_DT'] = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('WORK_DT'));
                    dataObject['TEMP_QTY_1'] = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('TEMP_QTY_1'));
                    dataObject['TEMP_QTY_3'] = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('TEMP_QTY_3'));

                    jsonData.list.push(dataObject);
                }

                jsonData.count = selectedIdx.length;
                jsonData.custId = jQuery("#vrSrchCustIdE4").val();

                jQuery.ajax({
                    url : "/WMSAC270/saveE4.action",
                    type : 'post',
                    data : JSON.stringify(jsonData),
                    contentType : "application/json; charset=utf-8",
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(data["MSG"]);
                        }
                        // Success ...
                        else{
                            alert(data["MSG"]);
                        }
                    },
                    error: function(request, status, error){
                        alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
                    }
                });
                
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }
        
    }

    DeleteClickCallBack(e){
        try {
            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = DICTIONARY['delete.confirm'];

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 선택해야 합니다.`;
            }

            if(cfn_isEmpty(jQuery("#vrSrchCustIdE4").val())){
                throw `화주를 선택해야 합니다.`;
            }

            if(confirm(saveMessage)){

                let jsonData = new Object();
                
                jsonData.list = new Array();

                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let dataObject = new Object();
                    let rowIdx = selectedIdx[i];

                    if(this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ACC_WORK_ID')) == null || this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ACC_WORK_ID')) ==''){
                        throw `저장되지 않은 데이터 입니다.`;
                    }

                    dataObject['ACC_WORK_ID'] = this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('ACC_WORK_ID'));

                    jsonData.list.push(dataObject);
                }

                jsonData.count = selectedIdx.length;
                jsonData.custId = jQuery("#vrSrchCustIdE4").val();

                jQuery.ajax({
                    url : "/WMSAC270/deleteAcc.action",
                    type : 'post',
                    data : JSON.stringify(jsonData),
                    contentType : "application/json; charset=utf-8",
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(data["MSG"]);
                        }
                        // Success ...
                        else{
                            alert(data["MSG"]);
                        }
                    },
                    error: function(request, status, error){
                        alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
                    }
                });
                
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }
    }

    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('통합정산조회', true);
    }

    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
     SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return jQuery.ajax({
            url : "/WMSAC270/listE4.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    //let packCnt = data.rows.map(dt=> (dt.PACK == 0) ? dt.SEQ : dt.PACK).filter((value, index, self)=>{return self.indexOf(value) === index;}).length;

                    //jQuery("#totalDlvCnt").text(packCnt);                                               // 합포단위수량
                    //jQuery("#totalOrdCnt").text(vm.#grid.GetRowCountByDistinctKey('SEQ'));              // 관리번호
                    //jQuery("#totalProcCnt").text(vm.#grid.GetRowCountByDistinctKey('PRD_SEQ'));         // 상품관리번호
                }

            }
        });

    }

    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '상품');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }



//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility
//DatePicker show for Click event
    showSpdDatepicker(row, col, pdbjspdList) {
        //jQuery('#spdDatePannel').datepicker('hide');
        jQuery('#spdDatePannel').datepicker('destroy');

        if(pdbjspdList.getCell(row, col).locked()) {
            return;
        }

        var dateString="";

        var spdRect = jQuery('#spdListE4');
        var cellRect = pdbjspdList.getCellRect(row, col);
        var hearRowCnt = pdbjspdList.getRowCount(GC.Spread.Sheets.SheetArea.colHeader);

        if (!cfn_isEmpty(pdbjspdList.getValue(row, col)))
        dateString  = replaceAll(pdbjspdList.getValue(row, col), "-", "");
        else
        dateString  = getNowDate();

        var year        = dateString.substring(0,4);
        var month       = dateString.substring(4,6);
        var day         = dateString.substring(6,8);

        var dateFull = year + "-" + month + "-" + day;

        var top = spdRect.offset().top + cellRect.height + cellRect.y;
        var left = spdRect.offset().left - (cellRect.width/2) + cellRect.x;

        jQuery('#spdDatePannel').datepicker({
            changeMonth : true,
            changeYear : true,
            dateFormat : "yy-mm-dd",
            //showOn : "button",
            //buttonImage : "/img/exam/btn/calendar.gif",
            buttonImageOnly : true,
            //showOn: 'both',
            onSelect: function(dateText) {
                pdbjspdList.setValue(row, col, dateText);
            },
            beforeShow: function (event, ui) {
            setTimeout(function () {
                ui.dpDiv.css({ left: left, top: top });      
            }, 0);               
        }
            
        });
        jQuery('#spdDatePannel').datepicker("setDate", dateFull);
        jQuery('#spdDatePannel').datepicker('show');

    }

//#endregion

}