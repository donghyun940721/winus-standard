import { WMSAC016E2 }  from './WMSAC016E2.js';


let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const ACC_ITEM_INFO 	= '0';
const ACC_DLV_INFO   	= '1';


jQuery(document).ready(function() {

    let view01 = null;
    let view02 = new WMSAC016E2();

    const ViewContainer = [view01,view02];

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){

        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;
        ViewContainer[activeTabIdx].ActiveViewEventCallBack();
        
        switch(activeTabIdx){
            case ACC_ITEM_INFO : 
                break;

            case ACC_DLV_INFO : 
                break;
        }

    });
});