const CONF_OUT_ACC_UPLOAD_TEMPLATE = "popConfOutAccUpload";
const RTN_ACC_UPLOAD_TEMPLATE = "popRtnAccUpload";
const ETC_ACC_UPLOAD_TEMPLATE = "popEtcAccUpload";
const ETC_ACC_SAVE = "popEtcAccSave";

export const ConfOutAccUpload = (function(){

    let custId = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";

        AddEvent();    
    }
    
    let AddEvent = function(){
        jQuery("#btnUploadE2").off().on("click", (e)=>{
            ConfOutAccUpload();
        });
        jQuery("#btnTemplateDownE2").off().on("click", (e)=>{
            ConfOutTemplateDown();
        });
    }

    /**
     * 반품 입고 템플릿 업로드
     */
    function ConfOutAccUpload(){

        if(jQuery("#txtFileE2").val() == ""){
            alert("$!message.getMessage('excel.send.fail')");
            return false;
        }

        let formData        = new FormData(document.getElementById(`form270E2_1pop`));

        formData.append('custId'           , custId);
        formData.append('startRow'         , '1');
        formData.append('txtFile', jQuery("#txtFileE2").get(0).files[0]);

        jQuery.ajax({
            url : "/WMSAC270/uploadE2.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${CONF_OUT_ACC_UPLOAD_TEMPLATE}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                alert(data.MSG);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    function ConfOutTemplateDown(){
        var frm 	  = document.createElement('form');
        frm.setAttribute("method","post");
        frm.action = "/WMSAC270/uploadExcelE2.action";
        document.body.appendChild(frm);
        frm.submit();
    }

    return {
        Init : Init
    }
    
})();

export const RtnAccUpload = (function(){

    let custId = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";

        AddEvent();    
    }
    
    let AddEvent = function(){
        jQuery("#btnUploadE5").off().on("click", (e)=>{
            RtnAccUpload();
        });
        jQuery("#btnTemplateDownE5").off().on("click", (e)=>{
            RtnAccTemplateDown();
        });
    }

    /**
     * 반품 입고 템플릿 업로드
     */
    function RtnAccUpload(){

        if(jQuery("#txtFileE5").val() == ""){
            alert("$!message.getMessage('excel.send.fail')");
            return false;
        }

        let formData        = new FormData(document.getElementById(`form270E5_1pop`));

        formData.append('custId'           , custId);
        formData.append('startRow'         , '1');
        formData.append('txtFile', jQuery("#txtFileE5").get(0).files[0]);

        jQuery.ajax({
            url : "/WMSAC270/uploadE5.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${RTN_ACC_UPLOAD_TEMPLATE}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                alert(data.MSG);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    function RtnAccTemplateDown(){
        var frm 	  = document.createElement('form');
        frm.setAttribute("method","post");
        frm.action = "/WMSAC270/uploadExcelE5.action";
        document.body.appendChild(frm);
        frm.submit();
    }

    return {
        Init : Init
    }
    
})();

export const EtcAccUpload = (function(){

    let custId = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";

        AddEvent();    
    }
    
    let AddEvent = function(){
        jQuery("#btnUploadE6").off().on("click", (e)=>{
            EtcAccUpload();
        });
        jQuery("#btnTemplateDownE6").off().on("click", (e)=>{
            EtcAccTemplateDown();
        });
    }

    /**
     * 기타비용 템플릿 업로드
     */
    function EtcAccUpload(){

        if(jQuery("#txtFileE6").val() == ""){
            alert("$!message.getMessage('excel.send.fail')");
            return false;
        }

        let formData        = new FormData(document.getElementById(`form270E6_1pop`));

        formData.append('custId'           , custId);
        formData.append('startRow'         , '1');
        formData.append('txtFile', jQuery("#txtFileE6").get(0).files[0]);

        jQuery.ajax({
            url : "/WMSAC270/uploadE6.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${ETC_ACC_UPLOAD_TEMPLATE}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                alert(data.MSG);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    function EtcAccTemplateDown(){
        var frm 	  = document.createElement('form');
        frm.setAttribute("method","post");
        frm.action = "/WMSAC270/uploadExcelE6.action";
        document.body.appendChild(frm);
        frm.submit();
    }

    return {
        Init : Init
    }
    
})();

export const EtcAccSave = (function(){

    let custId = null;

    let Init = function(args){

        opener      = "OPENER" in args ? args['OPENER'] : "";
        custId      = "CUST_ID" in args ? args['CUST_ID'] : "";

        let date = new Date();

        jQuery("#form270E6_2pop input[name='WORK_DT']").val("WORK_DT" in args ? args['WORK_DT'] : date.getFormattedString('YYYY-MM-DD'));
        jQuery("#form270E6_2pop input[name='MEMO1']").val("MEMO1" in args ? args['MEMO1'] : "");
        jQuery("#form270E6_2pop select[name='SALES_MEMO']").val("SALES_MEMO" in args ? args['SALES_MEMO'] : "01");
        jQuery("#form270E6_2pop input[name='SALES_COST']").val("SALES_COST" in args ? args['SALES_COST'] : "");
        jQuery("#form270E6_2pop input[name='MEMO2']").val("MEMO2" in args ? args['MEMO2'] : "");
        jQuery("#form270E6_2pop input[name='ORG_FILENAME']").val("ORG_FILENAME" in args ? args['ORG_FILENAME'] : "");
        jQuery("#form270E6_2pop input[name='MNG_NO']").val("MNG_NO" in args ? args['MNG_NO'] : "");
        jQuery("#form270E6_2pop input[name='ACC_WORK_ID']").val("ACC_WORK_ID" in args ? args['ACC_WORK_ID'] : "");
        jQuery("#inputFileE6").val("")
        AddEvent();
    }
    
    let AddEvent = function(){
        jQuery("#form270E6_2pop input[name='ORG_FILENAME']").off().on("click", (e)=>{
            EtcAccFileDown();
        });

        jQuery("#btnDeleteFileE6").off().on("click", (e)=>{
            EtcAccFileDelete();
        });

        jQuery("#btnSaveE6").off().on("click", (e)=>{
            EtcAccSave();
        });
    }

    /**
     * 기타비용 저장
     */
    function EtcAccSave(){
        let formData        = new FormData(document.getElementById(`form270E6_2pop`));

        formData.append('custId', custId);

        if(jQuery("#inputFileE6").val() != ""){
            formData.append('txtFile', jQuery("#inputFileE6").get(0).files[0]);
        }

        jQuery.ajax({
            url : "/WMSAC270/saveFormE6.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${ETC_ACC_SAVE}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                alert(data.MSG);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    
    }

    function EtcAccFileDown(){
        let formData        = new FormData(document.getElementById(`form270E6_2pop`));
        if(cfn_isEmpty(formData.get("MNG_NO"))){
            return;
        }
        var frm 	  = document.createElement('form');
        frm.setAttribute("method","post");
        frm.action = "/WMSAC270/downFileE6.action?MNG_NO=" + formData.get("MNG_NO");
        document.body.appendChild(frm);
        frm.submit();
    }

    function EtcAccFileDelete(){
        let formData        = new FormData(document.getElementById(`form270E6_2pop`));

        if(cfn_isEmpty(formData.get("MNG_NO"))){
            return alert("삭제할 첨부 파일이 없습니다.");
        }

        jQuery.ajax({
            url : "/WMSAC270/deleteFileE6.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                jQuery(`#${ETC_ACC_SAVE}`).bPopup().close();

                // 호출객체 CallBack 함수 수행
                if(!(opener == null)){
                    opener.SearchClickEventCallback();
                }
            },
            success : function(data){
                if(cfn_isEmpty(data)){
                    alert(`비정상 종료되었습니다. (관리자 문의)`);
                    return ;
                }
                alert(data.MSG);
            },
            error: function(request, status, error){
                alert(`관리자에게 문의바랍니다. (${request.status} / ${request.responseText})`);
            }
        });
    }

    return {
        Init : Init
    }
    
})();