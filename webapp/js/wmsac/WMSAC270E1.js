import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSAC270E1 {

    #custId         = null;
    #formId         = 'frm_listE1';
    #apiCustId      = null;

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE1',
        gridTemplate        :   'WMSAC270E1',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '-1',
        useFilter           :   true,
        useSort             :   true,
        checkFirstIdx       :   null,
    });


    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        jQuery("#vrSrchReqDtFromE1").val(date.getFormattedString('YYYY-MM'));        
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(DICTIONARY.ss_user_gb == "20"){
            custId = DICTIONARY.ss_cmpy_id
            custNm = DICTIONARY.ss_cmpy_name
            custCd = DICTIONARY.ss_cmpy_code
        }

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE1").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });

        // *'그리드변경' 버튼 클릭 이벤트
        jQuery("#btnChangeGridE1").off().on("click", (e)=>{
            this.GridChangedCallBack();
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                fn_Popup('CUST');
            }

        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        if(DICTIONARY.ss_user_gb == "20"){
            jQuery("input[name='vrSrchCustCdE1']").attr("class", "readOnly").attr("readonly", "readonly");
            jQuery("input[name='vrSrchCustNmE1']").attr("class", "readOnly").attr("readonly", "readonly");
            jQuery("#vrSrchCustImgE1").off();
            this.#compCust.SetValue(DICTIONARY.ss_cmpy_code,DICTIONARY.ss_cmpy_id,DICTIONARY.ss_cmpy_name);
        }
                
        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;
                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        
                        vm.#_store.SetOpenMallComboBox("vrSrchSendCompanyIdE1", this.#formId, this.#apiCustId??this.#custId);
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('통합정산조회', true);
    }

    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
     SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return jQuery.ajax({
            url : "/WMSAC270/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);

                    vm.SetDataCallBack(data);

                    //let packCnt = data.rows.map(dt=> (dt.PACK == 0) ? dt.SEQ : dt.PACK).filter((value, index, self)=>{return self.indexOf(value) === index;}).length;

                    //jQuery("#totalDlvCnt").text(packCnt);                                               // 합포단위수량
                    //jQuery("#totalOrdCnt").text(vm.#grid.GetRowCountByDistinctKey('SEQ'));              // 관리번호
                    //jQuery("#totalProcCnt").text(vm.#grid.GetRowCountByDistinctKey('PRD_SEQ'));         // 상품관리번호
                }

            }
        });

    }

    SetDataCallBack(data){

        let totalAccPrice = 0;

        for(var i = 0 ; i < data.rows.length; i++){
            totalAccPrice += Number(data.rows[i].CALC_SUM);
        }
        //style 설정
        let msgStyle = this.#grid.GetColumnStyle(0);
        let valStyle = this.#grid.GetColumnStyle(3);
        msgStyle.vAlign = 2 //RIGHT
        msgStyle.font = 'bold 14px arial';

        valStyle.font = 'bold 14px arial';


        let curRow = this.#grid.GetRowCount();
        //총 정산금액
        this.#grid.RowAddEventCallBack(true,null,null,null);
        this.#grid.spdList.addSpan(curRow,0,1,3);
        this.#grid.setValue(curRow,0,"총 정산금액");
        this.#grid.setValue(curRow,3,totalAccPrice);
        this.#grid.SetColumnStyle(curRow,0,msgStyle);
        this.#grid.SetColumnStyle(curRow,3,valStyle);

        curRow = this.#grid.GetRowCount();
        //부가세
        this.#grid.RowAddEventCallBack(true,null,null,null);
        this.#grid.spdList.addSpan(curRow,0,1,3);
        this.#grid.setValue(curRow,0,"부가세");
        this.#grid.setValue(curRow,3,Math.floor(totalAccPrice/10));
        this.#grid.SetColumnStyle(curRow,0,msgStyle);
        this.#grid.SetColumnStyle(curRow,3,valStyle);

        curRow = this.#grid.GetRowCount();
        this.#grid.RowAddEventCallBack(true,null,null,null);
        this.#grid.spdList.addSpan(curRow,0,1,3);
        this.#grid.setValue(curRow,0,"총 청구 금액");
        this.#grid.setValue(curRow,3,(Number(totalAccPrice)+Number(Math.floor(totalAccPrice/10))));
        this.#grid.SetColumnStyle(curRow,0,msgStyle);
        this.#grid.SetColumnStyle(curRow,3,valStyle);
    }

    GridChangedCallBack(){

        let url         = '/GridChangePopup.action';
        let name        = 'gridChangePopup';
        let width       = '1200';
        let height      = '760';

        let asppop = cfn_openPop(url, name, width, height);     // common.js

        /** 팝업구성 Parameter  */
        LocalStorageUtil.Input('GRID_TEMPLATE', this.#grid.gridTemplate);
        LocalStorageUtil.Input('TAB_NM', '상품');
        LocalStorageUtil.Input('LC_NM', jQuery('.company option:selected').text());

        /** PopUp Callback Event Listener */
        asppop.addEventListener('unload', function(e){

            // 자식 PopUp 닫혔을 때 수행
            if(e.target.URL != 'about:blank' && LocalStorageUtil.GetValue('REFRESH') == 'Y'){

                LocalStorageUtil.Remove('REFRESH');
                LocalStorageUtil.Remove('TAB_NM');
                LocalStorageUtil.Remove('LC_NM');
                LocalStorageUtil.Remove('GRID_TEMPLATE');

                window.location.reload();
            }

        });
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}