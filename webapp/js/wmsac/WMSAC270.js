import { WMSAC270E1 } from './WMSAC270E1.js';
import { WMSAC270E2 } from './WMSAC270E2.js';
import { WMSAC270E3 } from './WMSAC270E3.js';
import { WMSAC270E4 } from './WMSAC270E4.js';
import { WMSAC270E5 } from './WMSAC270E5.js';
import { WMSAC270E6 } from './WMSAC270E6.js';

let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const TOTAL_ACC_INFO  = '0';
const DELIVERY_ACC_INFO   = '1';
const IN_ACC_INFO    = '2';
const STOCK_ACC_INFO    = '3';
const RETURN_ACC_INFO    = '4';
const ETC_ACC_INFO    = '5';

const store = (function(){

})()


jQuery(document).ready(function() {

    let view01 = new WMSAC270E1({ store });      // 1. 통합정산조회
    let view02 = new WMSAC270E2({ store });      // 2. 택배출고정산조회
    let view03 = new WMSAC270E3({ store });      // 3. 입고정산조회
    let view04 = new WMSAC270E4({ store });      // 4. 보관정산조회
    let view05 = new WMSAC270E5({ store });      // 5. 반품정산조회
    let view06 = new WMSAC270E6({ store });      // 6. 기타비정산조회

    const ViewContainer = [view01, view02, view03, view04, view05, view06];

    jQuery.datepicker.setDefaults( jQuery.datepicker.regional[DICTIONARY.ss_lang ] );

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
    ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){
        
        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        ViewContainer[activeTabIdx].ActiveViewEventCallBack();

        switch(activeTabIdx){
            case TOTAL_ACC_INFO : 
                break;
            case DELIVERY_ACC_INFO : 
                break;
            case IN_ACC_INFO : 
                break;
            case STOCK_ACC_INFO : 
                break;
            case RETURN_ACC_INFO : 
                break;
            case ETC_ACC_INFO : 
                break;
        }
    });

});