import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil }   from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';


export class WMSST030E1{
    
    #custId         = null;
    #formId         = 'frm_listE1';
    
    #isGridEventWaiting = false;            // Grid 이벤트 대기상태 (외부 이벤트 호출 여부)

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE1',
        id          : 'vrSrchRitemIdE1',
        name        : 'vrSrchRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });

    #compWarehouse = new Component({
        code        : 'vrSrchWhCdE1',
        id          : 'vrnSrchWhIdE1',
        name        : 'vrSrchWhNmE1',
        category    : COMP_CATEGORY.WH
    });

    #compLocation = new Component({
        code        : 'vrSrchLocCdE1',
        id          : 'vrSrchLocIdE1',
        name        : 'vrSrchLocNmE1',
        category    : COMP_CATEGORY.LOCATION
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: '양도',
            code: 'SELL_GROUP',
            startColumnKey: 'LOC_CD',
            endColumnKey: 'SELL_ITEM_UOM'
        },
        {
            name: '양수',
            code: 'BUY_GROUP',
            startColumnKey: 'BUY_ITEM_CD',
            endColumnKey: 'BUY_QTY'
        }
    ];


    /** 컨텍스트 메뉴 정보 */
    #contextMenuInfo = [
        {
            text: '선택 행 채우기',
            name: 'menu001',
            workArea: "viewportcorner",
            command:'menu001',
            action: this.ContextMenuEventCallBack
        },
    ]


    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE1',
        gridTemplate        :   'WMSST030E1',
        headerHeight        :   '27',
        rowHeight           :   '25',
        useFilter           :   true,
        exFileExportArr     :   [0,11],
        useSort             :   true,
        headerMappingInfo   :   this.#headerMappingInfo,
        usedContextMenu     :   true,
        contextMenuInfo     :   this.#contextMenuInfo,
    });


    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

        let settingTimer = setInterval(()=>{
            /** Grid 생성대기  */
            if (vm.#grid.created) {
                clearInterval(settingTimer);

                /** Grid 컬럼 사용자 정의 */
                vm.#grid.SetCustomColumn('ARROW', function(obj, formatterData){ 
                    return '→' ;
                });
            }	
        }, 100);
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }
    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        this.AddGridEvent();

        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 창고정보
        jQuery("#vrSrchWhCdE1").off().on("change", (e)=>{
            this.WareHouseChangeEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchWhImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }

            StandardPopUp.call(this, COMP_CATEGORY.WH, param);
        });

        jQuery("#vrSrchWhNmE1").off().on("change", (e)=>{
            this.WareHouseChangeEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 로케이션
        jQuery("#vrSrchLocCdE1").off().on("change", (e)=>{
            this.LocChangeEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchLocImgE1").off().on("click", (e)=>{
            let param = {
                WH_ID     : jQuery("#vrnSrchWhIdE1").val(),
                RITEM_ID  : jQuery("#vrSrchRitemIdE1").val(),
                VIEW_QTY  : 'Y'
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.LOCATION, param);
        });

        jQuery("#vrSrchLocNmE1").off().on("change", (e)=>{
            this.LocChangeEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST, param);
            }

        });


        // * '명의변경' 버튼 클릭 이벤트
        jQuery("#btnExcute").off().on("click", (e)=>{

            if(confirm(DICTIONARY['confirm_save'])){
                this.ExcuteClickEventCallBack();
            }
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDown").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 

//#endregion


//#region   :: Grid Event


    AddGridEvent = () => {
        
        this.#grid.CellEditedCustomEvent = (sender, args) => {

            const column = this.#grid.GetColumnCodeByIdx(args.col);


            // TODO :: 자동완성

            switch(column){

                case 'BUY_ITEM_CD' : 
                    break;


                case 'BUY_RITEM_NM' :
                    break;


                default :
                    // 편집 행 자동 선택
                    this.#grid.SetValueByColKey(this.#grid.targetRow, 'CHECK', true);
            }
        }

        this.#grid.ButtonClickCustomEvent = (sender, args) => {

            const column = this.#grid.GetColumnCodeByIdx(args.col);

            switch(column){

                // 상품 조회 버튼 (양수)
                case 'ICON' :
                    let param = {
                        CUST_ID : this.#custId
                    }
        
                    this.#isGridEventWaiting = true;      // 이벤트 대기 토글
                    StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);

                    break;
            }
        }
    }


    /**
     * 우 클릭 Context Menu 이벤트 콜백함수
     * 
     * @this    : WinusGrid
     * @param {*} target 
     */
    ContextMenuEventCallBack(target){

        const rowIdx = target.activeRow;
        const colIdx = target.activeCol;
        const selectedRowIdx    = this.GetCheckedList();
        const isReadOnly        = this.columnInfo[colIdx].COLUMN_READ_ONLY == 'Y';

        let value = null

        if(selectedRowIdx.length == 0 || isReadOnly)  return ;
        
        // value = this.getValue(rowIdx, colIdx);    // [1안] : ContextMenu를 활성화 시킨 행의 값
        value = this.GetCellValue(this.GetColumnCodeByIdx(colIdx), selectedRowIdx[0]);  // [기존로직] : 선택 행 중 최상위 행의 값
        
        switch(target.cmd){

            /**
             * 일괄 채우기 [1안]
             * 선택 행들을 활성화된 셀 값을 기준으로 변경
             */
            case 'menu001' :
                selectedRowIdx.map(i => this.setValue(i, colIdx, value));

                break;
            
        }
    }


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                break;

            //* 창고
            case COMP_CATEGORY.WH :
                vm.#compWarehouse.SetValue(data[0][0].wh_cd, data[0][0].wh_id, data[0][0].wh_nm);

                break;

            //* 상품
            case COMP_CATEGORY.ITEM :
                // Grid Event Callback
                if(vm.#isGridEventWaiting){
                    vm.#grid.SetValueByColKey(vm.#grid.targetRow, 'BUY_ITEM_CD', data[0][0].item_code);
                    vm.#grid.SetValueByColKey(vm.#grid.targetRow, 'BUY_RITEM_ID', data[0][0].ritem_id);
                    vm.#grid.SetValueByColKey(vm.#grid.targetRow, 'BUY_RITEM_NM', data[0][0].item_kor_nm);

                    vm.#isGridEventWaiting = false; // 대기 토글 반환
                }
                else{
                    vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);
                }

                break;

            //* 로케이션
            case COMP_CATEGORY.LOCATION :
                vm.#compLocation.SetValue(data[0][0].loc_cd, data[0][0].loc_id, data[0][0].loc_cd);

                break;
        }

    }


    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async WareHouseChangeEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compWarehouse.Init();
            return;
        }

        await this.#compWarehouse.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'WH_CD' : 'WH_NM'] = refValue;
                        
                        StandardPopUp.call(vm, COMP_CATEGORY.WH, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async LocChangeEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compLocation.Init();
            return;
        }

        await this.#compLocation.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'LOC_CD' : 'LOC_ID'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.LOCATION, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        

        return jQuery.ajax({
            url : "/WMSST100/selectStockList.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }


    /**
     * '명의변경' 버튼 클릭 이벤트
     * 
     * @returns 
     */
    ExcuteClickEventCallBack = () => {

        const vm = this;
        let selectedData = this.#grid.GetSelectedRowData(0);
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.append('ROW_COUNT', selectedData.length);

        if(selectedData.length <=0){
            alert('1개 이상 행을 선택하세요.');
            return ;
        }

        const validInfo = this.ValidForChangedOwner(selectedData);

        if(!validInfo.VALID){
            alert(validInfo.MESSAGE);
            return;
        }

        for(let i = 0; i < selectedData.length; i++){

            formData.append(`I_CUST_CD`          + `_${i}`, jQuery("#vrSrchCustCdE1").val()       );       // 화주 코드
            formData.append(`I_FROM_RITEM_CD`    + `_${i}`, selectedData[i]['SELL_ITEM_CD']       );       // 양도 상품코드
            formData.append(`I_TO_RITEM_CD`      + `_${i}`, selectedData[i]['BUY_ITEM_CD']        );       // 양수 상품코드
            formData.append(`I_WORK_QTY`         + `_${i}`, selectedData[i]['BUY_QTY']            );       // 작업수량 (양수)
            formData.append(`I_SUB_LOT_ID`       + `_${i}`, selectedData[i]['SUB_LOT_ID']         );       // SUB_LOT_ID (양도 재고 위치)
            formData.append(`I_CHANGE_WORK_MEMO` + `_${i}`, selectedData[i]['CHANGE_WORK_MEMO']   );       // 변경사유

        }
        
        jQuery.ajax({
            url : "/WMSST030/excute.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();

                vm.SearchClickEventCallback();
            },
            success : function(data){

                if(data != null){
                    alert(data['MSG']);
                }
            }
        });
    }


    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('명의변경(부분재고)');
    }


//#endregion


//#region   :: Validation


    ValidForChangedOwner = (gridData) => {

        const INVALID_SEQ       = {
            'NOTNULL_BUY_ITEM_CD'    :  '00001',
            'NOTNULL_BUY_RITEM_NM'   :  '00002',  
            'NOTNULL_BUY_QTY'        :  '00003',
            'ZERO_OUT_EXP_QTY'       :  '00004',
            'COMPARE_WORK_QTY'       :  '00005'
        };

        let valid = true;
        let invalidMessage 	= null;	
        let invalidData 	= new Map();
        let rtnMessage 		= '';

        for(const rowData of gridData){

            const rowIdx = rowData['ROW_IDX'];
            let validSeq = null;

            // Get Value
            const targetInfo = {
                'ROW_IDX'       : rowData['ROW_IDX'],
                'SELL_QTY'      : rowData['SELL_QTY'],
                'BUY_ITEM_CD'   : rowData['BUY_ITEM_CD'],
                'BUY_RITEM_NM'  : rowData['BUY_RITEM_NM'],
                'BUY_QTY'       : rowData['BUY_QTY'],
                'OUT_EXP_QTY'   : rowData['OUT_EXP_QTY']
            }

            // Invalid Check (Mapping Seq)
            if(cfn_isEmpty(targetInfo.BUY_ITEM_CD))         { validSeq = INVALID_SEQ['NOTNULL_BUY_ITEM_CD'] }
            if(cfn_isEmpty(targetInfo.BUY_RITEM_NM))        { validSeq = INVALID_SEQ['NOTNULL_BUY_RITEM_NM'] }
            if(cfn_isEmpty(targetInfo.BUY_QTY?.toString()))  { validSeq = INVALID_SEQ['NOTNULL_BUY_QTY'] }
            if(Number(targetInfo.OUT_EXP_QTY) > 0)          { validSeq = INVALID_SEQ['ZERO_OUT_EXP_QTY'] }
            if(Number(targetInfo.BUY_QTY) > Number(targetInfo.SELL_QTY)) {
                validSeq = INVALID_SEQ['COMPARE_WORK_QTY'] 
            }

            // Print Message
            switch(validSeq){

                case INVALID_SEQ['NOTNULL_BUY_ITEM_CD'] :
                    valid = false;
                    invalidMessage = `상품코드는 공백일 수 없습니다.`;

                    break;

                case INVALID_SEQ['NOTNULL_BUY_RITEM_NM'] :
                    valid = false;
                    invalidMessage = `상품명은 공백일 수 없습니다.`;

                    break;
                    
                case INVALID_SEQ['NOTNULL_BUY_QTY'] :
                    valid = false;
                    invalidMessage = `상품수량(양수)은 공백일 수 없습니다.`;

                    break;
                    
                case INVALID_SEQ['ZERO_OUT_EXP_QTY'] :
                    valid = false;
                    invalidMessage = `출고예정수량이 있는 위치의 재고는 변경할 수 없습니다.`;

                    break;

                case INVALID_SEQ['COMPARE_WORK_QTY'] :
                    valid = false;
                    invalidMessage = `'수량(양수)'은 '수량(양도)' 보다 클 수 없습니다..`;

                    break;

                default :
                    valid = true;
            }

            if(!valid){
                /** Insert & Update */
                if(invalidData.has(validSeq)){
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: [...invalidData.get(validSeq).DATA, ...[targetInfo]] });
                }
                else{
                    invalidData.set(validSeq, { 'VALID'	: valid, 'MESSAGE' : invalidMessage, 'DATA'	: new Array(targetInfo) });
                }
            }
        }

        // Gathering Message
        if(invalidData.size > 0){
        
            invalidData.forEach((value, key, map)=>{

                rtnMessage += `[${key}] ${value.MESSAGE}\n`;

                for(const rowData of value['DATA']){
                    rtnMessage += `${(rowData.ROW_IDX + 1)} 행\n`;
                }

                rtnMessage += `\n`;
            });
        }


        return {
            'VALID' 	: (invalidData.size == 0), 
            'DATA'		: invalidData,
            'MESSAGE' 	: rtnMessage
        }

    }


//#endregion


//#region   :: Utility


//#endregion

}