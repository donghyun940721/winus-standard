import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil }  from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';


export class WMSST030E3{                // !(필수) 화면 명 수정

    #custId         = null;
    #formId         = 'frm_listE3';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE3',
        id          : 'vrSrchCustIdE3',
        name        : 'vrSrchCustNmE3',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE3',
        id          : 'vrSrchRitemIdE3',
        name        : 'vrSrchRitemNmE3',
        category    : COMP_CATEGORY.ITEM
    });

    #compLocation = new Component({
        code        : 'vrSrchLocCdE3',
        id          : 'vrSrchLocIdE3',
        name        : 'vrSrchLocNmE3',
        category    : COMP_CATEGORY.LOCATION
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: '양도',
            code: 'SELL_GROUP',
            startColumnKey: 'SELL_CUST_NM',
            endColumnKey: 'SELL_UOM_NM'
        },
        {
            name: '양수',
            code: 'BUY_GROUP',
            startColumnKey: 'BUY_RITEM_NM',
            endColumnKey: 'BUY_UOM_NM'
        }
    ];
    

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE3',
        gridTemplate        :   'WMSST030E3',
        rowSpanColIdx       :   1,
        rowSpanColArr       :   [1],
        headerHeight        :   '27',
        rowHeight           :   '25',
        useFilter           :   true,
        setColor            :   false,
        headerMappingInfo   :   this.#headerMappingInfo,
        exFileExportArr     :   [0]
    });


    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

        let settingTimer = setInterval(()=>{
            /** Grid 생성대기  */
            if (vm.#grid.created) {
                clearInterval(settingTimer);

                /** Grid 컬럼 사용자 정의 */
                vm.#grid.SetCustomColumn('ARROW', function(obj, formatterData){ 
                    return '→' ;
                });
            }	
        }, 100);
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        
        jQuery("#vrSrchReqDtFromE3").datepicker("setDate", date);         // 작업일자 (from)
        jQuery("#vrSrchReqDtToE3").datepicker("setDate", date);           // 작업일자 (to)

    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE3").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE3").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE3").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE3").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrcRitemImgE3").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE3").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 로케이션
        jQuery("#vrSrchLocCdE3").off().on("change", (e)=>{
            this.LocChangeEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchLocImgE3").off().on("click", (e)=>{
            let param = {
                WH_ID     : jQuery("#vrnSrchWhIdE3").val(),
                RITEM_ID  : jQuery("#vrSrchRitemIdE3").val(),
                VIEW_QTY  : 'Y'
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.LOCATION, param);
        });

        jQuery("#vrSrchLocNmE3").off().on("change", (e)=>{
            this.LocChangeEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE3").off().on("click", (e)=>{

            // 필수선택값 확인
           // if(!cfn_isEmpty(jQuery("#vrSrchCustIdE3").val())){
                this.SearchClickEventCallback(e);
//            }
//            else{
//                StandardPopUp.call(this, COMP_CATEGORY.CUST, param);
//            }

        });

        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE3").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // Set Cust Info
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), SessionUtil.GetValue(SessionKey.CUST_ID), SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 

//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST : 
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;
                break;

            //* 상품
            case COMP_CATEGORY.ITEM : 
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);
                break;

            //* 로케이션
            case COMP_CATEGORY.LOCATION: 
                vm.#compLocation.SetValue(data[0][0].loc_cd, data[0][0].loc_id, data[0][0].loc_cd);
                break;
        }

    }


    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async ItemChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compItem.Init();
            return;
        }

        await this.#compItem.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    async LocChangeEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compLocation.Init();
            return;
        }

        await this.#compLocation.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'LOC_CD' : 'LOC_ID'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.LOCATION, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        

        return jQuery.ajax({
            url : "/WMSST100/listE3.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }


    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('변경이력');
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}