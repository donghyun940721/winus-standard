import { WMSST030E1 } from './WMSST030E1.js';
import { WMSST030E2 } from './WMSST030E2.js';
import { WMSST030E3 } from './WMSST030E3.js';

let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const CHANGE_PROC               = '0';      // Tab Index : 0
const CHANGE_PROC_UOM           = '1';      // Tab Index : 1
const CHANGE_OWNER_HIS          = '2';      // Tab Index : 1

jQuery(document).ready(function() {

    let view01 = new WMSST030E1();      // 1. 부분재고 : SUB_LOT_ID 기준 변경
    let view02 = new WMSST030E2();      // 2. 전체재고 : 상품정보 기준 변경 (UOM만 변경)
    let view03 = new WMSST030E3();      // 3. 변경이력

    // let standardData = {};
    

    const ViewContainer = [view01, view02, view03];


    // jQuery.when(UI.Utility.GetData(UI.COMP_CATEGORY.UOM)).done((uomList)=>{

    //     standardData = {...standardData, ...uomList}

    // });


    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
    ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){
        
        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        ViewContainer[activeTabIdx].ActiveViewEventCallBack();

        switch(activeTabIdx){
            case CHANGE_PROC : 
                break;

            case CHANGE_PROC_UOM : 
                break;

            case CHANGE_OWNER_HIS : 
                break;
        }
    });
});