// TODO :: (삭제예정) StandardPopUp 사용 
function WareHousePopUpEventCallBack(){}
function CustPopUpEventCallBack(){}
function CustShopPopUpEventCallBack(){}
function ItemPopUpEventCallBack(){}
function LocPopUpEventCallBack(){}
function LocPopUpEventCallBack2(){}

// TODO :: (삭제예정) StandardPopUp 사용 
function fn_Popup(type, param={}, callBack){
	var strParameter = [];
	var url = '';
	var width = "800";
	var height = "530";
    let callback = null;
	
	switch(type){

		//* 창고
		case 'WH':
			callback = 'WareHousePopUpEventCallBack';
			param = `?func=${callback}`;
			url = "/WMSMS040/pop.action" + param;
			break;

        //* 화주
        case 'CUST':
            callback = "CustPopUpEventCallBack";
			strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);			// 화주코드
			strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);			// 화주명
			strParameter.push('');														// 거래처 ID
			strParameter.push('');														// LC_ID
			strParameter.push('');														// LC_ALL
			param = `?func=${callback}&custType=12&strParameter=${strParameter}`;
			url = "/WMSCM011.action" + param;
            break;

		//* 거래처
		//! 거래처 코드(custType)에 따라 조회 조건이 다름
		case 'CUST_SHOP' : 
			callback = 'CustShopPopUpEventCallBack';
			strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);				// 검색코드 (거래처)
			strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);				// 검색명칭 (거래처)
			strParameter.push('TRANS_CUST_ID' in param ? param['TRANS_CUST_ID'] : null);	// 거래처 ID
			strParameter.push('LC_ID' in param ? param['LC_ID'] : null);					// 센터 ID
			strParameter.push('LC_ALL');													// LC_ALL
			param  = `?func=${callback}&custType=3&strParameter=${strParameter}&stBtnYn=N`;
			url    = "/WMSCM011.action" + param;

			break;

		//* 로케이션
		case 'LOCATION': 
			callback = "LocPopUpEventCallBack";
			strParameter.push('LOC_CD' 			in 	param ? param['LOC_CD'] 		: null);		// 검색코드 (로케이션)
			strParameter.push('LOC_ID' 			in 	param ? param['LOC_ID'] 		: null);		// 검색명칭 (로케이션)
			strParameter.push('WH_ID' 			in 	param ? param['WH_ID'] 			: null);		// 창고ID
			strParameter.push('RITEM_ID' 		in 	param ? param['RITEM_ID'] 		: null);		// 상품 RITEM_ID
			strParameter.push('UOM_ID' 			in 	param ? param['UOM_ID'] 		: null);		// UOM_ID
			strParameter.push('VIEW_LOT_ID' 	in 	param ? param['VIEW_LOT_ID'] 	: null);  		// 조회형식 : SUB_LOT_ID, STOCK_ID
			strParameter.push('VIEW_QTY' 		in 	param ? param['VIEW_QTY'] 		: null);		// 조회형식 : 재고수량 조회여부 (AVAILABLE_QTY)
			strParameter.push('VIEW_ONLY_LOC' 	in 	param ? param['VIEW_QTY'] 		: '');			// 조회형식 : 로케이션 정보만 조회
			param = `?func=${callback}&strParameter=${strParameter}`;
			url = "/WMSCM080.action" + param;

			break;

        //  // 입고처 (화주조회)
		// case 'INCUST' : 
		// 	if("" == jQuery("form[name='frm_list'] input[name='vrSrchCustCd']").val()){
		// 		alert("$!message.getMessage('0가없음', '" + custCd_Text + "')");
		// 		fn_Popup('CUST');
		// 		return false;
		// 	}else{
		// 		strParameter.push('');
		// 		strParameter.push('');
		// 		strParameter.push('108');
		// 		strParameter.push('');
		// 		strParameter.push(jQuery("form[name='frm_list'] input[name='vrSrchCustId']").val());
		// 		strParameter.push('1');
		// 		param = `?func=${selectedInCustCallBack.name}&strParameter=${strParameter}`;
		// 		url = "/TMSCM010.action" + param;
		// 	}	
        //     break;	

		//* 상품코드
        case 'ITEM':	
            callback = "ItemPopUpEventCallBack";
            strParameter.push('RITEM_CD' in param ? param['RITEM_CD'] : null);		// 상품코드
            strParameter.push('RITEM_NM' in param ? param['RITEM_NM'] : null);		// 상품명
            strParameter.push('CUST_ID' in param ? param['CUST_ID'] : null);		// 화주ID
            strParameter.push('');													// 창고ID
            strParameter.push('');													// SET상품을 보여줄 것인지 여부
            strParameter.push('Y');													// 재고유무 관계없이 표시여부
            param = `?func=${callback}&strParameter=${strParameter}`;
            url = "/WMSCM091.action" + param;
            break;
		
		//* 로케이션
		case 'LOC': 
			callback = "LocPopUpEventCallBack2";
			strParameter.push('LOC_CD' in param ? param['LOC_CD'] : null);
			strParameter.push('LOC_ID' in param ? param['LOC_ID'] : null);
			param = `?func=${callback}&strParameter=${strParameter}`;
			url = "/WMSCM080.action" + param;
			break;	
	}
	
    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
	asppop.focus();

	// asppop.addEventListener('beforeunload', function(...args) {
	// 	debugger;
	// });
}


let viewer 		= null;
let callType 	= null;
let popupResult	= null;
function CommonPopUpCallBack(...args) { popupResult = args};

/**
 * 기준정보성 조회 팝업 호출
 * 
 * ! 각 View Class에 'StandardPopUpCallBack' 콜백함수가 정의되어 있어야 함.
 * ! Viewer 호출 시, Viewer의 'this'값으로 실행컨텍스트를 변경할 것. (Function.prototype.call)
 * ! 전역변수 호출 시, callBack 함수 전달
 * 
 * @param {*} type 		: 조회타입
 * @param {*} param 	: 매개변수
 * @param {*} callBack 
 */
function StandardPopUp(type, param={}, callBack){
	var strParameter = [];
	var url = '';
	var width = "800";
	var height = "530";
	const callbackNm = CommonPopUpCallBack.name;

	popupResult = null;
	viewer = this;
	callType = type;
	
	switch(type){

		//* 창고
		case 'WH':
			strParameter.push('WH_CD' in param ? param['WH_CD'] : null);				// 검색코드 (창고코드)
			strParameter.push('WH_NM' in param ? param['WH_NM'] : null);				// 검색명칭 (창고명)
			param = `?func=${callbackNm}`;
			url = "/WMSMS040/pop.action" + param;
			break;

        //* 화주
        case 'CUST':
			strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);			// 화주코드
			strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);			// 화주명
			strParameter.push('');														// 거래처 ID
			strParameter.push('');														// LC_ID
			strParameter.push('');														// LC_ALL
			param = `?func=${callbackNm}&custType=12&strParameter=${strParameter}`;
			url = "/WMSCM011.action" + param;
            break;

		//* 거래처
		//! 거래처 코드(CUST_TYPE)에 따라 조회 조건이 다름 (1: 입고처, 2: 출고처, 3: 판매처, 4: 배송처 ...)
		case 'STORE' : 
			strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);				// 검색코드 (거래처)
			strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);				// 검색명칭 (거래처)
			strParameter.push('TRANS_CUST_ID' in param ? param['TRANS_CUST_ID'] : null);	// 거래처 ID
			strParameter.push('LC_ID' in param ? param['LC_ID'] : null);					// 센터 ID
			strParameter.push('LC_ALL');													// LC_ALL
			param  = `?func=${callbackNm}&custType=${param['CUST_TYPE']}&strParameter=${strParameter}&stBtnYn=N`;
			url    = "/WMSCM011.action" + param;

			break;

		//* 로케이션
		case 'LOCATION': 
			strParameter.push('LOC_CD' 			in 	param ? param['LOC_CD'] 		: null);		// 검색코드 (로케이션)
			strParameter.push('LOC_ID' 			in 	param ? param['LOC_ID'] 		: null);		// 검색명칭 (로케이션)
			strParameter.push('WH_ID' 			in 	param ? param['WH_ID'] 			: null);		// 창고ID
			strParameter.push('RITEM_ID' 		in 	param ? param['RITEM_ID'] 		: null);		// 상품 RITEM_ID
			strParameter.push('UOM_ID' 			in 	param ? param['UOM_ID'] 		: null);		// UOM_ID
			strParameter.push('VIEW_LOT_ID' 	in 	param ? param['VIEW_LOT_ID'] 	: null);  		// 조회형식 : SUB_LOT_ID, STOCK_ID
			strParameter.push('VIEW_QTY' 		in 	param ? param['VIEW_QTY'] 		: null);		// 조회형식 : 재고수량 조회여부 (AVAILABLE_QTY)
			strParameter.push('VIEW_ONLY_LOC' 	in 	param ? param['VIEW_QTY'] 		: '');			// 조회형식 : 로케이션 정보만 조회
			param = `?func=${callbackNm}&strParameter=${strParameter}`;
			url = "/WMSCM080.action" + param;

			break;

		//* 상품코드
        case 'ITEM':	
            strParameter.push('RITEM_CD' in param ? param['RITEM_CD'] : null);		// 상품코드
            strParameter.push('RITEM_NM' in param ? param['RITEM_NM'] : null);		// 상품명
            strParameter.push('CUST_ID'  in param ? param['CUST_ID'] : null);		// 화주ID
            strParameter.push('');													// 창고ID
            strParameter.push('');													// SET상품을 보여줄 것인지 여부
            strParameter.push('Y');													// 재고유무 관계없이 표시여부
            param = `?func=${callbackNm}&strParameter=${strParameter}`;
            url = "/WMSCM091.action" + param;
            break;
		
		//* 로케이션
		case 'LOC': 
			strParameter.push('LOC_CD' in param ? param['LOC_CD'] : null);
			strParameter.push('LOC_ID' in param ? param['LOC_ID'] : null);
			param = `?func=${callbackNm}&strParameter=${strParameter}`;
			url = "/WMSCM080.action" + param;
			break;	
	}
	
    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
	asppop.focus();


	/** 팝업 종료 이벤트 (호출 부 콜백) */
	asppop.addEventListener('beforeunload', function(...args) {
		if('StandardPopUpCallBack' in viewer){
			viewer.StandardPopUpCallBack.call(viewer, callType, popupResult);
		}
		else{
			callBack();
		}
	});

}







// const CommonPopUp = (()=>{

// 	let callType = null;
	
// 	const Open = (type, param={}, callBack) =>{

// 		let strParameter = [];
// 		let url = '';
// 		let width = "800";
// 		let height = "530";
// 		let callback = null;

// 		custom_callser = this;
// 		callType = type;

// 		switch(type){

// 			//* 창고
// 			case 'WH':
// 				callback = 'WareHousePopUpEventCallBack';
// 				param = `?func=${callback}`;
// 				url = "/WMSMS040/pop.action" + param;
// 				break;
	
// 			//* 화주
// 			case 'CUST':
// 				callback = "CustPopUpEventCallBack";
// 				strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);			// 화주코드
// 				strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);			// 화주명
// 				strParameter.push('');														// 거래처 ID
// 				strParameter.push('');														// LC_ID
// 				strParameter.push('');														// LC_ALL
// 				param = `?func=${callback}&custType=12&strParameter=${strParameter}`;
// 				url = "/WMSCM011.action" + param;
// 				break;
	
// 			//* 거래처
// 			//! 거래처 코드(custType)에 따라 조회 조건이 다름
// 			case 'CUST_SHOP' : 
// 				callback = 'CustShopPopUpEventCallBack';
// 				strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);				// 검색코드 (거래처)
// 				strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);				// 검색명칭 (거래처)
// 				strParameter.push('TRANS_CUST_ID' in param ? param['TRANS_CUST_ID'] : null);	// 거래처 ID
// 				strParameter.push('LC_ID' in param ? param['LC_ID'] : null);					// 센터 ID
// 				strParameter.push('LC_ALL');													// LC_ALL
// 				param  = `?func=${callback}&custType=3&strParameter=${strParameter}&stBtnYn=N`;
// 				url    = "/WMSCM011.action" + param;
	
// 				break;
	
// 			//* 로케이션
// 			case 'LOCATION': 
// 				callback = "LocPopUpEventCallBack";
// 				strParameter.push('LOC_CD' 			in 	param ? param['LOC_CD'] 		: null);		// 검색코드 (로케이션)
// 				strParameter.push('LOC_ID' 			in 	param ? param['LOC_ID'] 		: null);		// 검색명칭 (로케이션)
// 				strParameter.push('WH_ID' 			in 	param ? param['WH_ID'] 			: null);		// 창고ID
// 				strParameter.push('RITEM_ID' 		in 	param ? param['RITEM_ID'] 		: null);		// 상품 RITEM_ID
// 				strParameter.push('UOM_ID' 			in 	param ? param['UOM_ID'] 		: null);		// UOM_ID
// 				strParameter.push('VIEW_LOT_ID' 	in 	param ? param['VIEW_LOT_ID'] 	: null);  		// 조회형식 : SUB_LOT_ID, STOCK_ID
// 				strParameter.push('VIEW_QTY' 		in 	param ? param['VIEW_QTY'] 		: null);		// 조회형식 : 재고수량 조회여부 (AVAILABLE_QTY)
// 				strParameter.push('VIEW_ONLY_LOC' 	in 	param ? param['VIEW_QTY'] 		: '');			// 조회형식 : 로케이션 정보만 조회
// 				param = `?func=${callback}&strParameter=${strParameter}`;
// 				url = "/WMSCM080.action" + param;
	
// 				break;
	
// 			//* 상품코드
// 			case 'ITEM':	
// 				callback = "CommonPopUpCallBack";
// 				strParameter.push('RITEM_CD' in param ? param['RITEM_CD'] : null);		// 상품코드
// 				strParameter.push('RITEM_NM' in param ? param['RITEM_NM'] : null);		// 상품명
// 				strParameter.push('CUST_ID' in param ? param['CUST_ID'] : null);		// 화주ID
// 				strParameter.push('');													// 창고ID
// 				strParameter.push('');													// SET상품을 보여줄 것인지 여부
// 				strParameter.push('Y');													// 재고유무 관계없이 표시여부
// 				param = `?func=${callback}&strParameter=${strParameter}`;
// 				url = "/WMSCM091.action" + param;
// 				break;
			
// 			//* 로케이션
// 			case 'LOC': 
// 				callback = "LocPopUpEventCallBack2";
// 				strParameter.push('LOC_CD' in param ? param['LOC_CD'] : null);
// 				strParameter.push('LOC_ID' in param ? param['LOC_ID'] : null);
// 				param = `?func=${callback}&strParameter=${strParameter}`;
// 				url = "/WMSCM080.action" + param;
// 				break;	
// 		}
		
// 		var asppop = cfn_openPop2(url, "fn_Popup", width, height);
// 		asppop.focus();
	
// 		asppop.addEventListener('beforeunload', function(...args) {
// 			custom_callser.CommonPopUpCallBack(callType, result);
// 		});

// 	}


// 	return {
// 		Open : Open
// 	}

// })();



/**
 * Velocity로 치환된 콤보박스 문자열 값을 파싱. 
 * $!comCode.getGridComboComCode
 * 
 * @param {*} name 	: 콤보박스 Key 값
 * @param {*} arg 	: 치환된 문자열 Object
 * @returns 
 */
function ParseComboString(name, arg){
	var rtn  = {};
	var all = [];
	var keys = [];
	var values = [];

	var comboStringList = arg.split(';');

	for(var i = 0; i < comboStringList.length; i++) {
		var comboArr = comboStringList[i].split(':');
		all.push([cfn_trim(comboArr[0]), cfn_trim(comboArr[1])]);
		keys.push(cfn_trim(comboArr[0]));
		values.push(cfn_trim(comboArr[1]));
	}
	rtn[name] = {};
	rtn[name].all = all;
	rtn[name].keys = keys;
	rtn[name].values = values;

	return {
		'all' 		: all,
		'keys'		: keys,
		'values'	: values
	}
}


/**
 * 사전정의된 콤보타입 문자열의 Key, Value 맵 구성 및 반환
 * 
 * $!comCode.getGridComboComCode
 * 
 * @param {*} category : 사전정의된 코드값
 * @returns {Map}
 */
function ParseComCodeMap(category){

	var rtn  = new Map;

	let codeString = COL_COMBO_LIST[category].value.split(';');

	for(var i = 0; i < codeString.length; i++) {
		var comboArr = codeString[i].split(':');
		rtn.set(cfn_trim(comboArr[0]), cfn_trim(comboArr[1]));
	}

	return rtn;
}