/** 
 * @typedef LogLevel
 * @property {String} INFO      : 정보성 로그 (이력)
 * @property {String} DEBUG     : 디버깅용 로그
 * @property {String} WARNING   : 경고수준 로그
 * @property {String} ERROR     : 오류 로그
 * @property {String} ETC       : 그 외
 */

/**
 * @typedef LogDataFormat
 * @property {*} LC_ID          :   센터 ID
 * @property {*} CUST_ID        :   화주 ID
 * @property {*} CUSTOM_KEY1    :   Key 1
 * @property {*} CUSTOM_KEY2    :   Key 2
 * @property {*} SERVICE        :   화면 ID
 * @property {*} FUNCTION       :   요청 URL or 기능명
 * @property {*} LOG_LEVEL      :   로그 레벨 (상수정의)
 * @property {*} LOG_TYPE       :   로그 타입 (사용자 정의)
 * @property {*} LOG_DESC       :   로그 내용
 * @property {*} ERROR_CODE     :   오류 코드
 * @property {*} ERROR_DESC     :   오류 내용
 * @property {*} REG_DT         :   등록일
 * @property {*} REG_NO         :   등록자
 * @property {*} USER_DEFINED1  :   사용자정의1 (기본값: 메뉴경로)
 * @property {*} USER_DEFINED2  :   사용자정의2
 * @property {*} USER_DEFINED3  :   사용자정의3
 * @property {*} USER_DEFINED4  :   사용자정의4
 * @property {*} USER_DEFINED5  :   사용자정의5
 */

/** @type {LogLevel} */
const LOG_LEVEL = {
    INFO      : 'INFO',
    DEBUG     : 'DEBUG',
    WARNING   : 'WARNING',
    ERROR     : 'ERROR',
    ETC       : 'ETC'
}

class LogData {

    #domain = 'WINUS';

    LC_ID;
    CUST_ID;
    CUSTOM_KEY1;
    CUSTOM_KEY2;
    SERVICE;
    FUNCTION;
    LOG_LEVEL;
    LOG_TYPE;
    LOG_DESC;
    ERROR_CODE;
    ERROR_DESC;
    REG_DT;
    REG_NO;
    USER_DEFINED1;
    USER_DEFINED2;
    USER_DEFINED3;
    USER_DEFINED4;
    USER_DEFINED5;
    
    /**
     * @param {LogDataFormat} args 
     */
    constructor(args) {
        this.LC_ID            = 'LC_ID'          in args ? args.LC_ID          : "";
        this.CUST_ID          = 'CUST_ID'        in args ? args.CUST_ID        : "";
        this.CUSTOM_KEY1      = 'CUSTOM_KEY1'    in args ? args.CUSTOM_KEY1    : "";
        this.CUSTOM_KEY2      = 'CUSTOM_KEY2'    in args ? args.CUSTOM_KEY2    : "";
        this.SERVICE          = 'SERVICE'        in args ? args.SERVICE        : window.location.href.split(`${this.#domain}/`)[1]?.replace('.action', ''),
        this.FUNCTION         = 'FUNCTION'       in args ? args.FUNCTION       : "",
        this.LOG_LEVEL        = 'LOG_LEVEL'      in args ? args.LOG_LEVEL      : LOG_LEVEL.DEBUG,
        this.LOG_TYPE         = 'LOG_TYPE'       in args ? args.LOG_TYPE       : "";
        this.LOG_DESC         = 'LOG_DESC'       in args ? args.LOG_DESC       : "";
        this.ERROR_CODE       = 'ERROR_CODE'     in args ? args.ERROR_CODE     : "";
        this.ERROR_DESC       = 'ERROR_DESC'     in args ? args.ERROR_DESC     : "";
        this.REG_DT           = 'REG_DT'         in args ? args.REG_DT         : "";
        this.REG_NO           = 'REG_NO'         in args ? args.REG_NO         : "";
        this.USER_DEFINED1    = 'USER_DEFINED1'  in args ? args.USER_DEFINED1  : typeof _fullPath === 'undefined' ? '' : _fullPath;
        this.USER_DEFINED2    = 'USER_DEFINED2'  in args ? args.USER_DEFINED2  : "";
        this.USER_DEFINED3    = 'USER_DEFINED3'  in args ? args.USER_DEFINED3  : "";
        this.USER_DEFINED4    = 'USER_DEFINED4'  in args ? args.USER_DEFINED4  : "";
        this.USER_DEFINED5    = 'USER_DEFINED5'  in args ? args.USER_DEFINED5  : "";
    }
}

const WinusLog = (()=>{

    const InsertForInfo = (logData, callback) => {
        logData.LOG_LEVEL = LOG_LEVEL.INFO;
        Insert(logData, callback);
    }

    const InsertForDebug = (logData, callback) => {
        logData.LOG_LEVEL = LOG_LEVEL.DEBUG;
        Insert(logData, callback);
    }

    const InsertForWarning = (logData, callback) => {
        logData.LOG_LEVEL = LOG_LEVEL.WARNING;
        Insert(logData, callback);
    }

    const InsertForError = (logData, callback) => {
        logData.LOG_LEVEL = LOG_LEVEL.ERROR;
        Insert(logData, callback);
    }

    const InsertForEtc = (logData, callback) => {
        logData.LOG_LEVEL = LOG_LEVEL.ETC;
        Insert(logData, callback);
    }


    /**
     * 로그 생성(적재)
     * 
     * @param {LogData} logData     : 화주 ID
     * @param {function} callback   : CallBack 함수
     */
    const Insert = function(logData, callback){

        let formData = new FormData();

        formData.append('LC_ID'          , logData.LC_ID);
        formData.append('CUST_ID'        , logData.CUST_ID);
        formData.append('CUSTOM_KEY1'    , logData.CUSTOM_KEY1);
        formData.append('CUSTOM_KEY2'    , logData.CUSTOM_KEY2);
        formData.append('SERVICE'        , logData.SERVICE);
        formData.append('FUNCTION'       , logData.FUNCTION);
        formData.append('LOG_LEVEL'      , logData.LOG_LEVEL);
        formData.append('LOG_TYPE'       , logData.LOG_TYPE);
        formData.append('LOG_DESC'       , logData.LOG_DESC);
        formData.append('ERROR_CODE'     , logData.ERROR_CODE);
        formData.append('ERROR_DESC'     , logData.ERROR_DESC);
        formData.append('REG_DT'         , logData.REG_DT);
        formData.append('REG_NO'         , logData.REG_NO);
        formData.append('USER_DEFINED1'  , logData.USER_DEFINED1);         // 화면진입 경로 (common.js)
        formData.append('USER_DEFINED2'  , logData.USER_DEFINED2);
        formData.append('USER_DEFINED3'  , logData.USER_DEFINED3);
        formData.append('USER_DEFINED4'  , logData.USER_DEFINED4);
        formData.append('USER_DEFINED5'  , logData.USER_DEFINED5);

        return new Promise((resolve, reject)=>{
            jQuery.ajax({
                url : "/TMSYS140/insertE2.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(data){
                    resolve();
                },
                error: function(request, status, error){
                    alert(`관리자에게 문의바랍니다. (${request.status})`);
                    reject();
                }
            });
        });
    }

    return {
        Insert : Insert,
        InsertForInfo : InsertForInfo,
        InsertForDebug : InsertForDebug,
        InsertForWarning : InsertForWarning,
        InsertForError : InsertForError,
        InsertForEtc : InsertForEtc
    }

})();