export let OpenMallInfo = (function(){

    /**
     * 오픈몰 계정정보 조회 (세션저장 : 'OPENMALL_INFO')
     * 오픈몰코드 참조 (인터페이스DB : OPENMALL_API_AUTH_MASTER)
     * 
     * @param {*} custId      : 화주 ID
     * @param {*} openMallCd  : 오픈몰 코드
     * @param {*} callback    : CallBack 함수
     */
    let GetAuthInfo = function(custId, openMallCd, callback){

        let formData = new FormData();

        formData.append("LC_ID", jQuery(`[name="SVC_INFO"]`).val());        // [UI Header]
        formData.append("CUST_ID", custId);
        formData.append("OPENMALL_CD", openMallCd);

        return new Promise((resolve, reject)=>{
            jQuery.ajax({
                url : "/WMSIF706/getOpenMallInfo.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
    
                    sessionStorage.setItem('OPENMALL_INFO', Object.toJSON(data));
    
                    if(callback != undefined && callback != null){
                        callback();
                    }

                    resolve();
                },
                error: function(request, status, error){
                    alert(`관리자에게 문의바랍니다. (${request.status})`);
                    reject();
                }
            });
        });
    }


    /**
     * 세션에 저장된 특정 화주의 오픈몰 정보를 가져온다.
     * 
     * @param {*} custId    : 화주ID
     * @returns 
     */
    let GetInfoByCustId = function(custId){
        return sessionStorage.hasOwnProperty('OPENMALL_INFO') 
                    ? JSON.parse(sessionStorage['OPENMALL_INFO'])['DS_APIAUTH'].filter((dt) => {return (dt.CUST_ID == custId)})
                    : null;
    }


    /**
     * 세션에 저장된 특정 화주의 오픈몰 정보를 가져온다.
     * 
     * @param {*} custId    : 화주ID
     * @returns 
     */
    let GetInfo = function(custId, openMallCd){
        return sessionStorage.hasOwnProperty('OPENMALL_INFO') 
                    ? JSON.parse(sessionStorage['OPENMALL_INFO'])['DS_APIAUTH'].filter((dt) => {return (dt.CUST_ID == custId) && (dt.OPENMALL_CD == openMallCd)})
                    : null;
    }


    /**
     * 세션에 저장된 오픈몰 정보에서 특정 키의 값을 가져온다.
     * 
     * @param {*} custId        : 화주ID
     * @param {*} selectedId    : 선택된 오픈몰계정 (MASTER_SEQ)
     * @param {*} key           : 조회 대상 KEY값
     * @returns 
     */
    let GetValueOfKey = function(custId, selectedId ,key){

        let info = new Array();
    
        if(!sessionStorage.hasOwnProperty('OPENMALL_INFO')) throw null; 

        info = JSON.parse(sessionStorage['OPENMALL_INFO'])['DS_APIAUTH']
                        .filter((dt) => {
                            return (dt.CUST_ID == custId && dt.MASTER_SEQ == selectedId)
                        });

        if(info.length == 0){
            throw null;
        }

        else{
            return info[0][key];
        }
    }


    /**
     * 세션에 저장된 오픈몰 정보의 특정 키-값 변경
     * 
     * @param {String} custId       : 화주ID
     * @param {Array} infos         : 화주별 세선졍보
     * @param {String} masterSeq    : 오픈몰 마스터 SEQ
     * @param {String} key          : 변경할 JSON KEY
     * @param {String} value        : 변경할 JSON VALUE
     */
    const SetValueOfKey = function(custId, infos, masterSeq, key, value){

        let openMall = infos ?? 
                        (sessionStorage.hasOwnProperty('OPENMALL_INFO') 
                            ? JSON.parse(sessionStorage['OPENMALL_INFO'])['DS_APIAUTH']
                                .filter((dt) => {return (dt.CUST_ID == custId) && (dt.MASTER_SEQ == masterSeq)})
                            : null);

        openMall[0][key] = value;

        sessionStorage.setItem('OPENMALL_INFO', JSON.stringify({'DS_APIAUTH' : openMall}));

    }




    return {
        GetAuthInfo : GetAuthInfo,
        GetValueOfKey : GetValueOfKey,
        GetInfoByCustId : GetInfoByCustId,
        GetInfo : GetInfo,
        SetValueOfKey : SetValueOfKey
    }

})();