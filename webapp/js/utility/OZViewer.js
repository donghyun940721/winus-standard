const PRINT_MODE = {
    DEFAULT        : true,                  // 기본값
    SHOW_PROGRESS  : 'showprogress',
    SILENT         : 'silent'
};


/**
 * @typedef PrintOptions
 * 
 * @property {string} mode
 * @property {boolean} once
 * @property {number} copies 
 * @property {boolean} lockopt
 * @property {size} size 
 */

/**
 * @typedef OZReportArgument     : OZ Report 정보
 * 
 * @property {string} title         : 팝업창 명칭
 * @property {string} fileName      : 파일 명칭 (export 시 파일명)
 * @property {string} reportName    : OZ Report (*.ozr) 파일 확장자 명시 O
 * @property {string} odiName       : OZ Report (*.odi) 파일 확장자 명시 X
 * @property {string} viewerMode    : 'preview': 미리보기 창, 'print': 프린트 미리보기 창 호출, 'export':PDF 추출 
 * @property {boolean} printOnce    : OZ Viewer 내 출력 1회 제한 (출력 후 비활성화)
 */


class OZReport {

    urlParams = new URLSearchParams();

    get URL() { return this.url ; }
    get URLString() { return this.url + '?' + this.GetQueryString(); }

    /**
     * 
     * @param {OZReportArgument} args 
     */
    constructor(args){
        this.url            = args.url ?? '/common/OZreport.action';
        this.title          = args.title;
        this.fileName       = args.fileName;
        this.reportName     = args.reportName;
        this.odiName        = args.odiName;
        this.viewerMode     = args.viewerMode;
        this.printOnce      = args.printOnce;

        this.#SetReportInfo();
    }


    #SetReportInfo() {
        if(this.title       != undefined)   this.urlParams.set('title'        , this.title);
        if(this.fileName    != undefined)   this.urlParams.set('fileName'     , this.fileName);
        if(this.reportName  != undefined)   this.urlParams.set('reportName'   , this.reportName);
        if(this.odiName     != undefined)   this.urlParams.set('odiName'      , this.odiName);
        if(this.viewerMode  != undefined)   this.urlParams.set('viewerMode'   , this.viewerMode);
        if(this.printOnce   != undefined)   this.urlParams.set('printOnce'    , this.printOnce);
    }


    /**
     * ODI 인자 설정
     * 
     * @param {Object} odiParams 
     */
    SetOdiParam(odiParams){
        this.urlParams.set('odiParams', JSON.stringify(odiParams));
    }


    /**
     * OZR 인자 설정
     * 
     * @param {Object} ozrParams 
     */
    SetOzrParam(ozrParams){
        this.urlParams.set('ozrParams', JSON.stringify(ozrParams));
    }


    /**
     * URL 인자 문자열 획득
     * 
     * @returns 
     */
    GetQueryString(){
        return this.urlParams.toString();
    }

}

/**
 * FormData에서 리포트(*.jsp)를 직접호출 하는 클래스
 * 
 * @class
 */
class OZReportForm extends OZReport{

    get URL() { return this.url ; }
    get URLParams() { return this.urlParams;}

    constructor(args){
        super({
            'url'        : '/jsp/OZReport.jsp',
            'title'      : args.title,
            'fileName'   : args.fileName,
            'reportName' : args.reportName,
            'odiName'    : args.odiName,
        });
    }

    AppendParamToForm(formData){
        for (const [key, value] of this.urlParams.entries()){

            let frmValue = document.createElement("input");
    
            frmValue.setAttribute("name", key);
            frmValue.setAttribute("value", value);
    
            formData.appendChild(frmValue);
    
            console.log(`name: ${key} || value: ${value}`);
        }

        return formData;
    }

}