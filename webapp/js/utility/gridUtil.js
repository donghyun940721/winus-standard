const GridUtility = (function(){

    /**
     * Grid Format 바인딩
     * 
     * @param {Number} 		colIdx 
     * @param {function} 	formatter 
     */
    const AddFormatter = function(spdList, colIdx, formatter, parse){

        var cell = spdList.getCell(-1, colIdx, GC.Spread.Sheets.SheetArea.viewport);

        var customFormatterTest = {};
        customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;
        customFormatterTest.format = formatter;
        customFormatterTest.parse  = parse;
        
        cell.formatter(customFormatterTest);
    }


    /**
     * 
     * @param {*} spdSheet 
     * @param {Array} menu 
     */
    const SetContextMenu = function (spdSheet, contextMenu, openMenuCallback = {}) {
        
        const contextMenuInfo = contextMenu.map(dt => Object.assign({}, dt));   //! Deep Copy
        let menuNameList = new Array();
        let menuMappingInfo = new Map();
        let commandManager = spdSheet.commandManager();

        spdSheet.options.allowContextMenu = true;

        /** Init */
        function ContextMenu(){}
        ContextMenu.prototype = new GC.Spread.Sheets.ContextMenu.ContextMenu(spdSheet);
        ContextMenu.prototype.onOpenMenu = openMenuCallback;
        spdSheet.contextMenu = new ContextMenu();
        spdSheet.contextMenu.menuData.clear();


        /** 주메뉴 정보 처리 맵핑정보 생성 */
        for(let info of contextMenuInfo){
                    
            menuMappingInfo.set(info.name, { 'event': info.action, 'type': info.type ?? 'text', depth: '0' } );
            if(info.visible) menuNameList.push(info.name);

            // delete info.action;

            /** 소메뉴 정보 처리 맵핑정보 생성 */
            if('subMenu' in info){
                for(let subMenu of info['subMenu']){
                    
                    menuMappingInfo.set(subMenu.name, { 'event': subMenu.action, 'type': subMenu.type ?? 'text', depth: '1' });
                    if(subMenu.visible) menuNameList.push(subMenu.name);
                    
                    // delete subMenu.action;
                }
            }
        }

        
        /** Item Binding */
        for(const dt of contextMenuInfo){
            if(dt.visible){
                if('subMenu' in dt){
                    dt.subMenu = dt.subMenu.filter(dt => dt.visible);
                }

                spdSheet.contextMenu.menuData.push(dt);
            }
        }


        /** Event Binding  */
        let callback = function(args, target, setShortcutKey){
            let eventFnc = menuMappingInfo.get(target.cmd)['event'];
            if(eventFnc != undefined){
                eventFnc(args, target);
            }
        }

        for(let menuNm of menuNameList){
            commandManager.register(menuNm, callback, null, false, false, false, false);
        }

        /** 메뉴별 서식지정  */
        function CustomMenuView(){}
        CustomMenuView.prototype = new GC.Spread.Sheets.ContextMenu.MenuView();
        CustomMenuView.prototype.createMenuItemElement = function (menuItemData) {
            var self = this;
            let type = menuMappingInfo.get(menuItemData.name)['type'];
            let depth = menuMappingInfo.get(menuItemData.name)['depth'];

            var menuItemView = GC.Spread.Sheets.ContextMenu.MenuView.prototype.createMenuItemElement.call(self, menuItemData);

            switch(type){

                case 'header' :
                    menuItemView[0].style.backgroundColor = '#d5dce6';
                    menuItemView[0].style.textTransform = 'none';
                    break;

                case 'childText' :
                    menuItemView[0].style.marginLeft = '1em';
                    break;

                case 'text' : 
                    break;
            }
            return menuItemView;
        }

        spdSheet.contextMenu.menuView = new CustomMenuView();
    }

    /**
     * Grid에 반영된 모든 행 데이터를 반환.
     * 
     */
    const GetAllRowData = (spdList) => {

        let rtn = new Array();
        
        for(let rowIdx=0; rowIdx < spdList.getRowCount(); rowIdx++){
            rtn.push(GetRowData(spdList, rowIdx));
        }

        return rtn;
    }
    

    /**
     * 특정 행의 데이터를 모두 반환한다.
     * 
     * @param {*} rowIdx 
     * @returns : {'colKey' : colVal ...}
     */
    const GetRowData = (spdList, rowIdx) => {
        return spdList.getDataItem(rowIdx);
    }

    return {
        AddFormatter,
        SetContextMenu,
        GetAllRowData,
        GetRowData,
    }
})();
