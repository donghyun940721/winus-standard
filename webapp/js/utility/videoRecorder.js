class VideoRecorder {

    /* Instance Variable */
    #created            = false;
    #videoDiv           = null;
    #streamVideo        = null;
    #videoRecorder      = null;
    #recordedVideoURL   = null;
    #videoBlob          = null;
    #videoData          = new Array(); 
    #canvas             = null;
    #animationFrameId 	= null;

    /** Options */
    #maxWidth           = null;
    #maxHeight          = null;
    #audioRecord        = false;
    #showCaptions 		= true; // 자막 여부 

    /** Custom Event */
    #recorderSaveEventCallBack = null;
    #recorderStopEventCallBack = null;


    /** getter setter */

    get created()           {   return this.#created            }
    get videoDiv()          {   return this.#videoDiv           }
    get streamVideo()       {   return this.#streamVideo        }
    get videoRecorder()     {   return this.#videoRecorder      }
    get recordedVideoURL()  {   return this.#recordedVideoURL   }
    get videoBlob()         {   return this.#videoBlob          }
    get videoData()         {   return this.#videoData          }
    get maxWidth()          {   return this.#maxWidth           }
    get maxHeight()         {   return this.#maxHeight          }
    get audioRecord()       {   return this.#audioRecord        }

    set videoBlob(blob)     {   this.#videoBlob = blob; }

    set recorderSaveEventCallBack(event)    {   this.#recorderSaveEventCallBack = event;    }
    set recorderStopEventCallBack(event)    {   this.#recorderStopEventCallBack = event;    }

    constructor(args){
        if(args != undefined){
            this.#videoDiv = "videoDiv" in args ? args.videoDiv : "";
            this.#maxWidth = "maxWidth" in args ? args.maxWidth : "1920";
            this.#maxHeight = "maxHeight" in args ? args.maxHeight : "1080";
            this.#audioRecord = "audioRecord" in args ? args.audioRecord : false;
            this.#canvas = "canvas" in args ? args.canvas : "";
        }
    }

    async StartRec(){
        let vm = this;
        if(!navigator.mediaDevices ||  !navigator.mediaDevices.getUserMedia){
            alert("지원불가한 서비스입니다.");
        } else{
            try{
                let stream = await navigator.mediaDevices.getUserMedia({
                    audio: this.#audioRecord,    video: {width: {max : this.#maxWidth},  height: {max : this.#maxHeight} },
                });
    
                this.#streamVideo = stream
                var cameraView = document.getElementById(this.#videoDiv);
                cameraView.srcObject = stream;
                if(cameraView.srcObject == null){
                    return;
                }

                await cameraView.play();
    
                this.#videoData.clear();
                
				this.initShowBtn();
                this.CreateCaption();
                
                this.#videoRecorder = new MediaRecorder(this.#streamVideo, { mimeType: "video/webm;codecs=vp9" });

		        this.#videoRecorder.ondataavailable = event => {
		            if (event.data?.size > 0)
		                vm.videoData.push(event.data);
		        }
                
                await this.#videoRecorder.start();
    
            } catch (e){
                console.log(e);
                this.StopRec();
            }
        }
    }
    
    initShowBtn() {
        this.#showCaptions = true;
        jQuery("#showBtn").val("숨기기");
	}

    drawContext(context, canvasElement) {
        const now = new Date().toLocaleString();
        const transCustName = document.getElementById('vrSrchTransCustNm').innerText;
        const transCustId = document.getElementById('vrSrchTransCustId').innerText;
        const pickingTotalCnt = document.getElementById('pickingTotalCnt').innerText;

        if (this.#showCaptions) {
            context.strokeStyle = '#000';
            context.lineWidth = 3;
            context.font = 'bold 26px Arial';
            context.fillStyle = 'white';

            const itemList = [];

            for (let i = 0; i < spdList.getRowCount(); i++) {
                const itemName = spdList.getValue(i, SPDCOL_ITEM_NAME);
                const itemQty = spdList.getValue(i, SPDCOL_ITEM_QTY);
                const itemCode = spdList.getValue(i, SPDCOL_ITEM_CD);
                const itemTemp = spdList.getValue(i, SPDCOL_ITEM_TMP);

                const itemString = `${itemName} ${itemQty}개`;
                itemList.push(itemString);
            }

            context.strokeText(`총 수량: ${pickingTotalCnt}개`, canvasElement.width - 190, 50); // 테두리
            context.fillText(`총 수량: ${pickingTotalCnt}개`, canvasElement.width - 190, 50);
        
            context.strokeText(now, 50, 50);
            context.fillText(now, 50, 50);
        
            context.strokeText(`${transCustName}[${transCustId}]`, 50, 90);
            context.fillText(`${transCustName}[${transCustId}]`, 50, 90);
            
            context.font = 'bold 22px Arial'; 
            for (let i = 0; i < itemList.length; i++) {
                context.strokeText(itemList[i], 50, 130 + i * 30);
                context.fillText(itemList[i], 50, 130 + i * 30);
            }
        }
    }

    CreateCaption() {
        if (this.#canvas) {
            const cameraView = document.getElementById(this.#videoDiv);
            const canvasElement = document.getElementById(this.#canvas);
            const context = canvasElement.getContext('2d');
            canvasElement.width = 1000;
            canvasElement.height = 750;

            const draw = () => {
				context.clearRect(0, 0, canvasElement.width, canvasElement.height);
                context.drawImage(cameraView, 0, 0, canvasElement.width, canvasElement.height);
                this.drawContext(context, canvasElement);
                this.#animationFrameId = requestAnimationFrame(draw);
            };

            draw();
            
            const canvasStream = canvasElement.captureStream(30);
            this.#streamVideo = canvasStream;
        }
    }

    ClearCaption() {
        if (this.#canvas) {
            const canvasElement = document.getElementById(this.#canvas);
            const context = canvasElement.getContext('2d');
            context.clearRect(0, 0, canvasElement.width, canvasElement.height);
            
            if (this.#animationFrameId) {
		        cancelAnimationFrame(this.#animationFrameId);
		        this.#animationFrameId = null;
		    }
        }
    }
   	
    ToggleCaption() {
        this.#showCaptions = !this.#showCaptions;
        this.ClearCaption();
        this.CreateCaption();
    }

    StopRec(){
        if(this.#videoRecorder){
            this.#videoRecorder.ondataavailable = null;
            this.#videoRecorder.onstop = null;
        }
        if(this.#streamVideo == null){
            return;
        }
    
        this.#streamVideo.getVideoTracks()[0].stop();
    
        var cameraView = document.getElementById(this.#videoDiv);
        cameraView.srcObject = null;

		this.ClearCaption();
		
        this.RecorderStopEvent();
    }
    
    SaveRec(paramObject){ 
        let vm = this;
        if(!this.#videoRecorder){
            return;
        }
        this.#videoRecorder.ondataavailable = event =>{
            if(event.data?.size > 0)
            vm.videoData.push(event.data);
        }
    
        this.#videoRecorder.onstop = async() =>{
            vm.videoBlob = new Blob(vm.videoData,{type:"video/mp4"});
            var newBlob;
            await vm.GetSeekableBlob(vm.videoBlob).then((seekableBlob)=>{
                newBlob = seekableBlob;
            });
            let fileName = new Date().getFormattedString('YYYYMMDDHHmmss') + ".mp4";
            const file= new File([newBlob?newBlob:vm.videoBlob], fileName);
            let param = new FormData();
            param.append("fname", fileName);
            param.append("txtFile",file);
            
            vm.RecorderSaveEvent(fileName,file,paramObject);
        }
    
        this.#videoRecorder.stop();
    }

    RecorderSaveEvent(fileName, file, paramObject){
        if(this.#recorderSaveEventCallBack != null){
            this.#recorderSaveEventCallBack.call(this, fileName, file, paramObject);
        }
    }

    RecorderStopEvent(){
        if(this.#recorderStopEventCallBack != null){
            this.#recorderStopEventCallBack.call(this);
        }
    }

    GetSeekableBlob(inputBlob) {
        return new Promise((resolve, reject)=>{
            // EBML.js copyrights goes to: https://github.com/legokichi/ts-ebml
            if(typeof EBML === 'undefined') {
                reject();
                //throw new Error('Please link: https://www.webrtc- experiment.com/EBML.js');
            }
    
            var reader = new EBML.Reader();
            var decoder = new EBML.Decoder();
            var tools = EBML.tools;
    
            reader.drop_default_duration = false;
    
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                var ebmlElms = decoder.decode(this.result);
                ebmlElms.forEach(function (element) {
                    if (element.type !== 'unknown') {
                        reader.read(element);
                    }
                });
                reader.stop();
                var refinedMetadataBuf = tools.makeMetadataSeekable(reader.metadatas, reader.duration, reader.cues);
                var body = this.result.slice(reader.metadataSize);
    
                var newBlob = new Blob([refinedMetadataBuf, body], {
                    type: 'video/mp4'
                });
                resolve(newBlob);
            };
    
            fileReader.readAsArrayBuffer(inputBlob);
        });
    }

}