export const SessionKey = {
    CUST_INFO   : 'CUST_INFO',
    CUST_ID     : 'CUST_ID',
    CUST_NM     : 'CUST_NM',
    CUST_CD     : 'CUST_CD'
}


export let SessionUtil = (function(){


    /**
     * 세션 스토리지에 key, value 형태로 저장
     * 
     * @param {*} sessionKey 
     * @param {*} value 
     */
    let Input = function(sessionKey, value){
        sessionStorage.setItem(sessionKey, value);
    }


    /**
     * 세션 스토리지에서 해당 key값에 해당하는 값 획득
     * 
     * @param {*} sessionKey 
     * @returns 
     */
    let GetValue = function(sessionKey){
        return sessionStorage.hasOwnProperty(sessionKey) ? sessionStorage[sessionKey] : null;
    }


    /**
     * 세션 스토리지의 json형태로 저장된 값의 특정 값을 추출 및 획득.
     * 
     * - 1 Depth Json 한정
     * 
     * @param {*} sessionKey    : 세션 키
     * @param {*} jsonKey       : JSON 키
     * @returns 
     */
    let GetValueOfJson = function(sessionKey, jsonKey){

        return sessionStorage.hasOwnProperty(sessionKey)
                    ? JSON.parse(sessionStorage.hasOwnProperty(sessionKey))[jsonKey]
                    : null;
    }


    return {
        Insert : Input,
        GetValue : GetValue,
        GetValueOfJson : GetValueOfJson
    }

})();



export let LocalStorageUtil = (function(){

    let Input = function(key, value){
        localStorage.setItem(key, value);
    }


    let GetValue = function(key){
        return localStorage.hasOwnProperty(key) ? localStorage.getItem(key) : null;
    }

    
    let Remove = function(key){
        localStorage.removeItem(key);
    }


    return {
        Input : Input,
        GetValue : GetValue,
        Remove : Remove
    }

})();
