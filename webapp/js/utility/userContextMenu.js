class UserSetting {

    #USER_STORAGE_KEY = 'WINUS_GRID_OPT';

    #userId = '';
    #lcId = '';
    #gridId = '';
    #defaultMenuInfo = '';
    #headerGroupInfo = new Map();

    #contextMenuSaveCallBack = null;
    set ContextMenuSaveCallBack(func) { this.#contextMenuSaveCallBack = func; }

    constructor(userId, lcId, gridId){
        this.#userId = userId;
        this.#lcId   = lcId;
        this.#gridId = gridId;
    }

    /**
     * ContextMenu 설정 팝업 초기구성
     * 
     * @file setMenuList.vm
     * @param {Array<Object>}   contextMenuInfo 
     */
    InitContextMenu = (contextMenuInfo, target) => {

        /** @type {Array<Object>} */
        this.#defaultMenuInfo = _.cloneDeep(contextMenuInfo);

        // Header Group 정보 구성
        for(let menu of contextMenuInfo){
            if(menu.type == 'header'){
                this.#headerGroupInfo.set(menu.name ?? menu.command, menu.group);
            }

            if('subMenu' in menu){
                for(let subMenu of menu.subMenu){
                    if(subMenu.type == 'header'){
                        this.#headerGroupInfo.set(subMenu.name ?? subMenu.command, subMenu.group);
                    }
                }
            }
        }
    }

    /**
     * 기본 메뉴 정보와 Storage에 저장된 Visible 정보를 병합
     * 
     * @param {Array<Object>} defaultMenuInfo 
     * @returns 
     */
    MergeStoredMenu(defaultMenuInfo, uncheckedList){

        let baseMenu = _.cloneDeep(defaultMenuInfo);
        const storageMenu = uncheckedList ?? (this.IsStored() ? this.GetStoredVisibleInfo() : {});

        // Early Return
        if(_.isEmpty(storageMenu)) return baseMenu;

        for(let menu of baseMenu){
            menu.visible = storageMenu[menu.name ?? menu.command] ?? true;

            if('subMenu' in menu){
                for(let subMenu of menu.subMenu){
                    subMenu.visible = storageMenu[subMenu.name ?? subMenu.command] ?? true;
                }
            }
        }

        return baseMenu;
    }


    /**
     * 저장. 브라우저 Local Storage에 반영
     * 
     * @param {*} e 
     */
    #SaveClickEventCallback(e){
        
        /** @type {Array<HTMLElement>} 미사용 Context Item */
        const $ItemList = Array.from(document.querySelectorAll('.userset_usedItem'));
        let uncheckedList = new Object();
        let baseMenuInfo = _.cloneDeep(this.#defaultMenuInfo);     //! Deep Copy

        /** Set Visible Toggle */
        for(let element of $ItemList){
            const { index, command, parentname } = element.dataset;
            const [ menuIdx, subMenuIdx ] = index.split('-');
            
            this.#FindMenuInfo(baseMenuInfo, menuIdx, subMenuIdx).visible = element.checked ?? false;

            if(!element.checked){
                uncheckedList[command] = element.checked;
            }
        }

        /** Set Visible Toggle for 'header' : All Item Hidden */
        if(!_.isEmpty(uncheckedList)){
            const uncheckedMenu = Object.keys(uncheckedList);

            for (let [header, menuList] of this.#headerGroupInfo.entries()) {

                if(!menuList) continue;

                const  { index } = document.querySelector(`#${header}`).dataset;
                const [ menuIdx, subMenuIdx ] = index.split('-');
                const isHidden = menuList.every(menu => uncheckedMenu.includes(menu));

                // 헤더 하위 목록이 모두 visible(false)인 경우, 헤더 표기 
                this.#FindMenuInfo(baseMenuInfo, menuIdx, subMenuIdx).visible = !isHidden;

                if(isHidden){
                    uncheckedList[header] = false;      // checked
                    console.log(`header : ${header}, ${isHidden}`);
                }
            }
        } 

        /** Set Local Storage */
        let winusOptions    = JSON.parse(localStorage.getItem(`${this.#USER_STORAGE_KEY}`)) ?? {};

        /** Init */
        winusOptions[this.#userId] = winusOptions[this.#userId] ?? {}       // User Level
        winusOptions[this.#userId][this.#lcId] = winusOptions[this.#userId][this.#lcId] ?? {}   // LC(Center) Level 
        winusOptions[this.#userId][this.#lcId][this.#gridId] = winusOptions[this.#userId][this.#lcId][this.#gridId] ?? {} // Grid Level

        // Assign
        winusOptions[this.#userId][this.#lcId][this.#gridId] = uncheckedList;

        localStorage.setItem(`${this.#USER_STORAGE_KEY}` , JSON.stringify(winusOptions));

        this.#contextMenuSaveCallBack(baseMenuInfo);        // return

        this.ClosePopUp();

    }


    /**
     * 초기화 버튼 이벤트 콜백
     * 
     * @param {*} e 
     */
    #InitClickEventCallback(e) {
        if(!this.IsStored()) return ;
        
        let winusOptions = JSON.parse(localStorage.getItem(`${this.#USER_STORAGE_KEY}`))
        winusOptions[this.#userId][this.#lcId][this.#gridId] = {};
        localStorage.setItem(`${this.#USER_STORAGE_KEY}` , JSON.stringify(winusOptions));

        // localStorage.clear(`${this.#USER_STORAGE_KEY}`);
        this.#CreateList(this.#defaultMenuInfo);
        alert('설정 정보가 초기화 되었습니다.');
    }


    /**
     * CheckBox 선택 이벤트
     * 
     * @param {*} e 
     */
    CheckedEventCallback(e){

        const menuId = e.currentTarget.dataset.command;
        const isChecked = e.currentTarget.checked;
        let groupMenuId = new Array();

        // 부모(Header)를 선택/해제하면 자식 값을 모두 선택/해제
        if(this.#headerGroupInfo.has(menuId)){
            groupMenuId = this.#headerGroupInfo.get(menuId);

            if(_.isEmpty(groupMenuId)) return ;

            groupMenuId.forEach( id => {
                document.querySelector(`#${id}`).checked = isChecked;
            });
        }
    }


    #KeyinClickEventCallback(e){
        document.getElementById('keyin_panel').classList.toggle('act');
    }


    #ImportClickEventCallback(e){
        try {

            const inputValue = document.getElementById('txtUserKeyin').value;
            const importMenuInfo = JSON.parse(this.#DeCompressString(inputValue));
    
            // Import한 자료를 기반으로 List 재구성
            this.#CreateList(this.MergeStoredMenu(this.#defaultMenuInfo, importMenuInfo));

        } catch (error) {

            alert(error);
            throw error;

        }
    }


    #ExportClickEventCallback(e){
        /** @type {Array<HTMLElement>} 미사용 Context Item */
        const $ItemList = Array.from(document.querySelectorAll('.userset_usedItem'));
        let uncheckedList = new Object();

        /** Set Visible Toggle */
        for(let element of $ItemList){
            const { command } = element.dataset;

            if(!element.checked){
                uncheckedList[command] = element.checked;
            }
        }

        /** Set Visible Toggle for 'header' : All Item Hidden */
        if(!_.isEmpty(uncheckedList)){
            const uncheckedMenu = Object.keys(uncheckedList);

            for (let [header, menuList] of this.#headerGroupInfo.entries()) {

                if(!menuList) continue;
                const isHidden = menuList.every(menu => uncheckedMenu.includes(menu));

                if(isHidden){
                    uncheckedList[header] = false;      // checked
                }
            }
        }

        const result = this.#CompressString(JSON.stringify(uncheckedList));
        window.navigator.clipboard.writeText(result).then(() => {
            alert("Key 클립보드 복사완료.");
        });
        
    }


    /**
     * 항목 선택에 대한 이벤트 발생. 하위 메뉴 DropDown
     * 
     * @param {event} e 
     */
    DropDownEventCallback(e){
        const GROUP_LIST_PREIFX = 'group-list-';
        const index = e.currentTarget.dataset.index;

        // TODO :: Display 조건 바꿔야함. (3000px 이상인 경우 짤림.)
        document.getElementById(`${GROUP_LIST_PREIFX}${index}`).style.maxHeight = e.currentTarget.checked ? '3000px' : '0px';
    }
    

    OpenPopUp () {
        /**
         * LocalStorage에 저장된 값이 
         * 있다면, Visible 속성에 대해서만 Merge
         * 없는 경우, 기본값으로만 반영
         */
        /** HTML 생성 */
        this.#CreateList(this.MergeStoredMenu(this.#defaultMenuInfo));

        jQuery('#popSetContextMenu').bPopup({
            zIndex: 999,
            escClose: 0,
            opacity: 0.6,
            positionStyle: 'fixed',
            transition: 'slideDown',
            speed: 400,
            // onClose: this.ClosePopUp
        });

        // Remove & Add Event
        jQuery("#btnUserOpionsKeyToggle").off().on("click", (e) => { this.#KeyinClickEventCallback(e) });
        jQuery("#btnUserOpionsKeyimport").off().on("click", (e) => { this.#ImportClickEventCallback(e) });
        jQuery("#btnUserOpionsExport").off().on("click", (e) => { this.#ExportClickEventCallback(e) });
        jQuery("#btnUserOptionsInit").off().on("click", (e) => { this.#InitClickEventCallback(e) });
        jQuery("#btnUserOptionSave").off().on("click", (e) => { this.#SaveClickEventCallback(e) });
    }


    ClosePopUp () {
        jQuery('#popSetContextMenu').bPopup().close();
    }


//#region   :: List Utility

    #CreateList(contextMenuInfo) {
        const $rootList = document.getElementById('rootList');
        $rootList.innerHTML = '';       // Clear

        for(let i=0; i<contextMenuInfo.length; i++){
            $rootList.innerHTML += this.#CreateGroup1((contextMenuInfo[i]), i);

            if('subMenu' in contextMenuInfo[i]){

                const $subList = document.getElementById(`group-list-${i}`);
                const parentName    = contextMenuInfo[i]['name'];
                const subMenu       = contextMenuInfo[i]['subMenu'];

                for(let j=0; j<subMenu.length; j++){
                    $subList.innerHTML += this.#CreateGroup2((subMenu[j]), parentName, i, j);
                }
            }
        }

        /** Add Event Listener */
        Array.from(document.querySelectorAll(".userset_contextItem"))
                .map(dt => dt.addEventListener('click', this.DropDownEventCallback));
        Array.from(document.querySelectorAll(".userset_usedItem"))
                .map(dt => dt.addEventListener('change', (e) => this.CheckedEventCallback.call(this, e)));
    }


    #CreateGroup1 = (menuInfo, index) => {
        return `<div style="display: flex; width: inherit; background-color: #4B5C76; padding-left: 2rem">
                    <input 
                        type="checkbox" 
                        class="userset_usedItem" 
                        id="${menuInfo.name ?? menuInfo.command}"
                        data-index="${index}" 
                        data-command="${menuInfo.name ?? menuInfo.command}" 
                        ${menuInfo.visible ? 'checked' : ''}
                    />
                    <input 
                        type="checkbox" 
                        class="userset_contextItem" 
                        id="group-${index}" 
                        data-index="${index}"  
                        hidden
                    />
                    <label for="group-${index}" 
                        style=" width:100%; 
                                font-weight: ${menuInfo.type === 'header' ? '800' : '0' }">
                        ${'subMenu' in menuInfo ? '<span class="fa fa-angle-right"></span>' : '' }
                        ${menuInfo.text}
                    </label>
                </div>
                <ul class="group-list" id="group-list-${index}">`;
    }


    #CreateGroup2 = (menuInfo, parentName, index1, index2) => {
        return `<div style="display: flex; width: inherit; padding-left:4rem; ${menuInfo.type === 'header' ? 'background-color: #aabacf;' : ''} ">
                    <input 
                        type="checkbox" 
                        class="userset_usedItem" 
                        id="${menuInfo.name ?? menuInfo.command}"
                        data-index="${index1}-${index2}" 
                        data-command="${menuInfo.name ?? menuInfo.command}" 
                        data-parentname="${parentName}"
                        ${menuInfo.visible ? 'checked' : ''}
                    />
                    <input 
                        type="checkbox" 
                        class="userset_contextItem" 
                        id="group-${index1}-${index2}" 
                        data-index="${index1}-${index2}"
                        hidden
                    />
                    <label for="group-${index1}-${index2}" 
                        style=" width:100%;
                                font-weight: ${menuInfo.type === 'header' ? '800' : '0' };
                                ${menuInfo.type === 'header' ? 'background-color: #aabacf;' : ''}">
                        ${menuInfo.text}
                    </label>
                </div>
                <ul class="sub-group-list" id="group-list-${index1}-${index2}">`;
    }


    /**
     * 인덱싱 값으로 Context Menu Data의 원소 탐색
     * 
     * @param {*} baseMenuInfo 
     * @param {*} menuIdx 
     * @param {*} subMenuIdx 
     * @returns 
     */
    #FindMenuInfo(baseMenuInfo, menuIdx, subMenuIdx){
        return subMenuIdx ? baseMenuInfo.at(menuIdx).subMenu.at(subMenuIdx)
                            : baseMenuInfo.at(menuIdx);
    }


//#endregion


//#region   :: Storage

    IsStored() {

        /** @type { Object } */
        let winusOptions    = JSON.parse(localStorage.getItem(`${this.#USER_STORAGE_KEY}`)) ?? {};

        return  winusOptions.hasOwnProperty(this.#userId) 
                    && winusOptions[this.#userId].hasOwnProperty(this.#lcId) 
                    && winusOptions[this.#userId][this.#lcId].hasOwnProperty(this.#gridId);
    }

    GetStoredVisibleInfo() {

        /** @type { Object } */
        let winusOptions    = JSON.parse(localStorage.getItem(`${this.#USER_STORAGE_KEY}`));

        return winusOptions[this.#userId][this.#lcId][this.#gridId];
    }

//#endregion


    #CompressString(string) {

        // 문자열을 Uint8Array로 변환
        const encoder = new TextEncoder();
        const uint8Array = encoder.encode(string);

        // Uint8Array를 base64로 인코딩하여 문자열로 변환
        return btoa(String.fromCharCode.apply(null, uint8Array));
    }


    #DeCompressString(string){

        // base64 디코딩
        const uint8Array = new Uint8Array(atob(string).split('').map(char => char.charCodeAt(0)));
        
        // Uint8Array를 TextDecoder를 사용하여 문자열로 변환
        const decoder = new TextDecoder('utf-8');
        return decoder.decode(uint8Array);
    }

}