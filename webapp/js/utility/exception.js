class CustomError extends Error {

    /**
     * @param {String} message 
     */
    constructor(message) {
        super(message);
        this.name =  this.constructor.name;
    }
}

/** 유효성 검사 구문 커스텀에러 */
class ValidationError extends CustomError {
    
    /**
     * @param {String} message 
     */
    constructor(message) {
        super(message);
    }
}

/** Winus DB 적재 커스텀에러 */
class StoredErrorHistory extends CustomError {

    errorRef = '';

    /**
     * @file logUtil.js
     * @param {Error} error 
     */
    constructor(error) {
        super(error.message);

        if(error instanceof Error) {
            this.errorRef = new LogData({
                LOG_TYPE      : 'EXCEPTION',
                LOG_DESC      : error.message,
                ERROR_CODE    : this.name,
                ERROR_DESC    : error.stack?.substr(0, 1000),
                USER_DEFINED2 : JSON.stringify(navigator.userAgentData?.brands[0])
            });
        }
        else if(error instanceof LogData) {
            this.errorRef = error;
        }

        WinusLog.InsertForError(this.errorRef);
    }
}

class HTTPErrorHistory extends StoredErrorHistory {
    constructor(service, url, desc, error){
        super(new LogData({
            LOG_TYPE      : 'EXCEPTION',
            SERVICE       : service,
            FUNCTION      : url,
            LOG_DESC      : desc,
            ERROR_CODE    : error?.message,
            ERROR_DESC    : error?.stack,
        }));
    }
}

/** Ajax 응답성공(200), 수행내용실패(Fail) 케이스에 대한 DB적재 커스텀에러 */
class ResponseErrorHistory extends StoredErrorHistory {

    /**
     * @param {String} message 
     * @param {String} fileName 
     * @param {String} lineNumber 
     */
    constructor(message, fileName, lineNumber) {
        super(new Error(message, fileName, lineNumber));
    }
}