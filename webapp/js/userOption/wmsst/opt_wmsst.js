import { ViewOption } from './../viewOption.js';

const OPT_WMSST050E2 = new Map([
    ['OPT_00', '주문정보수정']
]);

jQuery(document).ready(function() {

    const keys = [...OPT_WMSST050E2.keys()];

    const viewOptions = new ViewOption({
        'menuNm'    : 'WMSST050E2',
        'menuInfo'  : OPT_WMSST050E2
    });


    /** Default Value */
    document.querySelector(`#${keys[0]}`).checked = true;


    /** "설정" 버튼 클릭 */
    jQuery("#btnEditOptions").off().on("click", (e)=>{
        let custId = jQuery('#vrSrchCustId').val();

        document.querySelector(`#${keys[0]}`).checked = viewOptions.GetOptionValue(keys[0], custId ?? "");

		viewOptions.PopOpen();
	});


    /** "저장" 버튼 클릭 */
    document.getElementById("btnOptSave").addEventListener('click', (e)=>{
    
        let options = {};
        let custId = jQuery('#vrSrchCustId').val();

        // 1. Set Option Data
        options[keys[0]] = jQuery(`#${keys[0]}`).is(":checked");

        // 2. Save
        viewOptions.Save(options, custId);
        viewOptions.PopClose();
        
    });


    const optionStore = (() => {

        return {
            GetOptionValue : viewOptions.GetOptionValue
        }
    
    })();

    
});