export class ViewOption {

    #LOCAL_STORAGE_KEY = 'WINUS_USR_OPT';
    #menuNm;
    #menuInfo;

    constructor(args) {
        this.#menuNm    = "menuNm" in args ? args["menuNm"] : '';
        this.#menuInfo  = "menuInfo" in args ? args["menuInfo"] : {};
    }


    /**
     * 옵션 값 저장 (로컬 스토리지)
     * 
     * @param {*} optionData 
     * @param {*} custId 
     */
    Save (optionData, custId) {
        
        let lcId    = jQuery("select[name='SVC_INFO']").val();
        let winusOptions    = JSON.parse(localStorage.getItem(this.#LOCAL_STORAGE_KEY)) ?? {};
        
        /** 자료형 Depth 별 초기화 (센터 → 화주 → 메뉴명) */
        if (!winusOptions.hasOwnProperty(lcId)) winusOptions[lcId] = {};
        if (!winusOptions[lcId].hasOwnProperty(custId)) winusOptions[lcId][custId] = {};
        if (!winusOptions[lcId][custId].hasOwnProperty(this.#menuNm)) winusOptions[lcId][custId] [this.#menuNm]= {};

        winusOptions[lcId][custId][this.#menuNm] = optionData;

        localStorage.setItem(this.#LOCAL_STORAGE_KEY , JSON.stringify(winusOptions));
    }


    /**
    * 화주단위 설정값이 로컬 스토리지에 존재하는 지 확인.
    * 
    * @param {*} optionKey 
    * @param {*} custId 
    * @returns 
    */
    GetOptionValue = (optionKey, custId) => {

        let lcId            = jQuery("select[name='SVC_INFO']").val();
        let winusOptions    = localStorage.getItem(this.#LOCAL_STORAGE_KEY);
        let options         = {};
        let value           = null;

        if(winusOptions == null) { return null; }

        winusOptions = JSON.parse(winusOptions);
        options = winusOptions[lcId]?.[custId]?.[this.#menuNm];

        if(options == undefined) return ;

        if(optionKey in options){

            value = options[optionKey];

            if(value == "true" || value == "false") value = Boolean(value);
            
        }

        return value;
    }


    /**
     * 화면 설정 팝업 열기
     * 
     * @param {*} id 
     */
    PopOpen = (id="popViewOptions") => {

        jQuery(`#${id}`).bPopup({
			zIndex: 999,
			escClose: 0,
			opacity: 0.6,
			positionStyle: 'fixed',
			transition: 'slideDown',
			speed: 400
		});

    }


    /**
     * 화면 설정 팝업 닫기
     * 
     * @param {*} id 
     */
    PopClose = (id="popViewOptions") => {

        jQuery(`#${id}`).bPopup().close();

    }

}


const OptionDesigner = (()=>{

    

})();