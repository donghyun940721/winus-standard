const OptionParser = (() => {
    const LOCAL_STORAGE_KEY = 'WINUS_USR_OPT';

    /**
    * 화주단위 설정값이 로컬 스토리지에 존재하는 지 확인.
    * 
    * @param {*} optionKey 
    * @param {*} custId 
    * @returns 
    */
    const GetOptionValue = (optionKey, lcId, custId, menuNm) => {
        
        let winusOptions    = localStorage.getItem(LOCAL_STORAGE_KEY);
        let options         = {};
        let value           = null;

        if(winusOptions == null) { return null; }

        winusOptions = JSON.parse(winusOptions);
        options = winusOptions[lcId]?.[custId]?.[menuNm];

        if(options == undefined) return ;

        if(optionKey in options){

            value = options[optionKey];

            if(value == "true" || value == "false") value = Boolean(value);
            
        }

        return value;
    }

    return {
        GetOptionValue : GetOptionValue
    }

})();