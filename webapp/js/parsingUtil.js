function parseJsonToSettingFile(rootKey=null, jsonObject)
{
	let rtn = null;

    if(!cfn_isEmpty(rootKey))
    {
        rtn = 
            `<IDataXMLCoder version="1.0">
                <record javaclass="com.wm.data.ISMemDataImpl">
                    <record name="${rootKey}" javaclass="com.wm.data.ISMemDataImpl">
            `;
    }
	
    let target = !cfn_isEmpty(rootKey) ? jsonObject[rootKey] : jsonObject;
	for(let key of Object.keys(target))
	{
		rtn += 
			`			<value name="${key}">${target[key]}</value>
            `
	}

    
    if(!cfn_isEmpty(rootKey))
    {
        rtn += 
                `
                        </record>
                    </record>
                </IDataXMLCoder>`
    }

	console.log(rtn);
}