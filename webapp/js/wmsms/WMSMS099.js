import { WMSMS099E2 }  from './WMSMS099E2.js';


const ITEM_GRP_INFO     = '0';
const IN_ZONE_MAPPING   = '1';

let   ACTIVE_TAB_INDEX          = null;              // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = ITEM_GRP_INFO;     // 최초 화면로드 시, 출력되는 탭


jQuery(document).ready(function() {

    let view01 = null;
    let view02 = new WMSMS099E2();

    const ViewContainer = [view01,view02];

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });

    if(ViewContainer[DEFAULT_VIEW_INDEX] != null){
        ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
        ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();
    }

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){

        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        if(ViewContainer[activeTabIdx] != null){
            ViewContainer[activeTabIdx].ActiveViewEventCallBack();
        }
        
        switch(activeTabIdx){
            case ITEM_GRP_INFO : 
                ps_setSpreadColumnIndex();
                break;

            case IN_ZONE_MAPPING : 
                break;
        }

    });
});