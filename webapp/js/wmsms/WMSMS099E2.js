import { WinusGrid }                from '../utility/WinusGrid.js';


export class WMSMS099E2 {

    #custId         = null;
    #formId         = 'frm_listE2';

    #zoneDataset    = new Array();

    #firstGrid = new WinusGrid({ 
        spdListDiv          :   'spdListE2_1',
        gridTemplate        :   'WMSMS099E2_1',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        setColor            :   false
    });

    #secondGrid = new WinusGrid({ 
        spdListDiv          :   'spdListE2_2',
        gridTemplate        :   'WMSMS099E2_1',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        setColor            :   false
    });

    #thirdGrid = new WinusGrid({ 
        spdListDiv          :   'spdListE2_3',
        gridTemplate        :   'WMSMS099E2_1',
        headerHeight        :   '27',
        rowHeight           :   '22',
        frozenColIdx        :   '',
        setColor            :   false
    });

    #GRID_INDEX = { FIRST: 1, SECOND: 2, THIRD: 3 };


    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();
        this.AddEvent();

        this.#firstGrid.Init();
        this.#firstGrid.SetGrid();

        this.#secondGrid.Init();
        this.#secondGrid.SetGrid();

        this.#thirdGrid.Init();
        this.#thirdGrid.SetGrid();

    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        jQuery("#parentInfo_2").hide();
        jQuery("#parentInfo_3").hide();

        this.SetZonData();
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        const vm = this;

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSearchE2").off().on("click", (e)=>{

            vm.#firstGrid.Clear();
            vm.#secondGrid.Clear();
            vm.#thirdGrid.Clear();
            
            this.SearchItemGroupInfo(e);
        });


        // * '신규' 버튼 클릭 이벤트
        jQuery("#btnAddE2_1").off().on("click", (e)=>{
            this.#firstGrid.RowAddEventCallBack(false, null, true, 0);
            this.#firstGrid.SetValueByColKey(0, 'ST_GUBUN', 'INSERT');
        });
        
        jQuery("#btnAddE2_2").off().on("click", (e)=>{
            this.#secondGrid.RowAddEventCallBack(false, null, true, 0);
            this.#secondGrid.SetValueByColKey(0, 'ST_GUBUN', 'INSERT');
        });
        
        jQuery("#btnAddE2_3").off().on("click", (e)=>{
            this.#thirdGrid.RowAddEventCallBack(false, null, true, 0);
            this.#thirdGrid.SetValueByColKey(0, 'ST_GUBUN', 'INSERT');
        });

        
        // * '저장' 버튼 클릭 이벤트
        jQuery("#btnSaveE2_1").off().on("click", (e)=>{
            if(confirm(`선택 항목을 저장 하시겠습니까?`)) this.SaveClickEventCallBack(e, this.#firstGrid, this.#GRID_INDEX.FIRST);
        });
        
        jQuery("#btnSaveE2_2").off().on("click", (e)=>{
            if(confirm(`선택 항목을 저장 하시겠습니까?`)) this.SaveClickEventCallBack(e, this.#secondGrid, this.#GRID_INDEX.SECOND);
        });
        
        jQuery("#btnSaveE2_3").off().on("click", (e)=>{
            if(confirm(`선택 항목을 저장 하시겠습니까?`)) this.SaveClickEventCallBack(e, this.#thirdGrid, this.#GRID_INDEX.THIRD);
        });

        
        // * '삭제' 버튼 클릭 이벤트
        jQuery("#btnDeleteE2_1").off().on("click", (e)=>{
            if(confirm(`선택 항목을 삭제 하시겠습니까?`)) this.DeleteClickEventCallBack(e, this.#firstGrid, this.#GRID_INDEX.FIRST);
        });
        
        jQuery("#btnDeleteE2_2").off().on("click", (e)=>{
            if(confirm(`선택 항목을 삭제 하시겠습니까?`)) this.DeleteClickEventCallBack(e, this.#secondGrid, this.#GRID_INDEX.SECOND);
        });
        
        jQuery("#btnDeleteE2_3").off().on("click", (e)=>{
            if(confirm(`선택 항목을 삭제 하시겠습니까?`)) this.DeleteClickEventCallBack(e, this.#thirdGrid, this.#GRID_INDEX.THIRD);
        });


        /** Custom Grid Event */
        this.#firstGrid.CellDoubleClickEvent = function(sender, args){
            vm.GridDoubleClickEvent(sender, args, vm.#GRID_INDEX.FIRST);
        }
        
        this.#firstGrid.CellEditedCustomEvent = function(sender, args){
            vm.#firstGrid.SetCheckedUnitRow(args.row, args.col, true);
        }


        this.#secondGrid.CellDoubleClickEvent = function(sender, args){
            vm.GridDoubleClickEvent(sender, args, vm.#GRID_INDEX.SECOND);
        }

        this.#secondGrid.CellEditedCustomEvent = function(sender, args){
            vm.#secondGrid.SetCheckedUnitRow(args.row, args.col, true);
        }


        this.#thirdGrid.CellEditedCustomEvent = function(sender, args){
            vm.#thirdGrid.SetCheckedUnitRow(args.row, args.col, true);
        }
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // 횡 스크롤 새로고침
        this.#firstGrid.ShowHorizontalScrollbar(true);
        this.#secondGrid.ShowHorizontalScrollbar(true);
        this.#thirdGrid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


    SetZonData(){

        const vm = this;
        let formData = new FormData(document.getElementById(this.#formId));
        
        jQuery.ajax({
            url : "/WMSMS081/list.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                if(data != null && 'rows' in data){
                    vm.#zoneDataset = data.rows;
                }
            }
        });

    }


//#endregion


//#region   :: CallBack Event


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchItemGroupInfo(e, isProgressBar=true, param){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        const groupLevel =  param ? param.groupLevel : '1';
        const targetGrid = param ? param.gridObj : this.#firstGrid;
        const upItemGrpId = param ? param.upItemGrpId : null;

        formData.append('vrGrpLevel'    , groupLevel);
        
        if(upItemGrpId != null){
            formData.append('vrUpItemGrpId' , upItemGrpId);
        }
        

        return jQuery.ajax({
            url : "/WMSMS094/listLvl.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    targetGrid.SetData(data);
                }

            }
        });
    }


    /**
     * 저장
     * 
     * @param {*} e 
     * @param {WinusGrid}   targetGrid 
     * @param {Number}      gridIdx 
     */
    SaveClickEventCallBack(e, targetGrid, gridIdx){

        let vm = this;
        let formData = new FormData(document.getElementById(this.#formId));
        let selectedData = targetGrid.GetSelectedRowData(0);
        const upItemGrpId = jQuery(`#upItemGrpId_${gridIdx}`).text();

        formData.append('selectIds', selectedData.length);

        for(let i=0; i < selectedData.length; i++)
        {
            let rowIdx = selectedData[i]['ROW_IDX'];
            formData.append(`ST_GUBUN${i}`, targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('ST_GUBUN')) ?? 'UPDATE');

            let param = {
                ITEM_GRP_ID        :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('ITEM_GRP_ID'))
                , ITEM_GRP_CD      :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('ITEM_GRP_CD'))
                , ITEM_GRP_NM      :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('ITEM_GRP_NM'))
                , UP_ITEM_GRP_ID   :  upItemGrpId
                , GRP_LEVEL        :  gridIdx
                , IN_ZONE_CD       :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('IN_ZONE_CD'))
                , IN_ZONE_ID       :  this.ConvertZoneCodeToId(targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('IN_ZONE_CD')))
                , ITEM_GRP_TYPE    : 'G'
            }

            formData.append(`ITEM_GRP_ID${i}`     , param['ITEM_GRP_ID']);
            formData.append(`ITEM_GRP_CD${i}`     , param['ITEM_GRP_CD']);
            formData.append(`ITEM_GRP_NM${i}`     , param['ITEM_GRP_NM']);

            if(!cfn_isEmpty(upItemGrpId)){
                formData.append(`UP_ITEM_GRP_ID${i}`  , param['UP_ITEM_GRP_ID']);
            }

            formData.append(`GRP_LEVEL${i}`       , param['GRP_LEVEL']);
            formData.append(`IN_ZONE_NM${i}`      , param['IN_ZONE_CD']);
            formData.append(`IN_ZONE_ID${i}`      , param['IN_ZONE_ID']);
            formData.append(`ITEM_GRP_TYPE${i}`   , param['ITEM_GRP_TYPE']);
        }


        jQuery.ajax({
            url : "/WMSMS099/saveLvl.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                vm.SearchItemGroupInfo(null, true, { groupLevel:  gridIdx, gridObj: targetGrid, upItemGrpId: upItemGrpId});
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    alert(`저장되었습니다.`);
                }
        
            }
        });

    }


    DeleteClickEventCallBack = (e, targetGrid, gridIdx) => {

        let vm = this;
        let formData = new FormData(document.getElementById(this.#formId));
        let selectedData = targetGrid.GetSelectedRowData(0);
        const upItemGrpId = jQuery(`#upItemGrpId_${gridIdx}`).text();

        formData.append('selectIds', selectedData.length);

        for(let i=0; i < selectedData.length; i++)
        {
            let rowIdx = selectedData[i]['ROW_IDX'];
            formData.append(`ST_GUBUN${i}`, 'DELETE');

            let param = {
                ITEM_GRP_ID        :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('ITEM_GRP_ID'))
                , ITEM_GRP_CD      :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('ITEM_GRP_CD'))
                , ITEM_GRP_NM      :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('ITEM_GRP_NM'))
                , UP_ITEM_GRP_ID   :  upItemGrpId
                , GRP_LEVEL        :  gridIdx
                , IN_ZONE_CD       :  targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('IN_ZONE_CD'))
                , IN_ZONE_ID       :  this.ConvertZoneCodeToId(targetGrid.getValue(rowIdx, targetGrid.GetColumnIdx('IN_ZONE_CD')))
                , ITEM_GRP_TYPE    : 'G'
            }

            formData.append(`ITEM_GRP_ID${i}`     , param['ITEM_GRP_ID']);
            formData.append(`ITEM_GRP_CD${i}`     , param['ITEM_GRP_CD']);
            formData.append(`ITEM_GRP_NM${i}`     , param['ITEM_GRP_NM']);

            if(!cfn_isEmpty(upItemGrpId)){
                formData.append(`UP_ITEM_GRP_ID${i}`  , param['UP_ITEM_GRP_ID']);
            }

            formData.append(`GRP_LEVEL${i}`       , param['GRP_LEVEL']);
            formData.append(`IN_ZONE_NM${i}`      , param['IN_ZONE_CD']);
            formData.append(`IN_ZONE_ID${i}`      , param['IN_ZONE_ID']);
            formData.append(`ITEM_GRP_TYPE${i}`   , param['ITEM_GRP_TYPE']);
        }

        jQuery.ajax({
            url : "/WMSMS099/saveLvl.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
                vm.SearchItemGroupInfo(null, true, { groupLevel:  gridIdx, gridObj: targetGrid, upItemGrpId: upItemGrpId});
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    alert(`저장되었습니다.`);
                }
        
            }
        });

    }


//#endregion


//#region   :: Grid Event CallBack


    GridDoubleClickEvent(sender, args, gridIdx){

        let groupLevel = null;
        let grid = null;
        let upItemGrpId = null;
        let upItemGrpNm = null;

        switch(gridIdx){

            case this.#GRID_INDEX.FIRST :

                groupLevel = 2;
                grid = this.#secondGrid;    // Draw Taget
                upItemGrpId = this.#firstGrid.GetCellValue('ITEM_GRP_ID', args.row);
                upItemGrpNm = this.#firstGrid.GetCellValue('ITEM_GRP_NM', args.row);

                // 하위 그리드 초기화
                this.#secondGrid.Clear();
                this.#thirdGrid.Clear();

                // 하위 그리드 부모정보 설정
                jQuery("#parentInfo_2").show();
                jQuery("#upItemGrpId_2").text(upItemGrpId);
                jQuery("#upItemGrpNm_2").text(upItemGrpNm);

                break;

            case this.#GRID_INDEX.SECOND :

                groupLevel = 3;
                grid = this.#thirdGrid;     // Draw Taget
                upItemGrpId = this.#secondGrid.GetCellValue('ITEM_GRP_ID', args.row);
                upItemGrpNm = this.#secondGrid.GetCellValue('ITEM_GRP_NM', args.row);

                // 하위 그리드 초기화
                this.#thirdGrid.Clear();

                // 하위 그리드 부모정보 설정
                jQuery("#parentInfo_3").show();
                jQuery("#upItemGrpId_3").text(upItemGrpId);
                jQuery("#upItemGrpNm_3").text(upItemGrpNm);
                
                break;

            case this.#GRID_INDEX.THIRD :
                
                break;
        }

        this.SearchItemGroupInfo(null, true, { 
            groupLevel: groupLevel,     // 상품군 레벨 (대/중/소)
            gridObj: grid,              // 상품군별 그리드 객체
            upItemGrpId: upItemGrpId    // 선택한 상품군 ID
        });

    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


    ConvertZoneCodeToId(zoneId){

        if(cfn_isEmpty(zoneId)) return ;

        const result = this.#zoneDataset.filter((dt)=>{
            return (dt.ZONE_NM == zoneId.trim());
        });


        return (cfn_isEmpty(result) ? '' : result[0].ZONE_ID);
    }


//#endregion

}