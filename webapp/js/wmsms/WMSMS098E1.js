import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil, LocalStorageUtil }    from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';

export class WMSMS098E1 {

    #custId         = null;
    #formId         = 'frm_listE1';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #compItem = new Component({
        code        : 'vrSrchRitemCdE1',
        id          : 'vrSrchRitemIdE1',
        name        : 'vrSrchRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });

    #compSubItem = new Component({
        code        : 'vrSrchSubRitemCdE1',
        id          : 'vrSrchSubRitemIdE1',
        name        : 'vrSrchSubRitemNmE1',
        category    : COMP_CATEGORY.ITEM
    });


    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE1',
        gridTemplate        :   'WMSMS098E1',
        headerHeight        :   '27',
        rowHeight           :   '25',
        useFilter           :   true,
        useSort             :   true,
        setColor            :   false,
    });


    #_store = null;

    constructor(args){

        let vm = this;

        if(args != undefined){
            this.#_store = "store" in args ? args.store : null;
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성

    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        const vm = this;

        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * 상품정보
        jQuery("#vrSrchRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value, vm.#compItem);
        });

        jQuery("#vrSrchRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }
            
            StandardPopUp.call(this, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value,vm.#compItem);
        });

        // * sub상품정보
        jQuery("#vrSrchSubRitemCdE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value, vm.#compSubItem);
        });

        jQuery("#vrSrchSubRitemImgE1").off().on("click", (e)=>{
            let param = {
                CUST_ID : this.#custId
            }

            let viewer = this.#compSubItem;
            viewer.StandardPopUpCallBack=(type,data)=>{vm.#compSubItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);};
            
            StandardPopUp.call(viewer, COMP_CATEGORY.ITEM, param);
        });

        jQuery("#vrSrchSubRitemNmE1").off().on("change", (e)=>{
            this.ItemChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value, vm.#compSubItem);
        });

        // 저장 버튼 이벤트
        jQuery("#btnSaveData").off().on("click", (e)=>{
            // 필수선택값 확인
            if(!cfn_isEmpty(this.#custId)){
                this.SaveClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }
        });

        // 신규 버튼 이벤트
        jQuery("#btnAddRow").off().on("click", (e)=>{
            let unlockColumns = [
                vm.#grid.GetColumnIdx("PARENTS_RITEM_CD")
                ,vm.#grid.GetColumnIdx("PARENTS_RITEM_NM")
                ,vm.#grid.GetColumnIdx("CHILD_RITEM_CD")
                ,vm.#grid.GetColumnIdx("CHILD_RITEM_NM")
                ,vm.#grid.GetColumnIdx("CHILD_EXT_QTY")
            ];
            var nRow = vm.#grid.RowAddEventCallBack(false, unlockColumns, true);
            vm.#grid.spdList.showRow(nRow, GC.Spread.Sheets.VerticalPosition.top);
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE1").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{

            // 필수선택값 확인
            if(!cfn_isEmpty(jQuery("#vrSrchCustIdE1").val())){
                this.SearchClickEventCallback(e);
            }
            else{
                StandardPopUp.call(this, COMP_CATEGORY.CUST);
            }

        });

        this.#grid.ButtonClickCustomEvent = (sender, args)=>{

            const column = this.#grid.GetColumnCodeByIdx(args.col);
            const row = args.row;

            switch(column){
                // 상품 조회 버튼 (양수)
                case 'PARENTS_RITEM_SEARCH' :
                    var code = this.#grid.GetColumnIdx("PARENTS_RITEM_CD");
                    var name = this.#grid.GetColumnIdx("PARENTS_RITEM_NM");
                    var id = this.#grid.GetColumnIdx("PARENTS_RITEM_ID");
                    var chk = this.#grid.GetColumnIdx("CHECK");

                    var param = {
                        CUST_ID : this.#custId
                    }
        
                    var viewer = this.#grid;
                    viewer.StandardPopUpCallBack=(type,data)=>{
                        viewer.setValue(row,code,data[0][0].item_code);
                        viewer.setValue(row,name,data[0][0].item_kor_nm);
                        viewer.setValue(row,id,data[0][0].ritem_id);
                        viewer.setValue(row,chk,true);
                    };
        
                    StandardPopUp.call(viewer, COMP_CATEGORY.ITEM, param);

                    break;
                case 'CHILD_RITEM_SEARCH' :
                    var code = this.#grid.GetColumnIdx("CHILD_RITEM_CD");
                    var name = this.#grid.GetColumnIdx("CHILD_RITEM_NM");
                    var id = this.#grid.GetColumnIdx("CHILD_RITEM_ID");
                    var chk = this.#grid.GetColumnIdx("CHECK");

                    var param = {
                        CUST_ID : this.#custId
                    }
        
                    var viewer = this.#grid;
                    viewer.StandardPopUpCallBack=(type,data)=>{
                        viewer.setValue(row,code,data[0][0].item_code);
                        viewer.setValue(row,name,data[0][0].item_kor_nm);
                        viewer.setValue(row,id,data[0][0].ritem_id);
                        viewer.setValue(row,chk,true);
                    };
        
                    StandardPopUp.call(viewer, COMP_CATEGORY.ITEM, param);

                    break;
            }
        }

        this.#grid.CellEditedCustomEvent = (sender, args) => {
            const column = this.#grid.GetColumnCodeByIdx(args.col);
            switch(column){
                case 'PARENTS_RITEM_CD' :
                case 'PARENTS_RITEM_NM' :
                case 'CHILD_RITEM_CD' :
                case 'CHILD_RITEM_NM' :
                    this.GridItemChangedEventCallBack(args);
                    break;
                default :
                    this.#grid.SetValueByColKey(this.#grid.targetRow, 'CHECK', true);
                    return;
            }
            
        }
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        /* 화주 정보 설정*/
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), 
                                    SessionUtil.GetValue(SessionKey.CUST_ID), 
                                    SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     * @param {*} component : 컴포넌트 객체
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                vm.SearchClickEventCallback();

                break;

            //* 상품
            case COMP_CATEGORY.ITEM :
                
                vm.#compItem.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);

                break;
        }

    }

    async CustChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];

                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    async ItemChangedEventCallBack(type, refValue, component){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            component.Init();
            return;
        }

        await component.Search(type, refValue)
                .then((data)=>{
                    if(data.length > 1){
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                        popParam['CUST_ID'] = vm.#custId;
                        
                        let viewer = component;
                        viewer.StandardPopUpCallBack=(type,data)=>{component.SetValue(data[0][0].item_code, data[0][0].ritem_id, data[0][0].item_kor_nm);};
            
                        StandardPopUp.call(viewer, COMP_CATEGORY.ITEM, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    async GridItemChangedEventCallBack(args){
        const vm = this;
        const column = this.#grid.GetColumnCodeByIdx(args.col);
        const row = args.row;
        const chk = this.#grid.GetColumnIdx('CHECK');
        
        let code = '';
        let name = '';
        let id = '';
        let refValue = '';
        let type = 'ITEM';
        let refKey = '';

        switch(column){
            case 'PARENTS_RITEM_CD' :
                code = this.#grid.GetColumnIdx("PARENTS_RITEM_CD");
                name = this.#grid.GetColumnIdx("PARENTS_RITEM_NM");
                id = this.#grid.GetColumnIdx("PARENTS_RITEM_ID");
                refValue = this.#grid.getValue(row, code);
                refKey = 'vrSrchItemCd';
                break;
            case 'PARENTS_RITEM_NM' :
                code = this.#grid.GetColumnIdx("PARENTS_RITEM_CD");
                name = this.#grid.GetColumnIdx("PARENTS_RITEM_NM");
                id = this.#grid.GetColumnIdx("PARENTS_RITEM_ID");
                refValue = this.#grid.getValue(row, name);
                refKey = 'vrSrchItemNm';
                break;
            case 'CHILD_RITEM_CD' :
                code = this.#grid.GetColumnIdx("CHILD_RITEM_CD");
                name = this.#grid.GetColumnIdx("CHILD_RITEM_NM");
                id = this.#grid.GetColumnIdx("CHILD_RITEM_ID");
                refValue = this.#grid.getValue(row, code);
                refKey = 'vrSrchItemCd';
                break;
            case 'CHILD_RITEM_NM' :
                code = this.#grid.GetColumnIdx("CHILD_RITEM_CD");
                name = this.#grid.GetColumnIdx("CHILD_RITEM_NM");
                id = this.#grid.GetColumnIdx("CHILD_RITEM_ID");
                refValue = this.#grid.getValue(row, name);
                refKey = 'vrSrchItemNm';
                break;
            default :
                return;
        }

        if(cfn_isEmpty(refValue)){
            this.#grid.setValue(row,code,'');
            this.#grid.setValue(row,name,'');
            this.#grid.setValue(row,id,'');
            return;
        }

        let formData  = new FormData();
        formData.append('srchKey', COMP_CATEGORY.ITEM);
        formData.append(refKey, refValue);
        formData.append('CUST_ID', this.#custId);

        await new Promise((resolve, reject)=>{
            jQuery.ajax({
                url : "/selectMngCode.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(result){
                    result = result[type];
                    // 값 없음.
                    if(result == null || result.length == 0) { 
                        reject(result);
                    }
                    resolve(result);
                },
                error : function(xhr, textStatus, err){
                    reject(err);
                }
            });
        }).then((data)=>{
            if(data.length > 1){
                var param = {};
                param[refKey == 'vrSrchItemCd' ? 'RITEM_CD' : 'RITEM_NM'] = refValue;
                param['CUST_ID'] = vm.#custId;
    
                var viewer = this.#grid;
                viewer.StandardPopUpCallBack=(type,data)=>{
                    viewer.setValue(row,code,data[0][0].item_code);
                    viewer.setValue(row,name,data[0][0].item_kor_nm);
                    viewer.setValue(row,id,data[0][0].ritem_id);
                    viewer.setValue(row,chk,true);
                };
    
                StandardPopUp.call(viewer, COMP_CATEGORY.ITEM, param);
            }
            else if (data.length == 1){
                this.#grid.setValue(row,code,data[0].CODE);
                this.#grid.setValue(row,name,data[0].NAME);
                this.#grid.setValue(row,id,data[0].ID);
                this.#grid.setValue(row,chk,true);
            }
        })
        .catch((error)=>{
            alert(error);
            this.#grid.setValue(row,code,'');
            this.#grid.setValue(row,name,'');
            this.#grid.setValue(row,id,'');
            this.#grid.setValue(row,chk,false);
        });
    }

    SaveClickEventCallback(e){
        try {

            let vm = this;
            let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            let saveMessage = null;

            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 상품을 선택해야 합니다.`;
            }

            saveMessage = DICTIONARY['confirm_save'];

            if(confirm(saveMessage)){

                let formData = new FormData(document.getElementById(`${this.#formId}`));

                formData.append('ROW_COUNT', selectedIdx.length);

                let checkKeys = [
                    'PARENTS_RITEM_ID'
                    , 'CHILD_RITEM_ID'
                    , 'CHILD_EXT_QTY'
                ];

                // [Loop] Rows
                for(let i=0; i < selectedIdx.length; i++)
                {
                    let rowIdx = selectedIdx[i];

                    for (var j = 0 ; j < checkKeys.length; j++){
                        var value = gridData[i][[checkKeys[j]]];
                        if(value == null || value == ''){
                            throw `필수항목 입력이 되어야 합니다.(${rowIdx}행)`;
                        }
                    }

                    let param = {
                        PARENTS_RITEM_ID            : gridData[i]['PARENTS_RITEM_ID']
                        , CHILD_RITEM_ID            : gridData[i]['CHILD_RITEM_ID']
                        , CHILD_EXT_QTY             : gridData[i]['CHILD_EXT_QTY']
                        , OLD_PARENTS_RITEM_ID      : gridData[i]['OLD_PARENTS_RITEM_ID']
                        , OLD_CHILD_RITEM_ID        : gridData[i]['OLD_CHILD_RITEM_ID']
                        , DEL_YN                    : gridData[i]['DEL_YN']
                    };

                    formData.append(`PARENTS_RITEM_ID`            +   `_${i}`, param['PARENTS_RITEM_ID']);
                    formData.append(`CHILD_RITEM_ID`              +   `_${i}`, param['CHILD_RITEM_ID']);
                    formData.append(`CHILD_EXT_QTY`               +   `_${i}`, param['CHILD_EXT_QTY']);
                    formData.append(`OLD_PARENTS_RITEM_ID`        +   `_${i}`, param['OLD_PARENTS_RITEM_ID']);
                    formData.append(`OLD_CHILD_RITEM_ID`          +   `_${i}`, param['OLD_CHILD_RITEM_ID']);
                    formData.append(`DEL_YN`                      +   `_${i}`, param['DEL_YN']);
                }

                jQuery.ajax({
                    url : "/WMSMS098/saveItem.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        cfn_viewProgress();
                    },
                    complete: function(){
                        cfn_closeProgress();
                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(data["MSG"]);
                        }
                        // Success ...
                        else{
                            alert(`${DICTIONARY['save_success']} (${selectedIdx.length} 건)`);
                        }
            
                    },
                    error: function(request, status, error){
                        alert(`관리자에게 문의바랍니다. (${request.status})`);
                        console.log(request);
                        console.log(status);
                        console.log(error);
                    }
                });
            }
            else {
                return ;
            }

        } catch (error) {
            alert(error);
        }

    }

    SaveNullValidationCheck(gridRowData, checkKeys){

        
    }


    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad('추가상품목록관리', true);
    }

    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
     SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return jQuery.ajax({
            url : "/WMSMS098/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }

//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}