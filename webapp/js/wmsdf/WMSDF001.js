import { WMSDF001E2 }  from './WMSDF001E2.js';
import { WMSDF001E3 }  from './WMSDF001E3.js';


let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const INVC_CORP_INFO 	= '0';
const INVC_MANAGEMENT 	= '1';
const INVC_PRICE_SET	= '2';


jQuery(document).ready(function() {

    let view01 = null;
    let view02 = new WMSDF001E2();
    let view03 = new WMSDF001E3();

    const ViewContainer = [view01,view02, view03];

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
     //ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    // ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){

        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;
        ViewContainer[activeTabIdx].ActiveViewEventCallBack();
        
        switch(activeTabIdx){
            case INVC_CORP_INFO : 
                break;

            case INVC_MANAGEMENT : 
                break;
                
            case INVC_PRICE_SET : 
                break;
        }

    });
});