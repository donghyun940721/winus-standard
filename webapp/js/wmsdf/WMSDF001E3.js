import * as InterfaceUtil           from '../utility/interfaceUtil.js';
import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil }  from '../utility/userStorageManger.js';


export class WMSDF001E3{

    #CODE           = 'CODE';
    #NAME           = 'NAME';

    #parcelCd       = null;
    #parcelSeq      = null;
    #custId         = null;
    #formId         = 'frm_listE3';


    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE3',                             
        gridTemplate        :   'WMSDF001E3',                            
        headerHeight        :   '15',                             // 헤더 높이
        rowHeight           :   '27',                             // 데이터 행 높이
        frozenColIdx        :   '1',                             // 열 틀 고정할 열 인덱스
        setColor            :   false,
        useFilter           :   true,
        //headerMappingInfo   :   this.#headerMappingInfo, 
        // usedFooter          :   true
    });

    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩
        this.InitialEventCallBack();        // !(필수) 전역레벨 콜백함수 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

    }


    /**
     * 전역변수 Level의 이벤트 콜백 바인딩
     * 
     * ex) "commonUtil.js" 등 전역 Level 함수
     */
    InitialEventCallBack(){
        CustPopUpEventCallBack = this.SelectedCustCallBack;
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        let vm = this;

        // # 화주
        document.getElementById("vrSrchCustCdE3").addEventListener('change', (e)=>{

        	this.CustChangedEventCallBack(this.#CODE, e.target.value, 'vrSrchCustCdE3');
        });

        jQuery("#vrSrchCustImgE3").off().on("click", (e)=>{
            fn_Popup("CUST");
        });
        
        document.getElementById("vrSrchCustNmE3").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(this.#NAME, e.target.value, 'vrSrchCustNmE3');
        });

        // 화주 팝업 선택 후처리 이벤트
        jQuery("#vrSrchCustCdE3").off().on("custom", (e, customParam)=>{

        	/** 그리드, 컴포넌트 초기화 */
            this.#grid.Clear();                                 // 그리드 초기화
            jQuery("#vrSrchParcelComCdE3").val('');             // 택배구분 초기화
            this.#parcelCd = null;
            jQuery("#vrSrchParcelSeqE3").val('');
            this.#parcelSeq = null;

            this.#custId = jQuery("#vrSrchCustIdE3").val();
            
            //화주사 택배 정보 셋팅 
        	this.fn_getCustDlvCompInfo(this.#custId);	
        });


        // # '택배구분' 변경 이벤트
        document.getElementById("vrSrchParcelComCdE3").addEventListener('change', (e)=>{
        	jQuery("#vrSrchParcelSeqE3").val('');
        	this.#parcelSeq = null;
        	
            this.ParcelChagnedCallBack(e);
            
            this.#parcelCd = jQuery("#vrSrchParcelComCdE3").val();
        });


        // # '택배SEQ' 변경 이벤트
        document.getElementById("vrSrchParcelSeqE3").addEventListener('change', (e)=>{
            this.SearchClickEventCallback();
            this.#parcelSeq = jQuery("#vrSrchParcelSeqE3").val();
        });



        // # '저장' 버튼 클릭 이벤트
        jQuery("#btn_saveE3").off().on("click", (e)=>{
            this.SaveClickEventCallBack(e);
        });


        // # '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE3").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
        });


        this.#grid.CellEditedCustomEvent = function(sender, args){
            vm.#grid.SetCheckedUnitRow(args.row, args.col, true);
        }


        this.#grid.ButtonClickCustomEvent = function(sender, args){

            let rowIdx = args.row;
            let colIdx = args.col;
            
            let dlvSeq  = jQuery("#vrSrchParcelSeqE3").val();
            let dlvCd   = jQuery('#vrSrchParcelComCdE3').val();
            let custId  = jQuery('#vrSrchCustIdE3').val();

            if(cfn_isEmpty(custId) || cfn_isEmpty(dlvCd) || cfn_isEmpty(dlvSeq)){
                alert(`택배구분, 택배SEQ, 화주 정보를 선택하세요.`);
                return 
            }
   
        }
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){
    	
    	// Init Common CallBack
        this.InitialEventCallBack();

        // Set Cust Info
        if(cfn_isEmpty(jQuery('#vrSrchCustCdE3').val())){
            jQuery('#vrSrchCustCdE3').val(SessionUtil.GetValue(SessionKey.CUST_CD));
            jQuery('#vrSrchCustNmE3').val(SessionUtil.GetValue(SessionKey.CUST_NM));
            jQuery("#vrSrchCustIdE3").val(SessionUtil.GetValue(SessionKey.CUST_ID));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
            
           //화주사 택배 정보 셋팅
            this.fn_getCustDlvCompInfo(this.#custId);
        }
        
        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);

        if(cfn_isEmpty(this.#custId) || cfn_isEmpty(jQuery("#vrSrchCustIdE3").val())){
            fn_Popup("CUST");
        }
    }


    /**
     * 컨텍스트 메뉴 이벤트 콜백
     * 
     * (컨텍스트메뉴 사용 시 정의) 
     * @param {*} menuNm : 컨텍스트 메뉴명
     */
    ContextMenuEventCallBack(menuNm){

        alert( menuNm + ' Call ...');

    }


//#region   :: Component Set 

    fn_getCustDlvCompInfo(custId){
    	let vm = this;
    	var urlSend		= "/WMSOP642/dlvDlvCompInfo.action";
    	var paramSend   = "vrSrchCustId=" + custId;
    	
    	jQuery.post(urlSend, paramSend, function(data){
    		var data = data.DLV_COMP_INFO;
    		var cnt  = data.size();
    		
    		jQuery("#vrSrchParcelComCdE3").children('option').remove();
    		jQuery("#vrSrchParcelSeqE3").children('option').remove();
    		
    		if(cnt > 0){
    			for(var i = 0; i < cnt; i++){
    				jQuery("#vrSrchParcelComCdE3").append("<option value='" + data[i].PARCEL_COM_TY + "'>" + data[i].PARCEL_COM_TY_NM + "</option>");
    			}
    			
    			jQuery("#vrSrchParcelComCdE3 option:eq(0)").prop("selected", true);
    			 
    			vm.SetParcelSeq();
    		}
    		
    	}).fail(function(xhr, textStatus){
    		if(xhr.status == 404){	
    			location.href=('/index.html');
    		}
    	});
    }
    
    
    SetParcelSeq(){
        let vm = this;
        let formData = new FormData(document.getElementById(this.#formId));
        
        jQuery.ajax({
            url : "/WMSDF001/selectDlvSeq.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                jQuery("#vrSrchParcelSeqE3 option").remove();

                if(data == undefined || data == null || data.rows == null || data.rows.length == 0){
                    alert(`택배기본정보를 찾을 수 없습니다. (택배사 정보없음.)`);
                    return ;
                }

                for(let parcelInfo of data.rows){

                    let option = new Option();

                    option.value = parcelInfo['PARCEL_COM_TY_SEQ'];
                    option.text = parcelInfo['PARCEL_COM_TY_SEQ']+" : "+parcelInfo['SHORT_DESC'];

                    jQuery("#vrSrchParcelSeqE3").append(option);
                }
                
                vm.SearchClickEventCallback();
            }
        });
    }


//#endregion


//#region   :: CallBack Event


    CustChangedEventCallBack(type, refValue, id){

        let that = this;
        let paramKey = type == this.#CODE ? 'vrSrchCustCd' : 'vrSrchCustNm';
        let callback = "CustPopUpEventCallBack";
    
        this.#grid.Clear();

        if(!cfn_isEmpty(refValue)){
            
            var param   = `srchKey=CUST&${paramKey}=${refValue}&S_CUST_TYPE=12`;
            var url     = "/selectMngCode.action"
            
            jQuery.post(url, param, function(output){

                let data = output.CUST;

                if(cfn_isEmpty(data)){
                    jQuery('#vrSrchCustCdE3').val("").focus();
                    jQuery('#vrSrchCustNmE3').val("");
                    jQuery("#vrSrchCustIdE3").val("");
                    alert(DICTIONARY['list.nodata']);
                }
                else{
                    var strParameter = [];
                    strParameter.push(type == that.#CODE ? refValue : '');
                    strParameter.push(type == that.#NAME ? refValue : '');
                    var width  = "800";
                    var height = "530";
                    var param  = `?func=${callback}&custType=12&strParameter=${strParameter}&stBtnYn=N`;
                    var url    = "/WMSCM011.action" + param;
                    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
                    asppop.focus();
                }

                //that.SetParcelSeq();

            }).fail(function(xhr, textStatus, err){

            });
        }else{
            jQuery('#vrSrchCustCdE3').val("");
            jQuery('#vrSrchCustNmE3').val("");
            jQuery("#vrSrchCustIdE3").val("");
        }
    }


    SelectedCustCallBack(arr){

        var info = arr[0];	
        jQuery('#vrSrchCustCdE3').val(info.cust_cd);
        jQuery('#vrSrchCustNmE3').val(info.cust_nm);
        jQuery("#vrSrchCustIdE3").val(info.cust_id);

        SessionUtil.Insert(SessionKey.CUST_CD, info.cust_cd);
        SessionUtil.Insert(SessionKey.CUST_NM, info.cust_nm);
        SessionUtil.Insert(SessionKey.CUST_ID, info.cust_id);

        jQuery('#vrSrchCustCdE3').trigger('custom', ['customEvent']);
    }


    ParcelChagnedCallBack(e){

        this.#parcelCd = e.target.value;

        this.#grid.Clear();

        if(!cfn_isEmpty(this.#parcelCd) && !cfn_isEmpty(this.#custId)){
            this.SetParcelSeq();
        }

        //this.SearchClickEventCallback();
    }


    SaveClickEventCallBack(e, isProgressBar=true){
        try {
        	let vm = this;
        	let formData = new FormData(document.getElementById(`${this.#formId}`));
        	let selectedIdx = this.#grid.GetCheckedList();
            let gridData = this.#grid.GetSelectedRowData();
            
            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 행을 선택해야 합니다.`;
            }

            formData.append('ROW_COUNT', selectedIdx.length);


            for(let i=0; i < selectedIdx.length; i++){
            	let rowIdx = selectedIdx[i];
            	
            	let param = {

            			PARCEL_TYPE      :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('PARCEL_TYPE'))
                        , BOX_CD    	 :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('BOX_CD'))
                        , BOX_PRICE      :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('BOX_PRICE'))
                        , USE_YN    	 :  this.#grid.getValue(rowIdx, this.#grid.GetColumnIdx('USE_YN'))

                    }

                    formData.append(`PARCEL_TYPE`      +   `_${i}`, param['PARCEL_TYPE']);
                    formData.append(`BOX_CD`   		   +   `_${i}`, param['BOX_CD']);
                    formData.append(`BOX_PRICE`        +   `_${i}`, param['BOX_PRICE']);
                    formData.append(`USE_YN`       	   +   `_${i}`, param['USE_YN']);
            	
            }
            
            jQuery.ajax({
                url : "/WMSDF001/saveE3.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    if(isProgressBar) cfn_viewProgress();
                },
                complete: function(){
                    if(isProgressBar) cfn_closeProgress();
                },
                success : function(data){
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(data["MSG"]);
                    }
                    // Success ...
                    else{
                        alert(DICTIONARY['save_success']);
                        vm.SearchClickEventCallback();
                    }
                }
            });

        } catch (error) {
            alert(error);
        }

    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

    	 let dlvSeq  = jQuery("#vrSrchParcelSeqE3").val();
         let dlvCd   = jQuery('#vrSrchParcelComCdE3').val();
         let custId  = jQuery('#vrSrchCustIdE3').val();
    	
    	if(cfn_isEmpty(custId) || cfn_isEmpty(dlvCd) || cfn_isEmpty(dlvSeq)){
            alert(`택배구분, 택배SEQ, 화주 정보를 선택하세요.`);
            return 
        }
    	
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return jQuery.ajax({
            url : "/WMSDF001/listE3.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }


//#endregion


//#region   :: Validation
    


//#endregion


//#region   :: Utility


//#endregion


}