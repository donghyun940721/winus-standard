/*******************************************************************************
 *
 * HTML 입력 폼을 검증하는 스크립트
 * prototype 1.6.0.3.js, common.js 필수,
 *
 * 입력폼의 ID와 검증 문자열을 통해 입력폼의 입력값을 검증한다.
 *
 ******************************************************************************/

/** 사용법
 - 추가 정보 -
 prototype 스크립트가 본 스크립트 사용시에 같이 include되어야 한다.
 Validator를 prototype없이 독립적으로 사용하려면 $(), String.strip(), Hash, Template들을 수정해야 함

 함수 :
    clear() - 검증 내역을 초기화 한다.
    addItem() - 체크할 아이템을 추가한다.
    validateForm() - 에러 체크 후 부적합한 입력이 있으면 에러 메시지 창을 연 뒤,
        에러 유무를 반환한다.
    isValidatedForm() - 검증 후 에러 유무를 반환한다.
    setStackedMsg() - Validator의 에러 메시지를 한번에 출력하도록 설정한다.
    setErrMsgPerAlert(n) - Validator의 에러 메시지를 한번에 출력하도록 설정하였을
        때, 한번에 몇 개의 메시지를 출력할지 설정한다. 기본 값은 10개


 검증 문자열 구성 :
    필수여부(Y/N):검증식:포커스여부(Y/N):폼 아이템 이름:에러 메시지:특수문자 불허여부

    예)
        "Y:Number=8.2:Y:가격:"
        "Y:Number=8.2:Y:가격:잘못된 가격입니다."
        "N:MaxLength=256:Y:회사명:회사명이 너무 깁니다."
        "Y:Equal=fldPw2:Y:패스워드::N"

 검증식 :
    MaxLength - 최대 길이를 체크한다. ex) MaxLength=8
    MinLength - 최소 길이를 체크한다. ex) MinLength=2
    MinMaxLength - 최소, 최대 길이를 체크한다. ex) MinMaxLength=2=8
    MaxByteLength - 최대 byte 길이를 체크한다. ex) MaxByteLength=8
    MinByteLength - 최소 byte 길이를 체크한다. ex) MinByteLength=2
    MinMaxByteLength - 최소, 최대 byte 길이를 체크한다. ex) MinMaxByteLength=2=8
    Alphabet - 영문자로만 입력되었는지 체크한다.   ex) Alphabet[=8]
    AlphaNum - 영문자와 숫자로만 입력되었는지 체크한다.   ex) AlphaNum[=8]
    Figure - 숫자로만 입력되었는지 체크하다.   ex) Figure[=8]
    Number - 유리수로 입력되었는지 체크한다.    ex) Number[=8.2]
    Id - 영문자로 시작하고 영문자와 숫자로 이루러진 아이디 형식으로 입력되었는지 체크한다.  ex) Id[=8]
    Email - 이메일 형식으로 입력되었는지 체크한다.   ex) Email[=16]
    Selected - 콤보박스에 선택값이 있는지 체크한다.
    Checked - 라디오 버튼에 체크값이 있는지 체크하다. 아이템 추가시 입력 폼의 id 대신 name을 넣는다.
    		ex) validator.addItem("radio", "N:Checked:Y:라디오:");
    Ssn - 주민번호 형식에 맞는지 체크한다.
    BadWord - 금칙어가 포함되어 있는지 체크한다. 아래의 BAD_WORD 배열에 등록된 금칙어나, 다음과 같은 식으로 배열을 받아서 사용
        if(!cfn_isEmpty("$!codeMgr.badWord")) {
            val.setBadWord("$!codeMgr.badWord");
        }
    Equal - 두 입력값이 같은지 체크한다. ex) val.addItem("fldPw1", "Y:Equal=fldPw2:Y:패스워드:");
    date - 날짜인지를 체크한다. YYYYMMDD와 같이 8자리의 숫자로 이루어진 날짜만 체크 가능
    period - 입력받은 두 날짜중 먼저 입력받은 날짜가 나중에 입력받은 날짜보다 빠른지 체크(년/월/일단위 가능)
    		 기간 조건이 있으면 두 날짜의 기간도 체크한다.
              ex) validator.addItem("fldSrchStartDate", "N:period=fldSrchStartDate=fldSrchEndDate:Y:등록일:"); // 앞 날짜가 더 빠른지 체크
                  validator.addItem("fldSrchStartDate", "N:period=fldSrchStartDate=fldSrchEndDate=m1:Y:등록일:");	// 두 날짜 차이가 1달이내인지 추가 체크
	year - 년도인지 체크
	periodyear - 년도 기간인지 체크
	yearmonth - 올바른 년월인지 체크
	periodmonth - 입력받은 두 년월 중 먼저 입력받은 년월이 나중에 입력받은 년월보다 빠른지 체크(년/월단위 가능)
			기간 조건이 있으면 두 년월의 기간도 체크한다.
              ex) validator.addItem("fldSrchStartDate", "N:periodmonth=fldSrchStartDate=fldSrchEndDate:Y:등록일:"); // 앞 날짜가 더 빠른지 체크
                  validator.addItem("fldSrchStartDate", "N:periodmonth=fldSrchStartDate=fldSrchEndDate=m1:Y:등록일:");	// 두 날짜 차이가 1달이내인지 추가 체크
    periodyearmonth - 입력받은 두 년/년월 중 먼저 입력받은 년/년월이 나중에 받은 년/년월 보다 빠른지 체크(년이면 년, 월이면 월로 두 입력 포멧이 같아야함)
    		기간 조건이 있으면 기간도 체크한다.
    		ex) validator.addItem("fldSrchStartDate", "N:periodyearmonth=fldSrchStartDate=fldSrchEndDate:Y:등록일:"); // 앞 날짜가 더 빠른지 체크
                validator.addItem("fldSrchStartDate", "N:periodyearmonth=fldSrchStartDate=fldSrchEndDate=y2=m18:Y:등록일:");	// 두 날짜 차이가 2년이나 18개월이내인지 추가 체크

 포커스 여부 :
    에러시 해당 입력폼에 포커스를 줄 것인가의 여부, Y이면 에러시 포커스를 준다.

 폼 아이템 이름 :
    해당 입력 폼의 이름을 지정한다.

 에러 메시지 :
    에러시에 출력할 메시지. 없을 경우 기본 메시지가 출력된다.

 예 :
    var val = new Validator("testForm");
    val.addItem("testInput", "Y:Alphabet:Y:Test Input:");
    val.addItem("fldPw1", "Y:Equal=fldPw2:Y:패스워드:");
    val.validateForm();
*/


// 검증식과 그에 해당하는 검증함수 이름을 저장하는 배열
// 검증식 추가시에 이 배열에 검증식과 함수를 추가하고 검증 함수를 추가 함
// 배열의 검증식은 소문자로만 입력해야 함
var OPERATOR = { "maxlength"        : "isMaxLength",             // 최대 길이
                 "minlength"        : "isMinLength",             // 최소 길이
                 "minmaxlength"     : "isMinMaxLength",          // 최대/최소 길이
                 "maxbytelength"    : "isMaxByteLength",         // 최대 byte
                 "minbytelength"    : "isMinByteLength",         // 최소 byte
                 "minmaxbytelength" : "isMinMaxByteLength",      // 최대/최소 byte
                 "hangul"         	: "isHangul",              	 // 한글
                 "alphabet"         : "isAlphabet",              // 영문자
                 "alphanum"         : "isAlphaNum",              // 영문자 + 숫자
                 "figure"           : "isFigure",                // 숫자
                 "number"           : "isNumber",                // 수
                 "id"               : "isId",                    // 영문자로 시작, 영문자 + 숫자
                 "email"            : "isEmail",                 // 이메일
                 "selected"         : "isSelected",              // 콤보박스 선택 여부
                 "checked"          : "isChecked",               // 라디오 버튼 체크 여부
                 "ssn"              : "isSsn",                   // 주민번호 체크
                 "badword"          : "isContainBadWord",        // 금칙어 포함 여부
                 "equal"            : "isEqual",                 // 두 값이 같은지(문자) 체크
                 "notequal"         : "isNotEqual",              // 두 값이 다른지(문자) 체크
                 "date"				: "isDate",					 // 날짜인지 체크
                 "period"			: "isPeriod",				 // 앞 날짜가 뒷 날짜보다 빠른지 체크(년/월/일 단위 기간도 체크)
                 "year"				: "isYear",					 // 년도인지 체크
                 "periodyear"		: "isPeriodYear",			 // 앞 년도가 뒷 년도보다 빠른지 체크(년 단위 기간도 체크)
                 "yearmonth"		: "isYearMonth",			 // 올바른 년월인지 체크
                 "yearmonth2"		: "isYearMonth2",			 // 올바른 년월인지 체크 (추가 : chsong)
                 "periodmonth"		: "isPeriodMonth",			 // 앞 년월이 뒷 년월보다 빠른지 체크(월 단위 기간도 체크)
                 "periodyearmonth"	: "isPeriodYearMonth"		 // 앞 년이나 년월이 뒷 년이나 년월보다 빠른지 체크(각각 년/월 단위 기간도 체크 : SPIDC특화)
               };

/**
 * 기본 검증 메시지 헤더 vm에 정의되어 있음
 */

var DEFAULT_MSG_REQUIRED = new Template(strings['DEFAULT_MSG_REQUIRED']);
var DEFAULT_MSG_MAX_LENGTH = new Template(strings['DEFAULT_MSG_MAX_LENGTH']);
var DEFAULT_MSG_MIN_LENGTH = new Template(strings['DEFAULT_MSG_MIN_LENGTH']);
var DEFAULT_MSG_MIN_MAX_LENGTH = new Template(strings['DEFAULT_MSG_MIN_MAX_LENGTH']);
var DEFAULT_MSG_MAX_BYTE_LENGTH = new Template(strings['DEFAULT_MSG_MAX_BYTE_LENGTH']);

var DEFAULT_MSG_MIN_BYTE_LENGTH = new Template(strings['DEFAULT_MSG_MIN_BYTE_LENGTH']);
var DEFAULT_MSG_MIN_MAX_BYTE_LENGTH = new Template(strings['DEFAULT_MSG_MIN_MAX_BYTE_LENGTH']);
var DEFAULT_MSG_HANGUL = new Template(strings['DEFAULT_MSG_HANGUL']);
var DEFAULT_MSG_ALPHABET = new Template(strings['DEFAULT_MSG_ALPHABET']);
var DEFAULT_MSG_ALPHABET_LENGTH = new Template(strings['DEFAULT_MSG_ALPHABET_LENGTH']);

var DEFAULT_MSG_ALPHANUM = new Template(strings['DEFAULT_MSG_ALPHANUM']);
var DEFAULT_MSG_ALPHANUM_LENGTH = new Template(strings['DEFAULT_MSG_ALPHANUM_LENGTH']);
var DEFAULT_MSG_FIGURE = new Template(strings['DEFAULT_MSG_FIGURE']);
var DEFAULT_MSG_FIGURE_LENGTH = new Template(strings['DEFAULT_MSG_FIGURE_LENGTH']);
var DEFAULT_MSG_NUMBER = new Template(strings['DEFAULT_MSG_NUMBER']);

var DEFAULT_MSG_NUMBER_LENGTH = new Template(strings['DEFAULT_MSG_NUMBER_LENGTH']);
var DEFAULT_MSG_NUMBER_DECIMAL_LENGTH = new Template(strings['DEFAULT_MSG_NUMBER_DECIMAL_LENGTH']);
var DEFAULT_MSG_ID = new Template(strings['DEFAULT_MSG_ID']);
var DEFAULT_MSG_ID_LENGTH = new Template(strings['DEFAULT_MSG_ID_LENGTH']);
var DEFAULT_MSG_EMAIL = new Template(strings['DEFAULT_MSG_EMAIL']);

var DEFAULT_MSG_EMAIL_LENGTH = new Template(strings['DEFAULT_MSG_EMAIL_LENGTH']);
var DEFAULT_MSG_SELECT = new Template(strings['DEFAULT_MSG_SELECT']);
var DEFAULT_MSG_CHECK = new Template(strings['DEFAULT_MSG_CHECK']);
var DEFAULT_MSG_SSN = new Template(strings['DEFAULT_MSG_SSN']);
var DEFAULT_MSG_WRONG_SSN = new Template(strings['DEFAULT_MSG_WRONG_SSN']);

var DEFAULT_MSG_BAD_WORD = new Template(strings['DEFAULT_MSG_BAD_WORD']);
var DEFAULT_MSG_NOT_EQUAL = new Template(strings['DEFAULT_MSG_NOT_EQUAL']);
var DEFAULT_MSG_EQUAL = new Template(strings['DEFAULT_MSG_EQUAL']);
var DEFAULT_MSG_NOT_CERT_NO = new Template(strings['DEFAULT_MSG_NOT_CERT_NO']);
var DEFAULT_MSG_NOT_CERT_NO_LENGTH = new Template(strings['DEFAULT_MSG_NOT_CERT_NO_LENGTH']);

var DEFAULT_MSG_NOT_DATE = new Template(strings['DEFAULT_MSG_NOT_DATE']);
var DEFAULT_MSG_NOT_DATE1 = new Template(strings['DEFAULT_MSG_NOT_DATE1']);
var DEFAULT_MSG_NOT_YEAR = new Template(strings['DEFAULT_MSG_NOT_YEAR']);
var DEFAULT_MSG_NOT_YEAR1 = new Template(strings['DEFAULT_MSG_NOT_YEAR1']);
var DEFAULT_MSG_NOT_YEAR_MONTH = new Template(strings['DEFAULT_MSG_NOT_YEAR_MONTH']);

var DEFAULT_MSG_NOT_YEAR_MONTH1 = new Template(strings['DEFAULT_MSG_NOT_YEAR_MONTH1']);
var DEFAULT_MSG_NOT_PERIOD = new Template(strings['DEFAULT_MSG_NOT_PERIOD']);
var DEFAULT_MSG_NOT_PERIOD_YEAR = new Template(strings['DEFAULT_MSG_NOT_PERIOD_YEAR']);
var DEFAULT_MSG_NOT_PERIOD_YEAR_MONTH = new Template(strings['DEFAULT_MSG_NOT_PERIOD_YEAR_MONTH']);
var DEFAULT_MSG_NOT_VALID_PERIOD = new Template(strings['DEFAULT_MSG_NOT_VALID_PERIOD']);

var DEFAULT_MSG_NOT_EQUAL_PERIOD_FORMAT = new Template(strings['DEFAULT_MSG_NOT_EQUAL_PERIOD_FORMAT']);
var DEFAULT_MSG_NOT_VALID = new Template(strings['DEFAULT_MSG_NOT_VALID']);
var DEFAULT_MSG_SPECIAL_CHAR = new Template(strings['DEFAULT_MSG_SPECIAL_CHAR']);

//  내부 연산용 정규식
var REGEXP_FIGURE = /[^0-9]/g;          // 숫자

// 기본 검증 정규식
var DEFAULT_REGEXP_ALPHABET = /^[a-zA-Z]+$/;        // 영문자(알파벳)
var DEFAULT_REGEXP_ALPHANUM = /^[0-9a-zA-Z]+$/;     // 영문자 + 숫자
var DEFAULT_REGEXP_FIGURE = /^[0-9]+$/;              // 숫자
var DEFAULT_REGEXP_NUMBER = /^(\-)?[0-9]*(\.[0-9]*)?$/;    // 수
var DEFAULT_REGEXP_HANGUL = /^[\uac00-\ud79fㄱ-ㅎ]+$/;    // 한글 UTF-8은 ^[\x{3131}-\x{318E}]|[\x{AC00}-\x{D7A3}]+$
var DEFAULT_REGEXP_EMAIL = /^((\w|[\-\.])+)@((\w|[\-\.])+)\.([A-Za-z]+)$/;  // 이메일
var DEFAULT_REGEXP_ID = /^[a-zA-z]{1}[0-9a-zA-Z]+$/;    // 영문자로 시작하고 영문자와 숫자로 이루러진 아이디
var DEFAULT_REGEXP_CERT_NO = /^[a-zA-z]{1}[0-9a-zA-Z\-]+$/;    // 영문자로 시작하고 영문자와 숫자와 빼기부호로 이루러진 아이디
var DEFAULT_REGEXP_SEP_CHAR = /[~!@\#$%%^&*\\=+|:;?"<,.>']+/g; // 걸러낼 특수문자

// 임시 금칙어
Validator.BAD_WORD = ["바보", "멍청이"];

// 생성자
function Validator(objForm) {
    this.checkForm = objForm;
    this.validatorList = new Array();
    this.msg = "";
    this.arrMsg = new Array();
    this.IS_RETURN_MSG_IMMEDIATELY = true;
    this.ERR_MSG_PER_ALERT = 10;
    this.FOCUS_ITEM = "";
}

/**
 * 1. 이    름 : clear
 * 2. 설    명 : 검증 데이터를 클리어하다.
 *               이미 만들어진 Validator를 재사용 하거나 필요시에 기존 검증 데이터를
 *               클리어한다.
 * 3. 인    자 :
 * 4. 반 환 값 :
 * 5. 사 용 예 : val.clear();
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 28    이민재     최초 등록
 */
Validator.prototype.clear = function () {
    this.validatorList = new Array();   // Validator에 등록된 아이템 리스트
    this.msg = "";                      // 에러 메시지
    this.arrMsg = new Array();          // 에러 메시지 배열
    this.IS_RETURN_MSG_IMMEDIATELY = true;  // 에러발생시 메시지 즉시 출력 여부
    this.ERR_MSG_PER_ALERT = 10;        // 에러 메시지 출력시 한번에 출력할 메시지 수
    this.FOCUS_ITEM = "";               // 에러 발생시 포커스를 줄 아이템 이름
};


/**
 * 1. 이    름 : addItem
 * 2. 설    명 : 검증할 대상을 추가한다.
 * 3. 인    자 : String - strId : 검증할 element의 ID
 *               String - strVal : 검증 문자열
 * 4. 반 환 값 :
 * 5. 사 용 예 : val.addItem("testInput", "Y:Alphabet:Y:Test Input:");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 22    이민재     최초 등록
 */
Validator.prototype.addItem = function (strId, strVal) {
    this.validatorList.push({"id" : strId, "str" : strVal});
};


/**
 * 1. 이    름 : validateForm
 * 2. 설    명 : 폼을 검증한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Boolean : 정상 검증 여부
 * 5. 사 용 예 : val.validateForm()
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 22    이민재     최초 등록
 */
Validator.prototype.validateForm = function () {
    var rtn = this.executeValidation();

    // 에러 발생시
    if (!rtn) {
        this.msg = "";

        // 에러 메시지를 합친다.
        for (var i = 0; i < this.arrMsg.length; i++) {
            this.msg +=  this.arrMsg[i];
        }

        // 에러 메시지를 출력한다.
        alert(this.msg);

        // 에러가 발생한 첫 아이템에 포커스를 준다.
        if (this.FOCUS_ITEM != "") {
            $(this.FOCUS_ITEM).focus();
        }
    }

    return rtn;
};

/**
 * 1. 이    름 : isValidatedForm
 * 2. 설    명 : 입력받은 폼을 검증 여부를 반환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Boolean : 정상 검증 여부, 문제가 없으면 true
 * 5. 사 용 예 : val.isValidateForm();
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 22    이민재     최초 등록
 */
Validator.prototype.isValidatedForm = function () {
    return this.executeValidation();
};

/**
 * 1. 이    름 : setStackedMsg
 * 2. 설    명 : 에러 발생시 즉시 출력하지 않고, 검증 완료후 모아서 출력하게
 *               설정한다. 기본적으로 에러발생시 즉시 출력하도록 되어있다.
 * 3. 인    자 :
 * 4. 반 환 값 :
 * 5. 사 용 예 : val.setStackedMsg();
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 28    이민재     최초 등록
 */
Validator.prototype.setStackedMsg = function () {
    this.IS_RETURN_MSG_IMMEDIATELY = false;
};

/**
 * 1. 이    름 : setErrMsgPerAlert
 * 2. 설    명 : 검증 완료후 모아서 에러메시지를 출력하도록 설정 했을 때,
 *               번에 출력할 메시지 갯수를 설정한다.
 * 3. 인    자 : Number - 한번에 출력할 메시지 갯수. 기본 10개
 * 4. 반 환 값 :
 * 5. 사 용 예 : val.setErrMsgPerAlert(20);
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 28    이민재     최초 등록
 */
Validator.prototype.setErrMsgPerAlert = function (n) {
    this.ERR_MSG_PER_ALERT = n;
};

/**
 * 1. 이    름 : setBadWord
 * 2. 설    명 : 금칙어를 지정한다.
 * 3. 인    자 : String - 금칙어 목록(,로 나뉘어진)
 * 4. 반 환 값 :
 * 5. 사 용 예 : val.setBadWord("바보, 머저리, 병신");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 02. 04    이민재     최초 등록
 */
Validator.prototype.setBadWord = function (a) {
    var bw = a.split(",");
    this.BAD_WORD = bw;
};

// 실제 검증
Validator.prototype.executeValidation = function () {
    var list = this.validatorList;
    var item = null;
    var flag = true;

    if (typeof this.checkForm == 'undefined') {
        this.msg = "검증할 대상 폼이 존재하지 않습니다.";
        return false;
    }

    if (list == null || list.length == 0) {
        this.msg = "검증할 대상이 존재하지 않습니다.";
        return false;
    }

    for (var i = 0; i < list.length; i++) {
        item = list[i];

        // 들록된 아이템을 검증하고 반환값을 받는다.
        flag = this.validateItem(item.id, item.str);

        // 에러시
        if (!flag) {

            // 에러 메시지 등록
            this.arrMsg.push(this.msg + "\n");

            // 에러 메시지 즉시 반환여부 체크
            if (this.IS_RETURN_MSG_IMMEDIATELY) {
                return false;
            }

            // 에러 메시지 갯수 체크
            if (this.arrMsg.length >= this.ERR_MSG_PER_ALERT) {
                return false;
            }
        }
    }
    return true;
};

// 각각의 아이템에 대한 검증 실행
Validator.prototype.validateItem = function (strId, strVal) {

    var rtn = true;
    var arr = strVal.split(':');

    // 필수 입력 여부 체크
    if (arr[0].toLowerCase() == 'y') {
        if (this.isEmpty(strId, arr)) {
            if (arr[2].toLowerCase() == "y" && this.FOCUS_ITEM == "") {
                this.FOCUS_ITEM = strId;
                $(strId).value = "";
            }
            return false;
        }
    }
    // 입력값의 특수문자 제거
    if (arr.length > 5 && arr[5].toLowerCase() == 'y') {
		if(this.containSpecialKey(strId)) {
	    	if (arr[2].toLowerCase() == "y" && this.FOCUS_ITEM == "") {
	        	this.FOCUS_ITEM = strId;
	    	}
	    	var arrMsg = { "name" : arr[3]};
	    	this.msg = this.getItemValMsg("", arrMsg, DEFAULT_MSG_SPECIAL_CHAR);

	    	return false;
		}
    }

    var operArr = arr[1].split("=");
    var oper = operArr[0].toLowerCase();
    // 검증
    var op = 'this.' + OPERATOR[oper] + '(strId, arr)';
    rtn = eval(op);
    // 에러가 있고 포커스를 가도록 설정되어 있다면 포커스를 줄 첫 아이템의 ID를 저장
    if (!rtn) {
        if (arr[2].toLowerCase() == "y" && this.FOCUS_ITEM == "") {
            this.FOCUS_ITEM = strId;
        }
    }

    return rtn;
};

////////////////////////////////////////////////////////////////////////////////
//
// 검증시 내부적으로 사용하는 함수들
//
////////////////////////////////////////////////////////////////////////////////


// 빈 문자열인지 판정
Validator.prototype.isEmptyS = function (str) {
    if (str.strip().length < 1) {
        return true;
    } else {
        return false;
    }
};

// 에러 메시지 생성
Validator.prototype.getItemValMsg = function (userMsg, arrMsg, tmplDefMsg) {
    if (cfn_isUndefined(userMsg)) {
        userMsg = "";
    }

    // 사용자 메시지가 없으면 기본 메시지를 반환한다.
    if (this.isEmptyS(userMsg)) {
        return tmplDefMsg.evaluate(arrMsg);
    } else {
        return userMsg;
    }
};

// 입력값중 숫자만 반환
Validator.prototype.getFigure = function (str) {
    return (str + "").replace(REGEXP_FIGURE, "");
};

// 입력값의 byte 길이를 반환
Validator.prototype.byteLength = function (str) {
    var cnt = 0;
    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 127) {
            cnt += 2;
        } else {
            cnt++;
        }
    }

    return cnt;
};

Validator.prototype.trim = function (str) {
    str.replace(/(^\s*)|(\s*$)/g, "");
}


////////////////////////////////////////////////////////////////////////////////
//
//  검증 함수
//
////////////////////////////////////////////////////////////////////////////////

// 입력값 유무 확인
Validator.prototype.isEmpty = function (strId, arr) {
    if (this.isEmptyS($F(strId))) {
        var arrMsg = { "name" : arr[3]};
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_REQUIRED);
        return true;
    }
    return false;
};
// 특수문자 입력여부 체크
Validator.prototype.containSpecialKey = function (strId) {
	return (DEFAULT_REGEXP_SEP_CHAR).test($F(strId)) ? true : false;
};

// 입력값의 최대 길이 확인
Validator.prototype.isMaxLength = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if ($F(strId).length > oper[1]) {
        var arrMsg = { "name" : arr[3], "max" : oper[1]};

        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_MAX_LENGTH);

        return false;
    }

    return true;
};

// 입력값의 최소 길이 확인
Validator.prototype.isMinLength = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if ($F(strId).length < oper[1]) {
        var arrMsg = { "name" : arr[3], "min" : oper[1]};

        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_MIN_LENGTH);
        return false;
    }
    return true;
};

// 입력값의 최소~최대 길이 확인
Validator.prototype.isMinMaxLength = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if ($F(strId).length < oper[1] || $F(strId).length > oper[2]) {
        var arrMsg = { "name" : arr[3], "min" : oper[1], "max" : oper[2]};

        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_MIN_MAX_LENGTH);
        return false;
    }
    return true;
};

// 입력값의 최대 byte 확인
Validator.prototype.isMaxByteLength = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (this.byteLength($F(strId)) > oper[1]) {
        var arrMsg = { "name" : arr[3], "max" : oper[1]};

        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_MAX_BYTE_LENGTH);
        return false;
    }
    return true;
};

// 입력값의 최소 byte 길이 확인
Validator.prototype.isMinByteLength = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (this.byteLength($F(strId)) < oper[1]) {
        var arrMsg = { "name" : arr[3], "min" : oper[1]};

        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_MIN_BYTE_LENGTH);
        return false;
    }
    return true;
};

// 입력값의 최소~최대 byte 길이 확인
Validator.prototype.isMinMaxByteLength = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (this.byteLength($F(strId)) < oper[1] || this.byteLength($F(strId)) > oper[2]) {
        var arrMsg = { "name" : arr[3], "min" : oper[1], "max" : oper[2]};
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_MIN_MAX_BYTE_LENGTH);

        return false;
    }
    return true;
};


// 입력값의 한글 여부 확인
Validator.prototype.isHangul = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (oper.length == 1) {          // 최대 길이 값이 없을 경우
        if (!(DEFAULT_REGEXP_HANGUL).test($F(strId))) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_HANGUL);
            return false;
        }

    } else if (oper.length == 2) {	// 최대 길이 값이 있을 경우
        if (!(DEFAULT_REGEXP_HANGUL).test($F(strId)) || this.byteLength($F(strId)) > oper[1]) {
            var arrMsg = { "name" : arr[3], "max" : oper[1] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_HANGUL_LENGTH);
            return false;
        }
    }

    return true;
};

// 입력값의 알파벳 여부 확인
Validator.prototype.isAlphabet = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (oper.length == 1) {          // 최대 길이 값이 없을 경우
        if (!(DEFAULT_REGEXP_ALPHABET).test($F(strId))) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_ALPHABET);
            return false;
        }

    } else if (oper.length == 2) {	// 최대 길이 값이 있을 경우
        if (!(DEFAULT_REGEXP_ALPHABET).test($F(strId)) || $F(strId).length > oper[1]) {
            var arrMsg = { "name" : arr[3], "max" : oper[1] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_ALPHABET_LENGTH);
            return false;
        }
    }

    return true;
};

// 입력값의 알파벳 or 숫자 여부 확인
Validator.prototype.isAlphaNum = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (oper.length == 1) {          // 최대 길이 값이 없을 경우
        if (!(DEFAULT_REGEXP_ALPHANUM).test($F(strId))) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_ALPHANUM);
            return false;
        }
    } else if (oper.length == 2) {	// 최대 길이 값이 있을 경우
        if (!(DEFAULT_REGEXP_ALPHANUM).test($F(strId)) || $F(strId).length > oper[1]) {
            var arrMsg = { "name" : arr[3], "max" : oper[1] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_ALPHANUM_LENGTH);
            return false;
        }
    }

    return true;
};

// 입력값의 숫자 여부 확인
Validator.prototype.isFigure = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (oper.length == 1) {          // 최대 길이 값이 없을 경우
        if (!(DEFAULT_REGEXP_FIGURE).test($F(strId))) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_FIGURE);
            return false;
        }
    } else if (oper.length == 2) {	// 최대 길이 값이 있을 경우
        if (!(DEFAULT_REGEXP_FIGURE).test($F(strId)) || $F(strId).length > oper[1]) {
            var arrMsg = { "name" : arr[3], "max" : oper[1] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_FIGURE_LENGTH);
            return false;
        }
    }

    return true;
};

// 입력값의 수 여부 확인
Validator.prototype.isNumber = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (oper.length == 1) {              // 최대 길이 값이 없을 경우
        if (!(DEFAULT_REGEXP_NUMBER).test($F(strId))) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NUMBER);
            return false;
        }
    } else if (oper.length == 2) {	   // 최대 길이 값이 있을 경우
        var op = oper[1].split(".");

    	if (op.length == 1) {          // 최대 길이 값이 정수부만 있을 경우
        	if (!(DEFAULT_REGEXP_NUMBER).test($F(strId)) ||
            	   this.getFigure($F(strId)).length > oper[1]) {
                var arrMsg = { "name" : arr[3], "max" : oper[1] };
                this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NUMBER_LENGTH);
                return false;
            }
        } else if (oper.length == 2) {	// 최대 길이 값이 소수부도 있을 경우
            var val = $F(strId).split(".");

            if (val.length == 1) {       // 입력값이 정수부만 있을 경우
                if (!(DEFAULT_REGEXP_NUMBER).test($F(strId)) ||
                    this.getFigure(val[0]).length > op[0]) {
                    var arrMsg = { "name" : arr[3], "max" : op[0] };
                    this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NUMBER_LENGTH);
                    return false;
                }
            } else if (oper.length == 2) {	// 입력값이 소수부도 있을 경우
                if (!(DEFAULT_REGEXP_NUMBER).test($F(strId)) ||
                    this.getFigure($F(strId)).length > op[0] || val[1].length > op[1] ) {
                    var arrMsg = { "name" : arr[3], "max" : op[0], "decimal" : op[1]};
                    this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NUMBER_DECIMAL_LENGTH);
                    return false;
                }
            }
        }
    }

    return true;
};

// 입력값의 아이디 여부 확인
Validator.prototype.isId = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (oper.length == 1) {      // 최대 길이 값이 없는 경우
        if (!(DEFAULT_REGEXP_ID).test($F(strId))) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_ID);
            return false;
        }
    } else if (oper.length == 2) {      // 최대 길이 값이 있는 경우
        if (!(DEFAULT_REGEXP_ID).test($F(strId)) || $F(strId).length > oper[1]) {
            var arrMsg = { "name" : arr[3], "max" : oper[1] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_ID_LENGTH);
            return false;
        }
    }
    return true;
};

// 입력값의 이메일 여부 확인
Validator.prototype.isEmail = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if (oper.length == 1) {      // 최대 길이 값이 없는 경우
        if (!(DEFAULT_REGEXP_EMAIL).test($F(strId))) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_EMAIL);
            return false;
        }
    } else if (oper.length == 2) {      // 최대 길이 값이 있는 경우
        if (!(DEFAULT_REGEXP_EMAIL).test($F(strId)) || $F(strId).length > oper[1]) {
            var arrMsg = { "name" : arr[3], "max" : oper[1] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_EMAIL_LENGTH);
            return false;
        }
    }
    return true;
};

// Select 입력의 선택 여부 확인
Validator.prototype.isSelected = function (strId, arr) {
    var selIdx = $(strId).selectedIndex;

    if (selIdx < 0 || this.isEmptyS($F(strId))) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_SELECT);

        return false;
    }

    return true;
};

// Radio의 체크여부 확인
Validator.prototype.isChecked = function (strId, arr) {
    var ele = Form.getInputs(this.checkForm, "radio", strId);

    if (typeof(ele[0]) != "undefined") {
        // 라디오 버튼이 2개 이상일 경우
        for (var idx = 0 ; idx < ele.length ; idx++) {
            if (ele[idx].checked == true) {
                return true;
            }
        }
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_CHECK);

        return false;
    } else {
        // 라디오 버튼이 1개일 경우
        if (!ele.checked) {
            var arrMsg = { "name" : arr[3] };
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_CHECK);
            return false;
        }
    }

    return true;
};

// 주민번호 체크
Validator.prototype.isSsn = function (strId, arr) {
    var ssn = $F(strId);

    if (ssn == null || ssn.strip().length != 13 || !this.isFigure(strId, arr))  {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_SSN);
        return false;
    }
    var jNum1 = ssn.substr(0, 6);
    var jNum2 = ssn.substr(6);
    var bYear = (jNum2.charAt(0) <= "2") ? "19" : "20";
    bYear += jNum1.substr(0, 2);

    var bMonth = jNum1.substr(2, 2) - 1;
    var bDate = jNum1.substr(4, 2);
    var bSum = new Date(bYear, bMonth, bDate);

    if (bSum.getYear() % 100 != jNum1.substr(0, 2) || bSum.getMonth() != bMonth || bSum.getDate() != bDate) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_SSN);

        return false;
    }

    var total = 0;
    var temp = {};

    for (var i = 1; i <= 6; i++) {
        temp[i] = jNum1.charAt(i - 1);
    }

    for (var i = 7; i <= 13; i++) {
        temp[i] = jNum2.charAt(i - 7);
    }

    for (var i = 1; i <= 12; i++) {
        var k = i + 1;

        if (k >= 10) {
            k = k % 10 + 2;
        }

        total = total + (temp[i] * k);
    }

    var last_num = (11 - (total % 11)) % 10;

    if (last_num != temp[13]) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_WRONG_SSN);

        return false;
    }

    return true;
};

// 금칙어 포함 여부 검사
Validator.prototype.isContainBadWord = function (strId, arr) {
    for(var i = 0; i < this.BAD_WORD.length; i++) {
        if($F(strId).indexOf(this.BAD_WORD[i]) > -1) {
            var arrMsg = {"name" : arr[3], "word" : this.BAD_WORD[i]};
            this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_BAD_WORD);

            return false;
        }
    }
    return true;
}

// 두 값이 같은지 여부 검사
Validator.prototype.isEqual = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if ($F(strId) != $F(oper[1])) {
        var arrMsg = { "name" : arr[3]};
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_EQUAL);
        return false;
    }

    return true;
}

// 두 값이 다른지 여부 검사
Validator.prototype.isNotEqual = function (strId, arr) {
    if (this.isEmpty(strId, arr)) {
        return true;
    }

    var oper = arr[1].split("=");

    if ($F(strId) == $F(oper[1])) {
        var arrMsg = { "name" : arr[3]};
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_EQUAL);
        return false;
    }

    return true;
}

// 입력값의 날짜 여부 확인
Validator.prototype.isDate = function (strId, arr) {
	if (this.isEmpty(strId, arr)) {
        return true;
    }

	var d = $F(strId).trim().replace(/[^0-9]/g, "");

    if (!d.isDate()) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_DATE);
        return false;
    }
    return true;
}

// 입력값의 년도 여부 확인
Validator.prototype.isYear = function (strId, arr) {
	if (this.isEmpty(strId, arr)) {
        return true;
    }

	var d = $F(strId).trim().replace(/[^0-9]/g, "");

    if (d.length != 4) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR);
        return false;
    }
    return true;
}

// 입력받은 두 날짜중 앞의 입력값이 뒤의 입력값보다 빠른지 체크
Validator.prototype.isPeriod = function (strId, arr) {
	var oper = arr[1].split("=");
	$(oper[1]).value = $F(oper[1]).trim().replace(/[^0-9\-]/g, "");
	$(oper[2]).value = $F(oper[2]).trim().replace(/[^0-9\-]/g, "");
	var d1 = $F(oper[1]).trim().replace(/[^0-9]/g, "");
	var d2 = $F(oper[2]).trim().replace(/[^0-9]/g, "");
	var g = oper[3];

	if (this.isEmpty(strId, arr) && d2.trim().length == 0) {
        return true;
    }

	if(arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
		this.FOCUS_ITEM = oper[2];
        return false;
    }

    if ((d1.trim().length > 0 && d1.trim().length != 8) || (d1.trim().length == 8 && !d1.isDate())) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_DATE);
        return false;
    }

    if((d2.trim().length == 8 && !d2.isDate()) || ( d2.trim().length > 0 && d2.trim().length != 8)) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_DATE1);
		if (arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
			this.FOCUS_ITEM = oper[2];
		}
        return false;
    }
    if(d2.trim().length == 8 && d1.toDate() > d2.toDate()) {
    	var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_PERIOD);
        return false;
    }

    // 기간 조건 추가. 없으면 넘어감
    if(g != null && g.length > 1) {
    	var v = g.substring(1).parseInt();
    	var key = g.substring(0, 1);
    	if(v == null) {
			this.msg = "검증식의 날짜기간이 잘못되었습니다.\n관리자에게 문의하여주십시오";
			return false;
    	}
    	if(key.toLowerCase() == 'y') {			// 년단위 기간 체크
			if(d1.toDate().addYear(v) < d2.toDate()) {
				var arrMsg = { "name" : arr[3], "period" : '년', 'value' : v };
				this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_VALID_PERIOD);
				return false;
			}
    	} else if(key.toLowerCase() == 'm') {	// 월단위 기간 체크
			if(d1.toDate().addMonth(v) <  d2.toDate()) {
				var arrMsg = { "name" : arr[3], "period" : '개월', 'value' : v };
				this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_VALID_PERIOD);
				return false;
			}
    	} else if(key.toLowerCase() == 'd') {	// 일단위 기간 체크
			if(d1.toDate().addDay(v) <  d2.toDate() ) {
				var arrMsg = { "name" : arr[3], "period" : '일', 'value' : v };
				this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_VALID_PERIOD);
				return false;
			}
    	} else {
    		this.msg = "검증식의 기간조건이 잘못되었습니다.\n관리자에게 문의하여주십시오";
    		return false;
    	}
    } else if(g != null ){
    	this.msg = "검증식의 기간조건이 잘못되었습니다.\n관리자에게 문의하여주십시오";
    	return false;
    }

    return true;
}

// 입력받은 두 년도 중 앞의 입력값이 뒤의 입력값보다 빠른지 체크
Validator.prototype.isPeriodYear = function (strId, arr) {
	var oper = arr[1].split("=");
	$(oper[1]).value = $F(oper[1]).trim().replace(/[^0-9\-]/g, "");
	$(oper[2]).value = $F(oper[2]).trim().replace(/[^0-9\-]/g, "");
	var d1 = $F(oper[1]).trim().replace(/[^0-9]/g, "");
	var d2 = $F(oper[2]).trim().replace(/[^0-9]/g, "");
	var g = oper[3];

	if (this.isEmpty(strId, arr) && d2.trim().length == 0) {
        return true;
    }

	if(arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
		this.FOCUS_ITEM = oper[2];
        return false;
    }

    if (d1.trim().length != 4) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR);
        return false;
    }

    if(d2.trim().length != 4) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR1);
		if (arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
			this.FOCUS_ITEM = oper[2];
		}
        return false;
    }

    if(d1 > d2) {
    	var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_PERIOD_YEAR);
        return false;
    }
    // 기간 조건 추가. 없으면 넘어감
    if(g != null && g.length > 0) {
    	var v = g.parseInt();
    	if(v == null) {
			this.msg = "검증식의 기간이 잘못되었습니다.\n관리자에게 문의하여주십시오";
			return false;
    	}
		if(d1.parseInt() + (v - 1) < d2.parseInt()) {
				var arrMsg = { "name" : arr[3], "period" : '년', 'value' : v };
				this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_VALID_PERIOD);
				return false;
		}
    	
    } else if(g != null ){
    	this.msg = "검증식의 기간조건이 잘못되었습니다.\n관리자에게 문의하여주십시오";
    	return false;
    }

    return true;
}

// 입력값의 년월 여부 확인
Validator.prototype.isYearMonth = function (strId, arr) {
	if (this.isEmpty(strId, arr)) {
        return true;
    }
	$(strId).value = $F(strId).trim().replace(/[^0-9\-]/g, "");
	var d = $F(strId).trim().replace(/[^0-9-]/g, "");

    if (d.length != 6) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH);
        return false;
    }
	d = d + "01";
	if (!d.isDate()) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH);
        return false;
    }
    return true;
}

//입력값의 년월 여부 확인
//추가 : YYYY-MM 형태로 입력가능하게 수정 (기존: YYYYMM 형태만 입력가능했음)
//chsong
Validator.prototype.isYearMonth2 = function (strId, arr) {
	if (this.isEmpty(strId, arr)) {
        return true;
    }
	$(strId).value = $F(strId).trim().replace(/[^0-9\-]/g, "");
	var d = $F(strId).trim().replace(/[^0-9]/g, "");

    if (d.length != 6) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH);
        return false;
    }
	d = d + "01";
	if (!d.isDate()) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH);
        return false;
    }
    return true;
}

// 입력받은 두 년월 중 앞의 입력값이 뒤의 입력값보다 빠른지 체크
Validator.prototype.isPeriodMonth = function (strId, arr) {
	var oper = arr[1].split("=");
	$(oper[1]).value = $F(oper[1]).trim().replace(/[^0-9\-]/g, "");
	$(oper[2]).value = $F(oper[2]).trim().replace(/[^0-9\-]/g, "");
	var d1 = $F(oper[1]).trim().replace(/[^0-9]/g, "");
	var d2 = $F(oper[2]).trim().replace(/[^0-9]/g, "");
	var g = oper[3];

	if (this.isEmpty(strId, arr) && d2.trim().length == 0) {
        return true;
    }

	if(arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
		this.FOCUS_ITEM = oper[2];
        return false;
    }

    if (d1.trim().length != 6) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH);
        return false;
    }
	
	d1 = d1 + "01";
	if (!d1.isDate()) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH);
        return false;
    }

    if(d2.trim().length != 6) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH1);
		if (arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
			this.FOCUS_ITEM = oper[2];
		}
        return false;
    }
	
	d2 = d2 + "01"
	if (!d2.isDate()) {
        var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_YEAR_MONTH1);
		if (arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
			this.FOCUS_ITEM = oper[2];
		}
        return false;
    }

    if(d1 > d2) {
    	var arrMsg = { "name" : arr[3] };
        this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_PERIOD_YEAR_MONTH);
		if (arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
			this.FOCUS_ITEM = oper[2];
		}
        return false;
    }

    // 기간 조건 추가. 없으면 넘어감
    if(g != null && g.length > 0) {
		var v = g.substring(1).parseInt();
    	var key = g.substring(0, 1);
    	if(v == null) {
			this.msg = "검증식의 기간이 잘못되었습니다.\n관리자에게 문의하여주십시오";
			return false;
    	}
		if(key.toLowerCase() == 'y') {			// 년단위 기간 체크
			if(d1.toDate().addYear(v) < d2.toDate()) {
				var arrMsg = { "name" : arr[3], "period" : '년', 'value' : v };
				this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_VALID_PERIOD_YEAR);
				return false;
			}
    	} else if(key.toLowerCase() == 'm') {	// 월단위 기간 체크
			if(d1.toDate().addMonth(v-1) <  d2.toDate()) {
				var arrMsg = { "name" : arr[3], "period" : '개월', 'value' : v };
				this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_VALID_PERIOD);
				return false;
			}
		}
    	
    } else if(g != null ){
    	this.msg = "검증식의 기간조건이 잘못되었습니다.\n관리자에게 문의하여주십시오";
    	return false;
    }

    return true;
}


// 입력받은 두 년/년월 중 앞의 입력값이 뒤의 입력값보다 빠른지 체크
Validator.prototype.isPeriodYearMonth = function (strId, arr) {
	var oper = arr[1].split("=");
	$(oper[1]).value = $F(oper[1]).trim().replace(/[^0-9\-]/g, "");
	$(oper[2]).value = $F(oper[2]).trim().replace(/[^0-9\-]/g, "");
	var d1 = $F(oper[1]).trim().replace(/[^0-9]/g, "");
	var d2 = $F(oper[2]).trim().replace(/[^0-9]/g, "");
	var y = oper[3];
	var m = oper[4];

	if (this.isEmpty(strId, arr) && d2.trim().length == 0) {
        return true;
    }

	if(arr[0].toLowerCase() == 'y' && this.isEmpty($(oper[2]), arr)) {
		this.FOCUS_ITEM = oper[2];
        return false;
    }
	
	if(d1.length != d2.length) {
		var arrMsg = { "name" : arr[3] };
		this.msg = this.getItemValMsg(arr[4], arrMsg, DEFAULT_MSG_NOT_EQUAL_PERIOD_FORMAT);
		return false;
	}
	if(d1.length == 4) {
		y = y.substring(1).parseInt();
		var newArr = new Array(arr[0], "peroidYear="+oper[1]+"="+oper[2]+"="+y, arr[2], arr[3], arr[4]);
		return this.isPeriodYear(strId, newArr);
	} else {
		var newArr = new Array(arr[0], "peroidYear="+oper[1]+"="+oper[2]+"="+m, arr[2], arr[3], arr[4]);
		return this.isPeriodMonth(strId, newArr);
	}
	
}