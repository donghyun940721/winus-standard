import { TMSYS140E2 }  from './TMSYS140E2.js';
import { TMSYS140E3 }  from './TMSYS140E3.js';


let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '1';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)

const LOG_INFO 	        = '0';
const LOG_INFO_NEW 	    = '1';
const LOGIN_USER_INFO 	= '2';


jQuery(document).ready(function() {

    let view01 = null;
    let view02 = new TMSYS140E2();
    let view03 = new TMSYS140E3();

    const ViewContainer = [view01,view02, view03];

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
     //ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    if(ViewContainer[DEFAULT_VIEW_INDEX] != null ) ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){

        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;
        ViewContainer[activeTabIdx]?.ActiveViewEventCallBack();
        
        switch(activeTabIdx){
            case LOG_INFO : 
                break;

            case LOG_INFO_NEW : 
                break;
                
            case LOGIN_USER_INFO : 
                break;
        }

    });
});