import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil }  from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';


export class TMSYS140E3 {

    #formId         = 'frm_listE3';

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE3',
        gridTemplate        :   'TMSYS140E3',
        headerHeight        :   '27',
        rowHeight           :   '27',
        frozenColIdx        :   '3',
        setColor            :   false,
        useFilter           :   true,
    });

    #searchInterval;

    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();
        this.AddEvent();

        this.#grid.Init();
        this.#grid.SetGrid();
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE3").datepicker("setDate", date);                       // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE3").datepicker("setDate", date);                         // 수집등록일자 (to)

        // jQuery("#LOG_TYPE").val(LOG_LEVEL.DEBUG).prop('selected', true);        // 기본값(Default) : Debug
        jQuery("#intervaTime").val('3000');
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // # 키 입력 이벤트
        document.querySelector("#frm_listE3").addEventListener("keyup", (e) => {
            switch (e.keyCode) {
                case 13 : this.SearchClickEventCallback(e); break;
            }
        });


        // # '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE3").off().on("click", (e)=>{

            const vm = this;

            cfn_viewProgress();

            Promise.all([this.SearchClickEventCallback(e), this.GetStatisticsCount()])
                .then(([searchResult, statisticsResult]) =>{
                    const {LOGIN_USER_CNT, MULTI_USER_CNT, DB_SESSION_CNT, DB_PROCESS_CNT} = statisticsResult.rows[0];

                    cfn_closeProgress();
                    vm.#grid.SetData(searchResult);
                    document.querySelector('#totalUserCntE3').textContent = LOGIN_USER_CNT;
                    document.querySelector('#totalMutliUserCntE3').textContent = MULTI_USER_CNT;
                    document.querySelector('#totalSessionCntE3').textContent = DB_SESSION_CNT;
                    document.querySelector('#totalProcessCntE3').textContent = DB_PROCESS_CNT;
                })
                .catch((error) => {
                    debugger;
                    console.log(error);
                    alert(error);
                });

            // this.SearchClickEventCallback(e);

            // if(jQuery("#OPT_E3_01").is(":checked")){
            //     const intervalTime = jQuery("#intervaTime").val();

            //     this.#searchInterval = setInterval(()=> {
            //         this.SearchClickEventCallback(e);
            //     }, intervalTime);
            // }
            // else{
            //     clearInterval(this.#searchInterval);
            // }

        });


        // # "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE3").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 

//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        // switch(type)
        // {
        //     //* 화주
        //     case COMP_CATEGORY.CUST :

        //         vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

        //         SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
        //         SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
        //         SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

        //         vm.#custId = data[0][0].cust_id;

        //         break;
        // }

    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return new Promise((resolve, reject) => jQuery.ajax({
            url : "/TMSYS140/listE3.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)) reject(data["MSG"]);
                resolve(data);
            }
        }));
    }


    GetStatisticsCount() {
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return new Promise((resolve, reject) => jQuery.ajax({
            url : "/TMSYS140/getStatistics.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            success : function(data){
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)) reject(data["MSG"]);
                resolve(data);
            }
        }));
    }


    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad('로그이력');
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}