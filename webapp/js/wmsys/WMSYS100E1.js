import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil }  from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';


export class WMSYS100E1 {
    
    #custId         = null;
    #formId         = 'frm_listE1';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #grid = new WinusGrid({ 
        spdListDiv          : "spdListE1",
        gridTemplate        : "WMSYS100E1",     // Grid 구성요소 기준정보 코드
        checkDepth          : "0",              // [Select Column]
        rowHeight           : '27',             // 행 높이
        headerHeight        : '27',             // 헤더 높이
        frozenColIdx        : 7,                // 틀 고정
        setColor            : false             // Cell 단위 설정 배경색 적용
    });


    constructor(args){

        this.#grid.Init();
        this.#grid.SetGrid();

        this.Init();
        this.AddEvent();
    }


    Init(){
        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        
        jQuery("#vrSrchReqDtFrom").datepicker("setDate", date);     // 수집등록일자 (from)
        jQuery("#vrSrchReqDtTo").datepicker("setDate", date);       // 수집등록일자 (to)
    }


    DefaultSetting(){
        
        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }

    }
    

    AddEvent(args){

        // # 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        
        // # '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
        });


        // # "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_write").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // Set Cust Info
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), SessionUtil.GetValue(SessionKey.CUST_ID), SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                break;
        }

    }


    async CustChangedEventCallBack(type, refValue, id){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    AddClickEventCallBack(e){
        this.#grid.RowAddEventCallBack(false);
    }


    SaveClickEventCallBack(e, isProgressBar=true){
        
        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let selectedIdx = this.#grid.GetCheckedList();
        let saveData = new Array();

        
        try {
            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 행을 선택해야 합니다.`;
            }
    
            if(confirm(DICTIONARY['confirm_save'])){
    
                for(let rowIdx of selectedIdx){
                    saveData.push(this.#grid.GetRowData(rowIdx));
                }
    
                formData.append('gridData', JSON.stringify(saveData));
    
                jQuery.ajax({
                    url : "/WMSYS100/saveE1.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        if(isProgressBar) cfn_viewProgress();
                    },
                    complete: function(){
                        if(isProgressBar) cfn_closeProgress();

                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
        
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(data["MSG"]);
                        }
                        // Success ...
                        else{
                            alert(DICTIONARY['save_success'] + "\n(주문 재조회가 진행됩니다.)");
                        }
        
                    }
                });
            }
        } catch (error) {
            alert(error);
        }

    }


    DeleteClickEventCallBack(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let selectedIdx = this.#grid.GetCheckedList();
        let deleteData = new Array();


        try {
            if(cfn_isArray(selectedIdx) && selectedIdx.length == 0){
                throw `1개 이상 행을 선택해야 합니다.`;
            }
    
            if(confirm(DICTIONARY['delete.confirm'])){
    
                for(let rowIdx of selectedIdx){
                    deleteData.push(this.#grid.GetRowData(rowIdx));
                }
    
                formData.append('gridData', JSON.stringify(deleteData));
    
                jQuery.ajax({
                    url : "/WMSYS100/deleteE1.action",
                    type : 'post',
                    data : formData,
                    contentType : false,
                    processData : false,
                    beforeSend: function(xhr){
                        if(isProgressBar) cfn_viewProgress();
                    },
                    complete: function(){
                        if(isProgressBar) cfn_closeProgress();

                        vm.SearchClickEventCallback();
                    },
                    success : function(data){
        
                        // Fail ...
                        if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                            alert(data["MSG"]);
                        }
                        // Success ...
                        else{
                            alert(DICTIONARY['save_success'] + "\n(주문 재조회가 진행됩니다.)");
                        }
        
                    }
                });
            }
        } catch (error) {
            alert(error);
        }

    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        console.log('Search Click ...');
        

        return jQuery.ajax({
            url : "/WMSYS100/listE1.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }
    

    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('I/F 로그이력');

    }

    
//#endregion

}