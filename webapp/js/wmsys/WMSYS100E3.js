import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil }  from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';

export class WMSYS100E3{                // !(필수) 화면 명 수정

    #custId         = null;
    #formId         = 'frm_listE3';

    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE3',
        id          : 'vrSrchCustIdE3',
        name        : 'vrSrchCustNmE3',
        category    : COMP_CATEGORY.CUST
    });

    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: '송하인 정보',
            code: 'SND_INFO',
            startColumnKey: 'I_SND_CUST_NM',
            endColumnKey: 'I_SND_CUST_ADDR2'
        },
        {
            name: '수하인 정보',
            code: 'RCV_INFO',
            startColumnKey: 'I_RCV_CUST_NM',
            endColumnKey: 'I_RCV_CUST_ADDR2'
        },
        {
            name: '접수정보',
            code: 'INS_INFO',
            startColumnKey: 'I_FREIGHT_TYPE',
            endColumnKey: 'I_SHIP_AMT_YN'
        },
        {
            name: '수하인정보',
            code: 'RCVR_INFO',
            startColumnKey: 'RCVR_TEL',
            endColumnKey: 'RCVR_NM'
        }
    ];

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE3',
        gridTemplate        :   'WMSYS100E3',
        headerHeight        :   '27',                             // 헤더 높이
        rowHeight           :   '27',                             // 데이터 행 높이
        frozenColIdx        :   3,
        headerMappingInfo   :   this.#headerMappingInfo,
        setColor            :   false
    });


    constructor(args){

        if(args != undefined){
            
        }

        this.Init();
        this.AddEvent();

        this.#grid.Init();
        this.#grid.SetGrid();
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE3").datepicker("setDate", date);     // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE3").datepicker("setDate", date);       // 수집등록일자 (to)
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 화주
        document.getElementById("vrSrchCustCdE3").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE3").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });
        
        document.getElementById("vrSrchCustNmE3").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE3").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE3").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // Set Cust Info
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), SessionUtil.GetValue(SessionKey.CUST_ID), SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
        }

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                break;
        }
    }


    async CustChangedEventCallBack(type, refValue, id){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        console.log('Search Click ...');
        

        return jQuery.ajax({
            url : "/WMSYS100/listE3.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }


    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('로젠정제이력');

    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}