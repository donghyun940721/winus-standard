import { WMSYS100E1 }  from './WMSYS100E1.js';
import { WMSYS100E2 }  from './WMSYS100E2.js';
import { WMSYS100E3 }  from './WMSYS100E3.js';
import { WMSYS100E4 }  from './WMSYS100E4.js';


let   ACTIVE_TAB_INDEX          = null;     // 현재 활성화된 탭 번호
const DEFAULT_VIEW_INDEX        = '0';      // 최초 화면로드 시, 출력되는 탭 번호 (0 ~ n)


jQuery(document).ready(function() {

    let view01 = new WMSYS100E1();
    let view02 = new WMSYS100E2();
    let view03 = new WMSYS100E3();
    let view04 = new WMSYS100E4();

    const ViewContainer = [view01, view02, view03, view04];

    // 초기화면 설정 (Default View)
    jQuery('#tab1').tabs({ active: DEFAULT_VIEW_INDEX });
    ViewContainer[DEFAULT_VIEW_INDEX].DefaultSetting();
    ViewContainer[DEFAULT_VIEW_INDEX].ActiveViewEventCallBack();

    /* Tab(View) Changed Event */
    jQuery("#tab1 a").click(function(e){

        var activeTabIdx = jQuery('#tab1').tabs('option', 'active');
        ACTIVE_TAB_INDEX = activeTabIdx;

        ViewContainer[activeTabIdx].ActiveViewEventCallBack();

    });
});