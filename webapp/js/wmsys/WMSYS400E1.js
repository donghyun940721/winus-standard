import { WinusGrid }                from '../utility/WinusGrid.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';


export class WMSYS400E1{                // !(필수) 화면 명 수정

    #formId         = 'frm_listE1';

    /** UI Component */
    #compUser = new Component({
        code        : 'vrSrchUserCdE1',
        id          : 'vrSrchUserIdE1',
        name        : 'vrSrchUserNmE1',
        category    : COMP_CATEGORY.USER2
    });


    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE1',
        gridTemplate        :   'WMSYS400E1',
        headerHeight        :   '27',
        rowHeight           :   '25',
        frozenColIdx        :   '5',
        setColor            :   false,
        useFilter           :   true,
    });

    #searchInterval;

    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        
        jQuery("#vrSrchReqDtFrom").datepicker("setDate", date);         // 수집등록일자 (from)
        jQuery("#vrSrchReqDtTo").datepicker("setDate", date);           // 수집등록일자 (to)

        jQuery("#intervaTime").val('3000');
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * 사용자
        document.getElementById("vrSrchUserCdE1").addEventListener('change', (e)=>{
            this.UserChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        document.getElementById("vrSrchUserNmE1").addEventListener('change', (e)=>{
            this.UserChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });


        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE1").off().on("click", (e)=>{
            
            this.SearchClickEventCallback(e);

            if(jQuery("#OPT_01").is(":checked")){
                const intervalTime = jQuery("#intervaTime").val();

                this.#searchInterval = setInterval(()=> {
                    this.SearchClickEventCallback(e);
                }, intervalTime);
            }
            else{
                clearInterval(this.#searchInterval);
            }
        });


        // * "완료" 버튼 클릭 이벤트
        jQuery("#btnComplteE1").off().on("click", (e)=>{
            // this.ExcelDownLoadCallBack(e);
            this.CompleteClickEventCallBack(e);
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btnExcelDownE1").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 

//#endregion


//#region   :: CallBack Event


    async UserChangedEventCallBack(type, refValue){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compUser.Init();
            return;
        }

        await this.#compUser.Search(type, refValue)
                .catch((error)=>{
                    alert(error);
                });
        
    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        

        return jQuery.ajax({
            url : "/WMSYS400/list.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });
    }


    CompleteClickEventCallBack(e){

        const ValidNoRowSelected = (selectedData) => { 

            try{
                if((selectedData == null) || (selectedData.length == 0)) 
                    throw new ValidationError(`선택된 행이 없습니다.`);

            }catch(error){
                if(error instanceof ValidationError){
                    throw new StoredErrorHistory(error);
                }
                else{
                    throw error;
                }
            }
        }

        try {
            let vm = this;
            let formData = new FormData(document.getElementById(`${this.#formId}`));
            let selectedData = this.#grid.GetSelectedRowData();
            
            ValidNoRowSelected(selectedData);

            formData.append('gridData', JSON.stringify(selectedData));
    
            jQuery.ajax({
                url : "/WMSYS400/complete.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(data["MSG"]);
                        throw new ResponseErrorHistory('Fail Ajax Error...', 'WMSYS400E1.js', '220');
                    }
                    // Success ...
                    else{
                        alert(DICTIONARY['save_success']);
                        vm.SearchClickEventCallback();
                    }
                }
            });
        } catch (error) {
            alert(error.message);
        }

    }


    ExcelDownLoadCallBack(e){
        this.#grid.ExcelDownLoad('작업관리(인터페이스)');
    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}