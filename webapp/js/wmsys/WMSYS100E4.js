import { WinusGrid }                from '../utility/WinusGrid.js';

export class WMSYS100E4{                // !(필수) 화면 명 수정

    #custId         = null;
    #formId         = 'frm_listE4';

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE4',
        gridTemplate        :   'WMSYS100E4',
        headerHeight        :   '27',                             // 헤더 높이
        rowHeight           :   '27',                             // 데이터 행 높이
        frozenColIdx        :   3,
        setColor            :   false
    });

    #searchInterval;

    constructor(args){

        if(args != undefined){
            
        }

        this.Init();
        this.AddEvent();

        this.#grid.Init();
        this.#grid.SetGrid();
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){
        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
        
        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchReqDtFromE4").datepicker("setDate", date);     // 수집등록일자 (from)
        jQuery("#vrSrchReqDtToE4").datepicker("setDate", date);       // 수집등록일자 (to)

        jQuery("#intervaTime").val('3000');
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        // * '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE4").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);

            if(jQuery("#OPT_00").is(":checked")){
                const intervalTime = jQuery("#intervaTime").val();

                this.#searchInterval = setInterval(()=> {
                    this.SearchClickEventCallback(e);
                }, intervalTime);
            }
            else{
                clearInterval(this.#searchInterval);
            }
        });


        // * "엑셀" 버튼 클릭 이벤트
        jQuery("#btn_writeE4").off().on("click", (e)=>{
            this.ExcelDownLoadCallBack(e);
        });

    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 


//#endregion


//#region   :: CallBack Event


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        formData.set('rootContextIdE4'   , document.querySelector('#rootContextIdE4').value.trim());
        formData.set('parentContextIdE4' , document.querySelector('#parentContextIdE4').value.trim());
        formData.set('contextIdE4'       , document.querySelector('#contextIdE4').value.trim());
        formData.set('msgIdE4'           , document.querySelector('#msgIdE4').value.trim());
        
        return jQuery.ajax({
            url : "/WMSYS100/listE4.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }


    ExcelDownLoadCallBack(e){
        
        this.#grid.ExcelDownLoad('webMethod');

    }


//#endregion


//#region   :: Validation


//#endregion


//#region   :: Utility


//#endregion

}