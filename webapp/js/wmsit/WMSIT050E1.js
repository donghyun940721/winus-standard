import { Component, COMP_VALUE_TYPE, COMP_CATEGORY}     from '../component/contentFilter.js';
import { SessionKey, SessionUtil }  from '../utility/userStorageManger.js';


export class WMSIT050E1{                // !(필수) 화면 명 수정

    #custId         = null;
    #formId         = 'frm_listE1'
    
    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE1',
        id          : 'vrSrchCustIdE1',
        name        : 'vrSrchCustNmE1',
        category    : COMP_CATEGORY.CUST
    });

    #vm = this;
    #currentOrdInfo = undefined;
    #currentScanInfo = undefined;

    constructor(args){

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

        let date = new Date();

        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );

        jQuery(".DatePicker").datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/img/exam/btn/calendar.gif",
            buttonImageOnly: true
        });

        jQuery("#vrSrchOrdDegreeDateE1").datepicker("setDate", date);                   // 주문예정차수 (날짜)

        this.initBarcode();

        setTimeout(() => {
            document.getElementById("vrSrchItemBarcodeE1").focus();
        }, 1000);
    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){
        let custId = SessionUtil.GetValue(SessionKey.CUST_ID) ?? jQuery("#vrSrchCustIdE1").val();
        let custNm = SessionUtil.GetValue(SessionKey.CUST_NM) ?? jQuery("#vrSrchCustNmE1").val();
        let custCd = SessionUtil.GetValue(SessionKey.CUST_CD) ?? jQuery("#vrSrchCustCdE1").val();

        if(cfn_isEmpty(custId)){
            StandardPopUp.call(this, 'CUST');
        }
        else{
            this.#compCust.SetValue(custCd, custId, custNm);

            this.#custId = custId;
        }
    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){
        // * 화주
        document.getElementById("vrSrchCustCdE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE1").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });

        document.getElementById("vrSrchCustNmE1").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        // 상품바코드 (스캔)
        // document.getElementById("vrSrchItemBarcodeE1").addEventListener('change', (e)=>{
        //     this.ItemBarCodeChangedCallBack(e.target.value);
        // });

        // 상품바코드 (스캔)
        document.addEventListener('keypress', (event) => {
            this.keypressHandler.call(this, event);
        });

        // 초기화
        jQuery("#btnResetE1").off().on("click", (e)=>{
            this.#currentOrdInfo = undefined;
            this.#currentScanInfo = undefined;

            InitInputComponent();

            document.getElementById("inspectStateE1").innerHTML = '';
        });

        // 입고확정
        jQuery("#btnSaveE1").off().on("click", (e)=>{
            if(this.#currentOrdInfo === undefined){
                alert('조회된 주문정보가 없습니다.');
            }

            if(confirm('입고확정 하시겠습니까?')){
                
                WinusLog.InsertForInfo(new LogData({
                    FUNCTION      : '/WINUS/WMSIT050E1.action',
                    LOG_TYPE      : 'HISTORY',
                    LOG_DESC      : `입고확정(잔여수량존재)`,
                    USER_DEFINED1 : this.#currentOrdInfo.ORD_ID,
                    USER_DEFINED2 : this.#currentOrdInfo.IN_ORD_QTY,
                    USER_DEFINED3 : document.getElementById('inspectCntE1').innerHTML,
                }));

                SimpleInOrder(this.#currentOrdInfo);

                // Init
                this.#currentOrdInfo = undefined;
                this.#currentScanInfo = undefined;
            };
        });
    }

    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){
        
    }

//#region   :: Component Set 

    // 입고검수(확정) 바코드 생성
    initBarcode () {
        jQuery('#inOrderCompleteBarCdE1').qrcode({
            render	: "table",
            width  : 70,
            height : 70,
            text   : 'inOrderCompleteBarCdE1'
        });
    }

//#endregion


//#region   :: CallBack Event

    keypressHandler = this.debounce (function (event) {
        console.log('scan', event);
        
        switch(event.target?.name){

            // 상품바코드 입력란
            case "vrSrchItemBarcodeE1" :
                this.ItemBarCodeChangedCallBack(event.target.value);
                console.log('Item Barcode');
                break;

            // 검수확정
            case undefined: 
                if(this.#currentOrdInfo === undefined || this.#currentOrdInfo === null){
                    alert('검수정보가 없습니다.');
                    return ;
                }

                // 그 외 스캔 시 검수확정
                SimpleInOrder(this.#currentOrdInfo);
                break;

            default:

        }
    }, 300);


    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                // this.GetCustOrdDegree();

                break;
        }
    }


    async CustChangedEventCallBack(type, refValue){
        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }

    async ItemBarCodeChangedCallBack (value) {
        const vm = this;

        console.log(value);

        if((value != '' && this.#currentScanInfo !== undefined) && this.#currentScanInfo.ITEM_BAR_CD !== value) {
            alert('검수대상 상품이 아닙니다.');
            return ;
        }

        Promise.all([this.GetOrder(value), this.GetItem(value)])
            .then(values => {
                const [order, item] = values;

                vm.SetOrderInfoData(order["rows"][0]);
                vm.SetItemInfoData(item["rows"][0]);

                // 입력 포커싱 해제
                setTimeout(() => {
                    document.getElementById("vrSrchItemBarcodeE1").blur();
                    document.getElementById("inspectStateE1").innerHTML = '진행중';
                }, 300);

                console.log('API success');
            })
            .catch((errorMsg => {
                alert(errorMsg);
                console.log(errorMsg);
            })
        );
    }

    async GetOrder(value) {
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let bodyData = new FormData();
        const vm = this;

        if(value === null || value === undefined === value === "") return ;

        bodyData.append('vrSrchReqDtFrom', formData.get('vrSrchOrdDegreeDateE1'));
        bodyData.append('vrSrchReqDtTo', formData.get('vrSrchOrdDegreeDateE1'));
        bodyData.append('vrSrchCustCd', this.#compCust.GetValue(COMP_VALUE_TYPE.CODE));
        bodyData.append('vrSrchCustNm', this.#compCust.GetValue(COMP_VALUE_TYPE.NAME));
        bodyData.append('vrSrchCustId', this.#compCust.GetValue(COMP_VALUE_TYPE.ID));
        bodyData.append('vrSrchItemBarCd', value);
        bodyData.append('vrSrchOrderPhase', '01'); // 주문등록상태
        bodyData.append('cmbSrchType', '100'); 

        return new Promise((resolve, reject) => { 
            jQuery.ajax({
                url : "/WMSOP910/listE1.action",
                type : 'post',
                data : bodyData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
                    // Error ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        reject(data["MSG"]);
                    }
                    if(data['rows'].length === 0){
                        reject('입력된 상품정보로 등록된 입고주문이 없습니다.')
                    }
                    
                    vm.#currentOrdInfo = data["rows"][0];
                    resolve(data);
                }
            });
        });
    }

    async GetItem (value) {
        let bodyData = new FormData();
        const vm = this;

        if(value === null || value === undefined === value === "") return ;
        
        bodyData.append('S_CUST_CD', this.#compCust.GetValue(COMP_VALUE_TYPE.CODE));
        bodyData.append('S_CUST_NM', this.#compCust.GetValue(COMP_VALUE_TYPE.NAME));
        bodyData.append('S_CUST_ID', this.#compCust.GetValue(COMP_VALUE_TYPE.ID));
        bodyData.append('vrSrchItemBarType', 'I');      // 상품바코드
        bodyData.append('vrSrchItemBarCd', value);
        bodyData.append('rows', 1000);
        bodyData.append('page', 1);
        bodyData.append('sord', 'asc');

        return new Promise((resolve, reject) => { 
            jQuery.ajax({
                url : "/WMSMS090/list.action",
                type : 'post',
                data : bodyData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    cfn_viewProgress();
                },
                complete: function(){
                    cfn_closeProgress();
                },
                success : function(data){
                    // Error ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        reject(data["MSG"]);
                    }

                    if(data['rows'].length === 0){
                        reject('입력된 상품정보로 등록된 상품이 없습니다.')
                    }

                    vm.#currentScanInfo = data["rows"][0];
                    resolve(data);
                }
            });
        });
    }

    SetOrderInfoData (data) {
        // document.getElementById('inspectCntE1').innerHTML = 0;                   // 검수수량
        document.getElementById('inspectCntE1').value = data.IN_ORD_QTY;        // 검수수량
        document.getElementById('inOrdCntE1').innerHTML = data.IN_ORD_QTY;       // 입고수량
        document.getElementById('inRemainCntE1').innerHTML = data.IN_ORD_QTY;    // 잔여수량

        document.getElementById('ordIdE1').innerHTML = data.VIEW_ORD_ID;
        document.getElementById('inUomCdE1').innerHTML = data.IN_ORD_UOM_CD;
    }

    SetItemInfoData (data) {
        document.getElementById('ritemNmE1').innerHTML = data.ITEM_KOR_NM;       // 상품명
        document.getElementById('unitQtyE1').innerHTML = data.UNIT_QTY;          // 입수
    }

    IncreaseInsectCnt() {
        const injectCnt = Number(document.getElementById('inspectCntE1').innerHTML);
        const inRemainCnt = Number(document.getElementById('inRemainCntE1').innerHTML);

        document.getElementById('inspectCntE1').innerHTML = injectCnt + 1;
        document.getElementById('inRemainCntE1').innerHTML = inRemainCnt - 1;

        console.log(inRemainCnt);
        

        setTimeout(() => {
            if((inRemainCnt -1) === 0){
                if(confirm('입고확정 하시겠습니까?')){
                    SimpleInOrder(this.#currentOrdInfo);

                    // Init
                    this.#currentOrdInfo = undefined;
                    this.#currentScanInfo = undefined;
                };
            };
        }, 300);
    }

//#endregion

//#region   :: Validation


//#endregion

//#region   :: Utility

    debounce (func, delay) {
        let debounceTimer;
        return function() {
            const context = this;
            const args = arguments;
            clearTimeout(debounceTimer);
            debounceTimer = setTimeout(() => func.apply(context, args), delay);
        }
    }

//#endregion

}

/**
 * 간편입고
 * 
 * TODO :: 작업수량을 input으로하는 로케이션 추천 없어,
 * 고정지정 > 확정 단계로 수행
 * 
 * 
 * @param {*} currentOrdInfo 
 */
async function SimpleInOrder (currentOrdInfo) {
    let targetOrdInfo = currentOrdInfo;

    if(targetOrdInfo.WORK_ID === ''){
        await InitWork(targetOrdInfo);
        targetOrdInfo = await ReSearchOrder(targetOrdInfo);     // 재조회
        targetOrdInfo = targetOrdInfo['rows'][0];
    }

    await SaveLocation(targetOrdInfo);
}

/**
 * 로케이션지정작업 초기화 (WORK_ID)
 * 
 * @returns
 */
async function InitWork (currentOrdInfo) {
    let bodyData = new FormData();

    bodyData.append('vrRowCount', 1);
    bodyData.append('vrOrdId', currentOrdInfo.ORD_ID);
    bodyData.append('vrOrdSeq', currentOrdInfo.ORD_SEQ);
    bodyData.append('vrOrdType', '01');
    bodyData.append('vrWorkType', '');

    console.log('Init Work');

    return jQuery.ajax({
        url : "/WMSOP000Q5/saveLocInitType.action",
        type : 'post',
        data : bodyData,
        contentType : false,
        processData : false,
        beforeSend: function(xhr){
            cfn_viewProgress();
        },
        complete: function(){
            cfn_closeProgress();
        },
        success : function(data){
            // Error ...
            if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                throw data["MSG"]
            }
            // Success ...
            console.log(data);
        }
    });
}

/**
 * 입고주문 재조회 (WORK_ID 획득)
 * 
 * @param {*} currentOrdInfo 
 * @returns 
 */
async function ReSearchOrder (currentOrdInfo) {
    let bodyData = new FormData();
    let formData = new FormData(document.getElementById(`frm_listE1`));
    
    bodyData.append('vrSrchReqDtFrom', formData.get('vrSrchOrdDegreeDateE1'));
    bodyData.append('vrSrchReqDtTo', formData.get('vrSrchOrdDegreeDateE1'));
    bodyData.append('vrSrchCustCd', formData.get('vrSrchCustCdE1'));
    bodyData.append('vrSrchCustNm', formData.get('vrSrchCustNmE1'));
    bodyData.append('vrSrchCustId', formData.get('vrSrchCustIdE1'));
    bodyData.append('vrSrchOrderId', currentOrdInfo.ORD_ID);
    bodyData.append('vrSrchOrderPhase', '01'); // 주문등록상태
    bodyData.append('cmbSrchType', '100'); 

    console.log('Research');

    return jQuery.ajax({
        url : "/WMSOP910/listE1.action",
        type : 'post',
        data : bodyData,
        contentType : false,
        processData : false,
        beforeSend: function(xhr){
            cfn_viewProgress();
        },
        complete: function(){
            cfn_closeProgress();
        },
        success : function(data){
            // Error ...
            if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                throw data["MSG"]
            }
            // Success ...
            // SaveLocation(data["rows"][0]);
            
            return data["rows"][0];
        }
    });
}

/**
 * 로케이션 지정
 * TODO :: 추후 추천으로 변경되어야 함.
 * 
 * @param {*} currentOrdInfo 
 * @returns 
 */
async function SaveLocation (currentOrdInfo) {
    let bodyData = new FormData();
    const inspectCnt = document.getElementById('inspectCntE1').value;
    const fixLocId = currentOrdInfo.LC_ID === '0000001262' ? '0000153213' : '0000514149';      // TODO :: 로케이션 추천으로 변경

    if(!currentOrdInfo){
        throw new Error('잘못된 주문정보');
    }

    if(inspectCnt <= 0){
        alert('검수수량이 없습니다.');
        return ;
    }

    bodyData.append('I_selectIds', 1);
    bodyData.append('D_selectIds', 0);

    bodyData.append('I_ST_GUBUN0', currentOrdInfo.ORD_ID);
    bodyData.append('I_WORK_ID0', currentOrdInfo.WORK_ID);
    bodyData.append('I_WORK_SEQ0', currentOrdInfo.WORK_SEQ);
    bodyData.append('I_WORK_SUBSEQ0', '');
    bodyData.append('I_PDA_CD0', '');
    bodyData.append('I_EMPLOYEE_ID0', '');

    bodyData.append('I_LOC_ID0', fixLocId);
    bodyData.append('I_RITEM_ID0', currentOrdInfo.RITEM_ID);
    bodyData.append('I_WORK_QTY0', inspectCnt);
    bodyData.append('I_UOM_ID0', currentOrdInfo.IN_ORD_UOM_ID);
    bodyData.append('I_CONF_QTY0', currentOrdInfo.IN_ORD_QTY);

    bodyData.append('I_SUB_LOT_ID0', '');
    bodyData.append('I_STOCK_ID0', '');
    bodyData.append('I_REAL_PLT_QTY0', '');
    bodyData.append('I_REAL_BOX_QTY0', '');

    bodyData.append('I_ITEM_DATE_END0', '');

    console.log('Save Location');

    return jQuery.ajax({
        url : "/WMSOP000Q5/saveLoc.action",
        type : 'post',
        data : bodyData,
        contentType : false,
        processData : false,
        beforeSend: function(xhr){
            cfn_viewProgress();
        },
        complete: function(){
            cfn_closeProgress();
        },
        success : function(data){
            // Error ...
            if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                throw data["MSG"]
            }
            // Success ...
            InOrderComplete(currentOrdInfo);
        }
    });
}

/**
 * 입고확정
 * 
 * @returns
 */
function InOrderComplete (currentOrdInfo) {
    let bodyData = new FormData();

    bodyData.append('selectIds', 1);
    bodyData.append('ORD_ID0', currentOrdInfo.ORD_ID);
    bodyData.append('ORD_SEQ0', currentOrdInfo.ORD_SEQ);
    bodyData.append('REF_SUB_LOT_ID0', '');

    console.log('In Order Complete');

    return jQuery.ajax({
        url : "/WMSOP020/saveInComplete.action",
        type : 'post',
        data : bodyData,
        contentType : false,
        processData : false,
        beforeSend: function(xhr){
            cfn_viewProgress();
        },
        complete: function(){
            cfn_closeProgress();
        },
        success : function(data){
            // Error ...
            if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                throw data["MSG"]
            }
            // Success ...
            // alert(data["MSG"]);
            InitInputComponent();
            document.getElementById("inspectStateE1").innerHTML = '확정완료';
        }
    });
}

/**
 * 입력 및 정보 출력란 초기화
 */
function InitInputComponent () {
    
    document.getElementById('inspectCntE1').value = '';                   // 검수수량
    document.getElementById('inOrdCntE1').innerHTML = '';       // 입고수량
    document.getElementById('inRemainCntE1').innerHTML = '';    // 잔여수량

    document.getElementById('ordIdE1').innerHTML = '';
    document.getElementById('inUomCdE1').innerHTML = '';

    document.getElementById('ritemNmE1').innerHTML = '';       // 상품명
    document.getElementById('unitQtyE1').innerHTML = '';          // 입수
    
    document.getElementById("vrSrchItemBarcodeE1").value = '';
    document.getElementById("vrSrchItemBarcodeE1").innerHTML = '';
    document.getElementById("vrSrchItemBarcodeE1").focus();
}