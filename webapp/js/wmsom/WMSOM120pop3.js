let ViewComponent = (function(){

    const CODE = 'CODE';
    const NAME = 'NAME';
    let args = null;
    

    let init = function(){

        args = window.opener.GetParam();

        /* Tabs */
        jQuery("#tab1").tabs({show: function(event, ui) {resizeGrid();}});

        /* button */
        jQuery("input:button").button();


        // DatePicker
        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ DICTIONARY['ss_lang'] ] );
		// jQuery( "input[name='calInDt']").datepicker( {
        jQuery(".DatePicker").datepicker( {
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd",
			showOn: "button",
			buttonImage: "/img/exam/btn/calendar.gif",
			buttonImageOnly: true
		});

        initComponent();
    }


    let initComponent = function(){

        // 화주
        jQuery("#vrSrchCustId").val(('vrCustId' in args) ? args['vrCustId'] : null);
        jQuery("#vrSrchCustCd").val(('vrCustCd' in args) ? args['vrCustCd'] : null);
        jQuery("#vrSrchCustNm").val(('vrCustNm' in args) ? args['vrCustNm'] : null);
        
        // 입고일자
        jQuery("#calInDt").val(localStorage.LocalTime ?? new Date());

        // 주문구분 (입고타입만 바인딩)
        var selectHit = ['20','22','23','25','111','137','138','139'];
        var selectSize = jQuery("#vrSrchOrderPhase option").size();
        var searchHit = [];

        for(var i = 0 ; i < selectSize ; i++){
            
            let orderPhaseValue = jQuery("#vrSrchOrderPhase option:eq("+i+")").val();

            if(selectHit.indexOf(orderPhaseValue) < 0){
                searchHit.push(i);
            }
        }

        for(var i = searchHit.size() ; i > 0 ; i --){
            jQuery("#vrSrchOrderPhase option:eq("+searchHit[i-1]+")").remove();
        }
    }


    let AddEvent = function(args){

        // 화주
        document.querySelector("#vrSrchCustCd").addEventListener('change', (e)=>{
            CustChangedEventCallBack(CODE, e.target.value);
        });

        document.getElementsByName('btnSrchCustPop')[0].addEventListener('click', (e)=>{
            fn_Popup("CUST");
        });

        document.querySelector("#vrSrchCustNm").addEventListener('change', (e)=>{
            CustChangedEventCallBack(NAME, e.target.value);
        });

        
        // 입고처
        document.querySelector("#vrInCustCd").addEventListener('change', (e)=>{
            InCustChangedEventCallBack(CODE, e.target.value);
        });

        document.querySelector('#btnInCustPop').addEventListener('click', (e)=>{
            fn_Popup("INCUST");
        });

        document.querySelector("#vrInCustNm").addEventListener('change', (e)=>{
            InCustChangedEventCallBack(NAME, e.target.value);
        });


        // 창고
        document.querySelector("#vrSrchWhCd").addEventListener('change', (e)=>{
            WareHouseChangedEventCallBack(CODE, e.target.value);
        });

        document.querySelector('#btnWhPop').addEventListener('click', (e)=>{
            fn_Popup("WH");
        });

        document.querySelector("#vrSrchWhNm").addEventListener('change', (e)=>{
            WareHouseChangedEventCallBack(NAME, e.target.value);
        });


        // 신규버튼
        jQuery("#btnNew").off().on("click", ()=>{
            "GRID" in args ? args.GRID.RowAddEventCallBack() : null
        });

    
        // 초기화버튼
        jQuery("#btnInit").off().on("click", ()=>{
            "GRID" in args ? args.GRID.RowDeleteAllEventCallBack() : null
        });


        // 저장버튼
        jQuery("#btnSave").off().on("click", ()=>{
            "GRID" in args ? args.GRID.SaveEventCallBack() : null
        });
    }


    let Validation = function(){
        
        try{

            let formData = jQuery("form[name='frm_list']").serializeArray();
            let errorMsg = null;
            let conditionNm = null;

            formData.forEach((data)=>{

                console.log(`key : ${data.name} / valeu : ${data.value}`);

                if(cfn_isEmpty(data.value)){
                    switch(data.name){

                        case "vrOrgOrdId" : 
                            conditionNm = "원주문번호"; 
                            break;
    
                        case "vrSrchCustCd" :       case "vrSrchCustNm" :
                            conditionNm = "화주";
                            break;
    
                        case "calInDt" : 
                            conditionNm = "입고일자";
                            break;
    
                        case "vrSrchOrderPhase" :
                            conditionNm = "주문구분";
                            break;
    
                        case "vrOrderType" : 
                            conditionNm = "입고분류"
                            break;
                    }
                }


                if(!cfn_isEmpty(conditionNm)){
                    errorMsg = `[유효성검증(주문정보)] : 공백을 허용하지 않는 항목이 존재합니다. (${conditionNm})`;
                    throw errorMsg;
                }

            })

            return true;

        }
        catch(e){

            alert(e);
            return false;

        }
    }


//#region :: Event CallBack


    /**
     * 화주 입력값 변경 이벤트콜백
     * 
     * @param {*} type      : 코드(CODE) 혹은 명칭(NAME)
     * @param {*} refValue  : 컴포넌트 입력값(매개변수)
     * @param {*} id 
     */
    let CustChangedEventCallBack = function(type, refValue, id){

        let paramKey = type == CODE ? 'vrSrchCustCd' : 'vrSrchCustNm';
        
        if(!cfn_isEmpty(refValue)){
            
            var param   = `srchKey=CUST&${paramKey}=${refValue}&S_CUST_TYPE=12`;
            var url     = "/selectMngCode.action"
            jQuery.post(url, param, function(output){

                data = output.CUST;

                if(cfn_isEmpty(data)){
                    jQuery("form[name='frm_list'] input[name='vrSrchCustCd']").val("").focus();
                    jQuery("form[name='frm_list'] input[name='vrSrchCustNm']").val("");
                    jQuery('form[name="frm_list"] input[name="vrSrchCustId"]').val("");
                    alert(DICTIONARY['list.nodata']);
                }
                else{
                    var strParameter = [];
                    strParameter.push(type == CODE ? refValue : '');
                    strParameter.push(type == NAME ? refValue : '');
                    var width  = "800";
                    var height = "530";
                    var param  = "?func=selectedCustCallBack&custType=12&strParameter=" + strParameter + "&stBtnYn=N";
                    var url    = "/WMSCM011.action" + param;
                    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
                    asppop.focus();
                }

            }).fail(function(xhr, textStatus, err){
                if(xhr.status == 404){
                    fn_errCheck(err); // 메인창 로그인화면 이동처리
                    javascript:self.close(); // 팝업창 닫기
                }
            });
        }else{
            jQuery("form[name='frm_list'] input[name='vrSrchCustCd']").val("");
            jQuery("form[name='frm_list'] input[name='vrSrchCustNm']").val("");
            jQuery('form[name="frm_list"] input[name="vrSrchCustId"]').val("");
        }
        // fn_initProE3();
    }


    /**
     * 입고처 입력값 변경 이벤트콜백
     *  
     * @param {*} type 
     * @param {*} refValue 
     * @param {*} id 
     */
    let InCustChangedEventCallBack = function(type, refValue, id){

        let paramKey = type == CODE ? 'vrSrchCustCd' : 'vrSrchCustNm';
        let custId = jQuery("#vrSrchCustId").val();                     // 화주 ID
        
        if(!cfn_isEmpty(refValue) && !cfn_isEmpty(custId)){

            var param   = `srchKey=CLIENT&${paramKey}=${refValue}&srchSubCode3=108&srchSubCode5=${custId}&custType=1`;
            var url     = "/selectMngCode.action";

            jQuery.post(url, param, function(output){
                var data = output.CLIENT;
                if( !cfn_isEmpty(data) ){
                    var strParameter = [];
                    strParameter.push(type == CODE ? refValue : '');
                    strParameter.push(type == NAME ? refValue : '');
                    strParameter.push('108');
                    strParameter.push('');
                    strParameter.push(custId);
                    strParameter.push('1');
                                    
                    var width  = "800";
                    var height = "530";
                    var param = "?func=selectedInCustCallBack&strParameter=" + strParameter;
                    var url = "/TMSCM010.action" + param;
                    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
                    asppop.focus();
                }
                else{
                    jQuery('form[name="frm_list"] input[name="vrInCustCd"]').val(data[0].CODE);
                    jQuery('form[name="frm_list"] input[name="vrInCustNm"]').val(data[0].NAME);
                    jQuery('form[name="frm_list"] input[name="vrInCustId"]').val(data[0].ID);
                    jQuery('form[name="frm_list"] input[name="vrInCustAddr"]').val(data[0].ADDR);
                    jQuery('form[name="frm_list"] input[name="vrInCustEmpNm"]').val(data[0].EMPNM);
                    jQuery('form[name="frm_list"] input[name="vrInCustTel"]').val(data[0].TEL);
                    alert(DICTIONARY['list.nodata']);
                }
            }).fail(function(xhr, textStatus, err){
                if(xhr.status == 404){
                    fn_errCheck(err); // 메인창 로그인화면 이동처리
                    javascript:self.close(); // 팝업창 닫기
                }
            });
        }
    }


    /**
     * 창고 입력값 변경 이벤트콜백
     * 
     * @param {*} type 
     * @param {*} refValue 
     * @param {*} id 
     */
    let WareHouseChangedEventCallBack = function(type, refValue, id){

        if(!cfn_isEmpty(refValue)){

            var param 	= `srchKey=WH&vrSrchWhCd=${refValue}`;
            var url 	= "/selectMngCode.action";

            jQuery.post(url, param, function(data){

                var data = data.WH;

                if( !cfn_isEmpty(data) ){
                    var strParameter = [];
                    strParameter.push(type == CODE ? refValue : '');
                    strParameter.push(type == NAME ? refValue : '');
                    var width  = "800";
                    var height = "530";
                    var param  = "?func=selectedWHCallBack&strParameter=" + strParameter + "&stBtnYn=N";
                    var url    = "/WMSMS040/pop.action" + param;
                    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
                    asppop.focus();
                }
                else{
                    jQuery("form[name='frm_list'] input[name='vrSrchWhCd']").val("").focus();
                    jQuery("form[name='frm_list'] input[name='vrSrchWhNm']").val("");
                    jQuery('form[name="frm_list"] input[name="vrSrchWhId"]').val("");
                    alert(DICTIONARY['list.nodata']);
                }
            }).fail(function(xhr, textStatus, err){
                if(xhr.status == 404){
                    fn_errCheck(err); // 메인창 로그인화면 이동처리
                    javascript:self.close(); // 팝업창 닫기
                }
            });
        }
    }


    /**
     * 상품코드 조회 팝업 호출
     * 
     * @param {*} type 
     * @param {*} refValue 
     * @param {*} id 
     */
    let ItemSearchClickEventCallBack = function(type, refValue, id){

        let  strParameter = new Array();

        strParameter.push('');
        strParameter.push('');
        strParameter.push(jQuery("#vrSrchCustId").val());       // 화주 ID
        strParameter.push(jQuery("#vrSrchWhId").val());         // 창고 ID
        strParameter.push('');
        strParameter.push('viewAll');
        
        let param = `?func=${selectedItemCallBack.name}&strParameter=${strParameter}`;
        let url = "/WMSCM091Q2.action" + param;
        let asppop = cfn_openPop2(url, "fn_Popup", "800", "530");

        asppop.focus();
    }


    /**
     * 상품 바코드 조회 팝업 호출
     * 
     * @param {*} type 
     * @param {*} refValue 
     * @param {*} id 
     */
    let ItemBarCodeClickEventCallBack = function(type, refValue, id){

        let  strParameter = new Array();
        let param = `?func=${selectedItemBarcodeCallBack.name}&strParameter=`;
        let url = "/WMSMS090pop3.action";

        strParameter.push(jQuery("#vrSrchCustId").val());
        strParameter.push(grid.getValue(grid.targetRow, grid.GetColumnIdx("RITEM_ID")));    // 상품ID
        strParameter.push(grid.getValue(grid.targetRow, grid.GetColumnIdx("ITEM_CODE")));   // 상품코드
        strParameter.push("Y");                                                             // 조회용도 여부(Y/N)
        url = url + param + strParameter;
        
        var asppop = cfn_openPop2(url, "fn_Popup", "800", "530");
		asppop.focus();
    }


//#endregion


    return {
        init : init, 
        AddEvent : AddEvent,
        ItemSearchClickEventCallBack : ItemSearchClickEventCallBack,
        ItemBarCodeClickEventCallBack : ItemBarCodeClickEventCallBack,
        Validation : Validation
    }

})();


class WinusGrid {

    #spdList        = null;
    #spdSheet       = null;
    #spdListDiv     = null;
    #gridTemplate   = null;
    #columnInfo     = null;
    #colMap         = null;
    #seqJson        = null; 
    #targetRow      = null;
    #targetCol      = null;

    constructor(args){

        this.#spdListDiv    = "spdListDiv" in args ? args.spdListDiv : "";
        this.#gridTemplate  = "gridTemplate" in args ? args.gridTemplate : "";

    }

    set targetRow(row) { this.#targetRow = row; }
    set targetCol(col) { this.#targetCol = col; }

    get targetRow() { return this.#targetRow; }
    get targetCol() { return this.#targetCol; }

    get colMap() { return this.#colMap; }

    get spdList() {return this.#spdList; }
    get spdSheet() {return this.#spdSheet; }


    Init(){
        this.#spdSheet = new GC.Spread.Sheets.Workbook(jQuery(`#${this.#spdListDiv}`).get(0), {sheetCount : 1});

        this.#spdList = this.#spdSheet.getSheet(0);

		this.#spdSheet .options.selectionBorderColor = 'Accent 7';
		this.#spdSheet .options.selectionBackColor = 'transparent'
		this.#spdSheet .options.referenceStyle = GC.Spread.Sheets.ReferenceStyle.r1c1;

        ps_setSpreadSheet_init_new(this.#spdSheet);        
        ps_setSpreadEvents_Bind_Multi(this.#spdList, this.#spdSheet);
    }


    SetGrid(){
        let that = this;

        jQuery.ajax({
            url : "/WMSCM300/allList.action?vrTemplateType=" + this.#gridTemplate,
            type : 'GET',
            contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
            dataType : 'html',
            error : function(_, textStatus, errorThrown) {
                alert("$!lang.getText('처리 중 오류.')" + "($!lang.getText('관리자 문의. (조회 오류)'))");
            },
            success : function(data) {

                that.#colMap = that.fn_setGridIndex(data);
                that.ps_setSpreadColumnIndex();
                that.#spdList.suspendPaint();

                that.ps_setSpreadStyle();
                that.#spdList.resumePaint();

                // gfn_forceEventResize();
            }
        });
    }


    /**
     * Cell 입력
     * 
     * @param {*} rowIdx : 행 번호 (0부터 시작)
     * @param {*} colIdx : 열 번호 (1부터 시작)
     * @param {*} value  : 입력값
     */
    setValue(rowIdx, colIdx, value){
        this.#spdList.setValue(rowIdx, colIdx, value);
    }


    getValue(rowIdx, colIdx){
        return this.#spdList.getValue(rowIdx, colIdx);
    }


    SetActiveCell(rowIdx, colIdx){
        this.#spdList.setActiveCell(rowIdx, colIdx);
    }


    /**
     * 그리드관리 정보에 기반한 Column Index 값 조회
     * 
     * @param {*} key  : Column Key
     */
    GetColumnIdx(key){
        return this.#colMap.get(key);
    }


    GetColumnName(colKey){

        let columnInfo = this.#columnInfo.filter((dt)=>{

            return dt["COLUMN_CODE"] == colKey;

        });

        return columnInfo[0]["COLUMN_NAME"];

    }


    fn_setGridIndex(data){
	
        var gridSettingList = JSON.parse(data);
        var gridRows = gridSettingList.rows;
        
        this.#columnInfo = gridRows;
    
        var inputMap = new Map();
        for(let item of gridRows){
            inputMap.set(item["COLUMN_CODE"], item["COLUMN_SEQ"]);
            inputMap.set(item["COLUMN_SEQ"], item["COLUMN_CODE"]);
        }
    
        return inputMap;
    }


    ps_setSpreadColumnIndex(){

        let columnInfoDetail = this.#columnInfo;
        let columnCount = columnInfoDetail.length;

        if(columnCount < 1){
            ps_setSpreadList_init(this.#spdList, 1, columnCount + 1, 0, 2);
        }else{
            ps_setSpreadList_init(this.#spdList, 1, columnCount + 1, 0, columnInfoDetail[0].FIXED_COLUMN_INDEX);
        }

        ps_setSpreadCol_CHECKBOX_Property(this.#spdList, 0, 'CENTER', 'CENTER', 8, false, false, "");

        for(var i = 0; i < columnCount; i++){

            let columnData = columnInfoDetail[i];

            this.#seqJson = this.#seqJson ?? {};
            this.#seqJson[columnData.COLUMN_CODE] = columnData.COLUMN_SEQ;

            let color = "";
            let wrapWord = false;
            let readOnly = true;

            if(columnData.COLUMN_READ_ONLY === 'N'){
                readOnly = false;
                color = '#FFFBF7';
                this.#spdList.getRange(0,-1, columnData.COLUMN_SEQ, 1).locked(false);
            }
            
            if(columnData.COLUMN_AUTO_LINE_CHANGE === 'Y'){
                readOnly = true;
            }

            switch(columnData.COLUMN_TYPE){
                case 'TEXT':
                    ps_setSpreadCol_TEXT_Property(this.#spdList, columnData.COLUMN_SEQ, 'CENTER', columnData.COLUMN_ALIGN, 8, wrapWord, readOnly, color);
                    break;
                case 'NUMBER':
                    ps_setSpreadCol_NUMBER_Property(this.#spdList, columnData.COLUMN_SEQ, 'CENTER', columnData.COLUMN_ALIGN, 8, wrapWord, readOnly, color);
                    break;
                case 'COMBO':
                    ps_setSpreadCol_COMBO_Property(this.#spdList, columnData.COLUMN_SEQ, 'CENTER', columnData.COLUMN_ALIGN, info.comboList[columnData.COLUMN_CODE]['COMBO'], false, "#FFFBF7");
                    break;
                case 'BUTTON':
                    ps_setSpreadCol_BUTTON_Property(this.#spdList, columnData.COLUMN_SEQ, 'CENTER', columnData.COLUMN_ALIGN, 8, false, true, color, '검색');
                    break;
            }

        }
    }


    /**
     *  Grid Set Style : colName, colHidden
     */
    ps_setSpreadStyle() {
        var columnInfoDetail = this.#columnInfo;
        var spd = this.#spdList;
        
        var columnCount = columnInfoDetail.length;
    
        ps_setSpreadTitle_Named(spd, 0,  0, '선택', 35, true);
        
        for(var i = 0; i < columnCount; i++){
            var columnData = columnInfoDetail[i];
            var visible = false;
            if(columnData.COLUMN_VISIBLE === 'Y'){
                visible = true;
            }
            ps_setSpreadTitle_Named(spd, 0,  columnData.COLUMN_SEQ, columnData.COLUMN_NAME,  columnData.COLUMN_SIZE, visible);
            
        }
    
        var setHearderRow = spd.getRange(0, -1, 2, -1, GC.Spread.Sheets.SheetArea.colHeader);
        setHearderRow.backColor("rgba(216, 226, 240, 0.4)");
        setHearderRow.foreColor("#666666");
        setHearderRow.font("bold 13px Arial");
    }


    RowAddEventCallBack(){

        let latRowIdx = this.#spdList.getRowCount();

        this.#spdList.addRows(latRowIdx, 1);
        this.#spdList.getCell(latRowIdx, this.#colMap.get("ITEM_CODE")).locked(false);
        this.#spdList.getCell(latRowIdx, this.#colMap.get("ITEM_NM")).locked(false);
        this.#spdList.getCell(latRowIdx, this.#colMap.get("QTY")).locked(false);
        this.#spdList.getCell(latRowIdx, this.#colMap.get("UOM")).locked(false);
        this.#spdList.getCell(latRowIdx, this.#colMap.get("UOM_CD")).locked(true);
    
    };


    RowDeleteAllEventCallBack(){

        this.#spdList.deleteRows(0, this.#spdList.getRowCount());

    }


    SaveEventCallBack(){

        
        let rowCnt = grid.spdList.getRowCount();

        // 1. Create Form Data & Condition Data Set
        let formData = new FormData(document.getElementById("frm_list"));

        try{

            if(!ViewComponent.Validation()){
                return ;
            }

            if(rowCnt == 0){
                throw `1개 이상 상품이 등록되어야 합니다.`;
            }
            
            formData.append('ROW_COUNT', rowCnt);
    
            // 2. Grid Data Set (Row Loop)
            for(let rowIdx=0; rowIdx < rowCnt; rowIdx++)
            {
                // Column Loop
                for(let colKey of grid.colMap.keys())
                {
                    if(typeof(colKey) == "number") continue;
    
                    let errorMsg = null;
                    let colIdx = grid.colMap.get(colKey);
                    let value = cfn_trim(grid.getValue(rowIdx, colIdx));
    
                    // Validatiaon 
                    switch(colKey){

                        case "ITEM_CODE" :           // 상품코드
                        case "RITEM_NM" :            // 상품명
                        case "UOM_CD" :              // 상품명
                            if(cfn_isEmpty(value)){
                                errorMsg = `공백을 허용하지 않는 항목이 존재합니다. (${this.GetColumnName(colKey)})`;
                            }
                            break;

                        case "IN_WORK_ORD_QTY" :    // 주문수량
                            if(cfn_isEmpty(value)){
                                errorMsg = `공백을 허용하지 않는 항목이 존재합니다. (${this.GetColumnName(colKey)})`;
                            }
                            else if(Number(value) <= 0){
                                errorMsg = `0 보다 큰 수만 입력할 수 있습니다. (${this.GetColumnName(colKey)} : ${value})`;
                            }

                            break;
                    }

                    if(!cfn_isEmpty(errorMsg)){
                        throw `[유효성검사(Grid)] ` + errorMsg;
                    }

                    formData.append(`I_${colKey}${rowIdx}`, value);
    
                    console.log(`Row(${rowIdx}) >> key${colIdx} : ${colKey} / customKey : I_${colKey}${rowIdx} / value : ${grid.getValue(rowIdx, colIdx)}`);
                }
            }
    
            // 3. Send
            jQuery.ajax({
                url : "/WMSOM020/saveInOrderOm.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(data){

                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){

                        throw data["MSG"];
                    }
                    // Success ...
                    else{
                        alert('저장되었습니다.');
                        window.opener.fn_searchDetail();
                        self.close(); 
                    }

                }
            })

        }
        catch(e){

            alert(e);
            return ;
        }
    }
}


//#region :: Event CallBack


/* 화주 팝업(WMSCM011) 후 처리 */
function selectedCustCallBack(arr){
	var info = arr[0];	
	jQuery('form[name="frm_list"] input[name="vrSrchCustCd"]').val(info.cust_cd);
	jQuery('form[name="frm_list"] input[name="vrSrchCustNm"]').val(info.cust_nm);
	jQuery('form[name="frm_list"] input[name="vrSrchCustId"]').val(info.cust_id);
}


/* 입고처 팝업 후 처리 */
function selectedInCustCallBack(arr){
	var info = arr[0];	
	jQuery('form[name="frm_list"] input[name="vrInCustCd"]').val(info.code);
	jQuery('form[name="frm_list"] input[name="vrInCustNm"]').val(info.name);
	jQuery('form[name="frm_list"] input[name="vrInCustId"]').val(info.id);
	
	jQuery('form[name="frm_list"] input[name="vrInCustAddr"]').val(info.addr);
	jQuery('form[name="frm_list"] input[name="vrInCustEmpNm"]').val(info.emp_nm);
	jQuery('form[name="frm_list"] input[name="vrInCustTel"]').val(info.tel);
}


/* 창고코드 팝업 후 처리 */
function selectedWHCallBack(arr){
	var info = arr[0];	
	jQuery('form[name="frm_list"] input[name="vrSrchWhCd"]').val(info.wh_cd);
	jQuery('form[name="frm_list"] input[name="vrSrchWhNm"]').val(info.wh_nm);
	jQuery('form[name="frm_list"] input[name="vrSrchWhId"]').val(info.wh_id);
}


/**
 * 상품 선택 후 처리
 * 
 * @param {*} arr 
 */
function selectedItemCallBack(arr){

    let selectedData    = arr;
    let ritemId         = null;
    let itemCd          = null;
    let itemNm          = null;
    let uomId           = null;
    let uomCd           = null;
    let uomNm           = null;

    if(!cfn_isEmpty(arr) && arr.length == 1){

        ritemId     = arr[0].ritem_id;
        itemCd      = arr[0].item_code;
        itemNm      = arr[0].item_kor_nm;
        uomId       = arr[0].uom_id;
        uomCd       = arr[0].uom_cd;
        uomNm       = arr[0].uom_nm;

        grid.setValue(grid.targetRow, grid.GetColumnIdx("ITEM_CODE"), itemCd);       // 상품코드
        grid.setValue(grid.targetRow, grid.GetColumnIdx("RITEM_NM"), itemNm);        // 상품명
        grid.setValue(grid.targetRow, grid.GetColumnIdx("RITEM_ID"), ritemId);       // 상품ID
        grid.setValue(grid.targetRow, grid.GetColumnIdx("UOM_CD"), uomCd);       		// UOM CD
        
    }
}


/**
 * 상품 바코드 선택 후 처리
 * 
 * @param {*} arr 
 * @returns 
 */
function selectedItemBarcodeCallBack(arr){

    if(cfn_isEmpty(arr)){
        return ;
    }
    
    let data = arr[0];

    if("bar_cd" in data) { grid.setValue(grid.targetRow, grid.GetColumnIdx("ITEM_BARCODE"), data["bar_cd"]); }     // 세부상품코드

    grid.SetActiveCell(grid.targetRow, 5);

}


//#endregion


//#region :: Custom Function 

function fn_Popup(type, id){
	var strParameter = [];
	var param = '';
	var url = '';
	var width = "800";
	var height = "530";
	var custCd_Text = "$!lang.getText('화주정보')";
	//팝업의 값에 따라 사용되는 팝업분기
	switch(type){

        // 화주
        case 'CUST':
			param = `?func=${selectedCustCallBack.name}&custType=12`;
			url = "/WMSCM011.action" + param;
            break;
        
        // 입고창고
		case 'WH':		
			param = `?func=${selectedWHCallBack.name}`;
			url = "/WMSMS040/pop.action" + param;
			break;

        // 입고처 (화주조회)
		case 'INCUST' : 
			if("" == jQuery("form[name='frm_list'] input[name='vrSrchCustCd']").val()){
				alert("$!message.getMessage('0가없음', '" + custCd_Text + "')");
				fn_Popup('CUST');
				return false;
			}else{
				strParameter.push('');
				strParameter.push('');
				strParameter.push('108');
				strParameter.push('');
				strParameter.push(jQuery("form[name='frm_list'] input[name='vrSrchCustId']").val());
				strParameter.push('1');
				param = `?func=${selectedInCustCallBack.name}&strParameter=${strParameter}`;
				url = "/TMSCM010.action" + param;
			}	
            break;	
	}
	var asppop = cfn_openPop2(url, "fn_Popup", width, height);
	asppop.focus();
}

//#endregion


//#region :: Grid Event CallBack


/**
 * Spread Sheet 버튼 클릭 이벤트
 * 
 * @param {*} args 
 */
let ps_setSpreadEvents_ButtonClicked = function(args) {

    let row = args.row;
    let col = args.col;

    grid.targetRow = row;
    grid.targetCol = col;

    switch(col)
    {
        // 상품정보 검색 컬럼
        case 3 :    ViewComponent.ItemSearchClickEventCallBack();   break;
    }

}


let ps_setSpreadEvents_CellClick = function(args) {

}


let ps_setSpreadEvents_CellChanged = function(args){

}


let ps_setSpreadEvents_CellDoubleClick = function(args){

    let row = args.row;
    let col = args.col;

    grid.targetRow = row;
    grid.targetCol = col;

    switch(col)
    {
        // 상품세부코드 컬럼
        case 5 :    ViewComponent.ItemBarCodeClickEventCallBack();  break;

    }

}


let ps_setSpreadEvents_LostFocus = function(args){

}


let ps_setSpreadEvents_Pasted = function(args){

}


//#endregion


let grid = new WinusGrid({ 
    spdListDiv      : "spdList",
    gridTemplate    : "WMSOM120pop"
});


jQuery(document).ready(function() {

    grid.Init();
    grid.SetGrid();

    ViewComponent.init();
    ViewComponent.AddEvent({
        'GRID' : grid
    });

});