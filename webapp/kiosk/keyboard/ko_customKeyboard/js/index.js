window.addEventListener('DOMContentLoaded', function() {
	
    var keyboardzone = document.getElementById("keyboardzone");
    var selectElement = null;
    
    const keyboard = new customKeyboard(
        keyboardzone,
        null,
        function () {},
        function () {},
        function fn_onEnter() {
        	console.log("m,.ml");
        	$('#header #submitBtn').click();
        },
    );
    /*
	    zone : 생성될 위치
	    input : 입력할 변수
	    onClick : 키보드가 눌렸을때 동작
	    onESC : 뒤로 눌렸을때 동작
	    onEnter : enter 눌렀을때 동작
	    form : 키보드의 모습
	*/

    let inputElements = document.getElementsByClassName('keyInput');
    for (let i = 0; i < inputElements.length; i++) {
    	if(selectElement == null){chkSelectElement();}
        inputElements[i].addEventListener("click", function () {
            keyboard.setInput(inputElements[i]);
        })
    }
    
    function chkSelectElement() {
		var a = $('article').length;
		if(a == 0){
			selectElement = $('#header .keyInput')[0];
		}else{
			var $articleId = $('#main').children('article').hasClass('active').id;
			if ($articleId == "" || $articleId == undefined ){
				selectElement = $('#header .keyInput')[0];
			}else{
				selectElement = $('#main '+ $articleId +' .keyInput')[0];
			}
		}
		selectElement.click();
	}
    
});
