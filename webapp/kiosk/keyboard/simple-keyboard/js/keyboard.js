jQuery(document).ready(function() {
	
	let Keyboard = window.SimpleKeyboard.default;
	let selectElement  = null;
	let selectElementId = null;
	
	jQuery(".keyInput").click(function(e){
		if(selectElementId != jQuery(this).attr('id')){
			jQuery(this).val('');
			keyboard.clearInput();
		}
		selectElementId = jQuery(this).attr('id');
		selectElement = jQuery(this);
	});

	
	let keyboard = new Keyboard({
	  onChange: input => onChange(input),
	  onKeyPress: button => onKeyPress(button)
	});
	
	
	/**
	 * Update simple-keyboard when input is changed directly
	 */
	document.querySelector('.keyInput').addEventListener("click", event => {
		keyboard.setInput(event.target.value);
	});
	
	/*let inputElements = document.getElementsByClassName('keyInput');
	for (let i = 0; i < inputElements.length; i++) {
		if(selectElement == null){chkSelectElement();}
		
		inputElements[i].addEventListener("click", function (event) {
			console.log(event.target.value);
            keyboard.setInput(event.target.value);
        })
	}*/

	function onChange(input) {
		document.querySelector("#"+selectElementId).value = input;
	    //console.log("Input changed", input);
	}

	function onKeyPress(button) {
		/**
		 * If you want to handle the shift and caps lock buttons
		 */
		if(selectElement == null){chkSelectElement(event);}
		console.log(selectElement);
		console.log(selectElementId);
		if (button === "{shift}" || button === "{lock}") handleShift();
		//console.log("Button pressed", button);
	}
	
	function chkSelectElement(event) {
		if(selectElement == null){
			var a = $('article').length;
			if(a == 0){
				console.log(a);
				selectElement = jQuery('#header .keyInput')[0];
				selectElementId = jQuery('#header .keyInput')[0].id;
			}else{
				var $articleId = $('#main').children('article').hasClass('active').id;
				if ($articleId == "" || $articleId == undefined ){
					selectElement = $('#header .keyInput')[0];
					selectElementId = $('#header .keyInput')[0].id;
				}else{
					selectElement = $('#main '+ $articleId +' .keyInput')[0];
					selectElementId = $articleId;
				}
			}
			jQuery("#"+selectElementId).click;
		}
	}
	
	function handleShift() {
	  let currentLayout = keyboard.options.layoutName;
	  let shiftToggle = currentLayout === "default" ? "shift" : "default";

	  keyboard.setOptions({
	    layoutName: shiftToggle
	  });
	}
	

});