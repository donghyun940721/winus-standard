# 📍 Branch

* release-winuall : WINUS-ALL 배포 브랜치
* main-winusall : WINUS-ALL 메인 브랜치 (WINUS('main') 에서 취합된 배포 버전 관리)
* main : WINUS 통합 테스트 환경 브랜치
* shared-airov : 협력사 ('아이로브')의 메인 브랜치 (신규 개발 항목)


<br>

# ⚙ Config

* **config.properties** : DB 커넥션 정보 수정 및 프로젝트의 로컬 참조경로 수정.
* **config-local.properties** : DB 커넥션 정보 수정 및 프로젝트의 로컬 참조경로 수정.
* **context.xml** : Eclipse 내장 톰켓의 DB 커넥션 정보 수정.
* **web.xml** : Eclipse 내장 톰켓에 외부변수 추가 (config-local.properties를 참조하게 하기 위함)

```xml
<context-param>
    <param-name>spring.profiles.active</param-name>
    <param-value>local</param-value>
</context-param>
```

* **VM arguments** 추가 : Eclipse 프로젝트 Debug Configurations에서 Arguments 탭 

```
-Dspring.profiles.active=local
```

<br>

# 🔨 Local Test Setup

**1. JSP 파일 호출 부 경로 수정**

**[예시: WMSOP020E8.vm 中]**
"iframe" 태그 중 (\*.jsp) 파일 경로 수정 ( "/jsp/fileuploadMt.jsp" → "../../../jsp/fileuploadMt.jsp" )

```HTML
<iframe name="filechecklist" src="../../../jsp/fileuploadMt.jsp" style="visibility : hidden ; width : 0 ; height : 0" ></iframe>
<input type="hidden" name='D_ATCH_FILE_ROUTE' id='D_ATCH_FILE_ROUTE' value=''/>
<input type="hidden" name='D_ATCH_FILE_NAME' id='D_ATCH_FILE_NAME' value=''/>
```

<br>

